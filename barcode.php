<?php 

$lib = "application/third_party/emberlabs/Barcode";

require_once("$lib/BarcodeBase.php");
require_once("$lib/Code128.php");
require_once("$lib/Code39.php");


$width = isset($_GET['width'])?$_GET['width']:200;
$height = isset($_GET['height'])?$_GET['height']:40;
$barcode = isset($_GET['barcode'])?$_GET['barcode']:"No Barcode";
$type = isset($_GET['type'])?$_GET['type']:"code39";

//if you use 128 it has to be an even number so it will pad the barcodes with a leading zero
if($type=='code39'){
    $barcodeObj = new emberlabs\Barcode\Code39();
} elseif($type=='code128') {
    $barcodeObj = new emberlabs\Barcode\Code128();
}
$barcodeObj->setData($barcode);
$barcodeObj->setDimensions($width, $height);
$barcodeObj->setFontSize(5);
$barcodeObj->enableHumanText(true);
$barcodeObj->display();
