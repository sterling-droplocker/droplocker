$(document).ready(function(){
	
	$('#reassign_button').click(function(){
		$(this).css('display', 'none');
		$('#reassign_submit').css("display", "inline");
		//get_customer_bags(customer_id);
		get_location_lockers(location_id);
		var current_customer = $('#customer_input').html();
		$('#customer_input').html("<input type='input' name='customer' id='customer' value='"+current_customer+"' />");
		var current_location = $('#location_input').html();
		$('#location_input').html("<input type='input' name='location' id='location' value='"+current_location+"' />");
	
		if($('#customer').length!=0){
   		$('#customer').autocomplete({
		//source: '/ajax/customer_autocomplete/',
		source: function(req, add){  
  
                //pass request to server  
                $.getJSON("/ajax/customer_autocomplete/", req, function(data) {  
  					console.log(data);
  					dc=data;
                    //create array for response objects  
                    var suggestions = [];  
  
                    //process response  
                    $.each(data, function(i, val){  
                    	suggestions.push(data[i]); 
                    	 
                	}); 	 
  
                //pass array to callback  
                add(suggestions)
           })
          },
		appendTo: "#customer_results",
		select : function(e, ui){
			$.each(dc, function(i, val){  
                if(ui.item.value==val){
                $('#customer_id').val(i); 
                //need to get the bags for this customer
                	//get_customer_bags(i);
                }
            }); 
		}
		});
		}
	
	
	
	if($('#location').length!=0){
   		$('#location').autocomplete({
		//source: '/ajax/customer_autocomplete/',
		source: function(req, add){  
  
                //pass request to server  
                $.getJSON("/ajax/location_autocomplete/", req, function(data) {  
  					console.log(data);
  					dc=data;
                    //create array for response objects  
                    var suggestions = [];  
  
                    //process response  
                    $.each(data, function(i, val){  
                    	suggestions.push(data[i]); 
                    	 
                	}); 	 
  
                //pass array to callback  
                add(suggestions)
           })
          },
		appendTo: "#location_results",
		select : function(e, ui){
			$.each(dc, function(i, val){  
                if(ui.item.value==val){
                $('#location_id').val(i); 
               
                	get_location_lockers(i);
                }
            }); 
		}
		});
		}
	});
	
	function get_customer_bags(customer_id){
		$.ajax({
    		url:'/ajax/get_bags_of_customer',
    		data: {"customer_id": customer_id},
    		type:"post",
    		success:function(data){
    			if(data){
    			var select = $("<select id='bag_id' name='bag_id'></select>");
    				$.each(data, function(i,item){
    					console.log(item);
    					select.append("<option value='"+item.bagID+"'>"+item.bagNumber+"</option>");
    				})
    			}
    			//with the data, we need to create a dropdown of available bags
    			$('#dd_bag').html(select);
    			//selectValueSet('bag_id', bag_id);
    		},
    		dataType:'json'
    	});
	}
	
	function get_location_lockers(location_id){
		$.ajax({
			url:'/ajax/get_lockers_at_location',
			data: {"location_id": location_id},
			type:"post",
			success:function(data){
				if(data){
				var select = $("<select id='locker_id' name='locker_id'></select>");
					$.each(data, function(i,item){
						console.log(item);
						select.append("<option value='"+item.lockerID+"'>"+item.lockerName+"</option>");
					})
				}
				//with the data, we need to create a dropdown of available bags
				$('#dd_locker').html(select);
				selectValueSet('locker_id',locker_id);
			},
			dataType:'json'
		});
	}
	
	
});
