$(document).ready(function(){
	var url = '';
	
	
	var map;
	var markers = new Array();
	var points = new Array();
	var routePath;
	var colors = new Array('cc0000','ccff00','3698fb','6ed0f7','FF1493','87b93e','CD69C9','CD5C5C','FFA500','9370DB','90EE90','CAFF70','EE7621','FF1493','FFD39B','FFFF00');	
	var markerstr = '';
	var lines = new Array();
	var driver_notes;
	var route_arr;
	var type;
	var d;
	var currentRoutes = new Array();
	var bounds = new google.maps.LatLngBounds();
	//routes = '';
	$.each(routes,function(i,item){
		currentRoutes.push(parseInt(item));
	});
	
	display_map();
	
	// Starts here with a selection of one or more routes
	$('#select_route').click(function(){
		//alert();
		//currentRoutes = new Array();
		
		$(".routeID:" + "[checked]").each(function(i,item){
			if(!isNaN($(item).val()))
			url = url + $(item).val() + "-";
		});
		url = url.substring(0, url.length-1);
		top.location.href="http://"+domain+"/admin/reports/driver_map/"+url;
		//alert(url);
       	//update_map();
	}); 
	
	//sortable list
	function summary(){
		var summaryPanel = document.getElementById("sortable");
	      summaryPanel.innerHTML = "";
	      for (var i = 0; i < points.length; i++) {
	        var routeSegment = i+1;
	        summaryPanel.innerHTML += "<li id='item_"+i+"'><a href='javascript:;'><table cellspacing='2' cellpadding='2' width='100%' style='margin:1px;padding:2px;outline:1px solid #CCC;background-color:#"+colors[points[i].route_id]+"'><tr><td align='center' width='30' style='font-size:18px;'>"+routeSegment+"</td><td style='padding:-5px;margin:-5px' valign='top'>"+points[i].street+"</td><td align='center' width='20'><a href='javascript:;' class='del_route_button' id='del"+points[i].drop_id+"'><img src='/images/icons/delete.png' /></td></tr></table></a></li>";
	      }
	}
	
	
	//adds lines between markers
	function route(){
	    lines = [];
	    for(var key in points) {
	        lines.push(points[key].location);
	    }
	    if(routePath) {
	        routePath.setPath(lines);
	    } else {
	    	
	        routePath = new google.maps.Polyline({
	            path: lines,
	            strokeColor: "#FF0000",
	            strokeOpacity: 1.0,
	            strokeWeight: 3
	        })
	        routePath.setMap(map);
	    }
	}
	
	
	//opens a new window with a layout for printing
	$('#print').click(function(){
		
		if($(this).text()!='Print Route')
		return false;
		
		var m = '[';
		$.each(points, function(i,item){
			m += '{"sort_id":"'+i+'","drop_id":"'+item.drop_id+'","lat":"'+item.location.lat()+'","lng":"'+item.location.lng()+'","street":"'+item.street+'","city":"'+item.city+'","route_id":"'+item.route_id+'"},';
		});
		m += "]";
		m = m.replace(",]","]");
		var l = '[';
		$.each(lines, function(i,item){
			l += '{"lat":"'+item.lat()+'","lng":"'+item.lng()+'"},';
		});
		l += "]";
		l = l.replace(",]","]");
		
		//passed from the php page (md5 string)
		var unique = hash;
		var left_column = escape($('#left_column').html());
		var right_column = escape($('#right_column').html());
		
		$.ajax({
	          type: 'POST',
	          data: "ID="+unique+"&lines="+l+"&markers="+m+"&left_column="+left_column+"&right_column="+right_column,
	          url: "/admin/reports/dm_store_map",
	          success: function(data){
	          	if(data.status=='success'){
	          		window.open("http://"+domain+"/admin/reports/print_map/"+unique,'print_map','width=820,height=800');
	          	}
	          },
	          dataType: 'json'
	        });
		
	});


	/*** Standard Mapping Functions Below ***/
	var bounds;
	
	function update_map(){
		
		$.each(locations.routes,function(a,route){
	        	//alert(currentRoutes.indexOf(parseInt(a)));
	        	if(currentRoutes.indexOf(parseInt(a))>-1){
		        	$.each(route.dropLocations,function(b,location){
		        		add_marker(location.dropLocationAddress,a,false);
		        	});
	        	}
	    
	  	//map.fitBounds(bounds);
		});
	}
	
	function display_map(){
		bounds = new google.maps.LatLngBounds();
		var latlng = new google.maps.LatLng(37.780929, -122.413582);
	       var myOptions = {
	          zoom: 13,
	          center: latlng,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	        
	        //$.each(currentRoutes,function(i,item){alert(item)});
	        $.each(locations, function(r, route){
	        	$.each(route.sortOrder,function(i, item){
					points.push({
						id:i,
						route_id:item.route_id,
			          	location:new google.maps.LatLng(item.lat, item.lon),
			         	street:item.street,
			          	city:item.city,
			          	accessCode:item.accessCode,
			          	location_id:item.locationID,
			          	sort:item.sortOrder
			      		});	
					
			      	add_marker(points[i], item.route_id, false);
		        });
	        });
	        
	        
	        
	        route();
	        map.fitBounds(bounds);
	        
	}
	
	function add_marker(point,route_id,icon){
		//alert(point.lon);
		var count = 0;
		var latlng = point.location;
		if(icon){
			var mark = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+(count+1)+'|'+colors[route_id]+'|000000';}
			else{
			var mark = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+point.sort+'|'+colors[route_id]+'|000000';
			}
		var marker = new google.maps.Marker({
	      position: latlng, 
	      map: map, 
	      title:point.street,
	      icon: mark
	  });   
	  bounds.extend(latlng);
	  markers.push(marker);
	}
	
	function clear_markers(){
		for(var i=0; i < markers.length; i++){
	        markers[i].setMap(null);  
	    }
	    markers = new Array();
	}
	
	
	$('#listview').sortable({
		cursor: 'hand',
		update:function(event, ui){
			var out = $("#listview").sortable('toArray');
			//alert(out);
			clear_markers();
		}
	});
	
	
});


