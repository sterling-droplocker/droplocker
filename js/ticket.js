$(document).ready(function(){
   $('#order_button').click(function(){
       if($('#order_id').val()==''){
	   alert("Missing Order ID");
	   return false;
       }
       $.ajax({
	   url:'/admin/tickets/get_ticket_order_ajax',
	   type:"post",
	   data: {"orderID":$('#order_id').val()},
	   success:function(data){
	       if(data.status=='success'){
		   $('#bag').val(data.bagNumber);
		   $('#customer').val(data.firstName+" "+data.lastName);
		   $('#customer_id').val(data.customer_id);
		   $('#location').val(data.location);
		   $('#location_id').val(data.location_id);
		   $('#locker').val(data.locker);
		   $('#locker_id').val(data.locker_id);
	       }
	   },
	   dataType:"json",
       });
   });
   
   
   $("#add_note_button").live('click', function(){
   		$('#note_form').css('display', 'block');
   });
   
   $('.ticket_del').click(function(){
       
       if(!verify_close_ticket()){
	   return false;
       }
       
       var id = $(this).attr('id');
       id = id.replace('del-', '');
       
       $.ajax({
	   url:'/admin/tickets/delete_ticket_ajax/',
	   type:'post',
	   data:{"delete":"delete", "ticketID":id},
	   dataType:'JSON',
	   success:function(data){
	       top.location.href = '/admin/tickets';
	   }
       });
       
   });
   
   
    var dc = null;
    $('#customer').autocomplete({
        source: function(req, add){  

            //pass request to server  
            $.getJSON("/ajax/customer_autocomplete_aprax/", req, function(data) {  
                                    console.log(data);
                                    dc=data;
                //create array for response objects  
                var suggestions = [];  

                //process response  
                $.each(data, function(i, val){  
                    suggestions.push(data[i]); 

                    }); 	 

                //pass array to callback  
                add(suggestions);
            });
        },
        appendTo: "#customer_results",
        select : 
            function(e, ui) {
                $.each(dc, function(i, val){               
                    $('#customer_id').val(ui.item.customerID);                           
                    $('#email').val(ui.item.email);
                    $('#phone').val(ui.item.phone);
                    var linkStr = '<a target="_blank" href="/admin/customers/detail/'+ui.item.customerID+'">Customer Info</a></br></div>';
                    $('#customerInfo').html(linkStr);
                }); 
            }
    });
		
    var dl = null;
    $('#location').autocomplete({
        //source: '/ajax/customer_autocomplete/',
        source: function(req, add){  
            //pass request to server  
            $.getJSON("/ajax/location_autocomplete/", req, function(data) {                                    
                dl=data;
                //create array for response objects  
                var suggestions = [];  
                //process response  
                $.each(data, function(i, val){  
                    suggestions.push(data[i]); 
                }); 	 
                //pass array to callback  
                add(suggestions);
            });
        },
        appendTo: "#location_results",
        select : 
            function(e, ui){
                $.each(dl, function(i, val){  
                    if(ui.item.value==val) {
                        $('#location_id').val(i); 
                    } 	 
                }); 
            }
    });
    
});
