jQuery(document).ready(function($) {

  TestimonialRotator = {
    init: function(){
      var $el = $('footer .mb-testimonial li');
      var fadeOutTime = 2000;
      var fadeInTime = 2000;
      var showTime = 5000;
      var currentItem = 0;
      var items = $el.length;
      var initialFadeTime = 1000;
      $el.eq(currentItem).fadeIn(initialFadeTime);
      setInterval(function(){
        $el.eq(currentItem).fadeOut(fadeOutTime);
        if(items - currentItem == 1) currentItem = 0;
        else currentItem++;
        $el.eq(currentItem).fadeIn(fadeInTime);
      }, showTime);
    }
  } 
  TestimonialRotator.init();
});