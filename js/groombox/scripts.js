// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
  window.getComputedStyle = function(el, pseudo) {
    this.el = el;
    this.getPropertyValue = function(prop) {
      var re = /(\-([a-z]){1})/g;
      if (prop == 'float') prop = 'styleFloat';
      if (re.test(prop)) {
        prop = prop.replace(re, function () {
          return arguments[2].toUpperCase();
        });
      }
      return el.currentStyle[prop] ? el.currentStyle[prop] : null;
    }
    return this;
  }
}

jQuery(document).ready(function($) {

/** Detect support for media queries (all size and browser) and add a polyfill for browser that don't support it
    Find the polyfill under http://code.google.com/p/css3-mediaqueries-js/
    This required the modernizr load to be in the build
**/
  Modernizr.load([{
    test : Modernizr.mq('only all'),
    yep  : '',
    //nope : '/js/groombox/libs/css3-mediaqueries.js'
    nope : '/js/groombox/libs/respond.js'
  }]);

  if ($('.page-ccm').length) {
    $('.tab-trigger').click(function() {
      var $tab = $(this).parent('.tab'),
          $tabCurrent = $('.tab.collapsed');
      if ($tab.hasClass('collapsed')) {
        return;
      }
      $tabCurrent.siblings().stop().css('width', '80px');
      $tabCurrent.animate({
        'width': '80px'
      }, function() { $tabCurrent.removeClass('collapsed'); });
      $tab.addClass('collapsed').animate({
        'width': '760px'
      });
    });
  }

  if ($('.field-fake-required').length)
  {
    $('.ninja-forms-form').on('change', '.field-fake-required-toggler', function() {
      var val = $(this).val();
      var $fieldRequired = $('.field-fake-required');

      if (val === 'Votre entreprise')
      {
        if ($fieldRequired.val() === '-') {
          $fieldRequired.val('');
        }
        $fieldRequired.removeAttr('disabled');
      }
      else
      {
        $fieldRequired.val('-').attr('disabled', 'disabled');
      }
    });
  }

  var _footerPermute = function() {
    var responsiveViewport = $(window).width(),
        $columns = $('#footer dl');
    if (responsiveViewport < 1000)
    {
      $columns.css('height', 'auto');
    }
    else
    {
      var colMaxHeight = 0;
      $columns.each(function()
      {
        var height = $(this).height();
        if (height > colMaxHeight)
        {
          colMaxHeight = height;
        }
      });
      $columns.css('height', colMaxHeight);
    }
  }

  _footerPermute();
  $(window).resize(_footerPermute);

  var $submenus = $('#nav-main .sub-menu');
  $('#nav-main li').hover(function() {
    if ($("> ul", this).length > 0)
    {
        $("> ul", this).show();
    }
  }, function() {
    if ($('> ul', this).length > 0)
    {
      $('> ul', this).hide();
    }
  });
});


/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
  // This fix addresses an iOS bug, so return early if the UA claims it's something else.
  if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
  var doc = w.document;
  if( !doc.querySelector ){ return; }
  var meta = doc.querySelector( "meta[name=viewport]" ),
    initialContent = meta && meta.getAttribute( "content" ),
    disabledZoom = initialContent + ",maximum-scale=1",
    enabledZoom = initialContent + ",maximum-scale=10",
    enabled = true,
    x, y, z, aig;
  if( !meta ){ return; }
  function restoreZoom(){
    meta.setAttribute( "content", enabledZoom );
    enabled = true; }
  function disableZoom(){
    meta.setAttribute( "content", disabledZoom );
    enabled = false; }
  function checkTilt( e ){
    aig = e.accelerationIncludingGravity;
    x = Math.abs( aig.x );
    y = Math.abs( aig.y );
    z = Math.abs( aig.z );
    // If portrait orientation and in one of the danger zones
    if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
      if( enabled ){ disableZoom(); } }
    else if( !enabled ){ restoreZoom(); } }
  w.addEventListener( "orientationchange", restoreZoom, false );
  w.addEventListener( "devicemotion", checkTilt, false );
})( this );
