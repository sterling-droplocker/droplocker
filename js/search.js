$(document).ready(function(){
   var bag_id = '';
   var order_id = '';
   var ticketNum = '';
   var customer_id = '';
   var dc = null;
   if($('#customer').length!=0){
   		$('#customer').autocomplete({
		//source: '/ajax/customer_autocomplete/',
		source: function(req, add){  
  
                //pass request to server  
                $.getJSON("/ajax/customer_autocomplete/", req, function(data) {  
  					console.log(data);
  					dc=data;
                    //create array for response objects  
                    var suggestions = [];  
  
                    //process response  
                    $.each(data, function(i, val){  
                    	suggestions.push(data[i]); 
                    	 
                	}); 	 
  
                //pass array to callback  
                add(suggestions)
           })
          },
		appendTo: "#customer_results",
		select : function(e, ui){
			$.each(dc, function(i, val){  
                if(ui.item.value==val) 
                $('#customer_id').val(i);  	 
            }); 
		}
		});
	}
	
	$("#locker_search_button").live('click', function(){
		locker = $('#locker').val();

		$.ajax({
			url:"/ajax/search_lockers/",
			type:"post",
			data:{"lockerName":locker,
				  "submit":true},
			dataType:"json",
			success:function(data){
				if(data.length > 0){
				var table = $("<table id='box-table-a'></table>");
				table.append("<thead><tr><th>Street</th><th>Locker</th><th>Type</th><th>Service Type</th><th>Service Method</th><th>Status</th><th>Edit</th><th>history</th></tr></thead>");
				$.each(data, function(i, item){
					table.append("<tbody><tr><td>"+item.address+"</td><td>"+item.lockerName+"</td><td>"+item.type+"</td><td>"+item.style+"</td><td>"+item.locationTypeName+"</td><td>"+item.status+"</td><td width='25' align='center'><a href=''><img src='/images/icons/application_edit.png' /></a></td><td align='center' width='25'><img src='/images/icons/calendar.png' /></a></td></tr></tbody>")
				});
				}
				else{
					var table = "No Orders Found";
				}
				$('#results').html(table);
			}
		});
		
	});
	
	$("#customer_search_button").live('click', function(){
		var firstName= $('#firstName').val();
		var lastName= $('#lastName').val();
		var username= $('#username').val();
		var address= $('#address').val();
		var phone= $('#phone').val();
		var email= $('#email').val();

		$.ajax({
			url:"/ajax/search_customers/",
			type:"post",
			data:{"firstName":firstName,
				  "lastName":lastName,
				  "username":username,
				  "address":address,
				  "phone":phone,
				  "email":email,
				  "submit":true},
			dataType:"json",
			success:function(data){
				if(data.length > 0){
				var table = $("<table id='box-table-a'></table>");
				table.append("<thead><tr><th>First Name</th><th>Last Name</th><th>Address</th><th>Phone</th><th>Email</th><th>Business</th></tr></thead>");
				$.each(data, function(i, item){
					table.append("<tbody><tr><td><a href='/admin/customers/detail/"+item.customerID+"'>"+item.firstName+"</td><td>"+item.lastName+"</td><td>"+item.address+" "+item.address2+"</td><td>"+item.phone+"</td><td>"+item.email+"</td><td></td></tr></tbody>")
				});
				}
				else{
					var table = "No Orders Found";
				}
				$('#results').html(table);
			}
		});
		
	});
});
