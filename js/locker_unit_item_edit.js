$(document).ready(function(){
	
	var clickspan;
	var span;
	var text;
	var input;
	var pid;
	var field;
	$('.click').live('click',function(){
		var bid = $(this).attr('id');
		bid = bid.split("-");
		if(span){
			span.replaceWith(text);
			clickspan.addClass('click');
		}
		
		pid = bid[4];
		field = bid[0];
		clickspan = $(this).removeClass('click');
		clickspan.unbind();
		text = $(this).text();
		span = $("<span></span>");
		
		//set up a text input or a dropdown
		if($(this).hasClass('text')){
			if(field=='name'){var width='70%';} else {var width='50px';}
			input = $("<input style='width:"+width+"' type='text' name='name' />").val(text);
		}
		else{
			input = select;
		}
		
		var accept = $("<a class='save action' href='javascript:;'><img src='"+cdnimages+"icons/accept.png' /></a>");
		var cancel = $("<a class='revert action' href='javascript:;'><img src='"+cdnimages+"icons/cancel.png' /></a>");
		$(this).html(span.append(input).append($('<span id="btngrp"></span>')).append(accept).append(cancel));
		
	});
	
	$('.save').live('click',function(){
		$('#btngrp').html("<img src='"+cdnimages+"progress.gif' />");
		$('.action').css('display','none');
		value = input.val();
		$.ajax({
          type: 'POST',
          data: "productID="+pid+"&"+field+"="+value,
          url: "/admin/products/update_product_ajax/",
          success: function(data){
              if(data.status == 'success'){
              	  span.replaceWith(value);
				  clickspan.addClass('click');
                  return false;
              }
              else{
              	$('#btngrp').html("");
				$('.action').css('display','inline');
              	alert("Error: "+data.message);
              }
              
          },
          dataType: 'json'
        });
		
		
	});
	
	
	$('.revert').live('click',function(){
	
		if(span){
			span.replaceWith(text);
			clickspan.addClass('click');
		}
	});
	
})
