$(document).ready(function(){
    $('.img_button').live('click',function(){
        var $img = $(this);
        $img.attr("src", "/images/progress.gif");
        var c = $(this).attr('id');
        c = c.split("-");
        var business_id = c[0];
        var resource_id = c[1];
        var type_id = c[2];
        var type = c[3];

        $.ajax({
            type:"POST",
            url:"/admin/admin/update_role_ajax",
            data:"business_id="+business_id+"&resource_id="+resource_id+"&type_id="+type_id+"&type="+type,
            success:function(data){

                if(data=='allow')
                    $img.attr("src", "/images/icons/accept.png");
                else
                    $img.attr("src", "/images/icons/delete.png");
            }
        });
    });
});
