//-- Globals




// click handler, 
// individual items
$('p.question').click(function() {
  if ($(this).parent().hasClass('active')) {
    $(this).parent().removeClass('active');
  }
  else {
    $(this).parent().addClass('active');
  }
});

$('.exitSp').click(function() {
  $('.overlaySp').fadeOut('fast', function() {
    $('body').css('overflow', 'visible');
  });
});

$(".overlaySp").click(function(){
  $(this).fadeOut('fast', function() {
    $('body').css('overflow', 'visible');
  });
});
$(".overlaySp").children().click(function(e) {
  e.stopPropagation();
});


function requestServe() {
  $.scrollTo('0px', 300, function() {
    $('.overlaySp').fadeIn('fast', function() {
      $('body').css('overflow', 'hidden');
    });
  });
  
}


//-- contact form
function contactSuccess() {
    $('#contactFrm').fadeOut('slow', function() {
        $(this).replaceWith('<br><br><p class="formResponse">Thank You for Contacting Us.</p>').fadeIn();
        l.stop();
    });
}
function contactFailure() {
    if ($('.errorBig').length > 0) {
      $('.errorBig').fadeOut('slow', function() {
        $(this).remove();
        $('#contactFrm').append('<p class="errorBig" class="formResponse">Oops, something went wrong. Please try again.</p>').fadeIn();
        l.stop();
      });
    }
    else {
      $('#contactFrm').append('<p class="errorBig" class="formResponse">Oops, something went wrong. Please try again.</p>').fadeIn();
      l.stop();
    }
}
var submitContact = function() {
  l.start();
    var senddata = $('#contactFrm').serialize();
    $.ajax({
      url: ajaxUrl + '/register-process.php',
      type: 'post',
      data: senddata,
      dataType: 'script',
      success: contactSuccess,
      error: contactFailure
    });
};

// Contact Form : Value Prefills
$.fn.ToggleInputValue = function(){
  return $(this).each(function(){
      var Input = $(this);
      var default_value = Input.val();

      Input.focus(function() {
         if(Input.val() == default_value) Input.val("");
      }).blur(function(){
          if(Input.val().length == 0) Input.val(default_value);
      });
  });
}

function watchVal() {
  $('input.prefill').ToggleInputValue();
  $('textarea.prefill').ToggleInputValue();
}

/* ==========================================================================
   Gmaps API v3 
   ========================================================================== */

var all = [
  {'mtype': 'privateRes', 'lat': '43.636475', 'lng':  '-79.402061', 'name': '<span class="mapfirstline">West Harbour City Phase 2</span><br/><span class="mapsecondline"> 21 Grand Magazine St<br></span><span class="mapthirdline">Locker Service<br>Available 24HRS</span><br><img class="mapImg" src="' + ajaxUrl + '/img/map_markers_temp/image.png" />'}
];

var privateRes = [
  {'mtype': 'privateRes', 'lat': '43.636475', 'lng':  '-79.402061', 'name': '<span class="mapfirstline">West Harbour City Phase 2</span><br/><span class="mapsecondline"> 21 Grand Magazine St<br></span><span class="mapthirdline">Locker Service<br>Available 24HRS</span><br><img class="mapImg" src="' + ajaxUrl + '/img/map_markers_temp/image.png" />'}
];

var commercial = [

];

var publicDepot = [

];




var base = '/';
var data_set = "all";

function runGmap() {
    var mapDiv = document.getElementById('gmap');
    var aMap = new AmenityMap(mapDiv, all);
    aMap.init();
            
    $('#clear_points').bind('click', function() {
       aMap.killMarker();
    });

    $('.amenitybutton').on('click', function() {
        $('.current').removeClass('current');
        $(this).addClass('current');

        var arrow_name = "#arrow_" + $(this).attr('id');    
        aMap.killMarker();

        data_set = $(this).attr('href');
        switch(data_set)   {
            case '#private':
              aMap.points = privateRes;
              break;
            case '#commercial':
              aMap.points = commercial;
              break;
            case '#public-depot':
              aMap.points = publicDepot;
              break;
            case '#all':
              aMap.points = all;
              break;
           default:
              aMap.points = all;
        }
        aMap.setPoints();
        $.scrollTo('.aMap', 300);
    });
}

function AmenityMap(map_div, my_points) {
    this.map_div = map_div;
    this.initial_latlng = new google.maps.LatLng(43.636475, -79.402061);
    this.map;
    this.points = my_points;
    this.marker_array = [];
    this.ibArray = [];
    this.ibCount = 0;
}

AmenityMap.prototype = {
    'init': function() {
        var self = this;
        var opts = {
            scrollwheel: false,
            zoom: 16,
            center: this.initial_latlng,
            mapTypeControl: false,
              // mapTypeControlOptions: {
              //   style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
              // },
              zoomControl: true,
              zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
              },
              panControl: false,
              streetViewControl: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
                  styles: [ 
                      {
                          featureType: "all",
                          stylers: [
                            { saturation: -100 }
                          ]
                      }
                  ]
        };
        this.map = new google.maps.Map(self.map_div, opts);
        this.setPoints();
    },
    'setPoints' : function() {
        for (var i = 0; i < this.points.length; i++) {
            var pt = this.points[i];
            var point = this.createPoint(pt.lat, pt.lng);
            var marker = this.createMarker(point, pt);
            this.marker_array[i] = marker;
        }
        var image = new google.maps.MarkerImage(ajaxUrl + '/img/marker.png',
                    new google.maps.Size(98,40),
                    new google.maps.Point(0,0)
                );
        var myLatLng = new google.maps.LatLng(43.64925, -79.37421);
        var mainMarker = new google.maps.Marker({
                position: myLatLng,
                map: this.map,
                icon: image,
                zIndex: 1
        });
        this.marker_array[this.points.length] = mainMarker;
        this.addMarkersToMap();
    },
    'addMarkersToMap': function() {
        if (this.marker_array.length > 0) {
            for (var i = 0; i < this.marker_array.length; i++) {
                this.marker_array[i].setMap(this.map);
            }
        }
    },
    'createPoint': function(lat, lng) {
        var point = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
        return point;
    },
    'createMarker': function(point, siteObj) {
            var self = this;

            var marker = new google.maps.Marker({
              position: point,
              map: this.map,
              icon: ajaxUrl + '/img/pinoff.png',
              visible: true,
              zIndex: 2
            });


            

            var boxText2 = document.createElement("div");
            var offset_box = new google.maps.Size(-32, -355);
                
          
                boxText2.style.cssText = "font-size: 20px; padding: 16px 0 0 0; background-color:#000000; color: #ffffff; font-family: 'novecento_wide_bookbold'; max-width: 300px;";
                boxText2.innerHTML = '' + siteObj.name + '';
                offset_box = new google.maps.Size(22, -41);
            
            
            var myOptions2 = {
                content: boxText2
                ,pixelOffset: offset_box
                ,boxStyle: {opacity: 1}
                ,closeBoxURL: ajaxUrl + "/img/mapExit.png"
                ,closeBoxMargin: "2px 2px 7px 0px" //add these margins in to add the close button
            };

            var ib2 = new InfoBox(myOptions2);
            
            this.ibArray[this.ibCount] = ib2;
            this.ibCount = this.ibCount + 1;
            
            var ibTemp = this.ibArray;      
            var mktTemp = this.marker_array;

            // Add event listener for points
            google.maps.event.addListener(marker, 'mouseover', function(){
  
                for (i = 0;i < ibTemp.length; i++) {
                    myBox = ibTemp[i];
                    myBox.close();
                }
                // for (i = 0;i < (mktTemp.length - 1); i++) {
                //     tMarklake = mktTemp[i];
                //     console.log(tMarklake);
                //     // tMarklake.setIcon(ajaxUrl + '/img/pinoff.png');
                // }
                
                marker.setIcon(ajaxUrl + '/img/pinon.png');
                ib2.open(this.map, this);

            });
            google.maps.event.addListener(marker, 'mouseout', function(){
                for (i=0;i<ibTemp.length;i++) {
                    myBox = ibTemp[i];
                    myBox.close();
                }
                for (i=0;i<(mktTemp.length - 1);i++) {
                    tMarklake = mktTemp[i];
                    // console.log(tMarklake.icon);
                    tMarklake.setIcon(ajaxUrl + '/img/pinoff.png');
                }
            });


        return marker;
    },
    'killMarker': function() {
        if (this.marker_array.length > 0) {
            for (var i = 0; i < this.marker_array.length; i++) {
                this.marker_array[i].setMap(null);
            } 
        }
        
        for (i=0;i<this.ibArray.length;i++) {
                myBox = this.ibArray[i];
                myBox.close();
        }
        this.marker_array.length = 0;
    }
};


//-- watch for Element Triggers
function watchE() {

  if ($('.stickly').length > 0) {
    stickyHeader();
  }

  if ($('.aMap').length > 0) {
    runGmap();
  }


  //-- register : trigger if request popup

    $('.hsection a.register').css('background', '#000000');
    $('#contactFrm').validate();
    $('.dOne').dropkick();
    $('.dTwo').dropkick();
    $('.dThree').dropkick();
    $('.dFour').dropkick();
    $('.dFive').dropkick();
    l = Ladda.create( document.querySelector( '.ladda-button' ) );
    watchVal();


}


function discoverHash() {
  if (location.hash === '#things') {
    $.scrollTo('.scOne', 300, {
      offset: -40
    });
  }
  else if (location.hash === '#whyalfred') {
    $.scrollTo('.scTwo', 500, {
      offset: 1
    });
  }
  else if (location.hash === '#pricing') {
    $.scrollTo('.scThree', 600, {
      offset: 1
    });
  }
  else if (location.hash === '#works') {
    $.scrollTo('.scFour', 600, {
      offset: -40
    });
  }
  else if (location.hash === '#customize') {
    $.scrollTo('.scFive', 600, {
      offset: 1
    });
  }
  else if (location.hash === '#concierge') {
    $.scrollTo('.scSix', 600, {
      offset: 1
    });
  }
  else if (location.hash === '#get') {
    requestServe();
  }
}


$(window).on('hashchange', function() {
  discoverHash();
});



$(document).ready(function() {
  
  //-- watch for elements
  watchE();


  //-- photo Gallery
    if ($('.gallery').length !== 0) {

      // $('.gallery dl.gallery-item').css('display', 'none');
      // already being done via functions

      $('.gallery dl.gallery-item:first-child').css('display', 'block');

      var galleryArray = [];
      var j = 0;
      var i = 0;

      // get array of all links
      $('.gallery dl.gallery-item dt a').each(function() {
        galleryArray[j] = $(this).attr('href');
        j++;
      });

      // create nav block html: add first active link
      var linkOne = '<h5 class="alternate galleryNavi "><a class="galActive " href="' + galleryArray[0] + '">1</a></h5>';
      $('.galleryNavTitle').append(linkOne);
      // add the rest of the block
      for (i = 1; i < galleryArray.length; i++) {
        $('.galleryNavTitle').append('<h5 class="alternate galleryNavi "><a href="' + galleryArray[i] + '">' + (i + 1) + '</a></h5>');
      }

      $('.galleryNavi a').click(function(ev) {
        ev.preventDefault();
        $('.galleryNavi a').removeClass('galActive');
        $(this).addClass('galActive');

        var imgHeight = $(".gallery").height();
        $('.gallery').css('height', imgHeight + 'px');

        var inject = '<a href="' + $(this).attr('href') + '"><img src="' + $(this).attr('href') + '" class="attachment-full"></a>';

        $('.gallery dl.gallery-item:first-child').fadeOut('fast', function() {
          $('.gallery dl.gallery-item:first-child dt a').remove();

          // new height
          imgHeight = $('.gallery dl.gallery-item:first-child').height() + 94;
          $('.gallery').css('height', imgHeight);

          $('.gallery dl.gallery-item:first-child dt').append(inject);
          $('.gallery dl.gallery-item:first-child').fadeIn();
        });
      });
    }

  //-- ftb twitter plugin
  $('.twitterdiv').forthebirds({
    loadtext: 'Loading Tweets...',
    transition: 'fade',
    timer: '5000',
    date: 'true',
    datelocation: 'howLong',
    fetch: ftbAmount,
    count: '1',
    avatar: 'false',
    avatarlocation: 'false',
    user: 'alfredservice'
  });

});
