var DLMap = {

    map: null,
    autocomplete: null,
    searchMarker: null,
    locationMarkers: [],
    infoWindow: null,
    geocoder: null,
    locationRadius: null,
    autocomplete: null,
    hasPublic: false,
    circleRadius: 0.4318,
    
    icons: {
        search:  '/images/gmap3/marker_home.png',
        private: '/images/gmap3/marker_locker_private.png',
        public:  '/images/gmap3/marker_locker_public.png',
        active:  '/images/gmap3/marker_location.png'
    },
    
    legend: {
        home:    'Your Address',
        private: 'For Residents Only',
        public:  'Public'
    }

};


/**
 * Callback to instanciate the map when the API is loaded
 */
DLMap.initializeMap = function (customerLatLon, customerAddress, limitGeoCountry) {
    var options = {
        zoom: 12,
        center: new google.maps.LatLng(customerLatLon.lat, customerLatLon.lon),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    DLMap.map        = new google.maps.Map(document.getElementById('map_canvas'), options);
    DLMap.geocoder   = new google.maps.Geocoder();
    DLMap.infoWindow = new google.maps.InfoWindow();

    DLMap.drawLegend();

    // wait to load the autocomplete for when the map has been loaded
    google.maps.event.addListenerOnce(DLMap.map, 'idle', function() {
        DLMap.initializeSearch(customerLatLon, customerAddress, limitGeoCountry);
    });
};

DLMap.setSearchMarker = function(location) {
    if(DLMap.searchMarker != null) {
        DLMap.searchMarker.setPosition(location);
        DLMap.drawRadius(location, DLMap.circleRadius);
        return;
    }

    DLMap.searchMarker = new google.maps.Marker({
        icon: DLMap.icons['search'],
        position: location,
        map: DLMap.map
    });

    DLMap.drawRadius(location, DLMap.circleRadius);
};

DLMap.drawRadius = function (radiusPoint, radiusSizeInMiles) {
    if (DLMap.locationRadius){
        DLMap.locationRadius.setCenter(radiusPoint);
        DLMap.locationRadius.setRadius(radiusSizeInMiles * 1609.43);
        return;
    }

    DLMap.locationRadius = new google.maps.Circle({
        center: radiusPoint,
        radius: radiusSizeInMiles * 1609.43,
        map: DLMap.map,
        strokeColor: "6ed0f7",
        fillColor: "6ed0f7",
        visible: false
    });
};

DLMap.drawMarker = function(locationData, locationLatLon) {
    var icon = DLMap.icons['private'];
    if (Location.isPublic(locationData)) {
        icon = DLMap.icons['public'];
    }
    if (locationData.name == "active") {
        icon = DLMap.icons['active'];
    }

    var marker = new google.maps.Marker({
        icon: icon,
        position: locationLatLon,
        map: DLMap.map
    });

    google.maps.event.addListener(marker, 'click', function() {
        DLMap.map.setCenter(locationLatLon);
        DLMap.infoWindow.setContent(locationData.info);
        DLMap.infoWindow.open(DLMap.map, marker);

        if (marker.getAnimation() != null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
            setTimeout(function() {
                marker.setAnimation(null);
            }, 2200);
        }
    });

    DLMap.locationMarkers.push(marker);
};

DLMap.drawLocationMarker = function (locationData) {
    var locLat = locationData.lat || '';
    var locLon = locationData.lon || '';

    //if for some reason we don't have lat lon in the location record, geocode it
    if (locLat == '' || locLon == '') {
        var geoAddressString = locationData.address + ', ' + locationData.city + ', ' + locationData.state + ' ' + locationData.zip ;
        DLMap.geocoder.geocode({ address: geoAddressString }, function(res, status) {
            if (res && typeof(res[0]) != 'undefined' && res.length > 0) {
                var locationLatLon = res[0]['geometry']['location'];
                DLMap.drawMarker(locationData, locationLatLon);
            }
        });
    } else { //if we do have lat/lon, then make a lat/lon google object and render it
        var locationLatLon = new google.maps.LatLng(locLat, locLon);
        DLMap.drawMarker(locationData, locationLatLon);
    }
};

DLMap.clearMarkers = function() {
    for(var i in DLMap.locationMarkers) {
        DLMap.locationMarkers[i].setMap(null);
    }
    locationMarkers = [];
};

DLMap.drawLegend = function () {
    var div = document.createElement('div');
    div.id = 'map_legend';

    var content = [];
    content.push('<div><img src="'+DLMap.icons['search']+'" height="25" /> '+DLMap.legend['home']+'</div>');
    content.push('<div><img src="'+DLMap.icons['private']+'" height="25" /> '+DLMap.legend['private']+'</div>');
    content.push('<div><img src="'+DLMap.icons['public']+'" height="25" /> '+DLMap.legend['public']+'</div>');

    div.innerHTML = content.join('');
    div.index = 1;
    DLMap.map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(div);
};


DLMap.initializeSearch = function (customerLatLon, customerAddress, limitGeoCountry) {
    var mapBounds =  DLMap.map.getBounds();
    var mapInput = document.getElementById('address_search_field');

    var searchOptions = { bounds: mapBounds };

    if (limitGeoCountry) {
        searchOptions.componentRestrictions = { country: limitGeoCountry };
    }

    DLMap.autocomplete = new google.maps.places.Autocomplete(mapInput, searchOptions);
    
    //autocomplete listener for when the autocomplete selects a place
    google.maps.event.addListener(DLMap.autocomplete, 'place_changed', function () {
        var place = DLMap.autocomplete.getPlace();

        if (!place.geometry) {
          //TODO: Inform the user that a place was not found and return.
          return;
        }

        DLMap.searchByPoint(place.geometry.location);
    });

    if (customerLatLon.lat != '' && customerLatLon.lon != '' && customerLatLon.lat != '0' && customerLatLon.lon != '0') {
        var customerLocation = new google.maps.LatLng(customerLatLon.lat, customerLatLon.lon);
        DLMap.searchByPoint(customerLocation);
    } else {
        DLMap.geocoder.geocode({ address : customerAddress }, function(res, status) {
            if (res && typeof(res[0]) != 'undefined' && res.length > 0) {
                DLMap.searchByPoint(res[0].geometry.location);
            }
        });
    }

};

DLMap.searchByPoint = function (location, callback) {

    DLMap.setSearchMarker(location);

    var distanceSearch = {
        lat: location.lat(),
        lon: location.lng(),
        distance: 1.2,//in miles
        public: 'yes',
        private: 'yes'
    };

    $.post('/account/locations/radius_searchJSON', distanceSearch, function (res) {
        var sRes = JSON.parse(res);
        var results = sRes.results || [];
        DLMap.clearMarkers();

        //re-center map
        DLMap.map.setCenter(location);

        // wait to locations be loaded
        DLMap.whenLocationsLoaded(function() {
            Results.render(results);
            DLMap.resetMapBoundsFromCircle();
        });

        if (typeof callback == 'function') {
            callback(results);
        }
    });
};

/**
 * Defet lodaing until locations are loaded
 */
DLMap.whenLocationsLoaded = function(callback) {
    if (!$.isEmptyObject(locationsJSON)) {
        callback();
        return;
    }

    var interval = setInterval(function() {
        if (!$.isEmptyObject(locationsJSON)) {
            callback();
            clearInterval(interval);
            return;
        }
    }, 500);
};

DLMap.searchByAddress = function (address, callback) {

    if (address == '') {
        return;
    }
    
     var search = {
         term : address,
         public: 'yes',
         private: 'yes'
    };

    $.post('/account/locations/searchLocationsJSON', search, function(serverRes) {
        var sRes = JSON.parse(serverRes);

        if (sRes.geo) {
            var location = new google.maps.LatLng(sRes.geo.lat, sRes.geo.lon);
            DLMap.setSearchMarker(location);
            DLMap.map.setCenter(location);
        }

        DLMap.clearMarkers();
        var results = sRes.results || [];

        // wait to locations be loaded
        DLMap.whenLocationsLoaded(function() {
            Results.render(results);
            DLMap.resetMapBoundsFromCircle();
        });
    });

};

DLMap.resetMapBoundsFromCircle = function() {
    if(DLMap.locationRadius) {
        DLMap.map.setZoom(12);
        DLMap.map.fitBounds(DLMap.locationRadius.getBounds());
    }
};


/* ------------------------------------------------------------------ */

/**
 * Results functions
 */
var Results = {
    noResultsText: "No Results Found",
    maxVisibleResults: 3
};

/**
 * Clears the location tables
 */
Results.clear = function () {
   $('#public_locations').html('');
   $('#private_locations').html('');

   $('#private_locations_wrapper').hide();
   $('#public_locations_wrapper').hide();
};

/**
 * Render a search result
 */
Results.render = function (results) {
    if (!results.length) {
        return Results.noResult();
    }

    Results.clear();
    for (var r in results) {
        var result   = results[r];
        var location = locationsJSON[result.locationID];
        if (typeof(location) == 'undefined') {
            console.log("Missing Location: " + result.locationID);
            continue;
        }
        location.distance = result.distance ? result.distance : null;
        
        location.distance_in_miles = result.distance_in_miles;
        location.distance_unit = result.distance_unit;
        location.distance_unit_label = result.distance_unit_label;
        
        if (location) {
            Results.renderRow(location, function () {
               DLMap.drawLocationMarker(location);
            });
        }
    }
    $('#filter_results').show();
};

/**
 * Render a rows of the results
 */
Results.renderRow = function (loc, callback) {
    if (loc == '' || locationsJSON[loc.locationID] == '') {
       return false;
    }
    locationsJSON[loc.locationID] = locationsJSON[loc.locationID] || '';

    var is_public = Location.isPublic(loc);
    var row = '';
    row += '<tr style="display:none;">';
    row += '<td>';
    row +=      '<b>' + (loc.companyName ? loc.companyName : loc.address) + '</b><br>';
    row +=       loc.address + '<br>';
    row += '</td>';
    row += '<td>' + Location.renderDistance(loc) + '</td>';

    if ($.isEmptyObject(existingCustomerLocations[loc.locationID])) {
        row += '<td><a class="add_button" data-location_id="' + loc.locationID + '" href="javascript:;"><i class="fa fa-plus-circle fa-fw fa-2x"></i></a></td>';
    } else {
        row += '<td><i data-location_id="' + loc.locationID + '" class="fa fa-check-circle-o fa-fw fa-2x added_button"></i></td>';
    }
    row += '</tr>';

    var table = $(is_public ? '#public_locations' : '#private_locations');
    var wrap = $(is_public ? '#public_locations_wrapper' : '#private_locations_wrapper').show();

    var is_visible = $('tr', table).length < Results.maxVisibleResults;

    if (is_visible) {
        $(row).appendTo(table).fadeIn();
    } else {
        $(row).appendTo(table);
    }

    if (typeof callback == 'function') {
        callback();
    }
};


/**
 * Show a message when no results are found
 */
Results.noResult = function () {
    Results.clear();
    $('#public_locations_wrapper').show();
    $('#filter_results').show();
    $('<tr><td colspan="4"><div class="alert alert-warning">'+Results.noResultsText+'</div></td></tr>')
        .appendTo('#public_locations')
        .fadeIn();
};


/* ------------------------------------------------------------------ */

/**
 * Location functions
 */
var Location = {

    distanceUnit: 'm',
    selectLocationText: 'Select Location',
	hoursText: 'Hours:'
};

/**
 * Tells if a location is public
 */
Location.isPublic = function (location) {
    var acceptable_publics = ['Yes','yes','YES','public','Public'];
    return acceptable_publics.indexOf(location.public) >= 0;
};

/**
 * Renders the distance column for locations
 */
Location.renderDistance = function  (loc) {
    return loc.distance ? parseFloat(loc.distance).toFixed(2) + ' ' + (loc.distance_unit_label || Location.distanceUnit) : '-';
};

/**
 * Renders the info box HTML for locations
 */
Location.getInfoBox = function(location) {
    var t = function(x) { return x ? x : '' };
    var info = '<div class="gmap-buble">';
    if (location.url && location.image) {
        $info += '<div style="float:left;margin:5px;</div></div>"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src="'+location.image.url+'" width='+location.image.width+' height='+location.image.height+'></div>';
    }
    info += '<h3>'+t(location.companyName ? location.companyName : location.address)+'</h3>';
    info += (location.address)?'<p>'+t(location.address)+'</p>':"";
    info += (location.hours)?'<p><b>'+Location.hoursText+'</b><span> '+t(location.hours)+'</span></p>':"";
    info += '<div><div><a class="add_button" data-location_id="' + location.locationID + '" href="javascript:;"><i class="fa fa-plus-circle fa-fw fa-2x"></i> <span>'+Location.selectLocationText+'</span></a></div></div>';
    info += '</div>';

    return info;
};


/* ------------------------------------------------------------------ */

var Customer = {
	orderNowText: 'Order Now',
	addLocationError: 'Already added that location!'
};

/**
 * binding to click that adds the location via ajax
 */
Customer.addLocation = function(evt) {
    var location_id = $(this).attr('data-location_id');
    var locRef = this;

    $.post('/account/locations/add_customer_location_ajax', {locationID:location_id}, function(res) {
        var resStatus = JSON.parse(res);
        if (resStatus.status == 'success') {
            $(locRef).parent().parent().fadeOut(function() {
                Customer.addLocationRow(location_id);
            });

            if($.isEmptyObject(existingCustomerLocations)) {
                existingCustomerLocations = {};
            }
            existingCustomerLocations[location_id] = locationsJSON[location_id];

            setTimeout(function() {
                window.location.href = '/account/orders/new_order';
            }, 1000);

        } else {
            alert(Customer.addLocationError);
        }
    });
};


/**
 * adds a new row to the customer locations table
 */
Customer.addLocationRow = function(locationID) {
    var loc = locationsJSON[locationID];

    var row = '';
    row += '<tr style="display:none;">';
    row += '<td>';
    row +=      '<b>'+ (loc.companyName ? loc.companyName : loc.address) + '</b><br>';
    row +=      loc.address + '<br>';
    row +=      '<a href="/account/orders/new_order?loc='+loc.locationID+'">'+ Customer.orderNowText + '</a>';
    row += '</td>';
    row += '<td>' + Location.renderDistance(loc) + '</td>';
    row += '<td style="text-align:center;"><a class="delete_button" href="javascript:;"  data-location_id="' + loc.locationID + '"><i class="fa fa-times-circle-o fa-fw fa-2x"></i></a></td>';
    row += '</tr>';

    $('#no_locations_message').hide();
    $('#existing_locations').closest('table').show();
    $(row).appendTo('#existing_locations').fadeIn();
};


/**
 * deletes the location (binded by click)
 */
Customer.deleteLocation = function(evt) {
    var tr = $(this).closest('tr');
    var location_id = $(this).attr('data-location_id');

    $.post('/account/locations/delete_customer_location_ajax', { locationID:location_id }, function(res) {
        tr.fadeOut(function() {
            delete existingCustomerLocations[location_id];
            var cell = $('[data-location_id=' + location_id + '].added_button').parent();
            cell.html('<a class="add_button" data-location_id="' + location_id + '" href="javascript:;"><i class="fa fa-plus-circle fa-fw fa-2x"></i></a>');
        });
    });
};
