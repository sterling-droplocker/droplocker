$(function() {// generate mobile version for each table
$('table.make-responsive').each(function() {

    var table = $(this); // cache table object
    var head = table.find('thead th');
    var rows = table.find('tbody tr').clone(); // appending afterwards does not break original table

    rows.each(function(i) {

    // create new table
    var newtable = $(
      '<table class="table table-bordered table-striped visible-phone">' +
      '  <tbody>' +
      '  </tbody>' +
      '</table>'
    );

    // cache tbody where we'll be adding data
    var newtable_tbody = newtable.find('tbody');



      var cols = $(this).find('td');
      var classname = i % 2 ? 'even' : 'odd';
      cols.each(function(k) {
        var new_tr = $('<tr class="' + classname + '"></tr>').appendTo(newtable_tbody);
        
        var th = head.clone().get(k);
        $(th).removeAttr('width').removeAttr('style');
        
        new_tr.append(th);
        new_tr.append($(this).removeAttr('width').attr('style', 'width:100%;'));
      });

        $(table).after(newtable);
        
        table = newtable;

    });


    $(this).addClass('hidden-phone');
});

});
