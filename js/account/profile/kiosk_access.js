/* Note, this file depends on the Jquery.Validate library */
$(document).ready( function() {
    
$("#update_kiosk_card_form").validate(
{
    rules: {
        cardNumber: {
            digits: true,
            required: true,
            maxlength: 4,
            minlength: 4
        }
    },
    errorClass: "invalid",
    submitHandler: function(form) { 
       form.submit();
       $(":submit").attr("disabled", true);
    }
});

});
