/*
Author: Juan Mescher <jmescher@droplocker.com>
Config:
    1) Add: <script type="text/javascript" src="/js/DataTables-1.9.1/custom/orderPriceColumns.js"></script>
    2) Create the dataTables Instance:

        $("#box-table-a").dataTable({
            "aoColumns": [
                null,
                null,
                null,
                { "sType": "price" },   // <------------- 
                null
            ]
        });
 */
jQuery.fn.dataTableExt.oSort['price-asc']  = function(a,b) {
    a = a.match(/[+\-]?\d+(,\d+)?(\.\d+)?/)[0].replace( /,/, "" );
    b = b.match(/[+\-]?\d+(,\d+)?(\.\d+)?/)[0].replace( /,/, "" );
    var x = (a == "-") ? 0 : a;
    var y = (b == "-") ? 0 : b;
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['price-desc'] = function(a,b) {
    a = a.match(/[+\-]?\d+(,\d+)?(\.\d+)?/)[0].replace( /,/, "" );
    b = b.match(/[+\-]?\d+(,\d+)?(\.\d+)?/)[0].replace( /,/, "" );
    var x = (a == "-") ? 0 : a;
    var y = (b == "-") ? 0 : b;
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ?  1 : ((x > y) ? -1 : 0));
};
