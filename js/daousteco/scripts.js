jQuery(document).ready(function($) {
    var PROJECT = PROJECT || {};
    var GLOBAL = GLOBAL || {};
    var BARBA = BARBA || {};
    var UTILS = UTILS || {};
    var HOME = HOME || {};
    var MAP = MAP || {};
    var QSN = QSN || {};
    var FRANCHISE = FRANCHISE || {};
    var TECHNOLOGIES = TECHNOLOGIES || {};
    var PROMOTIONS = PROMOTIONS || {};
    var CONTACT = CONTACT || {};
    var SOINS = SOINS || {};
    var SINGLESOINS = SINGLESOINS || {};
    var SERVICES = SERVICES || {};
    var JOURNAL = JOURNAL || {};
    var FAQ = FAQ || {};
    var CARRIERE = CARRIERE || {};
    var GENERIC = GENERIC || {};
    var INCOMING = INCOMING || {};
    UTILS.attributes = {};
    UTILS.methods = {
        isMobile: function() {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() < 1024)
                return true;
            else
                return false;
        },
        getLang: function() {
            var lang = $('html').attr('lang');
            lang = lang.slice(0, 2);
            return lang;
        },
        getBrowser: function() {
            var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
            var isFirefox = typeof InstallTrigger !== 'undefined';
            var isSafari = /constructor/i.test(window.HTMLElement) || (function(p) {
                return p.toString() === "[object SafariRemoteNotification]";
            })(!window['safari'] || safari.pushNotification);
            var isIE = false || !!document.documentMode;
            var isEdge = !isIE && !!window.StyleMedia;
            var isChrome = !!window.chrome && !!window.chrome.webstore;
            var isBlink = (isChrome || isOpera) && !!window.CSS;
            if (isOpera) return "opera";
            if (isFirefox) return "mozilla";
            if (isSafari) return "safari";
            if (isIE) return "ie";
            if (isEdge) return "edge";
            if (isChrome) return "chrome";
            if (isBlink) return "blink";
        },
        getInternetExplorerVersion: function() {
            var rv = -1;
            var ua;
            var re;
            if (navigator.appName == 'Microsoft Internet Explorer') {
                ua = navigator.userAgent;
                re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.$1);
            } else if (navigator.appName == 'Netscape') {
                ua = navigator.userAgent;
                re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.$1);
            }
            return rv;
        },
        loadJsPerfectly: function(jsLinkArray, parent, callback) {
            if (jsLinkArray[0] && $('script[src*="' + jsLinkArray[0] + '"]').length == 0) {
                var jq = document.createElement("script");
                jq.type = "text/javascript";
                jq.async = true;
                parent[0].appendChild(jq);
                jq.onload = function() {
                    jsLinkArray = $.grep(jsLinkArray, function(value) {
                        return value != jsLinkArray[0];
                    });
                    UTILS.methods.loadJsPerfectly(jsLinkArray, parent, callback);
                };
                jq.src = jsLinkArray[0];
            } else if (callback) {
                callback();
            }
        },
        createGradient: function(svg, id, stops) {
            var svgNS = svg.namespaceURI;
            var grad = document.createElementNS(svgNS, 'linearGradient');
            grad.setAttribute('id', id);
            grad.setAttribute('x1', '22.398');
            grad.setAttribute('y1', '133.0977');
            grad.setAttribute('x2', '102.581');
            grad.setAttribute('y2', '-5.7832');
            grad.setAttribute('gradientUnits', 'userSpaceOnUse');
            for (var i = 0; i < stops.length; i++) {
                var attrs = stops[i];
                var stop = document.createElementNS(svgNS, 'stop');
                for (var attr in attrs) {
                    if (attrs.hasOwnProperty(attr)) stop.setAttribute(attr, attrs[attr]);
                }
                grad.appendChild(stop);
            }
            var defs = svg.querySelector('defs') || svg.insertBefore(document.createElementNS(svgNS, 'defs'), svg.firstChild);
            return defs.appendChild(grad);
        },
    };
    GLOBAL.attributes = {
        actualLang: undefined,
        loaded: false,
        loadedTimes: 0,
        menuCloseBtn: $('.mobile_btn'),
        menuCloseFrHtml: 'fermer',
        menuCloseEnHtml: 'close',
        langCloseBtn: $('.lang-mobile-btn'),
        langCloseEnHtml: 'Back',
        langCloseFrHtml: 'Retour',
        countryRestrict: {
            'country': 'ca'
        },
        autocomplete: undefined,
        //Scrollbar: window.Scrollbar,
        scrollbar: true,
        ready: false,
        loaderAnim: new TimelineMax(),
        menuAnim: new TimelineMax({
            paused: true
        }),
        menuAnimElem: $('.logo, .menu-item, .header-extends', '.header'),
        splitTextAnim: new TimelineMax({
            paused: true
        }),
        //scene: new ScrollMagic.Scene(),
    };
    GLOBAL.methods = {
        isLoaded: function() {
            BARBA.methods.initBarba();
            $('.header').removeClass('not--clickable');
            this.checkBrowser();
            this.eventListeners();
        },
        init: function() {
            this.imgLazyLoad();
            this.footerGallery();
            this.windowOnScroll();
            this.windowOnResize();
            if ($('.barba-container').data('namespace') === 'MAP') {
                $('li', '#main-nav').removeClass('active');
                $('.find-us--container').addClass('is--active');
            } else {
                $('.find-us--container').removeClass('is--active');
            }
            if (UTILS.methods.getInternetExplorerVersion() >= 10 || UTILS.methods.getBrowser() === 'edge') {
                this.grayScaleIE();
            }
            if (GLOBAL.attributes.loadedTimes === 0) {
                setTimeout(function() {
                    $('.module--popup').removeClass('is--hidden');
                }, 5000);
            }
            GLOBAL.attributes.loadedTimes++;
        },
        imgLazyLoad: function() {
            $('.bg-low-to-high').each(function(index) {
                if ($(this).data('src')) {
                    $(this).css('background-image', 'url(' + $(this).data('src') + ')');
                }
            });
            $('img.img-low-to-high').each(function(index) {
                if ($(this).data('src')) {
                    $(this).attr('src', $(this).data('src'));
                }
            });
        },
        eventListeners: function() {
            $('.mobile_btn').on('click', function() {
                if ($('.header').hasClass('is--open')) {
                    $('.header').removeClass('is--open');
                    $('body').removeClass('is--fixed');
                    GLOBAL.attributes.menuCloseBtn.html('menu');
                    if ($('.barba-container').data('namespace') === 'HOME') {
                        $('.header').addClass('is--home');
                    }
                } else {
                    $('.header').addClass('is--open');
                    if (UTILS.methods.getLang() === 'fr') {
                        GLOBAL.attributes.menuCloseBtn.html(GLOBAL.attributes.menuCloseFrHtml);
                    } else {
                        GLOBAL.attributes.menuCloseBtn.html(GLOBAL.attributes.menuCloseEnHtml);
                    }
                    if ($('.barba-container').data('namespace') === 'HOME') {
                        $('.header').removeClass('is--home');
                    }
                    setTimeout(function() {
                        $('body').addClass('is--fixed');
                    }, 300);
                }
            });
            $('.lang-mobile-btn', '.lang-selector--container.current').on('click', function() {
                GLOBAL.attributes.actualLang = typeof GLOBAL.attributes.actualLang === 'undefined' ? GLOBAL.attributes.langCloseBtn.html() : GLOBAL.attributes.actualLang;
                if ($('.lang-selector--container.list--container').hasClass('show')) {
                    $('.lang-selector--container.list--container').removeClass('show');
                    $('.header').removeClass('no-scrollable');
                    GLOBAL.attributes.langCloseBtn.html(GLOBAL.attributes.actualLang);
                } else {
                    if (UTILS.methods.getLang() === 'fr') {
                        GLOBAL.attributes.langCloseBtn.html(GLOBAL.attributes.langCloseFrHtml);
                    } else {
                        GLOBAL.attributes.langCloseBtn.html(GLOBAL.attributes.langCloseEnHtml);
                    }
                    $('.lang-selector--container.list--container').addClass('show');
                    $('.header').addClass('no-scrollable');
                }
            });
            $('li', '#main-nav').on('click', function() {
                var item = $(this);
                $('li', '#main-nav').removeClass('active');
                item.addClass('active');
            });
            $('.mobile_logo, .logo', '.header-content').on('click', function() {
                $('li', '#main-nav').removeClass('active');
            });
            $('.popup--close-btn').on('click', function() {
                if ($(this).parents('.module--popup').hasClass('is--hidden')) {
                    $(this).parents('.module--popup').removeClass('is--hidden');
                } else {
                    $(this).parents('.module--popup').addClass('is--hidden');
                }
            });
        },
        animMainButton: function() {
            $('.button--bubble').each(function() {
                var $circlesTopLeft = $(this).parent().find('.circle.top-left');
                var $circlesBottomRight = $(this).parent().find('.circle.bottom-right');
                var tl = new TimelineLite();
                var tl2 = new TimelineLite();
                var btTl = new TimelineLite({
                    paused: true
                });
                tl.to($circlesTopLeft, 1.2, {
                    x: -25,
                    y: -25,
                    scaleY: 2,
                    ease: SlowMo.ease.config(0.1, 0.7, false)
                });
                tl.to($circlesTopLeft.eq(0), 0.1, {
                    scale: 0.2,
                    x: '+=6',
                    y: '-=2'
                });
                tl.to($circlesTopLeft.eq(1), 0.1, {
                    scaleX: 1,
                    scaleY: 0.8,
                    x: '-=10',
                    y: '-=7'
                }, '-=0.1');
                tl.to($circlesTopLeft.eq(2), 0.1, {
                    scale: 0.2,
                    x: '-=15',
                    y: '+=6'
                }, '-=0.1');
                tl.to($circlesTopLeft.eq(0), 1, {
                    scale: 0,
                    x: '-=5',
                    y: '-=15',
                    opacity: 0
                });
                tl.to($circlesTopLeft.eq(1), 1, {
                    scaleX: 0.4,
                    scaleY: 0.4,
                    x: '-=10',
                    y: '-=10',
                    opacity: 0
                }, '-=1');
                tl.to($circlesTopLeft.eq(2), 1, {
                    scale: 0,
                    x: '-=15',
                    y: '+=5',
                    opacity: 0
                }, '-=1');
                var tlBt1 = new TimelineLite();
                var tlBt2 = new TimelineLite();
                tlBt1.set($circlesTopLeft, {
                    x: 0,
                    y: 0,
                    rotation: -45
                });
                tlBt1.add(tl);
                tl2.set($circlesBottomRight, {
                    x: 0,
                    y: 0
                });
                tl2.to($circlesBottomRight, 1.1, {
                    x: 30,
                    y: 30,
                    ease: SlowMo.ease.config(0.1, 0.7, false)
                });
                tl2.to($circlesBottomRight.eq(0), 0.1, {
                    scale: 0.2,
                    x: '-=6',
                    y: '+=3'
                });
                tl2.to($circlesBottomRight.eq(1), 0.1, {
                    scale: 0.8,
                    x: '+=7',
                    y: '+=3'
                }, '-=0.1');
                tl2.to($circlesBottomRight.eq(2), 0.1, {
                    scale: 0.2,
                    x: '+=15',
                    y: '-=6'
                }, '-=0.2');
                tl2.to($circlesBottomRight.eq(0), 1, {
                    scale: 0,
                    x: '+=5',
                    y: '+=15',
                    opacity: 0
                });
                tl2.to($circlesBottomRight.eq(1), 1, {
                    scale: 0.4,
                    x: '+=7',
                    y: '+=7',
                    opacity: 0
                }, '-=1');
                tl2.to($circlesBottomRight.eq(2), 1, {
                    scale: 0,
                    x: '+=15',
                    y: '-=5',
                    opacity: 0
                }, '-=1');
                tlBt2.set($circlesBottomRight, {
                    x: 0,
                    y: 0,
                    rotation: 45
                });
                tlBt2.add(tl2);
                btTl.add(tlBt1);
                btTl.to($(this).parent().find('.button.effect-button'), 0.8, {
                    scaleY: 1.1
                }, 0.1);
                btTl.add(tlBt2, 0.2);
                btTl.to($(this).parent().find('.button.effect-button'), 1.8, {
                    scale: 1,
                    ease: Elastic.easeOut.config(1.2, 0.4)
                }, 1.2);
                btTl.timeScale(2.6);
                $(this).on('mouseover', function() {
                    btTl.restart();
                });
            });
        },
        checkBrowser: function() {
            if (UTILS.methods.getBrowser() == 'ie') {
                $('body').addClass('ie');
            }
            if (UTILS.methods.getInternetExplorerVersion() >= 10) {
                $('body').addClass('ie11');
            }
        },
        grayScaleIE: function() {
            $('.gallery-image img').each(function() {
                var el = $(this);
                el.css({
                    "position": "absolute"
                }).wrap("<div class='img_wrapper' style='display: inline-block'>").clone().addClass('img_grayscale').css({
                    "position": "absolute",
                    "z-index": "5",
                    "opacity": "0"
                }).insertBefore(el).queue(function() {
                    var el = $(this);
                    el.parent().css({
                        "width": this.width,
                        "height": this.height
                    });
                    el.dequeue();
                });
                if (this.src) {
                    this.src = grayscaleIE10(this.src);
                }
            });
            $('.gallery-image img').hover(function() {
                $(this).parent().find('img:first').stop().animate({
                    opacity: 1
                }, 200);
            }, function() {
                $('.img_grayscale').stop().animate({
                    opacity: 0
                }, 200);
            });

            function grayscaleIE10(src) {
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                var imgObj = new Image();
                imgObj.src = src;
                if (imgObj.width > 0) {
                    canvas.width = imgObj.width;
                    canvas.height = imgObj.height;
                    ctx.drawImage(imgObj, 0, 0);
                    var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
                    for (var y = 0; y < imgPixels.height; y++) {
                        for (var x = 0; x < imgPixels.width; x++) {
                            var i = (y * 4) * imgPixels.width + x * 4;
                            var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
                            imgPixels.data[i] = avg;
                            imgPixels.data[i + 1] = avg;
                            imgPixels.data[i + 2] = avg;
                        }
                    }
                    ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
                    return canvas.toDataURL();
                }
            }
        },
        loaderAnim: function(bool, duration, callback) {
            bool = typeof bool !== 'undefined' ? bool : false;
            duration = typeof duration !== 'undefined' ? duration : 'short';
            GLOBAL.attributes.loaderAnim = new TimelineMax({
                paused: true,
                onComplete: function() {
                    GLOBAL.methods.clearLoaderAnim();
                    GLOBAL.methods.globalAnimPage();
                }
            });
            var loader = $('.loading'),
                water = $("#water"),
                box = $('.box'),
                circle = $('.loading .circle'),
                inner = $('.loading .inner'),
                percent = 0,
                interval, ready = bool;
            if (ready) {
                if (duration === 'short') {
                    GLOBAL.attributes.loaderAnim.set(circle, {
                        top: "50%",
                        left: "50%",
                        xPercent: -50,
                        yPercent: -50,
                        scale: 1,
                        width: 4000,
                        height: 4000,
                        borderRadius: "50%"
                    });
                    GLOBAL.attributes.loaderAnim.set(loader, {
                        css: {
                            background: 'transparent'
                        }
                    });
                    GLOBAL.attributes.loaderAnim.to(box, 0.5, {
                        ease: Back.easeInOut.config(1.3),
                        scale: 0
                    });
                    GLOBAL.attributes.loaderAnim.to(circle, 1, {
                        ease: Expo.easeOut,
                        scale: 0,
                        z: 0.001
                    });
                    GLOBAL.attributes.loaderAnim.play();
                } else {
                    loader.addClass('animated');
                    if (UTILS.methods.getBrowser() == 'ie') {
                        $('.water_wave').css('bottom', '53%');
                    }
                    interval = setInterval(function() {
                        percent += 2;
                        water.css('transform', 'translate(0' + ',' + (100 - percent) + '%)');
                        if (percent == 100) {
                            clearInterval(interval);
                            GLOBAL.attributes.loaderAnim.set(circle, {
                                top: "50%",
                                left: "50%",
                                xPercent: -50,
                                yPercent: -50,
                                scale: 1,
                                width: 4000,
                                height: 4000,
                                borderRadius: "50%"
                            });
                            GLOBAL.attributes.loaderAnim.set(loader, {
                                css: {
                                    background: 'transparent'
                                }
                            });
                            GLOBAL.attributes.loaderAnim.to(box, 0.5, {
                                ease: Back.easeInOut.config(1.3),
                                scale: 0
                            });
                            GLOBAL.attributes.loaderAnim.to(circle, 1, {
                                ease: Expo.easeOut,
                                scale: 0,
                                z: 0.001
                            }, '-=0');
                            GLOBAL.attributes.loaderAnim.play();
                            percent = 0;
                        }
                    }, 10);
                }
                if (callback) callback();
            }
        },
        clearLoaderAnim: function() {},
        menuScroll: function() {},
        initGlobalAnimElem: function() {
            //return; // Disable animations.. MyAccount doesn't need it.

            if (GLOBAL.attributes.loadedTimes === 1) {
                GLOBAL.attributes.menuAnim.set(GLOBAL.attributes.menuAnimElem, {
                    opacity: 0,
                    scale: 1.2
                });
                GLOBAL.attributes.menuAnim.staggerFromTo(GLOBAL.attributes.menuAnimElem, 0.4, {
                    ease: Expo.easeOut,
                    scale: 1.2,
                    opacity: 0
                }, {
                    scale: 1,
                    opacity: 1
                }, 0.15, '-=0');
            } else {
                GLOBAL.attributes.splitTextAnim.play()
            }
            var firstTitle = $('section').first().find($('h1:not(.not-animate)'));
            var firstText = $('section').first().find($('p:not(.not-animate)'));
            var fistTextIntro = $('section').eq(1).find($('.text-intro'));
            var firstButton = $('section').first().find($('.button--bubble__container'));
            if (!$('.washingmachine--container').length) {
                var firstImages = $('section').first().find($('.image-anim-intro'));
                var firstBg = $('.top-banner').first();
            }
            var firstInput = $('section').first().find($('.form--container'));
            var firstSeparator = $('.separator .line').first();
            var firstletter = $('.separator .letter').first();
            var windowHeight = $(window).height();
            if (firstTitle.length) {
                firstTitle.css('overflow', 'hidden');
                var splitTitle = new SplitText(firstTitle, {
                    type: "lines, words",
                    linesClass: 'split-line',
                    wordsClass: 'split-words',
                });
                var linesTitle = splitTitle.lines;
            }
            if (firstText.length) {
                firstText.css('overflow', 'hidden');
                var splitText = new SplitText(firstText, {
                    type: "lines, words",
                    linesClass: 'split-line',
                    wordsClass: 'split-words',
                });
                var linesText = splitText.lines;
            }
            GLOBAL.attributes.splitTextAnim = new TimelineMax({
                paused: true
            });
            if (firstTitle.length) {
                GLOBAL.attributes.splitTextAnim.staggerFrom(linesTitle, 0.8, {
                    y: linesTitle[0].offsetHeight * linesTitle.length,
                    ease: Power4.easeOut
                }, 0.1);
            }
            if (firstText.length) {
                GLOBAL.attributes.splitTextAnim.staggerFrom(linesText, 0.8, {
                    y: linesText[0].offsetHeight * linesText.length,
                    ease: Power4.easeOut
                }, 0.1, '-=0.5');
            }
            GLOBAL.attributes.splitTextAnim.fromTo(firstButton, 0.8, {
                ease: Back.easeOut,
                scale: 1.2,
                opacity: 0
            }, {
                scale: 1,
                opacity: 1
            }, '-=0.5');
            if (!$('.washingmachine--container').length) {
                GLOBAL.attributes.splitTextAnim.staggerFrom(firstImages, 0.8, {
                    ease: Back.easeOut,
                    y: 10,
                    scale: 0.9,
                    opacity: 0
                }, 0.3, '-=0.5');
                if (firstBg.length) {
                    GLOBAL.attributes.splitTextAnim.from(firstBg, 0.8, {
                        ease: Back.easeOut,
                        y: 10,
                        scale: 0.9,
                        opacity: 0
                    }, '-=0.5');
                }
            }
            GLOBAL.attributes.splitTextAnim.from(firstInput, 0.8, {
                ease: Back.easeOut,
                y: 200,
                opacity: 0
            }, 0.3, '-=0.5');
            GLOBAL.attributes.splitTextAnim.from(firstSeparator, 0.8, {
                ease: Back.easeOut,
                y: 200,
                opacity: 0
            }, 0.3, '-=0.5');
            GLOBAL.attributes.splitTextAnim.from(firstletter, 0.8, {
                ease: Back.easeOut,
                opacity: 0
            }, 0.3, '-=0.5');
            GLOBAL.attributes.splitTextAnim.from(fistTextIntro, 0.8, {
                ease: Back.easeOut,
                y: 200,
                opacity: 0
            }, 0.3, '-=0.5');
            GLOBAL.attributes.splitTextAnim.call(function() {
                if (firstTitle.length) {
                    splitTitle.revert();
                }
                if (firstText.length) {
                    splitText.revert();
                }
            });
        },
        globalAnimPage: function() {
            GLOBAL.attributes.menuAnim.play();
            setTimeout(function() {
                GLOBAL.attributes.splitTextAnim.play();
            }, 500);
        },
        initScrollAnimation: function() {
            var controller = new ScrollMagic.Controller({
                refreshInterval: 0,
            });
            $('.page .scroll-reveal').each(function(index) {
                var element = $(this),
                    offset = -100,
                    speed = 1,
                    delay = 6,
                    tl = new TimelineLite({
                        paused: true,
                        onComplete: function() {
                            if (elementToClear) {
                                tl.set(elementToClear, {
                                    clearProps: "all"
                                });
                            }
                            if (splitText) {
                                splitText.revert();
                            }
                            if (title) {
                                title.attr('style', '');
                            }
                        }
                    }),
                    elementToClear;
                GLOBAL.attributes.scene = new ScrollMagic.Scene({
                    triggerElement: this,
                    reverse: false
                });
                if (element.data('scrollReveal') === 'big-slider') {
                    var arrows = element.find($('.slick-arrow'));
                    var images = element.find($('.big-slider-img', '.slick-active'));
                    var ticket = element.find($('.big-slider-ticket.is--desktop', '.slick-active'));
                    var subTitle = element.find($('.slider-subtitle'));
                    var title = element.find($('.h1, h1'));
                    title.css('overflow', 'hidden');
                    if (element.hasClass('theme-blue')) {
                        var color = 'slider__theme-blue';
                    } else if (element.hasClass('theme-peach')) {
                        var color = 'slider__theme-peach';
                    } else {
                        var color = 'slider__theme-teal';
                    }
                    var splitText = new SplitText(title, {
                        type: "lines",
                        linesClass: 'split-line slider ' + color,
                    });
                    var linesTitle = splitText.lines;
                    elementToClear = [arrows, images, ticket];
                    if ($('.barba-container').data('namespace') === 'SOINS' && index === 0) {
                        tl.staggerFrom(linesTitle, 0.8, {
                            css: {
                                top: linesTitle[0].offsetHeight
                            },
                            ease: Power4.easeOut,
                            delay: delay
                        }, 0, 'start');
                        offset = -700;
                    } else {
                        tl.staggerFrom(linesTitle, 0.8, {
                            css: {
                                top: linesTitle[0].offsetHeight
                            },
                            ease: Power4.easeOut
                        }, 0, 'start');
                        offset = -300;
                    }
                    if (subTitle.length) {
                        tl.from(subTitle, speed, {
                            ease: Power4.easeOut,
                            opacity: 0
                        }, '-=0.5');
                    }
                    tl.staggerFrom([images, ticket], speed, {
                        ease: Back.easeOut,
                        y: 100,
                        opacity: 0
                    }, 0.3, '-=0.5');
                    tl.from(arrows[0], speed, {
                        ease: Power4.easeOut,
                        x: -50,
                        opacity: 0
                    });
                    tl.from(arrows[1], speed, {
                        ease: Power4.easeOut,
                        x: 50,
                        opacity: 0
                    }, '-=0.8');
                } else if (element.data('scrollReveal') === 'cards-slider') {
                    var arrows = element.find($('button'));
                    var images = element.find($('.cardslider__card'));
                    var title = element.find($('.is--title h2'));
                    title.css('overflow', 'hidden');
                    var blocText = element.find($('.is--text, .is--button', '.is--bloc-text'));
                    var splitText = new SplitText(title, {
                        type: "lines, words",
                        linesClass: 'split-line',
                        wordsClass: 'split-words',
                    });
                    var linesTitle = splitText.lines;
                    elementToClear = [arrows, blocText];
                    tl.from(images[0], 0, {
                        opacity: 0
                    }, 0);
                    tl.from(images, speed / 2, {
                        ease: Power4.easeOut,
                        css: {
                            top: '50px'
                        }
                    }, 0);
                    tl.staggerFrom([images[1], images[2]], 0, {
                        ease: Back.easeOut,
                        opacity: 0,
                        x: 0,
                        y: 0
                    });
                    tl.set(images[0], {
                        css: {
                            "transform": "translate3d(0, 0, 4px)"
                        }
                    });
                    tl.set(images, {
                        clearProps: "all"
                    });
                    tl.from(arrows[0], speed / 2, {
                        ease: Power4.easeOut,
                        x: 50,
                        opacity: 0
                    }, 'start');
                    tl.from(arrows[1], speed / 2, {
                        ease: Power4.easeOut,
                        x: -50,
                        opacity: 0
                    }, 'start');
                    tl.staggerFrom(linesTitle, speed / 2, {
                        y: linesTitle[0].offsetHeight * linesTitle.length,
                        ease: Power4.easeOut
                    }, 0.1, 'start');
                    tl.staggerFrom(blocText, speed / 2, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, 0.1, 'start');
                } else if (element.data('scrollReveal') === 'text-rounded-images') {
                    var images = element.find($('.is--rounded-image'));
                    var title = element.find($('.is--title > *'));
                    title.css('overflow', 'hidden');
                    var blocText = element.find($('.is--text, .is--button'));
                    var splitText = new SplitText(title, {
                        type: "lines, words",
                        linesClass: 'split-line',
                        wordsClass: 'split-words',
                    });
                    var linesTitle = splitText.lines;
                    elementToClear = [images, blocText];
                    tl.staggerFrom(images, speed * 1.5, {
                        ease: Back.easeOut,
                        opacity: 0,
                        scale: 0
                    }, 0.3);
                    tl.staggerFrom(linesTitle, speed / 2, {
                        y: linesTitle[0].offsetHeight * linesTitle.length,
                        ease: Power4.easeOut
                    }, 0.1, '-=2.5');
                    tl.staggerFrom(blocText, speed / 2, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, 0.1, '-=2.2');
                    if (element.hasClass('module-franchise--container')) {
                        offset = -400;
                    }
                } else if (element.data('scrollReveal') === 'services') {
                    var triggerElement = '.services--container';
                    var heightElement = $(triggerElement).outerHeight();
                    var background = $('#app');
                    elementToClear = [bloc, background];

                } else if (element.data('scrollReveal') === 'list-item') {
                    elementToClear = undefined;
                    if (element.hasClass('faq-bloc')) {
                        tl.staggerFrom(element, speed / 2, {
                            ease: Power4.easeOut,
                            y: 50,
                            opacity: 0
                        }, 0.3);
                        offset = -200;
                    } else {
                        tl.staggerFrom(element, speed * 1.5, {
                            ease: Back.easeOut,
                            y: 100,
                            opacity: 0
                        }, 0.3);
                        offset = -300;
                    }
                } else if (element.data('scrollReveal') === 'image-text') {
                    var images, blocText, subTitle, button, title;
                    images = element.find($('.is--bloc-image'));
                    if (element.find($('.video')).length) {
                        images = element.find($('.video'));
                    }
                    blocText = element.find($('.is--text'));
                    subtitle = element.find($('.is--subtitle'));
                    if (element.find($('.is--bloc-input')).length) {
                        blocText = element.find($('.is--text, .is--bloc-input'));
                    }
                    button = element.find($('span.is--button'));
                    title = element.find($('.is--title > *'));
                    if (element.hasClass('service-row')) {
                        blocText = element.find($('.is--bloc-text'));
                        title = 0;
                    }
                    if (title.length) {
                        title.css('overflow', 'hidden');
                        var splitText = new SplitText(title, {
                            type: "lines, words",
                            linesClass: 'split-line',
                            wordsClass: 'split-words',
                        });
                        var linesTitle = splitText.lines;
                    }
                    elementToClear = [images, blocText, button];
                    if (element.find($('.is--bloc-button')).length) {
                        button = element.find($('.is--bloc-button'));
                        elementToClear = [images, blocText];
                    }
                    if (element.hasClass('has--bg')) {
                        if (element.prev('.is--bloc-image-background').length) {
                            var background = element.prev('.is--bloc-image-background');
                        } else {
                            var background = element;
                        }
                        tl.from(background, speed / 2, {
                            ease: Power4.easeOut,
                            y: 200,
                            opacity: 0
                        });
                        offset = -200;
                        elementToClear = [images, blocText, background];
                    }
                    if (images.length) {
                        tl.from(images[0], speed, {
                            ease: Back.easeOut,
                            opacity: 0
                        });
                        if (images[1]) {
                            tl.from(images[1], speed / 2, {
                                ease: Back.easeOut,
                                opacity: 0,
                                x: -70,
                                y: -70
                            }, '-=0.5');
                        }
                    }
                    if (title.length) {
                        tl.staggerFrom(linesTitle, 0.8, {
                            y: linesTitle[0].offsetHeight * linesTitle.length,
                            ease: Power4.easeOut
                        }, 0.1, '-=0.5');
                    }
                    tl.staggerFrom(blocText, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, '-=0.4');
                    tl.from(subtitle, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, '-=0.4');
                    tl.from(button, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, '-=0.4');
                } else if (element.data('scrollReveal') === 'find-us-map') {
                    var bloc = element.find($('.find-us-prefooter-inner--container'));
                    var input = element.find($('.is--bloc-text form'));
                    var title = element.find($('.is--bloc-text h1'));
                    title.css('overflow', 'hidden');
                    var splitText = new SplitText(title, {
                        type: "lines, words",
                        linesClass: 'split-line',
                        wordsClass: 'split-words',
                    });
                    var linesTitle = splitText.lines;
                    elementToClear = [bloc, input];
                    tl.from(bloc, speed / 2, {
                        ease: Power4.easeOut,
                        y: '100%'
                    });
                    tl.staggerFrom(linesTitle, 0.8, {
                        y: linesTitle[0].offsetHeight * linesTitle.length,
                        ease: Power4.easeOut
                    }, 0.1, '-=0');
                    tl.from(input, speed, {
                        ease: Power4.easeOut,
                        css: {
                            'width': 0,
                            'opacity': 0
                        }
                    });
                } else if (element.data('scrollReveal') === 'history') {
                    var blocText = element.find($('.is--text'));
                    var timeline = element.find($('.timeline--container'));
                    var title = element.find($('.is--title h1'));
                    title.css('overflow', 'hidden');
                    var splitText = new SplitText(title, {
                        type: "lines, words",
                        linesClass: 'split-line',
                        wordsClass: 'split-words',
                    });
                    var linesTitle = splitText.lines;
                    elementToClear = [blocText];
                    tl.staggerFrom(linesTitle, 0.8, {
                        y: linesTitle[0].offsetHeight * linesTitle.length,
                        ease: Power4.easeOut,
                        delay: delay
                    }, 0.1);
                    tl.staggerFrom(blocText, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, 0.1, '-=0.5');
                    tl.from(timeline, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, '-=0.8');
                    offset = -500;
                } else if (element.data('scrollReveal') === 'form') {
                    var blocText = element.find($('.is--text'));
                    var form = element.find($('form'));
                    var title = element.find($('.is--title > *'));
                    title.css('overflow', 'hidden');
                    if (title.length) {
                        var splitText = new SplitText(title, {
                            type: "lines, words",
                            linesClass: 'split-line',
                            wordsClass: 'split-words',
                        });
                        var linesTitle = splitText.lines;
                        tl.staggerFrom(linesTitle, 0.8, {
                            y: linesTitle[0].offsetHeight * linesTitle.length,
                            ease: Power4.easeOut
                        }, 0.1);
                    }
                    elementToClear = [blocText];
                    tl.staggerFrom(blocText, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, 0.1, '-=0.5');
                    tl.from(form, 0.8, {
                        ease: Power4.easeOut,
                        y: 50,
                        opacity: 0
                    }, '-=0.5');
                    offset = -300;
                } else if (element.data('scrollReveal') === 'footer') {
                    var footer = element.find($('.footer'));
                    var bloc = element.find($('.pre-footer--container'));
                    var newsletter = element.find($('.newsletter-popup--container'));
                    elementToClear = [newsletter, bloc, footer];
                    tl.from(footer, speed, {
                        ease: Power4.easeOut,
                        css: {
                            top: '400px'
                        }
                    });
                    tl.from(bloc, speed / 2, {
                        ease: Power4.easeOut,
                        y: 160,
                        opacity: 0
                    }, '-=0.5');
                    tl.set($('.footer-big-container'), {
                        css: {
                            overflow: 'visible'
                        }
                    })
                    tl.set(bloc, {
                        css: {
                            zIndex: 1
                        }
                    })
                    tl.from(newsletter, speed / 2, {
                        ease: Power4.easeOut,
                        opacity: 0
                    });
                    offset = -300;
                } else {
                    elementToClear = element;
                    tl.from(element, speed, {
                        ease: Back.easeOut,
                        y: 100,
                        opacity: 0,
                        scale: 0.9
                    });
                }
                tl.play();
                GLOBAL.attributes.scene.offset(offset);
                GLOBAL.attributes.scene.setTween(tl);
                GLOBAL.attributes.scene.addTo(controller);
            });
        },
        initParallaxAnimation: function() {
            return;
            var controllerParallax = new ScrollMagic.Controller({
                refreshInterval: 0,
            });
            $('.page .i-parallax').each(function() {
                var element = $(this);
                var sceneParallax = new ScrollMagic.Scene({
                    triggerElement: this,
                    triggerHook: 'onEnter',
                    reverse: true,
                    duration: '200%'
                });
                var tl = new TimelineLite();
                tl.fromTo(this, Math.floor((Math.random() * 1.5) + 2), {
                    y: '20',
                }, {
                    y: '-50',
                    ease: Linear.easeNone
                });
                sceneParallax.setTween(tl);
                sceneParallax.addTo(controllerParallax);
            });
        },
        initBigSlider: function() {
            if ($('.big-slider-inner--container').length) {
                $('.big-slider-inner--container').each(function(index, el) {
                    var $statusRegular = $(this).siblings('.pagination_big_slider');
                    $(this).on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
                        var i = (currentSlide ? currentSlide : 0) + 1;
                        $statusRegular.text(i + '/' + slick.slideCount);
                    });
                    $(this).slick({
                        mobileFirst: true,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        speed: 500,
                        arrows: true,
                        prevArrow: '<button type="button" class="slick-prev"><div class="circle"><svg version="1.1" x="0px" y="0px" viewBox="0 0 7 13" style="enable-background:new 0 0 7 13;" xml:space="preserve"><path d="M0.3,6.7l6,6c0.1,0.1,0.3,0.1,0.4,0s0.1-0.3,0-0.4L0.9,6.5l5.8-5.8c0.1-0.1,0.1-0.3,0-0.4c0,0-0.1-0.1-0.2-0.1 s-0.1,0-0.2,0.1l-6,6C0.2,6.4,0.2,6.6,0.3,6.7z"/></svg></div></button>',
                        nextArrow: '<button type="button" class="slick-next"><div class="circle"><svg version="1.1" x="0px" y="0px" viewBox="0 0 7 13" style="enable-background:new 0 0 7 13;" xml:space="preserve"><path d="M0.3,6.7l6,6c0.1,0.1,0.3,0.1,0.4,0s0.1-0.3,0-0.4L0.9,6.5l5.8-5.8c0.1-0.1,0.1-0.3,0-0.4c0,0-0.1-0.1-0.2-0.1 s-0.1,0-0.2,0.1l-6,6C0.2,6.4,0.2,6.6,0.3,6.7z"/></svg></div></button>',
                    });
                });
                $('.big-slider--container').each(function() {
                    if (!UTILS.methods.isMobile() && (!UTILS.methods.isMobile() && UTILS.methods.getBrowser() !== 'ie')) {
                        cursorEvents($(this).find($('.big-slider-img')));
                    }
                });

                function cursorEvents(elem) {
                    var cursor = $('.cursor__slider');
                    elem.on('mousedown', function() {
                        cursor.addClass('dragged');
                    });
                    elem.on('mouseout', function() {
                        cursor.removeClass('dragged');
                    });
                    elem.on('mouseover', function() {
                        $('body').addClass('no-cursor');
                        if (elem.parents('.big-slider--container').hasClass('theme-blue')) {
                            cursor.attr('data-color', 'blue');
                        } else if (elem.parents('.big-slider--container').hasClass('theme-peach')) {
                            cursor.attr('data-color', 'peach');
                        } else if (elem.parents('.big-slider--container').hasClass('theme-teal')) {
                            cursor.attr('data-color', 'teal');
                        }
                        $(document).on("mousemove", function(evt) {
                            cursor.css({
                                transform: 'translateX(' + evt.pageX + 'px) translateY(' + evt.pageY + 'px)'
                            });
                            cursor.find($('.cursor__wrapper')).css({
                                transform: 'scale(1)'
                            });
                        });
                    });
                    elem.on('mouseout', function() {
                        $('body').removeClass('no-cursor');
                        $(document).off('mousemove');
                        cursor.find($('.cursor__wrapper')).css({
                            transform: 'scale(0)'
                        });
                    });
                }
            }
        },
        footerGallery: function() {
            $('.footer-gallery').slick({
                mobileFirst: true,
                autoplay: true,
                autoplaySpeed: 1000,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                speed: 500,
                arrows: false,
                prevArrow: '<button type="button" class="slick-prev"><div class="circle"><svg version="1.1" x="0px" y="0px" viewBox="0 0 7 13" style="enable-background:new 0 0 7 13;" xml:space="preserve"><path d="M0.3,6.7l6,6c0.1,0.1,0.3,0.1,0.4,0s0.1-0.3,0-0.4L0.9,6.5l5.8-5.8c0.1-0.1,0.1-0.3,0-0.4c0,0-0.1-0.1-0.2-0.1 s-0.1,0-0.2,0.1l-6,6C0.2,6.4,0.2,6.6,0.3,6.7z"/></svg></div></button>',
                nextArrow: '<button type="button" class="slick-next"><div class="circle"><svg version="1.1" x="0px" y="0px" viewBox="0 0 7 13" style="enable-background:new 0 0 7 13;" xml:space="preserve"><path d="M0.3,6.7l6,6c0.1,0.1,0.3,0.1,0.4,0s0.1-0.3,0-0.4L0.9,6.5l5.8-5.8c0.1-0.1,0.1-0.3,0-0.4c0,0-0.1-0.1-0.2-0.1 s-0.1,0-0.2,0.1l-6,6C0.2,6.4,0.2,6.6,0.3,6.7z"/></svg></div></button>',
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 1024,
                    settings: {
                        arrows: true,
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        arrows: true,
                    }
                }]
            });
        },
        initGradient: function() {
            UTILS.methods.createGradient($('svg#assign-defs')[0], 'gradient', [{
                offset: '0%',
                'stop-color': '#008883'
            }, {
                offset: '100%',
                'stop-color': '#6AB550'
            }]);
            $('svg path.gradient').attr('fill', 'url(#gradient)');
        },
        initImageDistorted: function() {
            var jsLinkArray = ["/js/daousteco/pixi.min.js", "/js/daousteco/main5.js?ver=6.2.3", "/js/daousteco/imagesloaded.pkgd.min.js", ];
            UTILS.methods.loadJsPerfectly(jsLinkArray, $(".barba-container"), function() {
                setTimeout(function() {
                    var spriteImages = document.querySelectorAll('.slide-item__image');
                    var spriteImagesSrc = [];
                    var texts = [];
                    for (var i = 0; i < spriteImages.length; i++) {
                        var img = spriteImages[i];
                        if (img.nextElementSibling) {
                            texts.push(img.nextElementSibling.innerHTML);
                        } else {
                            texts.push('');
                        }
                        spriteImagesSrc.push(img.getAttribute('src'));
                    }
                    var initCanvasSlideshow = new CanvasSlideshow({
                        sprites: spriteImagesSrc,
                        displacementImage: '/images/daousteco/clouds.jpg',
                        autoPlay: true,
                        autoPlaySpeed: [4, 3],
                        displaceScale: [5000, 10000],
                        interactive: true,
                        interactionEvent: 'click',
                        displaceAutoFit: false,
                        dispatchPointerOver: true,
                    });
                    if (GLOBAL.attributes.loadedTimes === 1) {
                        GLOBAL.methods.loaderAnim(true, 'long');
                    } else {
                        GLOBAL.methods.loaderAnim(true);
                    }
                }, 800);
            });
        },
        initSearchLocation: function() {},
        goToMap: function() {
            var place = GLOBAL.attributes.autocomplete.getPlace();
            if (place.geometry) {
                var baseUrl = $('.find-us--container').last().find('a').attr('href');
                var urlAdd = "?SID=";
                window.location = baseUrl + urlAdd + place.geometry.location;
            }
        },
        initSelecDropdown: function() {
            $('select').attr('class', '');
            $('select').niceSelect();
        },
        destroyNiceSelect: function() {
            $('select').niceSelect('destroy');
        },
        windowOnScroll: function() {

        },
        windowOnResize: function() {

        },
    };
    HOME.attributes = {};
    HOME.methods = {
        init: function() {
            if (!UTILS.methods.isMobile()) {
                GLOBAL.methods.initImageDistorted();
            } else {
                if (GLOBAL.attributes.loadedTimes === 1) {
                    GLOBAL.methods.loaderAnim(true, 'long');
                } else {
                    GLOBAL.methods.loaderAnim(true);
                }
            }
            GLOBAL.methods.initBigSlider();
            this.initCardsSlider();
            this.changeHeader();
        },
        initCardsSlider: function() {
            var cardslider = $('.my-cardslider').cardslider({
                swipe: true,
                dots: false,
                loop: true,
                direction: 'left',
                keys: {
                    next: 38,
                    prev: 40
                }
            }).data('cardslider');
        },
        changeHeader: function() {
            $('.header').addClass('is--home');
            var sliderOffsetHeight = $('.imagedistorted--inner-container').height();
            if (!UTILS.methods.isMobile() && (!UTILS.methods.isMobile() && UTILS.methods.getBrowser() !== 'ie') && (!UTILS.methods.isMobile() && UTILS.methods.getBrowser() !== 'edge')) {
                GLOBAL.attributes.scrollbar.addListener(function() {
                    var st = GLOBAL.attributes.scrollbar.offset;
                    st = st.y;
                    if (st > sliderOffsetHeight) {
                        if ($('.barba-container').data('namespace') === 'HOME') {
                            $('.header, .header-extends').removeClass('is--home');
                        }
                    } else {
                        if ($('.barba-container').data('namespace') === 'HOME') {
                            $('.header, .header-extends').addClass('is--home');
                        }
                    }
                });
            } else {
                $(window).on('scroll', function() {
                    var st = $(window).scrollTop();
                    if (st > sliderOffsetHeight) {
                        if ($('.barba-container').data('namespace') === 'HOME') {
                            $('.header, .header-extends').removeClass('is--home');
                        }
                    } else {
                        if ($('.barba-container').data('namespace') === 'HOME') {
                            if (!$('.header').hasClass('is--open')) {
                                $('.header, .header-extends').addClass('is--home');
                            }
                        }
                    }
                });
            }
        }
    };
    SOINS.attributes = {};
    SOINS.methods = {
        init: function() {
            GLOBAL.methods.initBigSlider();
            GLOBAL.methods.initSearchLocation();
        },
    };
    SINGLESOINS.attributes = {};
    SINGLESOINS.methods = {
        init: function() {},
        removeCursor: function() {
            var cursor = $('.cursor__slider');
            $('body').removeClass('no-cursor');
            $(document).off('mousemove');
            cursor.find($('.cursor__wrapper')).css({
                transform: 'scale(0)'
            });
            cursor.removeClass('dragged');
        }
    };
    SERVICES.attributes = {};
    SERVICES.methods = {
        init: function() {
            GLOBAL.methods.initBigSlider();
            if (location.hash) {
                GLOBAL.methods.loaderAnim(true, undefined, function() {
                    setTimeout(function() {
                        if (GLOBAL.attributes.scrollbar) {
                            GLOBAL.attributes.scrollbar.scrollIntoView(document.querySelector(location.hash), {
                                offsetTop: 100
                            });
                        } else {
                            $('html, body').animate({
                                scrollTop: $(location.hash).offset().top - 100
                            }, 500);
                        }
                    }, 1000);
                });
            } else {
                GLOBAL.methods.loaderAnim(true);
            }
        },
    };
    JOURNAL.attributes = {};
    JOURNAL.methods = {
        init: function() {
            $('select[name="subject"]').change(function() {
                var theFilter = $(this).val();
                if (theFilter == "all") {
                    $(".service-row").removeClass('show');
                    setTimeout(function() {
                        $(".service-row").css('display', 'block');
                        setTimeout(function() {
                            $(".service-row").addClass('show');
                        }, 50);
                    }, 200);
                } else {
                    $(".service-row").removeClass('show');
                    setTimeout(function() {
                        $(".service-row").css('display', 'none');
                        $(".service-row[data-cat='" + theFilter + "']").css('display', 'block');
                        setTimeout(function() {
                            $(".service-row[data-cat='" + theFilter + "']").addClass('show');
                        }, 50);
                    }, 200);
                }
            });
            GLOBAL.methods.initBigSlider();
            var jsLinkArray = ["/js/daousteco/jquery.nice-select.js"];
            UTILS.methods.loadJsPerfectly(jsLinkArray, $(".barba-container"), function() {
                GLOBAL.methods.initSelecDropdown();
            });
        },
    };
    GENERIC.attributes = {};
    GENERIC.methods = {
        init: function() {},
    };
    INCOMING.attributes = {};
    INCOMING.methods = {
        init: function() {
            if (!UTILS.methods.isMobile()) {
                GLOBAL.methods.initImageDistorted();
            } else {
                if (GLOBAL.attributes.loadedTimes === 1) {
                    GLOBAL.methods.loaderAnim(true, 'long');
                } else {
                    GLOBAL.methods.loaderAnim(true);
                }
            }
            $('.header').addClass('is--home');
        },
    };
    CONTACT.attributes = {};
    CONTACT.methods = {
        init: function() {
        },
        windowOnResize: function() {
            $(window).on('resize', function() {
                if (!UTILS.methods.isMobile()) {
                    GLOBAL.methods.initSelecDropdown();
                } else {
                    GLOBAL.methods.destroyNiceSelect();
                }
            });
        }
    };
    PROMOTIONS.attributes = {};
    PROMOTIONS.methods = {
        init: function() {
            this.initFormsEvents();
            GLOBAL.methods.initSearchLocation();
            $('.promotion img').css('height', '0');
            setTimeout(function() {
                $('.promotion img').css('height', 'auto');
            }, 300);
            if (location.hash) {
                GLOBAL.methods.loaderAnim(true, undefined, function() {
                    setTimeout(function() {
                        if (GLOBAL.attributes.scrollbar) {
                            GLOBAL.attributes.scrollbar.scrollIntoView(document.querySelector(location.hash), {
                                offsetTop: 0
                            });
                        } else {
                            $('html, body').animate({
                                scrollTop: $(location.hash).offset().top - 100
                            }, 500);
                        }
                    }, 1000);
                });
            } else {
                if (GLOBAL.attributes.loadedTimes === 1) {
                    GLOBAL.methods.loaderAnim(true, 'long');
                } else {
                    GLOBAL.methods.loaderAnim(true);
                }
            }
        },
        initFormsEvents: function() {
            var value;
            var button;
            $('.btn').stop().on('click', function() {
                button = $(this);
                button.addClass('clicked');
                value = button.parents('.is--bloc-input').find($('input')).val();
                if (PROMOTIONS.methods.checkEmailForm(value) === 'success') {
                    var subjectEmail = button.parents('.promotion-container').find($('h2')).html();
                    var imageSrc = button.parents('.promotion-container').find($('img')).attr('src');
                    var promoCode = button.parents('.promotion-container').data('promo-code');
                    var promoText = button.parents('.promotion-container').find($('.promotions.is--text p')).html();
                    var mailTo = value;
                    var lang = $('.lang_sel_current p').html();
                    if (!button.hasClass('mailchimp')) {
                        $.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            type: 'POST',
                            dataType: "html",
                            data: ({
                                action: 'send_promo_email',
                                subjectEmail: subjectEmail,
                                imageSrc: imageSrc,
                                promoCode: promoCode,
                                promoText: promoText,
                                mailTo: mailTo,
                                lang: lang,
                            }),
                            beforeSend: function() {},
                            success: function(response) {}
                        });
                    } else {
                        if (button.parents('#subscribe_signupForm').find($('input.newsletter-checkbox')).is(':checked')) {
                            var checked = true;
                            var toSubscribe = value;
                            $.ajax({
                                url: "/wp-admin/admin-ajax.php",
                                type: 'POST',
                                dataType: "html",
                                data: ({
                                    action: 'register_newsletter',
                                    checked: checked,
                                    toSubscribe: toSubscribe,
                                }),
                                beforeSend: function() {},
                                success: function(response) {}
                            });
                        }
                    }
                    setTimeout(function() {
                        button.addClass('success');
                        button.parents('.is--bloc-input').find($('input')).prop('disabled', true);
                    }, 300);
                } else {
                    setTimeout(function() {
                        button.addClass('error');
                        setTimeout(function() {
                            button.removeClass('clicked');
                            button.removeClass('error');
                        }, 2000);
                    }, 300);
                }
            });
            $('input.promotions-input-email').keypress(function(e) {
                button = $(this).parents('.is--bloc-input').find($('button.btn'));
                value = $(this).val();
                if (e.which == 13) {
                    e.preventDefault();
                    if (PROMOTIONS.methods.checkEmailForm(value) === 'success') {
                        setTimeout(function() {
                            button.addClass('success');
                            button.parents('.is--bloc-input').find($('input')).prop('disabled', true);
                        }, 300);
                    } else {
                        setTimeout(function() {
                            button.addClass('error');
                            setTimeout(function() {
                                button.removeClass('clicked');
                                button.removeClass('error');
                            }, 2000);
                        }, 300);
                    }
                }
            });
        },
        checkEmailForm: function(value) {
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            if (testEmail.test(value)) {
                return 'success';
            } else {
                return 'error';
            }
        }
    };
    FRANCHISE.attributes = {};
    FRANCHISE.methods = {
        init: function() {
            $('form.wpcf7-form').on('submit', function() {
                var uniqTransactionID = new Date().valueOf();
                var gtag_callback = function() {
                    if (typeof(url) != 'undefined') {
                        window.location = url;
                    }
                };
                if (UTILS.methods.getLang() === 'fr') {
                    gtag('event', 'conversion', {
                        'send_to': 'AW-814957963/VTK_CMzkl34Qi4vNhAM',
                        'user_id': uniqTransactionID,
                        'event_callback': gtag_callback
                    });
                } else {
                    gtag('event', 'conversion', {
                        'send_to': 'AW-814957963/UudNCO3rlH4Qi4vNhAM',
                        'user_id': uniqTransactionID,
                        'event_callback': gtag_callback
                    });
                }
            });
            GLOBAL.methods.initGradient();
            var jsLinkArray = ["/js/daousteco/jquery.nice-select.js"];
            UTILS.methods.loadJsPerfectly(jsLinkArray, $(".barba-container"), function() {
                if (!UTILS.methods.isMobile()) {
                    GLOBAL.methods.initSelecDropdown();
                }
            });
            this.windowOnResize();
            if (UTILS.methods.isMobile()) {
                var touch = 0;
                $('.btn-contact').on('touchstart', function(e) {
                    touch++;
                    e.stopPropagation();
                    if (touch === 2) {
                        $('html, body').animate({
                            scrollTop: $(".form--container").offset().top
                        }, 1000);
                    }
                });
            } else {
                $('.btn-contact').on('click', function() {
                    if (GLOBAL.attributes.scrollbar) {
                        GLOBAL.attributes.scrollbar.scrollIntoView(document.querySelector('.form--container'));
                    } else {
                        $('html, body').animate({
                            scrollTop: $('.form--container').offset().top - 100
                        }, 500);
                    }
                });
            }
        },
        windowOnResize: function() {
            $(window).on('resize', function() {
                if (!UTILS.methods.isMobile()) {
                    GLOBAL.methods.initSelecDropdown();
                } else {
                    GLOBAL.methods.destroyNiceSelect();
                }
            });
        }
    };
    QSN.attributes = {
        itemContainer: $('.timeline--container'),
    };
    QSN.methods = {
        init: function() {
            setTimeout(function() {
                QSN.methods.initTimeLine();
            }, 200);
            this.timeLineEvents();
            this.iFrameEvents();
            GLOBAL.methods.initGradient();
        },
        initTimeLine: function() {
            $('.timeline-item--container:first').addClass('show');
            $('.dots-item--container:first').addClass('selected');
            var itemHeight = $('.timeline-item--container:first').height();
            $('.timeline--container').css('height', itemHeight + 'px');
            if (UTILS.methods.isMobile() && $(window).width() < 768) {
                QSN.methods.translateTimeLine(1);
            }
        },
        timeLineEvents: function() {
            $('.dots-item--container').on('click', function() {
                QSN.methods.timeLineClick($(this));
            });
        },
        translateTimeLine: function(index) {
            var timeline = $('.timeline-dots');
            var translate;
            var toMove;
            toMove = 48;
            translate = (-toMove * index) + (toMove / 2);
            timeline.css({
                'transform': 'translateX(' + translate + 'px)'
            });
        },
        timeLineClick: function(dot) {
            var getDataDate = dot.data('date');
            var itemDisplayed = $('.timeline-item--container.show');
            var itemToShow = $('.timeline--container').find('[data-date="' + getDataDate + '"]');
            var itemIndex = dot.index() + 1;
            var itemHeight = itemDisplayed.height();
            if (itemDisplayed.data('date') === getDataDate) {} else {
                itemDisplayed.removeClass('show');
                if (UTILS.methods.isMobile() && $(window).width() < 768) {
                    QSN.methods.translateTimeLine(itemIndex);
                }
                $('.dots-item--container').removeClass('selected');
                setTimeout(function() {
                    itemToShow.addClass('show');
                    dot.addClass('selected');
                    QSN.attributes.itemContainer.css('height', itemHeight + 'px');
                }, 300);
            }
        },
        iFrameEvents: function() {
            if ($('.is--video-container').length) {
                $('.iframe-button').click(function() {
                    QSN.methods.videoFullscreen($(this).parent().find($(('iframe'))));
                });
            }
            $('.iframe').click(function() {
                if ($(this).hasClass('displayed')) {
                    $(this).removeClass('displayed');
                    $('body').removeClass('is--fixed');
                    setTimeout(function() {
                        $('.iframe iframe').remove();
                    }, 500);
                }
            });
        },
        videoFullscreen: function(el) {
            var element = el;
            var iframe = $('.iframe');
            var iframeData = element.attr('data-full');
            if (UTILS.methods.isMobile()) {
                iframe.append('<iframe src="https://player.vimeo.com/video/' + iframeData + '?autoplay=1&loop=1&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            } else {
                iframe.append('<iframe src="https://player.vimeo.com/video/' + iframeData + '?background=1&mute=0&autoplay=1&loop=1&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            }
            iframe.addClass('displayed');
            setTimeout(function() {
                $('body').addClass('is--fixed');
            }, 300);
        },
    };
    TECHNOLOGIES.attributes = {};
    TECHNOLOGIES.methods = {
        init: function() {
            if (!UTILS.methods.isMobile() && (!UTILS.methods.isMobile() && UTILS.methods.getBrowser() !== 'ie')) {
                var jsLinkArray = ["/js/daousteco/OES_texture_float_linear-polyfill.js", "/js/daousteco/lightgl.js", "/js/daousteco/cubemap.js", "/js/daousteco/renderer.js", "/js/daousteco/water.js", "/js/daousteco/main.js?ver=6.3", "/js/daousteco/particles.js", ];
                UTILS.methods.loadJsPerfectly(jsLinkArray, $(".barba-container"), function() {
                    setTimeout(function() {
                        if (GLOBAL.attributes.loadedTimes === 1) {
                            GLOBAL.methods.loaderAnim(true, 'long');
                        } else {
                            GLOBAL.methods.loaderAnim(true);
                        }
                    }, 800);
                });
            } else {
                if (GLOBAL.attributes.loadedTimes === 1) {
                    GLOBAL.methods.loaderAnim(true, 'long');
                } else {
                    GLOBAL.methods.loaderAnim(true);
                }
                $('.animation-canvas').attr("style", "display: flex !important");
                $('.animation-canvas').empty().append('<img src="/wp-content/themes/daoust/assets/images/porte-responsive.png" alt="logo-daoust" width="50%">');
            }
            GLOBAL.methods.initGradient();
        },
    };
    MAP.attributes = {
        findMebtn: $('.icon-geoloc--container'),
    };
    MAP.methods = {
        init: function() {
            var jsLinkArray = ["/js/daousteco/map.js", ];
            UTILS.methods.loadJsPerfectly(jsLinkArray, $(".barba-container"));
            if (!UTILS.methods.isMobile()) {
                this.initScrollBar();
                this.countMarkers();
                this.inputSearch();
                this.animPanel();
            } else {
                MAP.attributes.findMebtn.on('click', function() {
                    GMAP.methods.centerGeolocMobile();
                });
            }
        },
        initScrollBar: function() {
            $(".panel-locations--container, .horaires--list").mCustomScrollbar({
                theme: "my-theme"
            });
        },
        inputSearch: function() {
            $('#search-form-input,#search-form-input-mobile').on('keypress', function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                }
            });
        },
        countMarkers: function() {
            var count = $('.location--box').length;
            $('.slider--header-title h2 span').html(count);
        },
        animPanel: function() {
        },
        doSearch: function(value) {
            $.ajax({
                url: "/wp-admin/admin-ajax.php",
                type: 'POST',
                dataType: "json",
                data: ({
                    action: 'map_search',
                    value: value
                }),
                beforeSend: function() {},
                success: function(data) {
                    $('.locations--slider').empty();
                    $('.locations--slider').append(data.response);
                }
            });
        }
    };
    FAQ.attributes = {};
    FAQ.methods = {
        init: function() {
            this.initAccordeon();
            this.windowOnResize();
        },
        initAccordeon: function() {
            $('.faq-bloc').each(function() {
                var elem = $(this);
                elem.css({
                    'height': FAQ.methods.calcHeightElem(elem),
                });
                elem.on('click', function() {
                    var elem = $(this);
                    var elemHeight = {};
                    elemHeight.closeHeight = elem.find($('.question')).outerHeight();
                    elemHeight.openHeight = elemHeight.closeHeight + elem.find($('.reponse')).outerHeight();
                    if (elem.hasClass('is--open')) {
                        elem.css({
                            'height': elemHeight.closeHeight,
                        });
                        elem.removeClass('is--open');
                    } else {
                        elem.addClass('is--open');
                        elem.css({
                            'height': elemHeight.openHeight,
                        });
                    }
                });
            });
        },
        calcHeightElem: function(elem) {
            var height = elem.find($('.question')).outerHeight();
            return height;
        },
        windowOnResize: function() {
            $(window).on('resize', function() {
                $('.faq-bloc').each(function() {
                    var elem = $(this);
                    elem.css({
                        'height': FAQ.methods.calcHeightElem(elem),
                    });
                });
            });
        },
    };
    CARRIERE.attributes = {
        elemHeight: [],
        formOffsetTop: undefined,
    };
    CARRIERE.methods = {
        init: function() {
            this.initAccordeon();
            var jsLinkArray = ["/js/daousteco/jquery.nice-select.js"];
            this.fillOptionSelectForm();
            UTILS.methods.loadJsPerfectly(jsLinkArray, $(".barba-container"), function() {
                if (!UTILS.methods.isMobile()) {
                    GLOBAL.methods.initSelecDropdown();
                }
            });
            this.windowOnResize();
            this.onUploadFile();
            $('select#citySelect').on('change', function() {
                CARRIERE.methods.filterCity(this.value);
            });
        },
        initAccordeon: function() {
            $('.post-bloc').each(function(index) {
                var elem = $(this);
                var submitBtn = elem.find($('.submit'));
                CARRIERE.attributes.elemHeight[index] = CARRIERE.methods.calcHeightElem(elem);
                elem.find($('.post--desc-part')).css({
                    'height': 0,
                });
                elem.on('click', function() {
                    var elem = $(this);
                    if (elem.hasClass('is--open')) {
                        elem.find($('.post--desc-part')).css({
                            'height': 0,
                        });
                        elem.removeClass('is--open');
                    } else {
                        elem.addClass('is--open');
                        elem.find($('.post--desc-part')).css({
                            'height': CARRIERE.attributes.elemHeight[index],
                        });
                    }
                });
                submitBtn.on('click', function(e) {
                    e.stopPropagation();
                    var elem = $(this);
                    if (GLOBAL.attributes.scrollbar) {
                        GLOBAL.attributes.scrollbar.scrollIntoView(document.querySelector('.form--container'));
                    } else {
                        $('html, body').animate({
                            scrollTop: $('.form--container').offset().top - 100
                        }, 500);
                    }
                    var select = $('select[name="PostSelect"]');
                    var labelSelect = select.siblings('.nice-select').find('span.current');
                    var option = elem.siblings('.post--top-part').find($('.is--title h3')).html();
                    var optionSelected = select.find($('option[value="' + option + '"]'));
                    var liSelected = select.siblings('.nice-select').find($('li[data-value="' + option + '"]'));
                    optionSelected.siblings().attr('selected', '');
                    optionSelected.attr('selected', 'selected');
                    liSelected.siblings().removeClass('selected');
                    liSelected.addClass('selected');
                    labelSelect.html(option);
                });
            });
        },
        calcHeightElem: function(elem) {
            var height = 0;
            elem.find($('.post--desc-part p')).each(function() {
                height += $(this).outerHeight(true);
            });
            height = height + elem.find($('.post--desc-part span')).outerHeight(true);
            return height;
        },
        fillOptionSelectForm: function() {
            var select = $('select[name="PostSelect"]');
            select.find($('option')).remove();
            $('.post-bloc').each(function(index) {
                var elem = $(this);
                var option = elem.find($('.is--title h3')).html();
                select.append('<option value="' + option + '">' + option + '</option');
            });
        },
        onUploadFile: function() {
            $("input#uploadInput").on('change', function() {
                var filename = $(this).val();
                var lastIndex = filename.lastIndexOf("\\");
                if (lastIndex >= 0) {
                    filename = filename.substring(lastIndex + 1);
                }
                $("input#uploadFile").val(filename);
            });
            if (UTILS.methods.getBrowser() == 'ie') {
                $(".btn-upload").on('click', function(e) {
                    e.preventDefault();
                    $("input#uploadInput").click();
                });
            }
        },
        filterCity: function(value) {
            var city = value;
            if (city === 'All') {
                $('.post-bloc').removeClass('is--hidden');
            } else {
                if ($('.post-bloc[data-city="' + city + '"]').hasClass('is--hidden')) {
                    $('.post-bloc[data-city="' + city + '"]').removeClass('is--hidden');
                }
                $('.post-bloc').not('[data-city="' + city + '"]').addClass('is--hidden');
            }
        },
        windowOnResize: function() {
            $(window).on('resize', function() {
                $('.post-bloc').each(function(index) {
                    var elem = $(this);
                    $('.post-bloc').removeClass('is--open');
                    elem.find($('.post--desc-part')).css({
                        'height': 0,
                    });
                    CARRIERE.attributes.elemHeight[index] = CARRIERE.methods.calcHeightElem(elem);
                });
                if (!UTILS.methods.isMobile()) {
                    GLOBAL.methods.initSelecDropdown();
                } else {
                    GLOBAL.methods.destroyNiceSelect();
                }
            });
        },
    };
    BARBA.methods = {
        initBarba: function() {
            var self = this;
            var Transition = Barba.BaseTransition.extend({
                start: function() {
                    Promise.all([this.newContainerLoading, this.fadeOut()]).then(this.fadeIn.bind(this));
                },
                fadeOut: function() {
                    if ($('.header').hasClass('is--open')) {
                        $('.header').removeClass('is--open');
                        GLOBAL.attributes.menuCloseBtn.html('menu');
                        $('body').removeClass('is--fixed');
                    }
                    if ($('.header').hasClass('is--home')) {
                        $('.header').removeClass('is--home');
                    }
                    $('.loading').addClass('is--active');
                    return $(this.oldContainer).animate({
                        opacity: 0
                    }).promise();
                },
                fadeIn: function() {
                    $('html,body').animate({
                        scrollTop: 0
                    }, 0);
                    var _this = this;
                    $(this.oldContainer).hide();
                    _this.done();
                }
            });
            Barba.Pjax.getTransition = function() {
                return Transition;
            };
            Barba.Dispatcher.on('transitionCompleted', function() {
                var targetUrl = Barba.Utils.getCurrentUrl();
                var datanamespace = $('.barba-container').data('namespace');
                setTimeout(function() {
                    if (datanamespace === 'HOME' || datanamespace === 'TECHNOLOGIES' || datanamespace === 'SERVICES' || datanamespace === 'PROMOTIONS' || datanamespace === 'INCOMING') {} else {
                        if (GLOBAL.attributes.loadedTimes === 1) {
                            GLOBAL.methods.loaderAnim(true, 'long');
                        } else {
                            GLOBAL.methods.loaderAnim(true);
                        }
                    }
                    if ($('.header').hasClass('is--hidden')) {
                        $('.header').removeClass('is--hidden');
                    }
                }, 500);
                PROJECT.destroyEvents();
                PROJECT.initEvents(datanamespace);
            });
            Barba.Pjax.start();
            Barba.Prefetch.init();
        },
    };
    PROJECT = {
        fire: function(namespace, funcname, args) {
            funcname = (funcname === undefined) ? 'init' : funcname;
            var space = eval(namespace);
            if (space !== '' && space) {
                if (space.methods[funcname]) {
                    space.methods[funcname](args);
                }
            }
        },
        initEvents: function(datanamespace) {
            PROJECT.fire(GLOBAL);
            PROJECT.fire(datanamespace);
            PROJECT.fire(GLOBAL, 'initGlobalAnimElem');
            PROJECT.fire(GLOBAL, 'animMainButton');
            if (!UTILS.methods.isMobile()) {
                //PROJECT.fire(GLOBAL, 'initScrollAnimation');
                //PROJECT.fire(GLOBAL, 'initParallaxAnimation');
            }
        },
        destroyEvents: function() {
            if ($('body').removeClass('no-cursor')) {
                var cursor = $('.cursor__slider');
                $('body').removeClass('no-cursor');
                $(document).off('mousemove');
                cursor.find($('.cursor__wrapper')).css({
                    transform: 'scale(0)'
                });
            }
        }
    };
    $(window).load(function() {
        GLOBAL.attributes.loaded = true;
        GLOBAL.methods.isLoaded();
    });
});
