! function(t, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define("Barba", [], e) : "object" == typeof exports ? exports.Barba = e() : t.Barba = e()
}(this, function() {
    return function(t) {
        function e(i) {
            if (n[i]) return n[i].exports;
            var o = n[i] = {
                exports: {},
                id: i,
                loaded: !1
            };
            return t[i].call(o.exports, o, o.exports, e), o.loaded = !0, o.exports
        }
        var n = {};
        return e.m = t, e.c = n, e.p = "http://localhost:8080/dist", e(0)
    }([function(t, e, n) {
        "function" != typeof Promise && (window.Promise = n(1));
        var i = {
            version: "0.0.10",
            BaseTransition: n(4),
            BaseView: n(6),
            BaseCache: n(8),
            Dispatcher: n(7),
            HistoryManager: n(9),
            Pjax: n(10),
            Prefetch: n(13),
            Utils: n(5)
        };
        t.exports = i
    }, function(t, e, n) {
        (function(e) {
            ! function(n) {
                function i() {}

                function o(t, e) {
                    return function() {
                        t.apply(e, arguments)
                    }
                }

                function r(t) {
                    if ("object" != typeof this) throw new TypeError("Promises must be constructed via new");
                    if ("function" != typeof t) throw new TypeError("not a function");
                    this._state = 0, this._handled = !1, this._value = void 0, this._deferreds = [], h(t, this)
                }

                function s(t, e) {
                    for (; 3 === t._state;) t = t._value;
                    return 0 === t._state ? void t._deferreds.push(e) : (t._handled = !0, void d(function() {
                        var n = 1 === t._state ? e.onFulfilled : e.onRejected;
                        if (null !== n) {
                            var i;
                            try {
                                i = n(t._value)
                            } catch (t) {
                                return void c(e.promise, t)
                            }
                            a(e.promise, i)
                        } else(1 === t._state ? a : c)(e.promise, t._value)
                    }))
                }

                function a(t, e) {
                    try {
                        if (e === t) throw new TypeError("A promise cannot be resolved with itself.");
                        if (e && ("object" == typeof e || "function" == typeof e)) {
                            var n = e.then;
                            if (e instanceof r) return t._state = 3, t._value = e, void u(t);
                            if ("function" == typeof n) return void h(o(n, e), t)
                        }
                        t._state = 1, t._value = e, u(t)
                    } catch (e) {
                        c(t, e)
                    }
                }

                function c(t, e) {
                    t._state = 2, t._value = e, u(t)
                }

                function u(t) {
                    2 === t._state && 0 === t._deferreds.length && d(function() {
                        t._handled || p(t._value)
                    });
                    for (var e = 0, n = t._deferreds.length; n > e; e++) s(t, t._deferreds[e]);
                    t._deferreds = null
                }

                function f(t, e, n) {
                    this.onFulfilled = "function" == typeof t ? t : null, this.onRejected = "function" == typeof e ? e : null, this.promise = n
                }

                function h(t, e) {
                    var n = !1;
                    try {
                        t(function(t) {
                            n || (n = !0, a(e, t))
                        }, function(t) {
                            n || (n = !0, c(e, t))
                        })
                    } catch (t) {
                        if (n) return;
                        n = !0, c(e, t)
                    }
                }
                var l = setTimeout,
                    d = "function" == typeof e && e || function(t) {
                        l(t, 0)
                    },
                    p = function(t) {
                        "undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", t)
                    };
                r.prototype.catch = function(t) {
                    return this.then(null, t)
                }, r.prototype.then = function(t, e) {
                    var n = new this.constructor(i);
                    return s(this, new f(t, e, n)), n
                }, r.all = function(t) {
                    var e = Array.prototype.slice.call(t);
                    return new r(function(t, n) {
                        function i(r, s) {
                            try {
                                if (s && ("object" == typeof s || "function" == typeof s)) {
                                    var a = s.then;
                                    if ("function" == typeof a) return void a.call(s, function(t) {
                                        i(r, t)
                                    }, n)
                                }
                                e[r] = s, 0 == --o && t(e)
                            } catch (t) {
                                n(t)
                            }
                        }
                        if (0 === e.length) return t([]);
                        for (var o = e.length, r = 0; r < e.length; r++) i(r, e[r])
                    })
                }, r.resolve = function(t) {
                    return t && "object" == typeof t && t.constructor === r ? t : new r(function(e) {
                        e(t)
                    })
                }, r.reject = function(t) {
                    return new r(function(e, n) {
                        n(t)
                    })
                }, r.race = function(t) {
                    return new r(function(e, n) {
                        for (var i = 0, o = t.length; o > i; i++) t[i].then(e, n)
                    })
                }, r._setImmediateFn = function(t) {
                    d = t
                }, r._setUnhandledRejectionFn = function(t) {
                    p = t
                }, void 0 !== t && t.exports ? t.exports = r : n.Promise || (n.Promise = r)
            }(this)
        }).call(e, n(2).setImmediate)
    }, function(t, e, n) {
        (function(t, i) {
            function o(t, e) {
                this._id = t, this._clearFn = e
            }
            var r = n(3).nextTick,
                s = Function.prototype.apply,
                a = Array.prototype.slice,
                c = {},
                u = 0;
            e.setTimeout = function() {
                return new o(s.call(setTimeout, window, arguments), clearTimeout)
            }, e.setInterval = function() {
                return new o(s.call(setInterval, window, arguments), clearInterval)
            }, e.clearTimeout = e.clearInterval = function(t) {
                t.close()
            }, o.prototype.unref = o.prototype.ref = function() {}, o.prototype.close = function() {
                this._clearFn.call(window, this._id)
            }, e.enroll = function(t, e) {
                clearTimeout(t._idleTimeoutId), t._idleTimeout = e
            }, e.unenroll = function(t) {
                clearTimeout(t._idleTimeoutId), t._idleTimeout = -1
            }, e._unrefActive = e.active = function(t) {
                clearTimeout(t._idleTimeoutId);
                var e = t._idleTimeout;
                e >= 0 && (t._idleTimeoutId = setTimeout(function() {
                    t._onTimeout && t._onTimeout()
                }, e))
            }, e.setImmediate = "function" == typeof t ? t : function(t) {
                var n = u++,
                    i = !(arguments.length < 2) && a.call(arguments, 1);
                return c[n] = !0, r(function() {
                    c[n] && (i ? t.apply(null, i) : t.call(null), e.clearImmediate(n))
                }), n
            }, e.clearImmediate = "function" == typeof i ? i : function(t) {
                delete c[t]
            }
        }).call(e, n(2).setImmediate, n(2).clearImmediate)
    }, function(t, e) {
        function n() {
            u && s && (u = !1, s.length ? c = s.concat(c) : f = -1, c.length && i())
        }

        function i() {
            if (!u) {
                var t = setTimeout(n);
                u = !0;
                for (var e = c.length; e;) {
                    for (s = c, c = []; ++f < e;) s && s[f].run();
                    f = -1, e = c.length
                }
                s = null, u = !1, clearTimeout(t)
            }
        }

        function o(t, e) {
            this.fun = t, this.array = e
        }

        function r() {}
        var s, a = t.exports = {},
            c = [],
            u = !1,
            f = -1;
        a.nextTick = function(t) {
            var e = new Array(arguments.length - 1);
            if (arguments.length > 1)
                for (var n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
            c.push(new o(t, e)), 1 !== c.length || u || setTimeout(i, 0)
        }, o.prototype.run = function() {
            this.fun.apply(null, this.array)
        }, a.title = "browser", a.browser = !0, a.env = {}, a.argv = [], a.version = "", a.versions = {}, a.on = r, a.addListener = r, a.once = r, a.off = r, a.removeListener = r, a.removeAllListeners = r, a.emit = r, a.binding = function(t) {
            throw new Error("process.binding is not supported")
        }, a.cwd = function() {
            return "/"
        }, a.chdir = function(t) {
            throw new Error("process.chdir is not supported")
        }, a.umask = function() {
            return 0
        }
    }, function(t, e, n) {
        var i = n(5),
            o = {
                oldContainer: void 0,
                newContainer: void 0,
                newContainerLoading: void 0,
                extend: function(t) {
                    return i.extend(this, t)
                },
                init: function(t, e) {

                },
                done: function() {
                    this.oldContainer.parentNode.removeChild(this.oldContainer), this.newContainer.style.visibility = "visible", this.deferred.resolve()
                },
                start: function() {}
            };
        t.exports = o
    }, function(t, e) {
        var n = {
            getCurrentUrl: function() {
                return window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search
            },
            cleanLink: function(t) {
                return t.replace(/#.*/, "")
            },
            xhrTimeout: 5e3,
            xhr: function(t) {},
            extend: function(t, e) {
                var n = Object.create(t);
                for (var i in e) e.hasOwnProperty(i) && (n[i] = e[i]);
                return n
            },
            deferred: function() {
                return new function() {
                    this.resolve = null, this.reject = null, this.promise = new Promise(function(t, e) {
                        this.resolve = t, this.reject = e
                    }.bind(this))
                }
            },
            getPort: function(t) {
                var e = void 0 !== t ? t : window.location.port,
                    n = window.location.protocol;
                return "" != e ? parseInt(e) : "http:" === n ? 80 : "https:" === n ? 443 : void 0
            }
        };
        t.exports = n
    }, function(t, e, n) {
        var i = n(7),
            o = n(5),
            r = {
                namespace: null,
                extend: function(t) {
                    return o.extend(this, t)
                },
                init: function() {
                    var t = this;
                    i.on("initStateChange", function(e, n) {
                        n && n.namespace === t.namespace && t.onLeave()
                    }), i.on("newPageReady", function(e, n, i) {
                        t.container = i, e.namespace === t.namespace && t.onEnter()
                    }), i.on("transitionCompleted", function(e, n) {
                        e.namespace === t.namespace && t.onEnterCompleted(), n && n.namespace === t.namespace && t.onLeaveCompleted()
                    })
                },
                onEnter: function() {},
                onEnterCompleted: function() {},
                onLeave: function() {},
                onLeaveCompleted: function() {}
            };
        t.exports = r
    }, function(t, e) {
        var n = {
            events: {},
            on: function(t, e) {
                this.events[t] = this.events[t] || [], this.events[t].push(e)
            },
            off: function(t, e) {
                t in this.events != 0 && this.events[t].splice(this.events[t].indexOf(e), 1)
            },
            trigger: function(t) {
                if (t in this.events != 0)
                    for (var e = 0; e < this.events[t].length; e++) this.events[t][e].apply(this, Array.prototype.slice.call(arguments, 1))
            }
        };
        t.exports = n
    }, function(t, e, n) {
        var i = n(5),
            o = {
                data: {},
                extend: function(t) {
                    return i.extend(this, t)
                },
                set: function(t, e) {
                    this.data[t] = e
                },
                get: function(t) {
                    return this.data[t]
                },
                reset: function() {
                    this.data = {}
                }
            };
        t.exports = o
    }, function(t, e) {
        var n = {
            history: [],
            add: function(t, e) {
                e || (e = void 0), this.history.push({
                    url: t,
                    namespace: e
                })
            },
            currentStatus: function() {
                return this.history[this.history.length - 1]
            },
            prevStatus: function() {
                var t = this.history;
                return t.length < 2 ? null : t[t.length - 2]
            }
        };
        t.exports = n
    }, function(t, e, n) {
        var i = n(5),
            o = n(7),
            r = n(11),
            s = n(8),
            a = n(9),
            c = {
                Dom: n(12),
                History: a,
                Cache: s,
                cacheEnabled: !0,
                transitionProgress: !1,
                ignoreClassLink: "no-barba",
                start: function() {
                    this.init()
                },
                init: function() {
                    var t = this.Dom.getContainer();
                    this.Dom.getWrapper().setAttribute("aria-live", "polite"), this.History.add(this.getCurrentUrl(), this.Dom.getNamespace(t)), o.trigger("initStateChange", this.History.currentStatus()), o.trigger("newPageReady", this.History.currentStatus(), {}, t), o.trigger("transitionCompleted", this.History.currentStatus()), this.bindEvents()
                },
                bindEvents: function() {

                },
                getCurrentUrl: function() {
                    return i.cleanLink(i.getCurrentUrl())
                },
                goTo: function(t) {
                    window.history.pushState(null, null, t), this.onStateChange()
                },
                forceGoTo: function(t) {
                    window.location = t
                },
                load: function(t) {
 
                },
                onLinkClick: function(t) {
                    for (var e = t.target; e && !e.href;) e = e.parentNode;
                    this.preventCheck(t, e) && (t.stopPropagation(), t.preventDefault(), o.trigger("linkClicked", e), this.goTo(e.href))
                },
                preventCheck: function(t, e) {
                    return !(!(window.history.pushState && e && e.href) || t.which > 1 || t.metaKey || t.ctrlKey || t.shiftKey || t.altKey || e.target && "_blank" === e.target || window.location.protocol !== e.protocol || window.location.hostname !== e.hostname || i.getPort() !== i.getPort(e.port) || e.href.indexOf("#") > -1 || i.cleanLink(e.href) == i.cleanLink(location.href) || e.classList.contains(this.ignoreClassLink))
                },
                getTransition: function() {
                    return r
                },
                onStateChange: function() {

                },
                onNewContainerLoaded: function(t) {
                    this.History.currentStatus().namespace = this.Dom.getNamespace(t), o.trigger("newPageReady", this.History.currentStatus(), this.History.prevStatus(), t)
                },
                onTransitionEnd: function() {
                    this.transitionProgress = !1, o.trigger("transitionCompleted", this.History.currentStatus(), this.History.prevStatus())
                }
            };
        t.exports = c
    }, function(t, e, n) {
        var i = n(4).extend({
            start: function() {
                this.newContainerLoading.then(this.finish.bind(this))
            },
            finish: function() {
                document.body.scrollTop = 0, this.done()
            }
        });
        t.exports = i
    }, function(t, e) {
        var n = {
            dataNamespace: "namespace",
            wrapperId: "barba-wrapper",
            containerClass: "barba-container",
            parseResponse: function(t) {
                var e = document.createElement("div");
                e.innerHTML = t;
                var n = e.querySelector("title");
                return n && (document.title = n.textContent), this.getContainer(e)
            },
            getWrapper: function() {
                var t = document.getElementById(this.wrapperId);
                if (!t) throw new Error("Barba.js: wrapper not found!");
                return t
            },
            getContainer: function(t) {
                if (t || (t = document.body), !t) throw new Error("Barba.js: DOM not ready!");
                var e = this.parseContainer(t);
                return e && e.jquery && (e = e[0]), e || window.location.reload(!0), e
            },
            getNamespace: function(t) {
                return t && t.dataset ? t.dataset[this.dataNamespace] : t ? t.getAttribute("data-" + this.dataNamespace) : null
            },
            putContainer: function(t) {
                t.style.visibility = "hidden", this.getWrapper().appendChild(t)
            },
            parseContainer: function(t) {
                return t.querySelector("." + this.containerClass)
            }
        };
        t.exports = n
    }, function(t, e, n) {
        var i = n(5),
            o = n(10),
            r = {
                ignoreClassLink: "no-barba-prefetch",
                init: function() {
                    return !!window.history.pushState && (document.body.addEventListener("mouseover", this.onLinkEnter.bind(this)), void document.body.addEventListener("touchstart", this.onLinkEnter.bind(this)))
                },
                onLinkEnter: function(t) {
                    for (var e = t.target; e && !e.href;) e = e.parentNode;
                    if (e && !e.classList.contains(this.ignoreClassLink)) {
                        var n = e.href;
                        if (o.preventCheck(t, e) && !o.Cache.get(n)) {
                            var r = i.xhr(n);
                            o.Cache.set(n, r)
                        }
                    }
                }
            };
        t.exports = r
    }])
});