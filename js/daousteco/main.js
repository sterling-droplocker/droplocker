/*
 * WebGL Water
 * http://madebyevan.com/webgl-water/
 *
 * Copyright 2011 Evan Wallace
 * Released under the MIT license
 */

function text2html(text) {
  return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g, '<br>');
}

function handleError(text) {

  var html = text2html(text);
  if (html == 'WebGL not supported') {
    html = 'Your browser does not support WebGL.<br>Please see\
    <a href="http://www.khronos.org/webgl/wiki/Getting_a_WebGL_Implementation">\
    Getting a WebGL Implementation</a>.';
  }

}

window.onerror = handleError;

var gl = GL.create();
var water;
var cubemap;
var renderer;


// Sphere physics info
var radius;
var strength;

InitMain();



function InitMain() {

    var body = document.getElementsByTagName("BODY")[0];
    // var wrapper = document.getElementById('barba-wrapper');
    body.style.overflowX = 'hidden';


    var firstTime = true;

    var ratio = window.devicePixelRatio || 1;
    var webglDiv = document.getElementById('webgl-container');
    var porte = document.getElementById('porte');
    var machine = document.getElementById('machine');
    var machineContainer = document.getElementById('washing-overlay-container');
    var washingText = document.getElementById('washing-text');

    var dropAddedCompteur = 0;
    document.getElementsByClassName("blinker-button")[0].classList.add("glow");
    document.getElementsByClassName("reload-text")[0].classList.add("green");

    var clientWidth = function () {
      return Math.max(window.innerWidth, document.documentElement.clientWidth);
    };

    var clientHeight = function () {
      return Math.max(window.innerHeight, document.documentElement.clientHeight);
    };

    function onresize() {

      // console.log(webglDiv.clientWidth);

      ratio = 1;

      var width = clientWidth() / 2.5  - 20;
      var height = clientHeight() / 2.5;
      var ratioPorte = 2 - (clientWidth()/height);

      porte.style.width = webglDiv.clientWidth + 1 + 'px';
      porte.style.height = (webglDiv.clientWidth / 2) + 1 + 'px';
      // porte.style.left = -((height * 2) - webglDiv.clientWidth)/2 + 'px';

      if(machine) {

        machine.style.width = webglDiv.clientWidth + 1 + 'px';
        // machine.style.left = -((height * 2) - webglDiv.clientWidth)/2 + 'px';

        machineContainer.style.height = machine.style.height;

        // washingText.style.width = webglDiv.clientWidth + 'px';
        washingText.style.marginLeft = (webglDiv.clientWidth / 4) + 'px';

      }



      // canvasRight = ((width * ratio) - webglDiv.clientWidth) + 'px';
      // canvasTop = '0px';

      gl.canvas.width = webglDiv.clientWidth - 2;
      gl.canvas.height = webglDiv.clientHeight - 4;
      gl.canvas.style.width = webglDiv.clientWidth - 2;
      gl.canvas.style.height = webglDiv.clientHeight - 4;

      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
      gl.matrixMode(gl.PROJECTION);
      gl.loadIdentity();
      gl.perspective(30, gl.canvas.width / gl.canvas.height, 0.01, 100);
      gl.matrixMode(gl.MODELVIEW);
      draw();

    }

    webglDiv.appendChild(gl.canvas);

    gl.clearColor(0, 0, 0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT);

    water = new Water();
    renderer = new Renderer();
    cubemap = new Cubemap({
      xneg: document.getElementById('xneg'),
      xpos: document.getElementById('xpos'),
      yneg: document.getElementById('ypos'),
      ypos: document.getElementById('ypos'),
      zneg: document.getElementById('zneg'),
      zpos: document.getElementById('zpos')
    });

    if (!water.textureA.canDrawTo() || !water.textureB.canDrawTo()) {
      throw new Error('Rendering to floating-point textures is required but not supported');
    }

    radius = 0.10;
    strength = 0.02;

    for (var i = 0; i < 40; i++) {
      water.addDrop(Math.random() * 2 - 1, Math.random() * 2 - 1, radius, (i & 1) ? 0.01 : -0.01);
      onresize();
    }



    var requestAnimationFrame =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      function(callback) { setTimeout(callback, 0); };

    var prevTime = new Date().getTime();

    function animate() {
      var nextTime = new Date().getTime();
      update((nextTime - prevTime) / 1000);
      draw();
      prevTime = nextTime;
      requestAnimationFrame(animate);
    }

    requestAnimationFrame(animate);


    window.addEventListener('resize', function(event){
      onresize();
    });


    function startDrag(x, y) {

      var tracer = new GL.Raytracer();
      var ray = tracer.getRayForPixel(x * ratio, y * ratio);
      var pointOnPlane = tracer.eye.add(ray.multiply(-tracer.eye.y / ray.y));
      // Action de la souris
      // On change Z par Y

      // On limite la montée de l'eau

      if(pointOnPlane.z < 4.8) {
        water.addDrop(pointOnPlane.x, pointOnPlane.y, radius, strength);
      }

    }

    function reduceWaterLevel(previousCount,howMuch) {


      dropAddedCompteur = 0;
      document.getElementsByClassName("blinker-button")[0].classList.remove("glow");
      document.getElementsByClassName("reload-text")[0].classList.remove("green");


      if(previousCount) {
        var count = previousCount;
      } else {
        var count = 0;
      }

      let p1 = new Promise(

         (resolve, reject) => {


              window.setTimeout(
                  function() {
                      for (var i = 0; i < 18; i++) {
                        water.removeDrop(Math.random() * 2 - 1, Math.random() * 2 - 1, radius, -0.02);
                        count++;
                      }
                      resolve(count);
                  }, 1);
          }

      );

      p1.then(
          function(val) {
              if(val < howMuch) {
                reduceWaterLevel(val,howMuch);
              } else if(firstTime) {
                firstTime = false;
                dropAddedCompteur = 6;
                document.getElementsByClassName("blinker-button")[0].classList.add("glow");
                document.getElementsByClassName("reload-text")[0].classList.add("green");
              }
              //console.log('Handle  ('+val+') here.');

          })
      .catch(
         (reason) => {
              console.log('Handle rejected promise ('+reason+') here.');
          });


    }

    function upWaterLevel(previousCount,x,y) {

      if(dropAddedCompteur >= 6) {
          document.getElementsByClassName("blinker-button")[0].classList.add("glow");
          document.getElementsByClassName("reload-text")[0].classList.add("green");
          return true;
      }


      if(previousCount) {
        var count = previousCount;
      } else {
        dropAddedCompteur++
        var count = 0;
      }

      let p1 = new Promise(

         (resolve, reject) => {


              window.setTimeout(
                  function() {
                      for (var i = 0; i < 18; i++) {
                        water.addDrop(Math.random() * 2 - 1, Math.random() * 2 - 1, radius, 0.02);
                        count++;
                      }
                      resolve(count);
                  }, 1);
          }

      );

      p1.then(
          function(val) {
              if(val < 400) {
                upWaterLevel(val);
              }
              //console.log('Handle  ('+val+') here.');

          })
      .catch(
         (reason) => {
              console.log('Handle rejected promise ('+reason+') here.');
          });



    }

    // Handle mouvement desktop + mobile
    document.onmousemove = function(e) {
        e.preventDefault();
        startDrag(e.pageX, e.pageY);
    };

    document.ontouchmove = function(e) {
      if (e.touches.length === 1) {
        e.preventDefault();
        startDrag(e.touches[0].pageX, e.touches[0].pageY);
      }
    };

    webglDiv.onclick = function(e) {

      var tracer = new GL.Raytracer();
      var ray = tracer.getRayForPixel(e.pageX * ratio, e.pageY * ratio);
      var pointOnPlane = tracer.eye.add(ray.multiply(-tracer.eye.y / ray.y));

      // if(pointOnPlane.z > 3.1) {
      //   reduceWaterLevel(0,e.pageX, e.pageY);
      // }

      upWaterLevel(0,e.pageX, e.pageY);

    };

    washingText.onclick = function(e) {

      if(dropAddedCompteur >= 6) {
          reduceWaterLevel(0,2400);
      }

    };




    function update(seconds) {

      if (seconds > 1) return;

      // Update the water simulation and graphics
      water.stepSimulation();
      water.stepSimulation();
      water.updateNormals();
      renderer.updateCaustics(water);

    }

    var z = 1.5;
    var rotateAngle = 10;
    var revert = true;

    function draw() {

      if (rotateAngle >= -40.5 && revert) {

        rotateAngle -= 1;
        z -= 0.02;

      } else if( rotateAngle <= 40.5 && ( revert === false ) ) {

        rotateAngle += 1;
        z += 0.02;

      } else if( rotateAngle < -40.5 ) {

        revert = false

      } else if(rotateAngle > 40.5) {

        revert = true

      }

      // gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Machine location in space
      gl.loadIdentity();
      gl.translate(0, 0.15, -3.9);
      // gl.rotate(0, -1.5, 0, 0);
      gl.rotate(rotateAngle, 0, -0.5, 1.5);

      gl.translate(0, 0.5, 1.5);

      gl.enable(gl.DEPTH_TEST);

      renderer.renderCube();
      renderer.renderWater(water, cubemap);

      gl.disable(gl.DEPTH_TEST);

      // rotateCube();

    }



    function rotateCube() {
      // var SPEED = 0.01;
      //
      // console.log(gl);
      //   gl.rotate.x -= SPEED * 2;
      //   gl.rotate.y -= SPEED;
      //   gl.rotate.z -= SPEED * 3;
    }


      reduceWaterLevel(0,2400);


  };
