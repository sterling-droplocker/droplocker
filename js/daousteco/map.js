////////////////////////
//// Init Namespace ////
////////////////////////

var GMAP = GMAP || {};

GMAP.attributes = {

  activeMarkerCluster : false,
  geo_position : '',
  watchId: '',
  map: '',
  places: '',
  gmarkers: [],
  infoWindow: '',
  markers : [],
  markersSearch : [],
  markersSelf : [],
  automaticLatitude : 45.7472221,
  automaticLongitude : -72.9286378,
  geolocated : false,
  firstLocated : 0,
  alignLeft : 150,
  alignTop : 0,
  r : 3963.0,
  decimalIntoRadian : 57.2958,
  countryRestrict : {'country': 'ca'},
  customIcons : {
    Default: {
      icon: {
        url: 'data:image/svg+xml;utf-8, \
                <svg  width="22" height="27" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Calque_1" x="0px" y="0px" viewBox="0 0 11 14" style="enable-background:new 0 0 11 14;" xml:space="preserve"> \
                  <style type="text/css"> \
                  	.st0{fill:url(#SVGID_1_);} \
                  </style> \
                  <g> \
                  	<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="1.7111" y1="8.9709" x2="10.6411" y2="3.9576"> \
                  		<stop offset="0" style="stop-color:#008883"/> \
                  		<stop offset="1" style="stop-color:#6AB550"/> \
                  	</linearGradient> \
                  	<path class="st0" d="M5.7,0.3c-3,0-5.3,2.4-5.3,5.3c0,1.2,0.3,2.2,1,3.1c1,1.4,4.4,5.3,4.4,5.3S9,10.1,10,8.7c0.7-0.9,1-1.9,1-3.1   C11,2.7,8.6,0.3,5.7,0.3z M5.7,7.9c-1.3,0-2.3-1-2.3-2.3c0-1.3,1-2.3,2.3-2.3S8,4.4,8,5.6C8,6.9,6.9,7.9,5.7,7.9z"/> \
                  </g> \
                  </svg>',
          size: new google.maps.Size(22, 27),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(0, 27)
      }
    }
  },
  mapStyling : [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f7f7f7"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#deecdb"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":"25"}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":"25"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"saturation":"-90"},{"lightness":"25"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.station","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#e0f1f9"}]}],
};

/*
  Handle IE / Edge
*/

if (document.documentMode || /Edge/.test(navigator.userAgent)) {
  GMAP.attributes.customIcons.Default.icon.url = '/wp-content/themes/daoust/assets/map/img/marker.png';
}



GMAP.methods = {

    initGmap: function() {

      var self = this;

      map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(GMAP.attributes.automaticLatitude,GMAP.attributes.automaticLongitude),
        zoom: 9,
        gestureHandling: 'cooperative',
        zoomControl: false,
        scaleControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        styles: GMAP.attributes.mapStyling
      });

      GMAP.attributes.map = map;

      GMAP.attributes.infoWindow = new google.maps.InfoWindow({
        content: document.getElementById('info-content')
      });

      GMAP.attributes.map.panBy(-GMAP.attributes.alignLeft,GMAP.attributes.alignTop); /* centre en fonction du panneau de résultat */

      // Init de la geolocalisation du navigateur
      this.initGeoloc();

      // Resize de la map
      jQuery('.map-content').height(jQuery(window).height() - jQuery('sticky-search').height() - 100);

      self.initMarkers();
      self.initAutocomplete('search-form-input');
      self.initAutocomplete('search-form-input-mobile','mobile');
      /*
      Mobile'one
      */
      // self.initAutocomplete('search-form-input2');
      /*
      Mobile'one
      */

    },

    url_GET: function(param) {
    	var vars = {};
    	window.location.href.replace( location.hash, '' ).replace(
    		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
    		function( m, key, value ) { // callback
    			vars[key] = value !== undefined ? value : '';
    		}
    	);

    	if ( param ) {
    		return vars[param] ? vars[param] : null;
    	}
    	return vars;
    },

    initAutocomplete: function(input_id, type) {

        if(type == 'mobile') {

          autocomplete_mobile = new google.maps.places.Autocomplete((document.getElementById(input_id)), { componentRestrictions: GMAP.attributes.countryRestrict });
          places_mobile = new google.maps.places.PlacesService(GMAP.attributes.map);

          autocomplete_mobile.addListener('place_changed', this.onPlaceChangedMobile);

        } else {

          autocomplete = new google.maps.places.Autocomplete((document.getElementById(input_id)), { componentRestrictions: GMAP.attributes.countryRestrict });
          places = new google.maps.places.PlacesService(GMAP.attributes.map);

          autocomplete.addListener('place_changed', this.onPlaceChanged);

        }

    },

    onPlaceChangedMobile: function(type) {

        var place_mobile = autocomplete_mobile.getPlace();

        if (place_mobile.geometry) {

            map.panTo(place_mobile.geometry.location);
            map.setZoom(12);

        } else if (place_mobile) {

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ "address": firstResult }, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);

                    map.panTo(new google.maps.LatLng(lat, lng));
                    map.setZoom(12);

                    jQuery("#autocomplete").val(results[0].formatted_address);

                }
            });

        }

    },

    onPlaceChanged: function(type) {

        var place = autocomplete.getPlace();

        if (place.geometry) {

            map.panTo(place.geometry.location);
            map.setZoom(12);

        } else if (place) {

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ "address": firstResult }, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);

                    map.panTo(new google.maps.LatLng(lat, lng));
                    map.setZoom(12);

                    jQuery("#autocomplete").val(results[0].formatted_address);

                }
            });

        }

    },

    generateItineraryLink: function(finalPos) {
      return 'https://maps.google.com?saddr=Current+Location&daddr='+finalPos;
    },

    initMarkers: function() {

      var self = this;

      jQuery.ajax({
        url: "/wp-admin/admin-ajax.php",
        type : 'POST',
        data: ({action : 'getMarkers'}),
        dataType: "xml",
        beforeSend: function() {
        },
        success : function(result) {
          jQuery('.loading').removeClass('is--active');

          var xml = result;

          var markers = xml.documentElement.getElementsByTagName("marker");

          for (var i = 0; i < markers.length; i++) {

            var markerNum = markers[i].getAttribute("marker");
            var name = markers[i].getAttribute("name");
            var address = markers[i].getAttribute("address");
            var city = markers[i].getAttribute("city");
            var phone = markers[i].getAttribute("phone");
            var horaires = markers[i].getAttribute("horaires");
            var point = new google.maps.LatLng(
                parseFloat(markers[i].getAttribute("lat")),
                parseFloat(markers[i].getAttribute("lng")));

            if(GMAP.attributes.geo_position) {

                var htmlDistance = "<p>Distance : "+ self.getDistance(GMAP.attributes.geo_position, markers[i]) +" km</p>"

            } else {
                var htmlDistance = "";
            }

            if(markers[i].getAttribute("phone")) {
              var phone_final = "<a href='tel:"+phone+"' class='phone'>"+ phone +"</a>";
            } else {
              var phone_final = '';
            }

            if(markers[i].getAttribute("horaires")) {
              //on forme un array avec tous les horaires reçu
              var horaireArray = horaires.split(',');
              var horaires_final_li = '';
              var split;

              for (var j in horaireArray ) {
                var loop = 0;
                //on split au niveau de l'espace entre le jour et les horaires dans chaque jour
                split = horaireArray[j].split(' ');

                for (var x in split ) {
                  if (loop === 0) {
                    //on renvoie deux spans
                    horaires_final_li += "<li><span>"+split[0]+"</span><span>"+split[1]+"</span></li>";
                    loop++;
                  }
                }
              }
              var horaires_final_ul = '<ul>'+horaires_final_li+'</ul>';

            } else {
              var horaires_final_ul = '';
            }

            var depart = new google.maps.LatLng(GMAP.attributes.map.center.lat(),GMAP.attributes.map.center.lng());
            var arrivee = new google.maps.LatLng(markers[i].getAttribute("lat"),markers[i].getAttribute("lng"));
            var distance_km_affichage = google.maps.geometry.spherical.computeDistanceBetween(depart,arrivee);

            var initeraryLink = GMAP.methods.generateItineraryLink(address);

            var html = jQuery(["<marker>",
                              "  <div class='loc-fulladdress'><div class='titre_infobulle'>"+ city +"</div>",
                              "    <div class='data_infobulle'>",
                              "       <div class='address'>",
                              "         <p>"+ address +"</p>",
                              "       </div>",
                              "       <div class='infos'>",
                              "         "+phone_final+"",
                              "       </div>",
                              "       <div class='horaires'>",
                              "         <div class='horaires--list'>",
                              "           "+horaires_final_ul+"",
                              "         </div>",
                              "       </div>",
                              "     </div>",
                              "     <div class='permalink'>",
                              "         <a class='url' target='_blank' href='"+initeraryLink+"'>",
                              "           Directions",
                              "         </a>",
                              "    </div>",
                              "  </div>",
                              "</marker>"].join("\n"));



                  var icon = GMAP.attributes.customIcons['Default'] || {};

                  var marker = new google.maps.Marker({
                    map: GMAP.attributes.map,
                    position: point,
                    icon: icon.icon,
                    name: name,
                    address: address,
                    city: city,
                    phone: phone,
                    center: point,
                    lat:   parseFloat(markers[i].getAttribute("lat")),
                    lng:  parseFloat(markers[i].getAttribute("lng")),
                  });

                  GMAP.attributes.gmarkers.push(marker);
                  self.bindInfoWindow(marker, map, GMAP.attributes.infoWindow, html[0].innerHTML);

          }

          jQuery('body').on('click','.location--infos',function(e) {
              e.preventDefault();
              google.maps.event.trigger(GMAP.attributes.gmarkers[jQuery(this).attr('data-index')], 'click');
          });

        }

      });

    },


    initGeoloc: function() {

        if (navigator.geolocation) {
          var geoSuccess = function(position) {
           startPos = position;
           document.getElementById('startLat').innerHTML = startPos.coords.latitude;
           document.getElementById('startLon').innerHTML = startPos.coords.longitude;
         };
           GMAP.attributes.watchId = navigator.geolocation.watchPosition(this.successCallback,  null, {enableHighAccuracy:true});
           jQuery('.geome').click(function(){
               GMAP.attributes.watchId = navigator.geolocation.watchPosition(this.successCallback,  null, {enableHighAccuracy:true});
           });

           /*
             Si on a parametre dans l'url alors , sinon
           */
           if(this.url_GET('SID')) {
             var decodedUrl = decodeURIComponent(this.url_GET('SID')).replace(/[()]/g, '');
             var splitedUrl = decodedUrl.split(',');
             var newlat = splitedUrl[0];
             var newlng = splitedUrl[1];

             GMAP.attributes.map.panTo(new google.maps.LatLng(newlat, newlng));

           }

        } else {
          alert("Votre navigateur ne prend pas en compte la géolocalisation");
        }
    },


    centerGeolocMobile: function() {
      if (navigator.geolocation) {
        var geoSuccess = function(position) {
          startPos = position;
          document.getElementById('startLat').innerHTML = startPos.coords.latitude;
          document.getElementById('startLon').innerHTML = startPos.coords.longitude;

          var newlat = startPos.coords.latitude;
          var newlng = startPos.coords.longitude;

          GMAP.attributes.map.panTo(new google.maps.LatLng(newlat, newlng));
        };


      } else {
        alert("Votre navigateur ne prend pas en compte la géolocalisation");
      }
    },


    GetLocationByIp: function() {

      var self = this;

      jQuery.get("https://ipinfo.io", function (response) {
         if(response.loc) {
           // GMAP.attributes.automaticLatitude = response.loc.split(',')[0];
           // GMAP.attributes.automaticLongitude = response.loc.split(',')[1];
         }
         self.initGmap();
      }, "jsonp");

    },

    successCallback: function(position){

      if(!GMAP.attributes.geolocated){

         GMAP.attributes.map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
         GMAP.attributes.map.setZoom(14);
         GMAP.methods.deleteMarkersSelf();
         var markerSelfIcon = new google.maps.Marker({
           position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
           map: GMAP.attributes.map,
           center: position,
           icon: 'http://johnnybrown.ca/wp-content/themes/bridge/map/img/self.png'
         });
         GMAP.attributes.markersSelf.push(markerSelfIcon);
         var html = "<div class='loc-fulladdress'><div class='titre_infobulle'>Votre position estimée</div>";
         GMAP.methods.bindInfoWindow(markerSelfIcon, GMAP.attributes.map, GMAP.attributes.infoWindow, html);
         GMAP.attributes.geo_position = position.coords.latitude+","+position.coords.longitude;
         GMAP.methods.initMarkers();
         GMAP.attributes.geolocated = true;
         navigator.geolocation.clearWatch(GMAP.attributes.watchId);

      } else {

         GMAP.attributes.map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
         GMAP.attributes.geolocated = false;

      }

    },

    RemoveAccents: function(str) {
      var accents    = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž-';
      var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz ";
      str = str.split('');
      var strLen = str.length;
      var i, x;
      for (i = 0; i < strLen; i++) {
        if ((x = accents.indexOf(str[i])) != -1) {
          str[i] = accentsOut[x];
        }
      }
      return str.join('');
    },


    clearMarkersSearch:function() {
      this.setMapOnAllSearch(null);
    },

    clearMarkersSelf: function() {
      this.setMapOnSelf(null);
    },

    setMapOnAllSearch: function(map) {
      for (var i = 0; i < GMAP.attributes.markersSearch.length; i++) {
        GMAP.attributes.markersSearch[i].setMap(map);
      }
    },

    deleteMarkersSearch: function() {
      this.clearMarkersSearch();
      GMAP.attributes.markersSearch = [];
    },

    setMapOnSelf: function(map) {
      for (var i = 0; i < GMAP.attributes.markersSelf.length; i++) {
        GMAP.attributes.markersSelf[i].setMap(map);
      }
    },

    deleteMarkersSelf: function() {
      this.clearMarkersSelf();
      markersSelf = [];
    },

    downloadUrl: function(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = this.doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    },

    doNothing: function() {},

    bindInfoWindow: function(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        if(window.innerWidth > 885 ) {
          GMAP.attributes.map.setCenter(marker.getPosition());
        } else {
          GMAP.attributes.map.setCenter(marker.getPosition());
          map.panBy(0, -150);
        }
        GMAP.attributes.infoWindow.setContent(html);
        GMAP.attributes.infoWindow.open(map, marker);
        navigator.geolocation.clearWatch(GMAP.attributes.watchId);
      });

    },


    currentBounds: function(bounds) {

      this.center = bounds.getCenter();
      this.ne = bounds.getNorthEast();

      this.firstLatitude = this.center.lat() / GMAP.attributes.decimalIntoRadian
      this.secondLatitude = this.ne.lat() / GMAP.attributes.decimalIntoRadian
      this.firstLongitude = this.center.lng() / GMAP.attributes.decimalIntoRadian
      this.secondLongitude = this.ne.lng() / GMAP.attributes.decimalIntoRadian

      // distance = Radian par rapport au centre de la terre
      this.distance = (GMAP.attributes.r * Math.acos(Math.sin(this.firstLatitude) * Math.sin(this.secondLatitude)
                        + Math.cos(this.firstLatitude) * Math.cos(this.secondLatitude)
                        * Math.cos(this.secondLongitude - this.firstLongitude)))*1600;

    },

    getDistance: function(position, marker) {

      this.self_lat = position.split(',')[0];
      this.self_lng = position.split(',')[1];

      this.depart = new google.maps.LatLng(this.self_lat,this.self_lng);
      this.arrivee = new google.maps.LatLng(marker.getAttribute("lat"),marker.getAttribute("lng"));
      this.distance = google.maps.geometry.spherical.computeDistanceBetween(this.depart, this.arrivee);

      return (Math.round(this.distance)/1000).toFixed(0);

    },


};


String.prototype.like = function(search) {
  if (typeof search !== 'string' || this === null) {return false; }
  search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
  search = search.replace(/%/g, '.*').replace(/_/g, '.');
  return RegExp('^' + search + '$', 'gi').test(this);
}


////////////////////////
//// Init de la MAP ////
////////////////////////

GMAP.methods.GetLocationByIp();
