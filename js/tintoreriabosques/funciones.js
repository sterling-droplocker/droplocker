$(document).ready(function() {

    var productos_btn = $("#productos");
    var productos_menu = $("#productos_index");

    var descargas_btn = $("#descargas");
    var descargas_menu = $("#descargas_index");

    productos_btn.click(function() {

        if (productos_menu.is(":hidden")) {
            productos_menu.show("slide");
        }
        else {
            productos_menu.hide("slide");
        }
    });

    
    descargas_btn.click(function() {

        if (descargas_menu.is(":hidden")) {
            descargas_menu.show("slide");
        }
        else {
            descargas_menu.hide("slide");
        }
    });

});



function ValidaFormularios(controles)
{
	for ( i = 0, n = controles.length; i < n; i++ )
 	{
	var iControl = controles[i];
  	var e = document.getElementById(iControl[0]);
  		if (e)
  		{
   			if ( e.value.length == 0 )
   			{
    			alert("El campo " + iControl[1] + " estÃ¡ vacÃ­o.");
    			e.focus();
    			return 0;
   			}
  		}
 	}
        return 1;
}

function ValidaMails(mails)
{
	var filtro = /^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;
	
	for ( i = 0, n = mails.length; i < n; i++ )
 	{
		var iMail = mails[i];
  		var e = document.getElementById(iMail[0]);
		if (e)
  		{
   			if ( e.value.length > 0 )
   			{
				var s = e.value;
				if (!filtro.test(s))
				{
					alert("El campo " + iMail[1] + " no es vÃ¡lido.");
    				e.focus();
    				return 0;
				}
			}			
   		}
  	}
        return 1;
}

function ValidaURL(url) {
var re=/^(http:|https:|ftp:)\/\/\w+(\.\w+)*(\-\w+)?\.\w{2,3}(\:\d{2,6})?(\/{1,2}(\:|\-|\w|\.|\?|\/|\=|\&|\%|\@|\\|\,)*)?$/;
return re.test(url);
}

function Numeros(e)
{
	var key = (document.all) ? e.keyCode : e.which;
	if (key < 48 || key > 57)
	{
		return false;
	}
	else
	{
		return true;	
	}
}

function maximaLongitud(campo, maxlong, elemento)
{
    var out_value;
    var total;
    var texto = document.getElementById(campo).value;

    if (texto.length > maxlong)
    {
		in_value = texto;

		out_value = in_value.substring(0,maxlong);

		texto = out_value;



                document.getElementById(elemento).innerHTML = '0';

                document.getElementById(campo).value = texto;

		return false;
    }

    total = maxlong - texto.length;

    document.getElementById(elemento).innerHTML = total;


    var value= (texto.length * 100 / maxlong);



    if(total < 20)
    {
        document.getElementById(elemento).className='contador_min';


    }
    else
    {
        document.getElementById(elemento).className='contador';


    }

    return true;
}


function PaginacionSubmit(i,por,order)
{
	document.paginar.page.value=i;
	document.paginar.por.value=por;
	document.paginar.order.value=order;
	document.paginar.submit();
	
}

function SubmitID(id, command)
{
	var myForm = document.getElementById('formid');	
	myForm.command.value = command;
	myForm.id.value = id;
	myForm.submit();
}


function AgregarFavoritos(url,titulo)
{
	
	if ((navigator.appName=="Microsoft Internet Explorer") && (parseInt(navigator.appVersion)>=4)) 
	{
		window.external.AddFavorite(url,titulo);
	}
	else 
	{
		if(navigator.appName == "Netscape")
		window.sidebar.addPanel(titulo, url,"");
	}
}

function popUp(URL) 
{
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=660,height=420,left = 262,top = 259');");
}





function Encriptar(texto,semilla)
{
    var suma = 0;
    var newtexto = "";
        
    for(x=0; x < semilla.length; x++)
    {
        suma += semilla.charCodeAt(x);
    }

    semilla = suma.toString();
    suma = 0;


    for(z=0; z < semilla.length; z++)
    {
        suma += parseInt(semilla.charAt(z));
    }
    
    semilla = parseInt(suma);



    for(y=0; y < texto.length; y++)
    {
        if(texto.charCodeAt(y) + semilla > 126)
        {
            suma = ((texto.charCodeAt(y) + semilla) - 126) + 32;
        }
        else
        {
            suma = (texto.charCodeAt(y) + semilla);
        }

        newtexto += String.fromCharCode(suma)
      
    }

    return newtexto;
}

