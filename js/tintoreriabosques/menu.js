$(document).ready(function() {
    $(".btn_nav").hover(
            function() {
                $(this).children("a").addClass("active");
                $(this).children("ul").css("display", "block").animate({"opacity": 1}, 10, function() {
                    $(this).children("ul li").animate({"opacity": 1});
                });
            },
            function() {
                $(this).children("a").removeClass("active");
                $(".btn_nav > ul li").css({"opacity": 0});
                $(".btn_nav > ul").css({"opacity": 0, "display": "none"});
            });

    $(".cont_nav").click(
            function() {
                $(".principal_nav").slideToggle("slow", function()
                {
                    $(".cont_nav").toggleClass("active")
                    $(".principal_nav").toggleClass("despliegue")
                    if (!$(".principal_nav").is(":visible")) {
                        $(".principal_nav").css('display', '')
                    }
                })
            });
});