$(document).ready(function()
    {
        $(".required").blur(function(){
            var largo = ($(this).attr('minlength'));
            var largo_real = $(this).val().length;
  
      
            if((largo > largo_real && largo != "") || largo_real == 0)
            {
                $(this).addClass("error");
                $(this).next("span").next("span").addClass("bad").css('display','inline').removeClass("welldone");
               
            }
            else
            {
                $(this).removeClass("error");
                $(this).next("span").next("span").addClass("welldone").css('display','inline').removeClass("bad");
            }
       
        });


        $(".email").blur(function(){

            if($(this).val().length > 0)
            {
                if( /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test($(this).val()))
                {
                    $(this).removeClass("error");
                    $(this).next("span").next("span").addClass("welldone").css('display','inline').removeClass("bad");

                }
                else
                {
                    $(this).addClass("error");
                    $(this).next("span").next("span").addClass("bad").css('display','inline').removeClass("welldone");
                }
            }
        });


        $(".required").focus(function(){
            $(this).next("span").next("span").css('display','none');
        });
        $(".email").focus(function(){
            $(this).next("span").next("span").css('display','none');
        });


        jQuery.validator.setDefaults({
            debug: false
        });


        jQuery.validator.addMethod("url_ip", function()
        {
            var obj=document.getElementById('portscan');
            var checkStr = obj.host.value;
            var url=/^(http:|https:|ftp:)\/\/\w+(\.\w+)*(\-\w+)?\.\w{2,3}(\:\d{2,6})?(\/{1,2}(\:|\-|\w|\.|\?|\/|\=|\&|\%|\@|\\|\,)*)?$/;

            if(url.test(checkStr))
            {
                return true;
            }
            else
            {
                partes=checkStr.split('.');
                if (partes.length!=4)
                {
                    return false;
                }
                for (i=0;i<4;i++)
                {
                    num=partes[i];

                    if (num>255 || num<0 || num.length==0 || isNaN(num))
                    {

                        return false;

                    }

                }

                return true;

            }


        }, jQuery.format("Ingresa una URL o direccion IP v&aacute;lida"));
						   

        $("#frmContacto").validate({
		invalidHandler: function(e, validator) {
                        
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = errors == 1
					? 'Te faltÃ³ llenar 1 campo. EstÃ¡ marcado en rojo.'
					: 'Te faltÃ³ llenar ' + errors + ' campos.  EstÃ¡n marcados en rojo.';
				$("div.error_contacto").html(message);
				$("div.error_contacto").show();
			} else {
				$("div.error_contacto").hide();
			}
		},
		onkeyup: false,
		submitHandler: function() {
			$("div.error_contacto").hide();
			document.frmContacto.submit();
		},
		messages: {
			nombre: {
				required: "Escribe tu nombre completo"
			},
			mail: {
				required: "Escribe una direccion de e-mail",
				email: "El formato del e-mail no es vÃ¡lido"
			},
			asunto: {
				required: "Escribe el asunto de tu comentario"
			},
			comentarios: {
				required: "Escribe tu comentario",
                                minlength: "Escribe alemenos 5 caracteres"
			},
                        telefono: {
				required: "Escribe tu numero de telÃ©fono"
			}                        
		},
		debug:true
	});
/*seccion para validar formulario de AtenciÃ³n a Clientes*/
        $("#frmAtnclientes").validate({
		invalidHandler: function(e, validator) {
                        
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = errors == 1
					? 'Te faltÃ³ llenar 1 campo. EstÃ¡ marcado en rojo.'
					: 'Te faltÃ³ llenar ' + errors + ' campos.  EstÃ¡n marcados en rojo.';
				$("div.error_atencion").html(message);
				$("div.error_atencion").show();
			} else {
				$("div.error_atencion").hide();
			}
		},
		onkeyup: false,
		submitHandler: function() {
			$("div.error_atencion").hide();
			document.frmAtnclientes.submit();
		},
		messages: {
			nombre: {
				required: "Escribe tu nombre completo"
			},
			mail: {
				required: "Escribe una direccion de e-mail",
				email: "El formato del e-mail no es vÃ¡lido"
			},
			sucursal: {
				required: "Selecciona el nombre de la sucursal"
			},
                        motivo: {
				required: "Selecciona un motivo"
			},
			comentarios: {
				required: "Escribe tu comentario",
                                minlength: "Escribe alemenos 5 caracteres"
			},
                        telefono: {
				required: "Escribe tu numero de telÃ©fono"
			}
		},
		debug:true
	});
/**/
        $("#empleoForm").validate({
		invalidHandler: function(e, validator) {
                        
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = errors == 1
					? 'Te faltÃ³ llenar 1 campo. EstÃ¡ marcado en rojo.'
					: 'Te faltÃ³ llenar ' + errors + ' campos.  EstÃ¡n marcados en rojo.';
				$("div.error_empleo").html(message);
				$("div.error_empleo").show();
			} else {
				$("div.error_empleo").hide();
			}
		},
		onkeyup: false,
		submitHandler: function() {
			$("div.error_empleo").hide();
			document.empleoForm.submit();
		},
		messages: {
			nombre: {
				required: "Escribe tu nombre completo"
			},
			email: {
				
				email: "El formato del e-mail no es vÃ¡lido"
			},
			telefono: {
				
				required: "Escribe tu numero de telÃ©fono"
			},
			comentarios: {
				required: "Escribe tu comentario",
                                minlength: "Escribe alemenos 5 caracteres"
			}
		},
		debug:true
	});
        
        
        
          $("#frmServicioDomicilio").validate({
		invalidHandler: function(e, validator) {
                        
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = errors == 1
					? 'Te faltÃ³ llenar 1 campo. EstÃ¡ marcado en rojo.'
					: 'Te faltÃ³ llenar ' + errors + ' campos.  EstÃ¡n marcados en rojo.';
				$("div.error_contacto_domicilio").html(message);
				$("div.error_contacto_domicilio").show();
			} else {
				$("div.error_contacto_domicilio").hide();
			}
		},
		onkeyup: false,
		submitHandler: function() {
			$("div.error_contacto").hide();
			document.frmServicioDomicilio.submit();
		},
		messages: {
			nombre: {
				required: "Escribe tu nombre completo"
			},
			calle: {
				required: "Escribe el nombre de tu calle"
				
			},
			numero: {
				required: "Escribe el numero de tu domicilio"
			},
                        colonia: {
				required: "Escribe el nombre de tu colonia"
			},
                        cp: {
				required: "Escribe tu cÃ³digo postal"
			},
			servirle: {
				required: "Escribe como podemos servirte"
			},
                        mail: {
				required: "Escribe una direccion de e-mail",
				email: "El formato del e-mail no es vÃ¡lido"
			},
			telefono: {
				
				required: "Escribe tu numero de telÃ©fono"
			}
		},
		debug:true
	});
        
           $("#frmServicioEmpresa").validate({
		invalidHandler: function(e, validator) {
                        
			var errors = validator.numberOfInvalids();
			if (errors) {
				var message = errors == 1
					? 'Te faltÃ³ llenar 1 campo. EstÃ¡ marcado en rojo.'
					: 'Te faltÃ³ llenar ' + errors + ' campos.  EstÃ¡n marcados en rojo.';
				$("div.error_contacto_empresas").html(message);
				$("div.error_contacto_empresas").show();
			} else {
				$("div.error_contacto_empresas").hide();
			}
		},
		onkeyup: false,
		submitHandler: function() {
			$("div.error_contacto").hide();
			document.frmServicioEmpresa.submit();
		},
		messages: {
			nombre: {
				required: "Escribe tu nombre completo"
			},
                        empresa: {
				required: "Escribe el nombre de tu empresa"
			},
                       mail: {
				required: "Escribe una direccion de e-mail",
				email: "El formato del e-mail no es vÃ¡lido"
			},
			telefono: {
				
				required: "Escribe tu numero de telÃ©fono"
			},
			calle: {
				required: "Escribe el nombre de tu calle"
				
			},
			numero: {
				required: "Escribe el numero de tu domicilio"
			},
                        colonia: {
				required: "Escribe el nombre de tu colonia"
			},
                        cp: {
				required: "Escribe tu cÃ³digo postal"
			},
			servirle: {
				required: "Escribe como podemos servirte"
			}
		},
		debug:true
	});


$("div.buttonSubmit").hover(function(){

    $(this).addClass("buttonSubmitHover");
},
function(){

    $(this).removeClass("buttonSubmitHover");
});
	
 });



