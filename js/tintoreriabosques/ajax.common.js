function registroSubmit()
{
    var email = document.frmemail.registro_email.value;

    if(email != "")
    {
        if((/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(email)))
        {
            var query = 'function=registroSubmit&vars_ajax[]='+email;
            pasarelaAjax('GET',query,"registroSubmitHTML",'');
        }
        else
        {
            $(".msg_email").html("<div class='mal'><div><a href='javascript:void(0);' onclick=\"$('.msg_email').css('display','none');\" style=\"float:left; width:20px; height:20px; margin:0 0 0 5px; background:transparent url('/images/tintoreriabosques/footer/error-boletin.png') no-repeat 0 0;\"></a></div><span class='msgErr'>El Email no es válido</span></div>").css('display','block');
        }
    }
}

function registroSubmitHTML(response)
{
    var respuesta = null;

    if(response != "null")
    {

        respuesta = eval("(" + response + ")");

        if(respuesta[0] && respuesta[0]["message"])
        {

            $(".msg_email").html("<div class='bien'><div><a href='javascript:void(0);' onclick=\"$('.msg_email').css('display','none');\" style=\"float:left; width:20px; height:20px; margin:0 0 0 5px; background:transparent url('images/tintoreriabosques/footer/acierto-boletin.png') no-repeat 0 0;\"></a></div>"+respuesta[0]["message"]+"</div>").css('display','block');
            document.frmemail.registro_email.value='Registre su e-mail a nuestro boletín';
        }
    }


    return true;
}


function getSucursalEmpleo(id,sucursal)
{
     var query = 'function=getSucursalEmpleo&vars_ajax[]='+id;
     pasarelaAjax('GET',query,"getSucursalEmpleoHTML","'"+sucursal+"'");
}

function getSucursalEmpleoHTML(response,sucursal)
{
    var respuesta = null;
    var html = "";
    
    html = "<select name='sucursal' id='sucursal' class='textbox'>";
    html += "<option value=''>Indistinto</option>";
    if(response != "null")
    {

        respuesta = eval("(" + response + ")");

        if(respuesta && respuesta["sucursal"])
        {
            
            for(var i = 0; i < respuesta["sucursal"]["id"].length; i++)
            {
                html += "<option value='"+respuesta["sucursal"]["id"][i]+"' "+(sucursal == respuesta["sucursal"]["id"][i] ? "selected='selected'" : "")+">"+respuesta["sucursal"]["nombre"][i]+"</option>";
            }
        }
    }
    html += "</select>";
    
    $("#sucursal_select").html(html);

    return true;
}