function adjustStyle(width) {
    width = parseInt(width);
    if (width < 600) {
        $("#size-stylesheet").attr("href", "/skin/default/css/esqueleto_320.css");
    } else if ((width >= 600) && (width < 900)) {
        $("#size-stylesheet").attr("href", "/skin/default/css/esqueleto_640.css");
    } else {
       $("#size-stylesheet").attr("href", "/skin/default/css/esqueleto_960.css"); 
    }
}
$(function() {
    adjustStyle($(this).width());
    $(window).resize(function() {
        adjustStyle($(this).width());
    });
});