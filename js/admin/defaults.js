/* Note, this file depends on the Jquery.Validate library */

$(document).ready(function () {
	jQuery.validator.addMethod(
		    "mymultiemail",
		    function (value, element) {
		        var email = value.split(/[, ]+/); // split ;
		        valid = true;
		        for (var i in email) {
		            value = email[i];
		            valid = valid && jQuery.validator.methods.email.call(this, $.trim(value), element);
		        }
		        return valid;
		    },
		    jQuery.validator.messages.multiemail
		);
	
$("#email_defaults").validate(
        {
        rules: {
            customerSupport: {
                //required: true,
                //email: true
            	mymultiemail: true
            },
            sales: {
                //required: true,
                //email: true
            	mymultiemail: true

            },
            operations: {
                //required: true,
                //email: true
            	mymultiemail: true
            },
            accounting: {
                //required: true,
                //email: true
            	mymultiemail: true
            },
            timeoff: {
                //required: true,
                //email: true
            	mymultiemail: true
            }
        },
		messages: {
			customerSupport: {
                mymultiemail: "You must enter a valid email, or ',' separated value. Don't put a ',' in the end because will be taken as invalid"
            },
			sales: {
                mymultiemail: "You must enter a valid email, or ',' separated value. Don't put a ',' in the end because will be taken as invalid"
            },
			operations: {
	                mymultiemail: "You must enter a valid email, or ',' separated value. Don't put a ',' in the end because will be taken as invalid"
	            },
        	accounting: {
	            mymultiemail: "You must enter a valid email, or ',' separated value. Don't put a ',' in the end because will be taken as invalid"
	        },
			timeoff: {
                mymultiemail: "You must enter a valid email, or ',' separated value. Don't put a ',' in the end because will be taken as invalid"
            }
        },
        errorClass: "invalid",
            invalidHandler: function (event, validator) {
            $(':submit').loading_indicator('destroy');
        }
        });

});