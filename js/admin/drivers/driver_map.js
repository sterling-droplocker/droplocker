var google = google || {};
var DropLockerDriverMap = (function($, google, console, OverlappingMarkerSpiderfier) {
	var my = {};
	var global_hue = Math.random(); // Initial HUE for colors
	var icons_url = '/images/map/largeTDRedIcons/';

	my.initialize = function(mapOptions, settings) {
		my.stops = settings.stops || [];
		my.params = settings.params || {};
		my.markers = [];
		my.sortedStops = [];
		my.polyline = null;
		my.sortTableSelector = settings.sortTableSelector || '#sort-routes';

		// start the map
		try {
			createMap(mapOptions);
		} catch (e) {
			console.log(e);
		}

		if (settings.stops) {
                    //my.updateMapStops();
			for (var x in settings.stops) {
				my.sortedStops.push(x);
				my.addStop(x);
				my.drawRoute();
			}
		}
	};

	/* ************************************************************************ */
	/* API functions                                                            */
	/* ************************************************************************ */

	/**
	 * Adds a new marker to the map in the locaiton of the stop
	 * @param index int the index of the stop in my.stops[]
	 */
	my.addStop = function(index) {
		var stop = my.stops[index];
		var sortOrder = stop.sortOrder === null ? 0 : stop.sortOrder;
		//var point = new google.maps.Point(stop.lat, stop.lon);

		var color = '#fc6e87';
		if (stop.type == 'OrderHomePickup') {
			color = '#1ee5fa';
		}

		if (stop.type == 'OrderHomeDelivery') {
			color = '#6aff0a';
		}

		if (stop.type == 'Delivery_CustomerSubscription') {
			color = '#f70';
		}

                my.markers[index] = new google.maps.Marker({
                                position: new google.maps.LatLng(stop.lat, stop.lon),
                                map: my.map,
                                title: index,
                                label: "" + (parseInt(index) + 1),
                                icon: pinSymbol(color)
                });

		my.markers[index].desc = index;

		my.oms.addMarker(my.markers[index]);

		function pinSymbol(color) {
		    return {
		        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
		        fillColor: color,
		        fillOpacity: 1,
		        strokeColor: '#000',
		        strokeWeight: 1,
		        scale: 1,
		        labelOrigin: new google.maps.Point(0,-29)
		    };
		}

/*
        google.maps.event.addListener(my.markers[index], 'click', function(evt) {
            my.infoWindow.setContent(my.initInfoWindowContent(index));
            my.infoWindow.open(my.map, my.markers[index]);
            $('#info_window_update').unbind('click');
            $('#info_window_update').bind('click', updateLocationSortOrder);
        });
*/

	};

	my.drawRoute = function() {
        if (my.polyline != null) {
            my.polyline.setMap(null);
        }

        var routePath = [];
        for (var index in my.sortedStops) {
            var stop = my.stops[my.sortedStops[index]];
            routePath.push(new google.maps.LatLng(stop.lat, stop.lon));
        }

        my.polyline = new google.maps.Polyline({
            path: routePath,
            strokeColor: '#8800dd',
            strokeOpacity: .6,
            strokeWeight: 4
        });
        my.polyline.setMap(my.map);
	};

	/**
	 * Renders the info window for the stop
	 * @param index int the index of the stop in my.stops[]
	 */
	my.initInfoWindowContent = function(index) {
		var stop = my.stops[index];
		var locationType = 'Location';

		if (stop.type == 'OrderHomeDelivery') {
			locationType = 'Home Delivery';
		}

		if (stop.type == 'OrderHomePickup') {
			locationType = 'Homde Pickup';
		}

		if (stop.type != 'Location') {
			locationType += ' (' + stop.timeWindow + ')';
		}

        var html = '<div id="map-stop-info" class="map-stop">' +
						'<h4 class="map-stop-type">' + locationType + '</h4>' +
						'<div class="map-stop-address"><b>Address 1:</b> <span>' + stop.address1 + '</span></div>' +
						(stop.address2 ? '<div class="map-stop-address"><b>Address 2:</b> <span>' + stop.address2 + '</span></div>' : '') +
						(stop.accessCode ? '<div class="map-stop-access"><b>Access Code:</b> <span>' + stop.accessCode + '</span></div>' : '') +
						'<div class="map-stop-form">' +
							(stop.type == 'Location' ? '<label class="map-stop-route">Route: <input type="text" name="route" style="width:40px;" value="' + stop.route + '" /></label>' : '') +
							' <label class="map-stop-sort">Sort: <input type="text" name="sortOrder" style="width:40px;" value="' + stop.sortOrder + '" /></span>' +
							' <button id="info_window_update">Update</button>' +
						'</div>' +
						'<input type="hidden" name="index" value="' + index + '">' +
						'<input type="hidden" name="sortOrder_id" value="' + stop.sortOrder_id + '">' +
						'<input type="hidden" name="type" value="' + stop.type + '">' +
					'</div>';		

		return html;
	};

	my.updateSortOrder = function(index, sortOrder, route) {
            sortOrder = parseInt(sortOrder);

            var currentStop = my.stops[index];
            var offset = parseInt(currentStop.sortOrder) > sortOrder ? 1 : -1;
            var min = Math.min(sortOrder, currentStop.sortOrder);
            var max = Math.max(sortOrder, currentStop.sortOrder);

            var updated = [];
            for (var x in my.stops) {

                // update the stop
                if (x == index) {
                        my.stops[x].sortOrder = sortOrder;
                        if (typeof route != 'undefined') {
                                my.stops[x].route = route;
                        }
                        updated.push(x);
                        continue;
                }
			
                var newStopSortOrder = my.stops[x].sortOrder;

                // move old stops to cover the slot of this stop
                if (my.stops[x].sortOrder >= min && my.stops[x].sortOrder <= max) {
                                    var newSortOrder = parseInt(my.stops[x].sortOrder) + offset;
                                    my.stops[x].sortOrder = newSortOrder;
                                    updated.push(x);
                }
            }

            // update the sorted array and the map markers
            var newSort = [];
            var i = 1
            for (var x in my.stops) {
                    newSort[my.stops[x].sortOrder - 1] = x;
                    my.markers[x].setLabel("" + i);
                    i++;
            }

            my.sortedStops = newSort;

            // redraw the line
            my.drawRoute();

            // close active window
            if (my.infoWindow) {
                    my.infoWindow.close();			
            }

            // do the request to the API to update the changed stops
            var data = {
                    stops: [],

                    // report options
                    routes: my.params.routes,
                    type:   my.params.type,
                    day:    my.params.day,
                    loaded: my.params.loaded,
                    no_map: my.params.no_map,
            };

            for (var i = 0; i < updated.length ; i++) {
                    var x = updated[i];
                    data.stops.push({
                            route:        my.stops[x].route,
                            sortOrder:    my.stops[x].sortOrder,
                            sortOrder_id: parseInt(my.stops[x].sortOrder_id),
                            objectType:   my.stops[x].type,
                    });
            }

            $.post('/admin/drivers/update_stops', data, function(response) {
                    if (response.reload) {
                            window.location.href = window.location.href;
                            return;
                    }
            });
            
            window.location.reload(false); 
	};

	my.afterSort = function (evt, ui) {       
            var stopsInOrder = [];
            
            $(my.sortTableSelector + ' tbody tr').each(function(i, el) {              
                stopsInOrder.push (parseInt($(el).data('index')));
            });  
            index = ui.item.index();
            //alert (JSON.stringify(stopsInOrder) + " - " + index);

            var data = {
                    stops: [],

                    // report options
                    routes: my.params.routes,
                    type:   my.params.type,
                    day:    my.params.day,
                    loaded: my.params.loaded,
                    no_map: my.params.no_map,
            };
            
            
            var i=0;
            while (i < stopsInOrder.length -1) {
                var i1=stopsInOrder[i];
                var i2=stopsInOrder[i+1];           
                var sorder1 = parseInt (my.stops[i1].sortOrder);
                var sorder2 = parseInt (my.stops[i2].sortOrder);
                var changed = false;
                if (sorder1 >= sorder2) {  
                    if (index == i // on moved item
                            && sorder1 != sorder2 // not the same sort order
                            && changed != true // required because same sort orders possible.
                            ) {
                        // Have moved backward and don't have duplicate sort orders
                        my.stops[i1].sortOrder = sorder2;                      
                        data.stops.push({
                            route:        my.stops[i1].route,
                            sortOrder:    my.stops[i1].sortOrder,
                            sortOrder_id: parseInt(my.stops[i1].sortOrder_id),
                            objectType:   my.stops[i1].type,
                        });
                        my.stops[i2].sortOrder = sorder2 + 1; 
                    } else {
                      my.stops[i2].sortOrder = sorder1 + 1;
                    }
                    changed = true;
                    data.stops.push({
                            route:        my.stops[i2].route,
                            sortOrder:    my.stops[i2].sortOrder,
                            sortOrder_id: parseInt(my.stops[i2].sortOrder_id),
                            objectType:   my.stops[i2].type,
                    });
                } 
                i++;
            }

            // updating database with changes.
            $.post('/admin/drivers/update_stops', data, function(response) {
                if (response.reload) {
                        window.location.href = window.location.href;
                        return;
                }
            });
            
            var sortOrder = 1;
            $(my.sortTableSelector + ' tbody tr').each(function(i, el) {
                $('.manifest-sort', el).html(sortOrder);
                sortOrder ++;
            });
            
            my.updateMapStops(stopsInOrder);
        };
        
        my.updateMapStops = function(stopsInOrder) {
            // update the sorted array and the map markers
            var newSort = [];
            for (var i in stopsInOrder) {
                x = stopsInOrder[i];
                newSort[my.stops[x].sortOrder - 1] = i;
                my.markers[x].setLabel("" + (parseInt(i) + 1));
                //alert ("setting my.stops[" + x + "] to " + sortOrder);    
                //sortOrder++;
            }

            my.sortedStops = newSort;

            // redraw the line
            my.drawRoute();

            // close active window
            if (my.infoWindow) {
                    my.infoWindow.close();			
            }
        }

	/* ************************************************************************ */
	/* Event handlers                                                           */
	/* ************************************************************************ */

	var updateLocationSortOrder = function(evt) {

		var form = $('#map-stop-info');
		var index = $('input[name=index]', form).val();
		var sortOrder = parseInt($('input[name=sortOrder]', form).val());
		var route = $('input[name=route]', form).val();

		my.updateSortOrder(index, sortOrder, route);
	};

	/* ************************************************************************ */
	/* Map functions                                                            */
	/* ************************************************************************ */

	/**
	 * Creates the map
	 */
	var createMap = function (mapOptions) {
	    var options = {
	        zoom: mapOptions.zoom,
	        center: new google.maps.LatLng(mapOptions.lat, mapOptions.lon),
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    };

	    my.map        = new google.maps.Map(document.getElementById('map-canvas'), options);
	    my.geocoder   = new google.maps.Geocoder();
	    my.infoWindow = new google.maps.InfoWindow();
            my.oms	  = new OverlappingMarkerSpiderfier(my.map);

	    // wait to load the autocomplete for when the map has been loaded
	    google.maps.event.addListenerOnce(my.map, 'idle', function() {
	        fitStops();
	    });

            my.oms.addListener('click', function(marker, event) {
                var index = parseInt(marker.desc);
                my.infoWindow.setContent(my.initInfoWindowContent(index));
                my.infoWindow.open(my.map, my.markers[index]);
                $('#info_window_update').unbind('click');
                $('#info_window_update').bind('click', updateLocationSortOrder);
            });

            my.oms.addListener('spiderfy', function(markers) {
                my.infoWindow.close();
            });

	};

	/**
	 * Fit fences into view
	 */
	var fitStops = function() {
		var bounds = new google.maps.LatLngBounds();
		for (var x in my.markers) {
			bounds.extend(my.markers[x].getPosition());
		}
		my.map.fitBounds(bounds);
	};

	/**
	 * Converts points into a polygon path
	 */
	var pointsToPath = function(points) {
		var path = [];
		for (var i = 0 ; i < points.length ; ++i) {
			var start = new google.maps.LatLng(points[i].lat, points[i].lng);
			path.push(start);
		}

		return path;
	}

	/* ************************************************************************ */
	/* Fence functions                                                          */
	/* ************************************************************************ */



	/**
	 * Draws a new zone fence and setups the event handlers
	 */
	var drawGeoFence = function(path, color, id) {
		var polyline = google.maps.geometry.encoding.encodePath(path);

	    var geofence = new google.maps.Polygon({
			id: id,
			name: "Geofence " + id,
			editable: true,
			draggable: true,
			map: my.map,
			strokeOpacity: 1.0,
			strokeColor: color,
			strokeWeight: 2,
			fillColor: color,
			fillOpacity: 0.3,
			paths: path,
			polyline: polyline,
			area: google.maps.geometry.spherical.computeArea(path)/1000000
	    });

	    google.maps.event.addListener(geofence, 'click', function(){
	      this.setEditable(!this.getEditable());
	      this.setDraggable(this.getEditable());
	    });

	    var polygon_path = geofence.getPath();

	    google.maps.event.addListener(polygon_path, 'set_at', function (event) {
	      refreshGeofence(this, geofence);
	    });

	    google.maps.event.addListener(polygon_path, 'insert_at', function (event) {
	      refreshGeofence(this, geofence);
	    });

	    my.fences[id] = geofence;

	    return geofence;
	};

	/**
	 * Updates the geofence when an edge is moved
	 */
	var refreshGeofence = function(path, myGeofence) {
		 // Iterate over the vertices.
		var points = [];
		for (var i =0; i < path.getLength(); i++) {
			var xy = path.getAt(i);
			points.push({lat:xy.lat(), lng:xy.lng()});
		}

		var zone_id = myGeofence.id;
		my.setZonePoints(zone_id, points);
	};

	/* ************************************************************************ */
	/* Color Helpers                                                            */
	/* ************************************************************************ */
	/**
	 * Generates a new color in #RRGGBB format
	 */
	var randomColor = function() {
		var golden_ratio_conjugate = 0.618033988749895; // Offset to generate new colors
		global_hue += golden_ratio_conjugate;
		global_hue = global_hue > 1 ? global_hue - 1 : global_hue;

		return '#' + hsb2rgb(global_hue, 0.8, 0.95).join('');
	};

	/**
	 * Convers a color from HSL space to RGB
	 */
	var hsb2rgb = function (h, s, v) {
	    var r, g, b, i, f, p, q, t;
	    if (arguments.length === 1) {
	        s = h.s, v = h.v, h = h.h;
	    }
	    i = Math.floor(h * 6);
	    f = h * 6 - i;
	    p = v * (1 - s);
	    q = v * (1 - f * s);
	    t = v * (1 - (1 - f) * s);
	    switch (i % 6) {
	        case 0: r = v, g = t, b = p; break;
	        case 1: r = q, g = v, b = p; break;
	        case 2: r = p, g = v, b = t; break;
	        case 3: r = p, g = q, b = v; break;
	        case 4: r = t, g = p, b = v; break;
	        case 5: r = v, g = p, b = q; break;
	    }
	    return [Math.round(r * 255).toString(16), Math.round(g * 255).toString(16), Math.round(b * 255).toString(16)];
	}

	/**
	 * Converts a color in HEX format into R,G,B
	 */
	var hex2rgb = function (hex) {
	    var bigint = parseInt(hex.replace('#', ''), 16);
	    var r = (bigint >> 16) & 255;
	    var g = (bigint >> 8) & 255;
	    var b = bigint & 255;
	    return r + "," + g + "," + b;
	}


	

	return my;
}(jQuery, google, console, OverlappingMarkerSpiderfier));