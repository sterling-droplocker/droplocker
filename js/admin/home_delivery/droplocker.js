var google = google || {};
var DropLockerHomeDeliveryService = (function($, google, console) {
	var my = {};
	var global_hue = Math.random(); // Initial HUE for colors

	my.initialize = function(mapOptions, settings) {
		my.fences = [];
		my.locations = settings.locations || {};

		my.div = $('#home-delivery-map');
		my.lastWindowId = $('.delivery-zone', my.div).length - 1; //TODO read last id from fences

		// Setup slots event handlers
		my.div.on('click', '.delivery-zone-add-row', addRowClick);
		my.div.on('click', '.delivery-zone-remove-row', deleteRowClick);

		// Setup zone event handlers
		my.div.on('click', '.delivery-zone-remove', deleteZoneClick);
		my.div.on('click', '.delivery-zone-add', addZoneClick);
		my.div.on('click', '.delivery-zone-color', colorZoneClick);

		// start the map
		try {
			createMap(mapOptions);
		} catch (e) {
			console.log(e);
		}


		if (settings.zones) {
			for (var x in settings.zones) {
				var zone = settings.zones[x];
				var path = pointsToPath(zone.points);
				my.addZone(path, zone.color, zone.location_id, zone.windows, zone.delivery_zoneID);
			}
		}

	};

	/* ************************************************************************ */
	/* Map functions                                                            */
	/* ************************************************************************ */

	/**
	 * Creates the map
	 */
	var createMap = function (mapOptions) {
	    var options = {
	        zoom: mapOptions.zoom,
	        center: new google.maps.LatLng(mapOptions.lat, mapOptions.lon),
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    };

	    my.map        = new google.maps.Map(document.getElementById('map-canvas'), options);
	    my.geocoder   = new google.maps.Geocoder(); //TODO: this is not used
	    my.infoWindow = new google.maps.InfoWindow(); //TODO: this is not used

	    // wait to load the autocomplete for when the map has been loaded
	    google.maps.event.addListenerOnce(my.map, 'idle', function() {
	        fitFences();
	    });
	};

	/**
	 * Fit fences into view
	 */
	var fitFences = function() {
		if (!my.fences.length) {
			return;
		}

		var bounds = new google.maps.LatLngBounds();
		for (var x in my.fences) {
			var fence = my.fences[x];
			var path = fence.getPath();

			for (var i =0; i < path.getLength(); i++) {
				var point = path.getAt(i);
				bounds.extend(point);
			}
		}
		my.map.fitBounds(bounds);
	};

	/**
	 * Converts points into a polygon path
	 */
	var pointsToPath = function(points) {
		var path = [];
		for (var i = 0 ; i < points.length ; ++i) {
			var start = new google.maps.LatLng(points[i].lat, points[i].lng);
			path.push(start);
		}

		return path;
	}

	/* ************************************************************************ */
	/* Zones API                                                                */
	/* ************************************************************************ */

	/**
	 * Create a new zone
	 */
	my.addZone = function(path, color, location_id, windows, delivery_zoneID) {
		var zone_id = getNewZone(color, path, location_id, windows, delivery_zoneID);
	    drawGeoFence(path, color, zone_id);

		return zone_id;
	};

	/**
	 * Deletes a zone
	 */
	my.deleteZone = function(zone_id) {
		var zone = getZoneByID(zone_id).remove();
		if (zone.length) {
			var fence = my.fences[zone_id];
			fence.setMap(null);
		} else {
			console.log('Error: Unknown zone ' + zone_id);
		}
	};

	/**
	 * Updates a zone color
	 */
	my.setZoneColor = function(zone_id, color) {
		my.fences[zone_id].setOptions({strokeColor: color, fillColor: color});

		var zone = getZoneByID(zone_id);
		zone.css('background-color', 'rgba('+ hex2rgb(color) + ',0.5)');
		$('input[name="zones['+zone_id+'][color]"]', zone).val(color);
	};

	/**
	 * Updates a Zone points
	 */
	my.setZonePoints = function(zone_id, points) {
	    my.fences[zone_id].set("polyline", points);
		var zone = getZoneByID(zone_id);
		console.log(zone);
		$('input[name="zones['+zone_id+'][points]"]', zone).val(JSON.stringify(points));
	}

	/**
	 * Adds a slot to a zone
	 */
	my.addSlot = function (zone_id) {
		var zone = getZoneByID(zone_id);
		var row_id = parseInt($('.delivery-zone-row:last', zone).attr('data-zone-row-id')) + 1;
		if (isNaN(row_id)) {
			row_id = 0;
		}

		$('tbody', zone).append(getZoneTableRow(zone_id, row_id, {}));

	};

	/**
	 * Disables fences edition
	 */
	my.disableFencesEdition = function() {
		if (!my.fences.length) {
			return;
		}

		for (var x in my.fences) {
			var fence = my.fences[x];
	        fence.setEditable(false);
	        fence.setDraggable(false);
		}
	};

	/**
	 * Gets a zone div using its zone_id
	 */
	var getZoneByID = function(zone_id) {
		return $('.delivery-zone[data-zone-id='+zone_id+']');
	};

	/* ************************************************************************ */
	/* Fence functions                                                          */
	/* ************************************************************************ */

	/**
	 * Generates the default square polygon path for new fences
	 */
	var getDefaultPath = function () {
	    /* calculate side length based on zoom */
	    var step = (1 * 10000000)/(Math.pow(2, my.map.getZoom()));
	    var center = my.map.getCenter();
	    var path = [];

	    /* Create a fence with  4 sides */
	    for (var heading = 45; heading < 360; heading += 90) {
			var corner = google.maps.geometry.spherical.computeOffset(center, step, heading);
			path.push(corner);
	    }

		return path;
	};

	/**
	 * Creates a new zone
	 */
	var getNewZone = function(color, path, location_id, windows, delivery_zoneID) {
	    // Grab a new id for our zone based on existing ones
	    var id = ++my.lastWindowId;

	    // Create a new div
		var table = getZoneTable(id, color, path, location_id, windows, delivery_zoneID);
	    var div = $('<div data-zone-id="'+id+'" class="delivery-zone"></div>').html(table).css('background-color', 'rgba('+ hex2rgb(color) + ',0.5)');

		$('.delivery-zones-list', my.div).append(div);

	    return id;
	};

	/**
	 * Get the list of locations formatted as <option> elements
	 */
	var getLocations = function(selected_location) {
		var html = '';
		for (var serviceType in my.locations) {
			html += '<optgroup label="'+serviceType+'">';
			for (var id in my.locations[serviceType]) {
				html += '<option value="'+id+'" ' + (selected_location == id ? 'selected': '') + '>'+my.locations[serviceType][id]+'</option>';
			}
			html += '</optgroup>';
		}

		return html;
	};

	/**
	 * Generates the HTML for the zones
	 */
	var getZoneTable = function(zone_id, color, path, location_id, windows, delivery_zoneID) {
	    var table_rows = '';

		if (typeof windows == 'undefined') {
			windows = [{start: '9:00', end: '18:00'}];
		}

		if (typeof delivery_zoneID == 'undefined') {
			delivery_zoneID = '';
		}

	    for (var i = 0; i < windows.length; i++) {
	        table_rows += getZoneTableRow(zone_id, i, windows[i]);
	    }

		var locations = getLocations(location_id);

		return '<table>' +
			'<thead>' +
				'<tr>' +
					'<th style="width:120px;">Time Window</th>' +
					'<th>Mon</th>' +
					'<th>Tue</th>' +
					'<th>Wed</th>' +
					'<th>Thu</th>' +
					'<th>Fri</th>' +
					'<th>Sat</th>' +
					'<th style="width:120px;">Sun</th>' +
					'<th>Route</th>' +
				'</tr>' +
			'</thead>' +
		'<tbody>' +
			table_rows +
		'</tbody>' +
		'</table>' +

		'<div class="delivery-zone-location">'+
			'<b>Location:</b> <select name="zones['+zone_id+'][location_id]">'+locations+'</select>' +
		'</div>'+

		'<div class="delivery-zone-actions">' +
			'<a href="javascript:void(0);" class="delivery-zone-add-row"><img src="/images/icons/add.png">Add time slot</a> ' +
			'<a href="javascript:void(0);" class="delivery-zone-remove"><img src="/images/icons/delete.png">Remove fence</a> '+
			'<a href="javascript:void(0);" class="delivery-zone-color"><img src="/images/icons/arrow_refresh.png">Change color</a> '+
		'</div>' +

		'<input type="hidden" name="zones['+zone_id+'][color]" value="'+color+'">' +
		'<input type="hidden" name="zones['+zone_id+'][points]" value=\''+JSON.stringify(path)+'\'>' +
		'<input type="hidden" name="zones['+zone_id+'][delivery_zoneID]" value="'+delivery_zoneID+'">'
		;
	};

	/**
	 * Generates the HTML for each slot
	 */
	var getZoneTableRow = function(zone_id, row_id, timeWindow) {
	    var days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

		var start = timeWindow.start || '';
		var end = timeWindow.end || '';
		var route = timeWindow.route || '';

		if (typeof timeWindow.capacity == 'undefined') {
			timeWindow.capacity = {};
		}

		if (typeof timeWindow.delivery_windowID == 'undefined') {
			timeWindow.delivery_windowID = '';
		}

	    // If you change the TR class you must change it in the 'add slot' bind function as well
	    var template = '<tr class="delivery-zone-row" data-zone-row-id="' + row_id + '">' +
						'<td>' +
							'<input type="hidden" name="zones[' + zone_id + '][windows][' + row_id + '][delivery_windowID]" value="'  + timeWindow.delivery_windowID + '">'+
							'<input type="text" name="zones[' + zone_id + '][windows][' + row_id + '][start]" value="'  + start + '" placeholder="h:mm" style="width:30px;">-'+
							'<input type="text" name="zones[' + zone_id + '][windows][' + row_id + '][end]" value="'  + end + '" placeholder="h:mm" style="width:30px;">'+
						'</td>';

	    for (var i = 0, j = days.length; i < j; i ++) {
			var day = days[i];
			var capacity = timeWindow.capacity[day] || '';
	        template += '<td nowrap><input type="text" name="zones[' + zone_id + '][windows][' + row_id + '][capacity]['+ days[i] +']" style="width:30px;text-align: right;" value="'+capacity+'"></td>';
	    }

	    //add route row
	    template += '<td><input type="text" name="zones[' + zone_id + '][windows][' + row_id + '][route]" value="'+route+'" style="width:30px;text-align: right;"></td>';
	    template += '<td><a class="delivery-zone-remove-row" href="#"><img src="/images/icons/cross.png"></a></td>';
	    template += '</tr>';

	    return template;
	};

	/**
	 * Draws a new zone fence and setups the event handlers
	 */
	var drawGeoFence = function(path, color, id) {
		var polyline = google.maps.geometry.encoding.encodePath(path);

	    var geofence = new google.maps.Polygon({
			id: id,
			name: "Geofence " + id,
			editable: true,
			draggable: true,
			map: my.map,
			strokeOpacity: 1.0,
			strokeColor: color,
			strokeWeight: 2,
			fillColor: color,
			fillOpacity: 0.3,
			paths: path,
			polyline: polyline,
			area: google.maps.geometry.spherical.computeArea(path)/1000000
	    });

	    google.maps.event.addListener(geofence, 'click', function(){
	      this.setEditable(!this.getEditable());
	      this.setDraggable(this.getEditable());
	    });

	    var polygon_path = geofence.getPath();

	    google.maps.event.addListener(polygon_path, 'set_at', function (event) {
	      refreshGeofence(this, geofence);
	    });

	    google.maps.event.addListener(polygon_path, 'insert_at', function (event) {
	      refreshGeofence(this, geofence);
	    });

	    my.fences[id] = geofence;

	    return geofence;
	};

	/**
	 * Updates the geofence when an edge is moved
	 */
	var refreshGeofence = function(path, myGeofence) {
		 // Iterate over the vertices.
		var points = [];
		for (var i =0; i < path.getLength(); i++) {
			var xy = path.getAt(i);
			points.push({lat:xy.lat(), lng:xy.lng()});
		}

		var zone_id = myGeofence.id;
		my.setZonePoints(zone_id, points);
	};

	/* ************************************************************************ */
	/* Color Helpers                                                            */
	/* ************************************************************************ */
	/**
	 * Generates a new color in #RRGGBB format
	 */
	var randomColor = function() {
		var golden_ratio_conjugate = 0.618033988749895; // Offset to generate new colors
		global_hue += golden_ratio_conjugate;
		global_hue = global_hue > 1 ? global_hue - 1 : global_hue;

		return '#' + hsb2rgb(global_hue, 0.8, 0.95).join('');
	};

	/**
	 * Convers a color from HSL space to RGB
	 */
	var hsb2rgb = function (h, s, v) {
	    var r, g, b, i, f, p, q, t;
	    if (arguments.length === 1) {
	        s = h.s, v = h.v, h = h.h;
	    }
	    i = Math.floor(h * 6);
	    f = h * 6 - i;
	    p = v * (1 - s);
	    q = v * (1 - f * s);
	    t = v * (1 - (1 - f) * s);
	    switch (i % 6) {
	        case 0: r = v, g = t, b = p; break;
	        case 1: r = q, g = v, b = p; break;
	        case 2: r = p, g = v, b = t; break;
	        case 3: r = p, g = q, b = v; break;
	        case 4: r = t, g = p, b = v; break;
	        case 5: r = v, g = p, b = q; break;
	    }
	    return [Math.round(r * 255).toString(16), Math.round(g * 255).toString(16), Math.round(b * 255).toString(16)];
	}

	/**
	 * Converts a color in HEX format into R,G,B
	 */
	var hex2rgb = function (hex) {
	    var bigint = parseInt(hex.replace('#', ''), 16);
	    var r = (bigint >> 16) & 255;
	    var g = (bigint >> 8) & 255;
	    var b = bigint & 255;
	    return r + "," + g + "," + b;
	}

	/* ************************************************************************ */
	/* Event handlers                                                           */
	/* ************************************************************************ */

	/**
	 * Add Slot button clicked
	 */
	var addRowClick = function(evt) {
		var zone = $(evt.target).closest('.delivery-zone');
		var zone_id = zone.attr('data-zone-id');

		my.addSlot(zone_id);
	};

	/**
	 * Remove Slot button clicked
	 */
	var deleteRowClick = function(evt) {
		// TODO: could be an API
		$(evt.target).closest('tr').remove();
	};


	/**
	 * Add Zone button clicked
	 */
	var addZoneClick = function(evt) {
		var path = getDefaultPath();
		var color = randomColor();
		my.disableFencesEdition();
		my.addZone(path, color);
	};

	/**
	 * Delete Zone button clicked
	 */
	var deleteZoneClick = function(evt) {
		var zone = $(evt.target).closest('.delivery-zone');
		var zone_id = parseInt(zone.attr('data-zone-id'));

		my.deleteZone(zone_id);
	};

	/**
	 * Change Zone Color button clicked
	 */
	var colorZoneClick = function(evt) {
		var color = randomColor();
		var zone = $(evt.target).closest('.delivery-zone');
		var zone_id = parseInt(zone.attr('data-zone-id'));

		my.setZoneColor(zone_id, color);
	};

	return my;
}(jQuery, google, console));