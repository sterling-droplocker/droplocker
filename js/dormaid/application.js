// Images
var loading = new Image();

$(document).ready(function(){
  initDOM();
  registerEvents();

  // Preload Images
  loading.src = "/images/dormaid/loading.gif";
});

/* jQuery Extensions */

jQuery.extend(
  jQuery.expr[':'], {
    'contains-nocase': function(a, i, m) {
      return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
    },
    regex: function(a, i, m, r) {
      var r = new RegExp(m[3], 'i');
      return r.test(jQuery(a).text());
    }
  }
);

jQuery.fn.extend({
  startLoading: function(x, y){
    return this.each(function(){
      if (typeof x == 'undefined' || x == 'center') {
        x = ($(this).width() - loading.width)/2;
        x = Math.max(x,0);
        x = x+'px';
      }
      if (typeof y == 'undefined' || y == 'center') {
        y = ($(this).height() - loading.height)/2;
        y = Math.max(y,0);
        y = y+'px';
      }
      $(this).html('<div class="loading" style="width:'+ $(this).width()+'px; height:'+$(this).height()+'px">'+
                   '<img src="'+loading.src+'" style="margin-top:'+y+'; margin-left:'+x+'" />'+
                   '</div>');
    });
  }
});

/* Initialize Dom Elements */

function initDOM($context) {
  if (typeof $context == 'undefined') { $context = $(document.body); }

  $('input.autocomplete-off', $context).attr('autocomplete', 'off');

  $("a.get, a.post, a.put, a.delete", $context).removeAttr('onclick');
}

/* Register Events */

function registerEvents() {

  /* Global */
  $(".disabled").live('click', function(e){
    e.preventDefault();
  });
  
  $('.noenter input, .noenter select').live('keypress', function(){
    keycode = e.which ? e.which : e.keyCode;
    if(keycode == 13) {
      event.preventDefault();
      return false;
    }
  });
  
  $(".file-upload :submit").live('click', function(e){
    $(".file", $(this).closest("form")).startLoading();
  });

  /* Ajaxify Some Stuff */

  $("body").bind('ajaxSend', function(elm, xhr, s) {
    if (s.type == "GET") return;
    if (s.data && s.data.match(new RegExp("\\b" + window._auth_token_name + "="))) return;
    if (s.data) {
      s.data = s.data + "&";
    } else {
      s.data = "";
      // if there was no data, $ didn't set the content-type
      xhr.setRequestHeader("Content-Type", s.contentType);
    }
    s.data = s.data + encodeURIComponent(window._auth_token_name)
                    + "=" + encodeURIComponent(window._auth_token);
  });

  $("a:not(.disabled).get").live('click', function(){
    var link = $(this);
    $.get(link.attr('href'), function(data) {
      if (link.attr('ajaxtarget'))
        $(link.attr('ajaxtarget')).html(data);
    }, "script");
    return false;
  }).attr("rel", "nofollow");

  $("a:not(.disabled).post").live('click', function(){
    var link = $(this);
    $.post(link.attr('href'), "_method=post", function(data) {
      if (link.attr('ajaxtarget'))
        $(link.attr('ajaxtarget')).html(data);
    }, "script");
    return false;
  }).attr("rel", "nofollow");

  $("a:not(.disabled).put").live('click', function(){
    var link = $(this);
    $.post(link.attr('href'), "_method=put", function(data) {
      if (link.attr('ajaxtarget'))
        $(link.attr('ajaxtarget')).html(data);
    }, "script");
    return false;
  }).attr("rel", "nofollow");

  $("a:not(.disabled).delete").live('click', function(){
    var link = $(this);
    $.post(link.attr('href'), "_method=delete", function(data) {
      if (link.attr('ajaxtarget'))
        $(link.attr('ajaxtarget')).html(data);
    }, "script");
    return false;
  }).attr("rel", "nofollow");

  $("form.ajax :submit").live('click', function(e){
    e.preventDefault();
    var $form = $(this).closest("form.ajax");
    $form.prepend('<input name="ajax_target" type="hidden" value="'+$form.attr('id')+'" />');
    $form.ajaxSubmit({dataType:'script'});
    if ($form.hasClass('animated')) {
      $(this).closest("fieldset").startLoading();
    }
    return false;
  });
  
  $("form.ajax :input").live('keypress', function(e){
    keycode = e.which ? e.which : e.keyCode;
    if(keycode == 13) {
      e.preventDefault();
      // TODO: dry this up
      var $form = $(this).closest("form.ajax");
      $form.prepend('<input name="ajax_target" type="hidden" value="'+$form.attr('id')+'" />');
      $form.ajaxSubmit({dataType:'script'});
      if ($form.hasClass('animated')) {
        $(this).closest("fieldset").startLoading();
      }
      return false;
    }
  });

  $("a.open-login").live('click', function(e){
    e.preventDefault();
    $("#topbar-container").animate({ height: "330px" }, 600);
    $(this).delay(300, function(){
      $("#topbar-dialog").fadeIn(800, function(){});
      $(this).delay(500, function(){
        $("#topbar-dialog #user_session_email").select();
      });
    });
  });

  $("a.close-login").live('click', function(e){
    e.preventDefault();
    $("#topbar-dialog").fadeOut(800);
    $(this).delay(300, function(){
      $("#topbar-container").animate({ height: "35px" }, 800);
    });
  });
  
  /* School Selector */  
  $('#school-text').live('click', function(){
    $(this).hide();
    $('#school-input').show();
    initSchoolSelector();
    $('#school-input').select();
  });
  
  $('#school-input').live('focusin', function(){
    if (current_school == null) {
      $(this).attr('value', '');
    }
  });
  
  $('#school-input').live('focusout', function(){
    if (current_school != null) {
      $(this).hide();
      var school = $('#school-text .name').html();
      $(this).attr('value', school);
      $('#school-text').show();
    }
    else {
      $(this).attr('value', $(this)[0].defaultValue);
    }
  });
  
  $('#home_sub.disabled').live('click', function(){
    $('#school-input').addClass('error');
  });
  
  $('#inquiry_email').live('focusin', function(){
    if ($(this).attr('value') == "Your Email") {
      $(this).attr('value', '');
    }
  });
  
  $('#inquiry_email').live('focusout', function(){
    if ($(this).attr('value') == "") {
      $(this).attr('value', 'Your Email');
    }
  });
}


/* Animation Effects */

function slideDownFade(div, callback) {
  $(div).addClass('slider-down');
  var $inner = $(div).children(".inner:first");
  if ($inner.length == 0) {
    $inner = $(div).wrapInner('<div class="inner"></div>').children(".inner");
  }
  $inner.css('opacity', 0).show();
  var height = $inner.outerHeight();
  $(div).css('overflow', 'visible');
  $(div).animate({ height: height+"px" }, 300);
  $(this).delay(300, function(){
    $inner.animate({ opacity:1 }, 200);
    $(div).removeClass('slider-up');
    if (typeof callback == 'function') {
      callback();
    }
  });
}

function slideUpFade(div, callback) {
  if (!$(div).is(".slider-down")) return false;
  $(this).removeClass('slider-down');
  var $inner = $(div).children(".inner:first");
  if ($inner.length == 0) {
    $inner = $(div).wrapInner('<div class="inner"></div>').children(".inner");
  }
  $inner.animate({ opacity:0 }, 200);
  $(this).delay(200, function(){
    $(div).animate({ height: "0px" }, 300, function(){
      $(div).addClass('slider-up');
      $inner.hide();
      if (typeof callback == 'function') {
        callback();
      }
    });
  });
}

/* School Selector */
function initSchoolSelector() {
  if (school_autoselect == null) {
    school_autoselect = $('#school-input').autocomplete({
      width: 500,
      delimiter: /(,|;)\s*/,
      onSelect: onSchoolSelect,
      lookup: all_schools
    });
  }
}

var onSchoolSelect = function(value, data){
  // Waiting
  $('#school-input').hide();
  $('#school-text').show();
  $('#school-text').startLoading(0, 'center');

  // Now go set it for real
  $form = $('#select-school-form');
  $form.ajaxSubmit({dataType:'script'});
}
