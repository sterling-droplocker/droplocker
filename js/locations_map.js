/* 
 * Author: lb
 */

;

dl__url = (typeof dl__url !== 'undefined') ? dl__url : 'https://droplocker.com';

dl__business_location_data = (typeof dl__business_location_data !== 'undefined') ? dl__business_location_data : {};
dl__map_selector = (typeof dl__map_selector !== 'undefined') ? dl__map_selector : 'map';
dl__search_box_selector = (typeof dl__search_box_selector !== 'undefined') ? dl__search_box_selector : 'map-search';
dl__default_lat = (typeof dl__default_lat !== 'undefined') ? dl__default_lat : 37.800000;
dl__default_lng = (typeof dl__default_lng !== 'undefined') ? dl__default_lng : -122.357221;

dl__private_location_api = dl__url + '/public/v1/publicapi/get_all_private_locations_if_one_near';
dl__zoom_on_search = 18;
dl__icon_height = 40;
dl__mark_current_location = false;

dl__current_location = null;
dl__current_marker = null;

function loadLocationMap(containerId){
        var private_locations_loaded = false;
        

	var mapContainer = document.getElementById(containerId);
	var mapOptions = {
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
	};
        var map = null;
        var infowindow = new google.maps.InfoWindow();
        var defaultLocation = new google.maps.LatLng(dl__default_lat, dl__default_lng);

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, onError);
		// also monitor position as it changes
		navigator.geolocation.watchPosition(showPosition);
	} else {
		onError();
	}

       function setSearchBox(map) {
            // Create the search box and link it to the UI element.
            var input = document.getElementById(dl__search_box_selector);
            
            if (!input) {
                return;
            }
            
            var searchBox = new google.maps.places.SearchBox(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                  return;
                }

                var place = places[0];
                if (!place.geometry) {
                  console.log("Returned place contains no geometry");
                  return;
                }

                setCurrentLocation({
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng(),
                    info: place.formatted_address
                });
                
                var gpoint = new google.maps.LatLng(dl__current_location.lat, dl__current_location.lng);
                
                if (!private_locations_loaded) {
                    console.log('Searching private locations');
                    
                    jQuery.ajax({
                        method: 'GET',
                        url: dl__private_location_api,
                        data:{
                            business_id: dl__business_id,
                            lat: place.geometry.location.lat(),
                            lon: place.geometry.location.lng()
                        }
                    }).done(function( data ) {
                        console.log(data);
                        console.log(data.message);
                        
                        if (data.status !== 'error') {
                            dl__business_location_data = dl__business_location_data.concat(Object.values(data.data));
                            private_locations_loaded = true;
                        }
                        
                        showMap(mapContainer, mapOptions, gpoint, 'You searched a location');
                    });
                } else {
                    showMap(mapContainer, mapOptions, gpoint, 'You searched a location');
                }
                
           });
        };
        
	function onError() {
                var contentString = '';
		if (navigator.geolocation) {
                    contentString = "Error: Geolocation service failed.";
		} else {
                    contentString = "Error: Your browser does not support geolocation.";
		}

                setCurrentLocation(null);

		showMap(mapContainer, mapOptions, defaultLocation, contentString);
	};
        
	function showPosition(position) {
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		var initialLocation = new google.maps.LatLng(lat, lng);

		var contentString = "you are here (aproximatelly):" + lat + "," + lng;
                
                setCurrentLocation({
                    lat: lat,
                    lng: lng,
                    info: "You are here"
                });

		showMap(mapContainer, mapOptions, initialLocation, contentString);
	};
        
        function createMap(mapContainer, mapOptions) {
            map = new google.maps.Map(mapContainer, mapOptions);
            setSearchBox(map);
        }
        
	function showMap(mapContainer, mapOptions, center, contentString){
                if (!map) {
                    createMap(mapContainer, mapOptions);
                } else {
                    map.setZoom(dl__zoom_on_search);
                }
                
		map.setCenter(center);
                
                console.log(contentString);

                loadLocations(map);
	};
        
        function loadLocations(map) {
            for (var key in dl__business_location_data) {
                var item = dl__business_location_data[key];
                var lat = item.lat;
                var lon = item.lon;
                var icon_url = (item.locationType_id == 1) ? 
                    (dl__url + '/images/marker_public_24_7_2x.png') : ( (item.is_public)? dl__url + '/images/marker_locker_public.png' : dl__url + '/images/marker_locker_private.png');

                add_marker(map, lat, lon, '', item.info, icon_url);
            };
            
            if (dl__mark_current_location && dl__current_location) {
                dl__current_marker = add_marker(
                    map,
                    dl__current_location.lat, 
                    dl__current_location.lng, 
                    '', 
                    dl__current_location.info, 
                    dl__url + '/images/marker_home.png'
                );
            }
        };
        
        var markers = [];
        
        function add_marker(map, lat, lon, text, info, icon_url) {
            var latlng = new google.maps.LatLng(lat, lon);
            //bounds.extend(latlng);
            
            var icon_height = dl__icon_height;
            var icon_width = icon_height * 35 / 54;
            
            var icon = {
              url: icon_url,
              scaledSize: new google.maps.Size(icon_width, icon_height)
            };

            var marker = new google.maps.Marker({
              icon: icon,
              map: map,
              title: text,
              position: latlng
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(info);
                infowindow.open(map, marker);
            });
            
            markers.push(marker);
            
            return marker;
        };
        
        function setCurrentLocation(location) {
            if (dl__current_marker) {
                dl__current_marker.setMap(null);
                dl__current_marker = null;
            };
            
            dl__current_location = location;
        };
}

loadLocationMap(dl__map_selector);


