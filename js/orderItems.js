var type = 'POST';
var url = null;
var data = null;
var dataType = 'json';
var ajax_response = null;

$(document).ready(function(){
	
	$('#new_submit').click(function(){
		url = "/admin/orders/add_new_item_ajax/";
		data = {
			product_id: $('#product_id').val(),
			order_id: order_id,
			qty: $('#qty').val(),
			washLocation_id: $('#washLocation_id').val(),
			notes: $('#notes').val()
		};
		
		ajax();
		if(ajax_response.status=='fail'){
			alert(ajax_response.error);
		}
		else{
			top.location.reload();
		}
	});
	
	$('.del_button').live('click', function(){
		if(!verify_delete()){return false;
			}
		var d = $(this).attr('id');
		d = d.replace("del-","");
		url = "/admin/orders/delete_item_ajax/";
		data = {
			orderItemID: d,
		};
		
		ajax();
		if(ajax_response.status=='fail'){
			alert(ajax_response.error);
		}
		else{
			$('#row-'+d).css('display', 'none');
		}
	});
	
	$('#ticket_button').live('click', function(){
		
	   	
		var d = $(this).attr('id');
		d = d.replace("del-","");
		url = "/admin/orders/update_order_ticket_ajax/";
		data = {
			order_id: order_id,
			ticketNum: $('#ticketNum').val()
		};
		
		ajax();
		if(ajax_response.status=='fail'){
			alert(ajax_response.error);
		}
		else{
			$('#ticket_response').fadeIn().delay(1000).fadeOut();
		}
	});
	
	
});

function ajax(){
  var request = $.ajax({
      type: type,
      async:false,
      data: data,
      url: url,
      dataType: dataType
   });
   
   request.done(function(res){
    	ajax_response = res;
    });
}


function setDiscount(a, l) {
    document.getElementById("chargeType").value = l;
    document.getElementById("chargeAmount").value = a.toFixed(2);
}
function setPercentDiscount(p) {
    setDiscount(Math.floor(document.getElementById("h_orderTotal").value * p) / -100, p+"% Discount");
}
function setBestBayDiscount(p) {
    setDiscount(Math.floor(document.getElementById("h_orderTotal").value * p) / -100, p+"% Best of the Bay Promo");
}
function setDollarDiscount(p) {
    setDiscount(-p, "$"+p+" Discount");
}
function minimumTotal() {
    amt = 15 - document.getElementById("h_orderTotal").value;
    if (amt > 0) {
        setDiscount(amt, "$15 Minimum");
    } else {
        alert("Order total is > $15");
    }
}
function minimumTotal25() {
    amt = 25 - document.getElementById("h_orderTotal").value;
    if (amt > 0) {
        setDiscount(amt, "$25 Minimum");
    } else {
        alert("Order total is > $25");
    }
}

function minimumTotal50() {
    amt = 50 - document.getElementById("h_orderTotal").value;
    if (amt > 0) {
        setDiscount(amt, "$50 Minimum");
    } else {
        alert("Order total is > $50");
    }
}
function freeOrderDiscount() {
    setDiscount(-document.getElementById("h_orderTotal").value, "Free Order");
}

function homeDelivery() {
    setDiscount(document.getElementById("h_orderTotal").value * .1, "10% Home Delivery Surcharge");
}
