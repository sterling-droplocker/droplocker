$(document).ready(function(){
	var map;
	var markers = new Array();
	var points = new Array();
	var routePath;
	var colors = new Array('cc0000','ccff00','3698fb','6ed0f7','FF1493','87b93e','CD69C9','CD5C5C','FFA500','9370DB','90EE90','CAFF70','EE7621','FF1493','FFD39B','FFFF00');	var markerstr = '';
	var lines = new Array();
	var driver_notes;
	var route_arr;
	var type;
	var d;
	var currentRoutes = new Array();
	//routes = '';
	$.each(routes,function(i,item){
		currentRoutes.push(parseInt(item));
	})
	
	display_map();
	
	// Starts here with a selection of one or more routes
	$('.routechk').change(function(){
		currentRoutes = new Array();
		$(this + "[checked]").each(function(i,item){
			if(!isNaN($(item).val()))
			currentRoutes.push(parseInt($(item).val()));
		})
		
       	update_map();
	});
	
	//gets the orders for each route
	function get_orders(){
		$('#driver_notes').html('Loading Order Data... <img src="/images/progress.gif" />');
		$('#wf_orders').html('');
		$('#dc_orders').html('');
		$.ajax({
	          type: 'POST',
	          data: "routes="+route_arr.toString(),
	          url: "/admin/reports/dm_get_route_information",
	          success: function(data){
	          	driver_notes = data.notes;
				update_driver_notes();
				display_orders('wf',data);
				display_orders('dc',data);
	          },
	          dataType: 'json'
	        });
	}
	
	//sortable list
	function summary(){
		var summaryPanel = document.getElementById("sortable");
	      summaryPanel.innerHTML = "";
	      for (var i = 0; i < points.length; i++) {
	        var routeSegment = i+1;
	        summaryPanel.innerHTML += "<li id='item_"+i+"'><a href='javascript:;'><table cellspacing='2' cellpadding='2' width='100%' style='margin:1px;padding:2px;outline:1px solid #CCC;background-color:#"+colors[points[i].route_id]+"'><tr><td align='center' width='30' style='font-size:18px;'>"+routeSegment+"</td><td style='padding:-5px;margin:-5px' valign='top'>"+points[i].street+"</td><td align='center' width='20'><a href='javascript:;' class='del_route_button' id='del"+points[i].drop_id+"'><img src='/images/icons/delete.png' /></td></tr></table></a></li>";
	      }
	}
	
	var wf = new Array();
	var dc = new Array();
	var pickup = new Array();
	$('#sortable').sortable({
		start: function(event,ui){
			$(ui.item).addClass('shadow');
			$('.wf').each(function(i,item){
				var id = $(item).attr('id');
				wf.push({
					id:$(item).attr('id'),
					val:$('#'+id).html(),
				})
			});
			$('.dc').each(function(i,item){
				var id = $(item).attr('id');
				dc.push({
					id:$(item).attr('id'),
					val:$('#'+id).html(),
				})
			});
			$('.p').each(function(i,item){
				var id = $(item).attr('id');
				pickup.push({
					id:$(item).attr('id'),
					val:$('#'+id).html(),
				})
			});
		},
		update: function(event,ui){
			$(ui.item).removeClass('shadow');
			order = ($('#sortable').sortable('toArray'));
			var a = new Array();
			lines = new Array();
			
			clear_markers();
			var bounds = new google.maps.LatLngBounds();
				$.each(order,function(i,items){
				  var item = items.replace('item_','');
				  lines.push(points[item].location);
				  var point = points[item];
				  add_marker(point,i);
				  a.push(point);
				});
				
				 points = new Array();
				 points = a;
				 summary();

			route();
			pointorder = order;
			display_listview_locations();
			
			$.each(wf,function(i,item){
				$('#'+item.id).html(item.val);
			});
			$.each(dc,function(i,item){
				$('#'+item.id).html(item.val);
			});
			$.each(pickup,function(i,item){
				$('#'+item.id).html(item.val);
			});
		}
	});
	
	$('.del_route_button').live('click',function(){
		alert($(this).attr('id'));
	});
	
	//adds lines between markers
	function route(){
	    lines = [];
	    for(var key in points) {
	        lines.push(points[key].location);
	    }
	    if(routePath) {
	        routePath.setPath(lines);
	    } else {
	    	
	        routePath = new google.maps.Polyline({
	            path: lines,
	            strokeColor: "#FF0000",
	            strokeOpacity: 1.0,
	            strokeWeight: 3
	        })
	        routePath.setMap(map);
	    }
	}
	
	//opens a new window with a layout for printing
	$('#print').click(function(){
		
		if($(this).text()!='Print Route')
		return false;
		
		var m = '[';
		$.each(points, function(i,item){
			m += '{"sort_id":"'+i+'","drop_id":"'+item.drop_id+'","lat":"'+item.location.lat()+'","lng":"'+item.location.lng()+'","street":"'+item.street+'","city":"'+item.city+'","route_id":"'+item.route_id+'"},';
		});
		m += "]";
		m = m.replace(",]","]");
		var l = '[';
		$.each(lines, function(i,item){
			l += '{"lat":"'+item.lat()+'","lng":"'+item.lng()+'"},';
		});
		l += "]";
		l = l.replace(",]","]");
		
		//passed from the php page (md5 string)
		var unique = hash;
		var left_column = escape($('#left_column').html());
		var right_column = escape($('#right_column').html());
		
		$.ajax({
	          type: 'POST',
	          data: "ID="+unique+"&lines="+l+"&markers="+m+"&left_column="+left_column+"&right_column="+right_column,
	          url: "/admin/reports/dm_store_map",
	          success: function(data){
	          	if(data.status=='success'){
	          		window.open("http://"+domain+"/admin/reports/print_map/"+unique,'print_map','width=820,height=800');
	          	}
	          },
	          dataType: 'json'
	        });
		
	});
	
	function display_orders(type,data){
		var element = '';
		var label = '';
		//type=this.type;
		if(type=='wf'){
		element = 'wf_orders';
		label = "Wash & Fold Orders";
		d = data.wf_orders;
		}
		else{
		element = 'dc_orders';
		label = 'Dry Clean Orders';
		d = data.dc_orders;
		}
		
		$('#'+element).html('');
		var table = $('<table class="driver_notes"></table>').append($('<tr>').append("<th>Location</th><th>Customer</th><th>Bag</th><th>Lbs</th><th>R</th><th>$</th><th></th>"));
		$.each(d,function(a,dl){
			$.each(dl, function(b,order){
				if(!isNaN(b))
				table.append($('<tr>').append("<td>"+order.street+"</td><td>"+order.firstName+" "+order.lastName+"</td><td>"+order.bagNumber+"</td><td>"+order.qty+"</td><td>"+order.route_id+"</td><td>"+order.order_details+"</td><td>"+order.loaded+"</td>"));
				else
				$('#'+type+a).html(dl.count);
			});
		})
		$('#'+element).append('<h3>'+label+'</h3>').append(table);
		
		$.each(data.claims,function(i,item){
			$('#p'+item.dropLocation_id).html('P');
		});
		
		$('#print').removeClass('white').addClass('orange').text('Print Route');
		
	}
	
	function display_listview_locations(){
		$('#listview').html('');
		var table = $('<table class="driver_notes" width="100%"></table>').append($('<tr>').append("<th>ID</th><th>Code</th><th>Route Order</th><th>Address</th><th>WF</th><th>DC</th><th></th>"));
		$.each(points,function(i,point){
			table.append($('<tr id="did'+point.drop_id+'">').append("<td>"+point.drop_id+"</td><td>"+point.accessCode+"</td><td>"+(i+1)+"</td><td>"+point.street+"</td><td><span class='wf' id='wf"+point.drop_id+"'></span></td><td><span class='dc' id='dc"+point.drop_id+"'></span></td><td><span class='p' id='p"+point.drop_id+"'></td>"));
		})
		$('#listview').append('<h3>Route Information</h3>').append(table);
	}
	
	
	function update_driver_notes(){
		$('#driver_notes').html('');
		var table = $('<table class="driver_notes" width="100%"></table>').append($('<tr>').append("<th>Address</th><th>Note</th>"));
		$.each(driver_notes,function(i,note){
			table.append($('<tr>').append("<td>"+note.street+"</td><td>"+note.note+"</td>"));
			$("#did"+note.dropId).find('td').css('background-color',"#ccc");
		})
		$('#driver_notes').append('<h3>Driver Notes</h3>').append(table);
	}
	
	/*** Standard Mapping Functions Below ***/
	var bounds;
	
	function update_map(){
		
		$.each(locations.routes,function(a,route){
	        	//alert(currentRoutes.indexOf(parseInt(a)));
	        	if(currentRoutes.indexOf(parseInt(a))>-1){
		        	$.each(route.dropLocations,function(b,location){
		        		add_marker(location.dropLocationAddress,a,false);
		        	});
	        	}
	    
	  	map.fitBounds(bounds);
		});
	}
	
	function display_map(){
		bounds = new google.maps.LatLngBounds();
		var latlng = new google.maps.LatLng(37.780929, -122.413582);
	       var myOptions = {
	          zoom: 13,
	          center: latlng,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	        
	        //$.each(currentRoutes,function(i,item){alert(item)});
	        
	        $.each(locations.routes,function(a,route){
	        	//alert(currentRoutes.indexOf(parseInt(a)));
	        	if(currentRoutes.indexOf(parseInt(a))>-1){
		        	$.each(route.dropLocations,function(b,location){
		        		add_marker(location.dropLocationAddress,a,false);
		        	});
	        	}
	        	 
	   					//points.push({
	   					//	id:i,
	   					//	route_id:item.route_id,
				        //  	location:latlng,
				        // 	street:item.street,
				        //  	city:item.city,
				        //  	accessCode:item.accessCode,
				        //  	drop_id:item.id
				      	//	});	
				      	//add_marker(points[i], i, false);
	        });
	        
	}
	
	function add_marker(point,route_id,icon){
		var count = 0;
		var latlng = new google.maps.LatLng(point.lat, point.lon);
		if(icon){
			var mark = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+(count+1)+'|'+colors[route_id]+'|000000';}
			else{
			var mark = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+route_id+'|'+colors[route_id]+'|000000';
			}
		var marker = new google.maps.Marker({
	      position: latlng, 
	      map: map, 
	      title:point.street,
	      icon: mark
	  });   
	  bounds.extend(latlng);
	  markers.push(marker);
	}
	
	function clear_markers(){
		for(var i=0; i < markers.length; i++){
	        markers[i].setMap(null);  
	    }
	    markers = new Array();
	}
	
	
});


