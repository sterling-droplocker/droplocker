$(document).ready(function(){
   $('#add_button').click(function(){
       $('#add_button').css('display','none');
       $('#locker_form').slideDown('fast');
   });

    $('#cancel').click(function(){
       $('#add_button').css('display','inline');
       $('#locker_form').slideUp('fast');
   });

   var active = true;
   $('#insert_button').click(function(){
        if ($("#locker_type").val() == 7 && $("#lock_type").val() != 2) //Note, "7" is expected to be the in Process locker style and "2" is expected to be the Electronic locker type.
        {
            alert("The In Process locker style must be set to the electronic lock type.");
            return false;
        }
        $('#insert_button').removeClass('orange').addClass('white').html('<span><img src="/images/progress.gif" /></span> Working...');
        if(!active){
            return;
        }
        active = false;
        var end = ($('#number_end').val()==null)?0:$('#number_end').val();
         $.ajax({
          type: 'POST',
          data: "locker_type="+$('#locker_type').val()+"&lock_type="+$('#lock_type').val()+"&location_id="+$('#location_id').val()+"&start="+$('#number_start').val()+"&end="+end+"&prefix="+$('#number_prefix').val(),
          url: "/admin/locations/add_locker_ajax/",
          success: function(data){
              if(data.status != ''){
                  top.location.href="/admin/locations/update_lockers/"+$('#location_id').val();
                  return false;
              }
              $('#insert_button').removeClass('white').addClass('orange').html('Add Locker');
              active = true;
          },
          dataType: 'json'
        });

   });

   $('#check_all').change(function(){
   	var status = $('#check_all').prop('checked');
   	var $checkbox = $('.mass_delete');
   	$checkbox.prop('checked', status);

   });


      /**
    * Delete Rows
    */

   $('#mass_delete_button').click(function(){
   	if(!verify_delete()){
   		return false;
   	}
   	var str='';
   	var rows = [];
   		$boxs = $('.mass_delete').each(function(i,item){
   			if($(item).is(':checked')){
   				var id = $(item).attr('id');
   				id = id.split("-");
   				rows.push($('#row-'+id[1]));
   				str += id[1]+",";
   			}
   		});

   		$.ajax({
	          type: 'POST',
	          data: "batch_lockerIDs="+str,
	          url: "/admin/locations/delete_locker_ajax/",
	          success: function(data){
	              if(data.status == 'success'){
	              	top.location.href= '/admin/locations/update_lockers/'+$('#location_id').val();
	              }
	              else{
	              	alert("Error: "+data.message);
	              }

	          },
	          dataType: 'json'
	        });


   });

   /**
    * Editable regions
    */

   var ddstyle = $('<select id="style"  name="lockerStyle_id"></select>');
   $.each(lockerTypes,function(i,item){
		ddstyle.append("<option value='"+item.lockerStyleID+"'>"+item.name+"</option>");
   });

   var ddlock = $('<select id="lock"  name="lockerLockType_id"></select>');
   $.each(lockTypes,function(i,item){
		ddlock.append("<option value='"+item.lockerLockTypeID+"'>"+item.name+"</option>");
   });

   var ddstatus = $('<select id="status"  name="lockerStatus_id"></select>');
   $.each(lockerStatus,function(i,item){
		ddstatus.append("<option value='"+item.lockerStatusID+"'>"+item.name+"</option>");
   });


   // This is called when the user clicks on a text field to edit
	var clickspan;
	var span;
	var text;
	var input;
	var lockerid;
	var field;
	$('.click').live('click',function(){
		var sid = $(this).attr('id');
		sid = sid.split("-");
		if(span){
			span.replaceWith(text);
			clickspan.addClass('click');
		}

		lockerid = sid[1];
		field = sid[0];
		clickspan = $(this).removeClass('click');
		clickspan.unbind();
		text = $(this).text();
		span = $("<span></span>");

		//set up a text input or a dropdown
		if($(this).hasClass('text')){
			input = $("<input style='width:20%' type='text' name='lockerName' />").val(text);
		}
		else{
			if(field=='lockerStatus_id') {
                input = ddstatus;
			} else if(field=='lockerStyle_id') {
                input = ddstyle;
			} else if(field=='lockerLockType_id') {
                input = ddlock;
            }
		}

		var accept = $("<a class='save action' href='javascript:;'><img src='"+cdnimages+"icons/accept.png' /></a>");
		var cancel = $("<a class='revert action' href='javascript:;'><img src='"+cdnimages+"icons/cancel.png' /></a>");
		$(this).html(span.append(input).append($('<span id="btngrp"></span>')).append(accept).append(cancel));

	});

	//The little icon accept button to save an edited product
	$('.save').live('click',function(){
		$('#btngrp').html("<img src='"+cdnimages+"progress.gif' />");
		$('.action').css('display','none');
		value = input.val();
		$.ajax({
          type: 'POST',
          data: "lockerID="+lockerid+"&"+field+"="+value,
          url: "/admin/locations/update_locker_ajax/",
          success: function(data) {
              if(data.status == 'success'){

              	if(clickspan.hasClass('text')){
              		span.replaceWith(value);
              	}
              	else{
              		var selectid = input.attr('id');
              		span.replaceWith($("#"+selectid+" option:selected").text());
              	}
                clickspan.addClass('click');

                if (field == 'lockerStatus_id') {
                    var row = clickspan.closest('tr').remove();
                    if (value == 1) {
                        $('.enable_button', row).hide();
                        $('.delete_button', row).show();
                        $('#active_lockers tbody').append(row);
                    } else {
                        $('.enable_button', row).show();
                        $('.delete_button', row).hide();
                        $('#inactive_lockers tbody').append(row);
                    }
                }

                return false;
              }
              else{
              	$('#btngrp').html("");
				$('.action').css('display','inline');
              	alert("Error: "+data.message);
              }

          },
          dataType: 'json'
        });


	});

	//cancels the edit
	$('.revert').live('click',function(){

		if(span){
			span.replaceWith(text);
			clickspan.addClass('click');
		}
	});



   $('.del_button').click(function(){

		if(verify_delete()){
			var id = $(this).attr('id');
			id = id.split("-");
			$.ajax({
	          type: 'POST',
	          data: "lockerID="+id[1],
	          url: "/admin/locations/delete_locker_ajax/",
	          success: function(data){
	              if(data.status == 'success'){
	              	 //$("#row-"+id[1]).css('display', 'none');
	            	  top.location.href='/admin/locations/update_lockers/'+$('#location_id').val();
	              }
	              else{
	              	alert("Error: "+data.message);
	              }

	          },
	          dataType: 'json'
	        });
		}
	});
});
