$(document).ready(function(){
   var latitude;
   var logitude;
   var name;
   var address;
   var address2;
   var city;
   var state;
   var zipcode;
   var locationtypeID;
   var marker;
   var is_public;
    
    //Example: http://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false
    var active = true;
    $('#show_button').click(function(){
        
        name = $('#name').val();
        address = $('#address').val();
        address2 = $('#address2').val();
        city = $('#city').val();
        state = $('#state').val();
        zipcode = $('#zipcode').val();
        locationTypeID = $('#locationTypeID').val();
        
        if(address==''){
        	alert('Address is blank');
        	return false;
        }
        
        if(city==''){
        	alert('City is blank');
        	return false;
        }
        
        if(state==''){
        	alert('Please select a state');
        	return false;
        }
        
        $('#show_button').removeClass('orange').addClass('white').html('<span><img src="/images/progress.gif" /></span> Working...');
        if(!active){
            return;
        }
        active = false;
        
        $.ajax({
          type: 'POST',
          data: "address="+address+"&city="+city+"&state="+state+"&zipcode"+zipcode,
          url: "/ajax/google_geocode/",
          success: function(data){
              
              if(data.status=='OK'){
                  var loc = data.results[0].geometry.location;
                  var zip_comp = data.results[0].address_components;
                  
                  //if user has not entered zipcode, we try to set it from the data returned from google geocode
                  $.each(zip_comp, function(i,item){
                  	if(item.types == 'postal_code'){
                  		if(item.long_name !='' && $('#zipcode').val()==''){
                  			$('#zipcode').val(item.long_name);
                  		}
                  	}
                  });
                  
                  longitude = loc.lng;
                  latitude = loc.lat;
                  $('#glat').html(latitude);
                  $('#glng').html(longitude);
                  $('#map_div').css('display', 'block');
                  
                   var latlng = new google.maps.LatLng(latitude, longitude);
                   var myOptions = {
                      zoom: 20,
                      center: latlng,
                      mapTypeId: 'satellite'
                    };
                    var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
                    
                    marker = new google.maps.Marker({
                          position: latlng, 
                          map: map, 
                          draggable:true,
                          title:"test"
                      });  
                     google.maps.event.addListener(marker, 'dragend', function(){
                        new_loc = marker.getPosition();
                        $('#lat').val(new_loc.lat());
                        $('#lon').val(new_loc.lng());
                     });
              }
              
              $('#show_button').removeClass('white').addClass('orange').html('Show on Map');
              active = true;
          },
          dataType: 'json'
        });
    });
    
    // Saves the location
    $('#save_button').click(function(){
        
        $('#save_button').removeClass('orange').addClass('white').html('<span><img src="/images/progress.gif" /></span> Working...');
        if(!active){
            return;
        }
        active = false;
        
        var glat = $('#glat').html();
        var glng = $('#glng').html();
        name = $('#name').val();
        address = $('#address').val();
        address2 = $('#address2').val();
        city = $('#city').val();
        state = $('#state').val();
        zipcode = $('#zipcode').val();
        locationTypeID = $('#locationTypeID').val();
        is_public = $('#public').val(); 
        serviceType = $('#service_type').val();
        
        $.post(
          "/admin/locations/add_ajax",
            {        
                "submit" : true,
                "serviceType" : serviceType,
                "dc_min" : $('#dc_min').val(),
                "wf_min" : $('#wf_min').val(),
                "public" : is_public,
                "address" : address,
                "locationTypeID" : locationTypeID,
                "address2" : address2,
                "city" : city,
                "state" : state,
                "zipcode" : zipcode,
                "route_id" : $("#route_id").val(),
                "sortOrder" : $("#sortOrder").val(),
                "latitude" : glat,
                "longitude" : glng,
                "residentManager" : $("#residentManager").val(),
                "highTarget" : $("#highTarget").val(),
                "lowTarget" : $("#lowTarget").val(),
                "notes" : $("#notes").val(),
                "commission" : $("#commission").val(),
                "commissionFrequency" : $("#commissionFrequency").val(),
                "description" : $("#description").val(),
                "hours" : $("#hours").val(),
                "accessCode" : $("#accessCode").val(),
                "launchDate" : $("#launchDate").val(),
                "units" : $("#units").val(),
                "quarter" : $("#quarter").val()
          },
          function(data){
                if(data.status=='success'){
                    top.location.href='/admin/locations/view';   
                }
                else
                {
                    alert(data.message);
                    $('#save_button').removeClass('white').addClass('orange').html('Show on Map');
                    active = true;
                }
              
          },
          'json'
        );
    });
    
    
    // updates the location information
     $('#update_button').click(function(){
        
        $('#save_button').removeClass('orange').addClass('white').html('<span><img src="/images/progress.gif" /></span> Working...');
        if(!active){
            return;
        }
        active = false;
        
        var glat = $('#glat').html();
        var glng = $('#glng').html();
        name = $('#name').val();
        address = $('#address').val();
        address2 = $('#address2').val();
        city = $('#city').val();
        state = $('#state').val();
        zipcode = $('#zipcode').val();
        locationTypeID = $('#locationTypeID').val();
        is_public = $("#public").val();
        serviceType = $('#service_type').val();
        dc_min = $('#dc_min').val();
        wf_min = $('#wf_min').val();
        
        $.ajax({
          type: 'POST',
          data: {"location_id":$('#location_id').val(),
            "locationTypeID":locationTypeID,
            "serviceType":serviceType,
            "public":is_public,
            "submit":true,
            "address":address,
            "address2":address2,
            "city":city,
            "state":state,
            "zipcode":zipcode,
            "latitude":glat,
            'longitude':glng,
            'dc_min': dc_min,
            'wf_min': wf_min,
            "residentManager" : $("#residentManager").val(),
            "highTarget" : $("#highTarget").val(),
            "lowTarget" : $("#lowTarget").val(),
            "notes" : $("#notes").val(),
            "commission" : $("#commission").val(),
            "commissionFrequency": $("#commissionFrequency").val(),
            "description" : $("#description").val(),
            "hours" : $("#hours").val(),
            "accessCode" : $("#accessCode").val(),
            "launchDate" : $("#launchDate").val(),
            "units" : $("#units").val(),
            "quarter" : $("#quarter").val(),
            "masterRoute_id" : $("#masterRoute_id").val(),
            "masterSortOrder" : $("#masterSortOrder").val()
            
        },
          url: "/admin/locations/edit_ajax",
          success: function(data){
              if(data.status=='success'){
               top.location.href='/admin/admin/location_edit/'+$('#location_id').val();   
              }else
              {
                alert(data.message);
                $('#save_button').removeClass('white').addClass('orange').html('Show on Map');
                active = true;
              }
              
          },
          dataType: 'json'
        });
    });
    
  
});