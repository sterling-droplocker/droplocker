// The following code sets up an event handler for sending an error report to the developers if any Javascript errors occur.
try 
{
    xhr = new ActiveXObject("Microsoft.XMLHTTP");   // Trying IE
}
catch(e)    // Failed, use standard object
{
    xhr = new XMLHttpRequest();
}    
/**
window.onerror = function(message, url, lineNumber)
{
    if (url === "")
        {
            url = document.URL;
        }
    var parameters = "message=" + message + "&url=" + url + "&lineNumber=" + lineNumber + "&browser=" + navigator.userAgent;
    var post_url = "/utility/report_javascript_error";

    xhr.open("POST", post_url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("Content-length", parameters.length);
    xhr.setRequestHeader("Connection", "close");
    xhr.send(parameters);
}
*/