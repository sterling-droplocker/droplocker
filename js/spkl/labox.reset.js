// JavaScript Document
$(function(){
	var header = '\<header id="header">\
	<div class="wrap">\
	<h1 id="siteName"><a href="https://locker.spkl.co.jp/tes_index.html"><img src="/images/spkl/logoType.svg" alt="24時間クリーニング受け渡しサービス - LABOX（ラボックス）-"></a></h1>\
	<ul id="hNav">\
	<li><a href="https://myaccount.locker.spkl.co.jp/register"><i><svg><use xlink:href="#iconEdit"></use></svg></i>新規登録</a></li>\
	<li><a href="https://myaccount.locker.spkl.co.jp/account"><i><svg><use xlink:href="#iconMypage"></use></svg></i>マイページ</a></li>\
	</ul>\
	<div id="navButton">\
	<i></i><i></i><i></i>\
	<span>menu</span>\
	</div>\
	</div>\
	</header>\
	<nav id="gNav">\
	<div class="wrap">\
	<ul>\
	<li><a href="https://locker.spkl.co.jp/tes_index.html"><i><svg><use xlink:href="#iconChevronCircleRight"></use></svg></i>ホーム</a></li>\
	<li><a href="https://locker.spkl.co.jp/howItWorks/index.html"><i><svg><use xlink:href="#iconChevronCircleRight"></use></svg></i>利用方法</a></li>\
	<li><a href="https://locker.spkl.co.jp/pricing/index.html"><i><svg><use xlink:href="#iconChevronCircleRight"></use></svg></i>料金</a></li>\
	<li><a href="https://locker.spkl.co.jp/locations/index.html"><i><svg><use xlink:href="#iconChevronCircleRight"></use></svg></i>場所</a></li>\
	<li><a href="https://locker.spkl.co.jp/faqs/index.html"><i><svg><use xlink:href="#iconChevronCircleRight"></use></svg></i>よくある質問</a></li>\
	<li><a href="https://locker.spkl.co.jp/contact/index.html"><i><svg><use xlink:href="#iconChevronCircleRight"></use></svg></i>お問合せ</a></li>\
	</ul>\
	</div>\
	</nav>';


	var footer = '\<div id="toTop">\
	<a href="#pageTop"><i><svg><use xlink:href="#iconTotop"></use></svg></i></a>\
	</div>\
	<footer id="footer">\
	<div id="fNav" class="wrap">\
	<ul>\
	<li><a href="https://locker.spkl.co.jp/topics/index.html"><i><svg><use xlink:href="#iconTopics"></use></svg></i>お知らせ</a></li>\
	<li><a href="https://myaccount.locker.spkl.co.jp/account/main/terms"><i><svg><use xlink:href="#iconTerms"></use></svg></i>利用規約</a></li>\
	<li><a href="https://locker.spkl.co.jp/company/law.html"><i><svg><use xlink:href="#iconChevronRight"></use></svg></i>特定商取引法に基づく表記</a></li>\
	<li><a href="https://locker.spkl.co.jp/company/privacy.html"><i><svg><use xlink:href="#iconChevronRight"></use></svg></i>プライバシーポリシー</a></li>\
	</ul>\
	</div>\
	<div id="copyright">\
	<div class="wrap">\
	<ul>\
	<li><a href="http://www.spkl.co.jp/" target="_blank">株式会社スパークル<span><svg><use xlink:href="#iconPopup"></use></svg></span></a></li>\
	</ul>\
	<p>Copyright © Sparkle CO.,Ltd. All Rights Reserved.</p>\
	</div>\
	</div>\
	</footer>\
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" style="display: none;">\
	<symbol id="iconEdit" viewBox="0 0 24 24">\
	<path d="M20.719 7.031l-1.828 1.828-3.75-3.75 1.828-1.828c0.375-0.375 1.031-0.375 1.406 0l2.344 2.344c0.375 0.375 0.375 1.031 0 1.406zM3 17.25l11.063-11.063 3.75 3.75-11.063 11.063h-3.75v-3.75z"></path>\
	</symbol>\
	<symbol id="iconMypage" viewBox="0 0 24 24">\
	<path d="M12 14.016c2.672 0 8.016 1.313 8.016 3.984v2.016h-16.031v-2.016c0-2.672 5.344-3.984 8.016-3.984zM12 12c-2.203 0-3.984-1.781-3.984-3.984s1.781-4.031 3.984-4.031 3.984 1.828 3.984 4.031-1.781 3.984-3.984 3.984z"></path>\
	</symbol>\
	<symbol id="iconChevronCircleRight" viewBox="0 0 20 20">\
	<path d="M11 10l-2.302-2.506c-0.196-0.198-0.196-0.519 0-0.718 0.196-0.197 0.515-0.197 0.71 0l2.807 2.864c0.196 0.199 0.196 0.52 0 0.717l-2.807 2.864c-0.195 0.199-0.514 0.198-0.71 0-0.196-0.197-0.196-0.518 0-0.717l2.302-2.504zM10 0.4c5.302 0 9.6 4.298 9.6 9.6 0 5.303-4.298 9.6-9.6 9.6s-9.6-4.297-9.6-9.6c0-5.302 4.298-9.6 9.6-9.6zM10 18.354c4.613 0 8.354-3.74 8.354-8.354s-3.741-8.354-8.354-8.354c-4.615 0-8.354 3.74-8.354 8.354s3.739 8.354 8.354 8.354z"></path>\
	</symbol>\
	<symbol id="iconChevronRight" viewBox="0 0 20 20">\
	<path d="M9.163 4.516c0.418 0.408 4.502 4.695 4.502 4.695 0.223 0.219 0.335 0.504 0.335 0.789s-0.112 0.57-0.335 0.787c0 0-4.084 4.289-4.502 4.695-0.418 0.408-1.17 0.436-1.615 0-0.446-0.434-0.481-1.041 0-1.574l3.747-3.908-3.747-3.908c-0.481-0.533-0.446-1.141 0-1.576s1.197-0.409 1.615 0z"></path>\
	</symbol>\
	<symbol id="iconTotop" viewBox="0 0 20 20">\
	<path d="M2.582 13.891c-0.272 0.268-0.709 0.268-0.979 0s-0.271-0.701 0-0.969l7.908-7.83c0.27-0.268 0.707-0.268 0.979 0l7.908 7.83c0.27 0.268 0.27 0.701 0 0.969s-0.709 0.268-0.978 0l-7.42-7.141-7.418 7.141z"></path>\
	</symbol>\
	<symbol id="iconTopics" viewBox="0 0 24 24">\
	<path d="M14.391 6h5.625v9.984h-7.031l-0.375-1.969h-5.625v6.984h-1.969v-17.016h9z"></path>\
	</symbol>\
	<symbol id="iconCompany" viewBox="0 0 24 24">\
	<path d="M15.984 12v-5.016h-0.984v2.016h-0.984v-2.016h-1.031v3h2.016v2.016h0.984zM11.016 9.984v-3h-3v1.031h1.969v0.984h-1.969v3h3v-0.984h-2.016v-1.031h2.016zM18.984 6.984h3v13.031h-7.969v-4.031h-4.031v4.031h-7.969v-13.031h3v-3h13.969v3z"></path>\
	</symbol>\
	<symbol id="iconTerms" viewBox="0 0 24 24">\
	<path d="M12.984 14.016v-4.031h-1.969v4.031h1.969zM12.984 18v-2.016h-1.969v2.016h1.969zM0.984 21l11.016-18.984 11.016 18.984h-22.031z"></path>\
	</symbol>\
	<symbol id="iconPopup" viewBox="0 0 20 20">\
	<path d="M16 18h-8.021c-1.099 0-1.979-0.88-1.979-1.98v-8.020c0-1.1 0.9-2 2-2h8c1.1 0 2 0.9 2 2v8c0 1.1-0.9 2-2 2zM16 8h-8v8h8v-8zM4 10h-2v-6c0-1.1 0.9-2 2-2h6v2h-6v6z"></path>\
	</symbol>\
	</svg>';
	
//	いらないの削除	
	//	スマホフッタ
//	$('div.visible-phone').remove();
//	$('div.hidden-phone').remove();
	//	PCフッタ
	$("body div").first().remove();
	$("body div").last().remove();
	$("#footer").remove();
	$("body div").last().remove();
	$('body *').removeAttr('style');

	$('body').prepend(header); 
	$('body').append(footer);
	$('body').attr('id','pageTop');

	//$('body').wrapInner('<div id="wrapper"></div>');
	$('#gNav+div').attr('id','main');
});
