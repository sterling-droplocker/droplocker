// JavaScript Document
$(document).ready(function(){
	$('a[href^="#"]').on("click", function(){
		var speed = 250;
		var href = $(this).attr("href");
		var target = $(href == "#" || href == "" ? "html" : href);
		var offset = target.offset().top;
		$("body,html").animate({
				scrollTop:offset
			}, speed, "swing");
		return false;
	});
});


// アニメーションの実行等
$(window).on('load',function(){
	$('body').addClass('done');
});


//ハンバーガーメニュー
$(function(){
	var nav = $('#gNav');
	var navButton = $('#navButton');
	navButton.click(function(){
		$(window).on('touchmove.noScroll', function(e) {
			e.preventDefault();
		});
		$("body").toggleClass("nav");
	});
	navButton.click(function(){
		if(navButton.hasClass("")){
			$(window).off('.noScroll');
		}
	});
	nav.on("click", function(){
		if($("body").hasClass("nav")){
			navButton.click();
		}
		$("body").removeClass("nav");
	});

	$('body').on('touchstart', onTouchStart); //指が触れたか検知
	$('body').on('touchmove', onTouchMove); //指が動いたか検知
	$('body').on('touchend', onTouchEnd); //指が離れたか検知
	var direction, position;

	//スワイプ開始時の横方向の座標を格納
	function onTouchStart(event) {
		position = getPosition(event);
		direction = ''; //一度リセットする
	}

	//スワイプの方向（left／right）を取得
	function onTouchMove(event) {
		if (position - getPosition(event) > 70) { // 70px以上移動しなければスワイプと判断しない
			direction = 'left'; //左と検知
		} else if (position - getPosition(event) < -70){  // 70px以上移動しなければスワイプと判断しない
			direction = 'right'; //右と検知
		}
	}

	function onTouchEnd(event) {
		if (direction == 'right'){
			//alert('右だよ');
			$('body').removeClass('nav');
		} else if (direction == 'left'){
			//alert('左だよ');
			//$('body').addClass('nav');
		}
	}

	//横方向の座標を取得
	function getPosition(event) {
		return event.originalEvent.touches[0].pageX;
	}
});


//画像サムネイル化
$(function(){
	$('.thumb').each(function(){
		var list = $(this);
		if($(this).hasClass('lazyload')){
			var userAgent = window.navigator.userAgent.toLowerCase();
			if(userAgent.indexOf('msie') >= 0 || userAgent.indexOf('trident') >= 0) {
				var srcImg = list.data('src');
				list.css('opacity','0');
				list.wrap('<div class="lazyload thumbBox" data-bg="'+srcImg+'"></div>');
			} else {
				list.wrap('<div class="thumbBox"></div>');
			}
		} else{
			var userAgent = window.navigator.userAgent.toLowerCase();
			if(userAgent.indexOf('msie') >= 0 || userAgent.indexOf('trident') >= 0) {
				var srcImg = list.attr('src');
				list.css('opacity','0');
				list.wrap('<div class="thumbBox" style="background-image:url('+srcImg+');"></div>');
			} else {
				list.wrap('<div class="thumbBox"></div>');
			}
		}
	});
});


//TELリンク無効
var ua = navigator.userAgent.toLowerCase();
var isMobile = /iphone/.test(ua)||/android(.+)?mobile/.test(ua);
if (!isMobile) {
	$('a[href^="tel:"]').on('click', function(e) {
		e.preventDefault();
	});
}


//svgアイコンセット読み込み
$.ajax({
	type: 'get',
		url: window.location.origin+'/images/spkl/objectSet.svg'
	}).done(function(data) {
	var svg = $(data).find('svg');
		$('body').append(svg);
});


//svgスプライト表示バグ対策
(function(document, window) {
	"use strict";
	document.addEventListener("DOMContentLoaded", function() {
		var baseUrl = window.location.href
			.replace(window.location.hash, "");
		[].slice.call(document.querySelectorAll("use[*|href]"))
			.filter(function(element) {
				return (element.getAttribute("xlink:href").indexOf("#") === 0);
			})
			.forEach(function(element) {
				element.setAttribute("xlink:href", baseUrl + element.getAttribute("xlink:href"));
			});
	}, false);
}(document, window));

