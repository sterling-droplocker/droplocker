/**
 * THIS FILE HOLDS ALL THE LAYOUTS COMPONENTS FOR DESKTOP DEVICES
 */

;(function( $ ) {
	
	/**
	 * PRIVATE METHODS
	 */
	
	/**
	 * @description: the method to execute on ready inside the editor
	 * @param event
	 * @type: private
	 */
	function onReadyEditorMode(event) {
		initAfterAjax();
		for (var key in $.layoutDevice.components) {
			if($.layoutDevice.components[key]) {
				$.layoutDevice.components[key].onReadyEditorMode();
			}
		}
		$.layoutManager.markCurrentSelectedNavigation();
        addParallaxBehavior();
//		closeAllOpenNavs();
	}
	
	/**
	 * @description: the method to execute on ready not inside the editor
	 * @param event
	 * @type: private
	 */
	function onReadyPreviewMode(event) {
		initAfterAjax();
		for (var key in $.layoutDevice.components) {
			if($.layoutDevice.components[key]) {
				$.layoutDevice.components[key].onReadyPreviewMode();
			}
		}
		$.layoutManager.markCurrentSelectedNavigation();
        addParallaxBehavior();
//		closeAllOpenNavs();
	}
	
	/**
	 * @description: the method to execute on load inside the editor
	 * @param event
	 * @type: private
	 */
	function onLoadEditorMode(event) {
		for (var key in $.layoutDevice.components) {
			if($.layoutDevice.components[key]) {
				$.layoutDevice.components[key].onLoadEditorMode();
			}
		}
		$.layoutManager.updateContainerMinimumHeight();
	}
	
	/**
	 * @description: the method to execute on load not inside the editor
	 * @param event
	 * @type: private
	 */
	function onLoadPreviewMode(event) {
		for (var key in $.layoutDevice.components) {
			if($.layoutDevice.components[key]) {
				$.layoutDevice.components[key].onLoadPreviewMode();
			}
		}
		$.layoutManager.updateContainerMinimumHeight();
	}
	
	/**
	 * @description: initialize the after ajax command for all the active components
	 */
	function initAfterAjax() {
		Parameters.AfterAjaxCommand = function(data) {
			for (var key in $.layoutDevice.components) {
				if($.layoutDevice.components[key] && $.layoutDevice.components[key].afterAjaxCommand) {
					$.layoutDevice.components[key].afterAjaxCommand(data);
				}
			}
			$.layoutManager.updateContainerMinimumHeight();
		};
	}

    /**
     * Add parallax on site ready
     */
    function addParallaxBehavior() {
        "use strict";

        var data, selector, //speed,
            dataContainer = $("#dm").find("[data-background-parallax-selector]");

        // If site has parallax settings
        if(dataContainer.length > 0 && !$.browser.msie) {
            // Get data
            data = dataContainer.data();
            selector = data["backgroundParallaxSelector"];
            //speed = data["backgroundParallaxSpeed"];

            // Call the `makeParallax` method
            $(selector).makeParallax(0.1);
        }
    }
	
	/**
	 * @description: closes all the open navs
	 */
	function closeAllOpenNavs() {
		if($.layoutDevice.components.slideDownNav) {		
			$.layoutDevice.components.slideDownNav.slideDownNavHandlerImpl(false, true);
		}
	}
	
	
	/**
	 * ************************************************ touch here with caution ************************************
	 */
	
	/**
	 * THIS API INTERFACE MUST BE IMPLEMENTED ON EVERY NEW DEVICE (MOBILE,TABLET,DESKTOP)
	 */
	// implement the common api to have all the needed methods
	$.extend({layoutDevice : $.extend(true, {}, layoutDeviceInterface)});
	
	// override the methods we need to override
	$.extend($.layoutDevice , {
		/**
		 * holds the device type parameter (mobile/tablet/desktop)
		 */
		type : 'desktop',
		
		/**
		 * holds all the layout supported components
		 */
		components : {

		},
		
		/**
		 * fires when the preview is ready
		 * @param editorMode - true if we run in editor mode
		 * @param event
		 */
		onReady : function(editorMode, event) {
			$("body").addClass("dmDesktopBody").addClass("dmLargeBody");
			if(editorMode) {
				onReadyEditorMode(event);
			}
			else {
				onReadyPreviewMode(event);
			}
		},
		
		/**
		 * fires when the preview window is loaded
		 * @param editorMode - true if we run in editor mode
		 * @param event
		 */
		onLoad : function(editorMode, event) {
			if(editorMode) {
				onLoadEditorMode(event);
			}
			else {
				onLoadPreviewMode(event);
			}
		},
		
		/**
		 * get the total height of fixed elements at the top
		 * @returns {Number}
		 */
		getTopFixedElementsOffset : function() {
			return 0;
		},
		
		/**
		 * get the total height of fixed elements at the bottom
		 * @returns {Number}
		 */
		getBottomFixedElementsOffset : function() {
			return 0;
		},
		
		/**
		 * initialize the inner bar component
		 */
		initInnerBar : function(data) {
			if($.DM.isCurrentHomePage()) {
				$("#innerBar").addClass("dmDisplay_None");
			}
			else {
				$("#innerBar").removeClass("dmDisplay_None");
			}
			
			$.layoutManager.initInnerPageTitle(data);
		},
		
		/**
		 * fires on each navigation click
		 * @param anchor - the anchor jquery element that was clicked
		 * @param event - the click event
		 */
		onAjaxLinkClick : function(anchor, event) {
			for (var key in $.layoutDevice.components) {
				if($.layoutDevice.components[key] && $.layoutDevice.components[key].onAjaxLinkClick) {
					$.layoutDevice.components[key].onAjaxLinkClick(anchor, event);
				}
			}
		}
		
	} );
	
	/**
	 * END OF COMMON DEVICES API
	 */
	
	/**
	 * DO NOT WRITE CODE BELLOW THIS
	 */
	
})( jQuery );