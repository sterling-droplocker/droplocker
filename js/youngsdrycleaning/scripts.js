/* ------------------------------------------------
---------------------------------------------------

    Laundry Locker's Main JavaScript Document
    Version: 1.0
    Created By: Create Online

---------------------------------------------------
--------------------------------------------------- */

/* ------------------------------------------------
    Line Animation
--------------------------------------------------- */

jQuery('hr.green').each(function() {
  jQuery('hr.green').addClass('wow pulse');
});
jQuery('hr.white').each(function() {
  jQuery('hr.white').addClass('wow pulse');
});

/* ------------------------------------------------
    Sticky Navigation
--------------------------------------------------- */

/*---- Sticky Nav's Global Variables ----*/

var headerHeight = 0,
  headerVisiblePos = 0,
  headerFixedPos = 0,
  isHeaderFixed = false,
  isHeaderVisible = false;

function stickyMenu() {
  if (jQuery('.navbar').hasClass('sticky')) {
    window.addEventListener('scroll', function(e) {
      var screenTop = jQuery(window).scrollTop();
      if (screenTop > 0) {
        jQuery('.navbar').addClass('shrink');
      }
      if (screenTop <= 0) {
        jQuery('.navbar').removeClass('shrink');
      }
    });
    var screenTop = jQuery(window).scrollTop();
    if (screenTop > 0) {
      jQuery('.navbar').addClass('shrink');
    }
    window.addEventListener('scroll', function(e) {
      var screenTop = jQuery(window).scrollTop();
      if (screenTop > 282) {
        jQuery('.pricing .navbar-tab').addClass('shrink-tab');
        jQuery('.pricing .one-page-area').addClass('shrink-tab-area');
      }
      if (screenTop <= 282) {
        jQuery('.pricing .navbar-tab').removeClass('shrink-tab');
        jQuery('.pricing .one-page-area').removeClass('shrink-tab-area');
      }
    });
    var screenTop = jQuery(window).scrollTop();
    if (screenTop > 282) {
      jQuery('.pricing .navbar-tab').addClass('shrink-tab');
      jQuery('.pricing .one-page-area').addClass('shrink-tab-area');
    }
    if (jQuery(window).width() < 768) {
		window.addEventListener('scroll', function(e) {
			var screenTop = jQuery(window).scrollTop();
			if (screenTop > 0) {
				jQuery('.pricing .navbar-tab').addClass('shrink-tab');
				jQuery('.pricing .one-page-area').addClass('shrink-tab-area');
			}
			if (screenTop <= 0) {
				jQuery('.pricing .navbar-tab').removeClass('shrink-tab');
				jQuery('.pricing .one-page-area').removeClass('shrink-tab-area');
			}
		});
		var screenTop = jQuery(window).scrollTop();
		if (screenTop > 0) {
			jQuery('.pricing .navbar-tab').addClass('shrink-tab');
			jQuery('.pricing .one-page-area').addClass('shrink-tab-area');
		}
    }
  }
}

/* ------------------------------------------------

	Promo Bar

--------------------------------------------------- */

jQuery('.promo-bar a.promo-close').click(function(e) {
  e.preventDefault();
  jQuery('.promo-bar').slideUp('slow', function() {
    stickyMenu();
  });
});

/* ------------------------------------------------

	Reponsive Scripts

--------------------------------------------------- */

function responsive(){
	if (screenWidth < 992) {
		var mobOffset = jQuery('.navbar-header').outerHeight() + 60;
		var calcHeight = screenHeight-mobOffset;
		jQuery('.navbar-collapse .nav.navbar-nav').attr('style','max-height:'+calcHeight+'px');
	}
}

/* ------------------------------------------------
    One Page Navigation
--------------------------------------------------- */

function navOnePage() {
  if (jQuery('body').hasClass('one-page')) {
    var offset = 0,
      delay = 0;
    var $sections = jQuery('.one-page-section');
    if (jQuery('.navbar').hasClass('sticky')) {
      offset = 0;
    }
    if (jQuery('.how-it-works .navbar-tab').hasClass('sticky')) {
      offset = 315;
    }
    if (jQuery('.pricing .navbar-tab').hasClass('sticky')) {
      offset = 250;
    }
    if (jQuery(window).width() < 992) {
      if (jQuery('.how-it-works .navbar').hasClass('sticky')) {
        offset = 455;
      }
      if (jQuery('.pricing .navbar').hasClass('sticky')) {
        offset = 425;
      }
    }
    if (jQuery(window).width() < 768) {
      if (jQuery('.how-it-works .navbar').hasClass('sticky')) {
        offset = 341;
      }
      if (jQuery('.pricing .navbar').hasClass('sticky')) {
        offset = 258;
      }
    }
    if (jQuery('body').find('.owl-carousel.one-page-section')) {
      delay = 800;
    } else {
      delay = 100;
    }
	window.setTimeout(function() {
		if (jQuery('body').hasClass('how-it-works')) {
			
		}
		else if (jQuery('body').hasClass('pricing')) {
			sectionOffset();
		}
      
    }, delay);
    function sectionOffset() {
      var currentScroll = jQuery(this).scrollTop() + offset;
      var $currentSection;
      $sections.each(function() {
        var divPosition = jQuery(this).offset().top;
        var divHeight = jQuery(this).outerHeight();
        var total = divPosition + divHeight;
        if (jQuery(window).scrollTop() + screenHeight >= jQuery(document).height() - offset) {
          $currentSection = $sections.last();
        } else if (divPosition - 1 < currentScroll) {
          $currentSection = jQuery(this);
        }
      });
      var id = $currentSection.attr('id');
      jQuery('.navbar .nav > li').removeClass('active');
      jQuery('[href=#' + id + ']')
        .parent('li')
        .addClass('active');
    }
    var timer;
    jQuery(window).scroll(function() {
      if (timer) {
		  if (jQuery('body').hasClass('pricing')) {
        sectionOffset();
		  }
      } else {
        timer = window.setTimeout(function() {
			if (jQuery('body').hasClass('pricing')) {
          sectionOffset();
			}
        }, 100);
      }
    });
    var scrollActive = '';
    jQuery('.navbar .nav li a[href*=#]:not([href=#])').click(function() {
      if (scrollActive == true) {
        event.preventDefault();
      } 
	  else if (jQuery(this).parent('li.active').length){
		   event.preventDefault();
	  }
	  else if (jQuery('body').hasClass('how-it-works')) {
		    if (screenWidth < 992) {
				var offsst = 150;
			}
			else {
				var offsst = 200;
			}
		    var target = jQuery(this.hash);
			  target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
			  if (target.length) {
				jQuery('html,body').animate(
				  {
					scrollTop: target.offset().top - offsst
				  },
				  1000,
				  'easeInQuart',
				  function() {
					scrollActive = false;
				  }
				);
				return false;
			  }
			}
			
		
	  else {
		if (jQuery(this).hasClass('section-link')) {
          var windowW = window.innerWidth;
          if (windowW >= 991) {
            var screenTop = jQuery(window).scrollTop();
            if (screenTop > 0) {
              var offset =
                jQuery('.navbar-tab').outerHeight() +
                jQuery('.navbar-static-top')
                  .addClass('sticky shrink')
                  .outerHeight();
            } else {
              var offset =
                jQuery('.navbar-tab').outerHeight() +
                jQuery('.navbar-static-top').outerHeight();
            }
          } else {
            var offset =
              jQuery('.navbar-tab').outerHeight() +
              jQuery('.navbar-static-top')
                .addClass('sticky shrink')
                .outerHeight();
          }
        } else {
          var offset = 0;
        }
        console.log(offset);
        scrollActive = true;
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
          location.hostname == this.hostname
        ) {
          var target = jQuery(this.hash);
          target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            jQuery('html,body').animate(
              {
                scrollTop: target.offset().top - offset
              },
              1000,
              'easeInQuart',
              function() {
                scrollActive = false;
              }
            );
            return false;
          }
        }
      }
    });
  }
}

/* ------------------------------------------------

    Theme Background Section

--------------------------------------------------- */

function themeImageSection() {
  var fullScreenImage = document.getElementsByClassName('theme-background-section');
  if (document.getElementsByClassName('theme-background-section')) {
    var windowH = window.innerHeight;
    jQuery('.theme-background-section').each(function() {
      $selection = jQuery(this);
      if ($selection.hasClass('custom-height')) {
        var customHeight = $selection.attr('data-custom-height');
        if (typeof customHeight !== typeof undefined && customHeight !== false && customHeight !== '') {
          var decCustomHeight = customHeight / 100;
          windowH = windowH * decCustomHeight;
        }
      } else if ($selection.hasClass('half-screen')) {
        windowH = windowH / 2;
      } else if ($selection.hasClass('half-screen-width')) {
        windowW = screenWidth / 2;
        $selection.css('width', windowW + 'px');
      } else {
        var offsetContainer = $selection.attr('data-offset-container');
        if (
          typeof offsetContainer !== typeof undefined &&
          offsetContainer !== false &&
          offsetContainer !== '' &&
          screenWidth > 767
        ) {
          var containerArray = offsetContainer.split(',');
          var i,
            offsetHeight = 0,
            currentContainer;
          for (i = 0; i < containerArray.length; i++) {
            currentContainer = String(containerArray[i]);
            offsetHeight += jQuery(currentContainer).outerHeight();
          }
          windowH = windowH - offsetHeight;
        }
      }
      if (screenWidth < 991) {
        $selection.css('height', 'auto');
      } else {
        $selection.css('height', windowH + 'px');
      }
    });
  }
}

/* ------------------------------------------------

    Centred Modal Box

--------------------------------------------------- */

function centerModal() {
  if (jQuery(window).height() >= 320) {
    adjustModal();
  }
}
function adjustModal() {
  jQuery('.modal').each(function() {
    if (jQuery(this).hasClass('in') == false) {
      jQuery(this).show();
    }
    var contentHeight = jQuery(window).height() - 60;
    var headerHeight =
      jQuery(this)
        .find('.modal-header')
        .outerHeight() || 2;
    var footerHeight =
      jQuery(this)
        .find('.modal-footer')
        .outerHeight() || 2;
    jQuery(this)
      .find('.modal-content')
      .css({
        'max-height': function() {
          return contentHeight;
        }
      });
    jQuery(this)
      .find('.modal-body')
      .css({
        'max-height': function() {
          return contentHeight - (headerHeight + footerHeight);
        }
      });
    jQuery(this)
      .find('.modal-dialog')
      .addClass('modal-dialog-center')
      .css({
        'margin-top': function() {
          return -(jQuery(this).outerHeight() / 2);
        },
        'margin-left': function() {
          return -(jQuery(this).outerWidth() / 2);
        }
      });
    if (jQuery(this).hasClass('in') == false) {
      jQuery(this).hide();
    }
  });
}

/* ------------------------------------------------

	# Links

--------------------------------------------------- */

jQuery('a').click(function(e) {
  var link = jQuery(this).attr('href');
  if (link == '#') {
    e.preventDefault();
  }
});

/* ------------------------------------------------

	Jump Links

--------------------------------------------------- */

//modified to jump to 1.12: JQMIGRATE: Attribute selector with '#' must be quoted
jQuery("a[href*='#']:not([href='#']).jump").click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var jumpOffset = 0;
    if (jQuery(this).attr('data-jump-offset')) {
      jumpOffset = jQuery(this).attr('data-jump-offset');
    }
    var target = jQuery(this.hash);
    target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      jQuery('html,body').animate(
        {
          scrollTop: target.offset().top - jumpOffset
        },
        600
      );
      return false;
    }
  }
});

/* ------------------------------------------------

	Initializing Tooltips

--------------------------------------------------- */

function tooltip() {
  jQuery('.tip-top').tooltip({
    placement: 'top',
    container: 'body'
  }),
    jQuery('.tip-right').tooltip({
      placement: 'right',
      container: 'body'
    }),
    jQuery('.tip-bottom').tooltip({
      placement: 'bottom',
      container: 'body'
    }),
    jQuery('.tip-left').tooltip({
      placement: 'left',
      container: 'body'
    });
}



/* ------------------------------------------------

	Initializing Wow.js

--------------------------------------------------- */

function wowInit() {
  var wow = new WOW({
    //disabled for mobile
    mobile: false
  });
  wow.init();
}

/* ------------------------------------------------
    Init Parallax Container
--------------------------------------------------- */

function parallaxContainer() {
  if (document.getElementsByClassName('parallax')) {
    jQuery('.parallax').each(function() {
      jQuery(this).parallax('50%', 0.2);
    });
  }
}

/* ------------------------------------------------

	Accordion View

--------------------------------------------------- */

if (jQuery(window).width() < 768) {
  jQuery('.faqs .faq-tabs-mobile > .panel-default>.panel-heading h4 a').click(function() {
    //jQuery('.faqs .faq-tabs-mobile > .panel-default>.panel-heading').removeClass('active');
    jQuery(this)
      .parents('.faqs .faq-tabs-mobile > .panel-default>.panel-heading')
      .toggleClass('active');
    //jQuery('.faqs .faq-tabs-mobile > .panel-default>.panel-heading .panel-title').removeClass('actives'); //just to make a visual sense
    jQuery(this)
      .parent()
      .toggleClass('actives'); //just to make a visual sense
    var section = jQuery(this).attr('data-target');
    /*jQuery('html, body').animate({
			scrollTop: jQuery(section).offset().top
		}, 500);*/
  });
}

/* ------------------------------------------------

	Equal Height Coumns

--------------------------------------------------- */

equalheight = function(container) {
  var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
  jQuery(container).each(function() {
    $el = jQuery(this);
    jQuery($el).height('auto');
    topPostion = $el.position().top;

    if (currentRowStart != topPostion) {
      for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
      rowDivs.length = 0; // empty the array
      currentRowStart = topPostion;
      currentTallest = $el.height();
      rowDivs.push($el);
    } else {
      rowDivs.push($el);
      currentTallest = currentTallest < $el.height() ? $el.height() : currentTallest;
    }
    for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
  });
};

//modified to jump to 1.12: JQMIGRATE: jQuery.fn.load() is deprecated
jQuery(window).on("load", function() {
  equalheight('#locker-locations .process-column');
});
jQuery(window).resize(function() {
  equalheight('#locker-locations .process-column');
});
jQuery(window).on("load", function() {
  equalheight('#concierge .process-column');
});
jQuery(window).resize(function() {
  equalheight('#concierge .process-column');
});
jQuery(window).on("load", function() {
  equalheight('#home-delivery .process-column');
});
jQuery(window).resize(function() {
  equalheight('#home-delivery .process-column');
});
jQuery(window).on("load", function() {
  equalheight('#home-delivery .process-column h4.process-title');
});
jQuery(window).resize(function() {
  equalheight('#home-delivery .process-column h4.process-title');
});
jQuery(window).on("load", function() {
  equalheight('#home-delivery .process-column h4.process-title');
});
jQuery(window).resize(function() {
  equalheight('.sustainability-icon');
});
jQuery(window).on("load", function() {
  equalheight('.sustainability-icon');
});

jQuery(function() {
  if (typeof Storage !== 'undefined') {
    if (sessionStorage.hidePromo == 'undefined' || Number(sessionStorage.hidePromo) != 1) {
      jQuery('.promo-bar').slideDown();
    }
  }
});

jQuery('.promo-close').click(function() {
  sessionStorage.hidePromo = 1;
});

/* ------------------------------------------------

	Function Call and Initializing Global Variables

--------------------------------------------------- */

var headerHeight = jQuery('.main-nav').outerHeight(),
  headerVisiblePos = 0,
  headerFixedPos = 0,
  isHeaderFixed = !1,
  isHeaderVisible = !1,
  isStickyElementFixed = !1,
  stickyElementSetPoint = 0,
  stickyElementY = 0,
  screenWidth = window.innerWidth,
  screenHeight = window.innerHeight,
  stickyElementDisabled = !1,
  winScrollY = 0,
  stickyElementTop = 0;

var $win = jQuery(window);

/* ------------------------------------------------
    Window Resize Events
--------------------------------------------------- */

$win
  .on('resize', function() {
    /*---- Resetting Variables ----*/

    isStickyElementFixed = false;
    winScrollY = 0;
    stickyElementSetPoint = 0;
    stickyElementY = 0;
    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;
    winScrollY = 0;
    stickyElementTop = 0;
    stickyElementDisabled = false;
    headerVisiblePos = 0;
    headerFixedPos = 0;
    isHeaderFixed = false;
    isHeaderVisible = false;
    headerHeight = 0;

    themeImageSection();

    responsive();

    stickyMenu();

    navOnePage();

    setTimeout(centerModal, 800);
  })
  .resize();

/* ------------------------------------------------
    Window Load Events
--------------------------------------------------- */

$win.on('load', function() {
  tooltip(), parallaxContainer();
});
