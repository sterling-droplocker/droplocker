CREATE TABLE `conveyorOrderUnload` (
  `conveyorOrderUnloadID` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INTEGER UNSIGNED NOT NULL,
  `dateCreated` DATETIME NOT NULL,
  `updated` DATETIME,
  `status` ENUM('pending', 'done') NOT NULL,
  PRIMARY KEY (`conveyorOrderUnloadID`)
)
ENGINE = InnoDB;
