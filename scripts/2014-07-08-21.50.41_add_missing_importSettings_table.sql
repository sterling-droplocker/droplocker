CREATE TABLE `importSettings` (
  `importSettingsID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `importType` varchar(45) DEFAULT NULL,
  `active` tinyint(3) DEFAULT '1',
  `customerBusiness_id` int(11) DEFAULT NULL,
  `locker_id` int(10) NOT NULL,
  PRIMARY KEY (`importSettingsID`) USING BTREE,
  UNIQUE KEY `Unique` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

