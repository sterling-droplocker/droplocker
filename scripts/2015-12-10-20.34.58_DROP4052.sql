CREATE TABLE `invoiceCustomField` (
  `invoiceCustomFieldID` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `sortOrder` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`invoiceCustomFieldID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

CREATE TABLE `invoiceCustomFieldValue` (
  `invoiceCustomFieldValueID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceCustomField_id` int(11) NOT NULL,
  `business_language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`invoiceCustomFieldValueID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
