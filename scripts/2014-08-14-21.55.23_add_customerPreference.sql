-- create the final table for preferences
CREATE TABLE  `customerPreference` (
`customerPreferenceID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`customer_id` INT UNSIGNED NOT NULL ,
`productCategory_id` INT UNSIGNED NOT NULL ,
`product_id` INT UNSIGNED NOT NULL ,
UNIQUE (  `customer_id` ,  `productCategory_id` )
) ENGINE = INNODB;


-- Steps to parse the preferences data AND store that into the new table
--   1. create a temp table to hold that data
--   2. parse AND INSERT INTO that table
--   3. INSERT INTO the new table (filter invalid prefs)
--   4. drop the temp table


-- temp table to hold the parsed preferences
CREATE TABLE  `temp_preference` (
`preferenceID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`customer_id` INT NOT NULL ,
`pref_name` VARCHAR( 255 ) NOT NULL ,
`pref_value` TEXT NOT NULL ,
`business_id` INT NOT NULL
) ENGINE = INNODB;



-- load the preference table with the parsed php data

INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',1),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',2),':',-1)) AS pref_value ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',3),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',4),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',5),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',6),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',7),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',8),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',9),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',10),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',11),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',12),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',13),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',14),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',15),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',16),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',17),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',18),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',19),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',20),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',21),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',22),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',23),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',24),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',25),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',26),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',27),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',28),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',29),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',30),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';


INSERT INTO temp_preference (customer_id, pref_name, pref_value, business_id)
SELECT customerID,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',31),':',-1)) AS pref_name,
TRIM(BOTH '"' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(preferences,';',32),':',-1)) AS pref_value  ,
business_id
FROM customer
WHERE preferences LIKE "%s:%"
HAVING pref_name NOT LIKE '%;%';




-- save the preferences into the final table

INSERT IGNORE INTO customerPreference (customer_id, productCategory_id, product_id)
SELECT customer_id, productCategoryID, productID
FROM temp_preference
JOIN product ON productID = pref_value
JOIN product_process ON product_id = productID
JOIN productCategory ON productCategory_id = productCategoryID
WHERE pref_name != 'notes'
    AND pref_name != 'starchOnShirts'
    AND productType = 'preference'
    AND product_process.active = 1;


-- drop the temp table
DROP TABLE temp_preference;

