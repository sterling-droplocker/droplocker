ALTER TABLE `delivery_customerSubscription` ADD `delivery_zone_id` int(11) NOT NULL;

UPDATE `delivery_customerSubscription`
    JOIN `delivery_window` ON (`delivery_window_id` = `delivery_windowID`)
    SET `delivery_customerSubscription`.`delivery_zone_id` = `delivery_window`.`delivery_zone_id`;

ALTER TABLE `delivery_customerSubscription` DROP `delivery_window_id`;