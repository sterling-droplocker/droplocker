update languageView set name = 'business_account/programs_print_coupon' where name = 'account/programs_print_coupon';
update languageView set name = 'business_account/programs_rewards' where name = 'account/programs_rewards';
update languageView set name = 'business_account/programs_reward_terms' where name = 'account/programs_reward_terms';
update languageView set name = 'business_account/orders_new_order_sidebar' where name = 'account/orders_new_order_sidebar';
update languageView set name = 'business_account/orders_complete' where name = 'account/orders_complete';
update languageView set name = 'business_account/unsubscribe' where name = 'account/unsubscribe';

update languageView set name = 'admin/profile/closet' where name = 'account/profile/closet';





-- delete legacy keys --

delete from languageKey_business_language where languageKey_id in (
    SELECT languageKeyID from languageKey
    join languageView on languageView_id = languageViewID
    where languageView.name = 'facebook_rewards_share_dialog_settings'
);

delete from languageKey where languageView_id in (
    select languageViewID from languageView where name = 'facebook_rewards_share_dialog_settings'
);

delete from languageView where name = 'facebook_rewards_share_dialog_settings';
