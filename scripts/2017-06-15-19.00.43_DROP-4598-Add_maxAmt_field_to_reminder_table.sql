ALTER TABLE reminder ADD maxAmt DECIMAL(10,2) NULL AFTER amount;
ALTER TABLE customerDiscount ADD maxAmt DECIMAL(10,2) AFTER amountType DEFAULT "0";
