alter table location_discount add dollar_amount double default null;
alter table location_discount add only_apply_if_minimum_is_not_met tinyint default 0;
