CREATE TABLE `mobile_apiSession` (
  `mobile_apiSessionID` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(32) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `expireDate` datetime NOT NULL,
  `days` int(11) NOT NULL,
  PRIMARY KEY (`mobile_apiSessionID`)
) ENGINE=InnoDB;
