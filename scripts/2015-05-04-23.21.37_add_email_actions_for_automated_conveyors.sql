INSERT INTO emailActions (orderStatusOption_id,action,description,name)
VALUES (3,'order_inventoried_automated_conveyor','Email sent to customer when an automated conveyor order is inventoried', 'Order Inventoried For Automated Conveyor');

INSERT INTO emailActions (orderStatusOption_id,action,description,name)
VALUES (9,'ready_for_pickup_automated_conveyor','Sends an email to a customer to schedule a delivery for automated conveyor order', 'Ready For Pickup at Automated Conveyor');
