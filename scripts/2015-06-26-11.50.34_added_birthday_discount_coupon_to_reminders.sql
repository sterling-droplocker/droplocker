ALTER TABLE `reminder` MODIFY COLUMN `couponType` ENUM('newCustomer','inactiveCustomer','orderTotal','birthdayCoupon') NOT NULL,
 MODIFY COLUMN `orders` INT(11) UNSIGNED NOT NULL;