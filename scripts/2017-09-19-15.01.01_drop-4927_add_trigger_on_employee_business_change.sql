ALTER TABLE `employee_log` 
ADD COLUMN `from_business_id` INT(11) NULL DEFAULT NULL  AFTER `business_id` , 
ADD COLUMN `job` VARCHAR(45) NULL DEFAULT NULL  AFTER `dateStatus` ;

DROP TRIGGER IF EXISTS log_updated_business_employee_to_employee_log;
DELIMITER ;;
CREATE TRIGGER log_updated_business_employee_to_employee_log AFTER UPDATE ON business_employee FOR EACH ROW BEGIN
    IF (OLD.business_id != NEW.business_id or OLD.`default` != NEW.`default` or OLD.employee_id != NEW.employee_id) THEN
      INSERT INTO employee_log
          (employee_id, from_employee_id, business_id, ip_address, dateStatus, from_business_id, job)
      VALUES
          (NEW.employee_id, OLD.employee_id, NEW.business_id, '', NOW(), OLD.business_id , 'business_employee_update');
    END IF;

END ;;
DELIMITER ;


