ALTER TABLE `reports_customer` 
CHANGE COLUMN `orderCount` `orderCount` INT(10) NULL DEFAULT 0 ;
UPDATE `reports_customer` SET orderCount = 0 WHERE orderCount IS NULL;
