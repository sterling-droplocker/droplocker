insert into languageView (name) VALUES 
('global');

INSERT INTO languageKey (languageView_id, name, defaultText) SELECT languageViewID, 'nameComponents', 'firstName||lastName' FROM languageView WHERE languageView.name = 'global';
INSERT INTO languageKey (languageView_id, name, defaultText) SELECT languageViewID, 'addressComponents', 'address1||address2||city||state||zip||country' FROM languageView WHERE languageView.name = 'global';
INSERT INTO languageKey (languageView_id, name, defaultText) SELECT languageViewID, 'person_name', '%firstName% %lastName%' FROM languageView WHERE languageView.name = 'global';
INSERT INTO languageKey (languageView_id, name, defaultText) SELECT languageViewID, 'address_format', '%address1% %address2%, %city%, %state%, %zip%' FROM languageView WHERE languageView.name = 'global';

INSERT INTO languageKey (languageView_id, name, defaultText) SELECT languageViewID, 'dateComponents', 'month||day||year' FROM languageView WHERE languageView.name = 'business_account/profile_birthday_component';

-- translations for japanese business 398 
INSERT INTO languageKey_business_language (business_id, language_id, languageKey_id, translation) SELECT 398, 77, languageKeyID, 'lastName||firstName' FROM languageKey LEFT JOIN languageView ON languageView_id = languageViewID WHERE languageKey.name = 'nameComponents' AND languageView.name = 'global';
INSERT INTO languageKey_business_language (business_id, language_id, languageKey_id, translation) SELECT 398, 77, languageKeyID, 'zip||state||city||address1||address2' FROM languageKey LEFT JOIN languageView ON languageView_id = languageViewID WHERE languageKey.name = 'addressComponents' AND languageView.name = 'global';
INSERT INTO languageKey_business_language (business_id, language_id, languageKey_id, translation) SELECT 398, 77, languageKeyID, '%lastName% %firstName% 様' FROM languageKey LEFT JOIN languageView ON languageView_id = languageViewID WHERE languageKey.name = 'person_name' AND languageView.name = 'global';
INSERT INTO languageKey_business_language (business_id, language_id, languageKey_id, translation) SELECT 398, 77, languageKeyID, '%zip%, %state%, %city%, %address1% %address2%' FROM languageKey LEFT JOIN languageView ON languageView_id = languageViewID WHERE languageKey.name = 'address_format' AND languageView.name = 'global';
INSERT INTO languageKey_business_language (business_id, language_id, languageKey_id, translation) SELECT 398, 77, languageKeyID, 'year||month||day' FROM languageKey LEFT JOIN languageView ON languageView_id = languageViewID WHERE languageKey.name = 'dateComponents' AND languageView.name = 'business_account/profile_birthday_component';
