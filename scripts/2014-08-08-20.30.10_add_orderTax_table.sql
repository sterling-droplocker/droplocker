CREATE TABLE  `orderTax` (
`orderTaxID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`order_id` INT UNSIGNED NOT NULL ,
`taxGroup_id` INT UNSIGNED NOT NULL ,
`zipcode` VARCHAR( 100 ) NULL,
`amount` DECIMAL( 10, 2 ) NOT NULL DEFAULT  '0.0',
`taxRate` FLOAT NULL,
PRIMARY KEY (  `orderTaxID` ) ,
INDEX (  `order_id` ,  `taxGroup_id` )
) ENGINE = INNODB;

INSERT INTO orderTax (order_id, taxGroup_id, zipcode, amount, taxRate)
SELECT orderID, taxGroupID, location.zipcode, tax, taxRate FROM orders
LEFT JOIN taxGroup ON (taxGroup.business_id = orders.business_id AND taxGroup.name = 'default')
LEFT JOIN locker ON (lockerID = locker_id)
LEFT JOIN location ON (locationID = location_id)
LEFT JOIN taxTable ON (taxGroupID = taxGroup_id AND taxTable.zipcode = location.zipcode)
WHERE tax > 0 ;
