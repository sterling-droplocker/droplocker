INSERT INTO `emailActions` (`action`, `name`, `description`, `isHomeDelivery`) VALUES ('pos_order_invoiced', 'Pos Order Invoiced', 'Sent to customer when a POS order is invoiced', '0');
INSERT INTO `emailActions` (`action`, `name`, `description`, `isHomeDelivery`) VALUES ('pos_order_conveyored', 'Pos Order Conveyored', 'Sent to customer when a POS order is ready for customer pickup', '0');
INSERT INTO `emailActions` (`action`, `name`, `description`, `isHomeDelivery`) VALUES ('pos_order_conveyored_remainder', 'Pos Order Conveyored Remainder', 'Sent to customer as remainder when a POS order is ready for customer pickup', '0');

ALTER TABLE `orders` ADD COLUMN `source` ENUM('customer_app', 'driver_app', 'dla', 'dla_pos', 'pos_app', 'other') NULL AFTER `alertNotes` ;