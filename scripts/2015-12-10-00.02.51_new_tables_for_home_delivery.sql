DROP TABLE IF EXISTS `delivery_zone`;

CREATE TABLE `delivery_zone` (
  `delivery_zoneID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `points` text,
  `color` char(7) NOT NULL DEFAULT '#000000',
  `business_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`delivery_zoneID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `delivery_window`;

CREATE TABLE `delivery_window` (
  `delivery_windowID` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_zone_id` int(11) NOT NULL,
  `start` time NOT NULL DEFAULT '09:00:00',
  `end` time NOT NULL DEFAULT '11:00:00',
  `fee` decimal(10,2) NOT NULL,
  `route` int(11) NOT NULL,
  `notes` text NOT NULL,
  `monday` int(11) NOT NULL DEFAULT '0',
  `tuesday` int(11) NOT NULL DEFAULT '0',
  `wednesday` int(11) NOT NULL DEFAULT '0',
  `thursday` int(11) NOT NULL DEFAULT '0',
  `friday` int(11) NOT NULL DEFAULT '0',
  `saturday` int(11) NOT NULL DEFAULT '0',
  `sunday` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_windowID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
