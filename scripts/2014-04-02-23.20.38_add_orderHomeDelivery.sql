CREATE TABLE  `orderHomeDelivery` (
    `orderHomeDeliveryID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `order_id` INT UNSIGNED NOT NULL ,
    `business_id` INT UNSIGNED NOT NULL ,
    `customer_id` INT UNSIGNED NOT NULL ,
    `address1` VARCHAR( 100 ) NOT NULL ,
    `address2` VARCHAR( 100 ) NOT NULL ,
    `city` VARCHAR( 100 ) NOT NULL ,
    `state` VARCHAR( 100 ) NOT NULL ,
    `zip` VARCHAR( 45 ) NOT NULL ,
    `lat` VARCHAR( 15 ) NOT NULL ,
    `lon` VARCHAR( 15 ) NOT NULL ,
    `deliveryDate` DATE NOT NULL ,
    `windowStart` INT NOT NULL ,
    `windowEnd` INT NOT NULL ,
    `notes` TEXT NOT NULL ,
    `payment_order_id` INT NOT NULL ,
    `delivery_response` TEXT NOT NULL ,
    `delivery_window_id` INT UNSIGNED NULL DEFAULT NULL ,
    `delivery_id` INT NOT NULL ,
    `fee` DECIMAL( 10, 2 ) NULL DEFAULT NULL
) ENGINE = INNODB;

ALTER TABLE  `orderHomeDelivery` ADD INDEX (  `order_id` );
