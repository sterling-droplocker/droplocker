DELETE languageKey_business_language FROM languageKey_business_language
JOIN languageKey ON languageKey_id = languageKeyID
JOIN languageView ON languageView_id = languageViewID
WHERE languageKey.name = 'locationHelpModal_header'
AND languageView.name = 'mobile/home';

DELETE languageKey FROM languageKey
JOIN languageView ON languageView_id = languageViewID
WHERE languageKey.name = 'locationHelpModal_header'
AND languageView.name = 'mobile/home';
