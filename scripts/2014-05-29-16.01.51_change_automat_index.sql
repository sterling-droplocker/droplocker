-- change index order_id to include garcode
ALTER TABLE `automat` DROP INDEX `order_id`;
ALTER TABLE `automat` ADD INDEX `order_id` (`order_id`, `garcode`);

