ALTER TABLE business_language MODIFY COLUMN `locale` VARCHAR(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'en_US.utf-8';
UPDATE business_language set locale = (SELECT locale FROM business WHERE business_language.business_id = business.businessID );
