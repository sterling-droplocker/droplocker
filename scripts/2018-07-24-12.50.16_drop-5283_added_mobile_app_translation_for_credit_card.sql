INSERT INTO languageView (name) VALUES ('mobile/credit_card');

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'title', languageViewID, 'Payment Information' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'card_number', languageViewID, 'Card Number' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'month', languageViewID, 'Month' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'year', languageViewID, 'Year' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'cvv', languageViewID, 'CVV' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'billing_information', languageViewID, 'Billing Information' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'first_name', languageViewID, 'First name' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'last_name', languageViewID, 'Last name' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'address1', languageViewID, 'Address 1' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'address2', languageViewID, 'Address 2' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'city', languageViewID, 'City' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'state', languageViewID, 'State' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'zip', languageViewID, 'Zip' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'save', languageViewID, 'Save' FROM languageView WHERE languageView.name = 'mobile/credit_card';
