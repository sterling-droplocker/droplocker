SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- -----------------------------------------------------
-- Table `cash_register`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cash_register` (
  `cash_registerID` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `notes` TEXT NULL ,
  `location_id` INT NULL ,
  PRIMARY KEY (`cash_registerID`) )
ENGINE = InnoDB;

CREATE INDEX `fk_cash_register_location` ON `cash_register` (`location_id` ASC) ;


-- -----------------------------------------------------
-- Table `payment_method`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `payment_method` (
  `payment_methodID` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `notes` TEXT NULL ,
  `slug` VARCHAR(45) NULL ,
  PRIMARY KEY (`payment_methodID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cash_register_record_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cash_register_record_type` (
  `cash_register_record_typeID` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `notes` TEXT NULL ,
  `slug` VARCHAR(45) NULL ,
  PRIMARY KEY (`cash_register_record_typeID`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cash_register_record`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cash_register_record` (
  `cash_register_recordID` INT NOT NULL AUTO_INCREMENT ,
  `cash_register_id` INT NULL ,
  `cash_register_record_type_id` INT NULL ,
  `payment_method_id` INT NULL ,
  `amount` DECIMAL(12,2) NOT NULL DEFAULT 0 ,
  `paid` DECIMAL(12,2) NOT NULL DEFAULT 0 ,
  `change` DECIMAL(12,2) NOT NULL DEFAULT 0 ,
  `notes` TEXT NULL ,
  `employee_id` INT NULL ,
  `device_identifier` VARCHAR(512) NULL ,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cash_register_recordID`) )
ENGINE = InnoDB;

CREATE INDEX `fk_cash_register_record_payment_method` ON `cash_register_record` (`payment_method_id` ASC) ;

CREATE INDEX `fk_cash_register_record_cash_register1` ON `cash_register_record` (`cash_register_id` ASC) ;

CREATE INDEX `fk_cash_register_record_cash_register_record_type1` ON `cash_register_record` (`cash_register_record_type_id` ASC) ;

CREATE INDEX `fk_cash_register_record_employee1` ON `cash_register_record` (`employee_id` ASC) ;


-- -----------------------------------------------------
-- Table `cash_register_record_order`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cash_register_record_order` (
  `cash_register_record_orderID` INT NOT NULL AUTO_INCREMENT ,
  `cash_register_record_id` INT NULL ,
  `order_id` INT NULL ,
  `paid` DECIMAL(12,2) NOT NULL DEFAULT 0 ,
  `notes` TEXT NULL ,
  PRIMARY KEY (`cash_register_record_orderID`) )
ENGINE = InnoDB;

CREATE INDEX `fk_cash_register_record_order_cash_register_record1` ON `cash_register_record_order` (`cash_register_record_id` ASC) ;

CREATE INDEX `fk_cash_register_record_order_orders1` ON `cash_register_record_order` (`order_id` ASC) ;

INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Start Drawer',NULL,'drawer-start');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'End Drawer',NULL,'drawer-end');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Sale',NULL,'sale');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Refund',NULL,'refund');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Difference',NULL,'difference');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Employee Login',NULL,'login');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Employee Logout',NULL,'logout');
INSERT INTO `cash_register_record_type` (`cash_register_record_typeID`,`name`,`notes`,`slug`) VALUES (NULL,'Unneeded Logout',NULL,'unneeded-logout');

INSERT INTO `payment_method` (`payment_methodID`,`name`,`notes`,`slug`) VALUES (NULL,'Cash',NULL,'cash');
INSERT INTO `payment_method` (`payment_methodID`,`name`,`notes`,`slug`) VALUES (NULL,'Credit Card',NULL,'credit-card');
INSERT INTO `payment_method` (`payment_methodID`,`name`,`notes`,`slug`) VALUES (NULL,'Other',NULL,'other');
INSERT INTO `payment_method` (`payment_methodID`,`name`,`notes`,`slug`) VALUES (NULL,'Non Monetary',NULL,'non-monetary');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
