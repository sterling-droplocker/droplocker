ALTER TABLE  `reports_customer` CHANGE  `internalNotes`  `internalNotes` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `reports_customer` CHANGE  `preferences`  `preferences` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE  `reports_customer` CHANGE  `customerNotes`  `customerNotes` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

