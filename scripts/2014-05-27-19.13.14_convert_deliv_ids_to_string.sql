ALTER TABLE  `orderHomeDelivery`
CHANGE `delivery_window_id`  `delivery_window_id` VARCHAR( 255 ) NULL DEFAULT NULL ,
CHANGE  `delivery_id`  `delivery_id` VARCHAR( 255 ) NULL DEFAULT NULL;

ALTER TABLE  `location` CHANGE  `deliv_id`  `deliv_id` VARCHAR( 255 ) NULL DEFAULT NULL;

UPDATE orderHomeDelivery SET delivery_window_id = NULL WHERE delivery_window_id = 0;
UPDATE orderHomeDelivery SET delivery_id = NULL WHERE delivery_id = 0;
