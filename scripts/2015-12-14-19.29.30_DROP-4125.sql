UPDATE customerDiscount d
    LEFT JOIN customer c
        ON  d.customer_id = c.customerID 
    SET d.business_id = c.business_id
    WHERE d.business_id != c.business_id;
