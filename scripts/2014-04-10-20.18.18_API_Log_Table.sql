CREATE TABLE `api_request_log` (
  `api_request_logID` int(11) NOT NULL AUTO_INCREMENT,
  `success` tinyint(1) DEFAULT NULL,
  `requestType` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `requestBody` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clientAddress` varchar(50) DEFAULT NULL,
  `serverAddress` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`api_request_logID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
