ALTER TABLE `location` ADD `dc_min_type` VARCHAR(20) NULL DEFAULT NULL AFTER `wf_min_amount`, ADD `dc_min_amount` VARCHAR(20) NULL DEFAULT NULL AFTER `dc_min_type`;

ALTER TABLE `business` ADD `dc_min_type` VARCHAR(20) NULL DEFAULT NULL AFTER `wash_fold_min_amount`, ADD `dc_min_amount` VARCHAR(20) NULL DEFAULT NULL AFTER `dc_min_type`;

ALTER TABLE `business` CHANGE `wash_fold_min_type` `wf_min_type` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `business` CHANGE `wash_fold_min_amount` `wf_min_amount` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
