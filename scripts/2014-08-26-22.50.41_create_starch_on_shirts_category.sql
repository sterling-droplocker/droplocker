-- set module_id to DC for existing cateogries
UPDATE productCategory SET module_id = 3 WHERE slug = 'starchonshirts';

-- create category in the businees that don't have one
INSERT INTO productCategory (name, note, slug, business_id, module_id, sort_order)
SELECT "Starch on Shirts", "Starch on Shirts", "starchonshirts", businessMeta.business_id , 3, 0
FROM businessMeta
LEFT JOIN productCategory ON (businessMeta.business_id = productCategory.business_id AND slug = 'starchonshirts' )
WHERE
    `key`= 'displayStarchOptions'
    AND VALUE = 1
    AND slug IS NULL;

-- create all options for starchs
INSERT INTO product (name, displayName, unitOfMeasurement, productCategory_id, updated, sortOrder, business_id, productType, upchargeGroup_id)
SELECT 'no starch', 'None', 'N/A', productCategoryID, NOW(), 2, business_id, 'preference', upchargeGroupID
FROM productCategory
LEFT JOIN upchargeGroup USING (business_id)
WHERE slug = 'starchonshirts'
AND (SELECT productID FROM product WHERE name = 'no starch' AND business_id = productCategory.business_id LIMIT 1) IS NULL
GROUP BY business_id
ORDER BY upchargeGroupID;

INSERT INTO product (name, displayName, unitOfMeasurement, productCategory_id, updated, sortOrder, business_id, productType, upchargeGroup_id)
SELECT 'light starch', 'Light', 'N/A', productCategoryID, NOW(), 1, business_id, 'preference', upchargeGroupID
FROM productCategory
LEFT JOIN upchargeGroup USING (business_id)
WHERE slug = 'starchonshirts'
AND (SELECT productID FROM product WHERE name = 'light starch' AND business_id = productCategory.business_id LIMIT 1) IS NULL
GROUP BY business_id
ORDER BY upchargeGroupID;

INSERT INTO product (name, displayName, unitOfMeasurement, productCategory_id, updated, sortOrder, business_id, productType, upchargeGroup_id)
SELECT 'normal starch', 'Normal', 'N/A', productCategoryID, NOW(), 3, business_id, 'preference', upchargeGroupID
FROM productCategory
LEFT JOIN upchargeGroup USING (business_id)
WHERE slug = 'starchonshirts'
AND (SELECT productID FROM product WHERE name = 'normal starch' AND business_id = productCategory.business_id LIMIT 1) IS NULL
GROUP BY business_id
ORDER BY upchargeGroupID;

INSERT INTO product (name, displayName, unitOfMeasurement, productCategory_id, updated, sortOrder, business_id, productType, upchargeGroup_id)
SELECT 'heavy starch', 'Heavy', 'N/A', productCategoryID, NOW(), 4, business_id, 'preference', upchargeGroupID
FROM productCategory
LEFT JOIN upchargeGroup USING (business_id)
WHERE slug = 'starchonshirts'
AND (SELECT productID FROM product WHERE name = 'heavy starch' AND business_id = productCategory.business_id LIMIT 1) IS NULL
GROUP BY business_id
ORDER BY upchargeGroupID;


-- create all product_process
INSERT INTO product_process (product_id, process_id, price, taxable, dateCreated, active, taxGroup_id)
SELECT productID, 1, 0, 0, NOW(), 1, NULL
FROM product
LEFT JOIN product_process ON (product_id = productID)
WHERE name IN ('no starch', 'light starch', 'normal starch', 'heavy starch')
AND product_processID IS NULL;


-- store all customer preferences from starchOnShirts_id
INSERT IGNORE INTO customerPreference (customer_id, productCategory_id, product_id)
SELECT customerID, productCategoryID, productID
FROM customer
JOIN product ON (product.business_id = customer.business_id AND product.name = 'no starch')
JOIN productCategory ON (productCategoryID = productCategory_id)
WHERE starchOnShirts_id = 1;

INSERT IGNORE INTO customerPreference (customer_id, productCategory_id, product_id)
SELECT customerID, productCategoryID, productID
FROM customer
JOIN product ON (product.business_id = customer.business_id AND product.name = 'light starch')
JOIN productCategory ON (productCategoryID = productCategory_id)
WHERE starchOnShirts_id = 2;

INSERT IGNORE INTO customerPreference (customer_id, productCategory_id, product_id)
SELECT customerID, productCategoryID, productID
FROM customer
JOIN product ON (product.business_id = customer.business_id AND product.name = 'normal starch')
JOIN productCategory ON (productCategoryID = productCategory_id)
WHERE starchOnShirts_id = 3;

INSERT IGNORE INTO customerPreference (customer_id, productCategory_id, product_id)
SELECT customerID, productCategoryID, productID
FROM customer
JOIN product ON (product.business_id = customer.business_id AND product.name = 'heavy starch')
JOIN productCategory ON (productCategoryID = productCategory_id)
WHERE starchOnShirts_id = 4;
