CREATE TABLE `itemOption` (
  `itemOptionID` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `updated` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (`itemOptionID`),
  UNIQUE KEY `item_id` (`item_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
