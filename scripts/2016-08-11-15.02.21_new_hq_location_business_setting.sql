ALTER TABLE `business` ADD COLUMN `hq_location_id` INT(10) NULL DEFAULT NULL  AFTER `scheduled_orders_time` ;
UPDATE business JOIN location ON (business_id = businessID) SET hq_location_id=locationID WHERE location.companyName LIKE '%HQ%';
