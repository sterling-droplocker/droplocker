-- remove 00 prefix
UPDATE customer SET sms = SUBSTRING(sms, 3) WHERE business_id = 39 AND sms RLIKE '^00';
UPDATE customer SET phone = SUBSTRING(phone, 3) WHERE business_id = 39 AND phone RLIKE '^00';

-- remove +39 prefix
UPDATE customer SET sms = SUBSTRING(sms, 4) WHERE business_id = 39 AND sms RLIKE '^\\+3934';
UPDATE customer SET phone = SUBSTRING(phone, 4) WHERE business_id = 39 AND phone RLIKE '^\\+3934';

-- add 34 prefix
UPDATE customer SET sms = CONCAT('34', sms) WHERE business_id = 39 AND LENGTH(sms) = 9;
UPDATE customer SET phone = CONCAT('34', phone) WHERE business_id = 39 AND LENGTH(phone) = 9;

