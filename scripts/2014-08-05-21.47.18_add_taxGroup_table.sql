ALTER TABLE `taxTable` ADD  `taxGroup_id` INT UNSIGNED NULL;

CREATE TABLE `taxGroup` (
  `taxGroupID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `business_id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR( 255 ) NOT NULL ,
  UNIQUE (`business_id`, `name`)
) ENGINE = INNODB;

INSERT INTO taxGroup (business_id, name) SELECT businessID, 'default' FROM business;

UPDATE taxTable SET taxGroup_id = (
    SELECT taxGroupID FROM taxGroup 
    WHERE taxGroup.business_id = taxTable.business_id LIMIT 1
);

