alter table laundryPlan add businessLaundryPlan_id int;
alter table businessLaundryPlan add updateExistingCustomerPlansWhenPlanChange smallint default 0;

update laundryPlan lp 
join businessLaundryPlan blp on (blp.business_id=lp.business_id
and lp.type=blp.type and lp.description=blp.description and lp.active=1)
set lp.businessLaundryPlan_id=blp.businessLaundryPlanID;

-- populates 13 results that not matched the last query
update laundryPlan lp 
join businessLaundryPlan blp on (blp.business_id=lp.business_id
and lp.type=blp.type and lp.active=1 and lp.businessLaundryPlan_ID is null)
set lp.businessLaundryPlan_id=blp.businessLaundryPlanID;
