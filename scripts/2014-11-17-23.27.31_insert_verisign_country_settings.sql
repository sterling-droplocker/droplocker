-- Create the verisign country
INSERT INTO businessMeta (`key`, business_id, value)
SELECT 'verisign_country', businessID,
CASE country
    WHEN 'usa' THEN 'US'
END AS `value`
FROM business
JOIN businessMeta ON (businessID = business_id AND `key` = 'verisign_user');

-- Create the verisign country
INSERT INTO businessMeta (`key`, business_id, value)
SELECT 'verisign_currency', businessID,
CASE country
    WHEN 'usa' THEN 'USD'
END AS `value`
FROM business
JOIN businessMeta ON (businessID = business_id AND `key` = 'verisign_user');

