-- show products with mismatched categories
SELECT pc.name,productCategoryID,pc.business_id as pcbusiness_id,product.business_id, upchargeGroup_id
FROM productCategory AS pc
JOIN product ON product.productCategory_id = productCategoryID
WHERE product.business_id != pc.business_id
ORDER BY pc.name;

-- update all products that do have a matching category
UPDATE product
JOIN productCategory AS a ON (product.productCategory_id = productCategoryID)
JOIN productCategory AS b ON (b.name = a.name AND b.business_id = product.business_id)
SET productCategory_id = b.productCategoryID
WHERE product.business_id != a.business_id;

-- create missing categories
INSERT INTO productCategory (name,note,slug,business_id,module_id,sort_order,parent_id)
SELECT pc.name, pc.note, pc.slug, product.business_id, pc.module_id, pc.sort_order, pc.parent_id
FROM productCategory as pc
JOIN product ON (product.productCategory_id = productCategoryID)
WHERE product.business_id != pc.business_id
GROUP BY product.business_id, pc.name;

-- update products with the newly created categories
UPDATE product
JOIN productCategory AS a ON (product.productCategory_id = productCategoryID)
JOIN productCategory AS b ON (b.name = a.name AND b.business_id = product.business_id)
SET productCategory_id = b.productCategoryID
WHERE product.business_id != a.business_id;

