CREATE  TABLE `driver_app_journal` (
  `driver_app_journalID` INT NOT NULL AUTO_INCREMENT ,
  `tableName` VARCHAR(100) NULL ,
  `id` INT NULL ,
  `event` VARCHAR(20) NULL ,
  `business_id` INT NULL ,
  `oldData` TEXT NULL ,
  `newData` TEXT NULL ,
  `date` DATETIME NULL,
  PRIMARY KEY (`driver_app_journalID`) );

-- --------------------------------------------------------------
-- ------------------- orders -----------------------------------
DROP TRIGGER IF EXISTS log_new_orders_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_new_orders_to_driver_app_journal AFTER INSERT ON orders FOR EACH ROW 
BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('orders', NEW.orderID, 'insert', NEW.business_id, '', '', NOW() );
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_updated_orders_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_orders_to_driver_app_journal AFTER UPDATE ON orders FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('orders', NEW.orderID, 'update', NEW.business_id, '', '', NOW() );
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_deleted_orders_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_deleted_orders_to_driver_app_journal AFTER DELETE ON orders FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('orders', OLD.orderID, 'delete', OLD.business_id, '', '', NOW() );
END ;;
DELIMITER ;

-- --------------------------------------------------------------
-- -------------------- claims ----------------------------------
DROP TRIGGER IF EXISTS log_new_claim_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_new_claim_to_driver_app_journal AFTER INSERT ON claim FOR EACH ROW 
BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('claim', NEW.claimID, 'insert', NEW.business_id, '', '', NOW() );
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_updated_claim_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_claim_to_driver_app_journal AFTER UPDATE ON claim FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('claim', NEW.claimID, 'update', NEW.business_id, '', '', NOW() );
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_deleted_claim_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_deleted_claim_to_driver_app_journal AFTER DELETE ON claim FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('claim', OLD.claimID, 'delete', OLD.business_id, '', '', NOW() );
END ;;
DELIMITER ;

-- --------------------------------------------------------------
-- -------------------- order status (logged as change on related order) ----------------------------
DROP TRIGGER IF EXISTS log_new_orderStatus_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_new_orderStatus_to_driver_app_journal AFTER INSERT ON orderStatus FOR EACH ROW 
BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    SELECT 
        'orders' tableName, orderID id, 'insert_status' event, business_id , '' oldData, '' newData, NOW() `date`
        FROM orders WHERE orderID = NEW.order_id;
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_updated_orderStatus_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_orderStatus_to_driver_app_journal AFTER UPDATE ON orderStatus FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    SELECT 
        'orders' tableName, orderID id, 'update_status' event, business_id , '' oldData, '' newData, NOW() `date`
        FROM orders WHERE orderID = NEW.order_id;
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_deleted_orderStatus_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_deleted_orderStatus_to_driver_app_journal AFTER DELETE ON orderStatus FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    SELECT 
        'orders' tableName, orderID id, 'delete_status' event, business_id , '' oldData, '' newData, NOW() `date`
        FROM orders WHERE orderID = OLD.order_id;
END ;;
DELIMITER ;
