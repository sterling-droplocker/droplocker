ALTER TABLE `ticket` ADD `ticketType_id` INT NULL AFTER `nextActionDate` ;
ALTER TABLE `ticket` CHANGE `ticketType` `ticketSource` ENUM( 'email', 'phone', 'employee', '' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ;


CREATE TABLE IF NOT EXISTS `ticketType` (
`ticketTypeID` int(11) NOT NULL AUTO_INCREMENT,
`description` varchar(255) NOT NULL,
`business_id` int(11) DEFAULT NULL,
`roles` varchar(255) NULL,
PRIMARY KEY (`ticketTypeID`),
KEY `business_id` (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



