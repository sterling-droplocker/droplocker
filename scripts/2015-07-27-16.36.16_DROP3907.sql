CREATE TABLE IF NOT EXISTS `transbankOrders` (
`transbankOrderID` int(11) NOT NULL AUTO_INCREMENT,
`order_id` int(11) NOT NULL,
`code` varchar(255) NOT NULL,
`dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`transbankOrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
