CREATE TABLE mailchimpLog
(
    mailchimpLogID INT PRIMARY KEY AUTO_INCREMENT,
    business_id INT UNSIGNED NOT NULL,
    dateCreated TIMESTAMP,
    requestUrl VARCHAR(255),
    response TEXT
);
CREATE INDEX mailchimpLog_business_id_index ON mailchimpLog (business_id);
