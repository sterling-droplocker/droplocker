alter table laundryPlan add type varchar(20) default 'laundry_plan';
alter table laundryPlan add credit_amount double;
alter table laundryPlan add expired_discount_percent int;
