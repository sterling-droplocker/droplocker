ALTER TABLE ciSessions engine=InnoDB;
ALTER TABLE acl engine=InnoDB;
ALTER TABLE aclresources engine=InnoDB;
ALTER TABLE creditCard engine=InnoDB;
ALTER TABLE kioskAccess engine=InnoDB;
ALTER TABLE orderPaymentStatus engine=InnoDB;
ALTER TABLE taxTable engine=InnoDB;
ALTER TABLE itemHistory engine=InnoDB;
ALTER TABLE lockerLoot engine=InnoDB;
ALTER TABLE coupon engine=InnoDB;
ALTER TABLE product_location_price engine=InnoDB;

ALTER TABLE orderPaymentStatusOption engine=InnoDB;
ALTER TABLE lockerStatus engine=InnoDB;
ALTER TABLE settings engine=InnoDB;
