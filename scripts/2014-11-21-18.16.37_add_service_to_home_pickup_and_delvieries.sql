ALTER TABLE  `orderHomePickup` ADD  `service` VARCHAR( 64 ) NOT NULL;
ALTER TABLE  `orderHomeDelivery` ADD  `service` VARCHAR( 64 ) NOT NULL;

UPDATE `orderHomePickup` SET `service` = 'deliv';
UPDATE `orderHomeDelivery` SET `service` = 'deliv';
