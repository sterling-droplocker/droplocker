INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'current_pass', languageViewID, 'Current Password' FROM languageView WHERE languageView.name = 'mobile/change_password';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'new_pass', languageViewID, 'New Password' FROM languageView WHERE languageView.name = 'mobile/change_password';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'new_pass_check', languageViewID, 'Retype New Password' FROM languageView WHERE languageView.name = 'mobile/change_password';
-- --------
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'cardNumber', languageViewID, 'Card Number' FROM languageView WHERE languageView.name = 'mobile/kiosk_access';

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'cvc', languageViewID, 'CVV' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'firstname', languageViewID, 'First name' FROM languageView WHERE languageView.name = 'mobile/credit_card';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'lastname', languageViewID, 'Last name' FROM languageView WHERE languageView.name = 'mobile/credit_card';

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'address_1', languageViewID, 'Address 1' FROM languageView WHERE languageView.name = 'mobile/address_detail';

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'first_name', languageViewID, 'First name' FROM languageView WHERE languageView.name = 'mobile/settings';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'last_name', languageViewID, 'Last name' FROM languageView WHERE languageView.name = 'mobile/settings';

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'username', languageViewID, 'E-mail address' FROM languageView WHERE languageView.name = 'mobile/signin';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'password', languageViewID, 'Password' FROM languageView WHERE languageView.name = 'mobile/signin';

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'firstName', languageViewID, 'First' FROM languageView WHERE languageView.name = 'mobile/signup';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'lastName', languageViewID, 'Last' FROM languageView WHERE languageView.name = 'mobile/signup';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'email', languageViewID, 'Mobile' FROM languageView WHERE languageView.name = 'mobile/signup';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'password', languageViewID, 'Password' FROM languageView WHERE languageView.name = 'mobile/signup';
INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'phone', languageViewID, 'Mobile' FROM languageView WHERE languageView.name = 'mobile/signup';

INSERT INTO languageKey (name, languageView_id, defaultText) SELECT 'apiErrorModal_connectionErrorText2', languageViewID, 'Your device must be connected to the internet for this application to work.' FROM languageView WHERE languageView.name = 'mobile/global';
