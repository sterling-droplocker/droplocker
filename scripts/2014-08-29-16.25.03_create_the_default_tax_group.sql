INSERT INTO businessMeta (business_id, `key`, value)
SELECT business_id, 'default_taxGroup', taxGroupID
FROM taxGroup
GROUP BY business_id
ORDER BY business_id, taxGroupID;
