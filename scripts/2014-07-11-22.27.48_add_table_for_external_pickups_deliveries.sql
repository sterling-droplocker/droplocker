CREATE TABLE  `orderExternalDelivery` (
`orderExternalDeliveryID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`dateCreated` TIMESTAMP NOT NULL ,
`third_party_id` VARCHAR( 255 ) NOT NULL ,
`service` VARCHAR( 64 ) NOT NULL ,
`order_id` INT NOT NULL ,
`response` TEXT NOT NULL ,
UNIQUE (`order_id`)
) ENGINE = INNODB;

CREATE TABLE  `claimExternalPickup` (
`claimExternalPickupID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`dateCreated` TIMESTAMP NOT NULL ,
`third_party_id` VARCHAR( 255 ) NOT NULL ,
`service` VARCHAR( 64 ) NOT NULL ,
`claim_id` INT NOT NULL ,
`response` TEXT NOT NULL ,
UNIQUE (`claim_id`)
) ENGINE = INNODB;
