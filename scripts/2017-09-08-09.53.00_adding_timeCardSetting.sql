/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  doug
 * Created: Sep 8, 2017
 */

create table timeCardSetting (
  timeCardSettingID int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  business_id int(50),
  settingType varchar(45),
  startDay varchar(45),
  endDay varchar(45),
  value varchar(45)
);