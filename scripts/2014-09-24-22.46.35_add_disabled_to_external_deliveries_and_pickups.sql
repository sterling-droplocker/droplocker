ALTER TABLE  `orderExternalDelivery` ADD  `disabled` TINYINT UNSIGNED NOT NULL DEFAULT  '0';
ALTER TABLE  `claimExternalPickup` ADD  `disabled` TINYINT UNSIGNED NOT NULL DEFAULT  '0';

ALTER TABLE  `orderExternalDelivery` DROP INDEX  `order_id` , ADD INDEX  `order_id` (  `order_id` );
ALTER TABLE  `claimExternalPickup` DROP INDEX  `claim_id` , ADD INDEX  `claim_id` (  `claim_id` );
