CREATE TABLE  `productName` (
`productNameID` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
`product_id` INT UNSIGNED NOT NULL ,
`business_language_id` INT UNSIGNED NOT NULL ,
`title` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (  `productNameID` ) ,
INDEX (  `product_id` ,  `business_language_id` )
) ENGINE = INNODB;

CREATE TABLE  `productCategoryName` (
`productCategoryNameID` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
`productCategory_id` INT UNSIGNED NOT NULL ,
`business_language_id` INT UNSIGNED NOT NULL ,
`title` VARCHAR( 255 ) NOT NULL ,
PRIMARY KEY (  `productCategoryNameID` ) ,
INDEX (  `productCategory_id` ,  `business_language_id` )
) ENGINE = INNODB;
