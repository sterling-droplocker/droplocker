create table home_delivery_log(
 id int auto_increment primary key,
 business_id int not null,
 `url` varchar(100),
 `requestBody` text,
 `requestResponse` text,
 `date` datetime default now()
);
