alter table lockerLockType add isElectronicLock smallInt default 0;
UPDATE `droplocker`.`lockerLockType` SET `isElectronicLock`='1' WHERE `lockerLockTypeID`='2';
INSERT INTO `droplocker`.`lockerLockType` (`name`,`isElectronicLock`) VALUES ('Next SOLA',1);
INSERT INTO `droplocker`.`lockerLockType` (`name`,`isElectronicLock`) VALUES ('Digilock Classic DCK-ATV',1);
INSERT INTO `droplocker`.`lockerLockType` (`name`,`isElectronicLock`) VALUES ('Lockers.com 3090',1);
INSERT INTO `droplocker`.`lockerLockType` (`name`,`isElectronicLock`) VALUES ('Safe-O-Tronic Access LS200',1);
