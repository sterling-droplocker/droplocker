CREATE TABLE `ticketCannedResponse` (
  `ticketCannedResponseID` int(11) NOT NULL AUTO_INCREMENT,
  `active` set('Y','N') DEFAULT 'Y',
  `shortTitle` varchar(75) NOT NULL,
  `response` varchar(750) NOT NULL,
  PRIMARY KEY (`ticketCannedResponseID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;



CREATE TABLE `ticketEmailConfiguration` (
  `ticketEmailConfigurationID` int(11) NOT NULL AUTO_INCREMENT,
  `business_ID` int(11) DEFAULT NULL,
  `csEmailUser` varchar(255) DEFAULT NULL,
  `csEmailPassword` varchar(50) DEFAULT NULL,
  `csEmailHost` varchar(255) DEFAULT NULL,
  `csEmailPort` varchar(50) DEFAULT NULL,
  `csEmailType` set('IMAP','POP') DEFAULT NULL,
  `active` set('Y','N') DEFAULT 'N',
  `processedFolder` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`ticketEmailConfigurationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `ticketEmailConfiguration`
(
`business_ID`,
`csEmailUser`,
`csEmailPassword`,
`csEmailHost`,
`csEmailPort`,
`csEmailType`,
`active`,
`processedFolder`)
VALUES
(
0,
'lgDropTest@gmail.com',
'L0ck3rP0w3r!',
'imap.gmail.com',
'19',
'IMAP',
'Y',
'processedTickets');

insert into employee(employeeID,firstName,lastName)
values
(600,"Customer","Email");


ALTER TABLE `ticket` 
CHANGE COLUMN `status` `status` ENUM('Open','Urgent','Closed','Pending Review') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'Open' ;


ALTER TABLE `ticket` 
ADD COLUMN `customerName` VARCHAR(255) NULL AFTER `assignedTo`,
ADD COLUMN `customerEmail` VARCHAR(255) NULL AFTER `customerName`;


ALTER TABLE `ticketNote` 
ADD COLUMN `noteType` ENUM('Internal','Customer','Customer Reply') CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL DEFAULT 'Internal' ;

ALTER TABLE `ticketNote` 
ADD COLUMN `expectedDate` DATETIME NULL DEFAULT NULL;

CREATE TABLE `ticketStatus` (
  `ticketStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `responseTime` int(11) DEFAULT NULL,
  `notifyCustomer` set('Y','N') DEFAULT 'N',
  PRIMARY KEY (`ticketStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (1,'Open',12,'Y');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (2,'Urgent',6,'N');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (3,'Driver Checking',48,'N');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (4,'Locker Reset',8,'N');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (5,'Follow up',72,'N');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (6,'Search for Items',48,'N');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (7,'Video Review',48,'N');
INSERT INTO `ticketStatus` (`ticketStatusId`,`description`,`responseTime`,`notifyCustomer`) VALUES (8,'Closed',0,'Y');

INSERT INTO `ticketCannedResponse`
(
`active`,
`shortTitle`,
`response`)
VALUES
(
'Y',
'Locker Reset',
'Hey ____________!

I’m so sorry the code to your locker is not working. I’m sending a driver to verify the code and reset your locker ASAP. Thank you for your patience while we work to resolve this, we’ll be in touch soon!'),
('Y',
'Locker Reset Confirmation',
'Hey _______ , the driver just confirmed your locker _______ had been reset with the code _______ . '),
('Y',
'Missing Order Claim',
'Hey _________!

I’m sorry to hear your order seems to be delayed, let me check a few things for you. Can you send me the information below so I can track the order as quickly as possible. We may have the items at our plant but unidentified to your account. We’ll get working on this right away!


Did you place an order online or through our app? If yes, what locker did you claim?
Did you use your Laundry Locker bag? For new users, what type of bag did you drop off in?
What is the order type?
Can you provide a description of the items? Brand, colors, sizes or garment type?'),
('Y',
'Laundry Plan Charge',
'Hey  _________!

Thank you for your recent laundry plan purchase! Your discount per pound is included in the total price you pay up front. When you receive the invoices for your orders , your card is not charged the dollar amount reflected.  Each order will apply the pounds from your plan until they are used up, if you exceed your purchased pounds for that month your order will be charged the standard rate per pound. '),
('Y',
'Order after 9:00 AM',
'Hey _________!

Thank you for your note! Since the driver has not arrived to this location yet I am able to move the order to today’s manifest! You’ll receive a pick up notification later this afternoon. '),
('Y',
'Missing Dry Cleaning',
'Thank you for reaching out! We will look into this right away, can you please log into your Closet 2.0 and identify the bar code associated to the missing item. If you do not see it pictured can you please send me a description of the brand, color, and size?'),
('Y',
'Mixed WF bag',
'Your recent order for wash & fold was received and we found items which require special attention. There were dry clean only, hand wash or lay flat to dry items mixed into your order. Please advise on how you would like to proceed:

Remove the item(s) and create a new order to process them according to the care label .
Remove the item(s) and return them unserviced.
Process the entire bag as wash & fold regardless of the care label instructions.'),
('Y',
'Release of Liability',
'The order we received contained an item needing special attention. Due to the delicate nature we will need a release of liability to process the __________________. We take the utmost care of all orders and items received for cleaning, a confirmation via telephone or email to confirm the cleaning is needed.

 Please respond or call customer service (415) 255-7500 x103.'),
 ('Y',
 'First Order',
 'When placing an order without a Laundry Locker bag, feel free to leave your clothes in a garbage bag, hamper, etc. After your first order is processed, it will be returned with a Laundry Locker bag which will be directly linked to your account to use for any future orders. This will prevent any confusion in case you forget to properly claim your locker.');
 
