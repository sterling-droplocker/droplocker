UPDATE location SET zipcode = UPPER(REPLACE(zipcode, ' ', ''));


ALTER TABLE  `taxTable` ADD  `zipcode` VARCHAR( 100 ) NOT NULL;

CREATE TABLE taxTableOld LIKE taxTable;

INSERT INTO taxTableOld (state, taxRate, business_id, zipcode)
SELECT DISTINCT taxTable.state, taxTable.taxRate, taxTable.business_id, location.zipcode
FROM taxTable
JOIN location ON (taxTable.business_id = location.business_id and taxTable.state = location.state)
WHERE location.zipcode != '';

DROP TABLE taxTable;
RENAME TABLE taxTableOld TO taxTable;

