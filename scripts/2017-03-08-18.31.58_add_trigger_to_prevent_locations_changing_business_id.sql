DROP TRIGGER `update_location_timestamp`;

DELIMITER ;;
CREATE TRIGGER `prevent_business_id_change_on_location` BEFORE UPDATE ON `location` FOR EACH ROW BEGIN

IF (NEW.business_id != OLD.business_id) THEN
    SIGNAL sqlstate '45000' SET message_text = "You can not change Business ID of an existing Location.";
END IF;

SET NEW.lastUpdated = CURRENT_TIMESTAMP;

END ;;
DELIMITER ;
