ALTER TABLE `taxGroup` ADD `is_sub_tax` TINYINT(1) UNSIGNED DEFAULT 0;
UPDATE `taxGroup` SET `is_sub_tax` = 1 WHERE `parent_id` > 0;
