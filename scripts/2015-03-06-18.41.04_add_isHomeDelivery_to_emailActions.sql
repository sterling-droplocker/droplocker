ALTER TABLE `emailActions` ADD `isHomeDelivery` TINYINT  NOT NULL  DEFAULT '0'  AFTER `smsText`;
UPDATE emailActions SET isHomeDelivery = 1 WHERE action IN ('new_home_pickup', 'ready_for_home_delivery', 'order_inventoried_home_delivery');
