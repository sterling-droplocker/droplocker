/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  doug
 * Created: Apr 24, 2017
 */

DROP TRIGGER IF EXISTS log_updated_orders_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_orders_to_driver_app_journal AFTER UPDATE ON orders FOR EACH ROW BEGIN
    IF (OLD.orderStatusOption_id != 10 OR NEW.orderStatusOption_id != 10) THEN
      INSERT INTO driver_app_journal
          (tableName, id, event, `business_id`, oldData, newData, `date`)
      VALUES
          ('orders', NEW.orderID, 'update', NEW.business_id, '', '', NOW() );
    END IF;

END ;;
DELIMITER ;

DROP TRIGGER IF EXISTS log_updated_claim_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_claim_to_driver_app_journal AFTER UPDATE ON claim FOR EACH ROW BEGIN
    IF (OLD.active = 1 OR NEW.active = 1) THEN
      INSERT INTO driver_app_journal
          (tableName, id, event, `business_id`, oldData, newData, `date`)
      VALUES
          ('claim', NEW.claimID, 'update', NEW.business_id, '', '', NOW() );
    END IF;
END ;;
DELIMITER ;

