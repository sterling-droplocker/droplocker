ALTER TABLE  `laundryPlan` ADD  `expired_price` DECIMAL( 10, 2 ) NULL DEFAULT NULL;
ALTER TABLE  `businessLaundryPlan` ADD  `expired_price` DECIMAL( 10, 2 ) NULL DEFAULT NULL;
