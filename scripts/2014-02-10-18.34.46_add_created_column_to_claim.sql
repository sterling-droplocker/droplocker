ALTER TABLE  `claim` CHANGE  `updated`  `updated` TIMESTAMP NULL DEFAULT NULL,
ADD `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE `claim` SET `created` = `updated`;

