-- Create the paypal country
INSERT INTO businessMeta (`key`, business_id, value)
SELECT 'paypal_country', businessID,
CASE country
    WHEN 'usa' THEN 'US'
    WHEN 'canada' THEN 'CA'
    WHEN 'australia' THEN 'AU'
    WHEN 'netherlands' THEN 'NL'
    WHEN 'mexico' THEN 'MX'
    WHEN 'uk' THEN 'GB'
    WHEN 'france' THEN 'FR'
END AS `value`
FROM business
JOIN businessMeta ON (businessID = business_id AND `key` = 'paypal_user');


-- Create the paypal currency
INSERT INTO businessMeta (`key`, business_id, value)
SELECT 'paypal_currency', businessID,
CASE country
    WHEN 'usa' THEN 'USD'
    WHEN 'canada' THEN 'CAD'
    WHEN 'australia' THEN 'AUD'
    WHEN 'netherlands' THEN 'EUR'
    WHEN 'mexico' THEN 'MXN'
    WHEN 'uk' THEN 'GBP'
    WHEN 'france' THEN 'EUR'
END AS `value`
FROM business
JOIN businessMeta ON (businessID = business_id AND `key` = 'paypal_user');
