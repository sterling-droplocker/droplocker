-- will use location column and remove the redundant lockers_location

ALTER TABLE  `location` CHANGE  `location`  `location` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

UPDATE `location` SET 
    `location` = `lockers_location` 
WHERE 
    `lockers_location` != '' 
    AND (`location` IS NULL OR `location` = '');

ALTER TABLE  `location` DROP  `lockers_location`;
