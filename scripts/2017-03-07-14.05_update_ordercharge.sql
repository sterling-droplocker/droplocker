/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  doug
 * Created: Mar 7, 2017

Run this to get insert statements of orderCharges which need to be created.  These will now
be created automatically with DROP-4680.
 */

SELECT CONCAT("INSERT INTO orderCharge (order_id, chargeType, chargeAmount, updated) VALUES (", originalOrderId, ", 'Payed with Order ", payingOrderId, "', ", chargeAmount, ", NOW());") As inserts                
from
(
  select
    if (@prev != payingOrder.OrderId, payingCustomer.customerId, '') payingCustomerId, 
    if (@prev != payingOrder.OrderId, payingCustomer.firstName, '') payingCustomerFirstName, 
    if (@prev != payingOrder.OrderId, payingCustomer.lastname, '') payingCustomerLastName,
    payingOrder.orderId payingOrderId, 
    if (@prev != payingOrder.OrderId, payingOrder.closedGross + payingOrder.closedDisc, '') totalPaidAmount,
    payingCharge.chargeType chargeNote, payingCharge.chargeAmount,
    originalCustomer.customerId originalCustomerId, originalCustomer.firstName originalCustomerFirstName, originalCustomer.lastname originalCustomerLastName, 
    originalOrder.OrderId originalOrderId, originalOrder.closedGross + originalOrder.closedDisc originalAmount,
    @prev := payingOrder.OrderId
  FROM orderCharge payingCharge
  LEFT OUTER JOIN orders payingOrder
    ON payingCharge.order_id = payingOrder.orderid
  LEFT OUTER JOIN customer payingCustomer
    ON payingOrder.customer_id = payingCustomer.customerid
  LEFT OUTER JOIN orders originalOrder
    ON replace(chargeType, 'Order ', '') = originalOrder.OrderId
  LEFT OUTER JOIN customer originalCustomer
    ON originalCustomer.customerId = originalOrder.customer_id
  JOIN (SELECT @prev := 0) x
    ON 1=1
  WHERE chargeType REGEXP '^Order [0-9]'
    AND updated >= '2017'
    AND payingOrder.business_id = 3
) charges;