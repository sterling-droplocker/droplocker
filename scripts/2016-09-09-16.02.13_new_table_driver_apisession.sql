CREATE TABLE `driver_apiSession` (
  `driver_apiSessionID` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `expireDate` datetime NOT NULL,
  `days` int(11) NOT NULL,
  PRIMARY KEY (`driver_apiSessionID`)
) ENGINE=InnoDB;
