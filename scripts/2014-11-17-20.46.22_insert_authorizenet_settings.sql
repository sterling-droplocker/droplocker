-- Create the authorizenet country
INSERT INTO businessMeta (`key`, business_id, value)
SELECT 'authorizenet_country', businessID,
CASE country
    WHEN 'netherlands' THEN 'NL'
    WHEN 'uk' THEN 'GB'
    WHEN 'spain' THEN 'ES'
END AS `value`
FROM business
JOIN businessMeta ON (businessID = business_id AND `key` = 'api_login_id');

-- Create the authorizenet country
INSERT INTO businessMeta (`key`, business_id, value)
SELECT 'authorizenet_extra', businessID,
CASE country
    WHEN 'netherlands' THEN 1
    WHEN 'uk' THEN 1
    WHEN 'spain' THEN 1
    ELSE 0
END AS `value`
FROM business
JOIN businessMeta ON (businessID = business_id AND `key` = 'api_login_id');
