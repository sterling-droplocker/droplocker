CREATE TABLE `dataDumpsLog` (
  `dataDumpsLogID` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `dumpedTable` varchar(45) NOT NULL,
  `format` varchar(45) NOT NULL,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`dataDumpsLogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
