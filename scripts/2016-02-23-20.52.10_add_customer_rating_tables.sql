CREATE TABLE `customerRating` (
  `customerRatingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rating` tinyint(3) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `comment` text,
  `created` datetime DEFAULT NULL,
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerRatingID`),
  KEY `customer` (`customer_id`,`archived`)
) ENGINE=InnoDB