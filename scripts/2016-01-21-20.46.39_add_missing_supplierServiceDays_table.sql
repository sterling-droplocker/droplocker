CREATE TABLE `supplierServiceDays` (
  `supplierServiceDaysID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `day_id` int(50) NOT NULL,
  PRIMARY KEY (`supplierServiceDaysID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;