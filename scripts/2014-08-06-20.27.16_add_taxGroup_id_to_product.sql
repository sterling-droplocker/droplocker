ALTER TABLE  `product_process` ADD  `taxGroup_id` INT UNSIGNED NULL;

UPDATE product_process JOIN product ON (product_id = productID)
SET taxGroup_id = (
    SELECT taxGroupID FROM taxGroup
    WHERE taxGroup.business_id = product.business_id
    LIMIT 1
);
