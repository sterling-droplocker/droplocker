alter table `orderHomePickup` change windowStart windowStart timestamp;
alter table `orderHomePickup` change windowEnd windowEnd timestamp;

alter table `orderHomeDelivery` change windowStart windowStart timestamp;
alter table `orderHomeDelivery` change windowEnd windowEnd timestamp;