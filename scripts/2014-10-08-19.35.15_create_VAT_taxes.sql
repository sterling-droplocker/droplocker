-- Create VAT taxGroup
INSERT INTO taxGroup (business_id, name)
SELECT b1.business_id, 'VAT'
FROM businessMeta AS b1
JOIN businessMeta AS b2 ON (b1.business_id = b2.business_id AND b2.`key` = 'breakout_vat' AND b2.value = 1)
WHERE b1.`key` = 'vat_amount';

-- Delete old default taxGroup setting
DELETE b1 FROM businessMeta AS b1
JOIN businessMeta AS b2 ON (b1.business_id = b2.business_id AND b2.`key` = 'breakout_vat' AND b2.value = 1)
WHERE b1.`key` = 'default_taxGroup';

-- Set default taxGroup to VAT
INSERT INTO businessMeta (business_id, `key`, value)
SELECT business_id, 'default_taxGroup', taxGroupID
FROM taxGroup
WHERE name = 'VAT'
GROUP BY business_id
ORDER BY business_id, taxGroupID;


-- Add VAT for each location
INSERT INTO taxTable (taxGroup_id, taxRate, business_id, zipcode)
SELECT DISTINCT taxGroupID, b1.value, b1.business_id, location.zipcode
FROM businessMeta AS b1
JOIN businessMeta AS b2 ON (b1.business_id = b2.business_id AND b2.`key` = 'breakout_vat' AND b2.value = 1)
JOIN location ON (b1.business_id = location.business_id)
JOIN taxGroup ON (b1.business_id = taxGroup.business_id)
WHERE b1.`key` = 'vat_amount'
AND location.zipcode != ''
AND taxGroup.name = 'VAT'
ORDER BY b1.business_id;

-- delete taxTable from taxTable JOIN taxGroup ON (taxGroupID = taxGroup_id) where taxGroup.name = 'VAT';


-- Add the vat_breakout field to orderTax
ALTER TABLE  `orderTax` ADD  `vat_breakout` TINYINT UNSIGNED NOT NULL DEFAULT  '0';

-- Create the order taxes
INSERT INTO orderTax (order_id, taxGroup_id, zipcode, amount, taxRate, vat_breakout)
SELECT DISTINCT orderID, taxGroupID, location.zipcode, (closedGross + closedDisc) * taxRate , taxRate, 1
FROM businessMeta AS b1
JOIN businessMeta AS b2 ON (b1.business_id = b2.business_id AND b2.`key` = 'breakout_vat' AND b2.value = 1)
JOIN orders ON (orders.business_id = b1.business_id)
LEFT JOIN taxGroup ON (taxGroup.business_id = orders.business_id AND taxGroup.name = 'VAT')
LEFT JOIN locker ON (lockerID = locker_id)
LEFT JOIN location ON (locationID = location_id)
LEFT JOIN taxTable ON (taxGroupID = taxGroup_id AND taxTable.zipcode = location.zipcode)
WHERE tax = 0 
AND (closedGross != 0 OR closedDisc != 0);

-- Update the orders tax
UPDATE orders
JOIN businessMeta AS b2 ON (b2.business_id = orders.business_id AND b2.`key` = 'breakout_vat' AND b2.value = 1)
SET orders.tax = (select SUM(amount) FROM orderTax WHERE order_id = orderID);

-- Set the products to use the VAT tax
UPDATE product_process
JOIN product ON (productID = product_id)
JOIN taxGroup ON (taxGroup.business_id = product.business_id AND taxGroup.name = 'VAT')
SET taxGroup_id = taxGroupID
WHERE taxable = 1;
