alter table businessLaundryPlan add type varchar(20) default 'laundry_plan';
alter table businessLaundryPlan add credit_amount double;
alter table businessLaundryPlan add expired_discount_percent int;
