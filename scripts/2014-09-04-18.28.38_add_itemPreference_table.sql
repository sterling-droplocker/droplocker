CREATE TABLE  `itemPreference` (
`itemPreferenceID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`item_id` INT UNSIGNED NOT NULL ,
`productCategory_id` INT UNSIGNED NOT NULL ,
`product_id` INT UNSIGNED NOT NULL ,
`updated` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL ,
UNIQUE (`item_id` , `productCategory_id`)
) ENGINE = INNODB;

