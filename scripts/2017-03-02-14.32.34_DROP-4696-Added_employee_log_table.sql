CREATE TABLE employee_log
(
employeeLogID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
employee_id INT NOT NULL,
from_employee_id INT,
business_id INT NOT NULL,
ip_address VARCHAR(15) NOT NULL,
dateStatus DATETIME
);
CREATE INDEX employee_log_business_id_index ON employee_log (business_id);
CREATE INDEX employee_log_employee_id_index ON employee_log (employee_id);
