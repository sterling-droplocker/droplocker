-- --------------------------------------------------------------
-- ------------------- stops  -----------------------------------
-- --------------------location
-- only checking route for now
DROP TRIGGER IF EXISTS log_updated_location_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_location_to_driver_app_journal AFTER UPDATE ON location FOR EACH ROW BEGIN

    IF (OLD.route_id != NEW.route_id) THEN
        INSERT INTO driver_app_journal
            (tableName, id, event, `business_id`, oldData, newData, `date`)
        VALUES
            ('location', NEW.locationID, 'update_route', NEW.business_id, OLD.route_id, NEW.route_id, NOW() );
    END IF;
END ;;
DELIMITER ;

