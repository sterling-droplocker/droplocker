create table customerItemHistory (
customerItemHistoryID int auto_increment primary key,
from_customer_id int,
to_customer_id int,
employee_id int,
barcode varchar(20), 
orderID int,
date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
