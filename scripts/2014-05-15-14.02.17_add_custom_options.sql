CREATE TABLE IF NOT EXISTS `customOption` (
  `customOptionID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_id` INT UNSIGNED NOT NULL,
  `sortOrder` INT NOT NULL DEFAULT 0,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`customOptionID`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customOptionValue` (
  `customOptionValueID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `customOption_id` INT UNSIGNED NOT NULL,
  `business_id` INT UNSIGNED NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  `default` BOOL NOT NULL DEFAULT 0,
  `sortOrder` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`customOptionValueID`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customOptionName` (
  `customOptionNameID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customOption_id` int(10) UNSIGNED NOT NULL,
  `business_language_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`customOptionNameID`),
  UNIQUE KEY `customOption_id` (`customOption_id`,`business_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `customOptionValueName` (
  `customOptionValueNameID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customOptionValue_id` int(10) UNSIGNED NOT NULL,
  `business_language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`customOptionValueNameID`),
  UNIQUE KEY `customOption_id` (`customOptionValue_id`,`business_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
