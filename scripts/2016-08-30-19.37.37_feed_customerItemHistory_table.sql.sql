insert into customerItemHistory
select null, from_customer_id, to_customer_id, employee_id, barcode, orderID, updated from (
	select 
		@pre_customer_id as from_customer_id,
		@pre_customer_id := a.customer_id as to_customer_id,
		a.barcode,
		a.orderID,
		a.updated
	from 
	(
		select o.orderID, o.customer_id, b.barcode, oi.updated FROM 
			orders o join orderItem oi on oi.order_id = o.orderID
			join barcode b on b.item_id = oi.item_id
			join (
				/* X0: Barcodes whose customer has changed */
				SELECT distinct barcode
				FROM
				( 
					SELECT 
						 (@customer_idPre <> o.customer_id AND @barcodePre=b.barcode) AS customerChanged
						 , o.customer_id, b.barcode
						 , @customer_idPre := o.customer_id
						 , @barcodePre := b.barcode
						 , oi.updated
					FROM 
						orders o join orderItem oi on oi.order_id = o.orderID
						join barcode b on b.item_id = oi.item_id
					   , (SELECT @customer_idPre:=NULL, @barcodePre:=NULL) AS d
					   where o.orderType = 'DC'
					ORDER BY b.barcode, oi.updated, o.customer_id
				) AS x0
		WHERE customerChanged 
	) as c
on c.barcode = b.barcode
order by b.barcode, oi.updated) a,
(SELECT @pre_customer_id:=NULL) AS d
) as x1
join orderStatus os on os.order_id = x1.orderID and os.note like CONCAT('%', x1.barcode, '%')
where x1.from_customer_id is not null
and x1.from_customer_id <> x1.to_customer_id
order by x1.barcode, x1.updated asc
