ALTER TABLE orderStatusOption ADD COLUMN deletedAt DATETIME DEFAULT null;
UPDATE orderStatusOption SET deletedAt = now() WHERE orderStatusOptionID=11;
UPDATE orderStatusOption SET deletedAt = now() WHERE orderStatusOptionID=12;
