ALTER TABLE  `orderHomeDelivery` ADD  `trackingCode` VARCHAR( 255 ) NULL;

UPDATE orderHomeDelivery
SET trackingCode = SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(delivery_response, ',', 3), '"', -2), '"', 1);
