-- --------------------------------------------------------------
-- ------------------- driverNotes -----------------------------------
DROP TRIGGER IF EXISTS log_new_driverNotes_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_new_driverNotes_to_driver_app_journal AFTER INSERT ON driverNotes FOR EACH ROW 
BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('driverNotes', NEW.driverNotesID, 'insert', NEW.business_id, '', '', NOW() );
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_updated_driverNotes_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_updated_driverNotes_to_driver_app_journal AFTER UPDATE ON driverNotes FOR EACH ROW BEGIN

    IF (OLD.active = 1 OR NEW.active = 1) THEN
        INSERT INTO driver_app_journal
            (tableName, id, event, `business_id`, oldData, newData, `date`)
        VALUES
            ('driverNotes', NEW.driverNotesID, 'update', NEW.business_id, '', '', NOW() );
    END IF;
END ;;
DELIMITER ;
-- ------------------------------------------------------
DROP TRIGGER IF EXISTS log_deleted_driverNotes_to_driver_app_journal;
DELIMITER ;;
CREATE TRIGGER log_deleted_driverNotes_to_driver_app_journal AFTER DELETE ON driverNotes FOR EACH ROW BEGIN

    INSERT INTO driver_app_journal
        (tableName, id, event, `business_id`, oldData, newData, `date`)
    VALUES
        ('driverNotes', OLD.driverNotesID, 'delete', OLD.business_id, '', '', NOW() );
END ;;
DELIMITER ;

