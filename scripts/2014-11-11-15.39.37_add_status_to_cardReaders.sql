ALTER TABLE  `cardReader` ADD  `status` ENUM(  'enabled',  'disabled' ) NOT NULL;

UPDATE cardReader SET status = 'enabled';
