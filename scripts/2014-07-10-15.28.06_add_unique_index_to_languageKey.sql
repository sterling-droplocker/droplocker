-- remove duplicated index
ALTER TABLE  `languageView` DROP INDEX  `name_2`;

-- rename duplicated languageKey
UPDATE languageKey SET name = 'up_to_rollover' WHERE languageKeyID = 88;

-- change index to unique on languageView_id + name
ALTER TABLE  `languageKey` DROP INDEX  `languageView_id` ,
ADD UNIQUE  `languageView_id_name` (  `languageView_id` ,  `name` );

