-- delete invalid resources
delete from acl where resource_id in (select id from  aclresources where resource like "orders\_quick\_scan\_bag_%");
delete from aclresources where resource like "orders\_quick\_scan\_bag_%";

-- store repeated resources
create temporary table acl_repeated (id int unsigned primary key);
insert into acl_repeated select id from aclresources group by aclgroup, resource having count(*) > 1;

-- update repeated ids to the non repeated
update acl join aclresources as ra on (acl.resource_id = ra.id) join aclresources as rb on (ra.aclgroup = rb.aclgroup and ra.resource = rb.resource and ra.id != rb.id) set acl.resource_id = rb.id where resource_id in (select id from acl_repeated);

-- remove dupplicates
delete from aclresources where id in (select id from acl_repeated);
drop table acl_repeated;


