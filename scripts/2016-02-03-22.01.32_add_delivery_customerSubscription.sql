CREATE TABLE `delivery_customerSubscription` (
  `delivery_customerSubscriptionID` INT NOT NULL AUTO_INCREMENT,
  `customer_id` INT NOT NULL,
  `delivery_window_id` INT NOT NULL,
  `address1` VARCHAR(100) NULL,
  `address2` VARCHAR(100) NULL,
  `city` VARCHAR(45) NULL,
  `state` VARCHAR(45) NULL,
  `zip` VARCHAR(45) NULL,
  `lat` VARCHAR(15) NULL,
  `lon` VARCHAR(15) NULL,
  `sortOrder` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`delivery_customerSubscriptionID`));

