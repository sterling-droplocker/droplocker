-- add drop_address
ALTER TABLE  `claimExternalPickup` ADD  `drop_address` VARCHAR( 255 ) NULL;

-- set value for old drop_address
UPDATE claimExternalPickup
SET drop_address = SUBSTRING_INDEX(SUBSTRING_INDEX(response, '"dest_address":"', -1), '"', 1)
WHERE drop_address IS NULL;

-- add dropLocation
CREATE TABLE  `dropLocation` (
`dropLocationID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`business_id` INT NOT NULL ,
`address` VARCHAR( 255 ) NOT NULL ,
`default` TINYINT NOT NULL DEFAULT  '0',
INDEX (  `business_id` ,  `address` )
) ENGINE = INNODB;

-- insert default into dropLocation
INSERT INTO dropLocation (business_id, address, `default`)
SELECT business_id, value, 1
FROM businessMeta
WHERE `key` = 'rickshaw_delivery_address';

-- add location_dropLocation
CREATE TABLE  `location_dropLocation` (
`location_dropLocationID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`location_id` INT UNSIGNED NOT NULL ,
`dropLocation_id` INT UNSIGNED NOT NULL ,
UNIQUE (`location_id`, `dropLocation_id`)
) ENGINE = INNODB;
