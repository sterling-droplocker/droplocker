INSERT INTO `emailActions` (`emailActionID` , `orderStatusOption_id` , `action` , `description` , `name` , `subject` , `body` , `bodyText` , `adminSubject` , `adminBody` , `smsSubject` , `smsText`)
VALUES (NULL ,  '0',  'verify_email',  'Sent to the customer when the customer requests to change his email.',  'Verify email',  '',  '',  '',  '',  '',  '',  '');

SELECT emailActionID INTO @emailActionID FROM emailActions WHERE action = 'verify_email';

insert into emailTemplate ( emailAction_id, emailSubject, emailText, business_id, business_language_id )    
select @emailActionID, 'Update Email Address for %business_companyName% Account', '<p>\n        Hi %customer_firstName%,<br />\n        <br />\n        Someone, hopefully you, has requested an update to their %business_companyName% email.<br/><br/>\n        If you did not make this request, please ignore this email.<br/>\n        <br/>\n        Please click on the link below:<br/>\n        <a href="%verify_email_url%">%verify_email_url%</a></p>\n<p>\n        If you did not request this update, please contact us at %business_email%.</p>\n<br />\n<br />\n<p>\n        Sincerely,<br />\n        The %business_companyName% Team</p>', business_id, business_languageID from business_language;
