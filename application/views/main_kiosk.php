<?php
ini_set('display_errors', 'off');
/* $small='';
  $width='';
  if(isset($_COOKIE['small'])){
  if($_COOKIE['small'] == 'yes') {$width = ""; $small='yes';}
  else {$width = ' width: 550px;';}
  } */

//echo '<div style="float: left; padding-left: 5px; width: 550px;">';
?>
<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">
            <br><h1>24 / 7 Kiosks</h1>
            <br>
            Laundry Locker operates 24 x 7 kiosks for dropping off and picking up your dry cleaning and wash & fold at your convenience.  Registered customers can access our kiosks anytime, and take advantage of the same great service we provide to hundreds of apartment buildings throughout San Francisco.
            <br>
            <br>
            <div style="padding-bottom: 8px;"><h2>24 x 7 Kiosk Locations:</h2></div>
            &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?q=1339+Polk+St.+San+Francisco,+CA&sa=X&oi=map&ct=title" target="_blank" class="headingGreen">1339 Polk St.</a> (open 24 x 7)<br>
            &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?q=566+Haight+St,+San+Francisco,+CA+94117,+USA&sa=X&oi=map&ct=title" target="_blank" class="headingGreen">566 Haight</a> (open 24 x 7)<br>
            &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?q=2103+Van+Ness+Ave,+San+Francisco,+CA+94109,+USA&sa=X&oi=map&ct=title" target="_blank" class="headingGreen">2103 Van Ness</a> (open 24 x 7)<br>
            &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?f=q&hl=en&geocode=&q=145+valencia+st,+94103&sll=37.0625,-95.677068&sspn=46.946584,82.265625&ie=UTF8&z=16&iwloc=addr&om=1" target="_blank" class="headingGreen">145 Valencia Street</a> (open 24 x 7)<br>
            &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?f=q&hl=en&geocode=&q=2069+greenwich+st,+94123,+USA&sa=X&oi=map&ct=title" target="_blank" class="headingGreen">2069 Greenwich</a> (open 24 x 7)<br>
            &nbsp;&nbsp;&nbsp;<a href="http://maps.google.com/maps?f=q&hl=en&geocode=&q=140+brannan+st,+94105,+USA&sa=X&oi=map&ct=title" target="_blank" class="headingGreen">140 Brannan</a> (open 24 x 7)<br>
            <br>
            To use our kiosks, you need to first <a href="/register" class="greenLink">sign up as a new user</a>.  Once you establish an account, you can give yourself access to our kiosk.
            <br>
            <div style="padding-left: 15px;">
                <table width="100%" border="0" cellspacing="4" cellpadding="0">
                    <tr class="bodyBold">
                        <td colspan="3" class="headingBlue">How it works:<br></td>
                    </tr>
                    <tr class="bodyBig">
                        <td valign="top">1.</td>
                        <td></td>
                        <td valign="top"><a href="/register" class="greenLink">Register as a first time user</a>.  If you are already registered, skip to step 2.</td>
                    </tr>
                    <tr class="bodyBig">
                        <td valign="top">2.</td>
                        <td></td>
                        <td valign="top">Login to your account and click on the <strong>Kiosk Access</strong> link.</td>
                    </tr>
                    <tr class="bodyBig">
                        <td valign="top">3.</td>
                        <td></td>
                        <td valign="top">Enter some basic information from the credit card that you would like to use for kiosk access.</td>
                    </tr>
                    <tr class="bodyBig">
                        <td valign="top">4.</td>
                        <td></td>
                        <td valign="top">Follow our standard <a href="/main/howto" class="greenLink">locker drop off process</a>.</td>
                    </tr>
                </table>
            </div>
            <br>
            <span class="bodyBold">Please Note:</span> Our kiosks are secured and monitored 24 x 7.  If you are not a registered user and you have not setup your access, you will not be able to gain access to our kiosk.
            </span>
        </div>

        <?php if ($small <> 'yes'): ?>
            <div class="span4">
               <div id="howtoVideo">
              
                    <iframe src="https://www.youtube.com/v/2-qChq7VSSs" frameborder="0" allowfullscreen=""></iframe>
                    <p class="bodyBold">Watch our kiosk on KRON 4 News with Tech Reporter<br />Gabriel Slate <br /><img src="/images/logo_KRON.jpg" alt=""></p>
                       
                

            </div>
        <?php endif; ?>
    </div>
</div>
</div>