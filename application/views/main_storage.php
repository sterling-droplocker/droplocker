<div style="float:left; width: 640px; margin-left: 5px;">
	<div class="bodyBig" style="margin-top: 30px; float: left;">
	<h1>Clothing Storage</h1><br>
        <div class="visible-phone" style="text-align: center">
            <?= servicesNav('storage');?>
        </div>
	Closets overflowing? Downsizing? Trying to Fung Shui your apartment?
<br>
<br>
Laundry Locker is now offering clothing storage!! 
<br>
<br>
With our same great convenience and online management as our laundry and dry cleaning service, you can now store your off season items with us and we will deliver them back to a locker or your building at your request!  What could be simpler than that?  Have a vacation you are taking and need that aloha dress out of your storage?  You simply log on to your closet 2.0, order the dress (choose to have it cleaned or not) and we will deliver to you from your storage with our same great delivery service!
<br>
<br>
Here are some great ideas of things to get out of your closet and store with us to simplify your life:
<br>
<ul>
<li>Evening/Cocktail/Fancy Dresses <br>
<li>Ski clothes<br>
<li>Winter coats<br>
<li>Summer clothes inc bathing suits  (we know we never need these in SF!)<br>
<li>Halloween costumes<br>
<li>Items that you don't wear often (we can give away to goodwill at your request if you don't find yourself wearing it)<br>
<li>Anything that doesn't quite fit right now...but you are sure will again some day (we all have them!)<br>
<li>Maternity clothes<br>
<li>Outgrown baby clothes<br>
<li>Wedding gowns<br>
<li>Tuxedos<br>
</ul>
We are always able to deliver back to you in time for that ski vacation, summer wedding, or special event that you need those garments for! 
<br><br>
Costs to Store Items with Laundry Locker:
<br><ul>
<li>$.50 per month/per item
<li>$1.00 per month/per oversized item (comforters, large coats, fancy long dresses)
<br><li>$5.00 per month/per bag  (fill the bag as full as you'd like and we will store it)  *no photos or delivery of individual items, bulk service only
<br><li>$1.00 per item pickup / inventorying fee
<br><li>$1.00 per item delivery fee (waived if items are cleaned first)
<br><li>Complimentary option to donate to charity at any time if you find that you are no longer wearing the items!
</ul>
<br>
Here's how it works:
<br><ol><li>Place your order the normal way
<br><li>Put a note in your order saying "Please store these items"
<br><li>Make sure to separate storage items from regular items to be cleaned
<br><li>Browse your Closet 2.0 account to see photos of all your stored items
<br><li>When you need an item delivered back to you, simply click "deliver to me" on the Closet 2.0 photo of the garment
<br><li>Choose whether you would like the item cleaned or not before being returned
<br><li>Take a key to a locker (if you are in a locker location) and let us know which one.
<br><li>Pick up your clothes NEXT DAY!!
</ol><br><br>
All orders are billed on the 1st of the month through the credit card that you have on file with Laundry Locker!  Storage can be cancelled at any time by requesting delivery back of the item.
<br>

	</div>
</div>

<div style="float: right; width:320px;">
    <div class="hidden-phone">
       <?= servicesNav('storage');?>
    </div>
</div>
