<?php enter_translation_domain("admin/login/index"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo __("Drop Locker Business Admin Login", "Drop Locker Business Admin Login"); ?></title>
        <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        <link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
        <link rel='stylesheet' type="text/css" href='/css/admin/login.css' />
        <link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
    </head>
    <body>
        <div id='head'><h2><?php echo __("Drop Locker Business Admin", "Drop Locker Business Admin"); ?></h2></div>
        <div id='login_box'>
            <div style='padding:10px;'><?= $this->session->flashdata("status_message") ?></div>
            <form method='post' action=''>
                <table>
                    <tr>
                        <th align="right"><?php echo __("Email/Username", "Email/Username"); ?></th>
                        <td><input size='30' type='text' name='email_or_username' /></td>
                    </tr>
                    <tr>
                        <th align="right"><?php echo __("Password", "Password"); ?></th>
                        <td><input size='30' type='password' name='password' /></td>
                    </tr>
                </table>
                <input type='hidden' value='<?= isset($redirect)?$redirect:''?>' name='redirect' />
                <input class='button orange' type='submit' value='<?php echo __("Login", "Login"); ?>' name='submit' />
            </form>
            <div style='text-align:left; padding:0px 20px;'><a style='color:#333;' href='/admin/login/display_forgot_password_form'><?php echo __("Forgot password", "Forgot password?"); ?></a></div>
        </div>
        <?php $CI = get_instance(); ?>
        <?php if (DEV): ?>
            <div style='margin-top:10px;'>
                <p> Document Root: <?= $_SERVER['DOCUMENT_ROOT'] ?> </p>
                <p> Database Hostname: <?= $CI->db->hostname ?> </p>
                <p> Database: <?= $CI->db->database?> </p>
            </div>
        <?php endif ?>
    </body>
</html>
