<?php enter_translation_domain("admin/login/display_forgot_password_form"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo __("Forgot Password", "Forgot Password"); ?></title>
        <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        <link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
        <link rel='stylesheet' type='text/css' href='/css/admin/login.css' />
    </head>
    <body>
        <?= $this->session->flashdata('status_message');?>
        <div id='head'>
            <h2> <?php echo __("DropLocker Forgot Password", "DropLocker Forgot Password"); ?> </h2>
        </div>
        <div id='login_box'>
            <?= $this->session->flashdata('message')?>
            <div style="text-align: left;padding:10px">
                <?php echo __("Please enter the email address for your account", "Please enter the email address for your account. If you don't know the email address, please contact <a href='mailto://support@droplocker.com?subject=Trouble logging into admin section'>support</a>."); ?> 
            </div>
            <form method='post' action='/admin/login/send_forgot_password_email'>
                <table>
                    <tr>
                        <th align="right"><?php echo __("Email", "Email"); ?></th>
                        <td>
                            <input size='40' type='text' name='email' />
                        </td>
                    </tr>
                </table>
                <input type='hidden' value='<?= $redirect?>' name='redirect' />
                <input class='button orange' type='submit' value='<?php echo __("Send Verification Email", "Send Verification Email"); ?>' name='submit' />
            </form>
            <div style='text-align:left; padding:0px 20px;'><a style='color:#333;' href='/admin/login'><?php echo __("Login Here", "Login Here"); ?></a></div>
        </div>
    </body>
</html>
