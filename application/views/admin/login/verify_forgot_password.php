<?php enter_translation_domain("admin/login/verify_forgot_password"); ?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/css/admin/style.css" />
         <link rel='stylesheet' type='text/css' href='/css/admin/login.css' />
    </head>
    <body style='text-align: center;padding:100px;'>
        <?= $this->session->flashdata('status_message')?>
        <div id='head'>
            <h2><?php echo __("Reset Password", "Reset Password"); ?></h2>
        </div>
        <div id='login_box'>
            <form action='' method='post' style='margin:15px;'>
                <table class="detail_table" style='margin-bottom: 10px;'>
                    <tr>
                        <th style='width:30%'><?php echo __("Password", "Password"); ?></th>
                        <td style='width:70%'><input style='width:100%' type='password' name='password' /></td>
                    </tr>
                    <tr>
                        <th><?php echo __("Password Again", "Password Again"); ?></th>
                        <td><input type='password' style='width:100%' name='password2' /></td>
                    </tr>
                </table>
                <input type='submit' name='submit' value='<?php echo __("Submit", "Submit"); ?>' class='button orange' />
            </form>
        </div>
    </body>
</html>