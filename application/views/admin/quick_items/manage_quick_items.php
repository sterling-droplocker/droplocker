<?php enter_translation_domain("admin/quick_items/manage_quick_items"); ?>
<h2><?php echo __("Quick Item Buttons", "Quick Item Buttons"); ?></h2>
<p>
    <a href="/admin/quick_items/add"><img src="/images/icons/add.png"> <?php echo __("Add New Quick Item", "Add New Quick Item"); ?></a>
</p>

<?= $this->session->flashdata('message')?>

<table class="box-table-a">
    <thead>
        <tr>
            <th> <?php echo __("ID", "ID"); ?> </th>
            <th><?php echo __("Name", "Name"); ?></th>
            <th><?php echo __("Product", "Product"); ?></th>
            <th><?php echo __("Process", "Process"); ?></th>
            <th><?php echo __("Date Created", "Date Created"); ?></th>
            <th align="center" style='width:25px;'><?php echo __("Edit", "Edit"); ?></th>
            <th align="center" style='width:25px;'><?php echo __("Delete", "Delete"); ?></th>
        </tr>
    </thead>
    <tbody>
        <? if($quickItems):
            foreach($quickItems as $quickItem):?>
                <tr>
                    <td> <?= $quickItem->quickItemID ?> </td>
                    <td> <?= $quickItem->name?> </td>
                    <td> <?= $quickItem->product_name ?> </td>
                    <td> <?= $quickItem->process_name ?> </td>
                    <td> <?= convert_from_gmt_aprax($quickItem->dateCreated, "F j, Y", $quickItem->business_id) ?> </td>
                    <td align='center'>
                        <a id="edit-<?=$quickItem->quickItemID?>" href='/admin/quick_items/edit/<?= $quickItem->quickItemID?>'><img src='/images/icons/application_edit.png' /></a>
                    </td>
                    <td align='center'>
                        <a onClick='return verify_delete();' id="delete-<?=$quickItem->quickItemID?>" href='/admin/quick_items/delete/<?= $quickItem->quickItemID?>' ><img src='/images/icons/cross.png' /></a>
                    </td>
                </tr>
        <? endforeach; 
            else:?>
                <tr>
                    <td colspan="5"><?php echo __("No Quick Item Buttons", "No Quick Item Buttons"); ?></td>
                </tr>
        <? endif?>
    </tbody>
</table>
