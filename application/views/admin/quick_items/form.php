<?php enter_translation_domain("admin/quick_items/form"); ?>
<?php

// add show_all category
$productCategories = array('show_all' => __("SHOW ALL", "-- SHOW ALL --")) + (array)$productCategories;

if (empty($currentProductCategory_id)) {
    $currentProductCategory_id = 'show_all';
}

?>

<? if (empty($item->quickItemID)): ?>
    <h2><?php echo __("Add Quick Item", "Add Quick Item"); ?></h2>
<? else: ?>
    <h2><?php echo __("Edit Quick Item", "Edit Quick Item"); ?></h2>
<? endif; ?>
<p>
    <a href="/admin/quick_items"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Quick Items", "Back to Quick Items"); ?></a>
</p>

<?= $this->session->flashdata('message')?>

<form method="post" action="/admin/quick_items/save">

    <table class="detail_table" id="item_options">
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <td>
                <input type='text' name='name' id='item_name' value="<?= isset($item->name)?$item->name:''?>"/>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Product", "Product"); ?></th>
            <td>
                <?= form_dropdown('category', $productCategories, $currentProductCategory_id, 'id="productCategory" tabindex="10000"') ?>
                <select id='product_id' name='product_id' tabindex="9999"></select>
            </td>
        </tr>
        <tr>
            <th style="vertical-align: top"><?php echo __("Cleaning Method", "Cleaning Method"); ?></th>
            <td id="processes"></td>
        </tr>
    </table>

    <? if (empty($item->quickItemID)): ?>
        <input type='submit' id='quick_item_button' value='<?php echo __("Create Quick Item", "Create Quick Item"); ?>' class='button orange'/>
    <? else: ?>
        <input type="hidden" name="quickItemID" value="<?= $item->quickItemID ?>">
        <input type='submit' id='quick_item_button' value='<?php echo __("Update Quick Item", "Update Quick Item"); ?>' class='button orange'/>
    <? endif; ?>

</form>


<script type="text/javascript">
$.blockUI({message : "<h1> <?php echo __("Loading", "Loading..."); ?> </h1"});

// all the categories
var categories_products = <?= json_encode($categories_products); ?>;

// all the products for each category
var products_product_processes = <?= json_encode($products_product_processes); ?>;

// the item
var item = {};
item.product_id = <?= json_encode($product_id); ?>;
item.process_id = <?= json_encode($process_id); ?>;

$(document).ready(function() {

    // load products when the category change
    $("#productCategory").change(function() {
        var productCategory_id = $(this).val();
        
        if (!productCategory_id) {
            productCategory_id = "show_all";
        }

        var products = categories_products[productCategory_id];
        $("#product_id").empty();

        if (products === undefined) {
            $("<div class='ui-state-error' id='product_error_container'> There are no products in this category </div>").insertAfter("#product_id");
            return;
        }

        $("#product_error_container").remove();
        $('<option value=""><?php echo __("Select Product", "--Select Product--"); ?></option>').appendTo("#product_id");

        $.each(products, function(index, properties) {
            $option = $("<option>");

            $option.attr('value', properties.productID);
            $option.text(properties.name);

            if(product_id == properties.productID){
                $option.attr('selected', 'selected');
            }
                
            $option.appendTo("#product_id");
        });

    });

    // load the processes when the product changes
    $("#product_id").change(function(evt) {
        var product_id = $(this).val();
        var processes = products_product_processes[product_id];

        $("#processes").empty();

        if (typeof(processes) == "undefined" || !processes.length) {
            $("<p> <?php echo __("There are no processes defined for this product", "There are no processes defined for this product"); ?> </p>").appendTo("#processes");
            return;
        }

        $.each(processes, function(index, process) {
            $container = $("<div>");
            $container.css({
                "background-image": "url(/images/process/" + process.processID + ".png)",
                "background-repeat": "no-repeat",
                "background-position": "center",
                "width": "50px",
                "height": "60px",
            });

            $container.attr("class", "radio_selector process");
            $container.attr("id", "process-" + process.processID);

            $input = $("<input type='radio' name='process_id' style='display:none;'>");
            $input.attr("value", process.processID);
            $input.appendTo($container);

            $container.appendTo("#processes");
            if (processes.length < 2) {
                $container.trigger("click");
            }
        });
    });

    // generic handler for radio selectors
    $('#item_options').on('click', '.radio_selector', function(evt) {
        var $input = $('input', this);
        var type = $input.attr('type');
        
        if ($(this).hasClass('selected')) {
            $(this).removeClass("selected");
            $('input', this).attr("checked", false);
        } else {
            $(this).addClass("selected");
            $('input', this).attr("checked", true);

            if (type == 'radio') {
                var $siblings = $('.radio_selector', $(this).parent()).not(this);
                $siblings.removeClass("selected");
                $('input', $siblings).attr("checked", false);
            }
        }
    });

    // intercept the 'enter' and trigger a click on the create item button.
    $("html").keypress(function(event) {
        if (event.keyCode == 13) {
            $("#create_item_button").trigger("click");
        }
    });

    $("#productCategory").trigger("change");
    $('#product_id').val(item.product_id);

    $("#product_id").trigger("change");
    $("#process-" + item.process_id).not('.selected').trigger("click");
    
    $.unblockUI(); //remove the loading modal
});

</script>
