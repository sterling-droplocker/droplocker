<?php enter_translation_domain("admin/reports/license_invoice_view"); ?>
<h2><?php echo __("License Invoice for", "License Invoice for"); ?> <?= $invoice[0]->companyName ?> for <?= date("M Y", strtotime($invoice[0]->month)) ?></h2>
<div align="right" style="padding-top: 10px; padding-bottom: 10px;">
<form action="/admin/reports/dl_invoices_view" method="post">
<input type="hidden" name="invId" value="<?= $invoice[0]->licenseInvoiceID ?>">
    <? if (is_superadmin()): ?>
    <button id="export"><?php echo __("Export to QuickBooks", "Export to QuickBooks"); ?></button>
    <? endif; ?>
<? if($details == 1): ?>
    <input type="hidden" name="details" value="0">
    <input type="submit" value="<?php echo __("Hide Details", "Hide Details"); ?>">
<? else: ?>
    <input type="hidden" name="details" value="1">
    <input type="submit" value="<?php echo __("Show Details", "Show Details"); ?>">
<? endif; ?>
</form>
</div>
<table id="box-table-a">
<tbody>
    <tr>
        <td style="border-top: 1px solid #CCCCCC;"><?php echo __("Lockers", "Lockers"); ?></td>
        <td align="right" style="border-top: 1px solid #CCCCCC;"><?= $lockerCount ?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <? if($details): ?>
    <tr>
        <td></td>
        <td>
            <table id="box-table-a">
            <tr>
                <th><?php echo __("Location", "Location"); ?></th>
                <th><?php echo __("Locker", "Locker"); ?></th>
                <th align="right"><?php echo __("Price", "Price"); ?></th>
                <? if($this->session->userdata('super_admin') == 1): echo '<th></th>'; endif; ?>
            </tr>
            <? foreach($lockers as $l): ?>
            <tr>
                <td><?= empty($l->address) ? __("Deleted Locker", "Deleted Locker") : $l->address ?></td>
                <td><?= empty($l->lockerName) ? __("Deleted Locker", "Deleted Locker") : $l->lockerName ?></td>
                <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $l->price) ?></td>
                <? if($this->session->userdata('super_admin') == 1): ?>
                <td><form method='post' onSubmit="return verify_delete();" action='/admin/superadmin/license_invoicing_delete_item/<?= $l->licenseInvoiceDetailID ?>'><input type="image" id="delete" src='/images/icons/cross.png' /><input type='hidden' value='<?= $l->licenseInvoice_id ?>' name='invId' /></form></td>
                <? endif; ?>
            </tr>
            <? endforeach; ?>
            </table>
        </td>
    </tr>
    <? endif; ?>
    <tr>
        <td><?php echo __("Total Locker License Fee for", "Total Locker License Fee for"); ?> <?= date("M Y", strtotime($invoice[0]->month)) ?></td>
        <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $lockerTotal) ?></td>
    </tr>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td><?php echo __("Non-Locker Transactions from", "Non-Locker Transactions from"); ?> <?= date("M Y", strtotime($invoice[0]->month . "-1 month")) ?></td>
        <td align="right"><?= $tranCount ?></td>
    </tr>
    <? if($details): ?>
    <tr>
        <td></td>
        <td>
            <table id="box-table-a">
            <tr>
                <th><?php echo __("Order", "Order"); ?></th>
                <th><?php echo __("Customer", "Customer"); ?></th>
                <th><?php echo __("Date Created", "Date Created"); ?></th>
                <th align="right"><?php echo __("Amount", "Amount"); ?></th>
                <th><?php echo __("Location", "Location"); ?></th>
                <th><?php echo __("Locker", "Locker"); ?></th>
                <th align="right"><?php echo __("Price", "Price"); ?></th>
                <? if($this->session->userdata('super_admin') == 1): ?>
                    <th></th>
                <? endif; ?>
            </tr>
            <? foreach($tran as $t): ?>
            <tr>
                <td><a href="/admin/orders/details/<?= $t->orderID ?>" target="_blank"><?= $t->orderID ?></a></td>
                <td><a href="/admin/customers/detail/<?= $t->customerID ?>" target="_blank"><?=  getPersonName($t) ?></a></td>
                <td><?= convert_from_gmt_aprax($t->dateCreated, "m/d/Y") ?></td>
                <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $t->closedGross + $t->closedDisc) ?></td>
                <td><?= $t->address ?></td>
                <td><?= $t->lockerName ?></td>
                <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $t->price) ?></td>
                <? if($this->session->userdata('super_admin') == 1): ?>
                    <td><form method='post' onSubmit="return verify_delete();" action='/admin/superadmin/license_invoicing_delete_item/<?= $t->licenseInvoiceDetailID ?>?invId=<?= $invoice[0]->licenseInvoiceID ?>'><input type="image" id="delete" src='/images/icons/cross.png' /><input type='hidden' value='<?= $t->licenseInvoice_id ?>' name='licenseInvoiceID' /></form></td>
                <? endif; ?>
            </tr>
            <? endforeach; ?>
            </table>
        </td>
    </tr>
    <? endif; ?>
    <tr>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td><?php echo __("Total Revenue License Fee", "Total Revenue License Fee"); ?></td>
        <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $revenuesTotal) ?></td>
    </tr>
    <? if($details): ?>
    <tr>
        <td></td>
        <td>
            <table id="box-table-a">
            <tr>
                <th align="right"><?php echo __("Price", "Price"); ?></th>
                <? if($this->session->userdata('super_admin') == 1): echo '<th></th>'; endif; ?>
            </tr>
            <? foreach($revenues as $r): ?>
            <tr>               
                <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $r->price) ?></td>
                <? if($this->session->userdata('super_admin') == 1): ?>
                <td><form method='post' onSubmit="return verify_delete();" action='/admin/superadmin/license_invoicing_delete_item/<?= $r->licenseInvoiceDetailID ?>'><input type="image" id="delete" src='/images/icons/cross.png' /><input type='hidden' value='<?= $r->licenseInvoice_id ?>' name='invId' /></form></td>
                <? endif; ?>
            </tr>
            <? endforeach; ?>
            </table>
        </td>
    </tr>
    <? endif; ?>
    <tr>
        <td colspan="2"></td>
    </tr>


    <? if(!empty($minimum)): ?>
    <tr>
        <td><?php echo __("Monthly Minimum", "Monthly Minimum"); ?></td>
        <td align="right"><?= format_money_symbol($this->business_id,'%.2n', $minimum[0]->price) ?></td>
    </tr>
    <? endif; ?>
</tbody>
<tfoot>
    <tr>
        <th><?php echo __("Total Due", "Total Due"); ?></th>
        <th align="right"><?= format_money_symbol($this->business_id,'%.2n', $totalDue) ?></th>
    </tr>
</tfoot>    
</table>
<br>
<br>
<? if($this->session->userdata('super_admin') == 1): ?>
    <div align="right"><form method='post' onSubmit="return verify_delete();" action='/admin/superadmin/license_invoicing_delete/?invId=<?= $invoice[0]->licenseInvoiceID ?>'><input type="submit" id="delete" value="<?php echo __("Delete Invoice", "Delete Invoice"); ?>" /></form></div>
<? endif; ?>


<script>
$('#export').click(function(evt) {
        evt.preventDefault();
        window.location.href = window.location.href + '&export=quickbooks';
});
</script>

