<?php enter_translation_domain("admin/reports/by_order_type"); ?>
<h2><?php echo __("Sales By Order Type", "Sales By Order Type"); ?></h2>

<div style="padding:8px;">
<form action="" method="post">
<strong><?php echo __("From", "From:"); ?></strong>
<?= form_dropdown('start_month', $months, $start_month); ?>
<?= form_dropdown('start_year', $years, $start_year); ?>

<strong>To:</strong>
<?= form_dropdown('end_month', $months, $end_month); ?>
<?= form_dropdown('end_year', $years, $end_year); ?>

<select name="grouping">
    <option value="week" <? if($grouping == 'week') { echo ' SELECTED'; } ?>><?php echo __("Weeks", "Weeks"); ?></option>
    <option value="4week" <? if($grouping == '4week') { echo ' SELECTED'; } ?>><?php echo __("4 Week Grouping", "4 Week Grouping"); ?></option>
    <option value="month" <? if($grouping == 'month') { echo ' SELECTED'; } ?>><?php echo __("Months", "Months"); ?></option>
    <option value="quarter" <? if($grouping == 'quarter') { echo ' SELECTED'; } ?>><?php echo __("Quarters", "Quarters"); ?></option>
</select>
<br/>
<strong><?php echo __("Order Types (show only)", "Order Types (show only):"); ?></strong>
<?php 
      foreach($order_types as $k=>$v): 
        $checked = '';
        if (in_array($k, $selected_order_types)):
            $checked = 'checked="checked"';
        endif;
?>
        <input type="checkbox" name="order_types[]" value="<?=$k ?>" <?=$checked?>><?=$v ?>
<?php endforeach; ?>

<input type="submit" name="submit" value="<?php echo __("Run", "Run"); ?>" style="width:10em" />
</form>
</div>
<table id="box-table-a">
    <tr>
        <th><?php echo __("Totals", "Totals"); ?></th>
        <? foreach($headers as $h): ?>
            <th align="right"><?= $h ?></th>
        <? endforeach; ?>
    </tr>
    <? foreach($reportingClass as $key => $value): ?>
    <tr>
        <td><?= $key ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
            <td align="right">
                <div>
                <?= format_money_symbol($this->business->businessID, '%.2n', $value[$i]) ?><br/>
                <?= format_money_symbol($this->business->businessID, '%.2n', $totalDisc[$key][$i]) ?>
                </div>
            </td>
        <? endfor; ?>
    </tr>
    <? endforeach; ?>
    <tr>
        <td style="border-top: 2px solid;"><strong><?php echo __("TOTAL", "TOTAL"); ?></strong></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
            <td style="border-top: 2px solid;" align="right">
                <?= format_money_symbol($this->business->businessID, '%.2n', $gross[$i]) ?>
            </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td>Discounts</td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
            <td align="right">
                <a href="/admin/reports/discount_details/<?= date("Y-n",mktime(0, 0, 0, date("m")-$i, 1, date("Y"))) ?>" target="_blank"><?= format_money_symbol($this->business->businessID, '%.2n', $discounts[$i]) ?>  </a>
            </td>
        <? endfor; ?>
    </tr>
    <tr>
        <th><strong><?php echo __("TOTAL WDiscount", "TOTAL W/Discount"); ?></strong></th>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
            <th align="right">
                <?= format_money_symbol($this->business->businessID, '%.2n', $net[$i]) ?>
            </th>
        <? endfor; ?>
    </tr>

</table>