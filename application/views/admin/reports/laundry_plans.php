<?php enter_translation_domain("admin/reports/laundry_plans"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<script>
    $(document).ready(function() {
        $('#box-table-a').dataTable({
            "bPaginate": false,
            "bFilter": false,
            "aaSorting": []
        });

        $("input[name='start_date']").datepicker();
        $("input[name='end_date']").datepicker();

        $('#report_date_form').validate({
            rules: {
                start_date : {
                    required: false
                },
                end_date : {
                    required: false
                }
            },
            errorClass : "invalid"
        });
    });
</script>
<h2><?php echo __("Laundry Plans", "Laundry Plans"); ?></h2>
<br>

<?= form_open("/admin/reports/laundry_plans", array('id' => 'report_date_form')) ?>
<?= form_label(__("Start Date - Begin", "Start Date - Begin:"), "start_date") ?> <?= form_input("start_date", $start_date) ?>
<?= form_label(__("End", " End:"), "end_date") ?> <?= form_input("end_date", $end_date) ?>
<?= form_submit(array("class" => "button blue", "value" => "Generate Report")) ?>
<?= form_close() ?>
<?php if (isset($plans)): ?>
    <table id="box-table-a">
        <thead>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Active", "Active"); ?></th>
            <th><?php echo __("Plan", "Plan"); ?></th>
            <th><?php echo __("Price", "Price"); ?></th>
            <th><?php echo __("Start Date", "Start Date"); ?></th>
            <th><?php echo __("End Date", "End Date"); ?></th>
            <th><?= weight_system_format() ?> <?php echo __("Used", "Used"); ?></th>
            <th><?php echo __("Effective Rate", "Effective Rate"); ?></th>
            <th><?php echo __("Renewed", "Renewed"); ?></th>
            <th><?php echo __("Default Loc", "Default Loc"); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <? foreach($plans as $plan): ?>
            <tr>
                <td><a href="/admin/customers/detail/<?= $plan->customer_id ?>" target="_blank"><?= getPersonName($plan) ?></a></td>
                <td><?= $plan->active == 0 ? 'no' : 'yes'; ?></td>
                <td><?= $plan->description ?> (<?= $plan->pounds ?> <?= weight_system_format() ?>)</td>
                <td><?= format_money_symbol($plan->business_id, '%.2n', $plan->price) ?></td>
                <td><?= convert_from_gmt_aprax($plan->startDate, $dateFormat) ?></td>
                <td><?= convert_from_gmt_aprax($plan->endDate, $dateFormat) ?></td>
                <td><?= $lbsUsed[$plan->laundryPlanID] ?></td>
                <td><?= format_money_symbol($plan->business_id, '%.2n', $rate[$plan->laundryPlanID]) ?></td>
                <td><? if($plan->active == 1): ?> <?= $plan->renew == 1 ? 'Yes' : ''; ?> <? else: ?> <?= $plan->renewed == 1 ? 'Yes' : ''; ?> <? endif; ?></td>
                <td><?= $plan->address ?></td>
                <td><? if($plan->active == 1): ?><form action="/admin/reports/terminate_plan" method="post"><input type="hidden" name="customer_id" value="<?= $plan->customer_id ?>"><input type="submit" name="terminate" value="<?php echo __("Terminate Plan", "Terminate Plan"); ?>"></form><? endif; ?></td>
            </tr>
            <? endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
<br>