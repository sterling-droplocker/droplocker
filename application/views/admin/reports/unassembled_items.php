<?php enter_translation_domain("admin/reports/unassembled_items"); ?>
<script type="text/javascript">
    
$(document).ready(function() {

    $(".datefield").datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd"
    });

    var dataTable = $("table").dataTable({
        "bPaginate" : false,
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "aaSorting" : [[ 1, "desc"]],
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>            
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    });

});
    
</script>

<h2><?php echo __("Un-Assembled Items", "Un-Assembled Items"); ?></h2>
<div style="padding:8px;">
<form action="/admin/reports/unassembled_items" method="get">
<?php echo __("Order Date", "Order Date:"); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo __("From", "From"); ?>
<input type="text" name="fromDate" value="<?= $fromDate ?>" class="datefield">

&nbsp;&nbsp;&nbsp;&nbsp;To
<input type="text" name="endDate" value="<?= $endDate ?>" class="datefield">
&nbsp;&nbsp;&nbsp;
<input type="submit" value="<?php echo __("Run Report", "Run Report"); ?>" class="button blue">
</form></div>
<table>
<thead>
<tr>
    <th><?php echo __("Order", "Order"); ?></th>
    <th><?php echo __("Order Created", "Order Created"); ?></th>
    <th><?php echo __("Customer", "Customer"); ?></th>
    <th><?php echo __("Barcode", "Barcode"); ?></th>
    <th><?php echo __("Product", "Product"); ?></th>
    <th><?php echo __("Supplier", "Supplier"); ?></th>
    <th><?php echo __("Route", "Route"); ?></th>
    <th><?php echo __("Position", "Position"); ?></th>
    <th><?php echo __("Image", "Image"); ?></th>
    <th></th>
</tr>
</thead>
<tbody>
<? foreach($allItems as $item): ?>
<? if ($item->automat == ''): ?>
<tr>
    <td><a href="/admin/reports/metalprogetti_history/<?= $item->order_id ?>" target="_blank"><?= $item->order_id ?></a></td>
    <td><?= convert_from_gmt_aprax($item->dateCreated, STANDARD_DATE_FORMAT) ?></td>
    <td><a href="/admin/customers/detail/<?= $item->customerID ?>" target="_blank"><?= getPersonName($item) ?></a></td>
    <td><?= $item->barcode ?></td>
    <td><?= $item->productName ?></td>
    <td><?= $item->companyName ?></td>
    <td><?= $item->route_id ?></td>
    <td><?= $item->position ?></td>
    <td><?= $item->picture ? "<a href='/admin/items/view/?itemID=".$item->item_id."'><img class='img_bdr' style='width:100px;' src='https://droplocker.com/picture/resize?w=100&h=100&ac=1&src=https://s3.amazonaws.com/LaundryLocker/{$item->picture}' /></a>":"<img class='img_bdr' style='width:100px;' src='/images/missing_image.jpg'>" ?></td>
    <td>
        <form action="" method="post">
            <input type="hidden" name="order_id" value="<?= $item->order_id ?>">
            <input type="hidden" name="garcode" value="<?= $item->barcode ?>">
            <input type="submit" value="<?php echo __("mark as assembled", "mark as assembled"); ?>">            
        </form>
    </td>
</tr>
<? endif; ?>
<? endforeach; ?>
</tbody>
</table>
