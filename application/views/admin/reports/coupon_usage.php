<?php enter_translation_domain("admin/reports/coupon_usage"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("Coupon Usage", "Coupon Usage"); ?></h2>
<script type="text/javascript">
<? if(!empty($coupons)): ?>
jQuery(document).ready(function($){
    $("#box-table-a").dataTable({
        "bPaginate" : false,
        "bJQueryUI": true,
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            { "sSortDataType": "attr", "sType" : "date" },
            { "sSortDataType": "attr", "sType" : "date" }
        ],
        "aoColumnDefs": [],
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
            aButtons : [
                "pdf", "print"
            ],
            <?php endif ?>
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
        }
    });
});
<?php endif; ?>
</script>
<br>
<table width="100%">
<tr>
    <td>
    </td>
    <td align="right"><a href="/admin/admin/manage_coupons"><?php echo __("Manage Coupons", "Manage Coupons"); ?></a></td>
</tr>
</table>
<br>
<table id="box-table-a">
<thead>
<tr>
    <th><?php echo __("Coupon Code", "Coupon Code"); ?></th>
    <th><?php echo __("Description", "Description"); ?></th>
    <th><?php echo __("Times Issued", "Times Issued"); ?></th>
    <th><?php echo __("Discounts Given", "Discounts Given"); ?></th>
    <th><?php echo __("Avg Discount", "Avg Discount"); ?></th>
    <th><?php echo __("Created Date", "Created Date"); ?></th>
    <th><?php echo __("Expired Date", "Expired Date"); ?></th>
</tr>
</thead>
<tbody>
<? if(empty($coupons)): ?>
<tr>
    <td colspan=7><form action="/admin/reports/coupon_usage" method="post">
        <input type="hidden" name="code" value="%"><input type="submit" name="search" value="<?php echo __("Show All Coupons", "Show All Coupons"); ?>" class="button orange">
        </form></td>
</tr>
<? else: ?>
    <? foreach($coupons as $c): ?>
    <tr>
    	<td><a href="/admin/reports/coupon_usage_details/<?= $c->couponCode ?>" target="_blank"><?= $c->couponCode ?></a></td>
    	<td><?= $c->extendedDesc ?></td>
    	<td><?= $c->totCount ?></td>
    	<td><?= format_money_symbol($this->business->businessID, '%.2n', $c->totAmt) ?></td>
    	<td><?= format_money_symbol($this->business->businessID, '%.2n', $c->totAmt / $c->totCount) ?></td>
        <td><?= date($dateFormat, strtotime($c->origCreateDate)) ?></td>
        <td><?= empty($c->expireDate)?"-":date($dateFormat, strtotime($c->expireDate)) ?></td>
    </tr>
    <? endforeach; ?>
<? endif; ?>
</tbody>
</table>