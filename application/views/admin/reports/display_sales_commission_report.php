<?php enter_translation_domain("admin/reports/display_sales_commission_report"); ?>
<h2> <?php echo __("Commission Report for", "Commission Report for"); ?> <?= getPersonName($employee) ?> </h2>
<h3> <?php echo __("Named Accounts", "Named Accounts"); ?> </h3>
<table id="box-table-a">
    <thead>
        <tr>
            <th> <?php echo __("Name", "Name"); ?> </th>
            <th> <?php echo __("Date", "Date"); ?></th>
            <th> <?php echo __("Commission", "Commission"); ?> </th>
    </thead>
    <tbody>
        <?php foreach ($customer_signups as $customer): ?>
            <tr>
                <td> <?= getPersonName($customer) ?> (<?= $customer->customerID ?>) </td>
                <td> <?= $customer->signupDate->format("m/d/y") ?> </td>
                <td> <?= format_money_symbol($customer->business_id, '%.2n', $commission_customer_setting->amount) ?> </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>

<div style="font-size: 1.2em;"> <?php echo __("Total Customer Commission", "Total Customer Commission:"); ?> <?= format_money_symbol($this->business->businessID, '%.2n', $customer_commission_total) ?> </div>

<h3 style="margin: 10px 0px;"> Orders </h3>
<?php foreach($customers as $customerID => $orders): ?>
    <?php $customer = new \App\Libraries\DroplockerObjects\Customer($customerID, true); ?>
<div style="font-size: 1.4em; margin: 5px 0px;"> <?= getPersonName($customer) ?> (<?= $customer->customerID ?>) </div>
    <table id="box-table-a">
        <thead>
            <th> <?php echo __("Order ID", "Order ID"); ?> </th>
            <th> <?php echo __("Order Date", "Order Date"); ?> </th>
            <th> <?php echo __("Order Total", "Order Total"); ?> </th>
            <th> <?php echo __("Commission Applied", "Commission Applied"); ?> </th>
        </thead>
        <tbody>

            <?php foreach ($orders as $index => $order): ?>
            <tr>
                <td> <a href="/admin/orders/detail/<?= $order->orderID ?>"> <?= $order->orderID ?> </a> </td>
                <td> <?= $order->dateCreated->format("m/d/y") ?> </td>
                <td> <?= format_money_symbol($order->business_id, '%.2n', $order->getTotal()) ?>
                <td>
                    <?php
                        $commission_order_settings = App\Libraries\DroplockerObjects\Commission_Orders_Setting::search_aprax(array("employee_id" => $employee->employeeID, "index" => $index));
                        if (empty($commission_order_settings))
                        {
                            echo number_format_locale(0.00, 2);
                        }
                        else
                        {
                            echo number_format_locale($commission_order_settings[0]->amount, 2);
                        }
                    ?>
                </td>
            </tr>

            <?php endforeach; ?>

        </tbody>
    </table>
<?php endforeach; ?>

<div style="font-size: 1.2em;"> <?php echo __("Total Order Commission", "Total Order Commission:"); ?> <?= format_money_symbol($this->business_id,'%.2n', $orders_commission_total) ?>

<div style="font-size: 1.2em;"> <?php echo __("Total Commission", "Total Commission:"); ?> <?= format_money_symbol($this->business_id,'%.2n', $orders_commission_total + $customer_commission_total) ?>