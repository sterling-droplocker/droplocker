<?php enter_translation_domain("admin/reports/followUpCalls"); ?>
<?php $standardDateFormat = get_business_meta($this->business_id, "standardDateDisplayFormat"); ?>
<script type="text/javascript">
$(document).ready( function() {

$.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
{
	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
		return $('td:eq('+iColumn+')', tr).attr('data-value');
	});
}

$("#calls").dataTable(
{
    "aaSorting": [[ 2, "desc" ]],
    "bPaginate": false,
    "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
    "bJQueryUI": true,
    "sDom": '<"H"lTfr>t<"F"ip>',
    "oTableTools": {
        //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
        <?php if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
    },
    "aoColumns": [
		null,
		null,
		{"sSortDataType": "attr"},
		null,
		null
    ]
}
); 
});
</script>

<h2><?php echo __("Follow Up Calls", "Follow Up Calls"); ?></h2>
<span style="padding-top: 10px; padding-bottom: 10px;"><form action="" method="post"><?php echo __("Qty To Show", "Qty To Show:"); ?> <select name="getCount">
	<option value="100" SELECTED><?php echo __("100", "100"); ?></option>
	<option value="500"><?php echo __("500", "500"); ?></option>
	<option value="1000"><?php echo __("1000", "1,000"); ?></option>
	<option value="10000"><?php echo __("10000", "10,000"); ?></option>
</select>
<input type="submit" name="run" value="Run"></form>
</span>
<table id="calls">
    <thead>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?> </th>
            <th><?php echo __("Call Type", "Call Type"); ?></th>
            <th><?php echo __("Date Called", "Date Called"); ?> </th>
            <th><?php echo __("Note", "Note"); ?></th>
            <th><?php echo __("Called By", "Called By"); ?></th>
    </thead>
    <tbody>
        <?php foreach ($calls as $c): ?>
            <tr>
                <td> <a href="/admin/customers/detail/<?= $c->customer_id ?>" target="_blank"><?= $c->cfirst ?> <?= $c->clast ?></a> </td>
                <td> <?= $c->name ?> </td>
                <td data-value="<?= $c->dateCalled ?>">
					<?= convert_from_gmt_aprax($c->dateCalled, $standardDateFormat) ?>
				</td>
                <td> <?= $c->note?> </td>
                <td> <?= $c->efirst ?> <?= $c->elast ?> </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
