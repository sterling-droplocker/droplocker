<?php enter_translation_domain("admin/reports/dl_invoice"); ?>
<h2><?php echo __("Drop Locker Invoices", "Drop Locker Invoices"); ?></h2>
<br>
<form action="/admin/reports/dl_invoices_view/" method="post">
<select name="invId">
<? foreach($invoices as $invoice): ?>
    	<option value="<?= $invoice->licenseInvoiceID ?>"><?= $invoice->month ?></option>
<? endforeach; ?>
</select>
<input type="submit" name="view" value="<?php echo __("view", "view"); ?>">
</form>
