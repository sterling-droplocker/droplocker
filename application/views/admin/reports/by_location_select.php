<div style="padding:8px;">
    <?
    if(!empty($_REQUEST['locList'])) { echo '<h1>'.__("Filtered by", "Filtered by").' '.$_REQUEST['locList'].'</h1><br>'; }
    ?>
    <form action="" method="post">
        From:
        <?= form_dropdown('start_month', $months, $start_month); ?>
        <?= form_dropdown('start_year', $years, $start_year); ?>

        To:
        <?= form_dropdown('end_month', $months, $end_month); ?>
        <?= form_dropdown('end_year', $years, $end_year); ?>

        <select name="grouping">
            <option value="week" <? if($grouping == 'week') { echo ' SELECTED'; } ?>><?php echo __("Weeks", "Weeks"); ?></option>
            <option value="4week" <? if($grouping == '4week') { echo ' SELECTED'; } ?>><?php echo __("4 Week Grouping", "4 Week Grouping"); ?></option>
            <option value="month" <? if($grouping == 'month') { echo ' SELECTED'; } ?>><?php echo __("Months", "Months"); ?></option>
            <option value="quarter" <? if($grouping == 'quarter') { echo ' SELECTED'; } ?>><?php echo __("Quarters", "Quarters"); ?></option>
            <option value="year" <? if($grouping == 'year') { echo ' SELECTED'; } ?>><?php echo __("Years", "Years"); ?></option>
        </select>
        
        <? if ($display_order_counts != true): ?>
            <label><input type="checkbox" name="extra" value="1" <?= $show_extra ? 'checked' : '' ?> > <?php echo __("Show Extra Info", "Show Extra Info"); ?></label>
            <!-- <input type="checkbox" name="utilization" value="1" '; if($_POST['utilization'] == 1) { echo ' CHECKED'; } echo '>Show Utilization -->
            <input type="submit" name="submit" value="<?php echo __("Run", "Run"); ?>">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/admin/reports/by_order_type"><?= __("sales_by_order_type", "Sales By Order Type"); ?></a>
        <? else: ?>
            <input type="submit" name="submit" value="<?php echo __("Run", "Run"); ?>">
        <? endif; ?>

  
    </form>
</div>