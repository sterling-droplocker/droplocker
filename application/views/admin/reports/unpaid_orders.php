<?php enter_translation_domain("admin/reports/unpaid_orders"); ?>
<?php $standardDateFormat = get_business_meta($this->business_id, "standardDateDisplayFormat"); ?>
<script type="text/javascript">
function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements;
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}
</script>

<h2><?php echo __("Unpaid Orders", "Unpaid Orders"); ?></h2>
<br>
<form action="" method="post" name="captureOrders" id="captureOrders">
<table id="box-table-a">
    <thead>
        <tr>
            <th></th>
            <th><?php echo __("Order", "Order"); ?></th>
            <th><?php echo __("Bag", "Bag"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Payment Status", "Payment Status"); ?></th>
            <th><?php echo __("Orders Status", "Orders Status"); ?></th>
            <th><?php echo __("Order Date", "Order Date"); ?></th>
            <th><?php echo __("Order Amount", "Order Amount"); ?></th>
            <th><?php echo __("Route", "Route"); ?></th>
            <th><?php echo __("Autopay", "Autopay"); ?></th>
            <th><?php echo __("Capture", "Capture"); ?></th>
        </tr>
    </thead>
    <tbody>
    <? foreach($unpaid as $i => $u): ?>
        <tr>
            <td><?= $i + 1 ?></td>
            <td><a href="/admin/orders/details/<?= $u->orderID ?>" target="_blank"><?= $u->orderID ?></a></td>
            <td><?= $u->bagNumber ?></td>
            <td><a href="/admin/customers/detail/<?= $u->customer_id ?>" target="_blank"><?= getPersonName($u) ?></a></td>
            <td><?= $u->payStat ?></td>
            <td><?= $u->ordStat ?></td>
            <td><?= convert_from_gmt_aprax($u->dateCreated, $standardDateFormat) ?></td>
            <td><?= format_money_symbol($this->business->businessID, '%.2n', $total[$u->orderID]) ?></td>
            <td><?= $u->route; ?></td>
            <td><?= ($u->autoPay == 1) ? __("Yes", "Yes") : __("No", "No") ?></td>
            <td><? if($u->orderStatusOption_id <> 1 ): ?><input type="checkbox" name="captureOrders[<?= $u->orderID ?>]" value="<?= $u->orderID ?>"/><? endif; ?></td>
        </tr>
    <? endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan=7><?php echo __("Grand Total", "Grand Total"); ?></td>
            <td><?= format_money_symbol($this->business->businessID, '%.2n', $grandTotal) ?></td>
            <td colspan=2>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="10" align="right"><input type="button" onclick="SetAllCheckBoxes('captureOrders', 'captureOrders', true);" value="<?php echo __("Select All", "Select All"); ?>"><br><input type="submit" name="submit" value="<?php echo __("Capture Orders", "Capture Orders"); ?>"></td></tr>
        </tr>
    </tfoot>
</table>
</form>

<script type="text/javascript">
$('#captureOrders').submit(function(){
  $(this).find('input[type=submit]').attr('disabled','disabled').val(<?php echo __("Please wait", "Please wait..."); ?>);
});
</script>
