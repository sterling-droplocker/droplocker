<?php enter_translation_domain("admin/reports/supplier_manifest_receipt_printer_friendly"); ?>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
    <style type="text/css">
        table {
            font-size: 0.8em;
            width: 2.8in !IMPORTANT;
        }
        table th {
            font-weight: bold;
        }
        table td {
            text-align: left;
        }
        .number {
            text-align: right;
        }
        table td {
            padding: 5px;
        }
    </style>
</head>
<body>
<table>
    <tr>
        <td><?php echo __("Items checked in between from and to", "Items checked in between %from% and %to%:", array("from"=>date("D m/d/y g:ia", strtotime($startDate)), "to"=>date("D m/d/y g:ia", strtotime($endDate)))); ?></td>
    </tr>
</table>
<? if($items): ?>
<? if($_POST['details']): ?>
<table>
    <thead>
        <tr>
            <? if($_POST['barcode']) echo '<th>'.__("scanned", "Scanned").'</th>'; ?>
            <th><?php echo __("Order", "Order"); ?></th>
            <th><?php echo __("Barcode", "Barcode"); ?></th>
            <th><?php echo __("Product", "Product"); ?></th>
            <th><?php echo __("Process", "Process"); ?></th>
            <th><?php echo __("Product ID", "Product ID"); ?></th>
            <th align="right"><?php echo __("Qty", "Qty"); ?></th>
            <th align="right"><?php echo __("Cost", "Cost"); ?></th>
        </tr>
    </thead>
    <tbody>
    <? foreach($details as $d): ?>
        <tr>
            <? if($_POST['barcode'])
                {
                    echo '<td>';
                    if($d->scanned > '0000-00-0000')
                    { echo convert_from_gmt_aprax($d->scanned, "g:ia"); }
                    echo '</td>';
                }
            ?>
            <td <? if($d->scanned > '0000-00-0000') echo 'style="text-decoration:line-through;"'; ?>><a href="/admin/orders/details/<?= $d->orderID ?>" target="_blank"><?= $d->orderID ?></a></td>
            <td><a href="/admin/items/view?itemID=<?= $d->item_id ?>" target="_blank"><?= $d->barcode ?></a></td>
            <td><?= $d->product ?></td>
            <td><?= $d->process ?></td>
            <td><?= $d->productID ?></td>
            <td align="right"><?= $d->qty ?></td>
            <td align="right">$<?= number_format_locale($d->cost * $d->qty,2) ?></td>
        </tr>
    <? endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <? if($_POST['barcode']) echo '<th></th>';  ?>
            <th colspan=4><?php echo __("Total", "Total"); ?></th>
            <th align="right"><?= $total['qty'] ?></th>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n',  $total['cost']) ?></th>
        </tr>
    </tfoot>
    </table>
    <? else: ?>
    <table>
        <thead>
                <tr>
                    <th><?php echo __("Product", "Product"); ?></th>
                    <th><?php echo __("Process", "Process"); ?></th>
                    <th align="right"><?php echo __("Qty", "Qty"); ?></th>
                    <th align="right"><?php echo __("Cost", "Cost"); ?></th>
                </tr>
        </thead>
        <tbody>
            <? foreach($items as $key => $value): ?>
                <tr>
                    <td><?= $key ?></td>
                    <td class="number"><?= $value['qty'] ?></td>
                    <td class="number"><?= format_money_symbol($this->business->businessID, '%.2n', $value['cost']) ?></td>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan=2><?php echo __("Total", "Total"); ?></th>
                <th align="right"><?= $total['qty'] ?></th>
                <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total['cost']) ?></th>
            </tr>
        </tfoot>
    </table>

<? endif; ?>
<? else: ?>
<?php echo __("No data. Try re-running the report", "No data.  Try re-running the report."); ?>

<? endif; ?>

</body>
</html>
