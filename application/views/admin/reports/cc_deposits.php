<?php enter_translation_domain("admin/reports/cc_deposits"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("Credit Card Deposits", "Credit Card Deposits"); ?></h2>
<form action="" method="post">
<br>
    <select name="selectedDateStart">
    <?php
        for ($i = 0; $i <= 500 -1; $i++) {
                $selectDay = date("Y-m-d", strtotime(-$i." day"));
                echo '<option value="'.$selectDay.'"';
                if ($selectDay == $selectedDateStart) { echo ' SELECTED';}
                echo '>'.convert_from_gmt_aprax($selectDay, $dateFormat).'</option>';
        }
    ?>
    </select>
    <?= form_input(array("name" => "cutOffStart", "class" => "cut_off_time", "value" => $cutOffStart, "size" => 6)) ?>

    to
     <select name="selectedDateEnd">
    <?php
        for ($i = -1; $i <= 500 -1; $i++) {
                $selectDay = date("Y-m-d", strtotime(-$i." day"));
                echo '<option value="'.$selectDay.'"';
                if ($selectDay == $selectedDateEnd) { echo ' SELECTED';}
                echo '>'.convert_from_gmt_aprax($selectDay, $dateFormat).'</option>';
        }
    ?>
    </select>

    <?= form_input(array("name" => "cutOffEnd", "class" => "cut_off_time", "value" => $cutOffEnd, "size" => 6)) ?>

<input type="checkbox" name="details" value="1" <? if($this->input->post("details") == 1): ?> checked <? endif; ?>> <?php echo __("Show Details", "Show Details"); ?>&nbsp;&nbsp;&nbsp;
<select name="byDate">
	<option value="day" <?= $byDate == 'day' ? ' SELECTED' : '';?>><?php echo __("By Day", "By Day"); ?></option>
	<option value="location" <?= $byDate == 'location' ? ' SELECTED' : '';?>><?php echo __("By Location", "By Location"); ?></option>
</select>
<input type="submit" name="submit" value="<?php echo __("go", "go"); ?>" class="button blue">
</form>
<br>
<?php if(isset($total)): ?>
    <table id="box-table-a">
    <?php if($this->input->post("details") == 1): ?>
        <tr>
            <th><?php echo __("Date Charged", "Date Charged"); ?></th>
            <th><?php echo __("Order", "Order"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Location", "Location"); ?></th>
            <th><?php echo __("Custom1", "Custom1"); ?></th>
            <th><?php echo __("Locker", "Locker"); ?></th>
            <th><?php echo __("Reference", "Reference"); ?></th>
            <th><?php echo __("Processor", "Processor"); ?></th>
            <th><?php echo __("Charged", "Charged"); ?></th>
            <th><?php echo __("Gross", "Gross"); ?></th>
            <th><?php echo __("Discount", "Discount"); ?></th>
            <th><?php echo __("Net", "Net"); ?></th>
            <?php foreach($types as $t => $val): ?>
                <th><?php echo __("Cost", "Cost"); ?><br><?= $t ?></th>
            <? endforeach; ?>
            <th><?php echo __("Profit", "Profit"); ?></th>
        </tr>
    <? endif; ?>
    <?php foreach($charge as $day => $order): ?>
        <?php if($this->input->post("details") == 1): ?>
            <?php foreach($order as $o => $details): ?>
                <?php if($details['net'] == 0 and $details['gross'] == 0 and $details['charged'] == 0): ?>
                    <?php //do nothing ?>
                <?php else: ?>
                <tr>
                    <td align="left"><?= @convert_from_gmt_aprax($details['chargeDate'], $dateFormat) ?></td>
                    <td align="left"><a href="/admin/orders/details/<?= $o ?>"><?= $o ?></a></td>
                    <td align="left"><a href="/admin/customers/detail/<?= $details['custID'] ?>"><?= $details['customer'] ?></a></td>
                    <td align="left"><?= @$details['location'] ?></td>
                    <td align="left"><?= @$details['custom1'] ?></td>
                    <td align="left"><?= @$details['locker'] ?></td>
                    <td align="left"><?= @$details['pnref'] ?></td>
                    <td align="left"><?= @$details['processor'] ?></td>
                    <td align="right" <? if (number_format_locale($details['net'],2) != number_format($details['charged'],2)) { echo 'style="color: Red;"'; } ?>><?= format_money_symbol($this->business_id,'%.2n', $details['charged']) ?></td>

                    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $details['gross']) ?></td>
                    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $details['discounts']) ?></td>
                    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $details['net']) ?></td>
                    <? foreach($types as $t => $val): ?>
                        <td align="right"><? if($details['typeCost'][$t]): ?><?= format_money_symbol($this->business->businessID, '%.2n', $details['typeCost'][$t])?> <? endif;?></td>
                    <? endforeach; ?>
                    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $details['profit']) ?></td>
                </tr>
                <? endif; ?>
            <? endforeach; ?>
        <?php endif; ?>
        <tr>
            <th colspan=4><?php echo __("Total for", "Total for"); ?> <?= $byDate == 'day' ? convert_from_gmt_aprax($day, $dateFormat) : $day; ?></th>
            <th align="right"><?= $byDate == 'day' ? '' : $total[$day]['custom1']; ?></th>
            <th colspan=3></th>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total[$day]['charged']); $grandTotal += $total[$day]['charged']; ?></th>
            <? if($this->input->post("details") == 1): ?>
                <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total[$day]['gross']); $grossTotal += $total[$day]['gross']; ?></th>
                <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total[$day]['discounts']); $discTotal += $total[$day]['discounts']; ?></th>
                <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total[$day]['net']); $netTotal += $total[$day]['net']; ?></th>
                <? foreach($types as $t => $val): ?>
                    <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total[$day]['cost'][$t]); $costTotal[$t] += $total[$day]['cost'][$t]; ?></th>
                <? endforeach; ?>
                <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total[$day]['profit']); $profitTotal += $total[$day]['profit'];  ?></th>
            <?endif; ?>
        </tr>
        <?php if($this->input->post("details") == 1): ?>
            <tr>
                <td colspan=20>&nbsp;</td>
            </tr>
        <?php endif;?>
    <?php endforeach; ?>
    <tr>
        <th colspan=8><?php echo __("Grand Total", "Grand Total"); ?></th>
        <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $grandTotal) ?></th>
        <?php if($this->input->post("details") == 1): ?>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $grossTotal) ?></th>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $discTotal) ?></th>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $netTotal) ?></th>
            <?php foreach($types as $t => $val): ?>
                <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $costTotal[$t]) ?></th>
            <? endforeach; ?>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $profitTotal) ?></th>
        <?endif; ?>
    </tr>
    </table>
<? else: ?>
<?php echo __("No data returned", "No data returned for %selectedDate%.  Try to select a different date", array("selectedDate"=>$selectedDate)); ?>
<? endif; ?>