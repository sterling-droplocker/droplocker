<?php enter_translation_domain("admin/reports/customers_map"); ?>
<script type="text/javascript">

//globals... ahh, good ole JS...
var locationJSON        = <?=json_encode( $location );?>;
var customersJSON       = <?=json_encode( $customers );?>;
var customerMarkers     = {};
var visibleCustomers    = {};
var customerMapOptions  = {
                            zoom: 13,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                            };

var markerOptions       = {};
var customersMap        = {};
var infoWindow;

var northEast = {};
var southWest = {};
var radius = 0;
var locationLatLng = null;

var initMap = function( callback ){

    //for actually creating the map
    var drawMap = function(){
        customersMap = new google.maps.Map( document.getElementById("map_canvas"), customerMapOptions );
        processCustomerMarkers();
    };

    infoWindow = new google.maps.InfoWindow();

    //fix / more like a hack!
    var location = locationJSON[0];

    //short hand to check we have valid lat/lon JS trick ;)
    var centerLat = location['lat'] || '';
    var centerLon = location['lon'] || '';

    //if we don't have the lat/lon for whatever reason, grab it via geocoder
    if( centerLat === '' || centerLon === '' ) {
         var addressString = location.address + ", " + location.city + ", " + location.state + " " + location.zipcode;
         var mapCenterGeocoder = new google.maps.Geocoder();
         mapCenterGeocoder.geocode( { address: addressString }, function(geoResult, geoStatus) {
             customerMapOptions['center'] = geoResult[0]['geometry']['location'];
             drawMap();

             centerLat = geoResult[0].geometry.location.lat();
             centerLon = geoResult[0].geometry.location.lng();
             locationLatLng = new google.maps.LatLng(centerLat, centerLon);
         });
    } else {
         customerMapOptions['center'] = new google.maps.LatLng( centerLat, centerLon );
         locationLatLng = new google.maps.LatLng(centerLat, centerLon);
         drawMap();
    }

    if( typeof callback == 'function') {
        callback();
    }
};

var getDistanceLatLng = function(latlng1, latlng2, unit) {
    if (typeof(unit) == 'undefined') {
        unit = 'M';
    }

    var lat = [latlng1.lat(), latlng2.lat()];
    var lng = [latlng1.lng(), latlng2.lng()];
    var R = unit == 'K' ? 6378.137 : 3963.191;
    var dLat = (lat[1]-lat[0]) * Math.PI / 180;
    var dLng = (lng[1]-lng[0]) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat[0] * Math.PI / 180 ) * Math.cos(lat[1] * Math.PI / 180 ) * Math.sin(dLng/2) * Math.sin(dLng/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return Math.round(d);
}

var updateCustomers = function() {
    for (var i in customerMarkers) {
        var marker = customerMarkers[i];
        var visible = true;
        if (radius > 0) {
            var distance = getDistanceLatLng(locationLatLng, marker.position);
            visible = distance <= radius;
        }
        marker.setVisible(visible);
        visibleCustomers[i] = visible;
    }
};

var processCustomerMarkers = function(){
    customerMarkers = {};   //reset this...
    $.each( customersJSON, function( index, customer) {
        var customerLatLon = new google.maps.LatLng( customer.lat, customer.lon );
        customerMarkers[customer.customerID] = new google.maps.Marker( { position : customerLatLon, map : customersMap });
        visibleCustomers[customer.customerID] = true;

        google.maps.event.addListener(customerMarkers[customer.customerID], 'click', function() {
            var marker = customerMarkers[customer.customerID];
            var info = getCustomerInfo(customer.customerID);
            infoWindow.setContent(info);
            infoWindow.open(customersMap,marker);
        });
    });
};

var getCustomerInfo = function(customerID) {

    var i = 0;
    var customer = null;
    for (i = 0; i < customersJSON.length; i++) {
        if (customersJSON[i].customerID == customerID) {
            customer = customersJSON[i];
            break;
        }
    }

    if (!customer) {
        return null;
    }

    var info = '<table>';
    info += '<tr><td colspan="2"><h2><a href="/admin/customers/detail/'+customer.customerID+'">' +  customer.printableName + '</a></h2></td></tr>';
    info += '<tr><td>E-mail</b></td><td>' + customer.email + '</td></tr>';
    info += '<tr><td>Addredss</td><td>' + customer.address1 + '</td></tr>';
    info += '</table>';
    return info;
};

//when jquery is ready kick star the fancy map stuff
$(function(){
    initMap();

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var nTr = oSettings.aoData[ iDataIndex ].nTr;
        var customerID = $('td:eq(0)', nTr).attr('data-id');

        return typeof(visibleCustomers[customerID]) != 'undefined' ? visibleCustomers[customerID] : true;
    }
);


$locations_dataTable = $("#customers").dataTable({
    "bPaginate" : false,
    "bJQueryUI": true,
    "fnStateLoad": function (oSettings) {
        return JSON.parse( localStorage.getItem('DataTables') );
    },
    "sDom": '<"H"lTfr>t<"F"ip>',
    "oTableTools": {
        <?php if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
    }

});

var header = new FixedHeader($locations_dataTable);


$('#customer_radius').change(function() {
    radius = parseFloat($(this).val());
    updateCustomers();
    $locations_dataTable.fnDraw();
    customersMap.panTo(locationLatLng);
    customersMap.setZoom(13);
});


$('#customers').on('click', '.show-on-map', function(evt) {
    evt.preventDefault();
    var customerID = $(this).attr('data-id');
    var marker = customerMarkers[customerID];

    if (marker) {
        customersMap.setZoom(16);
        customersMap.panTo(marker.position);

        var info = getCustomerInfo(customerID);
        infoWindow.setContent(info);
        infoWindow.open(customersMap,marker);
    }
});



});
</script>

<h2><?php echo __("Customers at", "Customers at"); ?> <?= $location[0]->address ?></h2>
<div>
<label><?php echo __("Show customers in a radius of", "Show customers in a radius of"); ?>
<select id="customer_radius" name="radius">
    <option value="0"><?php echo __("Show All", "Show All"); ?></option>
    <option value="0.5"><?php echo __("0.5 mi", "0.5 mi"); ?></option>
    <option value="1.0"><?php echo __("1.0 mi", "1.0 mi"); ?></option>
    <option value="1.5"><?php echo __("1.5 mi", "1.5 mi"); ?></option>
    <option value="2.0"><?php echo __("2.0 mi", "2.0 mi"); ?></option>
    <option value="2.5"><?php echo __("2.5 mi", "2.5 mi"); ?></option>
    <option value="3.0"><?php echo __("3.0 mi", "3.0 mi"); ?></option>
</select>
</label>
</div>

<div id="map_canvas" style="height:395px;width:100%;"></div>

<br>
<table id='customers'>
    <thead>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Email", "Email"); ?></th>
            <th><?php echo __("Address", "Address"); ?></th>
            <th><?php echo __("Orders", "Orders"); ?></th>
            <th><?php echo __("Gross", "Gross"); ?></th>
            <th><?php echo __("Discounts", "Discounts"); ?></th>
            <th><?php echo __("Last Order", "Last Order"); ?></th>
            <th><?php echo __("GPS", "GPS"); ?></th>
            <th><?php echo __("Distance", "Distance"); ?></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($customers as $customer): ?>
        <tr>
            <td data-id="<?= $customer->customerID ?>">
                <a href="/admin/customers/detail/<?= $customer->customerID ?>" target="_blank"><?= getPersonName($customer) ?></a>
            </td>
            <td><?= $customer->email ?></td>
            <td><?= $customer->address1 ?></td>
            <td><?= $customer->orders ?></td>
            <td><?= format_money_symbol($this->business_id, '%.2n', $customer->gross) ?></td>
            <td><?= format_money_symbol($this->business_id, '%.2n', $customer->disc) ?></td>
            <td><?= convert_from_gmt_aprax($customer->lastOrder, SHORT_DATE_FORMAT) ?></td>
            <td>
                <span class="show-on-map" data-id="<?= $customer->customerID ?>">
                    <?= $customer->lat ?>, <?= $customer->lon ?>
                </span>
            </td>
            <td><? if ($customer->lat != '' && $customer->lon != ''): ?>
                <?= number_format_locale(distance($customer->lat, $customer->lon, $location[0]->lat, $location[0]->lon), 2); ?>
                <? else: ?>
                0.0
                <? endif; ?>
            </td>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
