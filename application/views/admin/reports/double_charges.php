<?php enter_translation_domain("admin/reports/double_charges"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("Double Charge Report", "Double Charge Report"); ?></h2>
<div>
    <form action='' method='post'>
        <table><tr><td><?php echo __("Days Back", "Days Back:"); ?> <select id='days' name='days'>
        <? for($i=1; $i < 31; $i++): ?>
            <option value='<?= $i?>'><?= $i ?></option>
        <? endfor; ?>
        </select> 
        <script>selectValueSet('days', '<?= $days?>')</script>
        <input type='submit' name='submit' value='<?php echo __("Update", "Update"); ?>'/></td></tr></table>
    </form>
</div>

<table id='box-table-a'>
    <tr>
        <th><?php echo __("OrderID", "OrderID"); ?></th>
        <th><?php echo __("Customer", "Customer"); ?></th>
        <th><?php echo __("Date", "Date"); ?></th>
        <th><?php echo __("Amount", "Amount"); ?></th>
        <th><?php echo __("Charges", "Charges"); ?></th>
    </tr>
    <?foreach($transactions as $transaction): ?>
    <tr>
        <td><a target='_blank' href='/admin/orders/details/<?= $transaction->order_id ?>'><?= $transaction->order_id ?></a></td>
        <td><a target='_blank' href='/admin/customers/detail/<?= $transaction->customerID ?>'><?= getPersonName($transaction) ?></a></td>
        <td><?= convert_from_gmt_aprax($transaction->updated, SHORT_DATE_FORMAT) ?></td>
        <td><?= $transaction->amount ?></td>
        <td><?= $transaction->charges?></td>
    </tr>
    <? endforeach; ?>
    
</table>