<?php enter_translation_domain("admin/reports/product_plans"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("Product Plans", "Product Plans"); ?></h2>
<br>
<table id="box-table-a">
    <thead>
    <tr>
        <th><?php echo __("Customer", "Customer"); ?></th>
        <th><?php echo __("Active", "Active"); ?></th>
        <th><?php echo __("Plan", "Plan"); ?></th>
        <th><?php echo __("Price", "Price"); ?></th>
        <th><?php echo __("Start Date", "Start Date"); ?></th>
        <th><?php echo __("End Date", "End Date"); ?></th>
        <th><?php echo __("Amount Used", "$ Used"); ?></th>
      	<th><?php echo __("Effective Rate", "Effective Rate"); ?></th>
       	<th><?php echo __("Renewed", "Renewed"); ?></th>
       	<th><?php echo __("Default Loc", "Default Loc"); ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
        <? foreach($plans as $plan): ?>
        <tr>
            <td><a href="/admin/customers/detail/<?= $plan->customer_id ?>" target="_blank"><?= getPersonName($plan) ?></a></td>
        	<td><?= $plan->active == 0 ? 'no' : 'yes'; ?></td>
        	<td><?= $plan->description ?> (<?= $plan->credit_amount ?> $)</td>
        	<td><?= format_money_symbol($plan->business_id, '%.2n', $plan->price) ?></td>
        	<td><?= convert_from_gmt_aprax($plan->startDate, $dateFormat) ?></td>
        	<td><?= convert_from_gmt_aprax($plan->endDate, $dateFormat) ?></td>
            <td><?= $amountUsed[$plan->laundryPlanID] ?></td>
            <td><?= format_money_symbol($plan->business_id, '%.2n', $rate[$plan->laundryPlanID]) ?></td>
            <td><? if($plan->active == 1): ?> <?= $plan->renew == 1 ? 'Yes' : ''; ?> <? else: ?> <?= $plan->renewed == 1 ? 'Yes' : ''; ?> <? endif; ?></td>
            <td><?= $plan->address ?></td>
            <td><? if($plan->active == 1): ?><form action="/admin/reports/terminate_plan" method="post"><input type="hidden" name="customer_id" value="<?= $plan->customer_id ?>"><input type="hidden" name="type" value="product_plan"><input type="submit" name="terminate" value="<?php echo __("Terminate Plan", "Terminate Plan"); ?>"></form><? endif; ?></td>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
<br>
<br>
<?php echo __("Report only shows 1000 records", "Report only shows 1000 records"); ?>