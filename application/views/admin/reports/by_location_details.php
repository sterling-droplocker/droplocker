<?php enter_translation_domain("admin/reports/by_location_details"); ?>
<h2><?php echo __("Order Details for", "Order Details for"); ?> <?= $location['address'] ?></h2>
<br>
<h3><?= date("F Y", strtotime($location['month'])) ?></h3>
<br>
<?php if (isset($ordCount)): ?>
    <div style="width: 400px;">

    <table id='hor-minimalist-a'>
        <tr>
            <td colspan=2><?php echo __("Total Customers:", "Total Customers:"); ?> <?= $location['custCount'] ?></td>
            <td colspan="3" align="right"><a href="/admin/reports/customers_map/<?= $locationID ?>" target="_blank"><?php echo __("Scatter Map of Customers", "Scatter Map of Customers"); ?></a></td>
        </tr>
        <tr>
            <th></th>
            <? foreach($ordCount as $key => $value): ?>
            <th align="right"><?= $key ?></th>
            <? endforeach; ?>
            <th align="right"><?php echo __("Total", "Total"); ?></th>
        </tr>
        <tr>
            <td><?php echo __("Orders", "Orders"); ?></td>
            <? foreach($ordCount as $key => $value): ?>
            <td align="right"><?= $ordCount[$key] ?></td>
            <? $ordTot += $ordCount[$key]; endforeach; ?>
            <td align="right"><?= $ordTot ?></td>
        </tr>
        <tr>
            <td><?php echo __("Gross", "Gross"); ?></td>
            <? foreach($ordCount as $key => $value): ?>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $gross[$key]) ?></td>
            <? $grossTot += $gross[$key]; endforeach; ?>
            <td align="right"><?= number_format_locale($grossTot,2) ?></td>
        </tr>
        <tr>
            <td><?php echo __("Discounts", "Discounts"); ?></td>
            <? foreach($ordCount as $key => $value): ?>
            <td align="right"><a href="/admin/reports/discount_details/<?= $location['month'] ?>/<?= $locationID ?>" target="_blank"><?= format_money_symbol($this->business->businessID, '%.2n', $discounts[$key]) ?></a></td>
            <? $discTot += $discounts[$key]; endforeach; ?>
            <td align="right"><?= number_format_locale($discTot,2) ?></td>
        </tr>
        <tr style="font-weight: bold;">
            <td><?php echo __("Total", "Total"); ?></td>
            <? foreach($ordCount as $key => $value): ?>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $net[$key]) ?></td>
            <? $netTot += $net[$key]; endforeach; ?>
            <td align="right"><?= number_format_locale($netTot,2) ?></td>
        </tr>
        <tr>

        </tr>
    </table>
    </div>
    <br><br>
    <h3><?php echo __("Daily Order Count", "Daily Order Count:"); ?></h3>
    <table id='hor-minimalist-a'>
        <tr>
            <th></th>
            <? foreach($dailyCount as $key => $value): ?>
            <th align="right"><?= date("D-d", strtotime($key)) ?></th>
            <? endforeach; ?>
        </tr>
        <? foreach($ordCount as $key => $value): ?>
        <tr>
            <td><?= $key ?></td>
            <? foreach($dailyCount as $xxx => $value): ?>
            <td align="right"><?= $value[$key] ?></td>
            <? endforeach; ?>
        </tr>
        <? endforeach; ?>
        <tr>
            <td><?php echo __("Gross Sales", "Gross Sales"); ?></td>
            <? foreach($dailyGross as $amt): ?>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $amt) ?></td>
            <? endforeach; ?>
        </tr>
        <tr>
            <td><?php echo __("Discounts", "Discounts"); ?></td>
            <? foreach($dailyDisc as $disc): ?>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $disc) ?></td>
            <? endforeach; ?>
        </tr>
        <tr>
            <td><?php echo __("Net Sales", "Net Sales"); ?></td>
            <? foreach($dailyNet as $net): ?>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $net) ?></td>
            <? endforeach; ?>
        </tr>
    </table>
    <br><br>
    <h3><?php echo __("Detailed Orders", "Detailed Orders:"); ?></h3>
    <table id='hor-minimalist-a'>
        <tr>
            <th><?php echo __("Date Created", "Date Created"); ?></th>
            <th><?php echo __("Order", "Order"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th align="right"><?php echo __("Amt", "Amt"); ?></th>
            <th align="right"><?php echo __("Discounts", "Discounts"); ?></th>
            <th align="right"><?php echo __("Total", "Total"); ?></th>
            <th align="right"><?php echo __("Type", "Type"); ?></th>
            <th align="right"><?php echo __("Items", "Items"); ?></th>
            <th></th>
        </tr>
        <? foreach($orders as $order): ?>
        <tr style="border-bottom: 1px solid gray;">
            <td><?= convert_from_gmt_aprax($order->dateCreated, SHORT_DATE_FORMAT)?></td>
            <td><a href="/admin/orders/details/<?= $order->orderID ?>" target="_blank"><?= $order->orderID ?></a></td>
            <td><a href="/admin/customers/detail/<?= $order->customer_id ?>" target="_blank"><?= getPersonName($order) ?></a></td>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n',  $ordAmts[$order->orderID]['subtotal']) ?></td>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $ordAmts[$order->orderID]['discounts']) ?></td>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $ordAmts[$order->orderID]['total']) ?></td>
            <td align="right"><?= $ordType[$order->orderID] ?> (<?= count($ordAmts[$order->orderID]['items']) ?>)</td>
            <td style="padding-bottom: 10px;">
                <? if($ordAmts[$order->orderID]['items']): ?>
                <table>
                    <tr>
                        <th><?php echo __("Item", "Item"); ?></th>
                        <th align="right"><?php echo __("Price", "Price"); ?></th>
                        <th align="right"><?php echo __("Qty", "Qty"); ?></th>
                        <th align="right"><?php echo __("Total", "Total"); ?></th>
                    </tr>
                    <? foreach ($ordAmts[$order->orderID]['items'] as $item): ?>
                    <tr>
                        <td><?= $item->displayName ?></td>
                        <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $item->unitPrice) ?></td>
                        <td align="right"><?= $item->qty ?></td>
                        <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $item->unitPrice * $item->qty) ?></td>
                    </tr>
                    <? endforeach; ?>
                </table>
                <? endif; ?>
        </tr>
        <? endforeach; ?>
        <? if($prePayments): foreach($prePayments as $p): ?>
            <?= $p ?>
        <? endforeach; endif; ?>
    </table>
<?php else: ?>
    <?= __("There are no orders in this location", "There are no orders in this location.") ?>
<?php endif ?>