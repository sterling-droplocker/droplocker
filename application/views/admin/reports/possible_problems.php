<?php enter_translation_domain("admin/reports/possible_problems"); ?>
<?php  $CI = get_instance(); ?>
<h2><?php echo __("End Of Route Report", "End Of Route Report"); ?></h2>

<div style="padding:5px;">
<form action="/admin/reports/possible_problems_redirect" method="post">
<?php echo __("Routes", "Routes:"); ?>
<?
$fullDateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat",'d/m/Y');
$dateFormat = get_business_meta($this->business_id, "standardDateDisplayFormat",'d/m/Y');

if (!isset($showRoutes)) {
    $showRoutes = '';
}

$routeArray = explode(",", $showRoutes);

foreach ($allRoutes as $a): ?>
&nbsp;&nbsp;<?= $a->route_id ?><input type="checkbox" name="route[]" value="<?= $a->route_id ?>"
<? if (in_array(strval($a->route_id ), $routeArray))  echo ' checked';  ?>
>
<? endforeach; ?>
<select name="dayOfWeek">
    <? for($i = 0; $i < 7; $i ++): ?>
	    <option value="<?= $i ?>" <?= $dayOfWeek == $i ? 'SELECTED' : ''; ?>><?= date("D", strtotime("sunday +$i days")) ?></option>
    <? endfor; ?>
</select>
<input type="submit" name="submit" value="<?php echo __("run", "run"); ?>" class="button blue">
</form>
</div>
<?= $this->session->flashdata("message") ?>
<div style="float:left"><h3><a name="deliveries"><?php echo __("Missed Deliveries", "Missed Deliveries"); ?></a></h3></div>
<table id="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("Order", "Order"); ?></th>
            <th><?php echo __("Picked Up", "Picked Up"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Address", "Address"); ?></th>
            <th><?php echo __("Title", "Title"); ?></th>
            <th><?php echo __("R", "R"); ?></th>
            <th><?php echo __("Locker", "Locker"); ?></th>
            <th><?php echo __("Status", "Status"); ?></th>
            <th><?php echo __("Loaded", "Loaded"); ?></th>
            <th colspan=3>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
            <form action="/admin/reports/possible_problems_update" method="post">
            <input type="hidden" name="showRoutes" value="<?= str_replace(",", "-", $showRoutes) ?>">
            <? if(!empty($deliveries)): foreach($deliveries as $delivery):?>
            <tr>
                <td><a href="/admin/orders/details/<?= $delivery->orderID ?>" target="_blank"><?= $delivery->orderID ?></a></td>
                <td><?= convert_from_gmt_aprax($delivery->created, $dateFormat) ?></td>
                <td><a href="/admin/customers/detail/<?= $delivery->customer_id ?>" ><?= getPersonName($delivery) ?> </a></td>
                <td><?= $delivery->address ?></td>
                <td><?= $delivery->companyName ?></td>
                <td><?= $delivery->route_id ?></td>
                <td><?= $delivery->lockerName ?></td>
                <td><?= $delivery->name ?></td>
                <td><?= $delivery->loaded == 1 ? 'Loaded' : '' ?></td>
                <td><a href="/admin/orders/details/<?= $delivery->orderID ?>" target="_blank">details</a></td>
                <td nowrap>
                <? if($CI->zacl->check_acl('delete_claims')): ?>
                    <? if($delivery->lockerName <> 'InProcess'):
                    // only allow customer service and management edit this
                        if($CI->zacl->check_acl('I delivered this link', $CI->session->userdata('buser_id'))){
                            ?>
                            <a href="/admin/reports/possible_problems_ready/?order_id=<?= $delivery->orderID ?>&locker_id=<?= $delivery->locker_id?>">I Delivered This</a><br>
                            <?
                        }
                    endif; ?>
                    <a href="/admin/reports/possible_problems_check/<?= str_replace(",", "-", $showRoutes) ?>/<?= $delivery->orderID ?>"><?php echo __("Ill Check Tomorrow", "I'll Check Tomorrow"); ?></a></td>
                <? else: ?>
                    ____________________
                <? endif; ?>
                <td><input type="text" name="llNotes[<?= $delivery->orderID ?>]" value="<?= $delivery->llNotes ?>" size="50"></td>
            </tr>
            <? endforeach; else:?>
            <tr><td colspan='11'><?php echo __("No missed deliveries for route", "No missed deliveries for route"); ?> <?= $showRoutes ?></td></tr>
            <? endif; ?>
    </tbody>
    <tfoot>
        <tr>
                <td colspan="11" align="right"><input type="submit" name="updateNotes" value="<?php echo __("Update Notes", "Update Notes"); ?>" class="button blue"></form></td>
        </tr>
    </tfoot>
</table>
<br>
<?= $this->session->flashdata("message") ?>
<div style="float:left"><h3><a name="pickups"><?php echo __("Missed Pickups", "Missed Pickups"); ?></a></h3></div>
<table id="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("ID", "ID"); ?></th>
            <th><?php echo __("Service Type", "Service Type"); ?></th>
            <th><?php echo __("Location", "Location"); ?></th>
            <th><?php echo __("Title", "Title"); ?></th>
            <th><?php echo __("Locker", "Locker"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Route", "Route"); ?></th>
            <th><?php echo __("Updated", "Updated"); ?></th>
            <th><?php echo __("Type", "Type"); ?></th>
            <th> <?php echo __("Delete", "Delete?"); ?> </th>
        </tr>
    </thead>
    <tbody>
        <?
        if(!empty($pickups)):
            foreach($pickups as $pickup):
            //if(strtotime($pickup->dateUpdated) < strtotime("9am today")):
        ?>
        <tr>
            <td><?= $pickup->claimID ?></td>
            <td><?= $pickup->serviceType ?></td>
            <td><a href="/admin/orders/view_location_orders/<?= $pickup->locationID ?>" target="_blank"><?= $pickup->address ?></a></td>
            <td><a href="/admin/orders/view_location_orders/<?= $pickup->locationID ?>" target="_blank"><?= $pickup->companyName ?></a></td>
            <td>
                <a href="/admin/reports/locker_history/<?= $pickup->locker_id ?>" target="_blank"><?= $pickup->lockerName ?></a>
                <table id="none">
                    <?foreach($lockOrders[$pickup->customer_id] as $order): ?>
                    <tr>
                        <td style="background: 0; border:0;"><a href="/admin/orders/details/<?= $order->orderID ?>" target="_blank"><?= $order->orderID ?></a></td>
                        <td style="background: 0; border:0;"><?= convert_from_gmt_aprax($order->dateCreated, $dateFormat) ?></td>
                        <td style="background: 0; border:0;"><?= substr($order->locAddress, 0, 10) ?></td>
                        <td style="background: 0; border:0;"><?= substr($order->firstName, 0, 1) ?>. <?= $order->firstName ?></td>
                    </tr>
                    <? endforeach; ?>
                </table>
            </td>
            <td>
                <a href="/admin/customers/detail/<?= $pickup->customer_id ?>" target="_blank"><?= getPersonName($pickup) ?> </a>
                <table>
                    <?foreach($custOrder[$pickup->customer_id] as $order): ?>
                    <tr>
                        <td style="background: 0; border:0;"><a href="/admin/orders/details/<?= $order->orderID ?>" target="_blank"><?= $order->orderID ?></td>
                        <td style="background: 0; border:0;"><?= convert_from_gmt_aprax($order->dateCreated, $dateFormat) ?></td>
                        <td style="background: 0; border:0;"><?= substr($order->locAddress, 0, 10) ?></td>
                        <td style="background: 0; border:0;"><?= $order->lockerName ?></td>
                    </tr>
                    <? endforeach; ?>
                </table>
            </td>
            <td><?= $pickup->route_id ?></td>
            <td
                <? if(strtotime(convert_from_gmt_aprax($pickup->dateUpdated, $fullDateFormat)) > strtotime($cutoff)): ?> style="background-color: Yellow; background-image: none;"<? endif; ?>
            ><?= convert_from_gmt_aprax($pickup->dateUpdated, $fullDateFormat)?></td>
            <td><?
                    if($pickup->orderType){
                            $string = '';
                            foreach(unserialize($pickup->orderType) as $ot)
                            {
                                    $string .= $serviceTypes[$ot].', ';
                            }
                            echo rtrim($string, ', ');
                    }
                    ?>
                    <br>
                    <?= $pickup->notes ?>
            </td>
            <td>
                <?
                if($CI->zacl->check_acl('delete_claims')): ?>
                    <a onclick="return verify_delete()" href="/admin/reports/problems_delete_claim/<?= str_replace(",", "-", $showRoutes) ?>/<?= $pickup->claimID ?>"><img src="/images/icons/cross.png"></a> (<?php echo __("no email", "no email"); ?>)
                    <br>
                    <br>
                    <a onclick="return verify_delete()" href="/admin/orders/claimed_noOrd/<?= $pickup->claimID ?>"><img src="/images/icons/cross.png"></a> (<?php echo __("send email", "send email"); ?>)
                <? endif; ?>
            </td>
        </tr>
        <? endforeach; ?>
        <? else: ?>
                <tr><td colspan='10'><?php echo __("No missed pickups for route", "No missed pickups for route"); ?> <?= $showRoutes ?></td></tr>
        <? endif; ?>
    </tbody>
</table>
<br>
<div style="float:left"><h3><a name="notes"><?php echo __("Open Notes", "Open Notes"); ?></a></h3></div>
<div style="float:right"><a href="/admin/reports/driver_notes"><?php echo __("Notes", "Notes"); ?></a></div>
<table id="box-table-a">
	<thead>
		<th><?php echo __("Route", "Route"); ?></th>
		<th><?php echo __("Created By", "Created By"); ?></th>
		<th><?php echo __("Date", "Date"); ?></th>
		<th><?php echo __("Address", "Address"); ?></th>
        <th><?php echo __("Title", "Title"); ?></th>
		<th><?php echo __("Note", "Note"); ?></th>
		<th></th>
	</thead>
	<tbody>
	<? if(!empty($notes)): foreach($notes as $n): ?>
		<tr>
			<td><?= $n->route_id ?></td>
			<td><font size="-1"><?= get_employee_name($n->createdBy) ?></font></td>
			<td><font size="-1"><?= date("D m/d h:ia", strtotime($n->created)) ?></font></td>
			<td><?= $n->address ?></td>
            <td><?= $n->companyName ?></td>
			<td><?= $n->note ?></td>
			<td valign="bottom">__________________________________</td>
		</tr>
	<? endforeach; ?>
	<? else: ?>
	<tr><td colspan='10'><?php echo __("No notes for route", "No notes for route"); ?> <?= $showRoutes ?></td></tr>
	<? endif; ?>
	</tbody>
</table>
<p>

<?php if(!empty($stopsMissed)): ?>
<div style="float:left"><h3><a name="notes"><?php echo __("Stops Not Scanned By Driver", "Stops Not Scanned By Driver"); ?></a></h3></div>
<div style="float:right"><a href="#deliveries"><?php echo __("Deliveries", "Deliveries"); ?></a> | <a href="#pickups"><?php echo __("Pickups", "Pickups"); ?></a> | <a href="#notes"><?php echo __("Notes", "Notes"); ?></a></div>
<table id="box-table-a">
	<thead>
		<th><?php echo __("Route", "Route"); ?></th>
		<th><?php echo __("Address", "Address"); ?></th>
        <th><?php echo __("Title", "Title"); ?></th>
	</thead>
	<tbody>
	<? foreach($stopsMissed as $stopMissed): ?>
		<tr>
			<td><?= $stopMissed->route_id ?></td>
			<td><?= $stopMissed->address ?></td>
            <td><?= $stopMissed->companyName ?></td>
		</tr>
	<? endforeach; ?>
	</tbody>
</table>
<p>
<? endif; ?>


<?php if($acceptedRoutes): ?>
<h3><?php echo __("Accepted Routes", "Accepted Routes"); ?></h3>
<table class='box-table-a'>
    <tr>
        <th><?php echo __("Route", "Route"); ?></th>
        <th><?php echo __("Employee", "Employee"); ?></th>
        <th><?php echo __("Date", "Date"); ?></th>
    </tr>
<?php $routes = explode("-", $routes);
foreach($acceptedRoutes as $acceptedRoute): ?>
<tr>
    <td><?php echo $acceptedRoute->routes ?></td>
    <td><?php echo \App\Libraries\DroplockerObjects\Employee::getName($acceptedRoute->employee_id); ?></td>
    <td><?php echo convert_from_gmt_aprax($acceptedRoute->dateCreated->date) ?></td>
</tr>
<?php

$key = array_search($acceptedRoute->routes, $routes);
if($key !== false){
    unset($routes[$key]);
}
endforeach; ?>
</table>
<?php endif; ?>

<?php if($routes && $CI->zacl->check_acl('accept_possible_problem_report')): ?>

    <?php foreach($routes as $route): ?>
    <p>
        <a href='/admin/reports/accept_possible_problem_report/<?php echo $route ?>' class='button orange'><?php echo __("Accept Report for Route", "Accept Report for Route:"); ?> <?php echo $route?></a>
    </p>
    <?php endforeach; ?>

<?php endif; ?>
</p>
<br>
<a href="/admin/tickets/add_ticket/"><?php echo __("Submit a maintenance request", "Submit a maintenance request"); ?></a><br>
<br>

