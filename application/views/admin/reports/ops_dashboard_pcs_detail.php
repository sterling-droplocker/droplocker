<?php enter_translation_domain("admin/reports/ops_dashboard_pcs_detail"); ?>
<h2><?php echo __("Operations Dashboard Pcs Detail", "Operations Dashboard Pcs Detail"); ?></h2>
<br>
<br>
<table class="box-table-a" style="width: 500px;">
<tr>
    <th></th>
    <th><?php echo __("Item", "Item"); ?></th>
    <th><?php echo __("Updated", "Updated"); ?></th>
    <th><?php echo __("Order", "Order"); ?></th>
</tr>
<? foreach($allItems as $item): $counter++?>
<tr>
    <td><?= $counter ?></td>
    <td><?= $item->item_id ?></td>
    <td><?= convert_from_gmt_aprax($item->updated, STANDARD_DATE_FORMAT)?></td>
    <td><a href="/admin/orders/details/<?= $item->order_id ?>" target="_blank"><?= $item->order_id ?></a></td>
</tr>
<? endforeach; ?>
</table>