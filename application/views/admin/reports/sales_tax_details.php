<?php enter_translation_domain("admin/reports/sales_tax_details"); ?>
<h2><?php echo __("Sales Tax Details for", "Sales Tax Details for"); ?> <?= $month ?></h2>
<div style="padding:8px;">
<table id="box-table-a">
<thead>
<tr>
    <th><?php echo __("Order", "Order"); ?></th>
    <th><?php echo __("Date Picked Up", "Date Picked Up"); ?></th>
    <th><?php echo __("Customer", "Customer"); ?></th>
    <th align="right"><?php echo __("Closed Gross", "Closed Gross"); ?></th>
    <th align="right"><?php echo __("Closed Discounts", "Closed Discounts"); ?></th>
    <th align="right"><?php echo __("Tax", "Tax"); ?></th>
</tr>
</thead>
<tbody>
<? foreach($taxes as $tax):
    $totGross += $tax->closedGross;
    $totDisc += $tax->closedDisc;
    $totTax += $tax->tax;
?>
<tr>
    <td><a href="/admin/orders/details/<?= $tax->orderID ?>" target="_blank"><?= $tax->orderID ?></a></td>
    <td><?= convert_from_gmt_aprax($tax->dateCreated, SHORT_DATE_FORMAT) ?></td>
    <td><a href="/admin/customers/detail/<?= $tax->customerID ?>" target="_blank"><?= getPersonName($tax) ?></a></td>
    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $tax->closedGross) ?></td>
    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $tax->closedDisc) ?></td>
    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $tax->tax) ?></td>
</tr>
<? endforeach; ?>
<tfoot>
    <tr>
        <th colspan=3><?php echo __("Total", "Total"); ?></th>
        <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n',$totGross) ?></th>
        <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n',$totDisc) ?></th>
        <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n',$totTax) ?></th>
    </tr>
</tfoot>
</tbody>
</table>