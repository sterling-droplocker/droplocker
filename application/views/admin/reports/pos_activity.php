<?php enter_translation_domain("admin/reports/$feature"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = '{ "mDataProp":"firstName" },';
$fieldList['lastName'] = '{ "mDataProp":"lastName" },';
            
$nameColSetup = getSortedNameForm($fieldList);

$fieldList = array();
$fieldList['firstName'] = '<th>' . __("First Name","First Name") . '</th>';
$fieldList['lastName'] = '<th>' . __("Last Name","Last Name") . '</th>';
            
$nameHeader = getSortedNameForm($fieldList);

?>

<h2><?php echo $title ?></h2>

<?php if (count($cash_registers)) : ?>

<script type="text/javascript">
jQuery(document).ready(function($){
    $("#pos_activity").dataTable({
        "bSort" : false,
        "iDisplayLength": 20, 
        "bProcessing" : true,
        "bServerSide" : true,
        "bStateSave" : true,
        "sAjaxSource" : "/admin/reports/<?php echo $feature ?>/",
        "fnPreDrawCallback": function( oSettings ) {
            oSettings._iDisplayLength = 20;
        },
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "start_date", "value": "<?php echo $start_date ?>" } );
          aoData.push( { "name": "end_date", "value": "<?php echo $end_date ?>" } );
          aoData.push( { "name": "cash_register_id", "value": "<?php echo $cash_register_id ?>" } );
        },
        "bJQueryUI": true,
        "sDom": 'tr<"F"ip>',
        "aoColumns": [
            { "mDataProp":"date" },
            <?= $nameColSetup ?>
            { "mDataProp":"record_type_name" },
            { "mDataProp":"payment_method_name" },
            { "mDataProp":"paid"},
            { "mDataProp":"amount"},
            { "mDataProp":"change"},
            { "mDataProp":"notes"},
        ]
    });


	$(".datefield").datepicker({
		showOn: "button",
		buttonImage: "/images/icons/calendar.png",
		buttonImageOnly: true,
		dateFormat: "yy-mm-d"
	});
});
</script>
<div style="padding: 8px;">
    <form action="" method="get">
        <table>
            <tr>
                <td>
                    <input readonly="true" name="start_date" value="<?= $start_date ?>" class="datefield"> 
                    <input readonly="true" name="end_date" value="<?= $end_date ?>" class="datefield">
                    <?php echo form_dropdown('cash_register_id', $cash_registers, $cash_register_id); ?>
                    <input type="submit" name="Submit" value="<?php echo __("Run", "Run"); ?>" class="button blue">
                </td>
            </tr>
        </table>
    </form>
</div>
<br>

<table id='pos_activity'>
	<thead>
		<tr>
                    <th><?= __("Date","Date")?></th>
                    <?= $nameHeader ?>
                    <th><?= __("record_type","Record Type")?></th>
                    <th><?= __("payment_method","Payment Method")?></th>
                    <th><?= __("paid","paid")?></th>
                    <th><?= __("amount","amount")?></th>
                    <th><?= __("change","change")?></th>
                    <th><?= __("notes","notes")?></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>

<?php else : ?>

<div> <?= __("no_cash_register", "Your business does not have cash registers to show") ?> </div>

<?php endif ?>

<?php leave_translation_domain(); ?>
