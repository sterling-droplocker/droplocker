                <?php $retentionRateCategory[$key][$i] = $retentionRateCategory[$key][$i] == 'N/A' ? 'N/A' : number_format_locale($retentionRateCategory[$key][$i] * 100, 0) . '%'; ?>
            <?php $retentionRate[$i] = $retentionRate[$i] == 'N/A' ? 'N/A' : number_format_locale($retentionRate[$i] * 100, 0) . '%'; ?>
<table id="details">
    <thead>
        <tr>
            <th><?= __("title", "Title"); ?></th>
            <th><?= __("address", "Address"); ?></th>
            <th><?= __("units", "Units"); ?></th>
            <th><?= __("lockers", "Lockers"); ?></th>
            <th><?= __("type", "Type"); ?></th>
            <th><?= __("frequency", "Frequency"); ?></th>
            <th><?= __("Customer Utilization", "Customer Utilization") ?></th>
            <th><?= __("Locker Utilization", "Locker Utilization") ?></th>
            <th><?= __("Route", "Route"); ?></th>
            <th><?= __("Target/Custom", "custom1"); ?></th>
            <? foreach($headers as $h): ?>
            <th><?= $h ?></th>
            <? endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <? foreach($location as $key => $l): ?>
        <tr>
            <td><?= $title[$key] ?></td>
            <td><?= $address[$key] ?></td>
            <td><?= $units[$key] ?></td>
            <td><?= isset($lockers[$key]) ? array_sum($lockers[$key]) : 0 ?></td>
            <td><?= $repClass[$key] ?></td>
            <td><?= $serviceType[$key] ?></td>
            <td><?= $utilization[$key]; ?></td>
            <td><?= $lockerUtilization[$key]; ?></td>
            <td><?= $route[$key] ?></td>
            <td><? if($target[$key]) { echo $target[$key]; } ?></td>
            <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
            <?php $l[$i] = empty($l[$i]) ? 0 : $l[$i]; ?>
            <td align="right" data-value="<?= number_format($l[$i], 2, ".", "") ?>">
                <? if ($l[$i]): ?>
                    <? if (empty($selectMonth)): ?>
                    <? if ($display_order_counts == true): ?>
                                <?= $numOrds[$key][$i] ?>
                    <? else: ?>
                        <?= format_money_symbol($this->business->businessID, '%.2n', $l[$i]); ?>
                    <? endif; ?>
                    <? else: ?>
                       
                        <?php $add_months = (-1 + $numMonths - $i);
                              $on_month = date('Y-n', strtotime("$add_months months", strtotime($selectMonth)));?>
                        <a href="/admin/reports/by_location_details/<?= $on_month ?>/<?= $key ?>">
                            <? if ($display_order_counts == true): ?>
                                <?= $numOrds[$key][$i] ?>
                            <? else: ?>
                                <?= format_money_symbol($this->business->businessID, '%.2n', $l[$i]); ?>
                            <? endif; ?>
                        </a>
                    <? endif; ?>
                <? endif; ?>
            </td>
            <? endfor; ?>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
