<?php enter_translation_domain("admin/reports/order_count"); ?>
<?php

$baseTime = $timeStart;
$delta = 86400;
if ($sortOrder == 'desc') {
    $baseTime = $timeEnd;
    $delta = -86400;
}

?>
<h2><?php echo __("Order Count By Location", "Order Count By Location"); ?></h2>

<br>
<form method="get">
From <input name="dateStart" class="date" value="<?= $dateStart ?>">
To <input name="dateEnd" class="date" value="<?= $dateEnd ?>">
Order <select name="sortOrder">
    <option value="asc" <?= $sortOrder == 'asc' ? 'selected' : '' ?>><?php echo __("ASC", "ASC"); ?></option>
    <option value="desc" <?= $sortOrder == 'desc' ? 'selected' : '' ?>><?php echo __("DESC", "DESC"); ?></option>
</select>
<input type="submit" value="<?php echo __("Apply", "Apply"); ?>">
</form>
<br><br>


<table id="orders">
    <thead>
    <tr>
        <th><?php echo __("Address", "Address"); ?></th>
        <th><?php echo __("Frequency", "Frequency"); ?></th>
        <? for($i = 0; $i <= $dayCount; $i++): ?>
        <th align="right"><?= date("D m/d", $baseTime + $i * $delta) ?></th>
        <? endfor; ?>
    </tr>
    </thead>
    <tbody>
    <? foreach($orders as $key => $order): ?>
    <tr>
        <td><?= $order['address'] ?></td>
        <td><?= $frequency[$key] ?></td>
        <? for($i = 0; $i <= $dayCount; $i++, $key = date("Y-m-d", $baseTime + $i * $delta)): ?>
        <td align="right"><?= $order[$key]->ordCount ?></td>
        <? endfor; ?>
    </tr>
    <? endforeach; ?>
    </tbody>
</table>

<script type="text/javascript">
$(document).ready(function() {
    $("#orders").dataTable({
        "sScrollX": "100%",
        "bPaginate" : false,
        "bScrollCollapse": true,
        "bJQueryUI": true,
        "sDom": '<"H"Tf>t<"F"i>',
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    })

    $(".date").datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        dateFormat: "yy-mm-d"
    });


});
</script>

