<?php enter_translation_domain("admin/reports/signupDetails"); ?>
<h2><?php echo __("Signups for", "Signups for"); ?> <?= $this->input->get_post("month") ?></h2>

<table id="box-table-a">
    <thead>
        <tr>
            <th></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Signup Date", "Signup Date"); ?></th>
            <th><?php echo __("Email", "Email"); ?></th>
        </tr>
    </thead>
    <tbody>
    <? foreach($customers as $customer): $counter ++;?>
    <tr>
        <td><?= $counter ?></td>
        <td><a href="/admin/customers/detail/<?= $customer->customerID ?>" target="_blank"><? if($customer->firstName != "") :?> <?= getPersonName($customer) ?><?else:?><?php echo __("no name provided yet", "no name provided yet"); ?><? endif; ?></a></td>
         <td><?= convert_from_gmt_aprax($customer->signupDate, SHORT_DATE_FORMAT) ?></td>
        <td><?= $customer->email ?></td>
    </tr>
    <? endforeach ?>    
    </tbody>
</table>