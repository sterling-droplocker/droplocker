<?php enter_translation_domain("admin/reports/assign_drivers"); ?>
<?= $this->session->flashdata('message') ?>

<style type="text/css">
    th {
        font-weight: bold;
    }
    td,th {
         border: 1px inset black;
         
    }
</style>
<h2><?php echo __("Assign Drivers", "Assign Drivers"); ?></h2>
<br>
<a style="text-decoration: none;" href="/admin/reports/driver_map_v2"><?php echo __("Return to Driver Map", "<- Return to Driver Map"); ?> </a> <br /><br />
<?= form_open("/admin/emp_routes/update_routes") ?>
    <table cellspacing="2" cellpadding="2" style="border-spacing: 2px; padding-top: 2px; padding-left: 6px; padding-right: 6px; border: 1px solid black; border-style: outset;">
        <thead>
            <tr>
                <th> <?php echo __("Route", "Route"); ?> </th>
                <th> <?php echo __("Employee", "Employee"); ?> </th>
                <th> <?php echo __("Work Phone", "Work Phone"); ?> </th>
                <th> <?php echo __("Home Phone", "Home Phone"); ?> </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($all_routes as $route): ?>
                <tr>
                    <td> <?= $route->route_id?> </td>
                    <td> 
                        <?php
                            if ($route->employee_id)
                            {
                                echo form_dropdown("routes[{$route->route_id}]", $drivers, $route->employee_id); 
                            }
                            else
                            {
                                echo form_dropdown("routes[{$route->route_id}]", $drivers, 0);
                            }
                        ?> 
                    </td>
                    <td> <?= $route->llphone ?> </td>
                    <td> <?= $route->phoneHome ?> </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<br />
<div> <?= form_submit("", __("Update", "Update")) ?> </div>
<?= form_close(); ?>
