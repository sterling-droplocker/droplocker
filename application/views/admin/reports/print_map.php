<?php enter_translation_domain("admin/reports/print_map"); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>Print Route</title>
		<script type="text/javascript" src='/js/jquery-1.6.4.min.js'></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?=get_default_gmaps_api_key();?>"></script>
		<link rel="shortcut icon" href="/favicon.ico" />
		<link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
	<script>
		$(document).ready(function(){
			var colors = new Array('cc0000','ccff00','3698fb','6ed0f7','FF1493','87b93e','CD69C9','CD5C5C','FFA500','9370DB','90EE90','CAFF70','EE7621','FF1493','FFD39B','FFFF00');
			var myOptions = {
	          zoom: 8,
	          center: new google.maps.LatLng(37, -122),
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
			var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
			var markers = <?= $markers?>;
			var lines = <?= $lines ?>;
			
			var bounds = new google.maps.LatLngBounds();
			var latlngs = new Array();
	            $(markers).each(function(i,item){
	           
		            var latlng = new google.maps.LatLng(item.lat, item.lng);
					var marker = new google.maps.Marker({
					      position: latlng, 
					      map: map, 
					      title:item.street,
					      icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+(i+1)+'|'+colors[item.route_id]+'|000000'
					  }); 
					latlngs.push(latlng);
	   				bounds.extend(latlng);
				    
	            });
	            
	            var routePath = new google.maps.Polyline({
		            path: latlngs,
		            strokeColor: "#FF0000",
		            strokeOpacity: 1.0,
		            strokeWeight: 3
		        })
		        routePath.setMap(map);
	            map.fitBounds(bounds);
	            
	            $('#print').click(function(){
	            	window.print();
	            });
	            	
	            
	       
		});
		
	
	</script>
	</head>
	<body>
		<div style='text-align: left;padding:5px;'>
		<a  href='javascript:;' id='print'><?php echo __("Print Route", "Print Route"); ?></a>
		</div>
		<div id='map_canvas' style='outline:1px solid #ccc; width:800px; height:970px;margin:0 auto'></div>
		<div style='float:left;width:50%' id='left_column'>
			<?= $left_column ?>
		</div>
		<div style='float:left;width:50%' id='right_column'>
			<?= $right_column ?>
		</div>
	</body>
</html>
