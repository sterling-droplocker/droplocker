<?php enter_translation_domain("admin/reports/howHear"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("How did you hear about us", "How did you hear about us?"); ?></h2>
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>
        <form action="/admin/reports/how_hear" method="post">
        <? if($rowCount > 500): ?><?php echo __("Last 500 of", "Last 500 of"); ?> <? endif; ?> <?= $rowCount ?> <?php echo __("Signups", "Signups:"); ?></a>
    </td>
    <td align="right">    
       <input type="checkbox" name="noOrders" value="1" <?= $_POST['noOrders'] == 1 ? 'CHECKED' : ''; ?>> <?php echo __("Show customers with no orders", "Show customers with no orders"); ?>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="searchParam" value="<?= @$_POST['searchParam'] ?>"> <input type="submit" name="search" value="<?php echo __("Search", "Search"); ?>" class="button orange"></form>
    </td>
</tr>
</table>
<br>
<table id="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("How", "How"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Last Order", "Last Order"); ?></th>
            <th><?php echo __("Number of Orders", "# Orders"); ?></th>
            <th><?php echo __("Signup Date", "Signup Date"); ?></th>
            <th><?php echo __("Email", "Email"); ?></th>
        </tr>
    </thead>
    <tbody>
    <? foreach($customers as $customer): ?>
    <tr>
        <td><?= $customer->how ?></td>
        <td><a href="/admin/customers/detail/<?= $customer->customerID ?>" target="_blank"><?= getPersonName($customer) ?></a></td>
        <td><?= $customer->orderCount > 0 ? convert_from_gmt_aprax($customer->lastOrder, SHORT_DATE_FORMAT) : '-' ?></td>
        <td><?= $customer->orderCount ?></td>
        <td><?= convert_from_gmt_aprax($customer->signupDate, $dateFormat) ?></td>
        <td><?= $customer->email ?></td>
    </tr>
    <? endforeach ?>    
    </tbody>
</table>

