<?php enter_translation_domain("admin/reports/display_discounts_given_over_time_period_form"); ?>
<h2> <?php echo __("Discounts Given over Time Period", "Discounts Given over Time Period"); ?> </h2>
<script type="text/javascript">
$(document).ready(function() {
    $("input[name='start_date']").datepicker();
    $("input[name='end_date']").datepicker();

    $('#report_date_form').validate({
        rules: {
            start_date : {
                required: true,
            },
            end_date : {
                required: true,
            }
        },
        errorClass : "invalid"
    });

});
</script>
<?= form_open("/admin/reports/get_discounts_given_over_time_period", array('id' => 'report_date_form')) ?>

    <?= form_label(__("Start Date", "Start Date"), "start_date") ?> <?= form_input("start_date") ?>

    <?= form_label(__("End Date", "End Date"), "end_date") ?> <?= form_input("end_date") ?>
    <?= form_submit(array("class" => "button blue", "value" => __("Generate Report", "Generate Report"))) ?>
<?= form_close() ?>
