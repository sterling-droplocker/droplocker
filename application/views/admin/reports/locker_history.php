<?php enter_translation_domain("admin/reports/locker_history"); ?>
<?php $standardDateFormat = get_business_meta($this->business_id, "standardDateDisplayFormat"); ?>
<h2><?php echo __("Locker History", "Locker History"); ?></h2>
<br>
<form action="" method="POST">
    <?php lockerDropdown($locker); ?>
    <input type="submit" value="<?php echo __("Run", "Run"); ?>" />
</form>
<br>
<? if(!$locker): ?>
<?php echo __("No Locker Passed", "No Locker Passed"); ?>
<?else: ?>
<table id="box-table-a">
	<thead>
		<tr>
			<th><?php echo __("Date Created", "Date Created"); ?></th>
			<th><?php echo __("OrderID", "OrderID"); ?></th>
			<th><?php echo __("Customer", "Customer"); ?></th>
			<th><?php echo __("Status", "Status"); ?></th>
		</tr>
	</thead>
	<tbody>
		<? if($history): foreach($history as $h):?>
		<tr>
			<td><?= convert_from_gmt_aprax($h->statusDate, $standardDateFormat) ?></td>
			<td><a href="/admin/orders/details/<?= $h->orderID ?>" target="_blank"><?= $h->orderID ?></a></td>
                        <td><a href="/admin/customers/detail/<?= $h->customer_id ?>" target="_blank"><?= getPersonName($h) ?></a></td>
			<td><?= $h->name ?></td>
		</tr>
		<? endforeach; else:?>
		<tr><td colspan='4'><?php echo __("No history for this locker", "No history for this locker"); ?></td></tr>
		<? endif; ?>
	</tbody>
</table>
<?endif; ?>