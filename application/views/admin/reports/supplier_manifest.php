<?php enter_translation_domain("admin/reports/supplier_manifest"); ?>
<style type="text/css">
    .no_picture td {
        background-color: #ff9999 !IMPORTANT;
    }
    .highlight td {
        background-color: #f7f986 !IMPORTANT;
    }
</style>
<script type="text/javascript">

$(document).ready(function() {

    $(".datefield").datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        dateFormat: "yy-mm-dd"
    });

    var supplier_manifest_dataTable = $("table").dataTable({
        "aaSorting": [],
        "bPaginate" : false,
        "bsort" : false,
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    });

    $("#barcode_id").focus()

    $(".cut_off_time").timepicker({format: "HH:MM"});
});

</script>



<h2 style="margin-bottom: 4px;"><?php echo __("Supplier Manifest", "Supplier Manifest"); ?></h2>
    <?= form_open("") ?>
    <?php echo __("Select Supplier:", "Select Supplier:"); ?>
    <?= form_dropdown("supplier", $suppliers, $_POST['supplier']) ?>

    <input type="text" class="datefield" name="selectedDateStart" value="<?= $selectedDateStart ?>">

    <?= form_input(array("name" => "cutOffStart", "class" => "cut_off_time", "value" => $cutOffStart, "size" => 6)) ?>

    to
    <input type="text" class="datefield" name="selectedDateEnd" value="<?= $selectedDateEnd ?>">

    <?= form_input(array("name" => "cutOffEnd", "class" => "cut_off_time", "value" => $cutOffEnd, "size" => 6)) ?>

    <?= form_multiselect("process[]", $processes, $_POST['process']) ?>

    </select>
    <input type="checkbox" name="details" value="1"  <? if($_POST['details'] == '1') echo ' checked'; ?>><?php echo __("Item Details", "Item Details"); ?>
    <input type="checkbox" name="margin" value="1"  <? if($_POST['margin'] == '1') echo ' checked'; ?>><?php echo __("Show pricing", "Show pricing"); ?>
    <? if($_POST['details'] == '1') echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;'.__("Barcode", "Barcode:").' <input type="text" name="barcode" id="barcode_id" size="12">&nbsp;&nbsp;'; ?>
    <?= form_checkbox("receipt_printer_friendly", "1") ?> <?= form_label(__("Receipt Printer Friendly", "Receipt Printer Friendly")) ?>
    <input type="submit" name="run" value="<?php echo __("Run", "Run"); ?>">
    <?= form_close() ?>
<br>


<? if($items): ?>
<? if($_POST['details']): ?>
<div style="margin-bottom: 4px; clear:both; float:left;"> <?php echo __("Items checked", "Items checked in between %date1% and %date2%:", array("date1"=>date("D m/d/y g:ia", strtotime($startDate)), "date2"=>date("D m/d/y g:ia", strtotime($endDate)))); ?></div>
<form action="/admin/admin/process_partials" method="post">
<table>
    <thead>
        <tr>
            <? if($_POST['barcode']) echo '<th>'.__("Scanned", "Scanned").'</th>'; ?>
            <th> <?php echo __("Supplier Name", "Supplier Name"); ?> </th>
            <th><?php echo __("Order", "Order"); ?></th>
            <th><?php echo __("Checked In", "Checked In"); ?></th>
            <th><?php echo __("Barcode", "Barcode"); ?></th>
            <th><?php echo __("Product", "Product"); ?></th>
            <th><?php echo __("Process", "Process"); ?></th>
            <th><?php echo __("Product ID", "Product ID"); ?></th>
            <th><?php echo __("Assembled", "Assembled"); ?> </tH>
            <th align="right"><?php echo __("Qty", "Qty"); ?></th>
            <? if($_POST['margin'] == '1'): ?><th align="right"><?php echo __("Sales Price", "Sales Price"); ?></th><? endif; ?>
            <th align="right"><?php echo __("Cost", "Cost"); ?></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($details as $detail): ?>
            <?php
                $pictures = \App\Libraries\DroplockerObjects\Picture::search_aprax(array("item_id" => $detail->item_id));
            ?>

            <?php if (empty($pictures)): ?>
                <tr id="<?php echo $detail->orderItemID ?>" class="no_picture">
            <?php elseif($detail->scanned > '0000-00-0000'): ?>
                 <tr class="highlight">
            <?php else: ?>
                <tr>
            <?php endif; ?>
                <? if($_POST['barcode'])
                    {
                        echo '<td>';
                        if($detail->scanned > '0000-00-0000')
                        { echo $detail->scannedTimes; }//echo convert_from_gmt_aprax($detail->scanned, "g:ia"); }
                        echo '</td>';
                    }
                ?>
                <td> <?= $detail->companyName ?> </td>
                <td><a href="/admin/orders/details/<?= $detail->orderID ?>" target="_blank"><?= $detail->orderID ?></a></td>
                <td><?= convert_from_gmt_aprax($detail->updated, "Y-m-d g:ia D") ?></td>


                <td>
                    <a href="/admin/items/view?itemID=<?= $detail->item_id ?>" target="_blank"><?= $detail->barcode ?></a>

                    <?php if (empty($pictures)): ?>
                        <img style="width: 20px; height: 20px; display: inline;"alt="No Picture" src="/images/no_picture.jpg" />
                    <?php endif; ?>
                </td>
                <td><?= $detail->product ?></td>
                <td><?= $detail->process ?></td>
                <td><?= $detail->productID ?></td>
                <td>
                    <?= $detail->assembled ?> <? if ($detail->assembled == 'NOT ASSEMBLED' and isset($detail->barcode)): ?> <input type="checkbox" name="partial[]" value="<?= $detail->orderItemID ?>" checked> <? endif; ?>
                </td>
                <td align="right"><?= $detail->qty ?></td>
                <? if($_POST['margin'] == '1'): ?><td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $detail->unitPrice * $detail->qty) ?></td><? endif; ?>
                <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $detail->cost * $detail->qty) ?></td>
            </tr>
        <? endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <? if($_POST['barcode']) echo '<th></th>';  ?>
            <th colspan=6>Total</th>
            <th><input type="submit" name="process" value="Process Partials"></th>
            <th align="right"><?= $total['qty'] ?></th>
            <? if($_POST['margin'] == '1'): ?><th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total['unitPrice']) ?></th><? endif ?>
            <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total['cost']) ?></th>
        </tr>
    </tfoot>
</table>
</form>
<? else: ?>
<div style="clear:both; float:left;"> <?php echo __("Items checked in between", "Items checked in between %date1% and %date2%:", array("date1"=>date("D m/d/y g:ia", strtotime($startDate)), "date2"=>date("D m/d/y g:ia", strtotime($endDate)))); ?></div>
<table>
<thead>
    <tr>
        <th> <?php echo __("Supplier Name", "Supplier Name"); ?> </th>
        <th><?php echo __("Product", "Product"); ?></th>
        <th><?php echo __("Process", "Process"); ?></th>
        <th><?php echo __("Product ID", "Product ID"); ?></th>
        <th align="right"><?php echo __("Qty", "Qty"); ?></th>
        
        <? if($_POST['margin'] == '1'): ?><th align="right"><?php echo __("Sales Price", "Sales Price"); ?></th><? endif; ?>
        <th align="right"><?php echo __("Cost", "Cost"); ?></th>
        
    </tr>
</thead>
<tbody>
<? foreach($items as $key => $value): ?>
    <tr>
        <td><?= $value['companyName'] ?> </td>

        <td><?= $key ?></td>
        <td><?= $value['productID'] ?> </td>
        <td align="right"><?= $value['qty'] ?></td>
        
        <? if($_POST['margin'] == '1'): ?><th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $value['unitPrice']) ?></th><? endif; ?>
        <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $value['cost']) ?></td>
    </tr>
<? endforeach; ?>
</tbody>
<tfoot>
    <tr>
        <th colspan=4><?php echo __("Total", "Total"); ?></th>
        <th align="right"><?= $total['qty'] ?></th>
        <? if($_POST['margin'] == '1'): ?><th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total['unitPrice']) ?></th><? endif; ?>
        <th align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $total['cost']) ?></th>
    </tr>
</tfoot>
</table>
<? endif; ?>
<? else: ?>
<div style="clear:both; float:left;">
    <?php echo __("No data.  Try re-running the report", "No data.  Try re-running the report."); ?>
</div>

<? endif; ?>


