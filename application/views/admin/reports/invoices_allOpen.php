<?php enter_translation_domain("admin/reports/invoices_allOpen"); ?>
<h2><?php echo __("All Open Invoices", "All Open Invoices"); ?></h2>
<? if(!empty($unpaidInvoices)): ?>
<table id="box-table-a">
	<thead>
		<tr>
			<th><?php echo __("Customer", "Customer"); ?></th>
			<th><?php echo __("Inv Number", "Inv #"); ?></th>
			<th><?php echo __("Date", "Date"); ?></th>
			<th align="right"><?php echo __("Amount", "Amount"); ?></th>
		</tr>
	</thead>
	<tbody>
	<? if($unpaidInvoices == 'x'): ?>
	<tr><td colspan="7"><?php echo __("No Open Invoices", "No Open Invoices"); ?></td></tr>
	<? else: $openTotal = 0; foreach($unpaidInvoices as $unpaid_order => $inv): $openTotal += $inv['total'] ?>
	<tr><td><?= $inv['cust'] ?></td><td><a href="/admin/reports/invoices_generate/view/<?= $unpaid_order ?>"><?= $unpaid_order ?></a></td><td><?= $inv['dateInvoiced'] ?></td><td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $inv['total']) ?></td></tr>
	<? endforeach; ?>
	</tbody>
	<tfoot>
	<tr><td colspan="3" align="right"><strong><?php echo __("Total Due", "Total Due:"); ?></strong></td><td align="right"><strong><?= format_money_symbol($this->business->businessID, '%.2n', $openTotal) ?></strong></td></tr>
	</tfoot>
	<? endif; ?>

</table>
<? else: ?>
<?php echo __("No open invoices", "No open invoices"); ?>
<? endif; ?>