<?php enter_translation_domain("admin/reports/activity_log"); ?>
<?php 
    $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); 
    $standardDateFormat = get_business_meta($this->business_id, "standardDateDisplayFormat"); 
?>
<h2><?php echo __("Complete Activity Log", "Complete Activity Log"); ?></h2>
<form action="" method="post">
    <br />
    <?php echo __("Select Date", "Select Date:"); ?>
    <select name="logDate">
        <?php
        for ($i = 0; $i < 180; $i++) {
            $currentDay = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
            echo '<option value="' . $currentDay . '"';
            if ($this->input->post("logDate") && $_POST['logDate'] == $currentDay)
                echo ' SELECTED';
            echo '>'. convert_from_gmt_aprax($currentDay, $dateFormat) .'</option>';
        }
        ?>
    </select>
    <input type="submit" name="submit" value="<?php echo __("go", "go"); ?>">
</form>
<br>
<table id="activityTable">
    <thead>
        <tr>
            <th><?php echo __("Employee", "Employee"); ?></th>	
            <th><?php echo __("Status", "Status"); ?></th>
            <th><?php echo __("Activity Time", "Activity Time"); ?></th>	
            <th><?php echo __("Order", "Order"); ?></th>	
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Note", "Note"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if(isset($activity)): foreach($activity as $a):?>
        <tr>
            <td><?= $a->eFirst . ' ' . $a->eLast ?></td>
            <td nowrap><?= convert_from_gmt_aprax($a->date, $standardDateFormat) ?></td>
            <td><?= $a->name ?></td>
            <td><a href="/admin/orders/details/<?= $a->orderID ?>" target="_blank"><?= $a->orderID ?></a></td>
            <td><a href="/admin/customers/detail/<?= $a->customerID ?>" target="_blank"><?= $a->cFirst . ' ' . $a->cLast ?></a></td>
            <td><?= $a->note ?></td>
        </tr>
        <?php endforeach; else: ?>
        <tr><td colspan="6"><?php echo __("No Activity", "No Activity"); ?></td></tr>
        <?php endif; ?>
    </tbody>
</table>
<script type="text/javascript">
    $("#activityTable").dataTable({
       iDisplayLength : 100,
       aLengthMenu : [ [100, 250, 500, -1], [100, 250, 500, "All"] ] ,
       bJQueryUI : true
    });
</script>
