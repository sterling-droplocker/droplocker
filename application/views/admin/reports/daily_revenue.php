<?php enter_translation_domain("admin/reports/daily_revenue"); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#report_cut_off_time").timepicker({format: "HH:MM"});
    });
</script>
<h2><?php echo __("Daily Revenue", "Daily Revenue"); ?></h2>

<form action="" method="post">
<br>
From:
<select name="dateFrom">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if($dateFrom == $currentDay)
			echo ' SELECTED';
		echo '>'.$currentDay.'</option>';
	}
	?>
</select>
&nbsp;&nbsp;&nbsp;<?php echo __("To", "To:"); ?>
<select name="dateTo">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if($dateTo == $currentDay)
			echo ' SELECTED';
		echo '>'.$currentDay.'</option>';
	}
	?>
</select>
&nbsp;&nbsp;&nbsp;<?php echo __("Location", "Location:"); ?>
<select name="selectedLocation">
    <option value="%"><?php echo __("All", "All"); ?></option>
	<?
	foreach($locations as $l)
	{
		echo '<option value="'.$l->locationID.'"';
		if(@$_POST['selectedLocation'] == $l->locationID)
			echo ' SELECTED';
		echo '>'.$l->address.'</option>';
	}
	?>
</select>
&nbsp;
    <?= form_label(__("Cut Off", "Cut Off:"), "report_cut_off_time") ?>
    <?= form_input(array("name" => "report_cut_off_time", "id" => "report_cut_off_time", "value" => $cutOff, "size" => 5)) ?>
&nbsp;&nbsp;<input type="submit" name="submit" value="<?php echo __("go", "go"); ?>" class="button blue">
</form>
<br>
<? if(isset($order)): ?>
<table class="detail_table">
<tr>
	<th><?php echo __("Date", "Date"); ?></th>
	<? foreach($order as $day => $o): ?>
	<th><?= date("D m/d", strtotime($day)) ?></th>
	<? endforeach; ?>
</tr>
<tr>
	<th><?php echo __("Number Of Orders", "# Of Orders"); ?></th>
	<? foreach($order as $day => $c): ?>
	<td align="right">
		 <?= count($order[$day]) ?>
	</td>
	<? endforeach; ?>
</tr>
<tr>
	<th><?php echo __("Gross", "$ Gross"); ?></th>
	<? foreach($order as $day => $o): ?>
	<td align="right">
		 <?= format_money_symbol($this->business_id, '%.2n', $gross[$day]) ?>
	</td>
	<? endforeach; ?>
</tr>
<tr>
	<th><?php echo __("Discounts", "$ Discounts"); ?></th>
	<? foreach($order as $day => $o): ?>
	<td align="right">
		 <?= empty($discounts[$day])? format_money_symbol($this->business_id, '%.2n', 0) : format_money_symbol($this->business_id, '%.2n', $discounts[$day]) ?>
	</td>
	<? endforeach; ?>
</tr>
<tr>
	<th><?php echo __("Total", "Total"); ?></th>
	<? foreach($order as $day => $o): ?>
	<td align="right">
		 <?= format_money_symbol($this->business_id, '%.2n', $gross[$day] + $discounts[$day]) ?>
	</td>
	<? endforeach; ?>
</tr>
<tr>
	<th><?php echo __("Cost", "Cost"); ?></th>
	<? foreach($order as $day => $o): ?>
	<td align="right">
		 <?= format_money_symbol($this->business_id, '%.2n', $cost[$day]) ?>
	</td>
	<? endforeach; ?>
</tr>
<? if(!empty($reg)): ?>
<tr>
	<th><?php echo __("Hours Worked", "Hours Worked"); ?></th>
	<? foreach($order as $day => $o): ?>
	<td align="right">
		 <?= $reg[$day] ?> / <?= $ot[$day] ?>
         <br>
         <span style="font-weight: bold;"><?= format_money_symbol($this->business_id, '%.2n', $gross[$day] / ($reg[$day] + ($ot[$day]*1.5))) ?> / <?php echo __("hour", "hour"); ?></span>
	</td>
	<? endforeach; ?>
</tr>
<? endif; ?>
<tr>
	<th valign="top"><?php echo __("Transactions", "Transactions"); ?></th>
	<? foreach($order as $day => $o): ?>
	<td align="right" valign="top" nowrap>
		<? foreach($o as $order => $amount): ?>
		<a href="/admin/orders/details/<?= $order ?>" target="_blank"><?= $order ?></a> <?= format_money_symbol($this->business_id, '%.2n', $amount) ?><br>
		<? endforeach; ?>
	</td>
	<? endforeach; ?>
</tr>

</table>
<? else: ?>
<?php echo __("No data returned", "No data returned for %dateFrom% to %dateTo%. Try to select a different date.", array("dateFrom"=>$dateFrom, "dateTo"=>$dateTo)); ?>
<? endif; ?>
