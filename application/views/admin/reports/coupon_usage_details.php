<?php enter_translation_domain("admin/reports/coupon_usage_details"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("Customers who used Coupon Code", "Customers who used Coupon Code:"); ?> <?= $couponCode ?></h2>
<br>
<h3><?= $couponInfo[0]->extendedDesc ?> (<a href="/admin/reports/coupon_usage_details/<?= $couponCode ?>/all"><?php echo __("Show all coupons with this description", "Show all coupons with this description"); ?></a>)</h3>
<br>
<table id="box-table-a" style="width: 250px;">
<tr>
    <td style="border-top: 1px solid #CCCCCC;"><?php echo __("Qty Issued", "Qty Issued:"); ?></td>
    <td style="border-top: 1px solid #CCCCCC;"><?= $issued[0]->totCount ?></td>
</tr>
<tr>
    <td><?php echo __("Qty  Redeemed", "Qty  Redeemed:"); ?></td>
    <td><?= $redeemed[0]->totCount ?></td>
</tr>
<tr>
    <td><?php echo __("Redemtion Rate", "Redemtion Rate:"); ?></td>
    <td> <?= number_format_locale($redeemed[0]->totCount/$issued[0]->totCount * 100, 0) ?>%</td>
</tr>
</table>
<br>
<br>
<table id="box-table-a">
<tr>
    <th><?php echo __("Customer", "Customer"); ?></th>
    <th><?php echo __("Email", "Email"); ?></th>
    <th><?php echo __("Default Location", "Default Location"); ?></th>
    <th><?php echo __("Orders", "Orders"); ?></th>
    <th><?php echo __("Gross Receipts", "Gross Receipts"); ?></th>
    <th><?php echo __("Discounts", "Discounts"); ?></th>
    <th><?php echo __("Total Spend", "Total Spend"); ?></th>
    <th><?php echo __("Order Avg", "Order Avg"); ?></th>
    <th><?php echo __("Last Order", "Last Order"); ?></th>
    <th><?php echo __("Days Since Last Order", "Days Since Last Order"); ?></th>
</tr>
<? foreach($custOrders as $key => $c): ?>
<tr>
    <td><a href="/admin/customers/detail/<?= $key?>" target="_blank"><?= getPersonName($custInfo[$key]) ?></a></td>
    <td><?= $custInfo[$key]->email?></td>
    <td><?= $custInfo[$key]->address?></td>
    <td><?= empty($c[numOrd])?0:$c[numOrd] ?></td>
    <td><?= format_money_symbol($this->business->businessID, '%.2n', $c[ordAmt])?></td>
    <td><?= format_money_symbol($this->business->businessID, '%.2n', $c[discAmt])?></td>
    <? $totSpend = $c[ordAmt] + $c[discAmt] ?>
    <td><?= format_money_symbol($this->business->businessID, '%.2n', $totSpend)?></td>
    <td><?= format_money_symbol($this->business->businessID, '%.2n', $totSpend/$c[numOrd])?></td>
    <td><?= ($c[lastOrder]=="-")?$c[lastOrder]:date($dateFormat, strtotime($c[lastOrder])) ?></td>
    <td><?= ($c[daysSince]=="NO ORDERS")?__("NO ORDERS", "NO ORDERS"):$c[daysSince]; ?></td>
    </tr>

</tr>
<? endforeach; ?>
</table>
