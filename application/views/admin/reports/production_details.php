<?php enter_translation_domain("admin/reports/production_details"); ?>
<script type="text/javascript" src="/js/DataTables-1.9.1/custom/orderPriceColumns.js"></script>
<script type="text/javascript">
$(document).ready(function() {

        $(".datefield").datepicker({
            showOn: "button",
            buttonImage: "/images/icons/calendar.png",
            buttonImageOnly: true,
            dateFormat: "yy-mm-dd"
        });

        <?if(isset($totals)): ?>
        var box_table_a = $("#box-table-a").dataTable({
            "iDisplayLength" : 25,
            "bPaginate" : false,

            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                <? if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <? endif ?>
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            },
            "aoColumns": [
                null,
                null,
                null,
                { "sType": "price" },
                { "sType": "price" }
            ],
            "fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
                var totalPcs = 0;
                for (var i = 0; i < aiDisplay.length; i++) {
                    var number = aaData[aiDisplay[i]][2];
                    totalPcs += parseInt(number);
                }

                var totalRevenue = 0;
                for (var i = 0; i < aiDisplay.length; i++) {
                    var number = aaData[aiDisplay[i]][3].match(/[+\-]?\d+(,\d+)?(\.\d+)?/)[0].replace( /,/, "" );
                    totalRevenue += parseFloat(number);
                }

                var totalAvg = 0;
                totalAvg = parseFloat(totalRevenue/totalPcs);

                /* Modify the footer row to match what we want */
                var nCells = nRow.getElementsByTagName('th');
                nCells[2].innerHTML = totalPcs;

                const formatter = new Intl.NumberFormat('en-US', {
                  style: 'currency',
                  currency: 'USD',
                  minimumFractionDigits: 2
                });

                var nCells = nRow.getElementsByTagName('th');
                nCells[3].innerHTML = formatter.format(totalRevenue);  

                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = formatter.format(totalAvg);              
            }
        });
        <?php endif; ?>

        <?if(isset($details)): ?>
        var box_table_b = $("#box-table-b").dataTable({
            "iDisplayLength" : 25,
            "bPaginate" : false,

            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                <? if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <? endif ?>
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
        });
        <?php endif; ?>
});
</script>

<?php 
$decimals = get_business_meta($this->business_id, 'decimal_positions', 2);
?>

<h2><?php echo __("Production Details for", "Production Details for"); ?> <?= $service ?> on <?= $day ?></h2>
<br>
<form action="" method="get">
    <?php echo __("Day", "Day"); ?>:&nbsp;
    <input type="text" class="datefield" name="day" value="<?= $day ?>">
    <?php echo __("To", "To"); ?>:&nbsp;
    <input type="text" class="datefield" name="dateEnd" value="<?= $dateEnd ?>">
    &nbsp
    <? foreach($modules as $p):?>
        <input type="checkbox" name="modules[]" value="<?=$p->moduleID?>" <? if(in_array($p->moduleID, $selectedServices)) { echo 'checked'; } ?> ><?= $p->name?>
    <? endforeach; ?>&nbsp;&nbsp;
    <input type="submit" name="filter" value="<?php echo __("Submit", "Submit"); ?>" class="button orange">
    <input type="hidden" name="service" value="<?=$service ?>">
    <input type="hidden" name="status" value="<?=$status ?>">
<br>
<br>
<?if(isset($totals)): ?>

<table id="box-table-a">
    <thead>
    <tr>
        <th><?php echo __("Product", "Product"); ?></th>
        <th><?php echo __("Category", "Category"); ?></th>
        <th><?php echo __("Qty", "Qty"); ?></th>
        <th><?php echo __("Gross revenue", "Gross revenue"); ?></th>
        <th><?php echo __("Avg", "Avg"); ?></th>
    </tr>
    </thead>
    </tbody>
<? foreach($totals as $total): $totalPcs += $total->pcs; $totalPrice += $total->price; ?>
    <tr>
        <td><?= $total->productName ?></td>
        <td><?= $total->category ?></td>
        <td><?= (int)$total->pcs ?></td>
        <td><?= format_money_symbol($this->business->businessID, '%.2n', $total->price) ?></td>
        <td><?= format_money_symbol($this->business->businessID, '%.2n', $total->price / $total->pcs) ?></td>
    </tr>
<? endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th id="totalPcs"><?= (int)$totalPcs ?></th>
            <th><?= format_money_symbol($this->business->businessID, '%.2n', $totalPrice) ?></th>
            <th><?= format_money_symbol($this->business->businessID, '%.2n',  $totalPrice/$totalPcs) ?></th>
        </tr>
    </tfoot>
</table>
            
<br/>
<?php if (!$show_customer_details): ?>
    <input type="submit" name="show_customer_details" value="<?php echo __("Show customer details", "Show customer details"); ?>" class="button orange">
<?php else: ?>
    <input type="submit" name="" value="<?php echo __("Hide customer details", "Hide customer details"); ?>" class="button orange">
<?php endif; ?>
            
<? endif; ?>
 </form>
<br><br>
<?if(isset($details)): ?>
<h2><?php echo __("Customers details between %start% and %end%", "Customers details between %start% and %end%", array("start" => $day, "end" => $dateEnd)); ?></h2>
<table id="box-table-b">
<thead>
<tr>
    <th>#</th>
    <th><?php echo __("Customer", "Customer"); ?></th>
    <th><?php echo __("Order ID", "Order ID"); ?></th>
    <th><?php echo __("Item ID", "Item ID"); ?></th>
    <th><?php echo __("Qty", "Qty"); ?></th>
    <th><?php echo __("Product", "Product"); ?></th>
    <th><?php echo __("Service", "Service"); ?></th>
    <th><?php echo __("Created", "Created"); ?></th>
</tr>
</thead>
<tbody>
<? foreach($details as $d):
    $counter++;
?>
    <tr>
        <td><?= $counter ?></td>
        <td><a href="/admin/customers/detail/<?php echo $d->customerID ?>" target="_blank"><?= $d->custName ?></a></td>
        <td><a href="/admin/orders/details/<?php echo $d->order_id ?>" target="_blank"><?= $d->order_id ?></a></td>
        <td>
        <?php if (!empty($d->item_id)): ?>
            <a href="/admin/orders/edit_item?orderID=<?php echo $d->order_id ?>&itemID=<?php echo $d->item_id ?>" target="_blank"><?= $d->item_id ?></a>
        <?php else: ?>
            -
        <?php endif; ?>
        </td>
        <td><?= (int)$d->qty ?></td>
        <td><?= $d->productName ?></td>
        <td><?= $d->processName ?></td>
        <td><?= date("m/d/y g:i a", strtotime($d->created)) ?></td>
    </tr>
<? endforeach; ?>
</tbody>
<tfoot>
</tfoot>
</table>
<? endif; ?>
