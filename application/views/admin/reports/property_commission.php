<?php enter_translation_domain("admin/reports/property_commission"); ?>
<h2><?php echo __("Property Commissions", "Property Commissions"); ?></h2>
<br>
<table id='hor-minimalist-a'>
<tr>
    <th><?php echo __("Address", "Address"); ?></th>
    <th><?php echo __("Title", "Title"); ?></th>
    <th><?php echo __("Edit", "Edit"); ?></th>
</tr>
<? foreach($locations as $l): ?>
	<tr>
		<td><a href="/admin/reports/property_commission_print/<?= $l->locationID ?>" target="_blank"><?= $l->address ?></a></td>
        <td><a href="/admin/reports/property_commission_print/<?= $l->locationID ?>" target="_blank"><?= $l->companyName ?></a></td>
		<td><a href="/admin/locations/display_update_form/<?= $l->locationID ?>" target="_blank"><img src="/images/icons/pencil.png"></a></td>
	</tr>
<? endforeach; ?>
</table>
