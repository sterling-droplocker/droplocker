<?php enter_translation_domain("admin/reports/operations_dashboard"); ?>
<script>
$(function() {
<? foreach($lots as $lot): ?>
$( "#dialog<?= $lot->lotID ?>" ).dialog({autoOpen:false});
$('#openModal<?= $lot->lotID ?>').click(function(){
$( "#dialog<?= $lot->lotID ?>" ).dialog( "open" );
});
<? endforeach; ?>
});
</script>

<meta http-equiv="refresh" content="60" > 

<h2><?php echo __("Operations Dashboard", "Operations Dashboard"); ?></h2>
<br>
<table>
<tr>
<td valign="top">
    <table class='box-table-a' style="width: 400px;">
    <tr>
        <th><?php echo __("Driver", "Driver"); ?></th>
        <th><?php echo __("R", "R"); ?></th>
        <th><?php echo __("Last Stop", "Last Stop"); ?></th>
        <th><?php echo __("DC", "DC"); ?></th>
        <th><?php echo __("WF", "WF"); ?></th>
        <th></th>
        <th></th>
    </tr>
    <? foreach($route as $key => $routes): 
        $totDC += $routes['DC'];
        $totWF += $routes['WF'];
    ?>
    <tr>
        <td><a href="/admin/reports/driver_dashboard?driver=<?= $routes['employee_id'] ?>&logDate=<?= $selected_date ?>&route=<?= $key ?>" target="_blank"><?= $routes['driver']?></a></td>
        <td><?= $key?> </td>
        <td><?= $routes['lastStop']?></td>
        <td><?= $route[$key]['DC']?></td>
        <td><?= $route[$key]['WF']?></td>
        <td><?= $route[$key]['finished'] == 1 ? '<a href="/admin/reports/ops_dashboard_finish_route?no='.$key.'"><img src="/images/icons/tick.png" alt="" border="0"></a>' : '<a href="/admin/reports/ops_dashboard_finish_route?yes='.$key.'"><img src="/images/icons/lorry.png" alt="" border="0"></a>' ?></td>
        <td><?= $route[$key]['eor'] == 1 ? '<a href="/admin/reports/ops_dashboard_finish_eor?no='.$key.'"><img src="/images/icons/tick.png" alt="" border="0"></a>' : '<a href="/admin/reports/ops_dashboard_finish_eor?yes='.$key.'"><img src="/images/icons/report_go.png" alt="" border="0"></a>' ?></td>
    </tr>
    <? endforeach; ?>
    <tfoot>
    <tr>
        <th colspan=3><?php echo __("Unprocessed", "Unprocessed"); ?></th>
        <th><?= $totDC ?></th>
        <th><?= $totWF ?></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
    </table>
</td>

<td valign="top" align="left">
    <table class="box-table-a" style="width: 500px;">
    <tr>
        <td style="border-top: 1px solid #CCCCCC;"><?php echo __("Total Pcs", "Total Pcs"); ?></td>
        <td style="border-top: 1px solid #CCCCCC;"><a href="/admin/reports/ops_dashboard_pcs_detail" target="_blank"><?= $totPieces ?></a></td>
    </tr>
    <tr>
        <td><?php echo __("Pcs In This Lot", "Pcs In This Lot"); ?> </td>
        <td><form action="/admin/reports/new_lot" method="post"><?= $currentLot ?>&nbsp;&nbsp;&nbsp; <input type="submit" name="newLot" value="New Lot"></form></td>
    </tr>
    <tr>
        <td><?php echo __("Lot Number", "Lot Number"); ?></td>
        <td><?= $lotNumber ?></td>
    </tr>
    <tr>
        <td valign="top"><?php echo __("Lot Times", "Lot Times"); ?></td>
        <td>
        <? foreach($lots as $lot): 
            $lotTime = number_format_locale((strtotime($lot->startTime) - strtotime($lastLot))/-60,0) ?>
            <?= $lot->lotNumber ?>: <? if($lastLot): ?><?= $lotTime ?> min <? endif; ?>(start <?= convert_from_gmt_aprax($lot->startTime, "g:i a") ?> <?if($lastCount): ?>- <?= $lastCount - $lot->lotCount ?> pcs<?endif;?>) 
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:;" id="openModal<?= $lot->lotID ?>">Routes</a>: <?= $lot->routes ?>
            <br>
        <?  $lastLot = $lot->startTime; 
            $lastCount = $lot->lotCount;
            endforeach; 
            ?>


        </td>
    </tr>
    <tr>
        <td valign="top"><?php echo __("Machines", "Machines"); ?><br><br><a href="/admin/reports/manage_machine"><?php echo __("add machine", "add machine"); ?></a></td>
        <td>
            <? foreach($machines as $machine): ?>
            <? if($timeLeft[$machine->machineID] < 0): ?>
            <font style="color: Red; font-weight: bold;"><? else: ?><font><? endif; ?>
            <form action="/admin/reports/reset_machine" method="post">
            <?= $machine->name ?>: <?= $timeLeft[$machine->machineID] ?>min Remain 
            <input type="submit" name="resetLot" value="reset"><input type="hidden" name="machine" value="<?= $machine->machineID ?>"></form><br>
            </font>
            <? endforeach; ?>
        </td>
    </tr>
    <tr>
        <td valign="top"><?php echo __("PPH", "PPH"); ?></td>
        <td><? foreach ($pph as $emp): if($emp['itemCount']): ?>
            <?= $emp['firstName'] ?>: <?= $emp['itemCount'] ?> pcs / <?= number_format_locale($emp['pph'],0) ?> pph (<?= $emp['userUpcharges'] ?> up$ / <?= $emp['entryErrors'] ?> buzz)<br>
            <? endif; endforeach; ?>
        </td>
    </tr>
    <tr>
        <td valign="top"><a href="/admin/reports/ops_dashboard_message"><?php echo __("Comments", "Comments"); ?></a></td>
        <td><a href="/admin/reports/ops_dashboard_message"><span style="font-weight: bold;"><?= $message ?></span></a></td>
    </tr>
    </table>
   </td>
   <td valign="top">
    <? if($calendarURL != ''): ?>
        <?= $calendarURL ?>    
        <br>
        <a href="/admin/reports/ops_dashboard_addCal"><?php echo __("remove calendar", "remove calendar"); ?></a>
    <? else: ?>
        <form action="/admin/reports/ops_dashboard_addCal" method="post"><?php echo __("Calendar to show", "Calendar to show:"); ?><br><input type="text" name="calendar"><br><input type="submit" name="add" value="<?php echo __("add", "add"); ?>"></form>
    <? endif; ?>
   </td>
</tr>
</table>


<? foreach($lots as $lot): ?>
<div id="dialog<?= $lot->lotID ?>" title="<?php echo __("Select Routes for Lot", "Select Routes for Lot"); ?> <?= $lot->lotNumber ?>" >
<? $lotRoutes = explode(',', $lot->routes) ?>
<form action="ops_dashboard_add_lot_to_route" method="post">
<? foreach($route as $routes => $value): ?>
<?= $routes ?> (<? $driver = explode("<br>", $value['driver']); echo $driver[0]; ?>)<input type="checkbox" name="Route[]" value="<?= $routes ?>"<? if(in_array($routes, $lotRoutes)): ?> checked<? endif; ?>><br>
<? endforeach; ?>
<br>
<input type="hidden" name="lotID" value="<?= $lot->lotID ?>">
<input type="submit" name="Update" value="<?php echo __("Update", "Update"); ?>" class="button blue">
</form>
</div>
<? endforeach; ?>
