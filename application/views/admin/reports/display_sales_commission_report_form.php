<?php enter_translation_domain("admin/reports/display_sales_commission_report_form"); ?>
<style type="text/css">
    input {
        padding: 3px;
    }
</style>
<script type="text/javascript">
$(document).ready(function() {

    
});
</script>
<h2> <?php echo __("Sales Commission Report", "Sales Commission Report"); ?> </h2>


<?= form_open("/admin/reports/display_sales_commission_report") ?>
    <?= form_dropdown("employeeID", $salespersons) ?>
    <?= form_label("Start Date", "start_date") ?><?= form_input(array("name" => "start_date", "class" => "date_field", "id" => "start_date")) ?>
    <?= form_label("End Date", "end_date") ?> <?= form_input(array("name" => "end_date", "class" => "date_field", "id" => "end_date")) ?>
    <?= form_submit(array("class" => "button orange", "value" => __("Display Report", "Display Report"))) ?>
<?= form_close() ?>


    

