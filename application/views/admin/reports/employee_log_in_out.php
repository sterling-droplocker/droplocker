<?php enter_translation_domain("admin/reports/employee_log_in_out"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = '{ "mDataProp":"firstName" },';
$fieldList['lastName'] = '{ "mDataProp":"lastName" },';
            
$nameColSetup = getSortedNameForm($fieldList);

$fieldList = array();
$fieldList['firstName'] = '<th>' . __("First Name","First Name") . '</th>';
$fieldList['lastName'] = '<th>' . __("Last Name","Last Name") . '</th>';
            
$nameHeader = getSortedNameForm($fieldList);

?>

<h2>Employee Log In/Out Report</h2>

<script type="text/javascript">
jQuery(document).ready(function($){
    $("#employee_login_out").dataTable({  
        "iDisplayLength": 20, 
        "bProcessing" : true,
        "bServerSide" : true,
        "bStateSave" : true,
        "sAjaxSource" : "/admin/reports/employee_login_out/",
        "fnPreDrawCallback": function( oSettings ) {
            oSettings._iDisplayLength = 20;
        },
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "start_date", "value": "<?php echo $start_date ?>" } );
          aoData.push( { "name": "end_date", "value": "<?php echo $end_date ?>" } );
          aoData.push( { "name": "firstName", "value": "<?php echo $firstName ?>" } );
          aoData.push( { "name": "username", "value": "<?php echo $username ?>" } );
          aoData.push( { "name": "email", "value": "<?php echo $email ?>" } );
        },
        "bJQueryUI": true,
        "sDom": 'tr<"F"ip>',
        "aoColumns": [
            { "mDataProp":"date" },
            <?= $nameColSetup ?>
            { "mDataProp":"username" },
            { "mDataProp":"email" },
            { "mDataProp":"loginFrom"},
        ]
    });


	$(".datefield").datepicker({
		showOn: "button",
		buttonImage: "/images/icons/calendar.png",
		buttonImageOnly: true,
		dateFormat: "yy-mm-d"
	});
});
</script>
<div style="padding: 8px;">
	<form action="" method="get">
		<table>
			<tr>
				<td>
					<input readonly="true" name="start_date" value="<?= $start_date ?>" class="datefield"> 
					<input readonly="true" name="end_date" value="<?= $end_date ?>" class="datefield">
					<input  name="firstName" value="<?= $firstName ?>" placeholder="<?= __("First Name","First Name")?>">
					<input  name="username" value="<?= $username ?>" placeholder="<?= __("Username","Username")?>">
					<input  name="email" value="<?= $email ?>" placeholder="<?= __("Email","Email")?>">
					<input type="submit" name="Submit" value="<?php echo __("Run", "Run"); ?>" class="button blue">
				</td>
			</tr>
		</table>
	</form>
</div>
<br>

<table id='employee_login_out'>
	<thead>
		<tr>
			<th><?= __("Date","Date")?></th>
                        <?= $nameHeader ?>
			<th><?= __("Username","Username")?></th>
			<th><?= __("Email","Email")?></th>
			<th><?= __("Login From","Login From")?></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
<?php leave_translation_domain(); ?>
