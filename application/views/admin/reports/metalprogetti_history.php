<?php enter_translation_domain("admin/reports/metalprogetti_history"); ?>
<h2><?php echo __("Assembly History for Order", "Assembly History for Order"); ?> <a href="/admin/orders/details/<?= $orderID ?>" target="_blank"><?= $orderID ?></a></h2>

<? if(true): ?>
<br>
<table border="1" cellspacing="2" cellpadding="4" class="normal">
<thead>
<tr>
    <th><?php echo __("ID", "ID"); ?></th>
	<th><?php echo __("Product", "Product"); ?></th>
	<th><?php echo __("Barcode", "Barcode"); ?></th>
	<th><?php echo __("Metalprojetti History", "Metalprojetti History"); ?></th>
</tr>
</thead>
<tbody>
<? 
    $itemList = array();
    foreach($item as $code => $details): ?>
<tr>
    <td <? if(!isset($info[$code])) { echo '  style="background-color: Yellow;"'; } ?>>
            <?php if (!empty($details['item_id'])): ?>
                <?= $details['item_id'] ?>  <? if(in_array($details['item_id'], $itemList)): ?> <br>(duplicate) <? endif; ?>
            <?php endif; ?>
    </td>
	<td <? if(!isset($info[$code])) { echo '  style="background-color: Yellow;"'; } ?>><?= $item[$code]['product'] ?></td>
	<td <? if(!isset($info[$code])) { echo '  style="background-color: Yellow;"'; } ?>>
            <?php if (!empty($details['item_id'])): ?>
                <a href="/admin/items/view?itemID=<?= $details['item_id'] ?>" target="_blank"><?= $code ?></a><? if(in_array($details['item_id'], $itemList)): ?> <br>(duplicate) <? endif; ?></td>
            <?php else: ?>
                <?= $code ?>
            <?php endif; ?>
	<td <? if(!isset($info[$code])) { echo '  style="background-color: Yellow;"'; } ?>>
	       <?php if(!empty($info)): foreach($info as $garCode => $value): ?>
	           <?php if($code == $garCode):?>
                                <?php foreach($value as $date => $detail): ?>
                                    <table border="0" cellspacing="0" cellpadding="2" class=''>
                                        <tr valign="top">
                                            <td width="150"><?= convert_from_gmt_aprax($date, "m/d/y  g:i:s a")  ?></td>
                                            <td width="200"><?= $detail['name'] ?></td>
                                            <td width="50"><?= $detail['slot'] ?></td>
                                            <td width="50"><?= $detail['outPos'] ?></td>
                                            <? if ($detail['outPos'] != '') { echo '<td nowrap><a href="/admin/reports/assembly_log/'.$detail->automatID.'">'.__("Unload Arm Log", "Unload Arm Log").'</a> </td>'; } ?>
                                        </tr>
                                    </table>
                                <?php endforeach; ?>
                    <? endif; ?>
           <?php endforeach; else: ?>
           <?php echo __("Not scanned", "Not scanned"); ?>
           <? endif; ?>
	</td>
</tr>
<? $itemList[] = $details['item_id'] ?>
<? endforeach; ?>
</tbody>
</table>
<? else: ?>
<?php echo __("No data returned for order", "No data returned for order"); ?> <?= $order ?>.
<? endif;?>
