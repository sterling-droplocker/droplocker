<?php enter_translation_domain("admin/reports/pcs_detail"); ?>
<?php

$counter = 0;

$start_date = date("D m/d/y", strtotime($from));
$end_date = date("D m/d/y", strtotime($to));

?>
<h2><?= "PCS Detail for ".  getPersonName($emp)." on {$start_date} - {$end_date}" ?></h2>

<br>
<table id="box-table-a">
<tr>
    <th></th>
    <th><?php echo __("Item (barcode)", "Item (barcode)"); ?></th>
    <th><?php echo __("Time", "Time"); ?></th>
    <th><?php echo __("Unit", "Unit"); ?></th>
    <th><?php echo __("Order", "Order"); ?></th>
</tr>
<? if(!empty($presses)): ?>
<? foreach($presses['items'] as $p):
    $counter++;
    $style = (strtotime($p['dateStamp']) - strtotime($lastTime)) > 300 ? 'background-color: Yellow; background: Yellow;' : ''
?>
<tr>
    <td><?= $counter ?></td>
    <td style="<?= $style ?>"><?= $p['id'] ?></td>
    <td style="<?= $style ?>"><?= convert_from_gmt_aprax($p['dateStamp'], STANDARD_DATE_FORMAT) ?></td>
    <td style="<?= $style ?>"><?= $p['description'] ?></td>
    <td style="<?= $style ?>"><?= $p['order_id'] ?></td>
</tr>
<? $lastTime = $p['dateStamp']?>
<? endforeach; ?>
<? endif; ?>

<? if (!empty($inventories)): ?>
<? foreach($inventories['details'] as $item): $counter++; ?>
<tr>
    <td><?= $counter ?></td>
    <td><?= $item['barcode'] ?></td>
    <td><?= convert_from_gmt_aprax($item['updated'], STANDARD_DATE_FORMAT)?> </td>
    <td><? if($item['upcharge'] != 0) { echo __("upcharge", "upcharge"); } ?></td>
    <td><a href="/admin/orders/details/<?= $item['order_id'] ?>" target="_blank"><?= $item['order_id'] ?></a></td>
</tr>
<? endforeach; ?>
<? endif; ?>
</table>


