<?php enter_translation_domain("admin/reports/firstTimeDetails"); ?>
<h2><?php echo __("First Time Customers for", "First Time Customers for"); ?> <?= $this->input->get_post("month") ?></h2>

<table id="box-table-a">
    <thead>
        <tr>
            <th></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("First Order", "First Order"); ?></th>
            <th><?php echo __("Signup Date", "Signup Date"); ?></th>
            <th><?php echo __("Email", "Email"); ?></th>
            <th><?php echo __("Phone", "Phone"); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($firstCustomers as $f):  $counter++; ?>
    <tr>
        <td><?= $counter ?></td>
        <td><a href="/admin/customers/detail/<?= $f->customerID ?>" target="_blank"><?= getPersonName($f) ?></a></td>
        <td><?= convert_from_gmt_aprax($f->firstOrder, SHORT_DATE_FORMAT) ?></td>
        <td><?= convert_from_gmt_aprax($f->signupDate, SHORT_DATE_FORMAT) ?></td>
        <td><?= $f->email ?></td>
        <td><?= $f->phone;?>
    </tr>
    <?php endforeach ?>
    </tbody>
</table>