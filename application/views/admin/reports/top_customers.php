<?php enter_translation_domain("admin/reports/top_customers"); ?>
<script type="text/javascript">
    $.get("/admin/reports/get_top_customers_report_dataTables_columns", {}, function(columns) {
        aoColumns = columns;
        top_customers_dataTable = $("#top_customers").dataTable( {
            "aoColumns" : aoColumns,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "/admin/reports/get_top_customers_report_dataTables_data",

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
            }
        }).fnSetFilteringDelay(); 
        new FixedHeader(top_customers_dataTable);
    }, "JSON");
    
</script>

<h2><?php echo __("Top Customers", "Top Customers"); ?></h2>

<table id="top_customers">
    <thead>
        <tr></tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" class="dataTables_empty"> <?php echo __("Loading data from server", "Loading data from server"); ?> </td>
        </tr>
    </tbody>
</table>
