<?php enter_translation_domain("admin/reports/assembly_log"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<script type="text/javascript">
    
$(document).ready(function() {

    var supplier_manifest_dataTable = $("table").dataTable({
        "bPaginate" : false,
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>            
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    });
});
    
</script>

<h2><?php echo __("Assembly Log", "Assembly Log"); ?></h2>
<br>
<br>
<form action="" method="post">
<select name="getDate">
<? for($i = 0; $i < 90; $i++): ?>
	<option value="<?= date("Y-m-d", strtotime("-$i days"))?>"
	<? if($_POST['getDate'] == date("Y-m-d", strtotime("-$i days"))) { echo ' SELECTED'; } ?>
	>
    <?= convert_from_gmt_aprax(date("D m/d/y", strtotime("-$i days")), $dateFormat)?>
    </option>
<? endfor; ?>
</select> &nbsp;&nbsp;
<input type="submit" name="View" value="<?php echo __("View Orders", "View Orders"); ?>" class="button orange">
</form>
<br>
<br>
<? if($allRecords): ?>
<table>
<thead>
<tr>
<th><?php echo __("Order", "Order"); ?></th>
<th><?php echo __("Customer", "Customer"); ?></th>
<th><?php echo __("Date", "Date"); ?></th>
<th><?php echo __("ArmRoute", "Arm / Route"); ?></th>
<th><?php echo __("Description", "Description"); ?></th>
<th><?php echo __("Barcode", "Barcode"); ?></th>
<th><?php echo __("OutPosHook", "OutPos / Hook"); ?></th>
<th><?php echo __("SlotSort", "Slot / Sort"); ?></th>
</tr>
</thead>
<tbody>
<? foreach($allRecords as $a): ?>
	<tr valign="top"
	<? if ($automatID == $a->automatID) { echo ' bgcolor="#00FF00"';} ?>
	>
    <td><?= $a->order_id ?></td>
        <td><?= getPersonName($a) ?></td>
	<td><?= local_date($this->business->timezone, $a->fileDate, 'm/d/y h:ia') ?></td>	
	<td><?= $a->arm ?></td>
	<td><?= $a->gardesc ?></td>
	<td><?= $a->garcode ?></td>
	<td><?= $a->outPos ?></td>
	<td><?= $a->slot ?></td>
	</tr>
<?endforeach; ?>
</tbody>
</table>
<?endif; ?>
