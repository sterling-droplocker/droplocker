<?php enter_translation_domain("admin/reports/customer_usage_pattern"); ?>
<h2><?php echo __("Customer Usage Pattern", "Customer Usage Pattern"); ?></h2>
<div style="padding:8px;">

<table id="box-table-a">
<tr>
    <th><?php echo __("Customer", "Customer"); ?></th>
    <th><?php echo __("First Order", "First Order"); ?></th>
    <th><?php echo __("Last Order", "Last Order"); ?></th>
    <th><?php echo __("Time Between Ords", "Time Between Ords"); ?></th>
    <th><?php echo __("Time Since Last Ord", "Time Since Last Ord"); ?></th>
    <th><?php echo __("Gross Spend", "Gross Spend"); ?></th>
    <th><?php echo __("Discounts", "Discounts"); ?></th>
    <th><?php echo __("Visits", "Visits"); ?></th>
</tr>
<? foreach($uses as $usage): ?>
<tr>
    <td><?= $usage[customer] ?></td>
    <td><?= $usage[firstOrder] ?></td>
    <td><?= $usage[lastOrder] ?></td>
    <td><?= $usage[avgOrderLapse] ?> days</td>
    <td><?= $usage[daysAgo] ?> days</td>
    <td>$<?= number_format_locale($usage[totGross],2) ?></td>
    <td>$<?= number_format_locale($usage[totDisc], 2) ?></td>
    <td><?= $usage[visits] ?></td>
</tr>

<? endforeach; ?>
</table>

<?= var_dump($uses) ?>