<?php enter_translation_domain("admin/reports/ticket_report"); ?>
<h2><?php echo __("Monthly Support Tickets", "Monthly Support Tickets"); ?></h2>
<br>
<br>
<table id="box-table-a">
	<thead>
		<tr>
			<th><?php echo __("Month", "Month"); ?></th>
            <? foreach($problems as $problem => $value): ?>
			<th align="right"><?= $problem ?></th>
            <? endforeach; ?>
		</tr>
	</thead>
	<tbody>
        <? foreach($months as $month => $value): ?>
        <tr>
            <td><?= $month ?></td>
            <? foreach($problems as $problem => $problem_id): ?>
			<td align="right">
                <? if (isset($ticket[$month][$problem])): ?>
                    <a href="/admin/tickets?month=<?= $month ?>&problem=<?= $problem_id ?>"><?= $ticket[$month][$problem] ?></a>
                <? endif; ?>
            </td>
            <? endforeach; ?>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
