<?php enter_translation_domain("admin/reports/location_history_form"); ?>
<style type="text/css">
    select {
        margin: 0 5px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $(":submit").loading_indicator();
    })
    
</script>

<h2> <?php echo __("Location History Report", "Location History Report"); ?> </h2>
<br>
<form action="/admin/reports/location_history" method="post">
<?= form_dropdown('locationID', $locations); ?>
<input type="submit" name="getHistoroy" value="<?php echo __("Get History", "Get History"); ?>" class="button blue">
</form>
