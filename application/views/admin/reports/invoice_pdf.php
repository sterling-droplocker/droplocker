<?
header('Content-Type: text/html; charset=utf-8');
error_reporting(0);

// backward compatibility with old view name
enter_translation_domain('admin/reports_invoice_pdf');

$columns = array(
    50,
    10,
    13,
    7,
    20,
);


$show_notes_column = false;
foreach($newInvoice['detail'] as $n => $ordNo) {
    if ($ordNo['ordPayment'] == 3) {
        $show_notes_column = true;
        $columns[0] = 40;
        $columns[1] = 10;
        $columns[2] = 10;
        $columns[3] = 7;
        $columns[4] = 10;
        $columns[5] = 23;
        break;
    }
}

?>
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <br />
        <br />
        <br />
        <table border="1" cellspacing="0" cellpadding="4">
            <thead>
                <tr>
                    <td width="<?= $columns[0] ?>%" align="left"> <strong><?= __('head_item',  'Item') ?></strong> </td>
                    <td width="<?= $columns[1] ?>%" align="right"> <strong><?= __('head_date',  'Date') ?></strong> </td>
                    <td width="<?= $columns[2] ?>%" align="right"> <strong><?= __('head_price', 'Price') ?></strong> </td>
                    <td width="<?= $columns[3] ?>%" align="right"> <strong><?= __('head_qty',   'Qty') ?></strong> </td>
                    <td width="<?= $columns[4] ?>%" align="right"> <strong><?= __('head_total', 'Total') ?></strong> </td>
                    <? if ($show_notes_column): ?>
                    <td width="<?= $columns[5] ?>%" align="right"> <strong><?= __('head_notes', 'Notes') ?></strong> </td>
                    <? endif; ?>
                </tr>
            </thead>

            <tbody>
                <? foreach($newInvoice['detail'] as $n => $ordNo): ?>
                    <tr>
                        <td width="<?= $columns[0] ?>%">
                            <?= __('row_order',  'Order') ?> <?= $n ?>
                            <? if($byBag) echo ' ('.$ordNo['bag'] .' - '. $ordNo['notes'].')';  ?> <br />
                            <? if($items[$n]): ?>
                                <table border="0" cellspacing="0" cellpadding="0" style="font-size: 75%;">
                                    <tr>
                                        <td align="left"> <strong><?= __('row_item',  'Item') ?></strong> </td>
                                        <td align="left"> <strong><?= __('row_barcode',  'Barcode') ?></strong> </td>
                                        <td align="right"> <strong><?= __('row_qty',  'Qty') ?></strong> </td>
                                        <? if($customer[0]->bagDetail == 2): ?>
                                            <td align="right"> <strong><?= __('row_price',  'Price') ?></strong> </td>
                                        <? endif; ?>
                                    </tr>
                                    <? foreach($items[$n] as $i): ?>
                                        <tr>
                                            <td align="left" nowrap>&nbsp;&nbsp;<?= htmlspecialchars($i->name) ?> <?= !empty($i->notes) ? '('.htmlspecialchars(preg_replace('#<br */?>#', "\n ", $i->notes)).')' : ''; ?> </td>
                                            <td align="left"><?=$i->barcode;?></td>
                                            <td align="right"><?= $i->qty ?> </td>
                                            <? if($customer[0]->bagDetail == 2): ?>
                                                 <td align="right"><?= $i->unitPrice ?> </td>
                                            <? endif; ?>
                                        </tr>
                                    <? endforeach; ?>
                                </table>
                            <? endif; ?>


                            <? if($charges[$n]): ?>
                                <table border="0" cellspacing="0" cellpadding="0" style="font-size: 75%;" width="200">
                                    <tr>
                                        <td align="left">
                                            <strong><?= __('row_charge_credits',  'Charges / Credits') ?></strong>
                                        </td>
                                        <td align="right">
                                            <strong><?= __('row_charge_amount',  'Amt') ?></strong>
                                        </td>
                                    </tr>
                                    <? foreach($charges[$n] as $c): ?>
                                        <tr>
                                            <td align="left" nowrap>&nbsp;&nbsp;<?=  strip_tags($c->chargeType, '<br><strong><p>') ?></td>
                                            <td colspan="2"><?= $c->chargeAmount ?></td>
                                        </tr>
                                    <? endforeach; ?>
                                </table>
                            <? endif; ?>
                        </td>
                        <td width="<?= $columns[1] ?>%" align="right"> <?= convert_from_gmt_aprax($ordNo['dateCreated'], $date_format, $this->business_id) ?> </td>
                        <td width="<?= $columns[2] ?>%" align="right"> <?= format_money_symbol($this->business_id, '%.2n',  $ordNo['ordTotal']) ?> </td>
                        <td width="<?= $columns[3] ?>%" align="right"> 1 </td>
                        <td width="<?= $columns[4] ?>%" align="right"> <?= format_money_symbol($this->business_id, '%.2n',  $ordNo['ordTotal']) ?> </td>
                        <? if ($ordNo['ordPayment'] == 3): ?>
                            <td width="<?= $columns[5] ?>%" valign="top" align="right"><?= $ordNo['ordNotes'] ?></td>
                        <? endif; ?>
                    </tr>
                <? endforeach; ?>
            </tbody>
            <tfoot>
                    <tr>
                        <td colspan="2" align="center" valign="middle" width="<?= $columns[0] + $columns[1] ?>%" >
                            <?= __('thank_you', 'Thank you for your business.  Please contact us if you have any questions regarding your bill.'); ?>
                        </td>
                        <td colspan="3" align="right" >
                            <table border="0" cellspacing="4" cellpadding="0" align="right" class="noBorder">
                            <? if ($breakout_vat):  ?>
                                <tr>
                                    <td align="right"><strong><?= __('total',  'Invoice Total:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n',  $newInvoice['total']) ?></td>
                                </tr>
                            <? endif; ?>
                                <tr>
                                    <td align="right"><strong><?= __('subtotal',  'Subtotal:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n',  $newInvoice['subTotal']) ?></td>
                                </tr>
                            <? if (count($newInvoice['taxDetail']) == 100): ?>
                                <tr>
                                    <td align="right"><strong><?= __('tax',  'Tax:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['taxTotal']) ?></td>
                                </tr>
                            <? else: ?>
                                <? foreach ($newInvoice['taxDetail'] as $taxGroup_id => $amount):
                                    if (isset($newInvoice['taxGroups'][$taxGroup_id])) {
                                        $tax_label = $newInvoice['taxGroups'][$taxGroup_id]->name;
                                    } else {
                                        $tax_label = __('tax',  'Tax:');
                                    }
                                ?>
                                <tr>
                                    <td align="right"><strong><?= $tax_label ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $amount) ?></td>
                                </tr>
                                <? endforeach; ?>
                            <? endif; ?>
                            <? if (!$breakout_vat):  ?>
                                <tr>
                                    <td align="right"><strong><?= __('total', 'Invoice Total:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['total']) ?></td>
                                </tr>
                            <? endif; ?>
                            <? if ($newInvoice['amtPaid']): ?>
                                <tr>
                                    <td align="right"><strong><?= __('paid',  'Paid:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right">(<?= format_money_symbol($this->business_id, '%.2n', $newInvoice['amtPaid'])?>)</td>
                                </tr>
                                <tr>
                                    <td align="right"><strong><?= __('total_due',  'Total Due:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['total'] - $newInvoice['amtPaid']) ?></td>
                                </tr>
                            <? endif; ?>
                            </table>
                        </td>
                    </tr>
            </tfoot>
        </table>
        <br />
        <div style="font-weight: bold;"><?= get_business_meta($this->business_id, 'invoiceNotes') ?> </div>
        <? if($byBag):?>
            <br />
            <br />
            <?= __('employee_breakdown_title',  'Breakdown by Employee:') ?>
            <br />
            <table border="1" cellspacing="2" class="normal">
                <tr>
                    <th><?= __('breakdown_employee',  'Employee') ?></th>
                    <th><?= __('breakdown_bag',  'Bag') ?></th>
                    <th align="right"><?= __('breakdown_total_spend',  'Total Spend') ?></th>
                </tr>
                <? foreach($byBag as $bagNum => $bagInfo): ?>
                    <tr>
                         <td> <?= $bagInfo['name']?> </td>
                         <td> <?= $bagNum?> </td>
                         <td align="right"> <?= format_money_symbol($this->business_id, '%.2n', $bagInfo['amt']) ?> </td>
                    </tr>
                <? endforeach;?>
            </table>
        <? endif; ?>
    </body>
</html>
<? leave_translation_domain(); ?>
