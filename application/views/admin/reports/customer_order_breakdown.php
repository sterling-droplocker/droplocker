<?php 
enter_translation_domain("admin/reports/customer_order_breakdown"); 
$dateFormatStd =  get_business_meta($this->business_id, "fullDateDisplayFormat");
?>
<br>
<h3><?=__("order_breakdown_for", "Order Breakdown for") ?> <a href="/admin/customers/detail/<?= $customer[0]->customerID ?>"><?= $customer[0]->fullName ?></a></h3>
<br>
<?= form_open("/admin/reports/customer_order_breakdown/" . $customer[0]->customerID, array('id' => 'report_date_form')) ?>
<?= form_label(__("start_date", "Start Date"), "start_date") ?> <?= form_input(array("name" => "start_date", "value"  => $start_date, "style" => "margin: 0 10px")) ?>
<?= form_label(__("end_date", "End Date"), "end_date") ?> <?= form_input(array("name" => "end_date",  "value"  => $end_date, "style" => "margin: 0 10px")) ?>
<?= form_submit(array("class" => "button blue", "value" => __("generate_report", "Generate Report"))) ?>
<?= form_close() ?>
<br>
<table id="box-table-a">
    <tr>
        <th><?=__("order", "Order") ?></th>
        <th align="right"><?=__("sub_amount", "Sub Amount") ?></th>
        <th align="right"><?=__("net_amount", "Net Amount") ?></th>
        <th><?=__("due_date", "Due Date") ?></th>
        <th><?=__("date_paid", "Date Paid") ?></th>
        <? foreach($products as $p => $value): ?>
            <th><?= $p ?></th>
        <? endforeach; ?>
        <th><?=__("was_inventoried", "Inventoried") ?></th>
    </tr>
    <? foreach($orders as $key => $value): ?>
        <tr>
            <th><a href="/admin/orders/details/<?= $key ?>"><?= $key ?></a></th>
            <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $amount[$key]) ?></td>
            <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $netAmount[$key]) ?></td>
            <td><?= apply_date_format($ordID[$key]->dueDate, $dateFormatStd) ?></td>
            <td>
            <?= $ordID[$key]->orderPaymentStatusOption_id == 3 ?convert_from_gmt_aprax($ordID[$key]->statusDate, $dateFormatStd) : 'Unpaid' ?>
                
            </td>
            <? foreach($products as $p): ?>
                <td><?= $value[$p] ?></td>
            <? endforeach; ?>
            <td>
            <?php 
                if (empty($inventoriedDate[$ordID[$key]->orderID])):
                    echo "-";
                else:
                    echo convert_from_gmt_aprax($inventoriedDate[$ordID[$key]->orderID], $dateFormatStd);
                endif;
            ?>                       
            </td>
        </tr>
    <? endforeach; ?>
    <tr>
        <th><strong><?=__("total", "Total:") ?></strong></th>
        <th align="right">$<?= array_sum($amount); ?></th>
        <th></th>
        <th></th>
        <? foreach ($products as $p): ?>
            <th><?= $total_products[$p]; ?></th>
        <? endforeach; ?>
        <th></th>
    </tr>
</table>
<script>
    $(document).ready(function() {
        $("input[name='start_date']").datepicker();
        $("input[name='end_date']").datepicker();
        $('#report_date_form').validate({
            rules: {
                start_date : {
                    required: false
                },
                end_date : {
                    required: false
                }
            },
            errorClass : "invalid"
        });
    });
</script>