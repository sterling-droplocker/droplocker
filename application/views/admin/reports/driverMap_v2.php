<?php enter_translation_domain("admin/reports/driverMap_v2"); ?>
<?php

if (empty($_POST)) {
    $_POST = array(
        'tranType' => null,
        'noMap' => null,
        'loaded' => null,
    );
}

?>
<html>
<head>
<title><?= $business ?> <?php echo __("Driver Map for", "Driver Map for"); ?> <?= date("D, M d")?></title>
<style type="text/css">
#driver_map{
    width:950px;
    height:1200px;
}
</style>

</head>

<script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
<script type="text/javascript" src='/js/jquery-ui-1.8.18.min.js'></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=<?=get_default_gmaps_api_key();?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".revert").click(function() {
        if (confirm("<?php echo __("Are you sure you want to revert all routes", "Are you sure you want to revert all routes?"); ?>"))
        {
            return true;
        }
        else
        {
            return false;
        }
    });
});

</script>
<h2><?= $business ?> <?php echo __("Driver Map for", "Driver Map for"); ?> <?= date("D, M d")?></h2>

<form action="driver_map_v2" method="post" id="driver_map_form">
<table>
<tr>
    <td>
        <?php echo __("Routes", "Routes:"); ?>
        <? foreach($allRoutes as $route): ?>
        	&nbsp;<?= $route->route_id ?><input type="checkbox" name="route[]" value="<?= $route->route_id ?>"<?  if(in_array($route->route_id, $selectedRoutes) == 1) echo ' checked' ?>>
        <? endforeach; ?>
        <select name="dayOfWeek">
            <? empty($_POST['dayOfWeek']) ? $dayOfWeek = date("N") : $dayOfWeek = $_POST['dayOfWeek']; ?>
            <? for($i = 1; $i <= 7; $i ++): ?>
        	    <option value="<?= $i ?>" <?= $dayOfWeek == $i ? 'SELECTED' : ''; ?>><?= date("D", strtotime("sunday +$i days")) ?></option>
            <? endfor; ?>
        </select>
    </td>
    <td> <select name="tranType">
        	    <option value="Both" <?= $_POST['tranType'] == 'Both' ? 'SELECTED' : ''; ?>><?php echo __("Both", "Both"); ?></option>
        	    <option value="Pickups" <?= $_POST['tranType'] == 'Pickups' ? 'SELECTED' : ''; ?>><?php echo __("Pickups", "Pickups"); ?></option>
        	    <option value="Deliveries" <?= $_POST['tranType'] == 'Deliveries' ? 'SELECTED' : ''; ?>><?php echo __("Deliveries", "Deliveries"); ?></option>
        </select>
    </td>
    <td>
        <label><input type="checkbox" name="noMap" value="1" <? if($_POST['noMap'] == 1) echo ' checked' ?>> <?php echo __("No Map", "No Map"); ?></label><br>
        <label><input type="checkbox" name="loaded" value="1" <? if(!empty($_POST['loaded']) && $_POST['loaded'] == 1) echo ' checked' ?>> <?php echo __("Only show loaded deliveries", "Only show loaded deliveries"); ?></label>
    </td>
    <td><input type="submit" name="Run" value="Run">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td><a href="/admin/reports/assign_drivers"><?php echo __("Assign Drivers", "Assign Drivers"); ?></a><br><a class="revert" href="/admin/reports/routes_revert"><?php echo __("Revert to master routes", "Revert to master routes"); ?></a></td>
</tr>
</table>
</form>
<!-- map -->
<? if($_POST['noMap'] != 1): ?><div id="driver_map" style="clear: both; page-break-after:always; margin-bottom:10px;"></div><? endif; ?>
<div style="float:left">
<table border=1 id="sort_routes" style="max-width:540px;">
<thead>
    <tr>
    <th><?php echo __("ID", "ID"); ?></th>
    <th width="220"><?php echo __("Access", "Access"); ?></th>
    <th width="80"><?php echo __("Sort", "Sort"); ?></th>
    <th width="250"><?php echo __("Address", "Address"); ?></th>
    <?= $_POST['tranType'] != 'Pickups'? '<th>'.__("Orders", "Orders").'</th>' : '' ?>
    <?= $_POST['tranType'] != 'Deliveries'? '<th>'.__("P/U", "P/U").'</th>' : '' ?>
</tr>
</thead>
<? foreach($allData['stops'] as $stop): ?>

    <tr <? if(!empty($hasNote[$stop->locationID]) && $hasNote[$stop->locationID] == 1): ?> bgcolor="#C0C0C0"<? endif; ?> id="location_<?=$stop->locationID;?>">
    
        <td align="center"><?= $stop->locationID ?></td>
        <td>
            <? if(stristr($stop->accessCode, 'key')): ?>
            <strong><?= $stop->accessCode ?></strong>
            <? else: ?>
            <?= str_replace(array("1", "2", "5", "6", "8", "0"), "?", $stop->accessCode) ?>
            <? endif; ?>
        </td>
        <td align="center">
            <span class="sort_order"><?= $stop->sortOrder  ?></span>
            <label><?= form_checkbox("skip[{$stop->locationID}]", 1, $stop->hidden, 'class="skip"'); ?> <?php echo __("Skip", "Skip"); ?></label>
        </td>
        <td><?= $stop->address . ($stop->companyName ? " <br>({$stop->companyName})" : "") ?></td>
        <?if($_POST['tranType'] != 'Pickups'): ?>
        <td>
           <? foreach($orderTypeList as $orderType => $value): if(!empty($allData['deliveries'][$stop->locationID]['orderTypes'][$orderType])): ?>
                <?= $orderType ?>:<?= $allData['deliveries'][$stop->locationID]['orderTypes'][$orderType] ?>
           <? endif; endforeach; ?>
        </td>
        <? endif; ?>
        <?if($_POST['tranType'] != 'Deliveries'): ?>
        <td><?= !empty($allData['claims'][$stop->locationID]) ? $allData['claims'][$stop->locationID]['count'] : "" ?></td>
        <? endif; ?>
    </tr>
<? endforeach; ?>
</table>
<br>
<strong><?php echo __("Driver Notes", "Driver Notes:"); ?></strong><br>
<table border="1" cellspacing="2" cellpadding="2" style="max-width:540px;">
<tr>
    <th><?php echo __("Address", "Address"); ?></th>
    <th><?php echo __("Note", "Note"); ?></th>
</tr>
<?
if (!empty($driverNotes))
foreach ($driverNotes as $driverNote): ?>
<tr>
    <td><?= $driverNote->address ?></td>
	<td><?= $driverNote->note ?></td>
</tr>
<? endforeach; ?>
</table>
</div>
<?if($_POST['tranType'] != 'Pickups'): ?>
<div style="float:left;">
<? foreach($orderTypeList as $orderType => $value): ?>
    <?= $orderType ?>
    <table border="1" cellspacing="0" cellpadding="0" style="font-size: 12px;max-width:410px;" id="<?=$orderType;?>_customers">
        <thead>
            <tr>
                <th><?php echo __("Location", "Location"); ?></th>
                <th><?php echo __("Customer", "Customer"); ?></th>
                <th><?php echo __("Bag", "Bag"); ?></th>
                <th><?php echo __("Qty", "Qty"); ?></th>
                <th><?php echo __("R", "R"); ?></th>
                <th><?php echo __("monetary_symbol", "$"); ?></th>
                <th><?php echo __("L", "L"); ?></th>
                <th><?php echo __("Note", "Note"); ?></th>
                <th><?php echo __("P/U", "P/U"); ?></th>
            </tr>
        </thead>
        <tbody>
	    <? $currentDate = gmdate('Y-m-d', time() );
		 foreach($allData['deliveries'] as $orders):
            foreach($orders['orders'] as $order):
            if($order['orderType'] == $orderType):

            $style = "";
            if ($allData['stops'][$order[0]->locationID]->hidden) {
                $style="display:none;";
            } else if ($order[0]->orderHomeDeliveryID) {
                $style = "background-color:silver";
            } else if ($order[0]->dateCreated >= $currentDate) {
                $style = "background-color:yellow";
            }

        ?>
        
        <tr style="<?= $style; ?>" data-location_id="<?= $order[0]->locationID ?>">
            <td nowrap><?= substr($order[0]->address, 0, 20) ?></td>
            <td><a href="https://droplocker.com/admin/customers/detail/<?= $order[0]->customerID ?>" target="_blank"><?= substr($order[0]->firstName,0, 8) ?> <?= substr($order[0]->lastName,0, 8) ?></a></td>
            <td><a href="/admin/orders/details/<?= $order[0]->orderID ?>" target="_blank"><?= $order[0]->bagNumber ?></a></td>
            <td align="center"><?= $qty[$order[0]->orderID] ?></td>
            <td><?= $order[0]->route_id ?></td>
            <td><?= $order[0]->orderPaymentStatusOption_id == 1 ? '<strong>$</strong>' : ''; ?></td>
            <td><?= $order[0]->loaded == 0 ? '<strong>Not Loaded</strong>' : 'Y'; ?></td>
            <td style="max-width:100px;"><?= $order[0]->llNotes ?></td>
            <td><?= convert_from_gmt_aprax($order[0]->dateCreated, "n/d") ?></td>
        </tr>
        <? endif; endforeach; endforeach; ?>
        </tbody>
    </table>
    <br>
<? endforeach; ?>
</div>

<div style="page-break-before: always; clear: both; float:left;">
<? foreach($allData['stops'] as $stop): if($stop->printManifestNow == 1): ?>
<div style="page-break-before: always; clear: both; float:left;">
<strong><font size="+3"><?php echo __("Delivery Manifest for", "Delivery Manifest for"); ?> <?= $stop->address ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<?= date("m/d/Y") ?>)</font></strong>
</div>
<br><br>
<table border="1">
<tr>
    <td><font size="+2"><u><?php echo __("Customer", "Customer"); ?></u></font></td>
    <td><font size="+2"><u><?php echo __("Address", "Address"); ?></u></font></td>
    <td><font size="+2"><u><?php echo __("Bag Number", "Bag Number"); ?></u></font></td>
    <td><font size="+2"><u><?php echo __("Number of Items", "Number of Items"); ?></u></font></td>
    <td><font size="+2"><u><?php echo __("Date Picked Up", "Date Picked Up"); ?></u></font></td>
</tr>
<? foreach($allData['deliveries'] as $orders):
    foreach($orders['orders'] as $order):
    //only show orders that are loaded on the van on the manifest.
    if($order[0]->locationID == $stop->locationID and $order[0]->loaded == 1):?>
<tr>
    <td><font size="+2"><?= getPersonName($order[0]) ?></font></td>
    <td><font size="+2"><?= $order[0]->address1 ?> <?= $order[0]->address2 ?></font></td>
    <td><font size="+2"><?= $order[0]->bagNumber ?></font></td>
	<td><font size="+2"><?= $qty[$order[0]->orderID] ?></font></td>
	<td><font size="+2"><?= date( "m/d/Y", strtotime($order[0]->dateCreated)) ?></font></td>
</tr>
<? endif; endforeach; endforeach; ?>
<tr>
    <td colspan="4">&nbsp;</td>
</tr>
</table>

<? endif; endforeach; ?>
</div>
<? endif; ?>
<script type="text/javascript">
var customerOrderData = <?= json_encode( $allData['deliveries'] );?>;
var customerOrderQty  = <?= json_encode( $qty );?>;

$(function(){

    $('#sort_routes tbody').sortable({
        stop: driverMapObj.updateRoute
    });

    //add handler here to serialize
});

//the driverMapObj JS, main logic kicks off in init() - for the OOP types, this is basically a static object/class
var driverMapObj = {
    iconUrl: '/images/map/largeTDRedIcons/',
    driverMap: null,
    driverLocationsJSON: <?= json_encode($allData['stops']); ?> ,
    sortedLocations: [],
    driverPoints: {},
    driverMarkers: [],
    routeLines: [],
    infoWindow: null,
    driverMapOptions: { //google maps object init options
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(-34.397, 150.644)
    },

    locationBounds: null,
    driverRoutePath: null,

    //after drag/dropping the sort order this updates the page and fires off the ajax
    updateRoute: function (event, ui) {
        var dMO = driverMapObj;
        dMO.sortedLocations = $(this).sortable("toArray");

        for (var i in dMO.sortedLocations) {
            var sort_order = parseInt(i) + 1;
            $('#' + dMO.sortedLocations[i] + ' .sort_order').html(sort_order);
            dMO.sortedLocations[i] = dMO.sortedLocations[i].replace('location_', '');
        }

        dMO.sortUpdate();
        dMO.drawRoute();
        dMO.customerSort();
    },
    //called to set the sortedLocations object with the location sort data
    initSort: function () {
        var dMO = driverMapObj;
        for (var locationID in dMO.driverLocationsJSON) {
            if(locationID != null ){
                dMO.sortedLocations[ dMO.driverLocationsJSON[ locationID ]['sortOrder'] ] = locationID;
            }
        }
    },
    //updates the location sort order via ajax
    sortUpdate: function () {
        var dMO = driverMapObj;
        var updates = {};
        for (var i in dMO.sortedLocations) {
            var locationID = dMO.sortedLocations[i];
            if (dMO.driverLocationsJSON[locationID].hidden) {
                continue;
            }
            updates[i] = locationID;
        }

        $.post('/admin/reports/update_driver_sort', {
            sort: updates
        }, function (res) {
            var result = JSON.parse(res);
            dMO.clearMarkers();
            dMO.updateLocationJSON(result.locations);
            dMO.drawDriverMarkers();
        });
    },
    //updates the driverLocationsJSON with the sort order (acting as a type of collection)
    updateLocationJSON: function (locationSortArray) {
        var dMO = driverMapObj;
        $.each(locationSortArray, function (sort, locationId) {
            dMO.driverLocationsJSON[locationId]['sortOrder'] = sort;
        });
    },
    //gets the location bounds of all the driver locations - return the center point
    getLocationBoundsCenter: function () {
        var dMO = driverMapObj;
        dMO.locationBounds = new google.maps.LatLngBounds();

        for (var b in dMO.driverLocationsJSON) {
            dMO.locationBounds.extend(dMO.getLocationLatLon(dMO.driverLocationsJSON[b]['location_id']));
        }

        return dMO.locationBounds.getCenter();
    },
    //returns a googleAPI lat/lon object
    getLocationLatLon: function (locationId) {
        var dMO = driverMapObj;
        var locLat = dMO.driverLocationsJSON[locationId]['lat'] || '';
        var locLon = dMO.driverLocationsJSON[locationId]['lon'] || '';

        if (locLat != '' && locLon != '') {
            return new google.maps.LatLng(locLat, locLon);
        } else {
            return false;
        }
    },
    //returns bounds of all points, returns object with ne, and sw
    getDriverPointsBounds: function () {
        var dMO = driverMapObj;
        return {
            ne: dMO.locationBounds.getNorthEast(),
            sw: dMO.locationBounds.getSouthWest()
        };
    },
    //sets up the map
    initializeMap: function () {
        var dMO = driverMapObj;
        dMO.driverMapOptions.center = dMO.getLocationBoundsCenter();

        dMO.driverMap = new google.maps.Map(document.getElementById("driver_map"), dMO.driverMapOptions);
        google.maps.event.addListenerOnce(dMO.driverMap, 'idle', function () {
            dMO.driverMap.fitBounds(dMO.locationBounds);

        });

        dMO.infoWindow = new google.maps.InfoWindow();
    },
    //array/collection of points for each location
    setDriverPoints: function () {
        var dMO = driverMapObj;
        dMO.driverPoints = {};
        for (var p in dMO.driverLocationsJSON) {
            if (dMO.driverLocationsJSON[p].hidden) {
                continue;
            }

            var driverPoint = new google.maps.Point(dMO.driverLocationsJSON[p]['lat'], dMO.driverLocationsJSON[p]['lon']);
            dMO.driverPoints[p] = driverPoint;
        }
        dMO.drawDriverMarkers(true);
    },
    //renders the markers from all of the driverPoints collection
    drawDriverMarkers: function (initExtra) {
        initExtra = initExtra || false;
        var dMO = driverMapObj;
        $.each(dMO.driverPoints, function (ind, driver_point) {
            var sortOrder = dMO.driverLocationsJSON[ind]['sortOrder'];
            if(!initExtra){
                sortOrder++;
            }
            var iconSortImage = dMO.iconUrl + 'marker' + sortOrder + '.png';
            var image = new google.maps.MarkerImage(iconSortImage, new google.maps.Size(20, 34), new google.maps.Point(0, 0), new google.maps.Point(10, 34));
            var locationMarkerOptions = {
                position: new google.maps.LatLng(driver_point.x, driver_point.y),
                map: dMO.driverMap,
                title: driver_point.location_id,
                icon: image
            };
            var marker = new google.maps.Marker(locationMarkerOptions);

            google.maps.event.addListener( marker, 'click', function(){
                dMO.infoWindow.setContent( dMO.initInfoWindowContent( ind ) );
                dMO.infoWindow.open(dMO.driverMap,marker);
                $('#info_window_update').unbind('click');
                $('#info_window_update').bind('click', dMO.updateLocation );
            });

            dMO.driverMarkers.push(marker);
        });
    },
    //gets the HTML for the infowindow for the specific address
    initInfoWindowContent: function( locationId ){
        var dMO = driverMapObj;
        var locationInfo = dMO.driverLocationsJSON[ locationId ];
        var locSortOrder = dMO.sortedLocations.indexOf( locationId ) + 1;

        var contentString = $('<div style="padding:10px;" />');
        $( '<div style="padding:10px;"><b><?php echo __("Address", "Address:"); ?></b><span id="info_window_address" style="margin-right:10px;">' + locationInfo['address'] + '</span>(' + locationInfo['accessCode'] + ')</div>' ).appendTo(contentString);

        var formString = $('<div style="padding:10px;" />');
        $( '<span style="margin-right:10px;"><?php echo __("Route", "Route:"); ?> <input type="text" id="info_window_route" style="width:40px;" value="' + locationInfo['route_id'] + '" /></span>' ).appendTo(formString);
        $( '<span style="margin-right:10px;"><?php echo __("Sort", "Sort:"); ?> <input type="text" id="info_window_sort" style="width:40px;" value="' + locSortOrder + '" /></span>' ).appendTo(formString);
        $( '<button id="info_window_update"><?php echo __("Update", "Update"); ?></button>' ).appendTo(formString);

        $(formString).appendTo(contentString);

        $( '<div style="padding:10px;"><span id="info_window_driver"><?php echo __("Driver", "Driver:"); ?> </span> | <a href="/admin/locations/display_update_form/' + locationId +'" target="_blank">edit</a><input type="hidden" id="location_id" value="' + locationId + '" /></div>' ).appendTo(contentString);

        return contentString[0];
    },
    updateLocation: function(event){
        var dMO = driverMapObj;

        var locationUpdateData = {
            location_id: $('input#location_id').val(),
            sort: $('input#info_window_sort').val(),
            route: $('input#info_window_route').val()
        };

        $.post('/admin/reports/update_location_route_sort',locationUpdateData,function(res){
            var updateData = JSON.parse(res);
            $('#driver_map_form').submit();
        });
    },
    //renders the polyline
    drawRoute: function () {
        var dMO = driverMapObj;
        var routePath = [];
        if (dMO.driverRoutePath != null) {
            dMO.driverRoutePath.setMap(null);
        }
        for (var r in dMO.sortedLocations) {
            var routePoint = dMO.driverPoints[dMO.sortedLocations[r]];
            if (typeof routePoint == 'undefined') {
                continue;
            }
            routePath.push(new google.maps.LatLng(routePoint.x, routePoint.y));
        }

        dMO.driverRoutePath = new google.maps.Polyline({
            path: routePath,
            strokeColor: '#8800dd',
            strokeOpacity: .6,
            strokeWeight: 4
        });
        dMO.driverRoutePath.setMap(dMO.driverMap);
    },
    //wipes out the markers
    clearMarkers: function () {
        var dMO = driverMapObj;
        for (var m in dMO.driverMarkers) {
            dMO.driverMarkers[m].setMap(null);
        }
        dMO.driverMarkers = [];
    },
    infoWindowUpdate: function(){

    },
    customerSort: function(){
        //just wash and fold sort for now
        $('[id$=_customers] tbody').html('');

        for( var newSort in this.sortedLocations ){ //this is the new order
            var customerSortLocationId = this.sortedLocations[ newSort ];
            var locationOrders = customerOrderData[ customerSortLocationId ] || '';

            if( locationOrders != '' ){
                for( var orderID in locationOrders['orders'] ){
                    var orderData = locationOrders['orders'][ orderID ];
                    var orderTypePrefix = orderData.orderType;
                    this.orderTableRowHTML( orderData[0] ).appendTo('#' + orderTypePrefix + '_customers' );
                }
            }
        }
    },
    orderTableRowHTML: function( orderData ){
        var $tableRow = $('<tr></tr>');

        var preDateValues = orderData.dateCreated.split(' ');
        var puDateValues = preDateValues[0].split('-');

        var loadedStr = 'Y';
        if( orderData.loaded == 0 ){
            loadedStr = '<strong><?php echo __("Not Loaded", "Not Loaded"); ?></strong>';
        }

        var paymentStr = ''
        if( orderData.orderPaymentStatusOption_id == 1 ){
            paymentStr = '<strong><?php echo __("monetary_symbol", "$"); ?></strong>';
        }

        $('<td>' + orderData.address.substr(0,20) + '</td>').appendTo( $tableRow );
        $('<td><a href="https://droplocker.com/admin/customers/detail/' + orderData.orderID + '" target="_blank">' + orderData.firstName.substr(0,8) + ' ' + orderData.lastName.substr(0,8) + '</a></td>').appendTo( $tableRow );
        $('<td><a href="/admin/orders/details/' + orderData.orderID + '" target="_blank">' + orderData.bagNumber + '</a></td>').appendTo( $tableRow );

        $('<td>' + customerOrderQty[ orderData.orderID ] + '</td>').appendTo( $tableRow );
        $('<td>' + orderData.route_id + '</td>').appendTo( $tableRow );

        $('<td>' + orderData.orderPaymentStatusOption_id + '</td>').appendTo( $tableRow );
        $('<td>' + loadedStr + '</td>').appendTo( $tableRow );

        $('<td style="max-width:100px;">' + orderData.llNotes + '</td>').appendTo( $tableRow );
       // $('<td>' + ( puDate.getMonth() + 1 ) + '/' + puDate.getDate() + '</td>').appendTo( $tableRow );  //month day
        $('<td>' + puDateValues[1].replace(/^0+/,"") + '/' + puDateValues[2] + '</td>').appendTo( $tableRow);

        return $tableRow;

    },
    initSkip: function() {
        $('.skip').click(function(evt) {
            var checked = $(this).attr('checked') ? true : false;
            var msg = checked ? "<?php echo __("Are you sure you want to skip this stop", "Are you sure you want to skip this stop?"); ?>" : "<?php echo __("Are you sure you want to enable this stop", "Are you sure you want to enable this stop?"); ?>";

            if (!confirm(msg)) {
                evt.preventDefault();
                return;
            }

            var tr = $(this).closest('tr');
            var location_id = $('td:eq(0)', tr).text();
            var sort_order = $('.sort_order', tr).text();
            var route = driverMapObj.driverLocationsJSON[location_id]['route_id'];

            driverMapObj.driverLocationsJSON[location_id].hidden = checked;
            if (checked) {
                sort_order = 99;
            }

            var locationUpdateData = {
                location_id: location_id,
                sort: sort_order,
                route: route
            };

            $.post('/admin/reports/update_location_route_sort',locationUpdateData,function(res){
                var updateData = JSON.parse(res);
                $('#driver_map_form').submit();
            });

        });
    },
    //pseudo constructor
    init: function () {
        var dMO = driverMapObj;
        dMO.initSort();
        dMO.initializeMap();
        dMO.setDriverPoints();
        dMO.drawRoute();
        dMO.initSkip();
    }
};

driverMapObj.init();
</script>
</html>
