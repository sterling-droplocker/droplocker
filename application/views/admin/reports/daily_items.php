<?php enter_translation_domain("admin/reports/daily_items"); ?>
<h2><?php echo __("Daily Items", "Daily Items"); ?></h2>

<div style="padding: 8px;">
<form action="""" method="post">
<select name="logDate">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if($_POST['logDate'] == $currentDay) { echo ' SELECTED'; }
		echo '>'.date("D m/d", strtotime($currentDay)).'</option>';
	}
	?>
</select>
<select name="product">
	<?
	foreach ($products as $p)
	{
		echo '<option value="'.$p->productID.'"';
		if($_POST['product'] == $p->productID) { echo ' SELECTED'; }
		echo '>'.$p->name.'</option>';
	}
	?>
</select>
<input type="submit" name="Submit" value="<?php echo __("Run", "Run"); ?>" class="button blue">
</form>
</div>
<br>

<table id='box-table-a'>
	<thead>
		<tr>
			<th><?php echo __("Added", "Added"); ?></th>
			<th><?php echo __("Customer", "Customer"); ?></th>
			<th><?php echo __("Order", "Order"); ?></th>
			<th><?php echo __("Barcode", "Barcode"); ?></th>
			<th><?php echo __("Picture", "Picture"); ?></th>
		</tr>
	</thead>
	<tbody>
		<? if($items): ?>
			<? foreach($items as $i):
                $bigger = $i->item_id > $newItem;
			?>
			<tr>
				<td><?= local_date($this->business->timezone, $i->updated, 'n/d/y g:i:s a') ?></td>
                                <td><?= getPersonName($i) ?></td>
				<td><a href="/admin/orders/details/<?= $i->orderID ?>" target="_blank"><?= $i->orderID ?></a></td>
				<td <?= $bigger ? 'bgcolor="#FFFF00" bordercolor="#FFFF00" style="background: Yellow;"' : '' ?>><?= $bigger ? 'NEW - ' : '' ?><?= $i->barcode ?></td>
				<td><a href="https://s3.amazonaws.com/LaundryLocker/<?= $i->picture ?>"><img src="https://s3.amazonaws.com/LaundryLocker/<?= $i->picture ?>" alt="" width="100" height="100" border="0"></a></td>
			</tr>
			<? endforeach; ?>
		<? else: ?>
			<tr>
				<td colspan=8><?php echo __("No transactions found", "No transactions found"); ?></td>
			</tr>
		<? endif; ?>

	</tbody>
</table>
