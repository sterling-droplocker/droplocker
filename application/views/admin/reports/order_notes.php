<?php enter_translation_domain("admin/reports/order_notes"); ?>
<?php 

?>
<?php
$statuses = array(
    1 => __("Picked Up", "Picked Up"),
    3 => __("Inventoried", "Inventoried"),
);

?>
<h2><?= $statuses[$status] ?> <?php echo __("Order Notes at", "Order Notes at"); ?> <?= date("m/d/y G:ia") ?></h2>

<p>
    <a href="/admin/reports/order_notes?status=3"><?php echo __("Inventoried Orders", "Inventoried Orders"); ?></a> |
    <a href="/admin/reports/order_notes?status=1"><?php echo __("Picked Up Orders", "Picked Up Orders"); ?></a>
</p>

<table class="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?> </th>
            <th><?php echo __("Order ID", "Order ID"); ?></th>
            <th><?php echo __("Route", "Route"); ?> </th>
            <th><?php echo __("Type", "Type"); ?></th>
            <th><?php echo __("Stop", "Stop"); ?></th>
            <th><?php echo __("Order Note", "Order Note"); ?></th>
            <th><?php echo __("Assembly Note", "Assembly Note"); ?></th>
            <th><?php echo __("Status Date", "Status Date"); ?></th>
            <th><?php echo __("Due Date", "Due Date"); ?></th>
            <th><?php echo __("Location", "Location"); ?></th>
            <th></th>
    </thead>
    <tbody>
        <?php 
            foreach ($notes as $n): 
                if (
                    (trim($n->notes) == "ORDER TYPE: \nTHIS ORDER ONLY:" || trim($n->notes) == '') &&
                    trim($n->postAssembly == '')
                ) {
                    continue;
                }
                $n->notes = str_ireplace("ORDER TYPE: \nTHIS ORDER ONLY:", "THIS ORDER ONLY:", $n->notes);
        ?>
            <tr>
                <td> <a href="/admin/customers/detail/<?= $n->customerID ?>" target="_blank"><?= getPersonName($n) ?></a> </td>
                <td> <a href="/admin/orders/details/<?= $n->orderID ?>"><?= $n->orderID ?></a> </td>
                <td> <?= $n->route_id ?> </td>
                <td><?= $type[$n->orderID] ?></td>
                <td> <?= $n->position ?> </td>
                <td> <?= $n->notes ?> </td>
                <td> <?= $n->postAssembly ?> </td>
                <td> <?= convert_from_gmt_aprax($n->date, get_business_meta($this->business_id, "standardDateDisplayFormat")) ?> </td>
                <td> <?= apply_date_format($n->dueDate, get_business_meta($this->business_id, "shortDateDisplayFormat")) ?> </td>
                <td><a href="/admin/admin/location_details/<?= $n->locationID ?>" target="_blank"><?= ucfirst($n->address) . " " . ucfirst($n->address2) ?></a></td>
                <td style="vertical-align: bottom;">______________________</td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
