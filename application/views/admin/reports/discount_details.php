<?php enter_translation_domain("admin/reports/discount_details"); ?>
<h2><?php echo __("Discount Details", "Discount Details"); ?></h2>
<br>

<table border="1" cellspacing="2" cellpadding="2" class="normal">
    <tr>
        <td><?php echo __("Order", "Order"); ?></td>
        <td><?php echo __("Date", "Date"); ?></td>
        <td><?php echo __("Description", "Description"); ?></td>
        <td><?php echo __("Credit", "Credit"); ?></td>
        <td><?php echo __("Charge", "Charge"); ?></td>
        <td><?php echo __("Customer", "Customer"); ?></td>
    </tr>
 <? foreach($discounts as $d): ?>
    <tr>
        <td><a href="/admin/orders/details/<?= $d->order_id ?>"><?= $d->order_id ?></a></td>
        <td><?= convert_from_gmt_aprax($d->updated, SHORT_DATE_FORMAT) ?></td>
        <td><?= $d->chargeType ?></td>
        <? if($d->chargeAmount < 0): ?>
            <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $d->chargeAmount) ?></td><td></td>
                <? $creditTotal += $d->chargeAmount ?>
        <? else: ?>
            <td></td><td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $d->chargeAmount) ?></td>
                <? $debitTotal += $d->chargeAmount ?>
        <? endif; ?>
        <td><?= getPersonName($d) ?></td>
   </tr>

 <? endforeach; ?>
 <tr>
     <td><?php echo __("Total", "Total"); ?></td>
     <td></td>
     <td></td>
     <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $creditTotal) ?></td>
     <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $debitTotal) ?></td></tr>
 </table>