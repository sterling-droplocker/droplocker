<?php enter_translation_domain("admin/reports/production_report"); ?>
<h2><?php echo __("Production Report for", "Production Report for"); ?> <?= date("M d,Y", strtotime($selectedDate)) ?></h2>
<br>
<div style="padding:10px;">
<form action="" method="post">
    <select name="selectedDate">
    <? for($i = 0; $i <= 900; $i++):?>
        <option value="<?= date("Y-m-d", strtotime("-$i days")) ?>" <?if($_POST['selectedDate'] == date("Y-m-d", strtotime("-$i days"))) echo ' SELECTED'; ?>><?= date("Y-m-d", strtotime("-$i days")) ?></option>    
    <? endfor; ?>
    </select>
    <select name="interval">
    <? for($i = 1; $i <= 50; $i++):?>
        <option value="<?= $i ?>" <?if($i == $interval) echo ' SELECTED'; ?>><?= $i ?> Days</option>    
    <? endfor; ?>
    </select>
    <select name="supplier">
        <option value="%"><?php echo __("All Suppliers", "All Suppliers"); ?></option>
        <? foreach($suppliers as $s):?>
        <option value="<?= $s->supplierID ?>" <?if($s->supplierID == $_POST['supplier']) echo ' SELECTED'; ?>><?= $s->companyName ?></option>
        <? endforeach; ?>
    </select>&nbsp;&nbsp;
    <? foreach($modules as $p):?>
        <input type="checkbox" name="modules[]" value="<?=$p->moduleID?>" <? if(in_array($p->moduleID, $selectedServices)) { echo 'checked'; } ?> ><?= $p->name?>
    <? endforeach; ?>&nbsp;&nbsp;
    <select name="byRoute">
        <option value="0" <? if($_POST['byRoute'] == 0) echo ' SELECTED' ?>>No Routes</option>
        <option value="1" <? if($_POST['byRoute'] == 1) echo ' SELECTED' ?>>By Route</option>
    </select>
    <select name="status">
        <option value="inv" <? if($_POST['status'] == 'inv') echo ' SELECTED' ?>>Inventoried</option>
        <option value="picked" <? if($_POST['status'] == 'picked') echo ' SELECTED' ?>>Picked Up</option>
    </select>

    <input type="submit" name="run" value="<?php echo __("Run", "Run"); ?>" class="button orange">
</form>
</div>

<form action="" method="post">
    <div style="display: none;">
        <select name="supplier">
            <option value="%"><?php echo __("All Suppliers", "All Suppliers"); ?></option>
            <? foreach($suppliers as $s):?>
            <option value="<?= $s->supplierBusiness_id ?>" <?if($s->supplierBusiness_id == $_POST['supplier']) echo ' SELECTED'; ?>><?= $s->companyName ?></option>
            <? endforeach; ?>
        </select>&nbsp;&nbsp;
        <? foreach($modules as $p):?>
            <input type="checkbox" name="modules[]" value="<?=$p->moduleID?>" <? if(in_array($p->moduleID, $selectedServices)) { echo 'checked'; } ?> ><?= $p->name?>
        <? endforeach; ?>&nbsp;&nbsp;
    </div>
    <?if(isset($allItems)): ?>
    <table id="box-table-a">
            <thead>
                    <tr>
                            <th rowspan="2"><?php echo __("Date", "Date"); ?></th>
                            <? if($_POST['byRoute'] == 1): foreach($routeList as $r => $value): ?>
                            <th rowspan="2"><?= $r ?></th>
                            <? endforeach; endif; ?>
                            <? foreach($services as $key => $value): ?>
                            <th colspan="<?= count($value) ?>">
                                <div style="text-align:center;"><?= $key ?></div>
                            </th>
                            <? endforeach; ?>
                            <th rowspan="2"><?php echo __("Tot Pcs", "Tot Pcs"); ?></th>
                            <th rowspan="2"><?php echo __("Notes", "Notes"); ?></th>
                            <? if($roles): foreach($roles as $r): ?>
                            <th rowspan="2"><?= $r ?></th>
                            <? endforeach; endif; ?>
                    </tr>
                    <tr>

                            <? foreach($services as $key => $value): ?>
                                <? foreach ($value as $process => $value): ?>
                                    <th><?= $process ?></th>
                                <? endforeach; ?>
                            <? endforeach; ?>
                    </tr>
            </thead>
            <tbody>
                    <? foreach($allItems as $a => $value):?>
                    <tr>
                            <td><?= date("D m/d/y", strtotime($a)) ?></td>
                            <? if($_POST['byRoute'] == 1): foreach($routeList as $rr => $valx): ?>
                            <td><?= $routes[$a][$rr] ?></td>
                            <? endforeach; endif; ?>
                            <? foreach($services as $s => $processes): ?>
                            <? foreach ($processes as $process => $nada): ?>
                                <td>
                                    <a href="/admin/reports/production_details?day=<?= $a ?>&service=<?= $s ?>&status=<?= $_POST['status'] ?>" target="_blank"><?= $value[$s][$process]['qty'] ?></a>
                                </td>
                            <? endforeach; ?>
                            <? endforeach; ?>
                            <td><?= $value['total'] ?></td>
                            <td><input type="text" name="note[<?=$a?>]" value="<?= $notes[$a]?>"></td>
                            <? if($roles):  foreach($roles as $r): ?>
                            <td>
                                <a href="/admin/reports/pcs_per_hour?day=<?= $a ?>&role=<?= $r ?>" target="_blank"><?= $roleHours[$a][$r]['dayReg'] ?>/<?= $roleHours[$a][$r]['dayOt'] ?></a>
                                <? if(($r == 'pressers' or $r == 'operations support') and $roleHours[$a][$r]['dayReg']) { echo ' | '.number_format_locale($value['total']  / ($roleHours[$a][$r]['dayReg'] + $roleHours[$a][$r]['dayOt']),1).'pph';} ?>
                            </td>
                            <? endforeach; endif; ?>
                    </tr>
                    <? endforeach; ?>
            </tbody>
    </table>
    <br>
    <div align="right"><input type="submit" name="update" value="<?php echo __("Update Notes", "Update Notes"); ?>" class="button blue"></form></div>
</form>
<? else: ?>
<br><?php echo __("No Data Returned", "No Data Returned"); ?>
<? endif; ?>


