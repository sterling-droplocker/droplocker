<?php enter_translation_domain("admin/reports/driver_notes_detail"); ?>
<h2><?php echo __("Driver Note Detail", "Driver Note Detail"); ?></h2>

<table class='detail_table'>
    <tr>
        <th><?php echo __("Status", "Status"); ?></th>
        <td><?= $status?></td>
    </tr>
     <tr>
        <th><?php echo __("Created", "Created"); ?></th>
        <td><?= $dateCreated?></td>
    </tr>
    <tr>
        <th><?php echo __("Posted By", "Posted By"); ?></th>
        <td><?= $employee?></td>
    </tr>
    <tr>
        <th><?php echo __("Note", "Note"); ?></th>
        <td><?= $note?></td>
    </tr>
    <?php if($closedBy):?>
    <tr>
        <th><?php echo __("Completed By", "Completed By"); ?></th>
        <td><?= $closedBy?></td>
    </tr>
    <tr>
        <th><?php echo __("Completed Date", "Completed Date"); ?></th>
        <td><?= $closedDate?></td>
    </tr>
    <?php endif;?>
    
</table>
<br>
<?php if($responses):?>
<h2><?php echo __("Responses", "Responses"); ?></h2>
<?php foreach($responses as $response):?>
<table class='detail_table'>
    <tr>
        <th><?= $response->name?><br>
        <?= $response->date ?></th>
        <td><?= $response->notes?></td>
    </tr>
</table>
<?php endforeach;?>
<?php endif;?>
<p>
<form action='/admin/reports/driver_notes_complete' method='post'>
<h3><?php echo __("Completed Message", "Completed Message"); ?></h3>
<textarea style='width:500px;height:100px' name='closedNote'><?= $note->closedNotes?></textarea>
<br><input type='submit' class='button orange' value='<?php echo __("Close Note", "Close Note"); ?>' name='submit' />
<input type='hidden' value='<?= $noteID?>' name='note_id' />
</form>
</p>