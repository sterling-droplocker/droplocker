<?php enter_translation_domain("admin/reports/driver_revenue"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat", '%a %m/%d'); ?>
<h2><?php echo __("Driver Revenue", "Driver Revenue"); ?></h2>
<br>
<form action="/admin/reports/driver_revenue" method="post">
<select name="driver">
    <? foreach ($drivers as $driver): ?>
    <option value="<?= $driver->employee_id ?>"<? if($_POST['driver'] == $driver->employee_id) { echo ' SELECTED'; }?>><?= getPersonName($driver) ?></option>
    <? endforeach; ?>
</select>
<select name="startDate">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if($_POST['startDate'] == $currentDay) { echo ' SELECTED'; }
		echo '>'.strftime($dateFormat, strtotime($currentDay)).'</option>';
	}
	?>
</select>
<select name="endDate">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if($_POST['endDate'] == $currentDay) { echo ' SELECTED'; }
		echo '>'.strftime($dateFormat, strtotime($currentDay)).'</option>';
	}
	?>
</select>
<input type="submit" name="run" value="<?php echo __("Run Report", "Run Report"); ?>">
</form>
<br>
<br>

<? if($orders):?>
<table id="box-table-a">
    <tr>
        <th><?php echo __("Order", "Order"); ?></th>
        <th><?php echo __("Delivered", "Delivered"); ?></th>
        <th><?php echo __("Customer", "Customer"); ?></th>
        <th><?php echo __("Address", "Address"); ?></th>
        <th><?php echo __("Gross", "Gross"); ?></th>
        <th><?php echo __("Discounts", "Discounts"); ?></th>
        <th><?php echo __("Total", "Total"); ?></th>
    </tr>
<? foreach($orders as $order): $orderTotal = $order->closedGross + $order->closedDisc?>
    <tr>
        <td><a href="/admin/orders/details/<?= $order->order_id ?>" target="_blank"><?= $order->order_id ?></a></td>
        <td><?= strftime($dateFormat, strtotime($order->delTime)) ?></td>
        <td><?= getPersonName($order) ?></td>
        <td><?= $order->locAddress ?></td>
        <td><?= format_money_symbol($order->business_id, '%.2n', $order->closedGross) ?></td>
        <td><?= format_money_symbol($order->business_id, '%.2n', $order->closedDisc) ?></td>
        <td><?= format_money_symbol($order->business_id, '%.2n', $orderTotal) ?></td>
    </tr>
<?
    $total += $orderTotal;
    $totalGross += $order->closedGross;
    $totalDisc += $order->closedDisc;
    endforeach; ?>
    <tfoot>
        <th colspan=4>Total</th>
        <th><?= format_money_symbol($this->business->businessID, '%.2n', $totalGross) ?></th>
        <th><?= format_money_symbol($this->business->businessID, '%.2n', $totalDisc) ?></th>
        <th><?= format_money_symbol($this->business->businessID, '%.2n', $total) ?></th>
    </tfoot>
</table>
<? endif; ?>
