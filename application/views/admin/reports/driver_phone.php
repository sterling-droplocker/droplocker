<?php enter_translation_domain("admin/reports/driver_phone"); ?>
<h2><?php echo __("Driver Phones", "Driver Phones"); ?></h2>
<br>
<? if($phones):?>
<table id="box-table-a">
    <tr>
        <th><?php echo __("Driver", "Driver"); ?></th>
        <th><?php echo __("Phone Number", "Phone Number"); ?></th>
        <th><?php echo __("Last Updated", "Last Updated"); ?></th>
    </tr>
<? foreach($phones as $phone): ?>
    <tr>
        <td><?= getPersonName($phone) ?></td>
        <td><?= $phone->phoneNumber ?></td>
        <td><?= convert_from_gmt_aprax($phone->dateCreated, STANDARD_DATE_FORMAT) ?></td>
    </tr>
<? endforeach; ?>
</table>
<? endif; ?>