<?php enter_translation_domain("admin/reports/discounts_given_over_time_period"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $("input[name='start_date']").datepicker();
    $("input[name='end_date']").datepicker();

    $('#report_date_form').validate({
        rules: {
            start_date : {
                required: true,
            },
            end_date : {
                required: true,
            }
        },
        errorClass : "invalid"
    });

    $("table").dataTable({
        "sScrollX": "100%",
        "bScrollCollapse": true,
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>            
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
        }        
    })
});
</script>

<h2> <?php echo __("Coupons Redeemed", "Coupons Redeemed"); ?> </h2>


<?= form_open("/admin/reports/get_discounts_given_over_time_period", array('id' => 'report_date_form')) ?>

    <?= form_label(__("Start Date", "Start Date"), "start_date") ?> <?= form_input("start_date") ?>

    <?= form_label(__("End Date", "End Date"), "end_date") ?> <?= form_input("end_date") ?>
    <?= form_submit(array("class" => "button blue", "value" => __("Generate Report", "Generate Report"))) ?>
<?= form_close() ?>

<hr />

<table style="width: 100%;">
    <thead>
        <tr>
            <th> <?php echo __("Date Created", "Date Created"); ?> </th>
            <th> <?php echo __("Coupon Code", "Coupon Code"); ?> </th>
           <?php foreach ($date_range as $date): ?>
            <th> <?= $date->format("m/d/y") ?> </th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($coupons as $index => $coupon): ?>
            <tr>
                <td> 
                    <?php if ($coupon->startDate instanceof \DateTime ): ?>
                          <?= $coupon->startDate->format("m/d/y") ?>
                    <?php endif; ?>
                </td>
                <td> <?= sprintf("%s %s", $coupon->prefix, $coupon->code) ?> </td>
                <?php foreach ($dates as $date => $coupons): ?>
                    <td> 
                        <?php $found = false; ?>
                        <?php foreach ($coupons as $couponCode => $total): ?>
                            <?php if (strcasecmp($couponCode,sprintf("%s%s", $coupon->prefix, $coupon->code)) == 0): ?>
                                <?php
                                    $found = true;
                                    echo $total;
                                ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        
                        <?php if (!$found): ?>
                            0
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
            </tr>   
        <?php endforeach; ?>
    </tbody>
</table>
