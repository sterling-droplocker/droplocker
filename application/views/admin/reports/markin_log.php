<?php enter_translation_domain("admin/reports/markin_log"); ?>
<script type="text/javascript">

$(document).ready(function() {

    var supplier_manifest_dataTable = $("table").dataTable({
        "bPaginate" : false,
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        },
        "aaSorting": []
    });

    $('.images').on('load', function(){
      $('.loader_img').hide();
    });
    $('.images').on('error', function(){
      $('.loader_img').hide();
      $(this).closest("td").html('<?php echo __("ERROR LOADING PICTURE", "PICTURE ERROR"); ?>');
    });

});

</script>
<h2><?php echo __("Mark-In Log", "Mark-In Log"); ?></h2>
<br><?php echo __("Report Description", "This report is helpful for seeing the order in which orders were marked in.  Often times an item can jump from one order to the next if the employee is not paying attention.  Also, sometimes a barcode might not have been scanned in and this report is helpful for figureing out what order it might be part of.  Here you can see what the order was before and after."); ?>
<form action="" method="post">
<br>
<?php echo __("Select Date", "Select Date:"); ?>
<select name="logDate">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if($_POST['logDate'] == $currentDay)
			echo ' SELECTED';
		echo '>'.date("D - m/d/Y", strtotime($currentDay)).'</option>';
	}
	?>
</select>
<input type="checkbox" name="pics" value="1" <? if($_POST['pics'] == 1) echo ' CHECKED'; ?>><?php echo __("Show Pictures", "Show Pictures"); ?> &nbsp;&nbsp;&nbsp;&nbsp;
<?php echo __("More than", "More than"); ?> <select name="NewItems">
	<?
	for ($i = 0; $i < 20; $i++)
	{
		echo '<option value="'.$i.'"';
		if($_POST['NewItems'] == $i)
			echo ' SELECTED';
		echo '>'.$i.'</option>';
	}
	?>
</select> <?php echo __("new items", "new items"); ?>&nbsp;&nbsp;
<input type="submit" name="submit" value="<?php echo __("go", "go"); ?>">
</form>
<br>
<table>
<thead>
<tr>
<th><?php echo __("Employee", "Employee"); ?></th>
<th><?php echo __("Time Inventoried", "Time Inventoried"); ?></th>
<th><?php echo __("Order", "Order"); ?></th>
<th><?php echo __("New", "New"); ?></th>
<th><?php echo __("Item ID", "Item ID"); ?></th>
<th><?php echo __("Description", "Description"); ?></th>
<th><?php echo __("Customer", "Customer"); ?></th>
<? if($_POST['pics'] == 1): ?>
<th><?php echo __("Picture", "Picture"); ?></th>
<? endif; ?>
</tr>
</thead>
<tbody>
	<? if($orders): foreach($orders as $o): if($newCount[$o->order_id] >= $_POST['NewItems']): ?>
	<tr>
	<? if($lastOrder != $o->order_id): ?>
        <td><?= getPersonName($o) ?></td>
		<td nowrap><?= local_date($this->business->timezone, $o->date, "m/d/Y g:ia") ?></td>
		<td nowrap><a href="/admin/orders/details/<?= $o->order_id ?>" target="_blank"><?= $o->order_id ?></a></td>
        <td><?= $newCount[$o->order_id] ?></td>
	<? else: ?>
		<td></td><td></td><td></td><td></td>
	<? endif; ?>

	<?
        $lastOrder = $o->order_id;
        $newItem = in_array($o->item_id, $newItems);
	?>
		<td nowrap <?= $newItem ? ' bgcolor="#FFFF00" bordercolor="#FFFF00" style="background-color: Yellow;"' : '' ?>>
            <?= $newItem ? 'NEW - ' : '' ?>
            <a href="/admin/items/view/?itemID=<?= $o->item_id ?>"><?= $o->item_id ?></a>
		</td>
        <td><?= $o->name ?></td>
		<td><a href="/admin/customers/detail/<?= $o->customerID ?>" target="_blank"><?= $o->custFirst .' '.$o->custLast?></a></td>
        <? if($_POST['pics'] == 1): ?>
		<td>
            <? if (!empty($pictures[$o->item_id])):
                $picture = 'https://s3.amazonaws.com/LaundryLocker/' . $pictures[$o->item_id]->file;
                $thumb = 'https://s3.amazonaws.com/LaundryLocker/' . $pictures[$o->item_id]->file;
            ?>
                <img class="loader_img" src="/images/loading_spinner.gif" style="width:20px;height:auto;" />
                <a target="_blank" href="<?= $picture ?>"><img class="images" src="<?=$thumb?>" width="100" height="100">
                </a>
            <? else: ?>
                <?php echo __("NO PICTURE", "NO PICTURE"); ?>
            <? endif; ?>
        </td>
		<? endif; ?>
	</tr>
	<? endif; endforeach; else: ?>
	<tr><td colspan="5"><?php echo __("No Orders", "No Orders"); ?></td></tr>
	<? endif; ?>
</tbody>
</table>
<?php echo __("Total Pieces", "Total Pieces:"); ?> <?= count($orders); ?>
