<?php enter_translation_domain("admin/reports/uniqueDetails"); ?>
<h2><?php echo __("Unique Customers for", "Unique Customers for"); ?> <?= $this->input->get_post("month") ?></h2>

<table id="box-table-a">
    <thead>
        <tr>
            <th></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Last Order", "Last Order"); ?></th>
            <th><?php echo __("Signup Date", "Signup Date"); ?></th>
            <th><?php echo __("Email", "Email"); ?></th>
        </tr>
    </thead>
    <tbody>
    <? foreach($firstCustomers as $f): $counter++; ?>
    <tr>
        <td><?= $counter ?></td>
        <td><a href="/admin/customers/detail/<?= $f->customerID ?>" target="_blank"><?= getPersonName($f) ?></a></td>
        <td><?= convert_from_gmt_aprax($f->lastOrder, SHORT_DATE_FORMAT) ?></td>
        <td><?= convert_from_gmt_aprax($f->signupDate, SHORT_DATE_FORMAT) ?></td>
        <td><?= $f->email ?></td>
    </tr>
    <? endforeach ?>    
    </tbody>
</table>