<?php enter_translation_domain("admin/reports/customers"); ?>
<?= $this->session->flashdata('message')?>
<h2><?php echo __("Customers", "Customers"); ?></h2>
<script type="text/javascript">
$(document).ready(function() {
    
    $.getJSON("/admin/customers/get_customer_report_dataTables_columns", {}, function(columns) {
        aoColumns = columns;
        inactive_customers_dataTable = $("#customers").dataTable( {
            "iDisplayLength": 25,
            "aLengthMenu": [ [25, 50, 100, -1],[25, 50, 100, "All"] ],
            "aoColumns" : aoColumns,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "/admin/customers/get_customer_report_dataTables_data",
            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
            }
        }).fnSetFilteringDelay(); 
        new FixedHeader(inactive_customers_dataTable);
    }).error(function() {
            alert("<?php echo __("An error occurred retrieving the customer report datatable columns", "An error occurred retrieving the customer report datatable columns"); ?>");
        });
    

});

    
</script>

<table id="customers">


</table>