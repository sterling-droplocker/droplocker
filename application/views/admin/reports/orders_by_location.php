<?php enter_translation_domain('reports/orders_by_location'); ?>
<script type="text/javascript">
    $(document).ready(function () {

        $.fn.dataTableExt.afnSortData['attr'] = function (oSettings, iColumn)
        {
            return $.map(oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
                return $('td:eq(' + iColumn + ')', tr).attr('data-value');
            });
        }

        $("#details").dataTable(
                {
                "aaSorting": [],
                        "bPaginate": false,
                        "bJQueryUI": true,
                        "sDom": '<"H"lTfr>t<"F"ip>',
                        "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null <? foreach($headers as $h) echo ",\n{\"sSortDataType\": \"attr\", \"sType\": \"numeric\"}" ?>
                        ],
                        "oTableTools": {
<?php if (in_bizzie_mode() && !is_superadmin()): ?>
                            aButtons : [
                                    "pdf", "print"
                            ],
<?php endif ?>
                        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                }
        );
    });
</script>

<h2><?= __("orders_by_location", "Orders By Location"); ?></h2>

<?=$select?>
<?=$body?>
