<?php enter_translation_domain("admin/reports/pcs_per_hour"); ?>
<?php
    function generate_time_select($type = 'hours', $selected = '00') {

        if ($type == 'hours') {
            $total = 24;
        } else {
            $total = 60;
        }

        $output = '';
        for ($i = 0; $i < $total; $i++) {
            $value = $i;
            if ($i < 10) {
                $value = '0'.$value;
            }
            $selectedOutput = '';
            if ($value == $selected) {
                $selectedOutput = 'selected="selected"';
            }
            $output.= '<option value="'.$value.'" '.$selectedOutput.'>'.$value.'</option>';
        }

        return $output;
    }


?>
<h2><?php echo __("PCS per Hour on", "PCS per Hour on"); ?> <?= date("D m/d/y", strtotime($from))?></h2>
<br>
<form action="/admin/reports/pcs_per_hour" method="post">
    <label>

        <?php echo __("From", "From:"); ?> <input type="text" name="from" value="<?= $from ?>" class="datefield">
        
        <select id="from_hours" name="from_hours">
           <?= generate_time_select('hours', $from_hours);?>
        </select>
        :
        <select id="from_minutes" name="from_minutes">
           <?=  generate_time_select('minutes', $from_minutes); ?>
        </select>
    </label>&nbsp;&nbsp;

    <label>
        <?php echo __("To", "To:"); ?> <input type="text" name="to" value="<?= $to ?>" class="datefield">
        
        <select id="to_hours" name="to_hours">
           <?= generate_time_select('hours', $to_hours);?>
        </select>
        :
        <select id="to_minutes" name="to_minutes">
           <?=  generate_time_select('minutes', $to_minutes); ?>
        </select>

    </label>
    <label><?php echo __("Role", "Role:"); ?> <?= form_dropdown('role', $roles, $role); ?></label>
    <input type="submit" name="Go" value="Run" class="button orange">
</form>
<br>
<br>
<? if(!empty($emps)): ?>
<table id="box-table-a">
<tr>
<th><?php echo __("Employee", "Employee"); ?></th>
<th><?php echo __("Hours", "Hours"); ?></th>
<th><?php echo __("Production Time", "Production Time"); ?></th>
<th><?php echo __("Non Production Time", "Non Production Time"); ?></th>
<th><?php echo __("Pcs", "Pcs"); ?></th>
<th><?php echo __("PPH", "PPH"); ?></th>
<th><?php echo __("Daily PPH", "Daily PPH"); ?></th>
<th><?php echo __("Upcharges", "Upcharges"); ?></th>
<? if(!empty($picturesPPH)): ?>
<th colspan=2><?php echo __("Pictures", "Pictures"); ?></th>
<? endif; ?>
</tr>
<?php foreach($emps as $e):
if(isset($showEmp[$e->employeeID]) && $showEmp[$e->employeeID] == 1):?>
<tr>
    <td><?= getPersonName($e) ?></td>
    <td><a href="/admin/employees/timecard?mgrEdit=1&cardId=<?= $card[$e->employeeID]->timeCardID ?>" target="_blank"><?= number_format_locale($hours[$e->employeeID]['regHours'],2) ?> / <?= number_format($hours[$e->employeeID]['overTime'],2) ?></a></td>
    <td><?= sec_to_time($pph[$e->employeeID]['elapsedTime']) ?></td>
    <td><?= isset($hours) ? sec_to_time(($hours[$e->employeeID]['totHrs'] * 60) - $pph[$e->employeeID]['elapsedTime']):''; ?></td>
    <td><a href="/admin/reports/pcs_detail?from=<?= $from ?>&from_hours=<?=$from_hours;?>&from_minutes=<?=$from_minutes;?>&to=<?= $to ?>&to_hours=<?=$to_hours;?>&to_minutes=<?=$to_minutes;?>&emp=<?= $e->employeeID ?>&role=<?= $role ?>" target="_blank"><?= $pph[$e->employeeID]['itemCount'] ?></a></td>
    <td><?= !empty($pph[$e->employeeID]['pph']) ? number_format_locale($pph[$e->employeeID]['pph'],1) : '0.0'?></td>
    <td>
        <? if(isset($hours[$e->employeeID]['totHrs'])): ?>
            <?= number_format_locale($pph[$e->employeeID]['itemCount'] /  ($hours[$e->employeeID]['totHrs'] / 60), 1); ?>
        <? endif; ?>
    </td>
    <td>
    <?php if($pph[$e->employeeID]['userUpcharges'] > 0): ?>
        <?=$pph[$e->employeeID]['userUpcharges'] . ' = '.number_format_locale(($pph[$e->employeeID]['userUpcharges'] / $pph[$e->employeeID]['itemCount'])*100,0).'%'; ?>
        <?php if (!empty($upchargesToday[$e->employeeID])): ?>
        (<?=count($upchargesToday[$e->employeeID]);?> <?php echo __("new", "new"); ?>)
        <?php endif;?>
    </td>
    <?php endif; ?>
    
    <? if(!empty($picturesPPH[$e->employeeID]['itemCount'])): ?>
    <td><?= $picturesPPH[$e->employeeID]['itemCount'] ?> pics</td>
    <td><?= number_format_locale($picturesPPH[$e->employeeID]['pph'],0) ?> <?php echo __("pph", "pph"); ?></td>
    <? endif; ?>
</tr>
<?php endif; endforeach; ?>

<tfoot>
    <th>Total</th>
    <th><?= number_format_locale($regTot,2) ?> / <?= number_format($otTot,2) ?></th>
    <th><?= sec_to_time($totHrs) ?></th>
    <th></th>
    <th><?= $totItems ?></th>
    <th><? if($totItems) { echo number_format_locale($totItems / ($totHrs/60/60), 1); } ?></th>
    <th></th>
    <th><?= $upchargeTot ?></th>
    <? if(!empty($picturesPPH)): ?>
    <th colspan=2></th>
    <? endif; ?>
</tfoot>
</table>
<? endif; ?>



<script type="text/javascript">
$(".datefield").datepicker({
       showOn: "button",
       buttonImage: "/images/icons/calendar.png",
       buttonImageOnly: true,
       dateFormat: "yy-mm-dd"
});
</script>
