<? enter_translation_domain('admin/reports_invoice_pdf'); ?>
<?php
$date_format = get_business_meta($this->business_id, "shortDateDisplayFormat");

$show_payment_info = ($filter == 'All invoices' ? true : false);

$show_notes_column = false;
if (!empty($newInvoice['detail'])) {
    foreach($newInvoice['detail'] as $n => $ordNo) {
        if ($ordNo['ordPayment'] == 3) {
            $show_notes_column = true;
        }
    }
}

?>
<script type="text/javascript">
function calculate2() {
var total = 0;

$('input[data-amount]:checked').each(function(i, e) {
    total += parseFloat($(e).attr('data-amount'));
});

$('input[name=total]').val(total.toFixed(2));

}

$(function() {
    calculate2();
});

</script>

<h2><?php echo __("Invoicing", "Invoicing"); ?></h2>
<br>
<a href="/admin/reports/invoicing"><?php echo __("Run Invoicing", "Run Invoicing"); ?></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/admin/reports/invoices_allCustomers?status=unpaid"><?php echo __("Show All Unpaid Invoices", "Show All Unpaid Invoices"); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/admin/reports/invoices_allCustomers?status=all"><?php echo __("Show all past invoices", "Show all past invoices"); ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="/admin/invoice_custom_fields"><?php echo __("Custom Fields", "Custom Fields"); ?></a>
<br>
<br>
<? if(!empty($allInvoices)): ?>
<table id="invoices_table">
	<thead>
		<tr>
                    <th><?php echo __("Customer", "Customer"); ?></th>
                    <th><?php echo __("Unpaid Invoices", "Unpaid Invoices"); ?></th>
                    <th><?php echo __("Amount", "Amount"); ?></th>
		</tr>
	</thead>
	<tbody>
		<?
        $total = 0;
        if($allInvoices): foreach($allInvoices as $a): $total += $a->amount; ?>
		<tr>
                    <td><a href="/admin/reports/invoices_open/<?= $a->customer_id ?>/check"><?= getPersonName($a) ?></a></td>
			<td>
			<? if($a->invoiceNumber): ?>
				<?php echo __("Invoiced on", "Invoiced on"); ?> <?=convert_from_gmt_aprax($a->dateInvoiced, $date_format) ?>, <?php echo __("invoice_number", "invoice #:"); ?><a href="/admin/reports/invoices_generate/view/<?= $a->invoiceNumber ?>"><?= $a->invoiceNumber ?></a>, <?= $a->ordCount ?> <?php echo __("orders", "orders"); ?>
			<? else: ?>
				<a href="/admin/reports/invoices_open/<?= $a->customer_id ?>"><?php echo __("Un-Invoiced Orders", "Un-Invoiced Orders"); ?></a> (<?= $a->ordCount ?>)
			<? endif; ?>
			</td>
            <td><?= !empty($a->invoiceNumber) ? format_money_symbol($this->business_id, '%.2n', $a->amount) : '' ?></td>
        </tr>
		<? endforeach; else:?>
		<tr><td colspan='2'><?php echo __("No Orders for Invoicing", "No Orders for Invoicing"); ?></td></tr>
		<? endif; ?>
	</tbody>
    <tfoot>
        <tr>
            <td colspan=2></td>
            <td><?= format_money_symbol($this->business_id, '%.2n', $total) ?></td>
        </tr>
    </tfoot>
</table>

<script type="text/javascript">
$(document).ready(function() {
        var invoices_table = $("#invoices_table").dataTable({
            "iDisplayLength" : 25,
            "bPaginate" : false,

            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                <? if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <? endif ?>
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
        });
});
</script>


<? endif; ?>

<? if(!empty($unpaidOrders)): ?>
<h2>Open Invoices for <a href="/admin/customers/detail/<?= $customer[0]->customerID ?>" target="_blank"><?= getPersonName($customer[0]) ?></a></h2>
    <br>
    <a href="/admin/reports/invoices_history/<?= $customer[0]->customerID ?>">Show invoice history</a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="/admin/reports/invoices_history/<?= $customer[0]->customerID ?>/open">Show Summary Of Open Invoices</a><br>
    <br>
    <?= $this->session->flashdata("message") ?>
    <form action="/admin/reports/invoices_generate" method="post" name="generate" id="generate">
    <table id="unpaid_orders">
        <thead>
            <tr>
                <th><?php echo __("Order", "Order"); ?></th>
                <th><?php echo __("Bag", "Bag"); ?></th>
                <th><?php echo __("Order Date", "Order Date"); ?></th>
                <th><?php echo __("Amount", "Amount"); ?></th>
                <th><?php echo __("Notes", "Notes"); ?></th>
                <th align="right"><?php echo __("Generate Invoice", "Generate Invoice"); ?></th>
                <th align="right"><?php echo __("Apply Payment", "Apply Payment"); ?></th>
            </tr>
        </thead>
        <tbody>
                <?
                    $selectedInvoice = null;
                    if ($this->uri->segment(5) == 'checkApply') {
                        $selectedInvoice = $this->uri->segment(6);
                    }
                    $checkUninvoiced = $this->uri->segment(5) == 'check';
                    $counter = 0;
                ?>
                <? foreach($unpaidOrders as $unpaid_order):?>
                    <?
                        $order_total = $unpaid_order->net_total;
                    ?>
                    <? if ($unpaid_order->orderStatusOption_id == 10 and $order_total <> 0): $counter++;?>
                    <tr>
                        <td><a href="/admin/orders/details/<?= $unpaid_order->orderID ?>" target="_blank"><?= $unpaid_order->orderID ?></a></td>
                        <td><?= $unpaid_order->bagNumber ?></td>
                        <td><?= convert_from_gmt_aprax($unpaid_order->dateCreated, $date_format) ?></td>
                        <td><?= format_money_symbol($this->business_id, '%.2n', $order_total) ?></td>
                        <td><? if($unpaid_order->dateInvoiced): ?>Invoiced on <?=convert_from_gmt_aprax($unpaid_order->dateInvoiced, $date_format)?>, invoice #<a href="/admin/reports/invoices_generate/view/<?= $unpaid_order->invoiceNumber ?>"><?= $unpaid_order->invoiceNumber ?></a><? endif; ?></td>
                        <td align="right">
                            <input type="checkbox" name="generate[<?= $unpaid_order->orderID ?>]" value="<?= $unpaid_order->orderID ?>" <?= $checkUninvoiced && !$unpaid_order->dateInvoiced ? ' CHECKED' : ''; ?> <?= $unpaid_order->dateInvoiced ? 'disabled' : '' ?>>
                        </td>
                        <td align="right">
                            <input type="checkbox" name="apply[<?= $unpaid_order->orderID ?>]" value="<?= $unpaid_order->orderID ?>" data-amount="<?= str_replace(',' ,'.' ,$order_total) ?>" onclick="calculate2()"
                            <?= $selectedInvoice && $selectedInvoice == $unpaid_order->invoiceNumber ? ' CHECKED' : '';  ?> > &nbsp;&nbsp;
                            <? if ($unpaid_order->invoiceNumber): ?>
                                <? if($unpaid_order->terms == 'Credit Card'): ?>
                                    <a href="/admin/reports/invoices_chargeToCard/<?= $customer[0]->customerID ?>/<?= $unpaid_order->invoiceNumber ?>"><?php echo __("Charge to Credit Card", "Charge to Credit Card"); ?></a>
                                <? else: ?>
                                    <a href="/admin/reports/invoices_open/<?= $customer[0]->customerID ?>/checkApply/<?= $unpaid_order->invoiceNumber ?>"><?php echo __("Check this invoice", "Check this invoice"); ?></a>
                                <? endif; ?>
                            <? endif; ?>
                        </td>
                    </tr>
                    <? endif; ?>
                <? endforeach; ?>
        </tbody>
    </table>

    <br>
    <table>
        <tr>
            <td width="100%"></td>
            <td align="center" valign="top">
                <input type="submit" name="generateInv" value="Generate Invoice" class="button blue"><br><br>
                <a href="/admin/reports/invoices_open/<?= $customer[0]->customerID ?>/check"><?php echo __("Check all Un-Invoiced", "Check all Un-Invoiced"); ?></a>
            </td>
            <td align="right" valign="top">
                <div style="width:300px;">
                <?php echo __("Total", "Total:"); ?> <input type="text" name="total" readonly><br>
                <?php echo __("Check Number", "Check Number:"); ?> <input type="text" name="checkNumber"><br><br>
                <input type="submit" name="applyInv" value="<?php echo __("Apply Payment", "Apply Payment"); ?>" class="button orange">
                </div>
            </td>
        </tr>
    </table>

        <input type="hidden" name="custID" value="<?= $customer[0]->customerID ?>">
    </form>
<script type="text/javascript">
$(document).ready(function() {
        var unpaid_orders = $("#unpaid_orders").dataTable({
            "iDisplayLength" : 25,
            "bPaginate" : false,

            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                <? if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <? endif ?>
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
        });
});
</script>


<? endif; ?>

<? if(!empty($unpaidInvoices)): ?>

<h2><?= $filter ?></h2>

<table id="unpaid_invoces">
	<thead>
            <tr>
                <th><?php echo __("Customer", "Customer"); ?></th>
                <th><?php echo __("Inv_number", "Inv #"); ?></th>
                <th><?php echo __("Date", "Date"); ?></th>
                <th align="right"><?php echo __("Amount", "Amount"); ?></th>
            <? if ($show_payment_info): ?>
                <th><?php echo __("Date Paid", "Date Paid"); ?></th>
                <th><?php echo __("Payment Method", "Payment Method"); ?></th>
            <? endif; ?>
            </tr>
	</thead>
	<tbody>
            <? if($unpaidInvoices == 'x'): ?>
                <tr><td colspan="7"><?php echo __("No Open Invoices", "No Open Invoices"); ?></td></tr>
            <? else: $openTotal = 0; foreach($unpaidInvoices as $unpaid_order => $inv): $openTotal += $inv['total'] ?>
                <tr>
                    <td><?= $inv['cust'] ?></td>
                    <td><a href="/admin/reports/invoices_generate/view/<?= $unpaid_order ?>"><?= $unpaid_order ?></a></td>
                    <td><?= $inv['dateInvoiced'] ?></td>
                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $inv['total']) ?></td>
                <? if ($show_payment_info): ?>
                    <td><? if (!is_null($inv['datePaid'])) {
                               echo ($inv['datePaid']);
                           } else {
                               echo '<em>' . __("unpaid", "unpaid") . '</em>';
                           } ?></td>
                    <td><? if ($inv['paymentMethod'] == 'Check') {
                               echo __('Check', 'Check') . ' #' . $inv['check'] . ' (' . format_money_symbol($this->business_id, '%.2n', $inv['checkAmt']) . ')';
                           } elseif ($inv['paymentMethod'] == 'Credit Card') {
                               echo __("Credit Card", "Credit Card");
                           } elseif ($inv['paymentMethod'] == 'Other') {
                               echo __("Other", "Other");
                           } else if (!is_null($inv['paymentMethod'])) {
                               echo htmlspecialchars($inv['paymentMethod']);
                           } else {
                               echo '<em>' . __("unpaid", "unpaid") . '</em>';
                           } ?></td>
                <? endif; ?>
                </tr>
            <? endforeach; ?>
	</tbody>
	<tfoot>
            <tr><td colspan="3" align="right"><strong><?php echo __("Total Due", "Total Due:"); ?></strong></td><td align="right"><strong><?= format_money_symbol($this->business_id, '%.2n', $openTotal) ?></strong></td><? if ($show_payment_info): ?><td></td><td></td><? endif; ?></tr>
	</tfoot>
	<? endif; ?>

</table>


<script type="text/javascript">
$(document).ready(function() {
        var unpaid_invoces = $("#unpaid_invoces").dataTable({
            "iDisplayLength" : 25,
            "bPaginate" : false,

            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                <? if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <? endif ?>
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
        });
});
</script>


<? endif; ?>

<? if(!empty($newInvoice)):

$customer_id = $customer[0]->customer_id;

?>



<br>
<br>
<a href="/admin/reports/invoices_generate/view/<?= $newInvoice['number'] ?>/email"><?php echo __("Email this to", "Email this to"); ?> <?= $customer[0]->email ?></a>
<br>
<br>
<a target='_blank' href="/admin/invoices/export_quickbooks?invoiceNumber=<?= $newInvoice['number'] ?>"><?php echo __("Export to QuickBooks", "Export to QuickBooks"); ?></a>
<br>
<br>
<a target='_blank' href="/admin/invoices/download?invoiceNumber=<?= $newInvoice['number'] ?>"><?php echo __("Download PDF", "Download PDF"); ?></a>
<br>
<br>
<br>
<br>
<div style="font-size: 18px;">
<?= $customer[0]->billingName ?><br><?= $customer[0]->address ?>
</div>
<br>
<br>
<h2><?= __("invoice", "INVOICE", $customer_id); ?> <?= $newInvoice['number'] ?></h2>
<br>
<table border="1" cellspacing="2" class="space-table border-black">
	<thead>
		<tr>
			<th><?= __("head_item", "Item", $customer_id); ?></th>
			<th align="right"><?= __("head_date","Date", $customer_id); ?></th>
			<th align="right"><?= __("head_price","Price", $customer_id); ?></th>
			<th align="right"><?= __("head_qty","Qty", $customer_id); ?></th>
			<th align="right"><?= __("head_total","Total", $customer_id); ?></th>
			<? if ($show_notes_column): ?>
			    <th align="right"><?= __("head_notes", "Notes") ?></th>
			<? endif; ?>
		</tr>
	</thead>
            <tbody>
                <? foreach($newInvoice['detail'] as $n => $ordNo): ?>
                    <tr>
                        <td width="250">
                            <?= __('row_order', 'Order') ?> <a href="/admin/orders/details/<?= $n ?>"><?= $n ?></a>
                            <? if(!empty($byBag)) echo ' ('.$ordNo['bag'] .' - '. $ordNo['notes'].')';  ?> <br />
                            <? if(!empty($items[$n])): ?>
                                <table border="0" cellspacing="0" cellpadding="0" style="font-size: 75%;">
                                    <tr>
                                        <td align="left"> <strong><?= __('row_item', 'Item') ?></strong> </td>
                                        <td align="left"> <strong><?= __('row_barcode', 'Barcode') ?></strong> </td>
                                        <td align="right"> <strong><?= __('row_qty', 'Qty') ?></strong> </td>
                                        <? if($customer[0]->bagDetail == 2): ?>
                                            <td align="right"> <strong><?= __('row_price', 'Price') ?></strong> </td>
                                        <? endif; ?>
                                    </tr>
                                    <? foreach($items[$n] as $i): ?>
                                        <tr>
                                            <td align="left">&nbsp;&nbsp;<?= htmlspecialchars($i->name) ?> <?= !empty($i->notes) ? '('.htmlspecialchars(preg_replace('#<br */?>#', " ", $i->notes)).')' : ''; ?> </td>
                                            <td align="left"><a href="/admin/orders/edit_item?orderID=<?=$n;?>&itemID=<?=$i->itemID;?>" target="_blank"><?=$i->barcode;?></a></td>
                                            <td align="right"><?= $i->qty ?> </td>
                                            <? if($customer[0]->bagDetail == 2): ?>
                                                 <td align="right"><?= $i->unitPrice ?> </td>
                                            <? endif; ?>
                                        </tr>
                                    <? endforeach; ?>
                                </table>
                            <? endif; ?>

                            <? if(!empty($charges[$n])): ?>
                                <table border="0" cellspacing="0" cellpadding="0" style="font-size: 75%;" width="200">
                                    <tr><td align="left"><strong><?= __('row_charge_credits', 'Charges / Credits') ?></strong></td>
                                    <td align="right"><strong><?= __('row_charge_amount', 'Amt') ?></strong></td></tr>
                                    <? foreach($charges[$n] as $c): ?>
                                        <tr>
                                            <td align="left" nowrap>&nbsp;&nbsp;<?= $c->chargeType ?></td>
                                            <td align="right"><?= $c->chargeAmount ?></td>
                                        </tr>
                                    <? endforeach; ?>
                                </table>
                            <? endif; ?>
                        </td>
                        <td width="70" align="right" valign="top"> <?=convert_from_gmt_aprax($ordNo['dateCreated'], $date_format) ?> </td>
                        <td width="60" align="right" valign="top"> <?= format_money_symbol($this->business_id, '%.2n', $ordNo['ordTotal']) ?> </td>
                        <td width="40" align="right" valign="top"> 1 </td>
                        <td width="70" align="right" valign="top"> <?= format_money_symbol($this->business_id, '%.2n', $ordNo['ordTotal']) ?> </td>
                        <? if ($ordNo['ordPayment'] == 3 || !empty($ordNo['error'])): ?>
                            <td valign="top" align="right">
                                <? if (!empty($ordNo['error'])): ?>
                                <p class="error"><?= $ordNo['error'] ?></p>
                                <? endif; ?>
                                <?= $ordNo['ordNotes'] ?>
                            </td>
                        <? endif; ?>
                    </tr>
                <? endforeach; ?>
            </tbody>
            <tfoot>
                    <tr>
                        <td colspan="2" align="center" valign="middle" width="320" >
                            <?php foreach ($custom_fields as $cf): ?>
                               <?php echo $cf->title; ?> <?php echo $cf->value; ?><br/>
                            <?php endforeach; ?>
                            <?= __('thank_you', 'Thank you for your business.  Please contact us if you have any questions regarding your bill.'); ?>
                        </td>
                        <td colspan="3" align="right" width="170" >
                            <table border="0" cellspacing="4" cellpadding="0" align="right" class="noBorder">
                            <? if ($breakout_vat):  ?>
                                <tr>
                                    <td align="right"><strong><?= __('total', 'Invoice Total:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['total']) ?></td>
                                </tr>
                            <? endif; ?>
                                <tr>
                                    <td align="right"><strong><?= __('subtotal', 'Subtotal:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['subTotal']) ?></td>
                                </tr>
                            <? if (count($newInvoice['taxDetail']) == 1): ?>
                                <tr>
                                    <td align="right"><strong><?= __('tax',  'Tax:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['taxTotal']) ?></td>
                                </tr>
                            <? else: ?>
                                <? foreach ($newInvoice['taxDetail'] as $taxGroup_id => $amount):
                                    if (isset($newInvoice['taxGroups'][$taxGroup_id])) {
                                        $tax_label = $newInvoice['taxGroups'][$taxGroup_id]->name . ' :';
                                    } else {
                                        $tax_label = __('tax',  'Tax:');
                                    }
                                ?>
                                <tr>
                                    <td align="right"><strong><?= $tax_label ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $amount) ?></td>
                                </tr>
                                <? endforeach; ?>
                            <? endif; ?>
                            <? if (!$breakout_vat):  ?>
                                <tr>
                                    <td align="right"><strong><?= __('total', 'Invoice Total:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['total']) ?></td>
                                </tr>
                            <? endif; ?>

                            <? if (!empty($newInvoice['amtPaid'])): ?>
                                <tr>
                                    <td align="right"><strong><?= __('paid', 'Paid:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right">(<?= format_money_symbol($this->business_id, '%.2n', $newInvoice['amtPaid'])?>)</td>
                                </tr>
                                <tr>
                                    <td align="right"><strong><?= __('total_due', 'Total Due:') ?>&nbsp;&nbsp;</strong></td>
                                    <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $newInvoice['total'] - $newInvoice['amtPaid']) ?></td>
                                </tr>
                            <? endif; ?>
                            </table>
                        </td>
                    </tr>
            </tfoot>
        </table>
        <br />
        <div style="font-weight: bold;"><?= get_business_meta($this->business_id, 'invoiceNotes') ?> </div>
        <? if(!empty($byBag)):?>
            <br />
            <br />
            <?= __('employee_breakdown_title', 'Breakdown by Employee:') ?>
            <br />
            <table border="1" cellspacing="2" class="normal">
                <tr>
                    <th><?= __('breakdown_employee', 'Employee') ?></th>
                    <th><?= __('breakdown_bag', 'Bag') ?></th>
                    <th align="right"><?= __('breakdown_total_spend', 'Total Spend') ?></th>
                </tr>
                <? foreach($byBag as $bagNum => $bagInfo): ?>
                    <tr>
                         <td> <?= $bagInfo['name']?> </td>
                         <td> <?= $bagNum?> </td>
                         <td align="right"> <?= format_money_symbol($this->business_id, '%.2n', $bagInfo['amt']) ?> </td>
                    </tr>
                <? endforeach;?>
            </table>
        <? endif; ?>

<? endif; ?>

<? if(!empty($history)): ?>
<? if($this->uri->segment(5) == 'open'): ?>
<h2><?php echo __("Open Invoices", "Open Invoices"); ?>
<? else: ?>
<h2><?php echo __("Invoicing History", "Invoicing History"); ?>
<? endif; ?>
for <a href="/admin/customers/detail/<?= $customer[0]->customerID ?>" target="_blank"><?= getPersonName($customer[0]) ?></a></h2>
<br>
<table id="box-table-a">
	<thead>
		<tr>
			<th><?php echo __("Invoice Number", "Invoice Number"); ?></th>
			<th><?php echo __("Amount", "Amount"); ?></th>
			<th><?php echo __("Invoice Date", "Invoice Date"); ?></th>
			<th><?php echo __("Status", "Status"); ?></th>
		</tr>
	</thead>
	<tbody>
<? foreach($history as $h): ?>
<? if($this->uri->segment(5) == 'open' and $h->datePaid): ?>
<? else: ?>
		<tr>
			<td><a href="/admin/reports/invoices_generate/view/<?= $h->invoiceNumber ?>"><?= $h->invoiceNumber ?></a></td>
			<td><?= format_money_symbol($this->business_id, '%.2n', $h->amount) ?></td>
			<td><?= $h->dateInvoiced ?></td>
			<td><? if($h->datePaid) { echo __("Paid on", "Paid on ").$h->datePaid,' '.__("with check", "with check").' #:'.$h->check; } else { echo __("unpaid", "unpaid"); } ?></td>
		</tr>
<? endif; ?>
<? endforeach; ?>
	</tbody>
</table>
<br>
<br>
<? else: ?>
<br>
<br>
<!-- <h3>No invoices</h3> -->

<? endif; ?>


