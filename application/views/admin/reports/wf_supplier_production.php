<?php enter_translation_domain("admin/reports/wf_supplier_production"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $(":submit").loading_indicator();
});
</script>
<h2><?php echo __("Wash and Fold Supplier Production", "Wash & Fold Supplier Production"); ?></h2>
<br>
<form action="" method="post">
<?php echo __("Select Supplier", "Select Supplier:"); ?> <select name="supplier">
<? foreach($suppliers as $s): ?>
	<option value="<?= $s->supplierID ?>"
	<? if($s->supplierID == $_POST['supplier']) echo ' SELECTED'; ?>
	><?= $s->companyName ?></option>
<? endforeach; ?>
</select>
<select name="selectedDate">
<?
for ($i = 0; $i <= 800 -1; $i++) {
	$selectDay = date("Y-m-d", strtotime(-$i." day"));
	echo '<option value="'.$selectDay.'"';
	if ($selectDay == $_POST['selectedDate']) { echo ' SELECTED';}
	echo '>'.date("D M-d",strtotime($selectDay)).'</option>';
}
?>
</select>

<input type="radio" name="byBag" value="byBag" checked<? if($_POST['byBag'] == 'byBag') echo ' checked'; ?>><?php echo __("By Bag", "By Bag"); ?>
<input type="radio" name="byBag" value="byEmp" <? if($_POST['byBag'] == 'byEmp') echo ' checked'; ?>><?php echo __("By Employee", "By Employee"); ?>
<input type="checkbox" name="justBags" value="1"  <? if($_POST['justBags'] == '1') echo ' checked'; ?>><?php echo __("Just Bags", "Just Bags"); ?>
<? if($this->zacl->check_acl('wf_price_checkbox', $this->session->userdata('buser_id'))): ?>
<input type="checkbox" name="pricing" value="1"  <? if($_POST['pricing'] == '1') echo ' checked'; ?>><?php echo __("Pricing", "Pricing"); ?>
<? endif; ?>
<input type="submit" class="button orange" name="run" value="<?php echo __("Run", "Run"); ?>">
</form>
<br>
<? if($_POST['justBags']): ?>
<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("Bag", "Bag"); ?></th>
	</tr>
</thead>
<tbody>
<? foreach($justBags as $bag): ?>
	<tr><td><?= $bag ?></td></tr>
<? endforeach; ?>
</tbody>
<tfoot>
	<tr><td># <?php echo __("Bags", "Bags:"); ?> <?= count($justBags)?></td></tr>
</tfoot>
</table>
<? elseif($allItems): ?>
<?php echo __("Orders Weighed out Between from and to", "Orders Weighed out Between %from% and %to%:", array("from"=>date("m/d/y G:ia", strtotime($rangeStart)), "to"=>date("m/d/y G:ia", strtotime($rangeEnd)))); ?>
<br>
<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("Weighed Out", "Weighed Out"); ?></th>
		<th><?php echo __("Order ID", "Order ID"); ?></th>
		<th><?php echo __("Bag", "Bag"); ?></th>
		<th><?php echo __("Route", "Route"); ?></th>
		<th><?php echo __("Customer", "Customer"); ?></th>
		<th><?php echo __("Item", "Item"); ?></th>
		<th><?php echo __("Your", "Your"); ?> <?= weight_system_format('',false, true) ?></th>
        <? if($_POST['pricing'] == '1'):?>
		<th><?php echo __("Cost", "Cost"); ?></th>
		<th><?php echo __("Extended Cost", "Extended Cost"); ?></th>
        <? endif; ?>
		<th><?php echo __("Supplier", "Supplier"); ?> <?= weight_system_format('',false, true) ?> In</th>
		<th colspan=2><?php echo __("Washed", "Washed"); ?></th>
		<th colspan=2><?php echo __("Dried", "Dried"); ?></th>
		<th colspan=2><?php echo __("Folded", "Folded"); ?></th>
		<th><?php echo __("Supplier", "Supplier"); ?> <?= weight_system_format('',false, true) ?> Out</th>
		<th><?php echo __("Your Notes", "Your Notes"); ?></th>
		<th><?php echo __("Supplier Notes", "Supplier Notes"); ?></th>
	</tr>
</thead>
<tbody>
<? foreach($allItems as $a): ?>
<tr>
	<td><?= local_date($this->business->timezone, $a->ordDate, 'm/d g:i a') ?></td>
	<td><a href="/admin/orders/details/<?= $a->order_id ?>" target="_blank"><?= $a->order_id ?></a></td>
	<td><?= $a->bagNumber ?></td>
	<td><?= $a->route_id ?></td>
        <td><?= getPersonName($a) ?></td>
	<td><?= $a->name ?></td>
	<td><?= $a->qty ?></td>
    <? if($_POST['pricing'] == '1'):?>
	<td><?= format_money_symbol($this->business->businessID, '%.2n', $a->cost) ?></td>
	<td><?= format_money_symbol($this->business->businessID, '%.2n', $cost[$a->orderItemID]) ?></td>
    <? endif; ?>
	<? if(stristr($a->productCategory_id, $wfCat)): ?>
		<td><?= $a->weightIn ?></td>
		<td><?= local_date($this->business->timezone, $a->washTime, 'g:i a'); ?></td>
		<td><?= $a->washCust ?> ( <?= $a->washStation ?>)</td>
		<td><?= local_date($this->business->timezone, $a->dryTime, 'g:i a'); ?></td>
		<td><?= $a->dryCust ?> ( <?= $a->dryStation ?>)</td>
		<td><?= local_date($this->business->timezone, $a->foldTime, 'g:i a'); ?></td>
		<td><?= $a->foldCust ?> ( <?= $a->foldStation ?>)</td>
		<td><?= $a->weightOut ?></td>
		<td><?= $a->busNotes ?></td>
		<td><?= $a->notes ?></td>
	<? endif; ?>
</tr>
<? endforeach; ?>
</tbody>
<tfoot>
<tr>
	<th colspan="5"><?= count($vars['bagTotal']) ?> <?php echo __("Bags", "Bags"); ?></th>
	<th colspan="2" align="right"><?= number_format_locale($vars['lbTotal'],0) ?> <?= weight_system_format('',false, true) ?></th>
	<? if($_POST['pricing'] == '1'):?>
    <th colspan="2" align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $vars['costTotal']) ?></th>
	<? endif; ?>
    <th colspan="8" align="right"><?php echo __("Supplier Total", "Supplier Total:"); ?> &nbsp;&nbsp;&nbsp;<?php echo __("In", "In:"); ?> <?= $vars['supplierIn'] ?> <?= weight_system_format('',false, true) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo __("Out", "Out:"); ?> <?= $vars['supplierOut'] ?> <?= weight_system_format('',false, true) ?></th>
    <th colspan="2"></th>
</tr>
</tfoot>
</table>
<? endif; ?>
<? if($allItems): ?>
<br>
<br>
<h2><?php echo __("Weekly Numbers", "Weekly Numbers"); ?></h2>
<br>
<?php echo __("Orders Weighed out Between from and to", "Orders Weighed out Between %from% and %to%:", array("from"=>date("m/d/y G:ia", strtotime($weekStart)), "to"=>date("m/d/y G:ia", strtotime($weekEnd)))); ?>
<br>

<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("Date", "Date"); ?></th>
		<th><?= weight_system_format('',false, true) ?></th>
		<? if($_POST['pricing'] == '1'):?>
        <th><?php echo __("Cost", "Cost"); ?></th>
		<? endif; ?>
        <th><?php echo __("Bags", "Bags"); ?></th>
	</tr>
</thead>
<tbody>
<? foreach($week as $w => $value): ?>
<tr>
	<td><?= date("D m/d/y", strtotime($w)) ?></td>
	<td><?= $value['qtyTotal'] ?></td>
    <? if($_POST['pricing'] == '1'):?>
	<td><?= format_money_symbol($this->business->businessID, '%.2n',  $value['extCost']) ?></td>
    <? endif; ?>
	<td><?= count($value['bags']) ?></td>
</tr>
<? endforeach; ?>
</tbody>
<tfoot>
	<tr>
		<th></th>
		<th><?= $sum['qtyTotal'] ?></th>
        <? if($_POST['pricing'] == '1'):?>
	   	<th><?= format_money_symbol($this->business->businessID, '%.2n', $sum['extCost']) ?></th>
        <? endif; ?>
		<th><?= count($sum['bags']) ?></th>
	</tr>
</tfoot>
</table>
<? elseif($byEmp): ?>

<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("Employee", "Employee"); ?></th>
		<? foreach($byEmp as $b => $value): ?>
		<th><?= date("D", strtotime($b)).'<br>'.date("m/d", strtotime($b)) ?></th>
		<? endforeach; ?>
	</tr>
</thead>
<tbody>
<? foreach($emp as $e => $empMetrics): ?>
<tr>
	<td><?=$e ?></td>
	<? foreach($byEmp as $b => $value): ?>
		<td><?= $empMetrics[$b]['lb'] ?><? if($empMetrics[$b]['bags']) { echo '<br>('.$empMetrics[$b]['bags'].')'; } ?></td>
	<? endforeach; ?>
</tr>
<? endforeach; ?>
</tbody>
<tfoot>
	<tr>
		<td><?php echo __("Total", "Total"); ?></td>
		<? foreach($byEmp as $b => $value): ?>
		<td><?= $total[$b]['lbs'] ?><? if($total[$b]['bags']) { echo '<br>('.$total[$b]['bags'].')'; } ?></td>
		<? endforeach; ?>
	</tr>
</tfoot>
</table>

<? else: ?>
<?php echo __("No Data Found. Change Search Parameters", "No Data Found.  Change Search Parameters"); ?>
<? endif; ?>

