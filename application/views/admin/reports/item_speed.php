<?php
enter_translation_domain("admin/reports/item_speed");
?>

<h2><?= __("Slow Mark-Ins","Slow Mark-Ins")?></h2>

<script type="text/javascript">
jQuery(document).ready(function($){
    $("#item_speed").dataTable({  
        "iDisplayLength": 250, 
        "bProcessing" : true,
        "bServerSide" : true,
        "bStateSave" : true,
        "sAjaxSource" : "/admin/reports/item_speed/",
        "fnPreDrawCallback": function( oSettings ) {
            oSettings._iDisplayLength = 250;
        },
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "start_date", "value": "<?php echo $start_date ?>" } );
          aoData.push( { "name": "end_date", "value": "<?php echo $end_date ?>" } );
        },
        "bJQueryUI": true,
        "aaSorting" : [[ 1, "desc"]],
        "sDom": 'tr<"F"ip>',
        "aoColumns": [
            { "mDataProp":"order_id" },
            { "mDataProp":"dateCreated" },
            { "mDataProp":"maxDiff" },
        ]
    });


	$(".datefield").datepicker({
		showOn: "button",
		buttonImage: "/images/icons/calendar.png",
		buttonImageOnly: true,
		dateFormat: "yy-mm-d"
	});
});
</script>
<div style="padding: 8px;">
	<form action="" method="get">
		<table>
			<tr>
				<th><?= __("Slow Mark-Ins","Slow Mark-Ins")?></th>
				<td>
					<input readonly="true" name="start_date" value="<?= $start_date ?>" class="datefield">
					<?= __("and","And")?> 
					<input readonly="true" name="end_date" value="<?= $end_date ?>" class="datefield">
				</td>
			</tr>
		</table>
		<input type="submit" name="Submit" value="Run" class="button blue">
	</form>
</div>
<br>
	<p>
	<?= __("Showing orders having items with more than 5 minutes gap between items","Showing orders having items with more than 5 minutes gap between items")?>
		
	</p>
<table id='item_speed'>
	<thead>
		<tr>
			<th><?= __("OrderID","OrderID")?></th>
			<th><?= __("Date","Date")?></th>
			<th><?= __("Max Diff","Max Diff")?></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
