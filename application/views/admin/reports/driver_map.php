<?php enter_translation_domain("admin/reports/driver_map"); ?>
<html>
	<head>
		<title><?php echo __("Driver Map", "Driver Map"); ?></title>
		
		<script>
			var hash = "<?= md5(date('Y-m-d H:i:s'))?>";
			var domain = "<?= $_SERVER['HTTP_HOST']?>";
			var routes = <?= $routes?>;
			var locations = <?= $locations_json?>;
		</script>
       <script type="text/javascript" src='/js/jquery-1.6.4.min.js'></script>
       <script type="text/javascript" src='/js/functions.js'></script>
		<script src='https://maps.googleapis.com/maps/api/js?sensor=false&key=<?=get_default_gmaps_api_key();?>'></script>
		<script src='/js/jquery-ui-1.8.16.custom.min.js'></script>
		<script src='/js/driver_map2.js'></script>
		<link rel='stylesheet' href='/css/ui-lightness/jquery-ui-1.8.16.custom.css' />
	</head>
	<body style='text-align:left'>
	<h2><?php echo __("Driver Map", "Driver Map"); ?></h2>
		<div style='padding-top:5px;'>
			<? if($routes) : ?>
			<?php echo __("Routes", "Routes:"); ?> 
			<? foreach ($routes_box as $r) : ?>
				<?if(in_array($r->route_id, $selected_routes)):?>
				<input type='checkbox' class='routeID' name='route' checked='checked' value='<?= $r->route_id ?>' /> <?= $r->route_id ?>
				<? else:?>
				<input type='checkbox' class='routeID' name='route' value='<?= $r->route_id ?>' /> <?= $r->route_id ?>
				<? endif;?>
			<? endforeach; ?>
			<? else : ?>
			<p><?php echo __("No Routes", "No Routes"); ?></p>
			<? endif ; ?>
			<a href='javascript:;' id='select_route' class='button white'><?php echo __("Go", "Go"); ?></a>
		</div>
			<div style='margin:15px 0px;'>
				<div style='float:left;width:100%;'>
					<div id='map_canvas' style='outline:1px solid #ccc; height:500px;'></div>
				</div>
				
			</div>
			
			<div style='float:left;width:50%' id='left_column'>
				<div class='inner_pad'>
				<?if($locations): ?>
					<table width='100%'>
						<thead>
							<tr style='background-color:#ccc;'>
								<th><?php echo __("ID", "ID"); ?></th>
								<th><?php echo __("Code", "Code"); ?></th>
								<th><?php echo __("Sort", "Sort"); ?></th>
								<th><?php echo __("Address", "Address"); ?></th>
								<th><?php echo __("WF", "WF"); ?></th>
								<th><?php echo __("DC", "DC"); ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody id='listview'>
						<? foreach($locations as $l): ?>
							<tr id='s-<?=$l->sortOrder?>' style='background-color:#eaeaea'>
								<td><?= $l->locationID?></td>
								<td><?= $l->accessCode?></td>
								<td><?= $l->sortOrder?></td>
								<td><?= $l->address?></td>
								<td><?= ''?></td>
								<td><?= ''?></td>
								<td></td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				<?endif;?>
				</div>
				<div id='driver_notes' class='inner_pad'>
				<?if($notes): ?>
					<table>
						<thead>
							<tr>
								<th><?php echo __("ID", "ID"); ?></th>
								<th><?php echo __("Code", "Code"); ?></th>
								<th><?php echo __("Sort", "Sort"); ?></th>
								<th><?php echo __("Address", "Address"); ?></th>
								<th><?php echo __("WF", "WF"); ?></th>
								<th><?php echo __("DC", "DC"); ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<? foreach($notes as $n): ?>
							<tr>
								<td><?php echo __("ID", "ID"); ?></td>
								<td><?php echo __("Code", "Code"); ?></td>
								<td><?php echo __("Sort", "Sort"); ?></td>
								<td><?php echo __("Address", "Address"); ?></td>
								<td><?php echo __("WF", "WF"); ?></td>
								<td><?php echo __("DC", "DC"); ?>DC</td>
								<td></td>
							</tr>
						<? endforeach; ?>
						</tbody>
					</table>
				<?endif;?>
				</div>
			</div>
			<div style='float:left;width:50%' id='right_column'>
				<div id='wf_orders' class='inner_pad'>
					<? if($locations): 
					//print_r($wforders);
					?>
					<h3><?php echo __("Wash Fold", "Wash Fold"); ?></h3>
					<table>
						<thead>
							<tr style='background-color:#ccc;'>
								<th><?php echo __("Location", "Location"); ?></th>
								<th><?php echo __("Customer", "Customer"); ?></th>
								<th><?php echo __("Bag", "Bag"); ?></th>
								<th><?php echo __("LBS", "LBS"); ?></th>
								<th><?php echo __("R", "R"); ?></th>
								<th><?php echo __("monetary", "$"); ?></th>
								<th></th>
							</tr>
						</thead>
						<? foreach($wforders as $w):?>
						<tr>
							<td><?= $w['address']?></td>
							<td><?= $w['customer']?></td>
							<td><?= $w['bagNumber']?></td>
							<td><?= $w['qty']?></td>
							<td><?= $w['route_id']?></td>
							<td><?= $w['status']?></td>
							<td><?= $w['loaded']?></td>
						</tr>
						<? endforeach;?>
					</table>
					
					<h3><?php echo __("Dry Clean", "Dry Clean"); ?></h3>
					<table>
						<thead>
							<tr style='background-color:#ccc;'>
								<th><?php echo __("Location", "Location"); ?></th>
								<th><?php echo __("Customer", "Customer"); ?></th>
								<th><?php echo __("Bag", "Bag"); ?></th>
							
								<th><?php echo __("R", "R"); ?></th>
								<th><?php echo __("monetary", "$"); ?></th>
								<th></th>
							</tr>
						</thead>
						<? foreach($dcorders as $w):?>
						<tr>
							<td><?= $w['address']?></td>
							<td><?= $w['customer']?></td>
							<td><?= $w['bagNumber']?></td>
							<td><?= $w['route_id']?></td>
							<td><?= $w['status']?></td>
							<td><?= $w['loaded']?></td>
						</tr>
						<? endforeach;?>
					</table>
					
					<? endif; ?>
				</div>
			</div>
	</body>
</html>

	
	
