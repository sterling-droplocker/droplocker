<?php enter_translation_domain("admin/reports/sales_tax"); ?>
<h2><?php echo __("Sales Tax Details", "Sales Tax Details"); ?></h2>

<form method="get" action="/admin/reports/sales_tax">
    <label>From: <input type="text" name="from" value="<?= $from ?>" class="datefield"></label>&nbsp;&nbsp;
    <label>To: <input type="text" name="to" value="<?= $to ?>" class="datefield"></label>

    <input type="submit" class="button orange" name="export" value="<?php echo __("Update", "Update"); ?>">
</form>

<br><br>

<h3><?php echo __("Taxes from-to", "Taxes from %from% to %to%", array("from"=>$from, "to"=>$to)); ?></h3>

<table class="box-table-a">
<thead>
<tr>
    <th><?php echo __("Month", "Month"); ?></th>
    <? foreach($taxes as $month => $tax): ?>
    <th align="right" style="white-space: pre;"><?= $tax->month ?></th>
    <? endforeach; ?>
</tr>
</thead>
<tbody>
<tr>
    <td><?php echo __("Gross", "Gross"); ?></td>
    <? foreach($taxes as $month => $tax): ?>
    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $tax->gross) ?></td>
    <? endforeach; ?>
</tr>
<tr>
    <td><?php echo __("Discounts", "Discounts"); ?></td>
    <? foreach($taxes as $month => $tax): ?>
    <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $tax->disc) ?></td>
    <? endforeach; ?>
</tr>
</tbody>
<tfoot>
    <tr>
        <th><?php echo __("Tax", "Tax"); ?></th>
        <? foreach($taxes as $month => $tax): ?>
        <th align="right"><a href="/admin/reports/sales_tax_details/<?= $tax->month ?>" target="_blank"><?= format_money_symbol($this->business->businessID, '%.2n', $tax->tax) ?></a></th>
        <? endforeach; ?>
    </tr>
    <? foreach($rates as $taxGroupID => $rate):
        $taxName = 'Tax';
        if (!empty($taxGroups[$taxGroupID])) {
            $taxName = $taxGroups[$taxGroupID]->name;
        }
    ?>
    <tr>
        <td><?= $taxName ?> <?= sprintf('%.2f%%', 100 * $rate); ?></td>

        <? foreach($taxes as $month => $tax): ?>
        <td align="right">
            <? if (isset($subTaxes[$month][$taxGroupID])): ?>
                <?= format_money_symbol($this->business->businessID, '%.2n', $subTaxes[$month][$taxGroupID]->taxAmount); ?>
            <? else: ?>
                -
            <? endif; ?>
        </td>
        <? endforeach ?>
    </tr>
    <? endforeach; ?>
</tfoot>
</table>


<script type="text/javascript">

$(".datefield").datepicker({
	showOn: "button",
	buttonImage: "/images/icons/calendar.png",
	buttonImageOnly: true,
	dateFormat: "yy-mm-dd"
});

</script>
