<?php enter_translation_domain("admin/reports/manage_machines"); ?>
<h2><?php echo __("Manage Machines", "Manage Machines"); ?></h2>

<table id="box-table-a">
    <tr>
        <th><?php echo __("Machine", "Machine"); ?></th>
        <th><?php echo __("Cycle Time", "Cycle Time"); ?></th>
        <th></th>
    </tr>
    <tr>
    <form action="add_machine" method="post">
        <td><input type="text" name="name" size="30"></td>
        <td><input type="text" name="cycleTime" size="5"> <?php echo __("Minutes", "Minutes"); ?></td>
        <td><input type="submit" name="add" value="<?php echo __("add", "add"); ?>"></td>
    </form>
    </tr>
    <? foreach($machines as $machine): ?>
    <tr>
        <td><?= $machine->name?></td>
        <td><?= $machine->cycleTime?> minutes</td>
        <td><a href="/admin/reports/delete_machine?machine=<?= $machine->machineID?>"><img src='/images/icons/delete.png' /></a></td>
    </tr>
    <? endforeach; ?>
</table>