<?php enter_translation_domain('reports/by_location'); ?>
<script type="text/javascript">
    $(document).ready(function () {


        $.fn.dataTableExt.afnSortData['attr'] = function (oSettings, iColumn)
        {
            return $.map(oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
                var value = $('td:eq(' + iColumn + ')', tr).attr('data-value');
                return value;
            });
        }

        $("#details").dataTable(
                {
                "aaSorting": [],
                        "bPaginate": false,
                        "bJQueryUI": true,
                        "sDom": '<"H"lTfr>t<"F"ip>',
                        "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null <? foreach($headers as $h) echo ",\n{\"sSortDataType\": \"attr\", \"sType\": \"numeric\"}" ?>
                        ],
                        "oTableTools": {
<?php if (in_bizzie_mode() && !is_superadmin()): ?>
                            aButtons : [
                                    "pdf", "print"
                            ],
<?php endif ?>
                        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                        }
                }
        );
    });
</script>



<h2><?= __("sales_by_location", "Sales By Location"); ?></h2>

<?=$select?>

<table id="box-table-a">
    <tr>
        <th><?= __("totals", "Totals"); ?></th>
        <? foreach($headers as $h): ?>
        <th align="right"><?= $h ?></th>
        <? endforeach; ?>
    </tr>
    <? foreach($reportingClass as $key => $value): ?>
    <tr>
        <td><?= $key ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <?php $value[$i] = empty($value[$i]) ? 0 : $value[$i]; ?>
            <div><?= format_money_symbol($this->business->businessID, '%.2n', $value[$i]) ?></div>
            <? if ($show_extra): ?>
            <div style="margin-top: 1em;">
                <b><?= __("unique_customers", "Unique Customers:"); ?></b> <?= (int) $custCountCategory[$key][$i] ?>
                <br> <b><?= __("first_time", "First Time:"); ?></b> <?= (int) $custFirstCategory[$key][$i] ?>
                <?php $lostCustCategory[$key][$i] = $lostCustCategory[$key][$i] == 'N/A' ? 'N/A' : (int) $retentionRateCategory[$key][$i] ?>
                <br> <b><?= __("lost_customers", "Lost Customers:"); ?></b> <?= $lostCustCategory[$key][$i] ?>
                <?php $retentionRateCategory[$key][$i] = $retentionRateCategory[$key][$i] == 'N/A' ? 'N/A' : number_format($retentionRateCategory[$key][$i] * 100, 0) . '%'; ?>
                <br> <b><?= __("retention_rate", "Retention Rate:"); ?></b> <?= $retentionRateCategory[$key][$i] ?>
                <?php $countNumOrders[$key][$i] = empty($countNumOrders[$key][$i]) ? 0 : $countNumOrders[$key][$i]; ?>
                <br> <b><?= __("total_orders", "Total Orders:"); ?></b> <?= (int) $countNumOrders[$key][$i] ?>
            </div>
            <? endif; ?>
        </td>
        <? endfor; ?>
    </tr>
    <? endforeach; ?>
    <tr>
        <td style="border-top: 2px solid;"><strong><?= __("total", "TOTAL"); ?></strong></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td style="border-top: 2px solid;" align="right">
            <?= format_money_symbol($this->business->businessID, '%.2n', $gross[$i]) ?>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("discounts", "Discounts"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <a href="/admin/reports/discount_details/<?= date("Y-n", mktime(0, 0, 0, date("m") - $i, 1, date("Y"))) ?>" target="_blank"><?= format_money_symbol($this->business->businessID, '%.2n', $discounts[$i]) ?>  </a>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <th><strong><?= __("total_w_discount", "TOTAL W/Discount"); ?></strong></th>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <th align="right">
            <?= format_money_symbol($this->business->businessID, '%.2n', $net[$i]) ?>
        </th>
        <? endfor; ?>
    </tr>
    <tr>
        <td><strong><?= __("sales_tax", "Sales Tax"); ?></strong></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <a href="/admin/reports/sales_tax_details/<?= date("Y-n", mktime(0, 0, 0, date("m") - $i, 1, date("Y"))) ?>" target="_blank">See Report</a>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("signups", "Signups"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <a href="/admin/reports/signupDetails?month=<?= date("Y-n", mktime(0, 0, 0, date("m") - $i, 1, date("Y"))) ?>" target="_blank"><?= $signups[$i] ?></a>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("unique_customers", "Unique Customers"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <a href="/admin/reports/uniqueDetails?month=<?= date("Y-n", mktime(0, 0, 0, date("m") - $i, 1, date("Y"))) ?>" target="_blank"><?= $uniqueCusts[$i] ?></a>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("1st_time_customers", "1st Time Customers"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <a href="/admin/reports/firstTimeDetails?month=<?= date("Y-n", mktime(0, 0, 0, date("m") - $i, 1, date("Y"))) ?>" target="_blank"><?= $firstCustomers[$i] ?></a>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("lost_customers", "Lost Customers"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <?= $lostCusts[$i] ?>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("number_of_orders", "Number of Orders"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <?= empty($numOrders[$i]) ? 0 : $numOrders[$i]; ?>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("retention_rate", "Retention Rate"); ?></td>
        <? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <?php $retentionRate[$i] = $retentionRate[$i] == 'N/A' ? 'N/A' : number_format($retentionRate[$i] * 100, 0) . '%'; ?>
            <?= $retentionRate[$i] ?>
        </td>
        <? endfor; ?>
    </tr>
    <tr>
        <td><?= __("lifetime_rev_per_cust", "Lifetime Rev Per Cust"); ?></td><? for ($i = 0; $i <= $numMonths -1; $i = $i + $increment): ?>
        <td align="right">
            <?php $lifetime[$i] = empty($lifetime[$i]) ? 0 : $lifetime[$i]; ?>
            <?= format_money_symbol($this->business->businessID, '%.2n', $lifetime[$i]) ?>
        </td>
        <? endfor; ?>
    </tr>
</table>
<br>
<br>
<?=$by_location?>
