<?php enter_translation_domain("admin/reports/employee_productivity"); ?>
<h2><?php echo __("Employee Productivity", "Employee Productivity"); ?></h2>
<br>
<br>
<form action="/admin/reports/employee_productivity" method="post">
<select name="daysToShow">

<? for ($i = 1; $i <= 180; $i ++): ?> 	
	<option value="<?= $i ?>"
	<? if($_POST['daysToShow'] == $i) { echo ' SELECTED'; } ?>
	><?= $i ?> <?php echo __("days", "days"); ?></option>
<? endfor; ?>
</select>
<select name="role">
<option value="%"><?php echo __("All Positions", "All Positions"); ?></option>
<? foreach ($roles as $r)
{ 	
	echo '<option value="'.$r->name.'"';
	if(strtolower($_POST['role']) == strtolower($r->name)) { echo ' SELECTED'; }
	echo '>'.$r->name.'</option>'; 
} 
?>
</select>
<input type="submit" name="Go" value="Go">
</form>
<br><br>
<table id="box-table-a">
<tr>
	<th><?php echo __("Day", "Day"); ?></th>
	<th colspan="2" align="center"><?php echo __("Dry Cleaning", "Dry Cleaning"); ?></th>
	<? foreach ($selectedRole as $r): ?>
		<th><?= $r->name ?></th>
	<? endforeach; ?>
    <th><?php echo __("Overall", "Overall"); ?></th>
</tr>
<? for ($day = 0; $day <= $_POST['daysToShow']; $day++): 
    $thisDate = date("Y-m-d",  strtotime($firstDay . "- $day ".__("days", "days").""));
    $dayTotal = 0;
?>
<tr>
	<td><?= date("D m/d/y",  strtotime($thisDate)) ?></td>
	<td>
        <? foreach($qtyByService[$thisDate] as $key => $value): ?>
            <?= $key ?>: <?= $value ?><br>
            <? $dayTotal += $value ?>
            <? $sumTotal += $value ?>
            <? $sum[$key] += $value ?>
        <? endforeach; ?>
    </td>
	<td><?php echo __("Total", "Total:"); ?> <?= $dayTotal ?></strong></td>
	<? foreach ($selectedRole as $r): ?>
		<td><? if($empHours[$thisDate][strtolower($r->name)]['dayReg']): ?>
            <a href="/admin/reports/pcs_per_hour?day=<?= $thisDate ?>&role=<?= $r->name ?>" target="_blank"><?= $empHours[$thisDate][strtolower($r->name)]['dayReg'] ?> / <?= $empHours[$thisDate][strtolower($r->name)]['dayOt'] ?><br>( <?= number_format_locale($dayTotal / ($empHours[$thisDate][strtolower($r->name)]['dayReg'] + $empHours[$thisDate][strtolower($r->name)]['dayOt']), 2) ?> <?php echo __("pph", "pph"); ?>)</a>
            <? 
                $sumRoleReg[$r->name] += $empHours[$thisDate][strtolower($r->name)]['dayReg'];
                $sumRoleOT[$r->name] += $empHours[$thisDate][strtolower($r->name)]['dayOt'];
                $dayReg[$day] += $empHours[$thisDate][strtolower($r->name)]['dayReg'];
                $dayOT[$day] += $empHours[$thisDate][strtolower($r->name)]['dayOt'];
                endif; 
            ?>
        </td>
	<? endforeach; ?>
    <td><? if($dayTotal > 0): ?><?= number_format_locale($dayTotal / ($dayReg[$day] + $dayOT[$day]),2)  ?> <?php echo __("pph", "pph"); ?><? endif; ?></td>
</tr>
<? endfor; ?>
<tr>
    <th><?php echo __("Total", "Total"); ?></th>
    <th><? foreach($sum as $key => $value): ?>
            <?= $key ?>: <?= $value ?><br>
        <? endforeach; ?>
    </th>
    <th><?php echo __("Total", "Total:"); ?> <?= $sumTotal ?></th>
    <? foreach ($selectedRole as $r): ?>
		<th>
            <? if($sumRoleReg[$r->name] > 0): ?>
            <?= $sumRoleReg[$r->name] ?> / <?= $sumRoleOT[$r->name] ?><br>( <?= number_format_locale($sumTotal / ($sumRoleReg[$r->name] + $sumRoleOT[$r->name]),2)  ?> <?php echo __("pph", "pph"); ?>)
            <? $timeHrs += $sumRoleReg[$r->name] + $sumRoleOT[$r->name] ?>
            <? endif; ?>
        </th>
    <? endforeach; ?>
    <th><?= number_format_locale($sumTotal / $timeHrs,2)  ?> <?php echo __("pph", "pph"); ?></th>
</tr>
</table>
<? 
//echo $this->business->timezone;
//var_dump($sumRoleReg); 
?>

