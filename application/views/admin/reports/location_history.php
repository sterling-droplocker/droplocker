<?php enter_translation_domain("admin/reports/location_history"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2> <?php echo __("History for Location", "History for Location"); ?> <?= $location->address ?> </h2>

<table id="location_history">
    <thead>
        <tr>
            <th> <?php echo __("Name", "Name"); ?> </th>
            <th> <?php echo __("Email", "Email"); ?> </th>
            <th> <?php echo __("Address", "Address"); ?> </th>
            <th> <?php echo __("Default Location", "Default Location"); ?> </th>
            <th> <?php echo __("Total Spent", "Total Spent"); ?> </th>
            <th> <?php echo __("Total Discounts", "Total Discounts"); ?> </th>
            <th> <?php echo __("Last Order Date", "Last Order Date"); ?> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($customers as $key => $customer):
            $date = __("Unknown Date", "Unknown Date");
            $date_val = 0;
            if ($customer['last_order_date'] instanceOf \DateTime)  {
                $date = $customer['last_order_date']->format(SHORT_DATE_FORMAT);
                $date = convert_from_gmt_aprax($date, $dateFormat);
                $date_val = $customer['last_order_date']->getTimestamp();
            }
        ?>
            <tr>
                <td> <a href="/admin/customers/detail/<?= $key ?>" target="_blank"><?= $customer['name'] ?></a> </td>
                <td> <?= $customer['email'] ?> </td>
                <td> <?= $customer['address'] ?> </td>
                <td>
                <?php if (!empty($customer['default_location'])): ?>
                        <a href="/admin/admin/location_details/<?=$customer['default_location']->locationID ?>" target="_blank"><?= $customer['default_location']->companyName ?></a>
                <?php endif; ?>
                </td>
                <td data-value="<?= $customer['total_spent'] ?>"> <?= format_money_symbol($this->business_id, '%.2n', $customer['total_spent']) ?> </td>
                <td data-value="<?= $customer['total_discounts'] ?>"> <?= format_money_symbol($this->business_id, '%.2n', $customer['total_discounts']) ?> </td>
                <td data-value="<?= $date_val ?>"> <?= $date ?> </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>


<script type="text/javascript">
$(document).ready( function() {

$.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
{
    return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
        return $('td:eq('+iColumn+')', tr).attr('data-value');
    });
}

$("#location_history").dataTable({
    "aaSorting": [],
    "bPaginate": false,
    "bJQueryUI": true,
    "sDom": '<"H"lTfr>t<"F"ip>',
    "aoColumns": [
        null,
        null,
        null,
        null,
        {"sSortDataType": "attr", "sType": "numeric"},
        {"sSortDataType": "attr", "sType": "numeric"},
        {"sSortDataType": "attr", "sType": "numeric"}
    ],
    "oTableTools": {
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
            aButtons : [
                "pdf", "print"
            ],
            <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
    }
});

});
</script>
