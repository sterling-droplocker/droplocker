<?php enter_translation_domain('admin/reports/reassigned_barcodes'); ?>
<h2><?php echo __("Reassigned Barcodes", "Reassigned Barcodes"); ?></h2>

<script type="text/javascript">
jQuery.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
 
        oSettings.oApi._fnReDraw(oSettings);
 
        // iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
 
    // draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};

jQuery(document).ready(function($){
    var table = $("#reassigned_barcodes").dataTable({  
        "iDisplayLength": 20, 
        "bProcessing" : true,
        "bServerSide" : true,
        "bStateSave" : true,
        "sAjaxSource" : "/admin/reports/reassigned_barcodes/",
        "fnPreDrawCallback": function( oSettings ) {
            oSettings._iDisplayLength = 20;
        },
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "start_date", "value": "<?php echo $start_date ?>" } );
          aoData.push( { "name": "end_date", "value": "<?php echo $end_date ?>" } );
        },
        "bJQueryUI": true,
        "sDom": 'tr<"F"ip>',
        "aoColumns": [
            { "mDataProp":"barcode", "asSorting": [ "asc", "desc" ] },
            { "mDataProp":"from_customer_name", "asSorting": [ "asc", "desc" ] },
            { "mDataProp":"to_customer_name", "asSorting": [ "asc", "desc" ] },
            { "mDataProp":"employee", "asSorting": [ "asc", "desc" ] },
            { "mDataProp":"order", "asSorting": [ "asc", "desc" ] },
            { "mDataProp":"picture", "asSorting": [] },
        ]
    });

    table.fnStandingRedraw();


	$(".datefield").datepicker({
		showOn: "button",
		buttonImage: "/images/icons/calendar.png",
		buttonImageOnly: true,
		dateFormat: "yy-mm-d"
	});
});
</script>
<div style="padding: 8px;">
	<form action="" method="get">
		<table>
			<tr>
				<th><?= __("Reassigned barcodes between","Reassigned barcodes between")?></th>
				<td>
					<input readonly="true" name="start_date" value="<?= $start_date ?>" class="datefield">
					<?= __("and","And")?> 
					<input readonly="true" name="end_date" value="<?= $end_date ?>" class="datefield">
				</td>
			</tr>
		</table>
		<input type="submit" name="Submit" value="<?php echo __("Run", "Run"); ?>" class="button blue">
	</form>
</div>
<br>

<table id='reassigned_barcodes'>
	<thead>
		<tr>
			<th><?= __("Barcode","Barcode")?></th>
			<th><?= __("Barcode Reassigned From","Barcode Reassigned From")?></th>
			<th><?= __("Barcode Reassigned To","Barcode Reassigned To")?></th>
			<th><?= __("Employee Name","Employee Name")?></th>
			<th style="width:15em"><?= __("Order","And")?></th>
			<th><?= __("Picture","Picture")?></th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
<?php leave_translation_domain(); ?>