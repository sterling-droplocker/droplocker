<?php enter_translation_domain("admin/reports/driver_notes"); ?>
<script type="text/javascript">
var business_id = '<?= $business_id?>';
$(document).ready(function(){
    var inactiveTable;

	$("#notesTabs").tabs(
	    {
	        fx: { opacity: 'toggle' },
            select: function(event, ui) {
	            if (ui.panel.id == "inactiveNotesTab" && inactiveTable === undefined) {
                    inactiveTable = $("#inactiveTable").dataTable(
                        {
                            "iDisplayLength": 25,
                            "bProcessing": true,
                            "sServerMethod": "GET",
                            "bJQueryUI": true,
                            "sDom": '<"H"lTfr>t<"F"ip>',
                            "sPaginationType": "full_numbers",
                            "aLengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                            "sAjaxSource": '/ajax/inactiveDriverNotesAjax/'+business_id,
                            "aaSorting":[[0,"desc"]],
                            "aoColumns": [null,null,null,null,
                                {

                                    "fnRender": function ( obj) {
                                        return '<a target="_blank" style="color:#3b7db9" href="/admin/reports/driver_note_detail/'+obj.aData[0]+'">'+obj.aData[4]+'</a>';
                                    }

                                },null,null,null

                            ],
                            "oTableTools": {
                                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                                aButtons : [
                                    "pdf", "print"
                                ],
                                <?php endif ?>
                                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                            }

                        }
                    );
                }
            }
        }
    );


	   
});
</script>

<h2><?php echo __("Driver Notes", "Driver Notes"); ?></h2>
<?= $this->session->flashdata("message") ?>


<div id="notesTabs">
    <ul> 
        <li> <a href="#activeNotesTab"> <?php echo __("Active Driver Notes", "Active Driver Notes"); ?> </a> </li>
        <li> <a href="#inactiveNotesTab"> <?php echo __("Inactive Driver Notes", "Inactive Driver Notes"); ?> </a> </lki>
    </ul>
    <div id='activeNotesTab'>
        <form action="" method="post">
            <table class='detail_table'>
                <tr>
                    <th><?php echo __("Location", "Location"); ?></th>
                    <td>
                        <?php echo validation_errors("<div style='color:#cc0000;padding:5px;'>", "</div>"); ?>
                        <?= form_dropdown('location', $locations, null, 'id="location"') ?>
                    </td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td><textarea style='width:98%; height:50px' name="note"><?= $this->input->post('note'); ?></textarea></td>
                </tr>
                <tr>
                    <td colspan='2'><input type="submit" name="create" value="<?php echo __("Create", "Create"); ?>" class="button blue"></td>
                </tr>
            </table>
            </form>
            
            <table class='box-table-a'>
            	<thead>
            		<tr>
            			<th></th>
            			<th><?php echo __("Route", "Route"); ?></th>
                        <th><?php echo __("Title", "Title"); ?></th>
            			<th><?php echo __("Address", "Address"); ?></th>
            			<th><?php echo __("Note", "Note"); ?></th>
            			<th><?php echo __("Created By", "Created By"); ?></th>
            			<th><?php echo __("Date", "Date"); ?></th>
            			<th><?php echo __("Response", "Response"); ?></th>
            			<th><?php echo __("View", "View"); ?></th>
            		</tr>
            	</thead>
            	<tbody>
            		<? if($activeNotes): ?>
            			<? foreach($activeNotes as $n):?>
            			<tr>
            				<td><?php echo __("print", "print"); ?></td>
            				<td><?= $n->route_id ?></td>
                            <td><?= $n->companyName ?></td>
            				<td><?= $n->locationAddress ?></td>
            				<td><?= $n->note ?></td>
            				<td><?= get_employee_name($n->employeeID) ?></td>
            				<td><?= convert_from_gmt_aprax($n->created, 'M j, Y') ?></td>
            				<td align='center'>
            				    <?php if($n->driverResponseCount): ?>
            				    <a href='/admin/reports/driver_note_detail/<?php echo $n->notesId?>'>(<?= $n->driverResponseCount?>) View</a>
            				    <?php endif; ?>
            				</td>
            				<td align='center'> <a href='/admin/reports/driver_note_detail/<?php echo $n->notesId?>'><img src='/images/icons/magnifier.png'></a></td>
            				</td>
            			</tr>
            			<? endforeach; ?>
            		<? else: ?>
            			<tr>
            				<td colspan=7><?php echo __("No notes found", "No notes found"); ?></td>
            			</tr>
            		<? endif; ?>
            		
            	</tbody>
            </table>
    </div>
    <div id='inactiveNotesTab'>
       
            <table id='inactiveTable'>
            	<thead>
            		<tr>
            			<th><?php echo __("ID", "ID"); ?></th>
            			<th><?php echo __("RouteID", "RouteID"); ?></th>
            			<th><?php echo __("Address", "Address"); ?></th>
                        <th><?php echo __("Title", "Title"); ?></th>
            			<th><?php echo __("Note", "Note"); ?></th>
            			<th align='center'><?php echo __("Created By", "Created By"); ?></th>
            			<th><?php echo __("Date", "Date"); ?></th>
            			<th width='25'><?php echo __("Response", "Response"); ?></th>
            			
            		</tr>
            	</thead>
            	<tbody>
            		
            		
            	</tbody>
            	<tfoot></tfoot>
            </table>
      
    </div>
</div>





