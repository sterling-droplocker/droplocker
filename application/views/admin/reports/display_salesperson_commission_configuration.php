<?php enter_translation_domain("admin/reports/display_salesperson_commission_configuration"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $(".edit_field").editable("/admin/customers/update", {
        width : "100%",
        height : "100%",
        id : "customerID",
        cancel : "<img src='/images/icons/delete.png' />",
        submit : "<img src='/images/icons/accept.png' />",
        indicator : "<?php echo __("Saving", "Saving ..."); ?> <img src='/images/progress.gif' />",
        data    : function(string) {
            return $.trim(string)
        },
        submitdata: function ( value, settings ) {
            return {
                    "property" : $(this).attr("data-property")
            }
        },
        callback : function(value, settings) {
            var response = $.parseJSON(value);
            if (response.status !== "success") {
                alert(response.message);
            }
        }
    });  
    $("#new_named_customer").autocomplete({
        source : "/ajax/customer_autocomplete_aprax",
        select: function (event, customer) {
            $("input[name='customerID']").val(customer.item.id);
        }

    }); 
});
</script>
<style type="text/css">
    fieldset {
        border: 1px solid;
        margin: 5px;
    }
    input {
        margin: 3px;
    }
    legend {
        font-weight: bold;
    }
</style>
<?php use App\Libraries\DroplockerObjects\Commission_Orders_Setting ?>

<h2> <?= getPersonName($salesperson) ?> <?php echo __("Sales Commission Configuration", "Sales Commission Configuration"); ?> </h2>


<div style="display: table;">

    <div style="display: table-cell">

        <?= form_open("/admin/employees/add_named_customer") ?>
            <fieldset> 
                <legend> <?php echo __("Named Accounts", "Named Accounts"); ?></legend>
                <?= form_label(__("Customer", "Customer"), "new_named_customer") ?> <?= form_input(array("id" => "new_named_customer")) ?>
                <br />
                 <?= form_label(__("Commission", "Commission"), "commission_rate") ?> <?= form_input(array(
                                                                          'name'        => 'commission_rate',
                                                                          'id'          => 'commission_rate',
                                                                          'value'       => '0',
                                                                          'size'        => '5')) ?> %
                <br />
                <?= form_hidden("customerID") ?>
                <?= form_hidden("employeeID", $salesperson->employeeID) ?>

                <?= form_submit(array("value" => __("Add Account", "Add Account"), "class" => "button orange")) ?>
            
        <?= form_close() ?>
          
        <br><br>
        <?php if (empty($salesperson->relationships['Named_Accounts'])): ?>
            <b> <?php echo __("There are no named accounts associated with this employee", "There are no named accounts associated with this employee"); ?></b>
            <?php else: ?>
                <table id='hor-minimalist-a'>
                    <thead> 
                        <tr> 
                            <th>
                               <strong> <?php echo __("Named Account", "Named Account"); ?> </strong>
                            </th>
                            <th> <strong><?php echo __("Commission", "Commission"); ?></strong> </th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php foreach ($salesperson->relationships['Named_Accounts'] as $named_account): ?>
                            <tr>
                                <td> <?= getPersonName($named_account) ?> </td>
                                <td class="edit_field" data-property="commission_rate" id="<?= $named_account->customerID?>"> <?= $named_account->commission_rate ?> </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
            </fieldset>
    </div>
    <div style="display: table-cell; margin-left: 10px 5px;">
        <fieldset>
        <legend> <?php echo __("Per Order Commissions", "Per Order Commissions"); ?></legend>
        <?= form_open("/admin/reports/update_salesperson_commission_settings") ?>
            <?= form_hidden("employeeID", $salesperson->employeeID) ?>
                <?php
                    for ($index = 0; $index < 10; $index++)
                    {
                        echo "<div>";
                            $number = $index + 1;
                            echo form_label("Order $number");
                            $commission_order_settings = \App\Libraries\DroplockerObjects\Commission_Orders_Setting::search_aprax(array("employee_id" => $salesperson->employeeID, "index" => $index));
                            $localeArray = localeconv();
                            if (!empty($commission_order_settings))
                            {
                                echo "   " . $localeArray['currency_symbol'] . form_input("orders[$x]", number_format_locale($commission_order_settings[0]->amount, 2));
                            }
                            else
                            {
                                echo "   " . $localeArray['currency_symbol'] . form_input("orders[$x]", number_format_locale(0.00, 2));
                            }
                        echo "</div>";
                    }
                ?>
                <?= form_submit(array("value" =>__("Update", "Update"), "class" => "button orange")) ?>
            </fieldset>
        <?= form_close() ?>
    </div>
</div>