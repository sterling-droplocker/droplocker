<?php enter_translation_domain("admin/reports/sales_commission"); ?>
<script type="text/javascript">
$(document).ready(function() {

    $("#configure_salesperson_commission").click(function()
    {
        window.location.href= "/admin/reports/display_salesperson_commission_configuration?employeeID=" + $("select[name='employee']").val();
    });

    $(".date_field").datepicker({
        dateFormat: "mm/dd/y"
    });

    today = new Date();

    if (!$("#start_date").val())
    {
        $("#start_date").datepicker("setDate", start_of_last_month);
         start_of_last_month = new Date(today.getFullYear(), today.getMonth()-1, 1);
    }

    if (!$("#end_date").val())
    {
        end_of_last_month = new Date(today.getFullYear(), today.getMonth(), 0);
        $("#end_date").datepicker("setDate", end_of_last_month);
    }

});
</script>
<h2><?php echo __("Salesperson Commission", "Salesperson Commission"); ?></h2>
<br>
<br>

<div style="float:left;"><form action="" method="post">
<?php echo __("From", "From:"); ?> <input type="text" name="fromDate" class="date_field" id="start_date" value="<?= $fromDate ?>">
<?php echo __("To", "To:"); ?> <input type="text" name="toDate" class="date_field" id="end_date" value="<?= $toDate ?>">
<?php echo __("For", "For:"); ?> <select name="employee">
<? foreach($employees as $e): ?>
<option value="<?= $e->employeeID?>" SELECTED><?= getPersonName($e) ?></option>
<? endforeach; ?>
	</select>
	<input type="submit" class="button orange" value="<?php echo __("Run", "Run"); ?>"></form></div><div style="float:right;"><input type="button" id="configure_salesperson_commission" class="button blue" value="<?php echo __("Configure Salesperson Commission", "Configure Salesperson Commission"); ?>" /></div>

<?if($_POST): ?>
<br>
<h3><?php echo __("Commission Report for", "Commission Report for"); ?> <?= getPersonName($thisEmployee) ?> (<?=$fromDate.' to '.$toDate ?>)</h3><br>


<h3><?php echo __("Named Accounts", "Named Accounts:"); ?></h3><br>
<table id='hor-minimalist-a'>
<?foreach($namedAccount as $n): ?>
<tr><td><strong><?= getPersonName($n) ?></strong></td></tr>
	<?if($orders[$n->customerID]): foreach($orders[$n->customerID] as $o): ?>
		<tr><td></td><td><a href="/admin/orders/details/<?= $o->orderID ?>" target="_blank"><?= $o->orderID ?></a></td><td><?= date("m/d/y", strtotime($o->dateCreated)) ?></td><td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $o->closedGross + $o->closedDisc) ?></td></tr>
	<? endforeach; endif;?>
	<tr><td colspan="3"></td><td align="right">$<?= $custTot[$n->customerID] ?></td><td><?= $n->commission_rate ?>%</td><td><?= format_money_symbol($this->business->businessID, '%.2n', $commission_total[$n->customerID]) ?></td></tr>
<? endforeach; ?>
<tfoot>
<tr><td colspan="5"><?php echo __("Total Commission", "Total Commission:"); ?></td><td><strong><?=format_money_symbol($this->business->businessID, '%.2n', $commTot) ?></strong></td></tr>
</tfoot>
</table>

<br>
<h3><?php echo __("First Orders", "First Orders:"); ?></h3><br>
<table id='hor-minimalist-a'>
<tr>
	<th><?php echo __("Customer", "Customer"); ?></th>
	<th><?php echo __("Order Date", "Order Date"); ?></th>
	<th><?php echo __("Order", "Order"); ?></th>
	<th><?php echo __("Order Number", "Order #"); ?></th>
    <th align="right"> <?php echo __("Commission", "Commission"); ?> </th>
	<th><?php echo __("Referred By", "Referred By"); ?></th>
    <th><?php echo __("Order Location", "Order Location"); ?></th>
</tr>
<? foreach($allOrders as $a): ?>
	<tr	<? if ($a->referredBy) echo ' bgcolor="#FFFF00"' ?>>

            <td><?= getPersonName($a, TRUE, 'cFirstName', 'cLastName') ?></td>
		<td><?= local_date($this->business->timezone, $a->dateCreated, 'm/d/y') ?></td>
		<td><?= $a->orderID ?></td>
		<td><?= $ordCount[$a->orderID] ?></td>
                <td align="right"><?= format_money_symbol($this->business->businessID, '%.2n', $commissionAmt[$a->orderID]) ?></td>
                <td><?= getPersonName($a, TRUE, 'rCustFirstName', 'rCustLastName') ?></td>
        <td><?= $a->lAddress ?></td>
	</tr>
<? endforeach; ?>
<tr><td colspan=4><strong><?php echo __("Total Commission", "Total Commission"); ?></strong></td><td align="right"><strong><?= format_money_symbol($this->business->businessID, '%.2n', $commission)?></strong></td></tr>
</table>
<?endif; ?>

