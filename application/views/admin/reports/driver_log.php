<?php enter_translation_domain("admin/reports/driver_log"); ?>
<style type="text/css">
	img.ui-datepicker-trigger {
		vertical-align: middle;
	}
</style>
<h2><?php echo __("Driver Log", "Driver Log"); ?></h2>

<div style="padding: 8px;">
<form action="""" method="post">
<?php echo __("Select Driver", "Select Driver"); ?> 
<select name="driver">
<? foreach($drivers as $driver): ?>
	<option value="<?= $driver->employeeID ?>"
	<? if ($this->input->get_post('driver') == $driver->employeeID) echo ' SELECTED'; ?>
        ><?= getPersonName($driver) ?></option>
<? endforeach; ?>
</select>
<input type="text" name="logDate" id="logDate" size="10" readonly="true" />
<input type="submit" name="Submit" value="<?php echo __("Run", "Run"); ?>" class="button blue">
</form>
</div>



<table id='box-table-a'>
	<thead>
		<tr>
			<th><?php echo __("Driver", "Driver"); ?></th>
			<th><?php echo __("Time", "Time"); ?></th>
			<th><?php echo __("Location", "Location"); ?></th>
			<th><?php echo __("Order", "Order"); ?></th>
			<th><?php echo __("Locker", "Locker"); ?></th>
			<th><?php echo __("Customer", "Customer"); ?></th>
			<th><?php echo __("Action", "Action"); ?></th>
			<th><?php echo __("Elapsed", "Elapsed"); ?></th>
		</tr>
	</thead>
	<tbody>
		<? if($transactions): ?>
            <? 
            	if (!in_array($transactions[0]->address, array("In", "Out"))) { 
            		$driveTime = $lastStop = strtotime($transactions[0]->delTime); 
            	} else {
            		$driveTime = $lastStop = strtotime($transactions[1]->delTime);
            	}
            ?>                
			<? 
				foreach($transactions as $t): 
					$clockInOut = false;
					if (in_array($t->address, array("In", "Out"))) $clockInOut = true; 		
					if (!$clockInOut) $counter++;
			?>
			<tr>
                            <td><?= getPersonName($t) ?></td>
				<td><?= convert_from_gmt_aprax($t->delTime, "m/d/y g:ia") ?></td>
				<td><?= $t->address ?></td>
				<?php if ($clockInOut): ?>
					<td colspan="5"></td>
				<?php else: ?>
					<td><a href="/admin/orders/details/<?= $t->order_id ?>" target="blank"><?= $t->order_id ?></a></td>
					<td><?= $t->lockerName ?></td>
					<td><?= $customers[$t->order_id] ?></td>
					<td><?= $t->delType ?></td>
					<?
						$driveTime = strtotime($t->delTime) - $lastStop;
						$lastStop = strtotime($t->delTime);
		                $min = number_format_locale($driveTime / 60, 0);
						if ($min > 15) { 
							echo '<td style="background: Yellow;">'. $min.' min</td>'; 
						} else {
							echo '<td nowrap>'.$min.' min</td>'; 
						}
					?>
				<?php endif; ?>
			</tr>
			<? endforeach; ?>
		<? else: ?>
			<tr>
				<td colspan=8><?php echo __("No transactions found", "No transactions found"); ?></td>
			</tr>
		<? endif; ?>
		
	</tbody>
</table>

<? if($transactions): ?>
	<br><br>
	<table id='box-table-a' style="width:250px;">
    <tr><th></th><th></th></tr>
	<tfoot>
		<tr><td><strong><?php echo __("Stops", "Stops"); ?></strong></td><td align="right"><?= $driverMetrics['stopCounter'] ?></td></tr>
		<tr><td><strong><?php echo __("Pickups", "Pickups"); ?></strong></td><td align="right"><?= $driverMetrics['pickupCounter'] ?></td></tr>
		<tr><td><strong><?php echo __("Deliveries", "Deliveries"); ?></strong></td><td align="right"><?= $driverMetrics['deliveryCounter'] ?></td></tr>
		<tr><td><strong><?php echo __("Time", "Time"); ?></strong></td><td align="right"><?= round($driverMetrics['totalTime'],2) ?> <?php echo __("hrs", "hrs"); ?></td></tr>
		<tr><td><strong><?php echo __("Stops Per Hr", "Stops Per Hr"); ?></strong></td><td align="right"><?= round($driverMetrics['stopsPerHour'],2) ?></td></tr>
		<tr><td><strong><?php echo __("Transactions Per Hr", "Transactions Per Hr"); ?></strong></td><td align="right"><?= round($driverMetrics['tranPerHour'],2) ?></td></tr>
	</tfoot>
	</table>
<? endif; ?>

<script type="text/javascript">
jQuery(document).ready(function($){
	$("#logDate").datepicker({
			dateFormat: "yy-mm-dd",
		    changeMonth: true,
		    changeYear: true,
			showOn: "both",
			buttonImage: "/images/icons/date.png",
			buttonImageOnly: true,
			buttonText: "<?php echo __("Select date", "Select date"); ?>"
		});

	<?php if ($logDate): ?>
	$('#logDate').datepicker("setDate", '<?php echo $logDate ?>' );
	<?php endif; ?>
});
</script>
