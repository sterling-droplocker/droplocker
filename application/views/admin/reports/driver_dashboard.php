<?php enter_translation_domain("admin/reports/driver_dashboard"); ?>
<h2><?php echo __("Driver Dashboard", "Driver Dashboard"); ?></h2>
<div style="padding: 8px;">
<form action="""" method="post">
<form action="">
<!-- <select name="logDate">
	<?
	for ($i = 0; $i < 180; $i++)
	{
		$currentDay = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")- $i, date("Y")));
		echo '<option value="'.$currentDay.'"';
		if ($this->input->get_post('logDate') == $currentDay) { echo ' selected'; }
		echo '>'.date("m/d/y - D", strtotime($currentDay)).'</option>';
	}
	?>
</select> -->
<select name="driver">
    <option value="%"><?php echo __("All Drivers", "All Drivers"); ?></option>
	<?
	foreach ($drivers as $driver)
	{
		echo '<option value="'.$driver->employeeID.'"';
		if ($this->input->get_post('driver') == $driver->employeeID) { echo ' selected'; }
		echo '>'. getPersonName($driver) .'</option>';
	}
	?>
</select>
<input type="submit" name="Submit" value="<?php echo __("Run", "Run"); ?>" class="button blue">
</form>
</div>

<table class="none" border=1>
	<thead>
		<tr>
			<th><?php echo __("Driver", "Driver"); ?></th>
			<th><?php echo __("Route", "Route"); ?></th>
			<th><?php echo __("Stops", "Stops"); ?></th>
			<th><?php echo __("Un-Inventoried Bags", "Un-Inventoried Bags"); ?></th>
			<th><?php echo __("Pickups", "Pickups"); ?></th>
			<th><?php echo __("Deliveries", "Deliveries"); ?></th>
                        
                        
                        <!-- Claimed orders, name should be hyperlinked -->
                        
                        <!-- claimed orders orders table -->
                        <th> <?php echo __("To be picked up", "To be picked up"); ?> </th>
                        
                        <!-- Address, bag # and customer name should be hyperlinked -->
                        <th> <?php echo __("To be delivered", "To be delivered"); ?></th>

		</tr>
	</thead>
	<tbody>
		<? if($routes): ?>
			<? foreach($routes as $key => $route):?>
			<tr>
				<td valign="top"><?= $route['driver'] ?><br><br><a href="/admin/reports/driver_log?logDate=<?= $logDate ?>&driver=<?= $route['employee_id']?>" target="_blank"><?php echo __("View Driver Log", "View Driver<br>Log"); ?></a></td>
				<td valign="top"><?= $key ?></td>
				<td valign="top">
					<table border="0" width="300">
					<? foreach($route_stops[$key] as $route_stop): ?>
                    <? if($route_stop['activeClaim'] == '0' and empty($delTimes[$route_stop[2]]['qty'])): ?>
                    <? else: ?>
                    <tr>
                        <td><?= substr($route_stop[1], 0, 20)?></td>
                        <td><?= $delTimes[$route_stop[2]]['when']?> 
						    <? if($delTimes[$route_stop[2]]['qty']) 
                                { echo ' ('.$delTimes[$route_stop[2]]['qty'].')'; } 
                            ?>
                        </td>
                    </tr>
                    <? endif; ?>
                    <? endforeach; ?>
					</table>
				</td>
				<td valign="top">
					<table>
                        <? foreach($picked[$key] as $pickup): ?>
                        <tr><td><?= getPersonName($pickup).'<br>' ?></td></tr>
                        <? endforeach;?>
					</table>
				<td valign="top"><?= $route['pickups'] ?></td>
				<td valign="top"><?= $route['delivery'] ?></td>
				</td>
                                <!-- To be picked up -->
                                <td valign="top">
                                    <table border=1 class="none">
                                        <thead>
                                            <tr>
                                                <th valign="top"> <?php echo __("Name", "Name"); ?> </th>
                                                <th valign="top"> <?php echo __("Location", "Location"); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($claims[$key] as $claim): ?>
                                                <tr">
                                                    <td> <a href="/admin/customers/detail/<?=$claim->customerID?>" target="_blank">  <?= substr(getPersonName($claim), 0, 20) ?> </a></td> 
                                                    <td> <a href="/admin/orders/view_location_orders/<?= $to_be_delivery->locationID?>" target="_blank"> <?= substr($claim->address, 0, 20) ?> </a> </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </td>
                                <!-- To be delivered -->
                                <td valign="top">
                                    <table border=1>
                                        <thead>
                                            <tr>
                                                <th> <?php echo __("Name", "Name"); ?> </th>
                                                <th> <?php echo __("Bag Name", "Bag Name"); ?> </th>
                                                <th> <?php echo __("Location", "Location"); ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($to_be_deliveries[$key] as $to_be_delivery): ?>
                                                <tr>
                                                    <td> <a href="/admin/customers/detail/<?=$to_be_delivery->customerID?>" target="_blank"> <?= substr(getPersonName($to_be_delivery), 0, 20) ?> </a> </td>
                                                    <td> <a href="/admin/orders/details/<?= $to_be_delivery->orderID ?>" target="_blank"> <?= $to_be_delivery->bagNumber ?> </a> </td>
                                                    <td> <a href="/admin/orders/view_location_orders/<?= $to_be_delivery->locationID?>" target="_blank"> <?= substr($to_be_delivery->address, 0, 20) ?> </a> </td>
                                                </tr>
                                            <?php endforeach ?>    
                                        </tbody>
                                    </table>
                                </td>
			</tr>
			<? endforeach; ?>
		<? else: ?>
			<tr>
				<td colspan=8><?php echo __("No transactions found", "No transactions found"); ?></td>
			</tr>
		<? endif; ?>
		
	</tbody>
</table>

