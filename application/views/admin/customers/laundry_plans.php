<?php enter_translation_domain("admin/customers/laundry_plans"); ?>
<?php
/**
 * Note, this view expects the following content variables to be set
 *  title string
 *  business stdClass This object must contains the properties for the current business
 *  businessLaundryPlans array An array that contains any number of stdClasses that contain the properties for a business laundry plan
 *  bizzie_mode bool
 *  superadmin_master_mode bool
 */
$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
?>
<script type="text/javascript">
$(document).ready(function(){
    $("#add_plan_button").click(function(){
        $('#plan_form').toggle('');
        if($('#add_plan_button').html()=="Cancel")
            $('#add_plan_button').html("<img src='/images/icons/add.png' /> <?php echo __("Add Laundry Plan", "Add Laundry Plan"); ?>");
        else
            $('#add_plan_button').html("Cancel");
    });

    $( "#datepicker" ).datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        altField: "#endDate",
        altFormat: "yy-mm-d",
        dateFormat: "MM d, yy"
    });
    
    $("#add_businessLaundryPlan_form").validate( {
        rules: {
           name : {
              required: true
           },
           price : {
              required: true,
              number: true
           },
           pounds : {
              required: true,
              digits: true
           }
        },
        errorClass : "invalid"
    });
});
</script>

<h2> <?= $title ?> </h2>
<p><a id='add_plan_button' href='javascript:;'><img src='/images/icons/add.png' /> <?php echo __("Add Laundry Plan", "Add Laundry Plan"); ?></a></p>

<?= $this->session->flashdata('message');?>

<div id='plan_form' style='display:none;'>
    <form id="add_businessLaundryPlan_form" action='' method='post'>
        <table class="detail_table">
            <tr>
                <th>*<?php echo __("Plan Name", "Plan Name"); ?></th>
                <td>
                    <input type='text' name='name' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Caption", "Caption"); ?></th>
                <td>
                    <input type='text' name='caption' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Group", "Group"); ?></th>
                <td>
                    <input type='text' name='grouping' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Description", "Description"); ?></th>
                <td>
                    <textarea name='description' style='width:98%;border:1px solid #ccc;' rows="2"></textarea>
                </td>
            </tr>
            <tr>
                <th>*<?php echo __("Price", "Price"); ?></th>
                <td>
                    <input type='text' name='price' style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= weight_system_format( '', true, true );?></th>
                <td>
                    <input type='text' name='pounds' style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Notes", "Notes"); ?></th>
                <td>
                    <input type='text' name='poundsDescription' /><br>
                <?php echo __("example (About 2-4 Orders Per Semester)", "example (About 2-4 Orders Per Semester)"); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Days", "Days"); ?></th>
                <td>
                    <input type='text' name='days' style='width:50px;' /> <?php echo __("(Optional) if you set an end date, days will not be used", "(Optional) if you set an end date, days will not be used"); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("End Date", "End Date"); ?></th>
                <td>
                    <input type='text' id='datepicker' style='width:200px;' />
                    <input type='hidden' id='endDate' name='endDate' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Rollovers", "Rollovers"); ?></th>
                <td>
                    <input type='text' name='rollover' style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Visible", "Visible"); ?></th>
                <td>
                    <input type='hidden' name='visible' value="0" />
                    <input type='checkbox' name='visible' value="1" checked />
                    <?php echo __("If disabled, won't be listed in laundry plans", "If disabled, won't be listed in laundry plans."); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Price After Plan", "Price After Plan"); ?></th>
                <td>
                    <input type='text' name='expired_price' style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
                <td>
                    <?= form_yes_no('updateExistingCustomerPlansWhenPlanChange', $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange); ?>
                </td>
            </tr>
        </table>
            <input type='submit' name='add_submit' value='<?php echo __("Add Plan", "Add Plan"); ?>' class='button orange' /><br><br>
    </form>
    <p> * <?php echo __("Required Fields", "Required Fields"); ?> </p>
</div>
<table class="box-table-a">
    <thead>
        <tr>
            <th> <?php echo __("ID", "ID"); ?> </th>
            <th><?php echo __("Name", "Name"); ?></th>
            <th><?php echo __("Caption", "Caption"); ?></th>
            <th><?php echo __("Group", "Group"); ?></th>
            <th><?php echo __("Description", "Description"); ?></th>
            <th><?php echo __("Price", "Price"); ?></th>
            <th><?= weight_system_format( '', true, true );?></th>
            <th><?php echo __("Days", "Days"); ?></th>
            <th><?php echo __("End Date", "End Date"); ?></th>
            <th><?php echo __("Rollover", "Rollover"); ?></th>
            <th><?php echo __("Visible", "Visible"); ?></th>
            <th><?php echo __("Expired Price", "Expired Price"); ?></th>
            <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
            <th align="center" style='width:20px;'><?php echo __("Edit", "Edit"); ?></th>
            <th align="center" style='width:20px;'><?php echo __("Delete", "Delete"); ?></th>
        </tr>
    </thead>
    <tbody>
        <? if($businessLaundryPlans): foreach($businessLaundryPlans as $businessLaundryPlan):?>
        <tr>
            <td> <?= $businessLaundryPlan->businessLaundryPlanID ?> </td>
            <td><?= $businessLaundryPlan->name?></td>
            <td><?= $businessLaundryPlan->caption?></td>
            <td><?= $businessLaundryPlan->grouping?></td>
            <td><?= $businessLaundryPlan->description?></td>
            <td><?= format_money_symbol($this->business_id,'%.2n', $businessLaundryPlan->price); ?></td>
            <td><?= $businessLaundryPlan->pounds?></td>
            <td><?= $businessLaundryPlan->days?></td>
            <td><?= $businessLaundryPlan->endDate != '0000-00-00' ? convert_from_gmt_aprax($businessLaundryPlan->endDate, $dateFormat) : 'NEVER'?></td>
            <td><?= $businessLaundryPlan->rollover?></td>
            <td><?= $businessLaundryPlan->visible ? 'Yes' : 'No' ?></td>
            <td><?= ((float)$businessLaundryPlan->expired_price) ? format_money_symbol($this->business_id,'%.2n', $businessLaundryPlan->expired_price) : '-'; ?></td>
            <td><?= $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange ? 'Yes' : 'No' ?></td>
            <td align="center">
                <a id="edit-<?=$businessLaundryPlan->businessLaundryPlanID?>" href='/admin/customers/edit_plan/<?= $businessLaundryPlan->businessLaundryPlanID?>'><img alt="<?php echo __("Edit Business Laundry Plan", "Edit Business Laundry Plan"); ?>" src='/images/icons/application_edit.png' /></a>          
            </td>
                <td align="center">
                    <a onClick='return verify_delete();' id="delete-<?=$businessLaundryPlan->businessLaundryPlanID?>" href='/admin/customers/delete_plan/<?= $businessLaundryPlan->businessLaundryPlanID?>'>
                        <img alt="Delete Laundry Plan" src="/images/icons/cross.png" />
                    </a>
                </td>
        </tr>
        <? endforeach; else: ?>
        <tr>
            <td colspan="13"><?php echo __("No Plans", "No Plans"); ?></td>
        </tr>
        <? endif; ?>
    </tbody>
</table>
