<?php
enter_translation_domain("admin/customers/product_plans");

/**
 * Note, this view expects the following content variables to be set
 *  title string
 *  business stdClass This object must contains the properties for the current business
 *  businessPlansPlans array An array that contains any number of stdClasses that contain the properties for a business product plan
 */
?>
<script src="/js/plugins/select2/select2.min.js"></script>
<link href="/js/plugins/select2/select2.min.css" rel="stylesheet" />
<style type="text/css">
    .select2-dropdown {
        z-index: 100000;
        width: 30%;
    }
</style>
<script type="text/javascript">
$(document).ready(function(){
    $("#add_plan_button").click(function(){
        $('#plan_form').toggle('');
        if($('#add_plan_button').html()=="Cancel")
            $('#add_plan_button').html("<img src='/images/icons/add.png' /> Add Product Plan");
        else
            $('#add_plan_button').html("Cancel");
    });

    $( "#datepicker" ).datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        altField: "#endDate",
        altFormat: "yy-mm-d",
        dateFormat: "MM d, yy"
    });
    
    $("#add_businessProductPlan_form").validate( {
        rules: {
           name : {
              required: true
           },
           price : {
              required: true,
              number: true
           },
           credit_amount : {
              required: true,
              number: true
           }
        },
        errorClass : "invalid"
    });

    <?php if (!empty(validation_errors())) { ?>
        $("#add_plan_button").click();
    <?php } ?>

    $("#available_products_ids").select2({
        placeholder: "Select multiple products"
    });
});
</script>
<h2> <?= $title ?> </h2>
<p><a id='add_plan_button' href='javascript:;'><img src='/images/icons/add.png' /> <?= __("add_product_plan", "Add Product Plan") ?></a></p>

<div id='plan_form' style='display:none;'>
    <?php echo validation_errors('<div class="message"><div class="error"><ul>', '</ul></div></div>'); ?>
    <form id="add_businessLaundryPlan_form" action='' method='post'>
        <table class="detail_table">
            <tr>
                <th>*<?= __("plan_name", "Plan Name") ?></th>
                <td>
                    <input type='text' name='name' value="<?php echo set_value('name'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("caption", "Caption") ?></th>
                <td>
                    <input type='text' name='caption' value="<?php echo set_value('caption'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("group", "Group") ?></th>
                <td>
                    <input type='text' name='grouping' value="<?php echo set_value('grouping'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("description", "Description") ?></th>
                <td>
                    <textarea name='description' style='width:98%;border:1px solid #ccc;' rows="2"><?php echo set_value('description'); ?></textarea>
                </td>
            </tr>
            <tr>
                <th>*<?= __("price", "Price") ?></th>
                <td>
                    <input type='text' name='price' value="<?php echo set_value('price'); ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th>*<?= __("allowed_items_of_clothing", "Allowed items of clothing") ?></th>
                <td>
                    <input type='text' name='credit_amount' value="<?php echo (int)set_value('credit_amount'); ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= __("notes", "Notes") ?></th>
                <td>
                    <input type='text' name='poundsDescription' value="<?php echo set_value('poundsDescription'); ?>" /><br>
                <?= __("example", "example (About 2-4 Orders Per Semester)") ?>
                </td>
            </tr>
            <tr>
                <th><?= __("days", "Days") ?></th>
                <td>
                    <input type='text' name='days' value="<?php echo set_value('days'); ?>" style='width:50px;' /> <?= __("days_tip", "(Optional) if you set an end date, days will not be used") ?>
                </td>
            </tr>
            <tr>
                <th><?= __("end_date", "End Date") ?></th>
                <td>
                    <input type='text' id='datepicker' style='width:200px;' />
                    <input type='hidden' id='endDate' name='endDate' value="<?php echo set_value('endDate'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("rollover_items_of_clothing", "Rollover items of clothing") ?></th>
                <td>
                    <input type='text' name='rollover' value="<?php echo (int)set_value('rollover'); ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= __("visible", "Visible") ?></th>
                <td>
                    <input type='hidden' name='visible' value="0" />
                    <input type='checkbox' name='visible' value="1" checked />
                    <?= __("visible_help", "If disabled, won't be listed in product plans.") ?>
                    
                </td>
            </tr>
            <tr>
                <th><?= __("expired_price", "Discount After Plan Expires") ?></th>
                <td>
                    <input type='text' name='expired_discount_percent' value="<?php echo set_value('expired_discount_percent'); ?>" style='width:50px;' />%
                </td>
            </tr>
                <tr>
                    <th><?= __("available_products", "Available products") ?></th>
                    <td>
                        <select name="available_products_ids[]" id="available_products_ids" multiple="multiple" style="width: 50%">
                        <?php 
                        foreach ($products as $k=>$b) {
                            $selected = "";
                            if (in_array($k, $selected_products)) $selected = 'selected="selected"';
                        ?>
                           <option value="<?php echo $k ?>" <?php echo $selected ?>>
                                <?php echo $b ?>
                           </option> 
                        <?php
                        }
                        ?>
                        </select>
                    </td>
                </tr> 
                <tr>
                    <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
                    <td>
                        <?= form_yes_no('updateExistingCustomerPlansWhenPlanChange', $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange); ?>
                    </td>
                </tr>
        </table>
            <input type='submit' name='add_submit' value='Add Plan' class='button orange' /><br><br>
    </form>
    <p> <?= __("required", "* Required Fields") ?> </p>
</div>
<table class="box-table-a">
    <thead>
        <tr>
             <th> ID </th>
             <th><?= __("plan_name", "Plan Name") ?></th>
             <th><?= __("caption", "Caption") ?></th>
             <th><?= __("group", "Group") ?></th>
             <th><?= __("description", "Description") ?></th>
             <th><?= __("price", "Price") ?></th>
             <th><?= __("allowed_items_of_clothing", "Allowed items of clothing") ?></th>
             <th><?= __("days", "Days") ?></th>
             <th><?= __("end_date", "End Date") ?></th>
             <th><?= __("rollover_items_of_clothing", "Rollover items of clothing") ?></th>
             <th><?= __("visible", "Visible") ?></th>
             <th><?= __("expired_price", "Discount After Plan Expires") ?></th>
             <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
            <th align="center" style='width:20px;'><?= __("edit", "Edit") ?></th>
            <th align="center" style='width:20px;'><?= __("delete", "Delete") ?></th>
        </tr>
    </thead>
    <tbody>
        <? if($businessProductPlans): foreach($businessProductPlans as $businessProductPlan):?>
        <tr>
            <td> <?= $businessProductPlan->businessLaundryPlanID ?> </td>
            <td><?= $businessProductPlan->name?></td>
            <td><?= $businessProductPlan->caption?></td>
            <td><?= $businessProductPlan->grouping?></td>
            <td><?= $businessProductPlan->description?></td>
            <td><?= format_money_symbol($this->business_id,'%.2n', $businessProductPlan->price); ?></td>
            <td><?= (int)$businessProductPlan->credit_amount; ?></td>
            <td><?= $businessProductPlan->days?></td>
            <td><?= $businessProductPlan->endDate != '0000-00-00' ? date("F j, Y",strtotime($businessProductPlan->endDate)) : 'NEVER'?></td>
            <td><?= (int)$businessProductPlan->rollover?></td>
            <td><?= $businessProductPlan->visible ? 'Yes' : 'No' ?></td>
            <td><?= ((int)$businessProductPlan->expired_discount_percent) ? $businessProductPlan->expired_discount_percent." %": '-'; ?></td>
            <td><?= $businessProductPlan->updateExistingCustomerPlansWhenPlanChange ? 'Yes' : 'No' ?></td>
            <td align="center">
                <a id="edit-<?=$businessProductPlan->businessLaundryPlanID?>" href='/admin/customers/edit_product_plan/<?= $businessProductPlan->businessLaundryPlanID?>'><img alt="Edit Business Product Plan" src='/images/icons/application_edit.png' /></a>          
            </td>
                <td align="center">
                    <a onClick='return verify_delete();' id="delete-<?=$businessProductPlan->businessLaundryPlanID?>" href='/admin/customers/delete_product_plan/<?= $businessProductPlan->businessLaundryPlanID?>'>
                        <img alt="Delete Product Plan" src="/images/icons/cross.png" />
                    </a>
                </td>
        </tr>
        <? endforeach; else: ?>
        <tr>
            <td colspan="15"><?= __("no_plans", "No Plans") ?></td>
        </tr>
        <? endif; ?>
    </tbody>
</table>
