<?php

enter_translation_domain("admin/customers/edit_subscriptions");

?>
<h2><?= __("customer_route_title", "%firstName% %lastName% (%customerID%) Routes", array(
    'firstName' => ucwords($customer->firstName),
    'lastName' => ucwords($customer->lastName),
    'customerID' => $customer->customerID,
)) ?></h2>
<?= $this->session->flashdata("static_message") ?>

<p>
    <a href="/admin/customers/detail/<?= $customer->customerID ?>"><img src="/images/icons/arrow_left.png"> Customer details</a>
    <a title="<?= __("edit_customer_details", "Edit Customer Details") ?>" href='/admin/customers/edit/<?= $customer->customerID?>'><img src='/images/icons/application_edit.png' /> Edit</a>
</p>


<form method="post" action="/admin/customers/edit_subscriptions/<?= $customer->customerID ?>">

<label><?= form_radio("routes[]", 0, empty($customer_subscriptions)) ?> None</label><br>
<? foreach($routes as $zone_id => $zone_name): ?>
<label><?= form_radio("routes[]", $zone_id, !empty($customer_subscriptions[$zone_id])) ?> <?= $zone_name?></label><br>
<? endforeach; ?>

<p>
    <label>Notes</label><br>
    <?= form_textarea('notes', $notes); ?>
</p>

<p><input type="submit" class="button orange" value="Update" name="submit"></p>

</form>
