<?php enter_translation_domain("admin/customers/list_emails"); ?>
<script>

$(document).ready(function(){
	
    $email_dataTable = $("#emails").dataTable({
    	"bPaginate" : true,
        "bJQueryUI": true,
        "aaSorting" : []
    });
    


});

</script>

<h2><?php echo __("Customer Emails for", "Customer Emails for"); ?> <a href='/admin/customers/detail/<?= $customer_id?>'><?= getPersonName($customer) ?> [<?= $customer_id?>]</a></h2>
<p><a href='/admin/customers/detail/<?= $customer_id?>'><?php echo __("Back to customer details", "Back to customer details"); ?></a></p>

<table id='emails'>
   <thead>
    <tr>
        <th><?php echo __("Subject", "Subject"); ?></th>
        <th><?php echo __("Date Sent", "Date Sent"); ?></th>
        <th><?php echo __("Status", "Status"); ?></th>
    </tr>
   </thead>
   <tbody>
    <? 
     foreach($emails as $email):  
    ?>
    <tr>
        <td><a target='_blank' href='/admin/customers/email_detail/<?= $email->emailID?>'>
        <?= strip_tags(empty($email->subject)?"(SMS) ".substr($email->body, 0, 300):$email->subject); ?>
        </a></td>
        <td><?= date(STANDARD_DATE_FORMAT, strtotime($email->sendDateTime)); ?></td>
        <td><?= $email->status?></td>
    </tr>
    <? endforeach; ?>
    </tbody>
</table>