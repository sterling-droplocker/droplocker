<?php enter_translation_domain("admin/customers/view"); ?>
<?= $this->session->flashdata('message');?>
<script type="text/javascript">

$(document).ready(function() {
    $(".delete").click(function() {
        if (confirm("<?php echo __("Are you sure you want to delete this customer", "Are you sure you want to delete this customer?"); ?>"))
        {
            return true;
        }
        else
        {
            return false;
        }
    });

});

</script>
<h2>Customers (<?= number_format_locale($total,0)?>)</h2>

<table id="box-table-a">
    <thead>
        <th><?php echo __("Name", "Name"); ?></th>
        <th><?php echo __("Username", "Username"); ?></th>
        <th><?php echo __("Email", "Email"); ?></th>
        <th><?php echo __("Phone", "Phone"); ?></th>
        <th><?php echo __("Sign Up Date", "Sign Up Date"); ?></th>
        <th width='25'><?php echo __("Login", "Login"); ?></th>
        <th width='25'><?php echo __("Edit", "Edit"); ?></th>
        <th width='25'><?php echo __("Sms", "Sms"); ?></th>
        <th width='25'><?php echo __("Delete", "Delete?"); ?></th>
    </thead>
    <tbody>
        <? foreach($customers as $customer):?>
        <tr>
                <td><a href='/admin/customers/detail/<?= $customer->customerID?>'>
                    <? if (trim($customer->lastName . $customer->firstName)): ?>
                    <?= getPersonName($customer) ?>
                    <? else: ?>
                    <?= '--EMPTY--'; ?>
                    <? endif; ?>
                </a></td>
                <td><?= $customer->username?></td>
                <td><?= $customer->email?></td>
                <td><?= $customer->phone?></td>
                <td>

                <?= convert_from_gmt_aprax($customer->signupDate, get_business_meta($this->business_id, 'standardDateDisplayFormat')) ?></td>
                <td align="center"><a target="_blank" href='/admin/customers/switch_to_customer/<?= $customer->customerID?>'><img src='/images/icons/user_go.png' alt="<?php echo __("Login as Customer", "Login as Customer"); ?>" /></a></td>
                <td align="center"><a href='/admin/customers/edit/<?= $customer->customerID?>'><img src='/images/icons/application_edit.png' alt="<?php echo __("Edit Customer", "Edit Customer"); ?>" /></a></td>
                <td align="center">
                    <?php if($customer->sms): ?>
                        <a href='/admin/customer_sms/new_sms/<?= $customer->customerID?>'><img src='/images/icons/comment_edit.png' alt="<?php echo __("Send sms to customer", "Send sms to customer"); ?>" /></a>
                    <?php endif ?>
                </td>
                <td align="center">
                    <?php
                        $customer = new App\Libraries\DroplockerObjects\Customer($customer->customerID);
                        //The following conditional displays the delete button only if the customer has no orders.
                    ?>
                    <?php if (empty($customer->relationships['Order'])): ?>
                        <?php if (in_bizzie_mode()): ?>
                            <a class='delete' href='/admin/customers/disable/<?=$customer->customerID?>'><img src='/images/icons/cross.png' alt='<?php echo __("Disable Customer", "Disable Customer"); ?>' /></a>
                        <?php else: ?>
                            <a class="delete" href="/admin/customers/delete/<?=$customer->customerID?>"><img src="/images/icons/cross.png" alt="<?php echo __("Delete Customer", "Delete Customer"); ?>" /></a>
                        <?php endif ?>
                    <?php endif; ?>
                </td>
        </tr>
        <? endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8"><?= $paging?></td>
        </tr>
    </tfoot>
</table>

