<?php
enter_translation_domain("admin/customers/everything_plans");

/**
 * Note, this view expects the following content variables to be set
 *  title string
 *  business stdClass This object must contains the properties for the current business
 *  businessEverythingPlans array An array that contains any number of stdClasses that contain the properties for a business laundry plan
 */
?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<script type="text/javascript">
$(document).ready(function(){
    $("#add_plan_button").click(function(){
        $('#plan_form').toggle('');
        if($('#add_plan_button').html()=="Cancel")
            $('#add_plan_button').html("<img src='/images/icons/add.png' /> Add Everything Plan");
        else
            $('#add_plan_button').html("Cancel");
    });

    $( "#datepicker" ).datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        altField: "#endDate",
        altFormat: "yy-mm-d",
        dateFormat: "MM d, yy"
    });
    
    $("#add_businessLaundryPlan_form").validate( {
        rules: {
           name : {
              required: true
           },
           price : {
              required: true,
              number: true
           },
           credit_amount : {
              required: true,
              number: true
           }
        },
        errorClass : "invalid"
    });

    <?php if (!empty(validation_errors())) { ?>
        $("#add_plan_button").click();
    <?php } ?>

});
</script>
<h2> <?= $title ?> </h2>
<p><a id='add_plan_button' href='javascript:;'><img src='/images/icons/add.png' /> <?= __("add_everything_plan", "Add Everything Plan") ?></a></p>

<div id='plan_form' style='display:none;'>
    <?php echo validation_errors('<div class="message"><div class="error"><ul>', '</ul></div></div>'); ?>
    <form id="add_businessLaundryPlan_form" action='' method='post'>
        <table class="detail_table">
            <tr>
                <th>*<?= __("plan_name", "Plan Name") ?></th>
                <td>
                    <input type='text' name='name' value="<?php echo set_value('name'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("caption", "Caption") ?></th>
                <td>
                    <input type='text' name='caption' value="<?php echo set_value('caption'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("group", "Group") ?></th>
                <td>
                    <input type='text' name='grouping' value="<?php echo set_value('grouping'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("description", "Description") ?></th>
                <td>
                    <textarea name='description' style='width:98%;border:1px solid #ccc;' rows="2">
                         <?php echo set_value('description'); ?>
                    </textarea>
                </td>
            </tr>
            <tr>
                <th>*<?= __("price", "Price") ?></th>
                <td>
                    <input type='text' name='price' value="<?php echo set_value('price'); ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th>*<?= __("amount_of_credit_purchased", "Amount of credit purchased") ?></th>
                <td>
                    <input type='text' name='credit_amount' value="<?php echo set_value('credit_amount'); ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= __("notes", "Notes") ?></th>
                <td>
                    <input type='text' name='poundsDescription' value="<?php echo set_value('poundsDescription'); ?>" /><br>
                <?= __("example", "example (About 2-4 Orders Per Semester)") ?>
                </td>
            </tr>
            <tr>
                <th><?= __("days", "Days") ?></th>
                <td>
                    <input type='text' name='days' value="<?php echo set_value('days'); ?>" style='width:50px;' /> <?= __("days_tip", "(Optional) if you set an end date, days will not be used") ?>
                </td>
            </tr>
            <tr>
                <th><?= __("end_date", "End Date") ?></th>
                <td>
                    <input type='text' id='datepicker' style='width:200px;' />
                    <input type='hidden' id='endDate' name='endDate' value="<?php echo set_value('endDate'); ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("credit_rollover", "Credit Rollover") ?></th>
                <td>
                    <input type='text' name='rollover' value="<?php echo set_value('rollover'); ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= __("visible", "Visible") ?></th>
                <td>
                    <input type='hidden' name='visible' value="0" />
                    <input type='checkbox' name='visible' value="1" checked />
                    <?= __("visible_help", "If disabled, won't be listed in everything plans.") ?>
                    
                </td>
            </tr>
            <tr>
                <th><?= __("expired_price", "Discount After Plan Expires") ?></th>
                <td>
                    <input type='text' name='expired_discount_percent' value="<?php echo set_value('expired_discount_percent'); ?>" style='width:50px;' />%
                </td>
            </tr>
            <tr>
                <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
                <td>
                    <?= form_yes_no('updateExistingCustomerPlansWhenPlanChange', $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange); ?>
                </td>
            </tr>
        </table>
            <input type='submit' name='add_submit' value='Add Plan' class='button orange' /><br><br>
    </form>
    <p> <?= __("required", "* Required Fields") ?> </p>
</div>
<table class="box-table-a">
    <thead>
        <tr>
             <th> ID </th>
             <th><?= __("plan_name", "Plan Name") ?></th>
             <th><?= __("caption", "Caption") ?></th>
             <th><?= __("group", "Group") ?></th>
             <th><?= __("description", "Description") ?></th>
             <th><?= __("price", "Price") ?></th>
             <th><?= __("amount_of_credit_purchased", "Amount of credit purchased") ?></th>
             <th><?= __("days", "Days") ?></th>
             <th><?= __("end_date", "End Date") ?></th>
             <th><?= __("credit_rollover", "Credit Rollover") ?></th>
             <th><?= __("visible", "Visible") ?></th>
             <th><?= __("expired_price", "Discount After Plan Expires") ?></th>
             <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
            <th align="center" style='width:20px;'><?= __("edit", "Edit") ?></th>
            <th align="center" style='width:20px;'><?= __("delete", "Delete") ?></th>
        </tr>
    </thead>
    <tbody>
        <? if($businessEverythingPlans): foreach($businessEverythingPlans as $businessEverythingPlan):?>
        <tr>
            <td> <?= $businessEverythingPlan->businessLaundryPlanID ?> </td>
            <td><?= $businessEverythingPlan->name?></td>
            <td><?= $businessEverythingPlan->caption?></td>
            <td><?= $businessEverythingPlan->grouping?></td>
            <td><?= $businessEverythingPlan->description?></td>
            <td><?= format_money_symbol($this->business_id,'%.2n', $businessEverythingPlan->price); ?></td>
            <td><?= $businessEverythingPlan->credit_amount?></td>
            <td><?= $businessEverythingPlan->days?></td>
            <td><?= $businessEverythingPlan->endDate != '0000-00-00' ? convert_from_gmt_aprax($businessEverythingPlan->endDate, $dateFormat) : 'NEVER'?></td>
            <td><?= $businessEverythingPlan->rollover?></td>
            <td><?= $businessEverythingPlan->visible ? 'Yes' : 'No' ?></td>
            <td><?= ((int)$businessEverythingPlan->expired_discount_percent) ? $businessEverythingPlan->expired_discount_percent." %": '-'; ?></td>
            <td><?= $businessEverythingPlan->updateExistingCustomerPlansWhenPlanChange ? 'Yes' : 'No' ?></td>
            <td align="center">
                <a id="edit-<?=$businessEverythingPlan->businessLaundryPlanID?>" href='/admin/customers/edit_everything_plan/<?= $businessEverythingPlan->businessLaundryPlanID?>'><img alt="Edit Business Everything Plan" src='/images/icons/application_edit.png' /></a>          
            </td>
                <td align="center">
                    <a onClick='return verify_delete();' id="delete-<?=$businessEverythingPlan->businessLaundryPlanID?>" href='/admin/customers/delete_everything_plan/<?= $businessEverythingPlan->businessLaundryPlanID?>'>
                        <img alt="Delete Everything Plan" src="/images/icons/cross.png" />
                    </a>
                </td>
        </tr>
        <? endforeach; else: ?>
        <tr>
            <td colspan="15"><?= __("no_plans", "No Plans") ?></td>
        </tr>
        <? endif; ?>
    </tbody>
</table>
