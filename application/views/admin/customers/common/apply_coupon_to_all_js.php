<script type="text/javascript">
$(document).ready(function() {
     var submitBtn = $("#apply_to_all");
     var dialog = $("#results_dialog");

     function showDialog(title) {
        dialog.dialog({
            modal: true,
            title: title,
            width: "700",
            close: function(event, ui) {
            },
            beforeClose: function(event, ui) {
            },
            buttons: {
                "Ok" : function() {
                    $(this).dialog("close");
                }
            }
        });        
     }

     function unLock() {
        submitBtn.loading_indicator("destroy");
        $.unblockUI();
     }

     function sendForm(){
         $.blockUI({message : "<h1> <?=__("Applying coupon", "Applying coupon ..."); ?> </h1>"});
         $("#results_dialog").html("");
         
         $.ajax({
            url:"/admin/coupons/apply_to_all_customers",
            type:"POST",
            data:{
                couponID:$('[name="couponID"]').val(),
                filterID:$('[name="filterID"]').val()
            },
            dataType: 'JSON',
            success: function(response){
                console.log(response);
                var html = "<ul>";
                $.each(response.data.errors, function(k, v) {
                    html += "<li class=\"error\" style=\"font-size:.8em\"><small>"+v+"</small></li>";
                });
                $.each(response.data.success, function(k, v) {
                    html += "<li class=\"success\" style=\"font-size:.8em\"><small>"+v+"</small></li>";
                });
                html += "</ul>";

                dialog.append(html);
                
                unLock();
                showDialog('<?= __("Results","Results") ?>');
            },
            error: function(response){
                console.log(response);
                dialog.append('<span class="error"><?= __("Could not apply coupon to selected customers", "Could not apply coupon to selected customers") ?></error>');
                showDialog('<?= __("Error","Error") ?>');
                unLock();
            }
        });
     }

    submitBtn.loading_indicator({
        "confirm_message" : "<?= __("confirm_message","Are you sure you want to apply this coupon to selected customers?") ?>",
        "callback": function(){
            sendForm();
        }
    });

});
</script>
<div id="results_dialog" style="display:none;"></div>