<?php enter_translation_domain("admin/customers/email_detail"); ?>
<h2><?php echo __("Email Details", "Email Details"); ?></h2>

<table class='detail_table'>
     <tr>
        <th><?php echo __("To", "To"); ?></th>
        <td><?= $email->to ?></td>
    </tr>
    <tr>
        <th><?php echo __("Subject", "Subject"); ?></th>
        <td><?= $email->subject ?></td>
    </tr>
    <tr>
        <th><?php echo __("Scheduled Send Time", "Scheduled Send Time"); ?></th>
        <td><?= $email->sendDateTime->format(STANDARD_DATE_FORMAT) ?></td>
    </tr>
    <tr>
        <th><?php echo __("From", "From"); ?></th>
        <td><?= $email->fromEmail ?> (<?= $email->fromName?>)</td>
    </tr>
    <tr>
        <th><?php echo __("Status", "Status"); ?></th>
        <td><?= $email->status ?><?= !empty($email->errorMessage)?"<br>--------------------------<br><h3>".__("Fail Message", "Fail Message")."</h3>--------------------------<br>".$email->errorMessage:""; ?></td>
    </tr>
    <tr>
        <th><?php echo __("Sent", "Sent"); ?></th>
        <?php if($email->status !='pending' && $email->sendDateTime != null):?>
        <td><?= $email->sendDateTime->format(STANDARD_DATE_FORMAT)?></td>
        <? else: ?>
        <td><span style="font-weight: bold;"><?php echo __("Not sent yet", "Not sent yet"); ?></span></td>
        <?php endif;?>
    </tr>
</table>
<table>
    <tr>
        <td colspan='2'>
            <?= $email->body?>
        </td>
    </tr>
</table>