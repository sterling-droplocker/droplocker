<?php enter_translation_domain("admin/customers/list_everything_plans"); ?>
<?php $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat"); ?>
<h2><?php echo __("Active Customer Plans", "Active Customer Plans"); ?></h2>
<?= message(); ?>

<div style='padding:5px;'><?= sizeof($plans)?> <?php echo __("Active Plans", "Active Plans"); ?></div>
<table id="box-table-a">
	<thead>
		<tr>
			<th><?php echo __("Customer", "Customer"); ?></th>
			<th><?php echo __("Plan", "Plan"); ?></th>
			<th><?php echo __("Start Date", "Start Date"); ?></th>
			<th><?php echo __("Delete", "Delete"); ?></th>
			<?php if (is_superadmin()): ?>
			<th><?php echo __("Renew", "Renew"); ?></th>
			<?php endif; ?>
		</tr>
	</thead>
	<tbody>
		<? if($plans): foreach($plans as $p):?>
		<tr>
			<td><a href='/admin/customers/detail/<?php echo $p->customerID?>'><?= getPersonName($p) ?></a></td>
			<td><?= $p->description?></td>
			<td><?= convert_from_gmt_aprax($p->startDate, $dateFormat)?></td>
			<td><a onclick="return verify_delete();" id="delete-<?php echo $p->laundryPlanID?>" href="/admin/customers/delete_customer_everything_plan/<?php echo $p->laundryPlanID?>">
                        <img alt="<?php echo __("Delete Everything Plan", "Delete Everything Plan"); ?>" src="/images/icons/cross.png">
					</a></td>
			<?php if (is_superadmin()): ?>
			<td><a href='/admin/customers/renew_plan/<?php echo $p->businessLaundryPlan_id?>/<?php echo $p->laundryPlanID?>'><?php echo __("Renew", "Renew"); ?></a></td>
			<?php endif; ?>
		</tr>
		<? endforeach; else: ?>
		<tr>
			<td colspan="<?php echo is_superadmin()?6:5; ?>"><?php echo __("No Records", "No Records"); ?></td>
		</tr>
		<? endif; ?>
	</tbody>
</table>