<?php enter_translation_domain("admin/customers/discounts"); ?>
<?php
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\CustomerDiscount;

/**
 * Expects at least the following content variables
 *  products
 */
?>

<script type="text/javascript">
var products = <?= $products?>;
var customer_id = <?= $customer_id?>;

$(document).ready(function(){
    $('#new_reason_link').click(function(){

        if($(this).text() == '<?php echo __("New reason", "New reason"); ?>'){
            $(this).text('<?php echo __("Cancel", "Cancel"); ?>');
        } else {
            $(this).text('<?php echo __("New reason", "New reason"); ?>');
        }
        $("#description_select").toggle();
        $("#description_text").toggle();
        $("#description_select").val('');
        $("#description_text").val('');

    });

    $("#add_discount_form form input[type='submit']").loading_indicator();
    //$(".button").loading_indicator();

    $("#add_discount_form form").validate({
        errorClass: "invalid",
        submitHandler: function(form) {
            if ($("#couponCode").val()) {
                $("select[name=couponCode]").attr("disabled", "disabled");
            }
            else {
                $("#couponCode").attr("disabled", "disabled");
            }

            form.submit();
        }
    });

    $(".edit_checkbox").change(function () {
        var $loading = $("<img src='/images/progress.gif' />");
        $(this).after($loading);

        var checked;
        if ($(this).is(":checked")) {
            checked = 1;
        }
        else {
            checked = 0;
        }

        $this = $(this);

        $.post("/admin/customer_discounts/update", {
            customerDiscountID : $(this).attr("data-id"),
            property : $(this).attr("data-property"),
            value: checked
        }, function(response) {
            if (response != checked) {
                $this.after($("<span> <?php echo __("Sorry, an error occurred", "Sorry, an error occurred"); ?> </span>"))
            }
            $loading.remove();
        });
    });


});

</script>

<?= $this->session->flashdata('message'); ?>

<style type="text/css">

    fieldset legend{
        font-weight: bold;
        background-color: yellow;
        font-size: 1.2em;
        padding: 6px;
    }

    .active td {
        background: #d4ffaa !important;
    }
    .inactive td {
        background:#ffcccc !important;
    }

</style>

<h2>Customer Discounts</h2>
<?= $this->session->flashdata("static_message") ?>
<div style='padding:5px;' class='orange_box'>Customer: <a href="/admin/customers/detail/<?= $customer[0]->customerID ?>"><?= getPersonName($customer[0]) ?></a></div>
<br>
<a href='javascript:;' style="margin-bottom: 5px" id='add_discount_button'>Add Discount <img src='/images/icons/add.png' /></a>

<div id='add_discount_form' class='hidden' style='display:none'>
    <form action='/admin/customers/add_code_discount' method='post'>
        <fieldset>
        <legend> <?php echo __("Existing Coupon", "Existing Coupon"); ?> </legend>
            <table class='detail_table'>
                    <tr>
                            <th style='background-color:#d4ffaa'><?php echo __("Coupon Code", "Coupon Code"); ?></th>
                            <td>
                                <?= form_dropdown("couponCode", $coupons) ?>  <p> -- or -- </p>  <?= form_input(array("id" => "couponCode", "name" => "couponCode", "style" => "width: auto")) ?>
                                <p> <input type="hidden" name="customer_id" value='<?= $customer_id?>'> </p>
                                <input class='button orange' type="submit" value="<?php echo __("Add", "Add"); ?>"></td>
                    </tr>
            </table>
        </fieldset>
    </form>
    <form action="" method="post">
        <fieldset>
            <legend> <?php echo __("New Coupon", "New Coupon"); ?> </legend>
            <table class='detail_table'>
                <tr>
                    <th><?php echo __("Amount", "Amount"); ?></th>
                    <td><input type="text" id='amount' name="amount" style='width:100px;'></td>
                </tr>
                <tr>
                    <th><?php echo __("Discount Type", "Discount Type"); ?></th>
                    <td>
                        <select id='amountType' name="amountType">
                            <option value="dollars" SELECTED><?php echo __("Dollars", "Dollars"); ?></option>
                            <option value="percent"><?php echo __("Percent", "Percent"); ?></option>
                            <option value="lockerloot"><?php echo __("LockerLoot", "LockerLoot"); ?></option>
                            <option value="pounds"><?= weight_system_format('',true,true);?></option>
                            <option value="item"><?php echo __("Item_amount", "Item $"); ?></option>
                            <option value="itemPercent"><?php echo __("Item_percentaje", "Item %"); ?></option>
                        </select>
                    </td>
                </tr>
                <tr id='type_row'>

                </tr>
                <tr id="process_id_wrapper" style="display:none;">
                    <th><?= __("label_process_id", "Process:") ?></th>
                    <td>
                        <?= form_dropdown("process_id", $processes, array('0'), 'id="process_id"'); ?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __("Frequency", "Frequency"); ?></th>
                    <td>
                        <select id='frequency' name="frequency">
                            <option value="one time" SELECTED><?php echo __("one time", "one time"); ?></option>
                            <option value="every order"><?php echo __("every order", "every order"); ?></option>
                            <option value="until used up"><?php echo __("until used up", "until used up"); ?></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __("Reason", "Reason"); ?></th>
                    <td>
                    <select id='description_select' name="description">
                    <?
                    foreach($reasons as $r)
                    {
                        echo '<option value="'.$r->description.'">'.ucwords($r->description).'</option>';
                    }
                    ?>
                    </select><input style='display:none;width:300px' id='description_text' type='text' name='new_description' /> <a id='new_reason_link' href='javascript:;'><?php echo __("New reason", "New reason"); ?></a>

                    </td>
                </tr>
                <tr id='employer_match_row' style='display:none'>
                    <th><?php echo __("Select Customer to Pay", "Select Customer to Pay"); ?></th>
                    <td><input type='text' name='customer' id='customer' />
                        <div id='customer_results'></div>
                        <input type='hidden' id='customer_id' name='customer_id' value='' />
                    </td>
                </tr>
                <tr>
                    <th><?php echo __("Expires", "Expires"); ?></th>
                    <td>
                        <select id='expireDate' name="expireDate">
                            <option value="" SELECTED><?php echo __("NEVER", "NEVER"); ?></option>
                            <?php
                                for ($i = 1; $i <= 400; $i++) {
                                    $selectDay = mktime(0, 0, 0, date("m"), date("d")+$i,  date("Y"));
                                    echo '<option value="'.date("Y-m-d", $selectDay).'">'.$i.' Days: '.date("M-d",$selectDay).'</option>';
                                }

                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><?php echo __("Location", "Location"); ?></th>
                    <td><?= form_dropdown("location_id", $locations, $customer->state, 'id="location_id"') ?></td>
                </tr>
                <tr>
                    <th style='vertical-align: top'><?php echo __("Description", "Description"); ?></th>
                    <td><textarea cols="30" rows="3" id='extendedDesc' name="extendedDesc"></textarea></td>
                </tr>
                <tr>
                    <th> <?php echo __("Recurring Types", "Recurring Types"); ?></th>
                    <td> <?= form_dropdown("recurring_type_id", $recurring_types) ?>  </td>
                </tr>
                <tr>
                    <th> <?php echo __("Combine", "Combine"); ?> </th>
                    <td> <?= form_checkbox("combine", "1", FALSE) ?> </td>
                </tr>
            </table>
            <input type="submit" id='create_button' name='submit' class='button orange' value="<?php echo __("Create", "Create"); ?>">
            <input type="hidden" name="custId" value="<?= $customer_id; ?>">
        </fieldset>
    </form>
</div>

<br />
<h2><?php echo __("Discount History", "Discount History"); ?></h2>
<table class='box-table-a'>
<thead>
    <tr>
        <th><?php echo __("Created", "Created"); ?></th>
        <th><?php echo __("Amount", "Amount"); ?></th>
        <th><?php echo __("Discount Type", "Discount Type"); ?></th>
        <th><?php echo __("Frequency", "Frequency"); ?></th>
        <th><?php echo __("Reason", "Reason"); ?></th>
        <th><?php echo __("Description", "Description"); ?></th>
        <th><?php echo __("Code", "Code"); ?></th>
        <th> <?php echo __("Combine", "Combine"); ?> </th>
        <th align='center'> <?php echo __("Active", "Active"); ?> </th>
        <th><?php echo __("Updated", "Updated"); ?></th>
        <th><?php echo __("Expires", "Expires"); ?></th>
        <th><?php echo __("Created By", "Created By"); ?></th>
        <th><?php echo __("Applied to Order", "Applied to Order"); ?></th>
        <th><?php echo __("Amount Applied", "Amount Applied"); ?></th>
        <th><?php echo __("Map", "Map"); ?> </th>
        <th><?php echo __("Delete", "Delete"); ?></th>
        <th> <?php echo __("Extend Date", "Extend Date"); ?> </th>
        <th> <?php echo __("Recurring", "Recurring"); ?> </th>
    </tr>
</thead>
<tbody>
    <?php if($discounts): ?>
        <?php foreach ($discounts as $discount) :?>
            <!-- This is the 'Created' column -->
            <!-- The following conditional determines whether or not to style the row as an active or an inactive row -->
            <?php if ((int) $discount->active): ?>
                <tr class="active">
            <?php else: ?>
                <tr class="inactive">
            <?php endif; ?>
                    <td nowrap="true"> <?= convert_from_gmt_aprax($discount->origCreateDate, STANDARD_DATE_FORMAT) ?> </td>
                    <!-- This is the 'Amount' column -->
                    <td>
                        <?php
                            if($discount->amountType == 'dollars') {
                                echo format_money_symbol($customer[0]->business_id, '%.2n', $discount->amount);
                            }
                            else if ($discount->amountType == "percent") {
                                echo number_format_locale($discount->amount,0).'%';
                            }
                            else if ($discount->amountType == "pounds") {
                                echo $discount->amount . weight_system_format('', false, true );
                            } 
                            else if ($discount->amountType == "product_plan") {
                                echo (int)$discount->amount;
                            }
                            else if ($discount->amountType == "product_plan_percent") {
                                echo number_format_locale($discount->amount,0).'%';
                            }
                            else {
                                echo $discount->amount;
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                        //The following statement checks to see if the amoutn type is a legacy 'item[productID]', if so, then the product ID is parsed from the value and the corresponding product is retrieved and displayed.
                            preg_match("%item\[(\w+)\]%", $discount->amountType, $matches);

                            if ($matches) {
                                $get_product_query = $this->db->query("select displayName FROM product WHERE productID = '{$matches[1]}'");
                                $products = $get_product_query->result();
                                if ($products)  {
                                    echo $products[0]->displayName;
                                }
                                else {
                                    echo $discount->amountType;
                                }
                            }

                            else {
                                echo $discount->amountType;
                            }

                        ?>
                    </td>
                    <!-- This is the 'Frequency' column -->
                    <td>
                        <?= $discount->frequency?>
                    </td>
                    <!-- THis is the 'Reason' column -->
                    <td>
                        <?= $discount->description?>
                    </td>
                    <!-- THis is the extended description colum -->
                    <td>
                        <?= $discount->extendedDesc?>
                    </td>
                    <!-- This is the 'Code' column -->
                    <td>
                        <?= $discount->couponCode?>
                    </td>
                    <!-- This is the 'combine' column -->
                    <td>
                        <?= form_checkbox(array("checked" => (bool) $discount->combine, "data-id" => $discount->{CustomerDiscount::$primary_key}, "data-property" => "combine", "class" => "edit_checkbox")) ?>
                    </td>
                    <!-- This is the 'Active' column -->
                    <td align='center'>
                        <?= $discount->active ?>
                    </td>
                    <!-- THis is the 'Updated' column -->
                    <td nowrap="true">
                        <?= convert_from_gmt_aprax($discount->updated, STANDARD_DATE_FORMAT) ?>
                    </td>
                    <!-- This is the 'Expires' column -->
                    <td nowrap="true">
                        <?= ($discount->expireDate)?  date("M j Y h:i A", strtotime($discount->expireDate)):"Never";?>
                    </td>
                    <!-- THis is the 'Created By' column -->
                    <td>
                        <?= ((int) $discount->createdBy)?get_employee_name($discount->createdBy):"&lt;unknown&gt;";?>
                    </td>
                    <!-- THis is the 'Applied to Order' column -->
                    <td>
                        <? if ($discount->order_id): ?>
                        <a href="/admin/orders/details/<?= $discount->order_id ?>" target="_blank"><?= $discount->order_id ?></a>
                        <? endif; ?>
                    </td>
                    <!-- This is the 'Amount Applied' column -->
                    <td>
                        <? if ($discount->amountType == 'pounds'): ?>
                        <?= weight_system_format($discount->amountApplied) ?>
                        <? else: ?>
                        <?= format_money_symbol($customer[0]->business_id, '%.2n', $discount->amountApplied)?>
                        <? endif; ?>
                    </td>
                    <!-- THis is the 'Map' colun -->
                    <td align='center'>
                        <?= ($discount->location_id>0)?"<a href='javascript:;' class='location_button' id='location-<?= $discount->location_id?>'><img title='".$discount->address.", ".$discount->city."' src='/images/icons/map.png' /></a>":"";?>
                    </td>
                    <!-- This is the 'Delete' column -->
                    <td align='center'>
                        <a href='javascript:;' class='del_button' id='del-<?= $discount->customerDiscountID?>'><img src='/images/icons/cross.png' /></a>
                    </td>
                    <!-- This is the 'Extend Date' column -->
                    <td>
                        <?php
                            //The following conditional determines whether to display the interface for extending an expiration date on a product, or to simply display the reactivate button if the discount has no expiration date.
                            if (isset($discount->expireDate)) {
                                echo form_open("/admin/customers/update_discount_expireDate", "", array("customerDiscountID" => $discount->customerDiscountID));
                                $todays_date = new DateTime();
                                $dates = array();
                                for ($x = 1; $x <= 279; $x++) {
                                    if ($x == 1) {
                                        $dates[date_add($todays_date, date_interval_create_from_date_string("1 day"))->format("Y-m-d")] = sprintf("%s day: %s %s", $x, $todays_date->format("M"), $todays_date->format("d"));
                                    }
                                    else {
                                        $dates[date_add($todays_date, date_interval_create_from_date_string("1 day"))->format("Y-m-d")] = sprintf("%s days: %s %s", $x, $todays_date->format("M"), $todays_date->format("d"));
                                    }
                                }
                                echo form_dropdown("expireDate", $dates);
                                echo form_submit("", "Extend");
                                echo form_close();
                            }
                            else {
                                //echo form_open("/admin/customers/update_discount_activation_status", "", array("customerDiscountID" => $discount->customerDiscountID, "active" => 1));
                                //echo form_submit("", "Reactivate");
                                //echo form_close();
                            }
                        ?>
                    </td>
                    <!-- This is the 'Recurring' column -->
                    <td>
                        <?php
                            echo $discount->name;
                        ?>
                        <?php if($discount->recurring_type_id != 1): ?>
                            <a class="deactivate button blue" href="/admin/customer_discounts/deactivate?customerDiscountID=<?= $discount->customerDiscountID?>"> <?php echo __("Deactivate", "Deactivate"); ?> </a>
                        <?php endif; ?>
                    </td>
                </tr>
        <?php endforeach; ?>
    <?php endif;?>
</tbody>
</table>

<script type='text/javascript'>
    var amount = $('#amount').val();
    var amountType = $('#amountType').val();
    var frequency = $('#frequency').val();
    var description = $('#description').val();
    var expireDate = $('#expireDate').val();
    var extendedDesc = $('#extendedDesc').val();
    var select_customer = '';
    $('#amountType').change(function(){
	$('#type_row').html('');

        $('#process_id').val(0);
        $('#process_id_wrapper').css('display', 'none');
                
	if($(this).val().indexOf('item') >= 0){
	    //create another row for product
	    var select = $('<select name="product_id"><option value=""><?php echo __("SELECT", "-- SELECT --"); ?></option></select>');
            select.append("<option value='dc'><?php echo __("All Dry Cleaning", "All Dry Cleaning"); ?></option>");
	    $.each(products, function(i,item){
		select.append("<option value='"+item.productID+"'>"+item.displayName+"</option>");
	    });
	    var th = $('<th><?php echo __("Select Product", "Select Product"); ?></th>');
	    var td = $('<td></td>').append(select);
	    $('#type_row').append(th);
	    $('#type_row').append(td);
        
            select.change(function(){
                if( $(this).val() == 'dc' ){
                    $('#process_id_wrapper').css('display', 'table-row');
                }else{
                    $('#process_id').val(0);
                    $('#process_id_wrapper').css('display', 'none');
                }
            });
	}
    });

    $("#description_select").change(function(){
    	if($(this).val() == 'employer match'){
            $('#employer_match_row').css('display', 'table-row');
            $('#customer').autocomplete({
                source: function(req, add) {

                    //pass request to server
                    $.getJSON("/ajax/customer_autocomplete/", req, function(data) {
                        console.log(data);
                        dc=data;
                        //create array for response objects
                        var suggestions = [];

                        //process response
                        $.each(data, function(i, val){
                            suggestions.push(data[i]);

                            });

                        //pass array to callback
                        add(suggestions)
                   });
              },
                appendTo: "#customer_results",
                select : function(e, ui){
                    $.each(dc, function(i, val){
                        if(ui.item.value==val)
                            $('#customer_id').val(i);
                    });
                }
            });
    	}
        else {
            $('#employer_match_row').css('display', 'none');
    	}

    });

    $("#add_discount_button").click(function(){
    	if($('#add_discount_form').hasClass('hidden')) {
            $('#add_discount_form').css('display', 'block');
            $('#add_discount_form').addClass('visible').removeClass('hidden');
            $('#add_discount_button').html("Cancel <img src='/images/icons/cross.png' />");
    	}
    	else {
            $('#add_discount_form').css('display', 'none');
            $('#add_discount_form').addClass('hidden').removeClass('visible');
            $('#add_discount_button').html("<?php echo __("Add Discount", "Add Discount"); ?> <img src='/images/icons/add.png' />");
    	}
    });

    $('.del_button').live('click', function(){
    	if(!verify_delete()){
            return false;
    	}
    	else {
            var discount_var = $(this).attr('id');
            var discount_id = discount_var.replace('del-', '');

            $.ajax({
                url: "/admin/customers/delete_discount/",
                type:"POST",
                data:{"delete" : "delete",
                          "customer_id" : customer_id,
                          "customerDiscountID" : discount_id,
                          "json" : "json"
                        },
                dataType: "JSON",
                success: function(data){
                    //alert(data.status);
                    if(data.status=='success'){
                        top.location.href='/admin/customers/discounts/'+customer_id;
                    }
                    else{
                        alert(data.error);
                    }
                }
            });
        }
    });

</script>
