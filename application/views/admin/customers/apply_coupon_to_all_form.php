<?php
enter_translation_domain("admin/customers/apply_coupon_to_all_form");
?>
<?php $this->load->view('admin/customers/common/apply_coupon_to_all_js'); ?>
<h2> <?= __("apply_to_all", "Apply Coupon to selected Customers") ?></h2>
<?= form_open("/admin/coupons/apply_to_all_customers", array("id"=>"frm_apply_to_all_customers", "style" => "padding-top: 10px;")) ?>
    <label><?= __("coupon", "Coupon:") ?></label>
    <?= form_dropdown("couponID", $coupons) ?>
    <label><?= __("filter", "Filter:") ?></label>
    <?= form_dropdown("filterID", $filters) ?>
    <input type="button" value="<?= __("apply", "Apply") ?>" class="button blue" id="apply_to_all">
<?= form_close() ?>