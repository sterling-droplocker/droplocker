<?php enter_translation_domain("admin/customers/edit_plan"); ?>
<?php
/** 
 * Note, this view expects the following content variables to be set
 *  businessLaundryPlan \App\Libraries\DroplockerObjects\BusinessLaundryPlan 
 *  bizzie_mode bool
 *  title string
 *  is_master_business_laundry_plan bool
 */
if (!isset($is_master_business_laundry_plan)) {
    $is_master_business_laundry_plan = false;
}

$decimals = get_business_meta($this->business_id, 'decimal_positions', 2);
?>
<script type="text/javascript">
$(document).ready(function(){

    $("#plan_form :submit").loading_indicator();

    $("#datepicker").datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        altField: "#endDate",
        altFormat: "yy-mm-d",
        dateFormat: "MM d, yy"
    });

	$("#datepicker").change(function(evt) {
		if (!$(this).val().length) {
			$('#endDate').val('');
		}
	});

    $('#days').keypress(function(){
        $("#datepicker").val("");
        $("#endDate").val("");
    });

    jQuery.validator.addMethod("days_or_endDate", function() {
        var days = $("#days").val();
        var date = $("#datepicker").val().trim().length;
        if (days == "0" && date === 0) return false;
        return true;
    }, "<small> <?php echo __("If Days is equal to 0 End Date must be not empty.", "If 'Days' is equal to 0 'End Date' must be not empty."); ?></small>");

    $("#edit_businessLaundryPlan_form").validate({
        rules: {
            name : {
                required: true
            },
            price : {
                required: true,
                number: true
            },
           pounds : {
                required: true,
                digits: true
           },
           days : {
                days_or_endDate: true,
           }
        },
        errorClass: "invalid",
        invalidHandler: function(form, validator) {
            $("#plan_form :submit").loading_indicator("destroy");
        }
    });
});
</script>
<h2><?=$title?></h2>
<p><a href="/admin/customers/laundry_plans"><img src="/images/icons/arrow_left.png"> <?php echo __("Manage Laundry Plans", "Manage Laundry Plans"); ?></a></p>

<?= $this->session->flashdata('message')?>
<div id='plan_form'>
    <form id="edit_businessLaundryPlan_form" action='' method='post'>
        <table class="detail_table">
            <tr>
                <th>*<?php echo __("Plan Name", "Plan Name"); ?></th>
                <td>
                    <input type='text' name='name' value="<?= $businessLaundryPlan->name?>" />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Caption", "Caption"); ?></th>
                <td>
                        <input type='text' name='caption' value="<?= $businessLaundryPlan->caption?>" />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Group", "Group"); ?></th>
                <td>
                    <input type='text' name='grouping' value="<?= $businessLaundryPlan->grouping?>" /> 
                </td>
            </tr>
            <tr>
                <th><?php echo __("Description", "Description"); ?></th>
                <td>
                    <textarea name='description' style='width:98%;border:1px solid #ccc;' rows="2"><?= $businessLaundryPlan->description?></textarea>
                </td>
            </tr>
            <tr>
                <th> *<?php echo __("Price", "Price"); ?> </th>
                <td>
                    <input type='text' name='price' style='width:50px;' value="<?= number_format_locale($businessLaundryPlan->price, $decimals, '.', '') ?>" />
                </td>
            </tr>
            <tr>
                <th> *<?php echo __("Pounds", "Pounds"); ?> </th>
                <td>
                    <input type='text' name='pounds' style='width:50px;' value="<?= $businessLaundryPlan->pounds?>" />
                </td>
            </tr>
            <tr>
                <th> <?php echo __("Notes", "Notes"); ?> </th>
                <td>
                    <input type='text' name='poundsDescription' value="<?= $businessLaundryPlan->poundsDescription?>" /><br>
                    <?php echo __("example (About 2-4 Orders Per Semester)", "example (About 2-4 Orders Per Semester)"); ?>
                </td>
            </tr>
            <tr>
                <th>*<?php echo __("Days", "Days"); ?></th>
                <td>
                    <input type='text' id='days' name='days' style='width:50px;' value="<?= $businessLaundryPlan->days?>" /> <?php echo __("(Optional) if you set an end date, days will not be used", "(Optional) if you set an end date, days will not be used"); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("End Date", "End Date"); ?></th>
                <td>
                    <?php 
                    $formattedEndTime = $businessLaundryPlan->endDate != '0000-00-00' ? strtotime( $businessLaundryPlan->endDate ) : 0;
                    $formDateValue    = $formattedEndTime ? date( "Y-m-d", $formattedEndTime ) : "";
                    ?>
                    <input type='text' id='datepicker' style='width:200px;' value="<?=( $formattedEndTime > 0 ) ? date("F j, Y", $formattedEndTime ) : ""; ?>" />
                    <input type='hidden' id='endDate' name='endDate' value="<?=$formDateValue ?>" />
                </td>
            </tr>
            <tr>
                <th><?php echo __("Rollovers", "Rollovers"); ?></th>
                <td>
                    <input type='text' name='rollover' style='width:50px;' value="<?= $businessLaundryPlan->rollover?>"/>
                </td>
            <tr>
                <th><?php echo __("Price After Plan", "Price After Plan"); ?></th>
                <td>
                    <input type='text' name='expired_price' style='width:50px;' value="<?= number_format_locale($businessLaundryPlan->expired_price, $decimals, ".", "") ?>" />
                </td>
            </tr>
            </tr>
            <tr>
                <th><?php echo __("Visible", "Visible"); ?></th>
                <td>
                    <input type='hidden' name='visible' value="0" />
                    <input type='checkbox' name='visible' value="1" <?= $businessLaundryPlan->visible ? 'checked' : '' ?> />
                    <?php echo __("If disabled, won't be listed in laundry plans", "If disabled, won't be listed in laundry plans."); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("URL", "URL"); ?></th>
                <td>
                    <input type='text' readonly  value="<?= $plan_url ?>"/>
                    <br><?php echo __("Use this to let the customers see the plan if hidden", "Use this to let the customers see the plan if hidden."); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
                <td>
                    <?= form_yes_no('updateExistingCustomerPlansWhenPlanChange', $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange); ?>
                </td>
            </tr>
        </table>
        <p> * <?php echo __("Required Fields", "Required Fields"); ?> </p>
        <input type='submit' name='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' /><br><br>
    </form>
</div>
