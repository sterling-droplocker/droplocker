<?php enter_translation_domain("admin/customers/setupInvoicing"); ?>
<?= $this->session->flashdata('message');?>
<h2><?php echo __("Setup Invoicing", "Setup Invoicing:"); ?> <a href="/admin/customers/detail/<?= $customer[0]->customerID ?>"><?= getPersonName($customer[0]) ?></a></h2>


<form action="" method="post">
<table class="detail_table">
	<tr>
		<th><?php echo __("Billing Name", "Billing Name"); ?></th>
		<td><input type='text' name='billingName' value="<?= ucwords($invoicing[0]->billingName)?>" /></td>
	</tr>
	<tr>
		<th><?php echo __("Frequency", "Frequency"); ?></th>
		<td><input type='radio' name='frequency' value='every month' <? if($invoicing[0]->frequency == 'every month') echo ' checked'; ?>/> <?php echo __("Every Month", "Every Month"); ?>
			<input type='radio' name='frequency' value='every week' <? if($invoicing[0]->frequency == 'every week') echo ' checked'; ?>/> <?php echo __("Every Week", "Every Week"); ?>
			<input type='radio' name='frequency' value='every other week' <? if($invoicing[0]->frequency == 'every other week') echo ' checked'; ?>/> <?php echo __("Every Other Week", "Every Other Week"); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo __("Method", "Method"); ?></th>
		<td><input type='radio' name='method' value='email' <? if($invoicing[0]->method == 'email') echo ' checked'; ?>/> <?php echo __("eMail", "eMail"); ?>
			<input type='radio' name='method' value='fax' <? if($invoicing[0]->method == 'fax') echo ' checked'; ?>/> <?php echo __("Fax", "Fax"); ?>
			<input type='radio' name='method' value='snail' <? if($invoicing[0]->method == 'snail') echo ' checked'; ?>/> <?php echo __("Snail Mail", "Snail Mail"); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo __("Terms", "Terms"); ?></th>
		<td><input type='radio' name='terms' value='Upon Receipt' <? if($invoicing[0]->terms == 'Upon Receipt') echo ' checked'; ?>/> <?php echo __("Upon Receipt", "Upon Receipt"); ?>
			<input type='radio' name='terms' value='Net 15' <? if($invoicing[0]->terms == 'Net 15') echo ' checked'; ?>/> <?php echo __("Net 15", "Net 15"); ?>
			<input type='radio' name='terms' value='Net 30' <? if($invoicing[0]->terms == 'Net 30') echo ' checked'; ?>/> <?php echo __("Net 30", "Net 30"); ?>
			<input type='radio' name='terms' value='Credit Card' <? if($invoicing[0]->terms == 'Credit Card') echo ' checked'; ?>/> <?php echo __("Credit Card", "Credit Card"); ?>
		</td>
	</tr>
	<tr>
		<th><?php echo __("Email Address", "Email Address"); ?></th>
		<td><input type='text' name='email' value="<?= $invoicing[0]->email ?>" /></td>
	</tr>
	<tr>
		<th><?php echo __("Billing Address", "Billing Address"); ?></th>
		<td><textarea cols="40" rows="4" name="address"><?= $invoicing[0]->address ?></textarea></td>
	</tr>
	<th><?php echo __("Details", "Details"); ?></th>
		<td><input type='radio' name='bagDetail' value='0' <? if($invoicing[0]->bagDetail == '0') echo ' checked'; ?>/> <?php echo __("None", "None"); ?>
			<input type='radio' name='bagDetail' value='1' <? if($invoicing[0]->bagDetail == '1') echo ' checked'; ?>/> <?php echo __("By Customer", "By Customer"); ?>
			<input type='radio' name='bagDetail' value='2' <? if($invoicing[0]->bagDetail == '2') echo ' checked'; ?>/> <?php echo __("Super Detail", "Super Detail"); ?>
			<input type='radio' name='bagDetail' value='3' <? if($invoicing[0]->bagDetail == '3') echo ' checked'; ?>/> <?php echo __("Summary", "Summary"); ?>
		</td>
	<tr>
</table>
<br>
<input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' />
</form>


