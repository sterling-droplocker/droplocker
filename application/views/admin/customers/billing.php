<?php enter_translation_domain("admin/customers/billing"); ?>
<?php

/** Expects the following content variables to be set
 * customer stdClass
 * card stdClass
 * showKioskCheckbox
 * accepted_cards
 */
$card_images = array(
    "visa"            => "/images/card_visa.gif",
    "mastercard"      => "/images/card_mastercard.gif",
    "americanexpress" => "/images/card_amex.gif",
    "discover"        => "/images/card_discover.gif"
);

$all_states = get_all_states_by_country_as_array();

$updateFormId = "updateForm";
?>

<?php

$fieldList = array();
$fieldList['firstName'] = 
        "<tr>
            <th>" . form_label(__("Billing First Name", "Billing First Name"), "firstName") . "</th>
          <td>
            <input id='firstName' type='text' value='" . set_value("firstName", $customer->firstName) . "' name='firstName'>
          </td>
        </tr>";
            
$fieldList['lastName'] = 
        "<tr>
            <th>" . form_label(__('Billing Last Name', 'Billing Last Name'), 'lastName') . "</th>
          <td>
            <input id='lastName' type='text' value='" . set_value('lastName', $customer->lastName) . "' name='lastName'>
          </td>
        </tr>";
            
$nameForm = getSortedNameForm($fieldList);

//------------------

$fieldList = array();
$fieldList['address1'] = 
        "<tr>
          <th>" . form_label(__('Billing Address', 'Billing Address'), 'address1') . "</th>
          <td>
            <input id='address1' type='text' value='" . set_value('address1', $customer->address1) . "' name='address1'>
          </td>
        </tr>";
            
$fieldList['address2'] = 
        "<tr>
          <th>" . form_label(__('Billing Address 2', 'Billing Address 2'), 'address2') . "</th>
          <td>
            <input id='address2' type='text' value='" . set_value('address2', $customer->address2) . "' name='address2'>
          </td>
        </tr>";

$fieldList['city'] = 
        "<tr>
          <th>" . form_label(__('Billing City', 'Billing City'), 'city') . "</th>
          <td>
            <input id='city' type='text' value='" . set_value('city', $customer->city) . "' name='city'>
          </td>
        </tr>";
            
$fieldList['state'] = 
        (isset($all_states[$this->business->country]))?
        "<tr>
            <th>" . form_label(__('Billing State', 'Billing State'), 'state') . "</th>
            <td>
                " . form_dropdown('state', $all_states[$this->business->country], set_value('state', $customer->state), 'id="state"' ) . "
            </td>
        </tr>"
        :
        "<tr>
            <th>" . form_label(__('Billing Locality', 'Billing Locality'),'state') . "</th>
            <td>
              " . form_input(array('id' => 'state', 'name' => 'state', 'value' => set_value('state', $customer->state))) . "
            </td>
        </tr>";

$fieldList['zip'] = 
        "<tr>
            <th>" . form_label(__('Zipcode', 'Zipcode'), 'zip') . "</th>
            <td>
                " . form_input(array('id' => 'zip', 'name' => 'zip', 'value' => set_value('zip', $customer->zip))) . "
            </td>
        </tr>";
        
$addressForm = getSortedAddressForm($fieldList);

?>

<h2><?php echo __("Update Customer's Credit Card", "Update Customer's Credit Card:"); ?> <?= getPersonName($customer) ?> <a href="/admin/customers/detail/<?= $customer->customerID ?>">(<?php echo __("view", "view"); ?>)</a></h2>
<p>
    <a href="/admin/customers/edit/<?= $customer->customerID ?>"><img src="/images/icons/arrow_left.png"> <?php echo __("Edit Customer", "Edit Customer"); ?></a>
    | <a href="/admin/customers/detail/<?= $customer->customerID ?>"><img src="/images/icons/table.png"> <?php echo __("Customer Details", "Customer Details"); ?></a>
    | <a href="/admin/customers/switch_to_customer/<?= $customer->customerID ?>" target="_blank"><img src="/images/icons/user_go.png"> <?php echo __("Log In as Customer", "Log In as Customer"); ?></a>
</p>

<?= $this->session->flashdata('captureResult'); ?>

<? if(!empty($card)): // show the card on file ?>
    <h3>Credit Card on File </h3>
    <form action="/admin/customers/delete_card/<?= $customer->customerID ?>" method="post">
        <table class="detail_table">
            <tr>
                <th><?php echo __("Last 4", "Last 4:"); ?></th>
                <td><?= $card->cardNumber?></td>
            </tr>
            <tr>
                <th><?php echo __("Expires", "Expires:"); ?></th>
                <td><?= $card->expMo?>/<?= $card->expYr ?></td>
            </tr>
        </table>
        <input type="submit" name="delete" value="<?php echo __("Delete Card", "Delete Card"); ?>" class="button red" />
    </form>
<? endif; ?>

<h3><?php echo __("Update Credit Card", "Update Credit Card"); ?></h3>

<?=form_open("/admin/creditCards/update", array("id"=>$updateFormId)) ?>
    <?= form_hidden("customer_id", $customer->customerID) ?>
    <table class="detail_table">
        <? if(!empty($direct_sale)): ?>
            <tr>
                <th> <?= form_label(__("Name of Card on File", "Name of Card on File"), "fullName") ?> </th>
              <td>
                <input id="fullName" type="text" value="<?= sprintf("%s %s", $customer->firstName, $customer->lastName) ?>" name="fullName">
              </td>
            </td>
        <? endif ?>
        
        <?php echo $nameForm; ?>
        
        
        <?php echo $addressForm; ?>
        
        <tr>
          <th> <?= form_label(__("Credit or Debit Card Number", "Credit or Debit Card Number"), "cardNumberzip") ?> </th>
          <td>
                <?= form_input(array("type" => "text", "id" => "cardNumber", "value" => set_value("cardNumber"), "size" => 20, "name" => "cardNumber", "autocomplete" => "off", "required" => TRUE, "pattern" => "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$")) ?>
                <p>
                <? if( !empty( $accepted_cards ) ): ?>
                    <? foreach( $accepted_cards as $cardType ): ?>
                        <? if(!empty($card_images[$cardType])): ?>
                            <img src="<?=$card_images[$cardType]?>" />
                        <? endif ?>
                    <? endforeach; ?>
                <? else: ?>
                    <img align="absmiddle" src="/images/cardTypes.gif" />
               <? endif; ?>
           </p>
          </td>
        </td>
        <tr>
            <th> <?= form_label(__("Expiration", "Expiration"), "expMo") ?> </th>
            <td>
                <?
                    $months = array("" => "- Month -");
                    for ($i = 1; $i <= 12 ; $i++) {
                        $m = sprintf("%02d", $i);
                        $months[$m] = $m;
                    }
                    echo form_dropdown("expMo", $months, set_value("expMo"), 'id="expMo" required="required"');
                ?>
                 /
                 <?
                        $start_date = date("Y");
                        for ($x = 0; $x <= 15; $x++) {
                            $year = $start_date + $x;
                            $dates[$year] = $year;
                        }
                        echo form_dropdown("expYr", $dates, set_value("expYr"), 'id="expYr" required="required"');
                    ?>
            </td>
        </td>
        <tr>
          <th> <?= form_label(__("Card Security Code", "Card Security Code"), "csc") ?></th>
          <td>
           <input type="text" id="csc" value="<?= set_value("csc") ?>" autocomplete="off" style="width:50px;" name="csc">
          </td>
        </td>
        <? if ($this->business->store_access): ?>
            <tr>
                <th><?php echo __("Kiosk Access", "Kiosk Access"); ?></th>
                <td>
                    <? if($showKioskCheckbox): ?>
                        <div class="alert alert-info"><?php echo __("To access our 24 hour kiosks, you will need your credit card on file to unlock the door", "To access our 24 hour kiosks, you will need your credit card on file to unlock the door"); ?> </div>
                        <input type="hidden" value="1" name="kioskAccess" />
                    <? else: ?>
                        <label class="checkbox">
                            <input name="kioskAccess" type="checkbox" value="1"> <?php echo __("Use this card to access retail locations (Open 24 Hrs)", "Use this card to access retail locations (Open 24 Hrs)"); ?>
                        </th>
                    <? endif ?>
                </td>
            </tr>
        <? endif; ?>
    </table>

    <input type="submit" class="button orange" value="<?php echo __("Authorize Credit Card", "Authorize Credit Card"); ?>" />
<?= form_close() ?>