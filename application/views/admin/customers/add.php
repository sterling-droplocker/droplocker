<?php

enter_translation_domain("admin/customers/add");

$all_states = get_all_states_by_country_as_array();

?>

<?php

$fieldList = array();
$fieldList['firstName'] = 
        "<tr>
            <th>" . __("first", "First Name") . "</th>
            <td><input type='text' name='firstName' /></td>
        </tr>";
            
$fieldList['lastName'] = 
        "<tr>
            <th>" . __("last", "Last Name") . "</th>
            <td><input type='text' name='lastName'/></td>
        </tr>";
            
$nameForm = getSortedNameForm($fieldList);

//------------------

$fieldList = array();
$fieldList['address1'] = 
        "<tr>
            <th>" . __("address", "Address") . "</th>
            <td><input type='text' name='address1' /></td>
        </tr>";
            
$fieldList['address2'] = 
        "<tr>
            <th>" . __("address_two", "Address 2") . "</th>
            <td><input type='text' name='address2' /></td>
        </tr>";

$fieldList['city'] = 
        "<tr>
            <th>" . __("city", "City") . "</th>
            <td><input type='text' name='city'/></td>
        </tr>";
            
$fieldList['state'] = 
        (isset($all_states[$this->business->country])) ? 
        "<tr>
            <th>" . __("state", "State") . "</th>
            <td>" . form_dropdown("state", $all_states[$this->business->country]) . "</td>
        </tr>"
        :
        "<tr>
            <th>" . __("locality", "Locality") . "</th>
            <td><input type='text' name='state' id='state' /></td>
        </tr>";

$fieldList['zip'] = 
        "<tr>
            <th>" . __("zipcode", "Zipcode") . "</th>
            <td><input type='text' name='zip' /></td>
        </tr>";
        
$addressForm = getSortedAddressForm($fieldList);

?>

<h2><?= __("customer_create", "Customer Create") ?></h2>
<form id="customer_add_form" action="/admin/customers/add_submit" method="post">
    <br>
    <table class="detail_table">
    <tr>
        <tr>
            <th><?= __("email", "*Email") ?></th>
            <td><input type='text' name='email'/></td>
        </tr>
        <tr>
            <th><?= __("password", "*Password") ?></th>
            <td><input type='text' name='password' value="" /></td>
        </tr>
        
        <?php echo $nameForm; ?>
        
        <tr>
            <th><?= __("phone", "Phone") ?></th>
            <td><input type='text' name='phone'/></td>
        </tr>
        <tr>
            <th><?= __("sms", "SMS") ?></th>
            <td><input type='text' name='sms'/></td>
        </tr>
        
        <?php echo $addressForm; ?>
        <tr>
            <th><?= __("company", "Company") ?></th>
            <td><input type='text' name='company'/></td>
        </tr>
        <tr>
            <th><?= __("default_location", "Default Location") ?></th>
            <td>
                <?= form_dropdown("location_id", $locations) ?>
            </td>
        </tr>
        <tr>
            <th><?= __("how_they_know", "How They Found Us") ?></th>
            <td><?= form_input('how'); ?></td>
        </tr>
        <tr>
            <th><?= __("promo", "Promo Code") ?></th>
            <td><?= form_input('promo'); ?></td>
        </tr>
        <tr>
            <th><?= __("default_language", "Default Language") ?></th>
            <td><?= form_dropdown("default_business_language_id", $business_languages_dropdown, $selected_business_language_id, "id='business_language_menu'"); ?></td>
        </tr>
        <tr>
            <th><?= __("welcome_email", "Send Welcome Email") ?></th>
            <td><input type="radio" name="welcomeEmail" value="1" checked>Yes &nbsp;&nbsp;<input type="radio" name="welcomeEmail" value="0">No
                <br><br>
                <p><?= __("email_temp", "Email template support merge values. Merge values are enclosed in percent symbols.<br>Example: %customer_username%.<br>%link%Click here%/link% to open a window with a complete list of values", array(
                        "link" => "<a href='javascript:;' onclick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' />",
                        "/link" => "</a>"
                    )) ?>
                </p>

                <div>
                    <label for="business_language_menu"><?= __("Language", "Language") ?></label>
                    <?= form_dropdown("business_language_id", $business_languages_dropdown, $selected_business_language_id, "id='business_language_menu'") ?>
                </div>
                <br>
                <div>
                    <label for="emailSubject"><?= __("subject", "Subject") ?></label>
                    <input type="text" name="emailSubject" id="emailSubject" value="">
                </div>
                <br>
                <div>
                    <label for="body"><?= __("body", "Body") ?></label><br>
                    <textarea id='body' name="emailText" cols="84" rows="10"></textarea>
                </div>

                <br><br>
                <div>
                    <label for="bodytext"><?= __("sms_text", "SMS Text") ?></label><br>
                    <textarea id='bodytext' name="smsText" cols="84" rows="4"></textarea>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <p><?= __("required", "* Required Fields") ?></p>
    <input type='submit' class='button orange' value='Create' name='submit' />
</form>

<script type="text/javascript">
var emailTemplates_by_business_language_for_action = <?=json_encode($emailTemplates_by_business_language_for_action)?>;


/**
 * @param object $incoming_emailSubject must be text input
 * @param object $incoming_emailText must be textarea
 * @param object $incoming_bodyText must be textarea
 * @param object $incoming_business_languageID must be form hidden input
 * @param object $incoming_submit_button must be input type button
 * @returns {EmailTemplateManager}
 */
var EmailTemplateManager = function($incoming_emailSubject, $incoming_emailText, $incoming_bodyText, $incoming_business_languageID, $incoming_submit_button) {
    this.$emailSubject = $incoming_emailSubject;
    this.$emailText = $incoming_emailText;
    this.$emailText.attr("id", "emailText");
    CKEDITOR.replace(this.$emailText.attr("id"), {
        uiColor : false,
        toolbar : 'MyToolbar',
        fullPage: true,
        allowedContent: true
    });

    this.$bodyText = $incoming_bodyText;

    this.$submit_button = $incoming_submit_button;
    this.$submit_button.loading_indicator();
    this.$business_languageID = $incoming_business_languageID;

    /**
     *
     * @param string incoming_emailSubject
     * @param string incoming_emailText
     * @param string incoming_bodyText
     * @param int incoming_business_languageID
     * @returns {undefined}
     */
    this.update = function(incoming_emailSubject, incoming_emailText, incoming_bodyText, incoming_business_languageID) {
        this.$emailSubject.val(incoming_emailSubject);

        CKEDITOR.instances[this.$emailText.attr("id")].destroy();

        this.$emailText.val(incoming_emailText);

        CKEDITOR.replace(this.$emailText.attr("id"), {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });

        this.$bodyText.val(incoming_bodyText);
        this.$business_languageID.val(incoming_business_languageID);
    }
}

$(document).ready(function() {
    if ($("#customer_add_form").length > 0) {
        var $emailTemplate_form = $("#customer_add_form");
        var $business_language_menu = $("#business_language_menu");

        var emailTemplate = emailTemplates_by_business_language_for_action[$business_language_menu.val()];

        var $emailSubject = $emailTemplate_form.find("[name='emailSubject']");
        var $emailText = $emailTemplate_form.find("[name='emailText']");
        var $bodyText = $emailTemplate_form.find("[name='bodyText']");

        if (typeof emailTemplate !== "undefined") {
            $emailSubject.val(emailTemplate['emailSubject']);
            $emailText.val(emailTemplate['emailText']);
            $bodyText.val(emailTemplate['bodyText']);
        }

        var $submit_button = $emailTemplate_form.find(":submit");
        var $hidden_business_language_id_input = $emailTemplate_form.find("[name='business_language_id']");

        var emailTemplate_management_interface = new EmailTemplateManager($emailSubject, $emailText, $bodyText, $hidden_business_language_id_input, $submit_button);

        $business_language_menu.change(function() {
            var emailTemplate = emailTemplates_by_business_language_for_action[$(this).val()];
            if (typeof emailTemplate === "undefined") {
                emailTemplate_management_interface.update("", "", "", $(this).val());
            }
            else {
                emailTemplate_management_interface.update(emailTemplate['emailSubject'], emailTemplate['emailText'], emailTemplate['bodyText'], $(this).val());
            }
        });
    }
    else {
        // The following statement initializes the old * interface for managing an emplte for a business with no defined business languages.
        CKEDITOR.replace( 'body', {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });
    }
});

</script>
