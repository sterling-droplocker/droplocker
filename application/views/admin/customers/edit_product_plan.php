<?php
enter_translation_domain("admin/customers/product_plans");
/** 
 * Note, this view expects the following content variables to be set
 *  businessLaundryPlan \App\Libraries\DroplockerObjects\BusinessLaundryPlan 
 *  bizzie_mode bool
 *  title string
 *  is_master_business_laundry_plan bool
 */
if (!isset($is_master_business_product_plan)) {
    $is_master_business_product_plan = false;
}
?>
<script src="/js/plugins/select2/select2.min.js"></script>
<link href="/js/plugins/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function(){
    $( "#datepicker" ).datepicker({
        showOn: "button",
        buttonImage: "/images/icons/calendar.png",
        buttonImageOnly: true,
        altField: "#endDate",
        altFormat: "yy-mm-d",
        dateFormat: "MM d, yy"
    });
    
    $("#add_businessLaundryPlan_form").validate( {
        rules: {
           name : {
              required: true
           },
           price : {
              required: true,
              number: true
           },
           credit_amount : {
              required: true,
              number: true
           }
        },
        errorClass : "invalid"
    });

    $("#available_products_ids").select2({
        placeholder: "Select multiple products"
    });
});
</script>
<style type="text/css">
    .select2-dropdown {
        z-index: 100000;
    }
</style>

<h2> <?= $title ?> </h2>

<p><a id='add_plan_button' href='/admin/customers/product_plans'><?= __("go_back", "Go back") ?></a></p>

<?= $this->session->flashdata('message');?>

<div>
    <form id="add_businessLaundryPlan_form" action='/admin/customers/edit_product_plan/<?php echo $businessProductPlan->businessLaundryPlanID; ?>' method='post'>
        <table class="detail_table">
            <tr>
                <th>*<?= __("plan_name", "Plan Name") ?></th>
                <td>
                    <input type='text' name='name' value="<?php echo $businessProductPlan->name; ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("caption", "Caption") ?></th>
                <td>
                    <input type='text' name='caption' value="<?php echo $businessProductPlan->caption; ?>"  />
                </td>
            </tr>
            <tr>
                <th><?= __("group", "Group") ?></th>
                <td>
                    <input type='text' name='grouping' value="<?php echo $businessProductPlan->grouping; ?>"  />
                </td>
            </tr>
            <tr>
                <th><?= __("description", "Description") ?></th>
                <td>
                    <textarea name='description' style='width:98%;border:1px solid #ccc;' rows="2"><?php echo $businessProductPlan->description; ?></textarea>
                </td>
            </tr>
            <tr>
                <th>*<?= __("price", "Price") ?></th>
                <td>
                    <input type='text' name='price' style='width:50px;'  value="<?php echo $businessProductPlan->price; ?>" />
                </td>
            </tr>
            <tr>
                <th>*<?= __("allowed_items_of_clothing", "Allowed items of clothing") ?></th>
                <td>
                    <input type='text' name='credit_amount' value="<?php echo (int)$businessProductPlan->credit_amount; ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= __("notes", "Notes") ?></th>
                <td>
                    <input type='text' name='poundsDescription'  value="<?php echo $businessProductPlan->poundsDescription; ?>" /><br>
                <?= __("example", "example (About 2-4 Orders Per Semester)") ?>
                </td>
            </tr>
            <tr>
                <th><?= __("days", "Days") ?></th>
                <td>
                    <input type='text' name='days' style='width:50px;'  value="<?php echo $businessProductPlan->days; ?>" /> 
                    <?= __("days_tip", "(Optional) if you set an end date, days will not be used") ?>
                </td>
            </tr>
            <tr>
                <th><?= __("end_date", "End Date") ?></th>
                <td>
                    <input type='text' id='datepicker' style='width:200px;'  value="<?php echo $businessProductPlan->endDate; ?>" />
                    <input type='hidden' id='endDate' name='endDate' ;'  value="<?php echo $businessProductPlan->endDate; ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("rollover_items_of_clothing", "Rollover items of clothing") ?></th>
                <td>
                    <input type='text' name='rollover' value="<?php echo $businessProductPlan->rollover; ?>" style='width:50px;' />
                </td>
            </tr>
            <tr>
                <th><?= __("visible", "Visible") ?></th>
                <td>
                    <input type='hidden' name='visible' value="0" />
                    <input type='checkbox' name='visible' value="1" checked />
                    <?= __("visible_help", "If disabled, won't be listed in product plans.") ?>
                    
                </td>
            </tr>
            <tr>
                <th><?= __("expired_price", "Discount After Plan Expires") ?></th>
                <td>
                    <input type='text' name='expired_discount_percent' style='width:50px;'  value="<?php echo $businessProductPlan->expired_discount_percent; ?>" />%
                </td>
            </tr>
            <tr>
                <th><?= __("available_products", "Available products") ?></th>
                    <td>
                        <select name="available_products_ids[]" id="available_products_ids" multiple="multiple">
                        <?php 
                        foreach ($products as $k=>$b) {
                            $selected = "";
                            if (in_array($k, $businessProductPlan->selected_products)) $selected = 'selected="selected"';
                        ?>
                           <option value="<?php echo $k ?>" <?php echo $selected ?>>
                                <?php echo $b ?>
                           </option> 
                        <?php
                        }
                        ?>
                        </select>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Update existing customer plans when plan details change", "Update existing customer plans when plan details change"); ?></th>
                <td>
                    <?= form_yes_no('updateExistingCustomerPlansWhenPlanChange', $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange); ?>
                </td>
            </tr>
        </table>
            <input type='submit' value='Update' class='button orange' /><br><br>
    </form>
    <p> <?= __("required", "* Required Fields") ?> </p>
</div>
