<?php

enter_translation_domain("admin/customers/detail");

$dateFormat =  get_business_meta($this->business_id, "shortDateDisplayFormat");
$fullDateFormat =  get_business_meta($this->business_id, "fullDateDisplayFormat");
?>
<script type="text/javascript">
$(document).ready(function() {

$(":submit").loading_indicator();

$('#go_button').click(function() {
    // if "Create Package order", show dialog
    if ($("#go_select").val() == "create_package_order") {
        $("#create_package_order_form_container").dialog({
            modal: true,
            title: "<?= __("create_package", "Create Package Order") ?>",
            buttons: {
                "<?= __("ok_btn", "Ok") ?>" : function() {
                    $("#create_package_order_form_container form").submit();
                    $(this).dialog("close");
                },
                "<?= __("cancel_btn", "Cancel") ?>" : function() {
                    $(this).dialog("close");
                }
            }
        });
    } else {
        window.open($("#go_select").val());
    }
});

<? if (in_bizzie_mode()): ?>
    $(".delete").click(function() {
        return confirm("<?= __("confirm_disable", "Are you sure you want to disable this customer?") ?>");
    });
<? else: ?>
    $(".delete").click(function() {
        return confirm("<?= __("confirm_delete", "Are you sure you want to delete this customer?") ?>");
    });
<? endif ?>

    $("#change_btn").click(function() {
        top.location.href='/admin/customers/detail/'+$("#customer_id").val();
    });

    $(".delete_bag").click(function() {
        return confirm("<?= __("confirm_delete_bag", "Are you sure you want to reassign this bag to the blank customer?") ?>");
    });

    $("#orders_history").dataTable({  
        "iDisplayLength": 20, 
        "bProcessing" : true,
        "bServerSide" : true,
        "bStateSave" : true,
        "sAjaxSource" : "/admin/customers/detail/<?= $customer->customerID?>",
        "fnPreDrawCallback": function( oSettings ) {
            oSettings._iDisplayLength = 20;
        },
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "dateFormat", "value": "<?php echo $dateFormat ?>" } );
          aoData.push( { "name": "iDisplayLength", "value": 20 } );
        },
        "bJQueryUI": true,
        "aaSorting" : [[ 1, "desc"]],
        "sDom": 'tr<"F"ip>',
        "aoColumns": [
            { "mDataProp":"orderLink" },
            { "mDataProp":"order_type" },
            { "mDataProp":"dateCreated" },
            { "mDataProp":"bagNumber" },
            { "mDataProp":"orderPaymentStatusOptionIcon" },
            { "mDataProp":"name" }
        ]
    });
});
</script>


<h2><?= __("customer_details", "Customer Details") ?></h2>
<?= $this->session->flashdata("static_message") ?>
<div class='orange_box' style='padding:5px;'>
    <div style="float:left;">
        <?= __("change", "Change Customer:") ?>
        <input type='text' name='customer' id='customer' value="<?= getPersonName($customer) ?>" />
        <div id='customer_results'></div>
    </div>
    <div style="float:left; padding-left: 20px;">
        <input type='hidden' id='customer_id' name='customer_id' value='customer_id' />
        <input type='button' id='change_btn' value='<?= __("btn_change", "Change") ?>' />
    </div>
    <div style='float:right'>
        <select id='go_select'>
            <option value='/admin/customers/switch_to_customer/<?= $customer->customerID?>'><?= __("login", "Login as Customer") ?></option>
            <option value='/admin/customers/discounts/<?= $customer->customerID?>'><?= __("view_discounts", "View Discounts") ?></option>
            <option value='create_package_order'><?= __("create_package", "Create Package Order") ?></option>
            <option value='/admin/customers/billing/<?= $customer->customerID?>'><?= __("edit_credit_card", "Edit Credit Card") ?></option>
            <? if($customer->invoice  == '1'): ?>
                <option value='/admin/customers/setupInvoicing/<?= $customer->customerID?>'><?= __("setup_invoicing", "Setup Invoicing") ?></option>
            <? endif; ?>
        </select>
        <input type='button' id='go_button' value='<?= __("btn_go", "Go") ?>' />
    </div>
    <div style="clear:both;"></div>
</div>
<br />
<div style='position:relative; width:100%'>
    <div style='float:left; width:100%'>
        <div style='margin-right:320px;'>
            <div style='float:left'>
                <a title=<?= __("view_as_closet", "View as Closet") ?> href="/admin/orders/view_order_as_closet/<?= $customer->customerID ?>"> <img src="/images/icons/camera.png" /> </a>
                <a title="<?= __("edit_customer_details", "Edit Customer Details") ?>" href='/admin/customers/edit/<?= $customer->customerID?>'><img src='/images/icons/application_edit.png' /></a>
                <?php if($customer->sms): ?>
                    <a title="<?= __("send_customer_sms", "Send sms to customer") ?>" href='/admin/customer_sms/new_sms/<?= $customer->customerID?>'><img src='/images/icons/comment_edit.png' /></a>
                <?php endif ?>
            </div>
            <h3 style="display: inline;">&nbsp;&nbsp;&nbsp;<?= getPersonName($customer) ?></h3>
            <!-- The following conditional displays the delete button only if the customer has no orders. -->
            <?php if ($order_count == 0): ?>
                <?php if (in_bizzie_mode()): ?>
                    <a class="delete" style="float: right;" title="<?= __("disable_customer", "Disable Customer") ?>" href="/admin/customers/disable/<?=$customer->customerID?>"><img src="/images/icons/cross.png" /></a>
                <?php else: ?>
                    <a class="delete" style="float: right;" title="<?= __("delete_customer", "Delete Customer") ?>" href="/admin/customers/delete/<?=$customer->customerID?>"><img src="/images/icons/cross.png" /></a>
                <?php endif ?>
            <?php endif; ?>

            <form action="/admin/customers/update_notes/" method="post">
                <table id='box-table-a'  style="margin-top:5px;">
                    <tr>
                        <td style="border-top:1px #CCCCCC solid;"><?= __("email", "Email") ?></td>
                        <td style="border-top:1px #CCCCCC solid;"><a href="mailto:<?= $customer->email ?>"><?= $customer->email?></a>
                        <? if($customer->noEmail==1):?>
                                <div class='error' style='padding:5px;margin:5px 0px;'><?= __("no_marketing_email", "No Marketing Email") ?></div>
                            <? endif; ?>
                            <? if($customer->bounced==1):?>
                                <div class='error' style='padding:5px;margin:5px 0px;'><?= __("email_bounced", "Email Bounced") ?> (<a href='/admin/customers/edit/<?=$customer->customerID?>'>Edit the customer</a> to remove block)</div>
                            <? endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= __("phone", "Phone") ?></td>
                        <td><?= $customer->phone?></td>
                    </tr>
                    <tr>
                        <td><?= __("address", "Address") ?></td>
                        <td><?= $customer->address1?></td>
                    </tr>
                    <tr>
                        <td><?= __("address_two", "Address 2") ?></td>
                        <td><?= $customer->address2?></td>
                    </tr>
                    <tr>
                        <td><?= __("company", "Company") ?></td>
                        <td><?= get_customer_meta($customer->customerID, 'company', ''); ?></td>
                    </tr>
                        <input type="hidden" name="customer_id" value="<?= $customer->customerID ?>">
                    <tr>
                        <td><?= __("customer_notes", "Customer Notes") ?><br>
                            <span style="font-weight: bold; color: Red;"><?= __("visible_to_customers", "Visible to<br>customers") ?></span>
                        </td>
                        <td><textarea cols="55" rows="3" name="customerNotes"><?= $customer->customerNotes?></textarea></td>
                    </tr>
                    <tr>
                        <td><?= __("internal_notes", "Internal Notes") ?></td>
                        <td><textarea cols="55" rows="3" name="internalNotes"><?= $customer->internalNotes ?></textarea></td>
                    </tr>
                    <tr>
                        <td><?= __("log_a_call", "Log a Call") ?></td>
                        <td><textarea cols="55" rows="2" name="callLog"></textarea></td>
                    </tr>
                    <tr>
                        <td><?= __("permanent", "Permanent Assembly Notes") ?></td>
                        <td><input type="text" name="permAssemblyNotes" value="<?= $customer->permAssemblyNotes?>" size="70"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="float:left">
                                <a class="orange button" href="/admin/customers/assign_new_bag_upon_new_order?customerID=<?=$customer->customerID?>"><?= __("new_bag", "Give New Bag With Next Order") ?></a>
                            </div>
                            <div style="float:right">
                                <? $disabled = is_resource_allowed("prevent_order_check_in") ? "" : "disabled='disabled'"; ?>
                                <?= form_checkbox("hold", 1, $customer->hold, $disabled) ?>
                                <?= __("prevent_order", "Prevent Order Check-In") ?>
                                <input style='margin-left:25px;' type="submit" class="button orange" name="update" value="<?= __("update_notes", "Update Notes") ?>" />
                            </div>
                        </td>
                    </tr>
                </table>
            </form>

            <table class='box-table-a'>
                <tr>
                    <td><?= __("orders", "Orders:") ?><br> <?= $custTotals ? $custTotals[0]->orderCount : 0?> </td>
                    <td><?= __("vists", "Visits:") ?><br> <?= $custTotals ? $custTotals[0]->visits : 0 ?> </td>
                    <td> Ords / Visit:<br> <?= $custTotals ? number_format_locale($custTotals[0]->orderCount/max($custTotals[0]->visits, 1), 1) : 0 ?> </td>
                    <td> Gross:<br> <?= $custTotals ? format_money_symbol($this->business_id, '%.2n',  $custTotals[0]->totGross) : 0?> </td>
                    <td> Discounts:<br> <?= $custTotals ? format_money_symbol($this->business_id, '%.2n', $custTotals[0]->totDisc) : 0?> </td>
                    <td> Last Order:<br> <?= $custTotals ? convert_from_gmt_aprax($custTotals[0]->lastOrder, $dateFormat) : '' ?> </td>
                    <td> Avg Ord Lapse:<br><?= $custTotals ? number_format_locale($custTotals[0]->avgOrderLapse,0) : 0 ?> days </td>
                </tr>
            </table>

            <table class='box-table-a'>
                <tr>
                    <td width="33%">Default Location: <?= $defaultLocation->address ?></td>
                    <td width="33%">Signup Date:
                        <?= ($customer->signupDate instanceOf \DateTime) ?
                            $customer->signupDate->format("M j, Y") : "&lt;No Date&gt;"; ?>
                    </td>
                    <td>CC:
                        <?if($cc): ?>
                        <?= $cc[0]->cardNumber?>&nbsp;&nbsp;&nbsp;Exp: <?= $cc[0]->expMo?>/<?= $cc[0]->expYr?>
                        <? else: echo "No credit card on File"; endif; ?>
                        <a style="float:right;" href="/admin/customers/billing/<?= $customer->customerID ?>"><img src="/images/icons/pencil.png"></a>
                    </td>
                    <?if($customer->customer_deliver_on_hold): ?>
                    <td>
                      <span style="color:red;">- Deliver without payment</span>                   
                    </td>
                    <?endif;?>
                </tr>
            </table>

            <table class='box-table-a'>
                <tr>
                    <td width="33%">How: <?= $customer->how ?></td>
                    <td width="33%">Created Account: <?= ucwords($customer->signup_method) ?></td>
                    <td >Referred By: <? if (!empty($referral)): ?><?= getPersonName($referral) ?><? endif; ?></td>
                </tr>
            </table>

            <table class='box-table-a'>
                <tr>
                    <td width="100"><strong>Bags</strong></td>
                    <td>


                        <?php if($bags): ?>
                            <?php foreach($bags as $bag):?>
                                <?php if ($customer->invoice == 1): ?>
                                    <a href="/admin/bags/edit/<?=$bag->bagID?>">
                                <?php endif ?>
                                    <?php if ($bag->printed == 1): ?>
                                        <span style="font-weight: bold;"> <?=$bag->bagNumber?> </span>
                                    <?php else: ?>
                                        <?= $bag->bagNumber ?>
                                    <?php endif ?>
                                <?php if ($customer->invoice == 1): ?>
                                    </a>
                                <?php endif ?>
                                <a class='delete_bag' id='delete-<?=$bag->bagID?>' href='/admin/bags/delete_bag?bagID=<?=$bag->bagID?>'> <img alt='Reassign Bag to Blank Customer' src='/images/icons/cross.png' /> </a>
                            <?php endforeach;?>
                        <?php else : ?>
                            No Bags
                        <?php endif; ?>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <form action="/admin/bags/add_bag_to_customer" method="post">
                            <?= form_label("Bag Number", "bagNumber") ?>
                            <?= form_hidden('customer_id', $customer->customerID) ?>
                            <input type='text' name='bagNumber' style='width:150px;' />
                            <?php
                                if ($customer->invoice == 1)
                                {
                                    echo form_label("Notes", "notes");
                                    echo form_input("notes");
                                }
                            ?>
                            <input type='submit' class="button orange" id='bag_button' value='Add Bag' />
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>Delivery Routes</td>
                    <td>
                        <a style="float:right" href="/admin/customers/edit_subscriptions/<?= $customer->customerID ?>"><img src="/images/icons/pencil.png"></a>
                        <? if ($home_delivery_subscriptions): ?>
                            <? foreach($home_delivery_subscriptions as $subscriptionID => $subscription): ?>
                                <?= $subscription ?><br>
                            <? endforeach; ?>
                        <? else: ?>
                            None
                        <? endif; ?>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <h3>Notes History</h3>
            <table class='box-table-a'>
                <thead>
                    <tr>
                        <th>Note</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Employee</th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if($history):
                            foreach($history as $history_item):?>
                                <tr>
                                        <td><?= $history_item->note ?></td>
                                        <td><?= $history_item->historyType ?></td>
                                        <td><?= convert_from_gmt_aprax($history_item->timestamp, $fullDateFormat)?></td>
                                        <td><?= getPersonName($history_item) ?></td>
                                </tr>
                                <? endforeach;
                        else: ?>
                            <tr><td colspan='4'>No History</td></tr>
                        <?endif;?>
                    </tbody>
                </table>
                <br>
                <h3> Followup Call History </h3>

                    <table class="box-table-a">
                        <thead>
                            <tr>
                                <th> Date Called </th>
                                <th> Employee </th>
                                <th> Call Type </th>
                                <th> Note </th>
                        </thead>
                        <tbody>
                            <?php if ($followupCalls): ?>
                                <?php foreach($followupCalls as $followupCall): ?>
                                    <tr>
                                        <td><?= $followupCall->dateCalled ?></td>
                                        <td><?= getPersonName($followupCall) ?> </td>
                                        <td><?= $followupCall->call_type ?></td>
                                        <td><?= $followupCall->note ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan='4'>No History</td>
                                </tr>
                            <?php endif ?>
                </tbody>
            </table>
            <br>
            <h3>Email History</h3>
            <table class='box-table-a'>
                <thead>
                    <tr>
                    <th>Subject</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <? if($emails):
                        foreach($emails as $email):?>
                            <tr>
                                <td><a target='_blank' href='/admin/customers/email_detail/<?= $email->emailID?>'><?= strip_tags(empty($email->subject)?"(SMS) ".substr($email->body, 0, 300):$email->subject); ?></a></td>
                                <td><?= convert_from_gmt_aprax($email->sendDateTime, $fullDateFormat) ?></td>
                                <td><?= $email->status?></td>
                                <td><a href="/admin/customers/resend_customer_email/<?=$email->emailID;?>" id="email_<?=$email->emailID;?>" onclick="confirm('Resend this email?');">Resend</a></td>
                            </tr>
                            <? endforeach;
                    else: ?>
                        <tr><td colspan='4'>No History</td></tr>
                    <?endif;?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>Total Emails: <?= number_format_locale($emailCount, 0)?></td>
                        <td colspan='3'>
                        <?if($emailCount>10): ?>
                            <a href='/admin/customers/list_emails/<?php echo $customer->customerID ?>'>View all Emails</a>
                        <?endif; ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div style='float:right; position:absolute; right:10px; width:300px;'>
        <div style="float:left;"><h3>Order History</h3></div>
        <div style="float:right;"><a href="/admin/reports/customer_order_breakdown/<?= $customer->customerID ?>" target="_blank">Order Breakdown</a></div>

        <table class='box-table-a' id='orders_history'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th> Order Type </th>
                    <th>Date</th>
                    <th>Bag</th>
                    <th>Captured </th>
                    <th>Status </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <form action='/admin/customers/toggleLockerLoot' method='post'>
            <table class='detail_table'>
                <tr>
                    <th>Reward Program</th>
                    <td>
                    <?php if($rewardsViolation == 1): ?>
                    <input type='submit' class='button red' value='Turn On Rewards' name='rewardSubmit' />
                    <input type='hidden' value='ON' name='status' />
                    <?php else : ?>
                    <input type='submit' class='button red' value='Turn Off Rewards' name='rewardSubmit' />
                    <input type='hidden' value='OFF' name='status' />
                    <?php endif; ?>
                    <input type='hidden' value='<?php echo $customer->customerID?>' name='customer_id' />
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>

<div id="create_package_order_form_container" style="display: none;">
    <?= form_open("/admin/customers/create_package_order/{$customer->customerID}/{$customer->location_id}") ?>
    <?= form_label("Tracking Number", "tracking_number") ?>
    <?= form_input(array("name" => "tracking_number", "id" => "tracking_number")) ?>
    <?= form_close() ?>
</div>
