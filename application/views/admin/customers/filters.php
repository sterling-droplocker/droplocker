<?php enter_translation_domain("admin/customers/filters"); ?>
<h2><?php echo __("Customer Filters", "Customer Filters"); ?></h2>
<br>
<? if(isset($query)): ?>
    <strong>Query:</strong> <?= $queryToShow ?><br><br>
    <strong>Showing <?= $firstRecord ?> - <?= $lastRecord ?> / <?= $total ?> Results</strong>
    <?php if (!empty($id)): ?>
    <?php $this->load->view('admin/customers/common/apply_coupon_to_all_js'); ?>
    <a href="/admin/customers/filter_data_dump/<?= $id ?>" title="Export to CSV">
    <img style="border:none;vertical-align:middle;" src="/images/icons/csv.png" alt="Export to CSV">
    </a>
    <?= form_open("/admin/coupons/apply_to_all_customers", array("style" => "padding-top: 10px;")) ?>
        <label><?= __("coupon", "Coupon:") ?></label>
        <?= form_dropdown("couponID", $coupons) ?>
        <input type="hidden" name="filterID" value="<?=$filterID; ?>" />
        <input type="button" value="<?= __("apply_to_all", "Apply to all") ?>" class="button blue" id="apply_to_all">
    <?= form_close() ?>
    <?php endif; ?>
    <br><br>
    <?php if (empty($id)): ?>
    <form action="/admin/customers/add_filter" method="post">
    <input type="hidden" name="query" value="<?= $query ?>">
    <?php echo __("Name this filter", "Name this filter:"); ?> <input type="text" name="name">
    <input type="submit" name="CreateFilter" value="<?php echo __("Create Filter", "Create Filter:"); ?>" class="button blue">
    </form>
    <?php endif; ?>
    <br>
    <table id="result" class="box-table-a">
	<thead>
            <tr>
                <? foreach($fields as $key => $value): ?>
                <th><?= $key ?></th>
                <? endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <? foreach($results as $result): ?>
                <tr>
                    <? foreach($results[0] as $key => $value): ?>
                    <? if ($key == 'preferences'): ?>
                    <td><?= json_encode(unserialize($result->$key))  ?></ul>
                    </td>
                    <? else: ?>
                    <td><?= $result->$key ?></td>
                    <? endif; ?>
                    <? endforeach; ?>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="<?=count($fields)?>"><?= $paging?></td>
        </tr>
    </tfoot>
    </table>
<? else: ?>
    <form action="/admin/customers/filters" method="post">
        <table class="filterParameters">
            <tr>
                <td>
                <select class="field" data-name="field[]">
                    <? foreach($fields as $key => $value): ?>
                        <option value="<?= $key ?>"><?= $key ?> [<?= substr($value, 0, 30) ?>]</option>
                    <? endforeach; ?>
                </select>
                </td>
                <td>
                <select class="field" data-name="comparison[]">
                    <? foreach ($comparators as $key => $comparator): ?>
                    <option value="<?= $key ?>"><?= htmlentities($comparator); ?></option>
                    <? endforeach; ?>
                </select>
                </td>
                <td>
                    <input class="field" type="text" data-name="amount[]">
                </td>
                <td>
                    <a href="#" class="addFilterParameter"><img src="/images/icons/add.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td>
                    <input type="submit" name="Run" value="<?php echo __("See Results", "See Results"); ?>" class="button orange">
                </td>
            </tr>
        </table>
    </form>
    <div class="filtersTpl" style="display:none;">
    <table>
    <tr>
        <td>
            <select name="field[]">
                <? foreach($fields as $key => $value): ?>
                <option value="<?= $key ?>"><?= $key ?> [<?= substr($value, 0, 30) ?>]</option>
            <? endforeach; ?>
            </select>
        </td>
        <td>
            <select name="comparison[]">
                    <? foreach ($comparators as $key => $comparator): ?>
                    <option value="<?= $key ?>"><?= htmlentities($comparator); ?></option>
                    <? endforeach; ?>
            </select>
        </td>
        <td>
            <input type="text" name="amount[]" />
        </td>
        <td><a href="#" class="removeFilterParameter"><img src="/images/icons/cross.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td></td>
    </tr>
    </table>
    </div>
    <script type="text/javascript">
        $('#toggle_all').click(function(evt) {
                evt.preventDefault();
                var selected = $('#field_list input:checked').length > 0;
                $('#field_list input').attr('checked', !selected);
        });

        var filtersCount = 0;

        $("body").on('click', '.removeFilterParameter', function(evt) {
                evt.preventDefault();
                $(this).closest("tr").remove();
                filtersCount--;
        });

        $('.addFilterParameter').on('click', function(evt) {
                evt.preventDefault();
                
                if (filtersCount > 7) {
                    alert("You can't add more than 8 parameters");
                    return;
                }

                $( ".filtersTpl tr" ).attr("id", "filter_"+filtersCount);
                $( ".filtersTpl tr" ).clone().appendTo(".filterParameters");
                
                $(".field").each(function(){
                    var name = $(this).attr("data-name");
                    $('#filter_'+filtersCount+' input[name="'+name+'"]').val($(this).val()); 
                    $('#filter_'+filtersCount+' select[name="'+name+'"]').val($(this).val()); 

                    //reset field
                    $(this).val(''); 
                });

                filtersCount++;
        });
    </script>

    <br>
    <br>
    <table id="box-table-a">
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <th><?php echo __("Query", "Query"); ?></th>
            <th><?php echo __("View", "View"); ?></th>
            <th><?php echo __("Delete", "Delete"); ?></th>
        </tr>
        <? foreach($filters as $filter): ?>
            <tr>
                <td><?= $filter->name ?></td>
                <td><?= $filter->query ?></td>
                <td><a href="/admin/customers/view_filter/<?= $filter->filterID?>" title="check results"><img src='/images/icons/eye.png' /></a></td>
                <td><a href="/admin/customers/delete_filter?id=<?= $filter->filterID?>"><img src='/images/icons/cross.png' /></a></td>
            </tr>
        <? endforeach; ?>
    </table>
<? endif; ?>
