<?php enter_translation_domain("admin/customers/import"); ?>
<h2><?php echo __("Import Customers", "Import Customers"); ?></h2>

<form id="customer_import_form" action="/admin/customers/import" method="post" enctype="multipart/form-data">
    <br>
    <table class="detail_table">
    <tr>
        <tr>
            <th><?php echo __("Upload CSV File", "Upload CSV File"); ?></th>
            <td><input type='file' name='file'/></td>
        </tr>
        <tr>
            <th><?php echo __("or Paste CSV data", "or Paste CSV data"); ?></th>
            <td>
                <?= form_textarea('data', "email*,password*,firstName*,lastName*,phone,sms,address1,address2,city,state,zip\n") ?>
                <div style="margin-top: 1em;">
                <?php echo __("Columns marked are required", "Columns marked with * are required"); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default Location", "Default Location"); ?></th>
            <td>
                <?= form_dropdown("location_id", $locations) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("How They Found Us", "How They Found Us"); ?></th>
            <td><?= form_input('how'); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Promo Code", "Promo Code"); ?></th>
            <td><?= form_input('promo'); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Default Language", "Default Language"); ?></th>
            <td><?= form_dropdown("default_business_language_id", $business_languages_dropdown, $selected_business_language_id, "id='business_language_menu'"); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Send Welcome Email", "Send Welcome Email"); ?></th>
            <td><input type="radio" name="welcomeEmail" value="1" checked>Yes &nbsp;&nbsp;<input type="radio" name="welcomeEmail" value="0">No
                <br><br>
                <p><?php echo __("Email template support merge values", "Email template support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?></p>

                <div><?php echo __("Subject", "Subject"); ?></div>
                <input type="text" name="emailSubject" value="<?= htmlentities('')?>">
                <br>
                <div><?php echo __("Body", "Body"); ?></div>
                <textarea id='body' name="emailText" cols="84" rows="10"><?= htmlentities('')?></textarea>

                <br><br>
                <div><?php echo __("SMS Text", "SMS Text"); ?></div>
                <textarea id='bodytext' name="smsText" cols="84" rows="4"><?= htmlentities('')?></textarea>
            </td>
        </tr>
    </table>
    <br>
    <p> *<?php echo __("Required Fields", "Required Fields"); ?>Required Fields </p>
    <input type='submit' class='button orange' value='<?php echo __("Import", "Import"); ?>' name='submit' />
</form>

<script type="text/javascript">
var emailTemplates_by_business_language_for_action = <?=json_encode($emailTemplates_by_business_language_for_action)?>;


/**
 * @param object $incoming_emailSubject must be text input
 * @param object $incoming_emailText must be textarea
 * @param object $incoming_bodyText must be textarea
 * @param object $incoming_business_languageID must be form hidden input
 * @param object $incoming_submit_button must be input type button
 * @returns {EmailTemplateManager}
 */
var EmailTemplateManager = function($incoming_emailSubject, $incoming_emailText, $incoming_bodyText, $incoming_business_languageID, $incoming_submit_button) {
    this.$emailSubject = $incoming_emailSubject;
    this.$emailText = $incoming_emailText;
    this.$emailText.attr("id", "emailText");
    CKEDITOR.replace(this.$emailText.attr("id"), {
        uiColor : false,
        toolbar : 'MyToolbar',
        fullPage: true,
        allowedContent: true
    });

    this.$bodyText = $incoming_bodyText;

    this.$submit_button = $incoming_submit_button;
    this.$submit_button.loading_indicator();
    this.$business_languageID = $incoming_business_languageID;

    /**
     *
     * @param string incoming_emailSubject
     * @param string incoming_emailText
     * @param string incoming_bodyText
     * @param int incoming_business_languageID
     * @returns {undefined}
     */
    this.update = function(incoming_emailSubject, incoming_emailText, incoming_bodyText, incoming_business_languageID) {
        this.$emailSubject.val(incoming_emailSubject);

        CKEDITOR.instances[this.$emailText.attr("id")].destroy();

        this.$emailText.val(incoming_emailText);

        CKEDITOR.replace(this.$emailText.attr("id"), {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });

        this.$bodyText.val(incoming_bodyText);
        this.$business_languageID.val(incoming_business_languageID);
    }
}

$(document).ready(function() {
    if ($("#customer_import_form").length > 0) {
        var $emailTemplate_form = $("#customer_import_form");
        var $business_language_menu = $("#business_language_menu");

        var emailTemplate = emailTemplates_by_business_language_for_action[$business_language_menu.val()];

        var $emailSubject = $emailTemplate_form.find("[name='emailSubject']");
        var $emailText = $emailTemplate_form.find("[name='emailText']");
        var $bodyText = $emailTemplate_form.find("[name='bodyText']");

        if (typeof emailTemplate !== "undefined") {
            $emailSubject.val(emailTemplate['emailSubject']);
            $emailText.val(emailTemplate['emailText']);
            $bodyText.val(emailTemplate['bodyText']);
        }

        var $submit_button = $emailTemplate_form.find(":submit");
        var $hidden_business_language_id_input = $emailTemplate_form.find("[name='business_language_id']");

        var emailTemplate_management_interface = new EmailTemplateManager($emailSubject, $emailText, $bodyText, $hidden_business_language_id_input, $submit_button);

        $business_language_menu.change(function() {
            var emailTemplate = emailTemplates_by_business_language_for_action[$(this).val()];
            if (typeof emailTemplate === "undefined") {
                emailTemplate_management_interface.update("", "", "", $(this).val());
            }
            else {
                emailTemplate_management_interface.update(emailTemplate['emailSubject'], emailTemplate['emailText'], emailTemplate['bodyText'], $(this).val());
            }
        });
    }
    else {
        // The following statement initializes the old * interface for managing an emplte for a business with no defined business languages.
        CKEDITOR.replace( 'body', {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });
    }
});

</script>
