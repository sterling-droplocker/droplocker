<?php enter_translation_domain("admin/customers/edit"); ?>
<?php

$all_states = get_all_states_by_country_as_array();

?>
<?php

$fieldList = array();
$fieldList['firstName'] = 
        "<tr>
            <th>" . __('First Name', 'First Name') . "</th>
            <td><input type='text' name='firstName' value='" . ucwords($customer->firstName) . "' /></td>
        </tr>";
            
$fieldList['lastName'] = 
        "<tr>
            <th>" . __('Last Name', 'Last Name') . "</th>
            <td><input type='text' name='lastName' value='" . ucwords($customer->lastName) . "' /></td>
        </tr>";
            
$nameForm = getSortedNameForm($fieldList);

//------------------

$fieldList = array();
$fieldList['address1'] = 
        "<tr>
            <th>" . __("Address", "Address") . "</th>
            <td><input type='text' name='address1' value='{$customer->address1}' /></td>
        </tr>";
            
$fieldList['address2'] = 
        "<tr>
            <th>" . __("Address 2", "Address 2") . "</th>
            <td><input type='text' name='address2' value='{$customer->address2}' /></td>
        </tr>";

$fieldList['city'] = 
        "<tr>
            <th>" . __("City", "City") . "</th>
            <td><input type='text' name='city' value='{$customer->city}' /></td>
        </tr>";
            
$fieldList['state'] = 
        (isset($all_states[$this->business->country])) ? 
        "<tr>
            <th>" . __("State", "State") . "</th>
            <td>" . form_dropdown("state", $all_states[$this->business->country], $customer->state) . "</td>
        </tr>" 
        :
        "<tr>
            <th>" . __("Locality", "Locality") . "</th>
            <td><input type='text' name='state' id='state'  value='" . htmlentities($customer->state). "'  /></td>
        </tr>";

$fieldList['zip'] = 
        "<tr>
            <th>" . __("Zipcode", "Zipcode") . "</th>
            <td><input type='text' name='zip' value='{$customer->zip}' /></td>
        </tr>";
            
$addressForm = getSortedAddressForm($fieldList);

?>

<script type="text/javascript">
$(document).ready( function() {
    $("#referredBy").autocomplete({
       source : "/ajax/customer_autocomplete_aprax",
       select: function (event, referrer) {
           $("input[name='referredBy']").val(referrer.item.id);
       }
    });
    $("#customer_edit_form").validate( {
        rules: {
            email : {
                required: true,
                email : true
            }
        },
        errorClass : "invalid"
    })
});
</script>
<h2><?php echo __("Customer Edit", "Customer Edit:"); ?> <?= getPersonName($customer) ?> <a href="/admin/customers/detail/<?= $customer->customerID ?>">(<?php echo __("view", "view"); ?>)</a></h2>

<p>
    <a href="/admin/customers/detail/<?= $customer->customerID ?>"><img src="/images/icons/arrow_left.png"> <?php echo __("Customer details", "Customer details"); ?></a>
    | <a href="/admin/customers/billing/<?= $customer->customerID ?>"><img src="/images/icons/creditcards.png"> <?php echo __("Update Credit Card", "Update Credit Card"); ?></a>
    | <a href="/admin/customers/switch_to_customer/<?= $customer->customerID ?>" target="_blank"><img src="/images/icons/user_go.png"> <?php echo __("Log In as Customer", "Log In as Customer"); ?></a>
</p>

<?if(isset($total_transactions) && $total_transactions > 0):?>
    <br><a href='/admin/customers/transactions/<?= $customer->customerID ?>'><img src='/images/icons/creditcards.png' /> <?= $total_transactions ?> <?php echo __("Transactions", "Transactions"); ?></a>
<?endif;?>

<form id="customer_edit_form" action="" method="post">
    <br>
    <input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' />
    <table class="detail_table">
    <tr>
        <th><?php echo __("Inactive", "Inactive"); ?></th>
        <td>
            <input type='radio'  value='1' name='inactive' <? if($customer->inactive == 1) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
            <input type='radio' value='0' name='inactive' <? if($customer->inactive == 0) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
        </td>
        </tr>
        
        <?php echo $nameForm; ?>
        
        <tr>
            <th>*<?php echo __("Email", "Email"); ?></th>
            <td><input type='text' name='email' value="<?= $customer->email?>" />
            <? if($customer->bounced==1):?>
                <div class='error' style='padding:5px;margin:5px 0px;'><a href='/admin/customers/removeBounceBlock/<?= $customer->customerID?>' class='button white'><?php echo __("Remove the Block", "Remove the Block"); ?></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo __("This email is blocked", "This email is blocked"); ?> </div>
            <? endif; ?>
            </td>
        </tr>
        <tr>
            <th> *<?php echo __("Phone", "Phone"); ?></th>
            <td> <input type='text' name='phone' value="<?= $customer->phone?>" /> </td>
        </tr>
        <tr>
            <th><?php echo __("SMS", "SMS"); ?></th>
            <td><input type='text' name='sms' value="<?= $customer->sms?>" /></td>
        </tr>
        <tr>
            <th><?php echo __("SMS2", "SMS2"); ?></th>
            <td><input type='text' name='sms2' value="<?= $customer->sms2 ?>" /></td>
        </tr>
        <tr>
            <th><?php echo __("Username", "Username"); ?></th>
            <td><input type='text' name='username' value="<?= $customer->username?>" /></td>
        </tr>
        <tr>
            <th><?php echo __("Password", "Password"); ?></th>
            <td><input type='text' name='password' value="" /></td>
        </tr>
        
        <?php echo $addressForm; ?>
        <tr>
            <th><?php echo __("Company", "Company"); ?></th>
            <td><?= form_input('company', get_customer_meta($customer->customerID, 'company', '')); ?></td>
        </tr>       
        <tr>
            <th><?php echo __("Customer Type", "Customer Type"); ?></th>
            <td>
                <input type='radio' name='type' value='customer' <? if($customer->type == 'customer') echo ' checked'; ?>/> <?php echo __("Customer", "Customer"); ?>
                <input type='radio' name='type' value='wholesale' <? if($customer->type == 'wholesale') echo ' checked'; ?>/> <?php echo __("Wholesale", "Wholesale"); ?>

            </td>
        </tr>
        <tr>
            <th><?php echo __("Invoicing", "Invoicing"); ?></th>
            <td>
                <input type='radio' name='invoice' value='1' <? if($customer->invoice == '1') echo ' checked="true"'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' name='invoice' value='0' <? if($customer->invoice == '0') echo ' checked="true"'; ?> /> <?php echo __("No", "No"); ?>
                <span id="detailed_receipt" style="display:<?= $customer->invoice ? 'inline' : 'none' ?>;margin-left:20px;">
                    <label><input type="checkbox" name="detailed_receipt" value='1' <?= $detailed_receipt ? 'checked' : '' ?>> <?php echo __("Print detailed receipt", "Print detailed receipt"); ?></label>
                </span>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Hold", "Hold"); ?></th>
            <td>
                <input type='radio' name='hold' value='1' <? if($customer->hold == '1') echo ' checked="true"'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' name='hold' value='0' <? if($customer->hold == '0') echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Deliver orders without payment", "Deliver orders without payment"); ?></th>
            <td>
                    <?= form_yes_no("customer_deliver_on_hold", set_value("customer_deliver_on_hold", $customer->customer_deliver_on_hold)) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default Wash & Fold", "Default Wash & Fold"); ?></th>
            <td><select name='defaultWF' id='defaultWF'>
            <? foreach($wfProducts as $wf): ?>
                <option value="<?= $wf->product_processID ?>"><?= $wf->name ?></option>
            <? endforeach; ?>
            </select><script> selectValueSet('defaultWF', '<?= $customer->defaultWF ?>') </script></td>
        </tr>
        <tr>
            <th><?php echo __("Default Location", "Default Location"); ?></th>
            <td>
                <?= form_dropdown("location_id", $locations, $customer->location_id) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Signup Date", "Signup Date"); ?></th>
            <td><?= convert_from_gmt_aprax($customer->signupDate, "F j, Y")?></td>
        </tr>
        <tr>
            <th><?php echo __("Auto Pay", "Auto Pay"); ?></th>
            <td>
                <input type='radio'  value='1' name='autoPay' <? if($customer->autoPay == 1) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' value='0' name='autoPay' <? if($customer->autoPay == 0) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Shirts", "Shirts"); ?></th>
            <td>
                <input type='radio' value='on hanger' name='shirts' <? if($customer->shirts == 'on hanger') echo ' checked'; ?> /> <?php echo __("On Hanger", "On Hanger"); ?>
                <input type='radio' value='in box' name='shirts' <? if($customer->shirts == 'in box') echo ' checked'; ?> /> <?php echo __("In Box", "In Box"); ?>
            </td>
        </tr>
       <?php if (get_business_meta($this->business_id, 'displayStarchOptions')): ?>
        <tr>
            <th><?php echo __("Starch on Shirts", "Starch on Shirts"); ?></th>
            <td>
                <? foreach($starch as $s): ?>
                        <input type='radio'  value='<?= $s->starchOnShirtsID ?>' name='starchOnShirts_id' <? if($customer->starchOnShirts_id == $s->starchOnShirtsID) echo ' checked'; ?> /> <?= $s->name ?>
                <? endforeach; ?>
            </td>
        </tr>
       <?php endif; ?>
        <tr>
            <th><?php echo __("Email (Mail List)", "Email (Mail List)"); ?></th>
            <td>
                <input type='radio'  value='0' name='noEmail' <? if($customer->noEmail == 0) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' value='1' name='noEmail' <? if($customer->noEmail ==1) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Referred By", "Referred By"); ?></th>
            <td>
                <?= form_hidden("referredBy", $customer->rid) ?>
                <!-- Note, this form input is only used for the display of the referral name, it is not submitted as part of the form. -->
                <?
                //The following conditional checks to see if we already have a referred by value, if so, then we prepolutate the text box with the refferrer information.
                // Otherwise, we simply display a textbox with no value.
                if (isset($customer->rid))
                    echo form_input(array("style" => "width:150px", "id" => "referredBy", "value" => trim("{$customer->rCompleteName} ({$customer->rid})")));
                else {
                    echo form_input(array("style" => "width:150px", "id" => "referredBy"));
                }
                ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Kiosk Access", "Kiosk Access"); ?></th>
            <td>
                <input type='radio'  value='1' name='kioskAccess' <? if($customer->kioskAccess == 1) echo ' checked'; ?> /> Yes
                <input type='radio' value='0' name='kioskAccess' <? if($customer->kioskAccess == 0) echo ' checked'; ?> /> No
            </td>
        </tr>
        <tr>
            <th><?php echo __("Deliver All Orders To", "Deliver All Orders To"); ?></th>
            <td>
                <?= form_dropdown("deliverTo_location_id", $locations, $customer->deliverTo_location_id) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Wash Fold Notes", "Wash Fold Notes"); ?></th>
            <td><textarea style='width:90%; height:50px' name='wfNotes'><?= $customer->wfNotes ?></textarea></td>
        </tr>
        <tr>
            <th><?php echo __("DryClean Notes", "DryClean Notes"); ?></th>
            <td><textarea style='width:90%; height:50px' name='dcNotes'><?= $customer->dcNotes ?></textarea></td>
        </tr>
        <tr>
            <th><?php echo __("Customer Notes", "Customer Notes"); ?></th>
            <td><textarea style='width:90%; height:50px' name='customerNotes'><?= $customer->customerNotes ?></textarea></td>
        </tr>
        <tr>
            <th><?php echo __("Internal Notes", "Internal Notes"); ?></th>
            <td><textarea style='width:90%; height:50px' name='internalNotes'><?= $customer->internalNotes ?></textarea></td>
        </tr>
        <tr>
            <th><?php echo __("Perm Assembly Notes", "Perm Assembly Notes"); ?></th>
            <td><textarea style='width:90%; height:50px' name='permAssemblyNotes'><?= $customer->permAssemblyNotes ?></textarea></td>
        </tr>
        <tr>
            <th><?php echo __("How They Found Us", "How They Found Us"); ?></th>
            <td><?= form_input('how', $customer->how); ?></td>
        </tr>
<? if ($lockerLootConfig[0]->lockerLootStatus == 'active'): ?>
        <tr>
            <th><?php echo __("LockerLoot Terms", "LockerLoot Terms"); ?></th>
            <td>
                <input type='radio'  value='1' name='lockerLootTerms' <? if($customer->lockerLootTerms == 1) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' value='0' name='lockerLootTerms' <? if($customer->lockerLootTerms == 0) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
<? endif; ?>
        <tr>
            <th><?php echo __("W9", "W9"); ?></th>
            <td>
                <input type='radio'  value='1' name='W9' <? if($customer->W9 == 1) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' value='0' name='W9' <? if($customer->W9 == 0) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Twitter ID", "Twitter ID"); ?></th>
            <td><?= form_input('twitterId', $customer->twitterId); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Manager Agreement", "Manager Agreement"); ?></th>
            <td>
                <input type='radio'  value='1' name='mgrAgreement' <? if($customer->mgrAgreement == 1) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' value='0' name='mgrAgreement' <? if($customer->mgrAgreement == 0) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("IP Number", "IP Number"); ?> </th>
            <td> <?= form_input('ip', $customer->ip) ?> </td>
        </tr>
        <tr>
            <th> <?php echo __("Latitude", "Latitude"); ?> </th>
            <td><?= form_input('lat', $customer->lat) ?></td>
        </tr>
        <tr>
            <th><?php echo __("Longitude", "Longitude"); ?></th>
            <td> <?= form_input('lon', $customer->lon) ?></td>
        </tr>
        <tr>
            <th><?php echo __("Owner ID", "Owner ID"); ?></th>
            <td>
                <?  $employees[0] = '';?>
                <?= form_dropdown("owner_id", $employees, $customer->owner_id) ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Birthday", "Birthday"); ?> </th>
            <td> <?= form_input('birthday', $customer->birthday); ?> </td>
        </tr>
        <tr>
            <th><?php echo __("Sex", "Sex"); ?></th>
            <td>
                <input type='radio'  value='M' name='sex' <? if($customer->sex =="M") echo ' checked'; ?> /> <?php echo __("Male", "Male"); ?>
                <input type='radio' value='F' name='sex' <? if($customer->sex == "F") echo ' checked'; ?> /> <?php echo __("Female", "Female"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Pays for Upcharges", "Pays for Upcharges"); ?></th>
            <td>
                <input type='radio'  value='0' name='noUpcharge' <? if($customer->noUpcharge == 0) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio' value='1' name='noUpcharge' <? if($customer->noUpcharge == 1) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Place Order w/No Credit Card", "Place Order w/No Credit Card"); ?></th>
            <td>
                <input type='radio' value='1' name='orderWithNoCC' <? if($customer->orderWithNoCC == 1) echo ' checked'; ?> /> <?php echo __("Yes", "Yes"); ?>
                <input type='radio'  value='0' name='orderWithNoCC' <? if($customer->orderWithNoCC == 0) echo ' checked'; ?> /> <?php echo __("No", "No"); ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Taxable", "Taxable"); ?> </th>
            <td>
                <?= form_checkbox("taxable", 1, $customer->taxable) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default Language", "Default Language"); ?></th>
            <td><?= form_dropdown("default_business_language_id", $business_languages_dropdown, $customer->default_business_language_id); ?></td>
        </tr>
        <?php if (is_superadmin()): ?>
        </table>
        <h2><?php echo __("Droplocker monthly invoices", "Droplocker monthly invoices"); ?></h2>
        <table class="detail_table">
        <tr>
            <th><?php echo __("Pay invoices for business", "Pay invoices for business"); ?></th>
            <td><?= form_dropdown("pay_droplocker_invoices", $business_dropdown, get_customer_meta($customer->customerID, "pay_droplocker_invoices")); ?></td>
        </tr>
        <?php endif; ?>
    </table>
    <br>
    <p> *<?php echo __("Required Fields", "Required Fields"); ?> </p>
    <input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' />
</form>

<script type="text/javascript">
    $('input[name=invoice]').change(function(evt) {
        var val = parseInt($('input[name=invoice]:checked').val());
        console.log(val);
        $('#detailed_receipt')[val ? 'show' : 'hide']();
    });

    $('input[name=inactive]').change(function(evt){
	    var inputValue = $("input[name=inactive]:checked").val();

	    if(inputValue == 1){
		    $('input:radio[name=noEmail]')[1].checked =true;
		}
    })
</script>
