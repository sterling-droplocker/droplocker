<?php enter_translation_domain("admin/customers/outOfPattern"); ?>
<?php

$amounts = array_merge(array(100, 200, 500), range(1000, 10000, 1000));

?>
<h2><?php echo __("Out of Pattern Customers", "Out of Pattern Customers"); ?></h2>
<br>
<form action="/admin/customers/outOfPattern" method="post">
<?php echo __("Total spend", "Total spend"); ?> <select name="spend">
<? foreach($amounts as $amount): ?>
	<option value="<?= $amount ?>" <?= $this->input->post("spend") == amount ? 'SELECTED' : '' ?>>&gt; <?= format_money_symbol($this->business_id,'%.2n', $amount) ?></option>
    <? endforeach; ?>
</select>
&nbsp;&nbsp;&nbsp;<?php echo __("Out of parttern more than", "Out of parttern more than"); ?> <select name="overDays"><? for($i = 0; $i < 20; $i++ ): ?>
	<option value="<?= $i ?>" <? if($this->input->post("overDays") == $i) echo 'SELECTED' ?>> <?= $i ?> days</option>
    <? endfor; ?>
</select>
<input type="submit" name="Run" value="<?php echo __("Run", "Run"); ?>" class="button orange">
</form>
<br>
<table id="box-table-a">
<tr>
    <th><?php echo __("Customer", "Customer"); ?></th>
    <th><?php echo __("Total spend", "Total spend"); ?></th>
    <th><?php echo __("number of Orders", "# of Orders"); ?></th>
    <th><?php echo __("Avg per visit", "Avg per visit"); ?></th>
    <th><?php echo __("Avg lapse", "Avg lapse"); ?></th>
    <th><?php echo __("Last order", "Last order"); ?></th>
</tr>
<? foreach($customers as $customer): ?>
<tr>
    <td><a href="/admin/customers/detail/<?= $customer->customerID ?>"><?= getPersonName($customer) ?></a></td>
    <td><?= format_money_symbol($this->business_id, '%.2n', $customer->totGross - $customer->totDisc)?></td>
    <td><?= $customer->orderCount?></td>
    <td><?= format_money_symbol($this->business_id, '%.2n', ($customer->totGross - $customer->totDisc)/$customer->visits )?></td>
    <td><?= $customer->avgOrderLapse ?></td>
    <td><?= convert_from_gmt_aprax($customer->lastOrder, SHORT_DATE_FORMAT) ?></td>
</tr>

<? endforeach; ?>
</table>
