<?php enter_translation_domain("admin/customers/transactions"); ?>
<h2><?php echo __("Transactions for", "Transactions for"); ?> <?= $customer_name?></h2>

<table id='box-table-a'>
	<thead>
		<tr>
			<th><?php echo __("Type", "Type"); ?></th>
			<th><?php echo __("PNref", "PNref"); ?></th>
			<th><?php echo __("Message", "Message"); ?></th>
			<th><?php echo __("Date", "Date"); ?></th>
		</tr>
	</thead>
	<tbody>
		<? if($transactions): foreach($transactions as $t):?>
		<tr>
			<td><?= $t->type?></td>
			<td><?= $t->pnref?></td>
			<td><?= $t->message?></td>
			<td><?= $t->updated?></td>
		</tr>
		<? endforeach; else: ?>
		<tr>
			<td colspan='5'><?php echo __("No Transactions", "No Transactions"); ?></td>
		</tr>
		<?endif;?>
	</tbody>
</table>