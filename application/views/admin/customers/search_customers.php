<?php enter_translation_domain("admin/customers/search_customers"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = 
    "<td>
        " . form_input(array("id"=> "firstName", "name" => "firstName", "value" => $this->input->post('firstName') ? $this->input->post('firstName') : ''))
        . "<br>" . __("First Name", "First Name") . "
    </td>";
            
$fieldList['lastName'] = 
    "<td>
        " . form_input(array("id" => "lastName", "name" => "lastName", "value" => $this->input->post("lastName") ? $this->input->post("lastName") : ''))
        . "<br>" . __("Last Name", "Last Name") ."
    </td>";
            
$nameForm = getSortedNameForm($fieldList);

?>
<h2><?php echo __("Search Customers", "Search Customers"); ?></h2>
<br>
<form action="" method="post">
<table id='hor-minimalist-a'>
    <tr>
        
        <?= $nameForm ?>
        
        <td><?= form_input(array("id" => "username", "name" => "username", "value" => $this->input->post("username") ? $this->input->post("username") : '')) ?><br><?php echo __("Username", "Username"); ?></td>
    </tr>
    <tr>
        <td><?= form_input(array("id" => "address", "name" => "address1", "value" => $this->input->post("address1") ? $this->input->post("address1") : '')) ?><br><?php echo __("Address", "Address"); ?></td>
        <td><?= form_input(array("id" => "phone_or_sms", "name" => "phone_or_sms", "value" => $this->input->post("phone") ? $this->input->post("phone") : '')) ?><br><?php echo __("Phone_SMS", "Phone/SMS"); ?></td>
        <td><?= form_input(array("id" => "email", "name" => "email", "value" => $this->input->post("email") ? $this->input->post("email") : '')) ?><br><?php echo __("Email", "Email"); ?></td>
    </tr>
    <tr>
        <td colspan='3'>
            <input class='button orange' type='submit' id='customer_search_button' value='<?php echo __("Search", "Search"); ?>' name='submit'/>
        </td>
    </tr>
</table>
</form>
<br>
<table id='hor-minimalist-a'>
<tr>
	<th><?php echo __("Name", "Name"); ?></th>
	<th><?php echo __("Address", "Address"); ?></th>
	<th><?php echo __("Phone", "Phone"); ?></th>
    <th><?php echo __("Email", "Email"); ?></th>
	<th style='width:30px'><?php echo __("View", "View"); ?></th>
	<th style='width:30px'><?php echo __("Edit", "Edit"); ?></th>
</tr>
<? if($customer): foreach ($customer as $c): ?>
<tr>
	<td><a href='/admin/customers/detail/<?= $c->customerID?>'><?= getPersonName($c) ?></a></td>
	<td><?= $c->address1 ?></td>
	<td><?= $c->phone?></td>
    <td><?= $c->email?></td>
	<td align='center'><a href='/admin/customers/detail/<?= $c->customerID?>'><img src='/images/icons/magnifier.png' /></a></td>
	<td align='center'><a href='/admin/customers/edit/<?= $c->customerID?>'><img src='/images/icons/application_edit.png' /></a></td>
</tr>
<? endforeach; endif; ?>
</table>
