<?php enter_translation_domain("admin/reminders/index"); ?>
<?php
/**
 * Expects the following content variables
 * reminders array
 */
?>
<h2> <?php echo __("Email Reminders", "Email Reminders"); ?> </h2>

<p> <a href='/admin/reminders/add'><img src="/images/icons/add.png"> <?php echo __("Add New Reminder", "Add New Reminder"); ?></a></p>

<table class='box-table-a'>
    <tr>
        <th> <?php echo __("Description", "Description"); ?> </th>
        <th> <?php echo __("Coupon Type", "Coupon Type"); ?> </th>
        <th> <?php echo __("Status", "Status"); ?> </th>
        <th> <?php echo __("Amount Type", "Amount Type"); ?> </th>
        <th> <?php echo __("Amount", "Amount"); ?> </th>
        <th> <?php echo __("Trigger", "Trigger"); ?> </th>
        <th align='center' width='25px'> <?php echo __("Edit", "Edit"); ?></th>
        <th align='center' width='25px'> <?php echo __("Delete", "Delete"); ?> </th>
    </tr>
    <?php foreach($reminders as $reminder): ?>
    <tr>
        <td> <?= $reminder->description ?></td>
        <td> <?= $reminder->couponType ?></td>
        <td> <?= $reminder->status ?></td>
        <td> <?= $reminder->amountType ?></td>
        <td> <?= $reminder->amount ?></td>
        <td>
            <?php
                echo  ($reminder->couponType == "orderTotal")?$reminder->orders.__("Orders", " Orders"):$reminder->days.__("days", " days");
            ?>
        </td>
        <td align='center'>
            <a id="edit-<?= $reminder->reminderID ?>" href='/admin/reminders/edit/<?= $reminder->reminderID ?>'><img src='/images/icons/application_edit.png' /></a>
        </td>
        <td align='center'>
            <a id="delete-<?= $reminder->reminderID ?>" onClick='return verify_delete();' href='/admin/reminders/delete/<?= $reminder->reminderID ?>'><img src='/images/icons/cross.png' /></a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
