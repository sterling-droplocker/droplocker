<?php enter_translation_domain("admin/reminders/add"); ?>
<?php
//The following statement checks to see if we have POST data stored in the flash data, if so, then we use the previously submitted values.
if ($this->session->flashdata("post")) {
    $_POST = $this->session->flashdata("post");
}
//The following  heoper is used to generate the menus for the amount type and the coupon type
$this->load->helper("coupon_helper");
?>
<h2> <?php echo __("Add Reminder", "Add Reminder"); ?> </h2>
<p> <a href='/admin/reminders/'> <img src="/images/icons/arrow_left.png"> <?php echo __("View All Reminders", "View All Reminders"); ?> </a> </p>

<form action='' method='post'>
    <h3><?php echo __("Coupon Details", "Coupon Details"); ?></h3>
    <table class='detail_table'>
        <tr>
            <th> <?= form_label(__("Coupon Type", "Coupon Type"), "couponType") ?> </th>
            <td>
                <?= form_dropdown("couponType", getCouponTypesInDropdownFormat(), set_value("couponType"), "id='couponType'")?>
            </td>
        </tr>
        <tr>
            <th> <?= form_label(__("NO DISCOUNT", "NO DISCOUNT"), "noDiscount") ?> </th>
            <td> <?= form_checkbox("noDiscount", 1, set_value("noDiscount", 0), "id='noDiscount'") ?>  <?php echo __("Email Only. Check this if you only want to send an email", "Email Only  <span> Check this if you only want to send an email.</span>"); ?>  </td>
        </tr>
        <tr>
            <th> <?= form_label(__("Discount Name", "Discount Name"), "description") ?> </th>
            <td> <?= form_input("description", set_value("description"), "id='description'") ?> </td>
        </tr>
        <tr id='dayRow'>
            <th> <?= form_label(__("Days", "Days"), "days") ?> </th>
            <td>
                <?= form_input("days", set_value("days"), "id='days' style='width:50px;'") ?>
                <span> <?php echo __("Number of inactive days in order to trigger a reminder", "Number of inactive days in order to trigger a reminder"); ?> </span>
            </td>
        </tr>
        <tr id='orderRow' style="display: none;">
            <th> <?= form_label(__("Orders", "Orders"), "orders") ?> </th>
            <td>
                <?= form_input("orders", set_value("orders"), "id='orders' style='width:50px'") ?>
                <span> <?php echo __("Number of orders to trigger a reminder", "Number of orders to trigger a reminder"); ?> </span>
            </td>
        </tr>
        <tr>
            <th> <?= form_label(__("Amount Type", "Amount Type"), "amountType") ?> </th>
            <td>
                <?= form_dropdown("amountType", getAmountTypesInDropdownFormat(), set_value("amountType"), "id='amountType'") ?>

            </td>
        </tr>
        <tr>
            <th> <?= form_label(__("Amount", "Amount"), "amount") ?> </th>
            <td> <?= form_input("amount", set_value("amount"), "id='amount' style='width:50px'") ?> </td>
        </tr>
        <tr id="maxAmountRow">
            <th> <?= form_label(__("Max $", "Max $"), "maxAmt") ?></th>
            <td> <?= form_input("maxAmt", set_value("maxAmt"), "style='width:50px'"); ?></td>
        </tr>
        <tr>
            <th> <?= form_label(__("Expire", "Expire"), "expireDays") ?> </th>
            <td>
                <?= form_dropdown("expireDays", $expireDays, set_value("expireDays", "10"), "id='expireDays'") ?>
            </td>
        </tr>
        <tr>
            <th> <?= form_label(__("Status", "Status"), "status") ?></th>
            <td>
                <?= form_dropdown("status", array("paused" => "Paused", "active" => "Active"), set_value("status"), "id='status'") ?>
            </td>
        </tr>
    </table>
    <?= form_submit(array("value" => __("Submit", "Submit"), "class" => "button orange")) ?>
    <a href='/admin/reminders'> <?php echo __("Cancel", "Cancel"); ?> </a>
</form>

<script type="text/javascript">
    //Unknown * code
    $("#couponType").live('change', function() {
        var type = $(this).val();
        if (type == 'orderTotal') {
            $("#orderRow").css("display", 'table-row');
            $("#dayRow").css("display", 'none');
            $("#days").val('');
        } else if (type == 'birthdayCoupon') {
            $("#orderRow").css("display", 'none');
            $("#dayRow").css("display", 'none');
            $("#days").val('');
        } else {
            $("#dayRow").css("display", 'table-row');
            $("#orderRow").css("display", 'none');
            $("#orders").val('');
        }
    });

    $("#amountType").live("change", function() {
        var type = $(this).val();
        if (type === "percent") {
            $("#maxAmountRow").css("display", "table-row");
        } else if (type === "dollars") {
            $("#maxAmountRow").css("display", "none");
            $("#maxAmt").val("");
        }
    });

</script>