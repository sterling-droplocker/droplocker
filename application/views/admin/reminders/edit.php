<?php enter_translation_domain("admin/reminders/edit"); ?>
<?php
/**
 * Expects the following condtent variables
 * reminder stdClass an stdClass data instance of a reminder entity
 * reminder_emails_for_reminder_by_business_language array Contains the translatoins made for this reminder by the currnt business
 * business_languages array
 */

//The following statemnet is used to  to populate hte menus for the coupon type and amount type.
$this->load->helper("coupon_helper");
if (!is_array($business_languages)) {
    trigger_error(__("business_languages must be an array", "'business_languages' must be an array"), E_USER_WARNING);
}
if (!is_array($reminder_emails_for_reminder_by_business_language)) {
    trigger_error(__("reminder_emails_for_reminder_by_business_language must be an array", "'reminder_emails_for_reminder_by_business_language' must be an array"), E_USER_WARNING);
}

if (!isset($reminder)) {
    trigger_error(__("reminder must be set as a content variable", "'reminder' must be set as a content variable"), E_USER_WARNING);
}
?>
<script type="text/javascript">
    var reminder_emails_for_reminders_by_business_language = <?= json_encode($reminder_emails_for_reminder_by_business_language) ?>;

    //Note, this class expects the global variable reminder_emails_for_remidners_by_business language to be set
    var Remind_Email_Editor = function($incoming_business_language_selector, $incoming_reminder_email_hidden_input, $incoming_subject_input, $incoming_body_textarea) {
        this.$business_language_selector = $incoming_business_language_selector;
        this.$reminder_email_hidden_input = $incoming_reminder_email_hidden_input;
        this.$subject_input = $incoming_subject_input;
        this.$body_textarea = $incoming_body_textarea;

        this.update = function() {
            var business_language_id = $incoming_business_language_selector.val();
            var reminder_email = reminder_emails_for_reminders_by_business_language[business_language_id];
            this.$reminder_email_hidden_input.val(reminder_email['reminder_emailID']);
            this.$subject_input.val(reminder_email['subject']);
            if ("body" in CKEDITOR.instances) {
                CKEDITOR.instances['body'].destroy();
            }
            this.$body_textarea.val(reminder_email['body']);
            CKEDITOR.replace('body', {
                uiColor: false,
                toolbar: 'MyToolbar',
                fullPage: true,
                allowedContent: true
            });
        };

    }

</script>

<h2>
    <?php echo __("Edit Reminder", "Edit Reminder"); ?>
</h2>
<p> <a href='/admin/reminders/'><img src="/images/icons/arrow_left.png" alt="Left Arrow"> <?php echo __("View All Reminders", "View All Reminders"); ?> </a> </p>

<?= form_open("/admin/reminders/edit/$reminder->reminderID", array("style" => "margin: 10px 0px")) ?>
<?= form_hidden("reminderID", $reminder->reminderID) ?>
<h3> <?php echo __("Coupon Details", "Coupon Details"); ?> </h3>
<table class='detail_table'>
    <tr>
        <th> <?= form_label(__("Coupon Type", "Coupon Type"), "couponType") ?> </th>
        <td>
            <?= form_dropdown("couponType", getCouponTypesInDropdownFormat(), set_value("couponType", $reminder->couponType), "id='couponType'") ?>
        </td>
    </tr>
    <tr>
        <th> <?= form_label(__("NO DISCOUNT", "NO DISCOUNT"), "noDiscount") ?> </th>
        <td> <?= form_checkbox("noDiscount", 1, set_value("noDiscount", $reminder->noDiscount), "id='noDiscount'") ?> <span><?php echo __("Check this if you only want to send an email with No Discount", "Check this if you only want to send an email with No Discount."); ?> </span> </td>
    </tr>
    <tr>
        <th> <?= form_label(__("Discount Name", "Discount Name"), "description", set_value("description", $reminder->description), "id='desciption'") ?> </th>
        <td> <?= form_input("description", set_value("description", $reminder->description), "id='description' style='width:90%'") ?> </td>
    </tr>
    <tr id='dayRow'>
        <th> <?= form_label(__("Days", "Days"), "days") ?> </th>
        <td> <?= form_input("days", set_value("days", $reminder->days), "id='days' style='width:q00") ?> <span id='couponDescription'> </span> </td>
    </tr>
    <tr id='orderRow'>
        <th> <?= form_label(__("Orders", "Orders"), "orders") ?> </th>
        <td> <?= form_input("orders", set_value("orders", $reminder->orders), "id='orders' style='width:50px;'") ?> <span> <?php echo __("Number of orders to trigger a reminder", "Number of orders to trigger a reminder"); ?> </span> </td>
    </tr>
    <tr>
        <th> <?= form_label(__("Amount Type", "Amount Type"), "amountType") ?> </th>
        <td>
            <?= form_dropdown("amountType", getAmountTypesInDropdownFormat(), set_value("amountType", $reminder->amountType), "id='amountType'") ?>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Amount", "Amount"); ?></th>
        <td><input style='width:50px;' type='text' value='<?= $reminder->amount ?>' name='amount' /></td>
    </tr>
    <tr id="maxAmountRow">
        <th> <?= form_label(__("Max $", "Max $"), "maxAmt") ?></th>
        <td> <?= form_input("maxAmt", set_value("maxAmt", $reminder->maxAmt), "style='width:50px'"); ?></td>
    </tr>
    <tr>
        <th><?php echo __("Expire", "Expire"); ?></th>
        <td>
            <select id='expireDays' name='expireDays'>
                <? for($i=1; $i<=30; $i++): ?>
                <option value='<?= $i ?>'><?= $i ?> <?php echo __("Days", "Days"); ?></option>
                <? endfor; ?>
            </select>
            <script>selectValueSet('expireDays', '<?= $reminder->expireDays ?>')</script>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Status", "Status"); ?></th>
        <td>
            <select name='status' id='status'>
                <option value='paused'><?php echo __("Paused", "Paused"); ?></option>
                <option value='active'><?php echo __("Active", "Active"); ?></option>
            </select>
            <script>
                selectValueSet('status', '<?= $reminder->status ?>');
            </script>
        </td>
    </tr>

</table>
<input type='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' style='margin: 10px 0px'/>
<?= form_close() ?>

<h3> <?php echo __("Reminder Emails", "Reminder Emails"); ?> </h3>
<?= form_open("/admin/reminder_emails/update", array("id" => "reminder_email_form", "style" => "margin: 10px 0px")) ?>
    <?= form_dropdown("business_language_id", $business_languages, "", "id='business_language_selector'") ?>
    <?= form_hidden("reminder_emailID") ?>
    <?= form_hidden("reminder_id", $reminder->reminderID) ?>
    <table class='detail_table'>
        <tr>
            <th> <?= form_label(__("Subject", "Subject"), "subject") ?> </th>
            <td> <input type='text' name='subject' id='subject' /> </td>
        </tr>
    </table>
    <p><?php echo __("Email template support merge values. Merge values are enclosed in percent symbols", "Email template support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\", \"500\", \"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?></p>

    <textarea style='width:99%; height:200px;' name='body' id='body'> </textarea>

    <?= form_submit(array("value" => __("Update Reminder Email", "Update Reminder Email"), "class" => "button orange")) ?>
<?= form_close() ?>
<div class='info' style='margin: 20px 0px;'>
    <h3><?php echo __("Send Test Email", "Send Test Email"); ?></h3>
    <input type='text' name='customer' id='customer' style='width:500px; margin: 10px 0px;' /> <div id='customer_results'> </div> <input type='hidden' id='customer_id' name='customer_id' value='customer_id' /> <input type='button' style='margin: 10px 0px;' class='button blue' id='send_email' value='<?php echo __("Send Test Email", "Send Test Email"); ?>' />
</div>
<script type="text/javascript">
    var $business_language_selector = $("#business_language_selector");
    var $reminder_email_form_input = $("#reminder_email_form input[name='reminder_emailID']");
    var $subject_input = $("#subject");
    var $body_input = $("#body");
    var reminder_email_editor = new Remind_Email_Editor($business_language_selector, $reminder_email_form_input, $subject_input, $body_input);
    $business_language_selector.change(function() {
        reminder_email_editor.update();
    });
    reminder_email_editor.update();
    $("#orderRow").css("display", 'none');
    var newCustomerText = '<?php echo __("Number of days since the customer has signed up. This is only sent if the customer has not placed an order yet", "Number of days since the customer has signed up. (This is only sent if the customer has not placed an order yet)"); ?>';
    var inactiveCustomerText = '<?php echo __("Number of days since the customer has had any activity", "Number of days since the customer has had any activity"); ?>';

    var couponType = '<?= $reminder->couponType ?>';
    if (couponType == 'orderTotal') {
        $("#orderRow").css("display", 'table-row');
        $("#dayRow").css("display", 'none');
    }

    if (couponType == 'birthdayCoupon') {
        $("#orderRow").css("display", 'none');
        $("#dayRow").css("display", 'none');
    }

    if (couponType == 'newCustomer') {
        $('#couponDescription').html(newCustomerText);
    } else {
        $('#couponDescription').html(inactiveCustomerText);
    }


    $('#customer').autocomplete({
        source: function(req, add) {

            //pass request to server
            $.getJSON("/ajax/customer_autocomplete_aprax/", req, function(data) {
                dc = data;
                //create array for response objects
                var suggestions = [];

                //process response
                $.each(data, function(i, val) {
                    suggestions.push(data[i]);

                });

                //pass array to callback
                add(suggestions)
            });
        },
        appendTo: "#customer_results",
        select: function(e, ui) {
            $.each(dc, function(i, customer) {
                if (ui.item.value == customer['label'])
                    $('#customer_id').val(customer['id']); //This should be the customer ID
            });
        }
    });

    $('#send_email').click(function() {
        var customer_id = $('#customer_id').val();
        $(this).val('<?php echo __("Please Wait", "Please Wait..."); ?>');
        $(this).attr('disabled', true);

        if (customer_id == 'customer_id')
        {
            alert("Missing customer");
            $('#send_email').val('<?php echo __("Send Test Email", "Send Test Email"); ?>');
            $('#send_email').attr('disabled', false);
            return false;
        }

        $.ajax({
            url: "/admin/reminders/send_test_reminder_ajax/",
            type: 'POST',
            dataType: 'HTML',
            data: {
                customer_id: $('#customer_id').val(),
                reminder_id:<?= $reminder->reminderID ?>,
            },
            success: function(data) {
                if (data == 'success') {
                    alert("<?php echo __("Email has been sent", "Email has been sent"); ?>");
                }
                else {
                    alert(data);
                }
                $('#send_email').val('<?php echo __("Send Test Email", "Send Test Email"); ?>');
                $('#send_email').attr('disabled', false);
            }
        });
    })


    $("#couponType").live('change', function() {
        var type = $(this).val();
        if (type == 'orderTotal') {
            $("#orderRow").css("display", 'table-row');
            $("#dayRow").css("display", 'none');
        } else if (type == 'birthdayCoupon') {
            $("#orderRow").css("display", 'none');
            $("#dayRow").css("display", 'none');
            $("#days").val('');
        } else {
            $("#dayRow").css("display", 'table-row');
            $("#orderRow").css("display", 'none');
            if (type == 'newCustomer') {
                $('#couponDescription').html(newCustomerText);
            } else {
                $('#couponDescription').html(inactiveCustomerText);
            }
        }
    });

    $("#amountType").live("change", function() {
        var type = $(this).val();
        if (type === "percent") {
            $("#maxAmountRow").css("display", "table-row");
        } else if (type === "dollars") {
            $("#maxAmountRow").css("display", "none");
            $("#maxAmt").val("");
        }
    });
    /**
     * *************************************
     * END *
     * *************************************
     */

</script>
