<?php enter_translation_domain("admin/index/dashboard_settings"); ?>
<h2><?php echo __("Dashboard Settings", "Dashboard Settings"); ?></h2>

<br>
<? foreach($modules as $module):  $counter = 0; ?>
<table id="box-table-a" style="width:75%">
<tr>
    <th colspan="2"><?php echo __($module, $module); ?></th>
</tr>
<form action="/admin/index/dashboard_update" method="post">
<input type="hidden" name="module" value="<?= $module ?>">
<? for($i = 1; $i <= 3; $i++):?>
<tr>
    <? for($j = 0; $j <= 1; $j++): $counter ++;?>
    <td>
        <select name="loc_<?= $counter ?>">
        <option value=""><?php echo __("Select Widget", "-- Select Widget --"); ?></option>
        <? foreach($widgets as $widget): ?>
        	<option value="<?= $widget->widgetID ?>" <?= $userSettings[$module][$counter] == $widget->widgetID ? ' SELECTED' : '' ?>><?= $widget->description ?></option>
        <? endforeach; ?>
        </select>
    </td>
    <? endfor; ?>
</tr>
<? endfor; ?>
<tr>
    <td colspan="2" align="left"><input type="submit" name="Update" value="<?php echo __("Update", "Update"); ?>" class="button orange"></td>
</tr>
</form>
</table>
<br>
<? endforeach; ?>
