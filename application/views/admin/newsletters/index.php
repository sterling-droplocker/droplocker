<?php enter_translation_domain("admin/newsletters/index"); ?>
<h2><?php echo __("Newsletters", "Newsletters"); ?></h2>
<?php if($mailchimp_enabled): ?>
    <h3><?php echo __("Mailchimp", "Mailchimp"); ?></h3>
    <p><?php echo __("Enabled", "Enabled:"); ?> <?= $mailchimp_enabled ? __("Yes", "Yes") : __("No", "No"); ?> - <?php echo __("Api Key", "Api Key:"); ?> <?= (is_a($mailchimp, '\DrewM\MailChimp\MailChimp')) ? __("Valid", "Valid") : __("Invalid", "Invalid"); ?></p>
    <h4><?php echo __("Members", "Members"); ?></h4>
    <p><?php echo __("Subscribed", "Subscribed:"); ?> <?= $mailchimp_stats['subscribed']; ?> - <?php echo __("Unsubscribed", "Unsubscribed:"); ?> <?= $mailchimp_stats['unsubscribed']; ?></p>
    <h4><?php echo __("Batches", "Batches"); ?></h4>
    <p><?php echo __("Finished", "Finished:"); ?> <?= $mailchimp_batch['finished']; ?> - <?php echo __("Pending", "Pending:"); ?> <?= $mailchimp_batch['pending']; ?></p>
<?php endif; ?>
<p><a id='addNewsletterLink' href='/admin/newsletters/add'><img src="/images/icons/add.png"> <?php echo __("Add Newsletter", "Add Newsletter"); ?></a></p>
<table id='box-table-a'>
    <tr>
        <th><?php echo __("Name", "Name"); ?></th>
        <th><?php echo __("StartDate", "StartDate"); ?></th>
        <th><?php echo __("Status", "Status"); ?></th>
        <th style='width:25px;'><?php echo __("Edit", "Edit"); ?></th>
        <th style='width:25px;'><?php echo __("Delete", "Delete"); ?></th>
    </tr>
    <? foreach($newsletters as $newsletter):
    
         if($newsletter->startDate != null) {
            $startDate = $newsletter->startDate->format("Y-m-d");
            $startDate = convert_from_gmt_aprax($startDate, SHORT_DATE_FORMAT);
        } else {
            $startDate = "N/A";
        }
        $not_sent = $newsletter->status=='pending';
    ?>
    <tr>
        <td><?= $newsletter->name?></td>
        <td><?= $startDate ?></td>
        <td><?= $not_sent ? __("Draft", "Draft") : __("Sent", "Sent")  ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <? if ($not_sent): ?>
            <a href='/admin/newsletters/activate/<?= $newsletter->newsletterID ?>'><?php echo __("Start Sending", "Start Sending..."); ?></a>
            <? endif; ?>
        </td>
        <td align='center'>
            <? if($not_sent): ?>
                <a href='/admin/newsletters/edit/<?= $newsletter->newsletterID?>'><img src='/images/icons/application_edit.png' /></a>
            <? else: ?>
                <a href='/admin/newsletters/view/<?= $newsletter->newsletterID?>'><img src='/images/icons/eye.png' /></a>
            <? endif; ?>
        </td>
        <td align='center'>
            <? if($not_sent): ?>
                <a onClick='return verify_delete();' href='/admin/newsletters/delete/<?= $newsletter->newsletterID?>'><img src='/images/icons/cross.png' /></a>
            <? endif; ?>
        </td>
    </tr>
    <?endforeach; ?>
</table>
