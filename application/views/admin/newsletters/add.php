<?php enter_translation_domain("admin/newsletters/add"); ?>
<script>
	$(function() {
		CKEDITOR.replace('body', {
            uiColor : false,
            fullPage: true,
            allowedContent: true
		});
	});
</script>

<h2><?php echo __("Add Newsletter", "Add Newsletter"); ?></h2>

<p><a href="/admin/newsletters"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to newsletters", "Back to newsletters"); ?></a></p>

<form action='' method='post'>
<table class='detail_table'>
    <tr>
        <th><?php echo __("Name", "Name"); ?></th>
        <td><input type='text' name='name' value=""/></td>
    </tr>
   
     <tr>
        <th><?php echo __("Subject", "Subject"); ?></th>
        <td><input type='text' name='subject' value=""/></td>
    </tr>
     <tr>
        <td colspan='2'><textarea name='body' id='body' style='width:99%; height:500px'></textarea></td>
    </tr>
   
</table>

<input type='submit' class='button orange' name='submit' value='<?php echo __("Save Newsletter", "Save Newsletter"); ?>' />
</form>
