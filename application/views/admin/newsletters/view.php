<?php enter_translation_domain("admin/newsletters/view"); ?>
<script>
	$(function() {
		CKEDITOR.replace('body', {
            uiColor : false,
            fullPage: true,
            allowedContent: true
		});
	});
</script>

<h2><?php echo __("Edit Newsletter", "Edit Newsletter"); ?></h2>

<p><a href="/admin/newsletters"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to newsletters", "Back to newsletters"); ?></a></p>

<table class='detail_table'>
    <tr>
        <th><?php echo __("Status", "Status"); ?></th>
        <td><?= $newsletter->status?></td>
    </tr>
    <tr>
        <th><?php echo __("Name", "Name"); ?></th>
        <td><?= $newsletter->name?></td>
    </tr>
   
     <tr>
        <th><?php echo __("Subject", "Subject"); ?></th>
        <td><?= $newsletter->subject?></td>
    </tr>
     <tr>
        <td colspan='2'><textarea name='body' id='body' style='width:99%; height:500px'><?= $newsletter->body?></textarea></td>
    </tr>
</table>


<div style='background-color:#eaeaea; padding:10px; margin:10px 0px; border:1px solid #333'>
<h3><?php echo __("Send Test", "Send Test"); ?></h3>
<?php echo __("This will send the email to the user that is currently logged in", "This will send the email to the user that is currently logged in."); ?> (<?= $employee->email?>) 
<a class='button blue' href='/admin/newsletters/test/<?= $newsletter->newsletterID?>' ><?php echo __("Send it to me", "Send it to me"); ?></a>
</div>

