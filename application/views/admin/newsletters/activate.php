<?php enter_translation_domain("admin/newsletters/activate"); ?>
<script>
    $(function() {
        var dateFormat = "yy-mm-dd";
        $( "#startDate" ).datepicker({dateFormat:dateFormat});
        $( "#endDate" ).datepicker({dateFormat:dateFormat});
    });
    </script>
    
<h2><?php echo __("Activate Newsletter", "Activate Newsletter"); ?></h2>
<form action='' method='post'>
<table class='detail_table'>
    <tr>
        <th><?php echo __("Status", "Status"); ?></th>
        <td><input type='text' name='status' value="<?= $newsletter->status?>"/></td>
    </tr>
    <tr>
        <th><?php echo __("Customers", "Customers"); ?></th>
        <td><select name='customerType'>
            <option value='allCustomers'><?php echo __("All Customers", "All Customers"); ?></option>
            <? foreach($filters as $filter): ?>
            <option value='<?= $filter->filterID ?>'><?= $filter->name ?></option>
            <? endforeach; ?>
        </select> <a href="/admin/customers/filters"><?php echo __("manage filters", "manage filters"); ?></a></td>
    </tr>
    <tr>
        <th><?php echo __("Start Date", "Start Date"); ?></th>
        <td><input type='text' id='startDate' name='startDate' style='width:100px;' value="<?= convert_from_gmt_aprax(($newsletter->startDate !=null)?$newsletter->startDate->format("Y-m-d"):"", "Y-m-d") ?>"/></td>
    </tr>
    <!-- 
    <tr>
        <th>End Date</th>
        <td><input type='text' id='endDate' name='endDate' style='width:100px;' value="<?=  convert_from_gmt_aprax(($newsletter->endDate !=null)?$newsletter->endDate->format("Y-m-d"):"", "Y-m-d") ?>"/></td>
    </tr> 
   -->
</table>
<input type='submit' class='button orange' name='activate' value='<?php echo __("Activate", "Activate"); ?>' />
</form>