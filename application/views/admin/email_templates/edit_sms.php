<?php enter_translation_domain("admin/email_templates/edit_sms"); ?>
<?php
/**
 * Note, part of the codebase for htis view is unknown * code thta handles the case where a business does not have any defined business languages. It is not known what the expected content variables are for the  .
 *
 * Expects the following content variables
 *  update_action string the contorller action that the form for updating the template shall post to
 *  business_languages array
 *  emailActionID int Used for legacy Matt Dunalp code
 *  emailAction stdClass an instance of an email action model entity
 *  emailTemplates_by_business_language_for_action array Must b ein the format defined by the documentation in the emailTemplate model
 *  employee stdClass An instance of an employee model entity
 *
 * Can take the following content variable
 *  selected_business_language_id int
 */

if (empty($update_action)) {
    trigger_error(__("update_action must be set", "'update_action' must be set"), E_USER_ERROR);
}

if (!is_array($emailTemplates_by_business_language_for_action)) {
    trigger_error(__("emailTemplates_by_business_language_for_action must be an array", "'emailTemplates_by_business_language_for_action' must be an array"), E_USER_ERROR);
}
if (!isset($business_languages_dropdown)) {
    trigger_error(__("business_languages must be set", "'business_languages' must be set"), E_USER_ERROR);
}
if (empty($emailAction)) {
    trigger_error(__("emailAction can not be empty", "'emailAction' can not be empty"), E_USER_ERROR);
}
if (!isset($selected_business_language_id)) {
    $selected_business_language_id = null;
}
?>
<style type="text/css">
    legend {
        font-weight: bold;
    }
    input,select {
        margin: 5px;
    }
</style>

<script type="text/javascript">
var emailTemplates_by_business_language_for_action = <?=json_encode($emailTemplates_by_business_language_for_action)?>;

/**
 * Sets up the interface and behavior for an SMS template editing interface.
 * @param object $incoming_message
 * @param object $incoming_business_languageID
 * @param object $incoming_submit_button
 * @returns {SmsTemplateManager}
 */
var SmsTemplateManager = function($incoming_message, $incoming_business_languageID, $incoming_submit_button) {
    this.$submit_button = $incoming_submit_button.loading_indicator();
    this.$message = $incoming_message;
    this.$business_language_id = $incoming_business_languageID;
    //The following function sets the correct form values when a new sms message and language ID are passed into this function.
    //This function is intended to be called when the business language ID selector changes.
    this.update = function(incoming_emailTemplate_message, incoming_business_languageID) {
        this.$message.val(incoming_emailTemplate_message);
        this.$business_language_id.val(incoming_business_languageID);
    };
};

$(document).ready(function() {
    if ($("#edit_form").length > 0) {
        var $emailTemplate_form = $("#edit_form");
        var $business_language_form = $("#business_language_menu");
        var $message_box = $emailTemplate_form.find("[name='message']");
        var $submit_button = $emailTemplate_form.find(":submit");
        var $hidden_business_language_id_input = $emailTemplate_form.find("[name='business_language_id']");

        var smsTemplate_management_interface = new SmsTemplateManager($message_box, $hidden_business_language_id_input, $submit_button);

        $business_language_form.change(function() {
            var emailTemplate = emailTemplates_by_business_language_for_action[$(this).val()];
            if (typeof emailTemplate === "undefined") {
                smsTemplate_management_interface.update("", $(this).val());
            }
            else {
                smsTemplate_management_interface.update(emailTemplate['smsText'], $(this).val());
            }
        });
        $business_language_form.trigger("change");
    }
});


</script>


<h2> <?php echo __("Edit SMS", "Edit SMS ::"); ?> <?= $emailAction->name?> </h2>
<p><a href="/admin/email_templates"><img src="/images/icons/arrow_left.png"> <?php echo __("Go Back to Email Templates", "Go Back to Email Templates"); ?></a></p>

<p><?php echo __("SMS template support merge values", "SMS template support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?></p>

<? if (!empty($business_languages_dropdown)): ?>
<?=  form_label("Language") . form_dropdown("", $business_languages_dropdown, $selected_business_language_id, "id='business_language_menu'") ?>
<? endif; ?>

<?= form_open($update_action, array("id" => "edit_form")) ?>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("SMS Message", "SMS Message"); ?></th>
            <td>
                <textarea style='width:100%; height:100px; border:1px solid #ccc;' name='message'></textarea>
            </td>
        </tr>
    </table>
    <input type="hidden" value='<?=$emailAction->emailActionID?>' name='emailActionID' />
    <?= form_hidden("business_language_id") ?>
    <input type="submit" value='<?php echo __("Update", "Update"); ?>' class='button orange' />
<?= form_close() ?>
