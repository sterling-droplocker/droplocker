<?php enter_translation_domain("admin/email_templates/edit_adminAlert"); ?>
<?php
/**
 * Expects atleast the following content variables
 *  $business_id
 *  $emailTemplate
 *  $name
 *  $recipients
 *  $message
 *  $subject
 *  $update_action
 *  $employee
 *
 */
if (!isset($emailTemplate)) {
    trigger_error(__("emailTemplate must be set as a content variable", "'emailTemplate' must be set as a content variable"), E_USER_WARNING);
}
?>
<h2><?php echo __("Edit Admin Alert", "Edit Admin Alert ::"); ?> <?= $name?></h2>
<p><a href="/admin/email_templates"><img src="/images/icons/arrow_left.png"> <?php echo __("Go Back to Email Templates", "Go Back to Email Templates"); ?></a></p>

<p> <?php echo __("Admin templates support merge values. Merge values are enclosed in percent symbols. Example", "Admin templates support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?> </p>

<?= form_open($update_action) ?>
    <table class='detail_table'>
        <tr>
            <th> <?php echo __("Admin Alert Subject", "Admin Alert Subject"); ?> </th>
            <td>
                <input type="text" value='<?= $subject?>' name='subject' />
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Admin Alert Message", "Admin Alert Message"); ?> </th>
            <td>
                <textarea id="alertMessage" style='width:100%; height:100px; border:1px solid #ccc;' name='message'> <?= $message?> </textarea>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Recipients", "Recipients"); ?> </th>
            <td>
                <?php if($customer_support = get_business_meta($business_id, 'customerSupport')):
                    $found = array_search($customer_support, $recipients['system']);
                    if ($found !== false) {
                        unset($recipients['system'][$found]);
                    }
                ?>
                    <input type='checkbox' name='email[]' <?= $found !== false ? "checked" : ""?> value='<?php echo $customer_support?>'>
                    <?php echo $customer_support?><br>
                <?php endif ?>
                <?php if($operations = get_business_meta($business_id, 'operations')):
                    $found = array_search($operations, $recipients['system']);
                    if ($found !== false) {
                        unset($recipients['system'][$found]);
                    }
                ?>
                    <input type='checkbox' name='email[]' <?= $found !== false ? "checked" : ""?> value='<?php echo $operations?>'>
                    <?php echo $operations?><?php endif;?><br>
                <?php if($sales = get_business_meta($business_id, 'sales')):
                    $found = array_search($sales, $recipients['system']);
                    if ($found !== false) {
                        unset($recipients['system'][$found]);
                    }
                ?>
                    <input type='checkbox' name='email[]' <?= $found !== false ? "checked" : ""?> value='<?php echo $sales?>'>
                    <?php echo $sales?>
                <?php endif;?>
                <br />
                <br />
                <?php echo __("Add more Recipients separate with a comma", "Add more Recipients (separate with a comma):"); ?>
                <div>
                    <input type='text' name='customEmails' value='<?php echo $recipients['custom']?>' />
                </div>

            </td>
        </tr>
    </table>
    <input type="hidden" value='<?= $emailTemplate->emailActionID?>' name='insert_id' />
    <input type="submit" name='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' />
</form>
<div style='background-color:#eaeaea; padding:10px; margin:10px 0px; border:1px solid #333'>
    <h3> <?php echo __("Send Test Admin Alert Email", "Send Test Admin Alert Email"); ?> </h3>
    <?php echo __("This will send the email to the user that is currently logged in", "This will send the email to the user that is currently logged in (%user%) <span style='color:#cc0000'>Make sure the Admin Alert template is saved!</span>", array("user"=>$employee->email)); ?>
    <?= form_open("/admin/emails/send_test_adminAlert_email")?>
        <?= form_hidden("emailActionID", $emailTemplate->emailActionID) ?>
        <?= form_submit(array('class' => 'button orange', 'style' => 'margin-top:10px;', 'value' => __("Send Test E-mail", "Send Test E-mail"))) ?>
    <?= form_close() ?>
</div>
<script type="text/javascript">
    CKEDITOR.replace("alertMessage", {
        uiColor : false,
        toolbar : 'MyToolbar',
        fullPage: true,
        allowedContent: true
    });
</script>
