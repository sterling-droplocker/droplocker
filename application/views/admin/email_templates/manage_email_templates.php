<?php enter_translation_domain("admin/email_templates/manage_email_templates"); ?>
<?php
/**
 * Expects the following content variables
 *  emailActions array
 *  business_languages array
 */
if (!is_array($emailActions)) {
    trigger_error(__("emailActions must be an array", "'emailActions' must be an array"), E_USER_WARNING);
}
if (!is_array($business_languages_menu)) {
    trigger_error(__("business_languages_menu must be an array", "'business_languages_menu must' be an array"), E_USER_WARNING);
}
if (!is_numeric($business_language_id)) {
    trigger_error(__("business_language_id must be numeric", "'business_language_id' must be numeric"), E_USER_WARNING);
}

?>
<style type='text/css'>
    label {
        margin: 5px;
        
    }
    input {
        margin: 5px;
    }
</style>
<h2><?php echo __("Alert Templates", "Alert Templates"); ?></h2>
<p><?php echo __("Manage all email templates for customer actions", "Manage all email templates for customer actions"); ?></p>

<?= form_open("/admin/email_templates", array("method" => "GET")) ?>
<?= form_label("Business Language","business_language_id") . form_dropdown("business_language_id", $business_languages_menu, $business_language_id) ?>
<?= form_submit(array("class" => "button orange", "value" => __("Update", "Update"))) ?>
<?= form_close() ?>

<h3><?php echo __("Standard Templates", "Standard Templates"); ?></h3>
<table class="box-table-a">
    <thead>
        <tr>
            <th style='width:150px;'> <?php echo __("Action Name", "Action Name"); ?> </th>
            <th> <?php echo __("Status", "Status"); ?> </th>
            <th> <?php echo __("Description", "Description"); ?> </th>
            <th width='45' align='center'> <?php echo __("SMS", "SMS"); ?> </th>
            <th width='45' align='center'> <?php echo __("Email", "Email"); ?> </th>
            <th width='45' align='center'> <?php echo __("Admin", "Admin"); ?> </th>
        </tr>
    </thead>
    <tbody>
        <? foreach ($emailActions as $key => $emailAction): ?>
            <tr>
                <td> <?= $emailAction['name']?> </td>
                <td> <?= $emailAction['status']?> </td>
                <td> <?= $emailAction['description'] ?> </td>
                <td align='center'>
                    <a class="edit_sms" href='/admin/email_templates/edit_sms/<?= $key?>'><?= !empty($emailAction['sms_found']) ? '<img src="/images/icons/tick.png" />' : '<img src="/images/icons/application_edit.png" />'; ?> </a>
                </td>
                <td align='center'>
                    <a class="edit_email" href='/admin/email_templates/edit_email/<?= $key?>'><?= !empty($emailAction['email_found']) ? '<img src="/images/icons/tick.png" />' : '<img src="/images/icons/application_edit.png" />'; ?> </a>
                </td>
                <td align='center'>
                    <a class="edit_admin" href='/admin/email_templates/edit_adminalert/<?= $key?>'><?= !empty($emailAction['admin_found']) ? '<img src="/images/icons/tick.png" />' : '<img src="/images/icons/application_edit.png" />'; ?> </a>
                </td>
            </tr>
        <?php endforeach ; ?>
    </tbody>
</table>

<? if ($home_delivery_setup): ?>
<br><br>
<h3><?php echo __("Home Delivery Templates", "Home Delivery Templates"); ?></h3>
<table class="box-table-a">
    <thead>
        <tr>
            <th style='width:150px;'> <?php echo __("Action Name", "Action Name"); ?> </th>
            <th> <?php echo __("Status", "Status"); ?> </th>
            <th> <?php echo __("Description", "Description"); ?> </th>
            <th width='45' align='center'> <?php echo __("SMS", "SMS"); ?> </th>
            <th width='45' align='center'> <?php echo __("Email", "Email"); ?> </th>
            <th width='45' align='center'> <?php echo __("Admin", "Admin"); ?> </th>
        </tr>
    </thead>
    <tbody>
        <? foreach ($homeDeliveryActions as $key => $emailAction): ?>
            <tr>
                <td> <?= $emailAction['name']?> </td>
                <td> <?= $emailAction['status']?> </td>
                <td> <?= $emailAction['description'] ?> </td>
                <td align='center'>
                    <a class="edit_sms" href='/admin/email_templates/edit_sms/<?= $key?>'><?= !empty($emailAction['sms_found']) ? '<img src="/images/icons/tick.png" />' : '<img src="/images/icons/application_edit.png" />'; ?> </a>
                </td>
                <td align='center'>
                    <a class="edit_email" href='/admin/email_templates/edit_email/<?= $key?>'><?= !empty($emailAction['email_found']) ? '<img src="/images/icons/tick.png" />' : '<img src="/images/icons/application_edit.png" />'; ?> </a>
                </td>
                <td align='center'>
                    <a class="edit_admin" href='/admin/email_templates/edit_adminalert/<?= $key?>'><?= !empty($emailAction['admin_found']) ? '<img src="/images/icons/tick.png" />' : '<img src="/images/icons/application_edit.png" />'; ?> </a>
                </td>
            </tr>
        <?php endforeach ; ?>
    </tbody>
</table>
<? endif; ?>
