<?php enter_translation_domain("admin/email_templates/edit_email"); ?>
<?php
/**
 * Note, part of this codebase contains Scumbag  , which handles the case where a buisness does not have any defined business languages.. It is not know the expected content variables.
 *
 * Expects the following content varibles
 *  update_action string
 *  business_languages array
 *  emailAction stdClass contains a codeigniter model result entity of an email action
 *  emailTemplates_by_business_language_for_action array Must b ein the format defined by the documentation in the emailTemplate model
 *  employee an entity from the Employee model of the current employee
 * Can take the following content varible
 *  selected_business_language_id int
 */

if (empty($update_action)) {
    trigger_error(__("update_action must be set", "'update_action' must be set"), E_USER_ERROR);
}

if (!is_array($emailTemplates_by_business_language_for_action)) {
    trigger_error(__("emailTemplates_by_business_language_for_action must be an array", "'emailTemplates_by_business_language_for_action' must be an array"), E_USER_ERROR);
}
if (!isset($business_languages_dropdown)) {
    trigger_error(__("business_languages must be set", "'business_languages' must be set"), E_USER_ERROR);
}
if (empty($emailAction)) {
    trigger_error(__("emailAction can not be empty", "'emailAction' can not be empty"), E_USER_ERROR);
}

if (!isset($selected_business_language_id)) {
    $selected_business_language_id = null;
}
?>

<style type="text/css">
    legend {
        font-weight: bold;
    }
    input,select {
        margin: 5px;
    }
</style>

<script type="text/javascript">
var emailTemplates_by_business_language_for_action = <?=json_encode($emailTemplates_by_business_language_for_action)?>;

/**
 * The following prototype contains the properties, buttons, and fields for managing th business template for a particular action
 * @param object $incoming_emailSubject must be text input
 * @param object $incoming_emailText must be textarea
 * @param object $incoming_bodyText must be textarea
 * @param object $incoming_business_languageID must be form hidden input
 * @param object $incoming_submit_button must be input type button
 * @returns {EmailTemplateManager}
 */
var EmailTemplateManager = function($incoming_emailSubject, $incoming_emailText, $incoming_bodyText, $incoming_business_languageID, $incoming_submit_button) {
    this.$emailSubject = $incoming_emailSubject;
    this.$emailText = $incoming_emailText;
    this.$emailText.attr("id", "emailText");
    CKEDITOR.replace(this.$emailText.attr("id"), {
        uiColor : false,
        toolbar : 'MyToolbar',
        fullPage: true,
        allowedContent: true
    });

    this.$bodyText = $incoming_bodyText;

    this.$submit_button = $incoming_submit_button;
    this.$submit_button.loading_indicator();
    this.$business_languageID = $incoming_business_languageID;

    /**
     * Updates the subject, body texty fields of the email tempalte interface to the language the user selected.
     * @param object incoming_emailSubject the email Subject text field
     * @param object incoming_emailText the email text area field
     * @param object incoming_bodyText The body textarea field
     * @param int incoming_business_languageID
     * @returns {undefined}
     */
    this.update = function(incoming_emailSubject, incoming_emailText, incoming_bodyText, incoming_business_languageID) {
        this.$emailSubject.val(incoming_emailSubject);

        CKEDITOR.instances[this.$emailText.attr("id")].destroy();

        this.$emailText.val(incoming_emailText);

        CKEDITOR.replace(this.$emailText.attr("id"), {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });

        this.$bodyText.val(incoming_bodyText);
        this.$business_languageID.val(incoming_business_languageID);
    }
}

</script>

<h2> <?php echo __("Edit Email Template", "Edit Email Template ::"); ?> <?= $emailAction->name?> </h2>
<p><a href="/admin/email_templates"><img src="/images/icons/arrow_left.png"> <?php echo __("Go Back to Email Templates", "Go Back to Email Templates"); ?></a></p>

<p><?php echo __("Email template support merge values. Merge values are enclosed in percent symbols. Example", "Email template support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%.<a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'><img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?>  </p>

<?= form_open($update_action, array("id" => "edit_form")) ?>

<? if (!empty($business_languages_dropdown)): ?>
    <p> <?=  form_label("Language") . form_dropdown("business_language_id", $business_languages_dropdown, $selected_business_language_id, "id='business_language_menu'") ?> </p>
<? endif; ?>

    <h3><?php echo __("Subject", "Subject"); ?></h3>

    <input style='width:100%;font-size:14px; padding:3px; border:1px solid #ccc;' type="text" id='subject' name='emailSubject' />
    <br /><br />

    <h3><?php echo __("HTML Body", "HTML Body"); ?></h3>

    <textarea name='emailText'></textarea>
    <br /><br />

    <h3><?php echo __("Text Body", "Text Body"); ?></h3>

    <textarea style='width:100%; height:200px; border:1px solid #ccc;'  name='bodyText'> </textarea>
    <input type="hidden" value='<?= $emailAction->emailActionID?>' name='emailActionID' />
    <input type="submit" name='submit' value='<?php echo __("Save Template", "Save Template"); ?>' class='button orange' style='margin-top:10px;'/>
    <input type="button" name='delete' value='<?php echo __("Delete Template", "Delete Template"); ?>' class='button red' style='margin-top:10px;'/>
<?= form_close() ?>


<div style='background-color:#eaeaea; padding:10px; margin:10px 0px; border:1px solid #333'>
    <h3><?php echo __("Send Test", "Send Test"); ?></h3>
    <?php echo __("This will send the email to the user that is currently logged in", "This will send the email to the user that is currently logged in."); ?> (<?= $employee->email?>)
    <span style='color:#cc0000'><?php echo __("Make sure the template is saved", "Make sure the template is saved!"); ?></span>
    <?= form_open("/admin/emails/send_test_email") ?>
        <?= form_hidden("emailActionID", $emailAction->emailActionID) ?>
        <?= form_input(array("name" => "business_language_id", "id" => "test_email_business_language_id", "type" => "hidden")) ?>
        <?= form_submit(array("class" => "button orange", "style" => "margin-top: 10px;", "value" => __("Send Test E-mail", "Send Test E-mail"))) ?>
    <?= form_close() ?>
</div>

<script type="text/javascript">
    //The following conditional checks to see if the form element with the id attribute '#edit_form' exists. If it does exist, then we initialize the edit form by instantiating the template manager prototype.
    if ($("#edit_form").length > 0) {
        var $emailTemplate_form = $("#edit_form");
        var $send_test_email_form = $("#send_test_email_form");
        var $business_language_menu = $("#business_language_menu");

        var emailTemplate = emailTemplates_by_business_language_for_action[$business_language_menu.val()];

        var $emailSubject = $emailTemplate_form.find("[name='emailSubject']");
        var $emailText = $emailTemplate_form.find("[name='emailText']");
        var $bodyText = $emailTemplate_form.find("[name='bodyText']");

        if (typeof emailTemplate !== "undefined") {
            $emailSubject.val(emailTemplate['emailSubject']);
            $emailText.val(emailTemplate['emailText']);
            $bodyText.val(emailTemplate['bodyText']);
        }

        var $submit_button = $emailTemplate_form.find("[name=submit]");
        var $delete_button = $emailTemplate_form.find("[name=delete]");
        var $hidden_business_language_id_input = $emailTemplate_form.find("[name='business_language_id']");

        $("#test_email_business_language_id").val($business_language_menu.val()); //THis statement sets the hidden business language ID input for the send email form to the value of the currently selected business lnaguage.
        var emailTemplate_management_interface = new EmailTemplateManager($emailSubject, $emailText, $bodyText, $hidden_business_language_id_input, $submit_button);

		$delete_button.click(function(evt) {
			if (!confirm('<?php echo __("Are you sure", "Are you sure?"); ?>')) {
				evt.preventDefault();
				evt.stopPropagation();
				return false;
			}

            emailTemplate_management_interface.update("", "", "", $business_language_menu.val());
			$submit_button.click();
		});

        //The following statement attaches a change event language to the business language menu switcher that wil update the template fields with the busines language translations for the current email action
        $business_language_menu.change(function() {
            var emailTemplate = emailTemplates_by_business_language_for_action[$(this).val()];
            $("#test_email_business_language_id").val($(this).val()); //THis statement sets the hidden business language ID input for the send email form to the value of the currently selected business lnaguage.
            if (typeof emailTemplate === "undefined") {
                emailTemplate_management_interface.update("", "", "", $(this).val());
            } else {
                emailTemplate_management_interface.update(emailTemplate['emailSubject'], emailTemplate['emailText'], emailTemplate['bodyText'], $(this).val());
            }
        });
    } else {
        // The following statement initializes the old * interface for managing an emplte for a business with no defined business languages.
        CKEDITOR.replace( 'body', {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });
    }
</script>
