<?php enter_translation_domain("admin/employees/roles"); ?>
<style type='text/css'>
    .img_button {
        cursor: hand; cursor: pointer;
    }
</style>
<h2><?php echo __("Employee Roles", "Employee Roles"); ?></h2>
<?php 
foreach ($resources as $resource => $aclresources): ?>
    <table class='box-table-a'>
        <thead>
            <tr>
                <th width='*'><h3><?php echo __($resource, $resource); ?></h3></th>
                <?php 
                $width = (80/sizeof($roles));
                foreach ($roles as $h) : ?>
                        <th align='center' width='<?= $width?>%'><?php echo __($h->name, $h->name); ?></th>
                <?php endforeach ?>
            </tr>
        </thead>
        <?php foreach ($aclresources as $property): ?>
            <tr>
                <td><?= $property['resource']?></td>
                <?php foreach ($roles as $role) : ?>
                    <td align='center'>
                        <img id='<?= $business_id?>-<?= $property['resource_id']?>-<?= $role->aclID?>-role' class='img_button' src='<?= ($this->zacl->check_acl($property['resource'],$role->name))?"/images/icons/accept.png":"/images/icons/delete.png" ?>' />
                    </td>
                <?php endforeach ; ?>
            </tr>
        <?php endforeach ; ?>
    </table>
<? endforeach ; ?>
