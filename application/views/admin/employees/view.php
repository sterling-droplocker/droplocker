<?php enter_translation_domain("admin/employees/view"); ?>
<?= $this->session->flashdata('message')?>
<h2><?php echo __("Employees", "Employees"); ?></h2>
<table id='hor-minimalist-a'>
    <thead>
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <th><?php echo __("Email", "Email"); ?></th>
            <th><?php echo __("Phone", "Phone"); ?></th>
            <th><?php echo __("Role", "Role"); ?></th>
			<th><?php echo __("Position", "Position"); ?></th>
            <th width='25'></th>
            <th width='25'></th>
            <th width='25'></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($active_employees as $e): ?>
            <tr>
                <td><a href='/admin/employees/manage/<?= $e->employeeID?>'><?= getPersonName($e) ?></a></td>
                <td><a href='mailto:<?= $e->email ?>'><?= $e->email ?></a></td>
                <td><?= $e->phoneCell ?></td>
                <td><a href='/admin/employees/manage/<?= $e->employeeID?>'><? $role = $this->employee_model->get_employee_role($e->employeeID, $this->business_id); echo $role[0]->name; ?></a></td>
                <td><?= $e->position ?></td>
                <td><a href='/admin/employees/employee_roles/<?= $e->employeeID?>' ><img src='/images/icons/lock.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
                <td><a href='/admin/employees/deactivate/<?= $e->employeeID?>'><img id='' src='/images/icons/cross.png' alt='<?php echo __("Deactivate", "Deactivate"); ?>' /></a></td>
                <td><a title='<?php echo __("switch to employee", "switch to employee"); ?>' href='/admin/employees/switch_to_employee/<?= $e->employeeID?>'><img src='/images/icons/user.png' /></a> </td>
            </tr>
        <?php endforeach; ?>
	<thead>
        <tr>
            <th colspan="8"><Br><Br><?php echo __("Inactive Employees", "Inactive Employees"); ?></th>
        </tr>
        </thead>
        <?php foreach($inactive_employees as $e):?>
            <tr>
                <td><?= getPersonName($e) ?> (<?= date("m/d/y", strtotime($e->endDate)) ?>)</td>
                <td><a href='mailto:<?= $e->email ?>'><?= $e->email ?></a></td>
                <td><?= $e->phoneCell ?></td>
                <td><? $role = $this->employee_model->get_employee_role($e->employeeID, $this->business_id); echo $role[0]->name; ?></td>
                <td><?= $e->position ?></td>
                <td><a href='/admin/employees/manage/<?= $e->employeeID?>' ><img src='/images/icons/application_edit.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
                <td><a href='/admin/employees/activate/<?= $e->employeeID?>'><img src='/images/icons/add.png' alt='<?php echo __("Reactivate", "Reactivate"); ?>' /></a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

