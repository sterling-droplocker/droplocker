<?php enter_translation_domain("admin/employees/timeOff"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = 
        "<tr>
            <th>" .  __("First Name", "First Name") . "</th>
            <td>
                " . $emp[0]->firstName . "
            </td>
        </tr>";
            
$fieldList['lastName'] = 
        "<tr>
            <th>" .  __("Last Name", "Last Name") . "</th>
            <td>" . $emp[0]->lastName . "</td>
        </tr>";
            
$nameForm = getSortedNameForm($fieldList);

?>
<script>
	$(function() {
		var dates = $( "#from, #to" ).datepicker({
			//defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 2,
			dateFormat:"DD, d MM, yy",
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
	});
</script>
<h2><?php echo __("Time Off Request", "Time Off Request"); ?></h2>
<br>
<br>
<form action="" method="post">
<table class='detail_table'>
    
        <?= $nameForm ?>
    
	<tr>
		<th><?php echo __("Requested Days Off", "Requested Day(s) Off:"); ?></th>
		<td><?php echo __("From", "From:"); ?> <input style='width:200px' type="text" id="from" name="from"/> <?php echo __("To", "To:"); ?> <input  style='width:200px' type="text" id="to" name="to"/></td>
	</tr>
	<tr>
		<th><?php echo __("Reason", "Reason"); ?></th>
		<td><textarea cols="60" rows="4" name="reason"></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" name="submit" value="<?php echo __("Submit Request", "Submit Request"); ?>" class="button blue"></td>
	</tr>
</table>
</form>