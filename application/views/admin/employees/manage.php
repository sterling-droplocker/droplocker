<?php enter_translation_domain("admin/employees/manage"); ?>
<link href="/js/plugins/select2/select2.min.css" rel="stylesheet" />
<?php
/**
 * Expects the following content variables
 *  $businesses array A codeigniter formatttd dropdown of all the businesses
 *  $business stdClass a Codeigniter query result of the current business the logged in employee is associated with
 *  $title string "New Employee", if creating a new employee, or "Edit Empoyee", if editing an existing employee
 *  $managers array A codeigniter formatted dropdown of all the managers for a particular business
 *  $roles array A codeigniter formatted dropdown of all the employee roles for a business
 *  $employee stdClass a Codeigniter query result of a single employee entity
 *  $business_employee stdClass a Codeigniter query result of a single business employee entity
 *  $employee_customer mixed A codeigniter query result that contains a single Customer entity that an employee is currently associated with. If the employee is not associated with a customer entity, then this value should be FALSE.
 */

$all_states = get_all_states_by_country_as_array();

$types = array(
    'non exempt' => 'Non-exempt',
    'exempt' => 'Exempt',
);

$rateTypes = array(
    'Hourly' => 'Per Hour',
    'Salary' => 'Per Year',
);

if (empty($employee)) {
    $employee = new stdClass();
    $fields = array(
        'firstName', 'lastName', 'email', 'username', 'position', 'phoneHome',
        'phoneCell', 'llPhone', 'address', 'address2', 'city', 'state', 'type',
        'rateType', 'zipcode', 'ssn', 'hourlyRate', 'notes', 'startDate', 'endDate',
        'commission', 'one_session', 'session_expiration', 'manager_id',
        'business_language_id', 'developer_mode', 'supplier_id',
    );

    $business_employee = (object) array(
        'business_id' => null,
        'aclRole_id' => null,
    );

    foreach ($fields as $field) {
        $employee->$field = null;
    }
}

?>

<?php 

$fieldList = array();
$fieldList['firstName'] = 
        "<tr>
            <th>*" .  __("First Name", "First Name") . "</th>
            <td>
                " . form_input('firstName', $employee->firstName) . "
            </td>
        </tr>";
            
$fieldList['lastName'] = 
        "<tr>
            <th>*" .  __("Last Name", "Last Name") . "</th>
            <td>" . form_input('lastName', $employee->lastName) . "</td>
        </tr>";
            
$nameForm = getSortedNameForm($fieldList);

//------------------

$fieldList = array();
$fieldList['address1'] = 
       "<tr>
            <th>" . __('Address', 'Address') . "</th>
            <td>
                " .  form_input('address', $employee->address) . "
            </td>
        </tr>";
            
$fieldList['address2'] = 
         "<tr>
            <th>" . __('Address 2', 'Address 2') . "</th>
            <td>
                " .  form_input('address2', $employee->address2) . "
            </td>
        </tr>";

$fieldList['city'] = 
        "<tr>
            <th>" . __('City', 'City') . "</th>
            <td>
                " .  form_input('city', $employee->city) . "
            </td>
        </tr>";
            
$fieldList['state'] = 
        (isset($all_states[$this->business->country])) ? 
            "<tr>
                <th>" . __('State', 'State') . "</th>
                <td>
                    " .  form_dropdown('state', $all_states[$this->business->country], $employee->state, 'id="state"' ) . "
                </td>
            </tr>"
            : 
            "<tr>
                <th>" . __('Locality', 'Locality') . "</th>
                <td>
                   " .  form_input('state', $employee->state, 'id="state"' ) . "
                </td>
            </tr>";

$fieldList['zip'] = 
        "<tr>
            <th>" . __('Zipcode', 'Zipcode') . "</th>
            <td>
                " .  form_input('zipcode', $employee->zipcode) . "
            </td>
        </tr>";
        
$addressForm = getSortedAddressForm($fieldList);
        
?>
<h2> <?= $title ?></h2>
<?= $this->session->flashdata('message'); ?>

<?= form_open("/admin/employees/update") ?>
<input type="text" style="display:none">
<input type="password" style="display:none">

    <? if (!empty($employee->employeeID)): ?>
        <?= form_hidden("employee_id", $employee->employeeID) ?>
    <? endif ?>
    <table class='detail_table'>
        
        <?= $nameForm ?>
        
        <tr>
            <th>*<?php echo __("Email", "Email"); ?></th>
            <td><?= form_input("email", $employee->email) ?> </td>
        </tr>
        <tr>
            <th><?php echo __("Username For LaundryDroid", "Username (For LaundryDroid)"); ?></th>
            <td><?= form_input('username', $employee->username) ?></td></td>
        </tr>
        <tr>
            <th><?php echo __("Position", "Position"); ?></th>
            <td><?= form_input('position', $employee->position) ?></td>
        </tr>
        <tr>
            <th><?= !empty($employee->employeeID) ? '':'*'; ?>Password</th>
            <td><?= form_password("password") ?></td>
        </tr>
        <tr>
            <th><?php echo __("Phone Home", "Phone Home"); ?></th>
            <td><?= form_input('phoneHome', $employee->phoneHome) ?></td>
        </tr>
        <tr>
            <th><?php echo __("Phone Cell", "Phone Cell"); ?></th>
            <td><?= form_input('phoneCell', $employee->phoneCell) ?></td>
        </tr>
        <tr>
            <th><?php echo __("Work Phone", "Work Phone"); ?></th>
            <td>
                <?= form_input('llPhone', $employee->llPhone) ?>
            </td>
        </tr>
        
        <?= $addressForm ?>
        
        <tr>
            <th><?php echo __("SSN", "SSN"); ?></th>
            <td>
                <?= form_input('ssn', $employee->ssn) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Type", "Type"); ?></th>
            <td>
                <?= form_dropdown('type', $types, $employee->type);?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Rate", "Rate"); ?></th>
            <td>
                <?= form_input('hourlyRate', $employee->hourlyRate) ?> 
                <?= form_dropdown('rateType', $rateTypes, $employee->rateType, "id='rateType'");?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Notes", "Notes"); ?></th>
            <td><?= form_input('notes', $employee->notes )?></td>
        </tr>

        <tr>
            <th> <?php echo __("Start Date", "Start Date"); ?> </th>
            <td> 
                <?= form_input(array("name" => 'startDate', "value" => $employee->startDate, "class" => "datetime")) ?> 
            </td>
        </tr>
        <tr>
            <th> <?php echo __("End date", "End date"); ?> </th>
            <td> 
                <?= form_input(array("name" => 'endDate', "value" => $employee->endDate, "class" => "datetime")) ?> 
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Commission", "Commission"); ?> </th>
            <td> 
                <?= form_input("commission", $employee->commission) ?> 
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Login to Single Machine Only", "Login to Single Machine Only"); ?> </th>
            <td> <?= form_checkbox("one_session", 1, $employee->one_session) ?> </td>
        </tr>
        <tr>
            <th> <?php echo __("Session Expiration Time seconds", "Session Expiration Time (seconds)"); ?> </th>
            <td>
                <?= form_input("session_expiration", $employee->session_expiration) ?> <?php echo __("Leave blank or 0 for no expiration", "(Leave blank or 0 for no expiration)"); ?>
            </td>
        </tr>
        <? //The following statement will display the functionality for changing the associated business of an employee, if the current user is a superadmin.
            if (is_superadmin()): ?>
                <tr>
                    <th> <?php echo __("Business", "Business"); ?> </th>
                    <td>
                        <?= form_dropdown("business_id", $businesses, $business_employee->business_id)  ?>
                    </td>
                </tr>

                <tr>
                    <th> <?php echo __("Switch between business", "<p>Switch between business</p> <p style=\"font-size:.7em;\">Allow user to switch between selected businesses</p>"); ?></th>
                    <td>
                        <select name="allowed_businesses_ids[]" id="allowed_businesses" multiple="multiple">
                        <?php 
                        foreach ($businesses as $k=>$b) {
                            $selected = "";
                            if (in_array($k, $allowed_businesses_ids) || $k == $business_employee->business_id) $selected = 'selected="selected"';
                        ?>
                           <option value="<?php echo $k ?>" <?php echo $selected ?>>
                                <?php echo $b ?>
                           </option> 
                        <?php
                        }
                        ?>
                        </select>
                    </td>
                </tr>
        <? endif ?>
        <tr>
            <th> <?php echo __("Manager", "Manager"); ?> </th>
            <td>
                <?= form_dropdown("manager_id", $managers, $employee->manager_id, 'id="manager_id"') ?>           
            </td>
        </tr>            
        <tr>
            <th> <?php echo __("Role", "Role"); ?> </th>
            <td>
                <?= form_dropdown("aclRole_id", $roles, $business_employee->aclRole_id) ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Default Business Language", "Default Business Language"); ?> </th>
            <td> <?= form_dropdown("business_language_id", $business_languages, $employee->business_language_id) ?> </td>
        </tr>
        <tr>
            <th> <?php echo __("Customer Account", "Customer Account"); ?> </th>
            <td> 
                <? if (empty($employee_customer)): ?>
                    <?= form_input(array("id" => "customer_id")) . " " . form_hidden('customer_id') ?>
                <? else: ?>
                    <?= form_input(array("id" => "customer_id", "value" => getPersonName($employee_customer))) . form_hidden("customer_id", $employee_customer->customerID) ?> 
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>  <?php echo __("Developer Mode", "<p>Developer Mode </p> <p style=\"font-size:0.7em;\"> Used to display extra debugging information </p>"); ?> </th>
            <td>
                <?= form_checkbox("developer_mode", 1, $employee->developer_mode) ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Limit to Supplier", "Limit to Supplier"); ?></th>
            <td>
                <?= form_dropdown("supplier_id", array('' => __("ALL-SUPPLIERS", "--ALL-SUPPLIERS--")) + (array)$suppliers, $employee->supplier_id) ?>
            </td>
        </tr>
    </table>
    <p> *<?php echo __("Required Fields", "Required Fields"); ?> </p>
    <? if (empty($employee->employeeID)): ?>
        <?= form_submit(array("id" => "add_employee_button", "class" => "button orange", "value" => __("Create", "Create"))) ?>
    <? else: ?>
        <?= form_submit(array("id" => "add_employee_button", "class" => "button orange", "value" => __("Update", "Update"))) ?>
    <? endif ?>
    
</form>


<script src="/js/plugins/select2/select2.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
    $("form").validate({
        rules: {
            firstName : {
                required: true
            },
            lastName: {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        errorClass: "invalid",
        invalidHandler: function(form,validator) {
            $("form").find(":submit").loading_indicator("destroy");
        },
        submitHandler: function(form) {
            if ($("#customer_id").val() == "") {
                $("input[name='customer_id']").val("");
            }
            form.submit();
        }
    });    
    
    $(".datetime").datetimepicker( {
        timeFormat: "hh:mm:ss",
        dateFormat: "yy-mm-dd"
    });
    
    $(":submit").loading_indicator();
    
    //The following event handler updates the manager and ACL Role dropdowns for the current selected business.
    
    $("select[name=business_id]").change(function() {
            var $loading_image_for_employees = $("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
            $loading_image_for_employees.insertAfter($("select[name=manager_id]"));
            $("select[name=manager_id]").attr("disabled", true);
            
            var $loading_image_for_aclroles = $("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
            $loading_image_for_aclroles.insertAfter($("select[name=aclRole_id]"));
            $("select[name=aclRole_id]").attr("disabled", true);
            
            var $loading_image_for_business_languages = $("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
            $loading_image_for_business_languages.insertAfter($("select[name=business_language_id]"));
            $("select[name=business_language_id]").attr("disabled", true);
            
            $("#add_employee_button").attr("disabled", true);
            $("#add_employee_button").addClass("disabled");
            var business_id = $(this).val();
            $.getJSON("/admin/employees/get_as_json", {
                business_id : business_id
                }, 
                function(employees) {
                    $("select[name=manager_id]").empty();
                    $.each(employees, function(index, employee) {
                        var $option = $("<option>");
                        $option.val(index);
                        $option.text(employee);

                        $("select[name=manager_id]").append($option);
                    });
                    $loading_image_for_employees.remove();
                    $("select[name=manager_id]").attr("disabled", false);
                    
                    $.getJSON("/admin/aclroles/get_as_json", {
                        business_id : business_id
                        }, function(aclRoles) {
                            $("select[name=aclRole_id]").empty();
                            $.each(aclRoles, function(index, aclRole) {
                                var $option = $("<option>");
                                $option.val(index);
                                $option.text(aclRole);

                                $("select[name=aclRole_id]").append($option);
                            });
                            $loading_image_for_aclroles.remove();
                            $("select[name=aclRole_id]").attr("disabled", false);
                            $.getJSON("/admin/business_languages/get_as_json", {
                                business_id : business_id
                            }, function(result) {
                                if (result['status'] == "success") {
                                    $("select[name=business_language_id]").empty();
                                    var business_languages = result['data'];
                                    $.each(business_languages, function(index, business_language) {
                                        var $option = $("<option>").val(index).text(business_language);
                                        $("select[name=business_language_id]").append($option);
                                    });
                                    
                                }
                                else {
                                    alert("Error retrieving business languages");
                                }
                                $loading_image_for_business_languages.remove();
                                $("select[name=business_language_id]").attr("disabled", false);
                                
                                $("#add_employee_button").attr("disabled", false);
                                $("#add_employee_button").removeClass("disabled");
                            });
                    });                    
            });  
        }
    );
    $("#customer_id").autocomplete({
        source : "/ajax/customer_autocomplete_aprax",
        select: function (event, customer) {
            $("input[name='customer_id']").val(customer.item.id);
        }
    });

    $("#allowed_businesses").select2({
        placeholder: "Select multiple business"
    });
});
</script>
