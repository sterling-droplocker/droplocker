<?php enter_translation_domain("admin/employees/employee_roles"); ?>
<style type='text/css'>
.img_button 
{
    cursor: hand; cursor: pointer;
}
</style>

<h2><?php echo __("Roles for", "Roles for"); ?> <?php echo getPersonName($employee) ?></h2>

<?php foreach ($resources as $resource => $aclresources): ?>
    <h2 style='clear:left'><?php echo $resource ? $resource : __("Empty", "-Empty-") ?></h2>
    <?php foreach($aclresources as $i => $property): ?>
	<?= $i && !($i%4) ? '<div style="clear:both;"></div>':''; ?>
	<div style='float:left;width:25%;word-wrap:break-word;'>
	<div style="padding:3px 3px 3px 20px; line-height: 16px; margin:3px; border:1px solid #eaeaea; position: relative;">
            <?php 
                if ($this->zacl->check_acl($property['resource'],$business_employee_id))
                {
                    $allowed = "/images/icons/accept.png";
                }
                else
                {
                    $allowed = "/images/icons/delete.png";
                }    
            ?>
            <img class='img_button' style="position:absolute;left:2px; top:2px;" id='<?=$business_id?>-<?=$property['resource_id']?>-<?= $business_employee_id?>-user' src='<?=$allowed?>' /> <?php echo $property['resource']?>
	</div>
        </div>

    <?php endforeach; ?>
<?php endforeach ?>
