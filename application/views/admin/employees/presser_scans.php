<?php 
enter_translation_domain("admin/employees/presser_scans"); 
$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
?>
<h2><?php echo __("Presser Log", "Presser Log"); ?></h2>

<? if(!isset($_COOKIE['pressingStation'])): ?>
	<br><br><?php echo __("Select Pressing Station for this computer", "Select Pressing Station for this computer:"); ?> 
	<form action="" method="post">
	<select name="pressingStation">
	<? foreach($stations as $s): ?>
		<option value="<?= $s->pressingStationID ?>"><?= $s->description ?> (<?= $s->location ?>)</option>
	<? endforeach; ?>
	</select><input type="submit" name="submit" value="<?php echo __("Submit", "Submit"); ?>"></form>
<? else: ?>

<table width="100%" border="0">
<tr><td valign="top"><table cellspacing="4" cellpadding="4">
	<tr><td><strong><?php echo __("Employee", "Employee:"); ?></strong></td><td align="right"><?= get_employee_name($this->buser_id) ?></td></tr>
	<tr><td><strong><?php echo __("Start Time", "Start Time:"); ?></strong></td><td align="right"><?= date("g:i a", strtotime($todaysHours['startTime'])) ?></td></tr>
	<tr><td><strong><?php echo __("Hours Worked", "Hours Worked:"); ?></strong></td><td align="right"><?= number_format_locale($todaysHours['todayHrs'],2)  ?><?php echo __("hrs", "hrs"); ?></td></tr>
	<tr><td><strong><?php echo __("Station", "Station:"); ?></strong></td><td align="right" nowrap><form action="" method="post"><?= $stationInfo->description ?> (<?= $stationInfo->location ?>) <input type="hidden" name="change" value="1"><input type="submit" name="Change" value="<?php echo __("Change", "Change"); ?>" style="font-size: 8px; color: Red;"></form></td></tr>
	<tr>
		<td><strong><?php echo __("Total Items", "Total Items:"); ?></strong> </td>
		<td align="right"><?= $pph['itemCount'] ?></td>
	</tr>
    <tr>
		<td><strong><?php echo __("Reworks", "Reworks:"); ?></strong> </td>
		<td align="right"><?= $pph['rework'] ?></td>
	</tr>
	<tr>
		<td><strong><?php echo __("Elapsed Items", "Elapsed Items:"); ?></strong> </td>
		<td align="right"><?= sec_to_time($pph['elapsedTime']) ?></td>
	</tr>
	<tr>
		<td><strong><?php echo __("Target", "Target:"); ?></strong> </td>
		<td align="right"><?= $stationInfo->targetPPH ?> PPH</td>
	</tr>
	<tr>
		<td><strong><?php echo __("Effective PPH", "Effective PPH:"); ?></strong></td>
		<td align="right"><?= number_format_locale($pph['pph'],1) ?></td>
	</tr>
	<tr>
		<td><strong><?php echo __("Actual PPH", "Actual PPH:"); ?></strong></td>
		<td align="right"><?= $actualPPH ?></td>
	</tr>
		

</td></tr></table>
<br>
<br>
<?php echo __("History", "History:"); ?>
<table id="box-table-a" style="width: 300px;">
<tr>
    <th><?php echo __("Day", "Day"); ?></th>
    <th><?php echo __("PPH", "PPH"); ?></th>
</tr>
<? foreach($history as $h => $value): ?>
<tr>
    <td><?= convert_from_gmt_aprax($h, $dateFormat, $this->business_id)?></td>
    <td><?= number_format_locale($value['pph'],1) ?></td>
</tr>
<? endforeach; ?>	
</table>
<? if ($stationInfo->targetPPH > $actualPPH ): ?>
	<td valign="top"><table><tr><td><span style="font-size:40px; font-weight: bold; color: red"><?= $actualPPH ?> <?php echo __("PPH", "PPH"); ?></span></td></tr>
	
<? elseif ($stationInfo->targetPPH < $actualPPH): ?>
		<td valign="top"><table><tr><td><span style="font-size:45px; font-weight: bold; color: green">Bueno! <u><?= $actualPPH ?></u> <?php echo __("pph", "pph"); ?></span></td></tr>
	
<? else: ?>
	<td valign="top"><table><tr><td><span style="font-size:40px; font-weight: bold;"><?= $actualPPH ?> <?php echo __("PPH", "PPH"); ?></span></td></tr>
	
<? endif; ?>

<? if($picture): foreach($picture as $p): ?>
	<a href="https://s3.amazonaws.com/LaundryLocker/<?= $p->file ?>" target="_blank"><img src="https://s3.amazonaws.com/LaundryLocker/<?= $p->file ?>" alt="" width="250" height="220" border="0"></a>
<? endforeach; endif; ?>		
<tr>
<td><br><br>
<table border="0" cellspacing="0" cellpadding="3">
<tr><form action="/admin/employees/presser_scans_add" method="post">
        <td><?php echo __("Barcode", "Barcode:"); ?></td>
        <td><input type="text" name="barcode" id="barcode"></td>
        <td><input type="hidden" name="previousBarCode" value="<?= $lastItem ?> "></input></td>
        <td><input type="submit" name="add" value="<?php echo __("Submit", "Submit"); ?>"></td>
</form><script>$('#barcode').focus()</script>
</tr>
</table>
</td></tr></table>
</td></tr></table>
<? endif; ?>
