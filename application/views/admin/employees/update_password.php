<?php enter_translation_domain("admin/employees/update_password"); ?>
<h2><?php echo __("Update Password for", "Update Password for"); ?> <?= getPersonName($employee[0]) ?></h2>

<?=$this->session->flashdata('message')?>

<form action='' method='post'>
	<table class="detail_table">
		<tr>
			<th><?php echo __("New Password", "New Password"); ?></th>
			<td><?= form_password('password1')?></td>
		</tr>
		<tr>
			<th><?php echo __("New Password Again", "New Password Again"); ?></th>
			<td><?= form_password('password2')?></td>
		</tr>
	</table>
	<input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit'/>
	
</form>