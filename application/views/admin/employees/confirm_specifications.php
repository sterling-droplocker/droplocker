<?php enter_translation_domain("admin/employees/confirm_specifications"); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#barcode_form").submit(function() {
            var expected_barcode = $("input[name='specificationConfirm']").val();
            var entered_barcode = $("#barcode").val();
            if (expected_barcode != entered_barcode)
            {
                alert("<?php echo __("The entered barcode", "The entered barcode"); ?> " + entered_barcode + " <?php echo __("does not match the expected barcode", "does not match the expected barcode"); ?> " + expected_barcode);
                return false;
            }
            else
            {
                return true;
            }
        });
    });
    

</script>

<h2><?php echo __("Confirm Specifications for", "Confirm Specifications for"); ?> <?= $barcode ?></h2>
<br>
<div align="center"><?php echo __("This item has special notes.  Please make sure the notes were followed and scan again", "This item has special notes.  Please make sure the notes were followed and scan again."); ?><br>
<br>
<font size="9">
<?
foreach($specifications as $specification)
{
    echo $specification->description.'<br>';
}
?>
</font><br><br>
<form id='barcode_form' action="/admin/employees/presser_scans_add" method="post" name="myform">
<td><?php echo __("Barcode", "Barcode:"); ?></td>
<td><input type="text" name="barcode" id="barcode"></td>
<td><input type="hidden" name="specificationConfirm" value="<?=$barcode?>"><input type="submit" name="add" value="<?php echo __("Submit", "Submit"); ?>"></td>
</form>
<body OnLoad="document.myform.barcode.focus();"></div>
