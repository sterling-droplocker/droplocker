<?php enter_translation_domain("admin/employees/update_email"); ?>
<h2><?php echo __("Update Email", "Update Email"); ?></h2>
<?= $this->session->flashdata('message')?>
<p><?php echo __("A confirmation email will be sent to the new email address. The email will not be changed until the confirmation email is verified", "A confirmation email will be sent to the new email address. The email will not be changed until the confirmation email is verified"); ?></p>

<?php echo validation_errors(); ?>
<form action='' method="post">
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Current Email", "Current Email"); ?></th>
            <td><?= $employee->email?></td>
        </tr>
        <tr>
            <th><?php echo __("New Email", "New Email"); ?></th>
            <td><input type='text' name="new_email" /></td>
        </tr>
        
    </table>
    <br />
    <input type="submit" value='<?php echo __("Update Email", "Update Email"); ?>' name="submit" class="button orange" />
    <br />
    <br />
    <? if($employee->pendingEmailUpdate): 
        $p = unserialize($employee->pendingEmailUpdate);
    ?>
    <h3><?php echo __("You currently have a pending email request", "You currently have a pending email request"); ?></h3>
    <div style='padding:5px;'><a href='/admin/employees/delete_pending_email'><?php echo __("Delete", "Delete"); ?></a></div>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Pending Email", "Pending Email"); ?></th>
            <td><?= $p['email']?></td>
        </tr>
        <tr>
            <th><?php echo __("Request Date", "Request Date"); ?></th>
            <td><?= date("F j, Y", strtotime($p['date']))?></td>
        </tr>
    </table>
    <? endif ?>
    
</form>