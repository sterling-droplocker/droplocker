<?php enter_translation_domain("admin/locations/details"); ?>
<h2><?=__("Location Details", "Location Details"); ?> - <?= $location->address ?> &nbsp;&nbsp;&nbsp;</h2>
<br>
<form action="/admin/admin/location_details_add_note" method="post">
    <?= form_hidden("locationID", $location->locationID) ?>
    <div style="float:left; padding-right:10px;">
        <table id='box-table-a' style="margin-top:5px;">
        <thead>
           <tr>
                <th><?=__("Address", "Address"); ?></th>
                <th>
        <div style="float:left"><?= get_translation_for_view('location_address', "%address% %address2%, %city%, %state%", $location->to_array()) ?></div>
                    <div style="float:right"><a href="/admin/locations/display_update_form/<?= $location->locationID ?>"><img src="/images/icons/application_edit.png" alt="" border="0"></a></div>
                </th>
           </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=__("Stop", "Stop"); ?></td>
                <td><?=__("Route:", "Route:"); ?> <?= $location->route_id ?>  &nbsp;&nbsp;&nbsp;<?=__("Position:", "Position:"); ?> <?=$location->sortOrder ?></td>
            </tr>

            <tr>
                <td><?=__("Location Type", "Location Type"); ?></td>
                <td>
                     <?= $locationTypes[$location->locationType_id]?>
                </td>
            </tr>
             <tr>
                <td><?=__("Public Location", "Public Location"); ?></td>
                <td>
                    <?= $location->public ?>
                </td>
            </tr>
            <tr>
                <td><?=__("Service Type", "Service Type"); ?></td>
                <td>
                    <?= $location->serviceType ?>
                </td>
            </tr>
            <tr>
                <td> <?=__("Description", "Description"); ?> </td>
                <td> <?= $location->description ?> </td>
            </tr>

            <tr>
                <td> <?=__("Access Code", "Access Code"); ?> </td>
                <td> <?= $location->accessCode ?> </td>
            </tr>
            <tr>
                <td> <?=__("Internal notes", "Internal notes<br>about this<br>location"); ?></td>
                <td> <?= form_textarea(array("name" => "notes", "value" => $location->notes, "rows" => 4, "cols" => 80 )) ?>
                    <br><input type="submit" value="Add Note"><br>
                    <br>
                    <table>
                    <? foreach ($notes as $note): ?>
                    <tr>
                        <td><?= getPersonName($note) ?></td>
                        <td><?= convert_from_gmt_aprax($note->updated, STANDARD_DATE_FORMAT) ?></td>
                        <td><?= $note->note ?></td>
                    </tr>
                    <? endforeach; ?>
                    </table>

                </td>
            </tr>
        </tbody>
        </table>
    </div>
</form>
    <div style="float:left">
    <table id='box-table-a'  style="margin-top:5px;">
    <thead>
    <tr>
        <th><?=__("Locker", "Locker"); ?></th>
        <th><?=__("Style", "Style"); ?></th>
        <th><?=__("Lock Type", "Lock Type"); ?></th>
        <th><div style="float:left"><?=__("Status", "Status"); ?></div><div style="float:right">&nbsp;&nbsp;&nbsp;<a href="/admin/locations/update_lockers/<?= $location->locationID ?>"><img src="/images/icons/application_edit.png" alt="" border="0"></a></div></th>
    </tr>
    </thead>
    <tbody>
        <? foreach( $lockers as $locker ){ ?>
        <tr id="row-1">
            <td><span id='lockerName-1?>' class='click text'><?=$locker->lockerName;?></span> (<a href="/admin/reports/locker_history/<?=$locker->lockerID;?>" target="_blank"><?=__("history", "history"); ?></a>)</td>
            <td><span id='lockerStyle_id-1?>' class='click dd-style'><?=$locker->name;?></td>
            <td><span id='lockerLockType_id-1?>' class='click dd-lock'><?=$locker->lockName;?></td>
            <td><span id='lockerStatus-1?>' class='click dd_status'><?=$locker->lockerStatus;?></td>
        </tr>
        <? } ?>
   </tbody>
   </table>
   </div>
   <div style="clear:both">
    <h2>Customers</h2>
    <table id='box-table-a'  style="margin-top:5px;">
    <thead>
    <tr>
        <th><?=__("Name", "Name"); ?></th>
        <th><?=__("Email", "Email"); ?></th>
        <th><?=__("Phone", "Phone"); ?></th>
        <th><?=__("Address", "Address"); ?></th>
        <th><?=__("Signup", "Signup"); ?></th>
        <th><?=__("Orders", "Orders"); ?></th>
        <th><?=__("Last Order", "Last Order"); ?></th>
        <th><?=__("Gross", "Gross"); ?></th>
        <th><?=__("Discounts", "Discounts"); ?></th>
    </tr>
    </thead>
    <tbody>
        <? foreach($customers as $customer ){ ?>
        <tr id="row-1">
            <td><a href="/admin/customers/detail/<?= $customer->customerID?>" target="_blank"><?= getPersonName($customer) ?></a></td>
            <td><?= $customer->email ?></td>
            <td><?= $customer->phone ?></td>
            <td><?= get_translation_for_view('customer_address', "%address1% %address2% %city%, %state% %zip%", stdobj_to_array($customer) ) ?></td>
            <td><?= convert_from_gmt_aprax($customer->signupDate, SHORT_DATE_FORMAT) ?></td>
            <td><?= $customer->orderCount ?></td>
            <td><?= convert_from_gmt_aprax($customer->lastOrder, SHORT_DATE_FORMAT) ?></td>
            <td><?= format_money_symbol($customer->business_id, '%.2n', $customer->totGross) ?></td>
            <td><?= format_money_symbol($customer->business_id, '%.2n', $customer->totDisc) ?></td>
        </tr>
        <? } ?>
   </tbody>
   </table>
   </div>
