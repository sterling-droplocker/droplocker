<?php enter_translation_domain("admin/locations/master_routes"); ?>
<? if (is_null($route)): ?>
    <h2><?=__("Master Routes", "Master Routes"); ?></h2>
<? else: ?>
    <h2><?=__("Edit Master Route #", "Edit Master Route #"); ?><?= $route ?></h2>
<? endif; ?>

<div style="padding:10px 5px; overflow: auto;">

<? if (!is_null($route)): ?>
<form action="/admin/locations/auto_sort_master_route" method="post" id="sort_route" style="float: right;">
    <input type="hidden" name="route" value="<?= $route ?>">
    <input type="submit" value="<?=__("Auto Sort Route", "Auto Sort Route"); ?>" class="button red" onclick="return confirm('<?=__("Are you sure you want to sort all locations in this route?", "Are you sure you want to sort all locations in this route"); ?>');">
</form>
<? endif; ?>

<form action="/admin/locations/master_routes" method="post" id="select_route">
    <label for="route"><?=__("Select Route", "Select Route"); ?></label>
    <?= form_dropdown('route', array('' => '<?=__("--SELECT--", "--SELECT--"); ?>') + $routes, $route, 'id="route"'); ?>
</form>

</div>

<? foreach($locations as $location): ?>
    <? if ($location->lat == '' || $location->lon == ''): ?>
    <div class="alert-warning">
        <?=__("location_missing_lat_lon", 'Location %link% is missing lat/lon.', array("link"=>'<a href="/admin/locations/display_update_form/'.$location->locationID.'">'.$location->address.'</a>')); ?>
    </div>
    <? endif; ?>
<? endforeach; ?>


<div style="float:left;width:450px;max-height: 600px;overflow-y: auto;">
<table id="sort_routes" class="hor-minimalist-a striped padded">
<thead>
    <tr>
        <th align="center"><?=__("Sort", "Sort"); ?></th>
        <th align="center"><?=__("ID", "ID"); ?></th>
        <th><?=__("Address", "Address"); ?></th>
    </tr>
</thead>
<tbody>
<? foreach($locations as $location): ?>
    <tr id="location_<?=$location->locationID;?>">
        <td align="center">
            <span class="sort_order"><?= $location->masterSortOrder + 1  ?></span>
        </td>
        <td align="center"><?= $location->locationID ?></td>
        <td>
            <a href="/admin/locations/display_update_form/<?= $location->locationID ?>">
            <?= $location->address ?>
            <? if ($location->companyName): ?>
                [<b><?= $location->companyName ?></b>]
            <? endif; ?>
            </a>
        </td>
    </tr>
<? endforeach; ?>
</tbody>
</table>
</div>

<div id="locations_map" style="margin-left:20px; float: left; width:800px;height:600px; border:1px solid #ccc;"></div>

<div class="clear: both;"></div>

<div id="info_window" style="padding: 10px 5px; display:none; width:250px;">
    <input type="hidden" name="locationID" value="">
    <p><b><?=__("Address:", "Address:"); ?></b> <span class="location-address"></span></p>
    <p>
        <b><?=__("Sort Order:", "Sort Order:"); ?></b> <input type="text" name="sortOrder" size="4">
        <button id="update_location"><?=__("Update", "Update"); ?></button>
    </p>
</div>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=<?=get_default_gmaps_api_key();?>"></script>
<script type="text/javascript">

var locations = <?= json_encode($locations_map) ?>;

// submit on route selection
$('#route').change(function(evt) {
    $('#select_route').submit();
});

var sortedLocations = [];
$('#sort_routes tbody tr').each(function(i, e) {
    sortedLocations[i] = parseInt(e.id.replace('location_', ''));
});

var MapObject = {
    map : null,
    infoWindow: null,
    locationBounds: null,
    routePath: null,
    markers: {},
    points: {}
};

MapObject.getLocationLatLon = function (location) {
    var locLat = location.lat || '';
    var locLon = location.lon || '';

    if (locLat != '' && locLon != '') {
        return new google.maps.LatLng(locLat, locLon);
    } else {
        return false;
    }
};

MapObject.getLocationBoundsCenter = function () {
    MapObject.locationBounds = new google.maps.LatLngBounds();

    for (var i in locations) {
        var point = MapObject.getLocationLatLon(locations[i]);
        if (point) {
            MapObject.locationBounds.extend(point);
        } else {
            console.log("Missing lat/lon for locationID: " + i);
        }
    }

    return MapObject.locationBounds.getCenter();
};

MapObject.initMap = function() {
    var center = MapObject.getLocationBoundsCenter();
    var mapOptions = {
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: center
    };

    MapObject.map = new google.maps.Map(document.getElementById("locations_map"), mapOptions);
    google.maps.event.addListenerOnce(MapObject.map, 'idle', function () {
        MapObject.map.fitBounds(MapObject.locationBounds);
    });

    MapObject.infoWindow = new google.maps.InfoWindow();
    MapObject.infoWindow.setContent(document.getElementById("info_window"));
    MapObject.infoWindow.close();
    $('#info_window').show();
};

MapObject.clearMarkers = function() {
    for (var i in MapObject.markers) {
        MapObject.markers[i].setMap(null);
    }
    MapObject.markers = {};
    
    if (MapObject.routePath != null) {
        MapObject.routePath.setMap(null);
    }
    MapObject.routePath = null;
    
    MapObject.infoWindow.close();
};

MapObject.getDistance = function(a, b)  {
    function rad(x) { return x * Math.PI/180; }
    var R = 6371; // radius of earth in km
    var dLat  = rad(a.lat - b.lat);
    var dLong = rad(a.lon - b.lon);
    var distance = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(rad(b.lat)) * Math.cos(rad(b.lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
    var c = 2 * Math.atan2(Math.sqrt(distance), Math.sqrt(1-distance));
    var d = R * c;
    return d;
};

MapObject.getClosestLocation = function(pos, skip) {
    var closest = null;
    var min_d = 99999;

    for (var i in locations) {
        if (i === skip) {
            continue;
        }

        var distance = MapObject.getDistance(pos, locations[i]);
        if (distance < min_d) {
            min_d = distance;
            closest = i;
        }
    }

    return closest;
};

MapObject.drawLocations = function() {

    MapObject.clearMarkers();
    
    var routePath = [];
    for (var i in sortedLocations) {
        new function(i) {
            var locationID = sortedLocations[i];
            var location = locations[locationID];
            var order = parseInt(i) + 1;

            var icon = '/images/map/largeTDRedIcons/marker' + order + '.png';
            if (icon > 99) {
                icon = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld="+order+"|FE6256|000000";
            }

            var options = {
                position: new google.maps.LatLng(location.lat, location.lon),
                map: MapObject.map,
                title: location.locationID + ':'  + location.address,
                draggable:true,
                //animation: google.maps.Animation.DROP,
                icon: icon
            };
            
            var marker = new google.maps.Marker(options);
            var locationID = location.locationID;
            google.maps.event.addListener(marker, 'click', function() {
                MapObject.infoWindow.open(MapObject.map, MapObject.markers[locationID]);
                MapObject.initInfoWindowContent(locationID);
            });

            google.maps.event.addListener(marker, 'drag', function(event) {
                var pos = {lat: event.latLng.lat(), lon: event.latLng.lng()};
                var closest = MapObject.getClosestLocation(pos, null);

                for (var x in MapObject.markers) {
                    if (x !== closest) {
                        MapObject.markers[x].setAnimation(null);
                    }
                }

                if (typeof(MapObject.markers[closest]) != 'undefined' && closest !== locationID) {
                    MapObject.markers[closest].setAnimation(google.maps.Animation.BOUNCE);
                }

            });

            google.maps.event.addListener(marker, 'dragend', function(event) {
                var pos = {lat: event.latLng.lat(), lon: event.latLng.lng()};

                // revert to old position
                marker.setPosition(new google.maps.LatLng(location.lat, location.lon));

                for (var x in MapObject.markers) {
                    MapObject.markers[x].setAnimation(null);
                }

                var closest = MapObject.getClosestLocation(pos, null);
                if (typeof(MapObject.markers[closest]) != 'undefined' && closest !== locationID) {
                    var from = sortedLocations.indexOf(parseInt(locationID));
                    var to = sortedLocations.indexOf(parseInt(closest));

                    if (from < to) {
                        MapObject.moveMarker(to, from);
                    } else {
                        MapObject.moveMarker(from, to);
                    }
                }

            });

            MapObject.markers[location.locationID] = marker;

            routePath.push(new google.maps.LatLng(location.lat, location.lon));
        }(i);
    }

    MapObject.routePath = new google.maps.Polyline({
        path: routePath,
        strokeColor: '#8800dd',
        strokeOpacity: .6,
        strokeWeight: 4,
        map: MapObject.map
    });
};


MapObject.initInfoWindowContent = function(locationID) {
    var location = locations[locationID];
    var order = sortedLocations.indexOf(parseInt(locationID)) + 1;
    $('#info_window input[name=locationID]').val(locationID);
    $('#info_window input[name=sortOrder]').val(order);
    $('#info_window .location-address').html(location.address);
};


MapObject.udpateSort = function(updates) {
    $.post('/admin/locations/update_master_sort', {
        sort: updates
    }, function (response) {
        MapObject.drawLocations();
    });
};


MapObject.onSortingStop = function() {
    var locations = $('#sort_routes tbody').sortable("toArray");
    var updates = {};
    for (var i in locations) {
        $('#' + locations[i] + ' .sort_order').html(parseInt(i) + 1);

        var locationID = locations[i].replace('location_', '');
        if (sortedLocations[i] != locationID) {
            updates[i] = locationID;
            sortedLocations[i] = parseInt(locationID);
        }
    }
    MapObject.udpateSort(updates);
};

MapObject.moveMarker = function(from, to) {
    // if no change, exit here
    if (to == from) {
        return;
    }

    var d_from = $('#location_' + sortedLocations[from]);
    var d_to = $('#location_' + sortedLocations[to]);

    // move the dom
    d_from.insertBefore(d_to);

    MapObject.onSortingStop();
};

$(function() {

    // hide sidebar
    $('#toggle_link').click();

    $('#sort_routes tbody').sortable({
        stop: MapObject.onSortingStop
    });


    $('#update_location').click(function() {
        var locationID = $('#info_window input[name=locationID]').val();
        var sortOrder = parseInt($('#info_window input[name=sortOrder]').val());

        // do not allow non-existing index
        if (sortOrder > sortedLocations.length) {
            sortOrder = sortedLocations.length;
        }

        var to = sortOrder > 0 ? sortOrder - 1 : 0;
        var from = sortedLocations.indexOf(parseInt(locationID));

        MapObject.moveMarker(from, to);
    });

    MapObject.initMap();
    MapObject.drawLocations();

});
</script>
