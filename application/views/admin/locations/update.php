<?php
enter_translation_domain('admin/locations/update');

use App\Libraries\DroplockerObjects\Location;
/**
 * Expects the following content variables
 *  location
 */
$all_states = get_all_states_by_country_as_array();
$decimals = get_business_meta($this->business_id, 'decimal_positions', 2);
$money_symbol_prefix = get_business_meta($this->business_id, 'money_symbol_position');
$money_symbol = get_business_meta($this->business_id, 'money_symbol_str');

$fieldList = array();
$fieldList['address1'] = 
        "<tr>
                <th>" .  __('address', 'Address') . "</th>
                <td>" .  form_input('address', isset($location->address) ? $location->address : null)  . " </td>
            </tr>";
$fieldList['address2'] = 
        "<tr>
                <th>" .  __('address2', 'Address 2') . "</th>
                <td>" .  form_input('address2', isset($location->address2) ? $location->address2 : null)  . " </td>
            </tr>";
$fieldList['city'] = 
        "<tr>
                <th>*" .  __('city', 'City') . "</th>
                <td>
                    " .  form_input('city', isset($location->city) ? $location->city : '') . "
                </td>
            </tr>";
$fieldList['state'] = 
        (isset($all_states[$location->country])) ?
            "<tr>
                <th id='state_label'>" .  __('state', 'State') . "</th>
                <td id='state_row'>
                    " .  form_dropdown('state', $all_states[$location->country], isset($location->state) ? $location->state : null, 'id="state_select" ' )  . "
                </td>
            </tr>"
            :
            "<tr>
                <th id='state_label'>" .  __('locality', 'Locality') . "</th>
                <td id='state_row'>
                    <input type='text' name='state' value='" .  set_value('state', isset($location->state) ? $location->state : null ) . "' />
                </td>
            </tr>";
$fieldList['zip'] = 
        "<tr>
                <th>" .  __('zipcode', 'Postal code') . "</th>
                <td>
                    " .  form_input('zipcode', isset($location->zipcode) ? $location->zipcode : null)  . "
                    <span>* This is used when calculating taxes</span>
                </td>
            </tr>";
        
$addressForm = getSortedAddressForm($fieldList);

?>
<style type="text/css">
.detail_table input[type=text] {
    width: auto !IMPORTANT;
}
.image_picker_image {
    width: 125px;
    height: 50px;
}

</style>

<script type="text/javascript">
$(document).ready(function(){
   /**
    * #######################################################
    * BEGIN * WHARRGARBLE
    * #######################################################
    */
   var latitude;
   var logitude;
   var name;
   var address;
   var address2;
   var city;
   var state;
   var zipcode;
   var locationtypeID;
   var marker;
   var is_public;
   /*
    * #######################################################
    * END * WHARRGARBLE
    * #######################################################
    */
    $("#show_current_coordinates").click(function() {
        $('#map_div').css('display', 'block');

        var latlng = new google.maps.LatLng($('[name="lat"]').val(), $('[name="lon"]').val());
        var myOptions = {
            zoom: 20,
            center: latlng,
            mapTypeId: 'satellite'
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);

        marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable:true,
                title:"test"
            });
        google.maps.event.addListener(marker, 'dragend', function(){
        new_loc = marker.getPosition();
        $('[name="lat"]').val(new_loc.lat());
        $('[name="lon"]').val(new_loc.lng());
        });
    });

    //Example: http://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false
    var active = true;
    $("#show_button").loading_indicator({});
    $('#show_button').click(function(){
        address = $('[name="address"]').val();
        address2 = $('[name="address2"').val();
        city = $('[name="city"]').val();
        state = $('[name="state"]').val();
        zipcode = $('[name="zipcode"]').val();
        locationTypeID = $('[name="locationType_id"]').val();

        if(address=='') {
            alert('Address is blank');
            $("#show_button").loading_indicator("destroy");
            return false;
        }

        if(city=='')  {
            alert('City is blank');
            $("#show_button").loading_indicator("destroy");
            return false;
        }

        $.ajax({
          type: 'POST',
          data: "address="+address+"&city="+city+"&state="+state+"&zipcode"+zipcode,
          url: "/ajax/google_geocode/",
          success: function(data){
              if(data.status=='OK') {
                  var loc = data.results[0].geometry.location;
                  var zip_comp = data.results[0].address_components;

                  //if user has not entered zipcode, we try to set it from the data returned from google geocode
                  $.each(zip_comp, function(i,item){
                  	if(item.types == 'postal_code'){
                  		if(item.long_name !='' && $('[name="zipcode"]').val()==''){
                  			$('[name="zipcode"]').val(item.long_name);
                  		}
                  	}
                  });

                  longitude = loc.lng;
                  latitude = loc.lat;
                  $('[name="lat"]').val(latitude);
                  $('[name="lon"]').val(longitude);
                  $('#map_div').css('display', 'block');

                   var latlng = new google.maps.LatLng(latitude, longitude);
                   var myOptions = {
                      zoom: 20,
                      center: latlng,
                      mapTypeId: 'satellite'
                    };
                    var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);

                    marker = new google.maps.Marker({
                          position: latlng,
                          map: map,
                          draggable:true,
                          title:"test"
                      });
                     google.maps.event.addListener(marker, 'dragend', function(){
                        new_loc = marker.getPosition();
                        $('[name="lat"]').val(new_loc.lat());
                        $('[name="lon"]').val(new_loc.lng());
                     });
                     $("#show_button").loading_indicator("destroy");
              }
          },
          dataType: 'json'
        });
    });
});

</script>

<script type="text/javascript">

$(document).ready(function() {
    $(".delete_location_neighborhood").click(function() {
        if (confirm("Are you sure you want to delete this location neighborhood?")) {
            var $loading_image = $("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;'");

            $loading_image.insertAfter($(this));
            var $location_neighborhood = $(this);
            $.getJSON("/admin/location_neighborhoods/delete", {location_neighborhoodID : $(this).attr("id")}, function (result) {
                $loading_image.remove();
                if (result['status'] != "success") {
                    alert("Failed to delete location");
                }
                else {
                    $location_neighborhood.fadeOut();
                }
            });
        }
    });
    $("#locationType_id").change(function() {
        if ($(this).val() == 3) {
            $("#show_locker_name_on_receipt_container").show();
        }
        else {
            $("#show_locker_name_on_receipt_container").hide();
        }

        if ($(this).val() == 7) {
            $("#show_wholesale_logo_container").show();
        } else {
            $("#show_wholesale_logo_container").hide();
        }
    });
    $("#locationType_id").trigger("change");
    $("#launchDate_datePicker").datepicker({
        buttonImage: "/images/icons/calendar.png",
        altField: "#launchDate",
        dateFormat : "mm/dd/y"
    });

    var latlng = new google.maps.LatLng($("#lat").val(), $("#lon").val());
    var myOptions = {
      zoom: 20,
      center: latlng,
      mapTypeId: 'satellite'
    };
     var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);

     marker = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable:true,
        title:"test"
    });
        google.maps.event.addListener(marker, 'dragend', function(){
        new_loc = marker.getPosition();
        $('[name="lat"]').val(new_loc.lat());
        $('[name="lon"]').val(new_loc.lng());
    });

    // logic for country/state selection (internation - canada)
    // should share select attributes for name, so one state comes through
    var countrySubSelect = function(){
        var countrySelVal =  $('#country_select').val();

        if (typeof(statesOptions[countrySelVal]) != 'undefined') {
            var states = statesOptions[countrySelVal];
            $('#state_row').html('<select name="state" id="state_select"></select>');
            $('#state_select').html(buildStateOptions(states));
        } else {
            $('#state_label').text('Locality');
            $('#state_row').html('<input type="text" name="state" />');
        }
    };

    $('#country_select').bind('change', countrySubSelect );

    //creates a dropdown for states
    var buildStateOptions = function(states) {
        var selectMain = $('<select />');
        for (var key in states) {
            $('<option></option>').val(key).text(states[key]).appendTo(selectMain);
        }
        return selectMain.html();
    }

    $("#wholesale_logo_id").imagepicker();
});

    var statesOptions    = <?= json_encode($all_states) ?>;
    var businessCountry  = '<?=$businessCountry;?>';
    var businessState    = '<?=$businessState;?>';

</script>

<?php 
if (!empty($location)):
    // files upload
?>
    <link href="/js/fine-uploader/fine-uploader-new.css" rel="stylesheet">
    <script src="/js/fine-uploader/fine-uploader.js"></script>
    <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Select files</div>
                </div>
                <button type="button" id="trigger-upload" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Upload
                </button>
            </div>

            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <a class="qq-uploaded-file-link" href="" target="_blank"><img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale></a>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>
    <style>
        #trigger-upload {
            color: white;
            background-color: #00ABC7;
            font-size: 14px;
            padding: 7px 20px;
            background-image: none;
        }

        #fine-uploader-manual-trigger .qq-upload-button {
            margin-right: 15px;
        }

        #fine-uploader-manual-trigger .buttons {
            width: 36%;
        }

        #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
            width: 60%;
        }
    </style>
<?php 
endif;
?>
<? if (empty($location->locationID)): ?>
    <h2> New Location </h2>
    <p>
        <a href="/admin/locations/"><img src="/images/icons/arrow_left.png"> Go back to locations</a>
    </p>
<? else: ?>
    <h2>Edit Location <?= $location->address ?> </h2>
    <p>
        <a href="/admin/locations/"><img src="/images/icons/arrow_left.png"> Go back to locations</a>
        <a href="/admin/locations/update_lockers/<?= $location->locationID ?>"><img src="/images/icons/server_edit.png"> Edit Lockers</a>
    </p>
<? endif ?>
<form action="/admin/locations/update" method="POST">
    <?= form_hidden("locationID", isset($location->locationID) ? $location->locationID : null) ?>
    <table class='detail_table'>
        <tbody>
            <tr>
                <th> <?= __('title', 'Title'); ?> </th>
                <td>
                    <?= form_input("companyName", isset($location->companyName) ? $location->companyName : null)?>
                </td>
            </tr>
            <?= $addressForm ?>
            <tr>
                <th><?= __('location_neighborhoods', 'Neighborhood(s)'); ?></th>
                <td>
                    <input type="hidden" name="location_neighborhoods" id="location_neighborhoods" value="" />
                    <span id="preexisting_neighborhoods">
                        <? foreach($locationNeighborhoods as $loc_neighborhood ): ?>
                            <a href="javascript:;" title="Click to remove neighborhood from location" id="<?=$loc_neighborhood->location_neighborhoodID;?>" class="delete_location_neighborhood color_selector white"><?=$loc_neighborhood->neighborhood;?></a>
                        <? endforeach ?>
                    </span>
                    <button id="add_neighborhood" type="button"> + </button>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Route: <?= form_input(array("name" => "route_id", "value" =>  isset($location->route_id) ? $location->route_id : null,  "style" => "width: 50px;")) ?>
					&nbsp;&nbsp;&nbsp;Sort: <?= form_input(array("name" => "sortOrder", "value" => isset($location->sortOrder) ? $location->sortOrder : 0, "style" => "width: 50px")) ?>
                </td>
            </tr>
            <tr>
                <th><?= __('locationType_id', 'Location Type'); ?></th>
                <td>
                     <?= form_dropdown("locationType_id", $locationTypes, isset($location->locationType_id) ? $location->locationType_id : null, "id='locationType_id'") ?>
                    <div style="margin-top: 5px;" id="show_locker_name_on_receipt_container">
                        <?= form_label("Show Locker Name On Receipt", "show_locker_name_on_receipt") . form_checkbox("show_locker_name_on_receipt", 1, isset($location->show_locker_name_on_receipt) ? $location->show_locker_name_on_receipt : null, "id='show_locker_name_on_receipt'") ?>
                    </div>
                    <div style="margin-top: 5px" id="show_wholesale_logo_container">
                        <select name="wholesale_logo_id" class="image-picker" id="wholesale_logo_id">
                            <option value=""></option>
                            <?php foreach ($wholesale_logos as $wholesale_logo): ?>
                                <option data-img-src="/images/logos/<?= $wholesale_logo->filename; ?>" value="<?= $wholesale_logo->imageID; ?>"<?= ($location->wholesale_logo_id == $wholesale_logo->imageID) ? ' selected="selected"' : null; ?>>
                                    <?= $wholesale_logo->imageID; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </td>
            </tr>
             <tr>
                <th><?= __('public', 'Public Location'); ?></th>
                <td>
                    <? $public_select = array( 'no' => 'No', 'yes' => 'Yes' ); ?>
                    <?= form_dropdown("public",$public_select, isset($location->public) ? strtolower($location->public) : null ) ?>
                </td>
            </tr>
            <tr>
                <th><?= __('printManifest', 'Print Manifest on Driver Map'); ?></th>
                <td>
                    <? $printManifest_select = array( '0' => 'No', '1' => 'Yes' ); ?>
                    <?= form_dropdown("printManifest",$printManifest_select, isset($location->printManifest) ? $location->printManifest : "" ) ?>
                </td>
            </tr>
            <tr>
                <th><?= __('serviceType', 'Service Type'); ?></th>
                <td>
                <?php 
                $listServiceTypes = Location::listServiceTypes();
                foreach ($listServiceTypes as $k=>$v) {
                    $listServiceTypes[$k] = get_translation_for_view($v, $v);
                }
                ?>
                    <?= form_dropdown("serviceType", $listServiceTypes, isset($location->serviceType) ? $location->serviceType : "") ?>
                </td>
            </tr>
            <tr>
                <th><?= __('locationServices', 'Location Services'); ?></th>
                <td>
                    <? if (empty($location->locationID)): ?>
                        <? 
                            foreach ($service_types as $service_type): 
                                $service_type['displayName'] = get_translation_for_view($service_type['displayName'], $service_type['displayName']);
                        ?>
                            <div>
                                <?= form_checkbox("available_services[]", $service_type['serviceType_id'], TRUE) . " " . form_label($service_type['displayName'],"available_services[]") ?>
                            </div>
                        <? endforeach ?>
                    <? else: ?>
                        <? foreach( $service_types as $service_type ): ?>
                            <div>
                                <input type="checkbox" value="<?= $service_type['serviceType_id']; ?>" name="available_services[]" <?= (in_array($service_type['serviceType_id'],$existing_service_types)) ? 'checked' : ''; ?> > <?=$service_type['displayName'];?>
                            </div>
                        <? endforeach ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __('wf_min_amount', 'Wash &amp; Fold Minimum'); ?></th>
                <td>
					<!-- why is it assigning wash_fold_minimum_weight?? shouldn't it be business-wf_min_amount?-->
                    <input type="text" name="wf_min_amount" size="5" value="<?= isset($location->wf_min_amount) ? $location->wf_min_amount : $business->wash_fold_minimum_weight?>" /> &nbsp;
                    <select name="wf_min_type">
                        <option value="">- SELECT -</option>
                        <option value="dollars" <?= (isset($location->wf_min_type) && $location->wf_min_type == 'dollars') ? 'selected' : '';?>>dollars</option>
                        <option value="pounds" <?= (isset($location->wf_min_type) && $location->wf_min_type == 'pounds') ? 'selected' : '';?>><?= weight_system_format('',false,true);?></option>
                    </select>
                </td>
            </tr>
			<tr>
                <th><?= __('dc_min_amount', 'Dry Clean Minimum'); ?></th>
                <td>
                    <input type="text" name="dc_min_amount" size="5" value="<?= !empty($location->dc_min_amount) ? $location->dc_min_amount : $business->dc_min_amount?>" /> &nbsp;
                    <select name="dc_min_type">
                        <option value="">- SELECT -</option>
                        <option value="dollars" <?= (!empty($location->dc_min_type) && $location->dc_min_type == 'dollars') ? 'selected' : '';?>>dollars</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th><?= __('no_order_fee', 'No Order Fee'); ?></th>
                <td><?php if($money_symbol_prefix){echo $money_symbol;} ?> 
                    <?= form_input(array(
                        "name" => "no_order_fee",
                        "value" => isset($location->no_order_fee) ? number_format_locale($location->no_order_fee, $decimals) : number_format_locale("5.00", $decimals)
                    )); ?> 
                    <?php if(!$money_symbol_prefix){echo $money_symbol;} ?>
                </td>
            </tr>
            <tr>
                <th><?= __('residentManager', 'Resident Manager'); ?></th>
                <td> <?= form_input(array("name" => "residentManager", "value" => isset($location->residentManager) ? $location->residentManager : null)) ?> </td>
            </tr>
            <tr>
                <th><?= __('highTarget', 'High Target'); ?></th>
                <td>
                    <?php if($money_symbol_prefix){echo $money_symbol;} ?> 
                    <?= form_input(array("style" => "width: 50px", "name" => "highTarget", "value" => isset($location->highTarget) ? number_format_locale($location->highTarget, $decimals) : null )) ?>
                    <?php if(!$money_symbol_prefix){echo $money_symbol;} ?> 
                </td>
            </tr>
            <tr>
                <th><?= __('lowTarget', 'Low Target'); ?></th>
                <td>
                    <?php if($money_symbol_prefix){echo $money_symbol;} ?> 
                    <?= form_input(array("style" => "width: 50px", "name" => "lowTarget",  "value" => isset($location->lowTarget) ?  number_format_locale($location->lowTarget, $decimals) : null )) ?> 
                    <?php if(!$money_symbol_prefix){echo $money_symbol;} ?> 
                </td>
            </tr>
            <tr>
                <th><?= __('notes', 'Internal Notes'); ?></th>
                <td> <?= form_textarea(array("name" => "notes", "value" => isset($location->notes) ? $location->notes : null, "rows" => 4 )) ?> </td>
            </tr>
            <tr>
                <th><?= __('commission', 'Commission'); ?></th>
                <td> <?= form_input(array("name" => "commission",  "value" => isset($location->commission) ? $location->commission : null)) ?> </td>
            </tr>
            <tr>
                <th><?= __('commissionFrequency', 'Commission Frequency'); ?></th>
                <td> <?= form_input("commissionFrequency", isset($location->commissionFrequency) ? $location->commissionFrequency : null) ?> </td>
            </tr>
            <tr>
                <th><?= __('commissionAddr', 'Commission Address'); ?></th>
                <td> <?= form_textarea(array("name" => "commissionAddr", "value" => isset($location->commissionAddr) ? $location->commissionAddr : null, "rows" => 4)) ?> </td>
            </tr>
            <tr>
                <th><?= __('description', 'Description'); ?></th>
                <td>
                    <?=  form_textarea(array("name" => "description",  "value" => isset($location->description) ? $location->description : null));?>
                </td>
            </tr>
            <tr>
                <th><?= __('location', 'Lockers location'); ?></th>
                <td>
                    <?=  form_textarea(array("name" => "location",  "value" => isset($location->location) ? $location->location : null, "rows" => 4));?>
                </td>
            </tr>
            <?php 
            if (!empty($location)):
            ?>
            <tr>
                <th>
                    <?= __('Location Photos', 'Location Photos'); ?><br/><br/>
                    <small style="font-weight:normal"><?= __('Allowed types', 'Allowed types'); ?>: gif | jpg | png | jpeg</small>
                </th>
                <td>
                    <div id="fine-uploader-manual-trigger"></div>

                    <!-- Your code to create an instance of Fine Uploader and bind to the DOM/template
                    ====================================================================== -->
                    <script>
                        var manualUploader = new qq.FineUploader({
                            element: document.getElementById('fine-uploader-manual-trigger'),
                            template: 'qq-template-manual-trigger',
                            request: {
                                endpoint: '/admin/locations/upload_locker_images/<?=$location->locationID?>'
                            },
                            deleteFile: {
                                enabled: true, // defaults to false
                                forceConfirm: true,
                                endpoint: '/admin/locations/delete_locker_images/<?=$location->locationID?>'
                            },
                            thumbnails: {
                                placeholders: {
                                    waitingPath: '/js/fine-uploader/placeholders/waiting-generic.png',
                                    notAvailablePath: '/js/fine-uploader/placeholders/not_available-generic.png'
                                }
                            },
                            autoUpload: false,
                            callbacks: {
                                onComplete: function(id, filename, responseJSON) {
                                    if (responseJSON.success) {
                                        var path = '/images/lockerImages/'+responseJSON.newUuid;
                                        $('.qq-file-id-'+id+' img').wrap('<a href="'+path+'" target="_blank"></a>');
                                    }
                                }
                            },
                            debug: true
                        });

                        <?php 
                        if (!empty($location->lockerImages)): 
                        ?>
                            manualUploader.addInitialFiles(<?=$location->lockerImages?>);
                            $.each(manualUploader.getUploads(), function(k, v){
                                var path = '/images/lockerImages/'+v.uuid;
                                $('.qq-file-id-'+v.id+' img').wrap('<a href="'+path+'" target="_blank"></a>');
                            });
                        <?php 
                        endif; 
                        ?>

                        qq(document.getElementById("trigger-upload")).attach("click", function() {
                            manualUploader.uploadStoredFiles();
                        });

                        $("#fine-uploader-manual-trigger").keypress(
                          function(event){
                            if (event.which == '13') {
                              event.preventDefault();
                            }
                        });
                    </script>
                </td>
            </tr>
            <?php 
            endif;
            ?>
            <tr>
                <th><?= __('hours', 'Hours'); ?></th>
                <td> <?= form_input(array("name" => "hours",  "value" => isset($location->hours) ? $location->hours : null)) ?> </td>
            </tr>
            <tr>
                <th><?= __('accessCode', 'Access Code'); ?></th>
                <td> <?= form_textarea(array("name" => "accessCode", "value" => isset($location->accessCode) ? $location->accessCode : null, "rows" => 4)) ?> </td>
            </tr>
            <tr>
                <th><?= __('launchDate', 'Launch date'); ?></th>
                <td>
                    <?=
                        form_input(array("id" => "launchDate_datePicker", "value" => isset($location->launchDate) ? convert_from_gmt_aprax($location->launchDate, "m/d/y", $location->business_id) : ""))
                        //form_hidden(array("id" => "launchDate","name" => "launchDate", "value" => convert_from_gmt_aprax($location->launchDate, "m/d/y", $location->business_id)))
                    ?>
                    <input type="hidden" id="launchDate" name="launchDate" value="<?=isset($location->launchDate) ? convert_from_gmt_aprax($location->launchDate, "m/d/y", $location->business_id) : ""?>" />
                </td>
            </tr>
            <tr>
                <th><?= __('units', 'Units'); ?></th>
                <td> <?= form_input(array("name" => "units",  "value" => isset($location->units) ? $location->units : null)) ?></td>
            </tr>
            <tr>
                <th><?= __('masterRoute_id', 'Master Routes'); ?></th>
                <td>Master Route: <input type='text' value='<?= isset($location->masterRoute_id) ? $location->masterRoute_id : ""?>' id='masterRoute_id' name="masterRoute_id" style="width:30px;"/>
					&nbsp;&nbsp;&nbsp;Master Sort: <input type='text' value='<?= isset($location->masterSortOrder) ? $location->masterSortOrder : ""?>' id='masterSortOrder' name="masterSortOrder" style="width:30px;"/>
                </td>
            </tr>
            <tr>
                <th><?= __('quarter', 'Quarter'); ?></th>
                <td> <?= form_input(array("name" => "quarter",  "id" => "quarter")) ?></th>
            </tr>
            <tr>
                <th><?= __('lat', 'Latitude'); ?></th>
                <td> <input readonly="readonly" type="textbox" name="lat" value="<?= isset($location->lat) ? $location->lat : ""?>" /> </td>
            </tr>
            <tr>
                <th><?= __('lon', 'Longitude'); ?></th>
                <td> <input readonly="readonly" type="textbox" name="lon" value="<?= isset($location->lon) ? $location->lon : ""?>" /> </td>
            </tr>
            <tr>
                <th><?= __('turnaround', 'Turnaround Time'); ?></th>
                <td> <input type="text" name="turnaround" value="<?= $turnaround?>" /> days </td>
            </tr>
            <tr>
                <th><?= __('custom1', 'Custom 1'); ?></th>
                <td><?= form_input(array("name" => "custom1",  "value" => isset($location->custom1) ? $location->custom1 : "")) ?> </td>
            </tr>
            <tr>
                <th><?= __('custom2', 'Custom 2'); ?></th>
                <td><?= form_input(array("name" => "custom2",  "value" => isset($location->custom2) ? $location->custom2 : "")) ?> </td>
            </tr>
            <? if (empty($location->locationID)): ?>
                <tr>
                    <th><?= __('serviceDays', 'Service Days'); ?></th>
                    <td>
                        <table>
                            <thead>
                                <tr>
                                    <th> Day </th>
                                    <th> Pick. </th>
                                    <th> Dlv. </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?
                                    $days = array(1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday", 7 => "Sunday");
                                ?>

                                <!-- The following statements display the service days. -->
                                <? foreach ($days as $index => $day): ?>
                                    <?
                                        $checked = false;
                                        if (in_array($index, array(1,2,3,4,5))) {
                                            $checked = TRUE;
                                        }
                                    ?>
                                    <tr>
                                        <td>
                                            <?= form_label($day) ?>
                                        </td>
                                        <!-- This is the pickup day column -->
                                        <td>
                                            <?= form_checkbox("pickupDays[]", $index, $checked) ?>

                                        </td>
                                        <!-- This is the delivery day column -->
                                        <td>
                                            <?= form_checkbox("deliveryDays[]", $index, $checked) ?>
                                        </td>
                                    </tr>
                                <? endforeach ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <? endif ?>
            <tr>
                <th><?= __('deliv_radius', '3rd Party Home Delivery Radius'); ?></th>
                <td>
                    <?= form_input(array("name" => "deliv_radius",  "value" => isset($location->deliv_radius) ? $location->deliv_radius : "")) ?> miles
                    ( set this to 0 if not using a 3rd party home delivery service in this location )
                </td>
            </tr>
            <tr>
                <th><?= __('deliv_pickup', '3rd Party Home Pickup Radius'); ?></th>
                <td>
                    <?= form_input(array("name" => "deliv_pickup",  "value" => isset($location->deliv_pickup) ? $location->deliv_pickup : "")) ?> miles
                    ( set this to 0 if not using a 3rd party home delivery service in this location )
                </td>
            </tr>
            <tr>
                <th><?= __('deliv_id', 'Deliv ID'); ?></th>
                <td>
                    <?= form_input(array("name" => "deliv_id",  "value" => isset($location->deliv_id) ? $location->deliv_id : "")) ?> (Leave empty to disable Deliv in this location)

                    <br><br><a href="/admin/home_delivery/locations" target="_blank">List Deliv Locations</a>
                </td>
            </tr>
            <tr>
                <th><?= __('zip_codes', '3rd Party Service Area ZIP Codes'); ?></th>
                <td>
                    <?= form_textarea(array("name" => "zip_codes",  "value" => isset($location->zip_codes) ? $location->zip_codes : "", "rows"=>4, "cols"=>10)) ?>
                    <br/>( Comma separated zip codes list. )
                </td>
            </tr>
        </tbody>

        <tfoot>
            <tr>
                 <td colspan='2'>
                    <p>
                        <?

                        echo(form_button(array("content" => "Get Location Coordinates from Address", "class" => "button blue", "style" => "margin-right: 5px;", "id" => "show_button")));
                        echo(form_button(array("content" => "Show Current Location Coordinates", "class" => "button blue", "style" => "margin-right: 5px;", "id" => "show_current_coordinates")));
                        if (empty($location->locationID)) {
                            echo(form_submit(array("value" => "Add Location", "class" => "button orange")));
                        }
                        else {
                            echo(form_submit(array("value" => "Update Location", "class" => "button orange")));
                        }
                        ?>
                    </p>
                 </td>
            </tr>
        </tfoot>
    </table>

    <div id='map_div' style='padding:5px; display: none;'>

        <div id="map_canvas" style="float:left; width:400px; height:400px;padding:5px 0px;"></div>
        <p>Hint: You can drag the icon to the exact location.</p>

    </div>
</form>

<!-- UI FOR ADDING LOCATION NEIGHBORHOODS ;?>
<!-- add neighborhood component -->
<div id="add_neighborhood_form" style="display:none;" title="Add Location Neighborhood">
    <p>
        <select id="neighborhood_select">
            <option value="">Select Neighborhood</option>
            <? foreach($businessNeighborhoods as $neighborhood ){ ?>
            <option value="<?=$neighborhood->neighborhood;?>"><?=$neighborhood->neighborhood;?></option>
            <? } ?>
        </select>
    </p>
    <p>
        Or add new neighborhood: <input type="text" id="new_neighborhood" />
    </p>
    <p id="neighborhood_error" style="color:red;"></p>
    <button id="add_location_neighborhood">Add</button>
</div>

<script type="text/javascript">
    $(function(){
        //click binding for triggering dialog
        $('#add_neighborhood').click(function(e){
            e.preventDefault();
            var buttonRef = this;
            $('#add_neighborhood_form').dialog({
                close: function( event, ui ){
                    $( buttonRef ).show();
                }
            });
            $(this).hide();
        });

        //so we don't get two inputs
        $('#neighborhood_select').focus(function(){
           $('#new_neighborhood').val('');
        });
        //same as above
        $('#new_neighborhood').focus(function(){
           $('#neighborhood_select').val('');
        });

        addNeighborhoodClick();
        adjustNeighborhoodOptions();
    });

    //click binding
    var addNeighborhoodClick = function(){

        $('#add_location_neighborhood').click(function(){
            $('#neighborhood_error').html('');

            //pull value from dialog
            var neighborhood = $('#neighborhood_select').val();

            if( neighborhood == '' ){
                neighborhood = $('#new_neighborhood').val();
            }

            if(neighborhood.length > 0 ){
                $('#add_neighborhood_form').dialog('close');

                addNeighborhoodValue( neighborhood );
                addNeighborhoodChicklet( neighborhood );
                removeNeighborhoodOption( neighborhood );

            }else{
                $('#neighborhood_error').html('<span style="color:red;">Enter valid neighborhood</span>');
            }

        });
    };

    //adds the value to the form input
    var addNeighborhoodValue = function( neighborhoodName ){
                //get value of form field
                var loc_neighs = $('#location_neighborhoods').val();
                var update_neighs = [];
                //checks to see if any data set already
                if( undefined == loc_neighs || 1 > loc_neighs.length ){
                    //no neighborhoods!
                    update_neighs = [ neighborhoodName ];
                    $('#location_neighborhoods').val( JSON.stringify( update_neighs ) );
                }else{
                    update_neighs = JSON.parse( loc_neighs );
                    update_neighs.push( neighborhoodName );
                    $('#location_neighborhoods').val( JSON.stringify( update_neighs ) );
                }
                return true;
    };

    //adds the link chicklet
    var addNeighborhoodChicklet = function( neighborhoodName ){
        $('<a href="javascript:;" class="color_selector white">' + neighborhoodName + '</a>').appendTo('#preexisting_neighborhoods');
        return true;
    };

    //when adding a neighbhorhood, this will remove added neighborhoods from the dropdown
    var removeNeighborhoodOption = function( neighborhoodName ){
        $('option[value="' + neighborhoodName + '"]').remove(); //no need to check preexisting, if we dont have it, no worries
        return true;
    };

    //remove or add neccessary options for select
    var adjustNeighborhoodOptions = function(){
        $('#preexisting_neighborhoods').children().each(function( ind, el ){
            var n_name = $(el).text();
            $('#neighborhood_select option[value="' + n_name + '"]').remove();
        });
    };

</script>
