<?php enter_translation_domain("admin/locations/search_lockers"); ?>
<?= $this->session->flashdata('message')?>
<h2><?=__("Search Lockers", "Search Lockers"); ?></h2>

<form action='' method='post'>
	<table width='100%'>
		<tr>
			<td><label><?=__("Locker Id", "Locker Id"); ?></label><br><input type='text' id='locker' name='locker' /> 
				<input class='button orange' id='locker_search_button' type='button' value='<?=__("Search", "Search"); ?>' name='submit'/></td>
			
		</tr>
	</table>
</form>
<br>
<div id='results'></div>