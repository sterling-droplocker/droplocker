<?php enter_translation_domain("admin/maintenance_log/index"); ?>
<h2><?php echo __("Maintenance Log", "Maintenance Log"); ?></h2>

<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("Date", "Date"); ?></th>	
		<th><?php echo __("Employee", "Employee"); ?></th>	
		<th><?php echo __("Machine", "Machine"); ?></th>	
		<th><?php echo __("Work Completed", "Work Completed"); ?></th>	
	</tr>
</thead>
<tbody>
	<? if($maintenanceLogs): foreach($maintenanceLogs as $maintenanceLog):?>
	<tr>
		<td><?= $maintenanceLog->datePerformed->format("m/d/y h:i a") ?></td>
                <td><?= getPersonName($maintenanceLog->relationships['Employee'][0]) ?></td>
		<td><?= $maintenanceLog->machine ?></td>
		<td><?= $maintenanceLog->workPerformed ?></td>
	</tr>	

	<? endforeach; else: ?>
	<tr><td colspan="6"><?php echo __("No Maintenance Log", "No Maintenance Log"); ?></td></tr>
	<? endif; ?>
</tbody>
<tfoot>
	<tr>
		<form action="/admin/maintenance_log/add" method="post">
		<td colspan=2>&nbsp;</td>
		<td><input type="text" name="machine"></td>
		<td><textarea cols="50" rows="4" name="workDone"></textarea><br><br>
			<input type="submit" value="<?php echo __("Add", "Add"); ?>" class="button blue"></td>
		</form>
	</tr>
</tfoot>
</table>
