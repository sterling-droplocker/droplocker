<?php enter_translation_domain("admin/notices/add_notice"); ?>
<script>
	window.onload = function()
	{

		CKEDITOR.replace( 'noticeText', {
             uiColor : false
		});
	};
</script>

<h2><?php echo __("Edit Notice", "Edit Notice"); ?></h2>

<form action='' method='post'>

<table class='detail_table'>
    <tr>
        <th><?php echo __("Notice Name", "Notice Name"); ?></th>
        <td>
        <span style='color:red'><?php echo __("Must start with notice", "Must start with \"notice\". Example noticetomanyitems"); ?></span><br>
        <input type='text' value="" name='barcodeText'/></td>
    </tr>
    <tr>
        <td colspan='2'>
        <p><?php echo __("PLEASE NOTE: This is for the receipt printer. The text will wrap to fit on narrow paper", "PLEASE NOTE: This is for the receipt printer. The text will wrap to fit on narrow paper"); ?></p>
        <textarea name='noticeText'></textarea>
        </td>
    </tr>
</table>
<input type='submit' name='submit' class='button orange' value='<?php echo __("Submit", "Submit"); ?>' />
</form>
