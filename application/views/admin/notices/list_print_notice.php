<?php enter_translation_domain("admin/notices/list_print_notice"); ?>
<?php

$full_base = get_full_base_url();

?>
<h2>Print Notice</h2>
<p>
    <?php if (isset($master_business) && $business_id == $master_business->businessID): ?>
        <a href="/admin/superadmin/add_master_deliveryNotice"> <?php echo __("Add Master Business Notice", "Add Master Business Notice"); ?> </a>
    <?php elseif (!in_bizzie_mode() || is_superadmin()): ?>
        <a href='/admin/notices/add'><?php echo __("Add Notice", "Add Notice"); ?></a>
    <?php endif ?>
</p>
<table class='box-table-a'>
    <tr>
        <th><?php echo __("Notice Name", "Notice Name"); ?></th>
        <th><?php echo __("Barcode", "Barcode"); ?></th>
        <th width='25'><?php echo __("Print", "Print"); ?></th>
        <?php if (!in_bizzie_mode() || is_superadmin()): ?>
            <th width='25'><?php echo __("Edit", "Edit"); ?></th>
        <?php else: ?>
            <th width='25'> <?php echo __("View", "View"); ?> </th>
        <?php endif ?>
        <?php if (!in_bizzie_mode() || is_superadmin()): ?>
            <th width='25'><?php echo __("Delete", "Delete"); ?></th>
        <?php endif ?>
    </tr>
    <?foreach($deliveryNotices as $notice): ?>
    <tr>
        <td>
            <?= $notice->barcodeText ?>
        </td>
        <td>
            <img src="<?= $full_base ?>/barcode.php?barcode=<?= $notice->barcodeText?>&width=<?= strlen($notice->barcodeText) * 22?>">
        </td>
        <td align='center'>
            <a href='http://<?= $_SERVER['HTTP_HOST'] ?>/admin/printer/print_receipts/<?= $notice->barcodeText ?>/<?=$printer_key?>/<?=$business_id?>'><img src='/images/icons/printer.png' ></a>
        </td>
        
        <?php if (!in_bizzie_mode() || is_superadmin()): ?>
        <td align='center'>
            <a id="edit-<?=$notice->deliveryNoticeID?>" href='/admin/notices/edit/<?= $notice->deliveryNoticeID?>'><img src='/images/icons/application_edit.png' ></a>
        </td>       
        <?php endif ?>
        

        <?php if (!in_bizzie_mode() || is_superadmin()): ?>
            <td align='center'>
                <!-- Note, the 'id' attribute is used by Selenium tests -->
                <a id="delete-<?=$notice->deliveryNoticeID?>" href='/admin/notices/delete/<?= $notice->deliveryNoticeID?>'><img src='/images/icons/cross.png' ></a>
            </td>
        <?php endif ?>
    </tr>
    <?endforeach;?>
</table>
<p><a target='_blank' href='http://<?= $_SERVER['HTTP_HOST'] ?>/admin/printer/print_all_notices/<?= $business_id?>'><?php echo __("Print All Barcodes", "Print All Barcodes"); ?></a></p>
