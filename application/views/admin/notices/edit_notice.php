<?php enter_translation_domain("admin/notices/edit_notice"); ?>
<?php

$full_base = get_full_base_url();

?>
<script type="text/javascript">
    window.onload = function()
    {

        CKEDITOR.replace( 'noticeText', {
            uiColor : false,
            toolbar:'MyToolbar'
        });
    };
</script>

<h2><?php echo __("Edit Notice", "Edit Notice"); ?></h2>

<form action='' method='post'>

<table class='detail_table'>
    <tr>
        <th><?php echo __("Notice Name", "Notice Name"); ?></th>
        <td>
            <?php if (!in_bizzie_mode() || is_superadmin()): ?>
                <input type='text' value="<?= $notice->barcodeText ?>" name='barcodeText'/>
            <?php else: ?>
                <?= $notice->barcodeText?>
            <?php endif ?>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Barcode", "Barcode"); ?></th>
        <td>
            <img src="<?= $full_base ?>/barcode.php?barcode=<?= $notice->barcodeText?>&width=700">
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <p><?php echo __("PLEASE NOTE: This is for the receipt printer. The text will wrap to fit on narrow paper", "PLEASE NOTE: This is for the receipt printer. The text will wrap to fit on narrow paper"); ?></p>
            <?php if (!in_bizzie_mode() || is_superadmin()): ?>
                <textarea name='noticeText'> <?= $notice->noticeText?> </textarea>
            <?php else: ?>
                <?= $notice->noticeText ?>
            <?php endif ?>
        </td>
    </tr>
</table>
    <?php if (!in_bizzie_mode() || is_superadmin()): ?>
        <input type='submit' name='submit' class='button orange' value='<?php echo __("Submit", "Submit"); ?>' />
    <?php endif ?>
</form>
