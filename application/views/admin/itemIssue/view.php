<?php enter_translation_domain("admin/itemIssue/view"); ?>
<h1> <?php echo __("Item Barcode", "Item Barcode"); ?> <?= $barcode->barcode ?> </h1> 
<?php
$total = count($itemIssues);

foreach ($itemIssues as $index => $itemIssue){

    //if the image doesn't come through, show the default [ blank ] images.
    $image_fallback = '';
    
    //see if we can access the image, if no good use the fallback below
    if( empty( $itemIssue->file ) ){
        switch( $itemIssue->displayName ){
            case 'Laundered Shirt':
                    $image_fallback = '/images/item_launderedShirt.jpg';
                    break;
            case 'Blouse, Sweater, Shirt':
                    $image_fallback =  '/images/item_launderedShirt.jpg';
                    break;
            case 'blazer':
                    $image_fallback =  '/images/item_blazer.jpg';
                    break;
            case 'polo':
                    $image_fallback =  '/images/item_polo.jpg';
                    break;
            default:
                    $image_fallback =  '/images/item_onePiece.jpg';
                    break;
        }   
    }else{
        $image_fallback = 'https://s3.amazonaws.com/LaundryLocker/' .  $itemIssue->file;
    } 
    
    if($index != $total){ 
        echo '<div style="page-break-after:always; WIDTH: 450px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">'; 
    }else{ 
        echo '<div style="WIDTH: 450px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">';   
    }
    echo "<table><tr><td></td>
    </tr><tr><td align='left' valign='top'>";


    /* -------------------- */
    echo '<div class="photo' . $itemIssue->itemIssueID . '">
            <a href="#">
                    <span></span>
                    <input type="image" src="';
                    echo $image_fallback;
    
    echo '" style="max-width:300px;">
                    </a>
            </div>

            <style type="text/css">
            .photo' . $itemIssue->itemIssueID . ' {
                    margin: 0px;
                    position: relative;
            }';

            if(!empty($itemIssue->x))
            {
                    echo "
                    .photo" . $itemIssue->itemIssueID . " span {
                        width: 48px;
                        height: 48px;
                        display: block;
                        position: absolute;
                        top: ". ($itemIssue->y-24) ."px;
                        left: " .($itemIssue->x-24)."px;
                        background: url('/images/target.png') no-repeat;
                    }";
            }
            echo '</style>';
    /* -------------------- */
    echo '</td>
    <td align="left" valign="top">
    <table><tr style="font-size: 24px;"><td valign="top"><u>'. $itemIssue->issueType .' / '.$itemIssue->frontBack .'</u><br>'. $itemIssue->note .'</td></tr></table>
    </td>
    </tr></table><br>
    </div>';
}
?>
<a class='button blue' href='/admin/orders/item_details/item_id/<?=$item->itemID?>/order/<?=$order->orderID?>'><?php echo __("continue", "continue"); ?></a>