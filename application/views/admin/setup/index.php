<?php enter_translation_domain("admin/setup/index"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = form_label(__("First Name", "First Name")) . ' ' . form_input("firstName");
$fieldList['lastName'] = form_label(__("Last Name", "Last Name")) . ' ' . form_input("lastName");
            
$nameForm = getSortedNameForm($fieldList);

?>
<script type="text/javascript">
$(document).ready(function() {

$("#server_setup").validate( {
    rules :{
        mode: {
            required: true
        },
        email_host: {
            required: true
        },
        email_password: {
            required: true
        }
    },
    errorClass : "invalid"
});
$(":submit").loading_indicator();

});
</script>

<style type="text/css">
    fieldset
    {
        border: 1px solid;
        margin: 5px 0px;
    }
    fieldset legend
    {
        font-weight: bold;
    }
</style>
<h1> <?php echo __("Droplocker Server Setup", "Droplocker Server Setup"); ?> </h1>
<?= form_open("/admin/setup/update_server_metadata", array("id" => "server_setup")) ?>
<fieldset>'
    <legend> <?php echo __("Mode Settings", "Mode Settings"); ?></legend>
    <?= form_label(__("Operation Mode", "Operation Mode")) ?> <?= form_dropdown("mode", $modes) ?>
</fieldset>
<fieldset>
    <legend> <?php echo __("Email Settings", "Email Settings"); ?> </legend>
    <?= form_label(__("Host", "Host")) ?> <?= form_input("email_host") ?>
    <?= form_label(__("User", "User")) ?> <?= form_input("email_user") ?>
    <?= form_label(__("Password", "Password")) ?> <?= form_password("email_password") ?>
</fieldset>

<fieldset> 
    <legend> <?php echo __("Super Admin Account", "Super Admin Account"); ?></legend>
    <?= $nameForm ?>
    <?= form_label(__("Email", "Email")) ?> <?= form_input("email") ?>
    <?= form_label(__("Password", "Password")) ?> <?= form_password("password") ?>
    
</fieldset>
<fieldset>
    <legend> <?php echo __("Business", "Business"); ?> </legend>
    <?= form_label(__("Company Name", "Company Name")) ?> <?= form_input("companyName") ?>
</fieldset>

<?= form_submit(array("class" => "button orange", "value" => __("Create", "Create"))) ?>
<?= form_close() ?>

