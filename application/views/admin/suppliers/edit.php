<?php enter_translation_domain("admin/suppliers/edit"); ?>
<h2><?php echo __("Edit Supplier", "Edit Supplier"); ?></h2>
<p><a href="/admin/suppliers"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Suppliers", "Back to Suppliers"); ?></a></p>

<?= $this->session->flashdata('message');?>

<form action='' method='post'>
	<table class="detail_table">
		<tr>
			<th style="vertical-align: top"><?php echo __("Company Name", "Company Name"); ?></th>
			<td><input type="text" id="companyName" name="companyName" value="<?= $supplierInfo[0]->companyName ?>" /></td>
		</tr>
		<tr>
			<th style="vertical-align: top"><?php echo __("Notes", "Notes"); ?></th>
			<td><textarea name='notes' style='width:100%; height:100px;'><?= $supplierInfo[0]->notes?></textarea></td>
		</tr>
                <tr>
			<th style="vertical-align: top">Default for Routes</th>
                        <td>
                            <? foreach($allRoutes as $a): ?>
                            <input type="checkbox" name="supplierRoutes[]" value="<?= $a->route_id ?>"
                                   <?
                                    foreach($supplierRoutes as $s)
                                    {
                                        if($s->route_id == $a->route_id) echo ' checked';
                                    }
                                   ?>
                              /><?= $a->route_id ?>&nbsp;&nbsp;&nbsp;&nbsp;
                            <? endforeach; ?>
                            <select name="productType">
                            	<option value="WF" <?= @$supplierRoutes[0]->productType == 'WF' ? ' SELECTED': ''; ?>><?php echo __("Just Wash And Fold", "Just Wash & Fold"); ?></option>
                            	<option value="DC" <?= @$supplierRoutes[0]->productType == 'DC' ? ' SELECTED': ''; ?>><?php echo __("Just Dry Cleaning", "Just Dry Cleaning"); ?></option>
                            	<option value="ALL" <?= @$supplierRoutes[0]->productType == 'ALL' ? ' SELECTED': ''; ?>><?php echo __("All", "All"); ?></option>
                            </select>
                        </td>
		</tr>
                <tr>
			<th style="vertical-align: top"><?php echo __("Service provided only on the following days", "Service provided only on the following days :<br /> (none selected means all days)"); ?></th>
                        <td>
                            <? foreach($allDays as $a): ?>
                            <input type="checkbox" name="supplierServiceDays[]" value="<?= $a->dayID ?>"
                                   <?
                                    foreach($supplierDays as $s)
                                    {
                                        if($s->day_id == $a->dayID) echo ' checked';
                                    }
                                   ?>
                              /><?= $a->name ?>&nbsp;&nbsp;&nbsp;&nbsp;
                            <? endforeach; ?>
                        </td>
		</tr>
		<tr>
			<th style="vertical-align: top"><?php echo __("Enabled", "Enabled:"); ?></th>
			<td><?= form_yes_no('enabled', $supplierInfo[0]->enabled); ?></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><input type='submit' value="<?php echo __("Update", "Update"); ?>" name='submit' class='button orange' /></td>
		</tr>

	</table>
	<br>

</form>
<h2><?php echo __("Pricing", "Pricing"); ?></h2>
<table id="box-table-a">
<thead>
    <th><?php echo __("ID", "ID"); ?></th>
	<th><?php echo __("Product", "Product"); ?></th>
	<th><?php echo __("Process", "Process"); ?></th>
	<th><?php echo __("Price", "Price"); ?></th>
	<th><?php echo __("Cost", "Cost"); ?></th>
	<th><?php echo __("Min Price", "Min Price"); ?></th>
	<th><?php echo __("Last Updated", "Last Updated"); ?></th>
	<th></th>
</thead>
<tbody>
<form action="/admin/suppliers/add_product" method="post">
<input type="hidden" name="supplier" value="<?= $supplierInfo[0]->supplierID ?>">
	<tr>
        <td></td>
		<td><select name="product" id='product'>
			<option value=''><?php echo __("SELECT", "-- SELECT --"); ?></option>
			<? foreach($products as $p): ?>
			<option value="<?= $p->productID ?>"><?= $p->name ?></option>
			<? endforeach; ?>
			</select>
		</td>
		<td id='select_holder'><select id='process_id' name='product_process_id'></select><span id='img_holder'></span></td>
		<td></td>
		<td><input type="text" name="cost"></td>
		<td><input type="text" name="minQty"></td>
		<td colspan=2><input type="submit" name="add" value="<?php echo __("Add", "Add"); ?>" class="button blue"></td>
	</tr>
</form>
<? foreach($supplierPricing as $s):
$process_name = ($s->process_name!='')?$s->process_name:"N/A";
?>
<tr>
    <td><?= $s->product_processID ?></td>
	<td><?= $s->name ?> (<?= $s->displayName ?>)</td>
	<td><?= $process_name?></td>
	<td><?= format_money_symbol($this->business->businessID, '%.2n', $s->price )?></td>
	<td><?= format_money_symbol($this->business->businessID, '%.2n', $s->cost) ?></td>
	<td><?= $s->minQty > 0 ?format_money_symbol($this->business->businessID, '%.2n', $s->minQty) : '';  ?></td>
	<td><?= @local_date($this->business->timezone, $s->updated, 'm/d/y g:i a'); ?></td>
	<td align="center" style="width:25px;"><a onclick="return verify_delete()" href="/admin/suppliers/delete_product/<?= $s->product_supplierID ?>/<?= $supplierInfo[0]->supplierID ?>"><img src="/images/icons/cross.png"></a></td>

</tr>
<? endforeach; ?>
</tbody>

</table>

<script>
$(document).ready(function(){
    $('#product').change(function() {
        var product_id = $(this).val();
        $('#img_holder').html('<img src="/images/progress.gif" />');
        $.ajax({
            url:"/admin/orders/get_product_processes_ajax",
            type:'POST',
            dataType:'JSON',
            data:{submit:true, product_id:product_id},
            success:function(data){
                $('#process_id').html('');
                if(data.length){
                    $.each(data, function(i, item){
                        var name = (item.process_id == 0)?"N/A":item.name;
                        var price = (item.price !='')?"($"+item.price+")":"";
                        var opt = $('<option value="'+item.product_processID+'">'+name+' '+price+'</option>');
                        $('#process_id').append(opt);
                    });
                }
                $('#img_holder').html('');
            }
        });
    });
});
</script>
