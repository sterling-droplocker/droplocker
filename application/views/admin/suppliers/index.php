<?php enter_translation_domain("admin/suppliers/index"); ?>
<?php
$allow_delete = is_superadmin();
?>
<h2>My Suppliers</h2>
<p><a href="/admin/suppliers/create_new_supplier"><img src="/images/icons/add.png"> <?php echo __("Create New Supplier", "Create New Supplier"); ?></a></p>
<?= $this->session->flashdata('message'); ?>
<br>
<table id="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <th><?php echo __("Cost Sheet", "Cost Sheet"); ?></th>
            <th align="center" width='25'><?php echo __("Edit", "Edit"); ?></th>
            <? if ($allow_delete): ?>
            <th align="center" width='25'><?php echo __("Delete", "Delete"); ?></th>
            <? endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($suppliers as $s): ?>    
        <tr>
            <td><?= $s->companyName ?></td>
            <td></td>
            <td align="center"><a href='/admin/suppliers/edit/<?= $s->supplierID ?>'><img src='/images/icons/application_edit.png' /></a></td>
            <? if ($allow_delete): ?>
            <td align="center"><a href='/admin/suppliers/delete/<?= $s->supplierID ?>' onclick="return verify_delete();" id='del-<?= $s->supplierBusiness_id; ?>'><img src='/images/icons/cross.png'></a></td>
            <? endif; ?>
        </tr>
        <? endforeach;?>
    </tbody>
</table>
