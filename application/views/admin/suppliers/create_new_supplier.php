<?php enter_translation_domain("admin/suppliers/create_new_supplier"); ?>
<script type="text/javascript">
$(document).ready(function(){
$(":submit").loading_indicator();

$("form").validate( {
    rules: {
        companyName : {
            required: true
        }
    },
    errorClass: "invalid",
    invalidHandler: function(form, validator)
    {
        $(":submit").loading_indicator("destroy");
    }
});
    
});
</script>
<h2> <?php echo __("Create New Supplier", "Create New Supplier"); ?> </h2>
<p><a href="/admin/suppliers"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Suppliers", "Back to Suppliers"); ?></a></p>

<?= form_open("/admin/suppliers/add") ?>
<table class="detail_table">
    <tr>
        <th width="200"><label><?php echo __("Supplier Name", "Supplier Name"); ?></label></th>
        <td><span class="fill"><?= form_input("companyName") ?></span> </td>
    </tr>
    <tr>
        <th><label><?php echo __("Notes", "Notes"); ?></label></th>
        <td><span class="fill"><?= form_textarea(array("name" => "notes", "rows" => 4, "cols" => 10)) ?></span></td>
    </tr>
</table>
<?= form_submit(array("class" => "button orange", "value" => __("Create", "Create"))) ?>
<?= form_close() ?>
