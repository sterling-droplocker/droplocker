<?php enter_translation_domain("admin/osha_injury/edit"); ?>
<h2><?=__("Edit OSHA Injury", "Edit OSHA Injury"); ?></h2>
<p><a href="/admin/osha_injury"><img src="/images/icons/arrow_left.png"><?=__("Return to OSHA Injury Log", "Return to OSHA Injury Log"); ?> </a></p>

<form action="" method="post">
    <input type="hidden" name="oshaInjuryID" value="<?= $injuryLog[0]->oshaInjuryID ?>">
<table class="detail_table">
	<tr>
		<th><?=__("Case No.", "Case No."); ?></th>
		<td><?= $injuryLog[0]->caseNo ?></td>
	</tr>
	<tr>
		<th><?=__("Employee", "Employee"); ?></th>
		<td><?= form_dropdown('employeeInjured_id', $employees, $injuryLog[0]->employeeInjured_id); ?></td>
	</tr>
	<tr>
		<th><?=__("Job Title", "Job Title"); ?></th>
		<td><input type="text" name="title" value="<?= $injuryLog[0]->title ?>"></td>
	</tr>
	<tr>
		<th><?=__("Injury Date", "Injury Date"); ?></th>
		<td><input type="text" name="injuryDate" size="6" value="<?= $injuryLog[0]->injuryDate ?>"></td>
	</tr>
	<tr>
		<th><?=__("Where the event occurred (e.g. Loading dock north end)", "Where the event occurred (e.g. Loading dock north end)"); ?></th>
		<td><input type="text" name="location" value="<?= $injuryLog[0]->location ?>"></td>
	</tr>
	<tr>
		<th><?=__("Describe injury or illness, parts of body affected, and object/substance that directly injured or made person ill (e.g. Second degree burns on right forearm from acetylene torch)", "Describe injury or illness, parts of body affected, and object/substance that directly injured or made person ill (e.g. Second degree burns on right forearm from acetylene torch)"); ?></th>
		<td><textarea cols="30" rows="4" name="description"><?= $injuryLog[0]->description ?></textarea></td>
	</tr>
	<tr>
		<th><?=__("The most serious outcome for that case", "The most serious outcome for that case"); ?></th>
        <td><?= form_dropdown('classification', $classifications, $injuryLog[0]->classification); ?></td>
	</tr>
	<tr>
		<th><?=__("Days away from work", "Days away from work"); ?></th>
		<td><input type="text" name="daysAway" value="<?= $injuryLog[0]->daysAway ?>"></td>
	</tr>
	<tr>
		<th><?=__("Days on job transfer or restriction", "Days on job transfer or restriction"); ?></th>
		<td><input type="text" name="daysRestricted" size="6" value="<?= $injuryLog[0]->daysRestricted ?>"></td>
	</tr>
	<tr>
		<th><?=__("Type of injury", "Type of injury"); ?></th>
        <td><?= form_dropdown('injury', $injuries, $injuryLog[0]->injury); ?></td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="submit" value="<?=__("Update", "Update"); ?>" class="button blue"></td>
	</tr>
</table>
</form>
