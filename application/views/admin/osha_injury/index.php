<?php enter_translation_domain("admin/osha_injury/index"); ?>
<h2><?=__("OSHA Form 300", "OSHA Form 300"); ?><br>
<?=__("Log of Work-Related Injuries and Illnesses", "Log of Work-Related Injuries and Illnesses"); ?></h2>
<br>
<?=__("attention_msg", "Attention: This form contains information relating to employee health and must be used in a manner that protects the confidentiality of employees to the extent possible while the information is being used for occupational safety and health purposes.<br><br>You must record information about every work-related injury or illness that involves loss of consciousness, restricted work activity or job transfer, days away from work, or medical treatment beyond first aid. You must also record significant work-related injuries and illnesses that are diagnosed by a physician or licensed health care professional. You must also record work-related injuries and illnesses that meet any of the specific recording criteria listed in 29 CFR 1904.8 through 1904.12. If you're not sure whether a case is recordable, call your local OSHA office for help."); ?>
<br>
<br>
<table id="box-table-a">
<thead>
	<tr>
		<th><?=__("Case No.", "Case No."); ?></th>
		<th><?=__("Employee", "Employee"); ?></th>
		<th><?=__("Job Title", "Job Title"); ?></th>
		<th><?=__("Injury Date", "Injury Date"); ?></th>
		<th><?=__("Where the event occurred (e.g. Loading dock north end)", "Where the event occurred (e.g. Loading dock north end)"); ?></th>
		<th><?=__("Describe injury or illness", "Describe injury or illness, parts of body affected, and object/substance that directly injured or made person ill (e.g. Second degree burns on right forearm from acetylene torch)"); ?></th>
		<th><?=__("The most serious outcome for that case", "The most serious outcome for that case"); ?></th>
		<th><?=__("Days away from work", "Days away from work"); ?></th>
		<th><?=__("Days on job transfer or restriction", "Days on job transfer or restriction"); ?></th>
		<th><?=__("Type of injury", "Type of injury"); ?></th>
		<th></th>
	</tr>
</thead>
<tbody>
	<? if($injuryLog): foreach($injuryLog as $i): ?>
	<? if(!isset($caseNo)) {$caseNo = $i->caseNo; } //set the case number to the max case no?>
	<tr>
		<td><?= $i->caseNo ?></td>
                <td><?= getPersonName($i) ?></td>
		<td><?= $i->title ?></td>
		<td><?= $i->injuryDate ?></td>
		<td><?= $i->location ?></td>
		<td><?= $i->description ?></td>
		<td><?= $i->classification ?></td>
		<td><?= $i->daysAway ?></td>
		<td><?= $i->daysRestricted ?></td>
		<td><?= $i->injury ?></td>
		<td><a href="/admin/osha_injury/edit/<?= $i->oshaInjuryID ?>" class="button orange"><?=__("Edit", "Edit"); ?></a></td>
	</tr>

	<? endforeach; else: ?>
	<tr><td colspan="6"><?=__("No OSHA Injuries", "No OSHA Injuries"); ?></td></tr>
	<? endif; ?>
</tbody>
<tfoot>
	<tr>
		<form action="/admin/osha_injury/add" method="post">
		<? $newCase = $caseNo+1 ?>
		<td><input type="hidden" name="caseNo" value="<?= $newCase ?>"><?= $newCase ?></td>
		<td>
            <?= form_dropdown('employeeInjured_id', $employees); ?>
		</td>
		<td><input type="text" name="title"></td>
		<td><input type="text" name="injuryDate" size="6"></td>
		<td><input type="text" name="location"></td>
		<td><textarea cols="30" rows="4" name="description"></textarea></td>
		<td><?= form_dropdown('classification', $classifications); ?></td>
		<td><input type="text" name="daysAway" size="6"></td>
		<td><input type="text" name="daysRestricted" size="6"></td>
		<td><?= form_dropdown('injury', $injuries); ?></td>
		<td align="right"><input type="submit" name="add" value="<?=__("Add", "Add"); ?>" class="button blue"></td>
		</form>
	</tr>
</tfoot>
</table>
