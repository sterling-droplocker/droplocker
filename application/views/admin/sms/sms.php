<?php enter_translation_domain("admin/sms/sms"); ?>
<div>
    <h2><?php echo __("SMS Settings", "SMS Settings"); ?></h2>
    <p><?php echo __("Use this form to setup the SMS provider", "Use this form to setup the SMS provider"); ?></p>
    <p id="provider_endpoint"></p>
</div>

<form method="post">

    <table class="detail_table">
        <tr>
            <th><?php echo __("Provider", "Provider"); ?></th>
            <td>
                <?=form_dropdown('provider',$provider_options, $sms_settings->provider,'id="sms_provider_dropdown"');?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("SMS Number", "SMS Number"); ?></th>
            <td>
                <input type="text" name="number" value="<?=$sms_settings->number;?>" style="width:120px;" />
                <br />
                <span style="font-size:12px;" id="provider_tip"><?php echo __("Your SMS Number should be formatted as follows: +14152557500", "Your SMS Number should be formatted as follows: +14152557500"); ?></span>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Mode", "Mode"); ?></th>
            <td>
                <?=form_dropdown('mode',$environment_mode,$sms_settings->mode );?>
            </td>
        </tr>
        <tr>
            <th id="field1"><?php echo __("Account Id Sid", "Account Id / Sid"); ?></th>
            <td>
                <input type="text" name="account_sid" style="width:240px;" value="<?=$sms_settings->account_sid;?>" />
            </td>
        </tr>
        <tr>
            <th id="field2"><?php echo __("Auth Token", "Auth Token"); ?></th>
            <td>
                <input type="text" name="auth_token" style="width:240px;" value="<?=$sms_settings->auth_token;?>" />
            </td>
        </tr>
        <tr style="display:none;">
            <th><?php echo __("Api Version", "Api Version"); ?></th>
            <td><?php echo __("2010-04-01", "2010-04-01"); ?>
                <input type="hidden" name="api_version" value="2010-04-01" style="width:120px;" />
                <br />
            </td>
        </tr>
        <tr>
            <th id="disable_unicode"><?php echo __("Disable Unicode", "Disable Unicode"); ?></th>
            <td>
				<input type="hidden" name="disable_unicode" value="0">
                <input type="checkbox" name="disable_unicode" value="1"  <?=$sms_settings->disable_unicode ? 'checked' : '';?> />
            </td>
        </tr>
        <tr>
        <th><?php echo __("Forbidden Words in SMS", "Forbidden Words in SMS"); ?></th>
            <td>
                <input type="text" name="forbidden_words" style="width:640px;" value="<?=$forbidden_words;?>" style="width:120px;" />
                <br />
                <span style="font-size:12px;" id="forbidden_words_tip"><?php echo __("The forbidden words must be formatted as follows: word1,word2,word3,word4 and so on", "The forbidden words must be formatted as follows: word1,word2,word3,word4 and so on"); ?></span>
            </td>
        </tr>
    </table>

    <div style="clear:left;">
        <div>
            <button type="submit" class="button orange"> <?php echo __("Update", "Update"); ?> </button>
        </div>
    </div>

    <div style="width:auto;margin-top:10px;" id="twilio_url_help_image">
        <?php echo __("Be sure to set the type to GET", "Be sure to set the type to \"GET\""); ?><br />
        <img id="provider_helper_image" src="/images/twilio_set_up.png" style="border:solid 1px #ccc;" />
    </div>

</form>
<script type="text/javascript">
var providerOptions = {
    twilio:{
        field1:'Account Id / Sid',
        field2:'Auth Token',
        provider_tip:'<?php echo __("Your SMS Number should be formatted as follows: +14152557500", "Your SMS Number should be formatted as follows: +14152557500"); ?>',
        provider_endpoint:'<?php echo __("Twilio: Please be sure to set your SMS Request URL to", "IMPORTANT: Please be sure to set your SMS Request URL to: <strong>%url%</strong> in your Twilio Account Settings", array("url"=>site_url('account/sms/twilio_claim'))); ?>',
        provider_helper_image:"/images/twilio_set_up.png"
    },
    nexmo:{
        field1:'Api Key',
        field2:'Api Secret',
        provider_tip:'<?php echo __("Your SMS Number should be formatted as follows: 14152557500", "Your SMS Number should be formatted as follows: 14152557500"); ?>',
        provider_endpoint:'<?php echo __("Nexmo: Please be sure to set your SMS Request URL to", "IMPORTANT: Please be sure to set your SMS Request URL to: <strong>%url%</strong> in your Nexmo Account Settings", array("url"=>site_url('account/sms/nexmo_claim'))); ?>',
        provider_helper_image:"/images/nexmo_set_up.png"
    }
};

$(function(){
    swapProvider();
    $('select#sms_provider_dropdown').bind('change',swapProvider);

});

var swapProvider = function(e){
    var providerChoice = $('#sms_provider_dropdown').val();
    $('#field1').html( providerOptions[providerChoice]['field1'] );
    $('#field2').html( providerOptions[providerChoice]['field2'] );
    $('#provider_tip').html( providerOptions[providerChoice]['provider_tip'] );
    $('#provider_endpoint').html( providerOptions[providerChoice]['provider_endpoint'] );
    $('img#provider_helper_image').attr('src', providerOptions[providerChoice]['provider_helper_image'] );
};

</script>