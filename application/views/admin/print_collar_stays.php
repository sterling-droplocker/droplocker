<?php enter_translation_domain("admin/print_collar_stays"); ?>
<html>
<head>
<title><?php echo __("Collar Stays", "Collar Stays"); ?></title>
</head>
<body>
<?

$full_base = get_full_base_url();


//if a barcode is passed, find out what open order that barcode is attached to
if($request['barcode'])
{
	if(strlen($request['barcode']) == 8)
	{
		$query1="SELECT max(order_id) as currOrder
			FROM barcode
			JOIN orderItem on orderItem.item_id = barcode.item_id
			where barcode = $request[barcode]";
		$query = $this->db->query($query1);
		$line = $query->row();
		if (!$line->currOrder)
		{ 
			echo __("barcode not found", "barcode not found");
			exit; 
		}
		
		$query2="SELECT firstName, lastName, orders.postAssembly, route_id
			FROM customer
 			join orders on orders.customer_id = customerID
      		join locker on  lockerID = orders.locker_id
      		join location on locationID = locker.location_id
			where orderID = $line->currOrder";
		$query = $this->db->query($query2);
		$line2 = $query->row();
		
		//add an assembly note to the order so that the items are reassociated during assembly
		//$update1 = "update `order` set postAssembly = concat('COLLAR STAYS<br>', '$line2[postAssembly]') where id = $line[currOrder];";
		//$update = $db->query($update1);		
		echo '<center>';
		echo getPersonName($line2) . ' - ' .$line->currOrder . ' ('.$line2->route_id.')<br>';
		echo '<img src="'.$full_base.'/barcode.php?barcode='.$request['barcode'].'">';
		echo '</center>';
	}
	else
	{
		echo __("barcode is wrong length", "barcode is wrong length");
		exit; 
	}
	
}
else
{
	echo __("no barcode passed", "no barcode passed");
}
?>
</body>
</html>
