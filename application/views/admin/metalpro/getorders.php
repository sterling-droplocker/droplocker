<?php enter_translation_domain("admin/metalpro/getorders"); ?>
<?php
use App\Libraries\DroplockerObjects\Business;

//if a bag is passed, get the open order for that bag
$startTime = date("h:i:s");
if($request['bagNumber'])
{
	//removed and product_id not in (34, 179, 180) replaced with and notes NOT LIKE '%box%'
$query1 = "SELECT distinct order_id, orders.customer_id, bagNumber
			FROM `orderItem`
			inner join orders on orderItem.order_id = orderID
       		inner join bag on bagID = orders.bag_id
       		where orderStatusOption_id <> 10
			and item_id > 0
			and bag.bagNumber = '$request[bagNumber]'
			order by orderItem.order_id;";
}
else
{	
    $business_id = $request['business_id'];
    if(empty($business_id)){
        throw new Exception('Missing business ID');
    }
//get all orders in status inventoried or on payment hold
$query1 = "SELECT distinct order_id, orders.customer_id, bagNumber
			FROM `orderItem`
			inner join orders on orderItem.order_id = orderID
			left join bag on bagID = orders.bag_id
			where orderStatusOption_id in (3,7)
			and orders.business_id = {$business_id}
			and item_id > 0
			order by orderItem.order_id;";
}
$currOrd = '';
$business = new Business($request['business_id']);
$prismFile = "/home/".$business->slug."/integration/AUTOMAT.IN";
$prismFileHandle = fopen($prismFile, 'w') or die("can't open file. location: ".$prismFile);

$query = $this->db->query($query1);
//echo 'First Query took: ' . $startTime . ' to ' . date("h:i:s");

foreach ($query->result() as $line)
{
	//if any record is found in automat for this order, don't reload it on the conveyor		
	$skip = 0;
	if (!$request['bagNumber'])
	{
		$query2 = "select automatID from automat
					where order_id = $line->order_id
					and slot <> ''";
		$query2 = $this->db->query($query2);
		if ($line2 = $query2->row())
		{
		  $skip = $line->order_id;
		  echo '<br>'. date("h:m s") . __("Skipped Order", " Skipped Order: ") . $line->order_id;
		} 
		else
		{
		  echo '<br>'. date("h:m s") . __("Loaded Order", " Loaded Order: ") . $line->order_id;	
		}
	}
	//don't load orders registered to delivery man.  Nothing should be registered to delivery man
	if ($line->customer_id == 18)
	{
		$skip = 1;
	}
	
	if ($skip == 0)
	{
		//Updated by Matt
		// old product_id's replaced with product_process_id's
		// 34 => 261
		// 179 = empty in product_process table and missing from prod
		// 180 = same as 179, but foud
		// removed "and product_id not in (261,180)"
		$query2 = "select name, displayName, item_id, orderItem.notes
					from orderItem
					join item on itemID = orderItem.item_id
					join product_process on product_processID = item.product_process_id
					join product ON productID = product_process.product_id
					where order_id = $line->order_id
					and (product.name not like '%box%' and orderItem.notes not like '%box%');";
		//echo $query2;
		$query2 = $this->db->query($query2);
		
		//clothespin cleaners doesn't send bags
		if ($line->customer_id == 18789) 
		{ 
			$ordItemCount = 1;
			$ngarm = $query2->num_rows(); 
		}
		else 
		{ 
			if(get_business_meta($this->business_id, 'loadBags') == 1)
            {
                $ngarm = $query2->num_rows() + 1; 		
    			//add the bag as an item
    			$ordItemCount = 1;
    			$data .= str_pad($line->order_id,20).str_pad($ngarm,2,"0", STR_PAD_LEFT).str_pad($ordItemCount, 2, "0", STR_PAD_LEFT).str_pad($line->bagNumber,20).str_pad("bag",30).'100SR            '; 
    			$data .= chr(13);
    			$data .= chr(10);
    			$ordItemCount ++;
            }
            else
            {
                $ordItemCount = 1;
    			$ngarm = $query2->num_rows(); 
            }
		}
		
		foreach($query2->result() as $line2)	
		{ 
			$query3 = "SELECT barcode from barcode
						where item_id = $line2->item_id";
			$query3 = $this->db->query($query3);
			$line3 = $query3->row();
			
			$product = $line2->product;
			$barcode = $line3->barcode;

			$data .= str_pad($line->order_id,20).str_pad($ngarm,2,"0", STR_PAD_LEFT).str_pad($ordItemCount, 2, "0", STR_PAD_LEFT).str_pad($barcode,20).str_pad($product,30).'100SR            '; 
			$data .= chr(13);
			$data .= chr(10);
			$currOrd = $line->order_id;
			//echo '<br>'. date("h:m s") .' Processed Order: ' . $line['order_id'] . ' Item: ('.$ordItemCount.') '.$line2['item_id'];
			$ordItemCount ++;
		}
	}
}

fwrite($prismFileHandle, $data);
fclose($prismFileHandle);
?>
