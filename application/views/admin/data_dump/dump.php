<?php enter_translation_domain("admin/data_dump/dump"); ?>

<h2><?php echo __("Zoho Data Dump", "Zoho Data Dump"); ?></h2>

<br><br>

<?php if (isset($url)): ?>

<h3 style="color: green"><?php echo __("URL For Zoho Reports Generated", "URL For Zoho Reports Generated:"); ?></h3>
<br/>
<textarea id="dump_url" cols="100" readonly><?=$url;?></textarea>
<script type="text/javascript">
$("#dump_url").focus(function() {
    var $this = $(this);
    $this.select();
    $this.mouseup(function() {
        $this.unbind("mouseup");
        return false;
    });
});
</script>
<p>
    <?php echo __("The data returned is in JSON format", "The data returned is in JSON format. Please refer to <a href=\"https://reports.wiki.zoho.com/Import-Data-from-Files-and-Feeds.html#schedule-import\" target=\"_blank\">this guide</a> for instructions on how to use this url."); ?>
</p>

<?php endif; ?>

