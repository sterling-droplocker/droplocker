<?php enter_translation_domain("admin/data_dump/logs"); ?>
<?= $this->session->flashdata('message');?>

<h2><?php echo __("Data Dumps Logs", "Data Dumps Logs"); ?> (<?= number_format_locale($total,0)?>)</h2>

<table id="box-table-a">
    <thead>
        <th><?php echo __("Date", "Date"); ?></th>
        <th><?php echo __("Employee", "Employee"); ?></th>
        <th><?php echo __("Table", "Table"); ?></th>
        <th><?php echo __("Format", "Format"); ?></th>
        <th><?php echo __("Date From", "Date From"); ?></th>
        <th><?php echo __("Date To", "Date To"); ?></th>
    </thead>
    <tbody>
        <?php foreach($logs as $l): ?>
        <?php
            $dateCreated = to_timezone($l->dateCreated, $timezone, get_business_meta($this->business_id, 'fullDateDisplayFormat'));
            $dateFrom = $l->dateFrom != '0000-00-00 00:00:00' ? to_timezone($l->dateFrom, $timezone, get_business_meta($this->business_id, 'standardDateDisplayFormat')) : '-';
            $dateTo = $l->dateTo != '0000-00-00 00:00:00' ? to_timezone($l->dateTo, $timezone, get_business_meta($this->business_id, 'standardDateDisplayFormat')) : '-';
        ?>
        <tr>
            <td><?= $dateCreated; ?></td>
            <td><?= getPersonName($l) ?></td>
            <td><?= $l->dumpedTable; ?></td>
            <td><?= $l->format; ?></td>
            <td><?= $dateFrom; ?></td>
            <td><?= $dateTo; ?></td>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8"><?= $paging; ?></td>
        </tr>
    </tfoot>
</table>