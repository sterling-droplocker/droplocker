<?php enter_translation_domain("admin/data_dump/index"); ?>

<h2><?php echo __("Data Dump", "Data Dump"); ?></h2>

<br><br>

<form method="get" action="/admin/data_dump/dump" id="form_dump">
<table class="detail_table">
    <tr>
        <th><?php echo __("Data", "Data"); ?></th>
        <td>
            <select name="table" id="types">
            	<option value="claims"><?php echo __("Claims", "Claims"); ?></option>
                <option value="customers"><?php echo __("Customers", "Customers"); ?></option>
                <option value="orders"><?php echo __("Orders", "Orders"); ?></option>
                <option value="order_items"><?php echo __("Order Items", "Order Items"); ?></option>
                <option value="order_charges"><?php echo __("Order Charges", "Order Charges"); ?></option>
                <option value="driver_notes"><?php echo __("Driver Notes", "Driver Notes"); ?></option>
                <option value="locations"><?php echo __("Locations", "Locations"); ?></option>
                <option value="timeCards"><?php echo __("TimeCards", "Time Cards"); ?></option>
                <option value="customerDiscounts"><?php echo __("customerDiscounts", "Customer Discounts"); ?></option>
            </select>
        </td>
    </tr>

    <tr>
        <th><?php echo __("Format", "Format"); ?></th>
        <td>
            <select name="format" id="format">
                <option value="csv"><?php echo __("CSV", "CSV"); ?></option>
                <option value="json"><?php echo __("JSON", "JSON"); ?></option>
                <option value="xml"><?php echo __("XML", "XML"); ?></option>
                <option value="zoho"><?php echo __("Zoho Dump", "Zoho Dump"); ?></option>
            </select>
            <input type="hidden" name="output_format" id="format_input" value="csv" />
        </td>
    </tr>

    <tr id="date_range_wrapper" class="toggle">
        <th><?php echo __("Date Range", "Date Range"); ?></th>
        <td>
            <label><?php echo __("From", "From:"); ?> <input type="text" name="from" class="datefield"></label>&nbsp;&nbsp;
            <label><?php echo __("To", "To:"); ?> <input type="text" name="to" class="datefield"></label>
        </td>
    </tr>

    <tr id="records_wrapper" style="display:none;">
        <th><?php echo __("Number of Records", "Number of Records"); ?></th>
        <td>
            <select name="records" id="records">
                <option value="1000"><?php echo __("1000", "1000"); ?></option>
                <option value="2000"><?php echo __("2000", "2000"); ?></option>
                <option value="5000"><?php echo __("5000", "5000"); ?></option>
                <option value="10000"><?php echo __("10000", "10000"); ?></option>
                <option value="20000"><?php echo __("20000", "20000"); ?></option>
                <option value="100000" selected="selected"><?php echo __("100000", "100000"); ?></option>
            </select>
        </td>
    </tr>

    <tr id="location_date_range_wrapper" style="display:none;" class="toggle">
        <th><?php echo __("Date Range", "Date Range"); ?></th>
        <td>
            <label><?php echo __("From", "From:"); ?></label>
            <?= form_dropdown('start_month', $months, $start_month); ?>
            <?= form_dropdown('start_year', $years, $start_year); ?>

            <label>To:</label>
            <?= form_dropdown('end_month', $months, $end_month); ?>
            <?= form_dropdown('end_year', $years, $end_year); ?>

            <select name="grouping">
                <option value="week"><?php echo __("Weeks", "Weeks"); ?></option>
                <option value="4week"><?php echo __("4 Week Grouping", "4 Week Grouping"); ?></option>
                <option value="month"><?php echo __("Months", "Months"); ?></option>
                <option value="quarter"><?php echo __("Quarters", "Quarters"); ?></option>
            </select>
        </td>
    </tr>
</table>

<input type="submit" class="button orange" name="export" value="<?php echo __("Export", "Export"); ?>">
</form>
    
<script type="text/javascript">

$(".datefield").datepicker({
	showOn: "button",
	buttonImage: "/images/icons/calendar.png",
	buttonImageOnly: true,
	dateFormat: "yy-mm-dd"
});

$('#form_dump').submit(function(evt) {
    evt.preventDefault();
    var url = $(this).attr('action');
    url += '?' + $(this).serialize();
    window.open(url);
});

function toggle_records_wrapper(format) {
    $('.toggle').hide();
    $('#format_input').val($('#format').val());

    if ($('#types').val() == 'locations') {
        $('#location_date_range_wrapper').show();
        //$('#format').val("zoho");
        return;
    }


    $('#date_range_wrapper').show();       
}

$('#types').on('change', function(){
    $('#format').attr("disabled", false);
    $('#format').val("");
    if ($(this).val()=="locations") {
        $('#format').val("zoho");
        //$('#format').attr("disabled", true);
        
    }
    toggle_records_wrapper($('#format'));
});

$('#format').on('change', toggle_records_wrapper);

</script>
