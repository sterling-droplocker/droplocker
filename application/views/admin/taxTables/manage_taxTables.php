<?php enter_translation_domain("admin/taxTables/manage_taxTables"); ?>
<?php

/**
 * Expects the following content variables:
 *  $taxTables array
 */

$taxGroupList = array();
foreach ($taxGroups as $taxGroup) {
    $taxGroupList[$taxGroup->taxGroupID] = $taxGroup->name;
}

?>

<h2><?php echo __("Manage Tax Rates", "Manage Tax Rates"); ?></h2>
<br>

<h3><?php echo __("Default Tax Table", "Default Tax Table"); ?></h3>

<? if ($taxGroupList): ?>
<form action='/admin/taxTables/save_default' method='post'>
<table class='detail_table'>
    <tr>
        <th><?php echo __("Tax Group", "Tax Group"); ?></th>
        <td>
            <?= form_dropdown('default_taxGroup', $taxGroupList, get_business_meta($this->business_id, 'default_taxGroup')); ?>
            <?php echo __("This will be used for orders without items, like Laundry Plans and Gift Cards", "This will be used for orders without items, like Laundry Plans and Gift Cards"); ?>
        </td>
    </tr>
</table>
<input class='button orange' type='submit' name='submit' value="Update" />
</form>
<br><br>
<? endif; ?>

<h3><?php echo __("Add New Tax Table", "Add New Tax Table"); ?></h3>
<form action='/admin/taxTables/add_group' method='post'>
<table class='detail_table'>
    <tr>
        <th><?php echo __("Add Group Name", "Add Group Name"); ?></th>
        <td><input type='text' name='name' value='' /></td>
    </tr>
    <tr>
        <th><?php echo __("Default Tax Rate", "Default Tax Rate"); ?></th>
        <td>
            <input type='text' name='taxRate' value='' style="width:250px;" placeholder="In decimal, eg: 0.15 => 15%" id="create_taxRate"/>
            <?php echo __("(If non empty, will add all the existing zipcodes with that value)", "(If non empty, will add all the existing zipcodes with that value)"); ?>
        </td>
    </tr>
</table>
<input class='button orange' type='submit' name='submit' value="<?php echo __("Add Group", "Add Group"); ?>" id="create_tax"/>

</form>
<br>

<? if (empty($taxGroups)): ?>
    <p> <?php echo __("--- No Defined Tax Rates ---", "--- No Defined Tax Rates ---"); ?> </p>
<? else: ?>

<div id="group_tabs">
    <ul>
    <? foreach($taxGroups as $taxGroup): ?>
        <li>
            <a href="#group_<?= $taxGroup->taxGroupID ?>">
                <?= $taxGroup->name ? $taxGroup->name : '<?php echo __("-NO-NAME-", "-NO-NAME-"); ?>' ?>
            </a>
        </li>
    <? endforeach; ?>
    </ul>

    <? foreach($taxGroups as $taxGroup): ?>
    <div class="tab" id="group_<?= $taxGroup->taxGroupID ?>">
        <div style="margin-bottom: 20px;">
            <h2 class="value" style="display:inline;"><?= $taxGroup->name ? $taxGroup->name : '<?php echo __("-NO-NAME-", "-NO-NAME-"); ?>' ?>
                <? if ($taxGroup->parent_id): ?>
                    (<?php echo __("Parent:", "Parent:"); ?> <?= $taxGroupList[$taxGroup->parent_id] ?>, <?php echo __("SubTax:", "SubTax:"); ?> <?= $taxGroup->is_sub_tax ? 'Yes' : 'No' ?>)
                <? endif; ?>
            </h2>
            <img src="/images/icons/application_edit.png" class="toggle-form">
            <form method="post" action="/admin/taxTables/update_group/<?= $taxGroup->taxGroupID ?>" class="inline-hidden">
                <label><?php echo __("Name:", "Name:"); ?> <input type="text" name="name" value="<?= htmlentities($taxGroup->name) ?>"></label>
                <span class="parent-field">| <label><?php echo __("Parent:", "Parent:"); ?> <?= form_dropdown('parent_id', array('' => '-None-') + $taxGroupsParents[$taxGroup->taxGroupID], $taxGroup->parent_id) ?></label></span>
                <span class="subtax-field" style="display:<?= $taxGroup->parent_id ? 'inline' : 'none' ?>">| <label><?php echo __("Is Sub Tax", "Is Sub Tax"); ?> <?= form_checkbox('is_sub_tax', 1, $taxGroup->is_sub_tax, 'title="<?php echo __("Subtaxes are calculated after the parent tax", "Subtaxes are calculated after the parent tax"); ?>"') ?></label></span>
                | <button type="submit" class="submit-edit"><?php echo __("Update", "Update"); ?></button>
                <button type="button" class="cancel-edit"><?php echo __("Cancel", "Cancel"); ?></button>
            </form>
            <form method="post" action="/admin/taxTables/delete_group/<?= $taxGroup->taxGroupID ?>" class="delete-form" style="display:inline;">
                <input type="image" name="delete" src="/images/icons/cross.png" onclick="return verify_delete();">
            </form>
        </div>

        <table class="box-table-a">
            <thead>
                <tr>
                    <th><?php echo __("Zipcode", "Zipcode"); ?></th>
                    <th><?php echo __("Tax Rate (in decimal, eg: 0.15 => 15%)", "Tax Rate (in decimal, eg: 0.15 => 15%)"); ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <form action='/admin/taxTables/create' method='post'>
                    <tr>
                        <td><input type='text' name='zipcode' placeholder="<?php echo __("Zipcode", "Zipcode"); ?>" value="<?= $zipcode ?>"></td>
                        <td><input type='text' name='taxRate' placeholder="<?php echo __("Tax Rate (in decimal, eg: 0.075 => 7.5%)", "Tax Rate (in decimal, eg: 0.075 => 7.5%)"); ?>" size="50"></td>
                        <td>
                            <input type="hidden" name="taxGroup_id" value="<?= $taxGroup->taxGroupID ?>">
                            <input type='submit' value="Add" />
                        </td>
                    </tr>
                </form>

                <? foreach ($taxTables[$taxGroup->taxGroupID] as $taxTable): ?>
                    <tr>
                        <td class="editable" data-property="zipcode" data-id="<?=$taxTable->id?>"> <?= $taxTable->zipcode ?> </td>
                        <td class="editable" data-property="taxRate" data-id="<?=$taxTable->id?>"> <?= number_format_locale($taxTable->taxRate,5) ?> </td>
                        <td>
                            <form method="post" action="/admin/taxTables/delete/<?= $taxTable->id ?>" style="display:inline;">
                                <input type="image" name="delete" src="/images/icons/cross.png" onclick="return verify_delete();">
                            </form>
                        </td>
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>

    </div>
    <? endforeach ?>
</div>
<? endif; ?>

<script type="text/javascript">

$(document).ready(function() {
    $(".editable").editable("/admin/taxTables/update", {
            cancel : "<img src='/images/icons/delete.png' alt='Delete' />",
            submit : "<img src='/images/icons/accept.png' alt='Accept' />",
            indicator : "Saving ... <img src='/images/progress.gif' alt='Working...'/>",
            data : function(string) {return $.trim(string)},
            submitdata : function ( value, settings ) {
                return {
                    "property" : $(this).attr("data-property"),
                    "id": $(this).attr("data-id")
                }
        }
    });

	$('.inline-hidden').hide();
	$('.toggle-form').click(function(evt) {
		$parent = $(this).hide().parent();
		$('.value', $parent).hide();
        $('.delete-form', $parent).hide();
		$('.inline-hidden', $parent).show();
	});

	$('.cancel-edit').click(function(evt) {
		$parent = $(this).parent().parent();
		$('.value', $parent).show();
        $('.toggle-form', $parent).show();
        $('.delete-form', $parent).show();
		$('.inline-hidden', $parent).hide();
	});

	$('.parent-field select').change(function(evt) {
		$parent = $(this).parent().parent();
		if ($(this).val()) {
			$parent.next().show();
		} else {
			$parent.next().hide();
		}
	});

    $("#group_tabs").tabs();


    $('#create_tax').click(function(evt) {
        var val = $('#create_taxRate').val();
        if (val >= 1) {
            if (!confirm('<?php echo __("This tax is greater than 100%, are you sure?", "This tax is greater than 100%, are you sure?"); ?>')) {
                evt.preventDefault();
            }
        }
    });

});

</script>
