<?php
enter_translation_domain("admin/orders/edit_item");
/**
 * It is not known all the content variables that are expected to be set in this file.
 * Expects the following content variables:
 *
 * $categories_products: an array containing the all categories and products associated with the business in the following format:
 *  {
 *      productCategory1 : {productID1 : {name, upchargeGroup_id }, {productID2 : {name, upchargeGroup_id}}, ... },
 *      productCategory2 : {productID3 : {name, upchargGroup_id}}, {productID4 : {name, upchargeGroup_id}, ...},
 *      ...
 *  }
 *
 * $products_product_processes: An array containing each product and its available processes in the folloiwng format:
 * {
 *      product1: {processID1, processID2, ...},
 *      product2: {processID1, processID2, ...}
 *      ...
 * }
 *
 * Expects the following content variables under certain conditions:
 *  $order_id int Must be passed when modifying an item for an existing order
 *  $customer stdClass This must  be passed when editing an item for an existing customer or adding an item to an order. The customer should be the owner of the item, or, if a new item, the customer associated with the order
 *  $customer_id int This must  be passed when editing an item for an existing customer or adding an item to an order. The customer should be the owner of the item, or, if a new item, the customer associated with the order
 *
 * Can take the following optional content variables:
 *  $message string a custom message to show display to the user
 *
 * When editing an existing item, the following content variables are expected
 *  $item
 *  $product_id int  The product the item is associated with
 *  $process_id The process the item is associated with
 *
 * Can take the following content variables:
 *  $groups array structure of Upcharge groups
 */

//The following code is a list of validation to ensure that known expected content variables are set when rendering this variable.
if (!isset($categories_products)) {
    trigger_error(__("var_categories_products_not_set", "'categories_products' must be set as a content variable for this view"), E_USER_WARNING);
}

if (!isset($products_product_processes)) {
    trigger_error(__("var_products_product_processes_not_set", "'products_product_processes' must be set as a content variable for this view."), E_USER_WARNING);
}

//If we are editing an item for an order, then the order ID and customer ID must be set as content variables.
if (!empty($order_id)) {
    if (!is_numeric($order_id)) {
        trigger_error("'order_id' must be numeric", E_USER_WARNING);
    }
    if (!is_numeric($customer_id)) {
        trigger_error("When managing an existing order, 'customer_id' set as a content varible for this view and must be numeric", E_USER_WARNING);
    }
    if (!isset($customer)) {
        trigger_error("When managing an existing order, 'customer_id' set as a content varible for this view and must be numeric", E_USER_WARNING);
    }
} else {
    $customer_id = null;
    $order_id = null;
}

//If we are editing an existing item, the item specifications, product ID, and process ID must also be defined as content variables.
if (isset($item)) {
    if (!isset($item_specifications)) {
        trigger_error("'item_specifications' must be set as a content variable when editing an existing item", E_USER_WARNING);
    }
    if (!is_numeric($product_id)) {
        trigger_error("'product_id' must be set as a content variable when editing an existing item", E_USER_WARNING);
    }
    if (!is_numeric($process_id)) {
        trigger_error("'process_id' must be set as a content variable when editing an existing item", E_USER_WARNING);
    }
} else {
    $item_specifications = array();
    $item_upcharges = array();
    $currentProductCategory_id = null;
    $product_id = null;
    $process_id = null;
}

// add show_all category
$productCategories = array('show_all' => __("show_all", "-- SHOW ALL --")) + (array)$productCategories;

if (!isset($currentProductCategory_id)) {
    $currentProductCategory_id = null;
}

?>

<div>
<form id="item_form" method="post" action="<?= empty($item->itemID) ? "/admin/orders/create_item_ajax/" : "/admin/orders/edit_item_ajax"; ?>">

    <h3><?=__("product_details", "Product Details")?></h3>

    <? if (isset($message)): ?>
        <div style="color: red; margin: 10px;"> <?= $message ?></div>
    <? endif ?>
    <table class="detail_table" id="item_options">
        <tr id="row-barcode">
            <th><?=__("barcodes", "Barcodes")?></th>
            <td>
                <div class="pull-right" style="width: 75%">
                    <input type="text" name="newBarcode" id="newBarcode" class="pull-left" style="width: 35%" />
                    <button id="addBarcode" class="button blue pull-right"><?=__("add_barcode", "Add Barcode")?></button>
                </div>
                <? if (!empty($barcode)): ?>
                    <? $currentBarcode = $barcode; ?>
                    <strong><?= $currentBarcode; ?></strong>
                <? else: ?>
                    <? foreach ($barcodes as $index => $barcode): ?>
                        <? if ($index === 0): ?>
                            <? $currentBarcode = $barcode->barcode; ?>
                            <strong><?= $barcode->barcode; ?></strong>
                        <? else: ?>
                            <br /><?= $barcode->barcode; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                <? endif; ?>
            </td>
        </tr>
        <tr id="row-product">
            <th><?=__("product", "Product")?></th>
            <td>
            <?= form_dropdown('productCategory', $productCategories, $currentProductCategory_id, 'id="productCategory" tabindex="10000"') ?>
                <select id='product_id' name='product_id' tabindex="9999"></select>
            </td>
        </tr>
        <tr id="row-processes">
            <th style="vertical-align: top"><?=__("cleaning_method", "Cleaning Method")?></th>
            <td id="processes"></td>
        </tr>
        <tr id="row-specifications">
            <th style='vertical-align:top'><?=__("special", "Special")?></th>
            <td>
                <? foreach($specifications as $specification):
                    $checked = isset($item_specifications[$specification->specificationID]);
                ?>
                    <span id='specification-<?= $specification->specificationID ?>' class='radio_selector <?= $checked ? 'selected' : '' ?>'>
                        <?=__($specification->description, $specification->description)?>
                        <input type="checkbox" name="specifications[]" value='<?= $specification->specificationID ?>' <?= $checked ? 'checked="checked"' : '' ?> style="display:none;">
                    </span>
                <? endforeach; ?>
            </td>
        </tr>
        <? if (get_business_meta($this->business_id, 'displayColorOptions')): ?>
        <tr id="row-color">
            <th><?=__("color", "Color")?></th>
            <td>
                <? foreach ($colors as $color => $label):
                    $font = $color == 'white' ? 'black; text-shadow: none' : 'white';
                    $checked = isset($item->color) ? $item->color == $color : false;
                ?>
                    <span class="radio_selector color_selector <?= $checked ? 'selected' : '' ?>" style="background-color: <?= $color ?>; color: <?= $font ?>">
                        <?=__($label, $label)?>
                        <input type="radio" name="color" value="<?= $color ?>" <?= $checked ? 'checked="checked"' : '' ?> style="display:none;">
                    </span>
                <? endforeach; ?>
            </td>
        </tr>
        <? endif; ?>
        <? if (get_business_meta($this->business_id, 'displayBrand')): ?>
        <tr id="row-brand">
            <th><?=__("Brand", "Brand")?></th>
            <td>
                <? if (!empty($brands)): ?>
                <?= form_dropdown('manufacturer', array('' => __('--SELECT--', '--SELECT--')) + $brands, isset($item->manufacturer) ? $item->manufacturer : ''); ?>
                <? else: ?>
                <input id='brand' type="text" name='manufacturer' value='<?= isset($item->manufacturer) ? $item->manufacturer : ''; ?>' />
                <? endif; ?>
            </td>
        </tr>
        <? endif; ?>
        <? if (!empty($customOptions)): ?>
        <? foreach($customOptions as $name => $customOption): ?>
        <tr id="custom-option-<?= $name ?>">
            <th><?= $customOption->title ? $customOption->title : $customOption->name ?></th>
            <td>
                <?
                    $default = null;
                    $optionValues = array();
                    foreach ($customOption->values as $value) {
                        $optionValues[$value->value] = $value->name;
                        if ($value->default) {
                            $default = $value->value;
                        }
                    }
                    if (isset($item_options[$name])) {
                        $default = $item_options[$name];
                    }
                ?>
                <?= form_dropdown("customOption[$name]", $optionValues, $default); ?>
            </td>
        </tr>
        <? endforeach ?>
        <? endif; ?>
        <tr id="row-upcharges">
            <th style='vertical-align:top'><?=__("up_charges", "Up-charges")?></th>
            <td id='upchargeHolder'></td>
        </tr>
        <tr id="row-notes">
            <th style='vertical-align: top'><?=__("notes", "Notes")?></th>
            <td>
                <textarea name="note" id="note" style='width:90%; height:50px'><?= isset($item->note) ? $item->note : '' ?></textarea>
            </td>
        </tr>
    </table>

    <p><?=__("customer_default_preference", "* Customer default preference.")?></p>

    <input type="hidden" name="barcode" value="<?= $currentBarcode ?>">
    <input type="hidden" name="customer_id" value="<?= $customer_id ?>">
    <input type="hidden" name="order_id" value="<?= $order_id ?>">

    <? if (empty($item->itemID)): ?>
        <input type="submit" id="create_item_button" name="order_submit" value="<?=__("btn_create_item", "Create Item");?>" class="button orange">
        <input type="hidden" name="add_to_order" value="1">
        <input type="hidden" name="redirect" value="<?= "/admin/orders/details/{$order_id}" ?>">
    <? elseif($order_id && !$closed): ?>
        <input type="submit" onClick="return verify_delete();" id="edit_item_order_button" name="edit_order_submit" value="<?=__("btn_update_item_and_update_order", "Update Item AND Update Order");?>" class="button red">
        <input type="hidden" name="add_to_order" value="1">
        <input type="hidden" name="redirect" value="<?= "/admin/orders/details/{$order_id}" ?>">
        <input type="hidden" name="itemID" value="<?= $item->itemID ?>">
    <? else: ?>
        <input type="submit" id="edit_item_button" name="edit_submit" value="<?=__("btn_update_item", "Update Item");?>" class="button orange">
        <input type="hidden" name="add_to_order" value="0">
        <input type="hidden" name="redirect" value="<?= "/admin/orders/edit_item?orderID={$order_id}&itemID={$item->itemID}"; ?>">
        <input type="hidden" name="itemID" value="<?= $item->itemID ?>">
    <? endif; ?>

    <span id='progress'></span>

</form>
</div>

<? if($order_id):?>
    <br>
    <div align="center"><a href='/admin/orders/details/<?= $order_id?>'><?=__("return_to_order_without_adding_item", "Return to order without adding item")?></a></div>
<? endif;?>



<script type="text/javascript">
$.blockUI({message : "<h1> <?=__("loading", "Loading...")?> </h1"});

// all the categories
var categories_products = <?= json_encode($categories_products); ?>;

// all the products for each category
var products_product_processes = <?= json_encode($products_product_processes); ?>;

// all the upcharges for each upcharge group
var upchargeGroups = <?= json_encode($groups); ?>;

// all the business preferences
var businessPreferences = <?= json_encode($businessPreferences); ?>;

// the customer preferences
var customerPreferences = <?= json_encode($customerPreferences); ?>;


// the item
var item = {};
item.process_id = <?= json_encode($process_id) ?>;
item.product_id = <?= json_encode($product_id) ?>;
item.upcharges = <?= json_encode($item_upcharges); ?>;
item.preferences = <?= json_encode($item_preferences); ?>;

// TODO: NOT USED ??
item.crease = <?= json_encode(isset($item->crease) ? $item->crease : ''); ?>;

$(document).ready(function() {

    // load products when the category change
    $("#productCategory").change(function() {
        var productCategory_id = $(this).val();

        if (!productCategory_id) {
            productCategory_id = "show_all";
        }

        var products = categories_products[productCategory_id];
        $("#product_id").empty();

        if (products === undefined) {
            $("<div class='ui-state-error' id='product_error_container'> <?=__("there_are_no_products_in_this_category", "There are no products in this category")?> </div>").insertAfter("#product_id");
            return;
        }

        $("#product_error_container").remove();
        $('<option value=""><?=__("select_product", "--Select Product--")?></option>').appendTo("#product_id");

        $.each(products, function(index, properties) {
            $option = $("<option>");

            $option.attr('data-upchargeGroup_id', properties.upchargeGroup_id);
            $option.attr('data-productCategory_id', properties.productCategory_id);
            $option.attr('value', properties.productID);
            $option.text(properties.name);

            if(item.product_id == properties.productID){
                $option.attr('selected', 'selected');
            }

            $option.appendTo("#product_id");
        });

        $("#product_id").trigger("change");
    });

    // load the processes when the product changes
    $("#product_id").change(function(evt) {
        var product_id = $(this).val();
        var processes = products_product_processes[product_id];

        $("#processes").empty();
        if (typeof(processes) == "undefined" || !processes.length) {
            $("<p> <?=__("there_are_no_processes_defined_for_this_product", "There are no processes defined for this product")?> </p>").appendTo("#processes");
        } else {
            $.each(processes, function(index, process) {
                $container = $("<div>");
                $container.css({
                    "background-image": "url(/images/process/" + process.processID + ".png)",
                    "background-repeat": "no-repeat",
                    "background-position": "center",
                    "width": "50px",
                    "height": "60px",
                });

                $container.attr("class", "radio_selector process");
                $container.attr("id", "process-" + process.processID);
                $container.attr("title", process.name);

                $input = $("<input type='radio' name='process_id' style='display:none;'>");
                $input.attr("value", process.processID);
                $input.appendTo($container);

                $container.appendTo("#processes");
                if (processes.length < 2) {
                    $container.trigger("click");
                }
            });
        }

        var upchargeGroup_id = $('#product_id option:selected').attr('data-upchargeGroup_id');
        $("#upchargeHolder").html("");
        if (upchargeGroups && upchargeGroups[upchargeGroup_id]) {
            $.each(upchargeGroups[upchargeGroup_id], function(i, item){
                $container = $("<span>");
                $container.attr("class", "radio_selector upcharge");
                $container.attr("id", "upcharge-" + item.upchargeID);
                $container.text(item.name);

                $input = $("<input type='checkbox' name='upcharges[]' style='display:none;'>");
                $input.attr("value", item.upchargeID);
                $input.appendTo($container);

                $container.appendTo("#upchargeHolder");
            });
        }

        $(".row-preference").remove();
        if (businessPreferences['Dry Clean']) {
            var preferences = businessPreferences['Dry Clean'];
            var category_id = $('#product_id option:selected').attr('data-productCategory_id');

            for (var slug in preferences) {
                var preference = preferences[slug];

                // skip preferences that have a specific category
                if (preference.parent_id > 0) {
                    if (preference.parent_id != category_id) {
                        continue;
                    }
                }

                var row = $('<tr class="row-preference"><th></th><td><select></select></td></tr>');
                $('th', row).text(preference.text);
                var select = $('select', row).attr('name', 'preference[' + preference.slug + ']');

                var customerDefault = customerPreferences[slug];
                var customerDefaultName = preference.options[customerDefault];

                for (var id in preference.options) {
                    $('<option></option>')
                        .text(preference.options[id] + (customerDefault == id ? '*' : ''))
                        .attr('value', id)
                        .appendTo(select);
                }

                // if customer has a default for this
                if (customerPreferences[slug]) {
                    //closure magic to capture customerDefault and customerDefaultName values
                    select.on('change', function (customerDefault, customerDefaultName) {
                        return function(evt) {
                            var $parent = $(this).parent();
                            $('.not-matching', $parent).remove();
                            if (customerDefault != $(this).val()) {
                                $parent.append('<p class="not-matching" style="color:red;">Does not match customer default: \''+ customerDefaultName  +'\'</p>');
                            }
                        }
                    }(customerDefault, customerDefaultName));

                    // set the customer default
                    select.val(customerPreferences[slug]);
                }

                row.insertAfter('#row-processes');
            }
        }

    });

    // quick item buttons
    $('.quick_button').click(function(evt) {
        var process_id = $(this).attr('data-process_id');
        var product_id = $(this).attr('data-product_id');

        $("#productCategory").val("show_all").trigger("change");
        $('#product_id').val(product_id).trigger("change");
        $("#process-" + process_id).not(".selected").trigger("click");
    });

    $('.quick_button').focus(function(evt) {
        $(this).trigger("click");
    });

    // generic handler for radio selectors
    $('#item_options').on('click', '.radio_selector', function(evt) {
        var $input = $('input', this);
        var type = $input.attr('type');

        if ($(this).hasClass('selected')) {
            $(this).removeClass("selected");
            $('input', this).attr("checked", false);
        } else {
            $(this).addClass("selected");
            $('input', this).attr("checked", true);

            if (type == 'radio') {
                var $siblings = $('.radio_selector', $(this).parent()).not(this);
                $siblings.removeClass("selected");
                $('input', $siblings).attr("checked", false);
            }
        }
    });

    // intercept the 'enter' and trigger a click on the create item button.
    $("html").keypress(function(evt) {
        if (evt.keyCode == 13) {
            $("#item_form input[type=submit]").trigger("click");
        }
    });

    $("#productCategory").trigger("change");
    $('#product_id').val(item.product_id);

    $("#product_id").trigger("change");
    $("#process-"+item.process_id).not('.selected').trigger("click");

    if (item.upcharges) {
        for (var upchargeID in item.upcharges) {
            $('#upcharge-' + upchargeID).trigger("click");
        }
    }

    if (item.preferences) {
        for (var slug in item.preferences) {
            $('select[name="preference['+slug+']"]').val(item.preferences[slug]).trigger('change');
        }
    }

    $('#item_form').submit(function(evt) {
        evt.preventDefault();
        var url = $(this).attr("action");

        $("#progress").html("<img src='/images/progress.gif' /> Working...");
        $('input[type=submit]', this).removeClass('orange').addClass('disabled');

        $.ajax({
            url: url,
            type:"POST",
            data: $(this).serialize(),
            dataType: "JSON",
            error: function(jqXHR, exception)
            {
                $(".disabled").removeClass('disabled').addClass('orange');
                $("#progress").html("");

                alert('<?=__("an_unhandled_server_side_error_occurred_could_not_create_item", "An unhandled server side error occurred, could not create item")?>');
            },
            success: function(data)
            {
                if(data.status == 'success') {
                    top.location.href = data.redirect;
                } else {
                    $(".disabled").removeClass('disabled').addClass('orange');
                    $("#progress").html("");
                    $("<div>" + data.message + "</div>").dialog({
                        modal: true,
                        title: "Error",
                        buttons: {
                            "Ok" : function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            }
        });
    });


    $.unblockUI(); //remove the loading modal
});
</script>
