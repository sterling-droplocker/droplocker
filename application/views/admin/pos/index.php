<?php enter_translation_domain("admin/pos/index"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = 
            "<tr>
                <td class='label'>
                    <label for='firstName'>" .  __('First Name', 'First Name') . "</label>
                </td>
                <td>
                    <span class='fill'><input type='text' name='firstName' id='firstName' class='big-input'></span>
                </td>
                <td style='width:200px;'>
                </td>
            </tr>";
            
$fieldList['lastName'] = 
            "<tr>
                <td class='label'>
                    <label for='lastName'>" .  __('Last Name', 'Last Name') . "</label>
                </td>
                <td>
                    <span class='fill'><input type='text' name='lastName' id='lastName' class='big-input'></span>
                </td>
                <td style='width:200px;'>
                </td>
            </tr>";
            
$nameForm = getSortedNameForm($fieldList);

?>
<?php

$customer_name = null;
if (!empty($customer)) {
    $customer_name = $customer->email;
    if ($customer->firstName || $customer->lastName) {
        $customer_name = getPersonName($customer);
    }
}

?>
<h2><?=__("Point of Sale", "Point of Sale"); ?></h2>

<div style="float: right; padding:5px; <?= empty($location) ? 'background: #f99; border: 1px solid red;' : 'background: #ccf' ?>;">
<form method="post" action="/admin/pos/set_location">
    <span id="current_location">
    <? if (!empty($location)): ?>
        <?=__("Location:", "Location:"); ?> <b><?= $location->address ?></b> 
        (<a href="#" onclick="$('#change_location').show(); $('#current_location').hide(); return false;"><?=__("Change", "Change"); ?></a>)
    <? else: ?>
        <b><?=__("Select a location:", "Select a location:"); ?></b>
    <? endif; ?>
    </span>

    <span id="change_location" style="display:<?= empty($location) ? 'inline' : 'none'; ?>">
        <?= form_dropdown('location_id', array('' => '<?=__("-- SELECT LOCATION --", "-- SELECT LOCATION --"); ?>') + $locations, $location_id, 'id="pos_location"'); ?>
        <input type="submit" value="<?=__("Set Location", "Set Location"); ?>">
    </span>
</form>
</div>

<form method="post" action="/admin/pos/create_order_by_bag" style="clear:both;">
    <input type="hidden" name="location_id" value="<?= $location_id ?>">

    <fieldset style="margin:0 auto 1em; max-width: 800px; padding: 1em 2em;">
        <legend><?=__("Create an Order Using a Bag", "Create an Order Using a Bag"); ?></legend>
        <table style="width:100%;">
            <tr>
                <td class="label">
                    <label for="bag_number"><?=__("Scan Bag", "Scan Bag"); ?></label>
                </td>
                <td>
                    <span class="fill"><input type="text" name="bag_number" id="bag_number" class="big-input"></span>
                </td>
                <td style="width:200px;">
                    <input class="button orange big-button" type="submit" value="<?=__("Create Order", "Create Order"); ?>">
                </td>
            </tr>
        </table>
    </fieldset>
</form>

<form method="post" action="/admin/pos/create_order_by_customer">
    <input type="hidden" name="location_id" value="<?= $location_id ?>">
    <input type="hidden" name="customer_id" value="<?= $customer_id ?>">

    <fieldset style="margin:0 auto 1em; max-width: 800px; padding: 1em 2em;">
        <legend><?=__("Create Order for Customer", "Create Order for Customer"); ?></legend>
        
        <table style="width:100%;">
            <tr>
                <td class="label">
                    <label for="customer"><?=__("Customer Lookup", "Customer Lookup"); ?></label>
                </td>
                <td>
                    <span class="fill"><input type="text" name="customer" value="<?= $customer_name ?>" id="customer" class="big-input"></span>
                    <? if (!empty($customer)): ?>
                    <div id="existing_customer" style="padding-left: 10px; color: red;"><?=__("The customer has been loaded, you can create the order.", "The customer has been loaded, you can create the order."); ?></div>
                    <? endif; ?>
                </td>
                <td style="width:200px;">
                    <input class="button orange big-button" type="submit" value="<?=__("Create Order", "Create Order"); ?>">
                </td>
            </tr>
        </table>
    </fieldset>
</form>

<form method="post" action="/admin/pos/create_customer">
    <input type="hidden" name="location_id" value="<?= $location_id ?>">

    <fieldset style="margin:0 auto 1em; max-width: 800px; padding: 1em 2em;">
        <legend><?=__("Create Customer", "Create Customer"); ?></legend>

        <table style="width:100%;">
            <?= $nameForm ?>
            <tr>
                <td class="label">
                    <label for="email"><?=__("E-Mail", "E-Mail"); ?></label>
                </td>
                <td>
                    <span class="fill"><input type="text" name="email" id="email" class="big-input"></span>
                </td>
                <td style="width:200px;">
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="phone"><?=__("Phone", "Phone"); ?></label>
                </td>
                <td>
                    <span class="fill"><input type="text" name="phone" id="phone" class="big-input"></span>
                </td>
                <td style="width:200px;">
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="sms"><?=__("SMS", "SMS"); ?></label>
                </td>
                <td>
                    <span class="fill"><input type="text" name="sms" id="sms" class="big-input"></span>
                    <label><input type="checkbox" name="copy" value="1"> <?=__("Copy phone number", "Copy phone number"); ?></label>
                </td>
                <td style="width:200px;">
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="promo_code"><?=__("Promo Code", "Promo Code"); ?></label>
                </td>
                <td>
                    <span class="fill"><input type="text" name="promo_code" id="promo_code" class="big-input"></span>
                </td>
                <td style="width:200px;">
                </td>
            </tr>
            <tr>
                <td class="label">
                </td>
                <td style="text-align: right;">
                    <input class="button orange big-button" type="submit" value="<?=__("Create Customer", "Create Customer"); ?>">
                </td>
                <td style="width:200px;">
                </td>
            </tr>
        </table>
    </fieldset>
</form>


<script type="text/javascript">
$(function() {
    
// toggle menu
$("#toggle_link").click();

$('#bag_number').focus();

$("#customer").autocomplete({
    source : "/ajax/customer_autocomplete_aprax",
    select: function (event, customer) {
        $("input[name='customer_id']").val(customer.item.id);
        $('#existing_customer').hide();
    }
})

$('input[name=copy]').click(function(evt) {
    if ($(this).attr('checked')) {
        $('input[name=sms]').attr('disabled', true).val($('input[name=phone]').val());
    } else {
        $('input[name=sms]').attr('disabled', false).val('');
    }
});

});
</script>
