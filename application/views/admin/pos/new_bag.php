<?php enter_translation_domain("admin/pos/new_bag"); ?>
<?php

$new_customer = !empty($new_customer);
$customer_name = '#' . $customer->customerID;
if ($customer->firstName || $customer->lastName) {
    $customer_name = getPersonName($customer);
}
$customer_url = "/admin/customers/detail/{$customer->customerID}";
$print_bag_url = "/admin/admin/print_bag_tag/{$customer->customerID}/{$bagNumber}/{$customer->phone}";


?>
<? if ($new_customer): ?>
    <h2><?=__("New Customer Account Created", "New Customer Account Created"); ?></h2>
<? else: ?>
    <h2><?=__("New Bag Added to Customer", "New Bag Added to Customer"); ?></h2>
<? endif; ?>

<div class="alert-success">
    <? if ($new_customer): ?>
        <h3><?=__("A new customer account was created!", "A new customer account was created!"); ?></h3>
    <? endif; ?>

    <p><?=__("A new bag with number", "A new bag with number <b>%bagNumber%</b> was added to %user_link%'s account.", array("bagNumber"=>$bagNumber, "user_link"=>'<a href="'.$customer_url.'" target="_blank">'.$customer_name.'</a>')); ?>
    </p>
</div>

<h3><?=__("You may now", "You may now %link%
    and then proceed to create the order.", array("link"=>'<a target="_blank" href="'.$print_bag_url.'">print the bag tag</a>')); ?></h3>

<br><br>
<form method="post" action="/admin/pos/create_order_by_bag">
    <input type="hidden" name="customer_id" value="<?= $customer->customerID ?>">
    <input type="hidden" name="bag_number" value="<?= $bagNumber ?>">
    <input type="hidden" name="location_id" value="<?= $location_id ?>">
    <input type="submit" class="button orange" value="<?=__("Create the Order", "Create the Order"); ?>">
</form>
