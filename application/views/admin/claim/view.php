<?php

enter_translation_domain("admin/claim/view");

$dateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat");
$cutoff_time = get_business_meta($this->business->businessID, 'cutOff');
$cutoff = strtotime($cutoff_time);


?>
<style type="text/css">
    label
    {
        margin-right: 5px;
    }
    form input
    {
        margin-right: 5px;
    }
    .editable:hover {
        border: 3px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3;
    }
</style>

<link rel="stylesheet" type="text/css" href="/css/tooltips/jquery.tooltip.css">
<script type="text/javascript" language="javascript" src="/js/tooltips/jquery.tooltip.js"></script>
<script type="text/javascript">
$(document).ready(function() {

$("#claims_tabs").tabs({
    fx: {
        opacity: 'toggle'
    }
});

$.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
{
	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
		return $('td:eq('+iColumn+')', tr).attr('data-value');
	});
}

    inactive_claims_dataTable = $("#inactive_claims").dataTable( {
        "aoColumns" : [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        "bProcessing" : true,
        "bServerSide" : true,
        "aaSorting" : [[ 0, "DESC" ]],
        "sAjaxSource" : "/admin/claim/get_inactive_claims_in_dataTables_format",
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
            aButtons : [
                "pdf", "print"
            ],
            <?php endif ?>
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
        },
        //server side tooltip
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            var data = $('td:eq(3) > a', nRow).attr('data-tooltip');
            $('td:eq(3)', nRow).attr( 'class', '_tooltip' );
            $('td:eq(3)', nRow).attr( 'title', data );
        },
        "fnDrawCallback": function( oSettings ) {
            inactive_claims_dataTable.$('._tooltip').tooltip({
                "delay": 0,
                "track": false,
                "fade": 250
            });
        }
        //end of tooltip
    }).fnSetFilteringDelay();

    active_claims_dataTable = $("#active_claims").dataTable({
        "iDisplayLength" : 25,
        "bPaginate" : false,

        "fnStateLoad": function (oSettings) {
            return JSON.parse( localStorage.getItem('DataTables') );
        },
        "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            { "sSortDataType": "attr" },
            null,
            null,
            null
        ],

        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
            //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
            aButtons : [
                "pdf", "print"
            ],
            <?php endif ?>
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
        }
    });

    $(".edit_field", active_claims_dataTable.fnGetNodes()).editable("/admin/claim/update", {
        "width" : "100%",
        "height" : "100%",
        "id" : "identifer",
        type: "textarea",
        "cancel" : "<img src='/images/icons/delete.png' />",
        "submit" : "<img src='/images/icons/accept.png' />",
        "indicator" : "Saving ... <img src='/images/progress.gif' />",
        "data"    : function(string) {
            return $.trim(string)
        },
        "callback": function(sValue, y) {
            var aPos = active_claims_dataTable.fnGetPosition( this );
            active_claims_dataTable.fnUpdate( sValue, aPos[0], aPos[1] )

            if ($('.access-denied', this).length) {
                $(this).editable('disable');
            }
        },
        "submitdata": function ( value, settings ) {
            return {
                    "property" : $(this).attr("data-property"),
                    "claimID": $(this).attr("data-id")
            }
        }
    });

    $(".delete_claim").click( function() {
        return confirm("<?= __("confirm_delete_claim", "Are you sure you want to delete this claim?") ?>");
    });

    $(".no_ord").click(function() {
    	return confirm("<?= __("confirm_no_ord", "Are you sure you want to continue?") ?>");
    });

    //no server side tooltip
    active_claims_dataTable.$('._tooltip').tooltip({
        "delay": 0,
        "track": false,
        "fade": 250
    });

});
</script>

<h2><?= __("claimed", "Claimed Orders") ?></h2>

<div id="claims_tabs">
    <ul>
        <li> <a href="#active_claims_tab"><?= __("active", "Active Claims") ?></a> </li>
        <li> <a href="#inactive_claims_tab"><?= __("inactive", "Inactive Claims") ?></a> </li>
    </ul>
    <div id="active_claims_tab">

    <? if($claims): ?>
        <table id="active_claims">
            <thead>
                <tr>
                    <th><?= __("claim", "ClaimID") ?> </th>
                    <th><?= __("service", "Service Type") ?></th>
                    <th><?= __("location", "Location") ?></th>
                    <th><?= __("locker", "Locker") ?></th>
                    <th><?= __("customer", "Customer") ?></th>
                    <th align='center'><?= __("route", "Rte") ?></th>
                    <th><?= __("when", "When Placed") ?></th>
                    <th><?= __("type", "Type") ?></th>
                    <th><?= __("notes", "Notes") ?></th>
                    <th><?= __("delete", "Delete") ?>?</th>
                </tr>
            </thead>
            <tbody>
            <? foreach($claims as $claim):?>
                <tr>
                    <td> <?= $claim->claimID ?> </td>
                            <td><?= $claim->serviceType?></td>
                            <td><a href='/admin/orders/view_location_orders/<?= $claim->locationID ?>'><?= ucwords($claim->address)?></a></td>
                            <td><?= $claim->lockerName?>
                                <br>
                                <a id="noOrd_<?=$claim->claimID?>" href="/admin/orders/claimed_noOrd/<?= $claim->claimID ?>" class="no_ord" style="color: red; font-size: 90%;">
                                    <?= __("no_order", "no ord") ?><?= __("email", "(email)") ?>
                                </a>
                            </td>
                            <td class="_tooltip" title="Email: <?=$claim->email?><br/>Phone: <?=$claim->phone?><br/>Address: <?=$claim->address1?><br/>Customer Notes: <?=$claim->customer_notes?>" ><a href='/admin/customers/detail/<?= $claim->customerID?>'><?= getPersonName($claim) ?></a></td>
                            <td align='center'><?= $claim->route_id?></td>
                            <td nowrap="true" data-value="<?= $claim->updated ?>"><?= convert_from_gmt_aprax($claim->updated, $dateFormat) ?>
                                <? if (strtotime($claim->updated . " GMT") > $cutoff): ?>
                                <br><a href="/admin/orders/claimed_today/<?= $claim->claimID ?>"><?= __("today", "today") ?></a>
                                <? endif; ?>
                            </td>

                            <td nowrap><?= isset($ordType[$claim->claimID]) ? $ordType[$claim->claimID] : '' ?></td>
                            <td class="edit_field editable" data-property="notes" data-id="<?= $claim->claimID?>">
                                <?= $claim->order_notes ?>
                            </td>
                            <td><a class="delete_claim" href="/admin/orders/claimed_delete/<?= $claim->claimID ?>"><img src='/images/icons/cross.png' /></a></td>
                        </tr>
                        <? endforeach; ?>
            </tbody>
        </table>
    <? else: ?>
            <div><?= __("no_active", "No Active Claims") ?></div>
    <? endif;?>

    </div>
    <div id="inactive_claims_tab">

        <table id="inactive_claims">
            <thead>
                <tr>
                    <th><?= __("claim", "ClaimID") ?> </th>
                    <th><?= __("location", "Location") ?></th>
                    <th><?= __("locker", "Locker") ?></th>
                    <th><?= __("customer", "Customer") ?></th>
                    <th><?= __("when", "When Placed") ?></th>
                    <th><?= __("type", "Type") ?></th>
                    <th><?= __("notes", "Notes") ?></th>
                    <th><?= __("employee_name", "Employee name") ?></th>
                    <th><?= __("last_updated", "Last Updated") ?>?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="6" class="dataTables_empty"><?= __("loading", "Loading Data From Server") ?> </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
