<?php enter_translation_domain("admin/third_party_deliveries/drop_locations_edit"); ?>
<h2><?php echo __("Edit Drop Location", "Edit Drop Location"); ?></h2>
<p><a href="/admin/third_party_deliveries/drop_locations"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Drop Locations", "Back to Drop Locations"); ?></a></p>
<br>

<form method="post" action="/admin/third_party_deliveries/drop_locations_save">
    <?= form_hidden('dropLocationID', $dropLocation->dropLocationID); ?>
    <table class="detail_table">
        <tr>
            <th>
                <label for="drop_location_address"><?php echo __("Address", "Address"); ?></label>
            </th>
            <td>
                <?= form_input('address', $dropLocation->address, 'id="drop_location_address"'); ?>
                <p><?php echo __("Address", "Address"); ?>, <?php echo __("City", "City"); ?>, <?php echo __("State", "State"); ?></p>
            </td>
        </tr>
        <tr>
            <th>
                <label for="drop_location_default"><?php echo __("Default", "Default?"); ?></label>
            </th>
            <td>
                <?= form_checkbox('default', 1, $dropLocation->default, 'id="drop_location_default"'); ?>
            </td>
        </tr>
    </table>
    <div>
        <input type="submit" value="<?php echo __("Update", "Update"); ?>" class="button orange">
    </div>
</form>
