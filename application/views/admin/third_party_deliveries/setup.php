<?php enter_translation_domain("admin/third_party_deliveries/setup"); ?>
<h2><?php echo __("Setup Third Party Delivery Service", "Setup Third Party Delivery Service"); ?></h2>
<p>
    <a href="/admin/third_party_deliveries/index/<?= $service ?>"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Third Party Deliveries", "Back to Third Party Deliveries"); ?></a>
</p>
<br>

<form method="post" action="/admin/third_party_deliveries/setup">
    <table class="detail_table">
        <tr>
            <th>
                <label for="service"><?php echo __("Select Service", "Select Service"); ?></label>
            </th>
            <td>
                <?= form_dropdown('service', array('' => __("SELECT", "--SELECT--")) + $services, $service, 'id="service"'); ?>
            </td>
        </tr>
    </table>
</form>

<br>
<br>
<form method="post" action="/admin/third_party_deliveries/setup/<?= $service ?>">
    <? if ($service): ?>
        <h2>Setup <?= $services[$service] ?> API</h2>

        <table class="detail_table">
    <? endif; ?>

    <? if ($service == 'rickshaw'): ?>
        <tr>
            <th>
                <label for="rickshaw_api_key"><?php echo __("Rickshaw API Key", "Rickshaw API Key"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_api_key', get_business_meta($this->business_id, 'rickshaw_api_key'), 'id="rickshaw_api_key"'); ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_route"><?php echo __("Default Route", "Default Route"); ?></label>
            </th>
            <td>
                <?= form_dropdown('rickshaw_route', $routes, get_business_meta($this->business_id, 'rickshaw_route'), 'id="rickshaw_route"'); ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_contact_name"><?php echo __("Contact Name", "Contact Name"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_contact_name', get_business_meta($this->business_id, 'rickshaw_contact_name'), 'id="rickshaw_contact_name" style="width:200px;"'); ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_contact_phone"><?php echo __("Contact Phone", "Contact Phone"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_contact_phone', get_business_meta($this->business_id, 'rickshaw_contact_phone', $this->business->phone), 'id="rickshaw_contact_phone" style="width:200px;"'); ?>
            </td>
        </tr>
	</table>

	<br>
	<h3><?php echo __("Pickups", "Pickups"); ?></h3>
	<table class="detail_table">
         <tr>
            <th>
                <label for="rickshaw_pickup_min_time"><?php echo __("Minimum Pickup Time", "Minimum Pickup Time"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_pickup_min_time', get_business_meta($this->business_id, 'rickshaw_pickup_min_time', '8:00'), 'id="rickshaw_pickup_min_time" style="width:40px;"'); ?>
                <b><?php echo __("hh_mm", "(hh:mm)"); ?></b>
                <br><small><?php echo __("Min time 08:00 PST. No order will be picked up before this", "Min time 08:00 PST. No order will be picked up before this."); ?></small>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_pickup_max_time"><?php echo __("Maximum Pikcup Time", "Maximum Pikcup Time"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_pickup_max_time', get_business_meta($this->business_id, 'rickshaw_pickup_max_time', '18:00'), 'id="rickshaw_pickup_max_time" style="width:40px;"'); ?>
                <b><?php echo __("hh_mm", "(hh:mm)"); ?></b>
                <br><small><?php echo __("Max time 18:00 PST. After this, the orders will be delivered next day", "Max time 18:00 PST. After this, the orders will be delivered next day."); ?></small>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_pickup_address"><?php echo __("Pickup Destination Address", "Pickup Destination Address"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_pickup_address', get_business_meta($this->business_id, 'rickshaw_pickup_address'), 'id="rickshaw_pickup_address"'); ?>
                <div><?php echo __("Claims will be delivered here", "Claims will be delivered here"); ?></div>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_pickup_notes"><?php echo __("Pickup Notes", "Pickup Notes"); ?></label>
            </th>
            <td>
                <?= form_textarea(array(
                    'name' => 'rickshaw_pickup_notes',
                    'value' => get_business_meta($this->business_id, 'rickshaw_pickup_notes'),
                    'id' => 'rickshaw_pickup_notes',
                    'rows' => 4
                )); ?>
            </td>
        </tr>
	</table>

	<br>
	<h3><?php echo __("Deliveries", "Deliveries"); ?></h3>
	<table class="detail_table">
         <tr>
            <th>
                <label for="rickshaw_delivery_min_time"><?php echo __("Minimum Delivery Time", "Minimum Delivery Time"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_delivery_min_time', get_business_meta($this->business_id, 'rickshaw_delivery_min_time', '8:00'), 'id="rickshaw_delivery_min_time" style="width:40px;"'); ?>
                <b><?php echo __("hh_mm", "(hh:mm)"); ?></b>
                <br><small><?php echo __("Min time 08:00 PST. No order will be picked up before this", "Min time 08:00 PST. No order will be picked up before this."); ?></small>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_delivery_max_time"><?php echo __("Maximum Delivery Time", "Maximum Delivery Time"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_delivery_max_time', get_business_meta($this->business_id, 'rickshaw_delivery_max_time', '18:00'), 'id="rickshaw_delivery_max_time" style="width:40px;"'); ?>
                <b><?php echo __("hh_mm", "(hh:mm)"); ?></b>
                <br><small><?php echo __("Max time 18:00 PST. After this, the orders will be delivered next day", "Max time 18:00 PST. After this, the orders will be delivered next day."); ?></small>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_delivery_address"><?php echo __("Delivery Source Address", "Delivery Source Address"); ?></label>
            </th>
            <td>
                <?= form_input('rickshaw_delivery_address', get_business_meta($this->business_id, 'rickshaw_delivery_address'), 'id="rickshaw_delivery_address"'); ?>
                <div><?php echo __("Orders will be picked up from here", "Orders will be picked up from here"); ?></div>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_delivery_notes"><?php echo __("Delivery Notes", "Delivery Notes"); ?></label>
            </th>
            <td>
                <?= form_textarea(array(
                    'name' => 'rickshaw_delivery_notes',
                    'value' => get_business_meta($this->business_id, 'rickshaw_delivery_notes'),
                    'id' => 'rickshaw_delivery_notes',
                    'rows' => 4
                )); ?>
            </td>
        </tr>

    <? else :?>
        <p class="alert-warning"><?php echo __("Please, select a valid service", "Please, select a valid service"); ?></p>
    <? endif; ?>

    <? if ($service): ?>
        </table>
        <div><input type="submit" value="<?php echo __("Update", "Update"); ?>" class="button orange"></div>
    <? endif; ?>

</form>

<script type="text/javascript">
    $('#service').change(function(evt) {
        var val = $(this).val();
        top.location.href = '/admin/third_party_deliveries/setup/' + val;
    });
</script>
