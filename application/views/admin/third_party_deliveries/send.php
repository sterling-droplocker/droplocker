<?php enter_translation_domain("admin/third_party_deliveries/send"); ?>
<h2><?php echo __("External Status", "External Status"); ?></h2>
<p>
    <a href="/admin/third_party_deliveries/index/<?= $service ?>/<?= $route ?>"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Third Party Deliveries", "Back to Third Party Deliveries"); ?></a>
</p>
<br>


<? if (!empty($deliveries_status)): ?>
<h2><?php echo __("Deliveries set to", "Deliveries set to"); ?> <?= $services[$service] ?></h2>

<? foreach($deliveries_status as $id => $status): ?>
    <div class="alert-<?= $status['status'] ?>">
        <h3><?php echo __("Order", "Order"); ?> <?= $id ?></h3>
        <?= nl2br($status['message']) ?>
    </div>
<? endforeach; ?>
<? endif; ?>

<? if (!empty($pickup_status)): ?>
<h2>Pickups sent to <?= $services[$service] ?></h2>

<? foreach($pickup_status as $id => $status): ?>
    <div class="alert-<?= $status['status'] ?>">
        <h3><?php echo __("Claim", "Claim"); ?> <?= $id ?></h3>
        <?= nl2br($status['message']) ?>
    </div>
<? endforeach; ?>
<? endif; ?>
