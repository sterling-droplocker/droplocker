<?php enter_translation_domain("admin/third_party_deliveries/index"); ?>
<?php

$date_format = get_business_meta($this->business_id, 'shortDateDisplayFormat');
$full_date_format = get_business_meta($this->business_id, 'fullDateDisplayFormat');

foreach($drop_locations as $k => $v) {
    list($drop_locations[$k]) = explode(',', $v);
}

?>
<h2><?php echo __("Third Party Deliveries Pickups", "Third Party Deliveries / Pickups"); ?></h2>
<br>

<form method="post" action="/admin/third_party_deliveries/">
    <table class="detail_table">
        <tr>
            <th>
                <label for="service"><?php echo __("Select Third Party", "Select Third Party"); ?></label>
            </th>
            <td>
                <?= form_dropdown('service', $services, $service, 'id="service"'); ?>
                <a id="setup_link" href="/admin/third_party_deliveries/setup/"><?php echo __("Setup", "Setup"); ?></a>
            </td>
        </tr>
        <tr>
            <th>
                <label for="route"><?php echo __("Select Route", "Select Route"); ?></label>
            </th>
            <td>
                <?= form_dropdown('route', $routes, $route, 'id="route"'); ?>
            </td>
        </tr>
    </table>
    <div>
        <input type="submit" value="<?php echo __("Update", "Update"); ?>" class="button orange">
    </div>
</form>

<br><br>

<form method="post" action="/admin/third_party_deliveries/send">
    <?= form_hidden('service', $service); ?>
    <?= form_hidden('route', $route); ?>

    <h3><?php echo __("Deliveries", "Deliveries"); ?></h3>
    <div style="float:right;">
        <?php echo __("Select", "Select:"); ?>
        <a href="javascript:selectAll('#deliveries')"><?php echo __("All Deliveries", "All Deliveries"); ?></a>
        / <a href="javascript:selectNone('#deliveries')"><?php echo __("No Delivery", "No Delivery"); ?></a>
        / <a href="javascript:selectAll('')"><?php echo __("All", "All"); ?></a>
        / <a href="javascript:selectNone('')"><?php echo __("None", "None"); ?></a>
    </div>
    <table id="deliveries" class="hor-minimalist-a striped">
        <thead>
            <tr>
                <th width="200"><?php echo __("Location", "Location"); ?></th>
                <th width="200"><?php echo __("Location Notes", "Location Notes"); ?></th>
                <th width="150"><?php echo __("Customer", "Customer"); ?></th>
                <th><?php echo __("Order ID", "Order ID"); ?></th>
                <th><?php echo __("Order Type", "Order Type"); ?></th>
                <th><?php echo __("Date Created", "Date Created"); ?></th>
                <th><?php echo __("Due Date", "Due Date"); ?></th>
                <th><?php echo __("3rd Party", "3rd Party"); ?></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($deliveries as $delivery): ?>
            <tr>
                <td><a target="_blank" href="/admin/admin/location_details/<?= $delivery->locationID ?>"><?= $delivery->address ?></a></td>
                <td>
                    <? if(stristr($delivery->accessCode, 'key')): ?>
                        <strong><?= $delivery->accessCode ?></strong>
                    <? else: ?>
                        <?= str_replace(array("1", "2", "5", "6", "8", "0"), "?", $delivery->accessCode) ?>
                    <? endif; ?>
                </td>
                <td><a target="_blank" href="/admin/customers/detail/<?= $delivery->customerID ?>"><?= getPersonName($delivery) ?></a></td>
                <td><a target="_blank" href="/admin/orders/details/<?= $delivery->orderID ?>"><?= $delivery->orderID ?></a></td>
                <td><?= $delivery->orderType ?></td>
                <td><?= convert_from_gmt_aprax($delivery->dateCreated, $date_format) ?></td>
                <td><?= apply_date_format($delivery->dueDate, $date_format) ?></td>
                <td style="padding:5px;">
                    <? if ($delivery->third_party_id): ?>
                        <b><?php echo __("Sent to", "Sent to:"); ?></b> <?= $services[$delivery->service] ?><br>
                        <b><?php echo __("Date", "Date:"); ?></b> <?= convert_from_gmt_aprax($delivery->deliveryDate, $date_format) ?><br>
                        <a class="button blue small-button" href="/admin/third_party_deliveries/resend_delivery/<?= $delivery->orderExternalDeliveryID ?>" onclick="return verify_delete();"><?php echo __("Resend", "Resend"); ?> (<?= $delivery->grouped ?>)</a>
                    <? else: ?>
                        <input type="checkbox" checked name="deliveries[]" value="<?= $delivery->orderID ?>">
                    <? endif; ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <br><br><br><br>
    <h3><?php echo __("Pickups", "Pickups"); ?></h3>
    <div style="float:right;">
        <?php echo __("Select", "Select:"); ?>
        <a href="javascript:selectAll('#pickups')"><?php echo __("All Pickups", "All Pickups"); ?></a>
        / <a href="javascript:selectNone('#pickups')"><?php echo __("No Pickup", "No Pickup"); ?></a>
        / <a href="javascript:selectAll('')"><?php echo __("All", "All"); ?></a>
        / <a href="javascript:selectNone('')"><?php echo __("None", "None"); ?></a>
    </div>

    <table id="pickups" class="hor-minimalist-a striped">
        <thead>
            <tr>
                <th width="200"><?php echo __("Location", "Location"); ?></th>
                <th width="200"><?php echo __("Location Notes", "Location Notes"); ?></th>
                <th width="150"><?php echo __("Customer", "Customer"); ?></th>
                <th><?php echo __("Claim ID", "Claim ID"); ?></th>
                <th width="100"><?php echo __("Date Created", "Date Created"); ?></th>
                <th width="200"><?php echo __("Notes", "Notes"); ?></th>
                <th width="100"><?php echo __("3rd Party", "3rd Party"); ?></th>
                <th width="200"><?php echo __("Drop Location", "Drop Location"); ?> (<a href="/admin/third_party_deliveries/drop_locations"><?php echo __("Edit", "Edit"); ?></a>)</th>
            </tr>
        </thead>
        <tbody>
        <? foreach($pickups as $pickup): ?>
            <tr>
                <td><a target="_blank" href="/admin/admin/location_details/<?= $pickup->locationID ?>"><?= $pickup->address ?></a></td>
                <td>
                    <? if(stristr($pickup->accessCode, 'key')): ?>
                        <strong><?= $pickup->accessCode ?></strong>
                    <? else: ?>
                        <?= str_replace(array("1", "2", "5", "6", "8", "0"), "?", $pickup->accessCode) ?>
                    <? endif; ?>
                </td>
                <td><a target="_blank" href="/admin/customers/detail/<?= $pickup->customerID ?>"><?= getPersonName($pickup) ?></a></td>
                <td><?= $pickup->claimID ?></td>
                <td><?= convert_from_gmt_aprax($pickup->created, $full_date_format) ?></td>
                <td><?= nl2br($pickup->notes) ?></td>
                <td>
                    <? if ($pickup->third_party_id): ?>
                        <b><?php echo __("Sent to", "Sent to:"); ?></b> <?= $services[$pickup->service] ?><br>
                        <b><?php echo __("Date", "Date:"); ?></b> <?= convert_from_gmt_aprax($pickup->pickupDate, $date_format) ?><br>
                    <? else: ?>
                        <input type="checkbox" checked name="pickups[]" value="<?= $pickup->claimID ?>">
                    <? endif; ?>
                </td>
                <td>
                    <? if ($pickup->third_party_id): ?>
                        <?= $pickup->drop_address ?>
                    <? else: ?>
                        <?= form_dropdown("dropLocation[{$pickup->claimID}]", $drop_locations, $default_location); ?>
                    <? endif; ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <div style="margin-top: 2em;text-align: right;">
        <input type="submit" value="<?php echo __("Send to", "Send to"); ?> <?= $services[$service] ?>" class="button orange">
    </div>
</form>


<script type="text/javascript">

$('#setup_link').click(function(evt) {
    evt.preventDefault();
    top.location.href = this.href + $('#service').val();
});


function selectAll(selector) {
    $(selector + ' input[type=checkbox]').attr('checked', true);
}

function selectNone(selector) {
    $(selector + ' input[type=checkbox]').attr('checked', false);
}

</script>
