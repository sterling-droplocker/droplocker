<?php enter_translation_domain("admin/third_party_deliveries/drop_locations"); ?>
<h2><?php echo __("Drop Locations", "Drop Locations"); ?></h2>
<p><a href="/admin/third_party_deliveries/"><img src="/images/icons/arrow_left.png"> <?php echo __("Back to Third Party Deliveries", "Back to Third Party Deliveries"); ?></a></p>
<br>
<h3><?php echo __("Add New Drop Location", "Add New Drop Location"); ?></h3>
<form method="post" action="/admin/third_party_deliveries/drop_locations_save">
    <table class="detail_table">
        <tr>
            <th>
                <label for="drop_location_address"><?php echo __("Address", "Address"); ?></label>
            </th>
            <td>
                <?= form_input('address', null, 'id="drop_location_address"'); ?>
                <p><?php echo __("Address", "Address"); ?>, <?php echo __("City", "City"); ?>, <?php echo __("State", "State"); ?></p>
            </td>
        </tr>
        <tr>
            <th>
                <label for="drop_location_default"><?php echo __("Default", "Default?"); ?></label>
            </th>
            <td>
                <?= form_checkbox('default', 1, 0, 'id="drop_location_default"'); ?>
            </td>
        </tr>
    </table>
    <div>
        <input type="submit" value="<?php echo __("Create", "Create"); ?>" class="button orange">
    </div>
</form>

<br><br>

<h3>Drop Locations</h3>
<? if ($dropLocations): ?>
<table class="hor-minimalist-a striped">
    <thead>
        <tr>
            <th>Address</th>
            <th>Default</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <? foreach ($dropLocations as $dropLocation): ?>
        <tr>
            <td><?= $dropLocation->address ?></td>
            <td><?= $dropLocation->default ? 'Yes' : 'No' ?></td>
            <td>
                <a href="/admin/third_party_deliveries/drop_locations_delete/<?= $dropLocation->dropLocationID ?>" onclick="return verify_delete();"><img src="/images/icons/cross.png"></a>
                <a href="/admin/third_party_deliveries/drop_locations_edit/<?= $dropLocation->dropLocationID ?>"><img src="/images/icons/pencil.png"></a>
            </td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
<? else: ?>
    <p class="alert-warning">There are no Drop Locations created yet.</p>
<? endif; ?>
