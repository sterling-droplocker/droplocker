<?php 

enter_translation_domain("admin/coupon/manage");
$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");


/**
 * Expects at least the following content variables
 * 
 * active_coupons array
 * inactive_coupons array
 * bizzie_superadmin bool
 * initialSearch string
 * business stdClass
 * products array
 * frequencies array
 * amountTypes array
 * first_time_fields array
 * recurring_types array
 * processes array
 */

if (!is_array($products)) {
    trigger_error("'products' must be an array", E_USER_WARNING);
}

if (!is_array($processes)) {
    trigger_error("'processes' must be an array", E_USER_WARNING);
}

?>
<style type="text/css">
    .expired {
       background-color: #FF0000 !IMPORTANT; 
       color: black; 
    }
    .editable:hover {
        border: 3px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3;
    }
    table.dataTable td {
        border: 3px solid transparent;
        padding: 10px;
    }
    h3 {
        margin: 5px 0px;
    }
</style>


<style type="text/css">
    fieldset {
        border: 1px solid;
    }
    fieldset legend {
        font-weight: bold;
    }
</style>

<h2> <?= __("create_new", "Create New Coupon") ?></h2>

    <?= form_open("/admin/coupons/add", array("id" => "add_coupon", "style" => "min-width:1000px; max-width:1000px;")) ?>
        <fieldset style='padding: 10px; margin:1em 0;'>
            <div style='margin: 10px'>
                <label for="were"><?= __("label_type", "Type:") ?></label>
                <select style='margin: 0px 5px' name="where" id="were">
                    <option value="PC" SELECTED><?= __("postcard", "Postcard") ?></option>
                    <option value="WS"><?= __("web", "Website") ?></option>
                    <option value="IP"><?= __("person", "In Person") ?></option>
                    <option value="PR"><?= __("print", "Print") ?></option>
                    <option value="FL"><?= __("flyers", "Flyers") ?></option>
                    <option value="TH"><?= __("other", "Other") ?></option>
                </select>

                <label for="amount"><?= __("label_amount", "Amount:") ?></label>
                <?= form_input(array("name" => "amount", "id" => "amount", "size" => "5", "style" => "margin: 0px 8px")) ?>

                <label for="amountType"><?= __("label_discountType", "Discount Type:") ?></label>
                <?= form_dropdown("amountType", $amountTypes, null, 'id="amountType"'); ?>

                <span id="product"> </span>

                <span id="process_id_wrapper" style="display:none;">
                <label for="process_id"><?= __("label_process_id", "Process:") ?></label>
                <?= form_dropdown("process_id", $processes, array('0'), 'id="process_id"'); ?>
                </span>

            </div>
            <div style='margin:10px;'>
                <label for="frequency"><?= __("label_requency", "Frequency:") ?></label>
                <?= form_dropdown("frequency", $frequencies, "one time", 'id="frequency"'); ?>

                <span id='maxAmoutSpan'>
                    <label form="maxAmt"><?= __("label_max", "Max $")  ?></label>
                    <?= form_input(array("name" => "maxAmt", "id" => "maxAmt", "size" => 5, "style" => 'margin: 0px 8px' )) ?>
                </span>

                <label for="extendedDesc"><?= __("label_published", "Where Published") ?></label>
                <?= form_input(array("name" => "extendedDesc", "id" => "extendedDesc", "size" => 35, "style" => "margin: 0px 8px")) ?>
            </div>
            <div style='margin: 10px'>
                <label for="startDate"><?= __("label_start", "Start:") ?></label>
                <?= form_input(array("name" => "startDate", "id" => "startDate", "class" => "date_field", "value" => date("m/d/y"), "style" => "margin: 0px 8px")) ?>

                <label for="endDate"><?= __("label_end", "End (NEVER if no expire)") ?> </label>
                <?= form_input(array("style" => "margin: 0px 5px", "name" => "endDate", "id" => "endDate", "size" => 10, "class" => "date_field", "type" => "text", "value" => date("m/d/y", strtotime("+1 year")))) ?>

                <span style='border: 1px solid; margin: 0px 5px; padding: 5px;'>
                    <label for="firstTime"><?= __("label_only", "1st order only") ?></label>
                    <?= form_checkbox(array("name" => "firstTime", "id" => "firstTime", "value" => 1, "checked" => true, "style" => "margin: 0px 8px")) ?>
                </span>
                <span style='border: 1px solid; margin: 0px 5px; padding: 5px;'>
                    <label for="combine"><?= __("label_combine", "Combine") ?></label>
                    <?= form_checkbox(array("name" => "combine", "id" => "combine", "value" => 1, "checked" => false)) ?>
                </span>       
                <label for="recurring_type_id"><?= __("label_recurrs", "Recurrs") ?></label>
                <?= form_dropdown("recurring_type_id", $recurring_types, null, 'id="recurring_type_id"') ?>
            </div>
            <input type="submit" value="<?= __("create", "Create") ?>" class="button orange">
        </fieldset>
    <?= form_close() ?>

<h2><?= __("manage", "Manage Coupons") ?></h2>
<div id="coupon_tabs" style="width: 100%;">
    <ul> 
        <li> <a href="#active_coupons_tab"><?= __("active_coupons", "Active Coupons") ?></a></li>
        <li> <a href="#inactive_coupons_tab"><?= __("inactive_coupons", "Inactive Coupons") ?></a></li>
    </ul>

    <div id="active_coupons_tab">
        <table id="active_coupons">
            <thead>
                <tr> 
                    <th><?= __("prefix", "Prefix") ?></th>
                    <th><?= __("code", "Code") ?>  </th>
                    <th><?= __("frequency", "Frequency") ?>  </th>
                    <th><?= __("description", "Description") ?></th>
                    <th><?= __("extended", "Extended Description") ?></th>
                    <th><?= __("start", "Start Date") ?></th>
                    <th><?= __("expires", "Expires") ?></th>
                    <th><?= __("amount", "Amount") ?>  </th>
                    <th><?= __("max_amount", "Max Amount") ?> </th>
                    <th><?= __("amount_type", "Amount Type") ?> </th>
                    <th><?= __("first_time", "1st time") ?></th>
                    <th><?= __("recurring", "Recurring") ?>  </th>
                    <th><?= __("created_by", "Created By") ?>  </th>
                    <th><?= __("combine", "Combine") ?>  </th>
                    <th><?= __("delete", "Delete") ?> </th>
                </tr>
            </thead>

            <tbody>
                <? foreach ($active_coupons as $coupon): ?>
                    <tr> 
                        <!-- This is the Prefix column -->
                        <td class="edit_field editable" data-property="prefix" id="<?= "prefix-{$coupon->couponID}"?>" data-id="<?=$coupon->couponID?>"> <?=$coupon->prefix?> </td>

                        
                        <!-- This is the Code column -->
                        <td class="edit_field editable" data-property="code" id="<?= "code-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>"> <?= $coupon->code ?> </td>
                            
                        <!-- This is the Frequency column -->
                        <td> 
                            <!-- Note, this hidden span is used to enable DataTable sorting on this column. -->
                            <span style="display: none;"> <?=$coupon->frequency?> </span> 
                            <?= form_dropdown("frequency", $frequencies, $coupon->frequency, "data-id='{$coupon->couponID}' id='frequency-{$coupon->couponID}' data-property='frequency' class='edit_select'") ?> 
                        </td>
                        
                        <!-- This is the Description column -->
                        <td class="edit_field editable" data-property="description" data-id="<?= $coupon->couponID ?>" id="<?= "description-{$coupon->couponID}"?>"> <?=$coupon->description ?></td>
                            
                        <!-- This is the extended description column -->
                        <td class="edit_field editable" data-property="extendedDesc" id="<?= "extendedDesc-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>" > <?=$coupon->extendedDesc?> </td>
                        
                        <!-- This is the Start Date column -->
                        <td class="edit_date editable" data-property="startDate" id="<?= "startDate-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>"> 
                            <?= convert_from_gmt_aprax($coupon->startDate, $dateFormat, $this->business_id); ?>
                        </td>
                        
                        <!-- This is the Expires/End Date column -->
                        <? if (empty($coupon->endDate) or $coupon->endDate == '0000-00-00 00:00:00'): ?>
                            <td class="edit_date editable" data-property="endDate" id="<?= "endDate-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID?>" style="font-weight: bold"> NEVER </td>
                        <? else: ?>
                            <? 
                                $today = new DateTime("now", new DateTimeZone($business->timezone));
                                $endDate = new DateTime($coupon->endDate, new DateTimeZone($business->timezone));
                                $is_expired = $endDate < new DateTime() ? "expired" : "";
                            ?>

                            <td class="edit_date editable <?= $is_expired ?>" data-property="endDate" id="<?="endDate-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>" > 
                                <?= convert_from_gmt_aprax($coupon->endDate, $dateFormat, $this->business_id) ?> 
                            </td>
                        <? endif; ?>
                        <!-- This is the Amount  column -->
                        <td class="edit_field editable" data-property="amount" id="<?= "amount-{$coupon->couponID}"?>" data-id="<?=$coupon->couponID?>">
                            <?=number_format_locale($coupon->amount, get_business_meta($this->business_id, 'decimal_positions', 2)); ?>
                        </td>
                        <!-- This is the Max Amount column -->
                        <td class="edit_field editable" data-property="maxAmt" id="maxAmt-<?=$coupon->couponID?>" data-id="<?=$coupon->couponID?>"> 
                            <?=number_format_locale($coupon->maxAmt, get_business_meta($this->business_id, 'decimal_positions', 2)); ?>
                        </td>
                        <!-- This is the Amount Type column -->
                        <td> 
                            <!-- Note, this hidden span is used for DataTable sorting. -->
                            <span style="display: none"> <?= $coupon->amountType ?> </span>
                            <?
                                if (substr($coupon->amountType, 0, 6) == 'item[%') {
                                    $amountType = "itemPercent";
                                } elseif (substr($coupon->amountType, 0, 5) == 'item[') {
                                    $amountType = "item";
                                } else {
                                    $amountType = $coupon->amountType;
                                }
                            ?>
                            <?= form_dropdown("amountType", $amountTypes, $amountType, "id='amountType-{$coupon->couponID}' data-id='{$coupon->couponID}' data-property='amountType' class='edit_select'") ?>
                        </td>
                        <!-- This is the 1st Time Column -->
                        <td>
                            <!-- Note, this hidden span is used for DataTable sorting. -->
                            <span style='display:none;'><?= $first_time_fields[$coupon->firstTime] ?></span>
                            <?= form_dropdown("firstTime", $first_time_fields, $coupon->firstTime, "id='firstTime-{$coupon->couponID}' data-id='{$coupon->couponID}' data-property='firstTime' class='edit_select'") ?>
                        </td>
                        <!-- This is the recurring column -->
                        <td> 
                            <?= form_dropdown("recurring_type_id", $recurring_types, $coupon->recurring_type_id, "id='recurring_type_id-{$coupon->couponID}' data-id='{$coupon->couponID}' data-property='recurring_type_id' class='edit_select'") ?>          
                        </td>
                        <td>
                            <?= getPersonName($coupon) ?>
                        </td>
                        <!-- This is the combine column -->
                        <td>
                            <?= form_checkbox(array("name" => "combine", "class" => "edit_checkbox", "value" => 1, "id" => "firstTime-{$coupon->couponID}", "data-id" => $coupon->couponID,  "checked" => $coupon->combine, "data-property" => "combine")) ?>
                        </td>
                        <!-- This is the delete column -->
                        <td>
                            <a class="delete" href="/admin/coupons/delete?couponID=<?=$coupon->couponID?>"> <img alt="Delete Coupon" src="/images/icons/cross.png" /> </a>
                        </td>
                    </tr>	
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div id="inactive_coupons_tab">
        <table id="inactive_coupons">
            <thead>
                <tr>
                     <th><?= __("prefix", "Prefix") ?></th>
                    <th><?= __("code", "Code") ?>  </th>
                    <th><?= __("frequency", "Frequency") ?>  </th>
                    <th><?= __("description", "Description") ?></th>
                    <th><?= __("extended", "Extended Description") ?></th>
                    <th><?= __("start", "Start Date") ?></th>
                    <th><?= __("expires", "Expires") ?></th>
                    <th><?= __("amount", "Amount") ?>  </th>
                    <th><?= __("amount_type", "Amount Type") ?> </th>
                    <th><?= __("first_time", "1st time") ?>
                    <th><?= __("created_by", "Created By") ?>  </th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($inactive_coupons as $coupon): ?>
                    <tr> 
                        <td class="edit_field editable" data-property="prefix" id="<?= "prefix-{$coupon->couponID}"?>" data-id="<?=$coupon->couponID?>"> <?=$coupon->prefix?> </td>
                        <td class="edit_field editable" data-property="code" id="<?= "code-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>"> <?= $coupon->code ?> </td>
                        <td> 
                            <!-- Note, this hidden span is used to enable DataTable sorting on this column. -->
                            <span style="display: none;"> <?=$coupon->frequency?> </span> 
                            <?= form_dropdown("frequency", $frequencies, $coupon->frequency, "data-id='{$coupon->couponID}' id='frequency-{$coupon->couponID}' data-property='frequency' class='edit_select'") ?> 
                        </td>
                        <td class="edit_field editable" data-property="description" data-id="<?= $coupon->couponID ?>" id="<?= "description-{$coupon->couponID}"?>"> <?=$coupon->description ?> </td>
                        <td class="edit_field editable" data-property="extendedDesc" id="<?= "extendedDesc-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>" > <?=$coupon->extendedDesc?> </td>

                        <td class="edit_date editable" data-property="startDate" id="<?= "startDate-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>"> 
                                <?= convert_from_gmt_aprax($coupon->startDate, "m/d/y") ?>
                        </td>
                        <?php if (empty($coupon->endDate) or $coupon->endDate == '0000-00-00 00:00:00'): ?>
                            <td class="edit_date editable" data-property="endDate" id="<?= "endDate-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID?>" style="font-weight: bold"> NEVER </td>

                        <?php else: ?>
                            <?
                                $endDate = new DateTime($coupon->endDate, new DateTimeZone("GMT"));
                                $is_expired = $endDate < new DateTime("GMT") ? "expired" : "";
                            ?>

                            <td class="edit_date editable <?= $is_expired ?>" data-property="endDate" id="<?="endDate-{$coupon->couponID}"?>" data-id="<?= $coupon->couponID ?>" > 
                                <?= convert_from_gmt_aprax($coupon->endDate, "m/d/y") ?> 
                            </td>
                        <?php endif; ?>

                        <td class="edit_field editable" data-property="amount" id="<?= "amount-{$coupon->couponID}"?>" data-id="<?=$coupon->couponID?>"> <?= number_format_locale($coupon->amount, 2); ?>   </td>
                        <td> 
                            <!-- Note, this hidden span is used for DataTable sorting. -->
                            <span style="display: none"> <?=$coupon->amountType?> </span> 
                            <?
                                if (substr($coupon->amountType, 0, 6) == 'item[%') {
                                    $amountType = "itemPercent";
                                } elseif (substr($coupon->amountType, 0, 5) == 'item[') {
                                    $amountType = "item";
                                } else {
                                    $amountType = $coupon->amountType;
                                }
                            ?>
                            <?= form_dropdown("amountType", $amountTypes, $coupon->amountType, "id='amountType-{$coupon->couponID}' data-id='{$coupon->couponID}' data-property='amountType' class='edit_select'") ?> 
                        </td>
                        <td>
                            <!-- Note, this hidden span is used for DataTable sorting. -->
                            <span style='display:none;'><?= $first_time_fields[$coupon->firstTime] ?></span>
                            <?= form_dropdown("firstTime", $first_time_fields, $coupon->firstTime, "id='firstTime-{$coupon->couponID}' data-id='{$coupon->couponID}' data-property='firstTime' class='edit_select'") ?>
                        </td>
                        <td>
                            <?= getPersonName($coupon) ?>
                        </td>
                    </tr>	
                <? endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
var products = <?= json_encode($products)?>;

<?php if (isset($initialSearch)): ?>
    var initialSearch = "<?= $initialSearch?>";
<?php else: ?>
    var initialSearch = ""; 
<?php endif; ?>

$(".modify_business_coupon_associations").each(function() {
    var couponID = $(this).attr("data-couponID");
    $("div." + couponID).dialog({
        autoOpen: false,
        title: "Modify Business Associations",
        modal: true,
        buttons: { "Ok" : function() {
            $(this).dialog("close");
        }}

    });        
})
$(".modify_business_coupon_associations").click(function() {
    var couponID = $(this).attr("data-couponID");
    $("div."+couponID).dialog("open");
});

$('#amountType').change(function(){
    $('#product').html('');

    $('#process_id').val(0);
    $('#process_id_wrapper').css('display', 'none');
                
    if($('#amountType').val() == 'dollars') {
        $('#maxAmoutSpan').css('display', 'none');
        $('#maxAmt').val('');
    } 
    else {
        $('#maxAmoutSpan').css('display', 'inline');
    }


    if($(this).val().indexOf('item') >= 0){
        //create another row for product
        var select = $('<select name="product_id"><option value="">-- SELECT --</option></select>');
        select.append("<option value='dc'>All Dry Cleaning</option>");
        $.each(products, function(i,item){
            select.append("<option value='"+item.productID+"'>"+item.displayName+"</option>");
        });
        var label = $('<label>Select Product:</label>');
        $('#product').append(label);
        $('#product').append(select);
        
        select.change(function(){
            if( $(this).val() == 'dc' ){
                $('#process_id_wrapper').css('display', 'inline');
            }else{
                $('#process_id').val(0);
                $('#process_id_wrapper').css('display', 'none');
            }
        });
    }
});



$(".delete").click(function() {
    if (confirm("Are you sure you want to delete this coupon?")) {
        return true;
    }
    else {
        return false;
    }
});

$("#add_coupon").validate({
    rules: {
        endDate : {
            required: true
        },
        startDate: {
            required: true
        },
        amount: {
            required: true,
            number: true
        }
    },
    errorClass: "invalid",
    invalidHandler: function(form, validator) {
        $("#add_coupon").find(":submit").loading_indicator("destroy");
    }
});   

$(".date_field").datepicker( {
   "dateFormat" : "mm/dd/y", 
   constrainInput : false
});

$("input[type='submit']").loading_indicator();


active_coupon_table = $("#active_coupons").dataTable({
        "oSearch" : {'sSearch' : initialSearch},
        "iDisplayLength" : 25,
        "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>'
    }
);       


//The following event handler is used to enablethe user to update a datefield in the active coupon datatable.
$(".edit_date", active_coupon_table.fnGetNodes()).editable("/admin/coupons/update_property", {
    "width" : "100%",
    "height" : "100%",
    "id" : "couponID",
    "type" : "datepicker",
    "data"    : function(string) {return $.trim(string)},
    "indicator" : "Saving... <img src='/images/progress.gif' />",
    "callback": function(sValue, y) {
        var response = $.parseJSON(sValue);
        if (response.status === "success") {
            var value = response.data.value;
            var aPos = active_coupon_table.fnGetPosition(this);
            active_coupon_table.fnUpdate(value, aPos[0], aPos[1]);
        } else {
            $(this).text("");
            alert(response.message);
        }
    },
    "submitdata": function ( value, settings ) {
        return {
            "property" : $(this).attr("data-property"),
            "couponID": $(this).attr("data-id")
        }
    },
    "dateFormat" : "mm/dd/y",
    constrainInput: false
});

//The following even thanlder is used to enable the user to update a data field for a parituclar coupo in the active coupon datatable.
$(".edit_field", active_coupon_table.fnGetNodes()).editable("/admin/coupons/update_property", {
        "width" : "100%",
        "height" : "100%",
    "id" : "identifer",
    "cancel" : "<img src='/images/icons/delete.png' />",
    "submit" : "<img src='/images/icons/accept.png' />",
    "indicator" : "Saving ... <img src='/images/progress.gif' />",
    "data"    : function(string) {return $.trim(string)},
    "callback": function(sValue, y) {
        var response = $.parseJSON(sValue);
        if (response.status === "success") {
            var value = response.data.value;
            var aPos = active_coupon_table.fnGetPosition( this );
            active_coupon_table.fnUpdate( value, aPos[0], aPos[1] )
        } else {
            $(this).text("");
            alert(response.message);
        }
    },
    "submitdata": function ( value, settings ) {
        return {
            "property" : $(this).attr("data-property"),
            "couponID": $(this).attr("data-id")
        }
    }
});

inactive_coupon_table = $("#inactive_coupons").dataTable( {
        "iDisplayLength" : 25,
        "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
            <?php endif ?>                
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    }
);       

// The following event handler enables the user to update a date field in the inactive coupon datatable.
$(".edit_date", inactive_coupon_table.fnGetNodes()).editable("/admin/coupons/update_property", {
    "width" : "100%",
    "height" : "100%",
    "id" : "couponID",
    "type" : "datepicker",
    "data"    : function(string) {return $.trim(string)},
    "indicator" : "Saving... <img src='/images/progress.gif' />",
    "callback": function(sValue, y) {
        var response = $.parseJSON(sValue);
        if (response.status === "success") {
            var value = response.data.value;
            var aPos = inactive_coupon_table.fnGetPosition( this );
            inactive_coupon_table.fnUpdate(value, aPos[0], aPos[1]);
        } else {
            $(this).text("");
            alert(response.message);
        }
    },
    "submitdata": function ( value, settings ) {
        return {
                "property" : $(this).attr("data-property"),
                "couponID": $(this).attr("data-id")
        }
    },
    "dateFormat" : "mm/dd/y",
    constrainInput: false
});

//The following event handler enables the user toupdate a data field in the ianctive coupon datatable.
$(".edit_field", inactive_coupon_table.fnGetNodes()).editable("/admin/coupons/update_property", {
    "width" : "100%",
    "height" : "100%",
    "id" : "identifer",
    "cancel" : "<img src='/images/icons/delete.png' />",
    "submit" : "<img src='/images/icons/accept.png' />",
    "indicator" : "Saving ... <img src='/images/progress.gif' />",
    "data"    : function(string) {return $.trim(string)},

    "callback": function(sValue, y) {
        var response = $.parseJSON(sValue);
        if (response.status === "success") {
            var value = response.data.value;
            var aPos = inactive_coupon_table.fnGetPosition(this);
            inactive_coupon_table.fnUpdate(value, aPos[0], aPos[1] );
        } else {
            $(this).text("");
            alert(response.message);
        }
    },
    "submitdata": function ( value, settings ) {
        return {
            "property" : $(this).attr("data-property"),
            "couponID": $(this).attr("data-id")
        }
    }
});


$(document).on("change", ".edit_checkbox", function () {
    var $loading = $("<img src='/images/progress.gif' />");
    $(this).after($loading);

    var checked;
    if ($(this).is(":checked")) {
        checked = 1;
    }
    else {
        checked = 0;
    }        

    $.post("/admin/coupons/update_property", {
        couponID : $(this).attr("data-id"),
        property : $(this).attr("data-property"),
        value: checked
    }, function(response) {
        if (response['status'] !== "success") {
            alert(response['message']);
        }
        $loading.remove();
    }).fail(function() { alert("Internal Server Error");});
});

// The following even thandler enables the user to update the coupons' properties that are displayed as a 'select' HTML element.'
$(document).on("change", ".edit_select", function() {
    var $loading = $("<img src='/images/progress.gif' />");

    $this = $(this);
    $(this).after($loading);

    $.post("/admin/coupons/update_property", {
        "couponID" : $(this).attr("data-id"),
        "property" : $(this).attr("data-property"),
        "method" : "ajax",
        "value" : $(this).val()
    }, function(response) {
        if (response['status'] !== "success") {
            alert(response['message']);
        }
        $loading.remove();
    }, "JSON").fail(function() { alert("Inernal Server Error");});
});

$("#coupon_tabs").tabs({fx: { opacity: 'toggle' }});            

</script>
