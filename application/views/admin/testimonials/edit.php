<?php enter_translation_domain("admin/testimonials/edit"); ?>
<h2><?php echo __("Update Testimonial", "Update Testimonial"); ?></h2>
<form method='post' action=''>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Customer Name", "Customer Name"); ?></th>
            <td><input type='text' name='customerName' value="<?= $testimonial->customerName?>" /></td>
        </tr>
         <tr>
            <th><?php echo __("Site Link", "Site Link"); ?></th>
            <td><input type='text' name='siteLink' value="<?= $testimonial->siteLink?>" /></td>
        </tr>
         <tr>
            <th><?php echo __("Testimonial", "Testimonial"); ?></th>
            <td><textarea name='testimonial' style='width:99%; height:200px;'><?= $testimonial->testimonial?></textarea></td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Update Testimonial", "Update Testimonial"); ?>' class='button orange' />
</form>


