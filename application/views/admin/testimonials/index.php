<?php enter_translation_domain("admin/testimonials/index"); ?>
<h2><?php echo __("Testimonials", "Testimonials"); ?></h2>
<p>
<a href='/admin/testimonials/add' class='button orange'><?php echo __("Add Testimonial", "Add Testimonial"); ?></a>
</p> 
<? if($testimonials): foreach($testimonials as $testimonial): ?>
    <div style='padding:0px; margin:20px 0px; border:1px solid #cccccc; border-radius:5px;'>
        <div style='float:left; width:10%;padding:5px;'><?= $testimonial->customerName?></div>
        <div style='float:left; width:87%;padding:5px;'><?= substr($testimonial->testimonial, 0, 500)?></div>
        <div style='clear:left'></div>
        <div style='padding:0px; background-color:#eaeaea'>
            <div style='padding:5px;'>
                <a href='<?= $testimonial->siteLink?>'><?= substr($testimonial->siteLink, 0, 500)?></a>
                <div style='float:right'>
                    <a href='/admin/testimonials/edit/<?= $testimonial->testimonialID?>'><?php echo __("Edit", "Edit"); ?></a> | 
                    <a style='color:#cc0000;' onClick='return verify_delete()' href='/admin/testimonials/delete/<?= $testimonial->testimonialID?>'><?php echo __("Delete", "Delete"); ?></a>
                </div>
            </div>
        </div>
    </div>
<? endforeach; endif;?>


