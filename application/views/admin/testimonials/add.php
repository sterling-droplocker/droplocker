<?php enter_translation_domain("admin/testimonials/add"); ?>
<h2><?php echo __("Add Testimonial", "Add Testimonial"); ?></h2>
<form method='post' action=''>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Customer Name", "Customer Name"); ?></th>
            <td><input type='text' name='customerName' value="<?= isset($customerName)?$customerName:''?>" /></td>
        </tr>
         <tr>
            <th><?php echo __("Site Link", "Site Link"); ?></th>
            <td><input type='text' name='siteLink' value="<?= isset($siteLink)?$siteLink:''?>" /></td>
        </tr>
         <tr>
            <th><?php echo __("Testimonial", "Testimonial"); ?></th>
            <td><textarea name='testimonial' style='width:99%; height:200px;'><?= isset($testimonial)?$testimonial:''?></textarea></td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Add Testimonial", "Add Testimonial"); ?>' class='button orange' />
</form>


