<?php enter_translation_domain("admin/error"); ?>
<?php

if (!isset($class)) {
    $class = 'error';
}

if (!isset($main_title)) {
    $main_title = 'Error';
}

?>
<h2><?= $main_title ?></h2>

<div class="<?= $class ?>" style="padding: 20px; margin:10px; border: 1px solid #000;">
    <h1><?= $title ?></h1>
    <? if (!empty($message)): ?>
    <p><?= $message ?></p>
    <? endif; ?>
</div>
