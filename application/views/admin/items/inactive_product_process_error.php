<?php enter_translation_domain("admin/items/inactive_product_process_error"); ?>
<script type="text/javascript">
/*
$(document).ready(function() {
    $(":submit").loading_indicator();
});
*/
</script>

<h1> <?php echo __("This item has an inactive product process. Please select a new product process for this item", "This item has an inactive product process. Please select a new product process for this item."); ?></h1>

<?= form_open("/admin/orders/update_product_process_and_add_item_to_order") ?>
<?= form_dropdown("product_process_id", $product_processes) ?>
<?= form_hidden("orderID", $orderID) ?>
<?= form_hidden("itemID", $itemID) ?>
<?= form_hidden("barcode", $barcode) ?>
<?= form_submit(array("value" => __("Add Item to Order", "Add Item to Order"), "class" => "button orange")) ?>
<?= form_close() ?>

