<?php enter_translation_domain("admin/items/view"); ?>
<?php
/**
 * Expects the following content variables:
 *  @param product stdClass The product associated with the item
 *  @param barcode stdClass The barcode associated with the item
 *  @param process stdClass The process associated with the item
 *  @param itemHistory array The history of the item
 *  @param pictures array  All the picutres associated with the item 
 *  @param itemIssues array All the issues associated with the item
 *  customer_item
 * 
 *  @param specifications array
 *  @param upcharges array
 */

if (!isset($product)) {
    trigger_error("".__("product must be set as a content variable", "'product' must be set as a content variable")."", E_USER_WARNING);
}
if (!isset($barcode)) {
    trigger_error("".__("barcode must be set as a content variable", "'barcode' must be set as a content variable")."", E_USER_WARNING);
}
if (!isset($process)) {
    trigger_error("".__("process must be set as a content variable", "'process' must be set as a content variable")."", E_USER_WARNING);
}
if (!is_array($item_histories)) {
    trigger_error("".__("item_histories must be an array", "'item_histories' must be an array")."", E_USER_WARNING);
}
if (!is_array($pictures)) {
    trigger_error("".__("pictures must be an array", "'pictures' must be an array")."", E_USER_WARNING);
}
if (!is_array($itemIssues)) {
    trigger_error("".__("itemIssues must be an array", "'itemIssues' must be an array")."", E_USER_WARNING);
}
if (!is_array($specifications)) {
    trigger_error("".__("specifications must be an array", "'specifications' must be an array")."", E_USER_WARNING);
}
if (!is_array($upcharges)) {
    trigger_error("".__("upcharges must be an array", "'upcharges' must be an array")."", E_USER_WARNING);
}
?>
<?php

$fieldList = array();
$fieldList['address1'] = "<th> ". __("Address", "Address") . "</th>";
$fieldList['city'] = "<th> ". __("City", "City") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['address1'] = "<td>{$location->address1}</td>";
    $fieldList['city'] = "<td>{$location->city}</td>";

    return getSortedAddressForm($fieldList);
}
        
?>
<script type="text/javascript">

if (window.jQuery) {
        $(document).ready(function() {
            $("#delete_picture").loading_indicator({
                "confirm_message" : "<?php echo __("Are you sure you want to delete this item's picture", "Are you sure you want to delete this item's picture?"); ?>"
            });    
        });
    }


</script>

<style type="text/css">
    fieldset {
        border: 1px solid;
        margin: 10px 0px;
        padding: 10px;
    }
    #box-table-a td {
        padding: 2px;
    }
</style>
<h2><?php echo __("Item Details", "Item Details"); ?> - <?= $barcode->barcode?></h2>

<!-- Note, the use of the width is a workaround for older Internet Explorer versions. -->
<div style="display: table;">
    <!-- Note, the use of 'vertical-align: top' is a workaround for a bug in Firefox. -->
    <div style="display: table-cell; vertical-align: top;">
        <table class="detail_table">
            <tr>
                <th style="width:35%"><?php echo __("Product", "Product"); ?></th>
                <td style="width:55%" width='*'><?= $product->name?></td>
            </tr>
            <tr>
                <th><?php echo __("Cleaning Method", "Cleaning Method"); ?></th>
                <td><?= $process->name ?></td>
            </tr>
            <tr>
                <th><?php echo __("Special", "Special"); ?></th>
                <td><? foreach ($specifications as $specification): ?>
                    <?= $specification->description ?> (<?= $specification->everyOrder == 1 ?__("Every Order", "Every Order"):__("This Order", "This Order"); ?>)<br>
                    <? endforeach; ?>
                </td>
            </tr>
            <? foreach($item_preferences as $name => $value): ?>
            <tr>
                <th><?php echo __($name, $name); ?></th>
                <td>
                    <?= $value ?>
                    <? if (!empty($changes[$name])): ?>
                        <div style='color: red; padding: 10px;'><?= $changes[$name] ?></div>
                    <? endif; ?>
                </td>
            </tr>
            <? endforeach; ?>
            <tr>
                <th> <?php echo __("Upcharges", "Upcharges"); ?> </th>
                <td> 
                    <?php 
                    if (empty($upcharges)) {
                        echo __("None", "None");
                    }
                    else {
                        foreach ($upcharges as $upcharge) {
                            echo __($upcharge->name, $upcharge->name);
                        }
                    }
                ?>
                </td>
            </tr>

            <tr>
                <th><?php echo __("Item Notes", "Item Notes"); ?></th>
                <td><?= $item->note?></td>
            </tr>
        </table>
		<p>* <?php echo __("Using customers preferences", "Using customer's preferences"); ?></p>
        <br />
            <?php if (is_numeric($order_id)): ?>
                <a href='/admin/orders/edit_item?itemID=<?= $item->itemID?>&orderID=<?= $order_id ?>' class='button blue'> <?php echo __("Edit Item", "Edit Item"); ?> </a>
            <?php else: ?>
                <a href='/admin/orders/edit_item?itemID=<?= $item->itemID?>' class='button blue'> <?php echo __("Edit Item", "Edit Item"); ?> </a>
            <?php endif ?>
        <br /><br />

        <h2><?php echo __("Item History", "Item History"); ?></h2>
        <table class="box-table-a">
            <thead>
                <tr>
                    <th><?php echo __("Date", "Date"); ?></th>
                    <th><?php echo __("Action", "Action"); ?></th>
                    <th><?php echo __("Employee", "Employee"); ?></th>
                </tr>
            </thead>
            <tbody>
                <? if(empty($item_histories)): ?>
                    <tr>
                        <td colspan="3"><?php echo __("No History", "No History"); ?></td>
                    </tr>

                <? else : ?>
                    <? foreach ($item_histories as $item_history): ?>
                        <tr>
                            <td><?= convert_from_gmt_aprax($item_history->date, STANDARD_DATE_FORMAT)?></td>
                            <td>
<?php if ($item_history->order_id): ?>
                                <?php echo __("Added to order", "Added to order"); ?> <a href="/admin/orders/details/<?= (string) $item_history->order_id; ?>" target="_blank"><?= (string) $item_history->order_id; ?></a>
<?php else: ?>
                                <?= $item_history->description; ?>
<?php endif; ?>
                            </td>
                            <td> <?= $item_history->creator ?>
                        </tr>
                    <? endforeach; ?>
                <? endif; ?>
            </tbody>
        </table>
</div>
<div style="padding-left: 5px; display: table-cell;">
    <div style='padding:5px;'>
        <?php 
            $last_picture = end($pictures);

            if (!empty($last_picture)) {
                $file = $last_picture->file;
                echo "<a target='_blank' href='https://s3.amazonaws.com/LaundryLocker/$file'><img style='width:400px' src='https://s3.amazonaws.com/LaundryLocker/$file' alt='".__("Current Picture", "Current Picture")."' /></a> 
                <p style='text-align: center;'> <a id='delete_picture' href='/admin/picture/delete?pictureID={$last_picture->pictureID}' class='button blue'> ".__("Delete Picture", "Delete Picture")." </a> </p>";
            }
            else {
                echo "<div style='padding: 40px 20px; font-size: 37pt; color: white; background-color: red'> ".__("NO PICTURE", "NO PICTURE")." </div>";
            }  
            //THe following conditional will output the previous taken picture if it exists
            if (count($pictures) > 1) {
                $second_to_last_picture = prev($pictures);
                echo "<a target='_blank' href='https://s3.amazonaws.com/LaundryLocker/{$second_to_last_picture->file}'> <img style='width:400px' alt='".__("Previous Picture", "Previous Picture")."' src='https://s3.amazonaws.com/LaundryLocker/{$second_to_last_picture->file}' /></a> 
                    <p style='text-align: center;'> <a id='delete_picture' href='/admin/picture/delete?pictureID={$second_to_last_picture->pictureID}' class='button blue'> ".__("Delete Picture", "Delete Picture")." </a> </p>";
            }
        ?>

        <? if(!empty($itemIssues)): ?>
            <br />
            <br />
            <h3><span style='color:#cc0000;'><?php echo __("Active Item Issues", "Active Item Issues"); ?></span></h3>
            <div style='border:1px solid #cc0000;background-color: #ffafa3; padding:5px;'>
                <table>
                    <? foreach($itemIssues as $issue): ?>
                        <tr>
                            <td><?= $issue->issueType?>: <?= $issue->frontBack?></td>
                            <td><?= $issue->note?></td>
                        </tr>
                    <? endforeach; ?>
                </table>
            </div>
        <? endif; ?>
    </div>
    <h2> <?php echo __("Customers", "Customers"); ?> </h2>
    <table class="box-table-a">
        <thead>
            <tr>
                <th> <?php echo __("Name", "Name"); ?> </th>
                <?= $addressTableHeader ?>
                <th> <?php echo __("Phone", "Phone"); ?> </th>
                <th> <?php echo __("Email", "Email"); ?> </th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($customer_items)): ?>
                <?php foreach ($customer_items as $customer_item): ?>
                    <tr>
                        <td> <a href='/admin/customers/detail/<?= $customer_item->customer_id?>'> <?= getPersonName($customer_item) ?></a> </td>
                        <?= getAddressRow($customer_item) ?>
                        <td> <?= $customer_item->phone ?> </td>
                        <td> <?= $customer_item->email ?> </td>
                    </tr>
                <?php endforeach; ?>               
            <?php endif; ?>
        </tbody>
    </table>

    </div>
</div>
