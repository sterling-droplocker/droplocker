<?php enter_translation_domain("admin/items/add_issue"); ?>
<?
    //if the image doesn't come through, show the default [ blank ] images.
    $image_fallback = '';
    
    //see if we can access the image, if no good use the fallback below
    if( empty( $item->file ) ){
        switch( $item->displayName ){
            case 'Laundered Shirt':
                    $image_fallback = '/images/item_launderedShirt.jpg';
                    break;
            case 'Blouse, Sweater, Shirt':
                    $image_fallback =  '/images/item_launderedShirt.jpg';
                    break;
            case 'blazer':
                    $image_fallback =  '/images/item_blazer.jpg';
                    break;
            case 'polo':
                    $image_fallback =  '/images/item_polo.jpg';
                    break;
            default:
                    $image_fallback =  '/images/item_onePiece.jpg';
                    break;
        }   
    }else{
        $image_fallback = 'https://s3.amazonaws.com/LaundryLocker/' .  $item->file;
    }    
?>

<style type="text/css">
    .headingGreen{color:#6D9E2C;font-size:18px;text-decoration:none;}
    .headingBlue{color:#266CAC;font-size:16px;font-weight:700;line-height:25px;margin:0 auto;}
    .item_detail_table th {
        vertical-align: text-top;
        text-align: left;
        border: none;
    }
    .item_detail_table td
    {
        text-align: right;
        padding: 10px;
    }

</style>
<script>
function validateType(){
    var $radios = $('input:radio[name=issueType]');
    if($radios.is(':checked') === false) {
        alert('<?php echo __("Please select a type", "Please select a type"); ?>');
        return false;
    } else {
        return true;
    }
    
}
</script>
<h2>Item Issues</h2>
<div style='float:left;'>
    <p><a href='/admin/orders/edit_item?orderID=<?=$order_id?>&itemID=<?= $item->itemID?>'><img src="/images/icons/arrow_left.png"> <?php echo __("Return to Item Details", "Return to Item Details"); ?></a></p>	
    <br>
    <? if($itemIssueID):?>
    <p><a href='/admin/items/add_issue/?item_id=<?= $item->itemID?>&order_id=<?php echo $order_id ?>'><?php echo __("Add More Notes", "Add More Notes"); ?></a></p>
    <? endif; ?>
    
    <div style="width:auto;">
        <?= $this->session->flashdata('alert');?>
        <h3><?php echo __("Click on the location of the image", "Click on the location of the image"); ?>
        <br><?php echo __("below to add a note to the item", "below to add a note to the item."); ?></h3>
        <input type='hidden' name='itemIssueID' value='<?= $itemIssueID?>' />
        <div>
            <br>
            <a class='' href='<?= $image_fallback?>'>
                <img style="max-width:300px;" id="issue_image" src='<?= $image_fallback?>' />
                <span class="issue_marker" data-original-title="Item note"></span>
            </a>
        </div>
    </div>
</div>

<div style='float:left; padding-left:40px; padding-top:40px;'>
    <div style='padding:5px;'>

        <table class="item_detail_table" style="border-collapse: collapse;">
            <tr>
                <th class="headingBlue"><?php echo __("Barcode", "Barcode"); ?></th>
                <td class="headingGreen"><?= $item->barcode?></td>
            </tr>
            <tr>
                <th class="headingBlue"><?php echo __("Product", "Product"); ?></th>
                <td class="headingGreen"><?= $item->displayName?></td>
            </tr>
            <tr>
                <th class="headingBlue"><?php echo __("Cleaning Method", "Cleaning Method"); ?></th>
                <td class="headingGreen"><?= $item->processName?></td>
            </tr>
            <tr>
                <td colspan="2"> <hr /> </td>
            </tr>
            <tr>
                <th class="headingBlue"  style="border: 0px outset gray; border-width: 0px 0px 1px 0px; "><table cellspacing="0" cellpadding="0"><tr><td><?php echo __("Open Issues", "Open Issues"); ?></td></tr></table></th>
                <td style="border: 0px outset gray; border-width: 0px 0px 1px 1px; vertical-align: top;">
                    <? if(isset($open)): ?>
                    <table>
                        <? foreach($open as $o): ?>
                            <tr>
                                <td align="left"><?= $o->issueType?>: <?= $o->frontBack?></td>
                                <td><?= $o->note?></td>
                                <td><a id="delete-<?= $o->itemIssueID?>" href='/admin/items/delete_issue/?item_id=<?= $item->itemID?>&item_issue_id=<?= $o->itemIssueID?>&order_id=<?php echo $order_id ?>'><img src='/images/icons/cross.png' /></a>
                                    <a id="modify-<?= $o->itemIssueID?>" href='/admin/items/add_issue/?item_id=<?= $item->itemID?>&item_issue_id=<?= $o->itemIssueID?>&order_id=<?php echo $order_id ?>'><img src='/images/icons/magnifier.png' /></a>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    </table>
                    <? endif; ?>
                </td>
            </tr>
            <tr >
                <th class="headingBlue" style="border: 0px outset gray; border-width: 0px 0px 0px 0px; "><table cellspacing="0" cellpadding="0"><tr><td><?php echo __("Past Issues", "Past Issues"); ?></td></tr></table></th>
                <td style="border: 0px outset gray; border-width: 1px 0px 0px 1px; ">
                    <? if(isset($closed)): ?>
                    <table>
                        <? foreach($closed as $o): ?>
                            <tr>
                                <td valign="top"><?= $o->issueType?>: <?= $o->frontBack?></td>
                                <td><?= $o->note?></td>
                                <td>
                                    <a href='/admin/items/delete_issue/?item_id=<?= $item->itemID?>&item_issue_id=<?= $o->itemIssueID?>&order_id=<?php echo $order_id ?>'><img src='/images/icons/cross.png' /></a>
                                    <a href='/admin/items/add_issue/?item_id=<?= $item->itemID?>&item_issue_id=<?= $o->itemIssueID?>&order_id=<?php echo $order_id ?>'><img src='/images/icons/magnifier.png' /></a>
                                </td>
                            </tr>
                        <? endforeach; ?>
                    </table>
                    <? endif; ?>
                </td>
            </tr>
        </table>
    </div>
</div>


<div id="note_popover" >
    <div class="" style="text-align:left;">
        <form method='post' action='/admin/items/edit_issue/?item_id=<?= $item->itemID?>&order_id=<?php echo $order_id ?>'>
            <input type='hidden' name='itemIssueID' value='<?= $itemIssueID?>' />
            <div style="margin-top:10px;"><?php echo __("Front or Back", "Front or Back"); ?> 
                <select id='frontBack' name='frontBack'>
                    <option value='front'><?php echo __("Front", "Front"); ?></option>
                    <option value='back'><?php echo __("Back", "Back"); ?></option>
                </select>
                <? if($frontBack):?>
                <script>
                    $(document).ready(function(){
                            $("#frontBack").val('<?= $frontBack?>');
                    });
                </script>
                <? endif; ?>
            </div>

            <div style="margin-top:10px;">Issue: &nbsp;
                <input type="radio" name='issueType' value='Stain' /> <?php echo __("Stain", "Stain"); ?> 
                <input type="radio" name='issueType' value='Repair' /> <?php echo __("Repair", "Repair"); ?> 
                <input type="radio" name='issueType' value='Other' /> <?php echo __("Other", "Other"); ?> 
            </div>

            <script type='text/javascript'>
                $(document).ready(function(){
                        var $radios = $('input:radio[name=issueType]');
                            if($radios.is(':checked') === false) {
                                $radios.filter('[value=<?= $issueType?>]').attr('checked', true);
                            }
                });
            </script>

            <div style="margin-top:10px;">
                <?php echo __("Notes", "Notes:"); ?><br>
                <textarea name='note'><?= $note?></textarea>
            </div>

            <input type="hidden" name="x" value="<?=$x;?>" id="issue_x" />
            <input type="hidden" name="y" value="<?=$y;?>" id="issue_y" />
            <input type='submit' name='submit' value='Save' class='btn btn-primary'/>
            <a href="javascript:;" class="close_popover pull-right" style="margin:10px;"><?php echo __("Hide Window", "Hide Window"); ?></a>
        </form>  
    </div>
</div>

<script>
    var issueX = <?= !empty( $x ) ? $x : 'null'; ?>;
    var issueY = <?= !empty( $y ) ? $y : 'null'; ?>;
   
    $(function(){
        $('img#issue_image').bind('click',bindIssueImage);
        
        //popover
        $('#note_popover').dialog({
            autoOpen: false
        });
        
        <? if( !empty( $itemIssueID ) ){ ?>
        renderCrossHairs();
        <? } ?>
        
    });
    
    var closePopover = function(){
        $('#note_popover').dialog('close');
    };
    
    var renderCrossHairs = function(){
        if( issueX != null && issueY != null ){
            var hOffset = $('#issue_image').height();
            var wOffset = $('#issue_image').width();
            
            $('.issue_marker').css('visibility','visible');
            $('.issue_marker').css('top', ( (issueY - hOffset) - 24 ) );
            $('.issue_marker').css('left', ( issueX - 24 ) );   
            
            $('#note_popover').dialog('open');
        
            $('input#issue_x').val( issueX );
            $('input#issue_y').val( issueY );  
            
            $('.close_popover').unbind('click');
            $('.close_popover').bind('click',closePopover);
        }
        
    };
    
    var bindIssueImage = function(e){
            
            e.preventDefault();
            
            var newX  = (e.offsetX || e.clientX - $(e.target).offset().left + window.pageXOffset ); //(e.offsetX || e.clientX - $(e.target).offset().left);
            var newY  = (e.offsetY || e.clientY - $(e.target).offset().top + window.pageYOffset ); //(e.offsetY || e.clientY - $(e.target).offset().top);
            
            var hOffset = $('#issue_image').height();
            
            $('.issue_marker').css('visibility','visible');
            $('.issue_marker').css('top', ( (newY - hOffset) - 24 ) );
            $('.issue_marker').css('left', ( newX - 24 ) );   
            
            $('#note_popover').dialog('open');
            
            $('input#issue_x').val( newX );
            $('input#issue_y').val( newY );
           
            $('.close_popover').unbind('click');
            $('.close_popover').bind('click',closePopover);
    };
    
    $(window).resize(function(){
         $('img#issue_image').unbind('click');
         $('img#issue_image').bind('click',bindIssueImage);
    });
    
</script>

<style type="text/css">
    .issue_marker {
        width: 48px;
        height: 48px;
        visibility: hidden;
        display:block;
        position: relative;
        background: url(/images/target.png) no-repeat;
    }
    
    #note_popover{
/*        background-color: #fff;
        border: solid 1px #ececec;
        z-index:1000;
        position:absolute;*/
    }
</style>
