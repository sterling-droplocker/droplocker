<?php enter_translation_domain("admin/items/barcode_not_found"); ?>
<?php if ($this->agent->is_browser("Internet Explorer") && $this->agent->version < 9): ?>
<embed src="/sounds/buzzer.mp3" autostart="true" loop="false" hidden="true">
<?php else: ?>
    <audio autoplay>
        <source src="/sounds/buzzer.ogg" type="audio/ogg">
        <source src="/sounds/buzzer.mp3" type="audio/mp3">
    </audio>
<?php endif ?>
<div style="background-color: blue; padding: 30px 50px; height: 100%; ">
    <div style="font-size: 4em; color: red; text-decoration: underline; text-transform: uppercase;"> <?php echo __("Warning", "Warning"); ?> </div>

    <div style="color: white; font-size: 2em; margin-top: 80px;"><?php echo __("barcode_not_found", "Barcode <b> '%barcode%' </b> was not found in the system, which means that it is not associated with an order. Please give to customer service.", array("barcode"=>$barcode)); ?>  </div>
</div>
<?= $this->agent->version ?>




    
