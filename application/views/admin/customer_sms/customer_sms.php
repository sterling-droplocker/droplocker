<?php

enter_translation_domain("admin/customer_sms/customer_sms");

?>
<h2><?= __("customer_sms", "Customer Sms") ?></h2>
<?= $this->session->flashdata("static_message") ?>
<p style="padding-bottom: 16px;">
    <a href="/admin/customers/detail/<?= $customer->customerID ?>"><img src="/images/icons/arrow_left.png"> Customer details</a>
</p>
<h3 style="display: inline;"><?= getPersonName($customer) ?></h3>
<form id="customer_sms_add_form" action="/admin/customer_sms/send_customer_sms/<?= $customer->customerID;?>" method="post">
    <br>
    <table class="detail_table">
    <tr>
        <th><?= __("sms_text", "*SMS Text") ?></th>
        <td><textarea id='bodytext' name="smsText" cols="84" rows="4"></textarea></td>
    </tr>
    </table>
    <br>
    <p><?= __("required", "* Required Fields") ?></p>
    <input type='submit' class='button orange' value='Create' name='submit' />
</form>
