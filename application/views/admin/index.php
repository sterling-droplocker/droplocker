<?php enter_translation_domain("admin/index"); ?>
<script type="text/javascript">
    $(function(){
<? foreach($widgets as $widget): ?>    
       $('#<?= $widget->fileName ?>').load('/admin/widgets/<?= $widget->fileName ?>'); 
<? endforeach; ?>       
    });
</script>
<h2><?= @$widgets[0]->module ?> <?=__("Dashboard", "Dashboard"); ?> &nbsp;&nbsp;&nbsp;<a href="/admin/index/dashboard_settings"><img src="/images/icons/cog.png" width="16" height="16" alt=""></a></h2>
<br>
<table>

<tr>
    <td valign="top">
        <div id="<?= @$widgets[0]->fileName ?>" style="width:auto;"></div><br>
        <div id="<?= @$widgets[2]->fileName ?>" style="width:auto;"></div><br>
        <div id="<?= @$widgets[4]->fileName ?>" style="width:auto;"></div><br>
    </td>
    <td>&nbsp;&nbsp;</td>
    <td valign="top">
        <div id="<?= @$widgets[1]->fileName ?>" style="width:auto;"></div><br>
        <div id="<?= @$widgets[3]->fileName ?>" style="width:auto;"></div><br>
        <div id="<?= @$widgets[5]->fileName ?>" style="width:auto;"></div><br>
    </td>

</tr>

</table>

