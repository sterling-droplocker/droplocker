<?php enter_translation_domain("admin/gift_cards/create"); ?>
<h2><?php echo __("Create A Gift Card", "Create A Gift Card"); ?></h2>
<?= $this->session->flashdata("message") ?>
<br>
<? if(isset($cardNumber)) { echo '<br><strong>'.$cardNumber.'</strong><br><br>'; } ?>
<form action="" method="post" id='gift_card_form'>
<table id="box-table-a">
	<tbody>
		
		<tr>
			<td style="border-top: 1px solid #CCCCCC;"><?php echo __("Gift Card Amount", "Gift Card Amount"); ?></td>
			<td style="border-top: 1px solid #CCCCCC;"><input type="text" name="amount"> <input type="checkbox" name="lbs" value="1"><span style="vertical-align: center;"><?php echo __("As", "As"); ?> <?= weight_system_format('',false,true);?></span></td>
		</tr>
		<tr>
			<td><?php echo __("Internal Description", "Internal Description"); ?></td>
			<td><input type="text" name="description" size="40"></td>
		</tr>
		<tr>
			<td><?php echo __("Expiration Date leave blank for none", "Expiration Date (leave blank for none)"); ?></td>
			<td><input type="text" name="expiration"></td>
		</tr>
		<tr>
			<td><?php echo __("Qty", "Qty"); ?></td>
			<td><input type="text" name="qty" value="1" size="5"></td>
		</tr>
		<tr>
			<td><?php echo __("Combine With Other Coupons", "Combine With Other Coupons?"); ?></td>
			<td>
			  <select name="combine">
			    <option value="0" SELECTED><?php echo __("No", "No"); ?></option>
			    <option value="1"><?php echo __("Yes", "Yes"); ?></option>
			  </select>
			</td>
		</tr>
		<tr>
			<td><?php echo __("Frequency", "Frequency"); ?></td>
			<td>
			  <select name="frequency">
			    <option value="every order"><?php echo __("every order", "every order"); ?></option>
			    <option value="one time"><?php echo __("one time", "one time"); ?></option>
			    <option value="until used up" selected><?php echo __("until used up", "until used up"); ?></option>
			  </select>
			</td>
		</tr>
		<tr>
			<td><?php echo __("1st Order Only", "1st Order Only"); ?></td>
			<td>
				<input type="checkbox" name="firstTime" value="1" style="margin: 0px 8px">
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="Create" value="<?php echo __("Create", "Create"); ?>" class="blue button"></td>
		</tr>
		
	</tbody>
</table>
</form>
<script type="text/javascript">
$(document).ready( function() {
    $("#gift_card_form").validate( {
        rules: {
            
            "amount" : {
                required: true,
                number: true
            }
            
        },
        "errorClass" : "invalid"
    });
});
</script>
