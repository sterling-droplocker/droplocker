<?php enter_translation_domain("admin/gift_cards/index"); ?>
<h2><?php echo __("Gift Cards", "Gift Cards"); ?></h2>
<p>
<form action="/admin/gift_cards/" method="get">
<?php echo __("Gift Card Number", "Gift Card Number:"); ?> <input type="text" name="code" value="<?= isset($_GET['code'])?$_GET['code']:''?>"> <input type="submit" name="Find" value="<?php echo __("Find", "Find"); ?>" class="button blue">
</form>
<br>
<table id='box-table-a'>
    <tr>
        <th><?php echo __("Card", "Card"); ?></th>
        <th><?php echo __("Description", "Description"); ?></th>
        <th><?php echo __("Start Date", "Start Date"); ?></th>
        <th><?php echo __("End Date", "End Date"); ?></th>
        <th><?php echo __("Amount", "Amount"); ?></th>
        <th><?php echo __("Extended Desc", "Extended Desc"); ?></th>
        <th><?php echo __("First Time", "First Time?"); ?></th>
        <th><?php echo __("Redeemed", "Redeemed"); ?></th>
        <th></th>
    </tr>
    <? if(isset($cards)): foreach($cards as $card): ?>
        <tr>
        <td><?= $card->prefix . $card->code?></td>
            <td><?= $card->description ?></td>
            <td><?= $card->startDate ?></td>
            <td><?= $card->endDate ?></td>
            <td><?= $card->amount ?> <?= $card->amountType ?> <?= $card->frequency ?></td>
            <td><?= $card->extendedDesc ?></td>
            <td><?= $card->firstTime == 1 ? __("first_time_link", "First Time Only (%link%)", array("link"=>'<a href="/admin/gift_cards/anyuse/'.$card->couponID.'">change</a>')) : __("Use On Any Order", "Use On Any Order"); ?></td>
            <td><? if($customer[$card->code]): ?>  
                <a href="/admin/customers/detail/<?= $customer[$card->code]->customerID ?>" target="_blank"><?= getPersonName($customer[$card->code]) .' ('.$customer[$card->code]->customerID.')'?></a>
                <? else: ?>
                    <? if($card->redeemed == 1): ?>
                        <?php echo __("YES", "YES"); ?>
                    <? else: ?>
                        <?php echo __("NO", "NO"); ?> 
                    <? endif; ?>
                <? endif; ?>
            </td>
            <td><a href="/admin/gift_cards/deactivate/<?= $card->couponID ?>"><?php echo __("Deactivate", "Deactivate"); ?></a></td>
        </tr>
    <? endforeach; else:?>
    <tr>
        <td colspan=9><?php echo __("Sorry no cards found", "Sorry, no cards found"); ?></td>
    </tr>
    <? endif;?>
</table>
