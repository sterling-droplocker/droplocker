<?php enter_translation_domain("admin/widgets/retention_rate"); ?>
<h2><?=$location_type?></h2>

<div class='stat'>
	<div class='value'><?=number_format_locale($retention_rate, 4)?></div>
	<div class='key'><?php echo __("Retention_Rate", "Retention<br>Rate"); ?></div>
</div>
