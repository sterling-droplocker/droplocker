<?php enter_translation_domain("admin/widgets/tickets_user"); ?>
<table id="box-table-a" style="width: 475px;">
    <tr>
        <td colspan=4 style="border-top: 1px solid rgb(204, 204, 204);">
            <form action="/admin/widgets/tickets_user_set" method="post">
            <?php echo __("Open Tickets Assigned To", "Open Tickets Assigned To:"); ?>&nbsp;&nbsp;&nbsp;
            <select name="employee">
                <option value="0" <?= 0 == $user ? ' SELECTED' : ''?>><?php echo __("Customer Service", "Customer Service"); ?></option>
                <? foreach($employees as $employee): ?>
            	<option value="<?= $employee->employeeID ?>" <?= $employee->employeeID == $user ? ' SELECTED' : ''?>><?= $employee->firstName ?></option>
                <? endforeach; ?>
            </select>
            <input type="submit" name="update" value="<?php echo __("update", "update"); ?>">
            </form>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Customer", "Customer"); ?></th>
        <th><?php echo __("Created", "Created"); ?></th>
        <th><?php echo __("Issue", "Issue"); ?></th>
        <th><?php echo __("Assigned To", "Assigned To"); ?></th>
    </tr>
    <? foreach($tickets as $ticket): ?>
       <tr>
           <td><a href="/admin/tickets/edit_ticket/<?= $ticket->ticketID ?>" target="_blank"><?= getPersonName($ticket) ?></a></td>
            <td><a href="/admin/tickets/edit_ticket/<?= $ticket->ticketID ?>" target="_blank"><?= convert_from_gmt_aprax($ticket->dateCreated, STANDARD_DATE_FORMAT) ?></a></td>
            <td><?= substr($ticket->issue,0,30) ?>...</td>
            <td><?= $ticket->assignedTo ?></td>
       </tr>
    
    <? endforeach; ?>
</table>
