<?php enter_translation_domain("admin/widgets/customer_stats"); ?>
<h2><?php echo __("Customer Stats", "Customer Stats"); ?></h2>

<div class='stat'>
	<div class='value'><?= number_format_locale($total,0)?></div>
	<div class='key'><?php echo __("Total_Signups", "Total<br>Signups"); ?></div>
</div>

<div class='stat'>
	<div class='value'><?= number_format_locale($total_items,0)?></div>
	<div class='key'><?php echo __("Unique_Items", "Unique<br>Items"); ?></div>
</div>
<div style='clear:both'>
<div class='stat'>
	<div class='value'><?= number_format_locale($customerOrders,0)?></div>
	<div class='key'><?php echo __("Customers_With Orders", "Customers<br>With Orders"); ?></div>
</div>
<div style='clear:both'>
