<?php enter_translation_domain("admin/widgets/order_stats"); ?>
<h2><?php echo __("Order Stats", "Order Stats"); ?></h2>


<div class='stat'>
	<div class='value'><?= format_money_symbol($this->business_id,"%.2n",$total->grossSales)?></div>
	<div class='key'><?php echo __("Gross_Sales", "Gross<br>Sales"); ?></div>
</div>

<div class='stat'>
	<div class='value'><?= format_money_symbol($this->business_id,"%.2n",$total->grossDisc)?></div>
	<div class='key'><?php echo __("Gross_Discounts", "Gross<br>Discounts"); ?></div>
</div>
<div style='clear:both'>
<div class='stat'>
	<div class='value'><?= number_format_locale(($total->grossDisc / $total->grossSales) * -100,1)?>%</div>
	<div class='key'><?php echo __("Percent_Discounts", "Percent<br>Discounts"); ?></div>
</div>
<div class='stat'>
	<div class='value'><?= format_money_symbol($this->business_id,"%.2n",($total->grossSales + $total->grossDisc) / $total->orderCount)?></div>
	<div class='key'><?php echo __("Avg Order_Amount", "Avg Order<br>Amount"); ?></div>
</div>
<div style='clear:both'>
<div class='stat'>
	<div class='value'><?= number_format_locale($total->orderCount,0)?></div>
	<div class='key'><?php echo __("Order_Count", "Order<br>Count"); ?></div>
</div>
<div style='clear:both'>
