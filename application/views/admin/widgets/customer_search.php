<?php enter_translation_domain("admin/widgets/customer_search"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = "<td><input type='text' id='firstName' name='firstName' autocomplete='off' /><br>". __('First Name', 'First Name') . "</td>";
$fieldList['lastName'] = "<td><input type='text' id='lastName' name='lastName' autocomplete='off' /><br>". __('Last Name', 'Last Name') . "</td>";
            
$nameForm = getSortedNameForm($fieldList);

?>
<h2><?php echo __("Customer Search", "Customer Search"); ?></h2>
<br>
<form action="/admin/customers/search_customers" method="post">
<table id='hor-minimalist-a'>
	<tr>
                <?= $nameForm ?>
		<td><input type='text' id='username' name='username' autocomplete="off" /><br><?php echo __("Username", "Username"); ?></td>
	</tr>
	<tr>
		<td><input type='text' id='address' name='address' autocomplete="off" /><br><?php echo __("Address", "Address"); ?></td>
		<td><input type='text' id='phone_or_sms' name='phone_or_sms' autocomplete="off" /><br><?php echo __("Phone_SMS", "Phone/SMS"); ?></td>
		<td><input type='text' id='email' name='email' autocomplete="off" /><br><?php echo __("Email", "Email"); ?></td>
	</tr>
	<tr>
		<td colspan='3'>
			<input class='button orange'  type='submit' id='customer_search_button' value='<?php echo __("Search", "Search"); ?>' name='submit'/>
		</td>
	</tr>
</table>
</form>
