<?php enter_translation_domain("admin/widgets/tickets_overXdays"); ?>
<table id="box-table-a" style="width: 475px;">
    <tr>
        <td colspan=4 style="border-top: 1px solid rgb(204, 204, 204);">
            <form action="/admin/widgets/tickets_overXdays_set" method="post">
            <?php echo __("Open Tickets Over", "Open Tickets Over"); ?> <input type="text" name="days" value="<?= $days->setting ?>" size="4"> <?php echo __("Days", "Days"); ?>
            <input type="submit" name="update" value="<?php echo __("update", "update"); ?>">
            </form>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Customer", "Customer"); ?></th>
        <th><?php echo __("Created", "Created"); ?></th>
        <th><?php echo __("Issue", "Issue"); ?></th>
        <th><?php echo __("Assigned To", "Assigned To"); ?></th>
    </tr>
    <? foreach($tickets as $ticket): ?>
       <tr>
           <td><a href="/admin/tickets/edit_ticket/<?= $ticket->ticketID ?>" target="_blank"><?= getPersonName($ticket) ?></a></td>
            <td><a href="/admin/tickets/edit_ticket/<?= $ticket->ticketID ?>" target="_blank"><?= convert_from_gmt_aprax($ticket->dateCreated, STANDARD_DATE_FORMAT) ?></a></td>
            <td><?= substr($ticket->issue,0,100) ?>...</td>
            <td><?= $ticket->assignedTo ?></td>
       </tr>
    
    <? endforeach; ?>
</table>
