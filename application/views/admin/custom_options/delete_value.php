<?php enter_translation_domain("admin/custom_options/delete_value"); ?>
<h2><?php echo __("Delete Value", "Delete Value"); ?></h2>

<div style="padding: 10px;">
	<a href="/admin/custom_options/edit/<?= $value->customOption_id ?>"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Custom Option", "Back To Custom Option"); ?></a>
</div>

<br><br>
<form action='' method='post'>
	<h3><?php echo __("Are you sure you want to delete the value", "Are you sure you want to delete the value"); ?> <b><?= htmlentities($value->value); ?></b> ?</h3>
	<br><br>
	<input type='submit' name='delete' value='<?php echo __("Delete", "Delete"); ?>' class='button orange' />
</form>

