<?php enter_translation_domain("admin/custom_options/edit"); ?>
<h2><?php echo __("Edit Custom Option", "Edit Custom Option"); ?></h2>

<div style="padding: 10px;">
	<a href="/admin/custom_options"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Custom Options Listing", "Back To Custom Options Listing"); ?></a>
</div>

<form action='' method='post'>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Option Name", "Option Name"); ?></th>
            <td>
                <?= form_input('name', $option->name); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Display Title", "Display Title"); ?></th>
            <td>
                <table style="width:100%;">
                <? foreach($languages as $businesLanguageID => $name):
                    $title = isset($optionNames[$businesLanguageID]) ? $optionNames[$businesLanguageID]->title : null;
                ?>
                <tr>
                    <td style="width:100px;"><label for="title_<?= $businesLanguageID ?>"><?= $name ?></label></td>
                    <td><?= form_input("title[$businesLanguageID]", $title, "id=\"title_{$businesLanguageID}\""); ?></td>
                </tr>
                <? endforeach; ?>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Sort Order", "Sort Order"); ?></th>
            <td>
                <?= form_input('sortOrder', $option->sortOrder); ?>
            </td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' />
</form>

<br><br>
<div>
<h2><?php echo __("Option Values", "Option Values"); ?></h2>
<p>
    <a href="#" id="add_value_link" onclick="$('#add_value').show(); $(this).hide(); $('#cancel_link').show(); return false;"><img src="/images/icons/add.png"> <?php echo __("Add Value", "Add Value"); ?></a>
    <a href="#" id="cancel_link" onclick="$('#add_value').hide(); $(this).hide(); $('#add_value_link').show(); return false;" style="display:none;" ><?php echo __("Cancel", "Cancel"); ?></a>
</p>

<div id="add_value" style="display:none;">
<form action='/admin/custom_options/add_value/<?= $option->customOptionID ?>' method='post'>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Value", "Value"); ?></th>
            <td>
                <?= form_input('value'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Display Title", "Display Title"); ?></th>
            <td>
                <table style="width:100%;">
                <? foreach($languages as $businesLanguageID => $name): ?>
                <tr>
                    <td style="width:100px;"><label for="value_title_<?= $businesLanguageID ?>"><?= $name ?></label></td>
                    <td><?= form_input("title[$businesLanguageID]", null, "id=\"value_title_{$businesLanguageID}\""); ?></td>
                </tr>
                <? endforeach; ?>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default", "Default"); ?></th>
            <td><?php echo __("Make this option the default", "Make this option the default"); ?><br>
                <?= form_yes_no('default'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Sort Order", "Sort Order"); ?></th>
            <td>
                <?= form_input('sortOrder', $new_sort_order, 'style="width:100px;"'); ?>
            </td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Add New Value", "Add New Value"); ?>' class='button orange' />
</form>
<br><br>
</div>

<?php if (!empty($values)): ?>
<table id="hor-minimalist-a">
<thead>
	<tr>
		<th><?php echo __("Value", "Value"); ?></th>
		<th><?php echo __("Default", "Default"); ?></th>
		<th><?php echo __("Sort Order", "Sort Order"); ?></th>
		<th width="25"></th>
		<th width="25"></th>
	</tr>
</thead>
<tbody>
<?php foreach($values as $i => $value): ?>
	<tr>
		<td><?= htmlentities($value->value) ?></td>
		<td><?= $value->default ? __("YES", "YES") : __("NO", "NO") ?></td>
		<td><?= $value->sortOrder ?></td>
		<td><a href='/admin/custom_options/edit_value/<?= $value->customOptionValueID ?>' ><img src='/images/icons/pencil.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
		<td><a href='/admin/custom_options/delete_value/<?= $value->customOptionValueID ?>' ><img src='/images/icons/cross.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<p class="message warning"> <?php echo __("There are no values defined for this option", "There are no values defined for this option."); ?> </p>
<?php endif;?>


</div>
