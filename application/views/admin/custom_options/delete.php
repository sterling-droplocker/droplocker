<?php enter_translation_domain("admin/custom_options/delete"); ?>
<h2><?php echo __("Delete Business Options", "Delete Business Options"); ?></h2>

<div style="padding: 10px;">
	<a href="/admin/custom_options"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Custom Options Listing", "Back To Custom Options Listing"); ?></a>
</div>

<br><br>
<form action='' method='post'>
	<h3><?php echo __("Are you sure you want to delete the custom option", "Are you sure you want to delete the custom option"); ?> <b><?= htmlentities($option->name); ?></b> ?</h3>
	<br><br>
	<input type='submit' name='delete' value='<?php echo __("Delete", "Delete"); ?>' class='button orange' />
</form>

