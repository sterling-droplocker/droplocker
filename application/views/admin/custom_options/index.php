<?php enter_translation_domain("admin/custom_options/index"); ?>
<h2><?php echo __("Custom Options", "Custom Options"); ?></h2>

<p>
	<a href="/admin/custom_options/add"><img src="/images/icons/add.png"> <?php echo __("Add New Custom Option", "Add New Custom Option"); ?></a>
</p>
<br><br>

<?php if (!empty($options)): ?>
<table id="hor-minimalist-a">
<thead>
	<tr>
		<th><?php echo __("Name", "Name"); ?></th>
		<th><?php echo __("Sort Order", "Sort Order"); ?></th>
		<th width="25"></th>
		<th width="25"></th>
	</tr>
</thead>
<tbody>
<?php foreach($options as $i => $option): ?>
	<tr>
		<td><?= htmlentities($option->name) ?></td>
		<td><?= $option->sortOrder ?></td>
		<td><a href='/admin/custom_options/edit/<?= htmlentities($option->customOptionID) ?>' ><img src='/images/icons/pencil.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
		<td><a href='/admin/custom_options/delete/<?= htmlentities($option->customOptionID) ?>' ><img src='/images/icons/cross.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>


	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<p class="message warning"> <?php echo __("There are no custom options defined", "There are no custom options defined."); ?> </p>
<?php endif;?>

