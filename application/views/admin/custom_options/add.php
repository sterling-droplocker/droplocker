<?php enter_translation_domain("admin/custom_options/add"); ?>
<h2><?php echo __("Add Custom Option", "Add Custom Option"); ?></h2>

<div style="padding: 10px;">
	<a href="/admin/custom_options"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Custom Options Listing", "Back To Custom Options Listing"); ?></a>
</div>

<form action='' method='post'>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Option Name", "Option Name"); ?></th>
            <td>
                <?= form_input('name', $option->name); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Display Title", "Display Title"); ?></th>
            <td>
                <table style="width:100%;">
                <? foreach($languages as $businesLanguageID => $name): ?>
                <tr>
                    <td style="width:100px;"><label for="title_<?= $businesLanguageID ?>"><?= $name ?></label></td>
                    <td><?= form_input("title[$businesLanguageID]", null, "id=\"title_{$businesLanguageID}\""); ?></td>
                </tr>
                <? endforeach; ?>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Sort Order", "Sort Order"); ?></th>
            <td>
                <?= form_input('sortOrder', $option->sortOrder, 'style="width:100px;"'); ?>
            </td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Add", "Add"); ?>' class='button orange' />
</form>

