<?php enter_translation_domain("admin/template"); ?>
<!DOCTYPE html>
<html>
   <head>
      <title><?php echo __("DLA -", "DLA -"); ?> <?= $page_title ?></title>
      
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> 
    <? if (!DEV): ?>
    <script type="text/javascript" src="/js/report_error.js"></script>
    <? endif; ?>

    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/jquery-ui-1.8.18.min.js'></script>

    <script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"> </script>

    <link rel="stylesheet" href="/css/thickbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/css/admin/style.css?v=2" />

    <!-- DataTable and TableTools functionality -->
    <link rel="stylesheet" type="text/css" href="/css/DataTables-1.9.1/css/jquery.dataTables.css" />
    <link rel="stylesheet" type="text/css" href="/css/DataTables-1.9.1/css/jquery.dataTables_themeroller.css" />

    <link rel="stylesheet" type="text/css" href="/js/DataTables-1.9.1/extras/TableTools/media/css/TableTools.css" />
    <link rel="stylesheet" type="text/css" href="/css/smoothness/jquery-ui-1.8.17.custom.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery.loading_indicator.css" />
    
    <script type="text/javascript" src="/js/DataTables-1.9.1/jquery.dataTables.js"> </script>
   
    <script type="text/javascript" src="/js/DataTables-1.9.1/extras/TableTools/media/js/TableTools.min.js"> </script>
    <script type="text/javascript" src="/js/DataTables-1.9.1/extras/TableTools/media/js/ZeroClipboard.js"> </script>
    <script type="text/javascript" src="/js/DataTables-1.9.1/extras/FixedHeader/js/FixedHeader.js"> </script>
    
    <script type="text/javascript" src="/js/jquery.cookie.js"> </script>
    <script type="text/javascript" src="/js/jquery.editable.js"> </script>
    <script type="text/javascript" src="/js/jquery.jeditable.datepicker.js"> </script>
    <script type="text/javascript" src="/js/jeditable.checkbox.js"> </script>
    <script type="text/javascript" src="/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/js/jquery.form.js"></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"> </script>
    <script type="text/javascript" src="/js/jquery.dataTables.columnFilter.js"> </script>
    <script type="text/javascript" src="/js/soundmanager/script/soundmanager2.js"> </script>
    <script type="text/javascript" src="/js/jquery.validate-1.11.1.js"> </script>
    
    <script type="text/javascript">

    soundManager.setup({
        url: "/js/soundmanager/swf",
        useHTML5Audio: true,
        debugMode: false,
        onready: function() {
            soundManager.createSound({
                id: "buzzer",
                url: "/sounds/buzzer.ogg"
            });
        }
    });
        
    //THe follwoing statement initializes the DataTable plguin for keytype filtering when entering a search query.
    jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
        var _that = this;

        if ( iDelay === undefined ) {
            iDelay = 250;
        }

        this.each( function ( i ) {
            $.fn.dataTableExt.iApiIndex = i;
            var
                $this = this,
                oTimerId = null,
                sPreviousSearch = null,
                anControl = $( 'input', _that.fnSettings().aanFeatures.f );

                anControl.unbind( 'keyup' ).bind( 'keyup', function() {
                var $$this = $this;

                if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                    window.clearTimeout(oTimerId);
                    sPreviousSearch = anControl.val(); 
                    oTimerId = window.setTimeout(function() {
                        $.fn.dataTableExt.iApiIndex = i;
                        _that.fnFilter( anControl.val() );
                    }, iDelay);
                }
            });

            return this;
        } );
        return this;
    };
    
    jQuery.fn.dataTableExt.afnSortData['dom-text'] = function  ( oSettings, iColumn )
{
    var aData = [];
    $( 'td:eq('+iColumn+') input', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
        aData.push( this.value );
    } );
    return aData;
}
    $.fn.dataTableExt.sErrMode = "throw";
    
    $(document).ready(function() {
       $("#change_business").click(function() {
           $("#change_business_dialog").dialog( {
               modal : true,
               width : '500px',
               title : "<?php echo __("Change Business", "Change Business"); ?>"
           });
       });
    });
    
    </script>
      
      
      <script type="text/javascript" src='/js/functions.js'></script>
        <script type="text/javascript" src="/js/thickbox.js"></script>
        <script type="text/javascript" src="/js/jquery.hotkeys-0.7.9.js"> </script>
        <link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
        
<style type="text/css">
    /* The following style is intended to style validation error messages generated by the jquery.validate library. */
    .invalid {
        color: red;
    
    }

    
    
    /* The following styles are used to style the DataTable display. */

    /* This is the container for the search. */
    .dataTables_filter
    {
        float:left; 
        text-align: none;
        margin: 10px;
    }
    /* This is the container for the TableTool buttons. */
    .DTTT_container
    {
        margin: 10px;
    }
    
    .dataTables_length
    {
        margin: 10px;
    }
    
    /* Timepicker styling */
    
    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left; }
    .ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }    
       
    @media print {
        #main_panel {
            margin: 0px !IMPORTANT;
        }
        #admin_sidebar {
            display: none;
        }
        #toggle_div {
            display : none;
        }
        
        #codeigniter_profiler {
            display: none;
        }
        #header {
            display : none;
        }
        #footer {
            display : none;
        }
        #admin_sidebar {
            display: none;
        }
        #ddcolortabs {
            display : none;
        }
    } 
</style>

      <?=$_scripts ?>
 
      <?=$_styles ?>


<script type="text/javascript">
     
//The following event binder makes the key combination "Control + S" redirect to the search view.
$(document).bind("keydown", "Ctrl+s", function(event) {
    event.preventDefault();
    $.blockUI({message : "<h1> Loading... </h1>"});
    window.location.replace("/admin/admin/search");
});


    var domain = '<?= $_SERVER['HTTP_HOST']?>';
    
$(document).ready(function(){
    var url = 'http://droplocker.com/wiki/forum?mingleforumaction=addtopic&forum=1.0';
    $('#bug_link').attr('href', url);
    $('#bug_link').attr('target', '_blank');
    
    
$(".aprax_dataTable").each(
    function(index, dataTable_instance) {
        $(dataTable_instance).dataTable({
            "iDisplayLength" : 25,
            "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
            "bStateSave": true,
                "fnStateSave": function (oSettings, oData) {
                localStorage.setItem( 'DataTables', JSON.stringify(oData) );
            },
            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },    

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //Note, only the pdf and print buttons should be shown to franchisee users when the server is in Bizzie mode
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
            }
        });
        new FixedHeader(dataTable_instance);
    });
});

</script>
   </head>
   <body>
    <div style='float:right;padding:5px;'>
        <!-- superadmin switcher was here... -->
        <?php 
            $allowed_businesses = allowed_businesses();
            if (is_superadmin() || $allowed_businesses): 
        ?>
            <input type="button" id="change_business" value="<?php echo __("Change Business", "Change Business"); ?>" />
        <?php endif ?>
         <span id="employee_name">
             <?= $this->session->userdata('buser_fname'); ?> <?= $this->session->userdata('buser_lname'); ?>
         </span> (<?= $this->zacl->get_employee_role($this->session->userdata('buser_id')); ?>) | <a href='/admin/index/logout'><?php echo __("Logout", "Logout"); ?></a>
    </div>

   	<?= $this->session->flashdata('status_message') ?>
      <div id="wrapper" style="min-width:1100px">
      	
         <div id="header"
         <?
         $domain = explode(".",$_SERVER['HTTP_HOST']);
          if(DEV){
          	echo "style='background-color:#cc0000'";
          }elseif(!DEV && $domain[sizeof($domain)-1]=='loc'){
          	echo "style='background-color:#fff200'";
          }

          ?>
         >

         	<h1>
	   	 <?= $this->session->userdata("companyName") ?> 
             :: <?= $page_title?>
             <?
              if(DEV){
          			echo __("DEV MODE", "(DEV MODE)");
         		 }elseif(!DEV && $domain[sizeof($domain)-1]=='loc'){
          			echo __("USING PRODUCTION DATA AND NOT IN DEV MODE", "(USING PRODUCTION DATA AND NOT IN DEV MODE)");
         		 }
             ?>
            <?php
            if ($domain[sizeof($domain) - 1] == "qa")
            {
                echo '------ <img alt="QA Environment" style="width: 53px; height: 58px; margin-right: 5px; " src="/images/qa_mode.png" />'.__("QA Environment", "QA Environment").' <img alt="QA Environment" style="width: 53px; height: 58px; margin-right: 5px; " src="/images/qa_mode.png" /> ------' . __FILE__ ;
            }
            
            echo get_current_branch();
            ?>
                </h1> 
         </div>
         
         <div>
            <?= $menu?>
         </div>
         
         <div id="content" style="min-height:680px;" >
            <div style='float:right;padding:10px;position:relative;z-index:9999;'>
                <?php if (empty($view) || $view != 'view_internet_explorer'): ?>
                    <script>var pfHeaderImgUrl = '';var pfHeaderTagline = '';var pfdisableClickToDel = 0;var pfHideImages = 0;var pfImageDisplayStyle = 'right';var pfDisablePDF = 0;var pfDisableEmail = 0;var pfDisablePrint = 0;var pfCustomCSS = 'https://droplocker.com/css/printFriendly.css';var pfBtVersion='1';(function(){var js, pf;pf = document.createElement('script');pf.type = 'text/javascript';if('https:' == document.location.protocol){js='https://pf-cdn.printfriendly.com/ssl/main.js'}else{js='http://cdn.printfriendly.com/printfriendly.js'}pf.src=js;document.getElementsByTagName('head')[0].appendChild(pf)})();</script><a href="http://www.printfriendly.com" style="color:#6D9F00;text-decoration:none;" class="printfriendly" onclick="window.print();return false;" title="Printer Friendly and PDF"><img style="border:none;-webkit-box-shadow:none;box-shadow:none;margin:0 6px"  src="https://pf-cdn.printfriendly.com/images/icons/pf-print-icon.gif" width="16" height="15" alt="Print Friendly Version of this page" />Print <img style="border:none;-webkit-box-shadow:none;box-shadow:none;margin:0 6px"  src="https://pf-cdn.printfriendly.com/images/icons/pf-pdf-icon.gif" width="12" height="12" alt="Get a PDF version of this webpage" />PDF</a>
                <?php endif; ?>
                    <?php if (in_bizzie_mode()): ?>
                        <a href="mailto:fransupport@1-800-DryClean.com?subject=Bug Report - <?=$page_title?> - <?=get_full_url()?>"> <img src='/images/icons/bug.png' /> </a>
                    <?php else: ?>
                        <a title='Submit a bug' id='bug_link' href=''><img src='/images/icons/bug.png' /></a>
                    <?php endif ?>
                                        <a title='Page Help' id='info' href='javascript:;' >
                        <img src='/images/icons/information.png' />
                    </a> 
                </div>            
             <?= $content ?>
         </div>
         
         <div id="footer" style='float:right;padding-right:15px;color:#333;'>
            <a href="http://droplocker.com">
                <img align="middle" src="/images/logo-powered.jpg" style="width: 75%; height: 75%;" />
              </a>
         </div>
      </div>

            <?php if(ENVIRONMENT != "production"): ?>
          <div style="text-align: left;padding: 10px;">
          <?php echo ENVIRONMENT. "| Page Load: ".$this->benchmark->elapsed_time()." seconds";?> | <?php echo "Memory Usage: ".$this->benchmark->memory_usage();?>
            <?php echo "<br>Database Host: ".$this->db->hostname." | Database: ". $this->db->database?>
            <?php echo "<br>GMT TIME: ".date("Y-m-d H:i:s")?>
            <?= "<br /> Business User ID: {$this->session->userdata('buser_id')}" ?>
            <?php echo "<BR>SERVER TIMEZONE: ". date_default_timezone_get();?>
          <?= $footer ?>
          </div>
            <?php endif; ?>

    <?php 
         //Note, the change business dialog needs to be the last form in the page, because a bug in selenium always selects the select element in this form.
         if (is_superadmin() || $allowed_businesses)
         {
             echo "<div id='change_business_dialog' style='display: none;'>";

             $this->load->model("business_model");
             $businesses = $this->business_model->get_all_as_codeigniter_dropdown(true);

             //filter not needed businesses
             if (!is_superadmin()) {
                foreach ($businesses as $k=>$b) {
                  if (!in_array($k, $allowed_businesses)) unset($businesses[$k]);
                }
             }

             echo form_open("/admin/superadmin/change_business");
             echo form_fieldset("Select Business", array("style" => "border: 1px solid; margin: 5px; padding:20px;"));
             echo '<div>' . form_dropdown("current_business_id", $businesses, $this->session->userdata("business_id")) . '</div>';
             echo form_submit(array("style" => "size: 0.7em; margin-top: 15px;", "class" => "button orange", "value" => "Change Business"));
             echo form_fieldset_close();
             echo form_close();
             echo "</div>";
         }
     ?>
   </body>
</html>
