<?php enter_translation_domain("admin/export/index"); ?>
<h2><?php echo __("Export", "Export"); ?></h2>
<form method="post" target="_blank">
<div>
    <div style="margin:10px;">
        <?php echo __("Export Type", "Export Type:"); ?>
        <select name="type">
            <option><?php echo __("Fabricare", "Fabricare"); ?></option>
            <option><?php echo __("Spot", "Spot"); ?></option>
        </select>
    </div>
    <div style="margin:10px;">
        <?php echo __("Start", "Start:"); ?> <input name="start_date" type="text" id="start_date" />&nbsp;&nbsp; 
        <?php echo __("End", "End:"); ?> <input name="end_date" type="text" id="end_date" />
    </div>
    <div>
        <button id="export_button"><?php echo __("EXPORT", "EXPORT"); ?></button>
    </div>
</div>
</form>

<script type="text/javascript">
$(function(){
    $('#start_date').datepicker({maxDate:"0d"});
    $('#end_date').datepicker({maxDate:"0d"});
});
</script>