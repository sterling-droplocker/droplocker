<?php enter_translation_domain("admin/mailchimp/index"); ?>
<h2><?php echo __("MailChimp Beta", "MailChimp (Beta)"); ?></h2>
<form action='/admin/mailchimp/settings_update' method='post'>
    <table class="detail_table">
        <tr>
            <th><?php echo __("Enabled", "Enabled"); ?></th>
            <td>
                <?= form_yes_no("mailchimp_enabled", get_business_meta($business->businessID, 'mailchimp_enabled')); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("API Key", "API Key"); ?></th>
            <td>
                <input type="text" name="mailchimp_api" value="<?= get_business_meta($business->businessID, 'mailchimp_api'); ?>" /> 
                <?php if(get_business_meta($business->businessID, 'mailchimp_enabled')): ?>
                    <?= ($mailchimp_valid_key) ? __("Valid", "(Valid)") : __("Not Valid", "Not Valid!!!"); ?>
                <?php endif; ?>                
            </td>
        </tr>
        <tr>
            <th><?php echo __("List Name", "List Name"); ?></th>
            <td>
                <input type="text" name="mailchimp_list" value="<?= get_business_meta($business->businessID, 'mailchimp_list'); ?>" />
                <?php if($mailchimp_valid_key): ?>
                    <?php if(get_business_meta($business->businessID, 'mailchimp_list')): ?>
                        <?php if(!isset($mailchimp_stats['list_id'])): ?>
                            <br /><?php echo __("Does not exist", "Does not exist:"); ?>
                            <a href="/admin/mailchimp/createDroplockerList" class="button blue"><?php echo __("Create List", "Create List"); ?></a>
                        <?php else: ?>
                            <?php echo __("Exists", "(Exists)"); ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <br /><?php echo __("Enter a new list name", "(Enter a new list name)"); ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Automatically Update", "Automatically Update"); ?></th>
            <td>
                <?= form_yes_no("mailchimp_automatic_update", get_business_meta($business->businessID, 'mailchimp_automatic_update')); ?>
                <br /><?php echo __("Automatic updates occur every night for all new customers and existing customers whose information has changed", "(Automatic updates occur every night for all new customers and existing customers whose information has changed.)"); ?>
            </td>
        </tr>
        <?php if($mailchimp_enabled && $mailchimp_valid_key && isset($mailchimp_stats['list_id'])): ?>
                <tr>
                    <th><?php echo __("Members in MailChimp", "Members in MailChimp"); ?></th>
                    <td>
                       <?php echo __("Subscribed", "Subscribed:"); ?> <?= $mailchimp_stats['subscribed']; ?> - <?php echo __("Unsubscribed", "Unsubscribed:"); ?> <?= $mailchimp_stats['unsubscribed']; ?><br />
                       <?php if(isset($mailchimp_pending_batches)): ?>
                          (<?php echo __("Pending Updates", "Pending Updates:"); ?> <?= $mailchimp_pending_batches; ?>)<br />
                       <?php endif; ?>
                    </td>
                </tr>
                
                <?php if ($mailchimp_last_update): ?>
                    <tr>
                        <th><?php echo __("Last Update", "Last Update"); ?></th>
                        <td>

                            <?php if ($mailchimp_last_update_qty): ?>
                                <?= $mailchimp_last_update_qty; ?> <?php echo __("updates were sent to MailChimp on", "updates were sent to MailChimp on"); ?> <?= $mailchimp_last_update; ?>
                            <?php else: ?>
                                <?php echo __("No updates were sent to MailChimp with this update", "No updates were sent to MailChimp with this update."); ?>                 
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo __("Manually Update MailChimp", "Manually Update MailChimp"); ?></th>
                    <td>
                        <a href="/admin/mailchimp/updateMembers" class="button blue"><?php echo __("All Customers", "All Customers"); ?></a> <?php echo __("will send or resend all customers information to MailChimp)", "(will send or resend all customer's information to MailChimp"); ?><br>
                        <?php if($mailchimp_stats['subscribed'] || $mailchimp_stats['unsubscribed']): ?>
                            <a href="/admin/mailchimp/updateMembers/1" class="button blue"><?php echo __("Changed customers", "Changed customers"); ?></a> (<?php echo __("will only send or resend customers whose data has changed since last update", "will only send or resend customers whose data has changed since last update"); ?>                       
                            <?php if ($mailchimp_last_update): ?>
                                - <?= $mailchimp_last_update; ?>)<br>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endif; ?>

    </table>
    <br>
    <div align="center" style="padding:10px;"><input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' /></div>
</form>
