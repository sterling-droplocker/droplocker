<?php

enter_translation_domain("admin/bags/edit");

?>

<h2><?= __("update_bag_number", "Update Bag %bagNumber%", array('bagNumber' => $bag->bagNumber)); ?></h2>

<br><br>
<?= form_open("/admin/bags/update") ?>
    <?= form_hidden("bagID", set_value('bagID', $bag->bagID)) ?>

    <label><?= __("bag_notes", "Bag Notes") ?></label>
    <?= form_input("notes", set_value('notes', $bag->notes)) ?>

    <input type="submit" value="<?= __("update_bag", "Update Bag") ?>" class="button orange">
<?= form_close() ?>
