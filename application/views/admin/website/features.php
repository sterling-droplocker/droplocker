<?php

enter_translation_domain("admin/website/features");

/**
 * Expects the following content variables
 *  facebook_share_rewards_program_thumbnail_url string
 */

$can_edit = !in_bizzie_mode() || is_superadmin();
$readonly = $can_edit ? "" : "disabled='disabled'";

$pages = array(
    'account'                     => __("main", " Main account page"),
    'account/profile/edit'        => __("customer_account", " Customer account information page"),
    'account/profile/preferences' => __("customer_settings", "Customer laundry settings page"),
    'account/profile/billing'     => __("credit_card", "Customer credit card page"),
);

?>
<h2><?= __("header", "Website Features")  ?></h2>
<br />
<? if ($can_edit): ?>
    <form action='' method='post'>
<? endif ?>
    <table class='detail_table'>
        <tr>
            <th><?= __("new_customer_page", "New Customer Page")  ?></th>
            <td><?= __("directed", "Select the page the customer will be directed to after registration")  ?><br>
                <?= form_dropdown('registerPage', $pages, get_business_meta($this->business_id, 'registerLandingPage'), $readonly); ?>
            </td>
        </tr>

        <tr>
            <th><?= __("gift_cards", "Gift Cards") ?></th>
            <td><?= __("allow_gift_cards", "Allow customers to buy gift cards") ?><br>
                <?= form_yes_no('gift_card', get_business_meta($this->business_id, 'sell_gift_cards'), ' ', $readonly) ?>
            </td>
        </tr>
        <tr>
            <th><?= __("facebook", "Facebook Share Programs Rewards Thumbnail") ?> </th>
            <td>
                <?= form_input("facebook_share_rewards_program_thumbnail_url", $facebook_share_rewards_program_thumbnail_url, $readonly) ?>
            </td>
        </tr>
        <tr>
            <th><?= __("limit_autocomplete", "Limit autocomplete of locations to country") ?></th>
            <td>
                <?= form_input("limit_geo_country", get_business_meta($this->business_id, 'limit_geo_country'), $readonly) ?><br>
                <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2" target="_blank"><?= __("must_be_iso_date", "Must be ISO_3166-1 format") ?></a>
            </td>
        </tr>
        <tr>
            <th><?= __("copy_phone_to_sms", "Copy Phone number to SMS") ?></th>
            <td><?= __("copy_phone_to_sms_note", "If enabled, will copy the Phone number to SMS number") ?><br>
                <?= form_yes_no('copy_phone_to_sms', get_business_meta($this->business_id, 'copy_phone_to_sms'), ' ', $readonly) ?>
            </td>
        </tr>
        <tr>
            <th><?= __("show_second_sms", "Show second SMS in profile") ?></th>
            <td>
                <?= form_yes_no('show_second_sms', get_business_meta($this->business_id, 'show_second_sms'), ' ', $readonly) ?>
            </td>
        </tr>
        <tr>
            <th><?= __("list_laundry_plans", "List Laundry Plans in Customer's 'My Orders' page") ?></th>
            <td>
                <?= form_yes_no('list_laundy_plans', get_business_meta($this->business_id, 'list_laundy_plans'), ' ', $readonly) ?>
            </td>
        </tr>
    </table>
<? if ($can_edit): ?>
    <input type='submit' name='submit' value= <?= __("update", "Update") ?> class='button orange' />
    </form>
<? endif ?>

