<?php enter_translation_domain("admin/website/manage_iphone_app"); ?>
<?php
/**
 * Expects the following content variables
 *  iphone_icon_path string
 *  iphone_icon_retina_path string
 *  iphone_launch_image_path string
 *  iphone_retina_launch_image_path string
 *
 *  name string
 *  token string
 *  secret string
 *  active bool
 */
/**
if (!isset($name)) {
    trigger_error("'name' must be set", E_USER_WARNING);
}
if (!isset($token)) {
    trigger_error("'token' must be set", E_USER_WARNING);
}
if (!isset($secret)) {
    trigger_error("'secret' must be set", E_USER_WARNING);
}
if (!isset($active)) {
    trigger_error ("'active' must be passed as a content varible", E_USER_WARNING);
}
if (!isset($iphone_retina_icon_path)) {
    trigger_error("'iphone_icon_retina_path' must be set", E_USER_WARNING);
}
if (!isset($iphone_icon_path)) {
    trigger_error("'iphone_icon_path' must be set", E_USER_WARNING);
}
if (!isset($iphone_launch_image_path)) {
    trigger_error("'iphone_launch_image_path' must be set", E_USER_WARNING);
}
if (!isset($iphone_retina_launch_image_path)) {
    trigger_error("'iphone_retina_launch_image_path' must be set", E_USER_WARNING);
}
if (!isset($iphone_retina_4_launch_image_path)) {
    trigger_error("'iphone_retina_4_launch_image_path' must be set", E_USER_WARNING);
}
 *
 */
?>
<style type="text/css">
    #main_panel form {
        margin-top: 10px;
    }
    fieldset {
        border: 1px solid #000000;
        padding: 10px;
    }
    fieldset legend {
        font-weight: bold;
    }

</style>

<h2> <?php echo __("iPhone App Settings", "iPhone App Settings"); ?> </h2>

<div id="app_settings_container">
    <?= form_open("/admin/apps/update_text_fields", array('id' => "text_fields_form")) ?>
        <?= form_fieldset("Data Fields", array( "style" => "width: 600px")) ?>
            <table class="detail_table">
                <tbody>
                    <tr>
                        <th> <?= form_label(__("Name", "*Name"), "name")?> </th>
                        <td> <?= form_input("name", $name, "id='name' required") ?> </td>
                    </tr>
                    <?php if(is_superadmin()): ?>
                        <tr>
                            <th> <?= form_label(__("Active", "Active"), "active") ?> </th>
                            <td> <?= form_checkbox("active", 1, $active, "id='active'") ?> </td>
                        </tr>
                        <tr>
                            <th> <?= form_label(__("API Token", "API Token"), "token") ?> </th>
                            <td> <?= form_input("token", $token, "id='token'") ?> </td>
                        </tr>
                        <tr>
                            <th> <?= form_label(__("API Secret", "API Secret"), "secret") ?> </th>
                            <td> <?= form_input("secret", $secret, "id='secret'") ?> </td>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <th> <?= form_label(__("Description", "*Description"), "description") ?>  </th>
                        <td> <?= form_textarea("description", $description, "id='description' required") ?></td>
                    </tr>
                    <tr>
                        <th> <?= form_label(__("Keywords", "*Keywords"), "keywords") ?>  </th>
                        <td> <?= form_input("keywords", $keywords, "id='keywords' required") ?></td>
                    </tr>
                    <tr>
                        <th> <?= form_label(__("Support URL", "*Support URL"), "support_url")?> </th>
                        <td> <?= form_input("support_url", $support_url, "id='support_url' required") ?></td>
                    </tr>
                    <tr>
                        <th> <?= form_label(__("Marketing URL", "Marketing URL"), "marketing_url") ?> </th>
                        <td> <?= form_input("marketing_url", $marketing_url, "id='marketing_url'") ?> </td>
                    </tr>
                    <tr>
                        <th> <?= form_label(__("Development Mode", "Development Mode"), "development_mode") ?> </th>
                        <td> <?= form_checkbox("development_mode", 1, $development_mode) ?> </td>
                    </tr>
                </tbody>
            </table>
            <div style="float: right;">
                <?= form_submit(array("class" => "button orange","value" => "Update", "style" => "margin-top: 5px")) ?>
            </div>
            <p> * <?php echo __("Required Fields", "Required Fields"); ?> </p>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>

    <p> <?php echo __("The image files must be in PNG format", "The image files must be in PNG format."); ?> &nbsp;&nbsp;  <a href="/admin/website/download_app_assets" target="_blank"><?php echo __("Download all as ZIP", "Download all as ZIP"); ?></a></p>
    <?= form_open_multipart("/admin/apps/update_app_icon_path", array("class" => "image_upload_form")) ?>
        <?= form_fieldset(__("Icon", "Icon (1024px x 1024px, 72 DPI)")) ?>
            <div>
                <?php if (empty($app_large_icon_path)): ?>
                    <p> <?php echo __("No Icon Image", "--- No Icon Image ---"); ?> </p>
                <?php else: ?>
                    <img src="<?=$app_large_icon_path?>" alt="<?php echo __("Icon Image", "Icon Image"); ?>" />
                <?php endif ?>
            </div>
            <?= form_upload("update_app_icon_path") ?>
            <?= form_submit(array("class" => "button orange", "value" => __("Update", "Update"))) ?>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>

    <!-- sólo éste falta -->
    <?= form_open_multipart("/admin/apps/update_app_splash_path", array("class" => "image_upload_form")) ?>
        <?= form_fieldset(__("Splash", "Splash (1242px x 2208px, 72 DPI)")) ?>
            <div>
                <?php if (empty($app_splash_path)): ?>
                    <p> <?php echo __("No Splash Image Set", "--- No Splash Image Set ---"); ?> </p>
                <?php else: ?>
                    <img src="<?=$app_splash_path?>" alt="<?php echo __("Splash Image", "Splash Image"); ?>" />
                <?php endif ?>
            </div>
            <?= form_upload("update_app_splash_path") ?>
            <?= form_submit(array("class" => "button orange", "value" => __("Update", "Update"))) ?>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>

    <?= form_open_multipart("/admin/apps/updateAndroidFeatureGraphic", array("class" => "image_upload_form"))?>
        <?= form_fieldset(__("Android Feature Graphic", "Android Feature Graphic")) ?>
            <div>
                <?php if (empty($android_feature_graphic_path)): ?>
                    <p> <?php echo __("No Android Feature Graphic Set", "--- No Android Feature Graphic Set ---"); ?> </p>
                <?php else: ?>
                    <img src="<?=$android_feature_graphic_path?>" alt="<?php echo __("Android Feature Graphic", "Android Feature Graphic"); ?>"/>
                <?php endif ?>
            </div>
            <?= form_upload("androidFeatureGraphicFile") ?>
            <?= form_submit(array("class" => "button orange","value" => __("Update", "Update"))) ?>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>

</div>
<script type='text/javascript'>
$("form :submit").loading_indicator();

$("#text_fields_form").validate( {
    rules: {
        name: {
            required: true
        },
        description: {
            required: true
        },
        keywords: {
            required: true
        },
        support_url: {
            required: true
        }
    },
    errorClass : "invalid",
    invalidHandler : function(form, validator) {
        $("#text_fields_form :submit").loading_indicator("destroy");
    }
})

$("#app_settings_container").submit(function() {
    if ($("#text_fields_form").valid()) {
        return true;
    } else {
        alert("<?php echo __("Please enter the required text fields first", "Please enter the required text fields first"); ?>");
        $("input.invalid").focus();
        $(this).find(":submit").loading_indicator("destroy");
        return false;
    }
});
</script>