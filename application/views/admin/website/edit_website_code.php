<?php

enter_translation_domain("admin/website/edit_website_code");

$can_edit = !in_bizzie_mode() || is_superadmin();

?>
<script type="text/javascript">
$(document).ready(function() {
    $(":submit").loading_indicator();
    
    //Display the macro reference dialog.
    $("#macros").click(function(evt) {
        evt.preventDefault();
        $("#footer_macros_reference").dialog({
            title : "<?= __("footer_macro_reference", "Footer Macro Reference") ?>"
        });
    });
});
</script>

<h2><?= (__("edit_code", "Edit Website Code"))?></h2>
<?= $this->session->flashdata('message');?>
<? if ($can_edit): ?>
    <?= form_open() ?>
<? endif ?>
        <table class="detail_table">
            <? if (is_superadmin()): ?>
            <tr>
                <th><?= __("template", "Template") ?></th>
                <td>
                    <?= form_dropdown('template', $templates, set_value('template', $website->template)) ?>
                </td>
            </tr>
            <? endif; ?>
            <tr>
                <th><?= __("title", "Title") ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <?= form_input('title', set_value('title', $website->tile)) ?>
                    <? else: ?>
                        <?= htmlentities($website->title) ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("javascript", "Javascript Files") ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <textarea placeholder="<script src='http://mysite.com/jsfile.js'></script>" style='width:99%;height:50px;border:1px solid #cccccc;' name='javascript'><?= set_value('javascript', $website->javascript) ?></textarea>
                    <? else: ?>
                        <pre><?= htmlentities($website->javascript) ?></pre>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("style_file", "Style (CSS) Files") ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <textarea placeholder="<link rel='stylesheet' type='text/css' href='http://mysite.com/style.css' />" style='width:99%;height:50px;border:1px solid #cccccc;' name='stylescript'><?= set_value('stylescript', $website->stylescript) ?></textarea>
                    <? else: ?>
                        <pre><?= htmlentities($website->stylescript) ?></pre>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("header", "Header<br>(Including the menu)") ?>
                </th>
                <td>
                    <? if ($can_edit): ?>
                        <textarea style='width:99%;height:200px;border:1px solid #cccccc;' name='header'><?= set_value('header', $website->header) ?></textarea>
                    <? else: ?>
                        <pre><?= htmlentities($website->header) ?></pre>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("footer", "Footer") ?> <br />
                    <a style="font-size: 0.7em;" id="macros" href="#"><?= __("macro", "Macro Help") ?> </a> </th>
                <td>
                    <? if ($can_edit): ?>
                        <textarea style='width:99%;height:200px;border:1px solid #cccccc;' name='footer'><?= $website->footer;?></textarea>
                    <? else: ?>
                        <pre><?= htmlentities($website->footer) ?></pre>
                    <? endif ?>
                </td>
            </tr>
        </table>
    
<? if ($can_edit): ?>
        <input type='submit' value=<?= __("update", "Update") ?> class='button orange'/>
    <?= form_close() ?>
<? endif ?>

<div id="footer_macros_reference" style="display: none;">
    <strong><?= __("display", "Display a random testimonial:") ?></strong>
    <i>%if_testimonial%  %testimonial% %/if_testimonial%</i>
</div>
