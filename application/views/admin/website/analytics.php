<?php enter_translation_domain("admin/website/analytics"); ?>
<h2><?php echo __("Analytics", "Analytics"); ?></h2>
<p><?php echo __("Add your Google analytic code to the form below. You do not need to enter the entire code block, only the analytic ID", "Add your Google analytic code to the form below. You do not need to enter the entire code block, only the analytic ID"); ?></p>
<p><?php echo __("Example: UA-31530097-1", "Example: UA-31530097-1"); ?></p>

<?php if (!in_bizzie_mode() || is_superadmin()): ?>
<form action='' method='post'>
<?php endif ?>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Analytic ID", "Analytic ID"); ?></th>
            <td>
                <?php if (!in_bizzie_mode() || is_superadmin()): ?>
                    <input type='text' name='ga' value='<?php echo $ga?>' />
                <?php else: ?>
                    <?= $ga ?>
                <?php endif ?>
            </td>
        </tr>
    </table>
<?php if (!in_bizzie_mode() || is_superadmin()): ?>    
    <input type='submit' name='submit' class='button orange' value='<?php echo __("Submit", "Submit"); ?>' />
</form>
<?php endif ?>