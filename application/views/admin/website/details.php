<?php enter_translation_domain("admin/website/details"); ?>
<h2><?php echo __("Website Status", "Website Status"); ?></h2>
<br>


<h3><?php echo __("Hosting Details", "Hosting Details"); ?></h3>
<table class="detail_table">
	<tr>
		<th><?php echo __("SFTP Location", "SFTP Location"); ?></th>
		<td><?php echo __("Droplocker.com", "Droplocker.com"); ?></td>
	</tr>
	<tr>
		<th><?php echo __("SSH Port", "SSH Port"); ?></th>
		<td><?php echo __("9253", "9253"); ?></td>
	</tr>
	<tr>
		<th><?php echo __("Username", "Username"); ?></th>
		<td><?php echo __("dash", "dash"); ?></td>
	</tr>
	<tr>
		<th><?php echo __("Password", "Password"); ?></th>
		<td><?php echo __("*****", "*****"); ?></td>
	</tr>
</table>
<br>
<table class="detail_table">
	<tr>
		<th><?php echo __("Status", "Status"); ?></th>
		<td><?= $status?></td>
	</tr>
</table>
