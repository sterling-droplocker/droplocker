<?php

enter_translation_domain("admin/website/edit_logo");

?>

<? if (in_bizzie_mode()): ?>
    <h2><?= __("manage_master_logos", "Manage Master Business Logos") ?></h2>
<? else: ?>
    <h2><?= __("manage_images", "Manage Images") ?></h2>
<? endif; ?>

<?php echo $this->session->flashdata('message')?>

<script type="text/javascript">

function create_placeholder(image) {
        image.src = "/images/missing_image.jpg";
        image.onerror = "";
        return true;
}

$(document).ready( function() {
    
$(".thumbnail").click(function() {
    var div = $('<div></div>').append($(this).clone().css({width: 'auto', height: 'auto'}));
    div.dialog({
        modal: true,
        height: 400,
        width: 600
    });
})
$(":submit").loading_indicator();

$(".delete").click(function() {
    var imageID = $(this).attr("data-imageID");
    if (imageID) {
        if (confirm("<?= __("confirm_delete", "Are you sure you want to delete this image?") ?>")) {
            window.location.replace("/admin/website/delete_image/" + imageID);
        }
    } else {
        alert("<?= __("error", "Sorry, an error occurred attempting to delete this image.") ?>");
    }

});

})  
</script>

<br>
<h3><?= __("upload_image", "Upload image:") ?></h3>
<br>
<?= form_open_multipart('/admin/website/edit_logo'); ?>
    <input type="file" name="userfile" size="20" />
    <br /><br />
    <input type="submit" name='submit' class="button orange" value="<?= __("upload", "Upload") ?>" />
</form>

<br><br>
<? if (!empty($placements)): ?>
    <? if($images): ?>

        <?= form_open('/admin/website/update_placements') ?>
            <table rules="groups" class="box-table-a">
                <thead>
                    <tr>
                        <th> <?= __("id", "ID") ?> </th>
                        <th> <?= __("image", "Image") ?> </th>
                        <? foreach ($placements as $id => $name): ?>
                            <th> <?= $name ?> </th>
                        <? endforeach; ?>
                        <th> <?= __("delete", "Delete?") ?> </th>
                    </tr>
                </thead>
                <tbody> 
                        <? foreach($images as $image): ?>
                            <tr>
                                <td> <?= $image->imageID ?> </td>
                                <td> <img style="width: 125px; height: 50px; cursor: pointer;" class="thumbnail" src="/images/logos/<?=$image->filename ?>" onerror="create_placeholder(this);">  </td>
                                <? foreach($placements as $id => $name):
                                    $selected = array_key_exists($image->imageID, $business_placements) && in_array($id, $business_placements[$image->imageID]);
                                ?>
                                <td align="center">
                                    <?= form_radio($id, $image->imageID, $selected ? 'selected="selected"' : ''); ?>
                                </td>
                                <? endforeach; ?>
                                <td align="center">
                                    <img class="delete" style="cursor:pointer;" id="image-<?= $image->imageID?>" data-imageID="<?= $image->imageID ?>" src="/images/icons/cross.png" />
                                </td>
                            </tr>
                        <? endforeach; ?>
                 </tbody>
            </table>
            <?= form_submit(array("value" => __("update", "Update Settings") , "style" => "margin: 10px 0px", "class" => "button orange")) ?>
        <?= form_close() ?>
    <? endif; ?>

<? else: ?>
    <?= __("not_defined", "There are no placements defined yet.") ?>
<? endif; ?>
