<?php enter_translation_domain("admin/home_delivery/locations"); ?>
<?php

$fieldList = array();
$fieldList['address1'] = "<th> ". __("Address", "Address") . "</th>";
$fieldList['city'] = "<th> ". __("City", "City") . "</th>";
$fieldList['state'] = "<th>" . __("State", "State") . "</th>";
$fieldList['zip'] = "<th>" . __("ZIP", "ZIP") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['address1'] = "<td>{$location->address_line_1}</td>";
    $fieldList['city'] = "<td>{$location->address_city}</td>";
    $fieldList['state'] = "<td>{$location->address_state}</td>";
    $fieldList['zip'] = "<td>{$location->address_zipcode}</td>";

    return getSortedAddressForm($fieldList);
}
        
?>
<h2><?php echo __("Deliv Locations", "Deliv Locations"); ?></h2>

<table id="deliv_locations">
<thead>
    <tr>
        <th><?php echo __("ID", "ID"); ?></th>
        <th><?php echo __("Name", "Name"); ?></th>
        <?= $addressTableHeader ?>
        <th><?php echo __("Locations", "Locations"); ?></th>
    </tr>
</thead>
<tbody>
<? foreach($stores as $store): ?>
<tr>
    <td><?= $store->id ?></td>
    <td><?= $store->name ?></td>
    <?= getAddressRow($store) ?>
    <td>
        <? foreach ($store->locations as $location): ?>
            <a href="/admin/locations/display_update_form/<?= $location->locationID ?>"><?= $location->address ?></a><br>
        <? endforeach; ?>
        <? if ($store->matchedLocations): ?>
        <? foreach ($store->matchedLocations as $location): ?>
            <a href="/admin/locations/display_update_form/<?= $location->locationID ?>"><?= $location->address ?></a>
            <? if ($location->deliv_id): ?>
                <span style="color:red;">(<?php echo __("Miss Assigned", "Miss Assigned"); ?>)</span>
            <? else: ?>
                <span style="">(<?php echo __("Not Assigned", "Not Assigned"); ?>)</span>
            <? endif; ?>
            <br>
        <? endforeach; ?>
        <? endif; ?>
    </td>
</tr>
<? endforeach; ?>
</tbody>
</table>


<script>

$locations_dataTable = $("#deliv_locations").dataTable({
    "bPaginate" : false,
    "bJQueryUI": true,
    "fnStateLoad": function (oSettings) {
        return JSON.parse( localStorage.getItem('DataTables') );
    },
    "sDom": '<"H"lTfr>t<"F"ip>',
    "oTableTools": {
        <?php if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
    }

});

var header = new FixedHeader($locations_dataTable);

</script>
