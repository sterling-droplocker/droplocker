<?php enter_translation_domain("admin/home_delivery/settings"); ?>
<?php

$can_edit = !in_bizzie_mode() || is_superadmin();

?>
<h2><?php echo __("Manage Home Delivery Settings", "Manage Home Delivery Settings"); ?></h2>

<? if ($can_edit): ?>
    <form action='/admin/home_delivery/set_current_service' id='select_form' method='post'>
<? endif ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Delivery Service", "Delivery Service"); ?></th>
                <td>
                <? if ($can_edit): ?>
                    <?= form_dropdown('home_delivery_service', array('' => __("SELECT", "--SELECT--")) + $services, $current_service, 'id="processor"'); ?>
                <? else: ?>
                    <?= $current_processor ?>
                <? endif ?>
                </td>
            </tr>
        </table>
 <? if ($can_edit): ?>
        <input class='button orange' type='submit' value="<?php echo __("Update", "Update"); ?>" />
    </form>
<? endif ?>
<br><br>

<? if ($service): ?>
<? include "services/$service.php"; ?>
<? endif; ?>