<?php enter_translation_domain("admin/home_delivery/services/boxture"); ?>
<? if ($can_edit): ?>
<form action="/admin/home_delivery/update_settings/<?= $service ?>" method="post">
<? endif ?>
<table class='detail_table'>
        <tr>
            <th>
                <label for="boxture_api_url"><?php echo __("Live api url", "Live api url"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('boxture_api_url', $boxture_api_url, 'id="boxture_api_url"'); ?>
                <? else: ?>
                    <?= $boxture_api_url ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="boxture_api_url"><?php echo __("Live api key", "Live api key"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('boxture_api_key', $boxture_api_key, 'id="boxture_api_key"'); ?>
                <? else: ?>
                    <?= $boxture_api_key ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="boxture_qa_api_url"><?php echo __("Qa api url", "Qa api url"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('boxture_qa_api_url', $boxture_qa_api_url, 'id="boxture_qa_api_url"'); ?>
                <? else: ?>
                    <?= $boxture_qa_api_url ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="boxture_qa_api_url"><?php echo __("Qa api key", "Qa api key"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('boxture_qa_api_key', $boxture_qa_api_key, 'id="boxture_qa_api_key"'); ?>
                <? else: ?>
                    <?= $boxture_qa_api_key ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Show quantity selector", "Show quantity selector"); ?></th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_radios('boxture_show_quantity_selector', array('yes' => 'Yes', 'no' => 'No'), $boxture_show_quantity_selector); ?>
                <? else: ?>
                    <?= $boxture_show_quantity_selector ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Quantity selector options (comma separated)", "Quantity selector options (comma separated)"); ?></th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('boxture_quantity_selector_options', $boxture_quantity_selector_options); ?>
                <? else: ?>
                    <?= $boxture_quantity_selector_options ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Mode", "Mode"); ?></th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_radios('boxture_mode', array('live' => __("Live", "Live"), 'test' => __("Sandbox", "Sandbox")), $boxture_mode); ?>
                <? else: ?>
                    <?= $boxture_mode ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Debug", "Debug"); ?></th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_radios('home_delivery_enable_debug', array(false => __("No", "No"), true => __("Yes", "Yes")), $home_delivery_enable_debug); ?>
                <? else: ?>
                    <?= $home_delivery_enable_debug ?>
                <? endif ?>
            </td>
        </tr>

</table>
<? if ($can_edit): ?>
    <input class='button orange' type='submit' value="<?php echo __("Update", "Update"); ?>" />
</form>
<? endif ?>
