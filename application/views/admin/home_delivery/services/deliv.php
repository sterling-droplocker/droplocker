<?php enter_translation_domain("admin/home_delivery/services/deliv"); ?>
<? if ($can_edit): ?>
<form action="/admin/home_delivery/update_settings/<?= $service ?>" method="post">
<? endif ?>
<table class='detail_table'>
    <tr>
        <th><?php echo __("API Key", "API Key"); ?></th>
        <td>
            <? if ($can_edit): ?>
                <input type='text' name='deliv_api_key' value='<?= $deliv_api_key;?>' />
            <? else: ?>
                <?= $deliv_api_key ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Sandbox Key", "Sandbox Key"); ?></th>
        <td>
            <? if ($can_edit): ?>
                <input type='text' name='deliv_sandbox_key' value='<?= $deliv_sandbox_key;?>' />
            <? else: ?>
                <?= $deliv_sandbox_key ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Mode", "Mode"); ?></th>
        <td>
            <? if ($can_edit): ?>
                <?= form_radios('deliv_mode', array('live' => __("Live", "Live"), 'test' => __("Sandbox", "Sandbox")), $deliv_mode); ?>
            <? else: ?>
                <?= $deliv_mode ?>
            <? endif ?>
        </td>
	    <tr>
	        <th><?php echo __("Pickup/Delivery fee", "Pickup/Delivery fee"); ?></th>
	        <td>
	            <? if ($can_edit): ?>
	                <input type='text' name='deliv_fee' value='<?= $deliv_fee; ?>' style="width:100px" />
	            <? else: ?>
	                <?= $deliv_fee; ?>
	            <? endif ?>
	        </td>
	    </tr>
	    <tr>
	        <th><?php echo __("Ready By Time for Deliveries", "Ready By Time for Deliveries"); ?></th>
	        <td>
	            <? if ($can_edit): ?>
	                <input type='text' name='deliv_ready_by' value='<?= $deliv_ready_by; ?>' style="width:100px" /> Format HH:MM
	            <? else: ?>
	                <?= $deliv_ready_by; ?>
	            <? endif ?>
	        </td>
	    </tr>
    </tr>
        <tr>
            <th>
                <label for="deliv_window_notes"><?php echo __("Notes for Pickup Windows", "Notes for Pickup Windows"); ?></label>
            </th>
            <td>
                <table>
                    <tr>
                        <th>
                            <?php echo __("Days", "Days"); ?>
                            <div class="day-align">
                                <span><?php echo __("S", "S"); ?></span>
                                <span><?php echo __("M", "M"); ?></span>
                                <span><?php echo __("T", "T"); ?></span>
                                <span><?php echo __("W", "W"); ?></span>
                                <span><?php echo __("T", "T"); ?></span>
                                <span><?php echo __("F", "F"); ?></span>
                                <span><?php echo __("S", "S"); ?></span>
                            </div>
                        </th>
                        <th><?php echo __("Window", "Window"); ?></th>
                        <th><?php echo __("Notes", "Notes"); ?></th>
                    </tr>
                <?
                    if (empty($deliv_window_notes)) {
                        $deliv_window_notes = array();
                    } else {
                        $deliv_window_notes = unserialize($deliv_window_notes);
                    }
                    $deliv_window_notes[] = array('window' => '', 'days' => array(), 'notes' => '');
                    foreach($deliv_window_notes as $key => $value): ?>
                    <tr>
                    <? if ($can_edit): ?>
                        <td>
                            <? for ($i = 0; $i < 7 ; $i++): ?>
                            <?= form_checkbox(array(
                                'name' => "deliv_window_notes[$key][days][]",
                                'value' => $i,
                                'checked' => in_array($i, $value['days']),
                            )); ?>
                            <? endfor; ?>
                        </td>
                        <td>
                            <?= form_input(array(
                                'name' => "deliv_window_notes[$key][window]",
                                'value' => $value['window'],
                                'placeholder' => 'h:mm-h:mm',
                            )); ?>
                        </td>
                        <td>
                            <?= form_textarea(array(
                                'name' => "deliv_window_notes[$key][notes]",
                                'value' => $value['notes'],
                                'rows' => 1,
                                'placeholder' => 'notes',
                            )); ?>
                        </td>
                    <? else: ?>
                        <td><b><?= $key ?></b></td>
                        <td><?= $value ?></td>
                    <? endif ?>
                    </tr>
                <? endforeach; ?>
            </table>
                <p class="note"><?php echo __("Window format should be in 24hs format", "Window format should be in 24hs format, example: '9:00-15:00' for 9:00 AM to 3:00 PM.<br> Leave notes or days empty to delete an entry."); ?></p>
            </td>
        </tr>
</table>
 <? if ($can_edit): ?>
    <input class='button orange' type='submit' value="<?php echo __("Update", "Update"); ?>" />
</form>
<? endif ?>
