<?php enter_translation_domain("admin/home_delivery/services/rickshaw"); ?>
<? if ($can_edit): ?>
<form action="/admin/home_delivery/update_settings/<?= $service ?>" method="post">
<? endif ?>
<table class='detail_table'>
        <tr>
            <th>
                <label for="rickshaw_api_key"><?php echo __("Rickshaw API Key", "Rickshaw API Key"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('rickshaw_api_key', $rickshaw_api_key, 'id="rickshaw_api_key"'); ?>
                <? else: ?>
                    <?= $rickshaw_api_key ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Mode", "Mode"); ?></th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_radios('rickshaw_mode', array('live' => __("Live", "Live"), 'test' => __("Sandbox", "Sandbox")), $rickshaw_mode); ?>
                <? else: ?>
                    <?= $rickshaw_mode ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_contact_name"><?php echo __("Contact Name", "Contact Name"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('rickshaw_contact_name', $rickshaw_contact_name, 'id="rickshaw_contact_name"'); ?>
                <? else: ?>
                    <?= $rickshaw_contact_name ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_contact_phone"><?php echo __("Contact Phone", "Contact Phone"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input('rickshaw_contact_phone', $rickshaw_contact_phone, 'id="rickshaw_contact_phone"'); ?>
                <? else: ?>
                    <?= $rickshaw_contact_phone ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_pickup_notes"><?php echo __("Pickup Notes", "Pickup Notes"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_textarea(array(
                        'name' => 'rickshaw_pickup_notes',
                        'value' => $rickshaw_pickup_notes,
                        'id' => 'rickshaw_pickup_notes',
                        'rows' => 4
                    )); ?>
                <? else: ?>
                    <?= $rickshaw_pickup_notes ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_delivery_notes"><?php echo __("Delivery Notes", "Delivery Notes"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_textarea(array(
                        'name' => 'rickshaw_delivery_notes',
                        'value' => $rickshaw_delivery_notes,
                        'id' => 'rickshaw_delivery_notes',
                        'rows' => 4
                    )); ?>
                <? else: ?>
                    <?= $rickshaw_delivery_notes ?>
                <? endif ?>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_delivery_addresses"><?php echo __("Destination Addresses for Pickup Windows", "Destination Addresses<br>for Pickup Windows"); ?></label>
            </th>
            <td>
                <table>
                    <tr>
                        <th><?php echo __("Window", "Window"); ?></th>
                        <th><?php echo __("Address", "Address"); ?></th>
                    </tr>
                <?
                    if (empty($rickshaw_pickup_addresses)) {
                        $rickshaw_pickup_addresses = array();
                    } else {
                        $rickshaw_pickup_addresses = unserialize($rickshaw_pickup_addresses);
                    }
                    $rickshaw_pickup_addresses[] = array('window' => '', 'address' => '');
                    foreach($rickshaw_pickup_addresses as $key => $value): ?>
                    <tr>
                    <? if ($can_edit): ?>
                        <td>
                            <?= form_input(array(
                                'name' => "rickshaw_pickup_addresses[$key][window]",
                                'value' => $value['window'],
                                'placeholder' => 'h:mm-h:mm',
                            )); ?>
                        </td>
                        <td>
                            <?= form_textarea(array(
                                'name' => "rickshaw_pickup_addresses[$key][address]",
                                'value' => $value['address'],
                                'rows' => 1,
                                'placeholder' => 'address',
                            )); ?>
                        </td>
                    <? else: ?>
                        <td><b><?= $key ?></b></td>
                        <td><?= $value ?></td>
                    <? endif ?>
                    </tr>
                <? endforeach; ?>
            </table>
                <p class="note"><?php echo __("Window format should be in 24hs format", "Window format should be in 24hs format, example: '9:00-15:00' for 9:00 AM to 3:00 PM.<br> Leave address empty to remove a row."); ?></p>
            </td>
        </tr>
        <tr>
            <th>
                <label for="rickshaw_delivery_address"><?php echo __("Source Address for Deliveries", "Source Address<br>for Deliveries"); ?></label>
            </th>
            <td>
                <? if ($can_edit): ?>
                    <?= form_input(array(
                        'name' => 'rickshaw_delivery_address',
                        'value' => $rickshaw_delivery_address,
                        'id' => 'rickshaw_delivery_address',
                    )); ?>
                    <div><?php echo __("Orders will be picked up from here", "Orders will be picked up from here"); ?></div>
                <? else: ?>
                    <?= $rickshaw_delivery_address ?>
                <? endif ?>
            </td>
        </tr>
</table>
<? if ($can_edit): ?>
    <input class='button orange' type='submit' value="<?php echo __("Update", "Update"); ?>" />
</form>
<? endif ?>
