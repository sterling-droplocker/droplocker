<?php enter_translation_domain("admin/home_delivery/services/droplocker"); ?>

<br><br>

<? if (empty($locations)): ?>
<div class="warning">
	<h1><?php echo __("There are no valid locations defined for Home Delivery", "There are no valid locations defined for Home Delivery."); ?></h1>
	<p><?php echo __("Please setup at least one location", "Please <a href=\"/admin/locations/\">setup</a> at least one location with service type \"Time Window Home Delivery\" or \"Recurring Home Delviery\"."); ?></p>
</div>
<? return; ?>
<? endif; ?>

<div id="home-delivery-map">
<form action='/admin/home_delivery/add_geofences' id='geofences_form' method='post'>
	<div id="map-canvas-container" style="width: 50%; float: left;">
		<div id="map-canvas" style="height: 500px; margin: 0px; padding: 0px;"></div>
		<input class='button blue delivery-zone-add' value="<?php echo __("Add fence", "Add fence"); ?>">
		<input class="button orange" value="Save" type="<?php echo __("submit", "submit"); ?>">
	</div>
	<div class="map-tables-container" style="float:left; margin: 0px; padding: 0px; width: 50%;">
		<div class="delivery-zones-list" style="overflow:auto;margin: 0 0 0 10px;">
		</div>
	</div>
</form>
</div>



<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&sensor=false"></script> -->
<script src="/js/admin/home_delivery/droplocker.js?v=2"></script>
<script type="text/javascript">

/**
 * Load google maps asynchronously
 */
function loadMaps() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&callback=initializeMap&key=<?=get_default_gmaps_api_key();?>';
    document.body.appendChild(script);
}

function initializeMap() {

    var mapOptions = <?=json_encode(array("lat"=>$center->lat, "lon"=>$center->lon, "zoom"=>13))?>

	var settings = {};
	settings.locations = <?= json_encode($locations); ?>;
	settings.zones = <?= json_encode($delivery_zones); ?>;

    DropLockerHomeDeliveryService.initialize(mapOptions, settings);
}


$(function() {
    loadMaps();
});
</script>
