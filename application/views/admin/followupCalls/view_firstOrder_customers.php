<?php 
enter_translation_domain("admin/followupCalls/view_firstOrder_customers"); 
?>
<div style="display: none;" id="error_dialog" title="Error">
    <?php echo __("Could not retrieve customers who just placed an order", "Could not retrieve customers who just placed an order"); ?>
</div>

<div style="font-size: 1.8em; padding-top: 10px;"><?php echo __("First Order Customers", "First Order Customers"); ?> </div>



<table id="firstOrder_customers">
    <thead>
        <tr></tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" class="dataTables_empty"> <?php echo __("Loading data from server", "Loading data from server"); ?> </td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">

$.getJSON("/admin/followupCalls/get_firstOrder_customers_dataTable_columns", {}, function(columns) {
    aoColumns = columns;
    firstOrder_customers_dataTable = $("#firstOrder_customers").dataTable( {
        "aoColumns" : aoColumns,
        "bProcessing" : true,
        "bServerSide" : true,
        "sAjaxSource" : "/admin/followupCalls/get_firstOrder_customers_dataTable_data",
        "bStateSave" : true,
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>            
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    }).fnSetFilteringDelay(); 
    new FixedHeader(firstOrder_customers_dataTable);
}).error(function() {
        $("#error_dialog").dialog();
    });

</script>