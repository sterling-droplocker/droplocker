<?php enter_translation_domain("admin/followupCalls/generate_followup_call_script"); ?>
<?php

$fieldList = array();
$fieldList['firstName'] = form_label(__("First Name", "First Name"),"firstName") . form_input(array("name" => "firstName", "id" => "firstName", "value" => $customer->firstName));
$fieldList['lastName'] = form_label(__("Last Name", "Last Name"), "lastName") . form_input(array("name" => "lastName", "id" => "lastName", "value" => $customer->lastName));

$nameForm = getSortedNameForm($fieldList);
                                    
//------------------

$fieldList = array();
$fieldList['address1'] = form_label(__("Address", "Address"), "address") . form_input(array("name" => "address", 'id' => 'address', "value" => $customer->address1));
$fieldList['address2'] = '';
$fieldList['city'] = form_label(__("City", "City"), "city") . form_input(array("name" => "city", 'id' => 'city', "value" => $customer->city));
$fieldList['state'] = form_label(__("State", "State"), "state") . form_input(array("name" => "state", 'id' => 'state', "value" => $customer->state));
$fieldList['zip'] = form_label(__("Zip Code", "Zip Code"), "zip") . form_input(array("name" => "zip", "type" => 'number', 'id' => 'zip', "value" => $customer->zip));
        
$addressForm = getSortedAddressForm($fieldList, "</div><div>", 'global', 2);

?>
<?php
/**
 * Expects at least the following content variables:
 *  business stdClass
 *  defaultLocation stdClass
 *  lastOrder stdClass
 *  card stdClass
 *  pastCalls array
 *  customer_notes_history array
 */
?>
<style type="text/css">
    fieldset legend {
        font-weight: bold;
    }
    #logFollowupCallForm div {
        margin-top: 10px;
    }
    #logFollowupCallForm input {
        margin-right: 10px;
    }
    
    #updateCreditCardForm {
        max-width: 725px;
        min-width: 725px;
    }
    #updateCreditCardForm fieldset div {
        margin: 5px;
        padding: 10px;
        background: #febbbb; /* Old browsers */
        background: -moz-linear-gradient(left,  #febbbb 0%, #fe9090 45%, #ff5c5c 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, right top, color-stop(0%,#febbbb), color-stop(45%,#fe9090), color-stop(100%,#ff5c5c)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(left,  #febbbb 0%,#fe9090 45%,#ff5c5c 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(left,  #febbbb 0%,#fe9090 45%,#ff5c5c 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(left,  #febbbb 0%,#fe9090 45%,#ff5c5c 100%); /* IE10+ */
        background: linear-gradient(to right,  #febbbb 0%,#fe9090 45%,#ff5c5c 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#febbbb', endColorstr='#ff5c5c',GradientType=1 ); /* IE6-9 */
    }
    #updateCreditCardForm label {
        display: inline-block;
        width: 140px;
    }
</style>
<?php if ($this->business->businessID == 3): ?>
    <!-- The following javascript import is for Laundry Locker customer service functionality -->
    <script type="text/javascript" src="https://callsocket.brightpattern.com/app/lib/servicepatternapi-dev.js"> </script>
<?php endif ?>
<h2> <?php echo __("Follow Up Calls Script", "Follow Up Calls Script"); ?> </h2>

<div>
    <div style="width: 50%; float: left;">
        <table class="box-table-a">
            <tbody>
                <form action="" method="post">
                    <input type="hidden" name="customerID" value="<?= $customer->customerID ?>">
                    <input type="hidden" name="callType" value="<?= $followupCallType_id ?>">
                    <tr>
                        <td> <?php echo __("Name", "Name"); ?> </td>
                        <td> <a href="/admin/customers/detail/<?= $customer->customerID ?>" target="_blank"><?= getPersonName($customer) ?> </a> </td>
                    </tr>
                    <tr>
                        <td> <?php echo __("Phone", "Phone"); ?> </td>
                        <td> <input type="text" name="phone" value="<?= $customer->phone ?>"> <a href="#" id="phone_number_link"> <?= $customer->phone ?> </a> </td>
                    </tr>
                    <tr>
                        <td> <?php echo __("Address", "Address"); ?> </td>
                        <td>
                            <input type="text" name="address1" size="35" value="<?= $customer->address1 ?>"> <br />
                            <input type="text" name="address2" size="35" value="<?= $customer->address2 ?>">
                        </td>
                    </tr>
                    <tr>
                        <td> <?php echo __("Email", "Email"); ?> </td>
                        <td> <input type="text" name="email" size="35" value="<?= $customer->email ?>"> </td>
                    </tr>
                    <tr>
                        <td> <?php echo __("Customers Notes", "Customer's Notes"); ?> </td>
                        <td> <textarea cols="40" rows="4" name="custNotes"> <?= $customer->customerNotes ?> </textarea> </td>
                    </tr>
                    <tr>
                        <td> <?php echo __("Internal Notes", "Internal Notes"); ?> </td>
                        <td> <textarea cols="40" rows="4" name="intNotes"> <?= $customer->internalNotes ?> </textarea> </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right"> <input type="submit" name="update" value="<?php echo __("Update", "Update"); ?>" class="button blue"> </td>
                    </tr>
                </form>
            </tbody>
        </table>
        <hr />
        <h1> <?php echo __("Script", "Script"); ?> </h1>
        <div style="margin-top: 10px; font-style: italic;"> <?= $followup_call_script ?> </div>
    </div>
    <div style="width: 50%; float: left;">
        <table class="box-table-a">
            <tbody>
                <tr>
                    <td><?php echo __("number of Orders", "# of Orders:"); ?></td>
                    <td> <?= $ordCount ?> </td>
                    <td> <?php echo __("Amount Spent (gross)", "Amount Spent (gross):"); ?></td>
                    <td align="right"> $<?= $ordTot->gross ?> </td>
                </tr>
                <tr>
                    <td> <?php echo __("Credit Card", "Credit Card:"); ?> </td>
                    <td> 
                        <?= $card->cardNumber ?> exp <?= $card->expMo ?> / <?= $card->expYr ?> 
                        <?= form_input(array("type" => "button","id" => "updateCreditCardButton", "class" => "button blue", "value" => __("Authorize New Card", "Authorize New Card"))) ?> 
                        <!-- The following form is for authorizing  a new credit card for this customer -->
                        <?= form_open("/admin/creditCards/update", array("id" => "updateCreditCardForm", "style" => "display: none")) ?>
                            <?= form_fieldset(__("New Credit Card", "New Credit Card")) ?>
                        <div>
                            <div> <?= form_hidden("customerID", $customer->customerID) ?> </div>        
                            <div> <?= form_label(__("Card Number", "*Card Number"), "cardNumber") . form_input(array("name" => "cardNumber", "style" => "width: 16em;", "id" => "cardNumber", "required" => "required","type" => "text", "pattern" => "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$",)) ?> </div>
                            <div> <?= form_label(__("Expiration Month", "*Expiration Month"), "expMo") . form_input(array("name" => "expMo", "id" => "expMo", "type" => "number", "required" => "required", "min" => 1, "max" => 12, "style" => "width: 2em;", "value" => (int) date("n") + 1)) . form_label(__("Year", "* Year"), "expYr") . form_input(array("name" => "expYr", "style" => "width: 4em;","id" => "expYr", "type" => "number", "min" => date("Y"), "value" => date("Y"), "required" => "required")) ?> </div>
                            <div> <?= form_label(__("CSC", "*CSC"), "csc") . form_input(array("name" => "csc", "id" => 'csc', "style" => "width: 3em;", "type" => "number", "required" => "required")) . form_label(__("Kiosk Access", "Kiosk Access"), "kioskAccess") . form_checkbox(array("name" => "kioskAccess", "id" => "kioskAccess", "checked" => TRUE)) ?> </div>
                            <div> <?= $nameForm ?> </div>
                            <div> <?= $addressForm ?> </div>

                            <p> * <?php echo __("Required Fields", "Required Fields"); ?> </p>
                        </div>
                            <?= form_fieldset_close() ?>
                        <?= form_close() ?>
                    </td>
                    <td> <?php echo __("Discounts", "Discounts:"); ?> </td>
                    <td align="right"> $<?= $ordTot->disc ?> </td>

                </tr>
                <tr>
                    <td colspan="1"><?php echo __("Days Since Last Order", "Days Since Last Order:"); ?></td>
                    <td colspan="3" align="right">
                        <?php if(isset($lastOrder->dateCreated)) { 
                            $date_created = convert_from_gmt_aprax($lastOrder->dateCreated, "Y-m-d H:i:s");
                            $days_ago = date_diff(new DateTime($date_created), new DateTime());
                            echo $days_ago->format("%a days $date_created");
                        }
                        else { 
                            echo __("NO ORDERS", "NO ORDERS"); 
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td> <?php echo __("Default Location", "Default Location:"); ?> </td>
                    <td align='right'> <?= form_dropdown("location_id", $locations, $customer->location_id, "id='editableDefaultLocation' data-id='$customer->customerID' data-property='location_id'") ?> </td>
                    <!-- <td id="editable" align="right"> <?= $defaultLocation->address ?> </td> -->
            <td colspan="2" align="right"> <?php echo __("Type", "Type:"); ?> <div id="defaultLocationTypeName"> <?= $defaultLocation->locationTypeName ?> </div> <div id="defaultLocationDescription"> <?= $defaultLocation->description ?> </div> </td>
                </tr>
                <tr>
                    <td> <?php echo __("Last Order", "Last Order"); ?> </td>
                    <td colspan="3" align="right"> <?= htmlspecialchars($lastOrder->address) ?> (<?= $lastOrder->lockerType ?>) </td>
                </tr>
                <tr>
                    <td colspan="3"> <?php echo __("Signup Date", "Signup Date"); ?> </td>
                    <td align="right">
                        <?= convert_from_gmt_aprax($customer->signupDate, "Y-m-d H:i:s") ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"> <?php echo __("How", "How"); ?> </td>
                    <td> <?= htmlspecialchars($customer->how) ?> </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php echo __("Past Calls", "Past Calls:"); ?><br>
                        <table>
                            <thead>
                                <tr>
                                    <th> <?php echo __("Date Called", "Date Called"); ?> </th>
                                    <th> <?php echo __("Employee", "Employee"); ?> </th>
                                    <th> <?php echo __("Call Type", "Call Type"); ?> </th>
                                    <th> <?php echo __("Note", "Note"); ?> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <? foreach($pastCalls as $past_followupCall): ?>
                                <tr>
                                    <td><?= $past_followupCall->dateCalled ?></td>
                                    <td><?= getPersonName($past_followupCall) ?> </td>
                                    <td><?= $past_followupCall->call_type ?></td>
                                    <td><?= $past_followupCall->note ?></td>
                                </tr>
                                <? endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php echo __("Discounts", "Discounts:"); ?><br>
                        <table>
                            <thead>
                                <tr>
                                    <th> <?php echo __("Coupon Code", "Coupon Code"); ?> </th>
                                    <th> <?php echo __("Amount", "Amount"); ?> </th>
                                    <th> <?php echo __("Description", "Description"); ?> </th>
                                    <th> <?php echo __("Extended Description", "Extended Description"); ?> </th>
                                    <th> <?php echo __("Start Date", "Start Date"); ?> </th>
                                    <th> <?php echo __("Expire Date", "Expire Date"); ?> </th>
                                </tr>
                            </thead>
                            <? foreach($discounts as $discount): ?>
                            <tr>                           
                                <td><?= $discount->couponCode ?></td>
                                <td><?= $discount->amount ?> <?= $discount->amountType ?></td>
                                <td><?= $discount->description ?></td>
                                <td><?= $discount->extendedDesc ?></td>
                                <td> <?= $discount->origCreateDate ?> </td>
                                <td> <?= $discount->expireDate ?> </td>
                            </tr>
                            <? endforeach; ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php echo __("Notes History", "Notes History:"); ?><br>
                        <table>
                            <thead>
                                <tr>
                                    <th> <?php echo __("Employee", "Employee"); ?> </th>
                                    <th> <?php echo __("Date", "Date"); ?> </th>
                                    <th> <?php echo __("Type", "Type"); ?> </th>
                                    <th> <?php echo __("Note", "Note"); ?> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($history as $note_history): ?>
                                    <tr>
                                        <td><?= getPersonName($note_history) ?></td>
                                        <td><?= convert_from_gmt_aprax($note_history->timestamp) ?></td>
                                        <td><?= $note_history->historyType ?></td>
                                        <td><?= $note_history->note ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <?= form_open("/admin/followupCalls/log", array("id" => "logFollowupCallForm")) ?>
            <fieldset style="border: 1px solid black;">
                <legend> <?php echo __("Log Call", "Log Call"); ?> </legend>
                <div>
                    <?= form_label(__("Date Called", "Date Called"), "dateCalled") ?>
                    <?= form_input(array("name" => "dateCalled", "class" => 'date')) ?>
                    <?= form_label(__("Followup Call Type", "Followup Call Type"), "followupCallType_id") ?>
                    <?= form_dropdown("followupCallType_id", $followupCallType_ids, $followupCallType_id) ?> <!-- "3" Is the expected ID for first type calls -->
                    <?= form_hidden("customer_id", $customer->customerID) ?>
                </div>
                <div>
                    <?= form_label(__("Notes", "Notes"), "note") ?>
                    <?= form_textarea(array("name" => "note", "rows" => 3, "cols" => 25)) ?>
                </div>
                <?= form_submit(array("value" => "Log Call", "class" => "button blue", "style" => "margin-top: 5px;")) ?>
            </fieldset>
        <?= form_close() ?>

    </div>
</div>

<script type="text/javascript">
    $("#updateCreditCardButton").click(function() {
        $("#updateCreditCardForm").dialog({
            modal: true,
            title : "<?php echo __("Authorize Credit Card", "Authorize Credit Card"); ?>",
            width: 775, 
            height: 550,            
            buttons : {
                Authorize: {
                    "text" : "<?php echo __("Authorize", "Authorize"); ?>",
                    click : function() {
                        $("#updateCreditCardForm").submit();
                    },
                    "class" : "button orange"
                },
                Cancel : function() {
                    $(this).dialog("close");
                }
            }
        });
    })
    
    //The following conditional is for Laundry Locker customer service functionality.
    if (window.bpspat !== undefined && window.bpspat.api !== undefined && typeof(window.bpspat.api.dialNumber) === "function") {
        $("#phone_number_link").click(function() {
            //The following variable store the extracted digits from a phone number string.
            var phone_number_digits = $(this).text().match(/\d+/g).join("");
            window.bpspat.api.dialNumber(phone_number_digits);
        });
    } else {
        $("#phone_number_link").remove();
    }
    //The following statement initializes any input field wit a class of 'date' to an instantiation of the datetimepicker jQuery plugin. Each datetimepicker is then set to preselect the initial date of now
    $(".date").datetimepicker( {
        timeFormat: 'hh:mm:ss',
        dateFormat: "yy-mm-dd"
    }).datetimepicker("setDate", (new Date));
    
    //The following variable stores all the locations for the business in a codeigniter form dropdown helper compatiable format.
    var locations = $.parseJSON('<?=json_encode($locations, JSON_HEX_APOS)?>');
    $("#editableDefaultLocation").change(function() {
        var $loading = $("<img src='/images/progress.gif' />");

        $this = $(this);
        $(this).after($loading);

        var customerID = $(this).attr("data-id");
        var property = $(this).attr("data-property");
        var locationID = $(this).val();
        $.post("/admin/customers/update", {
            customerID : customerID,
            property : property,
            value : locationID
        }, function(response) {
            if (response.status !== "success") {
                alert(response.message);
            } else {
                $.get("/admin/locations/get", {
                    locationID : locationID,
                    locationType: true
                }, function(response) {
                    //The following statements update the default location type and description fields with the new location's properties
                    $("#defaultLocationTypeName").text(response.data.name); 
                    $("#defaultLocationDescription").text(response.data.description);
                });
      
            }
            $loading.remove();
            
        }, "JSON");
    });
</script>

