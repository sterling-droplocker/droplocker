<?php enter_translation_domain("admin/followupCalls/index"); ?>
<h2><?php echo __("Follow Up Calls", "Follow Up Calls"); ?> </h2>
<?= $this->session->flashdata("message") ?>

<script type="text/javascript">

$(document).ready( function() {
    $("#view_inactive_customers").click(function() {
        window.location.href = "/admin/followupCalls/view_inactive_customers/";
    });

    $("#view_new_customers").click(function() {
        window.location.href = "/admin/followupCalls/view_new_signups";
    });
    $("#view_firstOrder_customers").click(function() {
        window.location.href = "/admin/followupCalls/view_firstOrder_customers/";
    });
    
    $("#view_onhold_customers").click(function() {
        window.location.href= "/admin/followupCalls/view_onhold_customers";
    });
    
});
</script>
<style type="text/css">
    input[type="button"],input[type="submit"] {
        display: block;
        margin-top: 10px;
    }
</style>
<table id='box-table-a'>
    <thead>
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <th></th>
            <th><?php echo __("Call Script", "Call Script"); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <!-- Note, this corresponds to the "Inactive" follow up call type in the database." -->
            <td><?php echo __("Inactive Customers", "Inactive Customers"); ?></td>
            <td>
                
                <input type="button" class="button blue" id="view_inactive_customers" value="<?php echo __("View Inactive Customers", "View Inactive Customers"); ?>" />
            </td>
            <td>
                <?= form_open("/admin/followup_call_scripts/update") ?>
                    <?= form_hidden("followupCallType_id", $inactive_followupCallType_id) ?>
                    <textarea cols="50" rows="5" name="text"><?=$inactive_script_text ?></textarea>
                    <input type="submit" value="<?php echo __("Update Script", "Update Script"); ?>" class="button blue">
                <?= form_close() ?>
            </td>
        </tr>
        <tr>
            <!-- Note, this corresponds to the "New Customer" follow up call type in the database." -->
            <td><?php echo __("New Signups", "New Signups"); ?></td>
            <td>
                
                <input type="button" class="button blue" id="view_new_customers" value="<?php echo __("View New Customers", "View New Customers"); ?>" />
            </td>
            <td>
                <?= form_open("/admin/followup_call_scripts/update") ?>
                    <?= form_hidden("followupCallType_id", $new_customer_followupCallType_id) ?>
                    <textarea cols="50" rows="5" name="text"><?= $new_customer_script_text ?></textarea>
                    <input type="submit" value="<?php echo __("Update Script", "Update Script"); ?>" class="button blue">
                <?= form_close() ?>
            </td>      
        </tr>
        <tr>
            <!-- Note, this corresponds to the "First TIme Followup" call type in the database. -->
            <td><?php echo __("Follow Up After First Order", "Follow Up After First Order"); ?></td>
            <td>
                <input type="button" class="button blue" id="view_firstOrder_customers" value="<?php echo __("View First Order Customers", "View First Order Customers"); ?>" />
            </td>
            <td>
                <?= form_open("/admin/followup_call_scripts/update") ?>
                    <?= form_hidden("followupCallType_id", $first_time_followup_followupCallType_id) ?>
                    <textarea cols="50" rows="5" name="text"><?= $first_time_followup_script_text ?></textarea>
                    <input type="submit" value="<?php echo __("Update Script", "Update Script"); ?>" class="button blue">
                <?= form_close() ?>
                
            </td>
        </tr>
        <tr>
            <!-- Note, this corresponds to the "Payment Hold" followup call type in the database. -->
            <td> Payment Hold</td>
            <td> 
                <input type="button" class="button blue" id="view_onhold_customers" value="<?php echo __("View Onhold Customers", "View Onhold Customers"); ?>" /> 
            </td>
            <td> 
                <?= form_open("/admin/followup_call_scripts/update") ?>
                <?= form_hidden("followupCallType_id", $payment_hold_followupCallType_id) ?>
                <textarea cols="50" rows="5" name="text" ?> <?= $payment_hold_script_text ?> </textarea ?>
                <?= form_submit(array("value" => __("Update Script", "Update Script"), "class" => "button blue")) ?>
                <?= form_close() ?>
            </td>
        </tr>
    </tbody>
</table>
