<?php enter_translation_domain("admin/followupCalls/view_inactive_customers"); ?>
<style type="text/css">
    .editor_button {
        cursor: hand; cursor: pointer;
    }
</style>

<div style="display: none;" id="error_dialog" title="Error">
    <?php echo __("Could not retrieve inactive customers", "Could not retrieve inactive customers"); ?>
</div>
<div style="font-size: 1.8em; margin-top: 10px;"> <?php echo __("Inactive Customers", "Inactive Customers"); ?></div>

<div style="margin: 10px 0">
<?php echo __("Show customers inactive for at least", "Show customers inactive for at least"); ?> <?= form_input(array("id" => "threshold", "value" => 10, "data-oldValue" => 10)) ?> days.
</div>
<table id="inactive_customers">
    
    <thead>
        <tr></tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" class="dataTables_empty"> <?php echo __("Loading data from server", "Loading data from server"); ?> </td>
        </tr>
    </tbody>
</table>


<script type="text/javascript">
    
$accept = $("<img>").attr("src", "/images/icons/accept.png").attr("class", "editor_button");

$delete = $("<img>").attr("src", "/images/icons/delete.png").attr("class", "editor_button");    
    
var $inactive_customers_dataTable;

$accept.click(function() {
    $inactive_customers_dataTable.fnDraw();

    $.cookie("inactive_threshold", $("#threshold").val());

    $delete.detach();
    $accept.detach();
});

$delete.click(function() {
    $("#threshold").val($("#threshold").attr("data-oldValue"));
    $delete.detach();
    $accept.detach();
});

if ($.cookie("inactive_threshold")) {
    $("#threshold").val($.cookie("inactive_threshold")).attr("data-previousValue", $.cookie("inactive_threshold"));
}

$("#threshold").focus(function()
{
    $(this).attr("data-oldValue", $(this).val());
    $(this).after($delete);
    $(this).after($accept);
});

$.getJSON("/admin/followupCalls/get_inactive_customers_dataTable_columns", {}, function(columns) {
    aoColumns = columns;
    $inactive_customers_dataTable = $("#inactive_customers").dataTable( {
        "aoColumns" : aoColumns,
        "bProcessing" : true,
        "bServerSide" : true,
        "sAjaxSource" : "/admin/followupCalls/get_inactive_customers_dataTable_data",
        "bJQueryUI": true,
        "aaSorting" : [[ 1, "desc"]],
        "sDom": '<"H"lTfr>t<"F"ip>',
        "bStateSave" : true,
        "fnServerParams" : function(aoData) {
            aoData.push({"name" : "threshold", "value" : $("#threshold").val()});
        },
        "oTableTools": {
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
            aButtons : [
                "pdf", "print"
            ],
            <?php endif ?>                
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    }).fnSetFilteringDelay(); 
    new FixedHeader($inactive_customers_dataTable);

}).error(function() {
    $("#error_dialog").dialog();
});
</script>