<?php enter_translation_domain("admin/followupCalls/view_new_signups"); ?>
<div style="display: none;" id="error_dialog" title="Error">
    <?php echo __("Could not retrieve new signups", "Could not retrieve new signups"); ?>
</div>

<div style="font-size: 1.8em; padding-top: 10px;"><?php echo __("New Customers w/No Orders Yet", "New Customers w/No Orders Yet:"); ?> </div>

<table id="new_customers">
    <thead>
        <tr></tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" class="dataTables_empty"> <?php echo __("Loading data from server", "Loading data from server"); ?> </td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
$.getJSON("/admin/followupCalls/get_new_signups_dataTable_columns", {}, function(aoColumns) {
    var new_customers_dataTable = $("#new_customers").dataTable( {
        "aoColumns" : aoColumns,
        "bProcessing" : true,
        "bServerSide" : true,
        "sAjaxSource" : "/admin/followupCalls/get_new_signups_dataTable_data",
        "bJQueryUI": true,
        "sDom": '<"H"lTfr>t<"F"ip>',
        "bStateSave" : true,
        "oTableTools": {
            <?php if (in_bizzie_mode() && !is_superadmin()): ?>
            aButtons : [
                "pdf", "print"
            ],
            <?php endif ?>                
            "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
    }).fnSetFilteringDelay(); 
    new FixedHeader(new_customers_dataTable);
}).error(function() {
    $("#error_dialog").dialog();
});
</script>

