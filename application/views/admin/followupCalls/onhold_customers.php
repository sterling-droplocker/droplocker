<?php enter_translation_domain("admin/followupCalls/onhold_customers"); ?>
<div style="font-size: 1.8em; margin-top: 10px;"><?php echo __("Customers With Unpaid Orders", "Customers With Unpaid Orders"); ?></div>

<table id="hor-minimalist-a">
    <thead>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Amt Owed", "Amt Owed"); ?></th>
            <th><?php echo __("Last Called", "Last Called"); ?></th>
            <th><?php echo __("Call Script", "Call Script"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?foreach ($holdCustomers as $hc):?>
        <tr>
            <td><?= getPersonName($hc) ?></td>
            <td><?= format_money_symbol($this->business_id,'%.2n', $hc->amtOwed) ?></td>
            <td><? if($hc->lastCall) { echo convert_from_gmt_aprax($hc->lastCall, STANDARD_DATE_FORMAT); } ?></td>
            <td><a href='/admin/followupCalls/generate_followup_call_script?customerID=<?= $hc->customer_id?>&followupCallType_id=4'> <?php echo __("Generate Call Script", "Generate Call Script"); ?> </a></td>
        </tr>
        <? endforeach; ?>
    </tbody>
</table>
