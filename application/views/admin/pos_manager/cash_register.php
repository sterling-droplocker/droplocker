<?php enter_translation_domain("/admin/pos_manager/cash_register"); ?>
<?php
$delete_enabled = $allow_delete && is_superadmin();
?>

<!-- item new --------------------------------------------------------------------------------------------------------->

<script type="text/javascript">
$(document).ready(function(){
$(":submit").loading_indicator();

$("form").validate( {
    rules: {
        name : {
            required: true
        },location_id : {
            required: true
        }
    },
    errorClass: "invalid",
    invalidHandler: function(form, validator)
    {
        $(":submit").loading_indicator("destroy");
    }
});
    
});
</script>
<h2> <?php echo __("Create New Cash Register", "Create New Cash Register"); ?> </h2>

<?= form_open("/admin/pos_manager/cash_register_add") ?>
<table class="detail_table">
    <tr>
        <th width="200"><label><?php echo __("Cash Register Name", "Cash Register Name"); ?></label></th>
        <td><span class="fill"><?= form_input("name") ?></span> </td>
    </tr>
    <tr>
        <th><label><?php echo __("Notes", "Notes"); ?></label></th>
        <td><span class="fill"><?= form_textarea(array("name" => "notes", "rows" => 4, "cols" => 10)) ?></span></td>
    </tr>
    <tr>
        <th width="200"><label><?php echo __("Location", "Location"); ?></label></th>
        <td>
            <span class="fill">
                <?= form_dropdown('location_id', $locations, null, 'id="location_id_select"'); ?>
            </span> 
        </td>
    </tr>
</table>
<?= form_submit(array("class" => "button orange", "value" => __("Create", "Create"))) ?>
<?= form_close() ?>

<!-- item list --------------------------------------------------------------------------------------------------------->

<h2>Cash Registers</h2>
<?= $this->session->flashdata('message'); ?>
<br>

<table id="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <th><?php echo __("Notes", "Notes"); ?></th>
            <th><?php echo __("Location", "Location"); ?></th>
            
            <? if ($allow_edit): ?>
            <th align="center" width='25'><?php echo __("Edit", "Edit"); ?></th>
            <? endif; ?>
            <? if ($delete_enabled): ?>
            <th align="center" width='25'><?php echo __("Delete", "Delete"); ?></th>
            <? endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($cash_registers as $item): ?>    
        <tr>
            <td><?= $item->name ?></td>
            <td><?= $item->notes ?></td>
            <td><?= $item->address ?></td>
            <? if ($allow_edit): ?>
            <td align="center"><a href='/admin/pos_manager/cash_register_edit/<?= $item->cash_registerID ?>'><img src='/images/icons/application_edit.png' /></a></td>
            <? endif; ?>
            <? if ($delete_enabled): ?>
            <td align="center"><a href='/admin/pos_manager/cash_register_delete/<?= $item->cash_registerID ?>' onclick="return verify_delete();" id='del-<?= $item->cash_registerID ?>'><img src='/images/icons/cross.png'></a></td>
            <? endif; ?>
        </tr>
        <? endforeach;?>
    </tbody>
</table>
