<?php enter_translation_domain("admin/holidays/holidays"); ?>
<script type="text/javascript">
$(document).ready(function(){
    $("input[name=date]").datepicker({
        showOn: "button",
        buttonImageOnly: true,
        buttonImage: "/images/icons/calendar.png",
        dateFormat : "mm/dd/yy"
    });

    $("input[name=date]").on("click", function(){
        $(".ui-datepicker-trigger").click();
    }) ;  
});
</script>
<h2><?php echo __("Holiday Schedule", "Holiday Schedule"); ?></h2>
<br>
<br>
<a href="/admin/holidays?old=1"><?php echo __("show past holidays", "show past holidays"); ?></a>
<br>
<table class='detail_table'>
<tr>
    <th><?php echo __("Holiday Name", "Holiday Name"); ?></th>
    <th><?php echo __("Date Closed", "Date Closed"); ?></th>
    <th><?php echo __("Pickups", "Pickups"); ?></th>
    <th><?php echo __("Deliveries", "Deliveries"); ?></th>
    <th></th>
</tr>
<tr>
    <form action="/admin/holidays/add" method="post">
        <td><input type="text" name="name"></td>
        <td><input type="text" name="date" style="width:10em;"></td>
        <td><input type="checkbox" name="pickup" value="1"></td>
        <td><input type="checkbox" name="deliver" value="1"></td>
        <td><input type="submit" name="Add" value="<?php echo __("Add", "Add"); ?>" class="button orange"></td>
    </form>
</tr>
<? foreach($holidays as $holiday): ?>
<tr>
    <td><?= $holiday->name; ?></td>
    <td><?= date("D m/d/Y", strtotime($holiday->date)) ?></td>
    <td><?= $holiday->pickup == 1 ? __("Yes", "Yes") : __("No", "No"); ?></td>
    <td><?= $holiday->delivery == 1 ? __("Yes", "Yes") : __("No", "No"); ?></td>
    <td><a href="/admin/holidays/delete?holidayID=<?= $holiday->holidayID ?>"><?php echo __("Delete", "Delete"); ?></a></td>
</tr><
<? endforeach ?>
</table>