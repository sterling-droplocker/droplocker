<?php enter_translation_domain("admin/invoice_custom_fields/edit"); ?>
<h2><?php echo __("Edit Custom Field", "Edit Custom Field"); ?></h2>
<div style="padding: 10px;">
    <a href="/admin/invoice_custom_fields"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Invoice Custom Fields Listing", "Back To Invoice Custom Fields Listing"); ?></a>
</div>

<form action='' method='post'>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Name", "Name"); ?></th>
            <td>
                <?= form_input('name', $field->name); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Display Title and Value", "Display Title and Value"); ?></th>
            <td>
                <table style="width:100%;">
                <? foreach($languages as $businessLanguageID => $name):
                    $title = isset($values[$businessLanguageID]) ? $values[$businessLanguageID]->properties['title'] : null;
                    $value = isset($values[$businessLanguageID]) ? $values[$businessLanguageID]->properties['value'] : null;
                ?>

                <tr>
                    <td style="width:100px;">
                        <label for="title_<?= $businessLanguageID ?>"><strong><?= $languages[$businessLanguageID] ?></strong></label>

                        <table style="margin: 10px 0;">
                            <tr>
                                <td>
                                    <label><?php echo __("Title", "Title"); ?></label>
                                    <?= form_input("title[$businessLanguageID]", $title, "id=\"title_{$businessLanguageID}\"" ); ?>
                                </td>
                                <td>
                                <label><?php echo __("Value", "Value"); ?></label>
                                <?= form_input("value[$businessLanguageID]", $value, "id=\"value_{$businessLanguageID}\""); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <? endforeach; ?>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Sort Order", "Sort Order"); ?></th>
            <td>
                <?= form_input('sortOrder', $field->sortOrder, 'style="width:100px;"'); ?>
            </td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Save", "Save"); ?>' class='button orange' />
</form>