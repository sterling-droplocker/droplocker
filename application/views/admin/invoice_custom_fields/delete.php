<?php enter_translation_domain("admin/invoice_custom_fields/delete"); ?>
<h2><?php echo __("Delete Invoice Custom Field", "Delete Invoice Custom Field"); ?></h2>
<div style="padding: 10px;">
	<a href="/admin/invoice_custom_fields"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Custom Fields Listing", "Back To Custom Fields Listing"); ?></a>
</div>

<br><br>
<form action='' method='post'>
	<h3><?php echo __("Are you sure you want to delete the custom field", "Are you sure you want to delete the custom field"); ?> <b><?= htmlentities($field->name); ?></b> ?</h3>
	<br><br>
	<input type='submit' name='delete' value='<?php echo __("Delete", "Delete"); ?>' class='button orange' />
</form>
