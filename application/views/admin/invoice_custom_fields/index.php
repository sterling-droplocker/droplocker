<?php enter_translation_domain("admin/invoice_custom_fields/index"); ?>
<h2><?php echo __("Custom Fields", "Custom Fields"); ?></h2>
<p>
    <a href="/admin/reports/invoicing"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Invoicing", "Back To Invoicing"); ?></a> |
    <a href="/admin/invoice_custom_fields/add"><img src="/images/icons/add.png"> <?php echo __("Add New Custom Field", "Add New Custom Field"); ?></a>
</p>
<br><br>
<?php if (!empty($fields)): ?>
<table id="hor-minimalist-a">
<thead>
	<tr>
		<th><?php echo __("Name", "Name"); ?></th>
		<th><?php echo __("Sort Order", "Sort Order"); ?></th>
		<th width="25"></th>
		<th width="25"></th>
	</tr>
</thead>
<tbody>
<?php foreach($fields as $i => $field): ?>
	<tr>
		<td><?= htmlentities($field->name) ?></td>
		<td><?= $field->sortOrder ?></td>
		<td><a href='/admin/invoice_custom_fields/edit/<?= htmlentities($field->invoiceCustomFieldID) ?>' ><img src='/images/icons/pencil.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
		<td><a href='/admin/invoice_custom_fields/delete/<?= htmlentities($field->invoiceCustomFieldID) ?>' ><img src='/images/icons/cross.png' alt='<?php echo __("edit", "edit"); ?>' /></a></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<p class="message warning"> <?php echo __("There are no custom fields defined", "There are no custom fields defined."); ?> </p>
<?php endif;?>
