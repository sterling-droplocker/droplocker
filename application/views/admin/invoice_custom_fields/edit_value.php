<?php enter_translation_domain("admin/invoice_custom_fields/edit_value"); ?>
<h2><?php echo __("Edit Value", "Edit Value"); ?></h2>
<div style="padding: 10px;">
    <a href="/admin/custom_options/edit/<?= $value->customOption_id ?>"><img src="/images/icons/arrow_left.png"> <?php echo __("Back To Custom Option", "Back To Custom Option"); ?></a>
</div>

<form action='' method='post'>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Value", "Value"); ?></th>
            <td>
                <?= form_input('value', $value->value); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Display Title", "Display Title"); ?></th>
            <td>
                <table style="width:100%;">
                <? foreach($languages as $businesLanguageID => $name):
                    $title = isset($valueNames[$businesLanguageID]) ? $valueNames[$businesLanguageID]->name : null;
                ?>
                <tr>
                    <td style="width:100px;"><label for="value_title_<?= $businesLanguageID ?>"><?= $name ?></label></td>
                    <td><?= form_input("title[$businesLanguageID]", $title, "id=\"value_title_{$businesLanguageID}\""); ?></td>
                </tr>
                <? endforeach; ?>
                </table>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default", "Default"); ?></th>
            <td><?php echo __("Make this option the default", "Make this option the default"); ?><br>
                <?= form_yes_no('default', $value->default); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Sort Order", "Sort Order"); ?></th>
            <td>
                <?= form_input('sortOrder', $value->sortOrder, 'style="width:100px;"'); ?>
            </td>
        </tr>
    </table>
    <input type='submit' name='submit' value='<?php echo __("Update Value", "Update Value"); ?>' class='button orange' />
</form>

</div>
