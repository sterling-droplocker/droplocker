<?php enter_translation_domain("admin/print_receipts"); ?>
<?php
/**
 * Note, the following view file is undocumented and unknown  .
 * It is not known all the exepcted content variables that must be set.
 */

use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Barcode;
$full_base = get_full_base_url();

!isset($request['auto']) && $request['auto'] = 'no';

if(!isset($request['business_id'])){
    $business_id = $this->business_id;
} else {
    $business_id = $request['business_id'];
}
$business = new Business($business_id);
$website_url = '';
$hideURLOnReceipt = get_business_meta($business->businessID, "hideURLOnReceipt");
if($business->website_url != '' &&  !$hideURLOnReceipt){
    $website_url = (in_bizzie_mode() || get_business_meta($business->businessID, "hideWWWOnURLOnReceipt")) ? $business->website_url : 'www.'.$business->website_url;
}

//get the logo for receipts
$CI = get_instance();
$CI->load->model('image_placement_model');
$CI->load->model('image_model');

$receipt_image = $CI->image_placement_model->get_by_name("receipt", $business_id);
if (!$receipt_image) {
    die("<h1 style='color:red;'>No 'receipt' image set for business.</h1>");
}

$pos_image = $CI->image_placement_model->get_by_name("POS receipt", $business_id);
$hide_business_in_pos = get_business_meta($business_id, 'hide_business_in_pos');
$print_barcode = get_business_meta($business_id, 'print_barcode_pos', 'bag');
$duplicate_receipt_pos = get_business_meta($business_id, 'duplicate_receipt_pos');

$items_limit = get_business_meta($business_id, 'max_receipt_items', 12);
$show_business_address_on_receipts = get_business_meta($business_id, 'show_business_address_on_receipts');

if($request['routes']) {
    if($request['routes'] == 'x') { $noNav = 0; } else { $noNav = 1; }
    $title = 'Print Receipts';
    $title = __($title, $title);
}
else {
    echo '<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>';
}

if(strtolower($request['barcode']) == 'tags') {
    $number = ($number_to_print)?$number_to_print:24;
    $content = array();
    $content['business'] = $business;
    $content['lastBag'] = $lastBag = Bag::get_max_bagNumber() +1;
    $content['endBag'] = $lastBag + $number -1;
    $content['include_date'] = $include_date;

    Bag::set_max_bagNumber($content['endBag']);

    $this->load->view('print/temp_tags', $content);
    return;
}

if(!$request['barcode']) { $request['barcode'] = ''; }

if($request['showYesterday'] == 1) {
    $printDate = date("Y-m-d", strtotime("-1 Day"));
}
else if(!empty($request['orderDate'])) {
    $printDate = $request['orderDate'];
}


if (!empty($request['dateBegin'])) {
    $dateBegin = $request['dateBegin'];
}

if (!empty($request['dateEnd'])) {
    $dateEnd = $request['dateEnd'];
}

/*else
{
    $printDate = date("Y-m-d");
}*/

if ($request['ignore_loaded']) {
    $ignoreLoaded = $request['ignore_loaded'];
} else {
    $ignoreLoaded = 0;
}

$request['short'] = 0;

if($request['routes']) {
    if($request['routes'] == 'x') {
        $select4 = "select max(route_id) as numRoutes from location where business_id = ".$business_id;
        $routes = $this->db->query($select4);
        $routes = $routes->result();
        echo '<table width="100%" cellspacing="0" cellpadding="0"><tr><td>'.__("select_routes_print", "Select routes to print (hold down CTRL key to select multiple routes)").'</td></tr></table>';
        echo '<form action="printReceipts.php" method="post">
        <select name="routes[]" multiple>';
                for ($i = 0; $i <= $routes[0]->numRoutes; $i++)
                        {
                                echo '<option value="'.$i.'"';
                                if ($request['route'] == $i) { echo ' selected'; }
                                echo '>'.$i.'</option>';
                        }
        echo '</select>';
        //echo '<input type="checkbox" name="showYesterday" value="1">ShowYesterday';
        echo '<input type="hidden" name="dc" value="'.$request['dc'].'">';
        echo '<input type="hidden" name="ordDate" value="'.$request['ordDate'].'">';
        echo '<input type="submit" value="Show Receipts">';
        exit;
    }
}
//if a barcode is passed, find out what open order that barcode is attached to
if($request['barcode']) {
    $barcode = $request['barcode'];
    $barcodeObj = Barcode::search(array('barcode'=>$barcode, 'business_id'=>$business_id));

    if(strlen($request['barcode']) == 8 or strlen($request['barcode']) == 10) {
        if(!$barcodeObj) {
            echo '
                <div style="FLOAT: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                <br>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                <tr>
                        <td align="center">
                                <font size="5">'.__("Barcode Not Found", "Barcode Not Found<br/>").'' . date('M. d Y') . '</font>
                                <br><br>
                        </td>
                </tr>
                </table>';
            exit;
        }

        $currentOrderId = $order_id = $barcodeObj->getMostRecentOrder($business_id);
        $request['order'] = $currentOrderId;

        if (empty($currentOrderId)) {
            $customer = $barcodeObj->getCustomer($business_id);

            if (!$customer){
                echo'
                        <div style="FLOAT: left; page-break-after:always; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                        <br>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                        <tr>
                                <td align="center">
                                        <font size="5">'.__("Barcode Not Found", "<br/>").'' . date('M. d Y') . '</font>
                                        <br><br>
                                </td>
                        </tr>
                        </table>';
            }
            else {
                echo'
                        <div style="FLOAT: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                        <br>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                        <tr>
                                <td align="center">
                                        <font size="5">'.__("Barcode Not Assigned to an Order", "Barcode Not Assigned to an Order<br/>It most likely belongs to %customer_first_name% %customer_last_name%<br/>%date%", array(
                                            "customer_first_name"=>$customer[0]->firstName, 
                                            "customer_last_name"=>$customer[0]->lastName, 
                                            "date"=>date('M. d Y')
                                            )
                                          ).'
                                        </font>
                                        <br><br>
                                </td>
                        </tr>
                        </table>';
            }
            exit;
        }
        else {
            //mark the item as manually scanned
            $insert1="insert into automat (order_id, garcode, gardesc, fileDate)
                    values ($currentOrderId, $barcode, 'Scanned at Receipt Printer', now())";
            $insert = $this->db->query($insert1);
        }
        $request['short'] = 1;
    }
    else if(strlen($request['barcode']) == '7') {
        $query1="SELECT max(orderID) as currOrder
                FROM bag
                JOIN orders on orders.bag_id = bagID
                where bagNumber = '$barcode'
            and orders.business_id = ".$business_id;
        $query = $this->db->query($query1);
        $line = $query->result();
        $currentOrderId = $line[0]->currOrder;

        if (!$currentOrderId) {
                echo ''.__("bag not found", "bag not found").'';
                exit;
        } else {
                //mark the item as manually scanned
                $insert1="insert into automat (order_id, garcode, gardesc, fileDate)
                        values ($currentOrderId, $barcode, 'Scanned at Receipt Printer', now())";
                $insert = $this->db->query($insert1);
        }
        $request['short'] = 2;
        $request['order'] = $currentOrderId;
    }
    else {
        echo ''.__("barcode is wrong length", "barcode is wrong length").'';
        exit;
    }

}

if($request['order']) {
    $query1 = "SELECT orderID as ordId,
                                       ticketNum,
                                       orders.customer_id,
                                       orderStatusOption.name as orderStatus,
                                       orderStatusOption.name,
                                       postAssembly,
                                       orders.dateCreated,
                                       orders.notes as orderNotes,
                                       bag.bagNumber,
                                       bag.notes as bagNotes,
                                       location.address,
                                       location.business_id,
                                       location.accessCode,
                                       locationID as dropId,
                                       lockerName,
                                       customer.firstName,
                                       customer.lastName,
                                       customer.email,
                                       customer.phone,
                                       customer.invoice,
                                       customer.address1,
                                       customer.address2,
                                       location.serviceType,
                                       location.route_id,
                                       location.sortOrder,
                                       location.wholesale_logo_id,
                                       orders.split,
                                       locationType.name locationType
                    FROM orders
                    LEFT JOIN customer on customerID = orders.customer_id
                    LEFT JOIN locker on lockerID = orders.locker_id
                    LEFT JOIN location on locationID = locker.location_id
                    LEFT JOIN locationType on locationTypeID = location.locationType_id
                    LEFT JOIN bag on bagID = orders.bag_id
                    LEFT JOIN orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
                    LEFT JOIN orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id
                    WHERE orderID = {$request['order']}
        and orders.business_id = ".$business_id;
    if ($ignoreLoaded) {
        $query1 .= " AND orders.orderStatusOption_id != 14 
                            AND orders.loaded != 1";
    }

    $query1 .= " ORDER By firstName, lastName";
}
else {
    if($request['routes']) {
        $printRoutes = '';
        $request['short'] = 2;
        foreach ($request['routes'] as $t) {
            $printRoutes .= $t .',';

            //empty out the assembly table for that route;
            $delete1 = "delete from assembly where
                            route_id = $t
                            and business_id = ".$business_id.";";
/*			$delete1 = "delete from assembly where
                            route_id = $t
                            and cleaner_id = $empInfo->cleaner_id
                            and printDate <> '$printDate';";
*/
            $delete = $this->db->query($delete1);
        }
        $printRoutes = substr($printRoutes, 0, -1);

        $routeField = empty($_POST['master_sort']) ? 'route_id' : 'masterRoute_id';
        $orderField = empty($_POST['master_sort']) ? 'sortOrder' : 'masterSortOrder';

        $query1 = "SELECT orderID as ordId,
                                          ticketNum,
                                          orders.customer_id,
                                          orderStatusOption.name as orderStatus,
                                          orderStatusOption.name,
                                          bag.bagNumber,
                                          location.address,
                                          location.business_id,
                                          location.accessCode,
                                          locationID,
                                          lockerName,
                                          customer.firstName,
                                          customer.lastName,
                                          customer.email,
                                          customer.invoice,
                                          customer.phone,
                                          customer.address1,
                                          customer.address2,
                                          location.serviceType,
                                          location.wholesale_logo_id,
                                          COALESCE(route, location.$routeField) AS route_id,
                                          COALESCE(orderHomeDelivery.sortOrder, location.$orderField) AS sortOrder,
                                          orders.split,
                                          locationType.name as locationType
                        FROM orders
                        LEFT JOIN customer on customerID = orders.customer_id
                        INNER JOIN locker on lockerID = orders.locker_id
                        INNER JOIN location on locationID = locker.location_id
                        INNER JOIN locationType on locationTypeID = locationType_id
                        INNER JOIN bag on bagID = orders.bag_id
                        INNER JOIN orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
                        INNER JOIN orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id
                        LEFT JOIN delivery_zone ON (locationID = delivery_zone.location_id)
                        LEFT JOIN delivery_window ON (delivery_zoneID = delivery_window.delivery_zone_id)
                        LEFT JOIN orderHomeDelivery ON (orderID = orderHomeDelivery.order_id)
                        WHERE orderStatusOption.name = 'inventoried'
                        AND orders.business_id = ".$business_id;

        if ($ignoreLoaded) {
            $query1 .= " AND orders.orderStatusOption_id != 14 
                            AND orders.loaded != 1";
        }

        if (!empty($printDate) && empty($dateBegin) && empty($dateEnd)) {
            $query1 .= " and date_format(orders.dateCreated, '%Y-%m-%d') = '". convert_from_local_to_gmt($printDate) ."'";
        }

        if (!empty($dateBegin)) {
            $dateBeginUse = convert_from_local_to_gmt($dateBegin . ' 00:00:00', "Y-m-d H:i:s");
            $query1 .= " AND orders.dateCreated >= '" . $dateBeginUse . "'";
        }

        if (!empty($dateEnd)) {
            $dateEndUse = convert_from_local_to_gmt($dateEnd . ' 00:00:00', "Y-m-d H:i:s");
            $query1 .= " AND orders.dateCreated <= '" . $dateEndUse . "'";
        }

        $query1 .= " GROUP BY orderID";
        $query1 .= " HAVING route_id IN($printRoutes)";
        $query1 .= " ORDER BY route_id, sortOrder, lastName";
        //echo $query1;
    }
    else {
        $query1 = "SELECT orderID as ordId,
                                              ticketNum,
                                              orders.customer_id,
                                              orderStatusOption.name as orderStatus ,
                                              orderStatusOption.name, bag.bagNumber,
                                              location.address,
                                              location.business_id,
                                              location.accessCode,
                                              locationID as dropId,
                                              locker.lockerName,
                                              customer.firstName,
                                              customer.lastName,
                                              customer.invoice,
                                              customer.email,
                                              customer.phone,
                                              customer.address1,
                                              customer.address2,
                                              location.serviceType,
                                              location.route_id,
                                              location.sortOrder,
                                              location.wholesale_logo_id,
                                              orders.split,
                                              locationType.name as locationType
            FROM orders
            left join customer on customerID = orders.customer_id
            inner join locker on lockerID = orders.locker_id
            inner join location on locationID = locker.location_id
            inner join locationType on locationTypeID = locationType_id
            inner join bag on bagID = orders.bag_id
            inner join orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
            inner join orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id
            where orderStatusOption.name = 'inventoried' ";

        if (!empty($request['orderDate'])) {
            $query1 .= "and date_format(CONVERT_TZ(orders.dateCreated, 'GMT', '$business->timezone'), '%Y-%m-%d') = '".$printDate."' ";
        } else {
            //last 30 days
            $query1 .= "and date_format(CONVERT_TZ(orders.dateCreated, 'GMT', '$business->timezone'), '%Y-%m-%d') BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ";
        }
        
        
        $query1 .= "and orders.business_id = ".$business_id;
        if ($ignoreLoaded) {
            $query1 .= " AND orders.orderStatusOption_id != 14 
                            AND orders.loaded != 1";
        }

        $query1 .= " order by route_id, firstName, lastName";
    }
} // end of if  request['order]
    $route = '';

    $query = $this->db->query($query1);
    $lines = $query->result();
    if(!$lines) {
        die(__("Order not found", "Order not found"));
    }


$locale_date_format = get_business_meta($lines[0]->business_id, 'fullDateLocaleFormat', '%a, %b %d %Y');
$date_format = get_business_meta($lines[0]->business_id, "shortDateDisplayFormat");


foreach ($lines as $line) {

    // allow admin to override short receipts on certain customers
    $detailed_receipt = get_customer_meta($line->customer_id, 'detailed_receipt');

    // whe the order is in a point of sale location
    $is_pos = $line->locationType == 'Point of Sale';

    $image = $receipt_image;

    // in a POS location, use custom POS locaiton image if available
    if ($is_pos && $pos_image) {
        $image = $pos_image;
    }

    // if enabled, hide the business website and phone in POS locations
    $hide_business = $is_pos && $hide_business_in_pos;

    //If this is a wholesale linens customer, just print a label with the product name on it
    if($line->invoice == 1 && !$detailed_receipt) {
            $request['short'] = 2;
    }

    if (!empty($request['show_price'])) {
        $request['short'] = 0;
    }

    // Arik - code to print the receipt on a small receipt printer
    $order = new Order($line->ordId, FALSE);
    $locker = new Locker($order->locker_id, FALSE);

    $items = '';
    $numToPrint = 1;
    $totQty = 0;
    $itemList = "";
    $ordType = $order->getOrderType();

    $initials = '';
    $inventoriedEmployee = $order->getInventoryEmployee();
    if(!empty($inventoriedEmployee)) {
        $initials = mb_substr($inventoriedEmployee->firstName, 0, 1).mb_substr($inventoriedEmployee->lastName, 0, 1);
    }

    //load up the items list
    $query2 = "SELECT orderItem.unitPrice,
        sum(orderItem.qty) as qty,
        product.displayName,
        supplier_id,
        orderItem.notes,
        product.unitOfMeasurement
        FROM orderItem
        JOIN product_process pp ON product_processID = orderItem.product_process_id
        JOIN product on productID = pp.product_id
        WHERE orderItem.order_id = {$order->orderID}
        GROUP BY product.displayName, orderItem.unitPrice";

    $query2 = $this->db->query($query2);
    $get_all_items_query_result = $query2->result();

	$box_label = get_translation_for_view('box', '(Box)');

    // Get wholesale logo if available
    if (!empty($line->wholesale_logo_id)) {
        $image = $CI->image_model->get_by_id($line->wholesale_logo_id, $business_id);
    }

    foreach ( $get_all_items_query_result as $line2) {
        $totQty = $totQty + $line2->qty;
        if($request['auto'] != 'auto') {
            if($request['short'] > 0) {
                $items .= '<tr>
                 <td>';
                 if (mb_stristr($line2->notes, "box")) { $items .= mb_substr($line2->name,0,25).' ' . $box_label; }
                 else if(mb_stristr($line2->displayName, "Linen - ")) { $items .= mb_substr($line2->displayName,8,30); }
                 else {  $items .= mb_substr($line2->displayName,0,30); }

                $items .= '</td>
                <td align="left" valign="top" nowrap>'.OrderItem_Model::getRoundedQty($business_id, $line2->qty, $line2->unitOfMeasurement).' </td>
                </tr>
                <tr>
                       <td height="5px"></td>
                </tr>';
            }
            else {
                $items .= '<tr>
                 <td>';

                 if (mb_stristr($line2->notes, "box")) { $items .= mb_substr($line2->name,0,7).' ' . $box_label; }
                 else if(mb_stristr($line2->displayName, "Linen - ")) { $items .= mb_substr($line2->displayName,8,30); }
                 else {  $items .= mb_substr($line2->displayName,0,13); }

                 $items .='</td>
                 <td align="left" valign="top" nowrap>'.format_money_symbol($business_id, '%.2n', $line2->unitPrice, 2, array('shrink_money_symbol' => 1)).' </td>
                 <td align="left" valign="top" nowrap>'.OrderItem_Model::getRoundedQty($business_id, $line2->qty, $line2->unitOfMeasurement).' </td>
                 <td align="left" valign="top" nowrap>'.format_money_symbol($business_id, '%.2n', $line2->qty * $line2->unitPrice, 2, array('shrink_money_symbol' => 1)).' </td>
                </tr>
                <tr>
                       <td height="5px"></td>
                </tr>';
            }

            //find out if this is a WF order
            if(isset($line2->name)){
                if ($line2->name == 'Wash and Fold') {
                    $ordType = 'WF';
                }
            }

            //option to only print wf orders
            //TODO CHECK THIS changed washlocation id
            if (isset($request['wf']) and $line2->supplierBusiness_id == '4') {
                $numToPrint = 0;
            }
            //option to only print dc order
            if (isset($request['dc']) && !empty($request['dc']))
            {
                //get order type
                if($ordType <> 'DC') {
                       $numToPrint = 0;
                }
            }
            //don't print wholesale receipts
            if(isset($line2->name)){
                if (stristr($line2->name, 'whsl') and $request['routes'])
                {
                        $numToPrint = 0;
                }
            }
        }
    }
    if($request['auto'] != 'auto') {
        //override and only print one receipt
        if ($numToPrint <> 0) { $numToPrint = 1;}

        //find out the barcodes for this order
        //if and item has multiple barcodes, it will only show one.
        $query9 = "select barcode
        from orderItem
        join barcode on barcode.item_id = orderItem.item_id
        where order_id = $order->orderID
        group by barcode.item_id
        order by barcode";
        $order_items = $this->db->query($query9)->result();
        foreach($order_items as $line9) {
            if($request['barcode'] == $line9->barcode)
                    $itemList .= "<b>".$line9->barcode . '</b>, ';
            else
                $itemList .= $line9->barcode . ', ';
        }

        //print a collar stay reminder with every route
        /*if($line->route_id <> $route and $request['routes']) {
                echo '
                <div id="clearHeaderRoutes" style="clear: both;">&nbsp;<br><br></div>
                <div id="mainDiv" style="FLOAT: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                <br>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                <tr>
                        <td align="center"><strong><font size="+2">Cuando pongas los tickets ponel los collar stays<br><br>IMPORTANT: Please hang up the collar stays</font></strong><br><br></td>
                </tr>
                </table>
                </div>';
                $route = $line->route_id;
        }*/

        if($line->orderStatus == 'Completed') {
                echo '
                <div id="clearHeaderCompleted" style="clear: both;">&nbsp;<br><br></div>
                <div style="FLOAT: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                <br>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                <tr>
                        <td align="center"><strong><font size="+2">' .
                        get_translation_for_view('order_time_completed', 'This is a COMPLETED order from %time%',
                            array('time' => strftime($locale_date_format, $order->dateCreated->getTimestamp())
                        ))
                        .'</font></strong><br><br></td>
                </tr>
                </table>
                </div>';
        }
        if($order->postAssembly and $numToPrint != 0 ) {
            if(@!$request['partial']) { //don't show assembly notes on partial printouts
                    echo '
                    <div id="clearHeaderPartial" style="clear: both;">&nbsp;<br><br></div>
                    <div style="page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                    <br>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                    <tr>
                            <td align="center"><strong><font size="+2">' . get_translation_for_view('assembly_notes', 'ASSEMBLY NOTES') . '</font><br>('. getPersonName($line) .')</strong><br><br></td>
                    </tr>
                    <tr><td>'.$order->postAssembly.'</td></tr>
                    </table>
                    </div>';
                    echo '<div style="clear: both; page-break-after:always">&nbsp;<br><br></div>';
            }
        }

        if($order->alertNotes and $numToPrint != 0 ) {
            if(@!$request['partial']) { //don't show alertNotes notes on partial printouts
                    echo '
                    <div id="clearHeaderPartial" style="clear: both;">&nbsp;</div>
                    <div style="page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                    <br>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                    <tr>
                            <td><strong><font size="+2">' . get_translation_for_view('alert_notes', 'ALERT NOTES') . '
                            </td>
                    </tr>
                    <tr><td>' . get_translation_for_view('ada_deliver_to_lower_locker', 'Always deliver to lower locker') . '</td></tr>
                    </table>
                    </div>';
                    echo '<div style="clear: both; page-break-after:always">&nbsp;<br><br></div>';
            }
        }
        //if it is DC and more than 10 pcs, print a note for the driver in case it needs multiple lockers.
        if($totQty >= $items_limit and $request['short'] <> 1) {
            if(!isset($request['partial'])) {
                if(is_numeric($locker->lockerName) or $locker->lockerName == "InProcess")  {
                        if($ordType <> 'WF')
                        {
                            echo '
                            <div style="clear: both;">&nbsp;<br><br></div>
                            <div style="FLOAT: left;page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                            <br>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                            <tr>
                                    <td align="center"><strong><font size="+2">' . get_translation_for_view('count_your_items', 'PLEASE COUNT YOUR ITEMS') . '</font></strong><br><br></td>
                            </tr>
                            <tr><td><span style="line-height: 200%; font-size: 15px;">' . get_translation_for_view('partial_delivery', 'This order may have been too large for one locker.  If so, the rest of your order is located in locker:') . '<br><br>#_____________.</span><br><br><br><br>&nbsp;</td></tr>
                            </table>
                            </div>';
                            echo '<div style="clear: both; page-break-after:always">&nbsp;<br><br></div>';
                        }
                }
            }
        }

        //print price details
        if($request['short'] == 0) {
            $sql = "SELECT * FROM orderCharge WHERE order_id = {$order->orderID}";
            $q = $this->db->query($sql);

            $charge_label = get_translation_for_view('charge', 'Charge');
            $credit_label = get_translation_for_view('credit', 'Credit');

            if ($charges = $q->result()){
                foreach($charges as $charge) {
                    $items .= '
                        <tr>
                          <td>('.($charge->chargeAmount > 0 ? $charge_label : $credit_label ).')</td>
                          <td colspan="3" align="right" valign="top" nowrap>'.format_money_symbol($business_id, '%.2n', $charge->chargeAmount).'</td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>';
                }
            }
            $CI->load->model("order_model");
            $net_total = $CI->order_model->get_net_total($order->orderID);

            $taxes = $CI->order_model->get_stored_taxes($order->orderID);

            $tax = 0;
            foreach ($taxes as $taxItem) {
                $tax += $taxItem['amount'];
            }

			$breakout_vat = $CI->order_model->is_breakout_vat($order->orderID);

            if ($breakout_vat) {
                $sub_total = $net_total - $tax;

                $net_total_label = get_translation_for_view('net_total', 'Order Net Total:');
                $items .= '
                <tr>
                        <td colspan="3" style="background-color: white" align="right">'.$net_total_label.'</td>
                        <td align="left" nowrap>'.format_money_symbol($business_id, '%.2n', $net_total, 2, array('shrink_money_symbol' => 1)).'</td>
                </tr>';

                        $sub_total_label = get_translation_for_view('sub_total', 'Sub Total:');
                        $items .= '
                <tr>
                        <td colspan="3" style="background-color: white" align="right">'.$sub_total_label.'</td>
                        <td align="left" nowrap>'.format_money_symbol($business_id, '%.2n', $sub_total, 2, array('shrink_money_symbol' => 1)).'</td>
                </tr>';
            }

            if ($tax > 0 && count($taxes) == 1) {
                $tax_label = get_translation_for_view('tax', 'Tax:');
                $items .= '
                <tr>
                        <td colspan="3" style="background-color: white" align="right">'.$tax_label.'</td>
                        <td align="left" nowrap>'.format_money_symbol($business_id, '%.2n', $tax).'</td>
                </tr>';
            } else {
                $CI->load->model("taxgroup_model");

                foreach($taxes as $taxItem) {
                    $taxGroup = $CI->taxgroup_model->get_by_primary_key($taxItem['taxGroup_id']);
                    if ($taxGroup) {
                        // If the group exists, use its name as label
                        $tax_label = $taxGroup->name . ' :';
                    } else {
                        // Generate the tax label from the tax rate
                        $taxPercent = $taxItem['taxRate'] * 100;
                        $tax_label = get_translation_for_view('tax_percent', 'Tax %taxPercent%%:', array('taxPercent' => $taxPercent));
                    }

                    $items .= '
                <tr>
                        <td colspan="3" style="background-color: white" align="right">'.$tax_label.'</td>
                        <td align="left" nowrap>'.format_money_symbol($business_id, '%.2n', $taxItem['amount']).'</td>
                </tr>';
                }

            }

			if (!$breakout_vat) {

						$net_total_label = get_translation_for_view('net_total', 'Order Net Total:');
						$items .= '
				<tr>
						<td colspan="3" style="background-color: white" align="right">'.$net_total_label.'</td>
						<td align="left" nowrap>'.format_money_symbol($business_id, '%.2n', $net_total).'</td>
				</tr>';
			}
        }
    }

    $CI->load->model('assembly_model');
    $grouping = get_business_meta($business_id, 'metalproGrouping');

    $rem = $totQty;
    if(get_business_meta($business_id, 'loadBags') == 1) {
        $rem += 1;
    }

    if($grouping)
    {
        $rem = $rem % $grouping;
    }

    $rte_label = empty($_POST['master_sort']) ? get_translation_for_view('route', 'Rte:') : get_translation_for_view('master_route', 'RTE:');
    $posBar = '<tr>
            <td colspan="4"><br><table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 5px; background: black;"><tr><td><strong><font size="+1" color="#FFFFFF">'. $rte_label .' ';

    if($order->postAssembly || $order->customer_id == 0 || $order->customer_id == 2884) {
        $posBar .= 'X</strong><font size="-2" color="#FFFFFF">('.$line->route_id.')</font><strong>';
    } else {
        $posBar .= $line->route_id;
    }

    //if this  is not in the assembly table add it
    $CI->assembly_model->route_id = $line->route_id;
    $CI->assembly_model->order_id = $line->ordId;
    $row = $CI->assembly_model->get();
    if($row) {
            $position = $row[0]->position;
    } else {
            //get the last number

            $position = $CI->assembly_model->getMaxPosition($line->route_id, $business_id);
            $position = $position + 1;
            $CI->assembly_model->clear();
            @$CI->assembly_model->insert($line->business_id, $line->route_id, $line->ordId, $position, $printDate);

    }
    if($line->split != 0)
    {
        $printQty = $totQty;
    }

    $pos_label = empty($_POST['master_sort']) ? get_translation_for_view('position', 'Pos:') : get_translation_for_view('master_position', 'STOP:');
    $qty_label = get_translation_for_view('quantity', 'Qty:');

    $posBar .= '</font></strong></td><td><strong><font size="+1" color="#FFFFFF">' . $pos_label
     . ' ' . $position.'</font></strong></td>
    <td align="right"><font size="-1" color="#FFFFFF">' . $qty_label . ' '.$totQty;
    if($line->split != 0)
    {
        $posBar .= '<br>Split: '.$line->split;
    }
    //if($line->bagNumber and $grouping)$items .= '<br>Rem:'.$rem;
    $posBar .= '</font></strong></td></tr></table>';

    if($business->assemblyBar == 'bottom')
    {
        $items .= $posBar;
    }
    $items .= '</table>';

	$items_label = get_translation_for_view('items', 'Items:');

    if(!empty($request['autoNotes']))
        $items .='<br>'.$request['autoNotes'];
    else
        $items .='<br>'.$items_label.' ('.$initials.') <span>'.$itemList.'</span></td>';

    $items .='</tr>
    </table><br><br>';

    //if it is POS order
    //need to get the loc
    $testLockerID = $order->locker_id;
    $locker = new Locker($testLockerID, FALSE);
    $loc = new Location($locker->location_id, FALSE);
    if ($loc->locationType_id == 12 && $duplicate_receipt_pos) { // it is POS order
        $numToPrint = 2;
    }
    //end check for pos order

    for ($i = 1; $i <= $numToPrint; $i++) {

        if ($request['short'] <> 1) {
            echo '<div id="clearDivNotShort" style="clear: both;">&nbsp;<br><br></div>';
        }
        $partialNote = '';
        if (isset($request['partial']) && $request['partial']!='') {
            //Print out a note to put on the bag
            echo'
                    <div style="page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
                    <br>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                    <tr>
                            <td align="center">';

            echo sprintf('<img src="'.$full_base.BUSINESS_IMAGES_DIRECTORY.'/%s" alt="" border="0" width="225"><br>', $image->filename);

            if (!$hide_business) {
                echo $business->phone.'<br>';

				if (!$hideURLOnReceipt) {
                    echo $website_url.'<br>';
                }
            }

            $partial_title = get_translation_for_view('partial_order', 'PARTIAL ORDER');
            $partial_text = get_translation_for_view('partial_text',
            "Dear %firstName%,<br>
            <br><strong>%partial_count%</strong> of your items required extra processing and we are working on getting them back to you as soon as possible.<br>
            <br>
            We apologize for the inconvenience, <br>
            The %companyName% Team", array(
                'firstName' => $line->firstName,
                'lastName' => $line->lastName,
                'companyName' => $business->companyName,
                'partial_count' => $request['partial'],
            ));

            echo '<br>
                        </td>
                </tr>
                <tr>
                        <td><h1>'.$partial_title.'</h1>
                        '.$partial_text.'
                        </td>
                </tr>
                </table>
                </div>';


            //change the order status to 12 - partial
            //TODO need to implement this
            $order->orderStatusOption_id = 12;
            //$order->save();

            $partialNote = '<br><br><h2>';
            if ($request['partial'] > 1) {
                $partialNote .= get_translation_for_view('many_pieces_short', "%count% pieces short", array('count' => $request['partial']));
            } else {
                $partialNote .= get_translation_for_view('one_piece_short', "1 piece short", array('count' => $request['partial']));
            }

            $partialNote = $partialNote . '</h2>'.$request['item'].'<br>'.$request['reason'].'<br>';
        }
        echo'
            <div style="clear: both; FLOAT: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
            '.$partialNote.'
            <br>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">';

        if(!$line->firstName)
        {
            $call_us_title = get_translation_for_view('call_us_title', "Please Call Us");
            $call_us_text = get_translation_for_view('call_us_text',
            "Dear Customer,<br>
            At the time this building was last checked, the items in this locker had not been claimed and / or paid for. Please contact us at %phone%, <u>and keep the key to this locker</u>, so that we can return your items back to you promptly.<br>
            <br>
            We apologize for the inconvenience,<br>
            The %companyName% Team", array(
                'companyName' => $business->companyName,
                'phone' => $business->phone,
            ));

            echo '<tr>
                <td>
                <h1>'.$call_us_title.'</h1>
                <strong>'.$call_us_text.'</strong><br><br><br><br>
                </td>
            </tr>';
        }

        echo '
        <tr>
                <td align="center">
                        <img src="' .$full_base.BUSINESS_IMAGES_DIRECTORY . $image->filename.'" alt="" border="0"  width="225"><br>';
                        if ($show_business_address_on_receipts) {
                          echo  $this->settings->address . '<br>';
                          if (!empty( $this->settings->address2 )) {
                            echo '<br>' . $this->settings->address2 . '<br>';
                          }
                        }
                        if (!$hide_business && !$hideURLOnReceipt) {
                            echo $this->settings->phone.'<br>'.$website_url;
                        }
                echo '<br><br>
                </td>
        </tr>';
        if ($request['short'] <> 1)
        {
            echo '
            <tr>';
                    if ($line->bagNumber) {
                        $barcode_number = $line->bagNumber;
                    } else {
                        $barcode_number = $request['order'];
                    }

                    if ($is_pos) {
                        switch ($print_barcode) {
                            case 'order': $barcode_number = $line->ordId; break;
                            case 'bag': $barcode_number = $line->bagNumber; break;
                            case 'item':
                                if (!empty($order_items)) {
                                    $barcode_number = $order_items[0]->barcode;
                                }
                                break;
                            default:
                                $barcode_number = $line->bagNumber;
                        }
                    }

                    echo '<td align="center"><img src="'.$full_base.'/barcode.php?barcode='.$barcode_number.'&height=35"></td>';

                    if (!empty($shipment)) {
                      foreach ($shipment as $k=>$s) {
                        if ($k==0) echo '</tr>';
                        echo '<tr><td align="center"><br/><img src="'.$full_base.'/barcode.php?barcode='.$s->waybill_nr.'&height=35&type=code128"></td></tr>';

                      }
                    } else {
                      echo '</tr>';
                    } 
        }

        if($business->assemblyBar == 'top')
        {
            echo $posBar;
        }

        $bag = new Bag($order->bag_id, FALSE);
        $CI->load->model('orderstatus_model');
        $sql = "SELECT * FROM orderStatus WHERE order_id = {$order->orderID} ORDER BY date DESC";
        $q = $this->db->query($sql);
        $orderHistory = $q->result();

		$customer_label = get_translation_for_view('customer', 'Customer:');
		$phone_label = get_translation_for_view('phone', 'Phone:');

        echo '
            <tr>
                    <td><br>
                            <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                            <tr>
                                    <td valign="top"><strong>'.$customer_label.'</strong></td>
                                    <td align="right" valign="top">'. getPersonName($line);
                                    if ($bag->notes && $bag->notes != "boxture") { echo ' ('.$bag->notes.')';}
                                    echo '</td>
                            </tr>
                            </table>
                    </td>
            </tr>';
        if($request['auto'] != 'auto') {
            echo '<tr>
                    <td><table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                            <tr>
                                    <td valign="top"><strong>'.$phone_label.'</strong></td>
                                    <td align="right" valign="top">'.$line->phone.'</td>
                            </tr>
                            </table>
                    </td>
                    </tr>';
            //header details
            if ($order->dateCreated instanceOf DateTime) {
                $dateCreated = convert_from_gmt_aprax($order->dateCreated->format('Y-m-d H:i:s'), $date_format,$order->business_id);

                $datePrinted= convert_from_gmt_aprax(date('Y-m-d'), $date_format, $order->business_id);
            }
            else {
                $dateCreated = "";
            }
            if($request['short'] != 1) {
                $date_printed_label = get_translation_for_view('date_printed', 'Date Printed:');
                $location_label = get_translation_for_view('location', 'Location:');

                echo'
                    <tr>
                            <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                                    <tr>
                                            <td valign="top"><strong>'.$date_printed_label.'</strong></td>
                                            <td align="right" valign="top">'. $datePrinted .'</td>
                                    </tr>
                                    </table>
                            </td>
                    </tr>';
            }
            
            //Use customer address if home delivery
            if ($line->locationType == 'Home Delivery') {
               $loc->address = $line->address1." ".$line->address2;
            }

            echo '
            <tr>
                    <td>
                            <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                            <tr>
                                    <td valign="top"><strong>'.$location_label.'</strong></td>
                                    <td align="right" valign="top">'.$loc->address.'</td>
                            </tr>
                            </table>
                    </td>
            </tr>';
        }

	$locker_label = get_translation_for_view('locker', 'Locker:');
        echo '
                    <tr>
                            <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                                    <tr>
                                            <td valign="top"><strong>'.$locker_label.'</strong></td>
                                            <td align="right" valign="top">';

        if($loc->locationType_id == 3 && $loc->show_locker_name_on_receipt != 1) {
            echo 'Concierge';
        }
        else {
            echo $locker->lockerName;

        }

        $order_label = get_translation_for_view('order', 'Order:');
        $order_style = '';
        $table_style = '';
        if (get_business_meta($order->business_id, 'print_day_tags')) {
            $order_style = 'font-size:1.9em;font-weight:bold;';
            $table_style = 'padding: .5em 0;';
        }



        echo '</td>
                        </tr>
                        </table>
                </td>
        </tr>
        <tr>
                <td>
                        <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt; '.$table_style.'">
                        <tr>
                                <td valign="bottom"><strong>'.$order_label.'</strong></td>
                                <td align="right" valign="bottom">'. $dateCreated.' - <span style="'.$order_style.'">' . $order->orderID.'</span></td>
                        </tr>
                        </table>
                </td>
        </tr>';
        
        // print due date
        if ( (Order::isPosOrder($order) && get_business_meta($this->business_id, "pos_receipt_include_due_date", 1)) || 
            (!Order::isPosOrder($order) && get_business_meta($this->business_id, "delivery_receipt_include_due_date", 0))) {
            $due_date_label = get_translation_for_view('due_date', 'Due Date:');
            $due_date_formated = strftime(get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d'), strtotime($order->dueDate));

            echo '<tr>
                    <td>
                            <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt; '.$table_style.'">
                            <tr>
                                    <td valign="bottom"><strong>'.$due_date_label.'</strong></td>
                                    <td align="right" valign="bottom">'. $due_date_formated .'</td>
                            </tr>
                            </table>
                    </td>
            </tr>';
        }
        
        if($request['auto'] != 'auto') {

			$ticket_label = get_translation_for_view('ticket_number', 'Ticket #:');
            echo '
                <tr>
                        <td>
                                <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                                <tr>
                                        <td valign="top"><strong>'.$ticket_label.'</strong></td>
                                <td align="right" valign="top">'.$order->ticketNum.'</td>
                                </tr>
                                </table>
                        </td>
                </tr>';

            //notes
            if($request['short'] <> 1) {
				$notes_label = get_translation_for_view('notes', 'Notes:');
                echo'
                <tr>
                        <td>
                                <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt;">
                                <tr>
                                        <td valign="top"><strong>'.$notes_label.'</strong>';
                    echo $order->notes;
                    echo '</td>
                                </tr>
                                </table>
                        </td>
                    </tr>';
            }
            echo '<tr><td>&nbsp;</td></tr></table>';
            if($request['short'] > 0) {
				$item_column = get_translation_for_view('item_col', 'Item');
				$qty_column = get_translation_for_view('quantity_col', 'Qty');

                echo '
                <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                <tr>
                        <td><strong>'.$item_column.'</strong></td>
                        <td align="left"><strong>'.$qty_column.'</strong></td>
                </tr>';
            }
            else {
				$item_column = get_translation_for_view('item_col', 'Item');
				$price_column = get_translation_for_view('price_col', 'Price');
				$qty_column = get_translation_for_view('quantity_col', 'Qty');
				$total_column = get_translation_for_view('total_col', 'Total');


                echo '
                <table width="100%" cellspacing="0" cellpadding="0" style="font-size: 10pt; font-family: Verdana;">
                <tr>
                        <td><strong>'.$item_column.'</strong></td>
                        <td align="left"><strong>'.$price_column.'</strong></td>
                        <td align="left"><strong>'.$qty_column.'</strong></td>
                        <td align="left"><strong>'.$total_column.'</strong></td>
                </tr>';
            }
        }
        echo $items;
        
        if ($show_payment && $order->orderPaymentStatusOption_id == 3){
            echo get_translation_for_view('order_paid', 'Order %order_id% paid', array('order_id' => $order_payment->order_id));
            echo "<br />";
            
            $transaction_message = $order_payment->message;
            $transaction_number = get_translation_for_view('transaction_number', 'Transaction #%transaction_id%', array('transaction_id' => $order_payment->transactionID));
            
            if (!$order_payment->pnref) {
                // this is not a credit card
                echo $transaction_message;
            } else {
                //for credit cards, summary is at the end of the message
                $transaction_message_comps = explode('.', $transaction_message);
                $transaction_detail = $transaction_message_comps[count($transaction_message_comps) - 1];
                
                echo $transaction_detail; 
            }
            
            echo "<br />";
            echo " ($transaction_number)";
            echo "<br />";
        }
        
        //echo '<div align="center" style="font-size: 25pt; font-family: Verdana;"><strong><u>Special</u><br>$4.99 Shoe Shines!</strong><br><font size="3">Thru Aug 31st.</font></div>';
        echo'
                  </div>
        ';
    }
}
?>
</body>
</html>
