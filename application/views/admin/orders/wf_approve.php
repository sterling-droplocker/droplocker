<?php enter_translation_domain("admin/orders/wf_approve"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $(":submit").loading_indicator();
});
</script>
<h2><?=__("title", "Approve Wash &amp; Fold Orders");?></h2>
<br>
<br>
<form action="/admin/orders/wf_approve_post" method="post">
<table border="1" cellspacing="2" cellpadding="2" class="normal">
<tr>
	<td><?=__("bag", "Bag");?></td>
	<td><?=__("customer", "Customer");?></td>
	<td><?=__("location", "Location");?></td>
	<td><?=__("notes", "Notes");?></td>
	<td><?=__("internal_notes", "Internal Notes");?></td>
	<td><?=__("approve", "Approve");?></td>
	<td><?=__("notes_sent_to_WF", "Notes Sent to WF");?></td>
</tr>
<?php $counter = 0;?>
<? foreach($orders as $order):
    if(!is_numeric(substr(ltrim($order->notes),0,1))) {
        continue;
    }
    $counter++;
?>

        <tr>
            <td valign="top"><a href="/admin/orders/details/<?= $order->orderID ?>" target="_blank"><?= $order->bagNumber ?></a></td>
            <td valign="top"><a href="/admin/customers/detail/<?= $order->customer_id ?>" target="_blank"><?= getPersonName($order) ?></a></td>
            <td valign="top"><?= $order->address ?><br><?=__("route", "Route");?> (<?= $order->route_id ?>)</td>
            <? $notes = str_replace("ORDER TYPE: Dry Cleaning / Laundered Shirts", "<b><u>ORDER TYPE: Dry Cleaning / Laundered Shirts</u></b>", $order->notes); ?>		
            <td valign="top"><?=__("notes", $notes);?></td>
            <td valign="top"><?= $order->internalNotes ?></td>
            <td width="8" valign="top">
                <input type="hidden" name="order_<?= $counter ?>" value="<?= $order->orderID ?>">
                <input type="hidden" name="customer_id_<?= $counter ?>" value="<?= $order->customer_id ?>">
                <input type="hidden" name="wfProduct_<?= $counter ?>" value="<?= $order->defaultWF ?>">
                <?php if($order->hold): ?>
                        Customer's account is on hold</td>
                <? else: ?>
                    <? if(in_array($order->locationID, array(557, 483))): //special carve out for farouk ?>
                        <input type="text" id="<?= $order->orderID ?>_checkbox" name="qty_<?= $counter ?>">
                        Supplier:
                        <select name="supplierBusinessID_<?= $counter ?>">
                            <option value=""><?=__("SELECT", "-SELECT-");?></option>
                            <?php foreach($suppliers as $supplier): ?>
                                <option value="<?= $supplier->businessID ?>"<?= $route[$order->route_id] == $supplier->businessID ? ' SELECTED' : '';?>><?= $supplier->companyName ?></option>
                            <?php endforeach; ?>
                        </select>
                    <? else: ?>
                        <input type='checkbox' id='{$order->orderID}_checkbox' name="qty_<?= $counter ?>" value='on'>
                    <? endif; ?>
                <? endif; ?>

            </td>
            <td valign="top"><textarea cols="25" rows="3" id="<?="{$order->orderID}_notes"?>" name="notes_<?= $counter ?>"><?= $order->wfNotes ?></textarea></td>
        </tr>
<? endforeach; ?>
</table>
<input type="hidden" name="counter" value="<?= $counter ?>">
<br><input type="submit" class="button orange" value="Approve">
</form><br>


