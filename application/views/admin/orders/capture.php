<?

enter_translation_domain("admin/orders/capture");

?>
<h2><?= __("payment_results","Payment Results")?></h2>
<p>
	<a href='/admin/orders/view'><?= __("back_to_all_orders","Back to all orders")?></a>
</p>

<table id='box-table-a'>
	<thead>
		<tr>
		<th><?= __("orderid","orderID")?></th>
		<th><?= __("status","Status")?></th>
		<th><?= __("amount","Amount")?></th>
		</tr>
	</thead>
	<tbody>
		<? if($payments):$total = 0;foreach($payments as $p):
		$total = $total + $p['amount'];
		?>
		<tr>
			<td><?= $p['order_id']?></td>
			<td><?= $p['message']?></td>
			<td><?= ($p['amount'])?format_money_symbol($this->business_id, '%.2n', $p['amount']):format_money_symbol($this->business_id, '%.2n', 0.00);?></td>
		</tr>
		<? endforeach; else:?>
		<tr>
			<td colspan='5'><?= __("no_payments_made","No Payments Made")?></td>
		</tr>
		<?endif; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan='2' align='right'><?= __("total","Total: ")?></td>
			<td><?= format_money_symbol($this->business_id, '%.2n', $total)?></td>
		</tr>
	</tfoot>
</table>