<?

enter_translation_domain("admin/orders/due_date");

$dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');
?>
<script type="text/javascript">
$(document).ready(function(){

$(".datefield").datepicker({
	showOn: "button",
	buttonImage: "/images/icons/calendar.png",
	buttonImageOnly: true,
	dateFormat: "yy-mm-dd"
});

$('.note_button').live('click', function(){
    var id = $(this).attr('id');
    id = id.replace("button-","");
    var note = $('#note-'+id).val();
    $('#work-'+id).css('display', 'inline').html(" <img src='/images/progress.gif' />");
    $.ajax({
        url: "/admin/orders/update_notes_ajax",
        data: {"note":note,"order_id":id},
        type:"post",
        success: function(data){
                if(data.status=="success"){
                        //show a message that it was updated
                        $('#work-'+id).html("<?= __("msg_saved", "Saved!") ?>").delay(1000).fadeOut('slow');
                }
                else{
                        $('#work-'+id).html("<?=__("msg_not_saved", "NOT SAVED!") ?>").delay(3000).fadeOut('slow');
                }

        },
        dataType:'json'
    });
});

$('#check_all_btn').click(function(){
    $(".process_chk").attr('checked', true);
});

$.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
{
	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
		return $('td:eq('+iColumn+')', tr).attr('data-value');
	});
};


$orders_dataTable = $("#orders").dataTable({
    "bPaginate" : false,
    "bJQueryUI": true,
    "fnStateLoad": function (oSettings) {
        return JSON.parse( localStorage.getItem('DataTables') );
    },
    "aoColumns": [
        null,
        null,
        null,
        null,
        null,
        { "sSortDataType": "attr" },
        { "sSortDataType": "attr" },
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ],
    "aoColumnDefs": [{
        "aTargets": [0]
    }],
    "sDom": '<"H"lTfr>t<"F"ip>',
    "oTableTools": {
        <?php if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
    }

});

new FixedHeader($orders_dataTable);
$('#capture_btn').loading_indicator().attr("class", "button orange");

}); //End of document.ready()

</script>

<?= $this->session->flashdata('message') ?>

<h2><?= __("order_by_due_date","Orders by Due Date")?></h2>
<div style='padding:5px;'>

<form action="/admin/orders/due_date" method="get">
    <label for="due_date"><?= __("due_date","Due Date:")?></label>
    <input type="text" id="due_date" class="datefield"  name="due_date" value="<?= $due_date ?>">
    <label><input type="checkbox" name="past_orders" value="1" <?= $past_orders ? 'checked' : '' ?>><?= __("show_orders_past_due","Show orders past due")?></label>
    <input type="submit" value="<?= __("apply", "Apply") ?>">
</form>
<br><br>

<table id="orders">
	<thead>
            <tr>
                <th><?= __("order","Order")?></th>
                <th><?= __("order_type","Order Type")?></th>
                <th><?= __("route","R")?></th>
                <th><?= __("location","Location")?></th>
                <th><?= __("original_locker","Original Locker")?></th>
                <th><?= __("picked","Picked")?><br /><?= __("up","Up")?></th>
                <th><?= __("due_date","Due Date")?></th>
                <th><?= __("bag","Bag")?></th>
                <th><?= __("stat","Stat")?></th>
                <th><?= __("capture","Capture")?></th>
                <th><?= __("pay_stat","Pay Stat")?></th>
                <th><?= __("customer","Customer")?></th>
                <th><?= __("order_count","Order Count")?></th>
                <th><?= __("claim","Claim")?></th>
                <th><?= __("receipt","Receipt")?></th>
                <th><?= __("notes","Notes")?></th>
            </tr>
	</thead>
	<tbody>
            <?  if(!empty($orders)): foreach($orders as $order): ?>
            <tr>
                <!-- This is the order column -->
                <td>
                    <a href="/admin/orders/details/<?= $order['orderID']?>"> <?= $order['orderID'] ?> </a>
                </td>
                <!-- This is the Order Type Column -->
                <td><?= $order['orderType'] ?></td>             
                <td><?= $order['route_id'] ?></td>   
                <!-- This is the Location column -->
                <td><?= $order['address']?></td>
                <!-- This is the Original Locker Column -->
                <td><?= $order['original_locker'] ?></td>
                <td nowrap='true' data-value="<?= $order['dateCreated'] ?>">
                    <?= convert_from_gmt_locale($order['dateCreated'], $dateFormat); ?>
                </td>
                <td nowrap='true' data-value="<?= $order['dueDate'] ?>">
                    <?= $order['dueDate'] ? apply_locale_date_format($order['dueDate'], $dateFormat): '-' ?>
                </td>
                <td><?= $order['bagNumber']?></td>
                <td>
                    <?= $order['order_status']?>
                    <? if ($order['loaded'] == 1): ?>
                    <br><?= __("loaded", "(loaded)") ?>
                    <? endif; ?>
                </td>
                <td>
                <? if($order['invoice'] == 0 && (($order['orderPaymentStatusOption_id'] == 2 || ($order['autoPay'] == 1 && $order['orderPaymentStatusOption_id'] == 1)) && $order['orderStatusOption_id'] != 1 && $order['orderStatusOption_id'] != 2)): ?>
                    <input type='checkbox' class='process_chk' name='process[]' value='<?= $order['orderID']?>' />
                <? endif; ?>
                </td>
                <td>
                    <? if($order['invoice'] == 1): ?>
                        <?= __("invoice", "Invoice") ?>
                    <? else: ?>
                        <?= $order['paymentStatusName']?>
                    <? endif; ?>
                </td>
                <td><a target='_blank' href='/admin/customers/detail/<?= $order['customer_id']?>'><?= getPersonName($order) ?></a></td>
                <td><?= $order['order_count'] ?></td>
                <td><?= $order['claimID']?></td>
                <td><?= $order['receipt']?></td>
                <td>
                    <textarea name='llnotes' id='note-<?= $order['orderID']?>'><?= $order['llnotes']?></textarea><br>
                    <input id='button-<?= $order['orderID']?>' class='note_button' style='font-size:10px' type='button' value="<?= __("update", "update") ?>"/>
                    <span id='work-<?= $order['orderID']?>'></span>
                </td>
            </tr>
            <? endforeach;endif;?>
	</tbody>
</table>


