<?

enter_translation_domain("admin/orders/create_order");

?>
<h2><?= __("create_order","Create Order")?></h2>
<br>
<br>
<div align="center"><h3 style="color:#FF0000">
    <? if (!empty($notice)): ?><?= $notice ?><? endif; ?>
    <? if (!empty($notice2)): ?><?= $notice2 ?><? endif; ?>
    </h3></div>
<br>


<? if(!empty($claim)):?>
<div class='info'>
<h3><?= __("open_claim","Open Claim")?></h3>
<table id='hor-minimalist-a'>
    <tr>
        <?= nameForTableHeader() ?>
        <th><?= __("claim_date","Claim Date")?></th>
        <th><?= __("location","Location")?></th>
        <th><?= __("service_method","Service Method")?></th>
        <th><?= __("locker","Locker")?></th>
        <th></th>
    </tr>
    <tr>
        <?= nameForRow($customer) ?>
        <td><?= convert_from_gmt_aprax($claim->updated)?></td>
        <td><?= $location->address?> <?= $location->address2?></td>
        <td><?= $location->serviceType?></td>
        <td><?= $locker->lockerName?></td>
        <td>
            <?= form_open("/admin/orders/quick_order_locker") ?>
                <?= form_hidden("lockerID", $locker->lockerID) ?>
                <?= form_hidden("locationID", $location->locationID) ?>
                <?= form_hidden("bagNumber", $bagNumber) ?>
                <?= form_hidden("claimID", $claim->claimID) ?>
                <input type="submit" value="<?= __("create_order", "Create Order") ?>">
            <?= form_close() ?>

            <!-- <a href="/admin/orders/quick_order_locker/<?= $locker->lockerID?>/page/<?= $bagNumber?>/<?=$claim->claimID?>">Create Order</a></td> -->
    </tr>
</table>
</div>
<? endif; ?>

<h3><?= __("prior_orders","Prior Orders:")?></h3><br>

<table id='hor-minimalist-a'>
    <tr>
        <?= nameForTableHeader() ?>
        <th><?= __("claim_date","Claim Date")?></th>
        <th><?= __("location","Location")?></th>
        <th><?= __("service_method","Service Method")?></th>
        <th><?= __("locker","Locker")?></th>
     <th></th>
    </tr>

<?
$counter = 0;
foreach($pastOrders as $pastOrder): ?>
<? $counter++ ?>
<tr>
    <?= nameForRow($pastOrder) ?>
    <td><?= convert_from_gmt_aprax($pastOrder->dateCreated, 'm/d/Y') ?></td>
    <td><?= $pastOrder->address ?></td>
    <td><?= $pastOrder->serviceMethod ?> / <?= $pastOrder->lockType ?></td>
    <td><?= $pastOrder->lockerName ?></td>
<?php
    if ($counter == 1)
    {
        //FIXME: hardcoded locationType name
        // if the location.lockerType is lockers (locationType.name = kiosk or lockers)
        if ($pastOrder->lockerType == "lockers") {
            //don't let them create if it is not an electronic locker and it is a locker number
            if($pastOrder->lockType != "Electronic"): ?>
            <td><?= __("this_is_a_locker","This is a locker location so the locker must be scanned by the driver. Maybe they did not sync. Give to customer service.")?></td>
            <?php else: ?>
                <td>
                    <?= form_open("/admin/orders/quick_order_locker") ?>
                        <?= form_hidden("lockerID", $pastOrder->lockerID) ?>
                        <?= form_hidden("locationID", $pastOrder->locationID) ?>
                        <?= form_hidden("bagNumber", $pastOrder->bagNumber) ?>
                        <input type="submit" value="<?= __("create_order_here", "Create order here") ?>">
                    <?= form_close() ?>
                </td>
            <?php endif ?>
        <?php }
        else
        {
            if($this->zacl->check_acl('add_order')): ?>
                <td>
                    <?= __("be_careful","BE CAREFUL!  Make sure this is the right locker.")?>
                    <?= form_open("/admin/orders/quick_order_locker") ?>
                        <?= form_hidden("lockerID", $pastOrder->lockerID) ?>
                        <?= form_hidden("locationID", $pastOrder->locationID) ?>
                        <?= form_hidden("bagNumber", $pastOrder->bagNumber) ?>
                        <input type="submit" value="<?= __("create_order_here", "Create order here") ?>">
                    <?= form_close() ?>
                </td>
            <?php else: ?>
                <td><?= __("give_to_customerservice","Give to Customer Service")?></td>
            <?php endif ?>
        <?php }
    }

?>
</tr>
<? endforeach; ?>
</table><br><br>
