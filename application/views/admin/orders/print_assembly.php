<?php enter_translation_domain("admin/orders/print_assembly"); ?>
<script type="text/javascript">
    $(document).ready(function() 
    {
        <? if(isset($route) and in_array($route, explode('|', $selectedRoutes) )): ?> <? else: ?>$("#barcode").focus();<? endif; ?>
    });
</script>
<h2><?= __("title","Manual Assembly")?></h2>
<div style="width: 50%; float: left; <? if(isset($route) and in_array($route, explode('|', $selectedRoutes) )) { echo 'background-color: Yellow;'; } ?>">
    <?= form_open() ?>
        <div style='padding:5px; border:1px solid #ff7f00; background-color:#ffd4aa; margin-top:5px;'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><?= __("barcode","Barcode:")?> <input type='text' id='barcode' name='barcode' value='' /><?= form_submit("submit", "submit") ?></td>
                    <td align="right"><? if(check_acl('orders_highlight_routes') == 1): ?><button id="highlighted_routes"><?= __("highlighted_routes","Highlighted Routes")?></button><? else: ?><?= __("highlighted_routes","Highlighted Routes")?>: <? endif; ?> <?= substr(str_replace('|', ',', $selectedRoutes),0,-1) ?>
                    </td>
                </tr>
            </table>
        </div>
    <?= form_close() ?>

    <?php if ($this->input->post("submit")): ?>
    <div style='padding:10px;'>
        <table style='width:100%; font-size:20px;'>
            <tr>
                <td><?= __("barcode","BARCODE:")?> </td>
                <td><?php echo $barcode ?></td>
            </tr>
        </table>
        <? if(!$hide): ?>
            <table style='width:100%; font-size:80px;'>
                <tr>
                    <td><?= __("route","Route:")?></td>
                    <td><?php echo $route ?></td>
                </tr>
                <tr>
                    <td><?= __("position","Position:")?></td>
                    <td><?php echo $pos ?></td>
                </tr>
            </table>
        <? endif; ?>
        <? if($error): ?>
            <div class='error' style='padding:5px; font-size:24px;'>
                <?php echo $error?>
         <? elseif($complete): ?>
            <div style='padding:5px; font-size:24px;background-color:#d1f9e5; border:1px solid #87b93e; margin:10px 0px'>
                <?= __("complete", "%automatCount% OF %totalItems% COMPLETE", array('automatCount' => $automatCount, "totalItems" => $totalItems)) ?>
         <?else: ?>
            <div style='padding:5px; font-size:24px;background-color:#eaeaea; margin:10px 0px'>
                <?= __("completed", "%automatCount% OF %totalItems%", array('automatCount' => $automatCount, "totalItems" => $totalItems)) ?>
                
        <? endif; ?>
                <div style='float:right;'>
                    <a href="/admin/printer/print_order_receipt/<?= $order_id ?>" target="_blank"><?= __("print_receipt","Print Receipt")?></a>&nbsp;&nbsp;
                </div>
            </div>
        <table style='width:100%;'>
            <tr>
                <td><?= __("order_id","OrderID:")?> </td>
                <td><a target='_blank' href='/admin/orders/details/<?php echo $order_id?>'><?php echo $order_id ?></a></td>
            </tr>

            <tr>
                <td><?= __("customer","Customer:")?></td>
                <td><a target='_blank' href='/admin/customers/detail/<?php echo $customer_id?>'><?php echo $customer ?></a></td>
            </tr>

            <tr>
                <td><?= __("bag_number","Bag Number:")?></td>
                <td><?php echo $bagNumber ?> <?= isset($bagNotes) ? '('.$bagNotes.')' : ''; ?></td>
            </tr>

        <? if($complete && $pos_enabled_business): ?>
            <tr>
                <td><?= __("slot_number","Slot Number")?>:</td>
                <td>
                    <?= form_open('admin/orders/update_slot_number') ?>
                        <input type='hidden' id='order_id' name='order_id' value='<?= $order_id ?>' />
                        <input type='text' id='slot_number' name='slot_number' value='<?= $slot_number ?>' />
                        <?= form_submit("submit", "submit") ?>
                    <?= form_close() ?>
                </td>
            </tr>
        <? endif; ?>
        </table>
    </div>
    <?php endif; ?>

</div>
<? if(isset($_POST)): ?>
    <div style='width:50%; float:left'>
        <div style='padding:5px;'>
            <img style='width:90%' src='<?php echo $image ?>' />
        </div>
    </div>
<? endif; ?>

        
        
        <div id="route_dialog" style="display:none;">
            
            <? $checkedRoutes = explode('|', $selectedRoutes); ?> 
            
            <h2><?= __("highlighted_routes","Highlighted Routes")?></h2>
            <br>
            <br>
            <div id="route_form">
            <form action="/admin/orders/highlight_routes" method="post" id="route_post_form">
            <? foreach($allRoutes as $route): ?>
                <input type="checkbox" name="routes[]" class="routes_check" value="<?= $route->route_id ?>" <? if(in_array($route->route_id, $checkedRoutes)) { echo 'CHECKED'; } ?>><?= $route->route_id?>&nbsp;&nbsp;
            <? endforeach; ?>
            <button id="update_route_button"><?= __("btn_update","Update")?></button>
            </form>
            </div>
        </div>
        <script>
        var dlg = $('#route_dialog').dialog({
            autoOpen:false,
            minHeight:'400',
            minWidth: '400',
            modal:true
        });
        
        $(function(){
            $('#highlighted_routes').click(function(e){
                e.preventDefault();
                $('#route_dialog').dialog('open');
                //dlg.parent().appendTo($("#route_post_form")); //append it, because jquery UI moves it in the DOM
            });
            
            $('#update_route_button').click(function(){
                var routeVals = [];
                var checkedRoutes = $.makeArray( $('.routes_check:checked') );
                for(var i in checkedRoutes){
                    routeVals.push( $( checkedRoutes[i] ).val() );
                }
                
                $.post('/admin/orders/highlight_routes',{routes:routeVals},function(){
                     location.reload();
                });
            });
            
        });
        </script>
