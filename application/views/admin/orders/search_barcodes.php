<?php enter_translation_domain("admin/orders/search_barcodes"); ?>
<?= $this->session->flashdata('message')?>
<h2><?php echo __("Search Barcodes", "Search Barcodes"); ?></h2>

<form action='' method='post'>
	<table width='100%'>
		<tr>
			<td><label><?php echo __("Barcode", "Barcode"); ?></label><br><input type='text' id='barcode' name='barcode' /> 
				<input class='button orange' type='submit' value='<?php echo __("Search", "Search"); ?>' name='submit'/></td>
			
		</tr>
	</table>
</form>

<table id="box-table-a">
    <thead>
        <tr>
            <th><?php echo __("barcode", "barcode"); ?></th>
            <th><?php echo __("customer", "customer"); ?></th>
            <th><?php echo __("product", "product"); ?></th>
            <th><?php echo __("order number", "order #"); ?></th>
            <th><?php echo __("price", "price"); ?></th>
            <th><?php echo __("order date", "order date"); ?></th>
            <th><?php echo __("order status", "order status"); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php
        if ($result): ?>
                <tr>
                <?php foreach ($result as $result_item): ?>
                    <td> <?= $result_item->barcode ?> </td>
                    <td> <?= getPersonName($result_item) ?> </td>
                    <td> <?= $result_item->displayName ?> </td>
                    <td> <?= $result_item->order_id ?> </td>
                    <td> <?= $result_item->unitPrice ?> </td>
                    <td> <?= $result_item->order_date ?> </td>
                    <td> <?= $result_item->order_status ?> </td>
                <?php endforeach ?>
                </tr>      
    <?php
        else:
    ?>
        <tr> <td colspan="10"> <?php echo __("No results found", "No results found"); ?> </td> </tr>
    <?php
        endif;
    ?>
    </tbody>
</table>
<div id='results'></div>