<?

enter_translation_domain("admin/orders/barcode_lookup");

?>
<h2><?= __("barcode_lookup","Barcode Lookup")?></h2>
<br>
    <?= __("this_page_is_used_for","This page is used for quickly jumping to an item in an open order.")?>
<br>
<br>
<form action="barcode_lookup_search" method="post">
    <label for="barcode"><?= __("barcode","Barcode:")?></label>
    <input type="text" id="barcode" name="barcode">&nbsp;&nbsp;&nbsp;<input type="submit" name="lookup" value="<?= __("lookup","Look up")?>" class="button blue">
</form>

<script type="text/javascript">
    $("[name='barcode']").focus();
</script>
