<?php 
enter_translation_domain("admin/orders/view_location_orders"); 
$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
?>
<?= $this->session->flashdata('message')?>

<script type="text/javascript">
$(document).ready(function() {

    $('#location_button').click(function(){
        top.location.href='/admin/orders/view_location_orders/'+$('#location').val();
    });
    
    $("#customer").autocomplete({
       source : "/ajax/customer_autocomplete_aprax",
       select: function (event, customer) {
           $("input[name='customerID']").val(customer.item.id);
       }

    });
 


    $(".delete_claim").click( function() {
        if (confirm("<?php echo __("Are you sure you want to delete this claim", "Are you sure you want to delete this claim?"); ?>"))
        {
            return true;
        }
        else
        {
            return false;
        }
    });

    $(".lockerTable").each(function(index, dataTable_instance) {
        locker_dataTable = $(dataTable_instance).dataTable({
            "iDisplayLength" : 25,
            "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
            "bPaginate" : false,
            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            },
            "aaSorting": [[1, 'asc']]
        });
        
    });
    $("#claimed_order_table").dataTable({
            "iDisplayLength" : 25,
            "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>            
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
    });
    
    $("#linked_account_table").dataTable({
            "iDisplayLength" : 500,
            "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
            "bJQueryUI": true,
            "sDom": '<"H">t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
    });
    
    
});
</script>

<h2><?php echo __("Lockers", "Lockers"); ?> <?= $location->address?></h2>
<p style='float:left'><img src='/images/icons/add.png' align='absmiddle' /> = <?php echo __("Create Order", "Create Order"); ?></p>

<table class="lockerTable">
    <thead>
        <tr>
            <th></th>
            <th><?php echo __("Locker", "Locker"); ?></th>
            <th><?php echo __("Bag", "Bag"); ?> </th>
            <th><?php echo __("Order Status", "Order Status"); ?> </th>
            <th><?php echo __("Customer", "Customer"); ?> </th>
            <th><?php echo __("Payment Status", "Payment Status"); ?> </th>
            <th><?php echo __("Claims", "Claims"); ?> </th>
            <th><?php echo __("Orders", "Orders"); ?> </th>
            <th><?php echo __("Last Order", "Last Order"); ?> </th>
            <th><?php echo __("Locker Status", "Locker Status"); ?></th>
            <th><?php echo __("Locker History", "Locker History"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if($lockers):
            foreach($lockers as $locker):?>
                <tr>
                    <td align='center'>
                    <? if($locker->lockerName!='InProcess'):?>
                        <a class='thickbox' title='Create Order' href='/admin/orders/quick_order_locker/<?= $locker->lockerID?>/modal/0/<?= @$locker->claims[0]->claimID?>/?keepThis=true&TB_iframe=true&height=500&width=600' id='create_order-<?= $locker->lockerName?>'><img src='/images/icons/add.png' /></a></td>
                    <? endif; ?>
                    </td>
                    <td style='text-align:center'> <a href="/admin/orders/details/<?= $locker->orderID ?>" target="_blank"><?= $locker->lockerName?></a></td>
                    <td style='text-align:center'> <a href="/admin/orders/details/<?= $locker->orderID ?>" target="_blank"><?= $locker->bagNumber?></a> 
                    
                    <td> <?= empty($statusOptions[$locker->orderStatusOption_id])?"":$statusOptions[$locker->orderStatusOption_id]; ?> </td>
                    <td> <a href='/admin/customers/detail/<?=$locker->order_customer_id ?>'> <?= getPersonName($locker, true, 'order_firstName', 'order_lastName') ?> </a></td>
                    <td> <?= empty($paymentStatusOptions[$locker->orderPaymentStatusOption_id])?"":$paymentStatusOptions[$locker->orderPaymentStatusOption_id]; ?></td>
                    <td>
                        <?php if ($locker->claims): ?>
                            <?php foreach ($locker->claims as $claim): ?>
                        <a href='/admin/customers/detail/<?= $claim->customer_id ?>'> <?= getPersonName($claim, true, 'customer_firstName', 'customer_lastName') ?> <br />
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                    <td> <?= $locker->orders?></td>
                    <? if($locker->lastOrder): ?>
                            <td <?if(strtotime($locker->lastOrder) < strtotime("30 days ago")) echo ' style="background: #FFFF00;"'; ?>><?= convert_from_gmt_aprax($locker->lastOrder, $dateFormat, $this->business_id)?></td>
                    <? else: ?>
                            <td></td>
                    <?endif; ?>
                    <td> <?= $lockerStatus[$locker->lockerStatus_id]?></td>
                    <td> <a href='/admin/reports/locker_history/<?= $locker->lockerID?>'><?= $locker->lockerName?></a></td>

                </tr>
            <? endforeach ;
        else: ?>
            <tr>
                <td colspan="10"><?php echo __("No Lockers", "No Lockers"); ?></td>
            </tr>
        <? endif ;?>
    </tbody>
</table>

<br><h3><?php echo __("InProcess Lockers", "InProcess Lockers"); ?></h3><br>
<?php if($process):?>
<table class="lockerTable">
    <thead>
        <tr>
            <th> </th>
            <th><?php echo __("Locker", "Locker"); ?></th>   
            <th><?php echo __("Bag", "Bag"); ?> </th>
            <th><?php echo __("Order Status", "Order Status"); ?> </th>
            <th><?php echo __("Customer", "Customer"); ?> </th>
            <th><?php echo __("Payment Status", "Payment Status"); ?> </th>
            <th><?php echo __("Claims", "Claims"); ?> </th>
            <th><?php echo __("Orders", "Orders"); ?> </th>
            <th><?php echo __("Last Order", "Last Order"); ?> </th>
            <th><?php echo __("Locker Status", "Locker Status"); ?></th>
            <th><?php echo __("Locker History", "Locker History"); ?></th>

        </tr>
    </thead>
    <tbody>
        <?
            foreach($process as $locker):?>
                <tr>
                    <td><a class='thickbox' title='Create Order' href='/admin/orders/quick_order_locker/<?= $locker->lockerID?>/<?= $claim->claimID ?>/modal/?keepThis=true&TB_iframe=true&height=500&width=600' id='create_order-<?= $locker->lockerName?>'><img src='/images/icons/add.png' /></a></td>
                    <td style='text-align:center'><?= $locker->lockerName?></td>
                    <td style='text-align:center'> <a href="/admin/orders/details/<?= $locker->orderID ?>" target="_blank"><?= $locker->bagNumber?></a> 
                    <? if($locker->lockerName!='InProcess'):?>
                    <a class='thickbox' title='Create Order' href='/admin/orders/quick_order_locker/<?= $locker->lockerID?>/modal/?keepThis=true&TB_iframe=true&height=500&width=600' id='create_order-<?= $locker->lockerName?>'><img src='/images/icons/add.png' /></a></td>
                    <? endif; ?>
                    <td> <?= empty($paymentStatusOptions[$locker->orderPaymentStatusOption_id])?"":$paymentStatusOptions[$locker->orderPaymentStatusOption_id]; ?></td>
                    <td> <a href="/admin/customers/detail/<?=$locker->order_customer_id ?>" target="_blank"> <?= getPersonName($locker, TRUE, 'order_firstName', 'order_lastName') ?> </a></td>
                    <td> <?= empty($statusOptions[$locker->orderStatusOption_id])?"":$statusOptions[$locker->orderStatusOption_id]; ?> </td>
                        <td>
                        <?php if (!empty($locker->claims)): ?>
                            <?php foreach ($locker->claims as $claim): ?>
                            <a href='/admin/customers/detail/<?= $claim->customer_id ?>'> <?= getPersonName($claim, TRUE, 'customer_firstName', 'customer_lastName') ?> <br />
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>
                    <td> <?= $locker->orders?></td>
                    <? if($locker->lastOrder): ?>
                            <td <?if(strtotime($locker->lastOrder) < strtotime("30 days ago")) echo ' style="background: #FFFF00;"'; ?>><?= convert_from_gmt_aprax($locker->lastOrder, $dateFormat, $this->business_id)?></td>
                    <? else: ?>
                            <td></td>
                    <?endif; ?>
                    <td> <?= $lockerStatus[$locker->lockerStatus_id]?></td>
                    <td> <a href="/admin/reports/locker_history/<?= $locker->lockerID?>" target="_blank"><?= $locker->lockerName?></a></td>

                </tr>
            <? endforeach ;?>
        
        
    </tbody>
</table>
<? else: ?>
<p><?php echo __("No Orders", "No Orders"); ?></p>
<? endif ;?>


<br>

<br><h3><?php echo __("Claimed Orders:", "Claimed Orders:"); ?></h3><br>
<? if($claims): ?>
<table id="claimed_order_table">
    <thead>
        <tr>
            <th><?php echo __("Service Type", "Service Type"); ?></th>
            <th><?php echo __("Location", "Location"); ?></th>
            <th><?php echo __("Locker", "Locker"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th align='center'><?php echo __("Rte", "Rte"); ?></th>
            <th><?php echo __("When Placed", "When Placed"); ?></th>
            <th><?php echo __("Type", "Type"); ?></th>
            <th><?php echo __("Notes", "Notes"); ?></th>
            <th><?php echo __("Delete", "Delete?"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?foreach($claims as $claim):?>
        <tr>
            <td><?= $claim->serviceType?></td>
            <td><?= ucwords($claim->address)?></td>
            <td><?= $claim->lockerName?>
            
                <br><a href="/admin/orders/claimed_noOrd/<?= $claim->claimID ?>"><?php echo __("no ord", "no ord"); ?></a>
            
            </td>


            <td><a href="/admin/customers/detail/<?= $claim->customerID?>" target="_blank"><?= getPersonName($claim) ?></a></td>
            <td align='center'><?= $claim->route_id?></td>
            <td nowrap="true"><?= convert_from_gmt_aprax($claim->updated, $dateFormat, $this->business_id)?>
                <?php
                    $newDay = convert_from_gmt_aprax('now', $dateFormat, $this->business_id);
                    $newTime = get_business_meta($this->business->businessID, 'cutOff');	
                    $cutoff = $newDay .' '.$newTime;
                    $orderTime = convert_from_gmt_aprax($claim->updated, $dateFormat, $this->business_id);
                    if(strtotime($orderTime) > strtotime($cutoff))
                        echo '<br><a href="/admin/orders/claimed_today/'.$claim->claimID.'">today</a>';
                ?>
            </td>

            <td nowrap><?= empty($ordType[$claim->claimID])?"":$ordType[$claim->claimID];?></td>
            <td><?= $claim->order_notes?></td>
            <td><a class="delete_claim" href="/admin/orders/claimed_delete/<?= $claim->claimID ?>"><img src='/images/icons/cross.png' /></a></td>
        </tr>
        <? endforeach;?>
            
    </tbody>
</table>
<? else:?>
<p><?php echo __("No claimed orders for this location", "No claimed orders for this location."); ?></p>
<? endif;?>

<br><h3><?php echo __("Linked Accounts", "Linked Accounts:"); ?></h3><br>
<table id="linked_account_table">
    <thead>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <form action="/admin/orders/add_linked_account" method="post">
            <input type="hidden" name="location_id" value="<?= $location->locationID?>">
            <input type="hidden" name="customerID">
            <td><input type='text' id='customer' value="<?= empty($customer->customerID)?"":$customer->customerID; ?>" />
            <div id='customer_results'></div></td>
            <td align="right"><input type="submit" name="add" value="<?php echo __("add", "add"); ?>"></td></form>
        </tr>
        <? if($linked): foreach($linked as $linked):?>
        <tr>
            <td><a href="http://droplocker.loc/admin/customers/detail/<?= $linked->customerID?>"><?= getPersonName($linked) ?></a></td>
            <td><a href="/admin/orders/linked_delete/<?= $linked->customerID ?>"><img src='/images/icons/cross.png' /></a></td>
        </tr>
        <? endforeach; endif;?>
            
    </tbody>
</table>
