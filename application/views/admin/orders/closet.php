<?php

enter_translation_domain("admin/orders/closet");

?>
<h2>
    <? if(isset($order)): ?>
        <?=__("closet_title_for_order", "My closet for Order")?> -
        <a href='/admin/orders/details/<?= $order ?>'><?= $order ?></a>
    <? else: ?>
        <?=__("closet_title", "My closet")?>
    <? endif; ?>
</h2>

<div style="padding-top: 8px; padding-bottom: 8px;">
    <label for="barcode"><?=__("search_by_barcode", "Search By Barcode")?></label>
    <input type='text' name='barcode' id="barcode" />&nbsp;&nbsp;<input type='submit' name='submit' value="<?=__("search_button", "Search")?>" class='button blue' />
</div>

<?php if($items): ?>
    <?php foreach($items as $key=>$value): ?>
        <h3 style='clear: left;'>
            <?= $key?>
        </h3>
        <?php foreach($value as $k=>$v):
            if ($v['file'])
            {
                $image = "/picture/resize?src=https://s3.amazonaws.com/LaundryLocker/{$v['file']}&h=125&w=150&zc=1";
            }
            else
            {
                $image = "/images/missing_image.jpg";
            }
            ?>
            <div style='float: left; padding-bottom: 15px;'>
                    <div style="float: left;">
                        <?php if (is_numeric($order)): ?>
                            <a href='/admin/items/view?itemID=<?= $k ?>&orderID=<?= $order ?>'>
                        <?php else: ?>
                            <a href='/admin/items/view?itemID=<?= $k ?>'>
                        <?php endif ?>

                        <img class='img_bdr' style='width: 150px; height: 125px;' src='<?=$image?>' /><br> <?= $v['barcode'] ?>
                    </a>
                    <div style="float: right"><? if($v['orders'] == 0): ?><a href="/admin/orders/remove_item_from_customer?customer=<?= $customer_id ?>&item=<?= $v['itemID']?>" onClick='return confirm("Unassociate this item with this customer?");'><img src="/images/icons/cross.png" alt="" border="0"></a>&nbsp;&nbsp;&nbsp;<? endif; ?></div></div>
            </div>
        <? endforeach;?>
    <? endforeach; ?>
<? endif; ?>

