<?php enter_translation_domain("admin/orders/search_orders"); ?>
<?= $this->session->flashdata('message')?>

<style type="text/css">
    label {
        display: block;
    }
    div.input_container {
        display: inline-block;
    }
</style>
<h2><?php echo __("Search Orders", "Search Orders"); ?></h2>
<!-- The following code creates the order search form -->
<?= form_open("/admin/admin/search", array("id" => "order_search"), array("search_type" => "order", "view" => "admin/orders_search_orders", "layout" => "inc/dual_column_left")) ?>
    <fieldset>
        <div class="input_container">
            <?= form_label(__("Bag ID", "Bag ID"), "bag_id") ?>
            <?= form_input(array("id" => "bag_id", "name" => "bag_id", "value" => isset($_POST['bag_id']) ? $_POST['bag_id'] : '')) ?>
        </div> 

        <div class="input_container">
            <?= form_label(__("Order ID", "Order ID"), "order_id") ?>
            <?= form_input(array("id" => "order_id", "name" => "order_id", "value" => isset($_POST['order_id']) ? $_POST['order_id'] : '')) ?>
        </div>

        <div class="input_container">
            <?= form_label(__("Customer Name", "Customer Name"), "customer_name") ?>
            <?= form_input(array("autocomplete" => "off", "id" => "customer_name", "name" => "customer_name", "value" => isset($_POST['customer_name']) ? $_POST['customer_name'] : '')) ?>
        </div>

        <div class="input_container">
            <?= form_label(__("Ticket number", "Ticket #"), "ticketNum") ?>
            <?= form_input(array("id" => "ticketNum", "name" => "ticketNum", "value" => isset($_POST['ticketNum']) ? $_POST['ticketNum'] : '')) ?>
        </div>
        <?= form_submit(array("value" => __("Search", "Search"), "class" => "button orange")); ?>
    </fieldset>
<?= form_close() ?>
<?php
if ($results):
?>
    <p> <?= count($results) . __("results returned", "results returned.") ?> </p>
    <table id="box-table-a">
        <thead>
            <tr>
                <?php 
                    $headers = array_keys($results[0]);
                    foreach ($headers as $header):
                ?>
                    <th> <?= $header ?> </th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $result): ?>
                <tr>
                    <?php foreach ($result as $result_item): ?>
                        <td> <?= $result_item ?> </td>
                    <?php endforeach ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php 
else:
?>
    <p> <?php echo __("No results found", "No results found"); ?> </p>
<?php
endif;
?>
