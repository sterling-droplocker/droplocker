<?php

enter_translation_domain("admin/orders/details");
$dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');
$decimal_positions = get_business_meta($this->business_id, 'decimal_positions', 2);

/**
 * Note, not all the required content variables are known for this view.
 * Expects at least the following content variables:
 *  dueDate_options array An array of valid due dates for an order, including the current due date for the order, if applicable, otherwise should show a place holder for the selected option if the order has no due date.
 *  locker stdclass
 *  location stclass
 *  order_id int
 *  gross_total float
 *  ctotal float
 *  subtotal float
 *  taxable_total float
 *  net_total float
 *
 *  items array Order items for the order. Each order item ust have the following properties
 *      orderItemID
 *      unitPrice
 *      displayName
 *      item_id
 *      barcode
 *      product_process_id
 *      supplier_id
 *      notes
 *      qty
 */

if (!is_array($dueDate_options)) {
    trigger_error("'dueDate_options' must be an array", E_USER_WARNING);
}

if (!empty($customer)) {
    if ($customer->firstName == '' && $customer->lastName == '') {
        $customerFullName = __("new_customer", "New Customer");
    } else {
        $customerFullName = getPersonName($customer);
    }
}

$closed = false;
$captured = false;
$captured_and_closed = false;
$processed = false;

if ($orderPaymentStatusOption_id == 3) {
    $captured = true;
    if ($order->pre_paid_amount <= 0) {
        $closed = true;
        $captured_and_closed = true;
    }
}

if(!in_array($orderStatusOption_id, array(1, 2, 3))) {
    $closed = true;
    $processed = true;
}

$inProcess = $locker->lockerName == 'InProcess';
$statuses_with_emails = $notify_status_emails; //remapped
$email_suffix = __("email_suffix", "[email]");

$status_dropdown = array();
foreach($status as $s) {
    // hide the 'Ready for Customer Pickup' if the order is in an 'InProcess' locker.
    if ($inProcess && $s->orderStatusOptionID == 9) {
        continue;
    }

    $status_dropdown[$s->orderStatusOptionID] = $s->name;
    if (in_array($s->orderStatusOptionID, $statuses_with_emails))  {
        $status_dropdown[$s->orderStatusOptionID] .= " " . $email_suffix;
    }
}


?>
<noscript style="margin: 10px;">
    <h1><?= __("javascript","You must have Javascript enabled in your browser to manage this order")?>  </h1>
</noscript>

<style type="text/css">
    /* The following style class is used to hide the display of content if javascript is disabled in the browser. */
    .javascript_only_content {
        display: none;
    }

    .editable {
        border: 1px transparent inset !IMPORTANT;

    }
    .editable:hover {
        border: 1px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3 !IMPORTANT;
    }

    #order_notes_warning .ui-button {
        padding: .2rem;
        vertical-align: middle;
    }
</style>

<script type="text/javascript">
$(document).ready(function() {
   $("#toggle_link").click();

   createCheckBox( $( ".order_notes_warning_checkbox" ), 0 );


});

/* DROP-4286 jquery styled checkbox */
function createCheckBox(ele, i) {
    var newID = "cbx-"+i;
    ele.attr({ "id": newID  })
        .prop({ "type": "checkbox" })
        .after($("<label />").attr({ for: newID  }))
        .button({ text: false }) 
        .click(function(e) {
            var toConsole = $(this).button("option", {
                icons: {
                    primary: $(this)[0].checked ? "ui-icon-check" : ""
                }
            });
            console.log(toConsole, toConsole[0].checked);
        });
    return ele;
}

</script>

<?= $this->session->flashdata('message')?>

<script type="text/javascript">
order_id = '<?= $order->orderID ?>';
var customer_id = '<?= (isset($customer->customerID)) ? $customer->customerID : "" ?>';

<? if (empty($location)): ?>
    var location_id = null;
<? else: ?>
    var location_id = '<?= $location->locationID?>';
<? endif?>

//The following variable stores any get variables passed in the URL
$_GET = <?= json_encode($_GET) ?>

var upchargeGroups = <?= json_encode($upchargeGroups) ?>;

$(document).ready(function(){

/* <? if (!$show_manual_discounts): ?> PHP commented so js editor plays nice */
$('#show_manual_discounts').click(function(evt) {
    evt.preventDefault();

        $("#active_coupons_warning").dialog({
            modal: true,
            title: "Warning",
            width: "700",
            close: function(event, ui) {
                $("#barcode").focus();
            },
            buttons: {
                "Cancel": function() {
                    $(this).dialog("close");
                },
                "Proceed": function() {
                    $('#manual_discounts').show();
                    $(this).dialog("close");
                }
            }
        })

});
/* <? endif; ?> PHP commented so js editor plays nice */

//The following statement is instended to content that requires javascript to function correctly.
$(".javascript_only_content").show();

$('#barcode').focus();
//The following statement binds the key combination "Control + n" to go to the process order action.
$(document).bind("keydown", "Ctrl+n", function(event) {
    event.preventDefault();
    $.blockUI({ message : "<h1> Processing Order... </h1>"});
    window.location.replace("/admin/orders/process_order/" + order_id);
});

$("#status_link").loading_indicator();
$("#process_next_order").loading_indicator();
$("#update_and_apply_discounts").loading_indicator();


valid_barcode_lengths = []; //Stores an array of valid barcode lengths for the business.

//The following statement retrieves the valid barcode lengths for the business.
$.post("/admin/barcode_lengths/get_valid_barcode_lengths", {}, function(result) {
    valid_barcode_lengths = result;
    $.validator.addMethod("valid_barcode_length", function(value, element)  {
        var found = false;
        if ($.inArray(element.value.length, valid_barcode_lengths) != -1)
        {
            found = true;
        }

        return found
        },
        "Barcode is not a valid length"
    );
    $("#barcode_submit").loading_indicator();
    // The following statement establishes validation on the barcode textbox.
    $("#barcode_form").validate( {
        rules: {
            barcode: {
                required: true,
                valid_barcode_length: true //Note, all barcodes must be 8 or 10 digits.
            }
        },
        errorClass: "invalid",
        "invalidHandler" : function(form, validator) {
            $("#barcode_submit").loading_indicator("destroy");
        },
        submitHandler: function(form) {

            $("body").on("keypress.mynamespace", function() {
                $.post("/admin/orders/entry_error", {orderID:order_id, barcode:$("#barcode").val()}, function(result) {});
                soundManager.play("buzzer");
                $("body").off("keypress.mynamespace");
            });
            form.submit();
            document.getElementById("barcode_submit").type = "input";
            $.blockUI({message : "<h1> <?=__("loading", "Loading..."); ?> </h1>"});

        }
    });
}, "JSON");

$("#uncapture_payment").loading_indicator({
    confirm_message: "<?=__("uncapture_payment_message", "Are you sure you want to uncapture this order?"); ?>",
    callback: function() {
        $.blockUI({message : "<h1 style='padding: 5px'><?=__("uncapture_payment_uncapturing", "Uncapturing Order ..."); ?> </h1>"});
        window.location.replace("/admin/orders/uncapture/" + order_id);
    }
})

$("#gut_order").click(function() {
    if (confirm("<?=__("gut_order_message", "Are you sure you want to gut this order?"); ?>")) {
        window.location.replace("/admin/orders/gut_order/" + order_id);
    }
});

// If true, then this conditional displays a warning message to the user informing them of the notes.
/* <? if (!empty($show_notes_warning)): ?> PHP commented so JS editor plays nice */
$("#order_notes_warning").dialog({
    modal: true,
    title: "<?=__("order_notes_warning_title", "Warning"); ?>",
    width: "700",
    close: function(event, ui) {
        $("#barcode").focus();
    },
    beforeClose: function(event, ui) {
        if ($(".order_notes_warning_checkbox:not(:checked)").length) {
            alert("<?=__("order_notes_warning_message", "You must check all note checkboxes."); ?>");
            return false;
        } else {
            return true;
        }
    },
    buttons: {
        "Proceed to Order" : function() {
            $(this).dialog("close");
        }
    }
});

$("#copy_internal_notes_to_assembly").click(function(evt) {
    $(this).attr("disabled", "disabled");
    $.post("/admin/customers/set_internal_notes", {
        customerID: $("#customer_id").val(),
        internalNotes: ""
    }, function(response) {
        if (response['status'] != "success") {
            alert("<?=__("copy_internal_notes_to_assembly_message", "Could not move to assembly notes."); ?>");
            return false;
        }

        $.post("/admin/orders/set_postAssembly_notes", {
            orderID: $("#order_id").val(),
            postAssembly: $.trim($("#internal_notes").text())
        }, function(response) {
            if (response['status'] == "success") {
                alert("<?=__("copy_internal_notes_to_assembly_moved_message", "Moved assembly notes."); ?>");
                $("#assembly_notes").val($("#internal_notes").text());
            } else {
                alert("<?=__("copy_internal_notes_to_assembly_moved_error", "Could not move to assembly notes."); ?>");
            }
        }, "json");
    }, "json");
});
/* <? endif; ?> PHP commented so JS editor plays nice */


//The following event handler binds the button for partialing an order to display the form to enter the information necessary for partialing an order and generating the partial order receipt.
$("#partial_order").click(function() {
    $("#partial_order_dialog").dialog( {
        width: "450px",
        title: "<?=__("partial_order_title", "Partial Order"); ?>",
        modal: true,
        buttons: {
            "Ok" : function() {
                $("#partial_order_form").submit();
                $(this).dialog("close");
            },
            "Cancel" : function() {
                $(this).dialog("close");
            }
        }
    })
});

$("#capture_payment_button").loading_indicator( {
    confirm_message : "<?=__("capture_payment_button_confirm", "Are you sure you want to CAPTURE this order?"); ?>",
    callback: function() {
        //$("#capture_payment").submit();

    }
});

$("#capture_payment").submit(function() {
    $.blockUI({message: "<h1 style='padding: 5px;'> <?=__("capture_payment_message", "Capturing payment..."); ?> </h1>"});
    return true;
});

$('#pay_action').change(function(evt) {
    var option = $(this).val();
    switch(option) {
        case 'update_credit_card':
            window.open('/admin/customers/billing/<?= $customer->customerID ?>');
            break;

        case 'pay_with_cash':
            showPayWithCashDialog();
            break;

        case 'pay_with_check':
            showPayWithCheckDialog();
            break;

        default:
            return;
    }

    $(this).val('');
});

function showPayWithCashDialog() {
    $("#pay_with_cash_dialog").dialog({
        width: "450px",
        title: "<?=__("pay_with_cash_dialog_title", "Pay with Cash"); ?>",
        modal: true,
        buttons: {
            "Cancel" : function() {
                $(this).dialog("close");
            },
            "Capture Order" : function() {
                $("#pay_with_cash_form").submit();
                $(this).dialog("close");
            }
        }
    })
};

function showPayWithCheckDialog() {
    $("#pay_with_check_dialog").dialog({
        width: "450px",
        title: "<?=__("pay_with_check_dialog_title", "Pay with Check"); ?>",
        modal: true,
        buttons: {
            "Cancel" : function() {
                $(this).dialog("close");
            },
            "Capture Order" : function() {
                $("#pay_with_check_form").submit();
                $(this).dialog("close");
            }
        }
    })
};

var updateUpcharges = function() {
    <? if ($print_day_tags): ?>
    var upchargeGroup_id = $('#product_id option:selected').attr('data-upchargegroup_id');
    var upcharges_holder = $('#upcharges_holder').html('');
    if (typeof(upchargeGroups[upchargeGroup_id]) != 'undefined' && upchargeGroups[upchargeGroup_id].length > 0) {
        var column_width = upchargeGroups[upchargeGroup_id].length / 4;
        var column_count = 0;
        $.each(upchargeGroups[upchargeGroup_id], function(i, o) {
            if (Math.ceil(column_width * column_count) == i) {
                column_count++;
                upcharges_holder.append('<div style="float:left;"></div>');
            }

            var item = '<label style="margin-right:10px;"><input type="checkbox" name="upcharge[]" value="'+o.upchargeID+'">'+o.name+'</label><br>';
            $('div:last', upcharges_holder).append(item);
        });
    }
    <? endif; ?>
};

$('#product_id').change(function(){
    var id = $(this).val();
    $('#img_holder').html('<img src="/images/progress.gif" />');
    $("#error").html("");
    $("#add_item_button").attr("disabled", true).addClass("disabled");

    updateUpcharges();

    $.ajax({
        url:"/admin/orders/get_product_suppliers_ajax/",
        type:'POST',
        dataType:'JSON',
        data:{
            product_process_id : id,
            order_id : order_id
        },
        error: function(jqXHR, exception) {
            //alert("Can not retrieve product suppliers");
        },
        success:function(data) {
            if(data.supplier_id){
                $("#product_processID").val(id);
                $("#washLocation_id").val(data.supplier_id);
            }
            else {
                //alert("Could not retrieve default supplier: " + data.message);
            }
            $("#add_item_button").attr("disabled", false).removeClass("disabled");
            $('#img_holder').html('');
        }
    });
}).change();


    $("#quickwf").click(function() {
        $('#product_id').val('<?= $default_wash_fold_product_process_id ?>').change();
    });

    var order_total = '<?= $order_total?>'.replace(',','.');
    var discount = null;
    $(".charge_button").live('click', charge_button_click);

    function charge_button_click(e, custom_type, custom_description){
        e.preventDefault();

        var custom_type = custom_type || false;
        var custom_description = custom_description || $(this).val();

        // get the number and the type to apply the discount

        var id = $(this).attr('id');
        var arr;
        if (!custom_type) {
            arr = id.split("_");
        } else {
            arr = custom_type.split("_");
        }
        
        var amount = arr[0];
        var type = arr[1];


        if(type=='percent')
        {
            discount = Math.floor(order_total * amount) / -100;
            $("#charge_amount").val(discount);
            $("#charge_type").val(custom_description);
        }

        if(type=='dollar')
        {
            discount = "-"+amount;
            $("#charge_amount").val(discount);
            $("#charge_type").val(custom_description);
        }

        if(type=='free')
        {
            discount = "-"+order_total;
            $("#charge_amount").val(discount);
            $("#charge_type").val(custom_description);
        }

        if(type=='minimum'){
            if(order_total > amount){
                    alert("<?=__("error_order_total_greater_than_amount", "Error: Order total is greater than"); ?> "+ amount);
            }
            else{
                discount = amount - order_total;
                $("#charge_amount").val(discount.toFixed(2));
                $("#charge_type").val(custom_description);
            }
        }

        if(type=='homedelivery'){
            discount = order_total*.1;
            $("#charge_amount").val(discount.toFixed(2));
            $("#charge_type").val(custom_description);
        }

        }


    var old_location = '';
    var old_locker = '';
    $("#location_link").click(function()
    {
        old_location = $("#location_holder").html();
        old_locker = $('#locker_holder').html();
        $("#location_holder").html('<img src="/images/progress.gif" />');
        $("#location_link").css('display', 'none');

        var select = $("<select name='locationID' id='locationID'></select>");
        $.ajax({
                url:"/ajax/get_locations/",
                type:'POST',
                dataType:'JSON',
                data:
                {
                    submit:true,
                    locationID: location_id
                },

                success: function(data){
                    if(data.length){
                        $("#location_cancel").css('display', 'inline');

                        $.each(data, function(i, item)
                        {
                            if(item.locationID == location_id)
                            {
                                var opt = $('<option selected="true" value="'+item.locationID+'">'+item.address+'</option>');
                            }
                            else
                            {
                                var opt = $('<option value="'+item.locationID+'">'+item.address+'</option>');
                            }
                            select.append(opt);
                        });
                    }
                    else
                    {
                        $("#error").html("<?=__("error_no_suppliers", "No Suppliers"); ?>, <a href='/admin/admin/suppliers'><?=__("add_one_supplier_now", "Add One Now"); ?></a>").css("color","#cc0000");
                    }
                    $('#location_holder').html(select);
                }
        });
    });

    $("#location_cancel").live("click", function(){
        $('#location_holder').html(old_location);
        $('#locker_holder').html(old_locker);
        $("#location_edit").css('display', 'none');
        $("#location_link").css('display', 'inline');
        $("#location_cancel").css('display', 'none');
    });

    $("#location_accept").live('click', function(){
        var location_id = $("#locationID").val();
        var locker_id = $("#lockerID").val();
        var order_id = '<?= $order->orderID?>';
        top.location.href = "/admin/orders/update_order_location/"+location_id+"/"+locker_id+"/"+order_id;
    });

    $('#locationID').live("change", function(){
        $('#locker_holder').html("<img src='/images/progress.gif' />");
        $("#locker_link").css('display', 'none');
        $("#location_cancel").css('display', 'none');
        var select = $("<select id='lockerID'></select>");
        $.ajax({
            url:"/ajax/get_lockers_at_location/",
            type:'POST',
            dataType:'JSON',
            data:{submit:true, location_id:$(this).val()},
            success:function(fullData) {
                $.each(fullData, function(i, item){
                    $('#lockerTypeName').html(i);
                    data = item;
                });

                $("#location_edit").css('display', 'inline');
                $.each(data, function(i, item){
                        var opt = $('<option value="'+i+'">'+item+'</option>');
                        select.append(opt);
                });

                $('#locker_holder').html(select);
            }
        });
    });

    $('#locker_link').live('click', function(){
        $('#locker_holder').html("<img src='/images/progress.gif' />");
        $(this).css('display', 'none');
        var select = $("<select id='lockerID'></select>");
        $.ajax({
            url:"/ajax/get_lockers_at_location/",
            type:'POST',
            dataType:'JSON',
            data:{submit:true, location_id:location_id},
            success:function(fullData){
                $.each(fullData, function(i, item){
                    $('#lockerTypeName').html(i);
                    data = item;
                });
                $("#location_edit").css('display', 'inline');

                $.each(data, function(i, item){
                    var key = i.split("-");
                    var opt = $('<option value="'+key[0]+'">'+item+'</option>');
                    select.append(opt);
                });


                $('#locker_holder').html(select);
            }
        });
    });

    $("#status").change(function(){
            $("#status_link").css('display', "inline");
    });

    $("#status_link").click(function(){
            top.location.href="/admin/orders/update_order_status/<?= $order->orderID?>/"+$("#status").val();
            $(this).attr("disabled", "disabled");
    });

    $("#bagID").change(function(){
            $("#bag_link").css('display', "inline");
    });

    $("#bag_link").click(function(){
            if(confirm("<?=__("bag_link_confirm", "Are you sure you want to change the bag number for this order?"); ?>")){
                top.location.href = "/admin/orders/update_order_bag/<?= $order->orderID?>/"+$('#bagID').val();
            }
    });

    var old_customer = '';
    $("#reassign_link").bind('click', function(){
        //reassign_order
        old_customer = $("#user_holder").html();
        $("#user_holder").html("<input type='text name='customer' id='customer' /><div id='customer_results'></div><input type='hidden' id='customer_id' name='customer_id' value='customer_id' />");
        $("#reassign_link").css("display", "none");
        $("#customer_action_holder").css("display", "inline");
        $('#customer').autocomplete({

            source: function(req, add){

            //pass request to server
                $.getJSON("/ajax/customer_autocomplete_aprax/", req, function(data) {
                    dc=data;
                    //create array for response objects
                    var suggestions = [];

                    //process response
                    $.each(data, function(i, val){
                        suggestions.push(data[i]);

                        });

                    //pass array to callback
                    add(suggestions)
                });
            },
                appendTo: "#customer_results",
                select : function(e, ui){
                    $.each(dc, function(i, customer){
                        if(ui.item.value== customer['label'])
                            $('#customer_id').val(customer['id']); //This should be the customer ID
                    });
                }
        });

    });

    $("#related_claims .close").bind('click', function(evt){
       $("#related_claims").hide();
    });

    $("#show_related_claims").bind('click', function(evt){
       $('#related_claims').toggle();
    });

    $("#related_claims li").bind('click', function(evt){
       $("#reassign_link").click();
       $("#related_claims").hide();
       $('#customer').val($('span', this).text());
       $('#customer_id').val($('a', this).text());
       $('#claim_id').val($(this).attr('data-claim_id'));
    });

    $("#update_customer_btn").click(function(){
        $("#update_customer_btn").attr('disabled',true).html("<img src='/images/progress.gif' /> working...");
        $.ajax({
            url:"/admin/orders/reassign_order/<?= $order->orderID?>",
            type:'POST',
            dataType:'HTML',
            data:{
                reassign_submit:true,
                AJAX:true,
                customer_id:$('#customer_id').val(),
                location: location_id,
                claim_id: $('#claim_id').val()
            },
            success:function(data){
                if(data=='success'){
                    top.location.href = "/admin/orders/details/<?= $order->orderID?>";
                }
                else{
                    alert(data);
                }
            }
        });
    });

    $("#cancel_customer_btn").click(function(){
            $("#user_holder").html(old_customer);
        $("#reassign_link").css("display", "inline");
        $("#customer_action_holder").css("display", "none");
        $('#claim_id').val(''); //Empty the claim_id
    });


    $("#add_item_button").click(function(){
        $(this).css('display', 'none');
        $('#after_add_item_submit').html("<img src='/images/progess.gif' /> Working...");
    });

    //The following event handler is for updating the order due date.
    $("#dueDate").editable("/admin/orders/update", {
        data : '<?= json_encode($dueDate_options);?>',
        type : 'datepicker',
        datepicker: {
            minDate: 0,
            dateFormat: 'yy-mm-dd'
        },
        "cancel" : "<img src='/images/icons/delete.png' />",
        "submit" : "<img src='/images/icons/accept.png' />",
        "indicator" : "Saving ... <img src='/images/progress.gif' />",
        "submitdata": function ( value, settings ) {
            return {
                "property" : $(this).attr("data-property"),
                "orderID": $(this).attr("data-id")
            }
        }
    });


    $(".print_day_tags").click(function(e){
        var href = $(this).attr("href");
        href += '?per_page=' + $("#tags_per_page").val();
        $(this).attr("href", href);
    });

    $(".charge_button_delete").live('click', function(e){
        if (confirm('<?=__("custom_discount_delete_confirm", "Are you sure ?"); ?>')) {
            var discount = $(this).parent();            
            var amount = discount.attr('id').split('_')[0];
            var description = discount.attr('value');
            var type = '_' + discount.attr('id').split('_')[1];

            if (type == '0_free') amount = 0;

            type = amount + type;

            // save the discount
            $.ajax({
                url:"/admin/orders/delete_custom_manual_discount",
                type:'POST',
                dataType:'html',
                data:{
                    amount:amount,
                    description: description,
                    type: type,
                },
                success:function(data){
                    if(data != 'success'){
                        alert('<?=__("custom_discounts_not_deleted_alert", "Something went wrong. Please, try again"); ?>');
                        return false;
                    }
                    discount.remove();
                },
                error:function(error) {
                    alert('<?=__("custom_discounts_not_deleted_alert", "Something went wrong. Please, try again"); ?>');
                }
            });
        };
        return false;
    });

    $(".charge_button_custom").click(function(e){
        $("#custom_discount_dialog").dialog({
            modal: true,
            title: "<?=__("custom_discount_dialog_title", "Custom discount"); ?>",
            width: "400",
            buttons: {
                "Cancel" : function() {
                    $(this).dialog("close");
                },
                "Apply" : function() {
                    var amount = parseFloat($('#custom_discount_amount').val());
                    var description = $('#custom_discount_description').val();
                    var type = $('#custom_discount_type').val();

                    if (isNaN(amount) || description.trim().length == 0 ) {
                        alert('<?=__("custom_discounts_alert", "Please, complete all fields with the correct values"); ?>');
                        return false;
                    }

                    if (type == '0_free') amount = 0;

                    type = amount + type;

                    // save the discount
                    $.ajax({
                        url:"/admin/orders/save_custom_manual_discount",
                        type:'POST',
                        dataType:'html',
                        data:{
                            amount:amount,
                            description: description,
                            type: type,
                        },
                        success:function(data){
                            if(data != 'success'){
                                if (data == 'exists') {
                                    alert('<?=__("custom_discounts_not_saved_exists_alert", "Something went wrong. Discount already exists"); ?>');
                                } else {
                                    alert('<?=__("custom_discounts_not_saved_alert", "Something went wrong. Please, try again"); ?>');
                                }
                                return;
                            }

                            var new_discount = '<button class="charge_button" id="'+type+'" value="'+description+'">'+description+' <a class="charge_button_delete" href=""><img src="/images/icons/cross.png"></a></button>';
                            $(new_discount).insertBefore('.charge_button_custom');
                        },
                        error:function(error) {
                            alert('<?=__("custom_discounts_not_saved_alert", "Something went wrong. Please, try again"); ?>');
                        }
                    });

                    $(this).dialog("close");
                }
            }
        });
    });
});


</script>

<div class="javascript_only_content">
    <div style="min-height: 40px; max-height: 40px;">
        <div style="display:table; font-size: 1.8em;">
            <div style="display: table-row">
                <div style="display: table-cell; vertical-align: top; padding: 10px;">
                    <?= __("order","Order") ?> <?= $order_id?>
                </div>
                <div style="display: table-cell; vertical-align: top; padding: 10px;" >
                    <?= __("due_date","Due Date: ")?>
                </div>
                <div style="display: table-cell; color: green; font-size: 0.8em; vertical-align: middle; padding: 7px;" class="editable" id="dueDate" data-property="dueDate" data-id="<?=$order->orderID?>">
                    <?= (empty($order->dueDate) || $order->dueDate == '0000-00-00') ? __("not_set", "---Not Set---") : strftime($dateFormat, strtotime($order->dueDate))?>
                </div>
            </div>
        </div>
    </div>
    <audio id="buzzer" src="/sounds/buzzer.mp3" preload="auto"> </audio>
    <div style='margin-top:5px;outline:0px solid #ccc;float:left;width:50%;'>
        <table class='order_detail_table'>
            <tr>
                <th><?= __("customer","Customer")?></th>
                <td>
                    <span id='user_holder'>
                        <? if (empty($customer)): ?>
                             <?= __("no_customer","---No Customer---")?>
                        <? else: ?>
                            <a href="/admin/customers/detail/<?= $customer->customerID ?>" target="_blank"><?= $customerFullName?></a>
                        <? endif ?>
                    </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a id='reassign_link' href='javascript:;'><?= __("reassign_order", "Reassign order") ?></a>

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a id='show_related_claims' href='javascript:;' title='<?= __("show_claimed_orders", "Show claimed orders in this location (%count% open claims)", array('count' => count($related_claims))) ?>'><img src="/images/icons/comments.png"></a>
                        &nbsp;
                        <div style='position: absolute; display: inline-block;'>
                        <div id='related_claims' class='popup_menu' style='display: none;'>
                            <div class="close">&times;</div>
                            <? if (!empty($related_claims)): ?>
                            <h4><?= __("claimed_orders_at","Claimed Orders at %address%, %city%, %state%", array('address' => $location->address, 'city' => $location->city, 'state' => $location->state))?></h4>
                            <ul>
                                <? foreach ($related_claims as $i => $claim): ?>
                                <li data-claim_id="<?= $claim->claimID ?>"><span><?= htmlentities(getPersonName($claim)) ?></span> <a style="color: blue; float: right;" href='/admin/customers/detail/<?= $claim->customerID?>'><?= $claim->customerID ?></a></li>
                                <? endforeach; ?>
                            </ul>
                            <? else: ?>
                            <h4><?= __("no_open_claims","No open claims at this location")?></h4>
                            <? endif; ?>
                        </div>
                        </div>
                        <input type='hidden' name='claim_id' value='' id='claim_id'>

                    <span id='customer_action_holder' style='display: none'>
                        <a id='update_customer_btn' href='javascript:;'><?= __("update","Update")?></a>
                        <a id='cancel_customer_btn' href="javascript:;"><?= __("cancel","Cancel")?></a>
                    </span>
                        <div id="internal_notes" style="display: none;"> <?= $customer->internalNotes ?> </div>
                        <input type="hidden" id="customer_id" value="<?= $customer->customerID ?>" />
                        <input type="hidden" id="order_id" value="<?= $order_id ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("bag","Bag")?></th>
                <td>
                    <? if(count($bags) > 1): ?>
                        <?= form_dropdown("bagID", $bags, $order->bag_id, "id='bagID'") ?>
                        <a style='display:none' id='bag_link' href='javascript:;'><img src='/images/icons/accept.png' /></a>
                    <? else: ?>
                        <? if(count($bags) == 1): ?>
                            <?= $bags[0]->bagNumber?>
                        <? else: ?>
                            <?= __("no_bags", "---No Bags---") ?>
                        <? endif; ?>
                        <input type='hidden' value='<?= $bags[0]->bagID?>' name='bagID' />
                    <? endif; ?>
                    <span id='bag_working'></span>
                </td>
            </tr>
            <tr>
                <th><?= __("location","Location")?></th>
                <td>
                    <span id='location_holder'><?= __('location_address', "%address%, %city%, %state%", stdobj_to_array($location) ) ?></span> <a id='location_link' href='javascript:;'><img src='/images/icons/pencil.png' /> </a> <i style="margin-left: 20px;"> (Route # <?= $location->route_id ?>) </i>
                    <span style='display:none' id='location_cancel'><a id='location_cancel' href='javascript:;'><?= __("cancel","Cancel")?></a></span>
                </td>
            </tr>
            <tr>
                <th><span id='lockerTypeName'><?= __("locker","Locker")?></span></th>
                <td><span id='locker_holder'><?= $locker->lockerName?></span> <a id='locker_link' href='javascript:;'><img src='/images/icons/pencil.png' /></a>
                    <span style='display:none' id='location_edit'><a id='location_accept' href='javascript:;'><img src='/images/icons/accept.png' /></a> <a id='location_cancel' href='javascript:;'><img src='/images/icons/cross.png' /></a></span>
                </td>
            </tr>
            <tr>
                <th><?= __("status","Status")?></th>
                <td>
                    <?= form_dropdown('status', $status_dropdown, $orderStatusOption_id, 'id="status"'); ?>
                    <span id="has_email"></span>
                    <input type="button" style='display:none' class="button blue" style="margin: 3px 1px;" id='status_link' href='javascript:;' value="<?= __("update", "Update") ?>" />
                </td>
            </tr>
            <tr>
                <th><?= __("payment","Payment")?></th>
                <td>

                    <? if($orderPaymentStatusOption_id!=3):?>
                        <? if($customer->invoice==1): ?>
                            <?= __("on_invoice","On Invoice")?>
                        <? else: ?>
                            <form id="capture_payment" action='/admin/orders/capture/<?= $order_id?>' method='post' style="display:inline;">
                                <input type='hidden' name='process[]' value='<?= $order_id?>' />
                                <input type='submit' name='submit' id="capture_payment_button" value='<?= __("capture_payment","Capture Payment")?>' />
                            </form>

                            <form id="mark_as_captured" action='/admin/orders/mark_as_captured/<?= $order_id?>' method='post' style="float:right; margin-left: 10px;">
                                <input type='hidden' name='order_id' value='<?= $order_id?>' />
                                <input type='submit' onclick='return confirm(<?= json_encode(__("confirm_mark_capture", "Are you sure you want to mark this order as captured?")) ?>);' name='submit' id="mark_as_captured_button" value='<?= __("change_to_captured","Change To Captured")?>' style="color: Red;"/>
                            </form>

                            <? if ($pay_with_cash): ?>
                                <select id="pay_action" style="float:right;">
                                    <option value=""><?= __("pay_action_description","--Select Payment--")?></option>
                                    <option value="pay_with_cash"><?= __("pay_with_cash","Pay with Cash")?></option>
                                    <option value="pay_with_check"><?= __("pay_with_check","Pay with Check")?></option>
                                    <option value="update_credit_card"><?= __("update_credit_card","Update Credit Card")?></option>
                                </select>

                            <? else: ?>
                                <button style="float:right;" onclick="window.open('/admin/customers/billing/<?= $customer->customerID ?>'); return false;">
                                    <?= __("update_cc","Update Credit Card")?>
                                </button>
                            <? endif; ?>

                        <? endif; ?>
                    <? else: ?>
                        <span><?= __("captured","Captured")?> </span><input type="button" id="uncapture_payment" value="<?= __("mark_order_as_not_captured","Mark order as not captured")?>" style="float: right; color: Red;">
                    <? endif; ?>
                </td>
            </tr>
        </table>

    </div>
    <div style='float:left; width:30%'>
        <form action='/admin/orders/update_notes' method='post'>
            <?= form_hidden("order_id", $order_id) ?>
            <div>
                <div style='margin:20px 5px 5px 5px;'>
                    <div style='float:left;'>
                        <h3><?= __("order_notes","Order Notes")?></h3>
                    </div>
                    <div>
                        <textarea id="order_notes" style='width:100%; height:80px; border:1px solid #ccc;' name='notes'><?= $order->notes ?></textarea>
                    </div>
                </div>
            </div>
            <div>
                <div style='margin:5px;'>
                    <h3><?= __("assembly_notes","Assembly Notes")?></h3>
                    <div>
                        <textarea id="assembly_notes" style='width:100%;height:33px; border:1px solid #ccc;' name='postAssembly'><?= $order->postAssembly?></textarea>
                        <input style='float:right;' type='submit' name='order_notes_submit' value="<?= __("update_notes","Update Notes")?>" />
                    </div>
                </div>
            </div>
        </form>
        <? if($order->llNotes): ?>
            <div>
                <div style='margin:5px;'>
                    <h3><?= __("internal_notes","Internal Notes")?></h3>
                    <div><?= $order->llNotes?></div>
                </div>
            </div>
        <? endif; ?>

    </div>
        <div style='text-align: right; padding-top:3px; padding-bottom:5px; position: absolute; right: 22px;'>
            <?php if($location->locationType_id=='12'): ?>
                <a href="/admin/orders/unload_conveyor/<?= $order_id?>"><?= __("unload","Unload")?></a> |
            <?php  endif; ?>
            <a href="/admin/printer/print_order_receipt/<?= $order_id?>" target="_blank"><?= __("print","Print")?></a> |
            <a href="/admin/orders/receipt/<?= $order_id?>" target="_blank"><?= __("details","Details")?></a> |
            <? if ($order->customer_id && $order->locker_id): ?>
                <a href="/admin/reports/metalprogetti_history/<?= $order_id?>" target="_blank"><?= __("assembly","Assembly")?></a> |
                <a href="/admin/orders/split_items?orderID=<?= $order_id ?>"><?= __("split","Split")?></a> |
                <!-- <a href="/admin/orders/create_shoe_shine?customerID=<?= $order->customer_id ?>&lockerID=<?= $order->locker_id ?>"><?= __("creat_shoe_shine","Create Shoe Shine")?></a> -->
            <? endif; ?>
        </div>
    <div style='float:right; width: 20%; margin-top: 20px;'>

        <table class='order_detail_table'>
            <tr>
                <th><?= __("gross_total","Gross Total")?></th>
                <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $gross_total) ?></td>
            </tr>
            <tr>
                <th><?= __("charges_discounts","Charges/Discounts")?></th>
                <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $ctotal) ?></td>
            </tr>
            <? if (!$breakout_vat): ?>
            <tr>
                <th><?= __("subtotal","Subtotal")?></th>
                <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $subtotal) ?> </td>
            </tr>
            <? foreach ($order_taxes as $tax): ?>
            <tr>
                <th> <?= count($order_taxes) > 1 && $tax->name ? sprintf('%s (%s%%)', $tax->name, number_format_locale($tax->taxRate * 100, 2)) : __("tax_field", "Tax") ?> </th>
                <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $tax->amount) ?></td>
            </tr>
            <? endforeach; ?>
            <tr>
                <th><?= __("net_total","Net Total")?></th>
                <th align="right"><?= format_money_symbol($this->business_id, '%.2n',$net_total) ?></th>
            </tr>
            <? else: ?>
            <tr>
                <th><?= __("net_total","Net Total")?></th>
                <th align="right"><?= format_money_symbol($this->business_id, '%.2n', $net_total) ?></th>
            </tr>
            <tr>
                <th><?= __("subtotal","Subtotal")?></th>
                <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $subtotal) ?> </td>
            </tr>
            <? foreach ($order_taxes as $tax): ?>
            <tr>
                <th> <?= count($order_taxes) > 1 && $tax->name ? sprintf('%s (%s%%)', $tax->name, number_format_locale($tax->taxRate * 100, 2)) : __("tax_field", "Tax") ?> </th>
                <td align="right"><?= format_money_symbol($this->business_id, '%.2n', $tax->amount) ?></td>
            </tr>
            <? endforeach; ?>
            <? endif; ?>
        </table>
        <? if( !$captured_and_closed ): ?>
            <input type="button" style="color: red; float: right; font-size: 0.7em;" id="gut_order" value="<?= __("gut_order","Gut Order")?>" />
        <? endif; ?>
    </div>

    <div>
    <br style='clear:both'>
    <div>
        <table width="100%">
            <tr>
                <td width='30%'>
                    <div style='padding:10px 0px; text-align: center;' class='orange_box'>
                        <? if($closed) : ?>

                            <? if ($captured && $processed): ?>
                                <?= __("message_order_captured_processed", "Adding Items is not allowed for this order because order has been processed and payment has been captured") ?>
                            <? elseif ($captured): ?>
                                <?= __("message_order_captured", "Adding Items is not allowed for this order because payment has been captured") ?>
                            <? elseif ($processed): ?>
                                <?= __("message_order_processed", "Adding Items is not allowed for this order because order has been processed"); ?>
                            <? endif ?>
                        <? else : ?>
                            <? if ( $captured ): ?>
                                <?= __("message_force_allow_edit", "Editing is allowed for this captured order because it has a pre paid amount of %pre_paid_amount%", array('pre_paid_amount' => format_money_symbol($this->business_id, '%.2n', $order->pre_paid_amount))) ?>
                            <? endif ?>
                            <? if ($print_day_tags): ?>
                                <div id="barcode_form">
                                    <a href="/admin/printer/print_day_tags/<?= $order->orderID ?>" target="_blank" class="button blue print_day_tags"><?= __("print_day_tags","Print Day Tags")?></a>
                                    <input type="text" name="tags_per_page" id="tags_per_page" value="1" size="2" />
                                    <?= __("tags_per_page"," per page")?>
                                </div>
                            <? else: ?>
                                <form id="barcode_form" action='/admin/orders/add_item_to_order/' method='post'>
                                    <label for="barcode"><?= __("barcode", "Barcode:") ?></label>
                                    <input type="text" style='width:100px;font-size:14px;padding:3px;border:1px solid #f8951c' autocomplete="off" name='barcode' id='barcode' />
                                    <input type="submit" id='barcode_submit' class="button blue" style="margin: 3px" value='<?= __("add","Add")?>'/>
                                    <input type='hidden' value='<?= $order_id?>' name='order_id' />
                                </form>
                            <? endif; ?>

                        <? endif; ?>
                    </div>
                </td>
                <td width="*" style='padding-left:20px;'>
                    <form action='/admin/orders/update_ticket/<?= $order_id?>' method='post'>
                        <table>
                            <tr>
                                <td style='vertical-align: top; text-align: right'><?= __("ticket_number","Ticket<br>Number")?></td>
                                <td>
                                        <input type="text" style='width:170px;font-size:16px;' id='ticketNum' name='ticketNum' value="<?= $order->ticketNum?>" />
                                </td>
                                <td>
                                        <input type='submit' value='<?= __("update_ticket","Update Ticket")?>' name='ticket_submit' />
                                </td>
                                <td>
                                      <?= __("items", "Items:") ?>  <span id='item_count'><?= $qty?></span> <span style='color:#cc0000;display:none' id='ticket_response'><?= __("saved","Saved")?></span>
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>
                <td align='right'>
                    <? if(in_array($orderStatusOption_id, array(1,5))): ?>
                    <a href='/admin/orders/process_order/<?= $order_id?>' id="process_next_order" class='button orange'><?= __("process_next_order","Process Next Order")?></a>
                    <? endif; ?>
                    <? if((!in_array($orderStatusOption_id, array(1,5))) && !$captured_and_closed ): ?>
                    <a href='/admin/orders/process_order/<?= $order_id?>' id="update_and_apply_discounts" class='button orange'><?= __("update_and_apply_discounts","Update and Apply Discounts")?></a>
                    <? endif; ?>
                </td>
            </tr>
        </table>
    </div>

<form action='/admin/orders/edit_order_items/<?= $order->orderID?>' method='post'>
    <table class="box-table-a">
        <thead>
            <tr>
                <th><?= __("product","Product")?></th>
                <th><?= __("qty","Qty")?></th>
                <th><?= __("unit_price","Unit Price")?></th>
                <th><?= __("notes","Notes")?></th>
                <th><?= __("supplier","Supplier")?></th>
                <th><?= __("barcode","Barcode")?> <a href="/admin/orders/view_order_as_closet/<?= $order->customer_id ?>/<?= $order->orderID ?>"><img src='/images/icons/camera.png' /></a></th>
                <? if(!$closed):?>
                <th align='center' style='width:35px;'>
                        <a style='color:#cc0000' onClick='return verify_delete()' href='/admin/orders/clear_order_items/<?= $order->orderID?>'><?= __("delete_all","Delete All")?></a>
                </th>
                <? endif; ?>
            </tr>
        </thead>
                <tbody>
                    <?php
                    $count = 0;
                    foreach ($items as $item) :
                        $id = $item->orderItemID
                    ?>
                        <tr id="row-<?= $id ?>">
                            <?= __("this_is_the_product_column","<!-- This is the 'Product' column -->")?>
                            <td>
                                <?= $item->displayName?> (<?= format_money_symbol($this->business_id, '%.2n',$item->unitPrice)?>)
                                <? if($item->slug == "wf"): ?>
                                    <br><a href="/admin/orders/wf_print/<?= $order->orderID ?>" target="_blank"><?= __("print_traveller","Print Traveller")?></a>
                                    <br><?= __("weight_in","Weight In ")?><?= $wfLog[0]->weightIn ?><?= __("weight_out","   | Weight Out: ")?><?= $wfLog[0]->weightOut ?>
                                <? endif; ?>
                            </td>
                            <!-- This is the 'Qty' column -->
                            <td><input style='width:40px' type='text' name='qty[<?= $id?>][qty]' value='<?= $item->qty?>' /></td>

                            <!-- This is the 'Unit Price' column -->
                            <td><input style='width:50px' type='text' name='unitPrice[<?= $id?>][unitPrice]' value='<?= number_format((float)$item->unitPrice, $decimal_positions, '.', ''); ?>' /></td>


                            <!-- This is the 'Notes' column -->
                            <td><textarea style='width:95%' name='notes[<?= $id ?>][notes]'><?= trim(preg_replace("#<br[ /]*>#i", "\n", $item->notes)) ?></textarea></td>
                            <!-- THis is the 'Supplier' column -->
                            <td>
                                <?= form_dropdown("supplier_id[$id][supplier_id]", $suppliers, $item->supplier_id) ?>
                            </td>
                            <!-- THis is the 'barcode' column -->
                            <td><a href='/admin/orders/edit_item?orderID=<?= $order->orderID?>&itemID=<?= $item->item_id?> '><?= ($item->barcode) ? $item->barcode : (($item->item_id) ? __("no_barcode_assigned","No barcode assigned") : '') ?></a></td>
                            <!-- This is the 'closed' column -->
                            <? if(!$closed):?>
                            <td align='center'>
                                    <a onClick='return verify_delete();' href='/admin/orders/delete_item/<?= $order->orderID?>/<?= $item->orderItemID ?>'><img src='/images/icons/cross.png' /><a>
                            </td>
                            <? endif; ?>
                        </tr>
                    <?php $count++; endforeach; ?>
                    <?php if(!$closed):?>
                        <tr>
                            <td colspan="10"><input type='submit' value='<?= __("update","Update")?>' class='button blue'/></td>
                        </tr>
                    <?php endif;?>
                </tbody>
    </table>
</form>

<? if(!$closed):?>
    <form action='/admin/orders/add_product_to_order' method='post'>
        <table class="box-table-a">
            <tbody>
                    <tr>
                        <td>
                            <select style='width:90%' id='product_id' name='product_process_id'>
                                <? foreach($products as $p) : ?>
                                    <option data-upchargeGroup_id="<?= $p->upchargeGroup_id ?>" value="<?= $p->product_processID?>"><?= $p->name?>&nbsp;&nbsp;&nbsp;&nbsp;<?= format_money_symbol($this->business_id, '%.2n', $p->price)?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $p->process_name?></option>
                                <? endforeach ; ?>
                            </select>
                            <div id='process_holder'></div>
                            <a id='quickwf' href='javascript:;'><?= __("wf","WF")?></a>
                            <div id='upcharges_holder' style=''></div>
                        </td>
                        <td>
                            <input type='text' style='width:30px;' id='qty' name='qty' value='1' />
                        </td>

                        <td colspan='2'>
                            <strong style='font-weight:bold'><?= __("notes","Notes")?></strong><br>
                            <textarea style='border:1px solid #ccc; width:98%;height:50px;' id='notes' name='notes'></textarea>
                        </td>
                        <td>
                            <?= form_dropdown("supplier_id", $suppliers, "", "id='washLocation_id'") ?>
                            <span id='img_holder'></span>

                            <div id='error'></div>
                        </td>

                        <td colspan='2'>
                            <input type='hidden' value='<?= $order->customer_id?>' name='customer_id' />
                            <input type='hidden' value='<?= $order->orderID?>' name='order_id' />
                            <input type='submit' class='button blue' id='add_item_button' name='submit' value='<?= __("add","Add")?>' />
                            <span id='after_add_item_submit'></span>
                        </td>
                    </tr>
            </tbody>
        </table>
    </form>
<? endif; ?>



    </div>
    <div style="display: table;">
        <!-- This is the Order Charges /Discounts table -->
        <div style="display: table-cell;">
            <div style='margin:5px;margin-left:0px;'>
                <h3 id="show_manual_discounts"><?= __("order_charges","Order Charges/Discounts")?></h3>

                <table class="box-table-a">
                    <thead>
                        <tr>
                            <th style="text-align: left;"><?= __("description","Description")?></th>
                            <th style="text-align: left;width: 200px;"><?= __("amount","Amount")?></th>
                            <th style="text-align: left;width: 50px;"><?= __("taxable","Taxable")?></th>
                            <? if(!$closed):?><th style="width: 50px;">&nbsp;</th><?endif;?>
                        </tr>
                    </thead>
                    <tbody>
                        <? if($charges): foreach($charges as $charge): ?>
                            <tr>
                                <td><?= $charge->chargeType?></td>
                                <td><?= number_format_locale($charge->chargeAmount, get_business_meta($this->business_id, 'decimal_positions', 2))?></td>
                                <td><?= $charge->taxable ? __('yes', 'Yes') : __('no', 'No') ?></td>
                                <? if(!$closed):?>
                                <td align="center">
                                        <a onClick='return verify_delete()' href='/admin/orders/delete_charge_from_order/<?= $charge->orderChargeID?>/<?= $order_id?>'><img src='/images/icons/cross.png' /></a>
                                </td>
                                <? endif; ?>
                            </tr>
                        <? endforeach; endif; ?>
                    </tbody>
                </table>

                <? if(!$closed):?>
                <form action="/admin/orders/add_charge_to_order/<?= $order_id?>" method='post' id="manual_discounts" style="<?= $show_manual_discounts ? "" : "display:none;" ?>">
                    <table class="box-table-a">
                        <tr>
                            <td><input type='text' id='charge_type' name='charge_type' /></td>
                            <td style="width:200px;"><input type='text' id='charge_amount' name='charge_amount' /></td>
                            <td style="width:50px;"><input type='submit' id="add_charge" name='submit' value='Add' /></td>
                        </tr>
                        <tr>
                            <td colspan="3" id="manual_dicounts">
                                <input type='button' class='charge_button' id='5_percent' value='5% Discount'/>
                                <input type='button' class='charge_button' id='10_percent' value='10% Discount'/>
                                <input type='button' class='charge_button' id='25_percent' value='25% Discount'/>
                                <input type='button' class='charge_button' id='50_percent' value='50% Discount'/>
                                <input type='button' class='charge_button' id='5_dollar' value='<?= format_money_symbol($this->business_id, '%.2n', 5)?> Discount'/>
                                <input type='button' class='charge_button' id='10_dollar' value='<?= format_money_symbol($this->business_id, '%.2n', 10)?> Discount'/>
                                <input type='button' class='charge_button' id='15_minimum' value='<?= format_money_symbol($this->business_id, '%.2n', 15)?> Minimum'/>
                                <input type='button' class='charge_button' id='10_homedelivery' value='10% Home Delivery Charge'/>
                                <input type='button' class='charge_button' id='0_free' value='Free Order'/>
                                <?php if (!empty($custom_manual_discounts)): ?>
                                    <?php 
                                    foreach ($custom_manual_discounts as $cd): 
                                        echo '
                                            <button class="charge_button" id="'.$cd->type.'" value="'.$cd->description.'">'.$cd->description.' <a class="charge_button_delete" href=""><img src="/images/icons/cross.png"></a></button>
                                        ';    
                                    endforeach; 
                                    ?>
                                <?php endif; ?>
                                <input type='button' class='charge_button_custom' value='Custom' style="margin-left:3px;"/>
                                <p class="alert-warning"><?= __("add_manual_dicount_warning","Adding a manual discount to this order will not allow further coupons to be automatically added to this order.")?></p>
                            </td>
                        </tr>
                    </table>
                </form>

                <div id="active_coupons_warning" style="display:none;">
                    <div class="alert-warning">
                        <h2><?= __("active_coupons_warning_title","Warning.")?></h2>
                        <p><?= __("active_coupons_warning_message","Adding a manual discount to this order will not allow further coupons to be automatically added to this order.")?></p>
                    </div>
                    <? if (!empty($activeCoupons)): ?>
                        <div style="text-align: left;">
                            <h4><?= __("active_coupons_warning_list","Currently the following coupons are cued to be added to this order automatically:")?> </h4>
                            <ul>
                            <? foreach($activeCoupons as $discount): ?>
                                <li><?= $discount->description ?>
                                    ( <?
                                        if($discount->amountType == 'dollars') {
                                            echo format_money_symbol($this->business_id, '%.2n', $discount->amount);
                                        } else if ($discount->amountType == "percent") {
                                            echo number_format_locale($discount->amount,0).'%';
                                        } else if ($discount->amountType == "pounds") {
                                            echo $discount->amount . weight_system_format('', false, true );
                                        } else {
                                            echo $discount->amount;
                                        }
                                    ?> )
                                </li>
                            <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                </div>
                <? endif; ?>
            </div>
        </div>
        <!-- This is the Order History Table -->
        <div style='display: table-cell;'>
            <div style='margin:5px;margin-right:0px;'>
                <h3><?= __("history_title","Order History")?></h3>
                <table id="box-table-a">
                    <thead>
                        <tr>
                            <th style="text-align: left"><?= __("history_status","Status")?></th>
                            <th style="text-align: left"><?= __("history_date","Date")?></th>
                            <th style="text-align: left"><?= __("history_employee","Employee")?></th>
                            <th style="text-align: left"><?= __("history_message","Message")?></th>
                            <th style="text-align: left"> <?= __("history_locker","Locker")?> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if($orderStatus_history): foreach($orderStatus_history as $orderStatus_history_item): ?>
                        <?   $dateFormatStd =  get_business_meta($this->business_id, "fullDateDisplayFormat"); ?>
                        <tr>
                            <td style="text-align: right"><?= $orderStatus_history_item['name']; ?>
                            </td>
                            <td style="text-align: right">
                            <?php 
                            //$orderStatus_history_item['date']->format('Y-m-d H:i:s');
                            echo convert_from_gmt_aprax($orderStatus_history_item['date'], $dateFormatStd, $this->business_id); 
                            ?>
                            </td>
                            <td style="text-align: right"><?= $orderStatus_history_item['employee']?></td>
                            <td style="text-align: right"><?= $orderStatus_history_item['note']?></td>
                            <td style="text-align: right"> <a href="/admin/reports/locker_history/<?= $orderStatus_history_item['lockerID'] ?>" target="_blank"><?= $orderStatus_history_item['locker'] ?></a> </td>
                        </tr>
                        <? endforeach; endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    <!-- This is the partial order dialog display -->
    <div id="partial_order_dialog" style="display: none;" >
        <?= form_open("/admin/printer/print_order_receipt/$order_id", array("id" => "partial_order_form")) ?>
        <table>
            <tbody>
                <tr> <td><?= form_label(__("partial_dialog_number_short","Number Short:"), "partial") ?>  </td> <td> <?= form_input(array("name" => "partial", "id" => "partial", "style" => "float: left;")) ?> </td> </tr>
                <tr> <td> <?= form_label(__("partial_dialog_item","Item:"), "item") ?> </td> <td> <?= form_input(array("name" => "item", "id" => "item", "style" => "float: left;")) ?>  </td> </tr>
                <tr> <td> <?= form_label(__("partial_dialog_reason","Reason:"), "reason") ?> </td> <td> <?= form_textarea(array("name" => "reason", "id" => "reason", "rows" => 6, "cols" => "30")) ?> </td> </tr>
            </tbody>

        </table>

        <?= form_close() ?>
    </div>


    <? if ($pay_with_cash): ?>
    <div id="pay_with_cash_dialog" style="display: none;">
        <form method="post" action="<?= "/admin/orders/pay_with_cash/$order_id" ?>" id="pay_with_cash_form">
        <table>
            <tbody>
                <tr>
                    <td><?= __("cash_dialog_total","Total")?></td>
                    <td style="text-align: right;">
                        <b id="cash_total" data-value="<?= number_format((float)$net_total, $decimal_positions, '.', '') ?>"><?= format_money_symbol($this->business_id, '%.2n', $net_total) ?></b>
                    </td>
                </tr>
                <tr>
                    <td><label for="cash_amount"><?= __("cash_dialog_amount","Amount:")?></label></td>
                    <td><input type="text" name="amount" id="cash_amount" style="text-align: right;" value="<?= number_format((float)$net_total, $decimal_positions, '.', '') ?>" autocomplete="off"></td>
                </tr>
                <tr>
                    <td><label for="cash_change"><?= __("cash_dialog_change","Change:")?></label></td>
                    <td><input type="text" name="change" id="cash_change" style="text-align: right;" value="0.00"></td>
                </tr>
            </tbody>

        </table>
        </form>
    </div>
    <script type="text/javascript">
    $(function() {
        $('#cash_amount').keyup(function(evt) {
            var value = $('#cash_amount').val().replace(',', '.').replace(/[^0-9\.]/g, '');
            if ($('#cash_amount').val() != value) {
                $('#cash_amount').val(value);
            }

            var total = parseFloat($('#cash_total').attr('data-value'));
            var amount = parseFloat(value);
            if (isNaN(amount)) {
                amount = 0;
            }

            var change = amount - total;
            if (change < 0) {
                change = 0;
            }

            $('#cash_change').val(change.toFixed(2));
        });
    });
    </script>

    <div id="pay_with_check_dialog" style="display: none;">
        <form method="post" action="<?= "/admin/orders/pay_with_check/$order_id" ?>" id="pay_with_check_form">
        <input type="hidden" name="amount" value="<?= $net_total ?>">
        <table>
            <tbody>
                <tr>
                    <td><?= __("check_dialog_total","Total")?></td>
                    <td style="text-align: right;">
                        <b id="check_total" data-value="<?= $net_total ?>"><?= format_money_symbol($this->business_id, '%.2n', $net_total) ?></b>
                    </td>
                </tr>
                <tr>
                    <td><label for="check_check">Check Number:</label></td>
                    <td><input type="text" name="check" id="check_check" value=""></td>
                </tr>
            </tbody>
        </table>
        </form>
    </div>
    <? endif; ?>

    <div id="order_notes_warning" style="display:none;">
        <h1> <?= __("notes_stop_this_customer_has_notes","STOP! This customer has notes")?> </h1>
        <p> <?= __("notes_check_all_boxes","Check all boxes to verify that you have read all customer notes.")?> </p>
        <table cellspacing="4" cellpadding="4" style="width: 100%; line-height: 200%; text-align: left;">
            <tbody>
                <tr>
                    <td style="vertical-align: top; width:120px;"><?= __("order_notes","Order Notes")?></td>
                    <td>
                        <div style="font-weight: bold;" id="order_notes_warning_notes"><?= nl2br($order->notes) ?></div>
                    </td>
                    <td style="vertical-align: top; width:250px;">
                        <label>
                            <input type="checkbox" class="order_notes_warning_checkbox" />
                           <span> <?= __("notes_checkbox","I have read all the notes")?></span>
                        </label>
                    </td>
                </tr>
                <? if (!empty($customer) && trim($customer->internalNotes)): ?>
                <tr>
                    <td style="vertical-align: top; width:120px;">Internal Notes</td>
                    <td>
                        <div style="font-weight: bold;" id="order_notes_warning_internal"><?= nl2br($customer->internalNotes) ?></div>
                    </td>
                    <td style="vertical-align: top; width:200px;">
                        <input style="font-size: 14px;" id="copy_internal_notes_to_assembly" type="button" class="button orange" value="Move to Assembly Notes">
                    </td>
                </tr>
                <? endif; ?>
                <? if (!empty($ada_deliver_to_lower_locker)): ?>
                <tr>
                    <td style="vertical-align: top; width:120px;">Alert Notes</td>
                    <td>
                        <div style="font-weight: bold;"><?= nl2br($order->alertNotes) ?></div>
                    </td>
                </tr>
                <? endif; ?>
            </tbody>
        </table>
    </div>

    <!-- This is the custom_discount_dialog dialog display -->
    <div id="custom_discount_dialog" style="display: none;" >
         <table>
            <tbody>
                <tr> 
                    <td><?= form_label(__("custom_discount_amount","Amount:"), "custom_discount_amount") ?></td> 
                    <td><?= form_input(array("name" => "custom_discount_amount", "id" => "custom_discount_amount", "style" => "float: left;")) ?></td>
                </tr>
                <tr>
                    <td><?= form_label(__("custom_discount_description","Description:"), "custom_discount_description") ?></td> 
                    <td><?= form_input(array("name" => "custom_discount_description", "id" => "custom_discount_description", "style" => "float: left;")) ?></td>
                </tr>
                <tr>
                    <td><?= form_label(__("custom_discount_type","Type:"), "custom_discount_type") ?></td>
                    <td>
                        <select name="custom_discount_type" id="custom_discount_type">
                            <option value="_percent">Percent</option>
                            <option value="_dollar">Dollar</option>
                            <option value="_minimum">Minimum</option>
                            <option value="_homedelivery">Home Delivery</option>
                            <option value="0_free">Free</option>
                        </select>
                    </td> 
                </tr>
            </tbody>
        </table>
    </div>
</div>
