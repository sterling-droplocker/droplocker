<?php

enter_translation_domain("admin/orders/edit_item");

?>
<h2>
    <?= __("edit_item","Edit Item") ?>
    <? if (!empty($order_id)): ?>
     | <?= __("order", "Order:") ?> <a href='/admin/orders/details/<?= $order_id ?>'><?= $order_id ?></a>
        <? if ($closed): ?>
            <span style='color:#cc0000;'><?= __("order_is_captured", "(Order is captured)") ?></span>
        <? endif; ?>
    <? endif; ?>
</h2>

<?= $this->session->flashdata('message')?>

<div style='padding:5px;background-color:#eaeaea;'>
		
		
    <? if(empty($customer_items)): ?>
        <div style="color: red; font-weight: bold;"><?= __("no_customer","No Customer")?></div>
    <? else: ?>
        <? foreach($customer_items as $customer_item):?>
            <div style='float:left;width:43%'>
                <?= __("customer", "Customer:") ?>
                <a href='/admin/customers/detail/<?= $customer_item->relationships['Customer'][0]->customerID?>'><?= getPersonName($customer_item->relationships['Customer'][0]) ?></a></div>
        <? endforeach; ?>
    <? endif;?>
    <br style='clear:left' />
</div>

<br />
<div style="display: table;">
    <div style='min-width:600px; max-width: 600px; display: table-cell; vertical-align: top;'>
        <?= $edit_item_subview; ?>
    </div>
    <div style='min-width:400px; max-width: 400px; float: left; text-align: center; display: table-cell;'>
        <a href="/admin/items/add_issue/<?=$item->itemID ?>/<?= $order_id ?>" class="button blue"><?= __("add_edit_issue","Add/Edit Issues")?></a>
        <div style='margin:5px 10px;'> 
            <? if (empty($pictures)): ?>
                <div style='padding: 40px 20px; font-size: 37pt; color: white; background-color: red'><?= __("no_picture","NO PICTURE")?></div>
            <? else: ?>
                <? foreach($pictures as $picture): ?>
                    <a target="_blank" href='https://s3.amazonaws.com/LaundryLocker/<?= $picture->file?>'><img style='width:100%' src="https://s3.amazonaws.com/LaundryLocker/<?= $picture->file?>" /></a><br><br>
                <? endforeach; ?>
            <? endif; ?>
        </div>	
        <a href="/admin/items/add_issue/<?=$item->itemID ?>/<?= $order_id ?>" class="button blue"><?= __("add_edit_issue","Add/Edit Issues")?></a>
    </div>
</div>
<br style='clear:left' />

<h2><?= __("item_history","Item History")?></h2>
<table class="box-table-a">
    <thead>
        <tr>
            <th><?= __("date","Date")?></th>
            <th><?= __("status","Status")?></th>
            <th><?= __("user","User")?></th>
        </tr>
    </thead>
    <tbody>
        <? if($history): ?>
            <? foreach ($history as $history): ?>
                <tr>
                    <td><?= convert_from_gmt_aprax($history['date'], STANDARD_DATE_FORMAT)?></td>
                    <td>
                        <? if (!empty($history['order_id'])): ?>
                            <?= __("added_to_order_link","Added to order %link%", array(
                                    "link" => "<a href='/admin/orders/details/{$history['order_id']}' target='_blank'>{$history['order_id']}</a>",
                                )) ?>
                        <? else: ?>
                            <?= $history['description']; ?>
                        <? endif; ?>
                    </td>
                    <td>
                        <? if (empty($history['order_id'])): ?>
                        <?= getPersonName($history) ?>
                        <? endif; ?>
                    </td>
                </tr>
            <? endforeach; ?>
        <? else : ?>
            <tr>
                <td colspan="3"><?= __("no_history","No History")?></td>
            </tr>
        <? endif; ?>
    </tbody>
</table>
