<?

enter_translation_domain("admin/orders/create_item_error");

?>
<?= $this->session->flashdata('message')?>

<h2><?= __("create_item_error","Create Item Error")?></h2>

<?= $message ?>

<script>
	$('#no_button').live('click',function(){
		history.back();
	})
</script>

<form action='' method='post' style='padding:10px'>
	<input type='submit' class='button orange' name='submit' value='<?= __("yes","Yes")?>'> 
	<input type='hidden' name='item_id' value='<?= $item_id?>' />
	<input type='hidden' name='customer_id' value='<?= $order_customer[0]?>' />
	<input type='button' class='button orange' id='no_button' value='<?= __("no","No")?>'>
</form>
