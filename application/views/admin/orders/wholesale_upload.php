<?php enter_translation_domain("admin/orders/wholesale_upload"); ?>
<h2><?php echo __("Upload Orders", "Upload Orders"); ?></h2>
<br>
<table id="box-table-a" style="width: 100px;">
<form action="/admin/orders/wholesale_upload" method="post" enctype="multipart/form-data" >
<tr>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
</tr>
<tr>
    <td><?php echo __("Customer", "Customer:"); ?></td>
    <td nowrap>
        <select name="customer">
        <? foreach($importSettings as $importSetting): ?>
	        <option value="<?= $importSetting->customerID ?>" <?= $customer == $importSetting->customerID ? ' SELECTED' : '' ?>>
                <?php 
                if (!empty($importSetting->firstName)  && !empty($importSetting->lastName)): 
                    echo getPersonName($importSetting);
                else:
                    echo $importSetting->email;
                endif;
                ?>               
            </option>
        <? endforeach; ?>
        </select>
        &nbsp;&nbsp;&nbsp;<input type="submit" name="getSettings" value="<?php echo __("Get Settings", "Get Settings"); ?>"></td>
</tr>
<? if(isset($customer) and empty($lockerInfo->locker_id)): ?>
    <?php echo __("Error.  No locker setup", "Error.  No locker setup. Plese go to <a href=\"/admin/orders/wholesale_mapping\">Import Settings</a> and setup a locker."); ?>
<? else: ?>
    <? if(isset($customer)): ?>
        <tr><td><?php echo __("Location", "Location:"); ?></td><td><?= $lockerInfo->address ?></td></tr>
        <tr><td><?php echo __("Locker", "Locker:"); ?></td><td><?= $lockerInfo->lockerName ?></td></tr>
        <br>
        </tr>
        <? if($lockerInfo->importType == 'DropLocker'): ?>
            <form action="admin/orders/wholesale_upload" method="post">
            <td><?php echo __("Database Transfer", "Database Transfer"); ?></td>
            <td align="center"><input type="submit" value="<?php echo __("Run Import", "Run Import"); ?>" class="button orange" onClick="this.disabled=true;this.form.submit();"></td>
            <input type="hidden" name="importType" value="database">
            </form>
        <? else: ?>
            <form enctype="multipart/form-data" action="" method="POST">
            <input type="hidden" name="importType" value="file">
            <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
            <tr><td><?php echo __("Choose a file to upload:", "Choose a file to upload:"); ?> </td><td align="right"><input type="file" name="uploadedfile" accept="text/plain"></td></tr>
            <tr>
                <td></td>
                <td align="right">
                    <input type="submit" name="fileImport" value="<?php echo __("Upload File", "Upload File"); ?>" class="button orange" onClick="this.disabled=true;this.form.submit();">
                </td>
            </tr>
        <? endif; ?>
    <? endif; ?>
<? endif; ?>
</table>
</form>
<br>
<br>
<div style="clear:both">
<pre>
    <?=$response; ?>    
    <?if($error) echo $error; ?>
    <?if($success) echo $success; ?>
</pre>
</div>

