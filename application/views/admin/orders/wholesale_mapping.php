<?php enter_translation_domain("admin/orders/wholesale_mapping"); ?>
<style type="text/css">
    .delete_wholesale_mapping {
        cursor: hand;
        cursor: pointer;
    }
</style>
<script type="text/javascript">
$(document).ready( function() {
    //The follwoing event handler is for the delete button in each row.
    $(".delete_wholesale_mapping").submit(function() {
        var result = confirm("<?php echo __("Are you sure", "Are you sure?"); ?>");
        return result;
    })
});    
</script>
<h2><?php echo __("Import Setup", "Import Setup"); ?></h2>
<br>
<br>
<form action="" method="post">
    <?php echo __("Customer", "Customer:"); ?> 
    <select name="customer_id">
    <option value=''><?php echo __("Customers Of Type Wholesale", "**** Customers Of Type Wholesale ****"); ?></option>
    <?foreach($customers as $c): ?>
            <option value="<?= $c->customerID ?>"
            <? if(isset($custId)){
            	if($custId == $c->customerID) { echo ' SELECTED'; }
            } ?>
            >
            <?php 
            if (!empty($c->firstName)  && !empty($c->lastName)): 
                echo getPersonName($c);
            else:
                echo $c->email;
            endif;
            ?>
            </option>
    <?endforeach; ?>
    </select>
    <input type="submit" name="submit" value="<?php echo __("Select", "Select"); ?>">
</form>

<? if(isset($custId)): ?>
<br>
<form action="/admin/orders/import_settings_update" method="post">
    <?php echo __("Import source", "Import source:"); ?> <select name="importType">
	<option value="File" <?= (!empty($settings->importType) && $settings->importType == 'File') ? ' SELECTED' : ''; ?>>File</option>
	<option value="DropLocker" <?= (!empty($settings->importType) && $settings->importType == 'DropLocker') ? ' SELECTED' : ''; ?>><?php echo __("Drop Locker Database", "Drop Locker Database"); ?></option>
    </select>
    &nbsp;&nbsp;&nbsp;<?php echo __("Locker for orders", "Locker for orders:"); ?>
    <select name="locker_id">
        <? foreach($lockers as $locker): ?>
        	<option value="<?= $locker->lockerID ?>" <?= (!empty($settings->locker_id) &&  $settings->locker_id == $locker->lockerID) ? ' SELECTED' : ''; ?>><?= substr($locker->locationName, 0, 15) .' - '. $locker->lockerName ?></option>
        <? endforeach; ?>    
    </select>

     &nbsp;&nbsp;&nbsp;&nbsp;<?php echo __("Active", "Active:"); ?> <input type="checkbox" name="active" value="1" <?= (!empty($settings->active) && $settings->active == 1) ? ' checked' : ''; ?>>
     <? if( (!empty($settings->importType) && $settings->importType == 'DropLocker') ):
            if(is_superadmin()): ?>
                 &nbsp;&nbsp;&nbsp;&nbsp;This customer's Drop Locker Business ID (secuity check): <input type="text" name="customerBusiness_id" value="<?= $settings->customerBusiness_id ?>" size="5">
             <? else: ?>
                &nbsp;&nbsp;&nbsp;&nbsp; <?php echo __("SuperAdmin has to setup a business ID for this customer", "SuperAdmin has to setup a business ID for this customer"); ?>
            <? endif; ?>
    <? endif; ?>    
    <input type="hidden" name="custId" value="<?= $custId ?>">
    <input type="submit" name="update" value="<?php echo __("update", "update"); ?>">
</form>
<? endif; ?>

<? if(isset($mapping)): ?>
<br>
<br>
    <table id='hor-minimalist-a'>
        <thead>
            <tr>
                <th><?php echo __("Customer Product", "Customer Product"); ?></th>
                <th><?php echo __("Customer Process", "Customer Process"); ?></th>
                <th>&nbsp;</th>
                <th><?php echo __("Your Product", "Your Product"); ?></th>
                <th align="right"><?php echo __("Price", "Price"); ?></th>
                <th align="right"><?php echo __("Delete", "Delete"); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <form action="/admin/orders/wholesale_mapping_add" method="post">
                    <input type="hidden" name="customer_id" value="<?= $custId ?>">
                    <? if($settings->importType == 'DropLocker'): ?>
                        <td colspan=2>
                            <select name="customer_product_id">
                            <option value="">- Select Customer Product -</option>';
                            <?foreach($customerProducts as $cp): ?>
                                    <option value="<?= $cp->product_processID ?>"><?= $cp->productName.' - '.$cp->processName ?></option>
                            <?endforeach; ?>
                            </select>
                        </td>
                    <? else: ?>     
                        <td><input type="text" name="productName"></td>
                        <td><input type="text" name="processName"></td>
                    <? endif; ?>
                    <td>=</td>
                    <td><select name="product_process_id">
                    <option value="">- <?php echo __("Select Your Product", "Select Your Product"); ?> -</option>';
                    <?foreach($product as $p): ?>
                            <option value="<?= $p->product_processID ?>"><?= $p->productName.' - '.$p->processName.' ('.format_money_symbol($this->business_id,'%.2n', $p->price).')' ?></option>
                    <?endforeach; ?>
                    </select>
                    </td>
                    <td align="right"><input type="text" name="price"></td>
                    <td align="right"><input type="hidden" name="addMapping" value="1"><input type="submit" value="<?php echo __("add mapping", "add mapping"); ?>"></td>
                </form>
            </tr>

            <?foreach($mapping as $m): ?>
            <tr>
                <td><?= $m->productName ?></td>
                <td><?= $m->processName ?></td>
                <td>=</td>
                <td><?= $m->myProductName ?> - <?= $m->myProcessName ?></td>
                <td align="right">$<?= $m->price ?></td>
                <td align="right">
                    <form class="delete_wholesale_mapping" action="/admin/orders/wholesale_mapping_delete" method="post">
                        <input type="hidden" name="customer_id" value="<?= $custId ?>">
                        <input type="hidden" name="deleteMapping" value="<?= $m->importMappingID ?>">

                        <input type="image" src="/images/icons/cross.png" value="<?php echo __("Submit", "Submit"); ?>" alt="Submit">
                    </form>
                </td>
            </tr>
            <?endforeach; ?>
        </tbody>
    </table>
<?endif; ?>
