<?php
enter_translation_domain("admin/orders/booked");

$dateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat");
$timeFormat = get_business_meta($this->business_id, "hourLocaleFormat");

?>
<style type="text/css">
    label
    {
        margin-right: 5px;
    }
    form input
    {
        margin-right: 5px;
    }
    .editable:hover {
        border: 3px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3;
    }
</style>

<link rel="stylesheet" type="text/css" href="/css/tooltips/jquery.tooltip.css">
<script type="text/javascript" language="javascript" src="/js/tooltips/jquery.tooltip.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#orders_tabs").tabs({
        fx: {
            opacity: 'toggle'
        }
    });

    $.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
    {
    	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
    		return $('td:eq('+iColumn+')', tr).attr('data-value');
    	});
    }


    orders_dataTable = $("#orders").dataTable({
            "iDisplayLength" : 25,
            "bPaginate" : false,

            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables') );
            },
            "aaSorting" : [[0, 'desc']],
            "aoColumns": [
                null,
                null,
                null,
                null,
                { "sSortDataType": "attr" }
            ],

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                //THe following conditional restricts the available buttons if the server is in Bizzie mode and not a superadmin user.
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
    });

    $(".no_ord").click(function() {
        return confirm("<?= __("confirm_no_ord", "Are you sure you want to continue?") ?>");
    });

        //no server side tooltip
    orders_dataTable.$('._tooltip').tooltip({
        "delay": 0,
        "track": false,
        "fade": 250
    });

});
</script>

<h2><?= __("booked_orders", "Booked Orders") ?></h2>

<div id="orders_tabs">
    <ul>
        <li> <a href="#orders_tab"><?= __("Orders", "Orders") ?></a> </li>
    </ul>
    <div id="orders_tab">

    <? if($orderHomeDeliveries): ?>
        <table id="orders">
            <thead>
                <tr>
                    <th><?= __("order", "orderID") ?> </th>
                    <th><?= __("location", "Location") ?></th>
                    <th><?= __("customer", "Customer") ?></th>
                    <th align='center'><?= __("route", "Rte") ?></th>
                    <th><?= __("Delivery Date", "Delivery Date") ?></th>
                </tr>
            </thead>
            <tbody>
            <? foreach($orderHomeDeliveries as $order):?>
                <tr>
                            <td><a href="/admin/orders/details/<?= $order->order_id ?>" target="_blank"><?= $order->order_id ?></a></td>
                            <td><a href='/admin/orders/view_location_orders/<?= $order->locationID ?>' target="_blank"><?= ucwords($order->address)?></a></td>
                            <td class="_tooltip" title="Email: <?=$order->email?><br/>Phone: <?=$order->phone?><br/>Address: <?=$order->address1?><br/>Customer Notes: <?=$order->customerNotes?>" ><a href='/admin/customers/detail/<?= $order->customerID?>'><?= ucwords($order->firstName)?> <?= ucwords($order->lastName)?></a></td>
                            <td align='center'><?= $order->route_id?></td>
                            <?php 
                                $windowStart = convert_from_gmt_aprax($order->windowStart, $dateFormat);
                                $windowEnd = convert_from_gmt_aprax($order->windowEnd, $dateFormat);
                                $windowEnd = explode(" ", $windowEnd);
                                $windowEnd = $windowEnd[1];
                            ?>
                            <td nowrap="true" data-value="<?= $order->updated ?>">
                            <?= $windowStart . "-" . $windowEnd ?>
                            </td>
                        </tr>
                        <? endforeach; ?>
            </tbody>
        </table>
    <? else: ?>
            <div><?= __("no_orders", "No Booked Orders") ?></div>
    <? endif;?>

    </div>
</div>
