<?

enter_translation_domain("admin/orders/close_wholesale");

?>
<h2><?= __("close_wholesale","Close Whole Sale Orders")?></h2>
<p><?= __("close_all_clothespin_orders","This will close all clothespin orders")?></p>

<? if($this->session->flashdata('closedOrders')):
    $orders = unserialize($this->session->flashdata('closedOrders'));
?>
    <h3><?= __("closed", "Closed: ") ?> <?= count($orders) ?></h3>
<? if($orders): ?>
    <? foreach($orders as $order): ?>
            <?= $order ?><br>
    <? endforeach; ?>
<? endif; ?>
    
<? else: ?>
<form action='' method='post'>
    <input type='submit' value='<?= __("close_all_orders","Close All Orders")?>'name='submit' class='button orange'/>
</form>
<? endif; ?>
