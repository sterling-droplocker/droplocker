<?

enter_translation_domain("admin/orders/highlight_routes");

?>
<h2><?= __("highlighted_routes","Highlighted Routes")?></h2>
<br>
<br>
<form action="/admin/orders/highlight_routes" method="post">
<? foreach($allRoutes as $route): ?>
    <input type="checkbox" name="routes[]" value="<?= $route->route_id ?>" <? if(in_array($route->route_id, $selectedRoutes)) { echo 'CHECKED'; } ?>><?= $route->route_id?>&nbsp;&nbsp;
<? endforeach; ?>
<input type="submit" name="Update" value="<?= __("update","Update")?>">
</form>
