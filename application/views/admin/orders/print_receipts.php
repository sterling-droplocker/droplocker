<?php enter_translation_domain("admin/orders/print_receipts"); ?>

<h2><?=__("print_receipts", "Print Receipts"); ?></h2>

<script type="text/javascript">
    $(document).ready(function() {

        $("#date_begin").datepicker({
            buttonImage: "/images/icons/calendar.png",
            buttonImageOnly: true,
            dateFormat: "yy-mm-dd"
        });

        $("#date_end").datepicker({
            buttonImage: "/images/icons/calendar.png",
            buttonImageOnly: true,
            dateFormat: "yy-mm-dd"
        });

    })
</script>

<br>
<form action='/admin/printer/print_route_receipts' method='post'>
<table border="0" cellspacing="0" cellpadding="4">
    <tr>
        <td valign="top" width="120"><?=__("select_routes", "Select Route(s):"); ?></td>
        <td><select multiple='true' name='routes[]' style='width:100px;'>
            	<? foreach($routes as $r): ?>
            		<option value='<?= $r->route_id?>'><?= $r->route_id?></option>
            	<? endforeach ?>
            </select></td>
    </tr>
    <tr>
        <td valign="top"><?=__("date_created", "Date Created:"); ?></td>
        <td>
            <input type="text" id="date_begin" class="datefield"  name="date_begin" value="">
        </td>
    </tr>
    <td valign="top">End Date:</td>
    <td>
        <input type="text" id="date_end" class="datefield"  name="date_end" value="">
    </td>
    <tr>
    <tr>
        <td valign="top">&nbsp;</td>
        <td>
            <label><input type="checkbox" name="show_price" value="1"> <?=__("show_price", "Show Price"); ?></label>
        </td>
    </tr>
    <tr>
        <td valign="top">&nbsp;</td>
        <td>
            <label><input type="checkbox" name="master_sort" value="1"> <?=__("print_in_master_sort_order", "Print in master sort order"); ?></label>
        </td>
    </tr>
    <tr>
        <td valign="top">&nbsp;</td>
        <td>
            <label><input type="checkbox" name="ignore_loaded" value="1"> <?=__("do_not_print_orders_loaded_on_van", "Do not print orders loaded on van"); ?></label>
        </td>
    </tr>
    <tr>
        <td valign="top">&nbsp;</td>
        <td>
            <input name='submit' value='<?=__("run", "Run"); ?>' type='submit' class='button blue'/>
        </td>
    </tr>
</table>
</form>
