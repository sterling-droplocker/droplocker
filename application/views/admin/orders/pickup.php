<?php enter_translation_domain("admin/orders/pickup"); ?>
<h2><?php echo __("Simulate a Pickup", "Simulate a Pickup"); ?></h2>

<?= $this->session->flashdata('message')?>

<form action='' method='post'>
	<table class="detail_table">
        <tr>
            <th><?php echo __("Location", "Location"); ?></th>
            <td>
                
                    <? if($locations) : ?>
                    <select id='location_id' name='location_id'> 
                    <? foreach($locations as $key => $value ) : ?>
                        <option value='<?= $key?>'><?= $value?></option>
                    <? endforeach ;?>
                    </select>
                    <?else: ?>
                    <?php echo __("Please add a location", "Please add a location"); ?>    
                    <? endif ; ?>                
            </td>
        </tr>
       
         <tr id="lt_row">
            <th><?php echo __("Locker Number", "Locker Number"); ?></th>
            <td>
                <div id="locker_holder">
                <select id='lockers' name='locker_id'>
                </select>              
                </div>
            </td>
        </tr>
        <tr id="lt_row">
            <th><?php echo __("Bag number", "Bag #"); ?></th>
            <td>
                <input type="text" style='width:100px' name='bag_number' />          
            </td>
        </tr>
	</table>
	<input type="submit" class='button orange' name='submit' value='<?php echo __("Submit", "Submit"); ?>' />
</form>
