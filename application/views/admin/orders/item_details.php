<?php

enter_translation_domain("admin/orders/item_details");

?>

<script type="text/javascript">

$(document).ready( function() {

$("#toggle_link").click();

$("#add_item_submit_button").loading_indicator();

$("#add_item_form").validate({

        rules: {
            barcode: {
                required: true,
            }
        },
        errorClass: "invalid",
        "invalidHandler" : function(form, validator) {
            $("#add_item_submit_button").loading_indicator("destroy");

        },
        submitHandler: function(form) {

            $("body").on("keypress.mynamespace", function() {
                $.post("/admin/orders/entry_error", {orderID: <?= !empty($order) ? $order->orderID : 'null' ?>, barcode:$("#barcode").val()}, function(result) {});
                soundManager.play("buzzer");
                $("body").off("keypress.mynamespace");
            });
            form.submit();

            $.blockUI({message : "<h1> <?= __("loading","Loading...")?></h1>"});

        }
});
});
</script>

<style type="text/css">
    fieldset {
        border: 1px solid;
        margin: 10px 0px;
        padding: 10px;
    }
    #box-table-a td {
        padding: 2px;
    }
</style>


<h2><?= __("item_details","Item Details")?> - <?= $barcode->barcode ?></h2>
<div style="min-width: 10000px;">
    <div style="float: left; width: 400px;">
        <table class="detail_table">
            <tr>
                <th style="width:35%"><?= __("product","Product")?></th>
                <td style="width:55%" width='*'>
                    <?= $product_process->relationships['Product'][0]->name?>
                </td>
            </tr>
            <tr>
                <th><?= __("cleaning_method","Cleaning Method")?></th>
                <td>
                    <?= $product_process->relationships['Process'][0]->name ?>
                </td>
            </tr>
            <tr>
                <th><?= __("special","Special")?></th>
                <td>
                    <?php if (!empty($item_specifications)): ?>
                        <script type="text/javascript">
                            $("<div><?= __("this_item_has_special_prefs", "This item has special preferences") ?></div>").dialog({
                                modal: true,
                                title: "<?= __("alert", "ALERT!") ?>",
                                width: 700,
                                buttons: {
                                    "<?= __("btn_ok", "Ok") ?>" : function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        </script>
                    <?php endif?>
                    <?php foreach ($item_specifications as $specification): ?>
                        <?= $specification->description ?><br>
                    <?php endforeach; ?>
                </td>
            </tr>
            <? foreach($item_preferences as $name => $value): ?>
            <tr>
                <th><?= $name ?></th>
                <td>
                    <?= $value ?>
                    <? if (!empty($changes[$name])): ?>
                        <div style='color: red; padding: 10px;'><?= $changes[$name] ?></div>
                    <? endif; ?>
                </td>
            </tr>
            <? endforeach; ?>
            <tr>
                <th><?= __("upcharges","Upcharges")?></th>
                <td>
                    <?php
                        if (empty($item_upcharges))
                        {
                            echo __("none", "None");
                        }
                        else
                        {
                            foreach ($item_upcharges as $upcharge)
                            {
                                $str .= $upcharge->name.", ";
                            }
                            echo rtrim($str, ", ");
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <th><?= __("item","Item Notes")?></th>
                <td>
                    <?= $item->note?>
                </td>
            </tr>
        </table>

        <?php if(!$closed): ?>
            <form id="add_item_form" action="/admin/orders/add_item_to_order" method='post'>
                <input type='hidden' name='order_id' value='<?= $order->orderID?>' />
                <fieldset>
                    <label for="barcode"><?= __("barcode", "Barcode:") ?></label>
                    <?= form_input(array("name" => "barcode", "autocomplete" => "off", "id" => "barcode")) ?>
                    <?= form_submit(array("id" => "add_item_submit_button", "class" => "button orange", "value" => __("add", "Add"))) ?>
                </fieldset>
            </form>
            <script type="text/javascript">
                $('#add_btn').focus();
            </script>
        <?php endif; ?>
        <br />
        <a href='/admin/orders/edit_item?orderID=<?= $order->orderID ?>&itemID=<?= $item->itemID ?>' class='button blue'><?= __("edit_item", "Edit Item") ?></a>
        <br><br>
        <a href='/admin/orders/details/<?= $order->orderID ?>'><?= __("return_to_order","Return to Order")?></a>


        <h2><?= __("item_history","Item History")?></h2>

        <table id="box-table-a">
            <thead>
                <tr>
                    <th><?= __("date","Date")?></th>
                    <th><?= __("action","Action")?></th>
                    <th><?= __("employee","Employee")?></th>
                </tr>
            </thead>
            <tbody>

                <?php if(empty($itemHistory)): ?>
                        <td colspan="3"><?= __("no_history","No History")?></td>
                <?php else : ?>
                    <?php foreach ($itemHistory as $history): ?>
                    <tr>
                        <td><?= convert_from_gmt_aprax($history->date, "m/d/y g:ia") ?></td>
                        <td><?= $history->description ?></td>
                        <td>
                            <?php
                                try {
                                    $employee = new \App\Libraries\DroplockerObjects\Employee($history->creator, false);
                                    echo getPersonName($employee);
                                }
                                catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e)  {
                                    echo __("unknown_employee", "Unknown Employee");
                                }
                            ?>
                        </td>
                    </tr>
                    <? endforeach; ?>
                <?php endif; ?>

            </tbody>
        </table>
    </div>
    <div style="float: left; width: 500px; margin-top: 9px; margin-left: 10px;">
        <table id="box-table-a" style="width">
            <tbody>
                <? foreach ($orderItems as $i => $orderItem):

                    if (empty($orderItem->product_process_id)) {
                        $orderItem->product_process_name = __("no_product_process","No product process defined for order item");
                    } else {
                        $product_process = new \App\Libraries\DroplockerObjects\Product_Process($orderItem->product_process_id, true);
                        $orderItem->product_process_name = $product_process->relationships['Product'][0]->displayName;
                    }
                    
                    if (!empty($orderItem->item_id)) {
                        $CI = get_instance();
                        $CI->load->model("barcode_model");
                        $barcode = $CI->barcode_model->get_one(array(
                            "item_id" => $orderItem->item_id,
                        ));

                         $orderItem->barcode = $barcode->barcode;
                    }
                ?>
                <tr>
                    <td><?= count($orderItems) - $i ?></td>
                    <td><?= $orderItem->product_process_name ?></td>
                    <td>
                        <? if (empty($orderItem->item_id)): ?>
                            <?= __("na","N/A") ?>
                        <? else: ?>
                            <a href='/admin/items/view?itemID=<?= $orderItem->item_id ?>&orderID=<?= $orderItem->order_id ?>'><?= $orderItem->barcode ?></a>
                        <? endif; ?>
                    </td>
                    <td>
                        <? if (empty($orderItem->item_id)): ?>
                            &bnsp;
                        <? else: ?>
                            <?= convert_from_gmt_aprax($orderItem->updated, "m/d/y g:ia") ?>
                        <? endif; ?>
                    </td>
                </tr>
                <? endforeach; ?>
            </tbody>
        </table>

        <? if (!empty($picture_file)): ?>
            <a target='_blank' href='https://s3.amazonaws.com/LaundryLocker/<?= $picture_file ?>'>
                <img style='padding: 30px 0px;' src='/picture/resize?src=https://s3.amazonaws.com/LaundryLocker/<?= $picture_file ?>&h=350&w=400&ac=1' />
            </a>
        <? else: ?>
            <div style='margin: 30px 0; padding: 40px 20px; font-size: 37pt; color: white; background-color: red'> <?= __("no_picture", "NO PICTURE") ?></div>
        <? endif; ?>
    </div>

</div>

<script type="text/javascript">
    if (document.getElementsByName("barcode").length)
    {
        document.getElementsByName("barcode")[0].focus();
    }
</script>

