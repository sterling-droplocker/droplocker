<?php

enter_translation_domain("admin/orders/split");

?>
<h2><?= __("select", "Select Items to Split Into New Order") ?></h2>
<form action="split_order" method="post">
<br>
<table class="box-table-a">
    <tr>
        <th></th>
        <th><?= __("item", "Item") ?></th>
        <th><?= __("barcode", "Barcode") ?></th>
    </tr>
<? foreach($orderItems as $orderItem): ?>
    <tr>
        <td><input type="checkbox" name="split_items[]" value="<?= $orderItem->orderItemID ?>"></td>
        <td><?= $orderItem->displayName ?></td>
        <td><?= $orderItem->barcode ?></td>
    </tr>
<? endforeach; ?>
</table>
<br>
<input type="hidden" name="orderID" value="<?= $_GET['orderID'] ?>">
<input type="submit" name="split" value="<?= __("split", "Split") ?>" class="button orange">
</form>
