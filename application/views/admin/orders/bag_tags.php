<?php

enter_translation_domain("admin/orders/bag_tags");

$printer_key = get_business_meta($business_id, "printer_key");
?>
<script type="text/javascript">

<?php if (isset($redirect)): ?>
    
    if ($.cookie("from_print_bag_tag") == "true")
    {
        $.cookie("from_print_bag_tag", null, { path: "/"});
    }
    else
    {
        setTimeout(function() {
            window.location = "<?= $redirect ?>";
        }, 2000);
    }
    
    
<?php endif; ?>
$(document).ready(function() {
    document.getElementById('bagNumber').focus();
    $(":button").loading_indicator();
    $(":submit").loading_indicator();
    $("#print_temp_tag_form").validate( {
        rules : {
            number_to_print : {
                required: true,
                digits: true
            }
        },
        errorClass: "invalid",
        invalidHandler: function(form,validator)
        {
            $("#print_temp_tag_form").find(":submit").loading_indicator("destroy");
        }
    });
    
}); // End of $(document).ready()

</script>


<?= $this->session->flashdata('message');?>
<h2><?= __("print_bag_tags","Print Bag Tags")?></h2>
<br>


<div style="display:block; margin: 20px 0">
<h3 style="margin-bottom: 10px;">Temp Tags:</h3>

    <?= form_open("/admin/printer/print_receipts/tags/$printer_key/$business_id", array("method" => "GET", "id" => "print_temp_tag_form")) ?>
        <label for="number_to_print"><?= __("number_to_print", "Number of Temp Tags to Print:") ?></label>
        <?= form_input(array("name" => "number_to_print", "value" => 24, "style" => "width: 25px;", "maxlength" => 2 )) ?><br />
        <label for="include_date">Include Date on Each Tag:</label>
        <input type="checkbox" name="include_date" id="include_date" /> <br/> <br />

        <input class='button orange' type='submit' style="padding: 5px;" value= "<?= __("print_temp_tags","Print Temp Tags")?>" />
    <?= form_close() ?>
</div>

<div>
    <form action="" method='GET'>
            <label for="bagNumber"><?= __("bag_number","Bag Number:")?></label>
            <input type='text' name='bagNumber' id='bagNumber' />
            &nbsp;&nbsp;
            <input class='button orange' type='submit' value= "<?= __("search","Search")?>" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="print_tag_logo"><?= __("one_sided_logo","Print One Sided Logo")?></a>

    </form>
</div>


<hr />



<? if (isset($notice)): ?>
<?= $notice ?>
<? endif; ?>

<? if(!empty($allOrder)): ?>
<table class='hor-minimalist-a'>
    <tr>
        <th><?= __("order","Order")?></th>
        <th><?= __("customer","Customer")?></th>
        <th><?= __("bag","Bag")?></th>
        <th><?= __("customer_notes","Customer Notes")?></th>
    </tr>
    <? foreach($allOrder as $a): ?>
    <tr>
        <td><a href="/admin/orders/details/<?= $a->orderID ?>" target="_blank"><?= $a->orderID ?></a></td>
        <td><a href="/admin/customers/detail/<?= $a->customer_id ?>" target="_blank"><?= getPersonName($a) ?></a></td>
        <td><?= $a->bagNumber ?></td>
        <td>
            <? if ($a->customerNotes): ?>
            <?= __("label_customer", "CUSTOMER:") ?> <?= $a->customerNotes ?>
            <? endif; ?>
            <? if ($a->customerNotes): ?>
            <?= __("label_internal", "INTERNAL:") ?> <?= $a->internalNotes ?>
            <? endif; ?>
        </td>
    </tr>
    <? endforeach; ?>
</table>
<br><a href="/admin/admin/print_bag_tag/<?= $a->customerID ?>/<?= urlencode($a->bagNumber) ?>/<?= urlencode($a->phone) ?>"><?= __("print_anyway","Print Anyway")?></a>
<br><br>
<br><a href="/admin/admin/print_bag_tag/<?= $a->customerID ?>/<?= urlencode($a->bagNumber) ?>/<?= urlencode($a->phone) ?>/with-default-location"><?= __("print_anyway_with_default_location","Print Anyway (With default location)")?></a>
<? endif; ?>


