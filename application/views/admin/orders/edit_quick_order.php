<?

enter_translation_domain("admin/orders/add_quick_order");

?>
<h2><?= __("edit_quick_order","Edit Quick Order")?></h2>
<p><a href="/admin/orders/quick_order"><img src="/images/icons/arrow_left.png"><?= __("back_to_quick_orders","Back to Quick Orders")?></a></p>

<form action='' method='post'>
<table class='detail_table'>
    <tr>
        <th><?= __("name", "Name") ?></th>
        <td><input type='text' name='name' value="<?= $quickOrder->name?>" /></td>
    </tr>
    <tr>
        <th><?= __("bag_number", "Bag Number") ?></th>
        <td><input type='text' name='bagNumber' value="<?= $quickOrder->bagNumber?>" /></td>
    </tr>
</table>
<input class='button orange' type='submit' name='submit' value='<?= __("update","Update")?>' />
</form>


