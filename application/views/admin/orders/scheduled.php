<?php enter_translation_domain("admin/orders/scheduled"); ?>


<h2><?php echo __("Scheduled Orders", "Scheduled Orders"); ?></h2>

<table class='hor-minimalist-a striped'>
<thead>
<tr>
	<th><?php echo __("Customer", "Customer"); ?></th>
	<th><?php echo __("Location", "Location"); ?></th>
    <th><?php echo __("Sun", "Sun"); ?></th>
    <th><?php echo __("Mon", "Mon"); ?></th>
	<th><?php echo __("Tue", "Tue"); ?></th>
	<th><?php echo __("Wed", "Wed"); ?></th>
	<th><?php echo __("Thur", "Thur"); ?></th>
	<th><?php echo __("Fri", "Fri"); ?></th>
	<th><?php echo __("Sat", "Sat"); ?></th>
	<th></th>
</tr>
</thead>
<tbody>
<form action="/admin/orders/scheduled_create" method="post">
<tr>
	<td valign="middle">
		<input type="text" name="customer" id='customer' value="">
		<div id="customer_results"></div>
		<input type='hidden' id='customer_id' name='customer_id' />
	</td>
	<td>
        <?= form_dropdown('location', $locations_dropdown, null, 'id="location"'); ?>
        <?= form_dropdown('locker_id', array(), null, 'id="locker"') ?>
    </td>
    <td align="center"><input type="checkbox" name="dayOfWeek[]" value="0"></td>
    <td align="center"><input type="checkbox" name="dayOfWeek[]" value="1"></td>
	<td align="center"><input type="checkbox" name="dayOfWeek[]" value="2"></td>
	<td align="center"><input type="checkbox" name="dayOfWeek[]" value="3"></td>
	<td align="center"><input type="checkbox" name="dayOfWeek[]" value="4"></td>
	<td align="center"><input type="checkbox" name="dayOfWeek[]" value="5"></td>
	<td align="center"><input type="checkbox" name="dayOfWeek[]" value="6"></td>
	<td align="center"><input type="submit" name="create" value="<?php echo __("Create", "Create"); ?>" class="button orange"></td>
</tr>
</form>

<? foreach($claims as $c): ?>
<tr>
<form action="/admin/orders/scheduled_update" method="post">
        <td><?= getPersonName($c) ?></td>
	<td><? if (!empty($c->lockerID)): ?>
        <?= $c->address ?> (<?= $c->lockerName ?>)
        <? else: ?>
        <span style="color:red;"><?php echo __("MISSING LOCKER", "MISSING LOCKER"); ?></span>
        <? endif; ?>
    </td>
	<? for($i=0; $i <= 6; $i++): ?>
		<td align="center"><input type="checkbox" name="dayOfWeek[]" value="<?= $i ?>" <? if(stristr($c->dayOfWeek, strval($i))) echo ' CHECKED' ?>></td>
	<? endfor; ?>
	<td>
        <input type="hidden" name="updRec" value="<?= $c->scheduledOrdersID ?>">
        <input type="hidden" name="customer_id" value="<?= $c->customerID ?>">
        <input type="hidden" name="locker_id" value="<?= $c->lockerID ?>">
        <input type="submit" name="update" value="<?php echo __("Update", "Update"); ?>" class="button blue">
    </td>
</form>
</tr>
<? endforeach; ?>
</tbody>
</table>

<script type="text/javascript">
function getLockers() {
    $.getJSON("/admin/lockers/get_all_for_location", {
        locationID : $("#location").val()
    }, function(result) {
        $("#locker").empty();
        if (result['status'] == "success") {
            if (result['data'].length < 1) {
                var $option = $("<option>").text("--- <?php echo __("No Lockers for this Location", "No Lockers for this Location"); ?> ---").val(0);
                $option.appendTo($("#locker"));
            }
            $.each(result['data'], function(lockerID, lockerName) {
                var $option = $("<option>").text(lockerName).val(lockerID);
                $option.appendTo($("#locker"));
            });
        }
        else {
            alert(result['error']);
        }
    }).error(function() {
        alert("<?php echo __("A server side error occurred attempting to retrieve the lockers for the return to office location", "A server side error occurred attempting to retrieve the lockers for the return to office location"); ?>");
    });
}

$(document).ready(function(){
   $('#location').change(function(){
       getLockers();
   });
   getLockers();
});

</script>
