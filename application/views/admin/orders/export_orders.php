<?

enter_translation_domain("admin/orders/export_orders");

?>
<h2><?= __("export_orders","Export Orders")?></h2>

<br><br>
<form method="get">

<table class="detail_table">
	<tbody>
		<tr>
			<th><?= __("export_to","Export to:")?></th>
			<td>
                <?= form_dropdown('export', $softwares, $selected); ?>
                <a href="/admin/orders/export_settings"><?= __("edit_settings","Edit Settings")?></a>
			</td>
		</tr>
		<tr>
			<th><?= __("orders_inventoried_between","Orders Inventoried Between:")?></th>
			<td>
				<?= __("from","From")?> <input name="from" value="<?= $start_date ?>" class="datefield">
				<?= __("to","To")?> <input name="to" value="<?= $end_date ?>" class="datefield">
			</td>
		</tr>
		<tr>
			<th><?= __("routes","Routes:")?></th>
			<td>
                <?= form_dropdown('routes[]', $routes, null, "multiple style='width:100px'"); ?>
			</td>
		</tr>
	</tbody>
</table>
<?php /* Remove this line after DROP-4397 being closed */ ?>
<input type="hidden" name="business_id" value="<?php echo $this->business_id;?>" />

<input type="submit" class="button orange" value="<?= __("export_orders","Export Orders")?>">

</form>

<br><br>
<? if (!empty($error_messages)): ?>
<h2><?= __("errors","Errors")?><?= __("error","Errors")?></h2>

<ul class="error">
<? foreach ($error_messages as $message): ?>
    <li><?= $message ?></li>
<? endforeach; ?>
</ul>
<? endif; ?>

<? if (!empty($imported_messages)): ?>
<br><br>
<h2><?= __("imported","Imported")?></h2>

<ul class="success">
<? foreach ($imported_messages as $message): ?>
    <li><?= $message ?></li>
<? endforeach; ?>
</ul>
<? endif; ?>


<script type="text/javascript">

$(".datefield").datepicker({
	showOn: "button",
	buttonImage: "/images/icons/calendar.png",
	buttonImageOnly: true,
	dateFormat: "yy-mm-d"
});

$(".button.orange").on("click", function(){
	$(this).loading_indicator();
});

</script>
