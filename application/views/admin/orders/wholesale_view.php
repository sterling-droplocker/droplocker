<?php enter_translation_domain("admin/orders/wholesale_view"); ?>
<?php 
	$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
?>
<h2><?php echo __("View Orders", "View Orders"); ?></h2>
<br>
<br>
<form action="" method="post">
<input type="hidden" name="customer_id" value="18789"> <!-- Note, 18789 is the customer ID expected for clothespin. -->
<?php echo __("Local Time", "Local Time:"); ?>
<select name="showOrders">
    
<!-- The following loop displays the dates from today to 90 days ago, in descending format. -->
<? for($i = 0; $i < 90; $i++): ?>
	<option value="<?= date("Y-m-d", strtotime("-$i days"))?>"
	<? if(!empty($_POST['showOrders']) && $_POST['showOrders'] == date("Y-m-d", strtotime("-$i days"))) { echo ' SELECTED'; } ?>
	><?=convert_from_gmt_aprax(date('Y-m-d', strtotime("-$i days")), $dateFormat);?>
	</option>
<? endfor; ?>
        
</select> &nbsp;&nbsp;
<input type="submit" name="View" value="<?php echo __("View Orders", "View Orders"); ?>" class="button orange">
</form>

<? if(!empty($orderHistory)): ?>
	<br>
	<table id="box-table-a">
	<thead>
		<tr>
		<th><?php echo __("Order Date Created", "Order Date Created"); ?></th>
		<th><?php echo __("LL Order", "LL Order"); ?></th>
		<th><?php echo __("Cust Order", "Cust Order"); ?></th>
		<th><?php echo __("Status", "Status"); ?></th>
		<th><?php echo __("Amt", "Amt"); ?></th>
		<th></th>
		</tr>
	</thead>
	<tbody>
	<?foreach($orderHistory as $o): ?>
	<tr <?if($lastOrder == $o->ticketNum): ?> bgcolor="#FF0000" <?endif; ?>>
	<td valign="top"><?= convert_from_gmt_aprax($o->dateCreated, "m/d/y") ?></td>
	<? if($this->zacl->check_acl(orders_view) == 1): ?>
		<td valign="top"><a href="/admin/orders/details/<?= $o->orderID ?>" target="_blank"><?= $o->orderID ?></a></td>
	<? else: ?>
		<td valign="top"><a href="/account/orders/order_details/<?= $o->orderID ?>" target="_blank"><?= $o->orderID ?></a></td>
	<? endif; ?>
	<td valign="top"><?= $o->ticketNum ?></td>
	<td valign="top"><?= $o->name ?></td>
	<td align="right" valign="top"><?= format_money_symbol($this->business_id,'%.2n', $closedGross[$o->orderID]['total']) ?></td>
	
	<? if($this->zacl->check_acl(orders_view) == 1): if($automat): ?>
		<td valign="top"><table>
		<? foreach($automat[$o->orderID] as $a): ?>
			<tr valign="top">
			<td><?= convert_from_gmt_aprax($a->fileDate, "m/d/y  g:i:sa") ?></td>
			<td><?= $a->garcode ?></td>
			<td><?= $a->gardesc ?></td>
			<td><?= $a->slot ?></td>
			<td><?= $a->outPos ?></td>
			<td nowrap>
			<? if ($a->outPos != ''): ?>
				<a href="/admin/reports/assembly_log/<?= $a->automatID ?>"><?php echo __("Unload Arm Log", "Unload Arm Log"); ?></a> 
			<? endif; ?>
			</td>
			</tr>
		<? endforeach; ?>
		</table></td>
	<? endif; endif; ?>
	</tr>
	<? $lastOrder = $o->ticketNum ?>
	<? endforeach; ?>
	</tbody>
	<tfoot>
	<tr><td colspan="4"><strong><?php echo __("Total", "Total:"); ?></strong></td><td align="right"><strong><?= format_money_symbol($this->business_id,'%.2n', $totalAmt) ?></strong></td><td></td></tr>
	</tfoot>
	</table>
<? endif; ?>
