<?php

/**
 * It is not known all the required content variables for this view.
 * 
 * Note, due to faulty design, all javascript related functionality must be put in the $(document).ready() event hanlder in createItem.js, because that is where the initialization of the edit item interface occurs.
 */

enter_translation_domain("admin/orders/create_item");

?>
<script type="text/javascript">
$(document).ready(function() {
   $("#toggle_link").click();
});
</script>
<h2><?= __("add_to_order", "Add to Order") ?>
    <? if ($order_id): ?>
    |  <?= __("order", "Order:") ?> <a href='/admin/orders/details/<?= $order_id ?>'><?= $order_id ?></a>
    <? endif; ?>
</h2>

<div style='padding:5px;background-color:#eaeaea;'>
        
        <div style='float:left;width:43%'>
            <? if (isset($customer)): ?>
                <?= __("customer","Customer: ")?><a href='/admin/customers/detail/<?= $customer_id?>'><?= getPersonName($customer) ?></a>
            <? endif ?>
        </div>
    <br style='clear:left' />
</div>
<br />
<div>
    <div class='all_div_padding orange_box' style='padding:5px;'>
        <div style='float:left;padding-top:10px; font-weight: bold; '><?= __("quick_add","Quick Add")?></div>
        <? foreach($buttons as $index => $button): ?>
            <div style='float:left;'>
                <button name="quick_button" tabindex="<?= $index+1 ?>" class="quick_button orange" data-process_id="<?=$button->process_id?>"  data-product_id="<?=$button->product_id?>" id="<?= $button->slug ?>"><?= __("btn_".$button->name,$button->name)?></button>
            </div>
        <? endforeach; ?>
        <br style='clear:left' />
    </div>
</div>
<br />

<?= $edit_item_subview; ?>
