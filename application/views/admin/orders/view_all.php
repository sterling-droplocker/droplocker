<?php

enter_translation_domain("admin/orders/view_all");

$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");

?>
<script type="text/javascript">
$(document).ready(function(){
$('#location_button').click(function(){
    top.location.href='/admin/orders/view_location_orders/'+$('#location').val();
});

$('.note_button').live('click', function(){
    var id = $(this).attr('id');
    id = id.replace("button-","");
    var note = $('#note-'+id).val();
    $('#work-'+id).css('display', 'inline').html(" <img src='/images/progress.gif' />");
    $.ajax({
        url: "/admin/orders/update_notes_ajax",
        data: {"note":note,"order_id":id},
        type:"post",
        success: function(data){
                if(data.status=="success"){
                        //show a message that it was updated
                        $('#work-'+id).html("<?= __("msg_saved", "Saved!") ?>").delay(1000).fadeOut('slow');
                }
                else{
                        $('#work-'+id).html("<?=__("msg_not_saved", "NOT SAVED!") ?>").delay(3000).fadeOut('slow');
                }

        },
        dataType:'json'
    });
});

$('#check_all_btn').click(function(){
    $(".process_chk").attr('checked', true);
});

$('#bag_number').keypress(function(e) {
    if (e.keyCode == '13') {
        e.preventDefault();
        top.location.href = "/admin/orders/bag_barcode_details/0/"+$("#bag_number").val();
    }
});

$.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
{
	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
		return $('td:eq('+iColumn+')', tr).attr('data-value');
	});
};


$orders_dataTable = $("#orders").dataTable({
    "bPaginate" : false,
    "bJQueryUI": true,
    "fnStateLoad": function (oSettings) {
        return JSON.parse( localStorage.getItem('DataTables') );
    },
    "aoColumns": [
        { "sSortDataType": "attr", "sType" : "numeric" },
        null,
        null,
        null,
        null,
        { "sSortDataType": "attr" },
        { "sSortDataType": "attr" },
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ],
    "aoColumnDefs": [{
        "aTargets": [0]
    }],
    "sDom": '<"H"lTfr>t<"F"ip>',
    "oTableTools": {
        <?php if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
    }

});

new FixedHeader($orders_dataTable);

$('#capture_btn').loading_indicator().attr("class", "button orange");

}); //End of document.ready()

</script>

<?= $this->session->flashdata('message') ?>

<h2><?= __("view_all_orders", "View All Open Orders") ?></h2>
<div style='padding:5px;'>

<?= form_open("/admin/orders/bag_barcode_details", array("method" => "get")) ?>
    <label for="bag_number"><?= __("label_bag_number", "Bag #:") ?></label>
    <?= form_input("bag_number", null, 'id="bag_number"') ?>
    <input type="submit" value="<?= __("process", "Process") ?>" class="button orange">
<?= form_close() ?>

<form action='' style='float:right;'>
    <label for="location"><?= __("label_location", "Location: ") ?></label>
    <?= form_dropdown('location', $locations, null, 'id="location"') ?>
    <input type='button' id='location_button' name='submit' value= <?= __("update", "Update") ?> />
</form>

<div style='clear:both'></div>
</div>

<form action='/admin/orders/capture' method='post'>
<table id="orders">
	<thead>
            <tr>

                <th><?= __("order", "Order") ?></th>
                <th><?= __("order_type", "Order Type") ?></th>
                <th><?= __("route", "R") ?></th>
                <th><?= __("location", "Location") ?></th>
                <th><?= __("original_locker", "Original Locker") ?></th>
                <th><?= __("picked_up", "Picked<br>Up") ?></th>
                <th><?= __("due_date", "Due date") ?></th>
                <th><?= __("bag", "Bag") ?></th>
                <th><?= __("stat", "stat") ?></th>
                <th><?= __("capture", "Capture") ?> </th>
                <th><?= __("pay_stat", "Pay Stat") ?> </th>
                <th><?= __("customer", "Customer") ?></th>
                <th><?= __("order_count", "Order count") ?></th>
                <th><?= __("claim", "Claim") ?></th>
                <th><?= __("receipt", "Receipt") ?></th>
                <th><?= __("notes", "Notes") ?></th>
            </tr>
	</thead>
	<tbody>
            <?  if(!empty($orders)): foreach($orders as $order): ?>
            <tr>
                <!-- This is the order column -->
                <td data-value="<?= $order['orderID'] ?>">
                    <a href="/admin/orders/details/<?= $order['orderID']?>"> <?= $order['orderID'] ?></a>
                </td>
                <!-- This is the Order Type Column -->
                <td><?= $order['orderType'] ?></td>
                <td><?= $order['route_id'] ?></td>
                <!-- This is the Location column -->
                <td><?= $order['address']?></td>
                <!-- This is the Original Locker Column -->
                <td><?= $order['original_locker'] ?></td>
                <td nowrap='true' data-value="<?= $order['dateCreated'] ?>">
                    <?= convert_from_gmt_aprax($order['dateCreated'], $dateFormat, $this->business_id) ?>
                </td>
                <td nowrap='true' data-value="<?= $order['dueDate'] ?>">
                    <?= $order['dueDate'] ? apply_date_format($order['dueDate'], $dateFormat, $this->business_id) : '-' ?>
                </td>
                <td><?= $order['bagNumber']?></td>
                <td>
                    <?= $order['order_status']?>
                    <? if ($order['loaded'] == 1): ?>
                    <br><?= __("loaded", "(loaded)") ?>
                    <? endif; ?>
                </td>
                <td>
                    <? if($order['invoice'] == 0 && (($order['orderPaymentStatusOption_id'] == 2 || ($order['autoPay'] == 1 && $order['orderPaymentStatusOption_id'] == 1)) && $order['orderStatusOption_id'] != 1 && $order['orderStatusOption_id'] != 2)): ?>
                        <input type='checkbox' class='process_chk' name='process[]' value='<?= $order['orderID']?>' />
                    <? endif; ?>
                </td>
                
                <td>
                    <? if($order['invoice'] == 1): ?>
                        <?= __("invoice", "Invoice") ?>
                    <? else: ?>
                        <?= $order['paymentStatusName']?>
                    <? endif; ?>
                </td>
                <td>
                    <a target='_blank' href='/admin/customers/detail/<?= $order['customer_id']?>'><?= getPersonName($order) ?></a>
                </td>
                <td><?= $order['order_count'] ?></td>
                <td><?= $order['claimID']?></td>
                <td><?= $order['receipt']?></td>
                <td>
                    <textarea name='llnotes' id='note-<?= $order['orderID']?>'><?= $order['llnotes']?></textarea><br>
                    <input id='button-<?= $order['orderID']?>' class='note_button' style='font-size:10px' type='button' value="<?= __("update", "update") ?>"/>
                    <span id='work-<?= $order['orderID']?>'></span>
                </td>
            </tr>
            <? endforeach; endif; ?>
	</tbody>
	<tfoot>
            <tr>
                <td colspan='5'></td>
                <td><input id='check_all_btn' type='button' value='X' /></td>
                <td colspan='3'>
                <input id='capture_btn' name='submit' type='submit' value=<?= __("capture", "Capture Checked Orders") ?> />
                <span id='capture_text' style='display:none'><?= __("wait", "Please wait while we process these orders...") ?></span></td>
                <td colspan='3'></td>
            </tr>
	</tfoot>
</table>
</form>


<a href='/admin/orders/quick_order'><?= __("quick", "Quick Orders") ?></a>
