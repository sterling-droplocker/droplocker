<?

enter_translation_domain("admin/orders/add_quick_order");

?>
<h2><?= __("add_quick_order","Add Quick Order")?></h2>
<p><a href="/admin/orders/quick_order"><img src="/images/icons/arrow_left.png"><?= __("back_to_order","Back to Quick Orders")?></a></p>

<form action='' method='post'>
<table class='detail_table'>
    <tr>
        <th><?= __("name","Name")?></th>
        <td><input type='text' name='name' /></td>
    </tr>
    <tr>
        <th><?= __("bag_number","Bag Number")?></th>
        <td><input type='text' name='bagNumber' /></td>
    </tr>
</table>
<input class='button orange' type='submit' name='submit' value="<?= __("submit","Submit")?>"/>
</form>


