<?php enter_translation_domain("admin/orders/quick_scan_bag");?>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#bag_number').keypress(function(e) {
		  if (e.keyCode == '13') {
		     e.preventDefault();
		    top.location.href = "/admin/orders/bag_barcode_details/0/"+$("#bag_number").val();
		   }
		});

	$("#submit").click(function(){
		top.location.href = "/admin/orders/bag_barcode_details/0/"+$("#bag_number").val();
	});

	$(".flash_data a").click(function(e){
		e.preventDefault();
		var clicked = $(this);
		var link = $(this).attr("href");
		if (link.includes("create_shoe_shine")) {			
			$( ".datepicker" ).datepicker({
				showOn: "button",
				buttonImage: "/images/icons/calendar.png",
				buttonImageOnly: true,
				altField: "#dueDate",
				altFormat: "yy-mm-d",
				dateFormat: "MM d, yy"
			});
		
			$("#due_date_dialog").dialog({
					width: "450px",
					title: "<?=__("due_date_dialog_title", "Due Date"); ?>",
					modal: true,
					buttons: {
						"Cancel" : function() {
							$(this).dialog("close");
						},
						"Save" : function() {
							var dueDate = $("#dueDate").val();
							link += '&due_date='+dueDate;
							window.location = link;
						}
					}
			});
		}
	});
});
</script>

<h2><?= __("title","Quick Scan Bag")?></h2><br>
<div>
    <div class="flash_data"><?php echo $this->session->flashdata('static_message');?></div>
    <?= __("bag_number","Bag #:")?> <input type='text' id='bag_number' name='bag_number' /> <input class='button orange' id='submit' type='button' name='button' value='Process' />
    <? if(isset($lotSize) and $thisLot >= $lotSize and $lotSize > 0): ?>
        <br>
        <h2><?= __("over_recommended_lot_size","You are over the recommended lot size.")?></h2>
    <? else: ?>
        <script>$('#bag_number').focus()</script>
    <? endif; ?>
</div>
<div align="right">
<? if($pph['itemCount'] > 1): ?>
<?php $upcharges = count($upcharges); ?>
<table class="bodyReg">
	<tr>
		<td><?= __("total_items","Total Items:")?> </td>
		<td align="right"><?= $pph['itemCount'] ?> ( <?= $pph['allItems'] ?>)</td>
	</tr>
    <tr>
		<td><?= __("upcharges","Upcharges:")?> </td>
		<td align="right"> <?= number_format_locale($upcharges,0) ?> (<?= number_format(($upcharges/$pph['allItems'])*100,0) ?>%)</td>
	</tr>
	<tr>
		<td><?= __("elapsed_items","Elapsed Items:")?> </td>
		<td align="right"> <?= sec_to_time($pph['elapsedTime']) ?></td>
	</tr>
	<tr>
		<td><?= __("pcs_per_hr","PCS Per Hr:")?> </td>
		<td align="right"> <?= number_format_locale($pph['pph'],0) ?></td>
	</tr>
    <? if(isset($lotSize)): ?>
    <tr>
		<td><?= __("lot_size","Lot Size:")?> </td>
		<td align="right"> <?= $thisLot ?> of <?= $lotSize ?></td>
	</tr>
    <? endif; ?>
	</table> 

<? endif; ?>
</td></tr></table>
</div>
<div id="due_date_dialog" style="display: none;">
        <table>
            <tbody>
                <tr>
                    <td><?= __("due_date","Due date")?>&nbsp;&nbsp;<input readonly="true" class="datepicker" type="text" /></td>
					<input type='hidden' id='dueDate' name='dueDate' value='' />
				</tr>
            </tbody>
        </table>
        </form>
</div>
