<?php enter_translation_domain("admin/orders/wf_processing"); ?>
<script type="text/javascript">
    $(function(){
       $('#bagNumber').focus();
    });
</script>

<h2><?= __("title","Process Wash &amp; Fold")?></h2>
<br>
<br>
<form action="/admin/orders/wf_processing" method="post" id="wf_search">
    <?= __("bag_number","Bag Number:")?> <input type="text" id="bagNumber" autocomplete="off" name="bagNumber">
    <input type="submit" name="find" value="<?= __("find","Find")?>" class="button blue">
</form>

<? if(!empty($notice)):?>
    <br><hr><br>
    <?= $notice; ?>
<? endif; ?>

<? if(!empty($order)): ?>
    <br>
    <table id="box-table-a">
    <tr>
        <th align="left" class="leftCol"><?= __("bag_number","Bag Number")?></th>
        <td><?= $order[0]->bagNumber ?></td>
        <th><?= __("weight","Weight:")?> <?= $wfPrefs[0]->qty ?></th>
        <td align="right"><? if(isset($wfPrefs[0]->qty)) :?><a href="/admin/orders/wf_print/<?= $order[0]->orderID ?>"><?= __("print","print")?></a><? else: ?> <?= __("order_must_have_weight_to_print","Order must have weight to print:")?><? endif; ?></td>
    </tr>
    <tr>
        <th align="left" class="leftCol"><?= __("customer","Customer")?></th>
        <td><?= getPersonName($order[0]) ?></td>
        <th><?= __("order","Order:")?> <a href="/admin/orders/details/<?= $order[0]->orderID ?>" target="_blank"><?= $order[0]->orderID ?></a></th>
        <td align="right"></td>
    </tr>
    <tr>
        <th align="left" valign="top" class="leftCol"><?= __("preferences","Preferences")?></th>
        <td colspan="3" valign="top"><?= nl2br(trim($wfPref->notes)) ?>
        <br><br><?= __("notes_sent_to_WF:","Notes sent to WF:")?><br><strong><?= nl2br(trim($order[0]->notes)) ?></strong></td>
    </tr>
    <form action="/admin/orders/wf_process_update" method="post" id="wf_notes">
    <input type="hidden" name="bagNumber" value="<?= $_POST['bagNumber'] ?>">
    <input type="hidden" name="orderId" value="<?= $order[0]->orderID ?>">
    <tr>
            <th align="left" class="leftCol"><?= __("internal_notes:","Internal Notes")?></th>
            <td colspan="2">
            <textarea cols="40" rows="2" name="notes"><?= $wfLog[0]->notes ?></textarea>
            </td>
            <td colspan="1"><input type="submit" name="update" value="<?= __("add_notes","Add Notes")?>" class="button blue small">
            </td>
    </tr>
    </form>

    <form action="/admin/orders/wf_process_update" method="post" id="wf_process">
        <input type="hidden" name="logData" value="1">
        <input type="hidden" name="bagNumber" value="<?= $_POST['bagNumber'] ?>">
        <input type="hidden" name="orderId" value="<?= $order[0]->orderID ?>">
        <tr>
            <th align="left" class="leftCol"><?= __("incoming_weight","Incoming Weight")?></th>
            <? if($wfLog[0]->weightIn): ?> <td><?= $wfLog[0]->weightIn  ?> <?= weight_system_format( '', false, true );?></td>
            <td><?= $wfLog[0]->weightInCust ?></td>
            <td><?= local_date($this->business->timezone, $wfLog[0]->weightInTime, 'D m/d/y h:i a') ?></td>
            <? else: ?>
            <td colspan=3><?= __("employee","Employee:")?> <input type="text" name="user" size="15"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= __("weight","Weight:")?> <input type="text" name="weightIn" size="5" autocomplete="off"></td>
            <? endif; ?>
        </tr>

        <?php if ($business->wash_fold_tracking): ?>
            <tr>
                <th align="left" class="leftCol"><?= __("wash","Wash")?></th>
                <? if($wfLog[0]->washCust): ?>
                        <td>Washed (<?= $wfLog[0]->washStation ?>)</td><td><?= $wfLog[0]->washCust ?></td>
                        <td><?= local_date($this->business->timezone, $wfLog[0]->washTime, 'D m/d/y h:i a') ?></td>
                <? elseif($wfLog[0]->weightIn): ?>
                        <td colspan="3"><?= __("employee","Employee:")?> <input type="text" name="user" size="15">&nbsp;&nbsp;&nbsp;&nbsp; <?= __("washer","Washer:")?> <input type="text" name="washStation" size="5"> <input type="submit" name="logWash" value="<?= __("log_washing","Log Washing")?>"></td>
                <? else: ?>
                        <td colspan="3">&nbsp;</td>
                <? endif; ?>
            </tr>
            <tr>
                <th align="left" class="leftCol"><?= __("dry","Dry")?></th>
                <? if($wfLog[0]->dryCust): ?>
                        <td><?= __("dried","Dried")?> (<?= $wfLog[0]->dryStation ?>)</td><td><?= $wfLog[0]->dryCust ?></td>
                        <td><?= local_date($this->business->timezone, $wfLog[0]->dryTime, 'D m/d/y h:i a') ?></td>
                <? elseif($wfLog[0]->washCust): ?>
                        <td colspan="3"><?= __("employee","Employee:")?> <input type="text" name="user" size="15">&nbsp;&nbsp;&nbsp;&nbsp; <?= __("dryer","Dryer:")?> <input type="text" name="dryStation" size="5">  <input type="submit" name="logDry" value="Log Drying"></td>
                <? else: ?>
                        <td colspan="3">&nbsp;</td>
                <? endif; ?>
            </tr>
            <tr>
                <th align="left" class="leftCol"><?= __("fold","Fold")?></th>
                <? if($wfLog[0]->foldCust): ?>
                        <td>Folded (<?= $wfLog[0]->foldStation ?>)</td><td><?= $wfLog[0]->foldCust ?></td>
                        <td><?= local_date($this->business->timezone, $wfLog[0]->foldTime, 'D m/d/y h:i a') ?></td>
                <? elseif($wfLog[0]->dryCust): ?>
                        <td colspan="3"><?= __("employee","Employee:")?> <input type="text" name="user" size="15">&nbsp;&nbsp;&nbsp;&nbsp; <?= __("station","Station:")?> <input type="text" name="foldStation" size="5">  <input type="submit" name="logFold" value="<?= __("log_folding","Log Folding")?>"></td>
                <? else: ?>
                        <td colspan="3">&nbsp;</td>
                <? endif; ?>
            </tr>
        <?php endif; ?>
        <tr>
            <th align="left" class="leftCol"><?= __("finished_weight","Finished Weight")?></th>
            <? if($this->business_id): ?>
                    <? if($wfLog[0]->weightOut): ?>
                            <td><?= $wfLog[0]->weightOut ?> <?= weight_system_format( '', false, true );?></td>
                            <td><?= $wfLog[0]->weightOutCust ?></td>
                            <td><?= local_date($this->business->timezone, $wfLog[0]->weightOutTime, 'D m/d/y h:i a') ?></td>
                    <? elseif((!$business->wash_fold_tracking && $wfLog[0]->weightIn) || $wfLog[0]->foldCust): ?>

                            <td colspan="3"><?= __("employee","Employee:")?> <input type="text" name="user" size="15"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= __("weight","Weight:")?> <input type="text" autocomplete="off" name="weightOut" size="5"></td>
                    <? else: ?>

                            <td colspan="3">&nbsp;</td>
                    <? endif; ?>
            <? else: ?>
                    <td colspan="3" class="leftCol"><?= __("you_must","You must")?> <a href="/admin/login"><?= __("login","login")?></a></td>
            <? endif; ?>
        </tr>
        <tr>
            <td colspan="4" align="center" class="leftCol"><input type="submit" name="update" value="<?= __("update","Update")?>" class="button orange"></td>
        </tr>
    </table>
</form>
<? elseif (!empty($noOrder)): ?>
    <hr>

    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#no_open_order_for_bag_form").validate(
                {
                    rules: {
                        noBag : {
                            required: true
                        },
                        weight: {
                            required: true
                        },
                        bagInfo : {
                            required: true
                        }

                    },
                    errorClass: "invalid"
                }
            );

        });
    </script>
    <p>  <?= __("sorry_theres_no_open_bag","Sorry, there is no open order for this bag so you can't wash it.  Bag: %bag_number%", array("bag_number"=>$_POST['bagNumber']))?></p>

    <p> <?= __("if_you_need_this_order_created","If you need this order created, please rescan the bag and an email will be sent to customer service.")?></p>

    <form id="no_open_order_for_bag_form" action="/admin/orders/wf_process_create" method="post">
        <input type="hidden" name="bagNumber" value="<?= $_POST['bagNumber'] ?>">
        <?= __("confirm_bag_number","Confirm Bag Number:")?> <input type="text" name="noBag" id="noBag">
        <?= __("weight","Weight")?> <input type="text" name="weight" size="5">

        <p> <?= __("is_there_any_name_or_info_inside_the_bag","Is there any name or info inside the bag or on the tag?")?> <input type="text" name="bagInfo" size="40"> </p>
        <input type="submit" name="submit" value="<?= __("please_create_this_order","Please Create This Order")?>" class="orange button"></strong><br><br>
    </form>
<? endif; ?>


<script type="text/javascript">
$('#wf_process').submit(function(evt) {

    var input = $('input[name=weightIn]');
    if (!input.length) {
        return true;
    }

    var weight = parseInt(input.val());
    if (weight < 100) {
        return true;
    }

    if (confirm("<?= __("confirm","Are you sure you want to continue with a weight of %weight% lbs ?")?>".replace('%weight%', weight))) {
        return true;
    }

    evt.preventDefault();
    input.focus();
});
</script>
