<?php

enter_translation_domain("admin/orders/export_settings");

$edit_form = !in_bizzie_mode() || is_superadmin();

$fabricare_service_days = !empty($fabricare_service_days) ? unserialize($fabricare_service_days) : array();
$fabricare_suppliers = !empty($fabricare_suppliers) ? unserialize($fabricare_suppliers) : array();

$days = array(
    1 => "Monday",
    2 => "Tuesday",
    3 => "Wednesday",
    4 => "Thursday",
    5 => "Friday",
    6 => "Saturday",
    7 => "Sunday"
);

?>

<h2><?= __("export_settings","Export Settings")?></h2>

<? if ($edit_form): ?>
    <form action='' id='select_form' method='post'>
<? endif ?>
        <table class='detail_table'>
            <th><?= __("select_software","Select Software")?></th>
            <td>
                <? if ($edit_form): ?>
                <?= form_dropdown('software', $softwares, $software, "id='software'"); ?>
                <? endif ?>
            </td>
        </table>
 <? if ($edit_form): ?>
    </form>
<? endif ?>



<? if ($edit_form): ?>
    <form action='' method='post'>
<? endif ?>

        <table class='detail_table'>
        <? if ($software == 'spot'): ?>
            <tr>
                <th><?= __("server_url","Server URL")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='spot_url' value='<?= $spot_url ?>' />
                    <? else: ?>
                        <?= $spot_url ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("account_key","Account Key")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='spot_accountKey' value='<?= $spot_accountKey ?>' />
                    <? else: ?>
                        <?= $spot_accountKey ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("device","Device Name")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='spot_deviceName' value='<?= $spot_deviceName ?>' />
                    <? else: ?>
                        <?= $spot_deviceName ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("user_name","User Name")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='spot_userName' value='<?= $spot_userName ?>' />
                    <? else: ?>
                        <?= $spot_userName ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("price_table","Price Table")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='spot_priceTable' value='<?= $spot_priceTable ?>' />
                    <? else: ?>
                        <?= $spot_priceTable ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("customer_prefix", "Customer Prefix")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='spot_customer_prefix' value='<?= $spot_customer_prefix ?>' />
                    <? else: ?>
                        <?= $spot_customer_prefix ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("add_split_item_count", "Add split items count")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <?= form_yes_no('spot_add_split_item_count', $spot_add_split_item_count); ?>
                    <? else: ?>
                        <?= $spot_add_split_item_count ?>
                    <? endif ?>
                </td>
            </tr>


            <? elseif ($software == 'fabricare'): ?>
            <tr>
                <th><?= __("fabricare_ready_by","Ready By Days")?></th>
                <td>
                    <? if ($edit_form): ?>
                        <input type='text' name='fabricare_ready_by' value='<?= $fabricare_ready_by ?>' />
                    <? else: ?>
                        <?= $fabricare_ready_by ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?= __("fabricare_service_days","Service Days")?></th>
                <td>
                    <? foreach ($days as $index => $day): ?>
                        <? if ($edit_form): ?>
                            <?= form_checkbox(array(
                                    "id" => "day_{$index}",
                                    "name" => "fabricare_service_days[]",
                                    "class" => "serviceDay",
                                    "checked" => in_array($index, $fabricare_service_days),
                                    "value" => $index
                                ));
                            ?>
                            <label for="day_<?= $index ?>"><?= $day ?></label>
                        <? else: ?>
                            <?= isset($index, $fabricare_service_days) ? $day : '' ?>
                        <? endif; ?>
                    <? endforeach; ?>
                </td>
            </tr>
            <tr>
                <th><?= __("fabricare_suppliers", "Suppliers")?>
                    <div class="note"><?= __("fabricare_suppliers_notes", "(Leave empty for ALL)") ?></div>
                </th>
                <td>
                    <? foreach ($suppliers as $index => $supplier): ?>
                        <? if ($edit_form): ?>
                            <?= form_checkbox(array(
                                    "id" => "supplier_{$index}",
                                    "name" => "fabricare_suppliers[]",
                                    "class" => "",
                                    "checked" => in_array($index, $fabricare_suppliers),
                                    "value" => $index
                                ));
                            ?>
                            <label for="supplier_<?= $index ?>"><?= $supplier ?></label><br>
                        <? else: ?>
                            <?= isset($index, $fabricare_suppliers) ? $supplier : '' ?>
                        <? endif; ?>
                    <? endforeach; ?>
                </td>
            </tr>
            <? endif; ?>
        </table>
         <? if ($edit_form): ?>
            <input class='button orange' type='submit' name='submit' value="<?= __("update","Update")?>" />
        <? endif ?>

<? if ($edit_form): ?>
    </form>
<? endif ?>


<script type="text/javascript">
    $(document).ready(function(){
        $("#software").change(function(){
            $("#select_form").submit();
        });
    });
</script>
