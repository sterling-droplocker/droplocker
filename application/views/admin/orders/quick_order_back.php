<?php enter_translation_domain("admin/orders/quick_order_back"); ?>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href='/css/admin/style.css'/>
	</head>
	<body>
            <?= $this->session->flashdata('status_message') ?>
		<div style='text-align:left; padding:10px;'>
			<h2><?php echo __("Quick Order", "Quick Order"); ?></h2><br>
			<? if(!empty($order_id)):?>
				<div>
				<div class='success' style='padding:5px;'><?php echo __("Order", "Order"); ?> <a target='_top' href="admin/orders/details/<?= $order_id ?>"><?= $order_id ?></a> <?php echo __("has been created.  You will be redirected to the order momentarily", "has been created.  You will be redirected to the order momentarily."); ?>
				<script>
				    setTimeout("redirect()",3000);
					function redirect(){
					self.parent.top.location.href='/admin/orders/details/<?= $order_id ?>/?new_order=true';
					self.parent.tb_remove();
					}
			    </script>   
				</div>
				</div>
			<? else: ?>
			<?if(strtoupper($locker->lockerName) == "FRONT_COUNTER" || $location->locationTypeName =="Concierge" || $this->zacl->check_acl('add_order')):?>
			
				<div style='padding:5px;'>
				<form action="" method="post">
				<?php echo __("Bag Number", "Bag Number:"); ?> <input type="text" name="bagNumber" value="<?= $bagNumber ?>" id="bagNumber">
				<input type="hidden" name="lockerID" value="<?=  $locker->lockerID?>">
				<input type="hidden" name="locationID" value="<?=  $location->locationID?>">
				<input type="hidden" name="claimID" value="<?=  $claim_id?>">
				<? if($dialog): ?>
				<input type="hidden" name="dialog" value="true">
				<? endif; ?>
				<input class='button orange' type="submit" name="submit" value="<?php echo __("Create Order", "Create Order"); ?>">
				</form>
				</div>
                <script type="text/javascript">
                   document.getElementById('bagNumber').focus();
                </script>
			<? else: ?>
			<div>
				<div class='error' style='padding:10px;'><?php echo __("This is not a Concierge location message", "This is not a Concierge location.<br><br><strong><u>YOU CAN NOT CREATE AN ORDER HERE!</u></strong><br><br>
				This most likely means that the driver has not synced.  Have them sync, put the bag aside, or contact a manager."); ?></div>
			</div>
			<?
			//if($this->zacl->check_acl('add_order')){
				
			//}
			?>
			<? endif; endif;?>
		</div>
		
	</body>
</html>
