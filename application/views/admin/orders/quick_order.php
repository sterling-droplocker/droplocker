<?php enter_translation_domain("admin/orders/quick_order"); ?>
<h2><?php echo __("Quick Order Form", "Quick Order Form"); ?></h2>
<br>



<? if($quickOrders): ?>
<p><?php echo __("Click on the business name to place an order", "Click on the business name to place an order"); ?></p>
<table id='box-table-a''>
    <tr>
        <th><?php echo __("Business Name", "Business Name"); ?></th>
        <th><?php echo __("Bag Number", "Bag Number"); ?></th>
        <th style='width:25px'><?php echo __("Edit", "Edit"); ?></th>
        <th style='width:25px'><?php echo __("Delete", "Delete"); ?></th>
    </tr>

    <? foreach($quickOrders as $quickOrder): ?>
    <tr>
        <td><a href="/admin/orders/quick_order_bag/<?= $quickOrder->bagNumber?>"><?= $quickOrder->name ?></a></td>
        <td><?= $quickOrder->bagNumber ?></a></td>
        <td align='center'><a href='/admin/orders/edit_quick_order/<?= $quickOrder->quickOrderID?>'><img src='/images/icons/application_edit.png' /></a></td>
        <td align='center'><a onClick='return verify_delete();' href='/admin/orders/delete_quick_order/<?= $quickOrder->quickOrderID?>'><img src='/images/icons/cross.png' /></a></td>
    </tr>
    <? endforeach; ?>
    
</table>
<? else: ?>
<h3><?php echo __("No Quick Orders", "No Quick Orders"); ?></h3>
<? endif; ?>

<p><a href="/admin/orders/add_quick_order" class='button orange'><?php echo __("Add Quick Order", "Add Quick Order"); ?></a><p>
