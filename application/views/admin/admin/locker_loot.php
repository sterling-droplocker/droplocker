<?php enter_translation_domain("admin/admin/locker_loot"); ?>
<?php
/**
 * Expects the following content variables:
 *  lockerLootConfig
 */
if (!isset($lockerLootConfig)) {
    trigger_error(__("lockerLootConfig must be set as a content variable", "'lockerLootConfig' must be set as a content variable"), E_USER_WARNING);
}

$prefix= '$';
$suffix = '';
if($this->business->businessID != '') {
    $monetary_symbol = get_business_meta($this->business->businessID , 'money_symbol_str');
    $monetary_symbol_position = get_business_meta($this->business->businessID, 'money_symbol_position');
    if($monetary_symbol != ''){
        if($monetary_symbol_position){
            $prefix= $monetary_symbol;
            $suffix= '';
        }else{
            $prefix = '';
            $suffix= $monetary_symbol;
        }
    }
}
?>

<script type="text/javascript">
$(document).ready(function() {
    $("#update_rewards_form :submit").loading_indicator();
    $("#update_rewards_form").validate( {
        rules: {
            percentPerOrder : {
                required: true,
                number: true
            },
            redeemPercent : {
                required: true,
                number: true
            },
            minRedeemAmount : {
                required: true,
                number : true
            },
            referralAmountPerReward : {
                required: true,
                number: true
            },
            referralNumberOrders : {
                required: true,
                number: true
            },
            referralOrderAmount : {
                required: true,
                number : true
            },
            programName : {
                required: true
            },
            terms : {
                required : true
            }
        },
        errorClass : "invalid",
        invalidHandler : function(form, validator) {
            $("#update_rewards_form :submit").loading_indicator("destroy");
        }
    });
});
</script>

<h2><?= $lockerLootConfig->programName?> Status</h2>

<br />
<?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
    <p>
        <a href='/admin/admin/manage_coupons?search=CR'><?php echo __("Edit CR Coupon", "Edit CR Coupon"); ?></a>
    </p>
<?php endif ?>

<?= form_open("/admin/lockerLootConfigs/update_rewards_program", array("id" => "update_rewards_form")) ?>
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Reward Program Status", "Reward Program Status"); ?></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <?= form_checkbox("lockerLootStatus", 1, $lockerLootConfig->lockerLootStatus) ?>
                <?php else: ?>
                    <?= $lockerLootConfig->lockerLootStatus ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Locker Loot Terms", "Locker Loot Terms"); ?> </th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <?= form_textarea("terms", $lockerLootConfig->terms) ?>
                <?php else: ?>
                    <?= $lockerLootConfig->terms ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Reward Program Name", "Reward Program Name"); ?></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <input type='text' name=programName value="<?= $lockerLootConfig->programName?>" />
                <?php else: ?>
                    <?= $lockerLootConfig->programName ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Percentage per Order", "Percentage per Order"); ?><br />
            <span style='font-size:10px;'><?php echo __("The percentage the customer will receive from each order they place", "The percentage the customer will receive from each order they place"); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <input type='text'  style='width:30px' name='percentPerOrder' value="<?= $lockerLootConfig->percentPerOrder?>" /> %
                <?php else: ?>
                    <?= $lockerLootConfig->percentPerOrder ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Allow to Redeem", "Allow to Redeem"); ?><br />
            <span style='font-size:10px;'><?php echo __("Allow your customer to redeem their lockerLoot points for cash", "Allow your customer to redeem their lockerLoot points for cash"); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <?= form_checkbox("allowToRedeem", 1, $lockerLootConfig->allowToRedeem) ?>

                <?php else: ?>
                    <?= $lockerLootConfig->allowToRedeem ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Redeem for Cash Percent", "Redeem for Cash Percent"); ?><br />
            <span style='font-size:10px;'><?php echo __("The percent of the customer reward total amount they will receive if they want to redeem their rewards for cash", "The percent of the customer reward total amount they will receive if they want to redeem their rewards for cash"); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <input type='text' style='width:30px' name='redeemPercent' value="<?= $lockerLootConfig->redeemPercent?>" />

                <?php else: ?>
                    <?= $lockerLootConfig->redeemPercent ?>
                <?php endif ?>
                %
            </td>
        </tr>
        <tr>
            <th><?php echo __("Redeem Minimum Amount", "Redeem Minimum Amount"); ?><br />
            <span style='font-size:10px;'><?php echo __("Minimum amount of rewards that customer needs to redeem for cash", "Minimum amount of rewards that customer needs to redeem for cash"); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <?= $prefix;?><input type='text'  style='width:30px' name='minRedeemAmount' value="<?= $lockerLootConfig->minRedeemAmount?>" /><?= $suffix;?>
                <?php else: ?>
                    <?= $prefix;?><?= $lockerLootConfig->minRedeemAount?><?= $suffix;?>
                <?php endif ?>
            </td>
        </tr>

        <tr>
            <th><?php echo __("Referral Amount per Order", "Referral Amount per Order"); ?><br />
            <span style='font-size:10px;'><?php echo __("Reward amount the customer will receive with their referred customer makes an order over the Referral Order Amount", "Reward amount the customer will receive with their referred customer makes an order over the \"Referral Order Amount\""); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <?= $prefix;?><input type='text' style='width:30px' name='referralAmountPerReward' value="<?= $lockerLootConfig->referralAmountPerReward?>" /><?= $suffix;?>
                <?php else: ?>
                    <?= $prefix;?><?= $lockerLootConfig->referralAmountPerReward?><?= $suffix;?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Referral Number of Orders", "Referral Number of Orders"); ?><br />
            <span style='font-size:10px;'><?php echo __("The customer will receive referral until the referred customer reaches this number of orders", "The customer will receive referral until the referred customer reaches this number of orders"); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <input type='text'  style='width:30px' name='referralNumberOrders' value="<?= $lockerLootConfig->referralNumberOrders?>" />
                <?php else: ?>
                    <?= $lockerLootConfig->referralNumberOrders ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Referral Order Amount", "Referral Order Amount"); ?><br>
            <span style='font-size:10px;'><?php echo __("The order has to be over this amount for the customer to earn referral rewards", "The order has to be over this amount for the customer to earn referral rewards"); ?></span></th>
            <td>
                <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
                    <?= $prefix;?><input type='text' style='width:30px' name='referralOrderAmount' value="<?= $lockerLootConfig->referralOrderAmount?>" /><?= $suffix;?>
                <?php else: ?>
                    <?= $prefix;?><?= $lockerLootConfig->referralOrderAmount ?><?= $suffix;?>
                <?php endif ?>
            </td>
        </tr>
    </table>
    <p>
        <?php if (!in_bizzie_mode() || in_bizzie_mode() && is_superadmin()): ?>
            <input type='submit' name='submit' value='<?php echo __("Submit", "Submit"); ?>' class='button orange' />
        <?php endif ?>
    </p>
</form>