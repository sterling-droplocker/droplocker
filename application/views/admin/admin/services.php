<?php enter_translation_domain("admin/admin/services"); ?>
<?php

$edit_services = !in_bizzie_mode() || is_superadmin();

?>
<h2> <?= $header ?> </h2>
<p><?php echo __("Select the services that your business provide. These options will be visible to the customer when placing an order", "Select the services that your business provide. These options will be visible to the customer when placing an order."); ?></p>

<?php if ($edit_services): ?>
<p>
    <a href="#" id="add_service"><img src="/images/icons/add.png"><?php echo __("Add New Service", "Add New Service"); ?></a>
</p>

<form action='/admin/admin/create_service' method="post" id="create_service" style="display:none;">
    <table class="detail_table">
    <tbody>
        <tr>
            <th><?php echo __("Name", "Name"); ?>*</th>
            <td>
                <input type="text" name="name" id="new_name" value="">
            </td>
        </tr>
        <tr>
            <th><?php echo __("Slug", "Slug"); ?>*</th>
            <td>
                <input type="text" name="slug" id="new_slug" value="">
            </td>
        </tr>
        </tbody>
    </table>

    <input type="submit" value='Add New Service' class='button orange' name='submit' />
    <p>* <?php echo __("This filed is required", "This filed is required"); ?></pd>

    <br><br><hr><br><br>
</form>
<?php endif ?>

<!-- Note the admin service fields shall be read only when the server is in Bizzie mode -->
<?php if ($edit_services): ?>
    <form action='/admin/admin/service_add_remove_to_all_locations' method="post" id="frm_add_remove_service" style="display:none">
        <input type="text" name="service_id" value="" />
        <input type="text" name="action" value="" />
    </form>
    <form action='' method="post">
<?php endif ?>
        <div id="services_container" style="width:840px;">
    <?php foreach ($serviceTypes as $serviceType) : ?>
        <div style='float:left; width:100%; padding:5px;margin-top:10px;'>
            <?php if ($edit_services): ?>
            <span class="ui-icon ui-icon-arrow-4-diag sort-handle" style="float:left;"></span>
            <div style="float:left;width:20px;margin-left:10px;">
                <input type="checkbox" checked="true" name="type[]" value="<?= $serviceType->serviceTypeID?>" style="display: none" />
                <?php if (in_array($serviceType->serviceTypeID, $services_is_use)): ?>
                        <span style="color:green;cursor:pointer;" title="In Use">&#10003;</span>
                <?php else: ?>
                        <span style="color:red;cursor:pointer;" title="Not In Use">X</span>
                <?php endif; ?>
            </div>
            <div style="float:left;width:110px;">
                <?= $serviceType->name?>
            </div>
            <div style="float:left;width:40%;">
                <input type="" style="width:90%;" name="displayName[<?=$serviceType->serviceTypeID;?>]" value="<?if(!empty($current_types[$serviceType->serviceTypeID]['displayName'])){ echo $current_types[$serviceType->serviceTypeID]['displayName']; }?>" />
            </div>
            <div style="float:left;width:40%;">
                <input type="button" class="btn_add_remove_service" name="add_to_all_locations" value="Add to all locations" data-service-id="<?=$serviceType->serviceTypeID;?>" />
                <input type="button" class="btn_add_remove_service" name="remove_from_all_locations" value="Remove from all locations"  data-service-id="<?=$serviceType->serviceTypeID;?>" />
            </div>

            <?php else: ?>
            <div style="float:left;width:60%;">
                <input disabled="disabled" type="checkbox" checked="true" name="type[]" value="<?= $serviceType->serviceTypeID?>" style="display: none" /> <?= $serviceType->name?>       
            </div>
            <?php endif ?>
        </div>
    <? endforeach ; ?>
        </div>
    <br style='clear:left;' />
    <br />
<?php if ($edit_services): ?>
    <input type="submit" value='<?php echo __("Update", "Update"); ?>' class='button orange' name='submit' />
</form>
<?php endif ?>


<script type="text/javascript">
$(document).ready(function() {
    $(":submit").loading_indicator();
    $('#services_container').sortable({handle: ".sort-handle"});

    $('#add_service').click(function(evt) {
       $('#create_service').show();
    });

    $('.btn_add_remove_service').on("click", function(){
        if (!confirm("<?php echo __("Are you sure", "Are you sure ?"); ?>")) {
            return false;
        }
        var service_id = $(this).attr("data-service-id");
        var action = $(this).attr("name");
        var frm = $("#frm_add_remove_service");
        frm.find("[name=service_id]").val(service_id);
        frm.find("[name=action]").val(action);
        frm.submit();
    });
});
</script>
