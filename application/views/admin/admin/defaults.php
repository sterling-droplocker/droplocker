<?php enter_translation_domain("admin/admin/defaults"); ?>
<?php
//Note, most of the following view is written by * and not all th eexpected view content variables are known.
/**
 * Expects atleast the following content variables
 * returnToOffice_location_id int
 * returnToOffice_locker_id int
 * locations_dropdown array For selecting the return the office location
 * lockers_dropdown array For selectingthe return to office locker
 * all_lockers array For selecting the prepayment lockers
 */

if (isset($returnToOffice_location_id) && !is_numeric($returnToOffice_location_id)) {
    trigger_error(__("returnToOffice_location_id must be numeric", "'returnToOffice_location_id' must be numeric"), E_USER_WARNING);
}
if (isset($returnToOffice_locker_id) && !is_numeric($returnToOffice_locker_id)) {
    trigger_error(__("returnToOffice_locker_id must be numeric", "'returnToOffice_locker_id' must be numeric"), E_USER_WARNING);
}
if (!is_array($locations_dropdown)) {
    trigger_error(__("locations_dropdown must be an array", "'locations_dropdown' must be an array"), E_USER_WARNING);
}
if (!is_array($lockers_dropdown)) {
    trigger_error(__("lockers_dropdown must be an array", "'lockers_dropdown' must be an array"), E_USER_WARNING);
}



?>

<style>
    .invalid {
        color: red;
        display: block;
        margin-top: 5px;
    }
</style>

<script type="text/javascript">

function getLockers(event) {
    var location_id = $(this).val();
    var element = $(this).next('select');

    $.getJSON("/admin/lockers/get_all_for_location", {
        locationID : location_id
    }, function(result) {
        $(element).empty();
        if (result['status'] == "success") {
            if (result['data'].length < 1) {
                var $option = $("<option>").text("<?php echo __("No Lockers for this Location", "--- No Lockers for this Location ---"); ?>").val(0);
                $option.appendTo(element);
            }
            $.each(result['data'], function(lockerID, lockerName) {
                var $option = $("<option>").text(lockerName).val(lockerID);
                $option.appendTo(element);
            });
        }
        else {
            alert(result['error']);
        }
    }).error(function() {
        alert("<?php echo __("A server side error occurred attempting to retrieve the lockers for the return to office location", "A server side error occurred attempting to retrieve the lockers for the return to office location"); ?>");
    });
 }

$(document).ready(function(){
    $("#blank_customer").autocomplete({
        source : "/ajax/customer_autocomplete_aprax",
        select: function (event, customer) {
            $("input[name='blank_customer_id']").val(customer.item.id);
        }
    });

    $('#returnToOffice_location_menu').change(getLockers);
    $('#prepayment_location_id').change(getLockers);

   $(":submit").loading_indicator();
});

</script>

<h2><?php echo __("Business Defaults", "Business Defaults"); ?></h2>

<form action='/admin/admin/update_defaults' method='post'>
    <table class="detail_table">
        <tr>
            <th><?php echo __("Blank Customer", "Blank Customer"); ?></th>

            <td>
                <?= form_input(array("id" => "blank_customer", "style" => "width: 500px;", "value" => getPersonName($customer))) ?>
                <?= form_hidden("blank_customer_id", $business->blank_customer_id) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Package Delivery Product", "Package Delivery Product"); ?></th>
            <td>
                <?= form_dropdown('default_package_delivery_id', $product_options, get_business_meta($this->business_id, 'default_package_delivery_id'), "style='width:90%' "); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default Redo Product", "Default Redo Product"); ?></th>
            <td>
                <?= form_dropdown('default_redo_id', $product_options, get_business_meta($this->business_id, 'default_redo_id'), "id='redo_id' style='width:90%' "); ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Laundry Plan Locker", "Laundry Plan Locker"); ?> </th>
            <td>
                <?= form_dropdown("prepayment_location_id", $locations_dropdown, $prepayment_location_id, "id='prepayment_location_id'") ?>
                <?= form_dropdown("prepayment_locker_id", $prepayment_dropdown, $business->prepayment_locker_id) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Return to Office Location", "Return to Office Location"); ?></th>
            <td>
                    <?= form_dropdown("returnToOffice_location_id", $locations_dropdown, $returnToOffice_location_id, "id='returnToOffice_location_menu'") ?>
                    <?= form_dropdown("returnToOffice_locker_id",$lockers_dropdown, $returnToOffice_locker_id, "id='returnToOffice_locker_menu'") ?>
            </td>
        </tr>
        <tr>
            <th> <?php echo __("Store Access", "Store Access"); ?> </th>
            <td>
                <?= form_checkbox("store_access", 1, $business->store_access) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("How it works link", "\"How it works\" link"); ?></th>
            <td><input type="text" name="howitworks" style="width:300px;" value="<?=$business->howitworks;?>" /></td>
        </tr>
        <tr>
            <th><?php echo __("Custom WF Traveller Text", "Custom WF Traveller Text:"); ?></th>
            <td><input type="text" name="wf_traveller_text" maxlength="255" style="width:600px;" value="<?=$business->wf_traveller_text;?>" /></td>
        </tr>
        <tr>
            <th><?php echo __("Require Credit Card For Order", "Require Credit Card For Order"); ?></th>
            <td>
                <?= form_yes_no("require_card_on_order", $business->require_card_on_order); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Customer requires to input Address2", "Customer requires to input Address2"); ?></th>
            <td>
                <?= form_yes_no('require_address2', get_business_meta($this->business_id, "require_address2", 0)); ?>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo __("Show Locations Map", "Show Locations Map"); ?>
            </th>
            <td>
                <?= form_dropdown('show_locations_map', $map_options, $business->show_locations_map); ?>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo __("Results for Location Search Map v3", "Results for Location Search<br>Map v3"); ?>
            </th>
            <td>
                <?= form_input('location_result_limit',  get_business_meta($this->business_id, 'location_result_limit', 3), 'style="width:50px;"'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Disable Home Delivery around this miles of a location", "Disable Home Delivery around this miles of a location"); ?></th>
            <td>
                <?= form_input('location_hd_exclusion_radius',  get_business_meta($this->business_id, 'location_hd_exclusion_radius', 0), 'style="width:50px;"'); ?>
                <?= form_dropdown("location_hd_exclusion_radius_unit", $units_dropdown, get_business_meta($this->business_id, 'location_hd_exclusion_radius_unit', Units::DISTANCE_MILES)) ?>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo __("Map api key", "Map api key"); ?>
            </th>
            <td>
                <input type="text" name="maps_api_key" maxlength="255" style="width:600px;" value="<?=get_business_meta($this->business_id, 'maps_api_key');?>" />
                <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank"><?php echo __("Get api key", "Get api key"); ?></a>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo __("Radius for Private Location Search", "Radius for Private Location Search"); ?>
            </th>
            <td>
                <?= form_input('private_location_radius',  get_business_meta($this->business_id, 'private_location_radius', 2000), 'style="width:50px;"'); ?>
                <?= form_dropdown("private_location_radius_unit", $units_dropdown, get_business_meta($this->business_id, 'private_location_radius_unit', Units::DISTANCE_FEET)) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Unit for location distance display", "Unit for location distance display"); ?></th>
            <td>
               <?= form_dropdown("location_distance_display_unit", $units_dropdown, get_business_meta($this->business_id, 'location_distance_display_unit', Units::DISTANCE_MILES)) ?>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo __("Hide Home Delivery Locations", "Hide Home Delivery Locations"); ?>
            </th>
            <td>
                <?= form_yes_no("hide_home_delivery_locations", get_business_meta($this->business_id, 'hide_home_delivery_locations')); ?>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo __("Show Contact Link", "Show Contact Link"); ?>
            </th>
            <td>
                <?php echo __("Yes", "Yes"); ?> <input type="radio" name="show_contact_link" value="1" <?= ($business->show_contact_link == 1 ) ? ' checked':'';?>/> &nbsp;
                <?php echo __("No", "No"); ?> <input type="radio" name="show_contact_link" value="0" <?= ($business->show_contact_link == 0 ) ? ' checked':'';?>/>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Wash and Fold Minimum", "Wash &amp; Fold Minimum"); ?></th>
            <td>
               <?php echo __("Amount", "Amount:"); ?> <input type="text" name="wf_min_amount" value="<?=$business->wf_min_amount;?>" size="5" style="width: 30px;"> &nbsp;
               <select name="wf_min_type">
                   <option value=""><?php echo __("- SELECT -", "- SELECT -"); ?></option>
                   <option value="pounds" <?= ($business->wf_min_type == 'pounds' ) ? ' selected':'';?>><?= weight_system_format('',false,true);?></option>
                   <option value="dollars" <?= ($business->wf_min_type == 'dollars' ) ? ' selected':'';?>><?php echo __("Dollars", "Dollars"); ?></option>
               </select>
            </td>
        </tr>
		<tr>
            <th><?php echo __("Dry Clean Minimum", "Dry Clean Minimum"); ?></th>
            <td>
               <?php echo __("Amount", "Amount:"); ?> <input type="text" name="dc_min_amount" value="<?=$business->dc_min_amount;?>" size="5" style="width: 30px;"> &nbsp;
               <select name="dc_min_type">
                   <option value=""><?php echo __("- SELECT -", "- SELECT -"); ?></option>
                   <option value="dollars" <?= ($business->dc_min_type == 'dollars' ) ? ' selected':'';?>><?php echo __("Dollars", "Dollars"); ?></option>
               </select>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Home Delivery Surcharge", "Home Delivery Surcharge"); ?></th>
            <td>
                <?php echo __("Amount", "Amount:"); ?> <input type="text" name="home_delivery_charge_amount" size="5" style="width: 30px;" value="<?=$business->home_delivery_charge_amount;?>" />
                &nbsp;
                <select name="home_delivery_charge_type">
                    <option value=""><?php echo __("- SELECT -", "- SELECT -"); ?></option>
                    <option value="dollars" <?= ($business->home_delivery_charge_type == 'dollars' ) ? 'selected':'' ;?>><?php echo __("Dollars", "Dollars"); ?></option>
                    <option value="percentage" <?= ($business->home_delivery_charge_type == 'percentage' ) ? 'selected':'' ;?>>%</option>
                </select>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Lot Size Warning", "Lot Size Warning"); ?></th>
            <td><input type="text" name="lotSize" size="6" style="width: 30px;" value="<?= $business->lotSize ?>"> <?php echo __("pcs", "pcs"); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Invoice Notes", "Invoice Notes"); ?></th>
            <td><textarea cols="60" rows="4" name="invoiceNotes"><?= get_business_meta($this->business_id, 'invoiceNotes') ?></textarea></td>
        </tr>
        <tr>
            <th><?php echo __("Receipt Assembly Bar", "Receipt Assembly Bar"); ?></th>
            <td><select name="assemblyBar">
                	<option value="top" <?= $business->assemblyBar == 'top' ? ' SELECTED' : '' ?>><?php echo __("Top", "Top"); ?></option>
                	<option value="bottom" <?= $business->assemblyBar == 'bottom' ? ' SELECTED' : '' ?>><?php echo __("Bottom", "Bottom"); ?></option>
                </select>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Default Turnaround", "Default Turnaround"); ?></th>
            <td><input type="text" name="turnaround" size="6" style="width: 30px;" value="<?= $business->turnaround ?>"> <?php echo __("days", "days"); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Schedule Orders At", "Schedule Orders At"); ?></th>
            <td>
                    <?= form_hours($business->businessID, 'scheduled_orders_time', $business->scheduled_orders_time); ?>  <?php echo __("Uses Company Timezone", "(Uses Company Timezone)"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Deactivate HD Orders At", "Deactivate HD Orders At"); ?></th>
            <td>
                    <?= form_hours($business->businessID, 'deactivate_hd_claims_time', $business->deactivate_hd_claims_time); ?>  <?php echo __("Uses Company Timezone", "(Uses Company Timezone)"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Claimed Orders Limit", "Claimed Orders Limit"); ?></th>
            <td><input type="text" name="order_limit" size="6" style="width: 30px;" value="<?= get_business_meta($this->business_id, 'order_limit', 3) ?>"> <?php echo __("0 disables limit", "(0 disables limit)"); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Start sending invoiced remainders for POS orders after", "Start sending invoiced remainders for POS orders after"); ?></th>
            <td>
                <?= form_input('pickup_remainder_after',  get_business_meta($this->business_id, 'pickup_remainder_after', 0), 'style="width:50px;"'); ?>
                 <?php echo __("days", "days"); ?> 
                 <?php echo __("0_to_disable", "(0 to disable)"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Breakout VAT", "Breakout VAT"); ?></th>
            <td>
                <?= form_yes_no('breakout_vat', $breakout_vat = get_business_meta($this->business_id, "breakout_vat")); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Weight difference limits", "Weight difference limits"); ?></th>
            <td>
                <?php echo __("Weight upper tolerance", "Weight upper tolerance:"); ?> <input type="text" name="weight_upper_tolerance" size="6" style="width: 30px;" value="<?= get_business_meta($this->business_id, "weight_upper_tolerance", 1.5) ?>"> <?php echo __("Alert fires when an order gains more weight than this limit", "(Alert fires when an order gains more weight than this limit.)<br>Weight lower tolerance:"); ?> <input type="text" name="weight_lower_tolerance" size="6" style="width: 30px;" value="<?= get_business_meta($this->business_id, "weight_lower_tolerance", 0) ?>"> <?php echo __("Alert fires when an order looses more weight than this limit", "(Alert fires when an order looses more weight than this limit.)"); ?>
                <br>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Print Day Tags", "Print Day Tags"); ?></th>
            <td>
                <?= form_yes_no('print_day_tags', get_business_meta($this->business_id, "print_day_tags")); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Allow to pay with cash", "Allow to pay with cash"); ?></th>
            <td>
                <?= form_yes_no('pay_with_cash', get_business_meta($this->business_id, "pay_with_cash")); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Show Manual Discounts", "Show Manual Discounts"); ?></th>
            <td>
                <?= form_yes_no('show_manual_discounts', get_business_meta($this->business_id, "show_manual_discounts", in_bizzie_mode())); ?>
            </td>
        </tr>

        <tr>
            <th><?php echo __("Max receipt items", "Max receipt items"); ?></th>
            <td>
                <input type="text" name="max_receipt_items" size="6" style="width: 30px;" value="<?= get_business_meta($this->business_id, 'max_receipt_items', 12) ?>">
                <?php echo __("Notice is printed in receipts with more than this number of items", "(Notice is printed in receipts with more than this number of items)"); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Show Brand", "Show Brand"); ?></th>
            <td><?php echo __("Show Brand input when adding order items", "Show Brand input when adding order items"); ?><br>
                <?= form_yes_no('displayBrand', get_business_meta($this->business_id, 'displayBrand')) ?>
                <br><br><a href="/admin/custom_options/edit/brand_options"><?php echo __("Edit Options", "Edit Options"); ?></a>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Show Colors", "Show Colors"); ?></th>
            <td><?php echo __("Show Colors options when adding order items", "Show Colors options when adding order items"); ?><br>
                <?= form_yes_no('displayColorOptions', get_business_meta($this->business_id, 'displayColorOptions')) ?>
                <br><br><a href="/admin/custom_options/edit/color_options"><?php echo __("Edit Options", "Edit Options"); ?></a>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Hide Business in POS", "Hide Business in POS"); ?></th>
            <td><?php echo __("Wont print website and phone number in tickets for a Point of Sale Location", "Won't print website and phone number in tickets for a Point of Sale Location"); ?><br>
                <?= form_yes_no('hide_business_in_pos', get_business_meta($this->business_id, 'hide_business_in_pos')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("POS barcode print", "POS barcode print"); ?></th>
            <td><?php echo __("Select what bardcode is printed for a Point of Sale Location", "Select what bardcode is printed for a Point of Sale Location"); ?><br>
                <?= form_dropdown('print_barcode_pos', $print_barcode_options, get_business_meta($this->business_id, 'print_barcode_pos', 'bag')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Show Address", "Show Address"); ?></th>
            <td><?php echo __("Show business address on receipts", "Show business address on receipts"); ?><br>
                <?= form_yes_no('show_business_address_on_receipts', get_business_meta($this->business_id, 'show_business_address_on_receipts')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Reassigned barcode report", "Reassigned barcode report"); ?></th>
            <td>
                <?php echo __("Send by email daily", "Send by email(daily)"); ?> ?<br>
                <?= form_yes_no('send_reassigned_barcode_report_daily', get_business_meta($this->business_id, 'send_reassigned_barcode_report_daily')) ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Print two receipts in POS", "Print two receipts in POS"); ?></th>
            <td>
                <?= form_yes_no('duplicate_receipt_pos', get_business_meta($this->business_id, "duplicate_receipt_pos")); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Include due date on POS receipts", "Include due date on POS receipts"); ?></th>
            <td>
                <?= form_yes_no('pos_receipt_include_due_date', get_business_meta($this->business_id, "pos_receipt_include_due_date", 1)); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Include due date on delivery receipts", "Include due date on delivery receipts"); ?></th>
            <td>
                <?= form_yes_no('delivery_receipt_include_due_date', get_business_meta($this->business_id, "delivery_receipt_include_due_date", 0)); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Retake Picture Alert after X times submitted for cleaning", "Retake Picture Alert after X times submitted for cleaning"); ?></th>
            <td>
                <input type="text" name="retake_picture_after_x_times_submitted_for_cleaning" size="6" style="width: 100px;" value="<?= get_business_meta($this->business_id, 'retake_picture_after_x_times_submitted_for_cleaning', '') ?>" placeholder="<?php echo __("How many times", "How many times"); ?>"> 
            </td>
        </tr>
        <tr>
            <th><?php echo __("Require Time Window Home Delivery Customer to select time window for delivery", "Require Time Window Home Delivery Customer to select time window for delivery"); ?></th>
            <td>
                <?= form_yes_no('hd_time_window_require_customer_delivery_selection', get_business_meta($this->business_id, "hd_time_window_require_customer_delivery_selection", true)); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Limit deliveries by due date", "Limit deliveries by due date"); ?></th>
            <td>
                <?= form_yes_no('limit_deliveries_by_due_date', get_business_meta($this->business_id, "limit_deliveries_by_due_date", false)); ?>
        </tr>
        <tr>
            <th>Wash & Fold Rounding Precision</th>
            <td>
                <?= form_dropdown('wf_rounding_precision', range(0,3), get_business_meta($this->business_id, 'wf_rounding_precision')); ?>

            </td>
        </tr>
        <tr>
            <th><?php echo __("Home Delivery Hide Driver Notes", "Home Delivery Hide Driver Notes"); ?></th>
            <td>
                <?= form_yes_no('home_delivery_hide_driver_notes', $home_delivery_hide_driver_notes = get_business_meta($this->business_id, "home_delivery_hide_driver_notes")); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Home Delivery Enable Order Reschedule", "Home Delivery Enable Order Reschedule"); ?></th>
            <td>
                <?= form_yes_no('home_delivery_enable_order_reschedule', $home_delivery_enable_order_reschedule = get_business_meta($this->business_id, "home_delivery_enable_order_reschedule")); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Autocomplete Orders", "Autocomplete Orders"); ?></th>
            <td>
                <?= form_yes_no('autocomplete_orders', get_business_meta($this->business_id, "autocomplete_orders", 1)); ?>
            </td>
        </tr>
    </table>
    <br>
    <div align="center" style="padding:10px;"><input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' /></div>
</form>

<h2><?php echo __("Email Addresses", "Email Addresses"); ?></h2>
<form action='/admin/admin/update_email_defaults' id="email_defaults" method='post'>
<table class="detail_table">
    <tr>
        <th><?php echo __("From Email Name", "From Email Name"); ?></th>
        <td><input type='text' name='from_email_name' value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'from_email_name'))?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Customer Support", "Customer Support"); ?></th>
        <td><input type='text' name='customerSupport' value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'customerSupport'))?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Sales", "Sales"); ?></th>
        <td><input type='text' name='sales' value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'sales'))?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Operations", "Operations"); ?></th>
        <td><input type='text' name='operations' class="emails" value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'operations'))?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Accounting", "Accounting"); ?></th>
        <td><input type='text' name='accounting' value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'accounting'))?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Time Off Requests", "Time Off Requests"); ?></th>
        <td><input type='text' name='timeoff' value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'timeoff'))?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Reassigned barcode report send to", "Reassigned barcode report (send to)"); ?> </th>
        <td>
        <input type="text" name="send_reassigned_barcode_report_email" value="<?= get_business_meta($this->business_id, 'send_reassigned_barcode_report_email') ?>">
        </td>
    </tr>
<? if(DEV): ?>
    <tr>
        <th style='background:#ff5656; color:#fff'><?php echo __("Test Email", "Test Email"); ?></th>
        <td><input type='text' name='testEmail' value="<?= str_replace(',',', ',get_business_meta($business->businessID, 'testEmail'))?>" /></td>
    </tr>
<? endif; ?>
</table>
<div align="center" style="padding:10px;"><input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' /></div>
</form>

<h2><?php echo __("Special Phone Alert Items", "Special Phone Alert Items"); ?> <font size="-1"><?php echo __("The driver will be alerted when delivering these items", "(The driver will be alerted when delivering these items)"); ?></font></h2>
<br>
<table class='box-table-a'>
<form action='/admin/admin/add_alert_items' method='post'>
    <tr>
        <th colspan=2>
            <?= form_dropdown('product_id[]', $product_options); ?> <input type='submit' name='add_alert_submit' value="<?php echo __("Add Alert", "Add Alert"); ?>"  class='button orange'/>
        </th>
    </tr>
    </form>
    <?php foreach($alertItems as $alertItem): ?>
    <tr>
        <td><?php echo $alertItem->name?></td>
        <td width='100'><form action='/admin/admin/delete_alert_item' method='post'><input type='hidden' value='<?php echo $alertItem->alertItemID?>' name='alertItem_id'><input type='submit' value='<?php echo __("delete", "delete"); ?>' name='delete_alert_submit'></form></td>
    </tr>
    <?php endforeach; ?>
</table>
<br>
<h2><?php echo __("reCAPTCHA", "reCAPTCHA"); ?> <font size="-1">(<a href="https://www.google.com/recaptcha/admin" target="_blank">https://www.google.com/recaptcha/admin</a>)</font></h2>
<br>
<form action='/admin/admin/update_recaptcha_defaults' id="recaptcha_defaults" method='post'>
<table class="detail_table">
    <tr>
        <th><?php echo __("Enabled", "Enabled"); ?></th>
        <td>
            <?= form_yes_no("recaptcha_enabled", get_business_meta($business->businessID, 'recaptcha_enabled')); ?>
        </td>
    </tr>
    <tr>
        <th><?php echo __("Site Key", "Site Key"); ?></th>
        <td><input type='text' name='recaptcha_public_key' value="<?= get_business_meta($business->businessID, 'recaptcha_public_key')?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Secret Key", "Secret Key"); ?></th>
        <td><input type='text' name='recaptcha_private_key' value="<?= get_business_meta($business->businessID, 'recaptcha_private_key')?>" /></td>
    </tr>
</table>
<div align="center" style="padding:10px;"><input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' /></div>
</form>
<?php if (is_superadmin()): ?>
<br>
<h2><?php echo __("Droplocker monthly invoices", "Droplocker monthly invoices"); ?></h2>
<br>
<form action='/admin/admin/update_monthly_invoices_defaults' method='post'>
<table class="detail_table">
        <tr>
            <th><?php echo __("Location", "Location"); ?></th>
            <td><?= form_dropdown("droplocker_invoices_default_location", $locations_dropdown, get_business_meta($business->businessID, "droplocker_invoices_default_location")); ?></td>
        </tr>
        <tr>
            <th><?php echo __("Product", "Product"); ?></th>
            <td><?= form_dropdown("droplocker_invoices_default_product", $product_options, get_business_meta($business->businessID, "droplocker_invoices_default_product")); ?></td>
        </tr>
</table>
<div align="center" style="padding:10px;"><input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' /></div>
</form>
<?php endif; ?>