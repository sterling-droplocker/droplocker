<?php enter_translation_domain("admin/admin/search"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = 
    "<div class='input_container'>"
        . form_label(__('First Name', 'First Name'), 'firstName')
        . form_input(array('autocomplete' => 'off', 'name' => 'firstName', 'id' => 'firstName', 'value' => isset($_POST['firstName']) ? $_POST['firstName'] : ''))
    ."</div>";
            
$fieldList['lastName'] = 
    "<div class='input_container'>"
        . form_label(__('Last Name', 'Last Name'), 'lastName')
        . form_input(array('autocomplete' => 'off', 'name' => 'lastName', 'id' => 'lastName', 'value' => isset($_POST['lastName']) ? $_POST['lastName'] : ''))
    ."</div>";
            
$nameForm = getSortedNameForm($fieldList);

?>

<script type="text/javascript">
$(document).ready( function() {
    //The following statement validates the barcode search form by requiring the barcode field to be not empty and digits only.
    $("#barcode_search").validate(
    {
        rules: {
            barcode_id : {
                required: true,
                digits: true
            }
        },
        errorClass: "invalid",
        invalidHandler: function(form, validator)
        {
            $("#barcode_search").find(":submit").loading_indicator("destroy");
        }
    });
    $("#bag_search").validate(
    {
        rules: {
            bagNumber : {
                required: true,
                digits: true
            }
        },
        errorClass: "invalid",
        invalidHandler: function(form, validator)
        {
            $("#bag_search").find(":submit").loading_indicator("destroy");
        }
    });
    //The following statement validates the locker search form by requiring the locker id field to be not empty.
    $("#locker_search").validate(
    {
        rules: {
            lockerName : {
                required: true
            }
        },
        errorClass: "invalid",
        submitHandler: function(form) {
            form.submit();
        },
        invalidHandler: function(form, validator)
        {
            $(form).find("#locker_search").loading_indicator("destroy");
        }
    });

    $("#dueDate").datepicker({
            minDate: 0,
            dateFormat: 'yy-mm-dd'
    });

    $(":submit").loading_indicator({});
    
    <?php if (in_bizzie_mode()): ?>
        $(".delete").click(function() {
            if (confirm("<?php echo __("Are you sure you want to disable this customer", "Are you sure you want to disable this customer?"); ?>"))
            {
                return true;
            }
            else
            {
                return false;
            }        
        });           
    <?php else: ?>
        $(".delete").click(function() {
            if (confirm("<?php echo __("Are you sure you want to delete this customer", "Are you sure you want to delete this customer?"); ?>"))
            {
                return true;
            }
            else
            {
                return false;
            }        
        });   
    <?php endif ?>
    
});
</script>
<style type="text/css">
    label {
        display: block;
    }
    div.input_container {
        display: inline-block;
    }
    fieldset {
        border: 1px solid #000000;
    }
    fieldset legend {
        font-weight: bold;
    }
    div.search_panel form legend{
        padding: 5px;
    }
    div.search_panel form input {
        margin-bottom: 5px;
        display: inline;
    }
    div.search_panel form input.button {
        padding: 4px 3px;
        font-size: 9pt;
    }
    
    div.search_panel {
        padding: 10px;
        display: table-cell;
    }
    
    /* The following statements align the search form panels */.
    div.search_panel:nth-child(1) {
        width: 30%;
    }
    div.search_panel:nth-child(2) {
        width: 25%;
    }
    div.search_panel:nth-child(3) {
        width: 45%;
    }
   
</style>
<h2> Search </h2>

<div>
    <div class="search_panel">
        <!-- The following code creates the order search form -->
        <?= form_open("/admin/admin/search", array("id" => "order_search"), array("search_type" => "order")) ?>
            <fieldset>
                <legend> <?php echo __("Order Search", "Order Search"); ?> </legend>
                <div class="input_container">
                    <?= form_label(__("Bag ID", "Bag ID"), "bag_id") ?>
                    <?= form_input(array("id" => "bag_id", "name" => "bag_id", "value" => isset($_POST['bag_id']) ? $_POST['bag_id'] : '')) ?>
                </div> 

                <div class="input_container">
                    <?= form_label(__("Order ID", "Order ID"), "order_id") ?>
                    <?= form_input(array("id" => "order_id", "name" => "order_id", "value" => isset($_POST['order_id']) ? $_POST['order_id'] : '')) ?>
                </div>

                <div class="input_container">
                    <?= form_label(__("Customer Name", "Customer Name"), "customer_name") ?>
                    <?= form_input(array("autocomplete" => "off", "id" => "customer_name", "name" => "customer_name", "value" => isset($_POST['customer_name']) ? $_POST['customer_name'] : '')) ?>
                </div>

                <div class="input_container">
                    <?= form_label(__("Ticket number", "Ticket #"), "ticketNum") ?>
                    <?= form_input(array("id" => "ticketNum", "name" => "ticketNum", "value" => isset($_POST['ticketNum']) ? $_POST['ticketNum'] : '')) ?>
                </div>
                <div class="input_container">
                    <?= form_label(__("Due Date", "Due Date"), "dueDate") ?>
                    <?= form_input(array("id" => "dueDate", "name" => "dueDate", "value" => isset($_POST['dueDate']) ? $_POST['dueDate'] : '')) ?>
                </div>
                <div class="input_container">
                    <?= form_label(__("Brand", "Brand"), "brand") ?>
                    <?= form_input(array("id" => "brand", "name" => "brand", "value" => isset($_POST['brand']) ? $_POST['brand'] : '')) ?>
                </div>
                <?= form_submit(array("value" =>__("Search", "Search"), "class" => "button orange")); ?>
            </fieldset>
        <?= form_close() ?>
    </div>

    <div class="search_panel">
            <fieldset>
                <legend> <?php echo __("Misc Search", "Misc Search"); ?> </legend>
                <!-- The following code creates the barcode search form -->
                <?= form_open("/admin/admin/search", array("id" => "barcode_search"), array("search_type" => "barcode")) ?>
                <div class="input_container">
                    <?= form_label(__("Barcode", "Barcode"), "barcode") ?>
                    <?= form_input(array("name" => "barcode_id", "id" => "barcode", "value" => isset($_POST['barcode_id']) ? $_POST['barcode_id'] : '')) ?>
                <?= form_submit(array("value" =>__("Search", "Search"), "class" => "button orange")) ?>
                <?= form_close() ?>
                </div>
            
                <!-- This following code creates the locker search form. -->
                <?= form_open("/admin/admin/search", array("id" => "locker_search"), array("search_type" => "locker")) ?>
                <div class="input_container">
                    <?= form_label(__("Locker Name", "Locker Name"), "lockerName") ?>
                    <?= form_input(array("id" => "lockerName", "name" => "lockerName", "value" => isset($_POST['lockerName']) ? $_POST['lockerName'] : '')) ?>
                </div>
                <?= form_submit(array("value" =>__("Search", "Search"), "class" => "button orange")) ?>
                <?= form_close() ?>
         
                <?= form_open("/admin/admin/search", array("id" => "bag_search"), array("search_type" => "bag")) ?>
                <div class="input_container">
                    <?= form_label(__("Bag Number", "Bag Number"), "bagNumber") ?>
                    <?= form_input(array("autocomplete" => "off", "name" => "bagNumber", "value" => isset($_POST['bagNumber']) ? $_POST['bagNumber'] : "")) ?>
                </div>
                <?= form_submit(array("value" =>__("Search", "Search"), "class" => "button orange")) ?>
                <?= form_close() ?>
                <?= form_open("/admin/admin/view_all_locations", array("id" => "location"), array("search_type" => "location")) ?>
                <div class="input_container">
                    <?= form_label(__("Location", "Location"), "locationAddress") ?>
                    <?= form_input(array("autocomplete" => "off", "name" => "locationAddress", "value" => isset($_POST['locationAddress']) ? $_POST['locationAddress'] : "")) ?>
                </div>
                <?= form_submit(array("value" =>__("Search", "Search"), "class" => "button orange")) ?>
                <?= form_close() ?>   
            </fieldset>
        

    </div>
    <div class="search_panel">
        <!-- This following code creates the customer search form -->
        <?= form_open("/admin/admin/search", "", array("search_type" => "customer")) ?>
            <fieldset>
                <legend> <?php echo __("Customer Search", "Customer Search"); ?> </legend>
                
                <?= $nameForm ?>
                
                <div class="input_container">
                    <?= form_label(__('CustomerID', 'CustomerID'), 'customerID'); ?>
                    <?= form_input(array("autocomplete" => "off", 'name' => 'customerID', 'id' => 'customerID', "value" => isset($_POST['customerID']) ? $_POST['customerID'] : '')) ?>
                </div>

                <div class="input_container">
                    <?= form_label(__('Address', 'Address'), 'address') ?>
                    <?= form_input(array('id' => 'address', 'name' => 'address', "value" => isset($_POST['address']) ? $_POST['address'] : '')) ?>
                </div>

                <div class="input_container">
                    <?= form_label(__('PhoneSMS', 'Phone/SMS'), 'Phone'); ?>
                    <?= form_input(array('id' => 'phone', 'name' => 'phone', "value" => isset($_POST['phone']) ? $_POST['phone'] : '')) ?>
                </div> 
                <div class="input_container">
                    <?= form_label(__('Email', 'Email'), 'email') ?>
                    <?= form_input(array('id' => 'email', 'name' => 'email', "value" => isset($_POST['email']) ? $_POST['email'] : '')) ?>
                </div>
                <div class="input_container">
                    <?= form_label(__('Customer Notes', 'Customer Notes'), 'customerNotes') ?>
                    <?= form_input(array('id' => 'customerNotes', 'name' => 'customerNotes', "value" => isset($_POST['customerNotes']) ? $_POST['customerNotes'] : '')) ?>
                </div>
                    <?= form_submit(array("value" =>__("Search", "Search"), "class" => "button orange")) ?>
            </fieldset>
        <?= form_close() ?>
    </div>

</div>


<hr style="clear: both;" />
<?php
if (isset($results)):
?>
    <p> <?= count($results) . " " ?> <?php echo __("results returned.", "results returned."); ?></p>
    <table class="box-table-a">
        <thead>
            <tr>
                <?php 
                    $headers = array_keys($results[0]);
                    foreach ($headers as $header):
                ?>
                    <th> <?= $header ?> </th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $result): ?>
                <tr>
                    <?php foreach ($result as $result_item):
                        $style = preg_match('#images/icons#', $result_item) ? 'width:25px;text-align:center;': '';
                    ?>
                        <td style="<?=$style?>"> <?= $result_item ?> </td>
                    <?php endforeach ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php 
else:
?>
    <p> <?php echo __("No results found", "No results found"); ?> </p>
<?php
endif;
?>
