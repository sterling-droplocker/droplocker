<?php enter_translation_domain("admin/admin/processor"); ?>
<?php

$can_edit = !in_bizzie_mode() || is_superadmin();
$is_main_processor = $current_processor == $processor;

?>
<? if ($is_main_processor): ?>
<h2><?php echo __("Manage Payment Processors", "Manage Payment Processors"); ?></h2>

<? if ($can_edit): ?>
    <form action='' id='select_form' method='post'>
<? endif ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Payment Processor", "Payment Processor"); ?></th>
                <td>
                <? if ($can_edit): ?>
                    <?= form_dropdown('processor', array('' => __("--SELECT--", "--SELECT--")) + $processors_list, $current_processor, 'id="processor"'); ?>
                <? else: ?>
                    <?= $current_processor ?>
                <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Allow PayPal Express Checkout", "Allow PayPal Express Checkout"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <?= form_yes_no('paypal_button', get_business_meta($this->business_id, 'paypal_button')); ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_button') ? 'Yes' : 'No' ?>
                    <? endif ?>
                    <? if ($processor != 'paypalprocessor'): ?>
                    <p><?php echo __("Requires a PayPal account", "Requires a PayPal account."); ?> <a href="/admin/admin/processor/paypalprocessor" target="_blank"><img src="/images/icons/pencil.png"> <?php echo __("Edit PayPal settings", "Edit PayPal settings"); ?></a></p>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Allow Bitcoin Coinbase", "Allow Bitcoin (Coinbase)"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <?= form_yes_no('bitcoin_button', get_business_meta($this->business_id, 'bitcoin_button')); ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_button') ? 'Yes' : 'No' ?>
                    <? endif ?>
                    <p><?php echo __("Requires a Coinbase account", "Requires a Coinbase account."); ?> <a href="/admin/admin/processor/coinbase" target="_blank"><img src="/images/icons/pencil.png"> <?php echo __("Edit Coinbase settings", "Edit Coinbase settings"); ?></a></p>
                </td>
            </tr>
        </table>
 <? if ($can_edit): ?>
        <input class='button orange' type='submit' name='processor_submit' value="<?php echo __("Update", "Update"); ?>" />
    </form>
<? endif ?>
<br><br>
<? endif ?>

<? if (isset($processors_list[$processor])): ?>
<h2>Processor Settings for <?= $processors_list[$processor] ?></h2>
<? if (!$is_main_processor): ?>
<p>
    <a href="/admin/admin/processor"><img src="/images/icons/arrow_left.png"> <?php echo __("Go Back To Processors", "Go Back To Processors"); ?></a>
</p>
<? endif; ?>

<? if ($can_edit): ?>
    <form action='' method='post'>
<? endif ?>
   <? if($processor=='authorizenet'): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("API Login ID", "API Login ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='api_login_id' value='<?= get_business_meta($this->business_id, 'api_login_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->buisness_id, 'api_login_id') ?>
                    <? endif ?>

                </td>
            </tr>
            <tr>
                <th><?php echo __("Transaction", "Transaction"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='transaction_id' value='<?= get_business_meta($this->business_id, 'transaction_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'transaction_id') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Send Customer Extra Fields", "Send Customer Extra Fields"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <?= form_yes_no('authorizenet_extra', get_business_meta($this->business_id, 'authorizenet_extra')); ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'authorizenet_extra') ?__("Yes", "Yes") : __("No", "No") ?>
                    <? endif ?>
                    <p><?php echo __("Required only for European payment processors or AVS", "Required only for European payment processors or AVS."); ?></p>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Country", "Country"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='authorizenet_country' value='<?= get_business_meta($this->business_id, 'authorizenet_country');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'authorizenet_country') ?>
                    <? endif ?>
                    <p><?php echo __("Required only for European payment processors or AVS", "Required only for European payment processors or AVS."); ?></p>
                </td>
            </tr>
        </table>

   <? elseif($processor=='paypro'): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Test Token", "Test Token"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypro_test_token' value='<?= get_business_meta($this->business_id, 'paypro_test_token');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypro_test_token') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Live Token", "Live Token"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypro_live_token' value='<?= get_business_meta($this->business_id, 'paypro_live_token');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypro_live_token') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Mode", "Mode"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <? if(get_business_meta($this->business_id, 'paypro_mode') == 'test'): ?>
                            <input type='radio' name='paypro_mode' checked='checked' value='test' /> <?php echo __("Test", "Test"); ?>
                            <input type='radio' name='paypro_mode' value='live' /> <?php echo __("Live", "Live"); ?>
                        <? else: ?>
                            <input type='radio' name='paypro_mode'  value='test' /> <?php echo __("Test", "Test"); ?>
                            <input type='radio' name='paypro_mode' checked='checked' value='live' /> <?php echo __("Live", "Live"); ?>
                        <? endif; ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypro_mode') ?>
                    <? endif ?>
                </td>
            </tr>
        </table>

   <? elseif($processor=='verisign'): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Verisign User", "Verisign User"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='verisign_user' value='<?= get_business_meta($this->business_id, 'verisign_user');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'verisign_user') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Verisign Vendor", "Verisign Vendor"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='verisign_vendor' value='<?= get_business_meta($this->business_id, 'verisign_vendor');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'verisign_vendor') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Verisign Partner", "Verisign Partner"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='verisign_partner' value='<?= get_business_meta($this->business_id, 'verisign_partner');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'verisign_partner');?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Verisign Password", "Verisign Password"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='password' name='verisign_password' value='<?= get_business_meta($this->business_id, 'verisign_password');?>' />
                    <? else: ?>
                        <?= preg_replace('/./', '*', get_business_meta($this->business_id, 'verisign_password'));?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Verisign Country", "Verisign Country"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='verisign_country' value='<?= get_business_meta($this->business_id, 'verisign_country', 'US');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'verisign_country', 'US') ?>
                    <? endif ?>
                    <a href="https://developer.paypal.com/docs/classic/api/country_codes/" target="_blank"><?php echo __("Valid country codes", "Valid country codes"); ?></a>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Verisign Currency", "Verisign Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='verisign_currency' value='<?= get_business_meta($this->business_id, 'verisign_currency', 'USD');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'verisign_currency', 'USD');?>
                    <? endif ?>
                    <a href="https://developer.paypal.com/docs/classic/api/currency_codes/" target="_blank"><?php echo __("Valid currency codes", "Valid currency codes"); ?></a>
                </td>
            </tr>
        </table>
   <? elseif( $processor == 'paypalprocessor' ): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Paypal API User", "Paypal API User"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypal_user' value='<?= get_business_meta($this->business_id, 'paypal_user');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_user') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paypal API Password", "Paypal API Password"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypal_password' value='<?= get_business_meta($this->business_id, 'paypal_password');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_password');?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paypal Signature", "Paypal Signature"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypal_signature' value='<?= get_business_meta($this->business_id, 'paypal_signature');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_signature') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paypal Country", "Paypal Country"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypal_country' value='<?= get_business_meta($this->business_id, 'paypal_country', 'US');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_country', 'US') ?>
                    <? endif ?>
                    <a href="https://developer.paypal.com/docs/classic/api/country_codes/" target="_blank"><?php echo __("Valid country codes", "Valid country codes"); ?></a>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paypal Currency", "Paypal Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paypal_currency' value='<?= get_business_meta($this->business_id, 'paypal_currency', 'USD');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paypal_currency', 'USD') ?>
                    <? endif ?>
                    <a href="https://developer.paypal.com/docs/classic/api/currency_codes/" target="_blank"><?php echo __("Valid currency codes", "Valid currency codes"); ?></a>
                    <p class="important"><?php echo __("This should match the main businesss processor currency", "This should match the main business's processor currency."); ?></p>
                </td>
            </tr>
        </table>

  

   <? elseif( $processor == 'payboxdirect' ): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("PayBox Site", "PayBox Site"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paybox_site' value='<?= get_business_meta($this->business_id, 'paybox_site');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paybox_site') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("PayBox Rang", "PayBox Rang"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paybox_rang' value='<?= get_business_meta($this->business_id, 'paybox_rang');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paybox_rang');?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("PayBox Password", "PayBox Password"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paybox_cle' value='<?= get_business_meta($this->business_id, 'paybox_cle');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paybox_cle');?>
                    <? endif ?>
                </td>
            </tr>
        </table>
        <input type="hidden" name="paybox_is_plus" value="1">

   <? elseif( $processor == 'coinbase' ): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Bitcoin Address", "Bitcoin Address"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='coinbase_address' value='<?= get_business_meta($this->business_id, 'coinbase_address');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'coinbase_address') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Coinbase Client ID", "Coinbase Client ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='coinbase_client' value='<?= get_business_meta($this->business_id, 'coinbase_client');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'coinbase_client') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Coinbase Secret", "Coinbase Secret"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='coinbase_secret' value='<?= get_business_meta($this->business_id, 'coinbase_secret');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'coinbase_secret') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Coinbase Currency", "Coinbase Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='coinbase_currency' value='<?= get_business_meta($this->business_id, 'coinbase_currency', 'USD');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'coinbase_currency', 'USD') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Coinbase Monthly Limit", "Coinbase Monthly Limit"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='coinbase_send_limit_amount' value='<?= get_business_meta($this->business_id, 'coinbase_send_limit_amount', 300);?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'coinbase_send_limit_amount', 300) ?>
                        (0 disables this limit)
                    <? endif ?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'conekta'): ?>
        <table class='detail_table'>      
            <tr>
                <th><?php echo __("Private Key", "Private Key"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='conekta_private_key' value='<?= get_business_meta($this->business_id, 'conekta_private_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_private_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Public Key", "Public Key"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='conekta_public_key' value='<?= get_business_meta($this->business_id, 'conekta_public_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_public_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Private Sandbox Key", "Private Sandbox Key"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='conekta_sandbox_private_key' value='<?= get_business_meta($this->business_id, 'conekta_sandbox_private_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_sandbox_private_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Public Sandbox Key", "Public Sandbox Key"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='conekta_sandbox_public_key' value='<?= get_business_meta($this->business_id, 'conekta_sandbox_public_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_sandbox_public_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Conekta Currency", "Conekta Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='conekta_currency' value='<?= get_business_meta($this->business_id, 'conekta_currency', 'USD');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_currency', 'USD') ?>
                    <? endif ?>
                    <a href="http://assemblysys.com/iso-4217-currency-codes/" target="_blank"><?php echo __("Valid currency codes", "Valid currency codes"); ?></a>
                    <p class="important"><?php echo __("This should match the main business's processor currency", "This should match the main business's processor currency."); ?></p>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Mode", "Mode"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <? if(get_business_meta($this->business_id, 'conekta_mode') == 'test'): ?>
                        <input type='radio' name='conekta_mode' id="test_radio" checked='checked' value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='conekta_mode' id="live_radio" value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <? else: ?>
                            <input type='radio' name='conekta_mode' id="test_radio" value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='conekta_mode' id="live_radio" checked='checked' value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <? endif; ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_mode') ?>
                    <? endif ?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'paymentexpress'): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("PaymentExpress Username", "PaymentExpress Username"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='pe_username' value='<?= get_business_meta($this->business_id, 'pe_username');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'pe_username') ?>

                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("PaymentExpress Password", "PaymentExpress Password"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='pe_password' value='<?= get_business_meta($this->business_id, 'pe_password');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'pe_password') ?>

                    <? endif ?>
                </td>
            </tr>
            <tr>

                <th><?php echo __("PaymentExpress Currency", "PaymentExpress Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='pe_currency' value='<?= get_business_meta($this->business_id, 'pe_currency', 'NZD');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'pe_currency', 'NZD') ?>
                    <? endif ?>
                    <a href="https://www.paymentexpress.com/technical_resources/ecommerce_nonhosted/pxpost.html#InputCurrency" target="_blank"><?php echo __("Valid currency codes", "Valid currency codes"); ?></a>
                
            </tr>
            <tr>
                <th><?php echo __("Mode", "Mode"); ?></th>
                <td>
                    <?php if ($can_edit): ?>

                        <?php if(get_business_meta($this->business_id, 'pe_mode') == 'test'): ?>
                        <input type='radio' name='pe_mode' id="test_radio" checked='checked' value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='pe_mode' id="live_radio" value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <?php else: ?>
                            <input type='radio' name='pe_mode' id="test_radio" value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='pe_mode' id="live_radio" checked='checked' value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <?php endif; ?>
                    <?php else: ?>
                        <?= get_business_meta($this->business_id, 'pe_mode') ?>
                    <?php endif;?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'transbank'): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Private Key Path", "Private Key Path"); ?></th>
                <td>
                    <?php if ($can_edit): ?>
                        <input type='text' name='tb_private_key' value='<?= get_business_meta($this->business_id, 'tb_private_key');?>' />
                    <?php else: ?>
                        <?= get_business_meta($this->business_id, 'conekta_private_key') ?>
                    <?php endif;?>
            </tr>
            <tr>
                <th><?php echo __("Certificate Server Path", "Certificate Server Path"); ?></th>
                <td>
                    <?php if ($can_edit): ?>
                        <input type='text' name='tb_certificate_server' value='<?= get_business_meta($this->business_id, 'tb_certificate_server');?>' />
                    <?php else: ?>
                        <?= get_business_meta($this->business_id, 'tb_certificate_server') ?>
                    <?php endif;?>
            </tr>
            <tr>
                <th><?php echo __("Transbank Currency", "Transbank Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='tb_currency' value='<?= get_business_meta($this->business_id, 'tb_currency', 'CLP');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'tb_currency', 'CLP') ?>
                    <? endif; ?>
                    <p class="important"><?php echo __("This should match the main business's processor currency", "This should match the main business's processor currency."); ?></p>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Mode", "Mode"); ?></th>
                <td>
                    <?php if ($can_edit): ?>

                        <?php if(get_business_meta($this->business_id, 'tb_mode') == 'test'): ?>
                        <input type='radio' name='tb_mode' id="test_radio" checked='checked' value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='tb_mode' id="live_radio" value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <?php else: ?>
                            <input type='radio' name='tb_mode' id="test_radio" value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='tb_mode' id="live_radio" checked='checked' value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <?php endif; ?>
                    <?php else: ?>
                        <?= get_business_meta($this->business_id, 'tb_mode') ?>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'cybersource' || $processor == 'cybersource-sa'): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("CyberSource Merchant ID", "CyberSource Merchant ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='cybersource_merchantID' value='<?= get_business_meta($this->business_id, 'cybersource_merchantID');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_merchantID') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Transaction Key SOAP", "CyberSource Transaction Key (SOAP)"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <textarea name='cybersource_transactionKey' rows="4" cols="80"><?= get_business_meta($this->business_id, 'cybersource_transactionKey');?></textarea>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_transactionKey') ?>
                    <? endif ?>
                </td>
            </tr>
            <? if ($processor == 'cybersource-sa'): ?>
            <tr>
                <th><?php echo __("CyberSource Profile ID", "CyberSource Profile ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input name='cybersource_profileID' type="text" value="<?= get_business_meta($this->business_id, 'cybersource_profileID');?>">
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_profileID') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Access Key", "CyberSource Access Key"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input name='cybersource_accessKey' type="text" value="<?= get_business_meta($this->business_id, 'cybersource_accessKey');?>">
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_accessKey') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Secret Key", "CyberSource Secret Key"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <textarea name='cybersource_secretKey' rows="4" cols="80"><?= get_business_meta($this->business_id, 'cybersource_secretKey');?></textarea>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_secretKey') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Locale", "CyberSource Locale"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='cybersource_locale' value='<?= get_business_meta($this->business_id, 'cybersource_locale', 'en-us');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_locale', 'en-us') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Terminal ID", "CyberSource Terminal ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input name='cybersource_terminalID' type="text" value="<?= get_business_meta($this->business_id, 'cybersource_terminalID');?>">
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_terminalID') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Merchant Name", "CyberSource Merchant Name"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input name='cybersource_merchantName' type="text" value="<?= get_business_meta($this->business_id, 'cybersource_merchantName');?>">
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_merchantName') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Decision Manager Profile ID", "Decision Manager Profile ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input name='cybersource_DMprofileID' type="text" value="<?= get_business_meta($this->business_id, 'cybersource_DMprofileID');?>">
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_DMprofileID') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Decision Manager Organization ID", "Decision Manager Organization ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input name='cybersource_orgID' type="text" value="<?= get_business_meta($this->business_id, 'cybersource_orgID');?>">
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_orgID') ?>
                    <? endif ?>
                </td>
            </tr>
            <? endif; ?>
            <tr>
                <th><?php echo __("CyberSource Currency", "CyberSource Currency"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='cybersource_currency' value='<?= get_business_meta($this->business_id, 'cybersource_currency', 'USD');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_currency', 'USD') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("CyberSource Country", "CyberSource Country"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='cybersource_country' value='<?= get_business_meta($this->business_id, 'cybersource_country', 'US');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_country', 'US') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Mode", "Mode"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <? if(get_business_meta($this->business_id, 'cybersource_mode') == 'test'): ?>
                            <input type='radio' name='cybersource_mode' checked='checked' value='test' /> <?php echo __("Test", "Test"); ?>
                            <input type='radio' name='cybersource_mode' value='live' /> <?php echo __("Live", "Live"); ?>
                        <? else: ?>
                            <input type='radio' name='cybersource_mode'  value='test' /> <?php echo __("Test", "Test"); ?>
                            <input type='radio' name='cybersource_mode' checked='checked' value='live' /> <?php echo __("Live", "Live"); ?>
                        <? endif; ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_mode') ?>
                    <? endif ?>
                </td>
            </tr>
            <? if ($processor == 'cybersource-sa'): ?>
            <tr>
                <th><?php echo __("Secure AcceptanceType", "Secure AcceptanceType"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <? if(get_business_meta($this->business_id, 'cybersource_silent', 0)): ?>
                            <input type='radio' name='cybersource_silent' value='0' /> Web/Mobile
                            <input type='radio' name='cybersource_silent' value='1' checked='checked' /> <?php echo __("Silent Post", "Silent Post"); ?>
                        <? else: ?>
                            <input type='radio' name='cybersource_silent' value='0' checked='checked' /> <?php echo __("Web Mobile", "Web/Mobile"); ?>
                            <input type='radio' name='cybersource_silent' value='1' /> <?php echo __("Silent Post", "Silent Post"); ?>
                        <? endif; ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'cybersource_silent') ?__("Silent", "Silent"):__("Web Mobile", "Web/Mobile") ?>
                    <? endif ?>
                </td>
            </tr>
            <? endif; ?>

        </table>
   <? elseif( $processor == 'paygate' ): ?>
        <table class='detail_table'>
            <tr>
                <th><?php echo __("Paygate ID", "Paygate ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paygate_id' value='<?= get_business_meta($this->business_id, 'paygate_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paygate_id') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paygate Secret", "Paygate Secret"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paygate_secret' value='<?= get_business_meta($this->business_id, 'paygate_secret');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paygate_secret') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paygate sandbox ID", "Paygate sandbox ID"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paygate_sandbox_id' value='<?= get_business_meta($this->business_id, 'paygate_sandbox_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paygate_sandbox_id') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Paygate sandbox Secret", "Paygate sandbox Secret"); ?></th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='paygate_sandbox_secret' value='<?= get_business_meta($this->business_id, 'paygate_sandbox_secret');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'paygate_sandbox_secret') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Mode", "Mode"); ?></th>
                <td>
                    <?php if ($can_edit): ?>

                        <?php if(get_business_meta($this->business_id, 'paygate_mode') == 'test'): ?>
                        <input type='radio' name='paygate_mode' id="test_radio" checked='checked' value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='paygate_mode' id="live_radio" value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <?php else: ?>
                            <input type='radio' name='paygate_mode' id="test_radio" value='test' /> <label for="test_radio"><?php echo __("Test", "Test"); ?></label>
                            <input type='radio' name='paygate_mode' id="live_radio" checked='checked' value='live' /> <label for="live_radio"><?php echo __("Live", "Live"); ?></label>
                        <?php endif; ?>
                    <?php else: ?>
                        <?= get_business_meta($this->business_id, 'paygate_mode') ?>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'braintree'): ?>
        <table class='detail_table'> 
            <tr>
                <th>Merchant ID</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_merchant_id' value='<?= get_business_meta($this->business_id, 'braintree_merchant_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_merchant_id') ?>
                    <? endif ?>
                </td>
            </tr>     
            <tr>
                <th>Private Key</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_private_key' value='<?= get_business_meta($this->business_id, 'braintree_private_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_private_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th>Public Key</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_public_key' value='<?= get_business_meta($this->business_id, 'braintree_public_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_public_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th>Sandbox Merchant ID</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_sandbox_merchant_id' value='<?= get_business_meta($this->business_id, 'braintree_sandbox_merchant_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_sandbox_merchant_id') ?>
                    <? endif ?>
                </td>
            </tr> 
            <tr>
                <th>Private Sandbox Key</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_sandbox_private_key' value='<?= get_business_meta($this->business_id, 'braintree_sandbox_private_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_sandbox_private_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th>Public Sandbox Key</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_sandbox_public_key' value='<?= get_business_meta($this->business_id, 'braintree_sandbox_public_key');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_sandbox_public_key') ?>
                    <? endif ?>
                </td>
            </tr>
            <tr>
                <th>Currency</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='braintree_currency' value='<?= get_business_meta($this->business_id, 'braintree_currency', 'USD');?>' style="width: 100px;" />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_currency', 'USD') ?>
                    <? endif ?>
                    <a href="http://assemblysys.com/iso-4217-currency-codes/" target="_blank">Valid currency codes</a>
                    <p class="important">This should match the main business's processor currency.</p>
                </td>
            </tr>
            <tr>
                <th>Mode</th>
                <td>
                    <? if ($can_edit): ?>
                        <? if(get_business_meta($this->business_id, 'braintree_mode') == 'test'): ?>
                        <input type='radio' name='braintree_mode' id="test_radio" checked='checked' value='test' /> <label for="test_radio">Test</label>
                            <input type='radio' name='braintree_mode' id="live_radio" value='live' /> <label for="live_radio">Live</label>
                        <? else: ?>
                            <input type='radio' name='braintree_mode' id="test_radio" value='test' /> <label for="test_radio">Test</label>
                            <input type='radio' name='braintree_mode' id="live_radio" checked='checked' value='live' /> <label for="live_radio">Live</label>
                        <? endif; ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'braintree_mode') ?>
                    <? endif ?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'sonypayment'): ?>
        <table class='detail_table'> 
            <tr>
                <th>Merchant ID</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='sonypayment_merchant_id' value='<?= get_business_meta($this->business_id, 'sonypayment_merchant_id');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'sonypayment_merchant_id') ?>
                    <? endif ?>
                </td>
            </tr>    
            <tr>
                <th>Merchant Pass</th>
                <td>
                    <? if ($can_edit): ?>
                        <input type='text' name='sonypayment_merchant_pass' value='<?= get_business_meta($this->business_id, 'sonypayment_merchant_pass');?>' />
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'sonypayment_merchant_pass') ?>
                    <? endif ?>
                </td>
            </tr>  
            <tr>
                <th>Mode</th>
                <td>
                    <? if ($can_edit): ?>
                        <? if(get_business_meta($this->business_id, 'sonypayment_mode') == 'test'): ?>
                        <input type='radio' name='sonypayment_mode' id="test_radio" checked='checked' value='test' /> <label for="test_radio">Test</label>
                            <input type='radio' name='sonypayment_mode' id="live_radio" value='live' /> <label for="live_radio">Live</label>
                        <? else: ?>
                            <input type='radio' name='sonypayment_mode' id="test_radio" value='test' /> <label for="test_radio">Test</label>
                            <input type='radio' name='sonypayment_mode' id="live_radio" checked='checked' value='live' /> <label for="live_radio">Live</label>
                        <? endif; ?>
                    <? else: ?>
                        <?= get_business_meta($this->business_id, 'sonypayment_mode') ?>
                    <? endif ?>
                </td>
            </tr>
        </table>
   <? elseif ($processor == 'testprocessor'): ?>
        <p class="alert-warning"><?php echo __("Settings are not needed for test processor", "Settings are not needed for test processor"); ?> <b><?= $processor ?></b></p>
   <? else: ?>

    <p class="alert-error"><?php echo __("Invalid processor", "Invalid processor"); ?> <b><?= $processor ?></b></p>
   <? endif; ?>


<? if ($can_edit): ?>
        <input class='button orange' type='submit' name='submit' value="<?php echo __("Update", "Update"); ?>" />
    </form>
<? endif ?>
<? endif ?>

<? if ($is_main_processor): ?>
<div style="padding-top:30px;">
    <h3><?php echo __("Accepted Cards", "Accepted Cards"); ?></h3>
    <form action="/admin/admin/accepted_cards" method="post">
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Select cards", "Select card(s):"); ?></th>
            <td>
                <label><input type="checkbox" name="cards[]" value="visa" <?= (in_array('visa', $accepted_cards)) ? 'checked' : '' ?> /> <?php echo __("VISA", "VISA"); ?> </label><br />
                <label><input type="checkbox" name="cards[]" value="mastercard" <?= (in_array('mastercard', $accepted_cards)) ? 'checked' : '' ?>  /> <?php echo __("MasterCard", "MasterCard"); ?> </label><br />
                <label><input type="checkbox" name="cards[]" value="americanexpress" <?= (in_array('americanexpress', $accepted_cards)) ? 'checked' : '' ?>  /> <?php echo __("AmericanExpress", "AmericanExpress"); ?> </label><br />
                <label><input type="checkbox" name="cards[]" value="discover" <?= (in_array('discover', $accepted_cards)) ? 'checked' : '' ?>  /> <?php echo __("Discover", "Discover"); ?> </label><br />
            </td>
        </tr>
    </table>
        <button type="submit" class="button orange"> <?php echo __("Update", "Update"); ?> </button>
    </form>
</div>
<? endif ?>
