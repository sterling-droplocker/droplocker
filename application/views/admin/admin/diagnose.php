<?php enter_translation_domain("admin/admin/diagnose"); ?>
<style>

    tr {
        font-size: 14px;
        margin: 2em;
    }

    td {
        height: 2em;
        border-bottom: 1px solid #ccc;
    }

    td.diagnose-success {
        color: #6D9F00;
    }

    td.diagnose-error {
        color: #cc0000;
    }
</style>
<div id="main_panel">
    <h2><?php echo __("Business Diagnostic", "Business Diagnostic"); ?></h2>
    <br/>
    <table class="dataTable stripe" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th><?php echo __("Description", "Description"); ?></th>
            <th><?php echo __("Status", "Status"); ?></th>
            <th><?php echo __("Fix it", "Fix it"); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php if (count($diagnose)): ?>      
        <?php foreach ($diagnose as $d): ?>
            <tr>
                    <td>
                        <?=$d['message'];?>
                    </td>
                    <td class="diagnose-<?=$d['status'];?>">
                        <?php echo ($d['status']=="success"?"Ok":"Fail"); ?>
                    </td>
                    <td>                    
                        <?php if (isset($d['solution'])): ?>
                            <a href="<?=$d['solution']['url'];?>" target="_blank"><?=$d['solution']['message'];?></a>
                        <?php endif;?>
                    </td>
            </tr>
        <?php endforeach;?>

    <?php else:?>
        <td colspan="3"><?php echo __("No issues found", "No issues found"); ?></td>
    <?php endif;?>
    </tbody>
    <tfoot></tfoot>
    </table>
</div>