<?php enter_translation_domain("admin/admin/locker_loot_terms"); ?>

<script>
	window.onload = function()
	{

		CKEDITOR.replace( 'body', {
             uiColor : false,
             toolbar : 'MyToolbar'
		});
	};
</script>

<h2><?php echo __("Locker Loot Terms", "Locker Loot Terms"); ?></h2>

<form action='' method='post'>
<table class='detail_table'>
<tr>
    <td>
        <textarea id='body' name='body'><?= $terms?></textarea>
    </td>
</tr>
</table>
<input type='hidden' name='id' value='<?= $id?>'/>
<input type='submit' value='<?php echo __("Update", "Update"); ?>' name='submit' class='button orange' />
</form>
