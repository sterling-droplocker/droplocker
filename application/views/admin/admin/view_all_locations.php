<?php enter_translation_domain("admin/admin/view_all_locations"); ?>
<?php
/**
 * Expects the following content variables:
 * 
 *  locations array
 *  pickup_location_serviceDays array
 *  dropoff_location_serviceDays array
 */

if (!is_array($locations)) {
    trigger_error(__("locations must be an array", "'locations' must be an array"), E_USER_WARNING);
}
if (!is_array($pickup_location_serviceDays)) {
    trigger_error(__("pickup_location_serviceDays must be an array", "'pickup_location_serviceDays' must be an array"), E_USER_WARNING);
}
if (!is_array($dropoff_location_serviceDays)) {
    trigger_error(__("dropoff_location_serviceDays must be an array", "'dropoff_location_serviceDays' must be an array"), E_USER_WARNING);
}
?>
<?php

$fieldList = array();
$fieldList['address1'] = "<th> ". __("Address", "Address") . "</th>";
$fieldList['city'] = "<th> ". __("City", "City") . "</th>";
$fieldList['state'] = "<th align='center'>" . __("State", "State") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['address1'] = "<td>". ucfirst($location->address) . ' ' .ucfirst($location->address2)."</td>";
    $fieldList['city'] = "<td>". ucfirst($location->city)."</td>";
    $fieldList['state'] = "<td align='center' > ". $location->state." </td>";

    return getSortedAddressForm($fieldList);
}
        
?>
<script type="text/javascript">
$(document).ready(function() {
    location_dataTable_instance = $("#view_location_table").dataTable({
        "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
        "bPaginate" : false,
        "bJQueryUI": true
    });
    new FixedHeader(location_dataTable_instance);
    
    $(".serviceDay").change(function() {
        var enable = 0;
        if ($(this).is(":checked")) {
            enable = 1;
        }
        else {
            enable = 0;
        }
        var $image = $("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $image.insertAfter($(this));
        
        $.post("/admin/locations/update_serviceDay", 
            {
                "locationID" : $(this).attr("data-locationID"),
                "day" : $(this).attr("data-day"),
                "location_serviceTypeID" : $(this).attr("data-location_serviceTypeID"),
                "enable" : enable
            }, 
            function(result) { 
                $image.remove(); 
        });
    });

});
</script>
<? if (!$show_deleted): ?>
    <h2>View Locations</h2>
    <p><a href="/admin/admin/view_all_locations/1"><img src="/images/icons/cross.png"> <?php echo __("Show Deleted Locations", "Show Deleted Locations"); ?></a></p>
<? else: ?>
    <h2>Deleted Locations</h2>
    <p><a href="/admin/admin/view_all_locations/0"><img src="/images/icons/arrow_left.png"> <?php echo __("Go Back to View Locations", "Go Back to View Locations"); ?></a></p>
<? endif; ?>

<table id='view_location_table'>

<?php
use App\Libraries\DroplockerObjects\Location_PickupDay;
?>
    <thead>
        <tr>
            <th><?php echo __("Service Days", "Service Days"); ?></th>
            <th><?php echo __("ID", "ID"); ?></th>
            <th><?php echo __("Route", "Route"); ?></th>
            <th><?php echo __("Title", "Title"); ?></th>
            <?= $addressTableHeader ?>
            <th> <?php echo __("Latitude", "Latitude"); ?> </th>
            <th> <?php echo __("Longitude", "Longitude"); ?> </th>
            <th align="left"><?php echo __("Location Type", "Location Type"); ?></th>
            <th align="left"><?php echo __("Lockers Type", "Lockers Type"); ?></th>
            <th align="center" width='25'><?php echo __("Edit", "Edit"); ?></th>
            <th align="center" width='25'><?php echo __("Details", "Details"); ?></th>
            <th align="center" width='25'><?php echo __("Delete", "Delete"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php if($locations): ?>
            <?php foreach($locations as $location): ?> <?php
                echo "<tr>";
                    echo "<td>";
                        echo "<table>";
                            echo "<thead>";
                                echo "<tr>";
                                    echo "<th>" . __("Day", "Day") . "</th>";
                                    echo "<th>" . __("Pick", "Pick.") . "</th>";
                                    echo "<th>" . __("Dlv", "Dlv.") . "</th>";
                                echo "</tr>";
                            echo "</thead>";
                            echo "<tbody>";
                            $days = array(1 => "Monday", 2 => "Tuesday", 3 => "Wednesday", 4 => "Thursday", 5 => "Friday", 6 => "Saturday", 7 => "Sunday");
                            //The following statements display the service days.
                                foreach ($days as $index => $day) {
                                    echo "<tr>";
                                        echo "<td>" . form_label($day) .  "</td>";
                                        //This is the pickup day column 
                                        echo "<td>";
                                                if (array_key_exists($location->locationID, $pickup_location_serviceDays) && in_array($index, $pickup_location_serviceDays[$location->locationID])) {
                                                    $pickup_checked = TRUE;
                                                }
                                                else {
                                                    $pickup_checked = FALSE;
                                                }
                                                echo form_checkbox(array("class" => "serviceDay", "data-day" => $index, "data-location_serviceTypeID" => 2, "data-locationID" => $location->locationID, "checked" => $pickup_checked), false);
                                        echo "</td>";
                                        //This is the delivery day column -->
                                        echo "<td>";
                                                if (array_key_exists($location->locationID, $dropoff_location_serviceDays) && in_array($index, $dropoff_location_serviceDays[$location->locationID])) {
                                                    $delivery_checked = TRUE;
                                                }
                                                else {
                                                    $delivery_checked = FALSE;
                                                }
                                                echo form_checkbox(array("class" => "serviceDay", "data-day" => $index, "data-location_serviceTypeID" => 1, "data-locationID" => $location->locationID, "checked" => $delivery_checked), false);
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            echo "</tbody>";
                        echo "</table>";
                    echo "</td>";

                    echo "<td>" . $location->locationID . "</td>";
                    echo "<td>" . $location->route_id  . "</td>";
                    echo "<td>" . $location->companyName  . "</td>";
                    echo getAddressRow($location);
                    // Latitude Column
                    echo "<td>" . $location->lat . "</td>";
                    // Longitude Column
                    echo "<td>" . $location->lon  . "</td>";
                    // This is the Location Type column
                    echo "<td align=\"left\">" . $location->name . "</td>";
                    // This is the Lockers Type column
                    echo "<td align=\"left\"><a id=\"type-" . $location->locationID . "\" href='/admin/locations/update_lockers/" . $location->locationID . "'>" . $location->type  . "</a></td>";
                    echo "<td align=\"center\"><a class=\"edit\" href=\"/admin/locations/display_update_form/" . $location->locationID . "\"><img alt=\"Edit Location\" src=\"/images/icons/application_edit.png\"/></a></td>";
                    echo "<td align=\"center\"><a class=\"edit\" href=\"/admin/admin/location_details/" . $location->locationID . "\"><img alt=\"Location Details\" src=\"/images/icons/magnifier.png\"/></a></td>";
                    echo "<td align=\"center\">";
                        if ($location->status == 'active') {
                          echo "<form method='post' onSubmit=\"return verify_delete();\" action=\"/admin/locations/delete/\"><input type=\"image\" id=\"delete-" . $location->locationID  . "\" src=\"/images/icons/cross.png\" /><input type=\"hidden\" value=\"" . $location->locationID . "\" name=\"locationID\" /></form>";
                        } else {
                          echo "<form method='post' onSubmit=\"return verify_delete();\" action=\"/admin/locations/undelete/\"><input type=\"image\" id=\"undelete-" . $location->locationID  . "\" src=\"/images/icons/arrow_undo.png\" /><input type=\"hidden\" value=\"" . $location->locationID . "\" name=\"locationID\" /></form>";
                        }
                    echo "</td>";
                echo "</tr>";
            ?><? endforeach; ?> 
        <?php endif;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan='10'><?php echo __("Pagination will go here", "Pagination will go here"); ?></td>
        </tr>
    </tfoot>
</table>
