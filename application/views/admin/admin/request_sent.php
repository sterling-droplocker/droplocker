<?php enter_translation_domain("admin/admin/request_sent"); ?>
<h2><?php echo __("Access Denied", "Access Denied"); ?></h2>

<p>
<?php echo __("Thank you", "Thank you, <br>
Your permission request has been sent."); ?>
</p>
