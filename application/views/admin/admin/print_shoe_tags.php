<?php enter_translation_domain("admin/admin/print_shoe_tags"); ?>
<?php

$full_base = get_full_base_url();

$barcode = $_GET['barcode'];
$barcodeEnd = $_GET['barcodeEnd'];


?>
<html>
<title><?php echo __("Print Show Tags", "Print Show Tags"); ?></title>
<body>

<? for ($i = $barcode; $i <= $barcodeEnd; $i++): ?>
<div style="clear:both; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
    <table>
        <tr>
            <td><img src="<?= $full_base ?>/barcode.php?barcode=<?= $i ?>&height=55"></td>
            <td width="30">&nbsp;</td>
            <td><img src="<?= $full_base ?>/barcode.php?barcode=<?= $i ?>&height=55"></td>
        </tr>
    </table>
</div>
<? endfor ?>

</body>
</html>


