<?php

enter_translation_domain("admin/admin/location_pricing");

?>
<script type="text/javascript">
    var listPrices = <?= $jsonProducts ?>;
    $(document).ready(function()
    {
        if($('#location').length!=0){
                $('#location').autocomplete({
                source: function(req, add){

                //pass request to server
                $.getJSON("/ajax/location_autocomplete/", req, function(data) {
                                        dc=data;
                    //create array for response objects
                    var suggestions = [];

                    //process response
                    $.each(data, function(i, val){
                        suggestions.push(data[i]);

                        });

                //pass array to callback
                add(suggestions)
           })
          },
                appendTo: "#results",
                select : function(e, ui){
                        $.each(dc, function(i, val){
                if(ui.item.value == val)
                    $('#location_id').val(i);
            });
                }
                });
        }

        $('#product').change(function(){
                var product_id = $(this).val();
                $('#img_holder').html('<img src="/images/progress.gif" />');
                $.ajax({
                        url:"/admin/orders/get_product_processes_ajax",
                        type:'POST',
                        dataType:'JSON',
                        data:{submit:true,product_id:product_id},
                        success:function(data){
                                if(data.length){
                                    $("#process_id").empty();
                                    $.each(data, function(i, item){
                                            var name = (item.process_id == 0)?"N/A":item.name;
                                            var price = (item.price !='')?"($"+item.price+")":"";
                                            var opt = $('<option value="'+item.product_processID+'">'+name+' '+price+'</option>');
                                            $('#process_id').append(opt);
                                    });
                                }
                                $('#img_holder').html('');
                        }
                });
        });

        $('#location_discount_expiration').datepicker( );
    });

</script>

<h2><?=__("Per Location Pricing", "Per Location Pricing")?></h2>
<?= $this->session->flashdata("message") ?>
<div style="padding: 8px;">
    <form action="" method="post">
        <?=__("Enter Location:", "Enter Location:")?>
        <input type="hidden" name="location_id" id="location_id" value="<?= isset($location[0]->locationID) ? $location[0]->locationID : "" ?>">
        <input type="text" name="location" id="location" value="<?= isset($location[0]->address) ? $location[0]->address: "" ?>">
        <input type="submit" name="submit" value="View Location Pricings" class="button blue">
        <div id="results"></div>
    </form>
</div>
<br>
<br>
<? if(isset($products)): ?>
<table class="box-table-a">
    <thead>
        <th><?=__("Product", "Product")?></th>
        <th><?=__("Process", "Process")?></th>
        <th><?=__("List Price", "List Price")?></th>
        <th><?=__("Location Price", "Location Price")?></th>
        <th><?=__("Last Updated", "Last Updated")?></th>
        <?php if (!in_bizzie_mode() || is_superadmin()): ?>
            <th><?=__("Delete?", "Delete?")?></th>
        <?php endif ?>
    </thead>
    <tbody>
        <?php if (!in_bizzie_mode() || is_superadmin()): ?>
            <form action="/admin/admin/add_product_location_price" method="post">
                <input type="hidden" name="location" value="<?= isset($location[0]->locationID) ? $location[0]->locationID: "" ?>">
                <tr>
                    <td colspan=2>
                        <select name="product_processID" id='product_processID'>
                            <option value=''><?=__("-- SELECT --", "-- SELECT --")?></option>
                            <?php foreach($products as $p): ?>
                                <option value="<?= $p->product_processID ?>"><?= $p->prodName ?> - <?= $p->procName ?></option>
                            <? endforeach; ?>
                        </select>
                    </td>
                    <td></td>
                    <td><input type="text" name="price"></td>
                    <td colspan=2><input type="submit" name="add" value="<?=__("Add", "Add")?>" class="button blue"></td>
                </tr>
            </form>
        <?php endif ?>
        <? foreach($pricing as $s):
        $process_name = ($s->process_name!='')?$s->process_name:__("N/A", "N/A");
        ?>
        <tr>
            <td><?= $s->name ?></td>
            <td><?= $process_name?></td>
            <td><?= format_money_symbol($this->business->businessID, '%.2n', $s->listPrice) ?></td>
            <td><?= format_money_symbol($this->business->businessID, '%.2n', $s->locPrice) ?></td>
            <td><?= local_date($this->business->timezone, $s->updated, 'm/d/y g:i a'); ?></td>
            <?php if (!in_bizzie_mode() || is_superadmin()): ?>
                <td align="center" style="width:25px;">
                    <a onclick="return verify_delete()" id="delete-<?=$s->product_location_priceID?>" href="/admin/admin/delete_product_location_price/<?= $s->product_location_priceID ?>/<?= isset($location[0]->locationID) ? $location[0]->locationID: "" ?>"><img src="/images/icons/cross.png"></a>
                </td>
            <?php endif ?>

        </tr>
        <? endforeach; ?>
    </tbody>

</table>
<?endif; ?>
<br />

<? if( !empty( $location[0] ) ){ ?>
<table class="box-table-a">
    <thead>
        <th><?=__("Discount", "Discount")?></th>
        <th><?=__("Percentage (%)", "Percentage (%)")?></th>
        <th><?=__("Dollar Amount ($)", "Dollar Amount ($)")?></th>
        <th><?=__("Expiration", "Expiration")?></th>
        <th><?=__("Combinable", "Combinable")?></th>
        <th><?=__("Apply if minimum is not met", "Apply if minimum is not met")?></th>
        <th colspan="2"></th>
    </thead>
    <tr id="location_discount_form_row" <?if(!empty($location_discount)){?>style="display:none;"<?}?>>
    <form method="post" action="/admin/admin/add_location_discount/<?=$location[0]->locationID;?>">
        <td>
            <div>
                <input type="text" name="description" value="<?=$location_discount->description?>" style="width:300px;" />
            </div>
        </td>
        <td style="width:200px;">
            <div>
                <input type="text" size="5" value="<?=$location_discount->percent?>" name="percentage" value="0" />%
            </div>
        </td>
        <td style="width:200px;">
            <div>
                <input type="text" size="5" value="<?=$location_discount->dollar_amount?>" name="dollar_amount" value="0" />$
            </div>
        </td>
        <td><? if( strtotime('now') > strtotime($location_discount->expire_date ) && !empty( $location_discount->expire_date ) ){ ?> (EXPIRED)<? } ?>
            <input type="text" id="location_discount_expiration" name="expiration" value="<?= !empty( $location_discount->expire_date ) ? date( 'Y-m-d',strtotime( $location_discount->expire_date ) ) : ''; ?>" />
        </td>
        <td>
            <input type="checkbox" name="combine" value="1" <?= ( $location_discount->combine == 1 ) ? 'checked' : ''; ?> />
        </td>
        <td>
            <input type="checkbox" name="only_apply_if_minimum_is_not_met" value="1" <?= ( $location_discount->only_apply_if_minimum_is_not_met == 1 ) ? 'checked' : ''; ?> />
        </td>
        <td colspan="2">
            <button type="submit" class="button blue">
            <? if( empty($location_discount)){ ?>Add<? }else{ ?>Save<?}?>
            </button>
        </td>
    </form>
    </tr>
    <tr <?if(empty($location_discount)){?>style="display:none;"<?}?> id="location_discount_row">
        <td>
            <?=$location_discount->description?>
        </td>
        <td>
            <?=empty($location_discount->percent)?"-":$location_discount->percent?>
        </td>
        <td>
            <?=empty($location_discount->dollar_amount)?"-":$location_discount->dollar_amount?>
        </td>
        <td>
            <?= !empty( $location_discount->expire_date ) ? date( 'Y-m-d',strtotime( $location_discount->expire_date ) ) : ''; ?>
        </td>
        <td>
            <?= ($location_discount->combine ==1 ) ? 'Yes' : 'No'; ?>
        </td>
        <td>
            <?= ($location_discount->only_apply_if_minimum_is_not_met ==1 ) ? 'Yes' : 'No'; ?>
        </td>
        <td align="center">
            <button class="button blue" id="edit_location_discount_button">Edit</button>
        </td>
        <td align="center">
            <br>
            <a onclick="return verify_delete()" id="delete-discount-<?=$location_discount->locationDiscountID?>" href="/admin/admin/delete_location_discount/<?= $location_discount->locationDiscountID ?>"><img src="/images/icons/cross.png"></a>
        </td>
    </tr>
</table>
<? } ?>

<?php if (empty($products)): ?>
<h2><?php echo __("locations_with_pricing_count", "Locations With Pricing") ?> (<?= number_format_locale(count($locations),0)?>)</h2>

<table id="box-table-a">
    <thead>
        <th><?php echo __("locations_with_pricing_title", "Locations With Pricing") ?></th>
        <th><?php echo __("number_of_products_title", "Number of Products") ?></th>
    </thead>
    <tbody>
        <? foreach($locations as $location):?>
        <tr>
                <td>
                <a href='/admin/admin/location_pricing/<?= $location->locationID?>'>
                    <?=$location->address." ".$location->address2." (".$location->locationID.")" ?>
                </a>
                </td>
                <td>
                    <?=$location->product_count?>
                </td>
        </tr>
        <? endforeach;?>
    </tbody>
</table>
<?php endif; ?>
<script type="text/javascript">
$(function(){
    $('#edit_location_discount_button').click(function(){
        $('#location_discount_row').hide();
        $('#location_discount_form_row').show();
    });
});
</script>
