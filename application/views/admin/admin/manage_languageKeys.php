<?php enter_translation_domain("admin/admin/manage_languageKeys"); ?>
<?php
/**
 * The following view is the adminstrative interface for a user to set existing language keys and associate them languages and  language views.
 * Expects the following content variables
 *  @business_languages_by_language_dropdown array
 *  @languages_dropdown array
 *  @languageViews_dropdown array
 *  @business_languages
 *  @business_languages_dropdown array
 *  @languageViews_languages_and_languageKeys: An array in the following format, where each first level index is the primary key of a language view entity
 *      @languageViewID1 object Consists of all the languages respective language keys defined for this language view
 *          @name string The The name of the language view
 *          @languages array Consists of the languages for this view, where each index is the primary key of a language entity
 *             @languageID1 object Consists of the language keys defined in this language for the language view
 *                 @tag string
 *                 @description string
 *                 @languageKeys array Consists of the langauge keys defined for this language for this language view, where each index is the primary key of a language key entity
 *                      @languageKeyID1 object Consists of the properties for this language key
 *                          name
 *                          defaultText
 *                          transaction
 *                      @languageKeyID2
 *                          ....
 *                      ...
 *             @languageID2
 *                 ...
 *             ...
 *      @languageViewID2
 *          ...
 *      ...
 */
if (!is_array($business_languages_by_language_dropdown)) {
    trigger_error( __("business_languages_by_language_dropdown must be an array", "'business_languages_by_language_dropdown' must be an array"), E_USER_ERROR);
}
if (!is_array($languageViews_languages_and_languageKeys)) {
    trigger_error( __("languageViews_languages_and_languageKeys must be an array", "'languageViews_languages_and_languageKeys' must be an array"), E_USER_ERROR);
}
if (!is_array($languages_dropdown)) {
    trigger_error( __("languages_dropdown must be an array", "'languages_dropdown' must be an array"), E_USER_ERROR);
}
if (!is_array($business_languages)) {
    trigger_error( __("business_languages must be an array", "'business_languages' must be an array"), E_USER_ERROR);
}
if (!is_array($languageViews_dropdown)) {
    trigger_error( __("languageView_dropdown must be an array", "'languageView_dropdown' must be an array"), E_USER_ERROR);
}
if (!is_array($business_languages_dropdown)) {
    trigger_error( __("business_languages_dropdown must be an array", "'business_languages_dropdown' must be an array"), E_USER_ERROR);
}
if (!isset($business)) {
    trigger_error( __("business must be passed as a content variable", "'business' must be passed as a content variable"), E_USER_ERROR);
}
?>

<style type="text/css">
    fieldset {
        border: 1px solid;
        margin: 10px 0px;
        padding: 10px;
    }
    legend {
        font-weight: bold;
    }
    input,select {
        margin: 5px;
    }
    .dialog_form {
        width: 350px;
    }
    .dialog_form label {
        display: inline-block;
        width: 125px;
    }
    .editable {
        min-height: 1em;
        max-height: 10em;
        overflow-y: auto;
        line-height: normal;
        padding: 2px;
        border: 1px #bc271c inset !IMPORTANT;
    }
    .editable:hover {
        padding: 2px;
        border: 1px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3 !IMPORTANT;
    }

</style>

<script type="text/javascript">

var languageViews_languages_and_languageKeys = <?=json_encode($languageViews_languages_and_languageKeys)?>;
var business_languages = <?=json_encode($business_languages) ?>;
var languageViews = <?= json_encode($languageViews); ?>;

var TranslationEditor = function($incoming_translation_container, incoming_languageID, incoming_languageView_id, incoming_languageKey_id, keyName) {
    var $container = $("<div>");
    var $label = $("<label><strong>Name:</strong> "+keyName+"</label><br/><br/>").appendTo($container);
    var $textarea = $('<textarea rows="5" cols="30" style="width:600px;height:20em;"></textarea>').appendTo($container);
    var translation = null;

    var languageKeys = languageViews_languages_and_languageKeys[incoming_languageView_id].languages[incoming_languageID].languageKeys;
    var i = 0;

    for (i = 0; i < languageKeys.length ; i++) {
        if (languageKeys[i].languageKeyID == incoming_languageKey_id) {
            translation = languageKeys[i].translation;
            break;
        }
    }

    $textarea.val(translation ? translation : $incoming_translation_container.text());

    incoming_languageKey_id

    $container.dialog({
        modal: true,
        title : "<?php echo __("Edit Translation", "Edit Translation"); ?>",
        width: 640,
        buttons : {
            "Update" : function() {
                var translation = $textarea.val();
                $.blockUI({message : "<h1><?php echo __("Updating translation", "Updating translation..."); ?>  </h1>"});
                $.post("/admin/languageKey_business_languages/update_business_translation", {
                    language_id : incoming_languageID,
                    languageKey_id : incoming_languageKey_id,
                    translation: translation
                }, function (response) {
                    if (response['status'] == "success") {
                       $incoming_translation_container.text(translation);
                       languageViews_languages_and_languageKeys[incoming_languageView_id]
                        .languages[incoming_languageID]
                        .languageKeys[i].translation = translation;
                       alert(response['message']);
                    }
                    else if (response['status'] == "error") {
                        alert(response['message']);
                    }
                    else {
                        alert('<?php echo __("A server side error occurred processing the request", "A server side error occurred processing the request"); ?>');
                    }
                    $container.dialog("close");
                    $.unblockUI();
                }, "json").fail(function() {
                    alert('<?php echo __("A server side error occurred processing the request", "A server side error occurred processing the request"); ?>');
                    $.unblockUI();
                });
            },
            "Cancel" : function() {
                $(this).dialog("close");
            }
        }
    });
}

var populate_languageKey_table = function($incoming_table, $incoming_languageView_menu, $incoming_language_menu) {
    var languageViewID = $incoming_languageView_menu.val();
    var languageID = $incoming_language_menu.val();

    var languageView = languageViews_languages_and_languageKeys[languageViewID];
    if (typeof languageView === "undefined") {
        //There are no language views  defined for this language view iD bail.
    } else {
        var languages = languageView['languages'];
        var language = languages[languageID];
        var languageKeys = language['languageKeys'];
        set_table_content($incoming_table, languageKeys, languageID);
   }
}

var filter_language_keys = function($incoming_table, $incoming_language_menu, $incoming_language_search_query) {
    var languageID = $incoming_language_menu.val();
    var query = $incoming_language_search_query.val().toLowerCase();


    var languageKeys = new Array();


    $.each(languageViews_languages_and_languageKeys, function(index, languageView) {
        var language = languageView['languages'][languageID];


        $.each(language['languageKeys'], function(index2, languageKey) {
            if ((languageKey.defaultText && languageKey.defaultText.toLowerCase().indexOf(query) != -1)
                 || (languageKey.translation && languageKey.translation.toLowerCase().indexOf(query) != -1)) {
                languageKeys.push(languageKey);
            }
        });

    });

   set_table_content($incoming_table, languageKeys, languageID);

};

var set_table_content = function($incoming_table, languageKeys, languageID) {

    var aaData = new Array();

    $.each(languageKeys, function(index, properties) {
        var $translation = $("<div>");
        if (properties['translation'] !== null) {
            $translation.text(properties['translation']);
        }

        var $translation_container = $("<div>");

        $translation.addClass("editable")
        .attr("data-languageKeyID", properties['languageKeyID'])
        .attr("data-languageViewID", properties['languageView_id'])
        .attr("data-keyName", properties['name'])
        .appendTo($translation_container);

        var $text_container = $("<div>");
        $text_container.text(properties['defaultText']);

        aaData.push([
            languageViews[ properties['languageView_id'] ], 
            properties['name'],
            '<div style="line-height:normal;max-height:10em;overflow-y:auto;">'+$text_container.html()+'</div>', 
            $translation_container.html()
        ]);
    });

    var $languageKey_dataTable = $incoming_table.dataTable( {
        aaData : aaData,
        bFilter : false,
        bSort : false,
        //bInfo : false,
        //sDom : 't',
        aoColumns : [
            { "sTitle" : "<?php echo __("Language View", "Language View"); ?>", "sWidth": "20%"},
            { "sTitle" : "<?php echo __("Key Name", "Key Name"); ?>", "sWidth": "20%" },
            { "sTitle" : "<?php echo __("Default Text", "Default Text"); ?>", "sWidth": "30%" },
            { "sTitle" : "<?php echo __("Translation", "Translation"); ?>", "sWidth": "30%"}
        ],
        bJQueryUI : true,
        bDestroy : true,
        bPaginate : false
    });
    $languageKey_dataTable.css("width", "");

    $languageKey_dataTable.find(".editable").click(function() {
        var languageKeyID = $(this).attr("data-languageKeyID");
        var languageViewID = $(this).attr("data-languageViewID");
        var keyName = $(this).attr("data-keyName");

        var $translation_container = $(this);
        var $translationEditor = new TranslationEditor($translation_container, languageID, languageViewID, languageKeyID, keyName);
    });
};

var initialize_languageKey_management = function($incoming_languageKeys_table, $incoming_languageView_menu, $incoming_language_menu, $incoming_language_search_query) {

    $incoming_languageView_menu.change(function(evt) {
        $incoming_language_search_query.val("");
        populate_languageKey_table($incoming_languageKeys_table, $incoming_languageView_menu, $incoming_language_menu);
    });

    $incoming_language_menu.change(function(evt) {
        var query = $incoming_language_search_query.val();

        if (query && query.length > 2) {
            filter_language_keys($incoming_languageKeys_table, $incoming_language_menu, $incoming_language_search_query);
        } else {
            populate_languageKey_table($incoming_languageKeys_table, $incoming_languageView_menu, $incoming_language_menu);
        }
    })

    populate_languageKey_table($incoming_languageKeys_table, $incoming_languageView_menu, $incoming_language_menu);

    $incoming_language_search_query.keyup(function(evt) {
        var query = $(this).val();

        if (query && query.length > 2) {
            filter_language_keys($incoming_languageKeys_table, $incoming_language_menu, $incoming_language_search_query);
        } else {
            populate_languageKey_table($incoming_languageKeys_table, $incoming_languageView_menu, $incoming_language_menu);
        }
    });

}

$(document).ready(function() {

    if (! $.isEmptyObject(business_languages)) {
        var $languageKeys_table = $("#languageKeys");
        var $languageView_menu = $("#languageView_menu");
        var $language_menu = $("#language_menu");
        var $language_seach_query = $("#language_search_query");

        initialize_languageKey_management($languageKeys_table, $languageView_menu, $language_menu, $language_seach_query);
    }

}); //End of $(document).ready();
</script>

<h2><?php echo __("Manage Language Translations", "Manage Language Translations"); ?></h2>

<?php if (empty($languages_dropdown)): ?>
    <p style="color:red"><?php echo __("There are no languages in the system. The Superadmin must create at least one language to manage", "There are no languages in the system. The Superadmin must create at least one language to manage."); ?>  </p>
<?php elseif (empty($business_languages_dropdown)): ?>
    <p style="color:red"><?php echo __("There are no languages for your business. You must first add a language to your business before you can manage language keys", "There are no languages for your business. You must first add a language to your business before you can manage language keys."); ?>  </p>
<?php elseif(empty($languageViews_dropdown)): ?>
    <p style="color:red;"><?php echo __("There are no language views in the system. The Superadmin must create at least one language view to manage", "There are no language views in the system. The Superadmin must create at least one language view to manage."); ?>  </p>
<?php else: ?>
    <?= form_label("Language View") . form_dropdown("", $languageViews_dropdown, "", "id='languageView_menu'") ?>
    <?= form_label("Business Language") . form_dropdown("", $business_languages_by_language_dropdown, "", "id='language_menu'") ?>
    <?= form_label("Search") . form_input("", "", "id='language_search_query'") ?>

    <table id="languageKeys">
    </table>
<?php endif ?>

