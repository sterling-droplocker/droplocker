<?php enter_translation_domain("admin/admin/manage_business_languages"); ?>
<?php
/**
 * The folowing view is the administrative interface to add/delete business languages and set the default business language.
 *  @business_languages_dropdown array
 *  @business_languages array
 *  @languages_dropdown array
 */

if (!is_array($business_languages_dropdown)) {
    trigger_error(__("business_languages_dropdown must be an array", "'business_languages_dropdown' must be an array'"));
}
if (!is_array($business_languages)) {
    trigger_error(__("business_languages must be an array", "'business_languages' must be an array"));
}
if (!is_numeric($default_business_language_id)) {
    $default_business_language_id = 0;
}

if (!is_array($languages_dropdown)) {
    trigger_error(__("languages_dropdown must be an array", "'languages_dropdown' must be an array"));
}
?>

<style type="text/css">
    fieldset {
        border: 1px solid;
        margin: 10px 0px;
        padding: 10px;
    }
    legend {
        font-weight: bold;
    }
    input,select {
        margin: 5px;
    }
    .dialog_form {
        width: 350px;
    }
    .dialog_form label {
        display: inline-block;
        width: 125px;
    }
    .editable {
        padding: 2px;
        border: 1px #bc271c inset !IMPORTANT;
    }
    .editable:hover {
        padding: 2px;
        border: 1px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3 !IMPORTANT;
    }

</style>

<script type="text/javascript">

var business_languages = <?=json_encode($business_languages) ?>;
var default_business_language_id = <?= $default_business_language_id ?>;
var initialize_business_language_management = function($incoming_business_language_table, $incoming_add_business_language_button, $incoming_add_business_language_dialog, $incoming_edit_business_language_button, $incoming_edit_business_language_dialog, $incoming_set_default_business_language_button, $incoming_set_default_business_language_dialog) {
    var aaData = new Array();
    $.each(business_languages, function(index, business_language) {
        var business_languageID = business_language['business_languageID'];
        var $delete_link_container = $("<div>");

        var $delete_link = $("<a>");
        $delete_link.attr("href", "/admin/business_languages/delete?business_languageID=" + business_languageID);
        $delete_link.attr("id", "delete-" + business_languageID); //Note, this identifier is used for Selenium testing to find a specific business language to delete.
        $delete_link.appendTo($delete_link_container);

        var $delete_image = $("<img>");
        $delete_image.attr("src", "/images/icons/delete.png");
        $delete_image.attr("alt", "Delete Business Language" + business_language['tag']);
        $delete_image.appendTo($delete_link);
        var default_business = "";
        if (business_languageID == default_business_language_id) {
            default_business = "Yes";
        }
        aaData.push([business_languageID, business_language['tag'], business_language['description'], business_language['locale'],default_business, $delete_link_container.html()])
    });
    var $business_language_dataTable = $incoming_business_language_table.dataTable( {
        oLanguage : {
            sEmptyTable : "<?php echo __("There are no defined business languages", "There are no defined business languages"); ?>"
        },
        aaData : aaData,
        aoColumns : [
            { sTitle : "<?php echo __("Business Language ID", "Business Language ID"); ?>" },
            { sTitle : "<?php echo __("Tag", "Tag"); ?>" },
            { sTitle : "<?php echo __("Description", "Description"); ?>"},
            { sTitle : "<?php echo __("Locale", "Locale"); ?>"},
            { sTitle : "<?php echo __("Default", "Default"); ?>"},
            { sTitle : "<?php echo __("Delete", "Delete?"); ?>"}
        ],
        bJQueryUI : true,
        bPaginate: false
    });

    $incoming_add_business_language_button.click(function() {
        $incoming_add_business_language_dialog.dialog({
            title : "<?php echo __("Add Business Language", "Add Business Language"); ?>",
            modal : true,
            height : "auto",
            width : "460"
        })
    });
    $incoming_edit_business_language_button.click(function() {
        $incoming_edit_business_language_dialog.dialog( {
            title : "<?php echo __("Edit Business Language Locale", "Edit Business Language Locale"); ?>",
            modal : true,
            height : "auto",
            width : "460"
        });
    });
    $incoming_set_default_business_language_button.click(function() {
        $incoming_set_default_business_language_dialog.dialog( {
            title : "<?php echo __("Set Default Business Language", "Set Default Business Language"); ?>",
            modal : true,
            height : "auto",
            width : "460"
        });
    });

}

$(document).ready(function() {
    var $add_business_language_button = $("#add_business_language_button");
    var $add_business_language_dialog = $("#add_business_language_dialog");

    var $business_language_table = $("#business_languages");
    var $edit_business_language_button = $("#edit_business_language_button");
    var $edit_business_language_dialog = $("#edit_business_language_dialog");
    var $set_default_business_language_button = $("#set_default_business_language_button");
    var $set_default_business_language_dialog = $("#set_default_business_language_dialog");
    initialize_business_language_management($business_language_table, $add_business_language_button, $add_business_language_dialog, $edit_business_language_button, $edit_business_language_dialog, $set_default_business_language_button, $set_default_business_language_dialog);
});

</script>


<h2> Manage Business Languages </h2>

<?= form_input(array("type" => "button", "class" => "button blue", "id" => "add_business_language_button", "value" => __("Add New Business Language", "Add New Business Language"))) ?>
<?= form_input(array("type" => "button", "class" => "button blue", "id" => "edit_business_language_button", "value" => __("Edit Business Language Locale", "Edit Business Language Locale"))) ?>
<?= form_input(array("type" => "button", "class" => "button blue", "id" => "set_default_business_language_button", "value" => __("Set Default Business Language", "Set Default Business Language"))) ?>
<table id="business_languages"> </table>

<!-- Dialog containers -->
<div id="set_default_business_language_dialog" style="display:none">
     <?= form_open("/admin/business/update") ?>
        <?= form_fieldset("Set default business language") ?>
            <?= form_hidden("property", "default_business_language_id") ?>
            <?= form_dropdown("value", $business_languages_dropdown, $default_business_language_id) ?>
            <?= form_submit(array("class" => "button orange", "value" => __("Set Default", "Set Default"))) ?>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>
</div>
<div id="edit_business_language_dialog" style="display:none">
     <?= form_open("/admin/business_languages/update", array("class" => "dialog_form")) ?>
        <?= form_fieldset("Edit business language locale") ?>
            <?= form_dropdown("default_business_language_id", $business_languages_dropdown, $default_business_language_id) ?>
            <?= form_dropdown('locale', $locales, isset($business->locale) ? $business->locale : null, 'id="locale"'); ?>
            <?= form_submit(array("class" => "button orange", "value" => __("Edit", "Edit"))) ?>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>
</div>
<div id="add_business_language_dialog" style="display: none;">
    <?= form_open("/admin/business_languages/add", array("class" => "dialog_form")) ?>
        <?= form_fieldset(__("Add language to business", "Add language to business")) ?>
            <?= form_dropdown("language_id", $languages_dropdown) ?>
        <?= form_fieldset_close() ?>
        <?= form_fieldset(__("Add locale to language", "Add locale to language")) ?>
            <?= form_dropdown('locale', $locales, isset($business->locale) ? $business->locale : null, 'id="locale"'); ?>
        <?= form_fieldset_close() ?>
        <?= form_submit(array("class" => "button orange forLocaleBtn", "value" => __("Add", "Add"))) ?>
    <?= form_close() ?>
</div>