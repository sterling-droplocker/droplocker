<?php enter_translation_domain("admin/admin/mobile"); ?>
<style type="text/css">
    div.mobileDownload {
        padding:5px; 
        border:1px solid #ccc;
        margin:5px;
    }
</style>
<h2> <?php echo __("Download DropLocker Mobile", "Download DropLocker Mobile"); ?> </h2>

<?php 
 if (in_bizzie_mode()) {
    ?>

<div class="mobileDownload">
     <h3> <?php echo __("BIZZIE Driver App 2018-05-09", "BIZZIE Driver App 2018-05-09"); ?></h3> 
    <p> <a href="http://www.wikihow.tech/Install-APK-Files-on-Android"><?php echo __("Instructions on how to install an app on android", "Instructions on how to install an app on android"); ?></a></p>
    <p> <a href="https://www.youtube.com/playlist?list=PLkcFK8uk1Vmr3h7kqfQKRLKgL5GwRNUGI"><?php echo __("Driver App Instruction Videos", "Driver App Instruction Videos"); ?></a></p>
    <p> <a class='button blue' href='http://droplocker.com/apk/DriverApp-v2018-05-09--05-35-54-117Z--bizzie_prod.apk'><?php echo __("Download Now", "Download Now"); ?></a></p>
     
</div>

<?php
 } else {
 ?>


<div class="mobileDownload">
    <h3> <?php echo __("Driver App  2018-03-23", "Driver App  2018-03-23"); ?></h3> 
    <p> <a href="http://www.wikihow.tech/Install-APK-Files-on-Android"><?php echo __("Instructions on how to install an app on android", "Instructions on how to install an app on android"); ?></a></p>
    <p> <a href="https://www.youtube.com/playlist?list=PLkcFK8uk1Vmr3h7kqfQKRLKgL5GwRNUGI"><?php echo __("Driver App Instruction Videos", "Driver App Instruction Videos"); ?></a></p>
    <p> <a class='button blue' href='http://droplocker.com/apk/DriverApp-v2018-03-23--18-18-50-901Z--prod.apk'><?php echo __("Download Now", "Download Now"); ?></a></p>
</div>

<?php
 } 
 ?>

<div class="mobileDownload">
    <h3> <?php echo __("Driver App  2017-08-25", "Driver App  2017-08-25") . " "; echo __("(Previous Version)", "(Previous Version)");?></h3> 
    <p> <a href="http://www.wikihow.tech/Install-APK-Files-on-Android"><?php echo __("Instructions on how to install an app on android", "Instructions on how to install an app on android"); ?></a></p>
    <p> <a href="https://www.youtube.com/playlist?list=PLkcFK8uk1Vmr3h7kqfQKRLKgL5GwRNUGI"><?php echo __("Driver App Instruction Videos", "Driver App Instruction Videos"); ?></a></p>
    <p> <a class='button blue' href='http://droplocker.com/apk/DriverApp-v2017-08-25--19-07-30-766Z--prod.apk'><?php echo __("Download Now", "Download Now"); ?></a></p>
</div>

<div class="mobileDownload">
    <h3> <?php echo __("Barcode Scanner", "Barcode Scanner"); ?> </h3> <p> <?php echo __("This is a customized barcode scanner which will return results much faster then the normal Android scanner", "This is a customized barcode scanner which will return results much faster then the normal Android scanner"); ?></p>
    <p><a class='button blue' href='http://droplocker.com/apk/laundrybarcodescanner.apk'><?php echo __("Download Now", "Download Now"); ?></a></p>
</div>

<div class="mobileDownload">
    <h3> <?php echo __("NFC Manager", "NFC Manager"); ?> </h3> <p> <?php echo __("Reads and writes NFC tags with location, locker and bag numbers", "Reads and writes NFC tags with location, locker and bag numbers"); ?> </p>
    <p> <a class='button blue' href='http://droplocker.com/apk/NFC-Admin.apk'><?php echo __("Download Now", "Download Now"); ?></a> </p>
</div>

<div class="mobileDownload">
    <h3> <?php echo __("Web camera Tool", "Web camera Tool"); ?> </h3> <p> <?php echo __("Takes pictures of items and stores it in the DLA", "Takes pictures of items and stores it in the DLA."); ?> </p>
    <p> <a class='button blue' href='http://droplocker.com/apk/webcamtool_setup.exe'><?php echo __("Download Now", "Download Now"); ?></a> </p>
</div>
