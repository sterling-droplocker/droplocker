<?php enter_translation_domain("admin/admin/reporting_parameters"); ?>
<script type="text/javascript">
$(document).ready( function() {
    $("#default_report_cut_off_time").timepicker(
    {
        timeFormat: "hh:mm"
    });
});
    
</script>
<h2><?php echo __("Reporting Parameters", "Reporting Parameters"); ?></h2>
<form action="/admin/business/update" method="post">
    <table class="detail_table">
        <tbody>
            <tr>
                <th><?php echo __("Daily Cut Off Time", "Daily Cut Off Time"); ?></th>

                <td>
                    <?= form_hidden("property", "default_report_cut_off_time") ?>
                    <input style="width:200px;" type="text" id="default_report_cut_off_time" name="value" value="<?= $business->default_report_cut_off_time ?>"> <?php echo __("eg1600", "e.g. 16:00"); ?>
                </td>
            </tr>	
        </tbody>
    </table>
    <div style="text-align: right;">
        <input type="submit" value="<?php echo __("Update", "Update"); ?>" class="button orange">
    </div>
</form>
