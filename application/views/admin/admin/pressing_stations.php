<?php enter_translation_domain("admin/admin/pressing_stations"); ?>
<h2><?php echo __("Pressing Stations", "Pressing Stations"); ?></h2>
<br>
<table class="box-table-a">
<tr>
    <th><?php echo __("Name", "Name"); ?></th>
    <th><?php echo __("Location", "Location"); ?></th>
    <th><?php echo __("Target PPH", "Target PPH"); ?></th>
    <th><?php echo __("Count towards rework", "Count towards rework"); ?></th>
    <th></th>
</tr>
<form action="/admin/admin/pressing_station_add" method="post">
<tr>
    <td><input type="text" name="description"></td>
    <td><input type="text" name="location"></td>
    <td><input type="text" name="targetPPH" size="5"></td>
    <td><input type="checkbox" name="rework" value="1"></td>
    <td><input type="submit" name="Add" value="<?php echo __("Add", "Add"); ?>" class="button blue"></td>
</tr>
</form>
<? foreach($stations as $station): ?>
<tr>
    <td><?= $station->description ?></td>
    <td><?= $station->location ?></td>
    <td><?= $station->targetPPH ?></td>
    <td><?= $station->rework ?></td>
    <td></td>
</tr>
<? endforeach; ?>
</table>
