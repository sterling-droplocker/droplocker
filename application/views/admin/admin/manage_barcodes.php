<?php enter_translation_domain("admin/admin/manage_barcodes"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $(".delete").click(function() {
        if (confirm("<?php echo __("Are you sure you want to delete this barcode length", "Are you sure you want to delete this barcode length?"); ?>"))
        {
            return true;
        }
        else
        {
            return false;
        }
    });
});
</script>

<style tyep="text/css">
fieldset 
{
    border: 1px solid #000000;
}
fieldset legend
{
    font-weight: bold;
}
</style>
<h2> <?php echo __("Manage Barcodes", "Manage Barcodes"); ?> </h2>

<?php if (!in_bizzie_mode() || is_superadmin()): ?>
    <?= form_open("/admin/barcode_lengths/add", array("style" => "padding: 10px 0")) ?>
        <fieldset>
            <legend> <?php echo __("Add New Barcode Length", "Add New Barcode Length"); ?> </legend>

            <?= form_label("Length", "length") ?>
            <?= form_input("length"); ?>
            <?= form_submit(array("class" => "button orange"), __("Add", "Add")) ?>
        </fieldset>

    <?= form_close() ?>

<?php endif ?>
<div style="font-size: 1.4em; padding: 10px 0"> <?php echo __("Current Allowed Barcode Lengths", "Current Allowed Barcode Lengths"); ?> </div>

<p><i> <?php echo __("Note, if no custom barcode lengths are defined, the defaults are 8 and 10. Add a new barcode length to override the defaults.", "Note, if no custom barcode lengths are defined, the defaults are 8 and 10. Add a new barcode length to override the defaults."); ?></i></p>


<table id="box-table-a">
    <thead>
        <tr>
            <th> <?php echo __("Length", "Length"); ?> </th>
            <?php if (!in_bizzie_mode() || is_superadmin()): ?>
                <th> <?php echo __("Delete", "Delete?"); ?> </th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($barcode_lengths as $barcode_length): ?>
            <tr>
                <td> <?= $barcode_length ?> </td>
                <?php if (!in_bizzie_mode() || is_superadmin()): ?>
                    <td> <a class="delete" href="/admin/barcode_lengths/delete?length=<?= $barcode_length?>"><img alt="<?php echo __("Delete Barcode Length", "Delete Barcode Length"); ?>" src="/images/icons/cross.png" /> </a> </td>
                <?php endif ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
