<?php enter_translation_domain("admin/admin/email_macros"); ?>
<link href='/css/admin/style.css' rel='stylesheet' />
<script type="text/javascript" src='/js/jquery-1.6.4.min.js'></script>
<script type="text/javascript" src='/js/functions.js'></script>
<script>
$(document).ready(function(){
	$("#options").change(function(){
		top.location.href='/admin/admin/email_macros/'+$(this).val();
	});
});
</script>
<div style='text-align:left;padding:5px'>
<h2><?php echo __("Email Macro Values", "Email Macro Values"); ?></h2>
<div style='padding:5px;background-color:#eaeaea; border:1px solid #cccccc;'>
	<form action='' style='margin-bottom:0px;'>
		<?php echo __("Select Macro Category", "Select Macro Category:"); ?> <select id='options'>
		    <option value=''><?php echo __("--SELECT--", "--SELECT--"); ?></option>
			<? foreach($dd_macros as $k=>$v):?>
				<option value='<?= $k?>' <?= $type == $k ? 'selected="selected"' : '' ?>?><?php echo __($k, $k); ?></option>
			<? endforeach; ?>
		</select>
	</form>
</div>

	<table>
		<? if($macros):
		foreach ($macros as $mkey=>$mvalue) : ?>
		<tr>
			<td><?= $mkey?></td>
		</tr>
		<? endforeach; endif;?>
	</table>

</div>
