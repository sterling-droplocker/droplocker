<?php enter_translation_domain("admin/admin/denied"); ?>
<h2><?php echo __("Access Denied", "Access Denied"); ?></h2>

<p>
<?php echo __("The page you are trying to access is unavailable", "The page you are trying to access is unavailable."); ?>
</p>

<p>
	<form action='/admin/main/request_permission' method='post' class="access-denied">
	<input type='hidden' name='getValues' value="<?= $getValues?>" />
	<input type='hidden' name='postValues' value="<?= $postValues?>" />
	<input type='hidden' name='page' value="<?= $page?>" />
	<input type='hidden' name='resource' value="<?= $resource?>" />
	<input type='submit' name='submit' value='<?php echo __("Request Permission", "Request Permission"); ?>' class='button orange' />
	</form>
</p>
