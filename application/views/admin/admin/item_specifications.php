<?php enter_translation_domain("admin/admin/item_specifications"); ?>
<h2><?php echo __("Item Specifications", "Item Specifications"); ?></h2>
<br>
<form action="item_specifications_add" method="post">
<?php echo __("Description", "Description:"); ?>
<input type="text" name="specification" size="30">
<select name="everyOrder">
	<option value="1"><?php echo __("Every Order", "Every Order"); ?></option>
	<option value="0"><?php echo __("This Order", "This Order"); ?></option>
</select>
<input type="submit" name="add" value="add">
</form>
<br>
<br>
<table id="box-table-a">
<tr>
    <th><?php echo __("Specification", "Specification"); ?></th>
    <th><?php echo __("Applied", "Applied"); ?> To</th>
    <th><?php echo __("Delete", "Delete"); ?></th>
</tr>
<? foreach($specifications as $specification): ?>
<tr>
    <td><?= $specification->description ?></td>
    <td><?= $specification->everyOrder == 1 ? __("Every Order", "Every Order"): __("This Order", "This Order"); ?></td>
    <td><a href="/admin/admin/item_specifications_delete?delete=<?= $specification->specificationID ?>" onclick="return verify_delete()"><?php echo __("delete", "delete"); ?></a></td>
</tr>
<? endforeach; ?>
</table>
