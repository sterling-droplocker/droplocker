<?php enter_translation_domain("admin/admin/shoe_tags"); ?>
<h2><?php echo __("Print Shoe Tags", "Print Shoe Tags"); ?></h2>

<p><?php echo __("Enter range of barcodes to print:", "Enter range of barcodes to print"); ?></p>

<form action="" method="get" target="_blank">
	<?php echo __("Barcode", "Barcode:"); ?> <input type="text" name="barcode">
    <?php echo __("to", "to"); ?> <input type="text" name="barcodeEnd">

    <input type="submit" class="button orange" value="<?php echo __("Print", "Print"); ?>">
</form>
