<?php enter_translation_domain("admin/admin/manual_email_report"); ?>
<h2><?php echo __("Email Report", "Email Report"); ?></h2>

<p>
<? if($this->session->flashdata('referral')): ?>
    <a href='/<?= $this->session->flashdata('referral')?>'><?php echo __("Back to Email Admin", "Back to Email Admin"); ?></a>
<? endif ?>
</p>

<table class='box-table-a'>
    <tr>
        <th><?php echo __("To", "To"); ?></th>
        <th><?php echo __("Subject", "Subject"); ?></th>
        <th><?php echo __("Status", "Status"); ?></th>
    </tr>
<? foreach($results as $result): ?>
    <tr>
        <td><?= $result['to']?></td>
        <td><?= $result['subject']?></td>
        <td><?= $result['status']?></td>
    </tr>
<? endforeach; ?>
</table>