<?php enter_translation_domain("admin/admin/timecard_setup"); ?>
<h2><?php echo __("Timecard Setup", "Timecard Setup"); ?></h2>
<br>
<?= $this->session->flashdata('message');?>
<form action="" method="post">
<table>
<tr>
    <td><?php echo __("Timecards", "Timecards"); ?></td>
    <td><select name="onOff">
	<option value="1" <? if($onOff == 1) echo ' SELECTED' ?>><?php echo __("On", "On"); ?></option>
	<option value="0" <? if($onOff == 0) echo ' SELECTED' ?>><?php echo __("Off", "Off"); ?></option>
</select></td>
</tr>
<tr>
    <td><?php echo __("Calculate Overtime", "Calculate Overtime"); ?></td>
    <td><select name="overtime">
	<option value="1" <? if($overtime == 1) echo ' SELECTED' ?>><?php echo __("On", "On"); ?></option>
	<option value="0" <? if($overtime == 0) echo ' SELECTED' ?>><?php echo __("Off", "Off"); ?></option>
</select></td>
</tr>
<tr>
    <td><?php echo __("Timecards Start Of Week", "Timecards Start Of Week"); ?></th>
    <td>
        <?= form_dropdown('timecards_start_of_week', $timecards_start_of_week_options, get_business_meta($this->business_id, 'timecards_start_of_week', 'Mon')) ?>
    </td>
</tr>
</table>
<br>
<br>
<input type="submit" name="update" value="<?php echo __("Update", "Update"); ?>" class="button blue">
</form>
