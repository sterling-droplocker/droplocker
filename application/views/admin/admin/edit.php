<?php
enter_translation_domain("admin/admin/edit");

$no_edit = in_bizzie_mode() && !is_superadmin();
$is_superadmin = is_superadmin();

$date_formats_array = array();
foreach ($dateFormats as $dateFormat) {
    $date_formats_array[$dateFormat] =  date($dateFormat);
}

$all_states = get_all_states_by_country_as_array();

?>
<?php

$fieldList = array();
$fieldList['address1'] = 
        "<tr>
        <th>". __('address', 'Address') ."</th>
        <td>
            " . (  ($no_edit) ? $business->address 
            : "
                <input type='text' name='address' value='". $business->address."' />
            " ) ."
        </td>
	</tr>";
$fieldList['address2'] = 
        "<tr>
        <th>". __('adress_b', 'Address 2') ."</th>
        <td>
            " . (  ($no_edit) ? $business->address2
            : "
                <input type='text' name='address2' value='". $business->address2."' />
            " ) ."
        </td>
	</tr>";
$fieldList['city'] = 
        "<tr>
        <th>". __('city', 'City') ."</th>
        <td>
            " . (  ($no_edit) ? $business->city
            : "
                <input type='text' name='city' value='". $business->city."' />
            " ) ."
        </td>
	</tr>";
$fieldList['state'] = 
        "<tr>
        <th id='state_label'>". __('state', 'State') ."</th>
        <td id='state_row'>
        " . (  ($no_edit) ? $business->state
        : (  (isset($all_states[$business->country])) ? form_dropdown('state', $all_states[$business->country], isset($business->state) ? $business->state : null, 'id="state_select"' )
            : "<input type='text' name='state' value='". set_value('state', isset($business->state) ? $business->state : null )."' />"
            )) . "
        </td>
    </tr>";
$fieldList['country'] = 
        "<tr>
        <th>". __('country', 'Country') ."</th>
        <td>
            " . (  ($no_edit) ? $business->country
            : form_dropdown('country', get_countries_as_array(), isset($business->country) ? $business->country : null, 'id="country_select"')
             ) ."
        </td>
    </tr>";
$fieldList['zip'] = 
        "<tr>
        <th id='zipcode_label'>". __('zipcode', 'Zip Code') ."</th>
        <td id='zipcode_row'>
        " . (  ($no_edit) ? $business->zipcode
        : "
            <input type='text' name='zipcode' value='". set_value('zipcode', isset($business->zipcode) ? $business->zipcode : null ) ."' />
            " ) ."
        </td>
    </tr>";
        
$addressForm = getSortedAddressForm($fieldList);

?>
<script type="text/javascript">
$(document).ready(function(){
   function getLockers(){
       $.ajax({
            url:"/ajax/get_lockers_at_location",
            type:"POST",
            data:{location_id:$('#location_id').val()},
            dataType: 'JSON',
            success: function(response){
                var select = $("<select name='returnToOfficeLockerId' id='locker_id'>");
                $.each(response, function(i, data){
                	$.each(data, function(i, item){
                        var key = i.split("-");
                        var option = $('<option value="'+key[0]+'">'+item+'</option>');
                        select.append(option);
                	});
                });
                $('#locker_holder').html(select);
            }
       });
   }

    $("#blank_customer").autocomplete({
        source : "/ajax/customer_autocomplete_aprax",
        select: function (event, customer) {
            $("input[name='blank_customer_id']").val(customer.item.id);
        }

    });
   $('#location_id').live('change', function(){
       getLockers();
   });
   $(":submit").loading_indicator();


    // logic for country/state selection (internation - canada)

    // should share select attributes for name, so one state comes through
    var countrySubSelect = function(){
        var needState = false;
        var countrySelVal =  $('#country_select').val();

        if (typeof(statesOptions[countrySelVal]) != 'undefined') {
            var states = statesOptions[countrySelVal];
            $('#state_row').html('<select name="state" id="state_select"></select>');
            $('#state_select').html(buildStateOptions(states));
        } else {
            $('#state_row').html('<input type="text" name="state" />');
        }
    };

    $('#country_select').bind('change', countrySubSelect);

    //creates a dropdown for states
    var buildStateOptions = function(states) {
        var selectMain = $('<select />');
        for (var key in states) {
            $('<option></option>').val(key).text(states[key]).appendTo(selectMain);
        }
        return selectMain.html();
    }

});

var statesOptions = <?= json_encode($all_states) ?>;

</script>



<h2><?= __("edit_details", "Edit Business Details") ?></h2>
<?= message(); ?>

<?= form_open("/admin/admin/edit") ?>
<table class="detail_table">
    <tr>
        <th><?= __("business_id", "Business ID") ?></th>
        <td>
            <?= $business->businessID?> <?= form_hidden("businessID", $business->businessID) ?>
        </td>
    </tr>
    <tr>
        <th><?= __("Sag Key Set", "Sag Key Set") ?></th>
        <td>
            <? if(!$is_superadmin): ?>
                <?= get_business_meta($business->businessID, "sag_key_set") ?>
            <? else: ?>
                <?= form_input("sag_key_set", get_business_meta($business->businessID, "sag_key_set")) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("print", "Print Temporary Bag Tag Headers") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= $business->print_temp_tag_header ?>
            <? else: ?>
                <?= form_yes_no("print_temp_tag_header", $business->print_temp_tag_header) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("company", "Company Name") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->companyName ?>
            <? else: ?>
                <input type='text' name='companyName' value="<?= $business->companyName?>" />
            <? endif ?>
        </td>
    </tr>

    <?= $addressForm ?>

    <tr>
        <th><?= __("email", "Email") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->email ?>
            <? else: ?>
                <input type='text' name='email' value="<?= $business->email?>" />
            <? endif ?>
        </td>
	</tr>
	<tr>
        <th><?= __("phone", "Phone") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->phone ?>
            <? else: ?>
                <input type='text' name='phone' value="<?= $business->phone?>" />
            <? endif ?>
        </td>
	</tr>
	<tr>
        <th><?= __("website", "Website URL") ?></th>
        <td>
            <? if (!$is_superadmin): ?>
                <?= $business->website_url ?>
            <? else: ?>
                <input type='text' name='website_url' value="<?= $business->website_url?>" style="width: 75%;"/>
            <? endif ?>
        </td>
	</tr>
        <tr>
            <th><?= __("hq_location_id", "Head Quarters Location") ?></th>
            <td>
            <? if($no_edit): ?>
                <?= $business->hq_location_id ?>
            <? else: ?>
                <?= form_dropdown("hq_location_id", $locations, $business->hq_location_id) ?>
            <? endif ?>
            </td>
        </tr>
    <tr>
        <th><?= __("money_symbol_str", "Monetary Symbol") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->money_symbol_str ?>
            <? else: ?>
                <input type='text' name='money_symbol_str' value="<?= $money_symbol_str?>" />
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("money_symbol_position", "Monetary symbol as prefix? ('No' will put it as suffix)") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= $business->money_symbol_position ?>
            <? else: ?>
                <?= form_yes_no("money_symbol_position", $money_symbol_position) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("decimal_positions", "Decimal positions") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->decimal_positions ?>
            <? else: ?>
                <input type='text' name='decimal_positions' value="<?=$decimal_positions?>" />
            <? endif ?>
        </td>
    </tr>
	<? if (!$no_edit): ?>
		<tr>
			<th><?= __("hide_url_on_receipt", "Hide URL on Receipt") ?></th>
			<td>
				<?= form_checkbox("hideURLOnReceipt", "1", get_business_meta($business->businessID, 'hideURLOnReceipt', false)); ?>
			</td>
		</tr>
	<? endif ?>
	<? if (!$no_edit): ?>
		<tr>
			<th><?= __("hide_www_on_url_on_receipt", "Don't add 'www' to URL on Receipt") ?></th>
			<td>
				<?= form_checkbox("hideWWWOnURLOnReceipt", "1", get_business_meta($business->businessID, 'hideWWWOnURLOnReceipt', false)); ?>
			</td>
		</tr>
	<? endif ?>
	<tr>
	    <th><?= __("business_locale", "Business Locale") ?></th>
	    <td>
            <? if ($no_edit): ?>
                <?= $business->locale ?>
            <? else: ?>
                <?= form_dropdown('locale', $locales, isset($business->locale) ? $business->locale : null, 'id="locale"'); ?>
                <p>
                    <?= __("monetary_format", "Monetary Format:") ?>
                    <b><?= format_money_symbol($business->businessID, '%.2n', 1234.56) ?></b>
                </p>
                <p>
                    <?= __("weight_format", "Weight Format:"); ?>
                    <b><?= weight_system_format(12.34) ?></b>
                </p>
            <? endif ?>
        </td>
	</tr>
	<tr>
        <th><?= __("company_timezone", "Company Timezone") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->timezone ?>
            <? else: ?>
               <?= form_dropdown("timezone", DateTimeZone::listIdentifiers(), array_search($business->timezone, DateTimeZone::listIdentifiers())) ?>
            <? endif ?>
        </td>
	</tr>
	<tr>
        <th><?= __("standard", "Standard Display Format") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, "standardDateDisplayFormat") ?>
            <? else: ?>
                <?= form_dropdown('standardDateDisplayFormat', $date_formats_array, get_business_meta($business->businessID, "standardDateDisplayFormat")); ?>
            <? endif ?>
        </td>
	</tr>
	<tr>
        <th><?= __("short", "Short Date Display Format") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, "shortDateDisplayFormat") ?>
            <? else: ?>
                <?= form_dropdown('shortDateDisplayFormat', $date_formats_array, get_business_meta($business->businessID, "shortDateDisplayFormat")); ?>
            <? endif ?>
            <?= __("used_for_invoices", "(Used for invoices)") ?>
        </td>
    </tr>
	<tr>
        <th><?= __("full", "Full Date Display Format") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($this->business->businessID, "fullDateDisplayFormat") ?>
            <? else: ?>
                <?= form_dropdown('fullDateDisplayFormat', $date_formats_array, get_business_meta($business->businessID, "fullDateDisplayFormat")); ?>
            <? endif ?>
        </td>
	</tr>
	<tr>
        <th><?= __("standard_locale", "Standard Localized Date Format") ?></th>
            <td>
                <? if ($no_edit): ?>
                    <?= get_business_meta($business->businessID, "standardDateLocaleFormat", "%A, %B %e") ?>
                <? else: ?>
                    <input name='standardDateLocaleFormat' id='standardDateLocaleFormat' value="<?= get_business_meta($business->businessID, "standardDateLocaleFormat", "%A, %B %e") ?>">
                    <a href="/date_format.html" target="_blank"><?= __("reference", "Reference") ?></a>
                <? endif ?>
            </td>
        </tr>
	<tr>
	<tr>
        <th><?= __("short_Locale", "Short Localized Date Format") ?></th>
            <td>
                <? if ($no_edit): ?>
                    <?= get_business_meta($business->businessID, "shortDateLocaleFormat", "%b %e, %Y") ?>
                <? else: ?>
                    <input name='shortDateLocaleFormat' id='shortDateLocaleFormat' value="<?= get_business_meta($business->businessID, "shortDateLocaleFormat", "%b %e, %Y") ?>">
                    <a href="/date_format.html" target="_blank"><?= __("reference", "Reference") ?></a>
                <? endif ?>
            </td>
        </tr>
	<tr>
	<tr>
        <th><?= __("full_Locale", "Full Localized Date Format") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, "fullDateLocaleFormat", "%a, %b %d %Y") ?>
            <? else: ?>
                <input name='fullDateLocaleFormat' id='fullDateLocaleFormat' value="<?= get_business_meta($business->businessID, "fullDateLocaleFormat", "%a, %b %d %Y") ?>">
                <a href="/date_format.html" target="_blank"><?= __("reference", "Reference") ?></a>
            <? endif ?>
        </td>
    </tr>
	<tr>
        <th><?= __("short_wdday_Locale", "Short Localized Date Format With Day ") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, "shortWDDateLocaleFormat", "%a %m/%d") ?>
            <? else: ?>
                <input name='shortWDDateLocaleFormat' id='shortWDDateLocaleFormat' value="<?= get_business_meta($business->businessID, "shortWDDateLocaleFormat", "%a %m/%d") ?>">
                <?= __("used_for_the_order_details", "(Used for order details)") ?>
                <a href="/date_format.html" target="_blank"><?= __("reference", "Reference") ?></a>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("hour_format_Locale", "Localized format for hours ") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, "hourLocaleFormat", "%l:%M %p") ?>
            <? else: ?>
                <input name='hourLocaleFormat' id='hourLocaleFormat' value="<?= get_business_meta($business->businessID, "hourLocaleFormat", "%l:%M %p") ?>">
                <?= __("used_to_show_times", "(Used show time. Eg: time windows)") ?>
                <a href="/date_format.html" target="_blank"><?= __("reference", "Reference") ?></a>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("cuttoff", "Order Cut Off Time") ?></th>
        <td><input type='text' name='cutOff' value="<?= get_business_meta($business->businessID, 'cutOff')?>" /></td>
    </tr>
    <tr>
        <th><?= __("driver_app_sync_time", "Driver app manifest sync time") ?></th>
        <td><input type='text' name='driver_app_sync_time' value="<?= get_business_meta($business->businessID, 'driver_app_sync_time', '6:00')?>" /></td>
    </tr>
    <tr>
        <th><?= __("pos_product_images", "Show product images on POS") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, 'pos_product_images') ?>
            <? else: ?>
                <?= form_checkbox("pos_product_images", "1",  get_business_meta($business->businessID, 'pos_product_images')) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("printer", "Printer Key") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, "printer_key") ?>
            <? else: ?>
                <?= form_input("printer_key", get_business_meta($business->businessID, "printer_key")) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("store", "Store Access Required") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->store_access ?>
            <? else: ?>
                <?= form_checkbox("store_access", "1",  $business->store_access) ?>
            <? endif; ?>
        </td>
    </tr>
    <tr>
        <th><?= __("track", "Track Wash and Fold") ?> </th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->wash_fold_tracking ?>
            <? else: ?>
                <?= form_checkbox("wash_fold_tracking", "1", $business->wash_fold_tracking) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("grouping", "MetalProgetti Grouping") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, 'metalproGrouping') ?>
            <? else: ?>
                <input type='text' name='metalproGrouping' value="<?= get_business_meta($business->businessID, 'metalproGrouping')?>" />
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("load_bags", "Load Bags on Conveyor") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= get_business_meta($business->businessID, 'loadBags') ?>
            <? else: ?>
                <?= form_checkbox("loadBags", "1",  get_business_meta($business->businessID, 'loadBags')) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("face", "Facebook Application Key") ?> </th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->facebook_api_key ?>
            <? else: ?>
                <?= form_input("facebook_api_key", $business->facebook_api_key) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("fb_secret", "Facebook API Secret Key") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= $business->facebook_secret_key ?>
            <? else: ?>
                <?= form_input("facebook_secret_key", $business->facebook_secret_key) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("bitly_client_id", "Bitly Client ID") ?> </th>
        <td>
            <? if ($no_edit): ?>
                <?= $bitly_client_id ?>
            <? else: ?>
                <?= form_input("bitly_client_id", $bitly_client_id) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("bitly_client_secret", "Bitly Client Secret") ?> </th>
        <td>
            <? if ($no_edit): ?>
                <?= $bitly_client_secret?>
            <? else: ?>
                <?= form_input("bitly_client_secret", $bitly_client_secret) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("bitly_access_token", "Bitly Access Token") ?> </th>
        <td>
            <? if ($no_edit): ?>
                <?= $bitly_access_token ?>
            <? else: ?>
                <?= form_input("bitly_access_token", $bitly_access_token) ?>
            <? endif ?>
            <br/><br/>You can  get the token by clicking in the following url: https://bitly.com/a/oauth_apps.
        </td>
    </tr>
    <tr>
        <th><?= __("open_facebook", "Open Registration with Facebook in a new tab") ?></th>
        <td> <?= form_checkbox("open_facebook_register_in_new_tab", "1", $business->open_facebook_register_in_new_tab) ?> </td>
    <tr>
        <th><?= __("terms", "Your Terms and Conditions") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= $business->terms ?>
            <? else: ?>
                <?= form_textarea("terms", $business->terms) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("sub", "Subdmain") ?></th>
        <td>
            <? if (!$is_superadmin): ?>
                <?= $business->subdomain ?>
            <? else: ?>
                <?= form_input("subdomain", $business->subdomain) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("dl_app", "Show in Droplocker iPhone App") ?></th>
        <td>
            <?= form_checkbox("show_in_droplocker_app", "1", $business->show_in_droplocker_app) ?>
        </td>
    </tr>
    <tr>
        <th><?= __("table_colors", "Table colors (Email)") ?></th>
        <td>
            <table>
                <tr>
                    <td><?= __("header_bgcolor", "Header Background Color:") ?></td>
                    <td><input type="text" name="header_bgcolor" value="<?= htmlentities($header_bgcolor)?>" style="width:200px;"></td>
                </tr>
                <tr>
                    <td><?= __("header_color", "Header Text Color:") ?></td>
                    <td><input type="text" name="header_color" value="<?= htmlentities($header_color)?>" style="width:200px;"></td>
                </tr>
                <tr>
                    <td><?= __("row_bgcolor", "Row Background Color:") ?></td>
                    <td><input type="text" name="row_bgcolor" value="<?= htmlentities($row_bgcolor)?>" style="width:200px;"></td>
                </tr>
                <tr>
                    <td><?= __("row_color", "Row Text Color:") ?></td>
                    <td><input type="text" name="row_color" value="<?= htmlentities($row_color)?>" style="width:200px;"></td>
                </tr>
                <tr>
                    <td><?= __("row_alt_bgcolor", "Row ALT Background Color:") ?></td>
                    <td><input type="text" name="row_alt_bgcolor" value="<?= htmlentities($row_alt_bgcolor)?>" style="width:200px;"></td>
                </tr>
                <tr>
                    <td><?= __("row_alt_color", "Row ALT Text Color:") ?></td>
                    <td><input type="text" name="row_alt_color" value="<?= htmlentities($row_alt_color)?>" style="width:200px;"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th><?= __("claim_deactivation", "Claim Automatic Deactivation") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= $business->claim_deactivation ?>
            <? else: ?>
                <?= form_yes_no("claim_deactivation", $claim_deactivation) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("inmediate_order_capture", "Capture Laundry Plans Order Inmediatelly") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= $business->inmediate_order_capture ?>
            <? else: ?>
                <?= form_yes_no("inmediate_order_capture", $inmediate_order_capture) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("active", "Active") ?></th>
        <td>
            <? if ($no_edit): ?>
                <?= ($business->active) ? 'Active' : 'Inactive'; ?>
            <? else: ?>
                <?= form_yes_no("active", $business->active); ?>
            <? endif; ?>
        </td>
    </tr>
    <tr>
        <th><?= __("Invoice Right Header", "Invoice Right Header") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= get_business_meta($this->business_id, "invoice_right_header", "") ?>
            <? else: ?>
                <?= form_textarea("invoice_right_header",  get_business_meta($this->business_id, "invoice_right_header", "")) ?>
            <? endif ?>
        </td>
    </tr>
    <tr>
        <th><?= __("Invoice PDF Message", "Invoice PDF Message") ?></th>
        <td>
            <? if($no_edit): ?>
                <?= get_business_meta($this->business_id, "invoice_pdf_message", "") ?>
            <? else: ?>
                <?= form_textarea("invoice_pdf_message",  get_business_meta($this->business_id, "invoice_pdf_message", "")) ?>
            <? endif ?>
        </td>
    </tr>
</table>



<br>
<input type='submit' name='submit' value=<?= __("update", "Update") ?> class='button orange' />
</form>
