<?php enter_translation_domain("admin/admin/bulk_pricing_update"); ?>
<h2><?php echo __("Bulk Pricing Update", "Bulk Pricing Update"); ?></h2>
<br>
<form action="/admin/admin/bulk_pricing_percent_run" method="post">
<?php echo __("Percent to increase all prices", "Percent to increase all prices."); ?>  <input type="text" name="percent" size="3">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="perLocation" value="1"><?php echo __("include per location pricing", 'Include <a href="/admin/admin/location_pricing" target="_blank">per location pricing</a>'); ?> &nbsp;&nbsp;&nbsp;<input type="submit" name="Run" value="<?php echo __("Run", "Run"); ?>" class="button orange">
</form>
<br>
<br>
<table class="box-table-a">
<tr>
    <th><?php echo __("Category", "Category"); ?></th>
    <th><?php echo __("Product", "Product"); ?></th>
    <th><?php echo __("Process", "Process"); ?></th>
    <th><?php echo __("Price", "Price"); ?></th>
</tr>
<? foreach ($products as $product): ?>
<tr>
    <td><?= $product->catName ?></td>
    <td><?= $product->prodName ?></td>
    <td><?= $product->procName ?></td>
    <td><?= format_money_symbol($this->business_id,'%.2n', $product->price) ?></td>

</tr>
<? endforeach; ?>
</table>