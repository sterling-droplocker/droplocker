<?php enter_translation_domain("admin/modules/edit_suplpier"); ?>
<h2><?php echo __("Edit Suppliers", "Edit Suppliers"); ?></h2>

<? if($suppliers):?>
    <?= form_open("/admin/products/set_supplier_product_properties/{$productProcess->product_id}/{$productProcess->product_processID}", array("class" => "set_supplier_product_properties")) ?>
        <table class="box-table-a">
            <thead>
                <tr>
                    <th width='25'><?php echo __("Default", "Default"); ?></th>
                    <th><?php echo __("Supplier", "Supplier"); ?></th>
                    <th width='50'><?php echo __("Cost", "Cost"); ?></th>
                </tr>
            </thead>
            <tbody>
                <? foreach($suppliers as $i => $supplier):
                    if (empty($supplier->supplierID)) {
                            continue;
                    }
                ?>
                    <tr data-supplier-index="<? echo $i; ?>">
                        <td align='center'><?= form_radio('default', $supplier->supplierID, $supplier->default); ?></td>
                        <td><b><?= $supplier->companyName; ?></b></td>
                        <td><input type="textbox" name="suppliers[<?= $supplier->supplierID ?>]" value="<?= round($supplier->cost, 2) ?>" /></td>
                    </tr>
                <? endforeach; ?>
            </tbody>
        </table>
        <br>
        <div style="text-align: right;">
            <input type="submit" value="<?php echo __("Update", "Update"); ?>" class="button orange">
        </div>
    <?= form_close(); ?>
<? else: ?>
    <p><?php echo __("No relationships with suppliers have been established", "No relationships with suppliers have been established"); ?></p>
<? endif; ?>
