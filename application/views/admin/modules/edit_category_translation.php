<?php enter_translation_domain("admin/modules/edit_category_translation"); ?>
<h2><?php echo __("Edit Translations", "Edit Translations"); ?></h2>

<? if($languages):?>
    <form method="post" action="/admin/modules/update_category_translations/<?= $productCategory->productCategoryID ?>" id="update_translation_<?= $productCategory->productCategoryID ?>">
        <table class="box-table-a">
            <thead>
                <tr>
                    <th width='25'><?php echo __("Language", "Language"); ?></th>
                    <th><?php echo __("Translation", "Translation"); ?></th>
                </tr>
            </thead>
            <tbody>
                <? foreach($languages as $businesLanguageID => $name):
                    $title = isset($productCategoryNames[$businesLanguageID]) ? $productCategoryNames[$businesLanguageID]->title : null;
                ?>
                <tr>
                    <td style="width:100px;"><label for="title_<?= $businesLanguageID ?>"><?= $name ?></label></td>
                    <td><?= form_input("title[$businesLanguageID]", $title, "id=\"title_{$businesLanguageID}\""); ?></td>
                </tr>
                <? endforeach; ?>
            </tbody>
        </table>
        <br>
        <div style="text-align: right;">
            <input type="submit" value="<?php echo __("Update", "Update"); ?>" class="button orange">
        </div>
    </form>
<? else: ?>
    <p><?php echo __("There are no languages defined for this business", "There are no languages defined for this business."); ?></p>
<? endif; ?>

<? if ($is_ajax): ?>
<script type="text/javascript">
$('#update_translation_<?= $productCategory->productCategoryID ?>').submit(function(evt) {
    evt.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serialize();
    var form = $(this);

    $.post(url, data, function (response) {
        if (response.status == 'success') {
            $(form).parent().dialog('close');
        }
        
        set_flash_message(response.status, response.message);
    });
});

function set_flash_message(type, message) {
    $('body > .message').remove();
    $('body').prepend('<div style="display:none"; class="message"><div class="'+type+'"><ul><li>'+message+'</li></ul></div></div>');
    $('body > .message').fadeIn();
    
    setTimeout(function() {
        $('body > .message').fadeOut(function() {
            $(this).remove();
        });
    }, 5000);
}
</script>
<? endif; ?>
