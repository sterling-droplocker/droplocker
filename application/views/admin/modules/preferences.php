<?php enter_translation_domain("admin/modules/preferences"); ?>
<?php

/**
 * Expects the following content variables
 *  module stdclass
 *  productCategories array An array of product categories froma codeigniter query result
 *  productCategory_options array
 *  module_id int
 */

$date_format = get_business_meta($this->business_id, "shortDateDisplayFormat"); //TODO: use internationalization

$bizzie_mode = in_bizzie_mode();
$can_edit = !$bizzie_mode || is_superadmin();
$in_developer_mode = in_developer_mode();

$taxGroups = array("" => __("Non Taxable", "Non Taxable")) + $taxGroups;
$processes = array("" => __("None", "None")) + $processes;
$upchargeGroups = array(0 => __("None", "None")) + $upchargeGroups;

$is_superadmin = is_superadmin();

$editableClass = $is_superadmin ? 'editable_productCategory editable':'';

$weight_unit = weight_system_format('', false, true);
$unitsOfMeasurement = array(
    "N/A" => __("N/A", "N/A"),
    $weight_unit => $weight_unit,
    "each" => __("each", "each"),
    "order" => __("order", "order"),
    "pcs" => __("pcs", "pcs"),
    "qty" => __("qty", "qty")
);

$types = array(
    'products' => __("Products", "Products"),
    'preferences' => __("Preferences", "Preferences"),
);

// don't show process for WF module or preferences
$show_process = $module_id == 3 && $type != 'preferences';

$is_product = $type != 'preferences';

?>
<style type="text/css">
    #sortable_productCategory {
        list-style-type: none;
        padding: 0;
        margin: 0;
    }

    #sortable_productCategory li {
        padding: 0;
        margin: 0 0 2em;
    }

    li.ui-sortable-helper {
        cursor: move;
    }

    .ui-sortable li {
        cursor: move;
        border: 1px solid;
        margin: 10px;
        padding: 10px;
        background-color: #E8E5E5;
    }

    .ui-sortable li .box-table-a,
    .ui-sortable li .editable {
        cursor : auto;
    }

    li table input, li table select {
        display: block;
        margin: 10px 0;
    }
    span form {
        display: inline;
    }
    span form input{
        display: inline;
        margin: 0px 5px !IMPORTANT;
        padding: 3px;

    }
    .add_productProcess {
        cursor: hand; cursor: pointer;
    }
    .editable {
        border: 1px transparent inset !IMPORTANT;
    }
    .editable:hover {
        border: 1px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3 !IMPORTANT;
    }
    tr.inactive td {
        background: #ff9999;
        display: none;
    }
    .cat-title {
        margin: 10px 0px;
        padding: 15px;
        font-size: 1.8em;
        display: inline-block;
        max-height: 25px;
        min-height: 25px;
    }
    
    .product_image_container {
        overflow-y: scroll;
        max-height: 300px;
    }
    
    .product_image_selector {
        width: 80px;
        border: 4px solid transparent;
        border-radius: 5px;
    }
    
    .product_image_selector.selected {
        border-color: blue;
    }
</style>

<h2> <?= $module->name ?> Module </h2>

<p style="overflow: auto;">
    <? foreach ($types as $prefix => $name): ?>
    <? if ($type == $prefix): ?>
        <span><?= $name ?></span> |
    <? else: ?>
        <a href="<?= "$base_url/$prefix/$master" ?>"><?= $name ?></a> |
    <? endif; ?>
    <? endforeach; ?>

    &nbsp;&nbsp;
    <?= form_checkbox(array("id" => "toggle_show_inactive_products", "checked" => FALSE)) . " " . form_label(__("Show Inactive", "Show Inactive"), "toggle_show_inactive_products") ?>
    <input class="orange button" style="float:right" type="button" id="add_productCategory" value="<?php echo __("Add New Product Category", "Add New Product Category"); ?>" />
</p>

<? if (!empty($productCategories)): ?>
    <ul id="sortable_productCategory">
        <? foreach ($productCategories as $key => $property):
            $module_id = $property['module_id'];
            $productCategory_id = $property['productCategoryID'];
            $tbody_id = $module_id."-".$productCategory_id."-".$business_id;
        ?>
            <li data-productCategoryID="<?=$productCategory_id?>">
                <span data-id="<?=$productCategory_id?>" data-property="name" class="editable_productCategory editable cat-title"><?= $property['categoryName']?></span>
                <a href="/admin/modules/edit_category_translation/<?= $productCategory_id ?>" class="ajax-dialog" style="display:inline-block;">
                    <img src="/images/icons/world_edit.png">
                </a>
                
                | <?php echo __("Slug", "Slug:"); ?> <span data-id="<?=$productCategory_id?>" data-property="slug" class="<?=$editableClass;?> cat-title"><?= $property['categorySlug']?></span>
                <? if ($type != 'products'): ?>
                | <label>Parent Category: <span data-id="<?=$productCategory_id?>" data-property="parent_id" class="editable_productCategory editable cat-title"><?= isset($productCategory_options[$property['parent_id']]) ? $productCategory_options[$property['parent_id']] : __("None", "--None--") ?></span></label>
                <? endif; ?>
                | <label><input type="checkbox" name="show_inactive"> <?php echo __("Show Inactive", "Show Inactive"); ?></label>
                <table class='box-table-a'>
                    <thead>
                        <tr>
                            <? if ($in_developer_mode): ?>
                                <th> <?php echo __("Product ID", "Product ID"); ?> </th>
                            <? endif ?>

                            <th width='200'><?php echo __("Name", "Name"); ?></th>
                            <th width='200'><?php echo __("Display Name", "Display Name"); ?></th>

                            <? if ($in_developer_mode): ?>
                                <th> <?php echo __("Product Process ID", "Product Process ID"); ?> </th>
                            <? endif ?>

                            <? if ($in_developer_mode): ?>
                                <th align="center" width="100"> <?php echo __("Product Process Price", "Product Process Price"); ?> </th>
                            <? else: ?>
                                <th align="center" width="100"> <?php echo __("Price", "Price"); ?> </th>
                            <? endif ?>

                            <th> <?php echo __("Tax Group", "Tax Group"); ?> </th>

                            <th align='center' width='100'><?php echo __("Unit", "Unit"); ?></th>

                            <? if ($show_process):  ?>
                            <th><?php echo __("Process", "Process"); ?></th>
                            <? endif; ?>

                            <th width='130'> <?php echo __("Type", "Type"); ?> </th>
                            <th width='30'> <?php echo __("Default Supplier", "Default Supplier"); ?> </th>

                            <? if ($can_edit): ?>
                                <th> <?php echo __("Active", "Active"); ?> </th>
                            <? endif ?>

                            <th align='center' width='100'><?php echo __("Default Cost", "Default Cost"); ?></th>

                            <? if ($can_edit): ?>
                                <th align='center' width='30'><?php echo __("Change Product Category", "Change Product Category"); ?></th>
                                <? if ($show_process):  ?>
                                <th align="center" width="30"> <?php echo __("Add Product Process", "Add Product Process"); ?> </th>
                                <? endif; ?>
                                <? if ($is_product): ?>
                                <th align="center" width="30"> <?php echo __("Upcharge Group", "Upcharge Group"); ?> </th>
                                <? endif; ?>
                            <? endif ?>
                            <? if ($is_product): ?>
                            <th align='center' width='30'> <?php echo __("Width", "Width"); ?> </th>
                        <? endif; ?>
                            <th align='center' width='30'> <?php echo __("Last Used", "Last Used"); ?> </th>
                            <? if ($in_developer_mode): ?>
                                <th> <?php echo __("Base ID", "Base ID"); ?> </th>
                            <? endif ?>
                            <th align='center' width='30'> <?php echo __("Image", "Image"); ?> </th>
                        </tr>
                    </thead>
                    <tbody class='tbody_sort' id='tbody-<?= $tbody_id?>'>
                    <? foreach($property['products'] as $product):
                        $row_id = $tbody_id."-".$product['productID'];
                        if ($product['active'] == 1) {
                            $class = "active";
                        } else {
                            $class = "inactive";
                        }

                        $company_result = "Add Company";
                        $supplier_cost = "0.00";
                        if (!empty($product['supplierID'])) {
                            $company_result = $product['supplier'];
                            $supplier_cost = $product['supplier_cost'];
                        }
                    ?>
                        <tr id='<?= $productCategory_id."-".$product['productID']?>' class="<?=$class?>">
                            <? if ($in_developer_mode): ?>
                                <td>
                                    <?= $product['productID']?>
                                </td>
                            <? endif ?>
                            <? if ($can_edit): ?>
                                <td class="edit_product_field" data-id="<?=$product['productID']?>" data-property="name">
                                    <?= __($product['name'], $product['name']) ?>
                                </td>
                            <? else: ?>
                                <td>
                                    <?= __($product['name'], $product['name']) ?>
                                </td>
                            <? endif ?>
                            <? if ($can_edit): ?>
                                <td>
                                    <a href="/admin/modules/edit_translation/<?= $product['productID'] ?>" class="ajax-dialog" style="float:right;">
                                        <img src="/images/icons/world_edit.png">
                                    </a>
                                    <div class="edit_product_field" data-id="<?= $product['productID']?>" data-property="displayName">
                                        <?= __($product['displayName'], $product['displayName']) ?>
                                    </div>
                                </td>
                            <? else: ?>
                                <td>
                                    <?= __($product['displayName'], $product['displayName']) ?>
                                </td>
                            <? endif ?>
                            <? if ($in_developer_mode): ?>
                                <td>
                                    <?= $product['product_processID'] ?>
                                </td>
                            <? endif ?>
                            <td class="edit_product_process_field" data-id="<?= $product['product_processID'] ?>" data-property="price">
                                <?= format_money_symbol($business_id, '%.2n', $product['price']); ?>
                            </td>
                            <? if ($can_edit): ?>
                                <td class="edit_product_process_field" data-id="<?= $product['product_processID'] ?>" data-property="taxGroup_id">
                                    <?= isset($taxGroups[$product['taxGroup_id']]) ? $taxGroups[$product['taxGroup_id']] : $taxGroups[""] ?>
                                </td>
                            <? else: ?>
                                <td>
                                    <?= $taxGroups[$product['taxGroup_id']]; ?>
                                </td>
                            <? endif ?>
                            <? if ($can_edit): ?>
                                <td class="edit_product_field" data-id="<?=$product['productID']?>" data-property="unitOfMeasurement">
                                    <?= $product['unit']; ?>
                                </td>
                            <? else: ?>
                                <td>
                                    <?= $product['unit'] ?>
                                </td>
                            <? endif ?>
                            <? if ($show_process):  ?>
                            <td class="edit_product_process_field" data-id="<?= $product['product_processID'] ?>" data-property="process_id">
                                <?= $product['process'] ? $product['process'] : $processes[""]; ?>
                            </td>
                            <? endif; ?>
                            <? if ($can_edit): ?>
                                <td class="edit_product_field" data-id="<?=$product['productID']?>" data-property="productType">
                                    <?= $product['productType'] ?>
                                </td>
                            <? else: ?>
                                <td nowrap="true">
                                    <?= $product['productType'] ?>
                                </td>
                            <? endif ?>
                            <td align="center">
                                <a href="/admin/modules/edit_suppliers/<?= $product['product_processID'] ?>" class="ajax-dialog">
                                    <?= $company_result; ?>
                                </span>
                            </td>
                            <? if ($can_edit): ?>
                                <td>
                                    <?= form_checkbox(array(
                                            "class" => "edit_checkbox",
                                            "name" => "active",
                                            "checked" => $product['active'],
                                            "data-id" => $product['product_processID']
                                        )) ?>
                                </td>
                            <? endif ?>
                            <td align='center' nowrap="true" class="supplier_cost">
                                <?= format_money_symbol($business_id, '%.2n', $supplier_cost); ?>
                            </td>
                            <? if ($can_edit): ?>
                                <td> <img style="cursor: pointer;" src="/images/icons/application_form_edit.png" data-id="<?= $product['productID']?>" class="change_productCategory" alt="<?php echo __("Change Product's Category", "Change Product's Category"); ?>" /> </td>

                                <? if ($show_process):  ?>
                                <td> <img class="add_productProcess" alt="<?php echo __("Add new process for product", "Add new process for product"); ?>" src="/images/icons/add.png" data-productID="<?= $product['productID'] ?>" /> </td>
                                <? endif; ?>

                                <? if ($is_product): ?>
                                <td class="edit_product_field" data-id="<?=$product['productID']?>" data-property="upchargeGroup_id">
                                    <?= !empty($product['upchargeGroupName']) ? $product['upchargeGroupName'] : "--"; ?>
                                </td>
                            <? endif; ?>
                            <? endif ?>

                            <? if ($is_product): ?>
                            <td class="edit_product_field" data-id="<?= $product['productID']?>" data-property="width">
                                <?= $product['width'] ?>
                            </td>
                            <? endif; ?>
                            <td>
                                <?= empty($product['last_used']) ? __("Never", "-- Never --") : convert_from_gmt_aprax($product['last_used'], $date_format, $business_id); ?>
                            </td>
                            <? if ($in_developer_mode): ?>
                                <td> <?= $product['base_id'] ?> </td>
                            <? endif ?>
                            <td>
                                <? if ($product['image_id']): ?>
                                    <img data-id="<?=$product['productID']?>" data-image-id="<?=$product['image_id']?>" src="<?php echo $product_image_path . $product['image']; ?>" class="update-image" style="max-width:100px;cursor:pointer;" title="<?= __("Change Image", "Change Image") ?>"/>
                                <? else: ?>
                                <img data-id="<?=$product['productID']?>" src="/images/icons/add.png" class="update-image" title="<?= __("Add Image", "Add Image") ?>" style="cursor:pointer;"/>
                                <? endif ?>
                            </td>
                        </tr>
                        <? endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="14">
                                    <div style="cursor: hand; cursor: pointer;" data-productCategory_id="<?= $productCategory_id ?>" data-name="<?=$property['categoryName']?>" class='add_product'><img src='<?= CDNIMAGES?>icons/add.png' /> Add</div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
            </li>
        <? endforeach; ?>
    </ul>
<? endif; ?>


<!-- The following code shows the product categories that have no products -->
<? foreach($productCategories_with_no_products as $productCategory): ?>
    <br />
    <h3><?= $productCategory->name?></h3>
    <table class='box-table-a'>
        <thead>
        <tr>
            <th> <?php echo __("Name", "Name"); ?> </th>
            <th> <?php echo __("Display Name", "Display Name"); ?> </th>
            <th> <?php echo __("Price", "Price"); ?> </th>
            <th> <?php echo __("Unit", "Unit"); ?> </th>
            <th> <?php echo __("Process", "Process"); ?> </th>
            <th> <?php echo __("Type", "Type"); ?> </th>
            <th> <?php echo __("Default Supplier", "Default Supplier"); ?> </th>
            <th> <?php echo __("Active", "Active"); ?> </th>
            <th> <?php echo __("Default Cost", "Default Cost"); ?> </th>
            <th> <?php echo __("Change Product Category", "Change Product Category"); ?> </th>
            <th> <?php echo __("Add Product Process", "Add Product Process"); ?> </th>
            <th> <?php echo __("Upcharge Group", "Upcharge Group"); ?> </th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="12"> <?php echo __("There are no products in this product category", "There are no products in this product category"); ?> </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="12">
                    <div style="cursor: hand; cursor: pointer;" data-productCategory_id="<?= $productCategory->productCategoryID ?>" data-name="<?=$productCategory->name?>" class='add_product'><img src='<?= CDNIMAGES?>icons/add.png' /> <?php echo __("Add", "Add"); ?></div>
                </td>
            </tr>
        </tfoot>
    </table>
<? endforeach; ?>

<? if (empty($productCategories) && empty($productCategories_with_no_products)): ?>
    <p><?= __("There are no product categories defined for this module", "There are no product categories defined for this module. To start, add a product category to this module and add products to that category.") ?> </p>
<? endif ?>

<!-- This div container is used for displaying the interface for changing a product's category. -->
<div style="display: none;" id="change_productCategory_dialog">
    <?= form_dropdown("new_productCategory_id", $productCategory_options, "", "id='new_productCategory_id'") ?>
</div>

<!-- This div container is used for displaying the interface for adding a new product. -->
<!-- Note, the productCategory_id hidden element value is set at the time the button is clicked by the .add_product click handler -->
<div style="display: none; text-align: left;" id="create_product_dialog">
    <?= form_open($add_product_action) ?>
    <?= form_input(array("name" => "productCategory_id", "id" => "productCategory_id", "type" => "hidden")) ?>
    <?= form_input(array("name" => "product_id", "id" => "product_id", "type" => "hidden")) ?>
<table>
    <tr>
        <td><?= form_label(__("Product Name", "Product Name"), "new_product_name")?></td>
        <td><?= form_input(array("name" => "name", "id" => "new_product_name")) ?></td>
    </tr>
    <tr>
        <td><?= form_label(__("Product Display Name", "Product Display Name"), "new_product_displayName") ?></td>
        <td><?= form_input(array("name" => "displayName", "id" => "new_product_displayName")) ?></td>
    </tr>
    <tr>
        <td><?= form_label(__("Units", "Units"), "new_product_unitOfMeasurement") ?></td>
        <td><?= form_dropdown("unitOfMeasurement", $units, "", "id='new_product_unitOfMeasurement'") ?></td>
    </tr>
    <tr>
        <td><?= form_label(__("Notes", "Notes"), "new_product_notes") ?></td>
        <td><?= form_input(array("name" => "notes", "id" => "new_product_notes")) ?></td>
    </tr>
    <tr>
        <td><?= form_label(__("Product Type", "Product Type"), "new_productType") ?></td>
        <td><?= form_dropdown("productType", $productTypes, "product", "id='new_productType'") ?></td>
    </tr>
    <? if ($show_process):  ?>
    <tr>
        <td><?= form_label(__("Process", "Process"), "new_product_process") ?></td>
        <td><?= form_dropdown("process_id", $processes, "", "id='new_product_process'") ?></td>
    </tr>
    <? endif; ?>
    <tr>
        <td><?= form_label(__("Price", "Price"), "new_product_price") ?></td>
        <td><?= form_input(array("name" => "price", "value" => "0.00", "id" => "new_product_price")) ?></td>
    </tr>
    <tr>
        <td><?= form_label(__("Upcharge Group", "Upcharge Group"), "new_product_upchargeGroup") ?></td>
        <td><?= form_dropdown("upchargeGroup_id", $upchargeGroups, "", "id='new_product_upchargeGroup'") ?></td>
    </tr>

    <tr class="product-image-row">
        <td><?= form_label(__("Product Image", "Product Image"), "new_product_productImage") ?></td>
        <td>
            <input type="hidden" name="image_id" id="product_image"/>
            <div class="product_image_container">
            <? foreach($productImages as $image): ?>
                <img src="<?php echo $product_image_path . $image->filename; ?>" data-image-id="<?=$image->imageID?>" class="product_image_selector" />
            <? endforeach; ?>
            </div>
        </td>
    </tr>

    <tr class="submit-buttons">
        <td></td>
        <td><br><?= form_submit(array("class" => "orange button"), __("Add Product", "Add Product")); ?></td>
    </tr>
</table>
    <?= form_close() ?>
</div>

<!-- This div container is used for adding a new product category -->
<div style="display: none; text-align: left;" id="create_productCategory_dialog">
    <?= form_open("/admin/product_categories/add") ?>
        <?= form_hidden("module_id", $module->moduleID) ?>
        <table>
            <tr>
                <td><?= form_label(__("Name", "Name")) ?></td>
                <td><?= form_input(array("name" => "name", "id" => "new_productCategory_name")) ?></td>
            </tr>
            <tr>
                <td><?= form_label(__("Note", "Note")) ?></td>
                <td><?= form_input(array("name" => "note", "id" => "new_productCategory_note")) ?></td>
            </tr>
        </table>
        <?= form_submit(array("class" => "orange button"), __("Add Product Category", "Add Product Category")); ?>
    <?= form_close() ?>
</div>

<!-- This div container is used for adding a process for a product -->
<div style="display: none; text-align: left;" id="add_productProcess_dialog">
    <?= form_open("/admin/product_process/add_process") ?>
        <input type="hidden" name="product_id" value="" />
        <?= form_label(__("New Product Process", "New Product Process"), "new_process_id") ?>
        <?= form_dropdown("process_id", $processes, "", "id='new_process_id'") ?>
        <?= form_submit(array("class" => "orange button"), __("Add Product Process", "Add Product Process")) ?>
    <?= form_close() ?>
</div>


<script type="text/javascript">
var add_product_action = "<?= $add_product_action ?>";

function resetAddProductAction() {
    setAddProductAction(add_product_action);
};
    
function setAddProductAction(action) {
    $("#create_product_dialog form").attr("action", action);
};

var cdnimages = "<?= CDNIMAGES ?>";

var select_options = {};
select_options["unitOfMeasurement"] = <?= json_encode($unitsOfMeasurement)?>;
select_options["productType"] = <?= json_encode($productTypes); ?>;
select_options["upchargeGroup_id"] = <?= json_encode($upchargeGroups)?>;
select_options["taxGroup_id"] = <?= json_encode($taxGroups); ?>;
select_options["parent_id"] = <?= json_encode($productCategory_options); ?>;
select_options["process_id"] = <?= json_encode($processes); ?>;


$(document).ready( function() {

    $(".editable_productCategory").each(function(i, e) {
        var options = {
            cancel : "<img src='/images/icons/delete.png' />",
            submit : "<img src='/images/icons/accept.png' />",
            indicator : "Saving ... <img src='/images/progress.gif' />",
            data : function(string) {
                return $.trim(string)
            },
            submitdata : function ( value, settings ) {
                return {
                    "property" : $(this).attr("data-property"),
                    "productCategoryID": $(this).attr("data-id")
                }
            }
        };

        var property = $(e).attr('data-property');
        if (typeof(select_options[property]) != 'undefined') {
            options.data = select_options[property];
            options.type = 'select';
        }

        $(e).editable("/admin/product_categories/update", options);
    });

    $(".edit_checkbox").change(function() {
        var checked = $(this).is(":checked") ? 1 : 0;
        var $image = $("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $image.insertAfter($(this));

        $.post("/admin/product_process/update", {
                "product_processID" : $(this).attr("data-id"),
                "property" : $(this).attr("name"),
                "value" : checked
            },
            function(result) {
                if (result == checked) {
                    $image.remove();
                } else {
                    $("<span> <?php echo __("The following error occurred", "The following error occurred:"); ?> " + result + "</span>").insertAfter($image);
                }
            }
        );
    });

    $(".edit_product_process_field").each(function(i, e) {
        var options = {
            select : true,
            "cancel": "<img src='/images/icons/delete.png' />",
            "submit": "<img src='/images/icons/accept.png' />",
            "indicator": "<?php echo __("Saving", "Saving ..."); ?> <img src='/images/progress.gif' />",
            "data": function(string) {
                //remove non-numeric characters when eidting the price field.
                if ($(this).attr("data-property") == "price") {
                    trimmed_string = $.trim(string);
                    var number = trimmed_string.replace(/,/, '.').replace(/[^0-9\.]+/g, "");
                    return number;
                } else {
                    return $.trim(string);
                }
            },
            "onsubmit": function (settings,original) {
                if ($(original).attr("data-id")) {
                    return true;
                } else {
                    alert("<?php echo __("You must add a product process for this product", "You must add a product process for this product"); ?>");
                    return false;
                }
            },
            "submitdata": function ( value, settings ) {
                return {
                    "property": $(this).attr("data-property"),
                    "product_processID": $(this).attr("data-id")
                }
            },
            "onerror": function (settings, original, xhr) {
                original.reset();
                alert(xhr.responseText);
            }
        };

        var property = $(e).attr('data-property');
        if (typeof(select_options[property]) != 'undefined') {
            options.data = select_options[property];
            options.type = 'select';
        }

        $(e).editable("/admin/product_process/update", options);
    });

    $(".edit_product_field").each(function(i, e) {
        var options = {
            select : true,
            "cancel": "<img src='/images/icons/delete.png' />",
            "submit": "<img src='/images/icons/accept.png' />",
            "indicator": "<?php echo __("Saving", "Saving ..."); ?> <img src='/images/progress.gif' />",

            "data": function(string) {return $.trim(string)},
            "submitdata": function ( value, settings ) {
                return {
                    "property": $(this).attr("data-property"),
                    "productID": $(this).attr("data-id")
                }
            },
            "onerror": function (settings, original, xhr) {
                original.reset();
                alert(xhr.responseText);
            }
        };

        var property = $(e).attr('data-property');
        if (typeof(select_options[property]) != 'undefined') {
            options.data = select_options[property];
            options.type = 'select';
        }

        $(e).editable("/admin/products/update", options);

    });

    // validate the form for creating a new product category.
    $("#create_productCategory_dialog form").validate({
        rules : {
            "name" : {
                required: true
            },
            "note" : {
                required: true
            },
            "slug" : {
                required: true
            }
        },
        errorClass: "invalid"
    });

    // validate the form for creating a new product.
    $("#create_product_dialog form").validate({
       rules : {
           "name" : {
               required: true
           },
           "displayName" : {
               required: true
           }
       },
       errorClass : "invalid"
    });

    // Display the dialog for adding a new product category.
    $("#add_productCategory").click(function() {
        $("#create_productCategory_dialog").dialog({
                title: "<?php echo __("Create Product Category", "Create Product Category"); ?>",
                modal: true
            }
        );
    });

    // Display the dialog for adding a new product proces to a product
    $(".add_productProcess").click(function() {
        $("#add_productProcess_dialog form [name='product_id']").val($(this).attr("data-productID"));
        $('#add_productProcess_dialog').dialog({
                title: "<?php echo __("Add Another Product Process for this Product", "Add Another Product Process for this Product"); ?>",
                modal: true
            }
        );
    });
    
    function resetProductDialog() {
        resetImageSelector();
        $("#create_product_dialog #product_id").val(null);
        $("#create_product_dialog tr").show();
        resetAddProductAction();
    }
    
    function resetImageSelector() {
        var selectedClass = "selected";
        $(".product_image_selector").removeClass(selectedClass);
        
        // Set the hidden product image 
        $("#product_image").val(null);
    }
    
    // Display the dialog to asign a new image to a product
    // recycle add product dialog
    $(".update-image").click(function() {
        resetImageSelector();
        
        var product_id = $(this).attr("data-id");
        var image_id = $(this).attr("data-image-id");
        
        $("#create_product_dialog #product_id").val(product_id);
        $("#create_product_dialog .product_image_selector[data-image-id='" + image_id + "']").click();
        $("#create_product_dialog tr:not(.product-image-row):not(.submit-buttons)").hide();
        setAddProductAction('<?= $update_image_action ?>');

        $("#create_product_dialog").dialog({
                title : "<?php echo __("Assign image", "Assign image"); ?> ",
                width: 500,
                modal: true
        });
    });

    // Display the interface for adding a new product.
    $(".add_product").click( function() {
        resetProductDialog();
        // Set the hidden product category ID based on which 'add' button the user clicked.
        $("#productCategory_id").val($(this).attr("data-productCategory_id"));

        $("#create_product_dialog").dialog({
                title : "<?php echo __("Create new product for", "Create new product for"); ?> '" + $(this).attr("data-name") + "' <?php echo __("category", "categoryr"); ?>",
                width: 500,
                modal: true
        });
    });

    // Selects product image.
    $(".product_image_selector").click( function() {
        var selectedClass = "selected";
        $(".product_image_selector").removeClass(selectedClass);
        
        // Set the hidden product image 
        $("#product_image").val($(this).attr("data-image-id"));
        $(this).addClass(selectedClass);
    });

    $(".change_productCategory").click(function() {
        product_id = $(this).attr("data-id");
        $("#change_productCategory_dialog").dialog({
            modal: true,
            title: "Change product category",
            buttons: {
               "Ok" : function() {
                   $.post("/admin/products/update_productCategory", {
                            product_id: product_id,
                            productCategory_id : $("#new_productCategory_id").val()
                        },
                        function (response) {
                            if (response['status'] == "success")
                                window.location.reload(true);
                        },
                        "json"
                   );
                   $(this).dialog("close");
               },
               "Cancel" : function() {
                   $(this).dialog("close");
               }
           }
        });

    });

    $('.ajax-dialog').click(function(evt) {
        evt.preventDefault();
        var url = $(this).attr('href');
        var div_id = this.href.replace(/[^0-9a-z]/gi, '');

        $.get(url, function(response) {
            if (!$('#' + div_id).length) {
                $('<div id="'+div_id+'" style="display:none;"></div>').appendTo('body');
            }

            $('#' + div_id).html(response);
            var title = $('h2:first', '#' + div_id).remove().text();

            $('#' + div_id).dialog({
                modal: true,
                width: 400,
                title: title
            });

            if (url.indexOf("edit_suppliers") > -1) {
                $('#' + div_id + ' form').submit(function() {
                    $(this).ajaxSubmit({
                        success: function (response) {
                            supplier_name = $('#' + div_id + ' form input:checked').parent('td').next().find('b').text();
                            supplier_cost = $('#' + div_id + ' form input:checked').parent('td').parent('tr').find('input[type="textbox"]').val();
                            link = $('a[href="' + url  + '"]');
                            row = link.parent('td').parent('tr');
                            link.text(supplier_name);

                            supplier_cost_td = row.find('.supplier_cost');
                            supplier_cost_items = supplier_cost_td.text().match(/\S+/g);

                            for (i=0; i<= supplier_cost_items.length; i++) {
                                if ($.isNumeric(supplier_cost_items[i])) {
                                    supplier_cost_items[i] = supplier_cost;
                                }
                            }

                            supplier_cost_td.text(supplier_cost_items.join(" "));

                            $('#' + div_id).dialog('close')

                        }
                    });

                    return false;
                });
            }
        });

    });


    $("#toggle_show_inactive_products").click(function() {
        if ($(this).attr('checked')) {
            $("#sortable_productCategory tr.inactive > td").css({display:'table-cell'});
        } else {
            $("#sortable_productCategory tr.inactive > td").css({display:''});
        }
    });

    $('input[name=show_inactive]').click(function(evt) {
        var li = $(this).closest('li');

        if ($(this).attr('checked')) {
            $("tr.inactive > td", li).css({display:'table-cell'});
        } else {
            $("tr.inactive > td", li).css({display:''});
        }

    });

    <? if ($type != 'products'): ?>
    // intialize the sortable funcitonality
    $(".tbody_sort").sortable({
        stop : function(event, ui) {
            /*
            var id = ui.item[0].id;
            var arr = id.split("-");
            var cat_id = arr[0];
            var product_id = arr[1];
            */
            var result = $(this).sortable('toArray');
            $.ajax({
                url: "/admin/products/update_product_sort_order_ajax",
                data: {items: result},
                type: "post"
            });
        }
    });

    $("#sortable_productCategory").sortable({
        stop : function(event, ui) {
            var productCategories = $("#sortable_productCategory").sortable("toArray", {
                attribute : "data-productCategoryID"
            });

            $.post("/admin/product_categories/update_sort_order", {
                "productCategories" : productCategories
            }, function(result) {
                if (result['status'] != "success") {
                    if (result['status'] == "error") {
                        alert($result['message']);
                    } else {
                        alert("<?php echo __("An error occurred updating the product category sort order", "An error occurred updating the product category sort order."); ?>");
                    }
                }
            }, "JSON");
        }
    });
    <? endif; ?>

}); //End of $document.ready
</script>
