<?php enter_translation_domain("admin/superadmin/edit_sms"); ?>

<?= $this->session->flashdata('message'); ?>
<h2><?php echo __("Edit SMS", "Edit SMS ::"); ?> <?= $name ?></h2>
<p><?php echo __("SMS template support merge values", "SMS template support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?></p>
<br />
<form action='' method='post'>
<table class='detail_table'>
	
    <tr>
        <th><?php echo __("SMS Message", "SMS Message"); ?></th>
        <td><textarea style='width:100%; height:100px; border:1px solid #ccc;' name='message'><?= $message?></textarea></td>
    </tr>
</table>
	<input type="hidden" value='<?= $sms->emailActionID?>' name='emailActionID' />
	<input type="submit" name='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' />
</form>