<?php enter_translation_domain("admin/superadmin/view_active_accounts"); ?>
<?php 

/* $fieldList = array();
$fieldList['businessid'] = '<th>' . __("businessid", "businessid") . '</th>';
$fieldList['companyname'] = '<th>' . __("companyname", "companyname") . '</th>';
            
$nameHeader = getSortedNameForm($fieldList); */

?><script type="text/javascript">
$(document).ready(function() {
    /**
    $("#employee_table").dataTable(
    }).columnFilter();
        */
       $("#business_orders_table").dataTable(
        {
            "iDisplayLength" : 25,
            "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
        }).columnFilter({
            sPlaceHolder: "head:before",
            aoColumns: [
                null,null,{ type: "select"}, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
            ]
    });
    
});
</script>
<?php
/* 
$fieldList = array();
$fieldList['yearDue'] = "<th> ". __("yearDue", "yearDue") . "</th>";
$fieldList['monthDue'] = "<th> ". __("monthDue", "monthDue") . "</th>";
$fieldList['closedGross'] = "<th> ". __("closedGross", "closedGross") . "</th>";
$fieldList['closedDisc'] = "<th> ". __("closedDisc", "closedDisc") . "</th>";
$fieldList['tax'] = "<th> ". __("tax", "tax") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['yearDue'] = "<td>{$location->address}</td>";
    $fieldList['monthDue'] = "<td>{$location->address2}</td>";
    $fieldList['closedGross'] = "<td>{$location->city}</td>";
    $fieldList['closedDisc'] = "<td>{$location->state}</td>";
    $fieldList['tax'] = "<td>{$location->zipcode}</td>";

    return getSortedAddressForm($fieldList);
} */
        
?>
<h2><?php echo __("Active Accounts", "Active Accounts"); ?></h2>

<table id="business_orders_table">
    <thead>
        <tr>
            
            <th> <?php echo __("Business", "Business"); ?> </th>
           
            <th> <?php echo __("Year Due", "Year Due"); ?> </th>
            <th> <?php echo __("Mth Due", "Mth Due"); ?> </th>
            <th> <?php echo __("Closed Gross", "Closed Gross"); ?> </th>
            <th> <?php echo __("Closed Disc", "Closed Disc"); ?> </th>
            <th> <?php echo __("Tax", "Tax"); ?> </th>
            <th> <?php echo __("Orders", "Orders"); ?> </th>
            
            
            
        </tr>
    </thead>
    <tbody>
            <? foreach($business_orders as $business_order):?>
            <tr>
                <!-- <td align="center" width='25'></td>
                <td align="center" width='25'></td>
                <td> </td> -->
                <td> <?= $business_order->companyname ?> </td>
                <td> <?= $business_order->yearDue ?> </td>
                <td> <?= $business_order->mthDue ?> </td>
                <td> <?= $business_order->closedGross ?> </td>
                <td> <?= $business_order->closedDisc ?> </td>
                <td> <?= $business_order->tax ?> </td>
                <td> <?= $business_order->orders ?> </td>
             
               
                
            </tr>
            <? endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>