<?php enter_translation_domain("admin/superadmin/shipment_edit"); ?>
<h2><?= $this->input->post('new') == 1 ? 'Create' : 'Edit' ?> <?php echo __("Shipment", "Shipment"); ?></h2>
<br>

<form action="/admin/superadmin/shipment_update" method="post">
<? if($this->input->post('new') == 1): ?>
<input type="hidden" name="new" value="1">
<? else: ?>
<input type="hidden" name="edit" value="<?= $this->input->post('edit') ?>">
<? endif; ?>

<table border="0">
<tr>
    <td><?php echo __("Status", "Status"); ?></td>
    <td colspan="3"><select name="status">
    	<option value="placed" <?= $shipment->status == 'placed' ? 'SELECTED': '' ?>><?php echo __("Order Placed", "Order Placed"); ?></option>
        <option value="manufacturing" <?= $shipment->status == 'manufacturing' ? 'SELECTED': '' ?>><?php echo __("Manufacturing", "Manufacturing"); ?></option>
    	<option value="onWater" <?= $shipment->status == 'onWater' ? 'SELECTED': '' ?>><?php echo __("Order On Water", "Order On Water"); ?></option>
    	<option value="onTruck" <?= $shipment->status == 'onTruck' ? 'SELECTED': '' ?>><?php echo __("Order On Truck", "Order On Truck"); ?></option>
    	<option value="completed" <?= $shipment->status == 'completed' ? 'SELECTED': '' ?>><?php echo __("Completed", "Completed"); ?></option>
    </select></td>
</tr>
<tr>
    <td><?php echo __("What is shipping", "What is shipping"); ?></td>
    <td colspan="3"><input type="text" name="what" value="<?= $shipment->what ?>"></td>
</tr>
<tr>
    <td><?php echo __("Customer", "Customer"); ?></td>
    <td colspan="3"><input type="text" name="customer" value="<?= $shipment->customer ?>"></td>
</tr>
<tr>
    <td><?php echo __("PO Number", "PO Number"); ?></td>
    <td colspan="3"><input type="text" name="po" value="<?= $shipment->po ?>"></td>
</tr>
<tr>
    <td><?php echo __("Invoice Number", "Invoice Number"); ?></td>
    <td colspan="3"><input type="text" name="invoice" value="<?= $shipment->invoice ?>"></td>
</tr>
<tr>
    <td><?php echo __("Order Placed", "Order Placed"); ?></td>
    <td><input type="text" name="placedDate" value="<?= $shipment->placedDate ?>"></td>
    <td><?php echo __("Estimated Ship Date", "Estimated Ship Date"); ?></td>
    <td><input type="text" name="estShipDate" value="<?= $shipment->estShipDate ?>"></td>
</tr>
<tr>
    <td><?php echo __("Order Shipped", "Order Shipped"); ?></td>
    <td><input type="text" name="shipDate" value="<?= $shipment->shipDate ?>"></td>
    <td><?php echo __("Estimated Delivery Date", "Estimated Delivery Date"); ?></td>
    <td><input type="text" name="estDelDate" value="<?= $shipment->estDelDate ?>"></td>
</tr>
<tr>
    <td><?php echo __("Notes", "Notes"); ?></td>
    <td colspan="3"><textarea cols="55" rows="4" name="notes"><?= $shipment->notes ?></textarea></td>
</tr>
<tr>
    <td colspan="4" align="right"><input type="submit" name="update" value="<?= $this->input->post('new') == 1 ? __("Create", "Create") : __("Update", "Update") ?>" class="button blue"></td>
</tr>
</table>
</form>
