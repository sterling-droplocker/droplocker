<?php enter_translation_domain("admin/superadmin/product_import"); ?>
<h2><?php echo __("Import Products", "Import Products"); ?></h2>
<br>
<a href="../../../../images/sampleImport.csv"><?php echo __("Sample Import File", "Sample Import File."); ?></a>  <?php echo __("Make sure it is .CSV and has a header row", "Make sure it is .CSV and has a header row.<br>- make sure you have a header row that reads Product, Category, Price<br>- make sure your price has 2 decimal points<br>"); ?>
<br>
<?= form_open_multipart("/admin/superadmin/product_import_run")?>
<?php echo __("Import only to:", "Import only to:"); ?>
<?php 
foreach ($processes as $process_id=>$name): 
?>
    <?=$name ?><input type="checkbox" name="process[<?=$process_id?>]" value="<?=$name ?>" checked="true" />
<?php 
endforeach; 
?>
<br>
<br>
<br>
<?php echo __("Attach CSV file here", "Attach CSV file here:"); ?><br><br>
<?= form_upload("importFile") ?>
<br><br>
<input type="submit" name="Import" value="<?php echo __("Run Import", "Run Import"); ?>" class="button orange">
<?= form_close()?>