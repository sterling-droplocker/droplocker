<?php enter_translation_domain("admin/superadmin/edit_email"); ?>
<?php 
/**
 * Expects at least the following content variables
 *  $subject string
 *  $message string
 *  $textBody string
 *  $email stdClass
 *  $employee stdclass
 *  $emailActionID 
 */
?>
<script type="text/javascript">
    window.onload = function()
    {

        CKEDITOR.replace( 'body', {
            uiColor : false,
            toolbar : 'MyToolbar',
            fullPage: true,
            allowedContent: true
        });
    };
</script>
<h2><?php echo __("Edit Email Template for all Businesses", "Edit Email Template for all Businesses ::"); ?> <?= $name?></h2>
<p><?php echo __("Email template support merge values", "Email template support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?></p>
<br />

<form action='' method='post'>
    <h3><?php echo __("Subject", "Subject"); ?></h3>
    <input style='width:100%;font-size:14px; padding:3px; border:1px solid #ccc;' type="text" id='subject' name='subject' value="<?= $subject?>" />
    <br /><br />
    <h3><?php echo __("HTML Body", "HTML Body"); ?></h3>
    <textarea id='body' name='body'><?= $message?></textarea>
    <br /><br />
    <h3><?php echo __("Text Body", "Text Body"); ?></h3>
    <textarea style='width:100%; height:200px; border:1px solid #ccc;' id='bodytext' name='bodytext'><?= $textBody?></textarea>

    <input type="hidden" value='<?= $email->emailTemplateID?>' name='insert' />
    <input type="submit" name='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' />
</form>



<div style='background-color:#eaeaea; padding:10px; margin:10px 0px; border:1px solid #333'>
    <h3><?php echo __("Send Test", "Send Test"); ?></h3>
    <?php echo __("This will send the email to the user that is currently logged in", "This will send the email to the user that is currently logged in."); ?> (<?= $employee->email?>) 
    <span style='color:#cc0000'><?php echo __("Make sure the template is saved", "Make sure the template is saved!"); ?></span> 
    <?= form_open("/admin/emails/send_test_email") ?>
        <?= form_hidden("emailActionID", $emailActionID) ?>
        <?= form_submit(array("class" => "button orange", "style" => "margin-top: 10px;", "value" => __("Send Test E-mail", "Send Test E-mail"))) ?>
    <?= form_close() ?>
</div>
