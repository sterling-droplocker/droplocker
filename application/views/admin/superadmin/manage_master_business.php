<?php enter_translation_domain("admin/superadmin/manage_master_business"); ?>
<style type="text/css">
    legend {
        font-weight: bold;
    }
    form {
        margin-top: 10px;
    }
    fieldset {
        border: 1px solid;
    }
    input {
        margin: 5px 0px;
    }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $(":submit").loading_indicator();
});
</script>
<h1> <?php echo __("Manage Master Business", "Manage Master Business"); ?> </h1>

<?=  form_open("/admin/superadmin/update_master_business"); ?>
<fieldset>
    <legend> <?php echo __("Set Master Business", "Set Master Business"); ?> </legend>

<?php
    //echo form_label("Master Business", "business_id");
    if (isset($master_business))
    {
        echo form_dropdown("business_id", $businesses, $master_business->businessID, "id='master_business'");
    }
    else
    {
        echo form_dropdown("business_id", $businesses, "", "id='master_business'");
    }
    echo form_submit(array("value" => __("Update", "Update"), "class" => "button orange"));
?>
</fieldset>
<?= form_close() ?>
