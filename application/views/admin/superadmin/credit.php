<?php enter_translation_domain("admin/superadmin/credit"); ?>
<script>
$(document).ready(function(){

$('#customer').autocomplete({
	//source: '/ajax/customer_autocomplete/',
	source: function(req, add){  

            //pass request to server  
            $.getJSON("/ajax/customer_autocomplete/", req, function(data) {  
					console.log(data);
					dc=data;
                //create array for response objects  
                var suggestions = [];  

                //process response  
                $.each(data, function(i, val){  
                	suggestions.push(data[i]); 
                	 
            	}); 	 

            //pass array to callback  
            add(suggestions)
       })
      },
	appendTo: "#customer_results",
	select : function(e, ui){
		$.each(dc, function(i, val){  
            if(ui.item.value==val){
            $('#customer_id').val(i); 
            //need to get the bags for this customer
            	//get_customer_bags(i);
            }
        }); 
	}
	});

});

</script>

<h2><?php echo __("Credit a customer", "Credit a customer"); ?></h2>
<form action="" method='post' >
    <table class='detail_table'>
        <tr>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <td><input type='text name='customer' id='customer' /><div id='customer_results'></div><input type='hidden' id='customer_id' name='customer_id' value='customer_id' /></td>
        </tr>
         <tr>
            <th><?php echo __("Amount", "Amount"); ?></th>
            <td><input name='amount' value='' /></td>
        </tr>
    </table>
    <input class='button orange' type='submit' value="<?php echo __("Credit Customer", "Credit Customer"); ?>" name='submit'>
</form>