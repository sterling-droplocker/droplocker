<?php enter_translation_domain("admin/superadmin/add_business"); ?>
<?php
/**
 * Expects some of the following content variables
 *  languages array
 */

if (!is_array($languages)) {
    trigger_error(__("languages must be an array", "'languages' must be an array"), E_USER_WARNING);
    
}

$all_states = get_all_states_by_country_as_array();

?>
<?php 

$fieldList = array();
$fieldList['firstName'] = 
            "<tr>
                <th> *" .  __('Admin First Name', 'Admin First Name') . "</th>
                <td>" .  form_input('admin_firstName') . "</td>
            </tr>";
            
$fieldList['lastName'] = 
            "<tr>
                <th> *" .  __('Admin Last Name', 'Admin Last Name') . "</th>
                <td>" .  form_input('admin_lastName') . "</td>
            </tr>";
            
$nameForm = getSortedNameForm($fieldList);

//-----------------------------------------------
$fieldList = array();
$fieldList['address1'] = 
        "<tr>
                <th>" . __('Address', 'Address') . "</th>
                <td><input type='text' name='address' /></td>
            </tr>";
$fieldList['address2'] = 
        "<tr>
                <th>" . __('Address 2', 'Address 2') . "</th>
                <td><input type='text' name='address2' /></td>
            </tr>";
$fieldList['city'] = 
        "<tr>
                <th>" . __('City', 'City') . "</th>
                <td><input type='text' name='city' /></td>
            </tr>";
$fieldList['state'] = 
        "<tr>
                <th id='state_label'>" . __('State', 'State') . "</th>
                <td id='state_row'>
                    " . form_dropdown('state', $all_states['usa'], '', 'id="state_select"' ) . "
                </td>
            </tr>";
$fieldList['country'] = 
        "<tr>
                <th>
                    " . __('Country', 'Country') . "
                </th>
                <td>
                    " . form_dropdown('country', get_countries_as_array(), '', 'id="country_select"' ) . "
                </td>
            </tr>";
$fieldList['zip'] = 
        "<tr>
                <th>" . __('Zipcode', 'Zipcode') . "</th>
                <td><input type='text' name='zipcode' /></td>
            </tr>";
        
$addressForm = getSortedAddressForm($fieldList);

?>
<style type="text/css">
    fieldset {
        border: 1px solid #000000;
        margin-top: 10px;
    }
    fieldset legend {
        font-weight: bold;
    }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#add_business_form").validate( {
    rules : {
        companyName : {
            required: true,
        },
        email : {
            required: true
        },
        admin_email : {
            required: true,
            email: true
        },
        admin_password : {
            required: true,
        },
        admin_firstName : {
            required: true
        },
        admin_lastName : {
            required : true
        }
            
    },
    errorClass : 'invalid'
})


   //
    // logic for country/state selection (internation - canada)

    // should share select attributes for name, so one state comes through
    var countrySubSelect = function(){
        var countrySelVal =  $('#country_select').val();
        if (typeof(statesOptions[countrySelVal]) != 'undefined') {
            var states = statesOptions[countrySelVal];
            $('#state_row').html('<select name="state" id="state_select"></select>');
            $('#state_select').html(buildStateOptions(states));
        } else {
            $('#state_row').html('<input type="text" name="state" />');
        }
    };

    $('#country_select').bind('change', countrySubSelect );
    
});

    //creates a dropdown for states
    var buildStateOptions = function(states) {
        var selectMain = $('<select />');
        for (var key in states) {
            $('<option></option>').val(key).text(states[key]).appendTo(selectMain);
        }
        return selectMain.html();
    }

    var statesOptions = <?= json_encode($all_states) ?>;
</script>
<h2><?php echo __("Add Business", "Add Business"); ?></h2>
<?= form_open("/admin/business/add", array("id" => "add_business_form")) ?>

    <fieldset>
        <legend> <?php echo __("Business Properties", "Business Properties"); ?> </legend>
        <table class="detail_table">
            <tr>
                <th>*<?php echo __("Company Name", "Company Name"); ?></th>
                <td><input type='text' name='companyName' /></td>
            </tr>
            
            <?= $addressForm ?>
            
            <tr>
                <th><?php echo __("Phone", "Phone"); ?></th>
                <td><input type='text' name='phone' /></td>
            </tr>
            <tr>
                <th>*<?php echo __("Email", "Email"); ?></th>
                <td><input type='text' name='email' /></td>
            </tr>
            <tr>
                <th><?php echo __("Company Timezone", "Company Timezone"); ?></th>
                <td>
                <?php
                    echo form_dropdown("timezone", get_timezones_as_codeigniter_dropdown(), $this->business->timezone);
                ?>
                </td>
            </tr>
            <tr>
                <th><?php echo __("Service Types", "Service Types"); ?></th>
                <td>
                <? foreach($serviceTypes as $key => $value): ?>
                    <input type='checkbox' name='serviceTypes[]' value='<?php echo $value->serviceTypeID ?>' /> <?= $value->name ?>
                <? endforeach; ?>
                </td>
           </tr>
           <tr>
               <th> <?php echo __("Default Language", "Default Language"); ?> </th>
               <td>
                   <?= form_dropdown("language_id",$languages, 39) ?>
               </td>
           </tr>
           <?php if (in_bizzie_mode()): ?>
                <tr>
                    <th> <?php echo __("Subdomain", "Subdomain"); ?> </th>
                    <td>
                        <?= form_input("subdomain") ?>
                    </td>
                </tr>
           <?php endif ?>
        </table>
    </fieldset>
    <fieldset>
        <legend> <?php echo __("Admin Employee", "Admin Employee"); ?></legend>
        <table class="detail_table">
            <tr>
                <th> *<?php echo __("Admin Email", "Admin Email"); ?> </th>
                <td> <?= form_input("admin_email") ?> <td> 
            </tr>
            <tr>
                <th> *<?php echo __("Admin Password", "Admin Password"); ?> </th>
                <td> <?= form_input("admin_password") ?> </td>
            </tr>
            <?= $nameForm ?>
        </table>
    </fieldset>
    <p> *<?php echo __("Required Fields", "Required Fields"); ?> </p>
    <input type='submit' name='submit' value='<?php echo __("Submit", "Submit"); ?>' class='button orange' />
</form>
