<?php enter_translation_domain("admin/superadmin/license_invoicing"); ?>
<h2><?php echo __("License Invoicing", "License Invoicing"); ?></h2>

<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("Business", "Business"); ?></th>	
        <th><?php echo __("Address", "Address"); ?></th>
		<th><?php echo __("Invoices", "Invoices"); ?></th>	
        <th></th>
        <th>Amount</th>
	</tr>
</thead>
<tbody>
<? foreach($businesses as $b): ?>
    <tr>
        <td><?= $b->companyName ?></td>
        <td><?= $b->city ?>, <?= $b->state ?></td>
        <td nowrap>
            <form action="/admin/superadmin/license_invoicing_generate/" method="post">
            <input type="hidden" value="<?= $b->businessID ?>" name="business_id">
            <select name="invMonth">
            <? for($i = 1; $i <= 36; $i++): ?>
                	<option value="<?= date("Y-m", strtotime("-$i months")) ?>"><?= date("M Y", strtotime("-$i months")) ?>
                        <?= !empty($existingInvoices[$b->businessID][date("Y-m", strtotime("-$i months"))]) ? __("generated", " [generated]") : ''; ?>
                    </option>
            <? endfor; ?>
            </select>
            <input type="submit" name="view" value="View">
            <? if (is_superadmin()): ?>
            <input type="submit" name="quickbooks" value="<?php echo __("Export to QuickBooks", "Export to QuickBooks"); ?>">
            <? endif; ?>
            </form>
        </td>
        <td width="25%"><a href="/admin/superadmin/license_pricing/<?= $b->businessID ?>"><?php echo __("Pricing", "Pricing"); ?></a>
            <br>
            <?= get_business_meta($b->businessID, 'licenseNotes'); ?>
        </td>
        <td width="25%">
            <? 
                if (is_superadmin()): 
                    $has_default_business_card = $b->has_default_business_card;
                    if ($has_default_business_card):
            ?>
                        <form action="/admin/superadmin/license_invoicing_capture" method="POST">
                            <input type="text" name="process[amount]" value="<?php echo number_format($b->amount, 2); ?>" />
                            <input type="hidden" name="process[business_id]" value="<?php echo $b->businessID ?>" />
                            <input type="submit" name="pay" value="Pay">
                        </form>
            <?php 
                    else:
            ?>
                        <?php echo __("No business Card found", "No business Card found"); ?>
            <?php
                    endif;
            ?>
                    <a href="/admin/customers/view"><?php echo __("Add/Change card", "Add/Change card"); ?></a>
            <? endif; ?>
        </td>
    </tr>
<? endforeach; ?>    
</tbody>
</table>
