<?php enter_translation_domain("admin/superadmin/view_email_log"); ?>
<style type="text/css">
    table.dataTable td {
        padding: 10px;
    }
</style>
<h2><?php echo __("Email Log", "Email Log"); ?></h2>
<script type="text/javascript">
$(document).ready(function() {
    <?
    if(isset($business_id)){
        echo "var business_id = {$business_id};";
    } else {
        echo "var business_id = '';";
    }
    ?>
    $.get("/ajax/getEmailLogColumns", {}, function(columns) {
        aoColumns = columns;
        emailLogDataTable = $("#emailLog").dataTable( {
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [ [25, 50, 100, -1],[25, 50, 100, "All"] ],
            "aoColumns" : aoColumns,
            "bProcessing" : true,
            "bServerSide" : true,
            "sAjaxSource" : "/ajax/getEmailLogData/" + business_id,

            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
            },
            "aoColumns": [
                          {
                             sTitle: aoColumns[0]['sTitle'],
      	                    
     	                     fnRender : function ( obj) {
     	                         return '<a target="_blank" style="color:#3b7db9" href="/admin/customers/email_detail/'+obj.aData[0]+'">'+obj.aData[0]+'</a>';
     	                     }
     	                     
     	                 },
                         aoColumns[1],
                         {
                            sTitle: aoColumns[2]['sTitle'],
                            fnRender : function (obj) {
                                return "<div style='max-height: 200px; max-width: 500px; overflow-y: scroll; overflow-x: scroll;'>" + $.trim(obj.aData[2]) + "</div>";
                            }
                         },
                         aoColumns[3],
     	                 aoColumns[4],aoColumns[5], 
                         {
                             sTitle: aoColumns[6]['sTitle'],
                             fnRender : function(obj) {
                                if (obj.aData[6] == null)
                                {
                                    return "";
                                }
                                else
                                {
                                    var extension = obj.aData[6].substr(obj.aData[6].length-4);
                                    if ( extension == ".png" || extension == ".gif" || extension == ".jpg")
                                    {
                                        return '<a target="_blank" style="color:#3b7db9" href="'+obj.aData[6]+'"> <img style="height: 200px; width: 200px" src="' + obj.aData[6] + '"/> </a>';
                                    }
                                    else
                                    {
                                        return '<a target="_blank" style="color:#3b7db9" href="'+obj.aData[6]+'">'+obj.aData[6]+'</a>';
                                    }
                                }
                                 
                             }
                         }
     	                
     	                
     	             ]
        }).fnSetFilteringDelay(); 
        new FixedHeader(emailLogDataTable);
    }, "json");
    

});

    
</script>

<table id="emailLog">


</table>