<?php enter_translation_domain("admin/superadmin/manage_languages"); ?>
<?php 
/**
 * Expects the following content variables
 *  languages: an array of languages 
 */
if (!is_array($languages)) {
    trigger_error(__("The content variable languages must be an array", "The content variable 'languages' must be an array"), E_USER_WARNING);
}
?>

<style type="text/css">
    fieldset {
        border: 1px solid;
        margin: 10px 0px;
        padding: 10px;
    }
    legend {
        font-weight: bold;
    }
    input,select {
        margin: 5px;
    }
    .dialog_form {
        width: 350px;
    }
    #add_new_languageKey_dialog_form {
        width: 500px;
    }
    .dialog_form label {
        display: inline-block;
        width: 125px;
        vertical-align: middle;
    }
    .editable {
        padding: 2px; 
        border: 1px #bc271c inset !IMPORTANT;
    }
    .editable:hover {
        padding: 2px;
        border: 1px #ffff99 inset !IMPORTANT;
        background-color: #ECFFB3 !IMPORTANT;
    }
   
</style>

<h2> <?php echo __("Manage Languages", "Manage Languages"); ?> </h2>
<?= form_input(array("type" => "button", "id" => "add_new_language_button", "value" => __("Add New Language", "Add New Language"), "class" => "button blue", "style" => "font-size: 0.8em")) ?>

<table id="language_dataTable"> 

</table>

<!-- Dialog containers -->
<div id="add_new_language_dialog" style="display: none;">
    <?= form_open("/admin/languages/add", array("class" => "dialog_form")) ?>
        <?= form_fieldset(__("Add New Language", "Add New Language")) ?>
        <?= form_label("Tag") ?> <?= form_input(array("name" => "tag", "value" => set_value("tag"), "maxlength" => 3)) ?>
        <br />
        <?= form_label(__("Description", "Description")) ?> <?= form_input("description", set_value("description")) ?>
        <br />
        <?= form_submit(array("value" => __("Add", "Add"), "class" => "button orange")) ?>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>
</div>

<script type="text/javascript">
    var languages = <?=json_encode($languages)?>;
    var initialize_language_management = function($incoming_language_dataTable, $incoming_add_new_language_button, $incoming_add_new_language_dialog, $incoming_editable_language_fields) {
        $incoming_add_new_language_button.click(function() {
           $incoming_add_new_language_dialog.dialog( {
               title : "<?php echo __("Add New Language", "Add New Language"); ?>",
               modal : true,
               height: "auto",
               width: "auto"
           });
        });
        var aaData = new Array();
        $.each(languages, function(index, language) {
            var languageID = language['languageID'];

            var tag = language['tag'];
            var $tag_container = $("<div>");

            var $tag = $("<div>");
            $tag.text(tag);
            $tag.addClass("editable");
            $tag.attr("data-id", languageID);
            $tag.attr("data-property", "tag");

            $tag.appendTo($tag_container);

            var description = language['description'];
            var $description_container = $("<div>");
            var $description = $("<div>");
            $description.text(description);
            $description.addClass("editable");
            $description.attr("data-id", languageID);
            $description.attr("data-property", "property");
            //$description.attr("data-primaryKey", "languageID")

            $description.appendTo($description_container);

            var $delete_link_container = $("<div>");

            var $delete_link = $("<a>");
            $delete_link.attr("href", "/admin/languages/delete?languageID=" + languageID);


            var $delete_link_image = $("<img>");
            $delete_link_image.attr("alt", "<?php echo __("Delete language", "Delete language"); ?> " + tag);
            $delete_link_image.attr("src", "/images/icons/delete.png");
            $delete_link_image.appendTo($delete_link);

            $delete_link.appendTo($delete_link_container);
            aaData.push([languageID, $tag_container.html(), $description_container.html(), $delete_link_container.html()]);
            //aaData.push([languageID, $tag_container, $description_container, $delete_link_container]);
        });
        $incoming_language_dataTable.fnClearTable();
        $incoming_language_dataTable.fnAddData(aaData);

        $incoming_language_dataTable.find(".editable").editable("/admin/languages/update", {
            cancel : "<img src='/images/icons/delete.png' />",
            submit : "<img src='/images/icons/accept.png' />",
            indicator : "Saving ... <img src='/images/progress.gif' />",
            data : function (string) {
                return $.trim(string);
            },
            submitdata : function (value, settings) {
                 var parameters = Object.create();
                 parameters['property'] = $(this).attr("data-property");
                 parameters["languageID"] = $(this).attr("data-id");
                 return parameters;
            } 
        });
    } 
    
    
        //The following statements initialize the interface for managing system languages.
    var $language_dataTable = $("#language_dataTable").dataTable( {
        oLanguage : {
            sEmptyTable : "<?php echo __("There are no languages defined in the system", "There are no languages defined in the system"); ?>"
        },
        aaData: [],
        aoColumns : [
            { sTitle : "<?php echo __("Language ID", "Language ID"); ?>"},
            { sTitle: "<?php echo __("Tag", "Tag"); ?>" },
            { sTitle: "<?php echo __("Description", "Description"); ?>" },
            { sTitle : "<?php echo __("Delete", "Delete?"); ?>"}
        ],
        bJQueryUI : true,
        bPaginate : false
    });
    
    var $editable_language_fields =  $(".edit_language_field");
    
    var $add_new_language_button = $("#add_new_language_button");
    var $add_new_language_dialog = $("#add_new_language_dialog");
    
    initialize_language_management($language_dataTable, $add_new_language_button, $add_new_language_dialog,$editable_language_fields);
</script>