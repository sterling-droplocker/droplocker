<?php enter_translation_domain("admin/superadmin/manage_languageKeys_and_languageViews"); ?>
<?php
/**
 * The following view is the superadmin interafce for creating languages, languageViews, and assigning language keys to language views.
 * 
 * Expects the following content variables:
 *  @languageViews_and_languageKeys: an associate array of language views and respective languageKeys associated with each languageview in the following format, where the first level child index is the primary key of a launge view
 *      @languageViewID1 object 
 *          @name string THe name of this language view
 *          @languageKeys array The launguage keys associated with this language view, where each index is the primary key of a launguage key entity
 *              languageKeyID1 object Contains the properties of a language key
 *              languageKeyID2
 *                  ...
 *              ...
 *      @languageViewID2
 *          ...
 *      ...
 *  @languageView_dropdown: an array of languageViews

 */
if (!is_array($languageViews)) {
    trigger_error(__("The content variable languageViews must be an array", "The content variable 'languageViews' must be an array"), E_USER_ERROR);
}
if (!is_array($languageView_dropdown)) {
    trigger_error(__("The content variable languageView_dropdown must be an array", "The content variable 'languageView_dropdown' must be an array"), E_USER_ERROR);
}

if (!is_array($languageViews_and_languageKeys)) {
    trigger_error(__("The content variable languageViews_and_languageKeys must be an array", "The content variable 'languageViews_and_languageKeys' must be an array"), E_USER_ERROR);
}
?>

<style type="text/css">
    form fieldset {
        
        border: 1px solid black;
    }
    form legend {
        font-weight: bold;
    }
    form label {
        width: 150px;
        display: inline-block;
        text-align: left;
    }
    form div {
        margin: 5px;
        text-align: left;
    }
</style>

<script type="text/javascript">
    
var languageViews = <?=json_encode($languageViews) ?>;
var languageViews_and_languageKeys =  <?=json_encode($languageViews_and_languageKeys)?>;


var initialize_languageView_management = function ($incoming_languageView_table, $incoming_add_new_languageView_button, $incoming_add_new_languageView_dialog) {
    
    $incoming_add_new_languageView_button.click(function() {
    $incoming_add_new_languageView_dialog.dialog( {
            title : "<?php echo __("Add New Language View", "Add New Language View"); ?>",
            modal : "true",
            height: "auto",
            width: "auto"
        });
    });
    $incoming_add_new_languageView_dialog.find("input#addMoreLanguageKeyRowsButton").click(function() {
        var $newLanguageKeyRows = $incoming_add_new_languageView_dialog.find("tbody#newLanguageKeyRows");
        for (var x = 0; x < 5; x++) {
            var $newRow = $("<tr>").appendTo($newLanguageKeyRows);
            var $nameColumn = $("<td>").appendTo($newRow);
            $("<input>").attr("type", "textbox").attr("name", "languageKeyName[]").appendTo($nameColumn);;
            var $defaultTextColumn = $("<td>").appendTo($newRow);
            $("<input>").attr("type", "textbox").attr("name", "defaultText[]").appendTo($defaultTextColumn);
        }
    });
    
    
    var aaData = new Array();
    
    $.each(languageViews, function(index, languageView) {
        var languageViewID = languageView['languageViewID'];
        var name = languageView['name'];
        
        var $name_container = $("<div>");
        var $name = $("<div>");
        $name.attr("data-id", languageViewID).attr("data-property", "name").attr("data-id", languageViewID).addClass("editable").text(name);
        $name.appendTo($name_container);
        
        var $delete_link_container = $("<div>");
        
        var $delete_link = $("<a>");
        $delete_link.attr("href", "/admin/languageViews/delete?languageViewID=" + languageViewID);
        
        var $delete_link_image = $("<img>")
        $delete_link_image.attr("alt", "Delete language view " + name).attr("src", "/images/icons/delete.png");
        $delete_link_image.attr("id", "delete-languageView-" + languageViewID); //Note, this identifier is used for selenium tests that need to delete a specific language view.
        $delete_link_image.appendTo($delete_link);
        
        $delete_link.appendTo($delete_link_container);
        
        aaData.push([languageViewID, $name_container.html(), $delete_link_container.html()]);
    });
    
    var $languageView_dataTable = $incoming_languageView_table.dataTable({
        oLanguage : {
            sEmptyTable : "<?php echo __("There are no language views defined in the system", "There are no language views defined in the system"); ?>"
        },
        aaData: aaData,
        aoColumns : [ 
            { "sTitle" : "<?php echo __("Language View ID", "Language View ID"); ?>"},
            { "sTitle" : "<?php echo __("Name", "Name"); ?>"},
            { "sTitle" : "<?php echo __("Delete", "Delete?"); ?>"}
        ],
        "bJQueryUI": true,
        bDestroy: true,
        bPaginate : false
    });
    $languageView_dataTable.css("width", "");
    $languageView_dataTable.find(".editable").editable("/admin/languageViews/update", {
        cancel : "<img src='/images/icons/delete.png' />",
        submit : "<img src='/images/icons/accept.png' />",
        indicator : "Saving ... <img src='/images/progress.gif' />",
        data : function (string) {
            return $.trim(string);
        },
        submitdata : function (value, settings) {
             var parameters = Object.create(Object.prototype);
             parameters['property'] = $(this).attr("data-property");
             parameters["languageViewID"] = $(this).attr("data-id");
             return parameters;
        } 
    });
}

var populate_languageKey_table = function($incoming_languageKey_table, $incoming_languageView_menu) {
    var aaData = new Array();
    if (typeof languageViews_and_languageKeys === 'object' && typeof $incoming_languageView_menu.val() !== "undefined") {
        var languageViewID = $incoming_languageView_menu.val();
        var languageView = languageViews_and_languageKeys[languageViewID];


        var languageKeys = languageView['languageKeys'];

        $.each(languageKeys, function(languageKeyID, languageKey_properties) {
           var name = languageKey_properties['name'];
           var defaultText = languageKey_properties['defaultText'];

           var $name_container = $("<div>");
           var $name = $("<div>");
           $name.attr("data-id", languageKeyID)
           $name.attr("data-property", "name");
           $name.addClass("editable");
           $name.text(name);
           $name.appendTo($name_container);

           var $defaultText_container = $("<div>");
           var $defaultText = $("<div>");
           $defaultText.attr("data-id", languageKeyID);
           $defaultText.attr("data-property", "defaultText");
           $defaultText.text(defaultText);
           $defaultText.addClass("editable_textarea");

           $defaultText.appendTo($defaultText_container);

           var $delete_container = $("<div>");

           var $delete_link = $("<a>");
           $delete_link.attr("href", "/admin/languageKeys/delete?languageKeyID=" + languageKeyID);
           var $delete_image = $("<img>");
           $delete_image.attr("src", "/images/icons/delete.png");
           $delete_image.attr("alt", "Delete " + name);


           $delete_link.appendTo($delete_container);
           $delete_image.appendTo($delete_link);
           aaData.push([languageKeyID, $name_container.html(), $defaultText_container.html(), $delete_container.html()]);
        });
    }
    
    var $languageKey_dataTable = $incoming_languageKey_table.dataTable( {
        oLanguage : {
            sEmptyTable : "<?php echo __("There are no language keys defined in the system", "There are no language keys defined in the system"); ?>"
        },
        aaData: aaData,
        aoColumns : [ 
            { "sTitle" : "<?php echo __("Language Key ID", "Language Key ID"); ?>"},
            { "sTitle" : "<?php echo __("Name", "Name"); ?>"},
            { "sTitle" : "<?php echo __("Default Text", "Default Text"); ?>"},
            { "sTitle" : "<?php echo __("Delete", "Delete?"); ?>"}
        ],
        "bJQueryUI": true,
        "bDestroy" : true,
        bPaginate : false
    });
    $languageKey_dataTable.css("width", "");

    $languageKey_dataTable.find(".editable").editable("/admin/languageKeys/update", {
        cancel : "<img src='/images/icons/delete.png' />",
        submit : "<img src='/images/icons/accept.png' />",
        indicator : "Saving ... <img src='/images/progress.gif' />",
        data : function (string) {
            return $.trim(string);
        },
        submitdata : function (value, settings) {
             return {
                 property : $(this).attr("data-property"),
                 languageKeyID : $(this).attr("data-id")
             }
        }
        /**
        callback : function (sValue, y) {
            var aPos = $incoming_languageView_dataTable.fnGetPosition( this.parentNode );
            this.innerHTML = sValue;
            $incoming_languageView_dataTable.fnUpdate( this.parentNode.innerHTML, aPos[0], aPos[1]);
        }
        */
    });
   
       $languageKey_dataTable.find(".editable_textarea").editable("/admin/languageKeys/update", {
        type : 'textarea',
        cancel : "<img src='/images/icons/delete.png' />",
        submit : "<img src='/images/icons/accept.png' />",
        indicator : "<?php echo __("Saving", "Saving ..."); ?> <img src='/images/progress.gif' />",
        data : function (string) {
            //Note, when editing the language key translation, we want the actual html characters to appear in the inteface instead of their encoded entities, and also want the actual html characters sent to the server, and not the encoded entities.
            return htmlDecode(string);
        },
        submitdata : function (value, settings) {
             return {
                 property : $(this).attr("data-property"),
                 languageKeyID : $(this).attr("data-id")
             }
        }
    });
   
};
    
var initialize_languageKey_management = function($incoming_languageView_table, $incoming_languageView_menu, $incoming_add_new_languageKey_button, $incoming_add_new_languageKey_dialog) {
    populate_languageKey_table($incoming_languageView_table, $incoming_languageView_menu);
    $incoming_languageView_menu.change(function() {
        populate_languageKey_table($incoming_languageView_table, $incoming_languageView_menu);
    });

    
    $incoming_add_new_languageKey_button.click(function() {
        $incoming_add_new_languageKey_dialog.find("[name='languageView_id']").val($incoming_languageView_menu.val());
        $incoming_add_new_languageKey_dialog.dialog( {
            title : "<?php echo __("Add New Language Key", "Add New Language Key"); ?>",
            modal : true,
            height: "auto",
            width: "auto"
        });
    });
};

$(document).ready(function() {
    $(":submit").loading_indicator();
    
    //The following statements initialize the interface for managing language Keys for a particular view.
    var $languageKey_table = $("#languageKey_table");

    var $languageView_menu = $("#languageView_menu");

    var $add_new_languageKey_button = $("#add_new_languageKey_button");
    var $add_new_languageKey_dialog = $("#add_new_languageKey_dialog");
    
    initialize_languageKey_management($languageKey_table, $languageView_menu, $add_new_languageKey_button, $add_new_languageKey_dialog);
    
    //The following statements intiailize the interface for managing language views.
    var $languageView_table = $("#languageView_table");
        
    var $add_new_languageView_button = $("#add_new_languageView_button");
    var $add_new_languageView_dialog = $("#add_new_languageView_dialog");

    initialize_languageView_management($languageView_table, $add_new_languageView_button, $add_new_languageView_dialog);   
});

</script>

<h2> Manage Language Keys </h2>
<?= form_input(array("type" => "button", "id" => "add_new_languageKey_button", "value" => "New Language Key", "class" => "button blue", "style" => "font-size:0.8em; margin: 5px;")) ?>

<?php if (empty($languageView_dropdown)): ?>
    <p style="color: red;"> There are no language views defined yet in the system </p>
<?php else: ?>
    <div>
        <div style="margin: 5px;"> <?= form_label("Language View: ") . form_dropdown("",$languageView_dropdown, set_value("languageView_id"), "id='languageView_menu'")?> </div>

        <table id="languageKey_table">

        </table>
    </div>
<?php endif ?>

<h2> Manage Language Views </h2>
<?= form_input(array("type" => "button", "id" => "add_new_languageView_button", "value" => "Add New Language View", "class" => "button blue", "style" => "font-size: 0.8em; margin: 5px;")) ?>

<table id="languageView_table"> </table>

<!-- Dialog containers -->
<div id="add_new_languageView_dialog" style="display: none;">
    <?= form_open("/admin/languageViews/add", array("class" => "dialog_form")) ?>
        <?= form_fieldset("New Language View") ?>
            <div> <?= form_label("Name", "name") . form_input(array("name" => "name", "id" => "name")) ?> </div>
            <div>
                <?php echo __("Language Keys", "Language Keys"); ?>
                <table>
                    <thead>
                        <tr>
                            <th> <?php echo __("Name", "Name"); ?> </th>
                            <th> <?php echo __("Default Text", "Default Text"); ?> </th>
                        </tr>
                    </thead>
                    <tbody id='newLanguageKeyRows'> 
                       <?php for ($x = 0; $x < 5; $x++): ?>
                           <tr>
                               <td> <?= form_input("languageKeyName[]") ?> </td> <td> <?= form_input("defaultText[]") ?> </td>
                           </tr>
                       <?php endfor ?>                           
                    </tbody>
                </table>
                <?= form_input(array("type" => "button", "id" => "addMoreLanguageKeyRowsButton", "value" => "More Keys", "class" => "button blue")) ?>

            </div>
            <div> <?= form_submit(array("value" => "Add", "class" => "button orange")) ?> </div>
        <?= form_fieldset_close() ?>
    <?= form_close() ?>
</div>
<div id="add_new_languageKey_dialog" style="display: none;">
    <?php if (empty($languageView_dropdown)): ?>
        <p style="color: red;"> <?php echo __("There are no language views defined in the system. You must create at least one language view before you can create a manage language key", "There are no language views defined in the system. You must create at least one language view before you can create a manage language key."); ?> </p>
    <?php else: ?>
        <?= form_open("/admin/languageKeys/add", array("class" => "dialog_form", "id" => "add_new_languageKey_dialog_form")) ?>
            <?= form_fieldset(__("Add New Language Key", "Add New Language Key")) ?>
                
                <div> <?= form_label(__("Name", "Name")) ?> <?= form_input(array("name" => "name", "style" => "width: 300px")) ?> </div>

                <div> <?= form_label(__("Default Text", "Default Text")) ?> <?= form_textarea(array("name" => "defaultText", "rows" => 4, "cols" => 40, "style" => "vertical-align: middle")) ?> </div>
                
                <div> <?= form_label("Language View")?> <?= form_dropdown("languageView_id", $languageView_dropdown, set_value("languageView_id")) ?> </div>
                <?= form_submit(array("value" => __("Add", "Add"), "class" => "button orange")) ?>
            <?= form_fieldset_close() ?>
        <?= form_close(); ?>
    <?php endif ?>
</div>


