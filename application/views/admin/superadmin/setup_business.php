<?php enter_translation_domain("admin/superadmin/setup_business"); ?>
<style type="text/css">
    select {
        margin-left: 5px;
    }
    input {
        margin-right: 10px;
    }
    fieldset {
        border: 1px solid;
    }
    fiedset legent {
        font-weight: bold;
    }
    
    form label {
        margin-left: 5px;
    }
    
</style>

<script type="text/javascript">
    /**
     * The folloing gfunction updates the toatls of the ACL Roles, Products, Reminders, Email Templates, Coupons, Discount Reasons, and Followup logs of the desination business.
     * 
     */
    function update_current_business_totals()  {
        var business_id = $("#destination_business_id").val();
        
        $("#aclRoles_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        
        $.getJSON("/admin/aclroles/get_total", {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#aclRoles_count").text(result['total']);
            }
            else
            {
                $("#aclRoles_count").text("<?php echo __("Could not retrieve ACL total", "Could not retrieve ACL total"); ?>").css("color", "red");
            }
        }).error(function() {
            $("#aclRoles_count").text("<?php echo __("Could not retrieve ACL total", "Could not retrieve ACL total"); ?>").css("color", "red");
        });
        
        $("#products_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />")
        $.getJSON("/admin/products/get_total", {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#products_count").text(result['total'])
            }
            else
            {
                $("#products_count").text("<?php echo __("Could not retreive product total", "Could not retreive product total"); ?>");
            }
        }).error(function() {
            $("#products_count").text("<?php echo __("Could not retrieve product total", "Could not retrieve product total"); ?>");
        });        
        
        $("#reminders_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $.getJSON('/admin/reminders/get_total', {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#reminders_count").text(result['total']);
            }
            else
            {
                $("#reminders_count").text('<?php echo __("Could not retrieve reminders total", "Could not retrieve reminders total"); ?>').css("color", "red");
            }
            
        }).error(function() {
            $("#reminders_count").text('<?php echo __("Could not retrieve reminders total", "Could not retrieve reminders total"); ?>').css("color", "red");
        });
        
        $("#emailTemplates_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $.getJSON('/admin/email_templates/get_total', {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#emailTemplates_count").text(result['data']['total']);
            }
            else
            {
                $("#emailTemplates_count").text("<?php echo __("Could not retrieve email template total", "Could not retrieve email template total"); ?>").css("color", "red");
            }
        }).error(function() {
            $("#emails_count").text("<?php echo __("Could not retrieve email total", "Could not retrieve email total"); ?>").css("color", "red");
        });
        
        //The following AJAX call retrieves the total coupons for the selected business
        $("#coupons_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $.getJSON('/admin/coupons/get_total', {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#coupons_count").text(result['total']);
            }
            else
            {
                $("#coupons_count").text("<?php echo __("Could not retrieve coupon total total", "Could not retrieve coupon total total"); ?>").css("color", 'red');
            }
        }).error(function() {
            $("#coupons_count").text("<?php echo __("Could not retrieve coupon total", "Could not retrieve coupon total"); ?>").css('color', 'red');
        });         
        
        //The following AJAX call retrieves the total reasons for the selected business
        $("#reasons_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $.getJSON('/admin/discount_reasons/get_total', {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#reasons_count").text(result['total']);
            }
            else
            {
                $("#reasons_count").text("<?php echo __("Could not retrieve reason total", "Could not retrieve reason total"); ?>").css("color", 'red');
            }
        }).error(function() {
            $("#reasons_count").text("<?php echo __("Could not retrieve reason total", "Could not retrieve reason total"); ?>").css('color', 'red');
        });           
        
        //The following AJAX call retrieves the total followup calls for the selected business
        $("#followupCalls_count").html("<img src='/images/progress.gif' style='margin: 0 5px; display: inline;' />");
        $.getJSON('/admin/followupCalls/get_total', {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success')
            {
                $("#followupCalls_count").text(result['total']);
            }
            else
            {
                $("#followupCalls_count").text("<?php echo __("Could not retrieve Followup Call total", "Could not retrieve Followup Call total"); ?>").css("color", 'red');
            }
        }).error(function() {
            $("#followupCalls_count").text("<?php echo __("Could not retrieve Followup Call total", "Could not retrieve Followup Call total"); ?>").css('color', 'red');
        });        
        
        $("#businessLaundryPlans_count").html("<img src='/images/progress.gif' style='margin: 0px 5px; display: inline;' />");
        $.getJSON("/admin/business_laundryplans/get_total", {business_id : business_id}, function(result) {
            if ('status' in result && result['status'] == 'success') {
                $("#businessLaundryPlans_count").text(result['data']['total']);
            }
            else {
                $("#businessLaundryPlans_count").text("<?php echo __("Could not retrieve Followup Call total", "Could not retrieve Followup Call total"); ?>").css("color", "red");
            }
        });
        
        $("#upcharges_count").html("<img src='/images/progress.gif' style='margin: 0px 5px; display: inline;' />");
        $.getJSON("/admin/upcharges/get_total", {business_id : business_id}, function(result) {
            if ("status" in result && result['status'] == "success") {
                $("#upcharges_count").text(result['data']['total']);
            }
            else {
                $("#upcharges_count").text("<?php echo __("Could not retrieve upcharges total", "Could not retrieve upcharges total"); ?>").css("color", "red");
            }
        });
        
        $("#upchargeGroups_count").html("<img src='/images/progress.gif' style='margin: 0px 5px; display: inline;' />");
        $.getJSON("/admin/upcharge_groups/get_total", {business_id : business_id}, function(result) {
            if ("status" in result && result['status'] == 'success') {
                $("#upchargeGroups_count").text(result['data']['total']);
            }
            else {
                $("#upchargeGroups_count").text("<?php echo __("Could not retrieve Upcharge Groups total", "Could not retrieve Upcharge Groups total"); ?>").css("color", "red");
            }
        });
        
        $("#product_processes_count").html("<img src='/images/progress.gif' style='margin: 0px 5px; display: inline;' />");
        $.getJSON("/admin/product_process/get_total", {business_id : business_id}, function(result) {
            if ("status" in result && result['status'] == 'success') {
                $("#product_processes_count").text(result['data']['total']);
            }
            else {
                $("#product_processes_count").text("<?php echo __("Could not retrieve Product Processes total", "Could not retrieve Product Processes total"); ?>").css("color", "red");
            }
        });
        
        $("#productCategories_count").html("<img src='/images/progress.gif' style='margin: 0px 5px; display: inline;' />");
        $.getJSON("/admin/product_categories/get_total", {business_id : business_id}, function(result) {
            if ("status" in result && result['status'] == "success") {
                $("#productCategories_count").text(result['data']['total']);
            }
            else {
                $("#productCategories_count").text("<?php echo __("Could not retrieve ProductCategories total", "Could not retrieve ProductCategories total"); ?>").css("color", "red");
            }
        });
    }
    
    
$(document).ready(function() {
    $("#destination_business_id").change(function() {
        update_current_business_totals();
    });
    $("#setup_business_form :submit").loading_indicator();
    
    update_current_business_totals();
});

</script>
<h2>Setup Business</h2>
<br>

<?= form_open("/admin/business/copy_data", "id='setup_business_form'")?>
    <?= form_fieldset("Copy data from source business to destination business") ?>
        <div style="display: table;">
            <div style="display: table-cell; padding: 0px 10px 10px 0px;">
                <?= form_label("Source", "source_business_id") . form_dropdown("source_business_id", $businesses, "", "id='source_business_id'") ?>
            </div>
            <div style="display: table-cell; padding: 10px 10px 10px 0px;">
                <?= form_label("Destination", "destination_business_id") . form_dropdown("destination_business_id", $businesses, "", "id='destination_business_id'") ?>
            </div>
        </div>
        <?= form_label(__("ACL", "ACL"), "acl") ?> <?= form_checkbox("acl", 1) ?>

        <!-- Note, the copy laundry plans functionality is broken, so this checkbox is hidden -->
        <?= form_label(__("Business Laundry Plans", "Business Laundry Plans"), "businessLaundryPlans") . form_checkbox("businessLaundryPlans", 1) ?>

        <?= form_label(__("Products, Product Categories, Product Processes, Upcharge Groups", "Products, Product Categories, Product Processes, Upcharge Groups"), "products") . form_checkbox("products", 1) ?>
        <?= form_label(__("Reminders", "Reminders"), "reminder") . form_checkbox("reminder", 1) ?>
        <?= form_label(__("Email Templates", "Email Templates"), "email") . form_checkbox("email", 1) ?>
        <?= form_label(__("Coupon", "Coupon"), "coupon") . form_checkbox("coupon", 1) ?>
        <?= form_label(__("Discount Reasons", "Discount Reasons"), "reason") . form_checkbox("reasons", 1) ?>
        <?= form_label(__("Followup Calls", "Followup Calls"), "followup") . form_checkbox("followup", 1) ?>
        <?= form_label(__("Upcharges, Upcharge Groups", "Upcharges, Upcharge Groups"), "upcharges") . form_checkbox("upcharges", 1) ?>
        <div style="margin-top: 10px;">
            <?= form_submit(array("class" => "button orange", "value" => __("Copy", "Copy"), "style" => "margin: 10px")) ?>
        </div>
    <?= form_fieldset_close() ?>

    
<?= form_close() ?>

<h2> <?php echo __("Destination Business Totals", "Destination Business Totals"); ?> </h2>

<table class='box-table-a'>
    <thead>
        <th> <?php echo __("ACL Roles", "ACL Roles"); ?> </th>
        <th> <?php echo __("Products", "Products"); ?> </th>
        <th> <?php echo __("Product Processes", "Product Processes"); ?> </th>
        <th> <?php echo __("Product Categories", "Product Categories"); ?> </th>
        <th> <?php echo __("Upcharge Groups", "Upcharge Groups"); ?>
        <th> <?php echo __("Reminders", "Reminders"); ?> </th>
        <th> <?php echo __("Email Templates", "Email Templates"); ?> </th>
        <th> <?php echo __("Coupons", "Coupons"); ?> </th>
        <th> <?php echo __("Reasons", "Reasons"); ?> </th>
        <th> <?php echo __("Followups", "Followups"); ?> </th>
        <th> <?php echo __("Business  Laundry Plans", "Business  Laundry Plans"); ?> </th>
        <th> <?php echo __("Upcharges", "Upcharges"); ?> </th>
    </thead>
    <tbody>
        <tr>
            <td id="aclRoles_count"> </td>
            <td id="products_count"> </td>
            <td id='product_processes_count'> </td>
            <td id="productCategories_count"> </td>
            <td id="upchargeGroups_count"> </td>
            <td id="reminders_count"> </td>
            <td id="emailTemplates_count"> </td>
            <td id="coupons_count"> </td>
            <td id='reasons_count'> </td>
            <td id='followupCalls_count'> </td>
            <td id="businessLaundryPlans_count"></td>
            <td id="upcharges_count"> </td>
        </tr>
    </tbody>
</table>
