<?php enter_translation_domain("admin/superadmin/manage_email_templates"); ?>
<h2><?php echo __("Alert Templates", "Alert Templates"); ?></h2>
<p><?php echo __("Manage all email templates for customer actions", "Manage all email templates for customer actions"); ?></p>

<table class="box-table-a">
    <thead>
        <tr>
            <th> <?php echo __("Email Template ID", "Email Template ID"); ?> </th>
            <th style='width:150px;'><?php echo __("Action Name", "Action Name"); ?></th>
            <th><?php echo __("Status", "Status"); ?></th>
            <th> <?php echo __("Description", "Description"); ?> </th>
            <th width='45' align='center'><?php echo __("SMS", "SMS"); ?></th>
            <th width='45' align='center'><?php echo __("Email", "Email"); ?></th>
            <th width='45' align='center'><?php echo __("Admin", "Admin"); ?></th>
        </tr>
    </thead>
    <tbody>
        <? foreach ($emailActions as $key=>$email) : ?>
        <tr>
            <td> <?= $email['emailTemplateID'] ?> </td>
            <td><?= $email['name']?></td>
            <td><?= $email['status']?></td>
            <td> <?= $email['description'] ?> </td>
            <td align='center'><a class="edit_sms" href='/admin/superadmin/edit_sms_template/<?= $key?>'><?= $email['sms_found']?> <img src='/images/icons/application_edit.png' /></a></td>
            <td align='center'><a class="edit_email" href='/admin/superadmin/edit_email_template/<?= $key?>'><?= $email['email_found']?> <img src='/images/icons/application_edit.png' /></a></td>
            <td align='center'><a class="edit_admin" href='/admin/superadmin/edit_adminalert_template/<?= $key?>'><?= $email['admin_found']?> <img src='/images/icons/application_edit.png' /></a></td>
        </tr>
        <? endforeach ; ?>
    </tbody>
</table>
  


