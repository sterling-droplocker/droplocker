<?php enter_translation_domain("admin/superadmin/view_email_server_settings"); ?>
<h1> <?php echo __("Email Server Settings", "Email Server Settings"); ?> </h1>

<?= form_open("/admin/superadmin/update_email_server_settings") ?>
<fieldset>
    
    <?= form_label("Host") ?> <?= form_input("email_host", $email_host->value) ?>
    <?= form_label("User") ?> <?= form_input("email_user", $email_user->value) ?>
    <?= form_label("Password") ?> <?= form_password("email_password", "1234") ?>
    <?= form_submit(array("class" => "button orange", "value" => __("Update", "Update"))) ?>
</fieldset>
<?= form_close() ?>
