<?php enter_translation_domain("admin/superadmin/list_recent_cron_jobs"); ?>
<h2><?php echo __("Recent Cron Jobs", "Recent Cron Jobs"); ?></h2>
<table id='box-table-a'>
    <tr>
        <th><?php echo __("Name", "Name"); ?></th>
        <th><?php echo __("Error", "Error"); ?></th>
        <th><?php echo __("Date", "Date"); ?></th>
        <th style='width:25px'><?php echo __("View", "View"); ?></th>
    </tr>
    <?foreach($jobs as $job):?>
    <tr>
        <td><?= $job->job?></td>
        <td></td>
        <td><?= convert_from_gmt_aprax($job->dateCreated, STANDARD_DATE_FORMAT);?></td>
        <td align='center'><a href='/admin/superadmin/cron_job_details/<?= $job->cronLogID?>'><img src="/images/icons/magnifier.png" /></a></td>
    </tr>
    <?endforeach;?>
</table>