<?php enter_translation_domain("admin/superadmin/edit_adminalert"); ?>

<?= $this->session->flashdata('message'); ?>
<h2><?php echo __("Edit Admin Alert", "Edit Admin Alert ::"); ?> <?= $name?></h2>
<p><?php echo __("Admin templates support merge values", "Admin templates support merge values. Merge values are enclosed in percent symbols. Example: %customer_username%. <a href='javascript:;' onClick='openCenteredWindow(\"/admin/admin/email_macros\",\"500\",\"600\");'> <img src='/images/icons/page_white_code.png' align='absmiddle' /> Click here</a> to open a window with a complete list of values"); ?></p>
<br />

<form action='' method='post'>
<table class='detail_table'>
	<tr>
		<th><?php echo __("Admin Alert Subject", "Admin Alert Subject"); ?></th>
		<td><input type="text" value='<?= $subject?>' name='subject' /></td>
	</tr>
	<tr>
		<th><?php echo __("Admin Alert Message", "Admin Alert Message"); ?></th>
		<td><textarea style='width:100%; height:100px; border:1px solid #ccc;' name='message'><?= $message?></textarea></td>
	</tr>
	<tr>
	    <th><?php echo __("Recipients", "Recipients"); ?></th>
	    <td>
	        <?php if($customer_support = get_business_meta($business_id, 'customerSupport')): ?> <input type='checkbox' name='email[]' <?php if(@in_array($customer_support, $recipients['system'])){echo "checked";}?> value='<?php echo $customer_support?>' /> <?php echo $customer_support?><?php endif;?><br>
	        <?php if($operations = get_business_meta($business_id, 'operations')): ?> <input type='checkbox' name='email[]' <?php if(@in_array($operations, $recipients['system'])){echo "checked";}?> value='<?php echo $operations?>' /> <?php echo $operations?><?php endif;?><br>
	        <?php if($sales = get_business_meta($business_id, 'sales')): ?> <input type='checkbox' name='email[]' <?php if(@in_array($sales, $recipients['system'])){echo "checked";}?> value='<?php echo $sales?>' /> <?php echo $sales?><?php endif;?><br>
	        <br>
	       <?php echo __("Add more Recipients", "Add more Recipients (seperate with a comma):"); ?> 
	        <div><input type='text' name='customEmails' value='<?php echo $recipients['custom']?>' /></div>
	        
	    </td>
	</tr>
</table>
	<input type="hidden" value='<?= $sms->emailActionID?>' name='insert_id' />
	<input type="submit" name='submit' value='<?php echo __("Update", "Update"); ?>' class='button orange' />
</form>




