<?php enter_translation_domain("admin/superadmin/license_pricing"); ?>
<script>
    jQuery(document).ready(function($) {
        $('.checkbox').change(function() {
            var select_all = $(this).hasClass('select_all');
            if (select_all)  {
                var checkboxes = $(this).closest('form').find(':checkbox');
                checkboxes.prop('checked', $(this).is(':checked'));
            } else {
                $('.select_all').prop('checked', false);
            }
        });
    })
</script>
<h2><?php echo __("License Pricing for", "License Pricing for"); ?> <?= $business[0]->companyName ?></h2>
<br>
<a href="/admin/superadmin/license_invoicing"><- <?php echo __("back to invoicing", "back to invoicing"); ?></a>
<br><br>
<form action="/admin/superadmin/license_pricing_add_minimum" method="post">
<strong><?php echo __("Monthly Minimum", "Monthly Minimum:"); ?></strong> <input type="text" name="minimum" value="<?= $minimum ?>" size="8">
<br>
<strong><?php echo __("Notes", "Notes:"); ?></strong><input type="text" name="notes" value="<?= $notes ?>" size="150">
<br><br>
<strong><?php echo __("Location type to charge", "Location type to charge: "); ?></strong>
<? 
    $checked='';
    if (in_array('-1', $license_princing_location_types)) {
        $checked='checked="checked';
    }
?>
<input class="checkbox select_all" <?=$checked;?> name="locationType_id[]" type="checkbox" value="-1" /><?php echo __("All Locations types", "All Locations types"); ?>
<? 
   foreach($locationsTypes as $lt): 
    $checked='';
    if (in_array($lt->locationTypeID, $license_princing_location_types)) {
        $checked='checked="checked"';
    }
?>
    <input class="checkbox" <?=$checked;?> name="locationType_id[]" type="checkbox" value="<?= $lt->locationTypeID ?>" /><?= $lt->name ?>
<? endforeach; ?>
<br><br>
<input type="submit" name="update" value="<?php echo __("update", "update"); ?>">
<input type="hidden" name="business_id" value="<?= $business[0]->businessID ?>">
</form>
<br/><br/>
<table id="box-table-a">
<thead>
	<tr>
		<th><?php echo __("License Type", "License Type"); ?></th>
        <th><?php echo __("Max Qty", "Max Qty"); ?></th>
		<th><?php echo __("Price", "Price"); ?></th>
        <th>$ <?php echo __("or", "or"); ?> %</th>
        <th></th>
	</tr>
</thead>
<tbody>
    <tr>
        <form action="/admin/superadmin/license_pricing_add" method="post">
        <input type="hidden" name="business_id" value="<?= $business[0]->businessID ?>">
        <td>
            <select name="licenseType_id">
        <? foreach($types as $t): ?>
            <option value="<?= $t->licenseTypeID ?>"><?= $t->name ?></option>
        <? endforeach; ?>
            </select>
        </td>
        <td><input type="text" name="maxQty"></td>
        <td><input type="text" name="price"></td>
        <td><select name="priceType">
            	<option value="Dollars"><?php echo __("Dollars", "Dollars"); ?></option>
            	<option value="Percent"><?php echo __("Percent", "Percent"); ?></option>
            </select>
        </td>
        <td><input type="submit" name="add" value="<?php echo __("add", "add"); ?>"></td>
        </form>
    </tr>
<? foreach($pricing as $p): ?>
    <tr>
        <td><?= $p->name ?></td>
        <td><?= $p->maxQty ?></td>
        <td>
            <?= $p->priceType == 'Dollars' ? format_money_symbol($business[0]->businessID, '%.2n', $p->price) : $p->price.'%'; ?></td>
        <td></td>
        <td><a href="/admin/superadmin/license_pricing_delete/<?= $p->licensePricingID ?>/<?= $business[0]->businessID ?>"><img src="../../../images/icons/cross.png" width="16" height="16" alt=""></a></td>
    </tr>
<? endforeach; ?>
</tbody>
</table>