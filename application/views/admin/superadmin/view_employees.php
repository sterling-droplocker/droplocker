<?php enter_translation_domain("admin/superadmin/view_employees"); ?>
<?php 

$fieldList = array();
$fieldList['firstName'] = '<th>' . __("FirstName", "FirstName") . '</th>';
$fieldList['lastName'] = '<th>' . __("LastName", "LastName") . '</th>';
            
$nameHeader = getSortedNameForm($fieldList);

?><script type="text/javascript">
$(document).ready(function() {
    /**
    $("#employee_table").dataTable(
    }).columnFilter();
        */
       $("#employee_table").dataTable(
        {
            "iDisplayLength" : 25,
            "aLengthMenu": [ [25, 50, 100, 250, 500, -1],[25, 50, 100, 250, 500, "All"] ],
            "bJQueryUI": true,
            "sDom": '<"H"lTfr>t<"F"ip>',
            "oTableTools": {
                <?php if (in_bizzie_mode() && !is_superadmin()): ?>
                aButtons : [
                    "pdf", "print"
                ],
                <?php endif ?>                
                "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
            }
        }).columnFilter({
            sPlaceHolder: "head:before",
            aoColumns: [
                null,null,{ type: "select"}, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
            ]
    });
    
});
</script>
<?php

$fieldList = array();
$fieldList['address1'] = "<th> ". __("Address", "Address") . "</th>";
$fieldList['address2'] = "<th> ". __("Address 2", "Address 2") . "</th>";
$fieldList['city'] = "<th> ". __("City", "City") . "</th>";
$fieldList['state'] = "<th> ". __("State", "State") . "</th>";
$fieldList['zip'] = "<th> ". __("Zipcode", "Zipcode") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['address1'] = "<td>{$location->address}</td>";
    $fieldList['address2'] = "<td>{$location->address2}</td>";
    $fieldList['city'] = "<td>{$location->city}</td>";
    $fieldList['state'] = "<td>{$location->state}</td>";
    $fieldList['zip'] = "<td>{$location->zipcode}</td>";

    return getSortedAddressForm($fieldList);
}
        
?>
<h2><?php echo __("View Employees", "View Employees"); ?></h2>

<table id="employee_table">
    <thead>
        <tr>
            <th> <?php echo __("Edit", "Edit"); ?> </th>
            <th> <?php echo __("Deactivate", "Deactivate"); ?> </th>
            <th> <?php echo __("Business", "Business"); ?> </th>
            <?= $nameHeader ?>
            <th> <?php echo __("Email", "Email"); ?> </th>
            <th> <?php echo __("Phone Home", "Phone Home"); ?> </th>
            <th> <?php echo __("Phone Cell", "Phone Cell"); ?> </th>
            <?= $addressTableHeader ?>
            <th> <?php echo __("Type", "Type"); ?> </th>
            <th> <?php echo __("Super Admin", "Super Admin"); ?> </th>
            <th> <?php echo __("Employee Status", "Employee Status"); ?> </th>
            <th><?php echo __("Start Date", "Start Date"); ?> </th>
            <th> <?php echo __("End Date", "End Date"); ?> </th>
            
        </tr>
    </thead>
    <tbody>
            <? foreach($employees as $employee):?>
            <tr>
                <td align="center" width='25'><a href='/admin/employees/manage/<?= $employee->employeeID?>'><img src='/images/icons/application_edit.png' /></a></td>
                <td align="center" width='25'> 
                    <?php if ($employee->empStatus == 1): ?>
                        <a href='/admin/employees/deactivate/<?=$employee->employeeID?>'> <img src='/images/icons/cross.png' /> </a> 
                    <?php endif ?>
                </td>
                <td> <?= @$empBus[$employee->employeeID] ?></td>
                <?= nameForRow($employee) ?>
                <td> <?= $employee->email ?> </td>
                <td> <?= $employee->phoneHome ?> </td>
                <td> <?= $employee->phoneCell ?> </td>
                <?= getAddressRow($employee) ?>
                <td> <?= $employee->type ?> </td>
                <td> <?= $employee->superAdmin ?> </td>
                <td> <?= $employee->empStatus ?> </td>
                <td> <?= convert_from_gmt_aprax($employee->startDate, SHORT_DATE_FORMAT) ?></td>
                <td> <?= convert_from_gmt_aprax($employee->endDate, SHORT_DATE_FORMAT) ?> </td>
                
            </tr>
            <? endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>