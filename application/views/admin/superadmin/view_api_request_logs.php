<?php enter_translation_domain("admin/superadmin/view_api_request_logs"); ?>
<h2> <?php echo __("Api Request Logs", "Api Request Logs"); ?> </h2>

<table class="box-table-a">
    <thead>
        <th> <?php echo __("ID", "ID"); ?> </th>
        <th> <?php echo __("Created", "Created"); ?> </th>
        <th> <?php echo __("Request Type", "Request Type"); ?> </th>
        <th> <?php echo __("URL", "URL"); ?> </th>
        <th> <?php echo __("Request Body", "Request Body"); ?> </th>
        <th> <?php echo __("Client IP Address", "Client IP Address"); ?> </th>
        <th> <?php echo __("User Agent String", "User Agent String"); ?> </th>
        <th> <?php echo __("Server IP Address", "Server IP Address"); ?> </th>
        <th> <?php echo __("Server Host Name", "Server Host Name"); ?> </th>
    </thead>
    <tbody>
        <?php foreach ($api_request_logs as $api_request_log): ?>
            <tr>
                <td> <?= $api_request_log->api_request_logID ?> </td>
                <td> <?= $api_request_log->created ?> </td>
                <td> <?= $api_request_log->requestType ?> </td>
                <td> <?= $api_request_log->url ?> </td>
                <td> <?= $api_request_log->requestBody ?> </td>
                <td> <?= $api_request_log->clientAddress ?> </td>
                <td> <?= $api_request_log->userAgentString ?> </td>
                <td> <?= $api_request_log->serverAddress ?> </td>
                <td> <?= $api_request_log->hostname ?> </td>
                
            </tr>
        <?php endforeach ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8"> <?= $pagination ?> </td>
        </tr>
    </tfoot>
</table>

