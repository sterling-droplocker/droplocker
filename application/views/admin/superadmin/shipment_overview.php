<?php enter_translation_domain("admin/superadmin/shipment_overview"); ?>
<h2><?php echo __("Shipment Overview", "Shipment Overview"); ?></h2>
<br>
<form action="/admin/superadmin/shipment_edit" method="post">
<input type="hidden" name="new" value="1">
<input type="submit" value="<?php echo __("Create Shipment", "Create Shipment"); ?>" class="button blue">
</form>
<br>
<br>
<table id="box-table-a">
<tr>
    <th><?php echo __("Shipment", "Shipment"); ?></th>
    <th><?php echo __("Status", "Status"); ?></th>
    <? for ($i = 10; $i >= -5; $i--): $week = date("M d", strtotime("-$i week"))?>
    <th><?= $week ?></th>
    <? endfor; ?>
    <th><?php echo __("Notes", "Notes"); ?></th>
    <th></th>
</tr>

<? foreach($shipments as $shipment): ?>
<tr>
    <td>
        <div style="font-weight: bold;"><?= $shipment->what ?> <?php echo __("for", "for"); ?> <?= $shipment->customer ?></div><br>
        <?php echo __("PO", "PO #:"); ?> <?= $shipment->po ?><br>
        <?php echo __("Invoice", "Invoice #:"); ?> <?= $shipment->invoice ?><br>
    </td>
    <td><?= $shipment->status ?></td>
    <? for ($i = 10; $i >= -5; $i--): $week = date("W", strtotime("-$i week"))?>
    <td <?= date("W") == $week ? ' style="background: Yellow;"' : '';?>><?= $weekEvents[$shipment->shipmentID][$week] ?></td>
    <? endfor; ?>
    <td><?= $shipment->notes ?></td>
    <td><form action="/admin/superadmin/shipment_edit" method="post">
        <input type="hidden" name="edit" value="<?= $shipment->shipmentID ?>">
        <input type="submit" name="view" value="<?php echo __("edit", "edit"); ?>" class="button orange">
        </form>
    </td>
</tr>
<? endforeach; ?>
</table>
<br>
<br>
<div style="float: left;">
<?php echo __("Upcoming Shipments and Notes", "Upcoming Shipments and Notes:"); ?><br><br>
<form action="/admin/superadmin/shipment_notes" method="post">
<textarea cols="50" rows="7" name="shipmentNotes"><?= $shipmentNotes ?></textarea>
<br>
<input type="submit" name="update" value="<?php echo __("Update", "Update"); ?>" class="button blue">
</form>
</div>
<div style="vertical-align: top; float: right;"><form action="/admin/superadmin/shipment_overview" method="post">
<input type="submit" name="completed" value="<?php echo __("Show Completed Shipments", "Show Completed Shipments"); ?>" class="button blue">
</form></div>

