<?php enter_translation_domain("admin/superadmin/locations"); ?>
<?php


?>
<?php

$fieldList = array();
$fieldList['state'] = "<th> ". __("State", "State") . "</th>";
$fieldList['country'] = "<th> ". __("Country", "Country") . "</th>";
$fieldList['city'] = "<th> ". __("City", "City") . "</th>";
$fieldList['address1'] = "<th> ". __("Address", "Address") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['state'] = "<td>{$location->state}</td>";
    $fieldList['country'] = "<td>{$location->country}</td>";
    $fieldList['city'] = "<td>{$location->city}</td>";
    $fieldList['address1'] = "<td>{$location->address}</td>";

    return getSortedAddressForm($fieldList);
}
        
?>

<h2><?php echo __("All Locations", "All Locations"); ?></h2>

<br>
Business: <select name="business_id" id="business_id">
    <option value=""><?php echo __("ALL", "ALL"); ?></option>
    <option value="HQ"><?php echo __("HQ", "HQ"); ?></option>
    <? foreach($businessArray as $businessID => $businessName): ?>
    <option value="<?= $businessID ?>"><?= $businessName ?></option>
    <? endforeach; ?>
</select>

<br><br>
<div id="map_canvas" style="width:1000px;height:500px;border:1px solid #666;margin-top:5px;"></div>

<br><br>
<table id="locations">
<thead>
    <tr>
        <th><?php echo __("ID", "ID"); ?></th>
        <th><?php echo __("Business", "Business"); ?></th>
        <th><?php echo __("Company", "Company"); ?></th>
        <?= $addressTableHeader ?>
        <th><?php echo __("Public", "Public"); ?></th>
        <th><?php echo __("Type", "Type"); ?></th>
        <th><?php echo __("Bizzie", "Bizzie"); ?></th>
        <th><?php echo __("Map", "Map"); ?></th>
    </tr>
</thead>
<tbody>
<?php foreach ($locations as $i => $location): ?>
    <tr>
        <td><?= $location->locationID ?></td>
        <td><?= $location->business ?></td>
        <td><?= $location->companyName ?></td>
        <?=  getAddressRow($location) ?>
        <td><?= $location->public ?></td>
        <td><?= $location->type ?></td>
        <td><?= $location->bizzie ? 'Yes' : 'No' ?></td>
        <td><a class="show-on-map" href="#" data-id="<?= $location->locationID ?>"><?php echo __("Show", "Show"); ?></a></td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>

<script src="/js/StyledMarker.js" type="text/javascript"></script>
<script type="text/javascript">

var filterBusiness = null;
var visibleLocations = {};

var defaultBusinessAddress = '<?= $business->address ?>';
var defaultBusinessCity = '<?= $business->city ?>';
var defaultBusinessState = '<?= $business->state ?>';

var locations = <?= json_encode($locations); ?>;

var locationsMap;
var locationsMapOptions = {
    zoom: 4,
    center: new google.maps.LatLng( '', '' ),
    mapTypeId: google.maps.MapTypeId.ROADMAP
};
var infoWindow;
var locationsGeocoder;
var locationRadiusOptions = { radius : 500 };
var locationMarkers = {};


var initializeMap = function() {
    locationsMap = new google.maps.Map( document.getElementById("map_canvas"), locationsMapOptions );

    var legend = document.createElement('div');
    legend.id = 'map_legend';
    legend.setAttribute("style", "padding:5px;margin: 0 5px 5px 0;background: #fff;");
    var content = [];
    content.push('<div style="clear:both;padding:0 0 5px 0;"><div class="color" style="border:1px solid #000; width:10px; height:10px; float:left; margin-right: 5px; background:#6ed0f7"></div> <?php echo __("Location", "Location"); ?> </div>');
    content.push('<div style="clear:both;padding:0 0 5px 0;"><div class="color" style="border:1px solid #000; width:10px; height:10px; float:left; margin-right: 5px; background:#56ff56"></div> <?php echo __("HQ", "HQ"); ?> </div>');
    legend.innerHTML = content.join('');
    legend.index = 1;
    locationsMap.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);

    //default business location
    var geoAddressString = defaultBusinessAddress + ', ' + defaultBusinessCity + ', ' + defaultBusinessState;
    locationsGeocoder    = new google.maps.Geocoder();
    infoWindow           = new google.maps.InfoWindow();

    createMarkers(locations);
    resetMapBoundsFromMarkers();
};

var resetMapBoundsFromMarkers = function() {
    var bounds = new google.maps.LatLngBounds();
    
    for (var i in locationMarkers) {
        //  And increase the bounds to take this point
        if (locationMarkers[i].getVisible()) {
            bounds.extend(locationMarkers[i]['position']);
        }
    }

    //  Fit these bounds to the map
    locationsMap.fitBounds (bounds);
};

var createMarkerFromAddress = function(locationData, color) {
    var geoAddressString = locationData.address + ', ' + locationData.city + ', ' + locationData.state + ' ' + locationData.zip ;
    locationsGeocoder.geocode( { address : geoAddressString }, function( res, status ){
        if(res && res.length > 0) {
            var locationLatLon = res[0]['geometry']['location'];

            locationData.lat = locationLatLon.lat;
            locationData.lon = locationLatLon.lng;

            createMarkerFromLatLong(locationData, color);
        }
    });
};

var createMarkerFromLatLong = function (locationData, color) {
    var locationLatLon = new google.maps.LatLng(locationData.lat, locationData.lon);

    var styleIcon = new StyledIcon(StyledIconTypes.MARKER, {color: color});
    var marker = new StyledMarker({styleIcon: styleIcon, position:locationLatLon, map: locationsMap});

    google.maps.event.addListener(marker, 'click', function() {
        locationsMap.setCenter( locationLatLon );
        infoWindow.setContent(locationData.info);
        infoWindow.open(locationsMap,marker);
    });

    locationMarkers[locationData.locationID] = marker;
    
    
};

var createMarker = function(locationData) {
    var color = "#6ed0f7"; //cyan  (location)
    if (String(locationData.companyName).match(/HQ/)) {
	   color = "#56ff56"; // green (HQ)
    }

    if (!locationData.lat || !locationData.lon) {
        createMarkerFromAddress(locationData, color);
    } else {
	    createMarkerFromLatLong(locationData, color);
    }
};

var createMarkers = function(locations) {
    for (var i in locations) {
	    var locationData = locations[i];
        visibleLocations[locationData.locationID] = true;
	    createMarker(locationData);
    }
};

var applyFilters = function() {
    for (var i in locations) {
	    var locationData = locations[i];
        var locationID = locationData.locationID;
        var marker = locationMarkers[locationID] ? marker = locationMarkers[locationID] : null;

        var visible = true
        if (filterBusiness == 'HQ') {
            visible = String(locationData.companyName).match(/HQ/) ? true : false;
        } else if (filterBusiness) {
            visible = locationData.business_id == filterBusiness;
        }

        if (marker) {
            marker.setVisible(visible);
        }

        visibleLocations[locationID] = visible;
    }    
};

$.fn.dataTableExt.afnSortData['attr'] = function  ( oSettings, iColumn )
{
	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
		return $('td:eq('+iColumn+')', tr).attr('data-value');
	});
};

var logged = false;

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var nTr = oSettings.aoData[ iDataIndex ].nTr;
        var locationID = $('td:eq(0)', nTr).text();

        return typeof(visibleLocations[locationID]) != 'undefined' ? visibleLocations[locationID] : true;
    }
);


$locations_dataTable = $("#locations").dataTable({
    "bPaginate" : false,
    "bJQueryUI": true,
    "fnStateLoad": function (oSettings) {
        return JSON.parse( localStorage.getItem('DataTables') );
    },
    "sDom": '<"H"lTfr>t<"F"ip>',
    "oTableTools": {
        <?php if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <?php endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls.swf"
    }

});

new FixedHeader($locations_dataTable);


$('#business_id').change(function(evt) {
    filterBusiness = $(this).val();
    applyFilters();
    resetMapBoundsFromMarkers();
    
    $locations_dataTable.fnDraw();
});

$('#locations').on('click', '.show-on-map', function(evt) {
    evt.preventDefault();
    var locationID = $(this).attr('data-id');
    var locationData = locations[locationID];
    var marker = locationMarkers[locationID];

    if (marker) {
        locationsMap.setZoom(20);
        locationsMap.panTo(marker.position);
        infoWindow.setContent(locationData.info);
        infoWindow.open(locationsMap,marker);
    }
});


$(function() {
    initializeMap();
});


</script>
