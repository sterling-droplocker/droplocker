<?php enter_translation_domain("admin/superadmin/view_businesses"); ?>
<h2><?php echo __("View Businesses", "View Businesses"); ?></h2>
<?= $this->session->flashdata('message')?>
<?php

$fieldList = array();
$fieldList['address1'] = "<th> ". __("Address", "Address") . "</th>";
$fieldList['address2'] = "<th> ". __("Address 2", "Address 2") . "</th>";
$fieldList['city'] = "<th> ". __("City", "City") . "</th>";
$fieldList['state'] = "<th> ". __("State", "State") . "</th>";

$addressTableHeader = getSortedAddressForm($fieldList);
                                    
//------------------

function getAddressRow($location) {
    $fieldList = array();
    $fieldList['address1'] = "<td>{$location->address}</td>";
    $fieldList['address2'] = "<td>{$location->address2}</td>";
    $fieldList['city'] = "<td>{$location->city}</td>";
    $fieldList['state'] = "<td>{$location->state}</td>";

    return getSortedAddressForm($fieldList);
}
        
?>

<table class="box-table-a">
    <thead>
        <tr>
            <th> <?php echo __("Edit", "Edit"); ?> </th> 
            <th> <?php echo __("Business ID", "Business ID"); ?> </th>
            <th> <?php echo __("Company Name", "Company Name"); ?> </th>
            <th> <?php echo __("Slug", "Slug"); ?> </th>
            <?= $addressTableHeader ?>
            <th> <?php echo __("Phone", "Phone"); ?> </th>
            <th> <?php echo __("Email", "Email"); ?> </th>
            <th> <?php echo __("Website URL", "Website URL"); ?> </th>
            <th> <?php echo __("Subdomain", "Subdomain"); ?> </th>
            <th> <?php echo __("Blank Customer ID", "Blank Customer ID"); ?></th>
            <th> <?php echo __("Latitude", "Latitude"); ?> </th>
            <th> <?php echo __("Longitude", "Longitude"); ?> </th>
            <th> <?php echo __("Default Report Cut Off Time", "Default Report Cut Off Time"); ?> </th>
            <th> <?php echo __("Store Access", "Store Access"); ?> </th>
            <th> <?php echo __("Facebook API Key", "Facebook API Key"); ?> </th>
            <th> <?php echo __("Facebook Secret Key", "Facebook Secret Key"); ?> </th>
            <th> <?php echo __("Timezone", "Timezone"); ?> </th>
            <th> <?php echo __("Locale", "Locale"); ?> </th>
            <th> <?php echo __("Payment Token", "Payment Token"); ?> </th>
            <th> <?php echo __("Return to Office Locker ID", "Return to Office Locker ID"); ?> </th>
            <th> <?php echo __("Service Types", "Service Types"); ?> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($businesses as $key => $business):?>
            <tr>
                <td align="center" width='25'> <a href='/admin/superadmin/edit_business/<?php echo $business->businessID?>'> <img src='/images/icons/application_edit.png' /> </a> </td>
                <td> <?= $business->businessID ?> </td>
                <td> <?= $business->companyName ?> </td>
                <td> <?= $business->slug ?> </td>
                <?= getAddressRow($business) ?>
                <td> <?= $business->phone ?> </td>
                <td> <?= $business->email ?> </td>
                <td> <?= $business->website_url ?> </td>
                <td> <?= $business->subdomain ?> </td>
                <td> <?= $business->blank_customer_id ?> </td>
                <td> <?= $business->lat ?> </td>
                <td> <?= $business->lng ?> </td>
                <td> <?= $business->default_report_cut_off_time ?> </td>
                <td> <?= $business->store_access ?> </th>
                <td> <?= $business->facebook_api_key ?> </td>
                <td> <?= $business->facebook_secret_key ?> </td>
                <td> <?= $business->timezone ?> </td>
                <td> <?= $business->locale ?> </td>
                <td> <?= $business->paymentToken ?> </td>
                <td> <?= $business->returnToOfficeLockerId ?> </td>
                <td> 
                    <?php $serviceTypes = unserialize($business->serviceTypes); ?> 
                    <?php if (is_array($serviceTypes)): ?>
                        <table class='box-table-a'>
                            <thead>
                                <tr>
                                    <th> <?php echo __("Service Type ID", "Service Type ID"); ?></th>
                                    <th> <?php echo __("Service Name", "Service Name"); ?> </th>
                                    <th> <?php echo __("Properties", "Properties"); ?> <th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($serviceTypes as $serviceTypeID => $properties): ?>
                                    <tr>
                                        <td> <?= $serviceTypeID ?> </td>
                                        <td> <?= get_serviceType_name($serviceTypeID) ?> </td>
                                        <td>
                                            <?php if (is_array($properties)): ?>                                 
                                                <table class='box-table-a'>
                                                    <thead>
                                                        <tr> 
                                                            <th> <?php echo __("Name", "Name"); ?> </th>
                                                            <th> <?php echo __("Key Name", "Key Name"); ?> </th>
                                                            <th> <?php echo __("Value", "Value"); ?> </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       <?php if (array_key_exists("serviceType_id", $properties)): ?>
                                                            <tr>

                                                                <td> <?php echo __("Module ID", "Module ID"); ?></td>
                                                                <td> <?php echo __("serviceType_id", "serviceType_id"); ?> </td>
                                                                <td> <?= $properties['serviceType_id'] ?> </td>

                                                            </tr>
                                                        <?php endif ?>
                                                        <?php if (array_key_exists("active", $properties)): ?>
                                                            <tr>
                                                                <td> <?php echo __("Active", "Active"); ?> </td>
                                                                <td> <?php echo __("active", "active"); ?> </td>
                                                                <td> <?= $properties['active'] ?> </td>
                                                            </tr>
                                                        <?php endif ?>
                                                        <?php if (array_key_exists("service", $properties)): ?>
                                                            <tr>
                                                                <td> <?php echo __("Service", "Service"); ?> </td>
                                                                <td> <? echo __("php", "php"); ?>service </td>
                                                                <td> <?= $properties['service'] ?> </td>

                                                            </tr>
                                                        <?php endif ?>
                                                        <?php if (array_key_exists("displayName", $properties)): ?>
                                                            <tr>
                                                                <td> <?php echo __("Display Name", "Display Name"); ?> </td>
                                                                <td> <?php echo __("displayName", "displayName"); ?> </td>
                                                                <td> <?= $properties['displayName'] ?> </td>
                                                            </tr>
                                                        <?php endif ?>
                                                    </tbody>
                                                </table>
                                            <?php endif ?>
                                        </td>

                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
