<?php enter_translation_domain("admin/superadmin/manage_aclresources"); ?>
<?= form_open("admin/aclresources/add", array("class" => "aprax_form")) ?>
<?= form_fieldset(__("Add New Acl Resource", "Add New Acl Resource")) ?>
<?= form_label(__("Resource", "Resource"), "resource") . " " . form_input("resource") ?>
<?= form_label(__("Description", "Description"), "description") . " " . form_input("description") ?>

<?= form_submit(array("value" => __("Add", "Add"), "class" => "button orange")) ?>
<?= form_fieldset_close() ?>
<?= form_close() ?>
<table id="aclresources_table">
    <thead>
        <tr>
            <th> <?php echo __("ID", "ID"); ?> </th>
            <th> <?php echo __("Resource", "Resource"); ?> </th>
            <th> <?php echo __("Description", "Description"); ?> </th>
            <th> <?php echo __("ACL Group", "ACL Group"); ?> </th>
            <th> <?php echo __("Acl Group Order", "Acl Group Order"); ?> </th>
            <th> <?php echo __("Default Value", "Default Value"); ?> </th>
            <th> <?php echo __("Delete", "Delete?"); ?> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($aclResources as $aclResource): ?>
            <tr>
                <td> <?= $aclResource->id ?> </td>
                <td> <?= $aclResource->resource?> </td>
                <td> <?= $aclResource->description?> </td>
                <td> <?= $aclResource->aclgroup?> </td>
                <td> <?= $aclResource->aclgrouporder ?> </td>
                <td> <?= $aclResource->default_value ?> </td>
                <td> <a href="/admin/aclresources/delete?resource=<?=$aclResource->resource?>"> <img src='/images/icons/cross.png' alt="Delete <?=$aclResource->resource?>" /> </a> </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
    $("#aclresources_table").dataTable({
        "bPaginate" : false,
        "bJQueryUI" : true
    });
</script>


