<?php enter_translation_domain("admin/superadmin/email_admin"); ?>
<?php

$date_format = get_business_meta($this->business_id, "fullDateDisplayFormat");

?>
<h2><?php echo __("Email Admin", "Email Admin"); ?></h2>
<p><?php echo __("Refresh", "Refresh:"); ?> <a href='/<?= $this->uri->uri_string()?>'><?php echo __("Now", "Now"); ?></a></p>

<h2><?php echo __("Email Server Settings", "Email Server Settings"); ?></h2>
<?php if(get_server_meta('amazon_ses_username') == '' || get_server_meta('amazon_ses_password') == ''): ?>
<div style='background-color:#cc0000; color:#FFF; padding:10px;'><?php echo __("You must set the email server settings before sending emails", "You must set the email server settings before sending emails"); ?></div>
<?php endif ?>
<form action='/admin/superadmin/update_amazon_ses_keys' method='post'>
<table class="detail_table">
<tr>
        <th><?php echo __("Amazon SES Username", "Amazon SES Username"); ?></th>
        <td><input type='text' name='amazon_ses_username' value="<?= get_server_meta('amazon_ses_username')?>" /></td>
    </tr>
    <tr>
        <th><?php echo __("Amazon SES Password", "Amazon SES Password"); ?></th>
        <td><input type='text' name='amazon_ses_password' value="<?= get_server_meta('amazon_ses_password')?>" /></td>
    </tr>
</table>
<input type='submit' class='button orange' value='<?php echo __("Update", "Update"); ?>' name='submit' />
</form>
<br>

<? if($this->uri->uri_string() == 'admin/superadmin/email_admin'): ?>
<table class='detail_table'>
<tr>
        <th><?php echo __("Send All", "Send All"); ?><br>
        <span style='font-size:10px'> <?php echo __("When this is ON, all pending emails will be sent. Normally only emails that are older then the current server time are sent", "When this is ON, all pending emails will be sent. Normally only emails that are older then the current server time are sent"); ?></span></th>
        <td><?= $sendAllEmails?></td>
    </tr>
    <tr>
        <th><?php echo __("Cron Status", "Cron Status"); ?><br>
        <span style='font-size:10px'><?php echo __("The email cron job runs every minute", "The email cron job runs every minute when this is \"ON\". If this is \"OFF\", the cron job is not running"); ?></span></th>
        <td width='*' class='<?= $class?>'><?= $emailCronStatus?></td>
    </tr>
    <tr>
        <th><?php echo __("Manual Mode", "Manual Mode"); ?><br>
        <span style='font-size:10px'><?php echo __("When any business is sending manually", "When any business is sending manually, this will be \"ON\". If this is \"ON\" the email cron job WILL NOT be running"); ?></span></th>
        <td class='<?= $class?>'><?= $manualEmailProcessing?></td>
    </tr>
    <tr>
        <th><?php echo __("Cron Email Limit", "Cron Email Limit"); ?><br>
        <span style='font-size:10px'><?php echo __("How many emails to send per cron job, or manual send", "How many emails to send per cron job, or manual send"); ?></span></th>
        <td colspan='2' class='<?= $class?>'>
            <form action='/admin/superadmin/updateLimit' method='post'>
                <input style='width:50px' type='text' name='emailLimit' value="<?= $emailLimit?>"/>
                <input type='submit' value='<?php echo __("Update Limit", "Update Limit"); ?>' name='updateLimitSubmit' />
            </form>
        </td>
    </tr>
     <tr>
        <th class='<?= $class?>'><?php echo __("Current Queue Size", "Current Queue Size"); ?><br>
        <span style='font-size:10px'><?php echo __("Number of emails currently in the queue", "Number of emails currently in the queue"); ?></span></th>
        <td colspan='2' class='<?= $class?>'><?= $totalEmails?></td>
    </tr>
    
</table>
<? endif; ?>

<p></p>
<form action='/admin/admin/manualProcess' method='post'>
<div style='clear:both; margin:5px;'>
    <div style='float:left'><h3><?php echo __("Current Queue", "Current Queue"); ?></h3></div>
    <div style='float:right; margin:0px 10px;'><input class='button red' type='submit' value='<?php echo __("Send All", "Send All (Up to %limit%)", array("limit"=>$emailLimit)); ?>' name='submitAll' /></div>
    <div style='float:right; margin:0px 10px;'><input class='button blue' type='submit' value='<?php echo __("Send Selected", "Send Selected"); ?>' name='submit' /></div>
<br style='clear:both'>
</div>

<table class='box-table-a'>
    <tr>
        <th><?php echo __("To", "To"); ?></th>
        <th><?php echo __("Subject", "Subject"); ?></th>
        <th><?php echo __("Send Date", "Send Date"); ?></th>
        <th><?php echo __("Date Created", "Date Created"); ?></th>
        <? if($this->uri->uri_string() == 'admin/superadmin/email_admin'): ?>
        <th><?php echo __("Business", "Business"); ?></th>
        <? endif; ?>
        <th width='25'><?php echo __("Send Now", "Send Now"); ?></th>
    </tr>
    <? if($emails):foreach($emails as $email): ?>
    <tr>
        <td><?= $email->to?></td>
        <? $subject = (strlen($email->subject)>100)?substr($email->subject,0, 100)."...":$email->subject; ?>
        <td><a href='/admin/customers/email_detail/<?= $email->emailID?>'><?= $subject?></a></td>
        <td><?= convert_from_gmt_aprax($email->sendDateTime, $date_format) ?></td>
        <td><?= convert_from_gmt_aprax($email->dateCreated, $date_format) ?></td>
        <? if($this->uri->uri_string() == 'admin/superadmin/email_admin'): ?>
        <td><?= $email->companyName?></td>
        <? endif; ?>
        <td align='center'><input type='checkbox' name='emails[]' value='<?= $email->emailID?>'></td>
    </tr>
    <? endforeach; else:?>
    <tr>
        <td colspan='10'><?php echo __("Nothing in the queue", "Nothing in the queue"); ?></td>
    </tr>
    <? endif; ?>
</table>
</form>
