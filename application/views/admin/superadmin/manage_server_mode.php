<?php enter_translation_domain("admin/superadmin/manage_server_mode"); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $(":submit").loading_indicator();
    });
    
</script>
<style type="text/css">
    fieldset
    {
        border: 1px solid;
        margin: 5px 0;
    }
    fieldset legend
    {
        font-weight: bold;
    }
    select
    {
        margin: 5px;
    }
</style>
<?= form_open("/admin/superadmin/update_server_mode") ?>
<h1> <?php echo __("Change Server Mode", "Change Server Mode"); ?> </h1>
<fieldset>
    <legend> <?php echo __("Mode", "Mode"); ?> </legend>
    

<?= form_dropdown("mode", $modes, $mode) ?>
<?= form_submit(array("class" => "button orange", "value" => __("Update", "Update"))) ?>
</fieldset>
<?= form_close() ?>

