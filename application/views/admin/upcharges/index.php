<?php enter_translation_domain("admin/upcharges/index"); ?>
<script>

$(document).ready(function(){

   var nameText = $("<input type='text' name='name' />");
   var nameSelect = $("<select id='name' name='name'>");

   var addButton = $("<a href='javascript:;'><img class='imgButtonAddTextBox' src='/images/icons/add.png' /></a>").click(function(){
       $(this).parent().html(nameText).append(cancelButton);
   });

   var cancelButton = $("<a href='javascript:;'><img src='/images/icons/cross.png' /></a>").click(function(){
       $(this).parent().html(nameSelect).append(addButton);
   });

   $(".upchargeNameSelectHolder").append(nameSelect).append(addButton);

   $('.editButton').click(function(){
       var id = $(this).attr('data-id');
       $("#editDiv").html("").load("/admin/upcharges/edit/" + id).dialog({modal:true, height: 400, width: 500,"title" :"<?=__("Edit Upcharge", "Edit Upcharge"); ?> ",});
   });


   $('.editSuppliers').click(function(){
       var id = $(this).attr('data-id');
       $("#editDiv").html("").load("/admin/upcharges/edit_suppliers/" + id).dialog({modal:true, height: 400, width: 500,"title" :"<?=__("Edit Upcharge Suppliers", "Edit Upcharge Suppliers"); ?>",});
   });


	$('.inline-hidden').hide();
	$('.toggle-form').click(function(evt) {
		$parent = $(this).hide().parent();
		$('.value', $parent).hide();
		$('.inline-hidden', $parent).show();
	});

});
</script>

<h2><?=__("Upcharges", "Upcharges"); ?></h2>

<form action='/admin/upcharges/add_group' method='post'>
<table class='detail_table'>
    <tr>
        <th><?=__("Add Group Name", "Add Group Name"); ?></th>
        <td><input type='text' name='name' value='' /></td>
        <td><input class='button orange' type='submit' name='submit' value="<?=__("Add Group", "Add Group"); ?>" /> </td>
    </tr>
</table>

</form>

<div>
<?php if(!empty($upchargeGroups)): foreach($upchargeGroups as $group):?>
    <h2>
		<span class="value"><?php echo $group->name ? $group->name : '<?=__("-NO-NAME-", "-NO-NAME-"); ?>' ?></span>
		<img src="/images/icons/application_edit.png" class="toggle-form">
		<form method="post" action="/admin/upcharges/update_group/<?= $group->upchargeGroupID ?>" class="inline-hidden">
			<input type="text" name="name" value="<?= htmlentities($group->name) ?>">
			<input type="submit" value="<?=__("Update", "Update"); ?>">
		</form>
    </h2>

    <table class='box-table-a'>
        <tr>
            <th><?=__("Upcharge", "Upcharge"); ?></th>
            <th><?=__("Price", "Price"); ?></th>
            <th><?=__("Every Order", "Every Order"); ?></th>
            <th><?=__("Supplier", "Supplier"); ?></th>
            <th><?=__("Cost", "Cost"); ?></th>
            <th></th>
        </tr>
        <form action='/admin/upcharges/add' method='post'>
         <tr>
            <!-- <td class='upchargeNameSelectHolder' id='nametd-<?php echo $group->upchargeGroupID?>'></td> -->
            <td><input type='text' name='name' /></td>
            <td>
                <input type="text" name="upchargePrice" size="5">
                <select name="priceType">
            	    <option value="dollars" SELECTED><?=__("Dollars", "Dollars"); ?></option>
            	    <option value="percent"><?=__("Percent", "Percent"); ?></option>
                </select>
            </td>
            <td>
                <select name="everyOrder">
            	    <option value="1" SELECTED><?=__("Every Order", "Every Order"); ?></option>
            	    <option value="0"><?=__("This Order Only", "This Order Only"); ?></option>
                </select>
            </td>
            <td><select id='supplier_id' name='supplier_id'>
            <?php supplier_dropdown($suppliers); //located in the utility_helper ?>
            </select></td>
            <td><input type='text' name='cost' value='' /></td>
            <td width='50'><input type='submit' name='add_upcharge' value="add" /></td>
        </tr>
        <input type='hidden' value='<?php echo $group->upchargeGroupID?>' name="upchargeGroup_id" />
        </form>
        <?php foreach($group->upcharges as $upcharge): ?>
        <tr>
            <td><span data-id='<?php echo $upcharge->upchargeID?>' class='editButton'><?php echo $upcharge->name?></span></td>
            <td><span data-id='<?php echo $upcharge->upchargeID?>' class='editButton'><?= $upcharge->priceType == 'dollars' ? format_money_symbol($this->business->businessID, '%.2n', $upcharge->upchargePrice) : rtrim($upcharge->upchargePrice,0).'%' ?></span></td>
            <td><span data-id='<?php echo $upcharge->upchargeID?>' class='editButton'><?= $upcharge->everyOrder == 1 ? '<?=__("Every Order", "Every Order"); ?>' :  '<?=__("This Order Only", "This Order Only"); ?>'?></span></td>
            <td><span data-id='<?php echo $upcharge->upchargeID?>' class='editSuppliers' ><?php echo $upcharge->companyName ?></span></td>
            <td><span data-id='<?php echo $upcharge->upchargeID?>' class='editSuppliers' ><?php echo format_money_symbol($this->business->businessID, '%.2n', $upcharge->cost) ?></span></td>
            <td><form action='/admin/upcharges/delete' method='post'><input type='hidden' name='upcharge_id' value='<?php echo $upcharge->upchargeID?>' /><input type='submit' onClick="return verify_delete();" name='delete_upcharge' value="delete" /></form></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endforeach;else:?>
<p>
<h3><?=__("No upcharge groups set. Please add an upcharge group", "No upcharge groups set. Please add an upcharge group"); ?></h3>
</p>
<?php endif;?>
</div>
<div id='editDiv' style='display:none'></div>
