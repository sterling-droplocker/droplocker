<?php enter_translation_domain("admin/upcharges/edit_upcharge"); ?>
<h2 style='text-align:left'><?=__("Update Upcharge", "Update Upcharge"); ?></h2>

<form action='/admin/upcharges/update/<?= $upcharge_id?>' method='post'>
   <table class='detail_table'>
       <tr>
           <th style='width:100px'><?=__("Name", "Name"); ?></th>
           <td align="right"><input type='text' name='name' value="<?= $name?>" /></td>
       </tr>
       
       <tr>
           <th style='width:100px'><?=__("Price", "Price"); ?></th>
           <td align="right"><input type='text' name='price' value="<?= $price?>" /></td>
       </tr>
       <tr>
           <th style='width:100px'><?=__("Every Order", "Every Order"); ?></th>
           <td align="right"><select name="everyOrder">
                    <option value="1" <?= $everyOrder == 1 ? 'SELECTED' :'' ?>><?=__("Every Order", "Every Order"); ?></option>
                    <option value="0" <?= $everyOrder == 0 ? 'SELECTED':''?>><?=__("This Order Only", "This Order Only"); ?></option>
                </select>
           </td>
       </tr>
   </table>
   <input type="submit" name='submit' class="button orange" value='<?=__("Update", "Update"); ?>'></button>
</form>
