<?php enter_translation_domain("admin/upcharges/edit_suppliers"); ?>
<p>
    <h2 style='text-align:left'><?=__("Group:", "Group:"); ?> <?= $groupName?></h2>
    <h2 style='text-align:left'><?=__("Upcharge:", "Upcharge:"); ?> <?= $name?></h2>
 </p>   
<form action='/admin/upcharges/update_suppliers/<?= $upchargeID?>' method='post'>
   <table class='box-table-a'>
       <tr>
           <th style='width:100px'><?=__("Default", "Default"); ?></th>
           <th style='width:100px'><?=__("Supplier", "Supplier"); ?></th>
           <th style='width:100px'><?=__("Cost", "Cost"); ?></th>
           
       </tr>
       <?php foreach($suppliers as $supplier): ?>
       <tr>
           <td><input <?php  if($supplier['default']==1) {echo "checked";}?> type='radio' name='default' value='<?= $supplier['supplier_id'] ?>'/></td>
           <td><?= $supplier['companyName']?></td>
           <td><input name='cost[<?= $supplier['supplier_id']?>][]' type='text' value="<?= number_format_locale($supplier['cost'], 2)?>" /></td>
       </tr>
       
       <?php endforeach; ?>
   </table>
   <input type="submit" name='submit' class="button orange" value='<?=__("Update", "Update"); ?>'></button>
</form>
