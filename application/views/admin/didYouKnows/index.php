<?php enter_translation_domain("admin/didYouKnows/index"); ?>
<?php

$can_edit = !in_bizzie_mode() || is_superadmin();

?>
<h2><?php echo __("Did You Know Notices", "Did You Know Notices"); ?></h2>
<?= $this->session->flashdata("message") ?>

<table class="box-table-a">
    <thead>
        <tr>
            <th width="20"> <?php echo __("ID", "ID"); ?> </th>
            <th> <?php echo __("Note", "Note"); ?> </th>
            <? if ($can_edit): ?>
            <th width="60"> <?php echo __("Delete", "Delete?"); ?> </th>
            <? endif ?>
    </thead>
    <tbody>
        <? if ($can_edit): ?>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <?= form_open($add_action, array("id" => "add_form")) ?>
                        <div class="fill">
                            <textarea cols="40" rows="6" name="note"></textarea>
                        </div>
                        <div style="text-align:right;">
                            <input type="submit" name="add" value="<?php echo __("Add Note", "Add Note"); ?>" class="button blue" style="margin-top:5px;">
                        </div>
                    <?= form_close() ?>
                </td>
                <td>&nbsp;</td>
            </tr>
        <? endif ?>
        <? if (empty($didYouKnows)): ?>
            <tr>
                <td colspan=4>
                    <p> <?php echo __("There are no Did You Know Notices set for this business", "There are no <i> Did You Know Notices </i> set for this business."); ?> </p>
                </td>
            </tr>
        <? else: ?>
            <? foreach($didYouKnows as $didYouKnow): ?>
                <tr>
                    <td> <?= $didYouKnow->didYouKnowID ?></td>
                    <td>
                        <? if ($can_edit): ?>
                            <?= form_open($update_action, array("id" => "update_form")) ?>
                                <div class="fill">
                                    <textarea cols="40" rows="6" name="note"><?= stripslashes($didYouKnow->note) ?></textarea> <br />
                                </div>
                                <input type="hidden" name="id" value="<?= $didYouKnow->didYouKnowID ?>">
                                <div style="text-align:right;">
                                    <input type="submit" name="update" value="<?php echo __("Update", "Update"); ?>" class="button orange" style="margin-top:5px;">
                                </div>
                            <?= form_close() ?>
                        <? else: ?>
                                <?= stripslashes($didYouKnow->note) ?>
                        <? endif ?>
                    </td>
                    <? if ($can_edit): ?>
                        <td align="center"> 
                            <a id="delete-<?=$didYouKnow->didYouKnowID?>" onclick="return verify_delete()" href="/admin/didYouKnows/delete/<?= $didYouKnow->didYouKnowID ?>"><img src="/images/icons/cross.png"></a>
                        </td>
                    <? endif ?>
                </tr>
            <? endforeach; ?>
        <? endif ?>
    </tbody>
</table>

