<?php enter_translation_domain("admin/drivers/manifest"); ?>
<?php

?>
<html>
<head>
<title><?= $companyName ?> <?php echo __("Driver Map for", "Driver Map for"); ?> <?= date("D, M d")?></title>

<style type="text/css">
html, body {
    font-family: Arial, Helvetica, sans-serif;
}

#map-canvas {
    height:1200px;
}

#driver_map_form
{
    background-color: #eaeaea;
    border: 1px solid #ccc;
    margin: 0 0 5px 0;
    padding: 10px 5px;
}

.map-stop-type {
    margin: 5px 0;
}

.manifest-table {
    border-collapse: collapse;
    border: 2px solid #000;
    font-size: 14px;
    margin: 0 0 20px 0;
}

.manifest-table th {
    background: #555;
    color: #fff;
    font-weight: normal;
}

.manifest-table td,
.manifest-table th {
    border: 1px solid #999;
    padding: 5px;
    vertical-align: top;
}

.stripped tr:nth-child(even) {
    background: #eee;
}

.stripped tr:nth-child(odd) {
    background: #fff;
}

.text-center {
    text-align: center;
}

.manifest-title {
    margin: 0;
}

.spinner {
    position: fixed;
    left: 50%;
    top: 50%;
    margin-top: -32px;
    margin-left: -32px;
    background: #fff;
    text-align: center;
    line-height: 50px;
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 15px;
    box-shadow: 1px 1px 3px rgba(0,0,0,.5);
    display: none;
}


</style>

</head>

<script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
<script type="text/javascript" src='/js/jquery-ui-1.8.18.min.js'></script>


<!--
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".revert").click(function() {
        return confirm("Are you sure you want to revert all routes?");
    });
});

</script>
-->
<h2><?= $companyName ?> <?php echo __("Driver Manifest for", "Driver Manifest for"); ?> <?= date("D, M d")?></h2>

<form action="/admin/drivers/manifest" method="get" id="driver_map_form">
<table>
<tr>
    <td>
        <?php echo __("Routes", "Routes:"); ?>
        <? foreach($routes as $route): ?>
        	<label><?= $route->route_id ?><?= form_checkbox('routes[]', $route->route_id, in_array($route->route_id, $selected_routes)); ?></label>
        <? endforeach; ?>
        <?= form_days($this->business_id, 'day', $selected_day); ?>
    </td>
    <td>
        <?= form_dropdown('type', $types, $selected_type); ?>
    </td>
    <td>
        <label><?= form_checkbox('no_map', 1, $no_map); ?><?php echo __("No Map", "No Map"); ?></label><br>
        <label><?= form_checkbox('loaded', 1, $loaded); ?> <?php echo __("Only show loaded deliveries", "Only show loaded deliveries"); ?></label>
    </td>
    <td><input type="submit" value="Run">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td><a href="/admin/reports/assign_drivers"><?php echo __("Assign Drivers", "Assign Drivers"); ?></a><br><a class="revert" href="/admin/reports/routes_revert"><?php echo __("Revert to master routes", "Revert to master routes"); ?></a></td>
</tr>
</table>
</form>

<div id="map-canvas" style="height: <?= $no_map ? 0 : 600 ?>px; margin: 0px; padding: 0px;"></div>
</div>

<script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
<script type="text/javascript" src='/js/jquery-ui-1.8.18.min.js'></script>

<script type="text/javascript">
function initializeMap() {
    $(function() {
        var mapOptions = <?=json_encode(array("lat" => $center->lat, "lon" => $center->lon, "zoom" => 13))?>;

        var settings    = {};
        settings.stops  = <?= json_encode($stops); ?>;
        settings.params = <?= json_encode(array(
            'routes' => $selected_routes,
            'day'    => $selected_day,
            'type'   => $selected_type,
            'loaded' => $loaded ? 1 : 0,
            'no_map' => $no_map ? 1 : 0,
        )); ?>;

        DropLockerDriverMap.initialize(mapOptions, settings);
    });
}

$(function() {
    $('#sort-routes tbody').sortable({
        stop: function(evt, ui) { DropLockerDriverMap.afterSort(evt, ui); }
    });


    var $loading = $('.spinner').hide();

    $(document).ajaxStart(function () {
        $loading.show();
    }).ajaxStop(function () {
        $loading.hide();
    });
});

</script>

<script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,visualization&callback=initializeMap&key=<?=get_default_gmaps_api_key();?>'></script>
<script type="text/javascript" src='/js/oms.min.js'></script>
<script type="text/javascript" src="/js/admin/drivers/driver_map.js?v=3"></script>

<div class="spinner"><img src="/images/loading.gif" width="32" height="32"></div>

<div style="margin: 20px 0 0 0;">
    <div style="float:left; max-width: 50%;">
        <h3 class="manifest-title"><?php echo __("Manifest", "Manifest"); ?></h3>
        <table id="sort-routes" class="manifest-table stripped">
        <thead>
            <tr>
                <th width="50"><?php echo __("Sort", "Sort"); ?></th>
                <th width="150"><?php echo __("Address", "Address"); ?></th>
                <th width="150"><?php echo __("Customer", "Customer"); ?></th>
                <th width="20%"><?php echo __("Access Notes", "Access Notes"); ?></th>
                <th><?php echo __("Start Time", "Start Time"); ?></th>
                <th><?php echo __("Stop Time", "Stop Time"); ?></th>
                <th><?php echo __("Order Type", "Order Type"); ?></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($stops as $i => $stop): ?>
            <tr class="mainfest-stop" data-sortOrder_id="<?= $stop->sortOrder_id ?>" data-type="<?= $stop->type ?>" data-index="<?= $i ?>">
                <td class="manifest-sort text-center"><?= $i + 1 ?></td>
                <td class="manifest-address"><?= $stop->address1 ?> <?= $stop->address2 ?></td>
                <td class="manifest-customer">
                    <a href="/admin/customers/detail/<?= $stop->customer_id ?>"><?= getPersonName($stop) ?></a><br>
                    <?= $stop->phone ?>
                </td>
                <td class="manifest-access"><?= $stop->accessCode ?></td>
                <td class="manifest-start"><?= $stop->localWindowStart ? $stop->localWindowStart : 'n/a' ?></td>
                <td class="manifest-end"><?= $stop->localWindowEnd ?></td>
                <td class="manifest-order-types">
                    <? if ($stop->pickups): ?>
                        Pickups:&nbsp;<?= count($stop->pickups) ?><br>
                    <? endif; ?>

                    <? if ($stop->deliveries): ?>
                        <b>Deliveries:&nbsp;<?= count($stop->deliveries) ?></b><br>
                    <? endif; ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
        </table>

        <? if ($driverNotes): ?>
        <h3 class="manifest-title"><?php echo __("Driver Notes", "Driver Notes"); ?></h3>
        <table id="driver-notes" class="manifest-table driver-notes">
        <thead>
            <tr>
                <th><?php echo __("Address", "Address"); ?></th>
                <th><?php echo __("Note", "Note"); ?></th>
            </tr>
        </thead>
        <tbody>
        <? foreach ($driverNotes as $driverNote): ?>
            <tr>
                <td><?= $driverNote->address ?></td>
            	<td><?= $driverNote->note ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
        </table>
        <? endif; ?>

    </div>

    <div style="float:left; max-width: 50%; margin-left: 5px;">
    <? foreach($ordersByType as $orderType => $orders): ?>
        <h3 class="manifest-title manifest-order-type"><?= $orderType ?></h3>
        <table id="wash-fold" class="manifest-table order-type-table">
        <thead>
            <tr>
                <th><?php echo __("Address", "Address"); ?></th>
                <th><?php echo __("Customer", "Customer"); ?></th>
                <th><?php echo __("Bag", "Bag"); ?></th>
                <th><?php echo __("Qty", "Qty"); ?></th>
                <th><?php echo __("R", "R"); ?></th>
                <th><?php echo __("monetary_symbol", "$"); ?></th>
                <th><?php echo __("L", "L"); ?></th>
                <th><?php echo __("Note", "Note"); ?></th>
                <th><?php echo __("P_U", "P/U"); ?></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($orders as $order): ?>
            <tr>
                <td><?= $order->address1 ?> <?= $order->address2 ?></td>
                <td><a href="/admin/customers/detail/<?= $order->customer_id ?>"><?= getPersonName($order) ?></a></td>
                <td><a href="/admin/orders/details/<?= $order->orderID ?>"><?= $order->bagNumber ?></a></td>
                <td><?= $order->items ?></td>
                <td><?= $order->route ?></td>
                <td><?= $order->payment == 1 ? '<strong>'.__("monetary_symbol", "$").'</strong>' : '' ?></td>
                <td><?= $order->loaded == 0 ? '<strong>'.__("Not Loaded", "Not Loaded").'</strong>' : 'Y' ?></td>
                <td><?= $order->llNotes ?></td>
                <td><?= convert_from_gmt_aprax($order->dateCreated, "n/d") ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
        </table>
    <? endforeach; ?>
    </div>

    <br style="clear: both;">
</div>


<? //pr($stops); ?>

</body>
</html>
