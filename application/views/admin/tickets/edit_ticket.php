<?php enter_translation_domain("admin/tickets/edit_ticket"); ?>
<style>
    #ticketContent {
        font-size: 14px !important;
        font-weight: bold;
        margin-top:10px;
        padding-bottom:10px;
        width: 1200px;
        overflow: hidden;
    }

    #content-left {
        float: left;
        width:410px;
        height: 900px;
    }
    #content-right {

        min-width:350px;
        overflow: hidden;
    }


    /*	#header {
                    height:20px;
                    width: 200px;
            }
    */
    #panel-left-header{
        width: 280px;
        font-size:large;
        font-weight: bold;
        height:20px;
        padding:10px;
    }

    .panel-left-row{
        display:inline-block;
        vertical-align: top;
        height: 23px;
        padding: 5px;
        width: 290px;
        text-align: right;
    }

    .panel-right-column{
        display:inline-block;
        vertical-align: top;
        height: 23px;
        padding: 5px;
        text-align: left;
    }



    #bottom {
        clear:both;
        text-align:right;
    }

    #order-history{
        padding: 10px;
        border-top: 2px solid #bbb;
        width: 280px;
    }


    /* css for timepicker */
    .ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
    .ui-timepicker-div dl { text-align: left;  }
    .ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px;  }
    .ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
    .ui-timepicker-div td { font-size: 90%; }
    .ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

    .ui-timepicker-rtl{ direction: rtl; }
    .ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
    .ui-timepicker-rtl dl dt{ float: right; clear: right; }
    .ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }

    .ui-datepicker {
        background: #ee8e0f;
        border: 1px solid #555;
        color: white;
    }
</style>	
<div id="header">
    <h2><?php echo __("Edit Ticket", "Edit Ticket"); ?></h2>
    <p><a href="/admin/tickets/"> <img src="/images/icons/arrow_left.png"><?php echo __("Return to Tickets", "Return to Tickets"); ?> </a></p>
</div>
<form id="theForm" name="theForm" method="post" action="/admin/tickets/update_ticket">
    <input type="hidden" name="ticket_id" id="ticket_id" value="<?= $ticket->ticketID ?>">
    <div id="ticketContent">
        <div id="content-left">

            <div id="tabs" style="width: 400px; height: 800px">
                <ul>
                    <li><a href="#tabs-1"><?php echo __("Ticket", "Ticket:"); ?> <?= $ticket->ticketID ?></a></li>
                    <li><a href="#tabs-2">
                            <? if($ticket->customerName){ ?>
                            <div style="font-weight:200" id="customerName"><?php echo __("From", "From:"); ?> <?= substr($ticket->customerName, 0, 18) ?></div>
                            <? } else { ?>
                            <div style="font-weight:200" id="customerName"><?php echo __("Employee Ticket", "Employee Ticket"); ?></div>
                            <? } ?>
                        </a></li>
                </ul>
                <div id="tabs-1">
                    <form method="post" id="theForm" action="/admin/tickets/create_ticket">
                        <table class='detail_table' style="width:350px">
                            <th style="width: 100px;"><?php echo __("Ticket", "Ticket"); ?></th>
                            <td><input type="text" id="title" name="title" value="<?= $ticket->title ?>">
                            </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Status", "Status"); ?></th>
                                <td>
                                    <select id="ticketstatus_id" name="ticketstatus_id" onchange="updateResponseTime();">
                                        <?php foreach($ticketStatus as $status): ?>
                                            <option value="<?= $status->ticketStatusID ?>"<? if($status->ticketStatusID == $ticket->ticketstatus_id){?> SELECTED<? } ?> ><?= $status->description ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Type", "Type"); ?></th>
                                <td>
                                    <select class="styled-select" id="ticketType_id" name="ticketType_id" onchange="updateEmployeeList();">
                                    <option><?php echo __("Select a ticket type", "Select a ticket type..."); ?></option>
                                    <?php foreach($ticketTypes as $type): ?>
                                        <?php $selected = $type->ticketTypeID == $ticket->ticketType_id ? 'SELECTED':''; ?>
                                        <option value="<?= $type->ticketTypeID ?>" <?=$selected;?>><?php echo __($type->description, $type->description); ?></option>
                                    <?php endforeach;  ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Priority", "Priority"); ?></th>
                                <td>
                                    <select id="priority" name="priority">
                                        <option value="Low" <? if('Low' == $ticket->priority){?> SELECTED<? } ?>><?php echo __("Low", "Low"); ?></option>
                                        <option value="Medium" <? if('Medium' == $ticket->priority){?> SELECTED<? } ?>><?php echo __("Medium", "Medium"); ?></option>
                                        <option value="High" <? if('High' == $ticket->priority){?> SELECTED<? } ?>><?php echo __("High", "High"); ?></option>
                                    </select>  	                                   </select>
                                </td>

                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Expected", "Expected"); ?></th>
                                <td><input id="nextActionDate"  name="nextActionDate" value="<?= $nextActionDate ?>"/></td>

                            </tr>
                            <tr>

                                <?  $this->load->model('ticketstatus_model');
                                $newStatusId = $this->ticketstatus_model->getNewStatusId($this->business_id); ?>
                                <th style="width: 100px;"><?php echo __("Assigned", "Assigned"); ?></th>
                                <td><select class="styled-select2" id="employee_id" name="employee_id">
                                        <option value="0"><?php echo __("Unassigned", "Unassigned"); ?></option>
                                        <?foreach($employees as $employee){?>
                                        <option value="<?= $employee->employeeID ?>" <? if ( ($employee->employeeID == $ticket->employee_id) || get_employee_id($this->buser_id) == $employee->employeeID && $ticket->ticketstatus_id == $newStatusId) { ?>SELECTED<? } ?>><?= getPersonName($employee) ?></option>
                                        <? } ?>

                                </td>
                            </tr>

                            <tr>
                                <th style="width: 100px;"><?php echo __("Order ID", "Order ID"); ?></th>
                                <td><input type='text' name='order_id' id='order_id' value='<?= $ticket->order_id ?>'/>
                                    <div id="orderHistory">
                                        <?if($ticket->order_id !=0){?>
                                        <a target="_blank" href="/admin/orders/details/<?= $ticket->order_id ?>"><?php echo __("Order History", "Order History:"); ?>  <?= $ticket->order_id ?></a></br>
                                        <? } ?>
                                    </div>
                                    <input type="button" value="Get Order" id="order_button" onclick="searchOrderId();">
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Location", "Location"); ?></th>
                                <td>
                                    <select class="styled-select2" id="location_id" name="location_id" style="width:250px" onchange="updateLockerList();">
                                        <option><?php echo __("Select Location", "Select Location"); ?></option>
                                        <?foreach($locations as $locationid=>$location){?>
                                        <option value="<?= $locationid ?>" <? if ($locationid == $ticket->locationID) { ?>SELECTED<? } ?>><?= $location ?></option>
                                        <? } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Locker_Unit", "Locker/Unit #"); ?></th>
                                <td>
                                    <select class="styled-select2" id="locker_id" name="locker_id" style="width:250px">
                                        <option><?php echo __("Select Locker", "Select Locker"); ?></option>
                                        <?foreach($lockers as $lockerid=>$locker){?>
                                        <option value="<?= $lockerid ?>" <? if ($lockerid == $ticket->locker_id) { ?>SELECTED<? } ?>><?= $locker ?></option>
                                        <? } ?>
                                </select>  		
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Bag", "Bag"); ?></th>
                                <td>
                                    <input type="text" id="bag_number" value="<?= $ticket->bagNumber ?>"/>&nbsp;&nbsp;<span id="bag_number_check">-</span>
                                    <input type="button" value="Check Bag Number" id="bag_button" onclick="searchBagNumber();">
                                    <input type="hidden" id="bag_id" name="bag_id"/>
                                </td>
                            </tr>
                            <tr>
                                <th style="width: 100px;"><?php echo __("Label", "Label"); ?></th>
                                <td>
                                    <input type="hidden" name="labelArray" id="labelArray" value='<?= json_encode($selectedLabels) ?>'>

                                    <? foreach($availableLabels as $label){ ?>
                                    <? if(in_array($label->ticketLabelID, $selectedLabels)){ ?>
                                    <? $class = "button blue" ?>
                                    <? } else { ?>
                                    <? $class = "button" ?>
                                    <? } ?>

                                    <input type="button" value="<?= $label->description ?>" class="<?= $class ?>" id="label<?= $label->ticketLabelID ?>" onclick="toggleTag(<?= $label->ticketLabelID ?>)">
                                    <? } ?>
                                </td>
                            </tr>
                        </table>
                        <input type="button" value="<?php echo __("Update Ticket", "Update Ticket"); ?>" class="button blue" onclick="assignAction('');">
                        </div>
                        <div id="tabs-2">
                            <table class='detail_table' >
                                <tr>
                                    <th style="width: 100px;"><?php echo __("Customer Name", "Customer Name"); ?></th>
                                    <td>
                                        <input type="text" id="customerName" name="customerName" value='<?= $ticket->customerName ?>'/>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 100px;"><?php echo __("Customer Email", "Customer Email"); ?></th>
                                    <td>
                                        <input type="text" id="customerEmail" name="customerEmail" value='<?= $ticket->customerEmail ?>'/>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width: 100px;"><?php echo __("Customer Phone", "Customer Phone"); ?></th>
                                    <td>
                                        <input type="text" id="customerPhone" name="customerPhone" value='<?= $ticket->customerPhone ?>'/>
                                    </td>
                                <input type="hidden" id="customer_id" name="customer_id" value="<?=$ticket->customer_id;?>" />

                                    <?php if ($ticket->customer_id != 0) { ?>
                                        <td colspan="2"><a href="/admin/customers/detail/<?= $ticket->customer_id ?>" target="_blank"><?php echo __("Customer Info", "Customer Info"); ?></a>
                                    <? } ?>
                                </tr>
                                </table>
                                <input type="button" value="<?php echo __("Update Ticket", "Update Ticket"); ?>" class="button blue" onclick="assignAction('');">
                            </div>
                    </div>
                </div>

                <div id="content-right">
                    <div id="tabs1" style="width: 700px; height: 800px">
                        <ul>
                            <li><a href="#tabs-3"><?php echo __("Email", "Email"); ?></a></li>
                            <li><a href="#tabs-4"><?php echo __("Internal Notes", "Internal Notes"); ?></a></li>
                        </ul>
                        <div id="tabs-3">
                            <table class='detail_table' >
                                <tr>
                                    <td colspan="2">

                                        <table style="width: 600px !important;">
                                            <tr><td><div style="font-weight: bold"><?= $ticket->title ?></div></BR>
                                                    <? if($ticket->customerName){ ?>
                                                    <div style="font-weight:100"><?php echo __("From", "From:"); ?> <?= $ticket->customerName ?>(<?= $ticket->customerEmail ?>)</div>
                                                    <? } else { ?>
                                                    <div style="font-weight:200"><?php echo __("Employee Ticket", "Employee Ticket"); ?></div>
                                                    <? } ?>
                                                </td><td align="right">
                                                    <div style="float:right; font-weight: 100; font-size: smaller;">
                                                        <?php echo __("Ticket Update", "Ticket Update:"); ?>  </BR><?= $dateUpdated ?><br />
                                                        (<?= $displayUpdate ?>)
                                                    </div>
                                                </td></tr></table>
                                    </td></tr>
                                <tr><td colspan="2">
                                        <div style="height:182px;width:600px;border:1px solid #ccc;font:16px/26px Georgia, Garamond, Serif;overflow:auto;">
                                            <pre style="font-size: 14px !important; font-family: Arial, Helvetica, sans-serif !important; ">
                                                <? foreach($emailNotes as $emailNote){ ?>
                                                 <div style="font-style: italic;"><?= $emailNote->localCreateTime ?></div><br />
                                                                                            <?= $emailNote->note ?>
                                                    <hr />
                                                <? } ?>
                                                <?= $dateCreated ?><br />
                                                <?= $ticket->issue ?>
                                            </pre>
                                        </div>
                                    </td></tr>
                                <tr><td colspan="2"><?php echo __("Reply", "Reply:"); ?>
                                        <select id='cannedResponse' name='cannedResponse' onchange="populateCannedResponse();" style="width: 300px;">
                                            <option><?php echo __("Use Canned Respons", "Use Canned Response"); ?></option>
                                        </select>
                                        <br /><br />
                                        <textarea id="emailBody" name="emailBody" cols="40" rows="20"></textarea>
                                    </td></tr>
                                <tr><td colspan="2">
                                        <input type="button" value="<?php echo __("Send Email", "Send Email"); ?>" class="button blue" onclick="assignAction('email');">
                                        <input type="button" value="<?php echo __("Send Email And Close", "Send Email & Close"); ?>" class="button blue" onclick="assignAction('email,close');" style="float:right;">
                                    </td></tr>

                            </table>
                        </div>
                        <div id="tabs-4">
                            <table class='detail_table' >
                                <tr>
                                    <td colspan="2">

                                        <table style="width: 600px !important;">
                                            <tr><td><div style="font-weight: bold"><?= $ticket->title ?></div></BR>
                                                    <? if($ticket->customerName){ ?>
                                                    <div style="font-weight:100"><?php echo __("From", "From:"); ?> <?= $ticket->customerName ?>(<?= $ticket->customerEmail ?>)</div>
                                                    <? } else { ?>
                                                    <div style="font-weight:200"><?php echo __("Employee Ticket", "Employee Ticket"); ?></div>
                                                    <? } ?>
                                                </td><td align="right">
                                                    <div style="float:right; font-weight: 100; font-size: smaller;"><?php echo __("Ticket Update", "Ticket Update:"); ?>  <?= $ticket->dateUpdated ?><br />
                                                        (<?= $displayUpdate ?>)
                                                    </div>
                                                </td></tr></table>
                                    </td></tr>
                                <tr><td colspan="2">
                                        <div style="height:182px;width:600px;border:1px solid #ccc;font:16px/26px Georgia, Garamond, Serif;overflow:auto;">
                                            <pre style="font-size: 14px !important; font-family: Arial, Helvetica, sans-serif !important; ">
                                            <? foreach($internalNotes as $internalNote){ ?>
                                                                                        <?= $internalNote->localCreateTime ?><br />
                                                                                        <?= $internalNote->note ?>
                                                <hr />
                                            <? } ?>
                                            </pre>
                                        </div>
                                    </td></tr>
                                <tr><td colspan="2"><?php echo __("Add Note", "Add Note"); ?><br /><br />
                                        <textarea id="internalNote" name="internalNote" cols="40" rows="20"></textarea>
                                    </td></tr>
                                <tr><td colspan="2">
                                        <input type="button" value="<?php echo __("Update And Add Driver Note", "Update & Add Driver Note"); ?>" class="button blue" onclick="assignAction('internalNote,driverNote');">
                                        <input type="button" value="<?php echo __("Updated", "Updated"); ?>" class="button blue" onclick="assignAction('internalNote');" style="float:right;">
                                    </td></tr>

                            </table>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="formAction" id="formAction">
                </form>



                <script>


                    $(function () {
                        $("#tabs").tabs();
                    });

                    $(function () {
                        $("#tabs1").tabs();
                    });

                    $("#nextActionDate").datetimepicker({
                        controlType: 'slider',
                        timeFormat: "h:mm tt"

                    });


                    <?
                        $this->load->model('ticketstatus_model');
                        $ticketStatusJson = json_encode($this->ticketstatus_model->get_aprax(array("business_id" => $this-> business_id)));
                    ?>
                            ///////////////////////////
                            var rHash = new Object();
                    var responseHash = new Object();

                    var responseData = <?= json_encode($cannedResponses) ?>;

                    var statusData = <?= $ticketStatusJson ?>;

                $.each(responseData, function (index) {
                    var shortDesc = responseData[index].shortTitle;

                    responseHash[shortDesc] = responseData[index].response;

                    $('#cannedResponse').append(new Option(shortDesc, shortDesc));
                })

                $.each(statusData, function (index) {
                    var ticketStatusID = statusData[index].ticketStatusID;

                    rHash[ticketStatusID] = statusData[index].responseTime;
                });

                $(function () {
                    searchBagNumber();
                });

                function populateCannedResponse() {
                    var selectCannedResponse = $('#cannedResponse').val();
                    var cannedMessage = responseHash[selectCannedResponse];

                    $("#emailBody").val(cannedMessage);
                }


                function assignAction(actionString) {
                    $('#formAction').val(actionString);
                    $('#theForm').submit();
                }


                function toggleTag(tagId) {
                    $('#label' + tagId).toggleClass("blue");

                    var activeText = "";
                    var activeArray = [];
                    activeText = $('#labelArray').val();

                    if (activeText) {
                        activeArray = JSON.parse(activeText);
                    }

                    toggleArrayItem(activeArray, tagId.toString());

                    var tempVal = JSON.stringify(activeArray);
                    $('#labelArray').val(tempVal);

                }

                function updateResponseTime() {
                    var selectResponse = $('#ticketstatus_id').val();
                    var hrAdd = rHash[selectResponse];

                    curDate = new Date();
                    var actionTime = new Date(curDate.getTime() + hrAdd * 60 * 60000);
                    var formatActionTime = formatDate(actionTime);

                    $("#nextActionDate").val(formatActionTime);
                    var responseMessage = "Assigned Action: " + selectResponse + ". Expected result by: " + formatActionTime;
                }

                function formatDate(dateIn) {
                    var yyyy = dateIn.getFullYear();
                    var mm = dateIn.getMonth() + 1;
                    var dd = dateIn.getDate();
                    var hr = dateIn.getHours();
                    var mi = dateIn.getMinutes();
                    var ss = dateIn.getSeconds();

                    return pad(mm) + "/" + pad(dd) + "/" + yyyy + " " + pad(hr) + ":" + pad(mi) + ":" + pad(ss);
                }

                function pad(num) {
                    num = num + '';
                    return num.length < 2 ? '0' + num : num;
                }


                function searchOrderId() {
                    var order_id = $('#order_id').val();

                    // execute ajax call
                    $.ajax({
                        url: "/admin/tickets/searchOrders",
                        type: "post",
                        data: {
                            "order_id": order_id
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.status == 'success') {
                                // before rendering data, delete the existing data

                                populateOrder(data);
                            } else {
                                alert('<?php echo __("No matching orderid found", "No matching orderid found."); ?>');
                            }
                        }
                    });


                }

                function populateOrder(data) {
                    var bag_number = data.bagNumber;
                    var locker_id = data.locker_id;
                    var location_id = data.location_id;
                    var customer_id = data.customer_id;
                    var location_id = data.location_id;
                    var firstName = data.firstName;
                    var lastName = data.lastName;
                    var email = data.email;
                    var phone = data.phone;
                    var orderID = data.orderID;

                    $("#location_id").val(location_id);updateLockerList(locker_id);
                    $('#bag_number').val(bag_number);searchBagNumber();
                    $('#locker_id').val(locker_id);
                    $('#firstName').val(firstName);
                    $('#lastName').val(lastName);
                    $('#phone').val(phone);
                    $('#email').val(email);

                    if (orderID == 0) {
                        $("#orderHistory").html("");
                    } else {
                        var linkStr = '<a target="_blank" href="/admin/orders/details/' + orderID + '"><?php echo __("Order History", "Order History:"); ?>  ' + orderID + '</a></br></div>';
                        $("#orderHistory").html(linkStr);
                    }
                }


                function renderCustomer(data) {
                    var firstName = data[0].firstName;
                    var lastName = data[0].lastName;
                    var email = data[0].email;
                    var phone = data[0].phone;

                    $('#firstName').val(firstName);
                    $('#lastName').val(lastName);
                    $('#email').val(email);
                    $('#phone').val(phone);

                }

                function toggleArrayItem(a, v) {
                    var i = a.indexOf(v);
                    if (i === -1)
                        a.push(v);
                    else
                        a.splice(i, 1);
                }

                function updateLockerList(selected_locker_id)
                {
                    $.post("/admin/locations/getLockersListForDropDown", {
                        location_id : $('#location_id').val()
                    }, function(response) {
                        $('#locker_id').empty();
                        $('#locker_id').append($("<option></option>").text("Select Locker"));
                        $.each(response, function(key, locker) {
                            $('#locker_id').append($("<option></option>").attr("value", key).text(locker));
                        });

                        if(selected_locker_id){
                            $('#locker_id').val(selected_locker_id);
                        }
                    }, "json");
                }

                function searchBagNumber()
                {
                    $('#bag_number_check').text("Checking...");
                    $.post("/admin/bags/getByNumber", {
                        bag_number : $('#bag_number').val()
                    }, function(response) {
                        $('#bag_id').attr("value", '');

                        var bagId = 0;        
                        if(response[0] && response[0].bagID){
                            bagId = response[0].bagID;
                        }

                        if(bagId){
                            $('#bag_number_check').text("<?php echo __("valid", "Valid ! :)"); ?> ");
                            $('#bag_id').attr("value", bagId);
                        }else{
                            $('#bag_number_check').text("<?php echo __("Non Valid", "Non Valid !!! :("); ?>");
                        }
                    }, "json");
                }
              
            </script>

