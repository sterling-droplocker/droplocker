<?php enter_translation_domain("admin/tickets/ticket_subscribers"); ?>
<h2><?php echo __("Ticket Subscribers", "Ticket Subscribers"); ?></h2>
<p><a href='/admin/tickets/'><img src="/images/icons/arrow_left.png"><?php echo __("Back to tickets", "Back to tickets"); ?> </a></p>

<?= $this->session->flashdata('message')?>


<form action='' method='post'>
	<table class='detail_table'>
		<tr>
			<th><?php echo __("From Email", "From Email"); ?></th>
			<td><?= SYSTEMEMAIL?></td>
		</tr>
		<tr>
			<th><?php echo __("From Name", "From Name"); ?></th>
			<td><?= SYSTEMFROMNAME?></td>
		</tr>
		<tr>
			<th><?php echo __("Subject", "Subject"); ?></th>
			<td><input type='text' name='subject' value='<?= $subject?>' /></td>
		</tr>
		<tr>
			<th style='vertical-align: top'><?php echo __("Emails", "Emails"); ?></th>
			<td>
				<span style='font-size:10px'><?php echo __("1 email per line", "1 email per line"); ?></span><br>
				<textarea style='width:99%;height:100px;' name='emails'>
<? 
foreach($subs as $sub){
echo $sub."\n";
} 
?>
				</textarea>
			</td>
		</tr>
	</table>
	<br>
	<input type='submit' value='<?php echo __("Submit", "Submit"); ?>' name='submit' class='button orange'/>
</form>
