<?php enter_translation_domain("admin/tickets/ticketType_form"); ?>
<?php
$title = empty($ticketTypeID) ? __("Add Ticket Type", "Add Ticket Type"):__("Edit Ticket Type", "Edit Ticket Type");
?>

<h2><?=$title;?></h2>

<p>
    <a href="/admin/tickets/settings"><img src="/images/icons/arrow_left.png"><?php echo __("Back to Tickets Settings", "Back to Tickets Settings"); ?> </a>
</p>

<?= $this->session->flashdata('message') ?>

<form method="post">
    <table class="detail_table">
        <tr>
            <th><?php echo __("Description", "Description"); ?></th>
            <td>
                <input type='text' name='description' id='description' value="<?= isset($ticketType->description) ? $ticketType->description : '' ?>"/>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Roles", "Roles"); ?></th>
            <td>
                <?php foreach ($roles as $id => $data): ?>
                <?php $checked = in_array($id, $checkedRoles) ? 'checked':''; ?>
                <label>
                    <input type="checkbox" name="roles[]" value="<?=$id;?>" <?=$checked;?>>
                    <?=$data;?>
                </label>
                <?php endforeach; ?>
            </td>
        </tr>
    </table>
    <input type='submit' id='submit_button' value='<?php echo __("Save", "Save"); ?>' class='button orange'/>
   
</form>
