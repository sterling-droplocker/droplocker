<?php enter_translation_domain("admin/tickets/status_form"); ?>
<?php
    $status_type = isset($status->ticketStatusType_id)  ? $status->ticketStatusType_id:'1';
?>

<? if (empty($label->ticketLabelID)): ?>
<h2><?php echo __("Add Status", "Add Status"); ?></h2>
<? else: ?>
<h2><?php echo __("Edit Status", "Edit Status"); ?></h2>
<? endif; ?>
<p>
    <a href="/admin/tickets/settings"><img src="/images/icons/arrow_left.png"><?php echo __("Back to Tickets Settings", "Back to Tickets Settings"); ?> </a>
</p>

<?= $this->session->flashdata('message') ?>

<form method="post" action="/admin/tickets/save_status">
    <table class="detail_table">
        <tr>
            <th><?php echo __("Description", "Description"); ?></th>
            <td>
                <input type='text' name='description' id='description' value="<?= isset($status->description) ? $status->description : '' ?>"/>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Response Time", "Response Time"); ?></th>
            <td>
                <input type='text' name='responseTime' id='responseTime' value="<?= isset($status->responseTime) ? $status->responseTime : '' ?>"/>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Status Type", "Status Type"); ?></th>
            <td>
                <?= form_dropdown('ticketStatusType_id', $status_types, $status_type, 'id="ticketStatusType_id" tabindex="10000"') ?>
            </td>
        </tr>
    </table>

    <? if (empty($status->ticketStatusID)): ?>
    <input type='submit' id='submit_button' value='<?php echo __("Add Status", "Add Status"); ?>' class='button orange'/>
    <? else: ?>
    <input type="hidden" name="ticketStatusID" value="<?= $status->ticketStatusID ?>">
    <input type='submit' id='submit_button' value='<?php echo __("Update Status", "Update Status"); ?>' class='button orange'/>
    <? endif; ?>
</form>
