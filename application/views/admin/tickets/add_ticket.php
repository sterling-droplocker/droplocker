<?php enter_translation_domain("admin/tickets/add_ticket"); ?>
<?php
    $ticketStatusJson = json_encode($ticketStatus);
    $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
?>

<style>
#ticketContent {
    font-size: 14px !important;
    font-weight: bold;
    margin-top:10px;
    padding-bottom:10px;
    overflow: hidden;
}
	
#content-left {
    float: left;
    width:300px;
    height: 900px;
}
#content-right {
    margin-left:10px;
    min-width:350px;
    overflow: hidden;
}

#panel-left-header{
    width: 280px;
    font-size:large;
    font-weight: bold;
    height:20px;
    padding:10px;
}

.panel-left-row{
    display:inline-block;
    vertical-align: top;
    height: 23px;
    padding: 5px;
    width: 290px;
    text-align: right;
}	

.panel-right-column{
    display:inline-block;
    vertical-align: top;
    height: 23px;
    padding: 5px;
    text-align: left;
}	

#bottom {
    clear:both;
    text-align:right;
}

#order-history{
    padding: 10px;
    border-top: 2px solid #bbb;
    width: 280px;
}	

/* css for timepicker */
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left;  }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px;  }
.ui-timepicker-div dl dd { margin: 0 10px 10px 45%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 45% 10px 10px; }
	
.ui-datepicker {
    background: #ee8e0f;
    border: 1px solid #555;
    color: white;
}	
</style>	

<div id="header">
    <h2><?php echo __("Add Ticket", "Add Ticket"); ?></h2>
    <p><a href="/admin/tickets/"> <img src="/images/icons/arrow_left.png"><?php echo __("Return to Tickets", "Return to Tickets"); ?> </a></p>
</div>
<div id="ticketContent">
<div id="content-left">
    <form method="post" id="theForm" action="/admin/tickets/create_ticket">
	<table class='detail_table'>
    	<tr>
        <th style="width: 100px;"><?php echo __("Order ID", "Order ID"); ?></th>
		<td><input type='text' name='order_id' id='order_id' value=''/>
    		<div id="orderHistory">
            </div>   
    		<input type="button" value="Get Order" id="order_button" onclick="searchOrderId();">
		</td>
    	</tr>
    	<tr>
        <th style="width: 100px;"><?php echo __("Customer", "Customer"); ?></th>
		<td><input type='text' name='customer' id='customer' value='' /><div id='customer_results'></div>
		    <div id="customerInfo"></div>
			<input type="hidden" name="customer_id" id="customer_id" value="" />
		</td>
    	</tr>
    	<tr>
        <th style="width: 100px;"><?php echo __("Email", "Email"); ?></th>
		<td><input type='text' name='email' id='email' value=''/>
		</td>
    	</tr>
    	<tr>
        <th style="width: 100px;"><?php echo __("Phone", "Phone"); ?></th>
		<td><input type='text' name='phone' id='phone' value=''/>
		</td>
    	</tr>
    	
    	<tr><td  colspan="2" height="555px" valign="top">&nbsp;</td></tr>
	</table>	
</div>	
	    
<div id="content-right">
        <table class='detail_table'>
    	    <tr>
                <th style="width: 100px;">* <?php echo __("Subject", "Subject"); ?></th>
                <td><input type='text' name='subject' id='subject' value=''/></td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Status", "Status"); ?></th>
                <td>  
                    <select class="styled-select" id="ticketstatusid" name="ticketstatusid" onchange="updateResponseTime();">
                    <?php foreach($ticketStatus as $status):?>
                	<option value="<?= $status->ticketStatusID ?>"><?php echo __($status->description, $status->description); ?></option>
                    <?php endforeach; ?>
                    </select>
		</td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Type", "Type"); ?></th>
                <td>
                    <select class="styled-select" id="ticketType_id" name="ticketType_id" onchange="updateEmployeeList();">
                    <option><?php echo __("Select a ticket type...", "Select a ticket type..."); ?></option>
                    <?php foreach($ticketTypes as $type): ?>
                	<option value="<?= $type->ticketTypeID ?>"><?= $type->description ?></option>
                    <?php endforeach;  ?>
                    </select>
		</td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Priority", "Priority"); ?></th>
                <td>
                    <select id="priority" name="priority">
                    <option value="Low"><?php echo __("Low", "Low"); ?></option>
                    <option value="Medium"><?php echo __("Medium", "Medium"); ?></option>
                    <option value="High"><?php echo __("High", "High"); ?></option>
                    </select>  		
                </td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Expected", "Expected"); ?></th>
                <td><input id="nextActionDate"  name="nextActionDate" value="<?= convert_from_gmt_aprax(date('m/d/Y H:i'), $dateFormat, $this->business_id);?>"/></td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Assigned", "Assigned"); ?></th>
                <td>
                    <select class="styled-select2" id="employee_id" name="employee_id">
                        <option><?php echo __("Select Employee", "Select Employee"); ?></option>
                        <?php foreach($employees as $employee): ?>
                            <?php $selected = (get_employee_id($this->buser_id) == $employee->employeeID) ? 'SELECTED':''; ?>
                            <option value="<?= $employee->employeeID ?>" <?=$selected;?>>
                            <?= getPersonName($employee) ?>
                            </option>
                        <?php endforeach;?>
                    </select>   
		</td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Location", "Location"); ?></th>
                <td>
                    <select class="styled-select2" id="location_id" name="location_id" style="width:370px" onchange="updateLockerList();">
                    <option><?php echo __("Select Location", "Select Location"); ?></option>
                    <?php foreach($locations as $locationid=>$location):?>
                    <option value="<?= $locationid ?>"><?= $location ?></option>  
                    <?php endforeach; ?>
                </select>  		
                </td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Locker Unit", "Locker/Unit"); ?> #</th>
                <td>
                    <select class="styled-select2" id="locker_id" name="locker_id" style="width:370px">
                    <option><?php echo __("Select Locker", "Select Locker"); ?></option>
                </select>  		
                </td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Bag", "Bag"); ?></th>
                <td>
                    <input type="text" id="bag_number"/>&nbsp;&nbsp;<span id="bag_number_check">Unchecked</span>
                    <input type="button" value="<?php echo __("Check Bag Number", "Check Bag Number"); ?>" id="bag_button" onclick="searchBagNumber();">
                    <input type="hidden" id="bag_id" name="bag_id"/>
                </td>
    	    </tr>
            <tr>
                <th style="width: 100px;"><?php echo __("Label", "Label"); ?></th>
                <input type="hidden" name="labelArray" id="labelArray">
                <td><?php foreach($availableLabels as $label): ?>
                        <input type="button" value="<?= $label->description ?>" class="button" id="label<?= $label->ticketLabelID ?>" onclick="toggleTag(<?= $label->ticketLabelID ?>)">
                    <?php endforeach; ?>
                </td>
    	    </tr>
            <tr>
                <td colspan="2"><?php echo __("Issue Description", "Issue Description"); ?></td>
            </tr>
            <tr>
                <td colspan="2"><textarea rows="15" cols="59" name="description" id="description"></textarea></td>
            </tr>
            <tr>
                <td colspan="2">
                <input type="button" value="<?php echo __("Create Ticket", "Create Ticket"); ?>" class="button blue" onclick="assignAction('Create');checkForm();">
                <input type="button" value="<?php echo __("Create Ticket And Acknowledge Customer", "Create Ticket & Acknowledge Customer"); ?>" class="button blue" onclick="assignAction('Create,Acknowledge');checkForm();" style="float:right;">	
                </td>
            </tr>    
	</table>		
        <input type="hidden" name="formAction" id="formAction">
    </form>  
</div>

<script>    
///////////////////////////
var rHash = new Object();
var statusData = <?= $ticketStatusJson ?>;

$.each(statusData, function(index) {
    var ticketStatusID = statusData[index].ticketStatusID;
    rHash[ticketStatusID] = statusData[index].responseTime;
});

function assignAction(actionString){
    $('#formAction').val(actionString);
}   

$("#nextActionDate").datetimepicker({controlType: 'slider', timeFormat: "h:mm tt" });

function updateResponseTime(){
    var selectResponse = $('#ticketstatusid').val();
    var hrAdd = rHash[selectResponse];

    curDate = new Date();
    var actionTime = new Date(curDate.getTime() + hrAdd * 60 * 60000);
    var formatActionTime = formatDate(actionTime);

    $("#nextActionDate").val(formatActionTime);
}	

function formatDate(dateIn){
    var yyyy = dateIn.getFullYear();
    var mm = dateIn.getMonth() + 1;
    var dd = dateIn.getDate();
    var hr = dateIn.getHours();
    var mi = dateIn.getMinutes();
    return pad(mm)+"/"+pad(dd)+"/"+yyyy+" "+pad(hr)+":"+pad(mi);
}

function pad(num) {
    num = num + '';
    return num.length < 2 ? '0' + num : num;
}

function toggleTag(tagId){
    $('#label'+tagId).toggleClass("blue");
    var activeText = "";
    var activeArray=[];
    activeText = $('#labelArray').val();
    
    if(activeText){
        activeArray = JSON.parse(activeText);
    }
       
    toggleArrayItem(activeArray, tagId.toString() ); 
    var tempVal = JSON.stringify(activeArray);    
    $('#labelArray').val(tempVal);
           
}    

function searchOrderId(){
    var order_id = $('#order_id').val();
    // execute ajax call
    $.ajax({
        url: "/admin/tickets/searchOrders",
        type:"post",
        data:{
            "order_id": order_id
        },
        dataType: "json",
            success: function(data){
            if(data.status=='success'){
                // before rendering data, delete the existing data
                populateOrder( data );
            } else{
                alert('<?php echo __("No matching orderid found", "No matching orderid found."); ?>');
            }
        }
    });	
}  

function checkForm(){
    var subject = $('#subject').val();
    if(subject == ""){
        alert('<?php echo __("Your ticket must have a subject", "Your ticket must have a subject"); ?>');
        exit;
    }
    $('#theForm').submit();
}    

function populateOrder(data){
    var bag_number  = data.bagNumber;
    var locker_id   = data.locker_id;
    var location_id = data.location_id;
    var customer_id = data.customer_id;
    var location_id = data.location_id;
    var firstName   = data.firstName;
    var lastName    = data.lastName;
    var completeName  = data.completeName;
    var email       = data.email;
    var phone       = data.phone;
    var orderID     = data.orderID;
    
    $("#location_id").val(location_id);updateLockerList(locker_id);
    $('#bag_number').val(bag_number);searchBagNumber();
    $('#customer').val(completeName);
    $('#phone').val(phone);
    $('#email').val(email);
    
    if(orderID==0){
        $("#orderHistory").html("");
    } else {
        var linkStr = '<a target="_blank" href="/admin/orders/details/'+orderID+'"><?php echo __("Order History", "Order History:"); ?>  '+orderID+'</a></br></div>';
        $("#orderHistory").html(linkStr);
        var linkStr2 = '<a target="_blank" href="/admin/customers/detail/'+customer_id+'"><?php echo __("Customer Info", "Customer Info"); ?></a></br></div>';
 
        $('#customerInfo').html(linkStr2);
    }    

}    

function renderCustomer(data){
    var customer_id = data[0].customerID;
    var firstName = data[0].firstName;
    var lastName = data[0].lastName;
    var email = data[0].email;
    var phone = data[0].phone;

    $('#customer_id').val(customer_id);
    $('#firstName').val(firstName);
    $('#lastName').val(lastName);
    $('#email').val(email);
    $('#phone').val(phone);
} 

function toggleArrayItem(a, v) {
    var i = a.indexOf(v);
    if (i === -1)
        a.push(v);
    else
        a.splice(i,1);
}

function updateEmployeeList()
{
    $.post("/admin/tickets/type_filter", {
        type_filter : $('#ticketType_id').val()
    }, function(response) {
        $('#employee_id').empty();
        $.each(response, function(i, employee) {
            $('#employee_id').append($("<option></option>").attr("value", employee.employeeID).text(employee.completeName));
        });
    }, "json");
}

function updateLockerList(selected_locker_id)
{
    $.post("/admin/locations/getLockersListForDropDown", {
        location_id : $('#location_id').val()
    }, function(response) {
        $('#locker_id').empty();
        $('#locker_id').append($("<option></option>").text("Select Locker"));
        $.each(response, function(key, locker) {
            $('#locker_id').append($("<option></option>").attr("value", key).text(locker));
        });
        
        if(selected_locker_id){
            $('#locker_id').val(selected_locker_id);
        }
    }, "json");
}

function searchBagNumber()
{
    $('#bag_number_check').text("<?php echo __("Checking", "Checking..."); ?>");
    $.post("/admin/bags/getByNumber", {
        bag_number : $('#bag_number').val()
    }, function(response) {
        $('#bag_id').attr("value", '');
        
        var bagId = 0;        
        if(response[0] && response[0].bagID){
            bagId = response[0].bagID;
        }
        
        if(bagId){
            $('#bag_number_check').text("<?php echo __("Valid", "Valid !"); ?> :)");
            $('#bag_id').attr("value", bagId);
        }else{
            $('#bag_number_check').text("<?php echo __("Non Valid", "Non Valid !!!"); ?> :(");
        }
    }, "json");
}
              
</script>

