<?php enter_translation_domain("admin/tickets/label_form"); ?>
<?php
    $colors = array('lightgreen' => 'Light Green', 'red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange');
    $color = empty($label->ticketLabelID) ? 'lightgreen':$label->color;

?>
<? if (empty($label->ticketLabelID)): ?>
    <h2><?php echo __("Add Label", "Add Label"); ?></h2>
<? else: ?>
    <h2><?php echo __("Edit Label", "Edit Label"); ?></h2>
<? endif; ?>
<p>
    <a href="/admin/tickets/settings"><img src="/images/icons/arrow_left.png"><?php echo __("Back to Tickets Settings", "Back to Tickets Settings"); ?> </a>
</p>

<?= $this->session->flashdata('message')?>

<form method="post" action="/admin/tickets/save_label">
    <table class="detail_table">
        <tr>
            <th><?php echo __("Description", "Description"); ?></th>
            <td>
                <input type='text' name='description' id='description' value="<?= isset($label->description)?$label->description:''?>"/>
            </td>
        </tr>
        <tr>
            <th><?php echo __("Color", "Color"); ?></th>
            <td>
                <?= form_dropdown('color', $colors, $color, 'id="color" tabindex="10000"') ?>
                <!--<select id='product_id' name='product_id' tabindex="9999"></select>-->
            </td>
        </tr>
    </table>

    <? if (empty($label->ticketLabelID)): ?>
        <input type='submit' id='submit_button' value='<?php echo __("Add Label", "Add Label"); ?>' class='button orange'/>
    <? else: ?>
        <input type="hidden" name="ticketLabelID" value="<?= $label->ticketLabelID ?>">
        <input type='submit' id='submit_button' value='<?php echo __("Update Label", "Update Label"); ?>' class='button orange'/>
    <? endif; ?>
</form>
