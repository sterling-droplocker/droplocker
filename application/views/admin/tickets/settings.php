<?php enter_translation_domain("admin/tickets/settings"); ?>
<?= $this->session->flashdata('message') ?>
<h2><?php echo __("Tickets Settings", "Tickets Settings"); ?></h2>
<p>
    <a href="/admin/tickets"><img src="/images/icons/arrow_left.png"><?php echo __("Back to Tickets", "Back to Tickets"); ?> </a>
</p>
<h3 style="margin: 20px 0 10px 0"><?php echo __("Labels", "Labels"); ?></h3>
<p>
    <a href="/admin/tickets/add_label"><img src="/images/icons/add.png"><?php echo __("Add New Label", "Add New Label"); ?> </a>
</p>

<table border=0 id="box-table-a" class="hovertable" >
    <thead>
        <tr class="headerRow" id="headerRow">
            
            <th><?php echo __("Description", "Description"); ?></th>
            <th><?php echo __("Color", "Color"); ?></th>
            <th><?php echo __("Edit", "Edit"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($labels as $l): ?>
            <tr>
                <td><?=$l->description;?></td>
                <td><?=$l->color;?></td>
                <td><a href="/admin/tickets/edit_label/<?=$l->ticketLabelID;?>"><img src="/images/icons/application_edit.png"></a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<h3 style="margin: 40px 0 10px 0">Status</h3>
<p>
    <a href="/admin/tickets/add_status"><img src="/images/icons/add.png"><?php echo __("Add New Status", "Add New Status"); ?> </a>
</p>

<table border=0 id="box-table-a" class="hovertable" >
    <thead>
        <tr class="headerRow" id="headerRow">

            <th><?php echo __("Description", "Description"); ?></th>
            <th><?php echo __("Response Time", "Response Time"); ?></th>
            <th><?php echo __("Status Type", "Status Type"); ?></th>
            <th><?php echo __("Edit", "Edit"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($statuses as $s): ?>
            <tr>
                <td><?=$s->description;?></td>
                <td><?=$s->responseTime;?></td>             
                <td><?=$s->ticketStatusTypeDescription;?></td>
                <td><a href="/admin/tickets/edit_status/<?=$s->ticketStatusID;?>"><img src="/images/icons/application_edit.png"></a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<h3 style="margin: 40px 0 10px 0"><?php echo __("Ticket Types", "Ticket Types"); ?></h3>
<p>
    <a href="/admin/tickets/add_ticket_type"><img src="/images/icons/add.png"><?php echo __("Add New Type", "Add New Type"); ?> </a>
</p>

<table border=0 id="box-table-a" class="hovertable" >
    <thead>
        <tr class="headerRow" id="headerRow">
            <th><?php echo __("Description", "Description"); ?></th>
            <th><?php echo __("Roles", "Roles"); ?></th>
            <th><?php echo __("Edit", "Edit"); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($types as $t): ?>
            <?php 
                $typeRoles = explode(',', $t->roles);
                $checkedRoles = array();
                foreach ($typeRoles as $r) {
                    $checkedRoles[] = $roles[$r];
                }
            ?>
            <tr>
                <td><?=$t->description;?></td>
                <td><?=implode(', ', $checkedRoles); ?></td>
                <td><a href="/admin/tickets/add_ticket_type/<?=$t->ticketTypeID;?>"><img src="/images/icons/application_edit.png"></a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>