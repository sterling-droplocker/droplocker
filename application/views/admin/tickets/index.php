<?php enter_translation_domain("admin/tickets/index"); ?>
<?= $this->session->flashdata('message') ?>

<h2><?php echo __("Tickets", "Tickets"); ?> <a href="/admin/tickets/settings"><img src="/images/icons/cog.png" width="16" height="16" alt=""></a></h2>

<div class="topbuttons" style="margin: 20px 0;">

    <input type="button" value="Create Ticket" class="button green" onclick="window.location.href = '/admin/tickets/add_ticket';">
    <input type="button" value="Reload Page" class="button blue" onclick="location.reload();">

    <div style="float:right;">
        <input type="text" size="20" max="20" placeholder="Enter order number" id="order_id" name="order_id" > <input type="button" value="Search" class="button blue" onclick="searchTickets();" >
    </div>
</div>
<table border=0 id="box-table-a" class="hovertable" >
    <thead>
        <tr class="headerRow" id="headerRow">
            <th><?php echo __("Source", "Source"); ?></th>
            <th><?php echo __("Type", "Type"); ?></th>
            <th><?php echo __("Customer", "Customer"); ?></th>
            <th><?php echo __("Subject", "Subject"); ?></th>

            <th><img width="25" src='/images/admin/speech.png' /></th>
            <th>
                <div class="styled-select slate">
                    <select class="styled-select" id="filterStatus" onselect="filterTickets();">
                        <option value=""><?php echo __("Any Status", "Any Status"); ?></option>
                        <?php foreach($ticketStatus as $status): ?>
                            <option value="<?= $status->ticketStatusID ?>"><?php echo __($status->description, $status->description); ?></option>
                        <?php endforeach;  ?>
                    </select>
                </div>
            </th>
            <th width="90px"><?php echo __("Updated", "Updated"); ?></th>
            <th>
                <div  class="styled-select2 slate">
                    <select class="styled-select" id="filterEmployee" onselect="filterTickets();"><option><?php echo __("All Employees", "All Employees"); ?></option>
                        <option value="Unassigned"><?php echo __("Unassigned", "Unassigned"); ?></option>
                        <? foreach($employees as $employee): ?>
                        <option value="<?= $employee->employeeID ?>"><?= getPersonName($employee) ?></option>
                        <? endforeach; ?>
                    </select>
                </div>
            </th>
            <th><?php echo __("Priority", "Priority"); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr></tr>
    </tbody>
</table> 

<script>
    //dataTable object
    var dataTable = false;

    // bind data reload to employee dropdown
    $("#filterEmployee").change(function () {
        dataTable.fnClearTable();
        dataTable.fnDestroy();
        filterTickets();
    });

    // bind data reload to status dropdown
    $("#filterStatus").change(function () {
        dataTable.fnClearTable();
        dataTable.fnDestroy();
        filterTickets();
    });

// function for executing AJAX call to search ticket data	
    function searchTickets() {

        var order_id = $("#order_id").val();

        // execute call
        $.ajax({
            url: "/admin/tickets/get_filter_tickets",
            type: "post",
            data: {
                "order_id": order_id
            },
            dataType: "json",
            success: function (data) {

                if (data.status == 'success') {
                    // before we can begin rending the data
                    // we need to clear the existing data
                    $("#box-table-a > tbody").html("");
                    drawTable(eval(data.data));
                }
                else {
                    alert(data.error);
                }
            }
        });

    }

// function for executing AJAX call to retrieve ticket data	
    function filterTickets() {

        var employee = $("#filterEmployee").val();
        var status = $("#filterStatus").val();

        // execute call
        $.ajax({
            url: "/admin/tickets/get_filter_tickets",
            type: "post",
            data: {
                "filterEmployee": employee,
                "filterStatus": status,
            },
            dataType: "json",
            success: function (data) {
                if (data.status == 'success') {
                    // before we can begin rending the data
                    // we need to clear the existing data
                    $("#box-table-a > tbody").html("");
                    drawTable(eval(data.data));
                }
            }
        });

    }

// function for render table data
    function drawTable(data) {
        var qrySize = data.length;

        // if there is data, loopover it/render it
        if (qrySize > 0) {
            // Loop over the data
            for (var i = 0; i < qrySize; i++) {
                // Render a single row
                drawRow(data[i]);

            }
        }

        createDataTable();
    }


// function for rending a single row of data
    function drawRow(rowData) {

        // if the nextActionDate has been execeeded
        // flag the row in pink and red for the mouseover
        if (rowData.overDue == 1) {
            var rowBgOver = 'red';
            var rowBgOut = 'pink';
            var alertStyle = "style='background-color:pink' ";
        } else {
            // otherwise use the standard styles
            var rowBgOver = '#f0ce93';
            var rowBgOut = 'white';
            var alertStyle = "";
        }

        // set attributes for <Tr> tag
        var row = $("<tr " + alertStyle + " onmouseover=\"this.style.backgroundColor='" + rowBgOver + "';\" onmouseout=\"this.style.backgroundColor='" + rowBgOut + "';\" >")
        $("#box-table-a").append(row);

        // define functionality for how we should open a specific ticket
        var ticketOpen = "onClick=(document.location.href='/admin/tickets/edit_ticket/" + rowData.ticketID + "')";
        //talbe row
        row.append($("<td " + ticketOpen + ">"+rowData.ticketSource + "</td>"));
        row.append($("<td " + ticketOpen + ">"+rowData.ticketType + "</td>"));      
        row.append($("<td " + ticketOpen + ">" + rowData.customerName + "</td>"));     
        row.append($("<td style='line-height:14px;' " + ticketOpen + "><a href=/admin/tickets/edit_ticket/" + rowData.ticketID + ">" + rowData.ticketLabels + rowData.title.substr(0, 40) + "</a></td>"));
        row.append($("<td style='text-align: center;' " + ticketOpen + ">" + rowData.numEmail + "</td>"));
        row.append($("<td style='text-align: center;' " + ticketOpen + "><div class=\"" + rowData.statusCSS + "\">" + rowData.status + "</div></td>"));
        row.append($("<td style='text-align: center; font-weight: bold;' " + ticketOpen + ">" + rowData.displayUpdate + "</td>"));
        row.append($("<td style='text-align: center;' " + ticketOpen + ">" + rowData.assignedTo + "</td>"));
        row.append($("<td style='text-align: center;' " + ticketOpen + ">" + rowData.priority + "</td>"));
        row.append($("<td id='deleteTicketCell' onClick='closeTicket(" + rowData.ticketID + ");' ><img src='/images/icons/cross.png'/></td>"));

    }

    function createDataTable() {
        try {
         dataTable = $("#box-table-a").dataTable({
                "bDestroy": true,
                "bPaginate" : false,
                "bJQueryUI": true,
                "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [ 5, 7, 9 ] }
                ]
            });
        } catch (e) {
            console.log(e);
        }
    }

    // function for closing ticket
    function closeTicket(ticketID) {
        if (confirm('<?php echo __("Are you sure you want delete the selected ticket", "Are you sure you want delete the selected ticket?"); ?>')) {
             // execute ajax call
            $.ajax({
                url: "/admin/tickets/closeTicket",
                type: "post",
                data: {
                    "ticketID": ticketID
                },
                dataType: "json",
                success: function (data) {

                    if (data.status == 'success') {
                        // before rendering data, delete the existing data
                        $("#box-table-a > tbody").html("");
                        filterTickets();
                    }
                    else {
                        alert(data.error);
                    }
                }
            });
            // Save it!
        } 
    }

// function call to render data on first viewing of page
    filterTickets();
</script>	