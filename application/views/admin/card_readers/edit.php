<?php enter_translation_domain("admin/card_readers/edit"); ?>
<?php


?>
<h2><?php echo __("Edit Card Reader", "Edit Card Reader"); ?></h2>
<p>
<a href="/admin/card_readers"><img src="/images/icons/arrow_left.png"> <?php echo __("Return to card reader management", "Return to card reader management"); ?></a>
</p>

<form action="/admin/card_readers/update/<?= $cardReader->cardReaderID ?>" method="post">
<table class='detail_table'>
<tr>
    <th style="text-align: left;"><?php echo __("Location", "Location"); ?></th>
    <td>
        <?= form_dropdown('location_id', $locations, $cardReader->location_id, 'style="width:200px;"'); ?>
    </td>
</tr>
<tr>
    <th style="text-align: left;"><?php echo __("Password", "Password"); ?></th>
    <td>
        <?= form_input('password', $cardReader->password); ?>
    </td>
</tr>
<tr>
    <th style="text-align: left;"><?php echo __("IP Address", "IP Address"); ?></th>
    <td nowrap>
        <input type="text" name="address" value="<?= $cardReader->address ?>" style="width: 290px;">:<input type="text" name="port" value="<?= $cardReader->port ?>" style="width: 50px;">,<input type="text" name="masterPort" value="<?= $cardReader->masterPort ?>" style="width: 50px;">
    </td>
</tr>
<tr>
    <th style="text-align: left;"><?php echo __("Model", "Model"); ?></th>
    <td>
        <?= form_dropdown('model', $models); ?>
    </td>
</tr>
</table>
    <input type="submit" value="<?php echo __("Update", "Update"); ?>" class="orange button">
</form>
