<?php enter_translation_domain("admin/card_readers/log"); ?>
<h2><?php echo __("Card Reader Log for", "Card Reader Log for"); ?> <?= $address ?></h2>
<p>
<a href="/admin/card_readers"><img src="/images/icons/arrow_left.png"> <?php echo __("Return to card reader management", "Return to card reader management"); ?></a>
</p>


<table id="log_table">
<thead>
<tr>
    <th><?php echo __("Date", "Date"); ?></th>
    <th><?php echo __("Status", "Status"); ?></th>
    <th><?php echo __("AccessTime", "AccessTime"); ?></th>
    <th><?php echo __("Card", "Card"); ?></th>
    <th><?php echo __("Exp", "Exp"); ?></th>
    <th><?php echo __("Customers", "Customers"); ?></th>
    <th><?php echo __("RAW", "RAW"); ?></th>
</tr>
</thead>
<tbody>
</tbody>
</table>


<script type="text/javascript">

$.fn.dataTableExt.afnSortData['attr'] = function (oSettings, iColumn) {
	return $.map( oSettings.oApi._fnGetTrNodes(oSettings), function (tr, i) {
		return $('td:eq('+iColumn+')', tr).attr('data-value');
	});
};

$dataTable = $("#log_table").dataTable({
    "aoColumns": [
        null,
        null,
        null,
        null,
        null,
        null,
        null
    ],
    "iDisplayLength" : 100,
    "bProcessing" : true,
    "bServerSide" : true,
    "sAjaxSource" : "/admin/card_readers/log_data/<?= $location->locationID ?>",
    "bJQueryUI": true,
    "aaSorting" : [[ 0, "desc"]],
    "sDom": '<"H"lTfr>t<"F"ip>',
    "bStateSave" : true,
    "bFilter": false,
    "oTableTools": {
        <? if (in_bizzie_mode() && !is_superadmin()): ?>
        aButtons : [
            "pdf", "print"
        ],
        <? endif ?>
        "sSwfPath": "/js/DataTables-1.9.1/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
    }
}).fnSetFilteringDelay();
new FixedHeader($dataTable);

</script>
