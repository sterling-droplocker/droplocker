<?php enter_translation_domain("admin/card_readers/index"); ?>
<?php

?>
<h2><?php echo __("Card Readers", "Card Readers"); ?></h2>

<form action="/admin/card_readers/add" method="post">
<table class='detail_table'>
<tr>
    <th style="text-align: left;"><?php echo __("Location", "Location"); ?></th>
    <th style="text-align: left;"><?php echo __("Password", "Password"); ?></th>
    <th style="text-align: left;"><?php echo __("IP Address", "IP Address"); ?></th>
    <th style="text-align: left;"><?php echo __("Model", "Model"); ?></th>
    <th style="text-align: left;"><?php echo __("Mode", "Mode"); ?></th>
    <th colspan="3" style="text-align: left;"><?php echo __("Add", "Add"); ?></th>
</tr>
<tr>
    <td>
        <?= form_dropdown('location_id', $locations, null, 'style="width:200px;"'); ?>
    </td>
    <td>
        <?= form_input('password', '11111'); ?>
    </td>
    <td nowrap>
        <input type="text" name="address" style="width: 90px;">:<input type="text" name="port" value="57" style="width: 20px;">,<input type="text" name="masterPort" value="87" style="width: 20px;">
    </td>
    <td>
        <?= form_dropdown('model', $models); ?>
    </td>
    <td>
        <?= form_dropdown('mode', $modes); ?>
        <div class="time-windows" style="display:none">
            <input type="text" name="windowStart" style="width: 60px;" value="">
             to <input type="text" name="windowEnd" style="width: 60px;" value="">
        </div>
    </td>
    <td colspan="2">
        <input type="submit" name="create" value="<?php echo __("create", "create"); ?>" class="blue button">
    </td>
</tr>
</table>
</form>
<br><br>
<table class='hor-minimalist-a striped'>
<thead>
<tr>
    <th style="width:200px;"><?php echo __("Location", "Location"); ?></th>
    <th style=""><?php echo __("IP Address", "IP Address"); ?></th>
    <th style=""><?php echo __("Model", "Model"); ?></th>
    <th style="width:350px;"><?php echo __("Mode", "Mode"); ?></th>
    <th style=""><?php echo __("Last Sync", "Last Sync"); ?></th>
    <th style="width: 130px;"><?php echo __("Status", "Status"); ?></th>
    <th style="width: 130px;"><?php echo __("Actions", "Actions"); ?></th>
</tr>
</thead>
<tbody>
<? foreach($readers as $reader): ?>
    <tr>
        <td>
            <b><?= $reader->locAddr ?></b><br><br>
            <a href="/admin/card_readers/command?ip=<?= $reader->address ?>:<?= $reader->port ?>"><?php echo __("Send Commands", "Send Commands"); ?></a>
        </td>
        <td><?= $reader->address ?>:<?= $reader->port ?></td>
        <td><?= $reader->model ?></td>
        <td>
            <form action="/admin/card_readers/change_mode" method="post">
                <?= form_hidden('cardReaderID', $reader->cardReaderID); ?>
                <?= form_dropdown('mode', $modes, $reader->mode) ?>
                <input type="submit" name="update" value="<?php echo __("update", "update"); ?>">
                <div class="time-windows" style="display:<?= $reader->mode == 'window' ? 'block' : 'none' ?>">
                    <input type="text" name="windowStart" style="width: 60px;" value="<?= $reader->windowStart?>">
                     to <input type="text" name="windowEnd" style="width: 60px;" value="<?= $reader->windowEnd?>">
                </div>
                
            </form>

            <br><a href="/admin/card_readers/log/<?= $reader->location_id ?>" target="_blank"><?php echo __("View Access Log", "View Access Log"); ?></a>
        </td>
        <td width="600">
            <div class="<?= $reader->success ? 'alert-success' : 'alert-error' ?>">
                <?= convert_from_gmt_aprax($reader->syncTime, STANDARD_DATE_FORMAT) .': '. $reader->syncMessage ?>
                <br><a href="/admin/card_readers/history/<?= $reader->location_id ?>" target="_blank"><?php echo __("View Sync History", "View Sync History"); ?></a>
            </div>
            <br>
            <form action="/admin/card_readers/open" method="post" style="display:inline;">
                <?= form_hidden('cardReaderID', $reader->cardReaderID); ?>
                <input type="submit" name="open" value="<?php echo __("Open", "Open"); ?>">
            </form>

            <form action="/admin/card_readers/reset" method="post" style="display:inline;">
                <?= form_hidden('cardReaderID', $reader->cardReaderID); ?>
                <input type="submit" name="manual" value="<?php echo __("Reset", "Reset"); ?>">
            </form>

            <form action="/admin/card_readers/reboot" method="post" style="display:inline;">
                <?= form_hidden('cardReaderID', $reader->cardReaderID); ?>
                <input type="submit" name="reboot" value="<?php echo __("Reboot", "Reboot"); ?>" style="color:red;font-weight:bold;">
            </form>

            <? if($reader->mode == 'registered'): ?>
                <form action="/admin/card_readers/sync" method="post" style="display:inline;">
                    <?= form_hidden('cardReaderID', $reader->cardReaderID); ?>
                    <input type="submit" name="manual" value="<?php echo __("Reload All Cards", "Reload All Cards"); ?>">
                </form>

                <form action="/admin/card_readers/sync" method="post" style="display:inline;">
                    <?= form_hidden('cardReaderID', $reader->cardReaderID); ?>
                    <input type="hidden" name="newCards" value="1">
                    <input type="submit" name="manual" value="<?php echo __("Load New Cards", "Load New Cards"); ?>">
                </form>
            <? endif; ?>
        </td>
        <td align="center">
            <div style="color:<?= $reader->status == 'enabled' ? 'green' : 'red' ?>">
                <img src="/images/icons/<?= $reader->status == 'enabled' ? 'accept.png' : 'delete.png' ?>" style="position: relative; top: 3px;">
                <?= ucfirst($reader->status) ?>
            </div>
        </td>
        <td align="center">
            <a title="Edit" href="/admin/card_readers/edit/<?= $reader->cardReaderID ?>"><img src="/images/icons/pencil.png" alt="<?php echo __("Disable", "Disable"); ?>" /></a>
            <? if ($reader->status == 'enabled'): ?>
            <a class="" title="Disable" href="/admin/card_readers/disable/<?= $reader->cardReaderID ?>"><img src="/images/icons/delete.png" alt="<?php echo __("Disable", "Disable"); ?>" /></a>
            <? else: ?>
            <a class="" title="Enable" href="/admin/card_readers/enable/<?= $reader->cardReaderID ?>"><img src="/images/icons/accept.png" alt="<?php echo __("Enable", "Enable"); ?>" /></a>
            <? endif; ?>
            <a class="delete" title="Delete" href="/admin/card_readers/delete/<?= $reader->cardReaderID ?>"><img src="/images/icons/cross.png" alt="<?php echo __("Disable", "Disable"); ?>" /></a>
        </td>
    </tr>
<? endforeach ?>
</tbody>
</table>

<script type="text/javascript">
$(document).ready( function() {

$(".delete").click(function() {
    if (confirm("<?php echo __("Are you sure you want to delete this reader", "Are you sure you want to delete this reader?"); ?>")) {
        return true;
    } else {
        return false;
    }        
});  

$('select[name=mode]').change(function(evt) {
    if ($(this).val() == 'window') {
        $('.time-windows', $(this).parent()).show();
    } else {
        $('.time-windows', $(this).parent()).hide();
    }
});

});
</script>
