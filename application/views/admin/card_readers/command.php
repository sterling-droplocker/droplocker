<?php enter_translation_domain("admin/card_readers/command"); ?>
<h2><?php echo __("Send Card Reader Commands", "Send Card Reader Commands"); ?></h2>
<p>
<a href="/admin/card_readers"><img src="/images/icons/arrow_left.png"> <?php echo __("Return to card reader management", "Return to card reader management"); ?></a>
</p>

<br>
<?php echo __("IP", "IP:"); ?> <?= $ip ?>
<br>
<br>
<form action="/admin/card_readers/command" method="post">
<input type="hidden" name="ip" value="<?= $ip ?>">

<? foreach($log as $i => $entry): ?>
<input type="hidden" name="log[<?=$i?>][command]" value="<?= $entry['command'] ?>">
<input type="hidden" name="log[<?=$i?>][response]" value="<?= $entry['response'] ?>">
<? endforeach; ?>

<?php echo __("Command", "Command:"); ?> <input type="text" name="command"> <input type="submit" name="Send" value="<?php echo __("Send", "Send"); ?>">
<br>
<span style="font-size: 11px; padding-left:70px;"><?php echo __("use ~ for NULL character", "use ~ for NULL character"); ?></span>
</form>
<br>
<br>
<table id="box-table-a">
<tr>
    <th><?php echo __("Command", "Command"); ?></th>
    <th><?php echo __("Response", "Response"); ?></th>
</tr>
<? foreach($log as $entry): ?>
<tr>
    <td><?= $entry['command'] ?></td>
    <td><?= $entry['response'] ?></tr>
<? endforeach; ?>
</table>
