<div style="float:left; width: 630px; margin-left: 5px;">
<span class="bodyReg">
<br>
<h2>You look good, and your clothes should too!</h2><br>
<br>
<h2>&nbsp;&nbsp;&nbsp;2433 Durant Ave.</h2>
<img src="/images/2433Durant.jpg" alt="">
<br>
<br>
<div style="float:left; margin-left:10px; margin-top:15px; margin-bottom:15px;">
		<div style="float:left;"><img src="/images/special_left.jpg" alt="" width="27" height="87" border="0"></div>
		<div style="float: left; height:90px; background-image: url(/images/special_background.jpg); width: 570px; background-repeat: repeat-x;">
			<div style="float: left; margin-top:15px; margin-left:20px; width:350px;" class="bodyReg">
			<h2>Low Priced Plans for College Students!</h2>
			With our monthly laundry plans, you can receive <strong>great savings</strong> on our wash & fold service.
			</div>
			<div style="float: left; margin-top:30px; margin-left:20px;">
				<a href="/main/plans/college" class="btnLink_med" style="padding: 4px 0px 0px 0px;">Less than $1 / day</a>
			</div>
		</div>
		<div style="float:left;"><img src="/images/special_right.jpg" alt="" width="17" height="87" border="0"></div>
	</div>
	<div style="clear:both;"></div>
<br><br>
<strong>Laundry Locker is so easy to use.  Here's how it works:</strong><br><br>
1. Sign up on our website to gain 24 x 7 access<br>
2. Swipe your credit card to enter the location<br>
3. Place your clothes in a locker and take the key<br>
4. Place your order on our website
<br>
<br>
That's it!  
<br>
<br>
We come by every day, Monday - Friday and as long as your clothes are in the locker by 9am, wash & fold will be <strong>delivered back the next day</strong> (3-day turnaround on dry cleaning).  We even email/text message you as soon as it's ready for you to pick up.  And, the location is open 24 hours a day so you can pick up your clothes anytime that's convenient for you! 
<br>
<br>
Though we are a San Francisco based company, our Berkeley location is <strong>locally owned and operated</strong> by a Berkeley dad, with 3 kids attending school.  So you are helping out a local business and keeping your clothes clean!
<br>
<br>
Go Bears!! <br>
The Laundry Locker Team</span>
<br>
<br>
</div>

<div align="right" style="float:right; padding-top:30px; width:300px; padding-right:3px;">
<?
	echo callOut_collegeLaundryPlan();
?>
<div class="hr" style="margin-top: 10px; margin-bottom: 10px; float:right; width:282px; margin-right:3px;"><hr /></div>
<div class="graySmall" style="width: 279px; float: right; margin-right:3px;">
<a href="/main/dryclean" class="greenLink">dry cleaning pricing</a><br><br>
<a href="/main/washfold" class="greenLink">wash & fold pricing</a><br>
</div>
<div class="graySmall" style="width: 269px; float: right; margin-right:3px; padding-top:10px;">
	Wash & Fold placed in a locker before 9am, Mon - Fri will be returned by 5pm the following day, Tue - Sat <br>(3 day turnaround on Dry Cleaning & Individually Pressed Items)<br>
</div>
<div class="hr" style="margin-top: 30px; margin-bottom: 10px; float:right; width:282px; margin-right:3px;"><hr /></div>

</div>