<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">

            <h1>Wholesale Cleaning</h1><br>
            Laundry Locker<SUP><FONT SIZE="-2">TM</FONT></SUP> provides wholesale, eco-friendly dry cleaning and wash and fold to select Dry Cleaning agencies.  We own and operate San Francisco's only large scale, eco-friendly dry cleaning plant, enabling you to offer Eco-Friendly dry cleaning to your customers, helping you gain many new customers and improve your bottom line.
            <br><br>
            Unlike other wholesale cleaners, Laundry Locker provides:<br>
            - Eco-Friendly, GreenEarth cleaning<br>
            - Free Delivery<br>
            - Credit card payment<br>
            - Detailed item and order tracking<br>
            - Local, San Francisco, plant<br>
            <br>
            If you would like more information on our wholesale, eco-friendly cleaning service, please contact <a href="mailto:sales@laundrylocker.com">sales@laundrylocker.com</a>.

        </div>

        <div class="span4">
            <?php
            echo howNav('wholesale');
            ?>
        </div>
    </div>
</div>