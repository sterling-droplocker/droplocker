<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig">
                <h1>Our Technology</h1><br>
                <div class="visible-phone" style="text-align: center">
                <?= aboutNav('technology'); ?>
                </div>
                Founded in the heart of San Francisco, technology has been one of core competencies since our inception.  It is integrated throughout our operations and ensures that we can give you a higher quality product, in a faster time frame, with six sigma accuracy.  If you use our service, you'll experience the difference.
                <br><br>
                <h2>Eco-Friendly Cleaning</h2><br>
                Utilizing GreenEarth&#0153 dry cleaning technology, we clean your clothes with the power of sand.  This produces an eco-friendly product that is better for the environment and better for your clothes.
                </span>
                <br><br>
                <h2>Closet 2.0</h2><br>
                Closet 2.0 is the most advanced wardrobe management software.  Every dry cleaning item is photographed and cataloged.  This provides our customers with visibility of what we are cleaning, how we are cleaning it and a detailed history of every item.  Closet 2.0 ensures that your items are serviced and priced the same way, every time.
                </span>
                <br><br>
                <h2>Video Surveillance</h2><br>
                We utilize advanced video surveillance that is time synced with our operations, protecting our customers from lost or misplaced items.
                </span>
                <br><br>
                <h2>Automated Assembly</h2><br>
                Our automatic assembly system utilizes bar codes and precision equipment to ensure that you get back every item you sent us, and no "bonus" items.  Integrated with Closet 2.0, we are able to see exactly how your order was packaged, down to the level of what bag each item was packaged in.
                </span>
                <br><br>
                <h2>RWDI - Realtime Wireless Driver Interface</h2><br>
                Each driver is equipped with their hand-held RWDI device.  The combination of our hand-held internet device and sophisticated bar coding technology provides real time order tracking and accountability from the second your order is picked up to the second it is delivered.
                </span>
                <br><br>
                <h2>Email and Text Alerts</h2><br>
                As soon as your goods have been processed and delivered back to your locker, the system can notify you via text message and/or email.
                </span>
                <br><br>
                <h2>Secure Online Payment</h2><br>
                We take security EXTREMELY seriously.  We have invested significant time and money in ensuring that our website, and especially our  payment system, is very secure.  This is as much for you as it is for us. <br>
                <br>
                Managing secure information such as credit cards and social security numbers is risky business.  We have built our system so that we never have to store any of this sensitive information.  To setup automatic payments, we do what is called a reference transaction.  This means that instead of passing your credit card number to the processor with each order, we simply tell the credit card processor to charge your card the same way it was charged last time.  Therefore, we never need to keep your credit card information in our system.
                <br><br>
                Additionally, we use secure technologies from Verisign and Comodo, two of the most trusted companies in online security.<br>
                <br>
                <table width="240" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td align="left" valign="bottom"><a href="http://www.PositiveSSL.com" target="_blank" style="font-family: arial; font-size: 10px; text-decoration: none;" title="SSL Certificate Authority"><img src="/images/PositiveSSL_tl_trans.gif" alt="SSL Certificate Authority" title="SSL Certificate Authority" border="0" /><br /></a></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="span4">
            
            <div class="hidden-phone">
                <?= aboutNav('technology'); ?>
            </div>

            <?= callOut_c20(); ?>
        </div>
    </div>
</div>