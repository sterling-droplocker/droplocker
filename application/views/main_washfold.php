<div class="fluid-container">
    <div class="row-fluid">

        <div class="span8">
            <div class="bodyBig">
                <h1>Wash & Fold</h1><br />
                <div class="visible-phone" style="text-align: center;"><?= servicesNav('wf');?></div>


                <div class="calloutSpecial bodyReg">
                    <h2>Laundry Plans as Low as $.99 Per Pound!</h2>
                    With our monthly laundry plans, you can receive <strong>great savings</strong> on our wash & fold service.
                    <a class="laundryPlansButton btn btn-primary" href="/main/plans" class="btn btn-primary">Learn More</a>
                </div>

                Wash & Fold is just like going to the laundromat, except we do all the work for you. First your clothes are sorted and colors are separated from whites. Then we wash and dry your clothes in separate machines for each customer, according to the preferences that you setup on our website. Next, your clothes are neatly folded, socks are matched and the clothes are wrapped up nice and tidy like a present...
                <br>
                <br>
                <strong>...all you've got to do is put them away.</strong>
                <div class="hr" style="margin-top: 30px;"><hr /></div>
            </div>
            <div class="row-fluid">
                <table class="table table-condensed" id="dryCleanTable" style="margin-top: 30px; margin-bottom: 0px">
                    <tbody>
                        <tr>
                            <td><strong>Wash & Fold</strong></td>
                            <td class="priceCell">
                                 from $.99 per lb (with a <a href="/main/plans">laundry plan</a>)<br/>
                                 $1.69 per pound (no plan)
                            </td>
                        </tr>
                        <tr><td></td></tr>
                    </tbody>
                </table>
                <table class="table table-condensed" id="dryCleanTable" style="margin-bottom: 0px">
                    <tbody>
                        <tr>
                            <td>Comforters</td><td class="priceCell"> $28.99 </td>
                        </tr>
                        <tr>
                            <td>Blankets</td><td class="priceCell">$13.05</td>
                        </tr>
                        <tr>
                            <td>Pillows</td><td class="priceCell"> $13.05 </td>
                        </tr>
                        <tr>
                            <td>Mattress Pad</td><td class="priceCell"> $15.00 </td>
                        </tr>
                        <tr>
                            <td>Heavy Rugs (over 4 lbs)</td><td class="priceCell"> $19.99</td>
                        </tr>
                        <tr>
                            <td>Small Rugs (under 4 lbs)</td><td class="priceCell">$10.00</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed" id="dryCleanTable" style="margin-bottom: 0px">
                    <tbody>
                        <tr>
                            <td>Anytime Home Delivery</td><td class="priceCell"> 10% surcharge </td>
                        </tr>
                        <tr>
                            <td>Scheduled On-Demand Home Delivery</td><td class="priceCell">20% surcharge</td>
                        </tr>
                        <tr>
                            <td>Heavily Soiled / Double Wash</td><td class="priceCell"> $.50/lb surcharge </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-condensed" id="dryCleanTable" style="margin-top: 30px;margin-bottom: 2px">
                    <tbody>
                        <tr><td><strong>Upgrades</strong></td></tr>
                        <tr>
                            <td>Detergent</td><td>Tide</td><td class="priceCell">$.09/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>All Free Clear</td><td class="priceCell">$.11/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>Ecos Magnolias & Lilies </td><td class="priceCell">$.11/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>Woolite Dark </td><td class="priceCell">$.16/lb</td>
                        </tr>
                        <tr><td></td></tr>
                        <tr><td></td></tr>
                        <tr>
                            <td>Bleach</td><td>Clorox 2</td><td class="priceCell">$.06/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>Oxy Clean</td><td class="priceCell">$.09/lb</td>
                        </tr>
                        <tr><td></td></tr>
                        <tr><td></td></tr>
                        <tr>
                            <td>Fabric Softener</td><td>Downy</td><td class="priceCell">$.09/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>Seventh Generation</td><td class="priceCell">$.11/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>Ecover</td><td class="priceCell">$.11/lb</td>
                        </tr>
                        <tr><td></td></tr>
                        <tr><td></td></tr>
                        <tr>
                            <td>Dryer Sheets</td><td>Bounce</td><td class="priceCell">$.09/lb</td>
                        </tr>
                        <tr>
                            <td></td><td>Bounce Scent Free</td><td class="priceCell">$.09/lb</td>
                        </tr>
                        <tr><td></td></tr>
                        <tr><td></td></tr>
                        <tr>
                            <td colspan="2">Dry on Low Temp</td><td class="priceCell">$.11/lb</td>
                        </tr>
                        <tr>
                            <td colspan="2">Super Wash</td><td class="priceCell">$.21/lb</td>
                        </tr>
                     </tbody>
                </table>                                            
            </div>
            <div class="bodyReg" style="margin: 20px 0">
                <strong>Special Requirements</strong>
                <br>
                <br>
                Would you like us to use fabric softer, hot water (by default, all clothes are cleaned in cold water to reduce shrinking, fading and bleeding), dry your clothes on low, use bleach or provide other special service? No problem! Once you sign up as a user, you will be asked to setup your wash & fold preferences so we can customize your order.
                <br>
                <br>
                Wash & Fold is a bulk service. If an item needs special attention such as a stain or repair, please send it in for dry cleaning. You can send us an email if you have a question about a specific request.
                <br>
                <br>
                <strong>Special Items</strong>
                <br>
                <br>
                Do you have special items such as comforters, pillows or blankets? No problem, we can take care of it. Because some of these items require special handling, we may need to charge by the piece, not the pound and some items may require second day service. If you have a specific item that you would like pricing and turnaround information for, just send us an email.
                <br>
                <br>
                <strong>Lost Key Fee</strong>
                <br>
                <br>
                $25 (we'll credit your account $10 if you find the key later)
            </div>
        </div>

        <div class="span4">

            <?php
            echo '<div class="hidden-phone">'.servicesNav('wf').'</div>';
            echo callOut_laundryPlan();
            ?>
            <br>
            <div class="dryCleanNote">
                <div class="graySmall">
                    There is a minimum weight of 15 lbs for wash & fold<br><br>
                    *Rates are subject to change without notice
                </div>


            </div>
        </div>
    </div>
</div>