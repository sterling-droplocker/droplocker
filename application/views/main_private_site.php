<html>
	<head>
		<title>Drop Locker Business Admin Login</title>
                <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
                <script type="text/javascript" src='/js/functions.js'></script>
		<style>
			body{
				text-align:center;
				margin-top:100px;
				background-color:#a6ccdd;
			}
			
			#login_box{
				margin:0 auto;
				width:500px;
				border-radius:5px;
				-moz-border-radius:5px;
				border:1px solid #000;
				background-color:#FFF;
				-moz-box-shadow: 0px 0px 6px #000;
				-webkit-box-shadow: 0px 0px 6px #000;
				box-shadow: 0px 0px 6px #000;
				min-height:190px;
				text-align:center;
			}
			
			table{
				margin:0 auto;
				width:80%;
			}
			
			
			
			.button{
				margin-left:160px;
			}
			
			#head{
				text-align: left;
				width:500px;
				margin:0 auto;
			}
			#head h2{
				color:#333;
				font-weight:bold;
				border-bottom: none;
			}
                        
                       .error{
                            text-align:left;
                        }
                        
                        .success{
                            text-align:left;
                        }
                        
                        li{
                            list-style-type: none;
                        }
			
		</style>
	</head>
	<body>
		<div id='head'><h2><?= $slug?>.<?= $_SERVER['HTTP_HOST']?> is private</h2></div>
		<div id='login_box'>
			<?= $this->session->flashdata('message')?>
			<h3>Please Login</h3>
			<form method='post' action=''>
				<table>
					<tr>
						<th align="right">Username</th>
						<td><input size='30' type='text' name='username' /></td>
					</tr>
					<tr>
						<th align="right">Password</th>
						<td><input size='30' type='password' name='password' /></td>
					</tr>
				</table>
				<input type='hidden' value='<?= $redirect?>' name='redirect' />
				<input class='button orange' type='submit' value='Login' name='submit' />
			</form>
			<div style='text-align:left; padding:0px 20px;'><a style='color:#333;' href='/admin/index/forgot_password/'>Forgot password?</a></div>
		</div>
	</body>
</html>
