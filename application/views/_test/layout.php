<html>
	<head>
		<title>layout</title>
		<script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
		<script src='/js/jquery-ui-1.8.16.custom.min.js'></script>
		<script src='/js/jquery.easyui.min.js'></script>
		<link href="/css/default/easyui.css" rel="stylesheet" />
		<link href="/css/custom-theme/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="/js/tinymce/jscripts/tiny_mce/tiny_mce.js" ></script >
		<script type="text/javascript" >
		tinyMCE.init({
		        mode : "textareas",
		        theme : "simple"   //(n.b. no trailing comma, this will be critical as you experiment later)
		});
		</script >
	</head>
	<body>
		<div id="cc" class="easyui-layout" style="width:600px;height:400px;">
			<div region="center" title="center title" style="padding:5px;background:#eee;">
				<form action='' method='post'>
					<textarea name='content'></textarea>
					<input type='submit' name='submit' value='submit' />
				</form>
			</div>
		</div>
	</body>
</html>