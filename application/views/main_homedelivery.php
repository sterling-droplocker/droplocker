<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/lp-header.jpg" class="max-image" />

            <h1 class="header-with-space">Dry cleaning and laundry delivered to your doorstep</h1>

            <div class="row-fluid">
                <div class="span4 centered">
                    <img src="/images/no-outsourcing.png" />
                    <h2 class="gray-header">No outsourcing</h2>
                    Don’t trust your delicates to just anyone! We run the largest and most sophisticated plant in San Francisco.
                </div>

                <div class="span4 centered">
                    <img src="/images/ups-drycleaning.png" />
                    <h2 class="gray-header">The "UPS" of Drycleaning</h2>
                    We created the most detailed order tracking for your garments in the industry
                </div>

                <div class="span4 centered">
                    <img src="/images/10000.png" />
                    <h2 class="gray-header">Shirts and counting.</h2>
                    This isn’t our first day, we have been at this a long time and perfected every aspect of the process along the journey!
                </div>
            </div>

            <h1 class="header-with-space">Two service types to meet your needs</h1>

            <div class="row-fluid">
                <div class="span2">&nbsp;</div>
                <div class="span4 centered">
                    <img src="/images/clock.png" />
                    <h2 class="gray-header">Scheduled Time Orders</h2>
                    <p>Want your orders picked up and delivered at a specific time?</p>
                    <p>We operate 7 days a week, from 9AM to 9PM within 2-hour windows.</p>
                    <p>Special pricing applies, <a href="/main/dryclean" class="greenLink">click here</a> to learn more.</p>
                    <a href="/main/athome" class="blue-btn" id="scheduled-lm-button">Learn More</a>
                </div>

                <div class="span4 centered">
                    <img src="/images/key.png" />
                    <h2 class="gray-header">Anytime Orders</h2>
                    <p>Don’t need your order delivered or picked up at a specific time?</p>
                    <p>We operate 5 days a week, from 9AM to 6PM.</p>
                    <p>Contact us to set up your first pickup and provide us with a key or a door code to get started.</p>
                    <a href="/main/anytimeorders" class="blue-btn">Learn More</a>
                </div>
                <div class="span2">&nbsp;</div>
            </div>
        </div>
    </div>
</div>