<?
if(! function_exists('getHours')){
function getHours($day, $emp)
{
	$selectDayz = date("Y-m-d",strtotime($day));	
	$inHours = "SELECT `day`, sum(time_to_sec(timeInOut) / 60) as inMinutes from timeCardDetail
				JOIN timeCard on timeCard.id = timeCardDetail.timeCard_id
				join employee on employee.customer_id = timeCard.customer_id
				WHERE inOut = 'In'
        		and day = '$selectDayz'
				and employee.customer_id = '$emp'
				group by timeCard.id;";
	$query = $GLOBALS['db']->query($inHours);
	while ($linex = $query->fetchRow(DB_FETCHMODE_ASSOC))
	{
		$inMinutes = $linex[inMinutes];
	}
	
	$outHours = "SELECT `day`, sum(time_to_sec(timeInOut) / 60) as outMinutes from timeCardDetail
				JOIN timeCard on timeCard.id = timeCardDetail.timeCard_id
				join employee on employee.customer_id = timeCard.customer_id
				WHERE inOut = 'Out'
        		and day = '$selectDayz'
				and employee.customer_id = '$emp'
				group by timeCard.id;";
	$query = $GLOBALS['db']->query($outHours);
	while ($linex = $query->fetchRow(DB_FETCHMODE_ASSOC))
	{
		$outMinutes = $linex[outMinutes];
	}
	
	$data[hours] = number_format_locale(($outMinutes - $inMinutes) / 60,2);
	
	
	$outHours = "SELECT min(timeInOut) as startTime
				from timeCardDetail
				JOIN timeCard on timeCard.id = timeCardDetail.timeCard_id
				join employee on employee.customer_id = timeCard.customer_id
				WHERE day = '$selectDayz'
				and employee.customer_id = '$emp';";
	$query = $GLOBALS['db']->query($outHours);
	$line = $query->fetchRow(DB_FETCHMODE_ASSOC);
	$data[startTime] = $line[startTime];

	return $data;
}
}

if(! function_exists('getStops')){
function getStops($day, $emp)
{
	$selectDayz = date("Y-m-d",strtotime($day));	
	$query = "SELECT delType, count(distinct driverLog.delTime) as transactions
				FROM driverLog
				join customer on customer.username = driverLog.delManId
				WHERE date_format(delTime, '%Y-%m-%d') = '$selectDayz'
				and customer.id = '$emp'
				group by delType;";
	$query = $GLOBALS['db']->query($query);
	while ($line = $query->fetchRow(DB_FETCHMODE_ASSOC))
	{
		$data[$line[delType]] = $line[transactions];
	}
	return $data;
}
}
?>