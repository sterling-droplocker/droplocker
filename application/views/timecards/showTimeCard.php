<?

require_once('minToHour.func');
include('driverData.php');
$overtime = get_business_meta($this->business_id, 'timecard_overtime');

//timecard.php passed a list of timeCards to print.  Now go through each one and $timeCardText = $timeCardText . it


$results = $queryName;
//var_dump($results);
if (sizeof($results) == 0 && !empty($_REQUEST[employeeID]) && $_REQUEST[employeeID] != '%' )
{
    $get_time_card_select = "SELECT 1 FROM timeCard where employee_id = $_REQUEST[employeeID] and weekOf = '$_REQUEST[weekOf]'";
    $get_time_card_query = $this->db->query($get_time_card_select);
    $get_time_card_result = $get_time_card_query->result();
    
    if (sizeof($get_time_card_result) == 0) 
    {
        ?>
        <form action="/admin/employees/finance/<? echo $_REQUEST[weekOf]; ?>" method="post">
           <input type="hidden" name="createTimeCard" value="1">
           <input type="hidden" name="weekOfInsert" value="<? echo $_REQUEST[weekOf]; ?>">
           <input type="hidden" name="employeeidInsert" value="<? echo $_REQUEST[employeeID] ?>">
           <input type="submit" name="submitCreateTimeCard" value="Create Time Card for <? echo $_REQUEST[weekOf]; ?>">
           <br>
           <br>
        </form>
        <?
    }
}

foreach($results as $line)
{
	$currentEmp = $line->employee_id;
	//find out if this employee gets OT on Saturdays
	$query3 = "SELECT *
                from employee
				WHERE employeeID = $currentEmp;";
	$query = $this->db->query($query3);
  	$linez = $query->result();
	$satOt = $linez[0]->satOt;
	$ssn = $linez[0]->ssn;
        $hourlyRate = $linez[0]->hourlyRate;

        //Determine Sick time accrued, used and available.
        //Default accrual rate is 1 hour for every 30 hours worked
        //Overriden by timeCardSetting value.
	
        //find out the total number of hours worked for this employee to calculate amount of accrued sick time by subtracting in time from out time
        $accrue_query = "                          
            SELECT sum(
                ( CASE WHEN in_out = 'Out' THEN time_to_sec(timeInOut) ELSE 0 END -
                  CASE WHEN in_out = 'In' THEN time_to_sec(timeInOut) ELSE 0 END )
                / 60 / 60 / CASE WHEN ptoRate.value is null then 30 else ptoRate.value END
              ) accruedSickHr            
            from timeCardDetail
            JOIN timeCard on timeCardID = timeCardDetail.timeCard_id
            join employee on employeeID = employee_id
            LEFT JOIN timeCardSetting ptoRate
              on ptoRate.settingType = 'PTO Accrual Rate' and 
                 (ptoRate.startDay = '' OR ptoRate.startDay <= timeCardDetail.day) and 
                 (ptoRate.endDay = '' OR ptoRate.endDay >= timeCardDetail.day) and 
                 ptoRate.business_id = $this->business_id
            LEFT JOIN timeCardSetting ptoDelay
              on ptoDelay.settingType = 'PTO Start Delay' and 
                 (ptoDelay.startDay = '' OR ptoDelay.startDay <= employee.startDate) and 
                 (ptoDelay.endDay = '' OR ptoDelay.endDay >= employee.startDate) and 
                 ptoDelay.business_id = $this->business_id
            WHERE employee_id = $currentEmp
              AND timeCardDetail.type in ('Regular')
              AND timeCardDetail.day > date_add(startDate, interval (CASE WHEN ptoDelay.value IS NULL THEN 90 else ptoDelay.value END) day)
        ";
        
        $query_results = $this->db->query($accrue_query);
        $accrued_results = $query_results->result();
        $accruedSickHr = number_format_locale($accrued_results[0]->accruedSickHr, 0);

	//subract out any sick time
	$sick_query = "
            SELECT sum(
                ( CASE WHEN in_out = 'Out' THEN time_to_sec(timeInOut) ELSE 0 END -
                  CASE WHEN in_out = 'In' THEN time_to_sec(timeInOut) ELSE 0 END )
                / 60 / 60
              ) usedSickHr            
            from timeCardDetail
            JOIN timeCard on timeCardID = timeCardDetail.timeCard_id
            join employee on employeeID = employee_id
            LEFT JOIN timeCardSetting ptoRate
              on ptoRate.settingType = 'PTO Accrual Rate' and 
                 (ptoRate.startDay = '' OR ptoRate.startDay <= timeCardDetail.day) and 
                 (ptoRate.endDay = '' OR ptoRate.endDay >= timeCardDetail.day) and 
                 ptoRate.business_id = $this->business_id
            LEFT JOIN timeCardSetting ptoDelay
              on ptoDelay.settingType = 'PTO Start Delay' and 
                 (ptoDelay.startDay = '' OR ptoDelay.startDay <= employee.startDate) and 
                 (ptoDelay.endDay = '' OR ptoDelay.endDay >= employee.startDate) and 
                 ptoDelay.business_id = $this->business_id
            WHERE employee_id = $currentEmp
              AND timeCardDetail.type in ('Sick', 'PTO')
              AND timeCardDetail.day > date_add(startDate, interval (CASE WHEN ptoDelay.value IS NULL THEN 90 else ptoDelay.value END) day)
            ";
	$query_results = $this->db->query($sick_query);
  	$sick_results = $query_results->result();
	$usedSickHr = number_format_locale($sick_results[0]->usedSickHr, 0);
        
	$availableSickTime = $accruedSickHr - $usedSickHr;
	
	$timeCardText = '';
	$timeCardText = $timeCardText . '<table border=0 cellpN0ing=0 cellspacing=0><tr><td bgcolor=9999cc>';
	$timeCardText = $timeCardText . '<table border=0>';

	//set some constants
	$weekOf = date('m/d/Y', strtotime($line->weekOf));
	$cardStatus = $line->status;
	$cardId = $line->timeCardID;
	$cardNote = $line->note;

	//reset any week counters
	$rowCounter = 0;
	$accumHours = 0;
	$weekRegHour = 0;
	$weekOverHour = 0;
	$weekPtoHour = 0;
	$weekHolHour = 0;
	$weekBonus = 0;
	$weekDriverAmt = 0;
	$currentDay = '';

	//$timeCardText = $timeCardText . the header for the timecard
	$empFname = $line->firstName;
	$empLname = $line->lastName;
	$empEmail = $line->email;
        
        $empName = getPersonName($line);

	//get role
	$rolex = "select * from employee where employeeID = $currentEmp";
	$query = $this->db->query($rolex);
  	$lineq = $query->result();
	if($lineq[0]->rateType == "Per Item") { $empRole = 'Driver'; }
	else { $empRole = ''; }

	$timeCardText = $timeCardText . '<tr>
			<td colspan=10>
			<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td align=left><a name='.$currentEmp.'><b>'.$empName.'- '.$empEmail.'</b</a></td>
					<td align=center>&nbsp;&nbsp;&nbsp;&nbsp;<b>Week Of: '.$weekOf.'</b></td>
					<td align=right>&nbsp;&nbsp;&nbsp;&nbsp;<b>Status: '.$line->status.'</b></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			</tr>
	<tr>
		<td><b><small>Day</small></b></td>
		<td colspan=2><b><small>Punch</small></b></td>';
        if($overtime == 1)
        {
		    $timeCardText .= '<td align="right"><b><small>&nbsp;&nbsp;Regular</small></b></td>
            		<td align="right"><b><small>&nbsp;&nbsp;Overtime</small></b></td>';
		}
        else
        {
            $timeCardText .= '<td align="right"><b><small>&nbsp;&nbsp;Hours</small></b></td>';
        }
        $timeCardText .= '<td align="right"><b><small>&nbsp;&nbsp;Note/PTO</small></b></td>
		<td align="right"><b><small>Date Entered</small></b></td>
		<td align="right"><b><small>Task</small></b></td>
		<td align="right"><b><small>Delete</small></b></td>
        <td align="right"><b><small>IP Address</small></b></td>
	</tr>';


	//Now go get all the details of this timecard
	$query1 = "SELECT *, (time_to_sec(timeInOut) / 60) as minutes, timeInOut
				FROM timeCardDetail
				WHERE timeCard_id = $line->timeCardID
				ORDER BY day, timeInOut ASC;";

	$query = $this->db->query($query1);
  	$cardDetails = $query->result();


//EACH ROW IN THE TIMECARD
	foreach($cardDetails as $line)
	{
		$rowCounter++;

//DAY FOOTER
		// if the day has changed at it is not the first record
		if($currentDay != $line->day and $rowCounter != 1)
				{
					include 'timeCardDayFooter.php';
					$lastCount['punches'] = $punchCounter;
					$lastCount['day'] = $currentDay;
					$lastCount['detailId'] = $lastCard;
					//$lastCount['detailId'] = $cardDetails[0]->timeCardDetailID;
					$lastCount['note'] = $currentNote;
					$punchCounter = 0;
				}
				$punchCounter ++;
				$lastCard = $line->timeCardDetailID;

//LINE ITEMS
		$timeCardText = $timeCardText . '<tr>';

		//if the day is the same as the previous day, don't display the day name again
		if($currentDay != $line->day)
		{
			$printDay = date('D - m/d', strtotime($line->day));
			$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF valign=top nowrap><small>'.$printDay.'</small></td>';
			//reset the day counters
			$dayRegMin = 0;
			$dayOverMin = 0;
			$dayPtoHour = 0;
			$dayHolHour = 0;
			$dayMinutes = 0;

			//$dayRegHours = 0;
			$ptoHour = 0;
			//$holidayHour = 0;
		}
		else { $timeCardText = $timeCardText . '<td bgcolor=#FFFFFF></td>'; }

		//if it is not holiday or pto
		if($line->timeInOut != '')
		{
			//if the time is after midnight, we format it as 28:03:08 so that needs to be converted to 4:03am
			if($line->minutes >= 1440) {
				$adjustedTime = $line->minutes - 1440;

				$parseTime = explode(":", $line->timeInOut);
				$parseHour = $parseTime[0] - 24;
				$parseTime = $parseHour.':'.$parseTime[1].':'.$parseTime[2];
				$printTime = date("g:i a",strtotime($parseTime));
			}
			else
			{
				//$printTime = date("g:i a",($line->minutes * 60));
				$printTime = date("g:i a",strtotime($line->timeInOut));
			}

			$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF colspan=2 valign=top nowrap>'.$line->in_out.' - '.$printTime;
			if ($line->type <> "Regular") { $timeCardText = $timeCardText . ' (' . $line->type . ')'; }
			$timeCardText = $timeCardText . '</small></td>';
		}
		else { $timeCardText = $timeCardText . '<td bgcolor=#FFFFFF colspan=2 valign=top nowrap><small>'.$line->type.'</small></td>'; }
		$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF>&nbsp;</td>';
		if($overtime == 1) { $timeCardText = $timeCardText . '<td bgcolor=#FFFFFF>&nbsp;</td>'; }

		// $timeCardText = $timeCardText . the notes
		$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF valign=top><small>'.urldecode($line->note).'</small></td>';

		// $timeCardText = $timeCardText . the time it was entered
		$printTimeStamp = $thisDate = local_date('UM8', $line->timestamp, 'm/d/y g:ia');
		$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF valign=top nowrap><small>'.$printTimeStamp.'</small></td>';
		$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF valign=top nowrap><small>'.$line->task.'</small></td>';

		//if it's in a Preliminary or Denied By Manager status, let the user delete it.
		if(($cardStatus  == 'Preliminary' or $cardStatus  == 'Denied By Manager' or $cardStatus  == 'Rejected By Finance') and $queryUser == 'delivery')
			{
				$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF valign=top><a href="/admin/employees/timecard_delete/'.$line->timeCardDetailID;
				if ($_POST['mgrEdit']) { $timeCardText = $timeCardText . '/mgrEdit'; }
				$timeCardText = $timeCardText .'"><img src="/images/icons/cross.png" /></a>';

				//only show on first line
				if($currentDay != $line->day)
				{
					if ($_POST['mgrEdit']) { $timeCardText .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/admin/employees/timecard?prior='.$line->timeCardDetailID.'&weekOf='.$weekOf.'&mgrEdit=1&cardId='.$cardId.'">prior</a>';}
				}
				$timeCardText .= '</small></td>';
			}
		else
			{$timeCardText = $timeCardText . '<td bgcolor=#FFFFFF></td>';}

        $timeCardText = $timeCardText . '<td bgcolor=#FFFFFF valign=top nowrap align=right><small>'.$line->ip.'</small></td>';
		$timeCardText = $timeCardText . '</tr>';

		//settings at the end of each record
		if ($line->in_out == 'In')
			{ $dayMinutes = $dayMinutes - $line->minutes; }
		else
			{ $dayMinutes = $dayMinutes + $line->minutes; }

		//accrue sick time
		if ($line->type == "Sick" || $line->type == "PTO")
		{
			if ($line->in_out == 'In')
				{ $dayPtoHour = $dayPtoHour - $line->minutes; }
			else
				{ $dayPtoHour = $dayPtoHour + $line->minutes; }
		}

		$currentDay = $line->day;
		$currentId = $line->id;
		$currentNote = $line->note;
		$dayRegMin = $dayMinutes;
		//$accumHours = $accumHours + $line['minutes'];
		if($line->type == 'PTO') //if this is PTO time
			{ $dayPtoHour = $dayPtoHour + $line->hour; }
		else if($line->type == 'Holiday') //if this is Holiday time
			{ $dayHolHour = $dayHolHour + $line->hour; }
	}
	{ include 'timeCardDayFooter.php';	}

//TIMECARD FOOTER
	$timeCardText = $timeCardText . '<tr>';
	$totHours = $weekRegHour + $weekOverHour + $weekPtoHour;
	$timeCardText .= '<td colspan="2" align="right" valign="top" nowrap><b>Total Hours:&nbsp;&nbsp;&nbsp;'.number_format_locale($totHours,2).'</b>';
	if($empRole == 'Driver') { $timeCardText .= '<br><strong>Total Compensation: <a href="../tools/driverLog.php?week='.$currentDay.'&driver='.$currentEmp.'" target="_blank">$'.number_format_locale($weekDriverAmt,2).'</a> or $'.number_format($weekDriverAmt/(($weekOverHour * 1.5) + $weekRegHour),2).' per hour</strong>'; }
	$timeCardText .= '</td>';
	$timeCardText = $timeCardText . '<td>&nbsp;</td><td align=right valign="top"><b>'.number_format_locale($weekRegHour,2).'</b></td>';
	if($overtime == 1) { $timeCardText = $timeCardText . '<td align=right valign="top"><b>'.number_format_locale($weekOverHour,2).'</b></td>'; }
	$timeCardText = $timeCardText . '<td align=right valign="top"><b>'.number_format_locale($weekPtoHour,2).'</b></td>';
	$timeCardText = $timeCardText . '<td></td><td></td><td align=right valign="top"><b>'.format_money_symbol($this->business_id, '%.2n', $weekBonus).'</b></td>';
	//let the manager edit it
	if ($queryUser == 'finance')
	{
	  	$timeCardText = $timeCardText . '<form action="/admin/employees/timecard" method=post>
										<input type=hidden name="cardId" value="'.$cardId.'">
										<input type=hidden name="weekOf" value="'.$weekOf.'">
										<input type=hidden name="mgrEdit" value="1">
										<tr><td colspan=9><hr></td></tr>
										<tr><td colspan=8 align=right valign=top><textarea cols="50" rows="2" name="note">'.urldecode($cardNote).'</textarea><br><small><b>timecard note</b></small></td>
										<td align=center valign=top><input type=submit name="Edit" value="Edit Time Card"></td></tr>
										</form>';
	}

	if (($cardStatus == 'Preliminary' or $cardStatus == 'Denied By Manager' or $cardStatus == 'Rejected By Finance') and $queryUser == 'delivery')
	//let the user submit it to their manager for approval
	{
		$timeCardText = $timeCardText . '<form action="/admin/employees/timecard" method="post">';
		$timeCardText = $timeCardText . '<input type=hidden name="cardId" value='.$cardId.'>';
		$timeCardText = $timeCardText . '<input type=hidden name="weekOf" value='.$_REQUEST['weekOf'].'>';
		$timeCardText = $timeCardText . '<input type=hidden name="mgrEdit" value='.$_REQUEST['mgrEdit'].'>';
		$timeCardText = $timeCardText . '<tr><td colspan=10><hr></td></tr>';
		$timeCardText = $timeCardText . '<tr><td colspan=8 align=right valign=top><textarea cols="50" rows="2" name="addNotes">'.urldecode($cardNote).'</textarea><br><small><b>timecard note</b></small></td>';
		$timeCardText = $timeCardText . '<td align=right valign=center colspan=2>&nbsp;<input type=submit name="notes" value="Add notes"></td></tr>';
		$timeCardText = $timeCardText . '</form>';
	}

	//let the manager approve it
	else if ($cardStatus == 'Waiting Approval' and $queryUser == 'manager')
	{
	  	$timeCardText = $timeCardText . '<form action="timecard.php" method=post>';
		$timeCardText = $timeCardText . '<input type=hidden name="cardId" value="'.$cardId.'">';
		$timeCardText = $timeCardText . '<tr><td colspan=9><hr></td></tr>';
		$timeCardText = $timeCardText . '<tr><td colspan=8 align=right valign=top><textarea cols="50" rows="2" name="note">'.$cardNote.'</textarea><br><small><b>timecard note</b></small></td>';
		$timeCardText = $timeCardText . '<td align=center valign=top><input type=submit name="Approve" value="Approve"><br><input type=submit name="Deny" value="Deny"></td></tr>';
		$timeCardText = $timeCardText . '</form>';
	}

	//let finance accept it it
	else if ($cardStatus == 'Approved By Manager' and $queryUser == 'finance')
	{
	  	$timeCardText = $timeCardText . '<form action="finance.php" method=post>';
		$timeCardText = $timeCardText . '<input type=hidden name="cardId" value="'.$cardId.'">';
		$timeCardText = $timeCardText . '<tr><td colspan=9><hr></td></tr>';
		$timeCardText = $timeCardText . '<tr><td colspan=8 align=right valign=top><textarea cols="50" rows="2" name="note">'.$cardNote.'</textarea><br><small><b>timecard note</b></small></td>';
		$timeCardText = $timeCardText . '<td align=center valign=top><input type=submit name="FinanceAccept" value="Accept by Finance"><br><input type=submit name="FinanceDeny" value="Reject by Finance"></td></tr>';
		$timeCardText = $timeCardText . '</form>';
	}

	$timeCardText = $timeCardText . '<tr><td colspan=9><hr></td></tr>';
	$timeCardText = $timeCardText . '<tr><td colspan=9 valign=top>Notes: '.urldecode($cardNote).'</td>';
	$timeCardText = $timeCardText . '<tr><td colspan=9><hr></td></tr>';
	$timeCardText = $timeCardText . '<tr><td colspan=9 valign=top>Accrued PTO: '.$accruedSickHr.' hrs</td>';
        $timeCardText = $timeCardText . '<tr><td colspan=9 valign=top>Used PTO: '.$usedSickHr.' hrs</td>';
	$timeCardText = $timeCardText . '<tr><td colspan=9 valign=top>Available PTO: '.$availableSickTime.' hrs</td>';
	//only show if admin
	if ($queryUser == 'finance') {
		$totPay = ($weekRegHour + $weekPtoHour + (1.5 * $weekOverHour)) * $hourlyRate;
		$timeCardText .= '<tr><td colspan=9 valign=top>'.$lineq['rateType'].' Rate: $'.$hourlyRate.'</td></tr><tr><td>';
		if($weekDriverAmt) { $timeCardText .= 'Driver Pay: '.format_money_symbol($this->business_id, '%.2n', $weekDriverAmt); } else { $timeCardText .= 'Hourly Pay: '.format_money_symbol($this->business_id,'%.2n', $totPay); }
		$timeCardText .= '</td>';
	}


	$timeCardText = $timeCardText . '</tr>';
	$timeCardText = $timeCardText . '</table>';
	$timeCardText = $timeCardText . '</table>';
	$timeCardText = $timeCardText . '<br><p>';

	if ($_REQUEST['summary'])
	{
		if ($currEmp and $currEmp <> $empEmail)
		{
			if($empTotalDriverAmt == 0) { $bold = "<strong>"; } else { $bold = ""; }
			$summaryData .=
				'<tr bgcolor="#C0C0C0">
				<td><strong>Total - '.$currEmp.'</strong></td>
				<td>'.$currSsn.'</td>
			 	<td align=right>'.$bold.number_format_locale($empRegular,2).'</strong></td>
				<td align=right>'.$bold.number_format_locale($empOt,2).'</strong></td>
				<td align=right>';
				if($empPto > 0) { $summaryData .= $bold.number_format_locale($empPto,2).'</strong>'; }
				$summaryData .= '</td></td>
				<td align=right>'.$bold.number_format_locale($empTotal,2).'</strong></td>
				<td align=right>';
				if($empTotalDriverAmt == 0) $summaryData .= $bold.format_money_symbol($this->business_id, '%.2n', $empTotalPay).'</strong>';
				$summaryData .= '</td><td align=right>';
				if($empTotalDriverAmt > 0) { $summaryData .= '<strong>'.format_money_symbol($this->business_id, '%.2n', $empTotalDriverAmt).'</strong>'; }
				$summaryData .= '</td>
				</tr>';
			$empRegular = 0;
			$empOt = 0;
			$empPto = 0;
			$empTotal = 0;
			$empBonus = 0;
			$empTotalPay = 0;
			$empTotalDriverAmt = 0;
            $ssn = '';
		}

		$summaryData .=
			'<tr>
			<td><a href="/admin/employees/manage/'.$currentEmp.'" target="_blank">'.$empName.'</a></td>
			<td>'.$weekOf.'</td>
		 	<td align=right>'.number_format_locale($weekRegHour,2).'</td>
			<td align=right>'.number_format_locale($weekOverHour,2).'</td>
			<td align=right>'.number_format_locale($weekPtoHour,2).'</td>';
			//<td align=right>'.number_format_locale($weekHolHour,2).'</td>
		$summaryData .= '<td align=right>'.number_format_locale($totHours,2).'</td>
			<td align=right>&nbsp;';
			if($lineq['rateType']  == 'Hourly') {$summaryData .= format_money_symbol($this->business_id, '%.2n', $totPay); }
			$summaryData .= '</td>
			<td align=right>'.format_money_symbol($this->business_id, '%.2n', $weekDriverAmt).'</td>
			<td>'.urldecode($cardNote).'</td>
			</tr>';
		$empRegular = $empRegular + $weekRegHour;
		$empOt = $empOt + $weekOverHour;
		$empPto = $empPto + $weekPtoHour;
		$empTotalDriverAmt += $weekDriverAmt;
		$empTotalPay += $totPay;
		$empTotal = $empTotal + $totHours;
		$empBonus = ($empBonus + $weekBonus + 29) * $linez['bonusRatio'];

		$currEmp = $empEmail;
        $currSsn = $ssn;

		if($empRole != 'Driver')
		{
			$reportRegular = $reportRegular + $weekRegHour;
			$reportOT = $reportOT + $weekOverHour;
			$reportPto = $reportPto + $weekPtoHour;
			$reportTotal = $reportTotal + $totHours;
			$reportPayTotal += $totPay;
			$bonusTotal = ($bonusTotal + $weekBonus + 29) * $linez['bonusRatio'];
		}
		$reportDriverAmt += $weekDriverAmt;
	}
	else
	{
		echo $timeCardText;
	}
}

if (isset($_REQUEST['summary']))
{
	echo '<table border="1" class="">
			<tr bgcolor="#00FFFF">
				<th><strong>Employee</strong></th>
				<th><strong>Week</strong></th>
				<td><strong>Regular</strong></td>
				<td><strong>OT</strong></td>
				<td><strong>PTO</strong></td>
				<td><strong>Total Hours</strong></td>
				<td><strong>Hourly Pay</strong></td>
				<td><strong>Per Item Pay</strong></td>
			</tr>'.$summaryData.'
			<tr bgcolor="#C0C0C0">
				<td><strong>Total - '.$currEmp.'</strong></td>
				<td>'.$currSsn.'</td>
			 	<td align=right><strong>'.number_format_locale($empRegular,2).'</strong></td>
				<td align=right><strong>'.number_format_locale($empOt,2).'</strong></td>
				<td align=right><strong>';
				if($empPto > 0) { echo number_format_locale($empPto,2); }
				echo '</strong></td>
				<td align=right><strong>'.number_format_locale($empTotal,2).'</strong></td>
				<td align=right><strong>'.format_money_symbol($this->business_id, '%.2n', $empTotalPay).'</strong></td>
				<td align=right>';
				if($empTotalDriverAmt > 0) { echo '<strong>'.format_money_symbol($this->business_id, '%.2n', $empTotalDriverAmt).'</strong>'; }
				echo '</td>
			</tr>
			<tr bgcolor="#FFFF00">
				<td></td><td></td>
				<td align=right>'.number_format_locale($reportRegular,2).'</td>
				<td align=right>'.number_format_locale($reportOT,2).'</td>
				<td align=right>'.number_format_locale($reportPto,2).'</td>';
				//<td></td>
				//<td></td>
				echo '<td align=right>'.number_format_locale($reportTotal,2).'</td>
				<td>'.format_money_symbol($this->business_id, '%.2n', $reportPayTotal).'</td>
				<td align=right>'.format_money_symbol($this->business_id, '%.2n', $reportDriverAmt).'</td>
			</tr>
			</table>';
}



?>



