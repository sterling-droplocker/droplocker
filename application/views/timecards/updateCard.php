<?
/**
 *  Purpose:    This page updates the status of the timecard
 *
 *  Use:		
 *
 *  Date    Sign    History
 *  ------  ----    -----------------------------------------------------------
 *  021014  ASL     Created.
 *	021104	ASL		Updated to stamp the record with who approved it
 *  
 */

$update1 = "UPDATE timeCard 
			SET status = '$newStatus',
				note = '$_REQUEST[note]'";
if($newStatus == 'Accepted By Finance')
{
	$dateFinance = date("Y-m-d H:i:s");
	$update1 = $update1.", dateFinance = '$_REQUEST[dateFinance]'";
}
if($newStatus == 'Approved By Manager')
{
	$dateApproved = date("Y-m-d H:i:s");
	$update1 = $update1.", dateApproved = '$dateApproved', approvedBy = '$user->id'";
}

$update1 = $update1." WHERE id = $_REQUEST[cardId];";
$updateStatus = $db->query($update1);

//get the manager (logged in user) info
$usrFname = $user->firstName;
$usrLname = $user->lastName;
$usrEmail = $user->email;
//get the info of the employee who's timecard was just updated
$query1 = "SELECT firstName, lastName, email, weekOf FROM customer, timeCard where customer.id = timeCard.customer_id and timeCard.id = $_REQUEST[cardId];";
$empInfo = $db->query($query1);
while ($line = $empInfo->fetchRow(DB_FETCHMODE_ASSOC)) 
{
	$empFname = $line['fName'];
	$empLname = $line['lName'];
	$empEmail = $line['email'];
	$cardWeekOf = $line['weekOf'];
}
?>





