<?
/**
 *  purpose:    this page displays all the timecards in waiting approval status so that "master" users can approve any timecard
 *				
 *	use:		in the database table called userRight, add an entry for each user that should have access to this page application_id=3 description=master
 *
 *  date    sign    history
 *  ------  ----    -----------------------------------------------------------
 *  021104  asl     created.
 *  
 */
 
$pageTitle = 'All time cards waiting approval';
$activeApp = '3';
require('intranetConfig.php');
require('/topNav.php');
require_once('empDetails.func');
?>
<br><p><table><tr><td>&nbsp;&nbsp;&nbsp;</td><td>

<?
  // Make sure the current user has master rights
  $query1 = "SELECT * FROM userRight WHERE application_id = '3' and description = 'master' and pniUser_id = '$activeEmp';";
  $masterUser = mysql_query($query1);
  if(mysql_num_rows($masterUser) == 0) //this person does not have finance rights
  {	
  	exit("Sorry you are not approved to use this feature");
  }
  
  //get all the timecards in Waiting Approval status
  $query3 = "SELECT * 
			FROM timeCard 
			WHERE status = 'Waiting Approval'
			ORDER BY pniUser_id, weekOf;"; 
  $timeCardRequest = mysql_query($query3);

  $queryName = $timeCardRequest;
  $queryUser = 'manager';
  include 'showTimeCard.php';

		
?>
<br><p>  
</td></tr></table>
<? require('footerNav.php');
 ?>