<?
/**
 *  Purpose:    This page lets finance view all of the timecards based on a bunch of parameters.
 *
 *  Use:		This is only used by finance
 *
 *  mm-dd-yy   	Sign    History
 *  --------  	----    -----------------------------------------------------------
 *  09-23-02  	ASL     Created.
 *	02-12-03  	ASL		Removed $empType from empDetails function.
 *	04-07-03	  ASL		I was using strtotime (Last Monday) to determine the week of, but PHP SUCKS! and that command shit the bed today!  
 *	04-18-03	  ASL		Replaced authorization with overall site authorization
 *	04-28-03	  ASL		Changed to only show active users.
 *  
 */


//checkRights($empInfo->customer_id, 'Timecard Management');

$getUser = "select * from employee where employeeID = ".$this->buser_id;
$query = $this->db->query($getUser);
$user = $query->result();
$empFname = $user[0]->firstName;
$empLname = $user[0]->lastName;
$empEmail = $user[0]->email;
$empId = $user[0]->employeeID;
        
$empName = getPersonName($user[0]);

?>
<table><tr><td>&nbsp;&nbsp;&nbsp;</td><td>

<?
  // If finance accepted the timecard
  if(isset($_POST['FinanceAccept']))
  {
  	$newStatus = "Accepted By Finance";
	include "updateCard.php";
	
	//Send an email to the employee 
	mail($empEmail, "Time Card Accepted By Finance", "$empName,\n\nYour time card for the week of $cardWeekOf has been Accepted By Finance.\n\nThis is a system generated message, please do not reply.", "From: {$currFname} {$currLname} <".$_SERVER['PHP_AUTH_USER']."@laundrylocker.com>");
  } 
  
  //Creating time card
  if(isset($_POST['employeeidInsert']) && isset($_POST['weekOfInsert']))
  {
    $weekOfInsert = $_POST['weekOfInsert'];
    $employeeidInsert = $_POST['employeeidInsert'];    
    
    $insertCard1 = "INSERT INTO timeCard (employee_id, weekOf, status) VALUES ('$employeeidInsert', '$weekOfInsert', 'Preliminary');";
    $getCard = $this->db->query($insertCard1);

    $getCard1 = "SELECT timeCardID FROM timeCard WHERE employee_id = '$activeUser' and weekOf = '$weekOfInsert';";
    $q = $this->db->query($getCard1);
    echo "Created time card for " . $weekOfInsert;
  }
  
  
  // If finance denied the timecard
  if(isset($_POST['FinanceDeny']))
  {
	$newStatus = "Rejected By Finance";
	include "updateCard.php";
	
	//Send an email to the employee 
	mail($empEmail, "Time Card Rejected By Finance", "$empName,\n\nYour time card for the week of $cardWeekOf has been Rejected by Finance.  Please go to $serverRoot/hr/timecard/timecard.php?weekOf=$cardWeekOf to resubmit this time card.\n\nThis is a system generated message, please do not reply.", "From: {$currFname} {$currLname} <".$_SERVER['PHP_AUTH_USER']."@laundrylocker.com>");
  } 
  
  
  //Get a list of all the employees in the same cleaner
  $sqlGetEmps = "SELECT * FROM `employee` 
				join business_employee on employee_id = employeeID
				where type='non exempt' 
				and business_id = $this->business_id
				ORDER BY firstName;";
  $query = $this->db->query($sqlGetEmps);
  $employees = $query->result();
  
  //If the user did a search for PTO requests
  if(isset($_POST['getRequest']) or $_GET['weekOf'])
  {
    if($_GET['weekOf'])
    {
        $_POST['employeeID'] = $_GET['employeeID'];
		$_POST['weekOf'] = $_GET['weekOf'];
		$_POST['status'] = $_GET['status'];
    }
    $getTimecards = "SELECT timeCard.*, employee.*
					    FROM timeCard 
					    join employee on employeeID = timeCard.employee_id 
                        join business_employee on business_employee.employee_id = employeeID
					    WHERE timeCard.employee_id like '$_POST[employeeID]' 
                        and business_id = $this->business_id
					    AND timeCard.weekOf like '$_POST[weekOf]' 
					    AND timeCard.status like '$_POST[status]' 
					    ORDER BY employee.firstName, weekOf";
	$query = $this->db->query($getTimecards);
  	$timeCardRequest = $query->result();
    
    $weekOf = $_POST['weekOf'];
  }
?>
<br>
<br>
<form action="/admin/employees/finance" method="post">
Find time card for:<br>
<select name="employeeID">
<option value="%" selected>-All Users-</option>
<?
foreach($employees as $e)
{	
	echo '<option value="'.$e->employeeID. '"';
	if ($_REQUEST['employeeID'] == $e->employeeID) { echo ' selected'; }
	echo '>'.getPersonName($e).'</option>'; 
}
?>
</select>

<br><p>
in the status of:<br>
<select name="status">
<option value="%">-All Statuses-</option>
<option value="Approved By Manager" <? if ($_GET['status']=="Approved By Manager") {echo 'selected'; } ?>>Approved By Manager</option>
<option value="Accepted By Finance" <? if ($_GET['status']=="Approved By Finance") {echo 'selected'; } ?>>Accepted By Finance</option>
<option value="Rejected By Finance" <? if ($_GET['status']=="Rejected By Finance") {echo 'selected'; } ?>>Rejected By Finance</option>
<option value="Preliminary" <? if ($_GET['status']=="Preliminary") {echo 'selected'; } ?>>Preliminary</option>
<option value="Waiting Approval" <? if ($_GET['status']=="Waiting Approval") {echo 'selected'; } ?>>Waiting Approval</option>
</select>
<br><p>
for the week of:<br>
<? 
	if(!isset($weekOf) or $_GET['weekOf'] == '%')
	{
                $startDay = get_business_meta($this->business_id, 'timecards_start_of_week', 'Mon');
		if(date("D") == $startDay)						//If today is Monday, this is the weekOf
			{ $weekOf = date("Y-m-d"); }
		else										//Otherwise keep going back one day until it finds Monday	
		{
			for ($i = 1; $i <= 7; $i++)
			{
				if(date("D", strtotime("-$i day")) == $startDay)
					{ $weekOf = date("Y-m-d", strtotime("-$i day")); }
			}
		}
	}
?>
<select name="weekOf">
<?    
	//	{ $weekOf = date("m/d/y", strtotime("last Monday")); }
	for ($i = -10; $i <= 10; $i++) {
		$iNew = $i * 7;
		$printWeek = date("m/d/y", strtotime("$weekOf $iNew day"));
		$valueWeek = date("Y-m-d", strtotime("$weekOf $iNew day"));
		if($iNew == '0')
		{
			echo '<option value="'.$valueWeek.'"';
			//if ($valueWeek == $_GET['weekOf']) {echo ' selected'; }
			//if (!$_GET['weekOf'] or $_GET['weekOf'] == '%' and $i=1) {echo ' selected'; }
			if ($weekOf == $valueWeek) {echo ' selected'; }
			if ((!$weekOf or $weekOf == '%') and $i=1) {echo ' selected'; }
			echo '>'.$printWeek.'</option>
			<option value="%">- All Weeks -</option>';
		}
		else
		{
			echo '<option value="'.$valueWeek.'"';
			if ($valueWeek == $_GET['weekOf']) {echo ' selected'; }
			echo '>'.$printWeek.'</option>';
		}
	}
?>
</select>
<br><br>
<input type="checkbox" name="summary" value="1">Show Summary
<select name="numWeeks">
<?
	if(!$_REQUEST['numWeeks']) { $_REQUEST['numWeeks'] = 2;}
	for ( $counter = 1; $counter <= 60; $counter += 1) {
		echo '<option value="'.$counter.'"'; if($_REQUEST['numWeeks'] == $counter) { echo 'SELECTED'; } echo '>'.$counter.' Weeks</option>';
	}
?>
</select><br> 
<input type="checkbox" name="bonus" value="1">Show Bonuses<br>
<input type="checkbox" name="contractor" value="1">Show Contractors<br>
<br>
<input type="submit" name="getRequest" value="Submit">
</form>

<?
//If the user did a search for time card requests
  if(isset($timeCardRequest))
  {
	echo '<hr>';
        

	
	//if summary, get the prior week too.
	if($_REQUEST['summary'])
	{
		if($_REQUEST['contractor'])
		{
		$query4 = "SELECT timeCard.*, firstName, lastName, email, position, ssn
	    			FROM timeCard
					join employee on employee.employeeID = timeCard.employee_id
                    join business_employee on business_employee.employee_id = employeeID
					WHERE timeCard.employee_id like '$_REQUEST[employeeID]' 
                    and business_id = $this->business_id
						AND timeCard.weekOf in ('$_REQUEST[weekOf]')
						AND timeCard.status like '$_REQUEST[status]'
						AND (employee.notes = 'Contractor')
						ORDER BY lastName, firstName, weekOf;"; 
		}
		else
		{
		for ( $counter = 1; $counter <= $_REQUEST['numWeeks'] -1; $counter += 1) {
			$toShow = $counter * 7;
			$weeks .= '\''.date("Y-m-d", strtotime("$_REQUEST[weekOf] -$toShow days")).'\', ';
		}
                $weeks = substr($weeks,0, -2);                
                $weeksWithRequest = "";
                if (!empty($weeks)) {
                    $weeksWithRequest = $weeks . ",";
                }
                $weeksWithRequest .= "'$_REQUEST[weekOf]'";
 		$query4 = "SELECT timeCard.*, firstName, lastName, email, position, ssn
	    			FROM timeCard
					join employee on employee.employeeID = timeCard.employee_id
                    join business_employee on business_employee.employee_id = employeeID
					WHERE timeCard.employee_id like '$_REQUEST[employeeID]' 
                    and business_id = $this->business_id
						AND timeCard.weekOf in ($weeksWithRequest)
						AND timeCard.status like '$_REQUEST[status]'
						AND (employee.notes != 'Contractor' or employee.notes is null)
						ORDER BY lastName, firstName, weekOf;"; 
		}
 		$query = $this->db->query($query4);
  		$timeCardRequest2 = $query->result();
		
		echo '<br><br>';
		$queryName = $timeCardRequest2;
		$queryUser = 'finance';
		include 'showTimeCard.php';
	}
	else
	{
		$queryName = $timeCardRequest;
		$queryUser = 'finance';
		include 'showTimeCard.php';
	}
  }
		
?>
<br><p> 
    
</td></tr></table>
