<script type="text/javascript">
function checkLunchChoice() {
  var sel = document.getElementById('lunch').value;
  if (sel == '') { alert('You must answer the lunch question'); return false; }
// remove one or more of the alerts if desired. 
}
</script>

<?php

function time_to_sec($time) {
    $hours = substr($time, 0, -6);
    $minutes = substr($time, -5, 2);
    $seconds = substr($time, -2);

    return $hours * 3600 + $minutes * 60 + $seconds;
}

/*  already declared in utility helper
 * function sec_to_time($seconds) {
    $hours = floor($seconds / 3600);
    $minutes = floor($seconds % 3600 / 60);
    $seconds = $seconds % 60;

    return sprintf("%d:%02d:%02d", $hours, $minutes, $seconds);
} */

echo '<div style="padding-left: 10px; padding-right: 10px; padding-bottom:300px;">';

if($this->uri->segment(5) == 'mgrEdit') { $_POST['mgrEdit'] = 1; }
if($this->uri->segment(4)) { $_POST['cardId'] = $this->uri->segment(4); }

/*if($timecard)
{
	if($this->uri->segment(5) == 'mgrEdit') { $_POST['mgrEdit'] = 1; }
	$_POST['cardId'] = $timecard[0]->timeCardID;
	$_POST['weekOf'] = $timecard[0]->weekOf;
}*/

if($_GET)
{
	$_POST['addLunch'] = $_GET['addLunch'];
	$_POST['day'] = $_GET['day'];
	$_POST['weekOf'] = $_GET['weekOf'];
	$_POST['mgrEdit'] = $_GET['mgrEdit'];
	$_POST['cardId'] = $_GET['cardId'];
}

$timeStamp = datestring_to_gmt("now");

if($_POST['mgrEdit'])
{	
	//if the manager is editing this card, find out who the employee is
	$sqlGetEmp = "select employeeID, firstName, lastName, timeCard.weekOf
				from employee
				inner join timeCard on timeCard.employee_id = employeeID
				where timeCardID=".$_POST['cardId'];
	$query = $this->db->query($sqlGetEmp);
  	$getEmp = $query->result();
	foreach($getEmp as $line)
	{ 
		$activeUser = $line->employeeID; 
		$empFname = $line->firstName;
		$empLname = $line->lastName;
		$passWeek = date("Y-m-d", strtotime($line->weekOf));
		$backTo = $line->employeeID; 
        $_POST['weekOf'] = $line->weekOf;
	}	
}
else
{
	$getUser = "select * from employee where employeeID = ".get_employee_id($this->buser_id);
	$query = $this->db->query($getUser);
	$user = $query->result();
		
	$activeUser = $user[0]->employeeID;
	$empFname = $user[0]->firstName;
	$empLname = $user[0]->lastName;
}

$empComplName = getPersonName(array('firstName' => $empFname, 'lastName' => $empLname));

if($_POST['noLunch'])
{
	$urlNotes = urlencode($_POST['note']);
	$update1 = "update timeCardDetail set note = '$urlNotes' where timeCardDetailID = $_POST[detailId]";
	$update = $this->db->query($update1);
}

  //require_once('empDetails.func');
  //include 'financeAdmin.func';

  //list ($empFname, $empLname, $empEmail) = empDetails($activeUser);
  //2-11-03 empType was removed from empDetails so we need to pull it out of the DB
  
  $getType1 = "SELECT type, defaultTask FROM employee WHERE employeeID = '$activeUser';";
  $query = $this->db->query($getType1);
  $line = $query->result();
  foreach($line as $l)
  { 
  	$empType = $l->type;
  	$defaultTask = $l->defaultTask;
  }
  
  if($_POST['defaultTime'])
  //If the user changed their default Time In and Time Out
  //update the database
  {
  	$t1 = date("H:i:s", strtotime($_POST['timeIn1'].$_POST['timeInDelim1']));
	$t2 = date("H:i:s", strtotime($_POST['timeOut1'].$_POST['timeOutDelim1']));
	$t3 = date("H:i:s", strtotime($_POST['timeIn2'].$_POST['timeInDelim2']));
	$t4 = date("H:i:s", strtotime($_POST['timeOut2'].$_POST['timeOutDelim2']));
	$update1 = "UPDATE employee SET time1='$t1', time2='$t2', time3='$t3', time4='$t4' WHERE customer_id=$activeUser;";
   	$changeTimes = mysql_query($update1);
  }
  
  if(!$_POST['weekOf'])
  {
    // Create server time object - this uses the business time zone,
    // assuring that it works correctly
    //$localTime = strtotime('now');

    //NOW we are using business timezone:
    $localTime = strtotime(convert_from_gmt_aprax('', "Y-m-d H:i:s", $business->businessID));

    $dayOfWeek = date("D",$localTime );
    $hourInDay = date("H",$localTime );    
    

    $defaultWeekStart = get_business_meta($this->business_id, 'timecards_start_of_week', 'Mon');    
    $lastEndOfWeek = date("d-m-Y", strtotime("last " . $defaultWeekStart, $localTime));
    $dayBeforeEndOfWeek = date("D",strtotime("-1 day", strtotime($lastEndOfWeek)) );


    if ($defaultWeekStart == 'Sun' and $dayOfWeek != $defaultWeekStart) {
        $weekOf = strtotime($defaultWeekStart . " last week",$localTime);
    } 
    else if($dayOfWeek == $defaultWeekStart and $hourInDay < 6){
    	//If it's defaultWeekStart-1day or defaultWeekStart before 6, it goes to last week's timecard
        $weekOf = strtotime("last " . $defaultWeekStart,$localTime);
    }else{
        //otherwise use this week timecard
        $weekOf = strtotime($defaultWeekStart . " this week",$localTime);
    }
    $weekOf  =  Date("Y-m-d", $weekOf);  
  }
  else 
  {
  	$weekOf = $_POST['weekOf'];
  }
  $weekOfInsert = date("Y-m-d", strtotime($weekOf));

  /* If the user has deleted a record */
  /*if(isset($_POST[delete]))
  {
    $delete1 = "DELETE FROM timeCardDetail WHERE id = '$_POST[delete]';";
	$deleteTime = $db->query($delete1);
  }*/
  
  /* If it should be from a prior day */
  if($_GET['prior'])
  {
  	//get the current time and day
	$sqlGetCard = "select timeInOut, day from timeCardDetail WHERE timeCardDetailID = '".$_GET['prior']."';";
	$q =  $this->db->query($sqlGetCard);
	$line = $q->result();
	 
	$minutes = time_to_sec($line[0]->timeInOut) / 60;
	//add 24 hours and set it back a day
	$minutes = $minutes + 1440;
	$time1 = sec_to_time($minutes * 60);
	$day1 = date("Y-m-d", strtotime($line[0]->day." -1 day"));
	
	$update1 = "update timeCardDetail set timeInOut = '$time1', day = '$day1' WHERE timeCardDetailID = '".$_GET['prior']."';"; 
	$this->db->query($update1);
  }
  
  
  /* If the user has added notes */
  if($_POST['addNotes'])
  {
  	$urlNotes = urlencode($_POST['addNotes']);
	$update1 = "update timeCard set note = '$urlNotes' where timeCardID = ".$_POST['cardId'].";";
	$this->db->query($update1);
  }
  
   /* If the user has added a lunch */
  if($_POST['addLunch'])
  {
		$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, ip) VALUES ('".$_POST['cardId']."', 'Regular', '13:00:00', 'Out', '".$_POST['day']."', '$timeStamp', '".$_SERVER['REMOTE_ADDR']."');";
		$this->db->query($insert1);
		$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, ip) VALUES ('".$_POST['cardId']."', 'Regular', '13:30:00', 'In', '".$_POST['day']."', '$timeStamp', '".$_SERVER['REMOTE_ADDR']."');";
	   	$this->db->query($insert1);
  }

  
  /* If the user has added a new record */
  if($_POST['Add'])
  {
	$getCard1 = "SELECT timeCardID FROM timeCard WHERE employee_id = '$activeUser' and weekOf = '$weekOfInsert';";
	//if there is not already a timecard
	$q = $this->db->query($getCard1);
	$numRowsRun = $getCard = $q->result();
	$numRows = count($numRowsRun);
	if ($numRows == 0)
	{
		//create one
		$insertCard1 = "INSERT INTO timeCard (employee_id, weekOf, status) VALUES ('$activeUser', '$weekOfInsert', 'Preliminary');";
   		$getCard = $this->db->query($insertCard1);
		
		$getCard1 = "SELECT timeCardID FROM timeCard WHERE employee_id = '$activeUser' and weekOf = '$weekOfInsert';";
		$q = $this->db->query($getCard1);
		$getCard = $q->result();
	}
	
	//now get the timeCard id
	foreach($getCard as $line) { $timeCard_id = $line->timeCardID; }
	
	//get the first letter of the postType and then decide what kind of time it is
	$p = $_POST['selectedPage']{18}; 
	switch ($p) {
		case 'R':
			$postType = 'Regular';
			break;
		case 'H':
			$postType = 'Holiday';
			break;
		case 'P':
			$postType = 'PTO';
			break;
		case 'S':
			$postType = 'Sick';
			break;
	}
	
	//if you put a * in time and set it to sick it cretes a whole sick day
	if($postType == 'Sick' and !$_POST['time1'])
	{
		$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, ip) VALUES ('".$_POST['cardId']."', 'Sick', '9:00:00', 'In', '".$_POST['day']."', '$timeStamp', '".$_SERVER['REMOTE_ADDR']."');";
		$q = $this->db->query($insert1);
		$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, ip) VALUES ('".$_POST['cardId']."', 'Sick', '17:00:00', 'Out', '".$_POST['day']."', '$timeStamp', '".$_SERVER['REMOTE_ADDR']."');";
	   	$q = $this->db->query($insert1);
	}
	else
	{
		if($_POST['clockIn'])
		{
			//insert the times
			$time1 = date("G:i:s");
			$day1 = date("Y-m-d");
			//$time1 = local_date($business[0]->timezone, 'now', "G:i:s");
			//$day1 = local_date($business[0]->timezone, '', "Y-m-d");
						
			$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, note, task, ip) VALUES ('$timeCard_id', 'Regular', '$time1', 'In', '$day1', '$timeStamp', '".addslashes($_POST['note'])."', '".$_POST['task']."', '".$_SERVER['REMOTE_ADDR']."');";
		   	$q = $this->db->query($insert1);
		}
		else if($_POST['clockOut'])
		{
			//insert the times
			$time1 = date("G:i:s");
			$day1 = date("Y-m-d");
			//$time1 = local_date($business[0]->timezone, '', "G:i:s");
			//$day1 = local_date($business[0]->timezone, '', "Y-m-d");
			
			//if they punch out before 7am, assume it is from the prior day
			$minutes = time_to_sec($time1) / 60;
			//if it's before 7am
			if($minutes < 420)
			{
				//add 24 hours and set it back a day
				$minutes = $minutes + 1440;
				$time1 = sec_to_time($minutes * 60);
				$day1 = date("Y-m-d", strtotime("$day1 -1 day"));
			}
			$note = addslashes($_POST['lunch'].' '.$_POST['note']);
			$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, note, task, ip) VALUES ('$timeCard_id', 'Regular', '$time1', 'Out', '$day1', '$timeStamp', '".$note."', '".$_POST['task']."', '".$_SERVER['REMOTE_ADDR']."');";
		   	$q = $this->db->query($insert1);
		}
		else
		{
			//insert manually entered times
			$time1 = date("H:i:s", strtotime($_POST['time1'] . $_POST['timeDelim']));
			
			if($_POST['inOut'] == 'Out')
			{
				//if they punch out before 7am, assume it is from the prior day
				$minutes = time_to_sec($time1) / 60;
				//if it's before 7am
				if($minutes < 420)
				{
					//add 24 hours and set it back a day
					$minutes = $minutes + 1440;
					$time1 = sec_to_time($minutes * 60);
					$_POST['day'] = date("Y-m-d", strtotime($_POST['day']." -1 day"));
				}
			}
			
			$insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, `in_out`, day, timeStamp, note, ip) VALUES ('$timeCard_id', '$postType', '$time1', '".$_POST['inOut']."', '".$_POST['day']."', '$timeStamp', '".addslashes($_POST['note'])."', '".$_SERVER['REMOTE_ADDR']."');";
		   	$q = $this->db->query($insert1);
		}
		//update default task
		$update1 = "update employee set defaultTask = '".$_POST['task']."' where employeeID = $activeUser";
		$updateTask = $this->db->query($update1);
		$defaultTask = $_POST['task'];
	}
  }

  // If the manager approved the timecard
  if($_POST['Approve'])
  {
  	$newStatus = "Approved By Manager";
	include "updateCard.php";
	
	//Send an email to the employee 
	mail($empEmail, "Time Card Approved By Manager", "$empComplName,\n\nYour time card for the week of $cardWeekOf has been approved by $usrFname $usrLname.\n\nThis is a system generated message, please do not reply.", "From: ".  getPersonName($line)." <".$_SERVER['PHP_AUTH_USER']."@laundrylocker.com>");
  	//Send an email to Finance
	mail("operations@laundrylocker.com", "Time Card Approved By Manager", "$empComplName has submitted a time card for the week of $cardWeekOf and it has been approved by $usrFname $usrLname.  Please go to $serverRoot/hr/timecard/finance.php to view this time card.\n\nThis is a system generated message, please do not reply.", "From: ". getPersonName($line)." <".$_SERVER['PHP_AUTH_USER']."@laundrylocker.com>");
  } 
  
  // if the manager denied the timecard
  if($_POST['Deny'])
  {
  	$newStatus = "Denied By Manager";
	include "updateCard.php";
	
	//Send an email to the employee 
	mail($empEmail, "Time Card Denied By Manager", "$empComplName,\n\nYour time card for the week of $cardWeekOf has been denied by $usrFname $usrLname.\n\nReason: ".$_POST['note']."\n\nPlease go to $serverRoot/hr/timecard/timecard.php?weekOf=$cardWeekOf to resubmit this time card.\n\nThis is a system generated message, please do not reply.", "From: ".getPersonName($line)." <".$_SERVER['PHP_AUTH_USER']."@laundrylocker.com>");
  } 
  
  //Get all of the PTO that the manager has to approve
  $mgrQuery = "select timeCardID, timeCard.note, timeCard.employee_id, timeCard.status, timeCard.weekOf, employee.firstName, employee.lastName, employee.email
			from timeCard
			join employee on employeeID = timeCard.employee_id
			where employee.manager_id = $activeUser and
			timeCard.status = 'Waiting Approval'
			ORDER BY timeCard.weekOf;";
  $query = $this->db->query($mgrQuery);
  $getMgrPto = $query->result();
  
  // create an array that has all of the days of the week
  for ($i = 0; $i <= 7; $i++) {
    $tempWeek[$i] = date("Y-n-j", strtotime("$weekOf +$i day"));
  }
  
  //Get all the timeCard data for the current user in the selected week
  $sqlEmpQuery = "SELECT timeCardID, status, employee_id, weekOf, note, employee.firstName, employee.lastName, employee.email
			FROM timeCard
			join employee on employeeID = timeCard.employee_id
			WHERE timeCard.weekOf = '$weekOfInsert'
			and timeCard.employee_id = $activeUser;";
  $query = $this->db->query($sqlEmpQuery);
  $getEmpPto = $statusValue = $query->result();

  
  //Get the employees default TimeIn and Time Out
  $timeQuery = "SELECT time1, time2, time3, time4
			FROM employee
			WHERE employeeID = $activeUser;";
  $getTimes = $this->db->query($timeQuery);
  $getTimesResults = $getTimes->result();
  
  //Find out if this person is a super user
  //$masterQuery = "SELECT * FROM userRight WHERE application_id = '3' and description = 'master' and pniUser_id = '$activeUser';";
  //$masterUser = mysql_query($masterQuery);

?>

<div style="position: relative; top:300px;">
<?php
	//If the employee has already entered some data, show it.
	if (count($getTimesResults) != 0) 
	{	
		$queryName = $getEmpPto;
		$queryUser = 'delivery';
		include 'showTimeCard.php';
	}
	
	//If the manager has some PTO to approve, show it.
	if (count($getMgrPto) != 0) 
	{	
		print "<br><p><b><u>Time Cards for $empComplName to Approve:</u></b><br><p>";
		$queryName = $getMgrPto;
		$queryUser = 'manager';
		include 'showTimeCard.php';
	}
	
	//If there are no timecards for the manager to approve
	if (!strstr($empType,'non exempt'))
	{
		echo '<br><br><a href="/admin/employees/finance">Click here</a> for a finance view.<br>
			  <br>
			  ';
	}

?>
</div>
<div style="position: absolute; top:50px; left:290px;">
	<?php
	if($_POST['mgrEdit'])
		{ echo '<a href="/admin/employees/finance?weekOf='.$passWeek.'&getRequest=1&employeeID=%&status=%#'.$backTo.'"><- back to finance view</a><br><br>';}	

	//If this employee is non exempt, meaning they need to do time cards, show them the timecard entry screens
	if (strstr($empType, 'non exempt') or $_POST['mgrEdit'])
	{
		print "<b><u>Time Card for: <a href='/admin/employees/manage/".$activeUser."' target='_blank'>$empComplName</a></u></b><br><br>";
	}
	echo '<a href="/admin/employees/timeoff">Time off request form</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/admin/admin/tickets">Submit a maintenance request</a><br><br>'; ?>
	
	<form action="" method="post">
					type: <?= $empType ?>

	<b>Week Of&nbsp;</b>
	<select name="weekOf">
	<?php
		for ($i = -3; $i <= 3; $i++) {
			$iNew = $i * 7;
			$thisWeek = date("m/d/y", strtotime("$weekOf $iNew day"));
			if($iNew == '0')
			{
				print "<option value=$thisWeek SELECTED>$thisWeek</option>";
			}
			else
			{
				print "<option value=$thisWeek>$thisWeek</option>";
			}
		}
	?>
	</select>
	&nbsp;
	<input type="submit" name="changeWeek" value="Change">
	</form>

	<?php
		//$statusValue = $getStatus->fetchRow(DB_FETCHMODE_ASSOC);
		if( $statusValue['status'] == '' or $statusValue['status'] == 'Preliminary' or $statusValue['status'] == 'Denied By Manager' or $statusValue['status'] == 'Rejected By Finance' or $_POST['mgrEdit'])
		{ 	?>
			<table border='0' cellspacing='1' cellpadding='5'>
			<tr>
				<td colspan='8'><hr></td>
			</tr>
			<form action='/admin/employees/timecard' method='post' name="punchCard">
			<?php
			echo '<input type="hidden" name="weekOf" value="'.$weekOf.'">';
			if ($_POST['mgrEdit'] or $empType == 'non exempt edit')
			{
			if ($_POST['mgrEdit'])
					{ echo '<input type="hidden" name="mgrEdit" value="1">'; }
			echo '<input type="hidden" name="Add" value="1">
				 <input type="hidden" name="cardId" value="'.$_POST['cardId'].'">';
			?>
			<tr>
				<td valign=top>
					<table>
						<tr>
							<td><b><small>Type</small></b></td>							
							<td><b><small>Day</small></b></td>
						</tr>
						<tr>
							<td valign=top>
							<?php
							  print "<select name='selectedPage' onChange='changePage(this.form.selectedPage)'>";
							  print "<option value = 'timecard.php?type=Regular&weekOf=$weekOf'";
							    if($_GET['type'] == 'Regular') { print 'selected'; } 
							  print "> Regular </option>";
							  print "<option value = 'timecard.php?type=Holiday&weekOf=$weekOf'";
							    if($_GET['type'] == 'Holiday') { print 'selected'; } 
							  print "> Holiday </option>";
							  print "<option value = 'timecard.php?type=PTO&weekOf=$weekOf'";
							    if($_GET['type'] == 'PTO') { print 'selected'; } 
							  print "> PTO </option>";
							  print "<option value = 'timecard.php?type=Sick&weekOf=$weekOf'";
							    if($_GET['type'] == 'Sick') { print 'selected'; } 
							  print "> Sick </option>";
							  print "</select>";
							 ?>
							</td>
							<td valign=top>
								<?php
								print "<select name='day'>";
								for ($i = 0; $i <= 6; $i++) 
								{
									$thisDay = date("m/d - D", strtotime("$weekOf +$i day"));
									$thisMDY = date("Y-m-d", strtotime("$weekOf +$i day"));
									
									//find out if there is a holiday entry in the corporate calendar
									//$query12 = "SELECT * FROM corpCalendar WHERE eventDate = '$thisMDY' and eventType = 'Holiday';";
									//$getEvents = mysql_query($query12);
									
									print "<option value=$thisMDY";
									//Make today the selected day
									if($thisMDY == local_date($business->timezone, '', "Y-m-d"))
										{ print " SELECTED "; }
									print ">$thisDay";
									/*while ($line = mysql_fetch_row($getEvents)) 
									{
										print " - HOLIDAY";
									}*/
									print "</option>";
								}
								print "</select>";
								?>
							</td>
						</tr>
					</table>
				</td>
				<td valign=top>
					<?php
					if($_GET['type'] == 'Holiday' or $_GET['type'] == 'PTO')
					{
					?>
					<table>
						<tr>
							<td><b><small>Hours</small></b></td>
							<td><b><small>Note</small></b></td>
						</tr>
						<tr>
							<td><input type='text' name='hour' size='6'></td>
							<td><input type='text' name='note' size='30'></td>
						</tr>
					</table>
					<?php
					}
					else
					{
					?>
					<table>
						<tr>
							<td colspan='2'><b><small>Time</small></b></td>
							<td><b><small>In / Out</small></b></td>
							<td><b><small>Notes</small></b></td>
						</tr>
						<tr>
						<?php
							echo '<td><input type="text" name="time1" size="6"></td>
								<td><select name="timeDelim">
								<option value="am">am</option>
								<option value="pm">pm</option>
								</select>
								</td>
								<td><select name="inOut">
								<option value="In" SELECTED>Clock In</option>
								<option value="Out">Clock Out</option>
								</select></td>
								<td><input type="text" name="note" size="30"></td>
							</tr>
							<tr>';
							
							?>
						</tr>
					</table>
					</td>
					<tr>
						<td colspan=2 align=right><input type='submit' name='Add' value='Add Record'></td>
					</tr>
					<?php }
			}
			else if ($empType == 'non exempt') 
			{
				//if they had less than 4 clocks the prior day, ask them why
				//var_dump($lastCount);
				if($lastCount and $lastCount['punches'] < 4 and !$lastCount['note'])
				{
					echo '<td>Why did you not take lunch on '.date("D m/d", strtotime($lastCount['day'])).'? 
							<input type="text" name="note" size="30">
							<input type="hidden" name="detailId" value="'.$lastCount['detailId'].'">
							<input type="submit" name="noLunch" value="Update">
							</td>';
				}
				else
				{	
					echo '<input type="hidden" name="Add" value="1">
					<tr>
					<td>Task: <select name="task">
						<option value="'.$defaultTask.'" selected>'.$defaultTask.'</option>
						<option value="Driving">Driving</option>
						<option value="MarkIn">Mark In</option>
						<option value="Assembly">Assembly</option>
						<option value="Pressing">Pressing</option>
						<option value="Sheets">Sheets</option>
						<option value="Support">Support</option>
					</select></td>
					<td><input type="submit" name="clockIn" value="Clock In"></td>	
                    <td>&nbsp;&nbsp;&nbsp;<select name="lunch" id="lunch">
						<option value="" selected>Did you take lunch today?</option>
						<option value="Taking it now">Taking it now</option>
						<option value="Willingly did not take lunch">Willingly did not take lunch</option>
                        <option value="Yes, I took lunch">Yes, I took lunch</option>
                        </select>		
					<td><input type="submit" name="clockOut" value="Clock Out" onclick="return checkLunchChoice()"></td>
					</tr>';
				}
			}
					?>
			</form>
			<tr>
				<td colspan='8'><hr></td>
			</tr>
			</table>
<?php
		}  //close the status if statement
echo '</div></td></tr></table><br><br></div>';

?>

