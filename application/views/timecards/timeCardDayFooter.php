<?
/**
 *  Purpose:    This file prints the footer for each day in a timecard.  It is called multiple times so it is better to break it out into its own section of code
 *
 *	Use:		This file is called from showTimeCard.php
 *
 *  Date    Sign    History
 *  ------  ----    -----------------------------------------------------------
 *  101102  ASL     Created.
 *	030503	ASL		Make it so that 10 hours is the overtime barrier for Levi
 *
 */


$this->load->library('production_metrics');
//require_once('../reporting/pcsPerHr.func.php');
//include('../timecard/driverData.php');

//convert week hours to minutes
$weekRegMin = $weekRegHour * 60;
//add the hours for this day to the hours for this week
$weekRegMin = $weekRegMin + $dayRegMin;




$dayOtMin = 480;

//For some employees, all Saturdays are OT
/*if(date('D', strtotime($currentDay)) == 'Sat' and $satOt == 1)
{
	$dayOverMin = $dayRegMin;
	$dayRegMin = 0;
}*/


if($dayPtoHour <> 0)
{
	$dayRegMin = 0;
}
//if it's more than 8 hours in a day or over 40 total for the week, then it is overtime
else if($overtime == 1) {
    if($dayRegMin > $dayOtMin or ($weekRegMin - $weekPtoHour) > 2400)
    {
    	//echo 'dayRegMin = '.minToHour($dayRegMin).' weekRegMin = '.minToHour($weekRegMin).'<br>';
    	if(($weekRegMin - $weekPtoHour) > 2400)// if this is overtime from over 40 hours
    	{
            if(($weekRegMin - $dayRegMin) > 2400)
            {
                $dayOverMin = $dayRegMin;
                $dayRegMin = 0;
            }
            else
            {
                $dayOverMin = $weekRegMin - 2400;
                $dayRegMin = $dayRegMin - $dayOverMin;
            }
            
            if ($dayRegMin > $dayOtMin)
            {
                //Fixing case when last day has overtime and previous 4 days regular < 32 hours
                $dayOverMin += $dayRegMin - $dayOtMin;
                $dayRegMin = $dayOtMin;
            }                
    	}
    	else
    	{
            $dayOverMin = $dayRegMin - $dayOtMin;
            $dayRegMin = $dayOtMin;
    	}
    }
}

// Convert the minutes to hours, with proper rounding
$dayRegHour = minToHour($dayRegMin);
$dayOverHour = minToHour($dayOverMin);
$dayPtoHour = minToHour($dayPtoHour);

$weekRegHour = $weekRegHour + $dayRegHour;
$weekOverHour = $weekOverHour + $dayOverHour;
$weekPtoHour = $weekPtoHour + $dayPtoHour;
$weekHolHour = $weekHolHour + $dayHolHour;
if($_REQUEST['bonus'])
{
	$bonusAmt = 0;
	$eligable =	$this->production_metrics->getEligability($currentDay, $currentEmp);
	if($eligable == 1)
	{
		$bonus = $this->production_metrics->getPcsPerHr($currentDay);
		if ($linez['bonusRatio'])
			{ $bonusAmt = round($bonus['pcs']/$bonus['hrs'],2); } //* $linez['bonusRatio']; }
		else
			{ $bonusAmt = round($bonus['pcs']/$bonus['hrs'],2); }
	}
}
else { $bonusAmt = 0; }
$weekBonus = $weekBonus + $bonusAmt;

if($empRole == 'Driver')
{
	//get payment rate
/*
	$select2 = "select value from settings where setting = 'regPickupRate'";
	$data = $db->query($select2);
	$rate = $data->fetchRow(DB_FETCHMODE_ASSOC);
	$regPickupRate = $rate['value'];
	$select2 = "select value from settings where setting = 'extraPickupRate'";
	$data = $db->query($select2);
	$rate = $data->fetchRow(DB_FETCHMODE_ASSOC);
	extraPickupRate = $rate['value'];
*/

	$regPickupRate = $hourlyRate;
	$extraPickupRate = '1.75'; //hardcoded
	//$extraPickupRate = $hourlyRate * 1.2; //20% bonus

	$transactions = getStops($currentDay, $currentEmp);
	$driverHours = getHours($currentDay, $currentEmp);
	if(strtotime($driverHours['startTime']) < strtotime("8:46 am"))
	{ $totTrans = ($transactions['pickup'] + $transactions['delivery']) * $extraPickupRate; }
	else
	{ $totTrans = ($transactions['pickup'] + $transactions['delivery']) * $regPickupRate; }
	if($dayPtoHour <> 0)
	{
		$weekDriverAmt += $dayPtoHour * 15;
	}
	else
	{
		$weekDriverAmt += $totTrans;
	}
}

if (!$_REQUEST['summary'])
{
	if($punchCounter < 4) { $timeCardText .=  '<tr bgcolor="#FF0000">'; }
	else {  $timeCardText .=  '<tr>'; }
	if($empRole == 'Driver')
	{
		if($dayPtoHour <> 0)
		{
			$timeCardText .= '<td align=left colspan=3>Sick Pay: $'. round($dayPtoHour * 15,2) .'</td>';
		}
		else
		{
			$timeCardText .= '<td align=left colspan=3>Pickups: '.$transactions['pickup'].' / Deliveries: '.$transactions['delivery'].' @ '.format_money_symbol($this->business->businessID, '%.2n', $totTrans/($transactions['delivery'] + $transactions['pickup'])).' = $'. $totTrans .'</td>';
		}
	}
	else
	{
		$timeCardText .= '<td align=left colspan=3>&nbsp;</td>';
	}
	$timeCardText .= '<td align=right>'.round($dayRegHour,2).'</td>
	<td align=right>'.round($dayOverHour,2).'</td>
	<td align=right>'.round($dayPtoHour,2).'</td>';
	$timeCardText = $timeCardText . '<td>&nbsp;</td><td><small>';
	if ($_POST['mgrEdit'] == 1)
	{ $timeCardText = $timeCardText . '<a href="/admin/employees/timecard?addLunch=1&day='.$currentDay.'&weekOf='.$weekOf.'&mgrEdit=1&cardId='.$cardId.'">add lunch</a>'; }
	$timeCardText = $timeCardText . '</small></td><td> $'.round($bonusAmt,2).'</td>
	</tr>';
}
?>

