<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/howitworks/hero-locker-loot.jpg" class="howto_hero_pic" />
            <h1>Locker Loot&trade;</h1>
             <div class="visible-phone" style="text-align: center">
                <?= howNav('lockerloot'); ?>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-order.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">1. Create an account</h2>
                        <p class="bodyBig concierge_p">
                           For every new customer you refer to Laundry Locker that uses your referral code, you will receive up to $50.00. When you create a Laundry Locker account you are automatically signed up for Locker Loot&trade;.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-locker-loot-refer.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">2. Refer a friend</h2>
                        <p class="bodyBig concierge_p">
                            You can find your personalized Locker Loot&trade; referral code on your Locker Loot&trade; account page. Your code also gives your friend 25% off their first order!
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-locker-loot.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">3. Get $50</h2>
                        <p class="bodyBig concierge_p">
                            Get paid for every order. Along with our Refer a Friend $50 program, all Laundry Locker customers receive $1 in Locker Loot for every $100 they spend with us.
                        </p>
                    </div>
                </div>
            </div>


        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= howNav('lockerloot'); ?>
            </div>
        </div>
    </div>
</div>