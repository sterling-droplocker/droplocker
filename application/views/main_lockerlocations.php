<div class="fluid-container">

    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/howitworks/hero-locations.jpg" class="howto_hero_pic" />

            <h1>Locker Locations</h1>
            <div class="visible-phone" style="text-align: center">
                <?= howNav('lockerlocations'); ?>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-location-drop.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">1. Drop</h2>
                        <p class="bodyBig concierge_p">
                            Place your laundry in any unlocked locker. Enter any 4-digit code to lock the locker.

                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-order.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">2. Order</h2>
                        <p class="bodyBig concierge_p">
                            Place an order via our 
                            <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.laundrylocker" class="green" target="_blank">Android app</a>
                            or <a href="http://laundrylocker.loc/images/iphoneApp.png" class="green" target="_blank">iPhone app</a>
                            or on our <a href="/register" class="green" target="_blank">website</a>.
                            Select your address and locker number.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-location-pickup.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">3. Pickup</h2>
                        <p class="bodyBig concierge_p">
                            When your order is ready to be picked up, we will send you a notification via the app, SMS, or email.<br />We'll send you the locker number and code to open the locker and retrieve your clean order. Enjoy!
                        </p>
                    </div>
                </div>
            </div>
           
            
        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= howNav('lockerlocations'); ?>
            </div>
            <div class="calloutBanner calloutRight" style="height: auto; margin-top: 20px">
                <h3>Important</h3><br />

                <p class="bodyBigGray">


                    1. Checkout the location page for hours of operation and site-specific details. <br /><br />

                    2. First time user? Grab a bag from the front desk or use any bag with your name, phone number and email information inside.<br /><br />

                    3. We will return your order with a reusable garment bag for all future orders.<br /><br />

                    4. Special requests? Need a hem sewn? Include these notes on the order screen... we'll take care of them.<br /><br />

                </p>
            </div>
        </div>
    </div>
</div>