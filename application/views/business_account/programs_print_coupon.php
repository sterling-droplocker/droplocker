<?php
header('Content-Type: text/html;charset=utf-8');

$business_url = $business->website_url;
if( empty( $business_url ) ){
    $business_url =  $_SERVER["HTTP_HOST"];
}

ob_start();
?>

<table width="400" cellspacing="0" cellpadding="4" style="border: dashed; border-color: Black; border-width: 2px; margin: 0 auto;">
<tr>
<td align="center" style="background-color: #669ACC; font-size: 10pt; font-family: Verdana; color: #FFF;">

<span class="whiteLink"><br>
Would you like to have your dry cleaning and wash &amp; fold picked up and delivered to your apartment building or office?
<br>
<br>
Then you've got to try %business_companyName%!
<br>
<br>
<br>
<font size="+1"><strong>Public locations are now available.</strong></font><br>
Visit http://%business_url% for details.
<br>
<br>
<font size="+1"><strong>Enter promotional code: %promo_code% </strong></font><br>
<br>
<span style="font-size: 10pt; font-family: Verdana; color: #FFF;">and receive</span>
<br>
<br>
<span style="font-size: 40pt; font-family: Verdana; color: #FFF; font-weight: bold;">%amount% Off</span>
<br>
<table width="320" border="0" align="center">
<tr>
<td align="center" style="background-color: #669ACC; font-size: 10pt; font-family: Verdana; color: #FFF;">
(Recieve %amount% off your first order by entering <font size="+1"><strong>promotional code: %promo_code%</strong></font>
 when you register as a first time customer at %business_url%.
 Maximum discount %max_discount%.)<br><br>
<span style="font-size: 8pt; font-family: Verdana; color: #FFF;">Offer only valid on first order.<br>May not be combined with any other offer.</span>
</td>
</tr>
</table>
<br><br>
<table width="100%" border="0" cellpadding="2">
<tr>
<td style="font-size: 10pt; font-family: Verdana; color: #FFF; font-weight: bold;">%business_website%</td>
<td align="right" style="font-size: 10pt; font-family: Verdana; color: #FFF; font-weight: bold;">%business_phone%</td>
</tr>
</table>
</span>
</td>
</tr>



</center>
<?php

$html = ob_get_clean();

echo get_translation_for_view('cupons_html', $html, array(
    'business_url' => $business_url,
    'business_companyName' => $business->companyName,
    'business_website' => $business->website,
    'business_phone' => $business->phone,
    'business_city' => $business->city,
    'promo_code' => $promo_code,
    "amount" => number_format_locale($coupon_amount, get_business_meta($business->businessID, 'decimal_positions', 2)).'%',
    'max_discount' => format_money_symbol($business->businessID, '%.2n', $coupon_maxAmt),
));

?>
