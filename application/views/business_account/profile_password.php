<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li>
      <a href="/account/profile/edit"><?=get_translation_for_view("profile", "Profile")?></a>  <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("update_password", "Update Password")?>
  </li>
</ul>

<h2 class='page-header'><?=get_translation_for_view("update_password", "Update Account Password")?></h2>
<?= $this->session->flashdata('alert');?>

<form class='form-horizontal' action='' method="post">
    <div class="control-group">
      <label class="control-label" for="new_password"><?=get_translation_for_view("new_password", "New Password")?> </label>
      <div class="controls">
        <input type="password" id="new_password" name='new_password' />
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="new_password_again"> <?=get_translation_for_view("confirm", "New Password Confirm")?> </label>
      <div class="controls">
        <input type="password" id="new_password_again" name='new_password_again' />
      </div>
    </div>
    <br />
    <?= form_submit(array("value" => get_translation_for_view("update_password", "Update Account Password"), "name" => "submit", "class" => "btn btn-primary")) ?>
</form>
	
