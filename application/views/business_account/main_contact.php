<?php

$fieldList = array();
$fieldList['address1'] = (!empty($this->business->address)) ? "<div>{$this->business->address}</div>" : '';
$fieldList['address2'] = (!empty($this->business->address2)) ? "<div>{$this->business->address2}</div>" : '';
$fieldList['city'] = (!empty($this->business->city)) ? "<div>{$this->business->city}</div>" : '';
$fieldList['state'] = (!empty($this->business->state)) ? "<div>{$this->business->state}</div>" : '';
$fieldList['zip'] = (!empty($this->business->zip)) ? "<div>{$this->business->zip}</div>" : '';

$formatedAddress = getSortedAddressForm($fieldList);
        
?>
<div class='row'>
    <div class="span10 offset1">
        <div>
            <h2>Contact <?= $this->business->companyName;?></h2>
        </div>
        
        <?
        $support_email = get_business_meta($this->business_id,'customerSupport');
        ?>
        <? if( !empty( $support_email ) ){ ?>
        <div>
            Customer Support: <a href="mailto:<?= $support_email;?>"><?= $support_email;?></a>
        </div>
        <? } ?>
        
        <?= $formatedAddress ?>
        
        <?if(!empty($this->business->phone)){?>
        <div>
            <?= $this->business->phone;?>
        </div>
        <?}?>
    </div>
</div>
