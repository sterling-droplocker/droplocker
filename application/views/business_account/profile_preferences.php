<?php
//var_dump($modulePreferencess); die();
?>
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("preferences", "Preferences")?>
  </li>
</ul>

<div class='hidden-phone'>
    <h2 class='page-header'><?=get_translation_for_view("settings", "Settings")?></h2>
    <?= $this->session->flashdata('alert'); ?>
    <? //tab_helper('laundry'); ?>
</div>

<div class='visible-phone'>
    <h2 class='page-header'><?=get_translation_for_view("laundry_preferences", "My Laundry Preferences")?></h2>
    <?= $this->session->flashdata('alert'); ?>
</div>

<div>

    <?= $this->session->flashdata('message')?>

	<form class="form form-horizontal" action='' method='post'>

    <? foreach($businessPreferences as $title => $modulePreferences): ?>
        <h3><?= get_translation($title, "service_types", array(), $title)?></h3>
        <div class='well'>
            <? foreach($modulePreferences as $preferenceSlug => $preference):
                if (count($preference['attr']) <= 1) {
                    continue;
                }
                $selected = isset($customerPreferences[$preferenceSlug]) ? $customerPreferences[$preferenceSlug] : null;
            ?>
            <div class='control-group'>
                <label class='control-label' for='select'><?= $preference['text']?></label>
                <div class='controls'><?= form_dropdown("preferences[{$preferenceSlug}]", $preference['options'], $selected, "id='{$preferenceSlug}'"); ?></div>
            </div>
            <? endforeach; ?>
        </div>
    <? endforeach; ?>

	<br>
	<h3><?=get_translation_for_view("account_notes", "Account Notes")?></h3>
	<div><?=get_translation_for_view("notes_explanation", "Account notes are added to every order. You will also be able to provide additional notes while placing orders")?></div>
	<textarea style='width:99%;height:100px;border:1px solid #ccc;font-size:1em;' name='notes'><?= $customer_notes?></textarea>
	<br />
	<input type='submit' class='btn btn-primary' value='<?= get_translation_for_view("submit_button", "Submit Changes") ?>' name='submit' />
	</form>
</div>
