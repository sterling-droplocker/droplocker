<h2>
	<?=get_translation_for_view("plan_buy_error", "Please, cancel your '%plan%' plan before", array(
		"plan" => $plan->description
	))?>
</h2>
<p>
	<div style='float:left; width:50%'>
	    <table class="table table-bordered">
		<tr>
			<th><?=get_translation_for_view("plan_name", "Plan Name")?></th>
			<td><?= $plan->description?></td>
		</tr>
		</table>
	</div>

	<div style="float:left; width:auto; margin-left:10px;">
		<a href="/account" class='btn btn-success btn-large'><?= get_translation_for_view("back_to_account", "Back To My Account") ?></a>
		<?php if ($plan->type == "laundry_plan"): ?>
	    	<a href="/main/plans" class='btn btn-success btn-large'><?= get_translation_for_view("back_to_laundry_plans", "View my Laundry Plans") ?></a>
		<?php else: ?>
	    	<a href="/main/everything_plans" class='btn btn-success btn-large'><?= get_translation_for_view("back_to_everything_plans", "View my everything Plans") ?></a>
		<?php endif; ?>
	</div>
	<br style='clear:left'>
</p>
