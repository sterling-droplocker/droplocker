<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li>
      <a href="/account/profile/edit"><?=get_translation_for_view("profile", "Profile")?></a>  <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("update_email", "Update Email Address")?>
  </li>
</ul>

<h2 class='page-header'><?=get_translation_for_view("update_email", "Update Email Address")?></h2>
<?= $this->session->flashdata('alert');?>

<p><?=get_translation_for_view("confirmation", "A confirmation email will be sent to the new email address. The email will not be changed until the confirmation email is verified")?></p>
			
<?php echo validation_errors(); ?>

    <table class="table table-striped table-bordered table-condensed">
        <tr>
            <th><?=get_translation_for_view("current_email", "Current Email")?></th>
            <td><?= $customer->email?></td>
        </tr>    
    </table> 
<form action='' class='form-inline well' method="post">
    <div class="control-group">
    
    <? if($bouncedEmail):?>
            <div class='alert alert-error'><?=get_translation_for_view("email_bounce", "We are not sending emails to this email address due to excessive bounces. Either confirm that this email is working properly or update your email below.")?>
            <br><br><?=get_translation_for_view("email_working", "Email Working:")?> <a href='/account/profile/removeBounceBlock/' class='btn btn-danger' style='color:#fff'><?=get_translation_for_view("remove_block", "Remove the Block")?></a>
            </div>
            <? endif; ?>
    
      <label class="control-label" for="new_email"><?=get_translation_for_view("new_email", "New Email")?></label>
      <div class="controls">
        <input type="text" id="new_email" name='new_email' />
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"></label>
      <div class="controls">
         <input type="submit" value='<?=get_translation_for_view("update_email_button", "Update Email") ?>' name="submit" class="btn btn-primary" />
      </div>
    </div>
</form>

<form action='' class='form-inline well' method="post">
<h3><?=get_translation_for_view("settings", "Email Delivery Settings")?></h3>
    <div class="control-group">
      <label class="control-label" for="new_email"><?=get_translation_for_view("set_times", "Set the times you would like to receive notifications.")?></label>
      <div class="controls">
        <div id='slider'></div>
        <span id="time"></span>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label"></label>
      <div class="controls">
          <?= form_submit(array("value" => get_translation_for_view("update_delivery_times_button", "Update Delivery Times"), "class" => "btn btn-primary", "name" => "submit")) ?>
      </div>
    </div>
</form>

   
    <br />
    <? if($customer->pendingEmailUpdate): 
        $p = unserialize($customer->pendingEmailUpdate);
    ?>
    
    <h3><?=get_translation_for_view("pending", "Your currently have a pending email request.")?></h3>
    <table class="table table-striped table-bordered table-condensed">
        <tr>
            <th><?=get_translation_for_view("pending_email", "Pending Email")?></th>
            <td><?= $p['email']?></td>
        </tr>
        <tr>
            <th><?=get_translation_for_view("request_date", "Request Date")?></th>
            <td><?= date("F j, Y", strtotime($p['date']))?></td>
        </tr>
    </table>
    <a class='btn btn-danger' href='/account/profile/delete_pending_email'><?=get_translation_for_view("delete", "Delete")?></a>
    <? endif ?>
    

<script>
   
        $('#slider').slider(
                { range: true,
                    min: 0,
                    max: 1439,
                    values: [0, 1439],
                    slide: slideTime,
                    step:1
                }
         );

        function slideTime(event, ui){
            var val0 = $("#slider").slider("values", 0),
                val1 = $("#slider").slider("values", 1),
                minutes0 = parseInt(val0 % 60, 10),
                hours0 = parseInt(val0 / 60 % 24, 10),
                minutes1 = parseInt(val1 % 60, 10),
                hours1 = parseInt(val1 / 60 % 24, 10);
            startTime = getTime(hours0, minutes0);
            endTime = getTime(hours1, minutes1);
            $("#time").text(startTime + ' - ' + endTime);
        }
        function getTime(hours, minutes) {
            var time = null;
            minutes = minutes + "";
            if (hours < 12) {
                time = "AM";
            }
            else {
                time = "PM";
            }
            if (hours == 0) {
                hours = 12;
            }
            if (hours > 12) {
                hours = hours - 12;
            }
            if (minutes.length == 1) {
                minutes = "0" + minutes;
            }
            return hours + ":" + minutes + " " + time;
        }
        slideTime();
    
</script>
