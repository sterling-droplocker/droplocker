<script src="/js/StyledMarker.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	var letters = "abcdefghijklmnopqrstuvwxyz";
    var locationID;
    var name;
    var bounds;
    var count = 0;
    bounds = new google.maps.LatLngBounds();
    var latlng = new google.maps.LatLng('37.818280450951214', '-122.24744845519069');
    var myOptions = {
      zoom: 10,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
     var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
     map.enableKeyDragZoom({
                boxStyle: {
                  border: "1px solid #000",
                  
                },
                paneStyle: {
                  backgroundColor: "gray",
                  opacity: 0.2
                }
          });
     var json = <?= $json_locations?>; 
     for (var i = 0;i < json.length; i += 1) {
        var lat = json[i].lat;
        var lon = json[i].lon;
        add_marker(lat,lon,json[i].locationID,json[i].address, json[i].name);
        map.fitBounds(bounds);
    };
 
    
    function add_marker(lat,lon,locationID,address,name){
     latlng = new google.maps.LatLng(lat, lon);
     bounds.extend(latlng);
	 var color = "#6ed0f7";
	 switch(name){
	 	case "Kiosk":
	 		color = "#56ff56";
	 	break;
	 	case "Corner Store":
	 		color = "#3698fb";
	 	break;
	 }
     var styleIcon = new StyledIcon(StyledIconTypes.MARKER,{color:color,text:letters.charAt(count)});
     var marker = new StyledMarker({styleIcon:styleIcon,position:latlng,map:map});
	 count = count+1;
     //var marker = new google.maps.Marker({
     //     position: latlng, 
     //     map: map,
     //     title:"test",
     // }); 
      
       var infowindow = new google.maps.InfoWindow({
        content: address+"<br>"+name
        });
      
      google.maps.event.addListener(marker, 'mouseover', function() {
          infowindow.open(map,marker);
      });
      
       google.maps.event.addListener(marker, 'mouseout', function() {
          infowindow.close(map,marker);
      });
      
       google.maps.event.addListener(marker, 'click', function() {
          add_location(locationID, address, name);
       });
    };
    
    function add_location(locationID){
    	$.ajax({
          type: 'POST',
          data: "locationID="+locationID,
          url: "/account/locations/add_customer_location_ajax",
          success: function(data){
              if(data.status=='success'){
              	top.location.href = "/account/locations/";
                  //$("#box-table-a").find('tbody')
                  //  .append($('<tr>')
                  //          .html('<td>'+address+'</td><td width="*">'+name+'</td><td width="25"><a href="javascript:;"><img src="<?= CDNIMAGES?>icons/cross.png" id="'+locationID+'" class="delete_button" /></a></td>'))
                        
                   
              };
          },
          dataType: 'json'
        });
    }
    
    $(".add_button").bind('click', function(){
    	add_location($(this).attr('id'));
    });
    
   $('.delete_button').live('click', function(){
   	     $(this).html("<img src='/images/progress.gif' />");
         var row =  $(this).closest('tr');
         $.ajax({
          type: 'POST',
          data: "locationID="+$(this).attr('id'),
          url: "/account/locations/delete_customer_location_ajax",
          success: function(data){
              if(data.status=='success'){
                  row.fadeOut();
              };
          },
          dataType: 'json'
        });
   }) 
   
});
</script>

<?= $this->session->flashdata('message')?>

<div class='container'>
    <div class='row'>
        <div class='span6'>
            <div id='location_content'>
                <p><h2>Drop-Off Locations</h2></p>
                <p>When placing an order, you will be able to choose the location</p>
                <div><?= $message ?></div>
                <br>
                <div style='padding:5px;'><a class='btn btn-success' href='/account/orders/new_order'><i class='icon-white icon-shopping-cart'></i> Place a new order</a></div>
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                        <tr>
                        	<th></th>
                            <th>Address</th>
                            <th width='*'>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                       <? 
                        if($locations) : 
                		$count = 0;
                		$letter = "abcdefghijklmnopqrstuvwxyz";
                        foreach ($locations as $l):
                        ?>
                        <tr>
                        	<td><?= substr($letter, $count, 1);?></td>
                            <td><?= $l->address?></td>
                            <td><?= $l->name?></td>
                            
                        </tr>
                        <? 
                        $count++;
                        endforeach; endif; ?>
                    </tbody>   
                </table>
                <br>
                <div>If you need assistance with finding a location or if you want to request service to your building, please send us an email at <?php echo get_business_meta($this->business_id, 'customerSupport')?> </div>
            </div>
        </div>
        <div class='span6'>
            <div id='location_map'>
                <div class='pad_10'>
                    <div id="map_canvas" style="width:100%; height:500px;border:1px solid #666"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>

