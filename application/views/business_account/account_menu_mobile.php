<?php

    // menu items
    $accountMenu = array(
        'home' => array('link'=>"/account/", 'menuText' => get_translation_for_view("account_home", "Account Home")),
        'my_orders' => array('link'=>"/account/orders/view_orders", 'menuText' => get_translation_for_view("my_orders", "My Orders")),
        'my_settings' => array('link'=>'/account/profile/edit', 'menuText' => get_translation_for_view("my_settings", "My Settings")),
        'laundry_settings' => array('link'=>'/account/profile/preferences/', 'menuText' => get_translation_for_view("laundry_settings", "Laundry Settings")),
        'my_closet' => array('link' => '/account/profile/closet', 'menuText' => get_translation_for_view("my_closet", "My Closet")),
        'credit_card' => array('link'=>'/account/profile/billing', 'menuText' => get_translation_for_view("credit_card", "Credit Card")),
        'discounts' => array('link'=>'/account/programs/discounts/', 'menuText' => get_translation_for_view("discounts", "Discounts")),
        'kiosk' => array('link'=>'/account/profile/billing#access', 'menuText' => get_translation_for_view("kiosk", "Kiosk")),
    );
    if( get_business_meta( $this->business_id, 'sell_gift_cards' ) ){
        $accountMenu['gift_cards'] = array('link' => '/account/programs/gift_cards/', 'menuText' => get_translation_for_view("gift_cards", "Gift Cards"));
    }

    
    //if no store access, don't show it
    if( !$this->business->store_access ){
        unset($accountMenu['kiosk']);
    }
    
    //if no rewards, don't show it
    if( !empty( $this->rewards_name ) ){
         $accountMenu['locker_loot'] = array( 'link'=>'/account/programs/rewards/', 'menuText'=> $this->rewards_name );
    }
 
    if( !empty( $this->plans ) ){
         $accountMenu['laundry_plans'] = array( 'link' => '/main/plans', 'menuText' => 'Laundry Plans' );
    }
 
    if( !empty( $this->evertyhing_plans ) ){
        $accountMenu['everything_plans'] = array(
            'link' => '/main/everything_plans',
            'menuText' =>get_translation_for_view("evertyhing_plans", "Everything Plan")
        );
    }

    if( !empty( $this->product_plans ) ){
        $accountMenu['product_plans'] = array(
            'link' => '/main/product_plans',
            'menuText' =>get_translation_for_view("product_plans", "Product Plan")
        );
    }

    $accountMenu['logout'] = array( 'link' => '/logout', 'menuText' => 'Logout' ); 
    
    $str = '<ul class="nav">';
    foreach($accountMenu as $key=>$value){
         $str .= "<li><a href='".$value['link']."'>".$value['menuText']."</a></li>";
    }
    
    $str .= '</ul>';
    
    echo $str;
?>
