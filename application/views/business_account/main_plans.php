<?php

$date_format = get_business_meta($this->business_id, 'standardDateLocaleFormat', '%A, %B %e');
$pound = weight_system_format('', true);

?>

<script type="text/javascript" src="/js/showHide.js"></script>

<div class="container">
<!-- attempt to bootstrap this stuff -->
<div class="row">
<div class="container span7"> <!-- plan container -->

<? if ($customer_id): ?>
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("laundry_plans", "Laundry Plans")?>
  </li>
</ul>

<? endif; ?>



<? if(!empty($active_plan)) : ?>
<h2><?=get_translation_for_view("active_plan", "Your Active Plan")?></h2>
<?= $this->session->flashdata('alert');?>

<br>
<table class="table table-bordered">
<tr>
        <th><?=get_translation_for_view("plan_name", "Plan Name")?></th>
        <td><?= $active_plan->description?></td>
</tr>
<tr>
        <th><?= get_translation_for_view("included_pounds", "Included Pounds") ?></th>
        <td><?= weight_system_format($active_plan->pounds );?></td>
</tr>
<tr>
        <th><?=get_translation_for_view("duration", "Duration")?></th>
        <td><?=get_translation_for_view("days", "%days% Days", array(
                    'days' => $active_plan->days,
        ))?></td>
</tr>
<tr>
        <th><?=get_translation_for_view("amount", "Amount")?></th>
        <td><?= format_money_symbol($this->business_id, '%.2n', $active_plan->price)?></td>
</tr>
<tr>
        <th><?=get_translation_for_view("price_after_plan", "Price after plan used up")?></th>
        <td><?= format_money_symbol($this->business_id, '%.2n', $active_plan->expired_price > 0 ? $active_plan->expired_price : $wfPrice)?></td>
</tr>
<? if ($active_plan->rollover): ?>
<tr>
        <th><?= get_translation_for_view("rollover", "Rollover Pounds") ?>
        <br><span style='font-size:10px;font-weight:normal'><?= get_translation_for_view("if_auto_renewal", "(If on automatic renewal)") ?></span></th>
        <td><?= weight_system_format( $active_plan->rollover )?></td>
</tr>
<? endif; ?>
</table>

<?if(empty($upgrade_plan)): ?>
<p>
    <? if($active_plan->renew==1): ?>
    <?= get_translation_for_view("on_auto_renew", "This plan will automatically be renewed on %date%",
        array(
            'date' => convert_from_gmt_locale($active_plan->endDate, $date_format, $this->business_id)
        )); ?>
    <? else: ?>
    <?= get_translation_for_view("off_auto_renew", "This plan is not set to renew"); ?>
    <? endif; ?>
</p>
<? if ($active_plan->days): ?>
<p>
	<form action='/account/programs/plan_update/' method='post'>
	<input type='hidden' value='<?= ($active_plan->renew==1)?0:1;?>' name='renew' />
	<input type='submit' class="btn btn-info" name='submit' value='<?= $active_plan->renew ? get_translation_for_view("turn_off_auto_renew", "Turn Off Auto renew") :  get_translation_for_view("turn_on_auto_renew", "Turn On Auto renew") ?>' />
	</form>
</p>
<? endif; ?>

<? else: ?>
<div class='static_success'>
<h4><?=
    get_translation_for_view("plan_upgrade", "Your plan will upgrade to %plan% on %date%",
    array(
        'plan' => $upgrade_plan[0]->description,
        'date' => convert_from_gmt_locale($upgrade_plan[0]->startDate, $date_format, $this->business_id),
    )); ?></h4>

<form action='/account/programs/cancel_laundryplan_upgrade' method='post'>
    <input type='hidden' value='<?= $upgrade_plan[0]->laundryPlanID?>' name='laundryplan_id'/>
    <input onClick='return confirm(<?= json_encode(get_translation_for_view('cancel_plan_upgrade', "Are you sure?")) ?>);' type='submit' name='submit' class="btn btn-danger" value='<?= get_translation_for_view("cancel_upgrade", "Cancel Upgrade") ?>' />
</form>
</div>
<? endif; ?>

<? endif; ?>

<div class="clearfix"></div>


<h2><?= $plan_title?></h2>
<p><?=get_translation_for_view("monthly_plan_subtitle", "With our monthly laundry plans, you can receive a great savings on our wash & fold service.")?></p>

<!-- THIS NEEDS BOOTSTRAP -->
<? if($groups && $current_group==" "):?>
<div>
    <div class="span1">
        <img src="/images/college_girl2.jpg" />
    </div>

    <div class="span3">
        <h4><?=get_translation_for_view("student_plans", "Semester and Yearly Plans Avaible")?></h4>
        <?=get_translation_for_view("student_subtitle", "Perfect For the Busy Student!")?><br>

        <? foreach($groups as $g): ?>
            <div><a href='/main/plans/<?= $g->grouping?>'><?= $g->grouping?> Plans</a></div>
        <? endforeach; ?>
    </div>
</div>
<? endif; ?>


<div class="clearfix"></div>


<h2 style="padding-bottom:20px;"><?=get_translation_for_view("choose_plan", "Choose the plan that is right for you:")?></h2>

<?if($current_group!=" "): ?>
<div class="row">
    <div class="span3 offset7"><a href='/main/plans'><?=get_translation_for_view("back_to_plans", "Back to Monthly Plans")?></a></div>
</div>
<? endif; ?>
<div style='clear:both'></div>


<? if (!empty($plans)): ?>
    <? foreach ($plans as $businessLaundryPlan):
        $price = number_format_locale($businessLaundryPlan->price, get_business_meta($this->business_id, 'decimal_positions', 2), '.', '');
        //$separator = strpos($price, ",") !== false?",":".";
        list($dollars, $cents) = explode(".", $price);
    ?>
        <!-- plan row -->
    <div class="container">
    <div class="row" style="padding-bottom:20px;">
        <div class="span3">
        <div class="container">
            <div class="row">
                <div class="span2">
                    <h2><?= $businessLaundryPlan->description?> <?= ($businessLaundryPlan->caption!='')?"<br><small class='green'>".$businessLaundryPlan->caption."</small>":"";?></h2>
                </div>
            </div>
            <div class="row">
                <div class="span2" style="padding-top:10px;">
                    <span style="font-size:48px;font-weight:bold;line-height:30px;"><?= format_money_symbol($this->business_id, '%.0n', $dollars, 0)?></span>
                    <span style="font-size:14px;position:relative;top:-20px;font-weight:bold;"><?= $cents;?></span>
                </div>
            </div>

                <?if(!isset($active_plan) || ($active_plan->description != $businessLaundryPlan->description)): ?>
                <div class="row">
                <div style='margin-top: 20px;' class="span2">
                    <? if (!empty($active_plan)): ?>
                        <a href="/account/programs/upgrade_plan/<?= $businessLaundryPlan->businessLaundryPlanID?>" id="<?=$businessLaundryPlan->businessLaundryPlanID?>" class="btn btn-primary"><?= get_translation_for_view('upgrade_plan', 'Upgrade Now!'); ?></a>
                    <? else: ?>
                        <a href="/account/programs/display_buy_plan_form/<?= $businessLaundryPlan->businessLaundryPlanID?>" id="<?=$businessLaundryPlan->businessLaundryPlanID?>" class="btn btn-primary"><?= get_translation_for_view('display_buy_plan_form', 'Buy Now!'); ?></a>
                    <? endif;?>
                </div>
                </div>
                <? endif;?>
        </div>
        </div>

        <div class="span3" style="margin:0px;padding:0px;">
            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                <tr>
                    <td style="padding:5px;">
                        <?= get_translation_for_view("included_in_plan", "<strong>%pounds%</strong> Included in Plan", array(
                                'pounds' => weight_system_format( $businessLaundryPlan->pounds ),
                        )) ?>
                    </td>
                </tr>
                <? if ($businessLaundryPlan->poundsDescription): ?>
                <tr>
                    <td style="padding:5px;">
                       <?= $businessLaundryPlan->poundsDescription?>
                    </td>
                </tr>
                <? endif; ?>
                <? if($businessLaundryPlan->rollover): ?>
                <tr>
                    <td style="padding:5px;">
                        <?=  get_translation_for_view("up_to_rollover", "Up to %pounds% Rollover", array(
                                'pounds' => weight_system_format( $businessLaundryPlan->rollover )
                        )) ?>
                    </td>
                </tr>
                <? endif; ?>
                <tr>
                    <td style="padding:5px;">
                        <?= get_translation_for_view("term", "Term: %term%", array('term' => $businessLaundryPlan->term)) ?>
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px;">
                        <?= get_translation_for_view("per_lbs_on_plan", "<strong>%price% Per %pound%</strong> on Plan", array(
                                'price' => format_money_symbol($this->business_id, '%.2n', $businessLaundryPlan->price/$businessLaundryPlan->pounds),
                                'pound' => $pound,
                            )) ?>
                    </td>
                </tr>
                <tr>
                    <td style="padding:5px;">
                         <?=get_translation_for_view("plan_used_up", "%price% After plan used up", array(
                            'price' => format_money_symbol($this->business_id, '%.2n', $businessLaundryPlan->expired_price > 0 ? $businessLaundryPlan->expired_price : $wfPrice),
                         ))?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
    <? endforeach; ?>
<? else: ?>
	<h3><?=get_translation_for_view("no_plans", "No Plans")?></h3>
<? endif; ?>
</div>

<? if( $this->session->userdata('user_id')){ ?>
<div class='span3 hidden-phone'>
    <div class='user-info-box' style="margin-bottom: 2em;">
        <h3><?= getPersonName($customer) ?></h3>
        <p><?= $customer->email ?></p>
        <div>
            <a href='/logout' class='btn btn-primary btn-small login_logout'><i class='icon-user icon-white'></i> <?= get_translation_for_view("logout", "Logout") ?> </a>
        </div>
    </div>
    <?= $this->load->view('/business_account/account_menu', array( 'business' => $this->business ), true );?>
</div>
<? } ?>

</div>

<div class="clearfix"></div>

<div style="margin-top:15px;margin-left:0;" class="span9">
    <h2><?=get_translation_for_view("faq_title", "Frequently Asked Questions")?></h2><br>
    <?=get_translation_for_view("faqs", "<strong>Which plan is right for me?</strong><br>
    Most individuals typically use about 30 Lbs a month. For 2 people, 60lbs/month is typical usage of wash & fold. As a reference, a regular washing machine typically holds 10-12 Lbs.  If the plan you initially selected does not suit your needs, you can change your plan at any time.  The new plan will take effect as soon as your current plan expires.
    <br><br>
    <strong>What about upgrades?</strong><br>
    No problem!  You can still select any upgrades you like on the preferences page.  Your base wash & fold will be included in your plan and any upgrades such as special soap, dryer sheets, or other available upgrades will be charged separately and billed to your account on a per order basis.
    <br><br>
    <strong>How do I switch plans?</strong><br>
    If the plan you initially selected does not suit your needs, you can change your plan at any time.  The new plan will take affect as soon as your current plan expires.
    <br><br>
    <strong>How about special items such as comforters and pillows?</strong><br>
    We are still more than happy to process special items.  These are not included in the plan and will be charged separately and billed to your credit card on a per order basis.
    <br><br>
    <strong>Is there a minimum order size?</strong><br>
    Since we wash every customer's clothes separately, each order is separated in to at least two loads.  Hence the reason for a 12 LB minimum order size.  It is fine to send in less, and we will just deduct 12 Lbs from your available laundry plan balance for the order.
    <br><br>
    <strong>What happens if I go over the pounds in my plan?</strong><br>
    No problem.  Should your usage exceed the amount of your plan, you will be charged the standard per pound rate (currently %wfPrice%) for any pounds over your plan amount.
    <br><br>
    <strong>How long is the plan for?</strong><br>
    The laundry plan is for the term specified in your plan, from the date you sign up for the plan.  After 11:59 pm on the end date, any remaining balance in your account will be removed (less any rollover).
    <br><br>
    <strong>What happens when my plan expires?</strong><br>
    Your monthly laundry plan will automatically renew at 11:59 pm on the end date of your plan. To cancel the plan click on the button saying Turn Off Automatic Renewal
    <br><br>
    <strong>What if I don't use all my allotted pounds?</strong><br>
    Don't worry, we've got you covered. As long as you are setup for automatic renewal, unused pounds, up to a maximum of the plan's rollover pounds, will be added to the automatically renewed plan for the next month.
    <br><br>
    <strong>Is there a cut off date / time?</strong><br>
    All transactions are based on the day your order is picked up.  That means that if you place your order on the 15th, but it is not picked up until the 16th, and you plan expires on the 15th, your order will be credited to the next month's balance.
    <br><br>
    <strong>Can I combine my laundry plan with other discounts?</strong><br>
    Sorry, but our laundry plans are such a great deal that they may not be combined with any other offers.  Other discounts and offers will be applied to any charges not associated with your laundry plan (upgrades, dry cleaning, etc.).  Discounts may not be used to purchase laundry plans.
    <br><br>
    <strong>Are dry cleaning and laundered shirts included</strong><br>
    These plans are only for wash & fold orders.  Dry cleaning and laundered shirts are charged per piece as part of our dry cleaning service.
    <br><br>
    <strong>Are plan valid for wholesale customers?</strong><br>
    The monthly laundry plans are not available for wholesale or commercial accounts.
    <br><br>
    <strong>How do I get a refund?</strong><br>
    Sorry, but no refunds will be issued for partially or completely unused laundry plans.
    <br><br>
    <strong>How do I cancel my plan?</strong><br>
    Automatic renewal of laundry plans may be terminated at any time by clicking the button labeled Turn Off Automatic Renewal.  The plan will remain active until 11:59 pm of the last day of your plan, at which time your plan will not be automatically renewed.
    <br><br>
    <strong>Are there any additional charges?</strong><br>
    Laundry plans cover the cost of basic wash and fold.  Any extra services, such as detergent upgrades or home delivery, are additional and will be charged per order.
    ", array(
        'wfPrice' => format_money_symbol($this->business_id, '%.2n', $wfPrice),
    )); ?>

    <br>
    <br>
</div>
</div>