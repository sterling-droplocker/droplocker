<?php
use App\Libraries\DroplockerObjects\Location;

$is_home_delivery = in_array($location->serviceType, Location::getHomeDeliveryServiceTypes());

$hide_driver_notes = get_business_meta($this->business_id, "home_delivery_hide_driver_notes");

?>
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account", "Account")?></a> <span class="divider">/</span>
  </li>
  <li>
    <a href="/account/orders/new_order"><?=get_translation_for_view("place_new_order", "Place New Order")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("confirm_order", "Confirm Order")?>
  </li>
</ul>

<h2 class='page-header'><?=get_translation_for_view("confirm_order", "Confirm Order")?></h2>
<form action='/account/orders/complete' id="account_orders_complete" method='post'>

        <div class="errorText">
            <?= get_translation_for_view("confirmation_message", 'IMPORTANT: Please make sure you <u>clearly separate your wash & fold from your dry cleaning and laundered shirts</u> as combining the two may cause a delay in your order or improper processing of your garments.')?>
        </div>
        <br />
        <div style='text-align: center'>
                <input type='submit' name='submit' class='btn btn-success btn-large' value='<?=get_translation_for_view("place_order", "Yes, Place This Order")?>' />
                <input type='hidden' name='claimLocker' value='1' /><br>
            <a href='/account/orders/new_order'><?=get_translation_for_view("locker_selection_error", "No, Select a different locker")?></a>
        </div>

        <h2><?=get_translation_for_view("order_details", "Order Details")?></h2>
        <table class="table table-striped table-bordered table-condensed" style='width:100%'>
                <tr>
                    <th style='width:150px'><?=get_translation_for_view("location", "Location")?></th>
                    <td>
                        <? if ($is_home_delivery): ?>
                            <?= $customer->address1 ?><br><?= $customer->address2?>
                        <? else: ?>
                            <?= $location->address?><br><?= $location->address2?>
                        <? endif; ?>
                        <input type='hidden' name='location_id' value='<?= $location->locationID ?>' />
                    </td>
                </tr>
                <? if (!empty($pickup_window_id)): ?>
                <tr>
                    <th><?=get_translation_for_view("pickup", "Home Pickup")?></th>
                    <td>
                        <div class="home-pickup">
                        <div><b><?= strftime($date_format, strtotime($pickupDate)) ?></b></div>
                        <?= convert_from_gmt_aprax($windowStart, 'ga', $this->business_id) ?> -
                        <?= convert_from_gmt_aprax($windowEnd, 'ga', $this->business_id) ?>
                        </div>

                        <?php if (!$hide_driver_notes): ?>
                        <br>
                        <label for="pickup_notes"><?=get_translation_for_view("pickup_notes", "Instructions for the driver:")?></label>
                        <textarea style='width:98%;height:50px;border:1px solid #ccc;' id="pickup_notes" name='pickup_notes'></textarea>
                        <?php endif; ?>

                        <input type='hidden' name='pickup_window_id' value='<?= $pickup_window_id ?>' />
                        <input type='hidden' name='pickupDate' value='<?= $pickupDate ?>' />
                        <input type='hidden' name='windowStart' value='<?= $windowStart ?>' />
                        <input type='hidden' name='windowEnd' value='<?= $windowEnd ?>' />
                        <input type='hidden' name='locker_id' value='<?= $locker->lockerID ?>' />
                        <?php if (!empty($weight)): ?>
                        <input type='hidden' name='weight' value='<?= $weight ?>' />
                        <?php endif; ?>
                    </td>
                </tr>
                <? else: ?>
                <tr>
                    <th><?=get_translation_for_view("locker", "Locker")?></th>
                    <td><?= str_replace("_"," ",$locker->lockerName)?>
                            <input type='hidden' name='locker_id' value='<?= $locker->lockerID ?>' />
                    </td>
                </tr>
                <? endif; ?>
                <tr>
                    <th><?=get_translation_for_view("service", "Service")?></th>
                    <td>
                        <? foreach($service_type as $name): ?>
                        - <?= $name ?><br>
                        <? endforeach; ?>
                        <input type='hidden' name='service_type_serialized' value='<?= $service_type_serialized ?>' />
                    </td>
                </tr>
                <tr>
                    <th><?=get_translation_for_view("order_notes", "Order Notes")?><br>
                            <span style='font-size:10px;'><?=get_translation_for_view("this_order_only", 'This Order Only')?></span>
                    </th>
                    <td><textarea style='width:98%;height:50px;border:1px solid #ccc;' name='order_notes'><?= $_POST['notes']?></textarea></td>
                </tr>
                <tr>
                    <th><?=get_translation_for_view("customer_notes", "Customer Notes")?><br>
                            <span style='font-size:10px;'><?=get_translation_for_view("customer_notes_subtitle", "(Applied to all orders. Set in your preferences section)")?></span>
                    </th>
                    <td><?= $customer_notes ?></td>
                </tr>
        </table>
        <div style='text-align: center'>
            <br>
            <input type='submit' name='submit' class='btn btn-success btn-large' value='<?=get_translation_for_view("place_order", "Yes, Place This Order")?>' /><br>
            <a href='/account/orders/new_order'><?=get_translation_for_view("locker_selection_error", "No, Select a different locker")?></a>
        </div>

</form>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#account_orders_complete").on("submit", function(){
            $('[type="submit"]').prop('disabled', true);
            return true;
        });
    });
</script>