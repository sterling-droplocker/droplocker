<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li>
    <a href="/main/everything_plans"><?=get_translation_for_view("everything_plans", "Laundry Plans")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
        <?=get_translation_for_view("change_everything_plan", "Upgrade Everything Plan")?>
  </li>
</ul>


<div class='bodyBig'>
<h2><?= get_translation_for_view('change_everything_plan_title', 'Are you sure you want to change your plan?'); ?></h2>
			<br>
				<strong><u><?= get_translation_for_view('current_everything_plan', 'Current Plan:'); ?></u></strong><br>
				<strong><?= get_translation_for_view('plan', 'Plan:'); ?></strong> <?= $current_plan->description?><br>
				<strong><?= get_translation_for_view('included_amount', 'Credit amount:'); ?></strong>  <?=format_money_symbol($this->business->businessID, '%.2n', $current_plan->credit_amount) ?><br>
				<strong><?= get_translation_for_view('duration', 'Duration:'); ?></strong> <?= get_translation_for_view('duration_days', '%days% Days', array('days' => $current_plan->days)) ?><br>
				<strong><?= get_translation_for_view('amount', 'Amount:'); ?></strong>  <?= format_money_symbol($this->business->businessID, '%.2n', $current_plan->price)?><br>
				<strong><?= get_translation_for_view('start_date', 'Start Date:'); ?></strong>  <?= local_date($this->business->timezone, $current_plan->startDate, 'F j, Y')?><br>
				<strong><?= get_translation_for_view('end_date', 'End Date:'); ?></strong>   <?= local_date($this->business->timezone, $current_plan->endDate, 'F j, Y')?><br>
				<br>
				<strong><u><?= get_translation_for_view('new_plan', 'New Plan:'); ?></u></strong><br>
				<strong><?= get_translation_for_view('plan', 'Plan:'); ?></strong> <?= $new_plan->description?><br>
				<strong><?= get_translation_for_view('included_amount', 'Credit amount:'); ?></strong>  <?=format_money_symbol($this->business->businessID, '%.2n', $new_plan->credit_amount)?><br>
				<strong><?= get_translation_for_view('duration', 'Duration:'); ?></strong>  <?= get_translation_for_view('duration_days', '%days% Days', array('days' => $new_plan->days)) ?><br>
				<strong><?= get_translation_for_view('amount', 'Amount:'); ?></strong>  <?= format_money_symbol($this->business->businessID, '%.2n', $new_plan->price)?><br>
				<? if ($new_plan->rollover): ?>
				<strong><?= get_translation_for_view('rollover', 'Credit rollover (if on automatic renewal):'); ?> </strong>  <?=format_money_symbol($this->business->businessID, '%.2n', $new_plan->rollover)?> <br>
				<? endif; ?>
				<strong><?= get_translation_for_view('everything_plan_starts', 'Plan will start:'); ?></strong>  <?= local_date($this->business->timezone, $current_plan->endDate, 'F j, Y')?><br><br>
			<br>
			<form action='' method='post'>
			<input type='hidden' name='new_plan_id' value="<?= $new_plan->businessLaundryPlanID?>" />
			<input type='submit' name='submit' value="<?= get_translation_for_view('yes', 'Yes, Upgrade My Plan') ?>" class='btn btn-primary' />
			</form>
			<br>
			<a href="/main/everything_plans" alt="return to my laundry plan"><?= get_translation_for_view('no', 'No, take me back'); ?></a><br/><br>
</div>
