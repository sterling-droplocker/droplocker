<?= $this->session->flashdata('alert'); ?>
<div style='text-align: justify;padding:0px 10px' >
<h1> <?= get_translation_for_view("terms_and_conditions_title", 
			"%companyName% %programName% Terms And Conditions",
			array(
				"companyName" => $business->companyName,
				"programName" => $programName
			))
	?>
</h1>

   <div> <?= $terms ?> </div>

<? if($agree): ?>
    <div class="alert alert-success"> <?= get_translation_for_view("currently_participating_message", "You are participating in this program.") ?> </div>
<? else: ?>
<form action="" method="post"><br>
    <?= form_submit(array("value" => 
			get_translation_for_view("i_agree_button", 
				"I agree to the %companyName% Terms and Conditions", 
				array("companyName" => $business->companyName)), 
			"name" => "submit", "class" => "btn btn-primary")) ?>
</form>
<? endif; ?>
</span>
</div>
