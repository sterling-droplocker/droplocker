<?
    //months
    //select form dropdown creation
    //short variable names! how dare I! c'mon man, it's just a birthday form

    //$m is for months!
    $months = array();
    $months[''] = get_translation_for_view('Month', 'Month');
    for( $m = 1; $m < 13; $m++ ){
        $months[ $m ] = ucfirst(strftime('%B', mktime(0, 0, 0, $m, 1, 2000) ));
    }

    //$d is for days
    $days = array();
    for( $d = 1; $d <32; $d++){
        $days[ $d ] = $d;
    }
    $days[''] = get_translation_for_view('Day', 'Day');

    //$y is for years
    $years = array();
    $ref_year = (int) date('Y', strtotime('now +6 years'));
    $end_year = (int) date('Y', strtotime('now -100 years'));
    for( $ref_year; $ref_year > $end_year; $ref_year-- ){
        $years[$ref_year] = $ref_year;
    }
    $years[''] = get_translation_for_view('Year', 'Year');

    ///parse out the birthday

    //if they passed in a birthday
    if( empty( $birthday ) || $birthday == '0000-00-00' ){
        $user_birth_day   = '';
        $user_birth_month = '';
        $user_birth_year  = '';
    }else{
        $user_birth_day   = date('d', strtotime( $birthday ) );
        $user_birth_month = date('m', strtotime( $birthday) );
        $user_birth_year  = date('Y', strtotime( $birthday) );
    }

?>

<?php
$formTemplateKey = 'dateComponents';
$formTemplateDefault = 'month||day||year';
$fieldSeparator = "&nbsp;";

$fieldList = array();

foreach($months as $k=>$v) 
{
    $months[$k] = get_translation_for_view($v, $v);
}

$fieldList['month'] = form_dropdown('birth_month',$months, $user_birth_month,'class="span2" style="min-width:80px !important;"');
$fieldList['day'] = form_dropdown('birth_day',$days, $user_birth_day,'class="span1" style="min-width:80px !important;"');
$fieldList['year'] = form_dropdown('birth_year',$years, $user_birth_year,'class="span1" style="min-width:80px !important;"');
            
$bdayForm = getSortedForm($formTemplateKey, $formTemplateDefault, $fieldList, $fieldSeparator, 'view');
?>

<?php echo $bdayForm; ?>
        
