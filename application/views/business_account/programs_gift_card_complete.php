<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("gift_cards", "Gift Cards")?>
  </li>
</ul>

<?php
/**
 * Expects the following content variables
 *  coupon stclass
 */
?>

<h2><?=get_translation_for_view("thank_you", "Thank You For Ordering a Gift Card!")?></h2>
<p>
    <?=get_translation_for_view("explanation",
        "Your Gift Card information has been mailed to <strong>%coupon_fullName%</strong> "
        ."<a href='mailto://%coupon_email%'>%coupon_email%</a> <br><br>"
        ."<strong>Your Gift Card code for \$%coupon_amount% is:<br><br>"
        ."<font size='+2'><div align='center'>GF%coupon_code%</div></font></strong>"
        ."<br><br> This code can be entered as a promotional code when placing an order, or under the discounts section of our website.",
        array(
            "coupon_fullName" => $coupon->fullName, 
            "coupon_email" => $coupon->email,
            "coupon_amount" => $coupon->amount,
            "coupon_code" => $coupon->code)
        )?>
</p>
