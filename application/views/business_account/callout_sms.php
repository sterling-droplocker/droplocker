<div class="hidden-phone hidden-tablet">
    <div class="calloutBanner calloutRight" style="margin-top: 25px">
        <div class="calloutBannerPic">
            <img src="/images/callOut_iphone.png" alt="Order Via SMS" style="width: 100%;">
        </div>
        <div class="calloutBannerText graySmall" style="width: 45%">
            <h4 style="font-size: 14px;"><?= get_translation_for_view("order_via_sms", "Order Via SMS") ?></h4>
            <div style="margin-bottom:5px; margin-top:7px;">Text LAUNDRY to MOSIO (66746) to <?= get_translation_for_view("place_your_order","Place Your Order") ?></div>
            <div><a href="/main/plans" class="btn btn-primary">Learn More</a></div>
        </div>
    </div>
</div>
