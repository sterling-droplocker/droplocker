<?php

$show_delivery_fees = true;

?>
<?= $this->session->flashdata('alert'); ?>
<? if (!empty($pickup_dates)): ?>
<ul class="nav nav-pills" style="width: 220px;">
    <li class="dropdown active step-width" id="select_pickup_date" style="float:none;">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <?= get_translation_for_view('select_pickup_day', 'Select a pickup day'); ?>
            <b class="caret pull-right"></b>
        </a>
        <ul class="dropdown-menu" style="top:0;left:100%;">
            <? foreach($pickup_dates as $pickup_date): ?>
            <li class="group-title"><a href="#" data-date="<?= $pickup_date ?>">
                <?php
                $pickup_date_timestamp = strtotime($pickup_date);
                
                $day = strftime("%A", $pickup_date_timestamp);
                $month = strftime("%B", $pickup_date_timestamp);                
                $day_short = strftime("%a", $pickup_date_timestamp);
                $month_short = strftime("%b", $pickup_date_timestamp);
                
                $dates_findme = array($day, $month, $day_short, $month_short);

                $dates_replacements = array(
                    get_translation_for_view($day, $day),
                    get_translation_for_view($month, $month),
                    get_translation_for_view($day_short, $day_short),
                    get_translation_for_view($month_short, $month_short)
                );

                echo str_replace($dates_findme,  $dates_replacements, strftime($date_format, strtotime($pickup_date)));
                ?>
            </a></li>
            <? endforeach ?>
        </ul>
    </li>
    <li class="dropdown active step-width" id="select_pickup_time" style="float:none;display:none;">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <?= get_translation_for_view('select_pickup_time', 'Select a pickup time'); ?>
            <b class="caret pull-right"></b>
        </a>
        <ul class="dropdown-menu" style="top:0;left:100%;">
            <? foreach($pickup_windows as $group): ?>
                <? if ($show_delivery_fees): ?>
                <li class="group-title" style="<?=$homePickup->pickupDate == $group['date'] ? '' : 'display:none;' ?>">
                    <a href="#" data-date="<?= $group['date'] ?>"><?php
                    if (!empty($group['fee'])):
                        echo get_translation_for_view('fee_title', 'Pickup Fee %fee%', array('fee' =>   format_money_symbol($this->business->businessID, '%.2n', $group['fee'])));
                    endif;
                    ?></a>
                </li>
                <? endif; ?>
                <? foreach($group['items'] as $pickup_window):  ?>
                <li class="sub-option" style="<?=$homePickup->pickupDate == $group['date'] ? '' : 'display:none;' ?>">
                    <a href="#" data-date="<?= $group['date'] ?>" data-id="<?= $pickup_window['id'] ?>" data-start="<?= $pickup_window['start_time'] ?>"  data-end="<?= $pickup_window['end_time'] ?>">
                        <?= date($time_format, strtotime($pickup_window['start'].':00')) ?>-<?= date($time_format, strtotime($pickup_window['end'].':00')) ?>
                        <? if (!empty($pickup_window['notes'])): ?>
                            <div class="subnote"><?= $pickup_window['notes'] ?></div>
                        <? endif; ?>
                    </a>
                </li>
                <? endforeach ?>
            <? endforeach ?>
        </ul>
    </li>
</ul>

<input type="hidden" name="pickupDate" value="<?= $homePickup->pickupDate ?>">
<input type="hidden" name="windowStart" value="<?= $homePickup->windowStart ? $homePickup->windowStart->format('Y-m-d H:i:s') : '' ?>">
<input type="hidden" name="windowEnd" value="<?= $homePickup->windowEnd ? $homePickup->windowEnd->format('Y-m-d H:i:s') : '' ?>">
<input type="hidden" name="pickup_window_id" value="<?= $homePickup->pickup_window_id ?>">
<? endif; ?>

<script>
$('#select_pickup_date .dropdown-menu a').click(function(evt) {
    evt.preventDefault();

    var date = $(this).attr('data-date');
    var text = $(this).text();

    $('input[name=pickupDate]').val(date);
    $('#select_pickup_date .dropdown-toggle').html(text + '<b class="caret pull-right"></b>');
    $('#select_pickup_time .dropdown-menu a').each(function(i, e) {
        var itemDate = $(e).attr('data-date');
        $(e).parent()[itemDate == date ? 'show' : 'hide']();
    });

    $('#select_pickup_time .dropdown-toggle').html("<?= get_translation_for_view('select_pickup_time', 'Select a pickup time'); ?>" + '<b class="caret pull-right"></b>');
    $('#select_pickup_time').show();
});

$('#select_pickup_time .dropdown-menu a').click(function(evt) {
    evt.preventDefault();

    if ($(this).parent().hasClass('group-title')) {
        return;
    }

    var date = $(this).attr('data-date');
    var start = $(this).attr('data-start');
    var end = $(this).attr('data-end');
    var id  = $(this).attr('data-id');
    var text = $(this).html();


    $('input[name=pickupDate]').val(date);
    $('input[name=windowStart]').val(start);
    $('input[name=windowEnd]').val(end);
    $('input[name=pickup_window_id]').val(id);

    $('#select_pickup_time .dropdown-toggle').html('<b class="caret pull-right"></b>' + text);

});
</script>
