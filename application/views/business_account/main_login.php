﻿<?php 
//The following code determines if the facebook buttons should open in a new tab or not, if applicable to the business.
$target = "_self";
if ($business->open_facebook_register_in_new_tab) {
    $target = "_blank";
}
?>
<div class='container'>
<div class='row'>
     <div class='span8 hidden-desktop hidden-tablet' id="register-phone" style='float:right'>
         <a href='/register' class='btn btn-info btn-large'><i class='icon-user icon-white'></i> <?=get_translation_for_view("register", "Register Now")?></a>
     </div>
</div>
<div class='row'>
    <div class='span8 offset1'>
        <h2 class='page-header'><?=get_translation_for_view("login", "Login")?></h2>
    </div>
</div>
<div class='row'>
    <div class='span4 offset1'>
        <?php echo validation_errors("<div class='alert alert-error'>", "</div>"); ?>
        <?php echo $this->session->flashdata('alert'); ?>
        <form method='post' action=''>
            <div class="control-group">
              <label class="control-label" for="usename"><?=get_translation_for_view("email", "Email")?></label>
              <div class="controls">
                <input size='30' id='username' type='text' name='username' />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="password"><?=get_translation_for_view("password", "Password")?></label>
              <div class="controls">
                <input size='30' id='password' type='password' name='password' />
              </div>
            </div>
            <input class='btn btn-primary' type='submit' value='<?=get_translation_for_view("login_button", "Login")?>' /> &nbsp;&nbsp;&nbsp;&nbsp;<a href='/forgot_password'><?=get_translation_for_view("forgot_password", "Change / Forgot Password?")?></a>
            <input type="hidden" name="lid" value="<?=$this->input->get('lid');?>" />
        </form>
        <hr />
        <?php if ($business->facebook_api_key && $business->facebook_secret_key): ?>
            <a name="register_with_facebook" id="login_with_facebook" target="<?=$target?>" style="background: url('/images/icons/facebook.png') no-repeat scroll 8px center #F1F1F1; font-size: 14px; padding: 8px 8px 8px 22px; text-decoration: none;" href="<?= $facebook_url ?>">&nbsp;&nbsp; <?=get_translation_for_view("login_with_facebook", "Login with Facebook")?> </a>
        <?php endif; ?>
    </div>
    <div class='span4 hidden-phone'>
        <div class='alert alert-info'>
            <h1><?=get_translation_for_view("first_time_user", "First Time User")?></h1>
            <p style='font-size:16px;'><?=get_translation_for_view("first_time_user_subtitle", "It's free, and fast!")?></p>
            <a href='/register' class='btn btn-info btn-large'><i class='icon-user icon-white'></i> <?=get_translation_for_view("register", "Register Now")?></a>

            <?php if ($business->facebook_api_key && $business->facebook_secret_key): ?>
                <div style="margin-top: 20px; margin-bottom: 10px;"> 

                    <a name="register_with_facebook" target='<?=$target?>' id="login_with_facebook" style="background: url('/images/icons/facebook.png') no-repeat scroll 8px center #F1F1F1; font-size: 14px; padding: 8px 8px 8px 22px; text-decoration: none;" href="<?= $facebook_url ?>">&nbsp;&nbsp; <?=get_translation_for_view("register_with_facebook", "Register with Facebook")?> </a> 
                </div>
            <?php endif; ?>
            
        </div>
    </div>
</div>
</div>
