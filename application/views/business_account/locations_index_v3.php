<?php

use App\Libraries\DroplockerObjects\Location;
$distance_unit = get_translation_for_view('distance_unit', 'm');
?>

<style type='text/css'>
@import url("/css/font-awesome-4.1.0/css/font-awesome.min.css");

#map_canvas {
    max-width:600px;
    height:500px;
    border:1px solid #666;
    margin-top:5px;
}

#map_canvas img {
    max-width: none;
}

#map_legend {
    padding: 6px;
    margin: 0 5px 5px 0;
    background: #fff;
    color: #000:
    font-size: 11px;
    border: 1px solid #999;
    box-shadow: 2px 2px 2px rgba(0,0,0,.3);
}

#map_legend div {
    line-height: 24px;
}

#map_legend span {
    width: 24px;
    text-align: center;
}

.legend-color {
    border: 1px solid #000;
    width: 10px;
    height:10px;
    float: left;
    margin-right: 5px;
}


.delete_button:hover,
.delete_button {
    color: #EA0000;
    text-decoration:none;
}

.added_button:hover,
.added_button {
    color: #0084FF;
    text-decoration:none;
}

.add_button:hover,
.add_button {
    color: #70BF41;
    text-decoration:none;
}

.table-locations th {
    border-right: none;
}

.gmap-buble {
    width: 260px;
    height: 180px;
}

.gmap-buble h3 {
    line-height: 1;
}

.gmap-buble .add_button span {
    font-weight: bold;
    font-size: 1.5em;
}

.banner {
    color: #666;
    background: #E5EBF4;
    border: 1px solid #70BF41;
    padding: 2px;
    margin-bottom: 18px;
}

.banner-body {
    padding: 10px;
    background: #E5EBF4 url(<?= get_translation_for_view('banner_image', '/images/ll_home_delivery.png'); ?>) top right no-repeat;
}

.banner h3 {
    margin-bottom: 0;
    font-weight: normal;
    font-size: 20px;
}

.banner p {
    font-size: 14px;
    margin-bottom: 14px;
}

.banner a {
    color: #0365C0;
    font-size: 18px;
    text-decoration: none;
}

#no_locations_message {
    text-align: center;
    margin:1em 0 0;
}
</style>

<?php echo $this->session->flashdata('alert')?>

<div class='container'>
    <div class="row" style="min-height: 150px;">
        <div class="span5" id="customer_locations">
            <h2><?=get_translation_for_view("your_locations", "Your Locations")?></h2>
            <div id="no_locations_message" class="alert alert-warning" style="<?= !empty($customer_locations) ? 'display:none;' : '' ?>">
                <?= get_translation_for_view('empty_customer_locations', "You don't have any locations yet. Please select a location.") ?>
            </div>
            <table class="table table-locations" style="<?= empty($customer_locations) ? 'display:none;' : '' ?>">
                <thead>
                    <tr>
                        <th><?=get_translation_for_view("address", "Address")?></th>
                        <th><?=get_translation_for_view("distance", "Distance")?></th>
                        <th width='50' align='center'><?=get_translation_for_view("remove", "Remove")?></th>
                    </tr>
                </thead>
                <tbody id="existing_locations">
                    <? foreach($customer_locations as $location):
                        $is_home_delivery = in_array($location->serviceType, Location::getHomeDeliveryServiceTypes());
                    ?>
                    <tr>
                        <td class="location-info">
                            <b><?= $is_home_delivery ? get_translation_for_view('location_home_delivery', 'Home Delivery') : ($location->companyName ? $location->companyName : $location->address); ?></b><br>
                            <?= $is_home_delivery ? $customer->address1 : $location->address ?><br>
                            <a href="/account/orders/new_order?loc=<?= $location->locationID ?>"><?= get_translation_for_view("order_now", "Order Now"); ?></a>
                        </td>
                        <td><?= ($customer->lat != 0 && $customer->lon != 0 && $location->lat != 0 && $location->lon != 0)
                                ? round(distance($customer->lat, $customer->lon, $location->lat, $location->lon, 'm'), 2) . ' ' . $distance_unit
                                : '-';
                        ?></td>
                        <td style="text-align:center;">
                            <a class="delete_button" href="javascript:;"  data-location_id="<?= $location->locationID ?>"><i class="fa fa-times-circle-o fa-fw fa-2x"></i></a>
                        </td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
            </table>

            <br>

            <div id="loading_message">
                <?=get_translation_for_view("loading", "Loading Locations...")?>
                <img src="/images/loading.gif" style="max-height:50px;" />
            </div>

            <div id="results_note"></div>

            <div id="filter_results" style="display:none;">
                <div id="public_locations_wrapper">
                    <h3><?= get_translation_for_view('public_locations', 'Public Locations'); ?></h3>
                    <p><?= get_translation_for_view('public_locations_desc', 'Your closest public locations'); ?></p>
                    <table class="table table-locations">
                        <thead>
                            <tr>
                                <th><?=get_translation_for_view("address", "Address")?></th>
                                <th><?=get_translation_for_view("distance", "Distance")?></th>
                                <th width='40' align='center'><?=get_translation_for_view("add", "Add")?></th>
                            </tr>
                        </thead>
                        <tbody id="public_locations">
                        </tbody>
                    </table>
                </div>

                <div id="private_locations_wrapper">
                    <h3><?= get_translation_for_view('private_locations', 'Private Locations'); ?></h3>
                    <p><?= get_translation_for_view('private_locations_desc', 'Available for residents only'); ?></p>
                    <table class="table table-locations">
                        <thead>
                            <tr>
                                <th><?=get_translation_for_view("address", "Address")?></th>
                                <th><?=get_translation_for_view("distance", "Distance")?></th>
                                <th width='50' align='center'><?=get_translation_for_view("add", "Add")?></th>
                            </tr>
                        </thead>
                        <tbody id="private_locations">
                        </tbody>
                    </table>
                </div>
            </div>
            <div>&nbsp;</div>
        </div><!-- /span5 -->

        <div class="span5">
            <? if ($show_home_pickup): ?>
            <div class="banner">
                <div class="banner-body">
                    <h3><?= get_translation_for_view('banner_title', 'Home pickup & delivery'); ?></h3>
                    <p><?= get_translation_for_view('banner_text', 'Pickup and delivery to your doorstep.<br>Fees apply.'); ?></p>
                    <div><a href="/account/locations/setup_home_delivery"><?= get_translation_for_view('banner_link', 'Schedule a pickup'); ?></a></div>
                </div>
            </div>
            <? endif; ?>

            <div id="locations_filter" >
                <h3><?= get_translation_for_view("location_search", "Location search"); ?></h3>
                <?php $location_requests = get_translation_for_view('location_requests', 'location_requests', array('email' => $this->business->email));?>
                <?php

                    if ($location_requests != 'location_requests') {
                        echo '<p>'.$location_requests.'</p>';
                    }
                ?>
                <input type="text" id="address_search_field" class="span4 search-query" placeholder="Enter an address" style="padding:5px;" value="<?= htmlentities($customerAddress) ?>"/>
                <button id="locations_search_button" type="button" class="btn"> <?= get_translation_for_view("search_button", "Search") ?> </button>
                <div id="map_canvas"></div>
            </div>
        </div><!-- /span5 -->
    </div><!-- /row -->
</div>

<!--script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<?=get_business_gmaps_api_key($business_id);?>" ></script--> 
<script type="text/javascript" src="/js/account/locations/locations.js?v=7"></script>

<script type="text/javascript">

var limitGeoCountry             = <?= json_encode(get_business_meta($business_id, 'limit_geo_country')) ?>;
var customerLatLon              = <?= json_encode($customerLatLon); ?>;
var customerAddress             = <?= json_encode($customerAddress);?>;
var existingCustomerLocations   = <?= $json_customer_locations; ?>;
var locationsJSON               = {}; //pull these via ajax locationsJSON()

/* map legend and messages */
DLMap.legend['home']              = <?= json_encode(get_translation_for_view("home_address", "Your Address")) ?>;
DLMap.legend['private']           = <?= json_encode(get_translation_for_view("private", "For Residents Only")) ?>;
DLMap.legend['public']            = <?= json_encode(get_translation_for_view("public", "Public")) ?>;

Results.maxVisibleResults       = <?= get_business_meta($business_id, 'location_result_limit', 3) ?>;
Results.noResultsText           = <?= json_encode(get_translation_for_view("no_results_found", "No Results Found")) ?>;
Location.distanceUnit           = <?= json_encode($distance_unit) ?>;
Location.selectLocationText     = <?= json_encode(get_translation_for_view("select_location", "Select location")) ?>;
Location.hoursText              = <?= json_encode(get_translation_for_view("hours_label", "Hours:")) ?>;
Customer.orderNowText           = <?= json_encode(get_translation_for_view("order_now", "Order Now")) ?>;
Customer.addLocationError       = <?= json_encode(get_translation_for_view("location_error", "Already added that location!")) ?>;

/* On Load */
$(function() {
    loadMaps();
    loadLocations();

    $('#filter_results,#locations_filter').on('click', '.add_button', Customer.addLocation);
    $('#existing_locations').on('click', '.delete_button', Customer.deleteLocation);

    $('#locations_search_button').click(function(evt) {
        evt.preventDefault();
        var searchTerm = $('#address_search_field').val();
        DLMap.searchByAddress(searchTerm);
    });

    $('#address_search_field').keydown(function(evt) {
        if(evt.which != 13) {
            return;
        }
        evt.preventDefault();
        var searchTerm = $('#address_search_field').val();
        DLMap.searchByAddress(searchTerm);
    });

    $('.location-info').click(function(evt) {
        window.location.href = $('a:first', this).attr('href');
    });

});

/**
 * Loads the locations data
 */
function loadLocations() {
    $.getJSON('/account/locations/locationsJSON', function(res) {
        locationsJSON = res;
        for (var x in locationsJSON) {
            var loc = locationsJSON[x];
            locationsJSON[x].info = Location.getInfoBox(loc);

            if (Location.isPublic(loc)) {
                DLMap.hasPublic = true;
            }
        }

        $('#loading_message').hide();
    });
}

/**
 * Load google maps asynchronously
 */
function loadMaps() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.9&libraries=places&sensor=false&callback=initializeMap&key=<?=get_business_gmaps_api_key($business_id);?>';
    <?php if (!empty($customerLanguage)): ?>
        script.src += '&language=<?=$customerLanguage->tag?>&region=<?=$customerLanguage->locale?>';
    <?php endif; ?>
    document.body.appendChild(script);
}

function initializeMap() {
    DLMap.initializeMap(customerLatLon, customerAddress, limitGeoCountry);
}

</script>
