<h2><?= get_translation_for_view('redeem_program_points_title', 'Redeem %program_name% Points', array('program_name' => $program_name)); ?></h2>

<br>
<div class="bodyBold">
    <?=  get_translation_for_view('redeem_program_points_text',
            'Thank you for redeeming your %program_name% and for being such a loyal customer. A %points% credit has been added to your account which will automatically be applied to your next orders.',
            array(
                'program_name' => $program_name,
                'points' => format_money_symbol($this->business->businessID, '%.2n', $points),
            )); ?>
</div>
<br>
<div>
    <a href='/account/programs/rewards' class='button blue'><?= get_translation_for_view('program_details', '%program_name% Details', array('program_name' => $program_name)); ?></a>
</div>
