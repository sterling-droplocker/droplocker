<?php
/**
 * This view appears to expect the following content variables
 *  $business stdclass the curent business with its properties
 */
    // menu items
    $accountMenu = array(
        'home'=>array('link'=>"/account/", 'menuText'=> get_translation_for_view("account_home", "Account Home")),
        'my_orders'=>array('link'=>"/account/orders/view_orders", 'menuText'=> get_translation_for_view("my_orders", "My Orders")),
        'my_settings'=>array('link'=>'/account/profile/edit', 'menuText'=>get_translation_for_view("my_account_settings", "My Account Settings")),
        'laundry_settings'=>array('link'=>'/account/profile/preferences/', 'menuText'=>get_translation_for_view("laundry_settings", "My Laundry Settings")),
        'my_closet' => array('link' => '/account/profile/closet', 'menuText'=>get_translation_for_view("my_closet", "My Closet")),
        'credit_card'=>array('link'=>'/account/profile/billing', 'menuText'=>get_translation_for_view("payment_settings", "Payment Settings")),
        'discounts'=>array('link'=>'/account/programs/discounts/', 'menuText'=>get_translation_for_view("discounts", "Discounts")),
        'kiosk'=>array('link'=>'/account/profile/billing#access', 'menuText'=>get_translation_for_view("store_access", "Store Access")),
    );
    if( get_business_meta( $this->business_id, 'sell_gift_cards' ) ){
        $accountMenu['gift_cards'] = array('link'=>'/account/programs/gift_cards/', 'menuText'=> get_translation_for_view("gift_cards", "Gift Cards"));
    }

    //if no store access, don't show it
    if( !$business->store_access ){
        unset($accountMenu['kiosk']);
    }
    
    //if no rewards, don't show it
    if( !empty( $this->rewards_name ) ){
         $accountMenu['locker_loot'] = array(
			'link'=>'/account/programs/rewards/',
			'menuText'=> get_translation_for_view("rewards_name_menu", 
							"Rewards Program", 
							array("rewards_name" => $this->rewards_name))
		);
    }

    if( !empty( $this->plans ) ){
         $accountMenu['laundry_plans'] = array(
			'link' => '/main/plans',
			'menuText' =>get_translation_for_view("laundry_plans", "Laundry Plans")
		);
    }

    if( !empty( $this->everything_plans ) ){
        $accountMenu['everything_plans'] = array(
            'link' => '/main/everything_plans',
            'menuText' =>get_translation_for_view("evertyhing_plans", "Everything Plans")
        );
    }

    if( !empty( $this->product_plans ) ){
        $accountMenu['product_plans'] = array(
            'link' => '/main/product_plans',
            'menuText' =>get_translation_for_view("product_plans", "Product Plans")
        );
    }
?>
<div id='default_account_nav' style='width:98%'><h3><?= get_translation_for_view("account_menu_title", "Account Menu") ?></h3>
	<ul style='width:100%' id="account_menu_items">
	<? foreach($accountMenu as $key=>$value): ?>
		<li><a href='<?=$value['link']?>'><?= $value['menuText'] ?></a></li>
	<? endforeach; ?>    
	</ul>
</div>

<p>
	<a class='btn btn-success btn-large' href='/account/orders/new_order'>
		<i class='icon-shopping-cart icon-white'></i>
		<?= get_translation_for_view("place_order_button", "Place Order Now"); ?>
	</a>
</p>
