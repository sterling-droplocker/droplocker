<?php

$date_format = get_business_meta($this->business_id, 'standardDateLocaleFormat', '%A, %B %e');


?>
<h2><?=get_translation_for_view("order_complete", "Order Complete")?></h2>
<p>
<div style='float:left; width:50%'>
    <table class="table table-bordered">
	<tr>
		<th><?=get_translation_for_view("plan_name", "Plan Name")?></th>
		<td><?= $plan->description?></td>
	</tr>
	<tr>
		<th><?=get_translation_for_view("included_pounds", "Included Pounds")?> </th>
		<td><?=  weight_system_format($plan->pounds) ?></td>
	</tr>
	<? if($plan->days > 0): ?>
            <tr>
                <th><?=get_translation_for_view("duration", "Duration")?></th>
                <td><?=get_translation_for_view("days", "%days% Days", array(
                            'days' => $plan->days,
                )) ?></td>
            </tr>
        <? else: ?>
            <tr>
                <th><?=get_translation_for_view("end_date", "End Date")?></th>
                <td><?= convert_from_gmt_locale($plan->endDate, $date_format, $this->business_id) ?></td>
            </tr>
        <? endif; ?>
	<tr>
            <th><?=get_translation_for_view("amount", "Amount")?></th>
            <td><?= format_money_symbol($this->business_id, '%.2n', $plan->price)?></td>
	</tr>
    <tr>
            <th><?=get_translation_for_view("price_after_plan", "Price after plan used up")?></th>
            <td><?= format_money_symbol($this->business_id, '%.2n', $plan->expired_price > 0 ? $plan->expired_price : $wfPrice)?></td>
    </tr>
	<? if ($plan->rollover): ?>
	<tr>
            <th><?= get_translation_for_view("rollover", "Rollover Pounds") ?>
            <br><span style='font-size:10px;font-weight:normal'><?= get_translation_for_view("if_auto_renewal", "(If on automatic renewal)") ?></span></th>
            <td><?= weight_system_format($plan->rollover); ?></td>
	</tr>
        <? endif; ?>
</table>
</div>
<div style="float:left; width:auto; margin-left:10px;">
	<a href="/account" class='btn btn-success btn-large'><?= get_translation_for_view("back_to_account", "Back To My Account") ?></a>
</div>
<br style='clear:left'>
<div style='margin:10px 0px;padding:10px; border:1px solid #eaeaea'>
		<p><?=get_translation_for_view("auto_renew_on", "This Plan Will Auto Renew On %date%", array(
                    'date' => convert_from_gmt_locale($plan->endDate, $date_format, $this->business_id),
        )) ?></p>
		<div>
		<form action='/account/programs/plan_update/' method='post'>
		<input type='hidden' value='0' name='renew' />
		<input type='submit' class="btn btn-warning" name='submit' value='<?= get_translation_for_view("autorenew_off", "Turn off Auto Renew") ?>'/>
		</form>
		</div>
	</div>
</p>
