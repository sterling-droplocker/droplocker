<?php

$date_format = get_business_meta($this->business_id, "fullDateDisplayFormat");

$home_delivery_enable_order_reschedule = get_business_meta($this->business_id, "home_delivery_enable_order_reschedule");
?>

<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("view_orders", "View Orders")?>
  </li>
</ul>

<?= $this->session->flashdata('alert'); ?>

<h2 class='page-header'><?=get_translation_for_view("my_orders", "My Orders")?></h2>
<p>
<?=get_translation_for_view("_order_not_here", "If your order is not showing up here, it may have not been picked up yet or you have have not used a %company_name% laundry bag.", array("company_name"=>$business->companyName))?>
</p>
<p>
    <a href='/account/orders/new_order' id="place_new_order" class='btn btn-success btn-large' ><i class='icon-shopping-cart icon-white'></i><?=get_translation_for_view("new_order", "New Order")?></a>
</p>

<br>
<h2><?=get_translation_for_view("pending_orders", "Pending Orders")?></h2>

<? if($claims): ?>
<p><?=get_translation_for_view("if_canceling", "If you need to cancel this order or get in touch with us, please email")?> <a href='mailto:<?= $business->email?>?subject=Order Cancellation'><?= $business->email?></a> <?=get_translation_for_view("or_call", "Or call us at")?> <?= $business->phone?></p>

<table class="table table-striped table-bordered table-condensed make-responsive">
    <thead>
		<tr>
			<th width='*' style='text-align: left'><?=get_translation_for_view("location", "Location")?></th>
            <th style='width:110px;'><?=get_translation_for_view("notes", "Notes")?></th>
			<th align='center'><?=get_translation_for_view("locker", "Locker")?></th>
			<th align='center'><?=get_translation_for_view("status", "Status")?></th>
			<th style='width:110px;'><?=get_translation_for_view("date", "Date")?></th>
		</tr>
    </thead>
    <tbody>
		<? foreach ($claims as $c) :?>
			<tr>
				<td><?= $c->address ?></td>
				<td align="left">
                    <a class="pull-right edit-notes" href="#"><i class="icon icon-pencil"></i></a>
                    <span class="note-text"><?= nl2br(htmlentities($c->userNotes)); ?></span>
                    <div style="display:none;" class="form-edit-notes">
                        <form method="post" action="/account/orders/update_claim_notes/<?= $c->claimID ?>">
                            <textarea name="notes"><?= htmlentities($c->userNotes) ?></textarea>
                            <button class="btn btn-success"><?= get_translation_for_view('update_notes', "Update Notes"); ?></button>
                        </form>
                    </div>
                </td>
				<td align='center'><?= $c->lockerName ?></td>
				<td align='center'><?=get_translation_for_view("pending", "Pending")?></td>
                                <td align="center">
                                    <?=get_translation_for_view("created", "Created")?>:
                                    <?= convert_from_gmt_aprax($c->updated, $date_format) ?>
                                    <? if($c->pickupDate): ?>
                                        <br>
                                        <?=get_translation_for_view("pickup", "Pickup")?>:
                                        <?= convert_from_gmt_aprax($c->windowStart, $date_format) . " - " .
                                            get_time_from_datetime(convert_from_gmt_aprax($c->windowEnd, $date_format))
                                        ?>
                                    <? endif; ?>
                                </td>
			</tr>
		<? endforeach ; ?>
    </tbody>
</table>

<? else:?>
<p><?=get_translation_for_view("no_pending", "No Pending Orders")?></p>

<? endif; ?>
<br>
<h2><?=get_translation_for_view("current_orders", "Current Orders")?></h2>

<? if($current_orders):
$awaiting_payment = get_translation_for_view("awaiting_payment", "Awaiting Payment");

?>
<p><?=get_translation_for_view("make_notes", "To make payment arrangements, add notes to an order, or to view order details, please click on the order below")?></p>

<table class="table table-striped table-bordered table-condensed make-responsive">
    <thead>
		<tr>
			<th><?=get_translation_for_view("order_id", "Order ID")?></th>
			<th><?=get_translation_for_view("locker", "Locker")?></th>
			<th align='center'><?=get_translation_for_view("location", "Location")?></th>
			<th align='center'><?=get_translation_for_view("status", "Status")?></th>
			<th style='width:100px;'><?=get_translation_for_view("order_date", "Order Date")?></th>
            <th><?=get_translation_for_view("order_type", "Order Type")?></th>
            <th><?=get_translation_for_view("total", "Total")?></th>
			
            <th><?=get_translation_for_view("payment", "Payment")?></th>
            
            <?php if ($home_delivery_enable_order_reschedule): ?>
            <th></th>
            <?php endif; ?>
		</tr>
    </thead>
    <tbody>
		<? foreach ($current_orders as $order):
            $is_unpaid = in_array($order->orderStatusOption_id, array(7, 13));
            $is_laundy_plan = $order->locker_id == $business->prepayment_locker_id;

            // skip orders without bag that are not a laundry plan (authorizations)
            if (!$order->bag_id && !$is_laundy_plan) {
                continue;
            }
        ?>
			<tr>
				<td align='center'><a href='/account/orders/order_details/<?= $order->orderID ?>'><?= $order->orderID ?></a></td>
				<? if($is_laundy_plan): ?>
                                    <td colspan='2'><?= $order->notes?></td>
                                    <td><?= $is_unpaid ? $awaiting_payment : get_translation($order->status, "OrderStatusOptions", array(), $order->status) ?></td>
                                    <td align='center'><?= local_date($zone, $order->dateCreated, $date_format);?></td>
				<?else: ?>
                                    <td><? if(strtolower($order->lockerName)!='inprocess'){ echo $is_unpaid ? "" : $order->lockerName; }?></td>
                                    <td align='center'><?= $order->address ?></td>
                                    <td><?= $is_unpaid ? $awaiting_payment : get_translation($order->status, "OrderStatusOptions", array(), $order->status) ?></td>
                                    <td align='center'><?= local_date($zone, $order->dateCreated, $date_format);?></td>
				<?endif;?>
                                <td>
                                    <? if (isset($orderTypes[$order->orderType])): ?>
                                        <?= get_translation($orderTypes[$order->orderType], "service_types", array(), $orderTypes[$order->orderType]) ?>
                                    <? else: ?>
                                        <?= $order->orderType ?>
                                    <? endif; ?>
                                </td>
                                <td><?= format_money_symbol($this->business_id, '%.2n', $order->orderTotal); ?></td>
				<td style='width:100px;text-align: center' nowrap>
                    <? if ($order->payment=='Captured'): ?>
                        <?= get_translation("paid", "OrderPaymentStatusOption", array(), "Paid") ?>
                    <? else: ?>
                        <?= get_translation($order->payment, "OrderPaymentStatusOption", array(), $order->payment) ?>
                    <? endif; ?>
                </td>
                <?php if ($home_delivery_enable_order_reschedule): ?>
                <td>
                    <a onClick="return confirm('<?=get_translation_for_view("reschedule_confirm", "Are you sure ?")?>');" href='/account/orders/home_delivery_reschedule/<?= $order->orderID ?>'><?=get_translation_for_view("reschedule", "Reschedule")?></a>
                </td>
                <?php endif; ?>
			</tr>
		<? endforeach ; ?>
    </tbody>
</table>

<? else: ?>
<p><?=get_translation_for_view("no_orders", "No Orders")?></p>
<? endif; ?>

<br>
<h2><?=get_translation_for_view("past_orders", "Past Orders")?></h2>
<? if($past_orders):
$awaiting_payment = get_translation_for_view("awaiting_payment", "Awaiting Payment");

?>
<p><?=get_translation_for_view("make_notes", "To make payment arrangements, add notes to an order, or to view order details, please click on the order below")?></p>

<table class="table table-striped table-bordered table-condensed make-responsive">
    <thead>
		<tr>
			<th><?=get_translation_for_view("order_id", "Order ID")?></th>
			<th><?=get_translation_for_view("locker", "Locker")?></th>
			<th align='center'><?=get_translation_for_view("location", "Location")?></th>
			<th align='center'><?=get_translation_for_view("status", "Status")?></th>
			<th style='width:100px;'><?=get_translation_for_view("order_date", "Order Date")?></th>
                        <th><?=get_translation_for_view("order_type", "Order Type")?></th>
                        <th><?=get_translation_for_view("total", "Total")?></th>
			<th><?=get_translation_for_view("payment", "Payment")?></th>
		</tr>
    </thead>
    <tbody>
		<? foreach ($past_orders as $order):
            $is_unpaid = in_array($order->orderStatusOption_id, array(7, 13));
            $is_laundy_plan = $order->locker_id == $business->prepayment_locker_id;

            // skip orders without bag that are not a laundry plan (authorizations)
            if (!$order->bag_id && !$is_laundy_plan) {
                continue;
            }
        ?>
			<tr>
				<td align='center'><a href='/account/orders/order_details/<?= $order->orderID ?>'><?= $order->orderID ?></a></td>
				<? if($is_laundy_plan): ?>
                                    <td colspan='2'><?= $order->notes?></td>
                                    <td><?= $is_unpaid ? $awaiting_payment : get_translation($order->status, "OrderStatusOptions", array(), $order->status) ?></td>
                                    <td align='center'><?= local_date($zone, $order->dateCreated, $date_format);?></td>
				<?else: ?>
                                    <td><? if(strtolower($order->lockerName)!='inprocess'){ echo $is_unpaid ? "" : $order->lockerName; }?></td>
                                    <td align='center'><?= $order->address ?></td>
                                    <td><?= $is_unpaid ? $awaiting_payment : get_translation($order->status, "OrderStatusOptions", array(), $order->status) ?></td>
                                    <td align='center'><?= local_date($zone, $order->dateCreated, $date_format);?></td>
				<?endif;?>
                                <td>
                                    <? if (isset($orderTypes[$order->orderType])): ?>
                                        <?= get_translation($orderTypes[$order->orderType], "service_types", array(), $orderTypes[$order->orderType]) ?>
                                    <? else: ?>
                                        <?= $order->orderType ?>
                                    <? endif; ?>
                                </td>
                                <td><?= format_money_symbol($this->business_id, '%.2n', $order->orderTotal); ?></td>
				<td style='width:100px;text-align: center' nowrap>
                    <? if ($order->payment=='Captured'): ?>
                        <?= get_translation("paid", "OrderPaymentStatusOption", array(), "Paid") ?>
                    <? else: ?>
                        <?= get_translation($order->payment, "OrderPaymentStatusOption", array(), $order->payment) ?>
                    <? endif; ?>
                </td>
			</tr>
		<? endforeach ; ?>
    </tbody>
</table>

<? else: ?>
<p><?=get_translation_for_view("no_orders", "No Orders")?></p>
<? endif; ?>
<p>
    <input type='button' onClick="top.location.href='/account'" class='btn btn-primary' value='<?=get_translation_for_view("return_button", "Return to My Account") ?>' />
</p>

<script>
$('body').on('click', '.edit-notes', function(evt) {
    evt.preventDefault();
    $(this).hide();
    $('.note-text', this.parentNode).hide();
    $('.form-edit-notes', this.parentNode).show();
});

</script>
