        <!-- Navbar -->
        <div class="navbar navbar-fixed-top visible-phone visible-tablet hidden-desktop">
          <div class="navbar-inner">
            <div class="container-fluid">
              <a class="btn btn-navbar" data-toggle="collapse" data-target="#account_menu.nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <a class="brand" href="<?= prep_url($this->business->website_url);?>"><?php echo $this->business->companyName?></a>
              <div class="btn-group pull-right">
                
                  <?php echo isset($this->customer->username)?$this->customer->username:''?"<a class='btn dropdown-toggle' data-toggle='dropdown' href='#'> <i class='icon-user'></i>".isset($this->customer->username)?$this->customer->username:''."<span class='caret'></span></a>":'';?>
               
                
                <ul class="dropdown-menu">
                  <li><a id="login_logout" href="/logout">Sign Out</a></li>
                </ul>
              </div>
              <div id="account_menu" class="nav-collapse">
                <?=$this->load->view('business_account/account_menu_mobile');?>
                <br>
                <p style='text-align:center'>
                    <a class="btn btn-success btn-large" href="/account/orders/new_order"><i class="icon-shopping-cart icon-white"></i> Place Order Now</a>
                </p>
              </div><!--/.nav-collapse -->
            </div>
          </div>
        </div> <!-- End Navbar -->
        

<div class='container'>
    <div class='row'>
        <div class='span7'>
            <?php echo $content ?>
        </div>
        <div class='span3 hidden-phone'>
                
            <div class='alert alert-info'>
                <? if($customer->firstName==''): ?>
                    <p>Hey there... Um, I don't know your name yet. Please complete your profile so we can get to know each other.<br><a href='/account/profile'>Profile Page</a></p>
                <? else: ?>
                <p>Logged in as: <?= getPersonName($customer) ?> (<?= $customer->email?>)</p>
                <? endif; ?>
                <div style='text-align:right'>
                    <a href='/logout' class='btn btn-info btn-small login_logout'><i class='icon-user icon-white'></i> Logout</a>
                </div>
            </div>
                 
            <?
             if(is_array($sidebar)){
                foreach($sidebar as $s)
                {
                    echo $s."";
                }
             } else {
                 
                 echo $sidebar;
             }
             ?>
        </div>
    </div>
    <script type="text/javascript">
        var login_logout_button = document.getElementById("login_logout");    
        //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
        <?php if ($facebook_user_id): ?>
        login_logout_button.textContent = "Logout";
            login_logout_button.setAttribute("href", "<?= $facebook_logout_url?>");

        <?php elseif (empty($this->customer)): ?>
            login_logout_button.textContent = "Sign In";
        <?php else: ?>
            login_logout_button.textContent = "Logout";
            login_logout_button.setAttribute("href", "/logout");
        <?php endif; ?>
    </script>
</div>
