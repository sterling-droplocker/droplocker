 <?

    if($business_id==3){
        echo $this->session->flashdata('status_message'); 
    } else {
        echo $this->session->flashdata('alert');
    }
    ?>
<h2><?= get_translation_for_view("unsuscribe_title", "Remove From Mailing List") ?></h2><br>

<span class="bodyReg">

<p>
	<?= get_translation_for_view("unsuscribe_text", 
		"This will remove you from all %company_name% marketing communications including promotions, office closures and other potentially important communications.",
		array('company_name' => $business->companyName)) 
	?>
</p>

<br>
<br>


<p><?= get_translation_for_view("unsuscribe_instructions", "If you would like to proceed, please enter your email address below.") ?></p>

<form action="" method="post">
<table>
<tr>
<td valign="top" class="bodyReg">
		<label for="unsubscribe_email" style="display:inline"><?= get_translation_for_view("unsuscribe_email_label", "Email address") ?></label>
		<input id="unsubscribe_email" type="text" name="email" size="30"><br>
</td>
<td valign="top"><input type="submit" name="submit" value="<?= get_translation_for_view("unsuscribe_button_label", "Remove From List") ?>" class="<?php echo $class?>"></td>
</tr>
</table>
</form>
</span>
