<?php

$date_format = get_business_meta($this->business_id, 'standardDateLocaleFormat', '%A, %B %e');

?>
<script type="text/javascript">
$(document).ready( function() {

    $(":submit").loading_indicator();
});
</script>
<h2><?=get_translation_for_view("buy_product_plan", "Buy Product Plan")?></h2>
<form action='/account/programs/buy_product_plan' method='post'>
    <table class="table table-bordered">
            <tr>
                    <th><?=get_translation_for_view("product_plan_name", "Plan Name"); ?></th>
                    <td><?=$plan->description; ?></td>
            </tr>
            <tr>
                    <th><?=get_translation_for_view("product_plan_allowed_items_of_clothing", "Allowed items of clothing"); ?></th>
                    <td><?=(int)$plan->credit_amount; ?></td>
            </tr>
        <? if($plan->days > 0): ?>
            <tr>
                    <th><?=get_translation_for_view("duration", "Duration")?></th>
                    <td><?=get_translation_for_view("days", "%days% Days", array(
                                'days' => $plan->days,
                    )) ?></td>
            </tr>
        <? else: ?>
        <tr>
            <th><?=get_translation_for_view("end_date", "End Date")?></th>
            <td><?= convert_from_gmt_locale($plan->endDate, $date_format, $this->business_id) ?></td>
        </tr>
        <? endif; ?>
        <tr>
            <th><?=get_translation_for_view("amount", "Amount")?></th>
            <td><?=format_money_symbol($this->business->businessID, '%.2n', $plan->price)?></td>
        </tr>
        <tr>
            <th><?=get_translation_for_view("product_plan_expired_discount_percent_", "expired discount percent")?></th>
            <td><?=$plan->expired_discount_percent ?> %</td>
        </tr>
	<? if ($plan->rollover): ?>
        <tr>
            <th><?= get_translation_for_view("product_plan_rollover_items_of_clothing", "Rollover items of clothing") ?>
            <br><span style='font-size:10px;font-weight:normal'><?= get_translation_for_view("if_auto_renewal_product_plan", "(If on automatic renewal)") ?></span></th>
            <td><?= (int)$plan->rollover; ?></td>
        </tr>
	<? endif; ?>

    </table>
    <p style='padding:10px; background-color:#ffffbe'><?=get_translation_for_view("product_plan_terms", "By clicking below you agree to the laundry plan terms and conditions.")?></p>
    <p><textarea style='width:98%;height:150px;' readonly="true">
        <?= get_translation_for_view("everyting_plans_faq_text", "
    Which plan is right for me?
    Most individuals typically use about 30 lb a month. For 2 people, 60 lb/month is typical usage of wash &amp; fold. As a reference, a regular washing machine typically holds 10-12 lb.  If the plan you initially selected does not suit your needs, you can change your plan at any time.  The new plan will take affect as soon as your current plan expires.

    What about upgrades?
    No problem!  You can still select any upgrades you like on the preferences page.  Your base wash &amp; fold will be included in your plan and any upgrades such as special soap, dryer sheets, or other available upgrades will be charged separately and billed to your account on a per order basis.

    How do I switch plans?
    If the plan you initially selected does not suit your needs, you can change your plan at any time.  The new plan will take affect as soon as your current plan expires.

    How about special items such as comforters and pillows? We are still more than happy to process special items.  These are not included in the plan and will be charged separately and billed to your credit card on a per order basis.

    Is there a minimum order size?
    Since we wash every customer`s clothes separately, each order is separated in to at least two loads.  Hence the reason for a 12 lb minimum order size.  It is fine to send in less, and we will just deduct 12 lb from your available laundry plan balance for the order.

    What happens if I go over the pounds in my plan?
    No problem.  Should your usage exceed the amount of your plan, you will be charged the standard per pound rate (currently $) for any pounds over your plan amount.

    How long is the plan for?
    The laundry plan is for the term specified in your plan, from the date you sign up for the plan.  After 11:59 pm on the end date, any remaining balance in your account will be removed (less any rollover).

    What happens when my plan expires?
    Your monthly laundry plan will automatically renew at 11:59 pm on the end date of your plan. to cancel the plan click on the button saying Turn Off Automatic Renewal

    What if I don`t use all my allotted pounds?
    Don`t worry, we`ve got you covered. As long as you are setup for automatic renewal, unused pounds, up to a maximum of the plan`s rollover pounds, will be added to the automatically renewed plan for the next month.

    Is there a cut off date / time?
    All transactions are based on the day your order is picked up.  That means that if you place your order on the 15th, but it is not picked up until the 16th, and you plan expires on the 15th, your order will be credited to the next month`s balance.

    Can I combine my laundry plan with other discounts?
    Sorry, but our laundry plans are such a great deal that they may not be combined with any other offers.  Other discounts and offers will be applied to any charges not associated with your laundry plan (upgrades, dry cleaning, etc.).  Discounts may not be used to purchase laundry plans.

    Are dry cleaning and laundered shirts included
    These plans are only for wash &amp; fold orders.  Dry cleaning and laundered shirts are charged per piece as part of our dry cleaning service.

    Are plan valid for wholesale customers?
    The monthly laundry plans are not available for wholesale or commercial accounts.

    How do I get a refund?
    Sorry, but no refunds will be issued for partially or completely unused laundry plans.

    How do I cancel my plan?
    Automatic renewal of laundry plans may be terminated at any time by clicking the button labeled Turn Off Automatic Renewal.  The plan will remain active until 11:59 pm of the last day of your plan, at which time your plan will not be automatically renewed.

    Are there any additional charges?
    Laundry plans cover the cost of basic wash and fold.  Any extra services, such as detergent upgrades or home delivery, are additional and will be charged per order.
    ")?>
    </textarea></p>
    <p>
        <?=form_hidden("businessLaundryPlanID", $plan->businessLaundryPlanID) ?>
            <input type='submit' name='submit' value = "<?= get_translation_for_view("i_agree", "I Agree. Sign Me Up!") ?>" class='btn btn-primary' />
    </p>
</form>
