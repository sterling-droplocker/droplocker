<?php
/**
 * Expects at least the following content variables
 * 
 * starch
 * starchPrefs
 * $location_services
 * $customer_locations
 * $service_types_by_key
 * $service_types_render_order
 * post
 */

if (!isset($post)) {
    $post = array();
}

$selected_location = empty($post['location_id']) ? $default_location : $post['location_id'];
?>
<style type="text/css">
@import url("/css/font-awesome-4.1.0/css/font-awesome.min.css");

.location-list {
    max-width: 400px;
    max-height: 24em;
    overflow-y: auto;
}

.location-item {
    padding: 10px;
    color: #999;
    cursor: pointer;
    border: 1px solid transparent;
}

.location-item h3 {
    margin-top: 0;
    margin-bottom: 0;
    line-height: 1;
}

.location-item.active {
    border: 1px solid #333;
    color: #333;
}

.location-icon {
    float: left;
    width: 40px;
    margin-right: 10px;
    height: 40px;
    text-align: center;
    font-size: 40px;
    color: #fff;
    text-shadow: -1px 0 0 #999, 1px 0 0 #999, 0 1px 0 #999, 0 -1px 0 #999;
}

.active .location-icon {
    text-shadow: -1px 0 0 #333, 1px 0 0 #333, 0 1px 0 #333, 0 -1px 0 #333;
}


.group-title a {
    font-weight:  bold;
}

.dropdown-menu .sub-option a {
    padding: 3px 15px 3px 25px;
}

.subnote {
    font-style: italic;
}

</style>

<script type="text/javascript">
var location_services  = <?= json_encode($location_services) ?>;
var customer_locations = <?= json_encode($customer_locations) ?>;
var service_types      = <?= json_encode($service_types_by_key) ?>;
var service_order      = <?= json_encode($service_types_render_order) ?>;
var post = <?= json_encode($post) ?>;
var empty_option      = <?= json_encode(get_translation_for_view("empty_dropdown_option", "--SELECT--")); ?>;

var customer_locations_descriptions = {
        "Units": <?php echo json_encode(get_translation_for_view("customer_locations_descriptions_units", "Units")) ?>,
        "Lockers": <?php echo json_encode(get_translation_for_view("customer_locations_descriptions_lockers", "Lockers")) ?>
};

/**
 * Populates the locker dropdown menu
 * @param int location_id
 * @param bool is_pickup
 * @param function callback
 * @returns {undefined}
 */
var location_swap =  function(location_id, is_pickup, callback) {
    $("#locker_dropdown").empty();

    if (!is_pickup) {
        $('<option>').text(empty_option).appendTo("#locker_dropdown");
        if (typeof(customer_locations[ location_id ]) != 'undefined') {
            var lockers = customer_locations[ location_id ]['lockers'];
             $.each(lockers, function(index, locker) {
                 if (typeof(locker) == 'object') {
                    $("<option>").val(locker['id']).text(locker['name']).appendTo("#locker_dropdown");
                }
             });
        }
    } else {
        var locker = customer_locations[ location_id ]['pickup_locker'];
        $("<option>").val(locker['id']).text(locker['name']).appendTo("#locker_dropdown");
    }
     
     location_services_swap( location_id );

     selectValueSet('locker_dropdown', $("#previous_selected_locker_id").val());
     if(typeof callback == 'function'){
         callback();
     }

    changeLocationDescription( location_id );
};

/*
* Changes selector label based on location type
 */
var changeLocationDescription = function ( location_id ){
    var lockerType = customer_locations[location_id].lockerType;

    if (customer_locations_descriptions.hasOwnProperty(lockerType))
    {
        $("#locker_label").text(customer_locations_descriptions[lockerType]);
    } else {
        $("#locker_label").text(customer_locations_descriptions["Lockers"]);
    }
};


var location_services_swap = function( location_id ) {
    //check to see if we have any set location services before we swap, 
    //if not, leave the regular business settings
    var serviceCheck = location_services[ location_id ] || [];
    $('#location_service_types').html('');

    if( $.makeArray( serviceCheck ).length > 0 ){
        for( var i in service_order ){ 
            var hasService = location_services[location_id][ service_order[ i ] ] || false;
            if( hasService != false ){
                var serviceTypeID       = service_order[ i ];
                var serviceDisplayName   = service_types[ serviceTypeID ]['displayName'];

                var serviceCheckboxLabel = $('<label class="checkbox" />');
                var serviceCheckbox      = $('<input type="checkbox" class="service_type" name="service_type[]" value="' + serviceTypeID + '" />');
                if (post.service_type instanceof Array && $.inArray(serviceTypeID, post.service_type) !== -1) {
                    serviceCheckbox.attr("checked", true);
                }
                serviceCheckbox.appendTo(serviceCheckboxLabel);
                $('<span>' + serviceDisplayName + '</span>').appendTo(serviceCheckboxLabel);
                serviceCheckboxLabel.appendTo('#location_service_types');
            }
        }
        
    } else {
        //throw the default ones for the business in there
        for( var s in service_order ) {
            var serviceTypeID = service_order[ s ].toString();
            var serviceDisplayName   = service_types[ serviceTypeID ]['displayName'];
            var serviceCheckboxLabel = $('<label class="checkbox" />');
            var serviceCheckbox      = $('<input type="checkbox" class="service_type" name="service_type[]" value="' + serviceTypeID + '" />');
           
            if (post.service_type instanceof Array && $.inArray(serviceTypeID, post.service_type) !== -1) {
                serviceCheckbox.attr("checked", true);
            }
            serviceCheckbox.appendTo(serviceCheckboxLabel);
            $('<span>' + serviceDisplayName + '</span>').appendTo(serviceCheckboxLabel);
            serviceCheckboxLabel.appendTo('#location_service_types');
        }
    }
    
    
};


/**
*  This is a client side validation function, could 
*  probably substitute this with a better jquery one or something

 * @returns {Boolean} - bound to onSubmit/onClick for the form
 * */
function verify_service(){
    if(!$("#locker_dropdown").val() || $("#locker_dropdown").val() == empty_option){
            alert("<?= get_translation_for_view("locker_validation_error_message", "Please select a locker") ?>");
            return false;
            }

    var values = $('input:checkbox:checked.service_type').map(function () {
          return this.value;
        }).get();
        if(values==''){
        alert("<?= get_translation_for_view("service_type_validation_error_message", "Please select a service type")?>");
        return false;
    }
}

var isNumber = function(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

$(document).ready(function() {
    var query = "";

    // if using location list
    if ($('.location-list').length) {
        $('.location-item').click(function(evt) {
            evt.preventDefault();

            var location_id = $(this).attr('data-location_id');
            var is_pickup = $(this).is('.pickup');

            $('#location_id').val(location_id);
            location_swap(location_id, is_pickup);

            if (is_pickup) {
                $('#select_locker').hide();
                $('#select_pickup').show();
                $('#select_pickup_options').html('<img src="/images/loading.gif" style="max-height:50px;" />');
                $.get('/account/orders/home_pickup/' + location_id + query, function (response) {
                    if ($('.location-list .location-item.active').is('.pickup')) {
                        $('#select_pickup_options').html(response);
                    } else {
                    }
                });

            } else {
                $('#select_locker').show();
                $('#select_pickup').hide();
                $('#select_pickup_options').html('');
            }

            $('.location-item.active').removeClass('active');
            $(this).addClass('active');
        });

        var location_id = $('#location_id').val();
        $('.location-item[data-location_id='+location_id+']:eq(0)').click();
    // else, using location drop down
    } else {

        $('#location_id').change(function() {
            var location_id = $('#location_id').val();
            location_swap(location_id, false);
        });

        var location_id = $('#location_id').val();
        location_swap(location_id, false);
    }

    <?php if ($show_quantity_selector): ?>
    $('#quantity_selector').find("select").first().change(function(){
        var location_id = $('#location_id').val();
        query = "?quantity="+$(this).val();
        $('.location-item[data-location_id='+location_id+']:eq(0)').click();
    });
    <?php endif; ?>
});
</script>

<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("place_new_order", "Place new Order")?>
  </li>
</ul>

<h2 class='page-header'> <?=get_translation_for_view("place_an_order", "Place an Order")?> </h2>

<?= $this->session->flashdata('alert')?>


<? include 'notice.php'; ?>

<!-- The following input is used to keep track of the locker that the user selected and preselect that locker in the locker menu using client side javascript. We use client side javascript for preselecting the locker ID because we populate the locker select options dynically based on the currently selected locations -->
<?= form_input(array("id" => "previous_selected_locker_id", "value" => set_value("locker_id"), "style" => "display: none;" )) ?>
<?= form_input(array("id" => "locations", "value" => json_encode($customer_locations), "style" => "display: none;")) ?>

<form action='/account/orders/confirm_order' name='form' id="new-order-form" method="post" class='form-horizontal'>
    <div class="control-group">
        <label class="control-label" for="location_id">
            <span class="big"><?=get_translation_for_view("location", "Pickup Location")?></span>
            <br><?= get_translation_for_view("select_one", "Select One") ?>
        </label>
        <div class="controls">
            <?php if (!$show_locations_map): ?>
                <?= form_dropdown('location_id', $location_dropdown, $selected_location, 'id="location_id"'); ?>
            <? else: ?>
                <?php if($customer_locations): ?>
                    <input type="hidden" name="location_id" id="location_id" value="<?= $selected_location ?>">

                    <div class="location-list">
                        <? if (!empty($pickup_location)): ?>
                        <div class="location-item pickup" id="location_home" data-location_id="<?= $pickup_location->locationID ?>">
                            <span class="location-icon fa fa-home"></span>
                            <h3><?= get_translation_for_view("home", "Home Delivery") ?></h3>
                            <p><?= $customer->address1 ?> <?= empty($customer->address2)?"":$customer->address2; ?></p>
                        </div>
                        <? endif; ?>

                        <? foreach($location_dropdown as $locationID => $name):
                            $location = $customer_locations[$locationID];
                        ?>
                        <div class="location-item" id="location_<?= $locationID ?>" data-location_id="<?= $locationID ?>">
                            <span class="location-icon fa fa-map-marker"></span>
                            <h3><?= $location['title'] ? $location['title'] : $location['address'] ?></h3>
                            <p><?= $location['address'] ?></p>
                        </div>
                        <? endforeach; ?>
                    </div>

                    <a href='/account/locations/'><?= get_translation_for_view("update_locations_button", "Update Locations") ?></a>

                <?php else: ?>
                    <a href="/account/locations"><?=get_translation_for_view("add_location", "Please add Location")?></a>
                <?php endif ?>
            <? endif; ?>
        </div>
    </div>

    <div class="control-group" id="select_locker">
        <label class="control-label" id='locker_label' for="lockers">
            <?=get_translation_for_view("lockers", "Lockers")?>
        </label>
        <div class="controls" id='locker_holder'>
            <select name="locker_id" id="locker_dropdown"> </select>
        </div>
    </div>

    <?php if ($show_quantity_selector): ?>
    <div class="control-group">
        <label class="control-label" for="quantity"><?=get_translation_for_view("quantity_selector", "Please, select the quantity of bags for your order")?> <br /> <small></label>
        <div class="controls" id="quantity_selector">
        <select name="quantity">
            <?php foreach($quantity_options as $q): ?>
                    <option value="<?php echo $q; ?>"><?php echo $q; ?></option>
            <?php endforeach; ?>
        </select>
        </div>
    </div>
    <?php endif; ?>

    <div class="control-group" id="select_pickup" style="display:none;">
        <label class="control-label" id='locker_label' for="lockers">
            <span class="big"><?=get_translation_for_view("schedule", "Schedule")?></span>
            <br><?= get_translation_for_view("pickup", "pickup") ?>
        </label>
        <div class="controls" id='select_pickup_options'>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="lockers"><?=get_translation_for_view("service_type", "Service Type")?> <br /> <small> <?=get_translation_for_view("select_all_that_apply", "Select All That Apply")?> </small> </label>
        <div class="controls" id="location_service_types">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="notes"><?= get_translation_for_view("notes", "Notes") ?></label>
        <div class="controls">
            <textarea id='notes' name='notes' placeholder='<?= get_translation_for_view("optional_placeholder", "Optional") ?>'><?= set_value('notes') ?></textarea>    
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="promo_code"><?=get_translation_for_view("promo_code", "Promotional Code")?></label>
        <div class="controls">
            <input id='promo_code' type='text' name='promotional_code' value='<?= set_value('promotional_code')?>' />  
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <input type='submit' value='<?=get_translation_for_view("next_button", "Next")?>' id='submit_button' onClick='return verify_service()' name='submit' class="btn btn-primary btn-large"/>
        </div>
    </div>
    <?php if(isset($locker_id)):?>
    <input type='hidden' value='<?= $locker_id?>' id='locker_id' name='locker_id' />
    <?php endif;?>
    
</form>

