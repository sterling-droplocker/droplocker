<h2 class='page-header'> <?=get_translation_for_view("billing", "Billing")?> </h2>

<div class='row'>
    <div class='span7'>
        <div class='alert alert-error'>
            <h3><?=get_translation_for_view("not_available", "Credit Card Processing is Not Available")?></h3>
            <p><?=get_translation_for_view("sorry", "Sorry for the inconvenience, but our processing is currently unavailable.")?></p>
        </div>
    </div>
</div>
