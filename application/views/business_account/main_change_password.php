<div class='row'>
    <div class='span8 offset2'>
        <?php echo isset($error)?$error:'';?>
        <h2 class='page-header'><?=get_translation_for_view("change_password", "Change Password")?></h2>
        <?= $this->session->flashdata('alert');?>
    </div>
    <div class='span4 offset2'>
        <div class='alert alert-success'>
				<h3><?=get_translation_for_view("success_alert", "Success!")?></h3>
				<?=get_translation_for_view("confirm_correct", "Confirmation code correct. Please use the form below to update your account password")?></div>
        <?= validation_errors();?>
        <form method='post' action=''>
            <div class="control-group">
              <label class="control-label" for="password"><?=get_translation_for_view("password", "Password")?></label>
              <div class="controls">
                <input size='30' type='password' id='password' name='password' />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="password_confirm"><?=get_translation_for_view("password_confirm", "Password Confirm")?></label>
              <div class="controls">
                <input size='30' type='password' id='password_confirm' name='password2' />
              </div>
            </div>
            <input class='btn btn-primary' type='submit' value='<?=get_translation_for_view("save_new_password", "Save New Password")?>' name='submit' /> &nbsp;&nbsp;&nbsp;&nbsp;<a href='/login'><?=get_translation_for_view("back_to_login", "Back to Login")?></a>
        </form>
    </div>
    <div class='span4'>
    </div>
</div>
 
