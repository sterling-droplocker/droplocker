<h2 class='page-header'><?=get_translation_for_view("verify_email", "Verify New Email Address")?></h2>
<div class='row'>
	<div class='span6 offset1'>
	    <div class='alert alert-<?= $class ?>'><h3><?= $message?></h3>
            <br><p><a class='btn btn-info' href='/account'><?=get_translation_for_view("my_account", "My Account")?></a></p>
        </div>
	</div>
</div>
