<?php

//this creates a well structured tree that we can easy grab values from
$js_possible_preferences = array();
foreach($businessPreferences as $title => $modulePreferences) {
    foreach($modulePreferences as $preferenceSlug => $preference) {
        foreach ($preference['options'] as $productID => $label) {
            $js_possible_preferences[$preferenceSlug][$productID] = $label;
        }
    }
}


?>

<h2><?=get_translation_for_view("wash_preferences_title", "Wash Preferences")?></h2>


<? foreach ($businessPreferences as $title => $preferences): ?>
    <h3><?= get_translation($title, "service_types", array(), $title)?></h3>
    <? foreach($preferences as $preferenceSlug => $preference ): ?>
    <div class="preference">
        <div class='preference-name'><?= $preference['text']; ?></div>
        <div class='preference-value' id="preference_<?=$preferenceSlug;?>">
            <? if (!empty($customerPreferences[$preferenceSlug])): ?>
                <?= $preference['options'][$customerPreferences[$preferenceSlug]]; ?>
            <? else: ?>
                <?= get_translation_for_view("none", "None") ?>
            <? endif; ?>
        </div>
        <div style='clear:both' ></div>
    </div>
    <? endforeach; ?>
<? endforeach; ?>


<? if (empty($read_only)): ?>
<div style='padding:5px;'>
    <a id="prefs_button" href='#TB_inline?inlineId=service_types_modal&height=600&width=600' class='thickbox btn btn-small'><?=get_translation_for_view("edit_button", "Edit Cleaning Preferences")?></a>
</div>
<? endif; ?>

<script type="text/javascript" src="/js/functions.js"></script>

<? if (empty($read_only)): ?>

<div style="padding:0px 10px;text-align:left;display:none;" id="service_types_modal">
    <h2><?=get_translation_for_view("customer_preferences_title", "Customer Preferences")?></h2>
    <form action='' method='post' id="preference_update_modal" class="form form-horizontal">

        <? foreach($businessPreferences as $title => $modulePreferences): ?>
            <h3><?= get_translation($title, "service_types", array(), $title)?></h3>
            <div class='well'>
                <? foreach($modulePreferences as $preferenceSlug => $preference):
                    if (count($preference['attr']) <= 1) {
                        continue;
                    }
                    $selected = isset($customerPreferences[$preferenceSlug]) ? $customerPreferences[$preferenceSlug] : null;
                ?>
                    <div class='control-group'>
                        <label class='control-label' for='select'><?= $preference['text']?></label>
                        <div class='controls'><?= form_dropdown("preferences[{$preferenceSlug}]", $preference['options'], $selected, "id='{$preferenceSlug}'"); ?></div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endforeach; ?>

        <h3> <?= get_translation_for_view("account_notes_title", "Account Notes")?> </h3>
        <p><?=get_translation_for_view("account_notes", "Account notes are added to every order. You will also be able to provide additional notes while placing orders")?></p>
        <textarea id="prefNotes" style="width:100%;height:100px;border:1px solid #ccc;" name="notes"><?= $customer_notes?></textarea>

        <br />
        <br />
        <button class="btn btn-primary btn-large" id="updatePrefModalButton"><?=get_translation_for_view("update_button", "Update")?></button>
    </form>

</div>

<script type="text/javascript">
	var height = Math.min($(window).height() * .9, $('#service_types_modal').height());
	if (height > 700) {
		height = 700;
	}
	$('#prefs_button').attr('href', '#TB_inline?inlineId=service_types_modal&height='+height+'&width=600');
	</script>

<script type="text/javascript">
var preferences = <?=json_encode($js_possible_preferences);?>;

$(function(){
    $('#updatePrefModalButton').click(function(e) {
        e.preventDefault();
        var sendData = setupPostData();
        $.post('/account/profile/update_preferences_ajax', sendData, function(res) {
            var response = JSON.parse(res);
            if(response.status === 1) {
                response.preferences = response.preferences || [];
                $.each(response.preferences, function(slug, val) {
                    if (typeof(preferences[slug]) != 'undefined') {
                        $('#preference_' + slug).html(preferences[slug][val]);
                    }
                });
            }
            window.tb_remove();
        }).error( function(){
            //error reporting here
        });
    });

});

/**
 * Creates the object we're going to use with the ajax POST
 */
var setupPostData = function(){
    var noteVal = $('#prefNotes').val();
    var postData = {
        notes : noteVal
    };

    var formFields = $('#preference_update_modal select');
    var totalFields = formFields.length;
    for( var index = 0; index < totalFields ; index++ ){
        var fieldSelector = $( formFields[ index ] ).attr('name');
        var fieldValue = $( formFields[ index ] ).val();
        postData[ fieldSelector ] = fieldValue;
    }
    return postData;
};
</script>

<? endif; //end of read_only check ?>
