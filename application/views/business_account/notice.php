<?php
$runTime = strtotime(convert_from_gmt_aprax(date('Y-m-d H:i:s'), "Y-m-d H:i:d"));
$endTime = strtotime("2014-12-12 08:00:00");
$holiday_language = '<div style="text-transform:none !important; font-size: 18px; line-height: 1.2;">Please be advised, we are experiencing delays in picking up and returning orders today due to the storm hitting San Francisco. We will do our best to have all orders delivered in a timely manner. Thank you.</div>';
if($runTime < $endTime and $this->business_id == 3):?>
<div style='text-align:center; padding:10px;text-transform:none; background: #ffc; border: solid 2px #cc6; margin-bottom: 2em;'>
<?=$holiday_language;?>
</div>
<?endif;?>