<style type='text/css'>
    #map_canvas img {
      max-width: none;
    }
    p {
      font-family:Arial,Helvetica;
    }
</style>
<script src="/js/StyledMarker.js" type="text/javascript"></script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<?=get_business_gmaps_api_key($business_id);?>"></script>
<script type="text/javascript">
    
    var customerLatLon              =  <?= json_encode($customerLatLon);?>;
    var customerAddress             =  <?= json_encode($customerAddress);?>;
    var customerLocation            =  {};
    var existingCustomerLocations   =  <?=$json_customer_locations;?>;
    var customerDefaultDistance     = .2;       //in miles
    
    var defaultBusinessAddress      = "<?=$businessAddress;?>";                 //for map - post filtering
    var defaultBusinessState        = '<?=$businessState;?>';
    var defaultBusinessCity         = '<?=$businessCity;?>';
    var defaultBusinessZip          = '<?=$businessZip;?>';
    var defaultBusinessLatLon       = {};                                       //make latlon object
    
    // if they have a lat/lon, don't geocode here, just pull it
    var businessLat                 = '<?=$businessLat;?>';
    var businessLon                 = '<?=$businessLon;?>';

    var businessNeighborhoods       = {}; //pull these via ajax  businessNeighborhoodsJSON() 
    var locationsJSON               = {};                //pull these via ajax locationsJSON()
    var cityLocationsJSON           = {};                 //pull these via ajax cityLocationsJSON()
    var neighborhoodsJSON           = {};      //pull these via ajax neighborhoodsJSON()
    
    var locationsCitySelect         = {};
    var filterCitySelectEl          = '#filter_city_select';
    var filterNeighborhoodSelectEl  = '#filter_neighborhood_select';
    var searchButtonEl              = '#locations_search_button';
    
    var searchMarkerRadius          = null;
    
    var filterResults   = [];         //for this we'll use the location ID as the key
    var locationMarkers = [];         //tracks location markers

    var locationsMap;
    var locationsMapOptions = {         //google maps object init options
                                zoom: 12,
                                center: new google.maps.LatLng( customerLatLon.lat, customerLatLon.lon ),
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                              };
                              
    var infoWindow;                         

    var hasPublic             = true;

    var locationsGeocoder;              //location geocoder opbject

    var locationRadius;
    var circleRadius          = 300;  
    var locationRadiusOptions = { radius : circleRadius };
    
    var totalCities = 0;
    
    //screening public/private
    var acceptable_publics = ['Yes','yes','YES','public','Public'];
    
    //form select creation for cities
    var setLocationsObject = function(callback){
        var countUsable = 0;
        for( var locIndex in locationsJSON ){                   //for filtering, only use public to populate
            var selectCity = locationsJSON[ locIndex ]['city'];
            if( acceptable_publics.indexOf( locationsJSON[locIndex]['public'] ) >= 0 ){ //check against array, acceptable_publics            
                locationsCitySelect[ selectCity ] = selectCity;
                countUsable++;
            }
        }
        
        //this is a hack inccase they have NO PUBLIC LOCATIONS
        if( countUsable <= 0 ){
           hasPublic = false;   //set the flag that we dont have any public locations
           for(var unUsableLocIndex in locationsJSON){
                var selectCity = locationsJSON[ unUsableLocIndex ]['city'];
                locationsCitySelect[ selectCity ] = selectCity;
            }
        }
        
        //populate it
        totalCities = setCitySelectFilter();
    };

    //form select creation continued - returns number of cities
    var setCitySelectFilter = function(){
        //populate the select dropdowns via json_locations   
        var cityCount = 0;
        for( var sel in locationsCitySelect ){
            $( filterCitySelectEl ).append(
                $('<option />').val( sel ).html( locationsCitySelect[ sel ] )
            );        
            cityCount++;
        }
        
        return cityCount;
    };

    var runCityFilter = function(){    
       $('#address_search_field').val('');
    
       var cityVal  = $( filterCitySelectEl ).val();
       
       $('#locations_result_table_body').html('');   //wipe out results;
       $('#filter_neighborhood').fadeOut();
       $('#filter_results').hide();  

       $( filterNeighborhoodSelectEl ).html('<option>Select Neighborhood</option>');
        //we have this already setup, so just use the cityVal as a key :)
       
       //wipe out filter results and populate
       filterResults = [];
       
       //clear map
       clearMarkers();
       for (var j in cityLocationsJSON[ cityVal ] ) {      
           
           //check for public
           if( acceptable_publics.indexOf( cityLocationsJSON[ cityVal ][ j ]['public'] ) >= 0 || !hasPublic ){
               
                rowResultRender( cityLocationsJSON[ cityVal ][ j ], function(){
                    populateNeighborhoods( cityLocationsJSON[ cityVal ][ j ]['locationID'] );   
                });

                filterResults.push( cityLocationsJSON[ cityVal ][ j ] ); 
               dropMarkerPostFilter( locationsJSON[ cityLocationsJSON[ cityVal ][ j ]['locationID'] ]);
                
                $('#filter_results').show();   
           }
       }
       resetMapBoundsFromMarkers(); 
    };
    
    var runNeighborhoodFilter = function(){
        $('#address_search_field').val('');
    
        $('#locations_result_table_body').html('');   //wipe out results;
        $('#filter_results').hide();  
        
        var neighborhood = $(filterNeighborhoodSelectEl).val();
        var city = $( filterCitySelectEl ).val();
        
        //wipe out results and repopulate
        filterResults = [];

        //clear map
        clearMarkers();
       
        if( neighborhood !== undefined && neighborhood !== '' && neighborhood !== "Select Neighborhood" ){
            
            for( var k in neighborhoodsJSON[ neighborhood ] ){
               if( cityLocationsJSON[ city ][ neighborhoodsJSON[ neighborhood ][ k ] ] !== undefined ){
                    var temp_loc_id = neighborhoodsJSON[ neighborhood ][ k ];
               
                    filterResults.push( cityLocationsJSON[ city ][ temp_loc_id ] );
                    dropMarkerPostFilter(locationsJSON[ temp_loc_id ]);
                    rowResultRender( cityLocationsJSON[ city ][ temp_loc_id ] );   
                    $('#filter_results').show();    
               }
            }
            resetMapBoundsFromMarkers();
            
        }else{
            runCityFilter();
        }
    };

    var renderCustomerLocations = function(){        
   
        if( $.isEmptyObject( existingCustomerLocations ) === false ){
            $('#customer_locations').fadeIn();
        }
    
        $('#existing_locations').html('');  //clear out locations
        
        $.each(existingCustomerLocations, function(ind,loc){        
            var rowEl   =  $('<tr></tr>');
            var column1 =  $('<td>' + loc.address + '</td><td>' + loc.name + '</td>');
            var column2 =  $('<td align="center" style="width:25px"><a class="del_button" href="javascript:;"  id="customer_location_' + loc.locationID + '"><img src="/images/icons/cross.png" class="delete_button" /></a></td>');

            $(column1).appendTo(rowEl);
            $(column2).appendTo(rowEl);

            $(rowEl).appendTo('#existing_locations');
            
            $( '#customer_location_' + loc.locationID ).bind('click', deleteLocation );
        });
    };

    var rowResultRender = function( cityLocation, callback ){  
           cityLocation = cityLocation || '';
           locationsJSON[cityLocation.locationID] = locationsJSON[cityLocation.locationID] || ''; 
           if( cityLocation != '' && locationsJSON[cityLocation.locationID] != '' ){
               
                var newLocationRow = '<tr style="display:none;">';
                newLocationRow       += '<td>' + cityLocation.address + '</td>';
                newLocationRow       += '<td>' + locationsJSON[cityLocation.locationID]['name'] + '</td>';
                
                var bindDelete = false;
                if( $.isEmptyObject( existingCustomerLocations[cityLocation.locationID] ) ){
                    newLocationRow += '<td><a class="add_button" id="location_result_' + cityLocation.locationID + '" href="javascript:;"><img src="<?= CDNIMAGES?>icons/add.png" /></a></td>';
                }else{
                    bindDelete = true;
                    newLocationRow += '<td><img src="/images/icons/accept_disabled.png" class="delete_button" alt="Already added!" title="Already added!" /></td>';
                }
                newLocationRow    += '</tr>';

                $( newLocationRow ).appendTo('#locations_result_table_body').fadeIn();       

                //bind the click to add
                $('a[id=location_result_' + cityLocation.locationID + ']').bind('click',addLocation);

                if( typeof callback == 'function'){
                    callback();
                }
           }
    };    

    var noResultRender = function(){
        $('#locations_result_table_body').html('');
        $('#filter_results').show();
        var noResultRow = $('<tr></tr>');
        $('<td colspan="3">No Results Found</td>').css('background-color','#ff8888').appendTo( noResultRow );
        
        $(noResultRow).appendTo('#locations_result_table_body').fadeIn(); 
    };

    //the select options - run this AFTER city is changed and we have a positive match - correlate the location with the neighborhoods/ we are FILTERING...
    var populateNeighborhoods = function( locationId ){
        if( businessNeighborhoods[ locationId ] !== undefined ){
            $.each( businessNeighborhoods[ locationId ], function( index, el ){
                //check pre-existing dropdown - so no duplicates
                var potentialNeighborhood = $('option[value="' + el.neighborhood + '"]').val();
                if( potentialNeighborhood == undefined ){
                    $('<option />').val( el.neighborhood ).text( el.neighborhood ).appendTo( filterNeighborhoodSelectEl ); 
                }
            });
            $('#filter_neighborhood').fadeIn();
        }
    };

    //lets do an autocomplete on city - then we'll keep the dropdown for neighborhoods

    //map update methods here - 
    var initializeMap = function(callback){
        //initialize the map
        locationsMap = new google.maps.Map( document.getElementById("map_canvas"), locationsMapOptions );

        var legend = document.createElement('div');
        legend.id = 'map_legend';
        legend.setAttribute("style", "padding:5px;margin: 0 5px 5px 0;background: #fff;");
        var content = [];
        content.push('<div><div class="color" style="border:1px solid #000; width:10px; height:10px; float:left; margin-right: 5px; background:#6ed0f7"></div> <?= get_translation_for_view("private", "For Residents Only"); ?></div>');
        content.push('<div><div class="color" style="border:1px solid #000; width:10px; height:10px; float:left; margin-right: 5px; background:#56ff56"></div> <?= get_translation_for_view("public", "Public"); ?></div>');
        legend.innerHTML = content.join('');
        legend.index = 1;
        locationsMap.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);


        //default business location
        var geoAddressString = defaultBusinessAddress + ', ' + defaultBusinessCity + ', ' + defaultBusinessState;
        locationsGeocoder    = new google.maps.Geocoder();
        infoWindow           = new google.maps.InfoWindow();
        
        //wait to load the autocomplete for when the map has been loaded
        google.maps.event.addListenerOnce( locationsMap, 'idle', function(){
                //get latlng for business location and setup geocoder
                
                //check to see if we have lat/lon from above, otherwise geocode the address
                businessLat = ( businessLat != 0 ) || '';
                businessLon = ( businessLon != 0 ) || '';
                
                if(businessLat != '' && businessLon != ''){         //if we have business lat/lon, use it
                    defaultBusinessLatLon =  new google.maps.LatLng( businessLat, businessLon );
                    google.maps.event.trigger( locationsMap, 'resize' );                 
                    locationsMap.setCenter( defaultBusinessLatLon ); 
                    initializeMapSearch();
                
                }else{                                              //if we dont' have business lat/lon, try to geocode
                    locationsGeocoder.geocode( { address : geoAddressString }, function( res, status ){
                        defaultBusinessLatLon = res[0]['geometry']['location'];
                        google.maps.event.trigger( locationsMap, 'resize' ); 
                        defaultBusinessLatLon = defaultBusinessLatLon || null;
                        if(defaultBusinessLatLon != null ){
                            locationsMap.setCenter( defaultBusinessLatLon ); 
                        }
                        initializeMapSearch();
                        
                    });
                }
                
                if(typeof callback == 'function'){
                    callback();
                }
        });
    };

    //this should run through the current marker array and calculate the new bounds, then re-center the map according to those bounds.
    var resetMapBoundsFromMarkers = function(){
        var bounds = new google.maps.LatLngBounds();
        //  Go through each...
        var LtLgLen = locationMarkers.length;
        
        for (var i = 0; i < LtLgLen; i++) {
          //  And increase the bounds to take this point
          bounds.extend( locationMarkers[i]['position'] );
        }
        
        //  Fit these bounds to the map
        locationsMap.fitBounds (bounds);
        if( LtLgLen < 2 ){
            locationsMap.setZoom(12);
        }
    };

    var resetMapBoundsFromCircle = function(){
        locationRadius = locationRadius || null;
        if( locationRadius !== null ){
          //  var bounds = locationRadius.getBounds();
           // locationsMap.setZoom(12);
            locationsMap.fitBounds( locationRadius.getBounds() );
        }
    };

    // pass this the location object / array whatever and it'll create a marker for it. 
    // we already geocode in here, so don't worry, just pass the object with basic address details
    var dropMarkerPostFilter = function( locationData ){

        var color = "#6ed0f7"; //cyan  (private)
        if (acceptable_publics.indexOf( locationData['public'] ) >= 0) {
			color = "#56ff56"; // green (public)
		}

		if (locationData.name == "active") {
			color = "#CC0000";
		}

/*
        var color = "#6ed0f7";
	switch(locationData.name){
	 	case "Kiosk":
	 		color = "#56ff56";
	 	break;
	 	case "Corner Store":
	 		color = "#3698fb";
	 	break;
	 	case "active":
	 		color = "#CC0000";
	 	break;
	}
*/
        var locLat = locationData.lat || '';
        var locLon = locationData.lon || '';
        //if for some reason we don't have lat lon in the location record, geocode it

        if( locLat == '' || locLon == ''){
            var geoAddressString = locationData.address + ', ' + locationData.city + ', ' + locationData.state + ' ' + locationData.zip ;
            locationsGeocoder.geocode( { address : geoAddressString }, function( res, status ){
                 if( res[0] != undefined && res.length > 0){
                     var locationLatLon = res[0]['geometry']['location'];

                     //should bind a click event to the marker here.
                     //styled marker here
                    var styleIcon = new StyledIcon(StyledIconTypes.MARKER,{color:color});    
                    var marker = new StyledMarker({styleIcon:styleIcon,position:locationLatLon,map:locationsMap});     
                    //var marker = new google.maps.Marker( { position : locationLatLon, map : locationsMap, title : "stuff" } );
                    
                    google.maps.event.addListener(marker, 'click', function() {
                        locationsMap.setCenter( locationLatLon );
                        google.maps.event.addListener( infoWindow, 'domready', function(){
                            $( '#location_pin_' + locationData.locationID ).bind('click', addLocation );
                        });
                        infoWindow.setContent(locationData.info); 
                        infoWindow.open(locationsMap,marker);
                    });
       
                    locationMarkers.push( marker );
                   
                 }    
            }); 
            
        //if we do have lat/lon, then make a lat/lon google object and render it
        }else{
            var locationLatLon = new google.maps.LatLng( locLat, locLon );
            //should bind a click event to the marker here
            
            var styleIcon = new StyledIcon(StyledIconTypes.MARKER,{color:color});    
            var marker = new StyledMarker({styleIcon:styleIcon,position:locationLatLon,map:locationsMap});     
            
            google.maps.event.addListener(marker, 'click', function() {
                locationsMap.setCenter( locationLatLon );
                google.maps.event.addListener( infoWindow, 'domready', function(){
                    $( '#location_pin_' + locationData.locationID ).bind('click', addLocation );
                });
                infoWindow.setContent(locationData.info); 
                infoWindow.open(locationsMap,marker);
            });

            locationMarkers.push( marker );
                    
        }
    };

    var clearMarkers = function(){
        for(var i in locationMarkers ){
            locationMarkers[ i ].setMap( null );    //set the marker to null map
        }  
        locationMarkers = [];                   //clear out the array (that tracks the markers)
    };
    
    var searchLocations = function( searchTerm, callback ){
        //set the dropdowns to blank
         $('#filter_neighborhood').fadeOut();
         $('#filter_results').hide();  
         $( filterNeighborhoodSelectEl ).html('<option>Select Neighborhood</option>');    
         $("#filter_city_select>option:eq('')").attr('selected', true);
         
        var public = $('input[name=residence_check]:checked').val(); 
        if(public == 0){
            public = 'yes';
        } 
         
        //check to see if we have public flag
        if( !hasPublic ){
            public = '';
        }
         
        $.post('/account/locations/searchLocationsJSON',{ term : searchTerm, public: public },function(serverRes){
            var sRes = JSON.parse(serverRes);
            //if we actually have good lat/lon, then do it
            var pointGeo = sRes.geo || null;

            if( pointGeo != null ){
                var pinGeo = new google.maps.LatLng( pointGeo.lat, pointGeo.lon );
               
                locationsMap.setCenter( pinGeo );
                //drop a pin where you searched, and place a radius circle in the middle
                var styledSearchIcon = new StyledIcon(StyledIconTypes.MARKER,{color:'#ff0000'});    
                if(searchMarkerRadius != null){
                    searchMarkerRadius.setMap(null);
                }
                searchMarkerRadius = new StyledMarker( { styleIcon: styledSearchIcon, position:pinGeo, map:locationsMap } ); 
                drawRadius( pinGeo, 1.1 );
                resetMapBoundsFromCircle();
            }
            
            var results = sRes.results || [];
            clearMarkers();

            if( results.length > 0 ){
                $('#locations_result_table_body').html('');
                for( var r in results ){
                    rowResultRender( locationsJSON[ results[ r ]['locationID'] ], function(){
                        dropMarkerPostFilter( locationsJSON[ results[ r ]['locationID'] ] ); 
                    });
                    $('#filter_results').show();
                }
                //resetMapBoundsFromMarkers();
                resetMapBoundsFromCircle();

             }else{
                 noResultRender();
             }

             if( typeof callback == 'function' ){
                 callback( results );
             }   
            
        });
    };
    
    var getInfoBox = function(location) {
        var info = '<table border="0" style="width:300px">';
        if (location.url && location.image) {
            $info += '<td rowspan="4" style="padding:5px;"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src="'+location.image.url+'" width='+location.image.width+' height='+location.image.height+'></td>';
        }
        var t = function(x) { return x ? x : '' };

        info += (location.companyName)?'<tr><td valign=top><span class=bodyBold><u>'+t(location.companyName)+'</u></span></td></tr>':"";
        info += (location.address)?'<tr><td><span class=bodyBold>'+t(location.address)+'</span><span class=bodyReg><br>'+ t(location.location)+'</span></td></tr>':"";
        info += (location.hours)?'<tr><td><span class=bodyBold>Hours: </span><span class=bodyReg>'+t(location.hours)+'</apan></td></tr>':"";
        info += '<tr><td><a class="add_button" id="location_pin_' + location.locationID + '" href="javascript:;"><img src="/images/icons/add.png"></a> Add to My Locations</td></tr>';
        info += '</table>';

        return info;
    };

    var loadLocationsResources = function( callback ){
    //theres a better way to do this, instead of nesting these ajax calls, but for now... we'll see how it rolls
    //break all of these out, we sohuld pull them regardess with a .error(function(){});
        $.getJSON('/account/locations/businessNeighborhoodsJSON',function(res){
            businessNeighborhoods = res;
            $.getJSON('/account/locations/locationsJSON',function(res){
                locationsJSON = res;
                for (var x in locationsJSON) {
                    var loc = locationsJSON[x];
                    locationsJSON[x].info = getInfoBox(loc);

                    if (typeof cityLocationsJSON[loc.city] == "undefined") {
                        cityLocationsJSON[loc.city] = {};
                    }
                    cityLocationsJSON[loc.city][loc.locationID] = {
                        'locationID': loc.locationID,
                        'address': loc.address,
                        'name': loc.name,
                        'city': loc.city,
                        'state': loc.state,
                        'lat': loc.lat,
                        'lon': loc.lon,
                        'zipcode': loc.zipcode,
                        'serviceType': loc.serviceType,
                        'public': loc.public
                    };
                }
                

                $('#loading_message').hide();
                $.getJSON('/account/locations/neighborhoodsJSON',function(res){
                    neighborhoodsJSON = res;
                    if( typeof callback == 'function' ) {
                      callback();
                    }
                });  //end getJSON
            });  //end getJSON
        });//end getJSON
    };
    
    
    var filterRadiusLocations = function( centerLatLon ){
        //[ circleRadius ]
        var circleBounds = locationRadius.getBounds();
        var northEast    = circleBounds.getNorthEast();
        var southWest    = circleBounds.getSouthWest();
    };
    
    //pass this the latLon object, via googles format to draw circle
    var drawRadius = function( radiusPoint,radiusSizeInMiles ){   
        locationRadius = locationRadius || '';
        if( locationRadius != '' ){
            locationRadius.setMap(null);
        }
        
        locationRadiusOptions.center        = radiusPoint;
        locationRadiusOptions.radius        = ( radiusSizeInMiles * 1609.43 );
        locationRadiusOptions.map           = locationsMap;
        locationRadiusOptions.strokeColor   = "6ed0f7";
        locationRadiusOptions.fillColor     = "6ed0f7";     
        
        locationRadius = new google.maps.Circle( locationRadiusOptions );
    }; 
    
    var addLocation = function(){           //binding to click that adds the location via ajax
        var locationElementId =  $(this).attr('id') || '';
        
        if( locationElementId !== '' ){
            var location_id = 0;
            if( locationElementId.indexOf('location_result_') >= 0 ){
                var locRef = this;
                location_id = locationElementId.replace('location_result_','');
            }else if( locationElementId.indexOf('location_pin_') >= 0 ){
                location_id = locationElementId.replace('location_pin_','');
                var locRef = $( '#location_result_' + location_id );
            }
            
            if( location_id > 0 ){
                $.post('/account/locations/add_customer_location_ajax',{locationID:location_id}, function(res){
                    var resStatus = JSON.parse(res);
                    if( resStatus.status == 'success' ){
                        $(locRef).parent().parent().fadeOut(function(){
                            addLocationRow( location_id );    
                        });
                        
                        if( $.isEmptyObject(existingCustomerLocations) ){
                            existingCustomerLocations = {};
                            $('#customer_locations').fadeIn();
                        }
                        existingCustomerLocations[ location_id ] = locationsJSON[ location_id ];
                    }else{
                        alert('Already added that location!');
                    }
                });
            }
        } 
    };
    
    //adds a new row to the customer locations table
    var addLocationRow = function( locationID ){
        var loc = locationsJSON[locationID];
        if( existingCustomerLocations.length < 1 ){
            $('#customer_locations').fadeIn();
        }
        
        var rowEl   =  $('<tr style="display:none;"></tr>');
        var column1 =  $('<td>' + loc.address + '</td><td>' + loc.name + '</td>');
        var column2 =  $('<td align="center" style="width:25px"><a class="del_button" href="javascript:;"  id="customer_location_' + loc.locationID + '"><img src="/images/icons/cross.png" class="delete_button" /></a></td>');

        $(column1).appendTo(rowEl);
        $(column2).appendTo(rowEl);

        $(rowEl).appendTo('#existing_locations').fadeIn();

        $( '#customer_location_' + loc.locationID ).bind('click', deleteLocation );
    };
    
    //deletes the location (binded by click)
    var deleteLocation = function(){
        var trRef = this;
        var existingLocationElementId = $(this).attr('id') || '';
        if( existingLocationElementId != '' ){
            var location_id = existingLocationElementId.replace('customer_location_','');
            $.post('/account/locations/delete_customer_location_ajax',{locationID:location_id}, function(res){
                $(trRef).parent().parent().fadeOut(function(){
                    $(trRef).remove();
                    delete existingCustomerLocations[ location_id ];
                    
                    if( $.isEmptyObject( existingCustomerLocations ) ){
                        $('#customer_locations').fadeOut();
                    }
                    
                }); 
            });
        }        
    };
    
    //searches by radius using the customer lat/lon, default distance of 300m
    var radiusSearch = function( distance, callback, public ){
    
        //check to make sure we have a proper address here
        //otherwise change the alert ( they should have a full address, but somethings aren't required - they should all be probably)
        
        customerLatLon.lat = customerLatLon.lat || '';
        customerLatLon.lon = customerLatLon.lon || '';
        customerAddress    = customerAddress || '';
        
        if( customerLatLon.lat == '' && customerLatLon.lon == '' ){
            //will have to return here
            if( customerAddress == '' ){
                $('#results_note').html('<div class="alert alert-error">' + <?= json_encode(get_translation_for_view("valid_address", 'Please enter a valid address into your <a href="/account/profile">Account Settings</a>.'))?> + '</div>');
                return false;
            }    
            $('#results_note').html('<div class="alert alert-error">' + <?= json_encode(get_translation_for_view("no_geo_data", "We were unable to receive geo data for your address.")) ?> + '</div>');
            return false;
        }
        
        var distance = distance || .2;  //.2 miles, or around 300m
        
        var distanceSearch = {
            lat : customerLatLon.lat,
            lon : customerLatLon.lon,
            distance : distance,
            public : public || ''
        };        
        
        //check to see if we have public flag
        if( !hasPublic ){
            distanceSearch['public'] = '';
        }
            
        $.post('/account/locations/radius_searchJSON',distanceSearch,function(res){
            var sRes = JSON.parse(res);
            var results = sRes.results || [];
            clearMarkers();
            
            if( results.length > 0 ){
                
                for( var r in results ){
                    rowResultRender( locationsJSON[ results[ r ]['locationID'] ], function(){
                       dropMarkerPostFilter( locationsJSON[ results[ r ]['locationID'] ] ); 
                    });
                    $('#filter_results').show();
                }
                resetMapBoundsFromMarkers();
                resetMapBoundsFromCircle();
                
            }else{
                 noResultRender();
            }
            
            if( typeof callback == 'function' ){
                callback( results );
            }
            
        }).error(function(){
            //console.log('error');
        });
    };
    
    //sets up the autcomplete and handler for an address - lat/lng
    var initializeMapSearch = function(){
        var mapBounds =  locationsMap.getBounds();
        var mapInput = document.getElementById('address_search_field');
        
        var searchOptions = {
            //types: [ "geocode" ],
            bounds: mapBounds
        };

		<?php if ($limit_geo_country = get_business_meta($business_id, 'limit_geo_country')): ?>
		  searchOptions.componentRestrictions = {country: "<?= $limit_geo_country ?>"};
		<? endif; ?>

        var autocomplete = new google.maps.places.Autocomplete( mapInput, searchOptions );
        
        //autocomplete listener for when the autocomplete selects a place
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              // Inform the user that a place was not found and return.
              return;
            }

            var searchPointLat = place.geometry.location.lat(); 
            var searchPointLon = place.geometry.location.lng();
            
            var distanceSearch = {
                lat:searchPointLat,
                lon:searchPointLon,
                distance:1.2,
                public: 'yes'
            };
            
            if( $('input[name=residence_check]:checked').val() == 1  ){
                distanceSearch['public'] = '';
                distanceSearch['distance'] = .4;
            }
            
            //drop a pin where you searched, and place a radius circle in the middle
            var styledSearchIcon = new StyledIcon(StyledIconTypes.MARKER,{color:'#ff0000'});    
            if(searchMarkerRadius != null){
                searchMarkerRadius.setMap(null);
            }
            searchMarkerRadius = new StyledMarker({styleIcon:styledSearchIcon,position:place.geometry.location,map:locationsMap}); 
            drawRadius( place.geometry.location, distanceSearch['distance'] );
            
            //check to see if we have public flag
            if( !hasPublic ){
                distanceSearch['public'] = '';
            }
            
            //console.log(distanceSearch);
            
            //add public/private here
            $.post('/account/locations/radius_searchJSON',distanceSearch,function(res){
                var sRes = JSON.parse(res);
                var results = sRes.results || [];
                clearMarkers();
                
                //re-center map
                locationsMap.setCenter( place.geometry.location );
                
                if( results.length > 0 ){
                   // drawRadius( place.geometry.location );
                    $('#locations_result_table_body').html('');
                    
                    for( var r in results ){
                        rowResultRender( locationsJSON[ results[ r ]['locationID'] ], function(){
                           dropMarkerPostFilter( locationsJSON[ results[ r ]['locationID'] ] ); 
                        });

                        $('#filter_results').show();    
                    }
                    resetMapBoundsFromMarkers();
                    resetMapBoundsFromCircle();

                }else{
                     noResultRender();
                }

                if( typeof callback == 'function' ){
                    callback( results );
                }

            }).error(function(){
                //console.log('error');
            });
            
          });
    };
    
    //initial/main locations logic 
    $(function(){
    
        //if for some reason we dont' have customer lat/lon geocode, use the address to try to pull it
        customerLatLon.lat = customerLatLon.lat || '';
        customerLatLon.lon = customerLatLon.lon || '';
        customerAddress = customerAddress || '';
        if( ( customerLatLon.lat === ''|| customerLatLon.lon === '' ) && customerAddress !== '' ){
            var customerGeocoder = new google.maps.Geocoder();
            customerGeocoder.geocode( { address : customerAddress }, function( res, status ){
                customerLocation = res[0]['geometry']['location'];
            });        
        }else{
            customerLocation = new google.maps.LatLng( customerLatLon.lat, customerLatLon.lon );
        }

        //load the JSON resources we need
        loadLocationsResources(function(){
            //create the city dropdown, etc.
            setLocationsObject();

            $('#residence_check').fadeIn();
            initializeMap(runCityFilter);            //initialize map
            $('#filter_city').fadeIn();
            
        });

        $( filterCitySelectEl ).bind('change', runCityFilter);
        
        $( filterNeighborhoodSelectEl ).bind('change', runNeighborhoodFilter);
        
        $(searchButtonEl).click(function(e){  
            e.preventDefault();
            //runs the search
            var searchTerm = $('#address_search_field').val();
            searchTerm = searchTerm || '';
            if(searchTerm !== ''){
                searchLocations( searchTerm );
            }
        });


        $('input[name=residence_check]:radio').change(function(e){      //new stab at the jQuery for our locations filtering           
            $('#address_search_field').val('');
            if( $(this).val() == 0 ){
                clearMarkers();
                $('#results_note').html('');
                        
                $('#filter_results').hide();
                $('#locations_result_table_body').html(''); 
                $('#filter_city').fadeIn();

                runCityFilter();
                

            }else if( $(this).val() == 1  ){
                clearMarkers();
                $('#locations_result_table_body').html('');   //wipe out results;
                $('#filter_city').fadeOut();     
                $('#filter_neighborhood').fadeOut();
                
                locationRadius = locationRadius || '';
                if( locationRadius != '' ){
                    locationRadius.setMap(null);
                }
        
                //FIRST check to see if we have an exact location match
                radiusSearch( customerDefaultDistance ,function( searchResults ){ //do we have a location within 200 meters? if so, let them see them and choose.
                    if( searchResults.length > 0 ){
                        //we have a very close result
                        $('#results_note').html('<div class="alert alert-success">' + <?= json_encode(get_translation_for_view("found_location_message", "We found the following locations near you."))?> + '</div>');
                    }else{
                        //coulnd find the building, pick on of these in a slightly larger radius
                        $('#results_note').html('<div class="alert alert-error">We were unable to find any locations near ' + customerAddress + '.</div>');
                      //  radiusSearch();
                    }
                });     
            }
        }); 
        
        $('#address_search_field').blur(function(){
            
        });
        
        //render any pre-existing customer locations
        renderCustomerLocations();
        
        
    });
    
    
</script>
<div class='container'>
    <div class="row" >
        
        <div id="customer_locations" style="display:none">
            <div class="span8">
                <p><h2><?=get_translation_for_view("your_locations", "Your Locations")?></h2></p>
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th> <?=get_translation_for_view("address", "Address")?> </th>
                            <th width='*'><?=get_translation_for_view("type", "Type")?></th>     
                            <th style='width:25px;' align='center'><?=get_translation_for_view("delete", "Delete")?></th>
                        </tr>
                    </thead>
                    <tbody id="existing_locations"></tbody>
                </table>
            </div>
            <div class="span2">
                 <p><h2>&nbsp;</h2></p>
                 <a href='/account/orders/new_order' class='btn btn-success btn-large' ><?=get_translation_for_view("new_order", "Place New Order")?></a>
            </div>
            <div class="span10">
                <hr />
            </div>
        </div>
        
        <div class="span10" id="loading_message">
                <?=get_translation_for_view("loading", "Loading Locations...")?>
                <img src="/images/loading.gif" style="max-height:50px;" />
        </div>
        
        <div class="clearfix"></div>
        
        <div class="row">
            
            <div class="span5">
            
                <!-- residence check -->
                <div id="residence_check" style="display:none;">
                    <div class="span5">
                        <p class="lead"><?=get_translation_for_view("is", "Is")?> <?=$this->business->companyName;?> <?=get_translation_for_view("in_your", "in your building?")?></p>

                        <p class="lead"> 
                            <input type="radio" name="residence_check" value="1" /> <?=get_translation_for_view("is_company_in_your_building_yes_option", "Yes")?>  &nbsp;&nbsp; 
                            <input type="radio" name="residence_check" value="0" checked /> <?= get_translation_for_view("is_company_in_your_building_no_option", "No") ?> 
                        </p>
                    </div>
                </div>

                <!-- city element -->
                <div id="filter_city" style="display:none;">
                    <p class="lead span2">
                        <?= get_translation_for_view("city_label", "City")?> &nbsp;
                    </p>
                    <p class="span2">
                        <select id="filter_city_select"></select>
                    </p>
                </div>

                <!-- neighborhood -->
                <div id="filter_neighborhood" style="display:none;">
                    <p class="lead span2">
                        <?= get_translation_for_view("neighborhood_label", "Neighborhood")?> &nbsp;
                    </p>
                    <p class="span2">
                        <select id="filter_neighborhood_select">
                            <option></option>
                        </select>
                    </p>
                </div>

                <!-- list of returned/result items -->
                <div id="results_note" class="span4"></div>
                <div id="filter_results" style="display:none;">
                    <table class="table table-striped table-bordered table-condensed span4">
                        <thead>
                            <tr>
                                <th><?=get_translation_for_view("address", "Address")?></th>
                                <th width='10%'><?=get_translation_for_view("type", "Type")?></th>     
                                <th style='width:25px;' align='center'><?=get_translation_for_view("add", "Add")?></th>
                            </tr>
                        </thead>
                        <tbody id="locations_result_table_body">
                        </tbody>
                    </table>                       
                </div>
            </div>
            
            <div class="span5">
                <div id="locations_filter" >
                    <!-- locations filter area -->                    
                    <div id="locations_map" class="" >
                         <div class="span5" style="margin:10px;">
                                <?php $location_requests = get_translation_for_view('location_requests', 'location_requests', array('email' => $this->business->email));?>
                                <?php

                                    if ($location_requests != 'location_requests') {
                                        echo '<p>'.$location_requests.'</p>';
                                    }
                                ?>
                                <input type="text" id="address_search_field" class="span4 search-query" placeholder="Enter an address" style="padding:5px;" />
                                <button id="locations_search_button" type="button" class="btn"> <?= get_translation_for_view("search_button", "Search") ?> </button>
                                <div id="map_canvas" style="max-width:600px;height:500px;border:1px solid #666;margin-top:5px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <div style="margin:10px;padding:10px;">
        <?=get_translation_for_view("help", "If you need assistance with finding a location or if you want to request service to your building, please send us an email at")?> <?= get_business_meta($business_id, 'customerSupport')?> 
    </div>        
</div>
