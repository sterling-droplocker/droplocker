<div class='container'>
<div class='row'>
    <div class='span8 offset1'>
        <?php echo isset($error)?$error:'';?>
        <h2 class='page-header'><?=get_translation_for_view("forgot_password", "Forgot Password")?></h2>
        <?= $this->session->flashdata('alert');?>
    </div>
</div>
<div class='row'>
    <div class='span4 offset1'>
        <p style='padding:10px;'><?=get_translation_for_view("directions", "Enter your email address and we will send directions for changing your passwod")?></p>
        <form method='post' action=''>
            <div class="control-group">
              <label class="control-label" for="email"><?=get_translation_for_view("email", "Email")?></label>
              <div class="controls">
                <input size='30' type='text' id='email' name='email' />
              </div>
            </div>
            <?= form_submit(array("class" => "btn btn-primary", "name" => "submit", "value" => get_translation_for_view("send_password_button_text", "Send Password"))) ?> &nbsp;&nbsp;&nbsp;&nbsp;<a href='/login'><?=get_translation_for_view("back_to_login", "Back to Login")?></a>
        </form>
    </div>
    <div class='span4'>
        <div class='alert alert-info'>
            <h3><?=get_translation_for_view("first_time_users", "First Time User")?></h3>
            <p><?=get_translation_for_view("first_time_user_subtitle", "It's free and fast, just like our laundry services")?></p>
            <a href='/register' class='btn btn-info'> <?= get_translation_for_view("register_now_button_text", "Register Now") ?> </a>
        </div>
    </div>
</div>
</div>