<h2><?= get_translation_for_view('redeem_program_cash_title', 'Redeem Cash', array('program_name' => $program_name)); ?></h2>


<br><br>
<?if(!$W9):?>

<div class="info">
    <p><?= get_translation_for_view('redeem_w9_1', 'In order to process your request for a check, we need to have a W-9 on file for you.') ?></p>
    <p><?= get_translation_for_view('redeem_w9_2', 'You can <a href="/images/fw9.pdf">download the W-9 form here</a>.') ?></p>
    <p><?= get_translation_for_view('redeem_w9_3', 'Please complete the form, sign it and fax %phone% or email (<a href="mailto:%email%">%email%</a>) it back to us and we\'ll get a check out to you right away.', array(
        'phone' => $this->business->phone,
        'email' => $this->business->email,
    )); ?></p>
</div>

<?else:?>

<div class="bodyBold">
    <?= get_translation_for_view('redeem_program_cash_text', 'Thank you for spreading the word! A check for %total% will be mailed to you in the next 5 business days.', array(
        'total' => format_money_symbol($business->businessID, '%.2n', $total),
    )); ?>
</div>

<? endif; ?>

<br>
<div>
    <a href='/account/programs/rewards' class='button blue'><?= get_translation_for_view('program_details', '%program_name% Details', array('program_name' => $program_name)); ?></a>
</div>
