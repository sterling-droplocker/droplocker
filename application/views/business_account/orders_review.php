<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li>
    <a href="/account/orders/new_order"><?=get_translation_for_view("place_new_order", "Place New Order")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("order_complete", "Order Complete!")?>
  </li>
</ul>

<?= $this->session->flashdata('alert');?>

<h3><?= get_translation_for_view("thank_you", "Thank You For Your Order!") ?></h3>

<? if ((int) $this->customer->autoPay == 1): ?>
<p><?= get_translation_for_view("on_autopay_message", "This order will be charged to the credit card we have on file for you. If you would like to modify any of your credit card information, please login to your account and go to \"MY PREFERENCES\". Select the \"CREDIT CARD\" tab and you can enter your new credit card information."); ?></p>
<? else: ?>
<p><?= get_translation_for_view("not_on_autopay_message", "You are not currently setup with automatic payments.  As soon as your items are picked up, we will email you with your order total.  At that time you can login to the website and enter your credit card information.  When you enter your credit card you will be able to setup automatic payments so as to ensure easier processing of future orders."); ?></p>
<? endif; ?>

<p><?= get_translation_for_view("contact_us_or_call",
        "If you have any questions or need to make any changes to this order, please contact us at <a href='mailto:%customerSupport%'>%customerSupport%</a> or call us at %phone%.",
        array(
            'customerSupport' => get_business_meta($this->business_id, 'customerSupport'),
            'phone' => $business->phone
        )) ?></p>

<p><?= get_translation_for_view("thank_you_for_choosing_company",
        "Thank you for choosing %companyName% and please let us know if there is anything we can do to improve your experience.",
        array(
            'companyName' => $business->companyName
        )) ?></p>

<p><?= get_translation_for_view("sincerely_the_team",
        "Sincerely,<br>The %companyName% Team",
        array(
            'companyName' => $business->companyName
        )) ?></p>

<p style='text-align: center'>
    <input type='button' onClick="top.location.href='/account'" class='btn btn-primary' value='<?=get_translation_for_view("return_to_account_button", "Return to My Account")?>' />
</p>
