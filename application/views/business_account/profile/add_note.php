<?php

$images = array(
    'Laundered Shirt' => '/images/item_launderedShirt.jpg',
    'Blouse, Sweater, Shirt' => '/images/item_launderedShirt.jpg',
    'blazer' => '/images/item_blazer.jpg',
    'polo' => '/images/item_polo.jpg',
    'default' => '/images/item_onePiece.jpg',
);

//if the image doesn't come through, show the default [ blank ] images.
if ($item->file) {
    $item_image = 'https://s3.amazonaws.com/LaundryLocker/' .  $item->file;
} else {
    $item_image = isset($images[$item->displayName]) ? $images[$item->displayName] : $images['default'];
}

?>
<ul class="breadcrumb">
  <li>
    <a href="/account"><?= get_translation_for_view("account_home_title", "Account Home") ?></a> <span class="divider">/</span>
  </li>
  <li>
   <a href="/account/profile/closet/"><?= get_translation_for_view("my_closet", "My Closet") ?></a> <span class="divider">/</span>
  </li>
  <li>
   <a href="/account/profile/item_detail/<?php echo $this->uri->segment(4);?>">
	<?= get_translation_for_view("item_details", "Item Details [%item%]", array('item' => $this->uri->segment(4))) ?></a> <span class="divider">/</span>
  </li>
  <li class='active'><?= get_translation_for_view("add_note", "Add Note") ?> </li>
</ul>

<?= $this->session->flashdata('alert');?>
<h2><?= get_translation_for_view('tell_us_the_problem', 'Tell us about the problem with this item:') ?></h2>
<input type='hidden' name='itemIssueID' value='<?= $itemIssueID?>' />


<div class='row'>
    <div class='span4'>
        <div>
			<?= get_translation_for_view('click_to_add_note', 'Click on the location of the image below to add a note to the item.') ?><br>
            <img id="issue_image" src='<?= $item_image?>' style="max-width:300px;cursor:pointer" />
            <span class="issue_marker" data-original-title="<?= get_translation_for_view('item_note', 'Item note') ?>"></span>

        </div>
        <? if($itemIssueID):?>
        <p><a href='/account/profile/add_note/<?= $item->itemID?>'><?= get_translation_for_view('add_more', 'Add More Notes') ?></a></p>
        <? endif; ?>
    </div>
</div>

<div>
    <table class="table table-striped table-bordered table-condensed">
        <tr>
            <th><?= get_translation_for_view('barcode', 'Barcode') ?></th>
            <td><?= $item->barcode?></td>
        </tr>
        <tr>
            <th><?= get_translation_for_view('product', 'Product') ?></th>
            <td><?= $item->displayName?></td>
        </tr>
        <tr>
            <th><?= get_translation_for_view('cleaning_method', 'Cleaning Method') ?></th>
            <td><?= get_translation($item->processName ? $item->processName : 'N/A', "service_types", array(), $item->processName ? $item->processName : 'N/A') ?></td>
        </tr>
    </table>

    <? if($open): ?>
    <h3><?= get_translation_for_view('open_issues', 'Open Issues') ?></h3>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= get_translation_for_view('issue_type', 'Type') ?></th>
                <th><?= get_translation_for_view('issue_notes', 'Notes') ?></th>
                <th><?= get_translation_for_view('issue_actions', 'Actions') ?></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($open as $o): ?>
            <tr>
                <td valign="top">
                    <? if ($o->issueType): ?>
                    <?= get_translation_for_view(strtolower($o->issueType), $o->issueType)?>:
                    <? endif; ?>
                    <?= get_translation_for_view(strtolower($o->frontBack), $o->frontBack)?></td>
                <td><?= $o->note?></td>
                <td>
                    <a id="modify-<?=$o->itemIssueID?>" href='/account/profile/add_note/<?= $item->itemID?>/<?= $o->itemIssueID?>'><i class="icon icon-pencil"></i> <?= get_translation_for_view("edit", "Edit") ?></a>
                    <a id="delete-<?=$o->itemIssueID?>" href='/account/profile/delete_note/<?= $item->itemID?>/<?= $o->itemIssueID?>'><i class="icon icon-remove"></i> <?= get_translation_for_view("remove", "Remove") ?></a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <? endif; ?>

    <? if($closed): ?>
    <h3><?= get_translation_for_view('past_issues', 'Past Issues') ?></h3>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= get_translation_for_view('issue_type', 'Type') ?></th>
                <th><?= get_translation_for_view('issue_notes', 'Notes') ?></th>
                <th><?= get_translation_for_view('issue_actions', 'Actions') ?></th>
            </tr>
        </thead>
        <tbody>
        <? foreach($closed as $o): ?>
            <tr>
                <td valign="top">
                    <? if ($o->issueType): ?>
                    <?= get_translation_for_view(strtolower($o->issueType), $o->issueType)?>:
                    <? endif; ?>
                    <?= get_translation_for_view(strtolower($o->frontBack), $o->frontBack)?></td>
                <td><?= $o->note?></td>
                <td>
                    <a id="modify-<?=$o->itemIssueID?>" href='/account/profile/add_note/<?= $item->itemID?>/<?= $o->itemIssueID?>'><i class="icon icon-pencil"></i> <?= get_translation_for_view("edit", "Edit") ?></a>
                    <a id="delete-<?=$o->itemIssueID?>" href='/account/profile/delete_note/<?= $item->itemID?>/<?= $o->itemIssueID?>'><i class="icon icon-remove"></i> <?= get_translation_for_view("remove", "Remove") ?></a>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <? endif; ?>
</div>


<div id="note_popover" style="display:none;">
    <div class="" style="text-align:left;">
        <form method='post' action='/account/profile/edit_note/<?= $item->itemID?>' id="note_form" onsubmit="return validateNote();" name="note_form">
            <input type='hidden' name='itemIssueID' value='<?= $itemIssueID?>' />
            <div style="margin-top:10px;"><?= get_translation_for_view('front_or_back', 'Front or Back') ?>
                <select id='frontBack' name='frontBack'>
                    <option value='front'><?= get_translation_for_view('front', 'Front') ?></option>
                    <option value='back'><?= get_translation_for_view('back', 'Back') ?></option>
                </select>
                <? if($frontBack):?>
                <script>
                    $(document).ready(function(){
                            $("#frontBack").val('<?= $frontBack?>');
                    });
                </script>
                <? endif; ?>
            </div>

            <div style="margin-top:10px;"><?= get_translation_for_view('issue', 'Issue:') ?> &nbsp;
                <input type="radio" name='issueType' <?= $issueType == 'Stain' ? 'checked' : '' ?> value='Stain' />
                <?= get_translation_for_view('stain', 'Stain') ?>

                <input type="radio" name='issueType' <?= $issueType == 'Repair' ? 'checked' : '' ?> value='Repair' />
                <?= get_translation_for_view('repair', 'Repair') ?>

                <input type="radio" name='issueType' <?= $issueType == 'Other' ? 'checked' : '' ?> value='Other' />
                <?= get_translation_for_view('other', 'Other') ?>
            </div>

            <div style="margin-top:10px;">
                <?= get_translation_for_view('notes', 'Notes:') ?><br>
                <textarea name='note' id="note_text"><?= $note?></textarea>
            </div>
            <div style="color:red;" id="error_message"></div>
            <input type="hidden" name="x" value="<?=$x;?>" id="issue_x" />
            <input type="hidden" name="y" value="<?=$y;?>" id="issue_y" />
            <a href="javascript:;" class="close_popover pull-right" style="margin:10px;"><?= get_translation_for_view('hide_window', 'Hide Window') ?></a>
            <button id="note_submit" class='btn btn-primary' type="submit"><?= get_translation_for_view('save', 'Save') ?></button>
        </form>
    </div>
</div>


<script>
    var validateNote = function(){
        //the popover actually makes a copy of th whole form,
        //so use this to grab the one from th epopover
        var note = $('.popover #note_text').val();
        if( note == ''){
            $('.popover #error_message').html('<?= get_translation_for_view('note_required', 'Note required') ?>');
            return false;
        }else{
            return true;
        }
    };

    var issueX = <?= !empty( $x ) ? $x : 'null'; ?>;
    var issueY = <?= !empty( $y ) ? $y : 'null'; ?>;

    $(function(){
        $('img#issue_image').bind('click',bindIssueImage);

        //popover
        $('.issue_marker').popover({
            html: true,
            trigger: 'click',
            placement: 'bottom',
            content: function(){
               return $('#note_popover').html();
            },
            delay: { show: 500, hide: 500 }
        });

        <? if( !empty( $itemIssueID ) ){ ?>
        renderCrossHairs();
        <? } ?>

    });

    var closePopover = function(){
        $('.issue_marker').popover('hide');
    };

    var renderCrossHairs = function(){
        if( issueX != null && issueY != null ){
            var hOffset = $('#issue_image').height();
            var wOffset = $('#issue_image').width();

            $('.issue_marker').css('visibility','visible');
            $('.issue_marker').css('top', ( (issueY - hOffset) - 24 ) );
            $('.issue_marker').css('left', ( issueX - 24 ) );

            $('.issue_marker').popover('show');

            $('input#issue_x').val( issueX );
            $('input#issue_y').val( issueY );

            $('.close_popover').unbind('click');
            $('.close_popover').bind('click',closePopover);
        }

    };

    var bindIssueImage = function(e){

            e.preventDefault();

            var newX  = (e.offsetX || e.clientX - $(e.target).offset().left + window.pageXOffset ); //(e.offsetX || e.clientX - $(e.target).offset().left);
            var newY  = (e.offsetY || e.clientY - $(e.target).offset().top + window.pageYOffset ); //(e.offsetY || e.clientY - $(e.target).offset().top);

            var hOffset = $('#issue_image').height();

            $('.issue_marker').css('visibility','visible');
            $('.issue_marker').css('top', ( (newY - hOffset) - 24 ) );
            $('.issue_marker').css('left', ( newX - 24 ) );

            $('.issue_marker').popover('show');

            $('input#issue_x').val( newX );
            $('input#issue_y').val( newY );

            $('.close_popover').unbind('click');
            $('.close_popover').bind('click',closePopover);
    };

    $(window).resize(function(){
         $('img#issue_image').unbind('click');
         $('img#issue_image').bind('click',bindIssueImage);
    });

</script>

<style type="text/css">
    .issue_marker {
        width: 48px;
        height: 48px;
        visibility: hidden;
        display:block;
        position: relative;
        background: url(/images/target.png) no-repeat;
    }
</style>
