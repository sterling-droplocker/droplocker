<ul class="breadcrumb">
  <li>
    <a href="/account"><?= get_translation_for_view("account_home_title", "Account Home") ?></a> <span class="divider">/</span>
  </li>
  <li class='active'>
   <?= get_translation_for_view("my_closet", "My Closet") ?>
  </li>
</ul>

<h2 class='page-header'>
    <?php if(empty($order_id)): ?>
            <?= get_translation_for_view("my_closet_title", "My closet") ?>
    <?php else: ?>
            <?= get_translation_for_view("my_closet_title_for_order", "My closet for order %order_id%", array('order_id' => $order_id)) ?>
    <?php endif ?>
</h2>

    <form class="form-inline" method="post">
        <label><?= get_translation_for_view("search_box_label", "Search By Barcode") ?> </label>
        <input type='text' name='barcode' value="<?=$barcode;?>"/>
        <?= form_submit(array("name" => "submit", "class" => "btn btn-primary", "value" => get_translation_for_view("search_button", "Search"))) ?>
    </form>

<?php if($items): ?>
    <?php foreach($items as $display_name_and_process => $item): ?>
    	<h4 style='clear:left;'><?= $display_name_and_process?></h4>
            <?php foreach($item as $itemID => $item_properties): ?>
                <?php
                    //The following conditional checks to see if the file property is defined for item. If it is not defined, then a missing image placeholder is set to the image source.
                    $image = empty($item_properties['file']) ? $default_image : "https://s3.amazonaws.com/LaundryLocker/".$item_properties['file'];
                    $link_location = "/account/profile/item_detail/$itemID";
                ?>
                <div class='thumbnail' style="float: left; margin: 10px 10px 10px 0;">
                    <a href='<?=$link_location?>'>
                        <img class='img_bdr' width="190" src='<?=$image?>' />
                        <br />
                        <?= $item_properties['barcode'] ?>
                    </a>
                </div>
            <?php endforeach;?>
    <?php endforeach; ?>
<?php else: ?>
    <?php if ($barcode == false):?>
    <div class='alert alert-info'><?= get_translation_for_view("no_result_message", "No items in this order. It is possible that the order has not been processed yet.") ?> </div>
    <?php else: ?>
    <div class='alert alert-info'><?= get_translation_for_view("no_result_search_message", "No item with barcode <i>%barcode%</i> has been found, please check that the barcode number you entered is correct.",array('barcode' => $barcode)) ?> </div>
    <?php endif; ?>
<?php endif; ?>

