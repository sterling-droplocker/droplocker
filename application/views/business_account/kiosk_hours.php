    <div class="clearfix">&nbsp;</div>
    <style>
        #kiosk_hours{
            margin-left:10px;
            margin-top:20px;
        }
    </style>
    <div id="kiosk_hours" class="hidden-phone hidden-tablet">
        <u>Kiosk Hours</u><br>
        24 Hours a Day. 7 Days a Week.<br><br>
        <u>Kiosk Locations</u><br>
        1339 Polk St.<br>
        145 Valencia St.<br>
        2103 Van Ness Ave.<br>
        566 Haight St.<br>
        2069 Greenwich St.<br>
        140 Brannan St.<br>
    </div>