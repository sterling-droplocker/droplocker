<?php
/**
 * Exepcts the following content variables
 *  total
 *  subtotal
 *  locker
 *  location
 *  bag
 *  items
 *  history
 * Must also have the variable 'content' set if the order is taxable
 * Must also have the variable 'charges' set if the order is taxable
 */

?>
<style type="text/css">
    .number_field {
        text-align: right;
    }
</style>

<ul class="breadcrumb">
  <li>
    <a href="/account"> <?=get_translation_for_view("account_home", "Account Home")?> </a> <span class="divider">/</span>
  </li>
  <li>
    <a href="/account/orders/view_orders"> <?=get_translation_for_view("view_orders", "View Orders")?> </a> <span class="divider"> / </span>
  </li>
  <li class="active"><?=get_translation_for_view("order_details", "Order Details")?> [<?php echo $order->orderID?>]</li>
</ul>

<?= $this->session->flashdata('alert');?>

<h2 class='page-header'> <?= get_translation_for_view("order_details", "Order Details") ?> </h2>
<div style="text-align: right; font-size: 0.8em;" >
    <?php
        if ($this->input->get("print_friendly") == "true"):
    ?>
        <a href="#" class="no_print" onclick="window.print()"> <img src="/images/icons/printer.png" alt="Print"/> <?=get_translation_for_view("print", "Print")?> </a>
    <?php else: ?>
        <a href="/account/orders/order_details/<?= $order->orderID ?>?print_friendly=true"> <img src="/images/icons/printer.png" alt="Print"/> <?=get_translation_for_view("printer_friendly", "Printer Friendly")?> </a>
    <?php endif; ?>
</div>
<? $is_unpaid = in_array($order->orderStatusOption_id, array(7, 13)); ?>
<dl class='dl-horizontal'>
    <dt> <?= get_translation_for_view("location", "Location")?> </dt>
    <dd> <?= $location->address?> </dd>
    <dt> <?= get_translation_for_view("locker", "Locker") ?> </dt>
    <? if (!$is_unpaid): ?>
    <dd> <?= ($locker->lockerName!='')?$locker->lockerName:"&nbsp;"?> </dd>
    <dt> <?= get_translation_for_view("locker_code", "Access Code") ?> </dt>
    <dd> <?= ($accessCode)?$accessCode:"&nbsp;"?> </dd>
    <? else: ?>
    <dd> <?= get_translation_for_view("awaiting_payment", "Awaiting Payment") ?> </dd>
    <? endif; ?>
    <dt> <?= get_translation_for_view("bag", "Bag") ?> </dt>
    <dd> <?= ($bag->bagNumber!='')?$bag->bagNumber:"&nbsp;";?> </dd>
    <dt> <?= get_translation_for_view("notes", "Notes") ?></dt>
    <dd>
        <? if ($order->orderStatusOption_id == 1): ?>
            <div id="syste_notes" style="white-space: pre;"><?= $order->systemNotes ?></div>
            <div id="user_notes" style="white-space: pre;"><?= $this_order_only . " " . $order->userNotes ?></div>
            <a href="#" id="edit_notes_link"><i class="icon icon-pencil"></i> <?= get_translation_for_view("edit_notes", "Edit Notes"); ?></a>
            <form id="edit_notes_form" style="display:none;" method="post" action="/account/orders/update_order_notes/<?= $order->orderID ?>">
                <textarea name="notes" rows="4" class="span4"><?= $order->userNotes ?></textarea>
                <div><button type="submit" class="btn btn-primary"><?= get_translation_for_view("update_notes", "Update Notes"); ?></button></div>
            </form>
        <? else: ?>
            <div style="white-space: pre;"><?= $order->notes ?></div>
        <? endif; ?>
    </dd>
</dl>

<script>
$('#edit_notes_link').click(function(evt) {
    evt.preventDefault();
    $('#user_notes').hide();
    $('#edit_notes_link').hide();
    $('#edit_notes_form').show();
});
</script>


<h3> <?=get_translation_for_view("order_history", "Order History")?> </h3>
<?php if($history):?>
    <table class='table'>
        <?php foreach($history as $h): ?>
            <tr>
                <td><?= get_translation($h->name, "OrderStatusOptions", array(), $h->name)?></td>
                <td><?= convert_from_gmt_aprax($h->date, SHORT_DATE_FORMAT) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <?=get_translation_for_view("no_history", "No History")?>
<?php endif;?>

<h3><?=get_translation_for_view("items_charges", "Items / Changes")?></h3>
<table class="table table-striped table-bordered table-condensed">
    <tr>
        <th style="text-align: left;"> <?=get_translation_for_view("items", "Items")?> </th>
        <th class="number_field"> <?=get_translation_for_view("price", "Price")?> </th>
        <th class="number_field"> <?=get_translation_for_view("qty", "Qty")?> </th>
        <th class="number_field" style="font-weight: bold;"> <?=get_translation_for_view("total", "Total")?> </th>
    </tr>
    <?php foreach($items as $i):?>
    <tr>
        <td>
            <?php if($i->item_id):?>
                <a href='/account/profile/item_detail/<?php echo $i->item_id?>'><?=get_translation_for_view($i->displayName, $i->displayName)?></a>
                <?php if (!empty($i->notes)): ?>
                    <p> <?= $i->notes ?> </p>
                <?php endif; ?>
            <?else: ?>
                <?= $i->displayName?>
            <?php endif; ?>
        </td>
        <td class="number_field"><?= format_money_symbol($this->business->businessID, '%.2n', $i->unitPrice)?></td>
        <td class="number_field"><?= $i->qty?></td>
        <td class="number_field"><?= format_money_symbol($this->business->businessID, '%.2n', $i->unitPrice*$i->qty)?></td>
    </tr>
    <?php endforeach;?>
    <? if (!$breakout_vat): ?>
    <tr>
        <td colspan='3'>
            <?=get_translation_for_view("subtotal", "Subtotal")?>
        </td>
        <td class="number_field"> <?= format_money_symbol($this->business->businessID, '%.2n', $subtotal) ?> </td>
    </tr>
    <? endif; ?>
    <?php if($charges): foreach($charges as $c): ?>
        <tr>
            <td><?=get_translation_for_view($c->chargeType, $c->chargeType)?></td>
            <td class="number_field">  <?= format_money_symbol($this->business->businessID, '%.2n', $c->chargeAmount) ?> </td>
            <td class="number_field"> 1 </td>
            <td class="number_field"> <?= format_money_symbol($this->business->businessID, '%.2n', $c->chargeAmount) ?> </td>
        </tr>
    <?php endforeach; endif; ?>

    <? if ($breakout_vat): ?>
    <tr>
        <td colspan='3' style="font-weight: bold;">
            <strong><?=get_translation_for_view("order_total", "Order Total") ?></strong>
        </td>
        <td class="number_field" style="font-weight: bold;">
            <?= format_money_symbol($this->business->businessID, '%.2n', $total) ?>
        </td>
    </tr>
    <tr>
        <td colspan='3'>
            <?=get_translation_for_view("subtotal", "Subtotal")?>
        </td>
        <td class="number_field"> <?= format_money_symbol($this->business->businessID, '%.2n', $total - $tax_total) ?> </td>
    </tr>
    <? endif; ?>

    <? if ($tax_total > 0 && count($taxes) == 1): ?>
    <tr>
        <td colspan="3"> <?= get_translation_for_view("tax_total", "Tax Total") ?> </td>
        <td class="number_field"> <?= format_money_symbol($this->business->businessID, '%.2n', $tax_total) ?> </td>
    </tr>
    <? else: ?>

        <? foreach ($taxes as $taxItem):
            if (isset($taxGroups[$taxItem['taxGroup_id']])) {
                $taxGroup = $taxGroups[$taxItem['taxGroup_id']];
                $tax_label = $taxGroup->name;
            } else {
                $taxPercent = $taxItem['taxRate'] * 100;
                $tax_label = get_translation_for_view('tax_percent', 'Tax %taxPercent%%:', array('taxPercent' => $taxPercent));
            }
        ?>
        <tr>
            <td colspan="3"><?=get_translation_for_view($tax_label, $tax_label)?></td>
            <td class="number_field"> <?= format_money_symbol($this->business->businessID, '%.2n', $taxItem['amount']) ?> </td>
        </tr>
        <? endforeach; ?>
    <? endif; ?>

    <? if (!$breakout_vat): ?>
    <tr>
        <td colspan='3' style="font-weight: bold;">
            <strong><?=get_translation_for_view("order_total", "Order Total") ?></strong>
        </td>
        <td class="number_field" style="font-weight: bold;">
            <?=format_money_symbol($this->business->businessID, '%.2n', $total) ?>
        </td>
    </tr>
    <? endif; ?>

</table>

