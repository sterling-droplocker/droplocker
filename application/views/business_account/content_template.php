<?php

// show logged in user for all account pages
$show_user_info = strpos('/' . trim($this->uri->uri_string, '/'), '/account') !== false;

// except rewards pages
if (strpos($this->uri->uri_string, 'account/programs/rewards') !== false) {
    $show_user_info = false;
}

?>
        <!-- Navbar -->
        <div class="navbar navbar-fixed-top visible-phone visible-tablet hidden-desktop" id="interior_mobile_menu">
          <div class="navbar-inner">
            <div class="container-fluid">
              <a class="btn btn-navbar" data-toggle="collapse" data-target="#account_menu.nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <a class="brand" href="<?= prep_url($this->business->website_url);?>"><?php echo $this->business->companyName?></a>
              <div class="btn-group pull-right">
                
                  <?php echo isset($this->customer->username)?$this->customer->username:''?"<a class='btn dropdown-toggle' data-toggle='dropdown' href='#'> <i class='icon-user'></i>".isset($this->customer->username)?$this->customer->username:''."<span class='caret'></span></a>":'';?>
               
                
                <ul class="dropdown-menu">
                  <li><a id="login_logout" href="/logout"><?=get_translation_for_view("sign_out", "Sign Out")?></a></li>
                </ul>
              </div>
              <div class="nav-collapse" id="account_menu">
                <?=$this->load->view('business_account/account_menu_mobile');?>
                <br>
                <p style='text-align:center'>
                    <a class="btn btn-success btn-large" href="/account/orders/new_order"><i class="icon-shopping-cart icon-white"></i><?=get_translation_for_view("place_order_now", "Place Order now")?></a>
                </p>
              </div><!--/.nav-collapse -->
            </div>
          </div>
        </div> <!-- End Navbar -->
        

<div class='container'>
    <div class='row'>
        <div class='span7 offset1' id="content_left_column">
            <?php echo $content ?>
        </div>
        <div class='span3 hidden-phone' id="content_right_column">
            <? if($show_user_info){ ?>
            <div class='user-info-box' style="margin-bottom: 2em;">
                <? if(empty($customer->firstName)): ?>
                    <p><?=get_translation_for_view("unknown_name", "Hey there... Um, I don't know your name yet. Please complete your profile so we can get to know each other.")?><br><a href='/account/profile'><?=get_translation_for_view("profile_page", "Profile Page")?></a></p>
                <? else: ?>
                    <h3><?= getPersonName($customer) ?></h3>
                    <p><?= $customer->email ?></p>
                <? endif; ?>
                <div>
                    <a href='/logout' class='btn btn-primary btn-small login_logout'><i class='icon-user icon-white'></i> <?= get_translation_for_view("logout", "Logout") ?> </a>
                </div>
            </div>
             <?}?>   
            <?
             if(is_array($sidebar)){
                foreach($sidebar as $s)
                {
                    echo $s."";
                }
             } else {
                 
                 echo $sidebar;
             }
             ?>
        </div>
    </div>
    <!-- The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
    <script type="text/javascript">
        var login_logout_button = document.getElementById("login_logout");
        //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
        <?php if ($facebook_user_id): ?>
            login_logout_button.textContent = "Logout";
            login_logout_button.setAttribute("href", "<?= $facebook_logout_url?>");

        <?php elseif (empty($this->customer)): ?>
            login_logout_button.textContent = "Sign In";
        <?php else: ?>
            login_logout_button.textContent = "Logout";
            login_logout_button.setAttribute("href", "/logout");
        <?php endif; ?>
    </script>
</div>
