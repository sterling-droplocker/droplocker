
<?php if ($this->business_id == 3): ?>
        <?= $content; ?>
<?php else: ?>
<?php
    $span = 'span12';
    if($this->business_id == 95) {
        $span = 'span10';
    }
?>
<div class='container'>
    <div class='row'>
        <div class="<?=$span;?>">
            <?php echo $content ?>
        </div>
    </div>
</div>
<?php endif; ?>