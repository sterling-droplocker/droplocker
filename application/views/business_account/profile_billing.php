<?php

/**
 * Expects at least the following content variables:
 *  business stdObject
 *  creditCard
 *  direct_sale
 *  hideKioskCheckbox
 */

$all_states = get_all_states_by_country_as_array();

$show_paypal = get_business_meta($this->business_id, 'paypal_button');
$show_bitcoin = get_business_meta($this->business_id, 'bitcoin_button');
$only_card = !$show_paypal && !$show_bitcoin;

$accepted_cards = get_business_meta($this->business_id, 'acceptedCards');
$accepted_cards = $accepted_cards ? unserialize($accepted_cards) : array();
$card_images = array(
    'visa'            => '/images/creditcards/visa.png',
    'mastercard'      => '/images/creditcards/master-card.png',
    'americanexpress' => '/images/creditcards/american-express.png',
    'discover'        => '/images/creditcards/discover.png',
);

$show_accepted_cards = false;

//from the current year to 15 years in the future
$years = array();
$short_years = array();
$long_year = date("Y");
$short_year = date("y");
for ($x = 0; $x <= 15; $x++) {
    $years[$long_year + $x] = $short_year + $x;
    $short_years[$short_year + $x] = $short_year + $x;
}

$months = array();
for ($x = 1; $x <= 12; $x++) {
    $months[$x] = sprintf("%02d", $x);
}

$has_billing_info = $customer->address1 && $customer->city && $customer->state;

$billing = (object) $customer->properties;
if (!empty($_POST)) {
    foreach($_POST as $key => $value) {
        $billing->$key = $value;
    }
}

$customer_billing_name = getPersonName($billing);
$customer_billing_address = getFormattedAddress($billing);

$credit_card_images = array(
    'Visa'             => '/images/creditcards/visa.png',
    'American Express' => '/images/creditcards/american-express.png',
    'Discover Card'    => '/images/creditcards/discover.png',
    'Mastercard'       => '/images/creditcards/master-card.png',
);

$credit_card_image = '/images/creditcards/all.png';
$payment_method = '';
$card_detected = false;
if (!empty($creditCard)) {
    if ($creditCard->cardType == 'PayPal') {
        $payment_method = 'paypal';
    } else if ($creditCard->cardType == 'Coinbase') {
        $payment_method = 'bitcoin';
    } else {
        $payment_method = 'creditcard';
    }

    if (isset($credit_card_images[$creditCard->cardType])) {
        $credit_card_image = $credit_card_images[$creditCard->cardType];
        $card_detected = true;
    }
}

if (!empty($_POST['cardNumber'])) {
    $payment_method = 'creditcard';
}

$kiosk_cardNumber = null;
$kiosk_expMo = null;
$kiosk_expYr = null;

if (!empty($kiosk_access)) {
    $kiosk_cardNumber = $kiosk_access[0]->cardNumber;
    $kiosk_expMo = $kiosk_access[0]->expMo;
    $kiosk_expYr = $kiosk_access[0]->expYr;
}

?>

<?php

$fieldList = array();
$fieldList['firstName'] = 
            "<div class='control-group span2'>
                <label class='control-label' for='firstName'>
                    " . get_translation_for_view('billing_first_name', 'First Name') . "
                </label>

                <div class='controls'>
                    <input class='span2' id='firstName' type='text' value='" . set_value('firstName', $customer->firstName) . "' name='firstName'>
                </div>
            </div>";

$fieldList['lastName'] = 
            "<div class='control-group span2'>
                <label class='control-label' for='lastName'>
                    " . get_translation_for_view('billing_last_name', 'Last Name') . "
                </label>
                <div class='controls'>
                    <input class='span2' id='lastName' type='text' value='" . set_value('lastName', $customer->lastName) . "' name='lastName'>
                </div>
            </div>";

$nameForm = getSortedNameForm($fieldList);
                                    
//------------------

$fieldList = array();
$fieldList['address1'] = 
        "<div class='row'>
            <div class='control-group span4'>
                <label class='control-label' for='address'>
                    " . get_translation_for_view('billing_address', 'Billing Address')  . "
                </label>
                <div class='controls'>
                    <input id='address' class='span4' type='text' value='" . set_value('address1', $customer->address1)  . "' name='address1' >
                </div>
            </div>
        </div>";
$fieldList['address2'] = 
        "<div class='row'>
            <div class='control-group span4'>
                <label class='control-label' for='address2'>
                    " . get_translation_for_view('billing_address_subtitle', 'Billing Address 2')  . "
                </label>
                <div class='controls'>
                    <input id='address2' class='span4' type='text' value='" . set_value('address2', $customer->address2)  . "' name='address2' >
                </div>
            </div>
        </div>";
$fieldList['city'] = 
        "<div class='row'>
            <div class='control-group span4'>
                <label class='control-label' for='city'>
                    " . get_translation_for_view('billing_city', 'City')  . "
                </label>
                <div class='controls'>
                    <input id='city' class='span4' type='text' value='" . set_value('city', $customer->city) . "' name='city' >
                </div>
            </div>
        </div>";
$fieldList['state'] = 
        "<div class='row'>
            <div class='control-group span4'>
                <label class='control-label' for='state'>
                    " . get_translation_for_view('state', 'State')  . "
                </label>
                <div class='controls'> " . 
                    ( (isset($all_states[$this->business->country])) ? 
                        form_dropdown('state', $all_states[$this->business->country], set_value('state', $customer->state), 'id="state" class="span4"' )
                    :
                        form_input(array('id' => 'state', 'class' => 'span4', 'name' => 'state', 'value' => set_value('state', $customer->state))) )
                . " </div>
            </div>
        </div>";
$fieldList['zip'] = 
        "<div class='row'>
            <div class='control-group span4'>
                <label class='control-label' for='zip'>
                    " . get_translation_for_view('zip', 'Zipcode')  . "
                </label>
                <div class='controls'>
                    " . form_input(array('id' => 'zip', 'class' => 'span4', 'name' => 'zip', 'value' => set_value('zip', empty($creditCard->zip)?$this->customer->zip:$creditCard->zip)))  . "
                </div>
            </div>
        </div>";
        
$addressForm = getSortedAddressForm($fieldList);
        
?>
<? if ($business->businessID == 3 && !DEV): ?>
    <script type="text/javascript" src="https://callsocket.brightpattern.com/app/lib/servicepatternapi-dev.js"></script>
    <script type="text/javascript">
        window.bpspat.api.stopCurrentCallRecording();
    </script>
<? endif ?>

<ul class="breadcrumb">
  <li>
    <a href="/account"> <?=get_translation_for_view("account_home", "Account Home")?> </a> <span class="divider">/</span>
  </li>
  <li class="active">
    <?=get_translation_for_view("billing", "Billing")?>
  </li>
</ul>

<h2 class='page-header'><?=get_translation_for_view("payment_settings", "Payment Settings")?></h2>
<?= $this->session->flashdata('alert'); ?>

<p><?= get_translation_for_view("select_payment_method", "Please select your preferred payment method."); ?></p>

<div class="pay-options <?= $only_card ? 'no-border pay-options-horizontal' : ''?>">

    <div class="pay-option <?= $payment_method == 'creditcard' ? 'current' : '' ?>" data-option="creditcard">
        <div class="pay-option-wrap">
            <div class="pay-option-icon">

                <? if(!empty($accepted_cards) && !$card_detected): ?>
                    <? foreach($accepted_cards as $i => $card ): ?>
                        <? if(!empty($card_images[$card])): ?>
                            <img src="<?=$card_images[$card]?>" width="35">
                        <? endif ?>
                        <?= ($i && ($i % 2)) ? '<br>' : '' ?>
                    <? endforeach ?>
                <? else: ?>
                    <img src="<?= $credit_card_image ?>">
                <? endif ?>
            </div>
            <div class="pay-option-title"><?= get_translation_for_view("credit_card_option", "Credit Card"); ?></div>

            <div class="pay-option-actions">
                <? if ($payment_method == 'creditcard'): ?>
                <div class="pay-option-selected"><img src="/images/selected-icon.png"></div>
                <? else: ?>
                <div class="pay-option-action">
                    <? if (!$only_card): ?>
                    <a class="show-cc-form" href="javascript:;"><?= get_translation_for_view("add_credit_card", "Add a credit card"); ?></a>
                    <? endif; ?>
                </div>
                <? endif; ?>
            </div>
        </div>

        <? if ($payment_method == 'creditcard' && !empty($creditCard)): ?>
        <div class="pay-option-sub on-creditcard" style="display:<?= $payment_method == 'creditcard' ? 'block' : 'none' ?>">

            <h4><?=get_translation_for_view("credit_card_on_file", "Credit Card on File")?></h4>
            <p><?=get_translation_for_view("last", "Last 4:") ?> <?= $creditCard->cardNumber ?></p>
            <p><?=get_translation_for_view("expires", "Expires:") ?> <?= $creditCard->expMo ?>/<?= substr($creditCard->expYr, -2) ?></p>
            <p><a class="btn btn-primary show-cc-form" href="javascript:;"><?= get_translation_for_view("update_card", "Update Credit Card") ?></a></p>
            <form action='/account/profile/delete_card' method='post'>
                <input type="hidden" name="redirect" value="" />
                <input type='submit' value='<?=get_translation_for_view("delete_paypal", "Delete")?>' class='btn btn-danger' />
            </form>
        </div>
       <? endif; ?>
    </div>

    <? if ($show_paypal): ?>
    <div class="pay-option <?= $payment_method == 'paypal' ? 'current' : '' ?>" data-option="paypal">
        <div class="pay-option-wrap">
            <div class="pay-option-icon">
                <img src="/images/paypal-logo.png">
            </div>
            <div class="pay-option-title"><?= get_translation_for_view("paypal_option", "Paypal"); ?></div>

            <div class="pay-option-actions">
                <? if ($payment_method == 'paypal'): ?>
                <div class="pay-option-selected"><img width="" src="/images/selected-icon.png"></div>
                <? else: ?>
                <div class="pay-option-action">
                    <a href="/account/profile/paypal"><?= get_translation_for_view("paypal_subscribe_btn", "<img src='https://www.paypalobjects.com/en_US/i/btn/btn_subscribe_LG.gif'>") ?></a>
                </div>
                <? endif; ?>
            </div>
        </div>

        <div class="pay-option-sub on-paypal" style="display:<?= $payment_method == 'paypal' ? 'block' : 'none' ?>">
            <h4><?= get_translation_for_view("paypal_account", "PayPal Account") ?></h4>
            <? if ($payment_method == 'paypal' && !empty($creditCard)): ?>
                <p><?= $creditCard->cardNumber ?></p>
                <p><a href="/account/profile/paypal" class="btn btn-primary" id="add_paypal"><?= get_translation_for_view("change_paypal", "Change account") ?></a></p>
                <form action='/account/profile/delete_card' method='post'>
                    <input type="hidden" name="redirect" value="" />
                    <input type='submit' value='<?=get_translation_for_view("delete_paypal", "Delete")?>' class='btn btn-danger' />
                </form>
            <? else: ?>
                <p><a href="/account/profile/paypal" class="btn btn-primary" id="add_paypal"><?= get_translation_for_view("add_paypal", "Add account") ?></a></p>
            <? endif; ?>
        </div>
    </div>
    <? endif; ?>

    <? if ($show_bitcoin): ?>
    <div class="pay-option <?= $payment_method == 'bitcoin' ? 'current' : '' ?>" data-option="bitcoin">
        <div class="pay-option-wrap">
            <div class="pay-option-icon">
                <img src="/images/coinbase-logo.png">
            </div>
            <div class="pay-option-title"><?= get_translation_for_view("bitcoin_option", "Bitcoin"); ?></div>

            <div class="pay-option-actions">
                <? if ($payment_method == 'bitcoin'): ?>
                <div class="pay-option-selected"><img width="" src="/images/selected-icon.png"></div>
                <? else: ?>
                <div class="pay-option-action">
                    <a href="/account/profile/coinbase" class="add-btn" id="add_bitcoint">
                        <?= get_translation_for_view("coinbase_subscribe_btn", "<img src='/images/coinbase_subscribe.png'>") ?>
                    </a>
                </div>
                <? endif; ?>
            </div>
        </div>

        <div class="pay-option-sub on-bitcoin" style="display:<?= $payment_method == 'bitcoin' ? 'block' : 'none' ?>">
            <h4><?= get_translation_for_view("coinbase_account", "CoinBase Account") ?></h4>
            <? if ($payment_method == 'bitcoin' && !empty($creditCard)): ?>
                <p><?= $creditCard->cardNumber ?></p>
                <p><a href="/account/profile/coinbase" class="btn btn-primary" id="add_bitcoin"><?= get_translation_for_view("change_bitcoin", "Change account") ?></a></p>
                <form action='/account/profile/delete_card' method='post'>
                    <input type="hidden" name="redirect" value="" />
                    <input type='submit' value='<?=get_translation_for_view("delete_bitcoin", "Delete")?>' class='btn btn-danger' />
                </form>
            <? else: ?>
                <p><a href="/account/profile/coinbase" class="btn btn-primary" id="add_bitcoin"><?= get_translation_for_view("add_bitcoin", "Add account") ?></a></p>
            <? endif; ?>
        </div>
    </div>
    <? endif; ?>

    <? if (!$only_card): ?>
    <div style="clear:both;"></div>
    <? endif; ?>
</div>

<div class="on-creditcard row" id="credit_card_form_holder" class="row" style="display:<?= $payment_method == 'creditcard' || $only_card ? 'block' : 'none' ?>">
<form action='' id="credit_card_form" method='post' class="form span4 credit-card-form" style="display:<?= $payment_method != 'creditcard' || empty($creditCard) ? 'block' : 'none' ?>">

    <fieldset id="credit_card_fields">
        <? if (!$only_card): ?>
        <legend><?= get_translation_for_view("update_card", "Update Credit Card"); ?></legend>
        <? endif; ?>

        <div class="row">
            <div class="control-group span4">
                <label class="control-label" for="cardNumber">
                    <?= get_translation_for_view("card_number", "Card Number"); ?>
                </label>
                <div class="controls">
                    <input type='text' value="<?= set_value("cardNumber") ?>" name='cardNumber' id="cardNumber" class="span4" autocomplete="off" />

                    <? if ($show_accepted_cards): ?>
                    <p class="help-block">
                        <? if(!empty( $accepted_cards)): ?>
                            <? foreach( $accepted_cards as $card ): ?>
                                <? if(!empty($card_images[$card])): ?>
                                    <img src="<?=$card_images[$card]?>" />
                                <? endif ?>
                            <? endforeach ?>
                        <? else: ?>
                            <img align='absmiddle' src='/images/cardTypes.gif' />
                        <? endif ?>
                    </p>
                    <? endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="control-group span2">
                <label class="control-label" for="expMo">
                    <?=get_translation_for_view("expiration", "Expiration") ?>
                </label>
                <div class="controls fit-selects">
                    <?= form_dropdown("expMo", $months, set_value("expMo"), "id='expMo' class='up-down'>") ?>
                    <span class="separator">/</span>
                    <?= form_dropdown("expYr", $years, set_value("expYr"), "id='expYr' class='up-down'"); ?>
                </div>
            </div>

            <div class="control-group span2">
                <label class="control-label" for="csc">
                    <?= get_translation_for_view("security", "Security Code") ?>
                </label>
                <div class="controls">
                    <input type='text' id='csc' name='csc' value="<?= set_value("csc")?>" autocomplete="off" style='width:50px;'>
                </div>
            </div>
        </div>

        <div class="billing-info" style="display:<?= $has_billing_info ? 'block' : 'none' ?>">
            <h4><?= get_translation_for_view("billing_info", "Billing Info"); ?></h4>
            <p>
                <?= $customer_billing_name ?><br>
                <?= $customer_billing_address ?>
                <?php 
                $edit_btn_extra_classes = "";
                if ($processor == "conekta"):
                    $edit_btn_extra_classes = "btn btn-primary";
                endif;
                ?>
                <a id="edit_billing_info" class="pull-right <?php echo $edit_btn_extra_classes; ?>" href="javascript:"><?= get_translation_for_view("edit_billing_info", "Edit"); ?></a>
            </p>
        </div>

    </fieldset>

    <fieldset id="billing_info_fields" style="display:<?= $has_billing_info ? 'none' : 'block' ?>;">
        <legend><?= get_translation_for_view("billing_info", "Billing Info"); ?></legend>

        <div class="row">
            <?= $nameForm ?>
        </div>
    <?php if ($processor == "conekta"): ?>
        <div class="row">
            <div class="control-group span4">
                <label class="control-label" for="phone_number">
                    <?= get_translation_for_view("billing_address_phone_number", 'Teléfono') ?>
                </label>
                <div class="controls">
                    <input id='phone_number' class="span4" type='text' value="<?= set_value("phone_number", empty($creditCard->phone_number)?$this->customer->phone:$creditCard->phone_number) ?>" name='phone_number' >
                </div>
            </div>
        </div>
    <?php endif; ?>
        <?= $addressForm ?>
    </fieldset>

    <? if ($business->store_access): ?>
        <? if($hideKioskCheckbox): ?>
            <input type='hidden' value='1' name='kioskAccess' />
        <? else: ?>
            <div class='alert alert-info'>
                <label class="checkbox">
                    <input name="kioskAccess" type="checkbox" value="1" checked>
                    <?= get_translation_for_view("use_this_card", "Use this card to access retail locations (Open 24 Hrs)") ?>
                </label>
            </div>
        <? endif; ?>
    <? endif; ?>

     <div style="text-align: center; margin-top: 20px;">
         <?= form_submit("", get_translation_for_view("add_creditcard", "Add Credit Card"), "class='btn btn-primary'") ?>
     </div>
</form>
</div>


<? if ($business->store_access): ?>

<hr style="clear:both;">

<h3 id="access"><?= get_translation_for_view("store_access", "After hour store access") ?></h3>
<p><?= get_translation_for_view("add_store_access", "Add a card below for after hour store access."); ?></p>

<div class="row kiosk-access">

    <div class="span2 kiosk-access-info">
        <div class="kiosk-access-bg">
            <? if (empty($kiosk_access)): ?>
            <div class="kiosk-access-error">
                <?= get_translation_for_view("kiosk_access_no", "No after hour access") ?>
            </div>
            <? else: ?>
            <div class="kiosk-access-success">
                <?= get_translation_for_view("kiosk_access_yes", "Authorized") ?>
            </div>
            <? endif; ?>
        </div>
    </div>

    <div class="span4 kiosk-access-form">
        <form method="post" action="/account/profile/update_kiosk_card" id="update_kiosk_card_form">
            <div class="row">
                <div class="control-group span4">
                    <label for="kiosk_cardNumber">
                        <?= get_translation_for_view("kiosk_card_number", "Card Number (Last Four Only)") ?>
                    </label>
                    <div class="controls">
                        <span class="cc-mask">**** **** **** ****</span>
                        <input id="kiosk_cardNumber" type='text' name='cardNumber' value="<?= $kiosk_cardNumber ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span2">
                    <div class="control-group">
                        <label for="kiosk_expMo">
                            <?=get_translation_for_view("kiosk_expiration", "Expiration") ?>
                        </label>
                        <div class="controls fit-selects">
                            <?= form_dropdown("expMo", $months, $kiosk_expMo, "id='kiosk_expMo' class='up-down'>") ?>
                            <span class="separator">/</span>
                            <?= form_dropdown("expYr", $short_years, $kiosk_expYr, "id='kiosk_expYr' class='up-down'"); ?>
                        </div>
                    </div>
                </div>

                <div class="span2">
                    <div class="control-group kiosk-access-buttons">
                        <? if (!empty($kiosk_access)): ?>
                            <p><input type='submit' id="remove_access_button" name="delete" value='<?=get_translation_for_view("kiosk_delete", "Remove access")?>' class='btn btn-danger span2'></p>
                            <p><input type='submit' id="update_access_button" value='<?=get_translation_for_view("kiosk_update", "Update Access")?>' class='btn btn-primary span2'></p>
                        <? else: ?>
                            <p>&nbsp;</p>
                            <p><input type='submit' id="setup_access_button"value='<?=get_translation_for_view("kiosk_setup", "Setup Access")?>' class='btn btn-primary span2'></p>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<? endif; ?>

<script type="text/javascript" src="/js/jquery.payment.js"></script>
<script type="text/javascript">

function showCreditCardForm() {
    $('html, body').animate({
        scrollTop: ($('#credit_card_form_holder').offset().top - 10) + 'px'
    }, {
        complete: function() { 
            $('#credit_card_form').slideDown('slow');
            $('#credit_card_form').attr('style','');
        }

    }, 'fast');
}

$('.show-cc-form').click(function(evt) {
    showCreditCardForm();
});


$('.pay-option-wrap').click(function(evt) {
    var current = $('.pay-options .pay-option.selected');
    var option = current.attr('data-option');
    $('.on-' + option).hide();
    current.removeClass('selected');

    $(this).parent().addClass('selected');
    var new_option = $(this).parent().attr('data-option');
    
    var items = $('.on-' + new_option).fadeIn('slow');

    if ($('body').width() <= 979) {
        $('html, body').animate({
            scrollTop: ($(items[0]).offset().top - 10) + 'px'
        });
    }
});


$('#edit_billing_info').click(function(evt) {
    evt.preventDefault();
    $('.billing-info').hide();
    $('#billing_info_fields').fadeIn('slow');
});

jQuery.validator.addMethod("ccNumber",
    function(value, element) {
        var val = value ? value.replace(/[^0-9]/g, '') : '';
        return $.payment.validateCardNumber(value);
    },""
);

$("#credit_card_form").validate({
    rules: {
        csc: {
            required: true,
            digits: true,
            maxlength: 4
        },
        cardNumber: {
            required: true,
            ccNumber: true,
        },
        address1:{
            required: true
        },
        city:{
            required:true
        },
        state:{
            required:true
        }
        <?php if ($processor == "conekta"): ?>
        ,
        zip:{
            required:true
        }
        ,phone_number:{
            required:true
        }
        <?php endif; ?>
    },
    messages: {
        csc: "<?=get_translation_for_view("invalid_csc_message", "Please enter your credit card's CSC")?>",
        cardNumber : "<?=get_translation_for_view("invalid_card_number_message", "Please enter a valid credit card number")?>",
        address1 : "<?=get_translation_for_view("invalid_address_message", "Please enter an address")?>",
        city: "<?=get_translation_for_view("invalid_city_message", "Please enter a city")?>",
        state: "<?=get_translation_for_view("invalid_state_message", "Please enter a state")?>",
        zip: "<?=get_translation_for_view("invalid_zip_message", "Please enter a zip code")?>",
        phone_number: "<?=get_translation_for_view("invalid_phone_number_message", "Please enter a phone number")?>"
    },
    errorClass: "invalid",
    submitHandler: function(form) {
       form.submit();
       $(":submit", form).attr("disabled", true);
    }
});


$("#update_kiosk_card_form").validate({
    rules: {
        cardNumber: {
            digits: true,
            required: true,
            maxlength: 4,
            minlength: 4
        }
    },
    messages: {
        cardNumber : "<?=get_translation_for_view("invalid_kiosk_card_number_message", "Enter the last 4 digits of the card number")?>",
    },
    errorClass: "invalid",
    submitHandler: function(form) {
       form.submit();
       $(":submit", form).attr("disabled", true);
    }
});

$('#cardNumber').payment('formatCardNumber');

</script>
