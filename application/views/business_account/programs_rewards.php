<?php
/**
 * Expects at least some of the content variables
 *  facebookLink string
 *  twitterLink string
 *  programName string
 *  business stdClass
 *  facebook_share_rewards_program_thumbnail_url string
 *  facebookLink
 */


if (!isset($facebook_share_rewards_program_thumbnail_url)) {
    trigger_error("'facebook_share_rewards_program_thumbnail_url' must be set");
}


$text_macros = array(
    "promo_code" => $promo_code,
    "url" => $lockerLootEndPoint,
    "companyName" => $business->companyName,
    "programName" => $programName,
    "totalReward" => format_money_symbol($business->businessID, '%.0n', $totalReward, 0),
    "website_url" => $business->website_url,
);

?>
<script type="text/javascript" src="//connect.facebook.net/en_US/all.js"> </script>
<script type="text/javascript">
$(document).ready(function() {
    FB.init({
        appId: "<?=$business->facebook_api_key?>",
        cookie: true

    });
    $("#share_rewards_program_with_facebook").click(function() {
        FB.ui(
        {
          method: 'feed',
          name: <?= json_encode(get_translation_for_view("rewards_program_facebook_share_title", "%companyName% changed the way I do my Dry Cleaning and Wash &amp; Fold", $text_macros)) ?>,
          link: "<?=$lockerLootEndPoint?>",
          picture: "<?=$facebook_share_rewards_program_thumbnail_url?>",
          caption: <?= json_encode(get_translation_for_view("rewards_program_facebook_share_caption", "24/7 Dry Cleaning and Laundry", $text_macros)) ?>,
          description: <?= json_encode(get_translation_for_view("rewards_program_facebook_share_description", "Place your dirty clothes in a secure Locker, pick it up clean the next day. Get 25% off your first order with promo code %promo_code% at %url%", $text_macros)) ?>
        },
        function(response) {
          if (response && response.post_id) {
            alert('Post was published.');
          } else {
            alert('Post was not published.');
          }
        }
        );
    });
});
</script>

<?= $this->session->flashdata('alert'); ?>

<div>
    <h2 class='page-header'><?= $programName ?></h2>
    <div class="hidden-desktop">
    <p><h3 style='border:0px'>
    <?=get_translation_for_view("balance", "Balance")?>: 
    <?= ($balance>0)?format_money_symbol($this->business->businessID, '%.2n', $balance):format_money_symbol($this->business->businessID, '%.2n',  0.00);?></h3>
    <?if($balance>0): ?>
    <a href='/account/programs/redeem_points' class='btn btn-primary'><?=get_translation_for_view("redeem", "Redeem Credit")?></a>
        <? if($balance >= $minRedeemAmount && !empty($this->rewards_config->allowToRedeem) ): ?>
            <p>
            <a href='/account/programs/redeem_cash' class='btn btn-primary'><?=get_translation_for_view("check", "Send Check for")?> <?= format_money_symbol($this->business->businessID, '%.2n', $redeemAmount)?></a>
            </p>
        <? endif; ?>
    
    <? endif; ?>
    <br/>
    </p>

    </div>
    <br>
    <h3>
        <?= get_translation_for_view("title",
                "Refer a friend and receive %totalReward%!!!",
                array("totalReward" => format_money_symbol($business->businessID, '%.0n', $totalReward, 0)))
        ?>
    </h3>
    
    <p>
        <?= get_translation_for_view("description_announcement",
                "%companyName% has just launched a tremendous new program.",
                array("companyName" => $business->companyName))
        ?>
        <?php if( $percentPerOrder > 0 ): ?>
            <?= get_translation_for_view("description_percentPerOrder",
                    "Along with the %percentPerOrder% in %programName% per %per_spending_amount% you spend, you will now receive up to %totalReward% for every customer you refer to us!",
                    array(
                        "percentPerOrder" => format_money_symbol($business->businessID, '%.0n', $percentPerOrder, 0),
                        "programName" => $programName,
                        "per_spending_amount" => format_money_symbol($business->businessID, '%.0n',  100, 0),
                        "totalReward" => format_money_symbol($business->businessID, '%.2n', $totalReward)
                    ))
            ?>
        <?php else:?>
            <?= get_translation_for_view("description_notPercentPerOrder",
                    "You will now receive up to %totalReward% for every customer you refer to us!",
                    array("totalReward" =>  format_money_symbol($business->businessID, '%.2n', $totalReward)))
        ?>
        <?php endif ?>
        <?php if($allowToRedeem): ?>
            <?= get_translation_for_view("description_allowToRedeem",
                    "And now you can convert your %programName% to cash!!!",
                    array("programName" => $programName))
            ?>
        <?php endif;?>
    </p>
    <p></p>
    <h3><?= get_translation_for_view("how_it_works_title", "How it works") ?></h3>
    <p></p>
    <p>
        <?= get_translation_for_view("how_it_works_description",
                "Referring a friend couldn't be easier. When a friend of yours signs up for %companyName%, they just need to enter your promotional code. They then become part of your referral network, and they will receive 25% off their first order.",
                array("companyName" => $business->companyName))
        ?>
    </p>
    <p>
        <?= get_translation_for_view("refer_a_friend_description",
                "The \"Refer a Friend... Get %totalReward% offer pays %referralAmountPerReward% for every paid order over %referralOrderAmount% (after discounts) from your referred customer, for the first %referralNumberOfOrders% orders over %referralOrderAmount% (after discounts). There is no cap on earnings, or the number of referrals you may enroll.",
                array(
                    "totalReward" => format_money_symbol($business->businessID, '%.2n', $totalReward),
                    "referralAmountPerReward" => format_money_symbol($business->businessID, '%.2n', $referralAmountPerReward),
                    "referralOrderAmount" => format_money_symbol($business->businessID, '%.2n', $referralOrderAmount),
                    "referralNumberOfOrders" => $referralNumberOrders,
                    "referralOrderAmount" => format_money_symbol($business->businessID, '%.2n', $referralOrderAmount)
                ))
        ?>

    </p>
    <h2 style='border: 0px;' class="promo-code-text">
        <?= get_translation_for_view("promotional_code",
                "Your promotional code is: %promo_code%",
                array("promo_code" => $promo_code))
        ?>
    </h2>
    <br>
    <h2> <?= get_translation_for_view("sharing_title", "Sharing") ?> </h2>
    <br>
    <h3> <?= get_translation_for_view("social_networks_title", "Social Networks") ?></h3>
    <p> <?= get_translation_for_view("social_networks_description", "Facebook and Twitter. You know how to use them! Click the icons below and you can quickly share your promo code with your friends.") ?></p>
    <p>
        <img id="share_rewards_program_with_facebook"
            src="/images/facebook-logo.png" alt="<?= get_translation_for_view('facebook_alt', 'Share with Facebook'); ?>"
            border="0" style="cursor: pointer;">
        <a
            href="<?= $twitterLink ?>"
            target="_blank"
            title="<?= get_translation_for_view('twitter_tooltip', "%companyName% changed the way I do my Dry Cleaning and Wash &amp; Fold.", array('companyName' => $business->companyName)); ?>">
            <img src="/images/twitter-logo.png" alt="" border="0">
        </a>
    </p>
    <p></p>
    <h3> <?= get_translation_for_view("email_link_to_friend_title", "Email a link to your friend") ?></h3>
    <p></p>
    <p>
        <?= get_translation_for_view("email_link_to_friend_description", "If you want to email someone a link to the new user registration form, copy and paste this URL so that your promotional code is automatically populated:") ?>
    </p>
    <p></p>
    <?php

    echo form_input(array('name'=>'email_link', "style" => "padding:5;width: 100%;", "id" => "email_link", "value" => $lockerLootEndPoint));
    ?>

    <p></p>
    <h3> <?= get_translation_for_view("print_flyers_title", "Print Flyers") ?> </h3>
    <p></p>
    <p>
        <?= get_translation_for_view("print_flyers_description", "You can also <a target='_blank' style='font-weight: bold' href='/account/programs/print_coupon'> Print Out Flyers </a> with your promotional code on them and a <strong>25% off coupon</strong>. Hang these up in your building, put them under the new tenant's door or give them to your co-workers.") ?>
    </p>
    <p></p>
    <h3> <?= get_translation_for_view("how_to_redeem_title",
                "How to redeem your %programName%",
                array("programName" => $programName))
        ?>
    </h3>
    <p></p>
    <p> <?= get_translation_for_view("how_to_redeem_description",
                "%programName% is just like airline miles. Every time you use our service, you earn %programName%. Your %programName% can then be redeemed to pay for your %companyName% service",
                array(
                    "programName" => $programName,
                    "companyName" => $business->companyName
                ))
        ?>
        <? if($allowToRedeem): ?>
         <?= get_translation_for_view("how_to_redeem_description_allowed_to_redeem", "or as cash*") ?>
       <?endif; ?>
      <?= get_translation_for_view("how_to_redeem_description_instruction", "by simply clicking the REDEEM button on the right.") ?></p>

    <?php if($allowToRedeem): ?>
    <p>
        <span style='font-size: 12px; padding: 5px;'>
        <?=
            get_translation_for_view("allowToRedeem_requirement",
                "*In order to redeem for cash, you must have a %programName% balance of over %minRedeemAmount%.",
                array(
                    "programName" => $programName,
                    "minRedeemAmount" => $minRedeemAmount
                ))
        ?></span>
    </p>
    <?php endif; ?>
    <p></p>
    <h3><?= get_translation_for_view("customers_earning_title",
                "Customers earning %programName% for you:",
                array("programName" => $programName))
        ?>
    </h3>
    <p></p>
    <p></p>
    <table class="table table-bordered table table-bordered">
        <thead>
            <tr>
                <th style="text-align: left;"><?= get_translation_for_view("name", "Name") ?> </th>
                <th style="text-align: right;"><?= get_translation_for_view("last_order", "Last Order") ?></th>
                <th style="text-align: right;"><?= get_translation_for_view("points_earned", "Points Earned") ?> </th>
            </tr>
        </thead>
        <tbody>
            <? if($earners): foreach($earners as $e):?>
            <tr>
                <td><?= getPersonName($e) ?></td>
                <td style="text-align: right;"><?= $days[$e->customerID]?></td>
                <td style="text-align: right;"><?= format_money_symbol($business->businessID, '%.2n', $points[$e->customerID])?></td>
            </tr>
            <? endforeach; else: ?>
            <tr>
                <td colspan='5'><?= get_translation_for_view("no_customer_earners", "Nothing Yet") ?> </td>
            </tr>
            <? endif; ?>
        </tbody>
    </table>
    <p></p>
</div>

