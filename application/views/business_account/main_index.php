<? include 'notice.php'; ?>


<?= $this->session->flashdata('alert') ?>
<?= $this->session->flashdata('message'); ?>

<h2 class="page-header"><?=get_translation_for_view("account_section", "Account Section")?></h2>

<?  if($bouncedEmail): ?>
    <div class='alert alert-error'>
        <?=get_translation_for_view("email_error", "The email address for this account bounced the last time we tried to send a notification. Please verify that the email is working properly or update your email.")?> <br>
        <a href='/account/profile/email' class='btn btn-danger'><?=get_translation_for_view("update_email", "Update Email")?></a>
    </div>
<? endif; ?>


    <div class="menu-item">
        <h3><a href='/account/orders/new_order'><?=get_translation_for_view("place_new_order_title", "Place New Order")?></a></h3>
        <div><?= get_translation_for_view("place_new_order_subtitle", "Let us know when you have an order to pick up") ?></div>
    </div>

    <div class="menu-item">
        <h3><a href='/account/orders/view_orders'><?= get_translation_for_view("my_orders_title", "My Orders")?></a></h3>
        <div><?= get_translation_for_view("my_orders_subtitle", "View your orders and make payment arrangements")?></div>
    </div>

<? if ($business->store_access): ?>
    <div class="menu-item">
        <h3><a href='/account/profile/kiosk_access'> <?= get_translation_for_view("store_access_title", "Store Access")?></a></h3>
        <div> <?= get_translation_for_view("store_access_subtitle", "Register your credit card for access to our stores") ?> </div>
    </div>
<? endif; ?>

    <div class="menu-item">
        <h3><a href='/account/profile/edit'> <?= get_translation_for_view("account_settings_title", "My Account Settings") ?> </a></h3>
        <div><?= get_translation_for_view("account_settings_subtitle", "Make changes to your account information") ?></div>
    </div>

    <div class="menu-item">
      <h3><a href='/account/profile/preferences/'><?= get_translation_for_view("laundry_settings_title", "Laundry Settings") ?> </a></h3>
      <div><?= get_translation_for_view("laundry_settings_subtitle", "Update your laundry preferences") ?></div>
    </div>

    <div class="menu-item">
        <h3><a href='/account/profile/closet'><?= get_translation_for_view("my_closet", "My Closet")?></a></h3>
        <div><?= get_translation_for_view("view_pictures_subtitle", "View Pictures and make notes on your dry cleaned items")?></div>
    </div>

    <div class="menu-item">
        <h3><a href='/account/profile/billing'><?=get_translation_for_view("credit_card", "Credit Card")?></a></h3>
        <div><?= get_translation_for_view("credit_card_subtitle", "Change your credit card")?></div>
    </div>

    <div class="menu-item">
        <h3><a href='/account/programs/discounts'><?=get_translation_for_view("discounts", "Discounts")?></a></h3>
        <div><?= get_translation_for_view("discounts_subtitle", "View Discounts / apply coupons")?></div>
    </div>

    <? if(get_business_meta($this->business_id, 'sell_gift_cards')): ?>
        <div class="menu-item">
            <h3><a href='/account/programs/gift_cards'><?=get_translation_for_view("gift_cards", "Gift Cards")?></a></h3>
            <div><?=get_translation_for_view("gift_card_subtitle", "Buy gift cards for friends and family")?></div>
        </div>
    <? endif; ?>

<? if($lockerLootActive): ?>
    <div class="menu-item">
       <h3><a href='/account/programs/rewards'><?=
            get_translation_for_view("rewards_title",
                "%program_name% Rewards",
                array("program_name" => $programName))
        ?></a></h3>
        <div><?=get_translation_for_view("rewards_subtitle", "Customer loyalty and referral rewards program")?></div>
    </div>
<? endif; ?>

<? if( count( $this->plans ) >= 1 ):?>
    <div class="menu-item">
        <h3><a href='/main/plans'><?=get_translation_for_view("laundry_plans", "Laundry Plans")?></a></h3>
        <? if (isset($active_plan->laundryPlanID)): ?>
        <div class='alert alert-success'>
            <img align="absmiddle" src='/images/icons/accept.png' /> <?=get_translation_for_view("current_plan_subscription", "Your Current Laundry Plan: %description%", array("description" => $active_plan->description))?>
        </div>
        <? endif; ?>
        <div><?=get_translation_for_view("laundry_plans_subtitle", "Great savings with our laundry plans")?></div>
    </div>
<?endif;?>

<? if( count( $this->everything_plans ) >= 1 ):?>
    <div class="menu-item">
        <h3><a href='/main/everything_plans'><?=get_translation_for_view("everything_plans", "Everything Plans")?></a></h3>
        <? if (isset($active_everything_plan[0]->laundryPlanID)): ?>
        <div class='alert alert-success'>
            <img align="absmiddle" src='/images/icons/accept.png' /> <?=get_translation_for_view("current_everything_plan_subscription", "Your Current Everything Plan: %description%", array("description" => $active_everything_plan[0]->description))?>
        </div>
        <? endif; ?>
        <div><?=get_translation_for_view("everything_plans_subtitle", "Great savings with our everything plans")?></div>
    </div>
<?endif;?>

<? if( count( $this->product_plans ) >= 1 ):?>
    <div class="menu-item">
        <h3><a href='/main/product_plans'><?=get_translation_for_view("product_plans", "Product Plans")?></a></h3>
        <? if (isset($active_product_plan[0]->laundryPlanID)): ?>
        <div class='alert alert-success'>
            <img align="absmiddle" src='/images/icons/accept.png' /> <?=get_translation_for_view("current_product_plan_subscription", "Your Current Product Plan: %description%", array("description" => $active_product_plan[0]->description))?>
        </div>
        <? endif; ?>
        <div><?=get_translation_for_view("product_plans_subtitle", "Great savings with our product plans")?></div>
    </div>
<?endif;?>

<div class='visible-phone'>
    <p style="margin-top:20px;">
        <a href="/account/orders/new_order" class='btn btn-success btn-large span12'><i class='icon-white icon-shopping-cart'></i><?=get_translation_for_view("place_new_order_title", "Place New Order")?></a>
    </p>
</div>

<br>

<? if(!$isProfileComplete): ?>
    <div style='display:none' class='alert alert-error'><?= get_translation_for_view("profile_is_not_complete", "Profile is Not Complete")?><a href='/account/profile'><?=get_translation_for_view("complete_profile", "Complete Profile")?></a></div>
<? endif; ?>
