<?php

$show_delivery_fees = false;

$all_states = get_all_states_by_country_as_array();
$hide_driver_notes = get_business_meta($this->business_id, "home_delivery_hide_driver_notes");
?>
<ul class="breadcrumb">
  <li>
    <a href="/account"> <?=get_translation_for_view("account_home", "Account Home")?> </a> <span class="divider">/</span>
  </li>
  <li>
    <a href="/account/orders/view_orders"> <?=get_translation_for_view("view_orders", "View Orders")?> </a> <span class="divider"> / </span>
  </li>
  <li class="active"><?=get_translation_for_view("home_delivery", "Home Delivery")?> [<?php echo $order->orderID?>]</li>
</ul>


<h2 class='page-header'><?=get_translation_for_view("schedule_home_delivery", "Schedule a home delivery")?></h2>

<?= $this->session->flashdata('alert'); ?>

<form method="post">
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <?= get_translation_for_view('home_delivery', 'Home Delivery') ?> -
            <? if (isset($orderTypes[$order->orderType])): ?>
                <?= get_translation($orderTypes[$order->orderType], "service_types", array(), $orderTypes[$order->orderType]) ?>
            <? else: ?>
                <?= $order->orderType ?>
            <? endif; ?>
            <div class="pull-right">
                <?= get_translation_for_view('order_id', 'Order ID:') ?>
                <span class="muted"><?= $order->orderID ?></span>
            </div>
        </h4>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <p class="semi-lead">
                <b><?= get_translation_for_view('locker', 'Locker:') ?></b> <? if(strtolower($locker->lockerName)!='inprocess'){ echo $locker->lockerName; } ?><br>
            </p>
            <br>
            <? if ($step >= 1):  ?>
            <div class="step">
                <h3><?= get_translation_for_view('deliver_to', '1. Deliver To:'); ?></h3>
                <div class="step-content">
                    <div class="border-block">
                        <?= $homeDelivery->address1 ?><br>
                        <?= $homeDelivery->address2 ? $homeDelivery->address2 . '<br>' : ''?>
                        <?= $homeDelivery->city ?>, <?= $homeDelivery->state ?> <?= $homeDelivery->zip ?>
                    </div>
                    <a href="#edit_address_modal" id="edit_address" data-toggle="modal"><?= get_translation_for_view('change_delivery_address', 'Change delivery address') ?></a>
                </div>
            </div>
            <? endif; ?>

            <? if ($step >= 2): ?>
            <div class="step">
                <h3><?= get_translation_for_view('select_delivery_title', '2. Select delivery date/time:'); ?></h3>

                <div class="step-content">
                    <ul class="nav nav-pills">
                        <li class="dropdown active step-width" id="select_delivery_date" style="clear:both;">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <? if (empty($homeDelivery->deliveryDate) || $homeDelivery->deliveryDate == '0000-00-00'): ?>
                                    <?= get_translation_for_view('select_delivery_day', 'Select a delivery day'); ?>
                                <? else: ?>
                                    <?= strftime($date_format, strtotime($homeDelivery->deliveryDate)) ?>
                                <? endif; ?>
                                <b class="caret pull-right"></b>
                            </a>
                            <ul class="dropdown-menu" style="top:0;left:100%;">
                                <? foreach($delivery_dates as $delivery_date): ?>
                                <li class="group-title">
                                <a href="#" data-date="<?= $delivery_date ?>">
                                <?php
                                $pickup_date_timestamp = strtotime($delivery_date);
                                
                                $day = strftime("%A", $pickup_date_timestamp);
                                $month = strftime("%B", $pickup_date_timestamp);                
                                $day_short = strftime("%a", $pickup_date_timestamp);
                                $month_short = strftime("%b", $pickup_date_timestamp);
                                
                                $dates_findme = array($day, $month, $day_short, $month_short);

                                $dates_replacements = array(
                                    get_translation_for_view($day, $day),
                                    get_translation_for_view($month, $month),
                                    get_translation_for_view($day_short, $day_short),
                                    get_translation_for_view($month_short, $month_short)
                                );

                                echo str_replace($dates_findme,  $dates_replacements, strftime($date_format, strtotime($delivery_date)));    
                                ?>                                        
                                </a></li>
                                <? endforeach ?>
                            </ul>
                        </li>
                        <li class="dropdown active step-width" id="select_delivery_time" style="clear:both;<?= empty($homeDelivery->deliveryDate) ? 'display:none;' : ''?>">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <? if (empty($homeDelivery->windowStart) && empty($homeDelivery->windowEnd)): ?>
                                    <?= get_translation_for_view('select_delivery_time', 'Select a delivery time'); ?>
                                <? else: ?>
                                    <?= convert_from_gmt_aprax($homeDelivery->windowStart, $time_format, $homeDelivery->business_id) ?>-<?= convert_from_gmt_aprax($homeDelivery->windowEnd, $time_format, $homeDelivery->business_id) ?>
                                <? endif; ?>
                                <b class="caret pull-right"></b>
                            </a>
                            <ul class="dropdown-menu" style="top:0;left:100%;">
                                <? foreach($delivery_windows as $group): ?>
                                    <? if ($show_delivery_fees): ?>
                                    <li class="group-title" style="<?=$homeDelivery->deliveryDate == $group['date'] ? '' : 'display:none;' ?>">
                                        <a href="#" data-date="<?= $group['date'] ?>"><?php
                                        if (!empty($group['fee'])):
                                            get_translation_for_view('fee_title', 'Delivery Fee %fee%', array('fee' => format_money_symbol($homeDelivery->business_id, '%.2n', $group['fee'])));
                                        endif;
                                        ?></a>
                                    </li>
                                    <? endif; ?>
                                    <? foreach($group['items'] as $delivery_window):  ?>
                                    <li class="sub-option" style="<?=$homeDelivery->deliveryDate == $group['date'] ? '' : 'display:none;' ?>">
                                        <a href="#" data-date="<?= $group['date'] ?>" data-id="<?= $delivery_window['id'] ?>" data-start="<?= $delivery_window['start_time'] ?>"  data-end="<?= $delivery_window['end_time'] ?>">
                                            <?= date($time_format, strtotime($delivery_window['start'].':00')) ?>-<?= date($time_format, strtotime($delivery_window['end'].':00')) ?>
                                        </a>
                                    </li>
                                    <? endforeach ?>
                                <? endforeach ?>
                            </ul>
                        </li>
                    </ul>

                    <input type="hidden" name="deliveryDate" value="<?= $homeDelivery->deliveryDate ?>">
                    <input type="hidden" name="windowStart" value="<?= $homeDelivery->windowStart ? $homeDelivery->windowStart->format('Y-m-d H:i:s') : '' ?>">
                    <input type="hidden" name="windowEnd" value="<?= $homeDelivery->windowEnd ? $homeDelivery->windowEnd->format('Y-m-d H:i:s') : '' ?>">
                    <input type="hidden" name="delivery_window_id" value="<?= $homeDelivery->delivery_window_id ?>">
                </div>
            </div>
            <? endif; ?>

            <? if ($step >= 3): ?>
            <div class="step">
                <h3><?= get_translation_for_view('confirm_order', '3. Confirm order'); ?></h3>
                <div class="step-content">
                <?php if (!$hide_driver_notes): ?>
                    <label for="homedelivery_nores"><?= get_translation_for_view('homedelivery_nores', 'Instructions for the driver:'); ?></label>
                    <textarea id="homedelivery_nores" name="notes" rows="4"><?= htmlentities($homeDelivery->notes) ?></textarea>
                <?php endif; ?>
                    <div><button type="submit" class="btn btn-success btn-large step-width" name="schedule" value="1"><?= get_translation_for_view("schedule_home_delivery", "Schedule home delivery") ?></button></div>
                </div>
            </div>
            <? endif; ?>

            <? if ($step < 3 && $can_continue): ?>
            <div>
                <button type="submit" class="btn btn-success btn-large step-width" name="goto" value="<?= $step + 1 ?>"><?= get_translation_for_view("continue", "Continue") ?></button>
            </div>
            <? endif; ?>

        </div>
    </div>
</div>
</form>

<form class='form-horizontal' id="account_settings" method="post" action=''>

<div class="modal hide fade" id="edit_address_modal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3><?= get_translation_for_view("deliver_address_form_title", "Change delivery address"); ?></h3>
  </div>

  <div class="modal-body">
        <div class="control-group">
            <label class="control-label" for="address"><?=get_translation_for_view("address", "Address")?></label>
            <div class="controls">
                <?= form_input(array("id" => "address", "name" => "address", "value" => set_value("address", $homeDelivery->address1))) ?>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="address2"><?=get_translation_for_view("address_subtitle", "Address 2")?></label>
            <div class="controls">
                <?= form_input(array("id" => "address2", "name" => "address2", "value" => set_value("address2", $homeDelivery->address2))) ?>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="city"><?=get_translation_for_view("city", "City")?></label>
            <div class="controls">
                <?= form_input(array("id" => "city", "name" => "city", "value" => set_value("city", $homeDelivery->city))) ?>
            </div>
        </div>

    <? if (isset($all_states[$business_country])): ?>
        <div class="control-group">
            <label class="control-label" for="state"><?=get_translation_for_view("state", "State")?></label>
            <div class="controls">
                <?= form_dropdown("state", $all_states[$business_country], set_value("state", $homeDelivery->state), 'id="state"' ) ?>
            </div>
        </div>
    <? else: ?>
        <div class="control-group">
            <label class="control-label" for="state"><?=get_translation_for_view("locality", "Locality")?></label>
            <div class="controls">
              <?= form_input(array("id" => "state", "name" => "state", "value" => set_value("state", $homeDelivery->state))) ?>
            </div>
        </div>
    <? endif; ?>
        <div class="control-group">
            <label class="control-label" for="zipcode"><?=get_translation_for_view("zipcode", "Zipcode")?></label>
            <div class="controls">
                <?= form_input(array("id" => "zipcode", "name" => "zip", "value" => set_value("zipcode", $homeDelivery->zip))) ?>
            </div>
        </div>
  </div>
  <div class="modal-footer">
    <input type='submit' name="update_address" value=' <?=get_translation_for_view("update_button", "Update Location Details")?> ' class='btn btn-primary'/>
  </div>

</div>

</form>

<script>
    $('#select_delivery_date .dropdown-menu a').click(function(evt) {
        evt.preventDefault();

        var date = $(this).attr('data-date');
        var text = $(this).text();

        $('input[name=deliveryDate]').val(date);
        $('#select_delivery_date .dropdown-toggle').html(text + '<b class="caret pull-right"></b>');
        $('#select_delivery_time .dropdown-menu a').each(function(i, e) {
            var itemDate = $(e).attr('data-date');
            $(e).parent()[itemDate == date ? 'show' : 'hide']();
        });

        $('#select_delivery_time .dropdown-toggle').html("<?= get_translation_for_view('select_delivery_time', 'Select a delivery time'); ?>" + '<b class="caret pull-right"></b>');
        $('#select_delivery_time').show();
    });

    $('#select_delivery_time .dropdown-menu a').click(function(evt) {
        evt.preventDefault();

        if ($(this).parent().hasClass('group-title')) {
            return;
        }

        var date = $(this).attr('data-date');
        var start = $(this).attr('data-start');
        var end = $(this).attr('data-end');
        var id  = $(this).attr('data-id');
        var text = $(this).text();


        $('input[name=deliveryDate]').val(date);
        $('input[name=windowStart]').val(start);
        $('input[name=windowEnd]').val(end);
        $('input[name=delivery_window_id]').val(id);

        $('#select_delivery_time .dropdown-toggle').html(text + '<b class="caret pull-right"></b>');

    });
</script>

<style type="text/css">
.text-right {
    text-align: right;
}

.text-center {
    text-align: center;
}


.group-title a {
    font-weight:  bold;
}

.dropdown-menu .sub-option a {
    padding: 3px 15px 3px 25px;
}

.step {
    margin-bottom: 2em;
}

.step-content {
    padding-left: 20px;
}

.step-width {
    width: 220px;
}

.semi-lead {
    color: #666;
    font-size: 15px;
    font-weight: 200;
    line-height: 24px;
}

.border-block {
    border: 1px solid #ddd;
    padding: 5px 10px;
    margin: 5px 0;
    width: 200px;
}
</style>
