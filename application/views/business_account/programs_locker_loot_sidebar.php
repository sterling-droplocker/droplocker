<div class=''>
    <h2><?= $programName?> <?=get_translation_for_view("balance", "Balance")?></h2>
    <p><h2 style='border:0px'><?= ($balance>0)?format_money_symbol($this->business->businessID, '%.2n', $balance):format_money_symbol($this->business->businessID, '%.2n',  0.00);?></h2></p>
    <?if($balance>0): ?>
    <p><a href='/account/programs/redeem_points' class='btn btn-primary'><?=get_translation_for_view("redeem", "Redeem Credit")?></a>
        <? if($balance >= $minRedeemAmount && !empty($this->rewards_config->allowToRedeem) ): ?>
            <p>
            <a href='/account/programs/redeem_cash' class='btn btn-primary'><?=get_translation_for_view("check", "Send Check for")?> <?= format_money_symbol($this->business->businessID, '%.2n', $redeemAmount)?></a>
            </p>
        <? endif; ?>
    </p>
    <? endif; ?>
    <p style='font-size:12px;'><?=get_translation_for_view("share", "Share Your Referral Code and Get")?> <?= format_money_symbol($this->business->businessID, '%.2n', $totalReward )?>!</p>
    <p>
        <textarea name="affiliateLink" id="referral_code_link" style="width: 98%; height: 50px; border: 1px solid #ccc"><?= $lockerLootEndPoint ?></textarea>
    </p>
    <span style='text-align: center; font-weight: bold;font-size:12px;'><a href='/account/programs/reward_terms/'><?=get_translation_for_view("terms", "Rewards Terms and Conditions")?></a></span>

</div>
<div style='clear:right;'></div>
