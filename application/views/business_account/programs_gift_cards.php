
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Acoount Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("gift_cards", "Gift Cards")?>
  </li>
</ul>

<?= $this->session->flashdata('alert')?>

<h2 class='page-header'><?=get_translation_for_view("gift_cards", "Gift Cards")?></h2>

    <p>
        <?=get_translation_for_view("explanation",
			"A %companyName% Gift Card makes the perfect gift. When you purchase a gift card, we will email you, or your designated recipient, a unique code. This code can then be redeemed online for %companyName% credit and can used at any of our hundreds of locations.",
			array("companyName" => $business->companyName)) ?>
    </p>

<br><br>
<div class=''>
    <div class=''>

        <form id="gift_card_form" class='form-horizontal' action='' method='post'>

            <div class="control-group">
                <div class="controls">
                    <h3><?= $this->business->companyName?> <?=get_translation_for_view("gift_cards", "Gift Cards")?></h3>
                </div>
            </div>

          <div class="control-group">
            <label class="control-label" for="to" ><?=get_translation_for_view("name", "To Name")?></label>
            <div class="controls">
              <input type='text' name='to' value="<?= set_value('to') ?>" />
            </div>
          </div>
          <div class="control-group">
          <label class="control-label" for="email" ><?=get_translation_for_view("email", "To Email")?></label>
            <div class="controls">
              <input type='text' name='email' value="<?= set_value('email') ?>" />
            </div>
          </div>
          <div class="control-group">
          <label class="control-label" for="name" ><?=get_translation_for_view("from", "From")?></label>
            <div class="controls">
              <input type='text' name='from' value='<?= set_value('from', $customer_fullName) ?>' />
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="name" ><?=get_translation_for_view("amount", "Amount")?></label>
            <div class="controls">
               <div class="input-prepend">
               <? $currency = localeconv(); ?>
                  <span class="add-on"><?= $currency['currency_symbol'] ?></span>
                  <input style='width:50px' type='text' name='amount' value="<?= set_value('') ?>" />
                  <?=get_translation_for_view("min", "($5.00 Minimum)")?>
               </div>
            </div>
          </div>

            <div class="control-group">
                <div class="controls">
                    <?= form_submit(array("class" => "btn btn-primary btn-large", "name" => "submit", "value" => get_translation_for_view("purchase_button", "Purchase"))) ?>
                </div>
            </div>
        </form>
    </div>
</div>



