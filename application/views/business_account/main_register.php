<?php
/**
 * Expects the following content variables
 *  selected_business_language_id array
 *  business_language_dropdown array
 *  facebook_url string
 */
if (!is_array($business_language_dropdown)) {
    trigger_error("'business_language_dropdown' must be an array", E_USER_WARNING);
}
if (empty($business)) {
    trigger_error("'business' can not be empty", E_USER_WARNING);
}
if (!isset($facebook_url)) {
    trigger_error("'facebook_url' must be set as a content variable", E_USER_WARNING);
}
//The following code determines if the facebook buttons should open in a new tab or not, if applicable to the business.
$target = "_self";
if ($business->open_facebook_register_in_new_tab) {
    $target = "_blank";
}


?>


<script type='text/javascript'> 
var email = '';
$(document).ready(function() {

    $("#registration_form").validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            password: {
                required: true
            },
            agree: {
                required: true
            }

        },
        messages: {
            email: "<?= htmlspecialchars(get_translation_for_view("valid_email_error_message", "Please enter a valid e-mail")) ?>",
            password: "<?= htmlspecialchars(get_translation_for_view("valid_password_error_message", "Please enter a password"))?>",
            agree: "<?= htmlspecialchars(get_translation_for_view("must_agree_to_terms_and_conditions_error_message", "You must agree to the Terms and Conditions")) ?>"
        },
        errorClass: "invalid",
        submitHandler: function(form) { 
            form.submit();
           $(":submit").attr("disabled", true);
        }
    });

});
</script>

<?php if ($recaptcha_enabled): ?>
    <script src='//www.google.com/recaptcha/api.js'></script>
<?php endif; ?>

<div class="container">
<div class='row register_page_container'>
    <div class='span8 offset1'>
        <h2 class='page-header'> <?= get_translation_for_view("register", "Register") ?> </h2>
        <p> <?=get_translation_for_view("register_subtitle", "Please complete the following information to create your account. You will be able to setup your preferences after you register.")?> </p>
    </div>
</div>
<div class='row register_page_container'>
    <div class='span5 offset1'>
       <?php echo $this->session->flashdata('alert');?>
    	<div class='js_error'></div>

        <form action='/account/main/register_new_user' id="registration_form" method='post' class='form-horizontal'>
            <div class="control-group">
                <label class="control-label" for="email"><?=get_translation_for_view("email", "*Email")?></label>
                <div class="controls">
                    <input autocomplete="off" type='text' id='email' value='<?= (isset($_POST['email'])) ? $_POST['email'] : "" ?>' name='email' />
                </div>
            </div>
            
             <div class="control-group <?php echo (form_error('username'))?"error":"";?>">
                <label class="control-label" for="password"><?=get_translation_for_view("create_password", "*Create Password")?></label>
                <div class="controls">
                    <input autocomplete="off" type='password' id='password' name='password' />
                </div>
            </div>
            
            <div class="control-group <?php echo (form_error('how'))?"error":"";?>">
                <label class="control-label" for="how"> <?=get_translation_for_view("hear_about_us", "How did you hear about us ?")?></label>
                <div class="controls">
                    <input id='how' autocomplete="off" type='text' value='<?= $how ?>' name='how' />
                </div>
            </div>
            <div class="control-group <?php echo (form_error('promo_code'))?"error":"";?>">
                <label class="control-label" for="promo_code"><?=get_translation_for_view("promo_code", "Promotional Code")?></label>
                <div class="controls">
                    <input id='promo_code' autocomplete="off" type='text' value='<?= $promo_code; ?>' name='promo_code' />
                </div>
            </div>
            
            <div class="control-group  <?php echo (form_error('agree'))?"error":"";?>">
                <label class="control-label" for="agree"></label>
                <div class="controls">
                    <label class='checkbox'><input name="agree" type="checkbox" value="1" id="qf_2793d4">
						<?= get_translation_for_view("i_agree", 
								"I agree to the %companyName% <a class='greenlink' target='_blank' href='/account/main/terms'>Terms And Conditions</a>",
								array('companyName' => $this->business->companyName))
						?>
					</label>
                </div>
            </div>
            
            <?php 
                // The following conditional will display a dropdown selector of the defined business languages if there is more than one defined business language for the business.
                if (count($business_language_dropdown) > 1): 

            ?>
                <div class="control-group">
                    <?= form_label(get_translation_for_view("language", "Language"), "default_business_language_id", array("class" => "control-label")) ?>
                    <!-- <label class="control-label" for='business_language_id'></label> -->
                    <div class="controls">
                        <?= form_dropdown("default_business_language_id", $business_language_dropdown, $selected_business_language, "id='default_business_language_id'") ?>
                    </div>
                </div>
            <?php endif ?>
            
            <div class="control-group">
                <label class="control-label" for=""></label>
                <div class="controls">
                    <? if($promo_code!=''): ?>
                        <input type='hidden' autocomplete="off" value='<?= $promo_code?>' name='promo_code' />
                    <? endif; ?>
                    <?php if ($recaptcha_enabled): ?>
                        <div class="g-recaptcha" data-sitekey="<?= $recaptcha_public_key; ?>"></div>
                        <br/>
                    <?php endif; ?>
                    <input type='submit' value='<?=get_translation_for_view("register_button", "Register")?>' data-loading-text="Please Wait..." class='btn btn-primary'/>
                </div>
            </div>
            <input type="hidden" name="lid" value="<?=$this->input->get('lid');?>" />
        </form>
    </div>
    <div class='span3 hidden-tablet hidden-phone' id="already_a_member_box">
        <div class='alert alert-info'>
            <h3><?=get_translation_for_view("already_a_member", "Already a member?")?></h3>
            <a href='/login' class='btn btn-info'><?=get_translation_for_view("login_here", "Login Here")?></a>
            <br><br>
            <p><a href='/forgot_password'><?=get_translation_for_view("forgot_password", "Forgot / Change Password")?></a></p>
        </div>
        <?php if ($business->facebook_api_key && $business->facebook_secret_key): ?>
            <div class="alert alert-info">
                <h3> <?= get_translation_for_view("register_with_facebook_title", "Have a Facebook Account?")?> </h3>
                
                <div style="margin-top: 20px; margin-bottom: 10px;"> 
                    <a name="register_with_facebook" target="<?=$target?>" id="login_with_facebook" style="background: url('/images/icons/facebook.png') no-repeat scroll 8px center #F1F1F1; font-size: 14px; padding: 8px 8px 8px 22px; text-decoration: none;" href="<?= $facebook_url ?>">
                            &nbsp;&nbsp; <?= get_translation_for_view("register_with_facebook_button_text", "Register") ?> 
                    </a> 
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class='span4 offset2 visble-tablet visible-phone hidden-desktop'>
        <div class='alert alert-info'>
            <h3><?= get_translation_for_view("already_a_member", "Already a member?") ?></h3>
            <a href='/login' class='btn btn-info'><?= get_translation_for_view("login_here", "Login Here") ?></a>
            <br><br>
            <p><a href='/forgot_password'> <?= get_translation_for_view("forgot_password", "Forgot Password") ?> </a></p>
        </div>
        <?php if ($business->facebook_api_key && $business->facebook_secret_key): ?>
            <div class="alert alert-info">
                <h3> <?= get_translation_for_view("register_with_facebook_title", "Have a Facebook Account?")?> </h3>
                
                <div style="margin-top: 20px; margin-bottom: 10px;"> 
                    <a name="register_with_facebook" target="<?=$target?>" id="login_with_facebook" style="background: url('/images/icons/facebook.png') no-repeat scroll 8px center #F1F1F1; font-size: 14px; padding: 8px 8px 8px 22px; text-decoration: none;" href="<?= $facebook_url ?>">
                        &nbsp;&nbsp; <?= get_translation_for_view("register_with_facebook_button_text", "Register") ?> 
                    </a> 
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
</div>

