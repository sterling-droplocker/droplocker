
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("store_access", "Store Access")?>
  </li>
</ul>

<?= $this->session->flashdata('alert')?>

<h2 class='page-header'><?=get_translation_for_view("store_access", "Store Access")?></h2>
<? if(!$kiosks): ?>
	<div class='alert alert-danger'><?=get_translation_for_view("error", "No Store for this Business")?></div>
<? endif; ?>

	<p>
	<?= $this->business->companyName?> <?=get_translation_for_view("operates_locations", "operates several self-service stores providing the ultimate in convience. In order to access our secured locations, you will use a credit card that you set up below")?>
	</p>

	<h3><?=get_translation_for_view("how_it_works_title", "Here is how it works")?></h3>
        <p>
            <ol>
                <?=get_translation_for_view("how_it_works_text", '
                     <li>Using the form below, enter the last 4 digits and expiration date for a credit card that you would like to use for store access.</li>
                     <li>Within 2 minutes your card will be added to our acccess system and you will be able to enter the store by swiping the card you registered below.</li>
                     <li>Follow our standard <a href="%howitworks%">Locker Drop Off Process</a>.</li>',
					array("howitworks" => $this->business->howitworks))?>
            </ol>
	</p>
    <? if($access): ?>

    <h3><?=get_translation_for_view("card", "Card on File")?></h3>
    <table class="table table-striped table-bordered table-condensed">
        <tr>
            <td><?=get_translation_for_view("last", "Last 4:")?> <?= $access[0]->cardNumber?></td>
            <td><?=get_translation_for_view("expire", "Expires:")?> <?= $access[0]->expMo?>/<?= $access[0]->expYr?></td>
        </tr>
    </table>

<? endif;?>
<p>
    <h3><?=get_translation_for_view("update", "Update Card for Store Access")?></h3>
    <?= form_open("/account/profile/update_kiosk_card", array("class" => "well", "id" => "update_kiosk_card_form")) ?>
        <label><?=get_translation_for_view("last_four", "Last 4")?></label>
        <input type='text' name='cardNumber'/>

        <label><?=get_translation_for_view("expiration", "Expiration")?></label>
        <select name='expMo'>
                <? for($i=1; $i<=12; $i++):?>
                <option value='<?= $i?>'><?= $i?></option>
                <? endfor;?>
        </select>
        <label><?=get_translation_for_view("expiration_year", "Expiration Year")?></label>
        <select name='expYr'>
                <? for($i=0; $i<=15; $i++): $year =  date('Y') + $i; $value = date('y') + $i; ?>
                <option value='<?= $value ?>'><?= $year ?></option>
                <? endfor;?>
        </select>
        <br>
        <input type='submit' value='<?=get_translation_for_view("submit_button", "Submit Changes")?>' class='btn btn-primary'/>
    </form>
</p>

