<?php

/**
 * This view expects the following content parameters to be set
 *
 */
if (!is_array($business_languages)) {
    trigger_error("'business_languages' must be an array", E_USER_ERROR);
}

$birthday_timestamp = time();
if (!empty($customer->birthday)) {
    $birthday_timestamp = $customer->birthday->getTimestamp();
}

$providers = array(
    "" => get_translation_for_view("cellphone_carrier", "-- Select Carrier --"),
    "Alltel" => "Alltel",
    "ATT" => "ATT",
    "MetroPCS" => "Metro PCS",
    "Nextel" => "Nextel",
    "Sprint" => "Sprint",
    "T-Mobile" => "T-Mobile",
    "Verizon" => "Verizon",
    "BoostMobile" => "Boost Mobile",
);

$all_states = get_all_states_by_country_as_array();

$ada_deliver_to_lower_locker = get_customer_meta($customer->customerID, "ada_deliver_to_lower_locker", 0);

?>

<?php

$fieldList = array();
$fieldList['firstName'] = 
    "<div class='control-group'>
        <label class='control-label' for='firstName'>" . get_translation_for_view('first_name', '*First Name') . "</label>
        <div class='controls'>
            " . form_input(array('id' => 'firstName', 'name' => 'firstName', 'value' => set_value('firstName', $customer->firstName))) . "
        </div>
    </div>";
            
$fieldList['lastName'] = 
    "<div class='control-group'>
        <label class='control-label' for='lastName'>" . get_translation_for_view('last_name', '*Last Name') . "</label>
        <div class='controls'>
            " . form_input(array('id' => 'lastName', 'name' => 'lastName', 'value' =>  set_value('lastName', $customer->lastName))) . "
        </div>
    </div>";
            
$nameForm = getSortedNameForm($fieldList);

//------------------

$fieldList = array();
$fieldList['address1'] = 
    "<div class='control-group'>
        <label class='control-label' for='address'>" . get_translation_for_view('address', '*Address') . "</label>
        <div class='controls'>
            " . form_input(array('id' => 'address', 'name' => 'address', 'value' => set_value('address', $customer->address1))) . "
        </div>
    </div>";
    
$fieldList['address2'] = 
    "<div class='control-group'>
        <label class='control-label' for='address2'>" . get_translation_for_view('address_subtitle', 'Address 2') . "</label>
        <div class='controls'>
            " . form_input(array('id' => 'address2', 'name' => 'address2', 'value' => set_value('address2', $customer->address2))) . "
        </div>
    </div>";
    
$fieldList['city'] = 
    "<div class='control-group'>
        <label class='control-label' for='city'>" . get_translation_for_view('city', 'City') . "</label>
        <div class='controls'>
            " . form_input(array('id' => 'city', 'name' => 'city', 'value' => set_value('city', $customer->city))) . "
        </div>
    </div>";
    
if ($business_country == 'france') {
    // Groombox does not show location
} else {
$fieldList['state'] = 
    (isset($all_states[$business_country])) ? 
        "<div class='control-group'>
            <label class='control-label' for='state'>" . get_translation_for_view('state', 'State') . "</label>
            <div class='controls'>
                " . form_dropdown('state', $all_states[$business_country], set_value('state', $customer->state), 'id="state"' ) . "
            </div>
        </div>"
        :
        "<div class='control-group'>
            <label class='control-label' for='state'>" . get_translation_for_view('locality', 'Locality') . "</label>
            <div class='controls'>
              " . form_input(array('id' => 'state', 'name' => 'state', 'value' => set_value('state', $customer->state))) . "
            </div>
        </div>";
        
$fieldList['zip'] = 
    "<div class='control-group'>
            <label class='control-label' for='zipcode'>" . get_translation_for_view('zipcode', 'Zipcode') . "</label>
            <div class='controls'>
                " . form_input(array('id' => 'zipcode', 'name' => 'zipcode', 'value' => set_value('zipcode', $customer->zip))) . "
            </div>
        </div>";
}
            
$addressForm = getSortedAddressForm($fieldList);

?>

<?php if (isset($_GET['new'])): ?>
<!-- Google Code for Account Creation Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970116318;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "0oC9CI7F4VoQ3pnLzgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/970116318/?label=0oC9CI7F4VoQ3pnLzgM&guid=ON&script=0"/>
</div>
</noscript>

<?php endif; ?>
<script type="text/javascript">
var emailWindowStart = "<?= $emailWindowStart?>";
var emailWindowStop = "<?= $emailWindowStop?>";

if( parseInt(emailWindowStart) > parseInt(emailWindowStop) ){
    var swap = emailWindowStop;
    emailWindowStop = emailWindowStart;
    emailWindowStart = swap;
}
</script>

<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("profile", "Profile")?>
  </li>
</ul>

<h2 class='page-header'><?=get_translation_for_view("settings", "Settings")?></h2>
<?= $this->session->flashdata('alert'); ?>

<div>
<h3><?=get_translation_for_view("edit_info", "Edit Account Information")?></h3>
<div>
    <dl class='dl-horizontal'>
        <dt><?=get_translation_for_view("email", "Email")?></dt>
        <dd><?= $customer->email?> (<a href='/account/profile/email'><?=get_translation_for_view("change_email", "Change Email")?></a>)</dd>
        <dt><?=get_translation_for_view("password", "Password")?></dt>
        <dd><a href='/account/profile/password' ><?=get_translation_for_view("update_password", "Update Password")?></a></dd>
    </dl>
</div>
<form class='form-horizontal' id="account_settings" method="post" action='/account/profile/update'>

        
    <?php echo $nameForm; ?>
        
    <div class="control-group">
        <label class="control-label" for="phone"><?=get_translation_for_view("phone", "*Phone Number")?></label>
        <div class="controls">
            <?= form_input(array("id" => "phone", "name" => "phone", "value" => set_value("phone", $customer->phone))) ?>
        </div>
    </div>
        
    <?php echo $addressForm; ?>
        
    <div class="control-group">
        <label class="control-label" for="birthday"><?=get_translation_for_view("birthday", "Birthday") ?></label>
        <div class="controls">
            <?= $this->load->view('business_account/profile_birthday_component', array('birthday' => date('Y-m-d', $birthday_timestamp))); ?>
        </div>
    </div>

    <br>
    <h3><?= get_translation_for_view("text_alert", "Text Message Alerts") ?></span> <span style='font-size:10px;'><?=get_translation_for_view("optional", "Optional")?></span></h3>

    <div class="control-group">
        <label class="control-label" for="sms"><?=get_translation_for_view("cellphone", "Mobile Phone Number") ?></label>
        <div class="controls">
            <?= form_input("sms", set_value("sms", $sms), 'id="sms"') ?>
            <? if ($show_carriers): ?>
            <?= form_dropdown("cellProvider", $providers, set_value("cellProvider", $cellProvider), 'id="cellProvider" class="span2"') ?>
            <? endif; ?>
        </div>
    </div>
    <? if ($sms2 || get_business_meta($this->business_id, 'show_second_sms')): ?>
    <div class="control-group">
        <label class="control-label" for="sms2"><?=get_translation_for_view("cellphone2", "2nd Mobile Phone Number") ?></label>
        <div class="controls">
            <?= form_input("sms2", set_value("sms2", $sms2), 'id="sms2"') ?>
            <? if ($show_carriers): ?>
            <?= form_dropdown("cellProvider2", $providers, set_value("cellProvider2", $cellProvider2), 'id="cellProvider2" class="span2"') ?>
            <? endif; ?>
        </div>
    </div>
    <? endif; ?>
    <? if (!empty($location_dropdown)): ?>
    <h3><?=get_translation_for_view("your_locations", "Your Locations")?></h3>
    <div class="control-group">
       <label class="control-label">
           <?=get_translation_for_view("location", "Location")?>
       </label>
       <div class="controls">
           <? $location_dropdown = array(get_translation_for_view("choose_location", "- Choose Location -")) + $location_dropdown;  ?>
           <?= form_dropdown('location_id', $location_dropdown, $current_location, 'id="location_id"');  ?>
       </div>
    </div>
    <? endif; ?>

    <h3> <?=get_translation_for_view("delivery_window", "Email and Text Message Delivery Window")?> <span style='font-size:10px;'> <?=get_translation_for_view("optional_window", "(optional)")?> </span> </h3>
    <div class="control-group">
        <div class='' id='emailWindow' style="width: 90%"></div>
        <div class='span5' id='hourRange' style='padding:10px 0px'><?=get_translation_for_view("send", "Send between:")?> <span></span></div>
        <input type='hidden' name='hourSpan' id='hourSpan'>
     </div>

<? if (count($business_languages) > 1): ?>
    <h3> <?=get_translation_for_view("lang", "Language")?> </h3>
     <?= form_dropdown("default_business_language_id", $business_languages, $customer->default_business_language_id)?>
    <br />
<? endif ?>


     <div class="control-group ada-preference">
    <h3> 
    <?= get_translation_for_view("ada_deliver_to_lower_locker", "Always deliver to lower locker")?>
    </h3>  
     <?= form_checkbox("ada_deliver_to_lower_locker", 1, $ada_deliver_to_lower_locker, 'id="ada_deliver_to_lower_locker" style="margin:.3em;padding:.5em"') ?>
     <label for="ada_deliver_to_lower_locker" class="ada_deliver_to_lower_locker"></label>  
     <br />
     </div>

    <input style="margin-top: 20px;" type='submit' name="submit" value=' <?=get_translation_for_view("update_button", "Update Account Details")?> ' class='btn btn-primary'/>
</form>
</div>

<script type='text/javascript'>

$(document).ready(function() {


<?php if ($require_address2): ?>
$("#account_settings").validate({
    rules: {
        phone: {
            required: true
        },
        firstName: {
            required: true
        },
        lastName: {
            required: true
        },
        address: {
            required: true
        },
        address2: {
            required: true
        }
    },
    messages: {
        firstName: "<?= htmlspecialchars(get_translation_for_view("valid_first_name_error_message", "Please enter your first name")) ?>",
        lastName: "<?= htmlspecialchars(get_translation_for_view("valid_last_name_error_message", "Please enter your last name")) ?>",
        phone: "<?= htmlspecialchars(get_translation_for_view("valid_phone_name_error_message", "Please enter your phone number")) ?>",
        address: "<?= htmlspecialchars(get_translation_for_view("valid_address_error_message", "Please enter your address")) ?>",
        address2: "<?= htmlspecialchars(get_translation_for_view("valid_address2_error_message", "Please enter your address unit number")) ?>",
    },
    errorClass: "invalid"
});
<?php else: ?>
$("#account_settings").validate({
    rules: {
        phone: {
            required: true
        },
        firstName: {
            required: true
        },
        lastName: {
            required: true
        },
        address: {
            required: true
        }

    },
    messages: {
        firstName: "<?= htmlspecialchars(get_translation_for_view("valid_first_name_error_message", "Please enter your first name")) ?>",
        lastName: "<?= htmlspecialchars(get_translation_for_view("valid_last_name_error_message", "Please enter your last name")) ?>",
        phone: "<?= htmlspecialchars(get_translation_for_view("valid_phone_name_error_message", "Please enter your phone number")) ?>",
        address: "<?= htmlspecialchars(get_translation_for_view("valid_address_error_message", "Please enter your address")) ?>"
    },
    errorClass: "invalid"
});
<?php endif; ?>


$("#emailWindow").slider({
    range: true,
    min: 0,
    max: 24,
    step: 1,
    values: [ emailWindowStart, emailWindowStop],
    slide: function(e, ui) {
        if (ui.values[0] == ui.values[1])  {
            return false; //dont allow to set same hour
        }

        var hourStart = ui.values[0];
        var hourStop = ui.values[1];
        $("#hourRange span").html(getProperHour(hourStart) + " - " + getProperHour(hourStop) );
        $("#hourSpan").val(hourStart+"-"+hourStop);
    }
});

$("#hourRange span").html(getProperHour(emailWindowStart) + " - " + getProperHour(emailWindowStop) );


function getProperHour(hour) {
    if (hour == 0) {
        hour = 12;
        time = "am";
    } else if (hour < 12) {
        time = "am";
    } else if (hour == 12) {
       hour = 12;
       time = "pm";
    } else if (hour > 12 && hour < 24) {
        hour = hour - 12;
        time = "pm";
    } else {
        hour = 12;
        time = "am";
    }

    hour += ':00 ';
    return hour + time;
}

});

</script>
