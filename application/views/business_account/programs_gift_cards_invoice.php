
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("gift_cards", "Gift Cards")?>
  </li>
</ul>

<div style="padding-left:10px;">
    <h2><?=get_translation_for_view("confirm", "Confirm Gift Card Purchase")?></h2>
    <br>

    <form action='/account/programs/gift_card_complete' method='post'>

        <table class='table table-striped table-bordered table-condensed'>
                <thead>
                        <tr>
                                <th style=' text-align: left'><?=get_translation_for_view("name", "Name")?></th>
                                <th style=' text-align: left'><?=get_translation_for_view("email", "Email")?></th>
                                <th style='width:60px; text-align: left'><?=get_translation_for_view("amount", "Amount")?></th>
                        </tr>
                </thead>
                <tbody>
                        <tr>
                                <td><?= $post['to']?>
                                <input type="hidden" id='to' name="to" value="<?= $post['to'] ?>">
                                </td>
                                <td><?= $post['email'] ?>
                                <input type='hidden' id='email' name='email' value='<?= $post['email'] ?>' />
                                </td>
                                <td>
                                    <input type="hidden" id="total" name="total" value="<?= $post['amount'] ?>" />
                                    <?= format_money_symbol($this->business->businessID, '%.2n', $post['amount']) ?>
                                </td>
                        </tr>
                </tbody>
        </table>

        <br>
        <div id='result'></div>

        <div align="center"><a class='btn btn-primary btn-large' id='buy_btn'><?=get_translation_for_view("purchase_now", "Purchase Now")?></a></div>

        <input type="hidden" id="from" name="from" value="<?= $post['from'] ?>" />

    </form>
    <br />

    <h2><?=get_translation_for_view("on_file", "Credit Card on File")?></h2>
    <dl class="dl-horizontal">
        <dt>
            <? if ($card->cardType == 'PayPal'): ?>
                <?= get_translation_for_view("paypal", "Paypal Account:") ?>
            <? elseif ($card->cardType == 'Coinbase'): ?>
                <?= get_translation_for_view("coinbase", "Coinbase Account:") ?>
            <? else: ?>
                <?= get_translation_for_view("coinbase", "Last 4:") ?>
            <? endif; ?>
        </dt>
        <dd><?= $card->cardNumber?></dd>

        <dt><?=get_translation_for_view("expires", "Expires:")?></dt>
        <dd><?= $card->expMo?>/<?= $card->expYr ?></dd>
    </dl>

</div>


<script>
$(document).ready(function(){

$('#buy_btn').unbind('click').click(function() {

	$('#buy_btn').off('click');
	$('#buy_btn').attr('data-old-text', $('#buy_btn').html());

	$('#buy_btn').html("<span style=' text-decoration:none'><?= get_translation_for_view("working", "Working... Please Wait") ?></span>");


    $.ajax({
        url:"/account/programs/gift_card_complete_ajax/",
        type:"POST",
        data:{
            submit: true,
            total: $('#total').val(),
            email: $('#email').val(),
            to: $('#to').val(),
            from: $('#from').val()
        },
        dataType:"json",
        success:function(data){
            if(data.status == 'SUCCESS'){

                top.location.href = "/account/programs/gift_card_complete/"+data.insert_id;
            } else{
                $('#result').html("<div class='alert alert-error'>" + data.message + "</div>");
                $('#buy_btn').on('click');


            }
        }
    });
});

});
</script>
