<?php

$frequency_labels = array(
	'every order' => get_translation_for_view('every order', 'every order'),
	'one time' => get_translation_for_view('one time', 'one time'),
	'until used up' => get_translation_for_view('until used up', 'until used up'),
);

?>
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li class="active">
   <?=get_translation_for_view("discounts", "Discounts")?>
  </li>
</ul>

<h2 class='page-header'><?=get_translation_for_view("discounts", "Discounts")?></h2>
<?= $this->session->flashdata('alert')?>

<h3><?=get_translation_for_view("pending_discounts", "Pending Discounts")?></h3>
<? if ($discounts): ?>

<table class="table table-striped table-bordered table-condensed make-responsive" id="pending-discounts-table">
    <thead>
		<tr>
			<th><?=get_translation_for_view("amount", "Amount")?></th>
			<th><?=get_translation_for_view("frequency", "Frequency")?></th>
			<th><?=get_translation_for_view("description", "Description")?></th>
			<th><?=get_translation_for_view("expires", "Expires")?></th>
		</tr>
    </thead>
    <tbody>
		<? foreach($discounts as $d):?>
		<tr>
			<td><? if($d->amountType == 'percent') { echo number_format_locale($d->amount,0).'%'; }
                        else if($d->amountType == 'dollars') { echo format_money_symbol($this->business_id, '%.2n', $d->amount); }
                        else if($d->amountType == 'pounds') { echo number_format_locale($d->amount,2).' ' . weight_system_format('',false,true); }?></td>
            <td><?= isset($frequency_labels[$d->frequency]) ? $frequency_labels[$d->frequency] : $d->frequency ?></td>
			<td><?= $d->description?></td>
			<td><?= ($d->expireDate!=null) ? convert_from_gmt_locale($d->expireDate, $date_format) : "N/A";?></td>
		</tr>
		<? endforeach; ?>
    </tbody>
</table>
<? else: ?>
    <p><?=get_translation_for_view("no_discounts", "No Discounts")?></p>
<? endif; ?>


<h3><?=get_translation_for_view("applied", "Applied Discount")?></h3>
<? if ($applied_discounts): ?>
<table class="table table-striped table-bordered table-condensed make-responsive" id="applied-discounts-table">
    <thead>
		<tr>
			<th><?=get_translation_for_view("date", "Date")?></th>
			<th><?=get_translation_for_view("amount", "Amount")?></th>
			<th><?=get_translation_for_view("description", "Description")?></th>
			<th><?=get_translation_for_view("order", "Order")?></th>
		</tr>
    </thead>
    <tbody>
		<? foreach($applied_discounts as $discount):?>
		<tr>
			<td align='left'><?= convert_from_gmt_aprax($discount->dateCreated, SHORT_DATE_FORMAT) ?></td>
				<td><?=format_money_symbol($this->business_id, '%.2n', $discount->chargeAmount)?></td>
				<td><?= $discount->chargeType?></td>
				<td align='center'><a href='/account/orders/order_details/<?= $discount->orderID?>'><?= $discount->orderID?></a></td>
		</tr>
		<? endforeach;?>
    </tbody>
</table>
<? else: ?>
    <p><?=get_translation_for_view("no_discounts", "No Discounts")?></p>
<? endif; ?>

<div class="visible-phone">

<? include "programs_discount_sidebar.php"; ?>

</div>
