<? 
$str = sprintf('<div class="well"><h3>%s</h3>', get_translation("title", "promoCodeForm", array(), "Redeem a Gift Card"));
    $str .= '<form action="/account/programs/discounts" method="post">';
    $str .= sprintf('<label>%s</label>
            <input name="code" type="text" class="input-block-level">
           <button type="submit" name="submit" class="btn btn-primary"> %s </button>
          </form></div>', get_translation("description", "promoCodeForm", array(), "Promotional Code"), get_translation("submit_button_text", "promoCodeForm", array(), "Submit") );
   echo $str;
   
?>