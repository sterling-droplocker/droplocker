<?php

$date_format = get_business_meta($this->business_id, "shortDateDisplayFormat");


?>
<ul class="breadcrumb">
  <li>
    <a href="/account"><?=get_translation_for_view("account_home", "Account Home")?></a> <span class="divider">/</span>
  </li>
  <li>
   <a href="/account/profile/closet/"><?=get_translation_for_view("my_closet", "My Closet")?></a> <span class="divider">/</span>
  </li>
  <li class='active'><?=get_translation_for_view("item_details", "Item Details")?> [<?php echo $this->uri->segment(4);?>]</li>
</ul>

<!-- <div style="margin-bottom: 20px; margin-top: 30px;"> <a class='btn btn-primary' href='/account/profile/closet'>My Whole Closet 2.0</a> </div>-->

<?php if (empty($item)): ?>
<div class='alert alert-error'> <?=get_translation_for_view("not_found", "Item Not Found")?>. </div>
<?php else: ?>

<table class="table table-bordered table-condensed">
    <tr>
        <th><?=get_translation_for_view("barcode", "Barcode")?></th>
        <td><?= $item->barcode?></td>
    </tr>
    <tr>
        <th><?=get_translation_for_view("product", "Product")?></th>
        <td><?= $item->displayName?></td>
    </tr>
    <tr>
        <th><?=get_translation_for_view("method", "Cleaning Method")?></th>
        <td><?= $item->processName ? 
            get_translation($item->processName, "Processes", array(), $item->processName) : 
            get_translation("not_set", "Processes", array(), "Not set") ?></td>
    </tr>
</table>


<div class='row'>
    <div class='span2'>
        <?php if (isset($item->file)):?>
            <a class='thumbnail' href='https://s3.amazonaws.com/LaundryLocker/<?= $item->file?>'>
                    <img style="width: 170px;" src='https://s3.amazonaws.com/LaundryLocker/<?= $item->file?>' />
            </a>
        <?php else: ?>
            <img style="width: 170px;" src='<?= $default_image ?>' />
        <?php endif; ?>
        <br>
        <p><a class='btn btn-primary' href='/account/profile/add_note/<?= $item->itemID?>'><?=get_translation_for_view("add_notes", "Add Notes")?></a></p>
    </div>
    <div class='span5'>
        <h3><?=get_translation_for_view("history", "Order History")?></h3>
        <table class="table table-striped table-bordered table-condensed">
            <tr>
                <th><?=get_translation_for_view("order", "Order")?></th>
                <th><?=get_translation_for_view("date", "Date")?></th>
                <th><?=get_translation_for_view("customer", "Customer")?></th>
                <th><?=get_translation_for_view("bags", "Bags")?></th>
                <th><?=get_translation_for_view("notes", "Notes")?></th>
            </tr>
            <? if($history): ?>
            <? foreach($history as $h): ?>
            <tr>
                <td> <a href="/account/orders/order_details/<?= $h->order_id?>"> <?= $h->order_id ?> </a> <a href="/account/profile/closet/<?= $h->order_id?>"><?=get_translation_for_view("pics", "(pics)")?></a></td>
                <?php 
                    $update_date = new DateTime($h->updated);
                    
                ?>
                <td><?= $update_date->format($date_format) ?></td>
                <td style="text-align: left;" ><?= getPersonName($h) ?></td>
                <td><?= $h->bagNumber?></td>
                <td><?= $h->notes?></td>
            </tr>
            <? endforeach; ?>
            <? else: ?>
            <tr>
                    <td colspan="7"><?=get_translation_for_view("no_history", "No History for this Item")?></td>
            </tr>
            <? endif; ?>
        </table>
    </div>
</div>



<?php endif; ?>
