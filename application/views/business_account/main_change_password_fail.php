<div class='row'>
    <div class='span6 offset3'>
        <div class='alert alert-error'>
            <h2><?=get_translation_for_view("code_fail", "Verification Code Not Found")?></h2>
            <p><?=get_translation_for_view("contact_support", "Please contact support if you need help.")?></p>
            <p><a class='btn btn-danger' href='/login'><?=get_translation_for_view("back_to_login", "Back to Login")?></a></p>
        </div>
    </div>
</div>
 
