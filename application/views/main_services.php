<div class="fluid-container">
<div class="row-fluid">
    <div class="span8">
        <div class="bodyBig">
            <h1>Our Services</h1><br>

            <div class="visible-phone" style="text-align: center">
            <?= servicesNav();?>
            </div>
            Laundry Locker&reg; provides San Francisco with the most convenient laundry services possible!  Available in apartment buildings, condos, offices, fitness facilities and other locations throughout the city, Laundry Locker strives to make doing your laundry so easy, it almost does itself.
            <br><br>
            <a href="/main/dryclean"><h2>Eco-Friendly Dry Cleaning</h2></a><br>
            Place your dirty clothes in a secure Laundry Locker, lock the locker with any four digit code, and we'll do your dry cleaning for you. Items are packaged just like any other dry cleaning, but you pick them up at your convenience, not theirs.  <a href="/main/dryclean" class="greenLink">Learn more...</a>
            <br><br><br>
            <a href="/main/washfold"><h2>Wash & Fold</h2></a><br>
            Give us your dirty laundry, we'll wash it, fold it, and package it up for you. What could be easier?  All clothes are separated into white and colored loads and <strong>washed separately from everyone else's</strong>. We operate brand new, high efficiency machines and our expert attendants ensure that your clothes come back neatly folded, nice and clean and smelling fresh. You can even go on to our website and customize exactly how you want your laundry done.    <a href="/main/washfold" class="greenLink">Learn more...</a>
            <br><br><br>

            <a href="/main/washfold"><h2>Home Delivery</h2></a><br>
            You are in luck! Not only do we have 100's of locker locations throughout the Bay Area, but we also offer home pickup and delivery directly to your doorstep!
            Schedule a window of time that is convenient for you today. <a href="/main/homedelivery" class="greenLink">Learn more...</a>
            <br><br><br>

            <a href="/main/shoeshine"><h2>Bag &amp; Shoe Repairs &amp; Shines</h2></a><br>
            Has it been a while since your last shoe shine? Are the heels on your favorite shoes or straps on your favorite purse starting to wear out? Give us your shoes, bags, and suitcases and we'll make them look like new again!  <a href="/main/shoeshine" class="greenLink">Learn more...</a>
            <br><br><br>
            <a href="/main/packagedelivery"><h2>Package Delivery</h2></a><br>
            We've all received that frustrating notice from FedEx or UPS because they couldn't deliver a package.  Or worse yet, sat at home all day waiting for your package to arrive.  Well no more.  Now you can have your package delivered to a convenient Laundry Locker location and <strong>pick it up at your convenience.</strong> <a href="/main/packagedelivery" class="greenLink">Learn more...</a>
            <br><br><br>
        </div>
    </div>
    <div class="span4">

            <?php

            echo '<div class="hidden-phone">'.servicesNav().'</div>';
            ?>
   
    </div>
</div>
</div>