<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <br>
            <h1>Get Lockers In Your Building</h1>
            <br>
            <span class="bodyBig">
                Laundry Locker is the most convenient and easy way to offer your tenants, customers or employees 24 x 7 on-site wash and fold, dry cleaning and shoe repair services. Our simple lockers can be placed discreetly inside your building, offering a needed service proven to help you attract and retain tenants, employees and customers.<br><br>
            </span>
            <span class="bodyBold">Completely free service for property owners</span><br>
            <span class="bodyReg">Laundry Locker will place lockers to your specification in your property. No setup fees, maintenance fees, service fees, or any other kind of fees will be charged to properties.</span><br><br>
            <span class="bodyBold">Helps attract and retain quality tenants, employees and customers</span><br>
            <span class="bodyReg">Placing a locker in your building keeps your existing clients happy and helps attract new clients to your property.</span><br><br>
            <span class="bodyBold">Free advertising on Laundry Locker website</span><br>
            <span class="bodyReg">All locations have the opportunity to be listed on the Laundry Locker website.  Current Laundry Locker customers look for locations that have Laundry Locker services.</span><br><br>
            <span class="bodyBold">Requires zero maintenance</span><br>
            <span class="bodyReg">Laundry Locker lockers require no electricity, phone lines or any other services.  Maintained on a daily basis, they are always clean and a welcome addition to your property.</span><br><br>
            <span class="bodyBold">Choose your services</span><br>
            <span class="bodyReg">Different lockers for laundry and dry cleaning allows property owners to choose the services they want to offer.</span><br><br>
        </div>
        <div class="span4">
            <div style="margin-right:3px; padding-top:50px; text-align: center" class="greenLink"><img src="/images/lockerDrawing.jpg" alt="illustration of lockers"/><br><a href="/main/lockerpics" class="greenLink">view pictures of actual lockers</a></div>

            <div class="dryCleanNote calloutRight">
                <p class="graySmall">
                    Please contact our sales department<br> to discuss adding a Laundry Locker<br> to your location today<br><br>
                    Phone: <?= $this->business->phone ?><br>
                    <a href="mailto:sales@laundrylocker.com" class="whiteLink">sales@laundrylocker.com</a>
                    </p>
            </div>
        </div>
    </div>
</div>
</div>