<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig" style="margin-bottom: 10px">
                <h1>Specials</h1><br>
                <div class="visible-phone" style="text-align: center">
                <?= servicesNav('specials'); ?>
                </div>

                Laundry Locker&reg; regularly offers specials for new and existing customers.  Check back frequently to see our current promotions.
                </span>
                <br><br>
                <h2>$.99 / lb Wash & Fold!</h2>
                Sign up for one of our laundry plans with rates as low as $.99 per pound!  <a href="/main/plans">Click here to learn more.</a></span>
                <br><br><br>
                <h2>Refer a Friend ... Get $50.00!</h2>
                With our Locker Loot&#0153 program, customers automatically receive credit for every order they place.  <strong>And for every customer you refer, we'll give you $50!</strong>  We even pay cash!!!  <a href="/main/lockerloot">Learn more</a>.</span>
                <br><br><br>
                <h2>Thursday is Laundry Day</h2>
                On Thursdays, Laundry Locker customers receive free upgraded detergent.  If you select a detergent upgrade under my preferences, the upgrade is free on Thursdays.</span>
            </div>
        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= servicesNav('specials'); ?>
            </div>
        </div>
    </div>
</div>