<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DropLocker FAQs</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'faq';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Frequently Asked Questions</h1>
					<h3>Can I use the Laundry Locker name</h3>
					<p>The Drop Locker platform is designed to run YOUR business.  This means that each locker is branded with your company name and logo, helping you build your brand.</p>
					<p>Because the Laundry Locker name and logo is a registered trademarks, you are not permitted to use it on your lockers or in your marketing materials.  However, the "Powered By Drop Locker" logo will be included on your website and adhered to your lockers.</p>
					<br />
					<h3>Can I have exclusivity in my city?</h3>
					<p>The Drop Locker solution is primarily a software platform combined with some hardware (lockers), similar to Open Table.  It's up to you to choose what products you want to offer, what prices you want to charge and how you want to brand your business.  Because of that, at this time we are not offering any exclusivity arrangements.</p>
					<br />
					<h3>What is your SLA?</h3>
					<p>Drop Locker provides a 99% uptime guarantee.  This means that our software will be running and accessible via the internet at least 99% of the time.  In the event that our network is not available for more than 99% of the time, Drop Locker will credit the following month's service fee as follows:</p>
					<p>For any client in good financial standing at time of the outage, such credit shall be retroactive and shall be measured 24 hours a day in a calendar month, with the maximum credit not to exceed the monthly service charge for the affected month. 
						<ul>
							<li>Less than 90% = 100% refund</li>
							<li>90.1% to 99% = 50% refund</li>
						</ul></p>
					<p>Credits shall not be provided to you in the event that you have any outage resulting from; (i) scheduled maintenance as posted from time to time by Drop Locker; (ii) your behavior or the performance or failure of your equipment, facilities or applications; (iii) circumstances beyond Drop Lockers' reasonable control, including, without limitation, acts of any governmental body, war, insurrection, sabotage, embargo, fire, flood, strike or other labor disturbance, interruption of or delay in transportation, unavailability of interruption or delay in telecommunications or third party services, including DNS propagation, domain name registration/transfer, failure of third party software or hardware or inability to obtain raw materials, supplies, or power used in or equipment needed for provision of your web site.</p>
					<br />
					<h3>Are there any royalties?</h3>
					<p>Since you are not using the Drop Locker or Laundry Locker names, this is not a franchise opportunity and you are simply licensing our software.  Your only monthly fees are your software subscription fess and there are no royalties.</p>
					<br />
					<!-- <h3>What kind of support will I receive?</h3>
					<p>The entire Drop Locker team is committed to your success, as the better you do, the better we do.  On our <a href="/main/pricing">pricing page</a>, we explain our tiered support, which is based on the number of lockers you operate.</p>
					<p>However, all plans offer FREE emails support and our powerful knowlegebase is always avaialble 24/7.</p>
					<br /> -->
					<h3>How long will it take me to be up and running</h3>
					<p>The Drop Locker platform is completely cloud based, meaning all you need is a web browser to run our software.  For ease of use you will want a barcode scanner, receipt printer and some other hardware for managing your orders, but the basic system can be up and running in minutes.  Once you purchase your lockers, we will ship them to you and start to setup the software for you business.  The software setup takes 5 business days and your lockers will take several days to arrive, depending on where you are located.</p>
				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Join Our Mailing List</h3>
						<p><form action="/main/newsletter" class="email-form">
						<fieldset>
							<div class="row">
								<div class="text-field-white">
									<input type="text" id='semail_input' placeholder='Email address' value=""/>
									<input type="text" id='semail' name='email' />
									<input type="text" id='sname' name='name' />
								</div>
							</div>
							<a href="/main/newsletter" id='e_link_submit' class="more">Subscribe</a>
						</fieldset>
					</form></p>
					</div>
				</div>
			</div>
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>