<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drop Locker For Business Owners</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Powered By Drop Locker</h1>
					<p><img src="/images/headshot_arik.jpg" alt="" style="margin-top: 6px; margin-right: 15px; margin-bottom: 10px;" align="left" />In 2005, I started Laundry Locker, Inc in response to my frustration with the local dry cleaners.  With their inconvenient hours, poor service and lack of technology, I knew there had to be a better way.  I thought to myself, "What if I could put lockers in apartment buildings?"  I knew this would solve my problems as a customer and from a business perspective, it would elminate all the issues that plague the delivery side of the business.<br />
<br />
I knew from that moment that my mission was to "change the way the world does laundry".  With that mission in mind, I created Laundry Locker, and revolutionized the dry cleaning, wash & fold, shoe shine and package delivery industries in San Francisco.  <br />
<br />
Now we have taken the same software and lockers that power Laundry Locker and made them available to you to create your own locker based delivery business.  With the Drop Locker solution, my vision is to see delivery lockers everywhere.<br />
<br />
No longer do I just want to change the way the world does laundry; I want to disrupt the way consumers look at all delivery services.  Locker based delivery is the wave of the future.  I look forward to sharing my vision with you and seeing you succeed the way Laundry Locker has.
<br />
<br />
Sincerely,<br>
Arik S. Levy<br>
CEO - Drop Locker, Inc.</span></p>
					
				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Low entry cost</h3>
						<p>Low startup fees and pricing based on the size of your operation.<br></p><a href="contact">Contact us</a> for a free pricing analysis and business consultation
					</div>
					<br />
					<br />
					<div class="post">
						<h3>More than just Laundry</h3>
						<p>Drop Locker is a platform that can power package delivery, shoe shines, eCommerce and much more.  Learn about all the different solutions you can provide to your customers.</p><a class="more" href="/main/products">Read More</a>
					</div>
				</div>
			</div>

		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>