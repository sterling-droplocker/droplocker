<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DropLocker Press</title>
<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
<script type="text/javascript" src="/js/jquery.main.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
    <div id="wrapper" class="sub">
        <? $active = 'press';
        include ('businessHeader.php'); ?>
        <div id="main">
            <div class="content-block">
                <div class="content">
                    <h1>Recent Press</h1>

                <strong> April 10, 2015: Citinerary</strong> <br />
                Embracing the 24/7 culture <br />
                <a href="http://citinerary.net/journal/embracing-the-24-7-culture" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> March 12, 2015: Digital Journal</strong> <br />
                Champion Cleaners Awarded National "Best Overall Service 2014" <br />
                <a href="http://www.digitaljournal.com/pr/2493835" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> February 9, 2015: psfk</strong> <br />
                24/7 Launderette Boasts Text-Message Ordering and App Notifications <br />
                <a href="http://www.psfk.com/2015/02/247-smart-launderette-amsterdam-bubble-stitch.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> January 27, 2015: The San Antonio Business Times</strong> <br />
                Dry cleaners invest in high-tech service to add more customers <br />
                <a href="http://www.bizjournals.com/sanantonio/news/2015/01/27/dry-cleaners-invest-in-high-tech-service-to-add.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> January 16, 2015: San Francisco Business Times</strong> <br />
                There’s an app for that: The new concierge economy promises to transform urban living for the masses <br />
                <a href="http://www.bizjournals.com/sanfrancisco/print-edition/2015/01/16/tech-firms-concierge-services-mobile-apps.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> January 10, 2015: Star Tribune</strong> <br />
                Twin Cities laundry service adds high-tech twist <br />
                <a href="http://www.startribune.com/housing/288085831.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> January 5, 2015: The Indianapolis Star</strong> <br />
                What we want Indy to get and do in 2015 <br />
                <a href="http://www.indystar.com/story/life/2015/01/03/indy-lifestyle-wish-list/21225515" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>December 07, 2014: Crain's Detroit Business</strong> <br />
                Martinizing acquisition makes Huntington largest dry-cleaning firm<br />
                <a href="http://www.crainsdetroit.com/article/20141207/NEWS/312079978/martinizing-acquisition-makes-huntington-largest-dry-cleaning-firm" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>August 31, 2014: Miami Herald</strong> <br />
                Technology, fresh ideas are creating new services for traditional businesses to serve customers<br />
                <a href="http://www.miamiherald.com/2014/08/31/4320335/inventive-in-vogue.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> July 16, 2014: DNAinfo Chicago</strong> <br />
                La startup du jour: Groombox, la conciergerie connectée pour gagner du temps<br />
                <a href="http://frenchweb.fr/startup-du-jour-groombox/160658" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> May 28, 2014: DNAinfo Chicago</strong> <br />
                Urban Laundry: Father and Son Bring 'Redbox of Dry Cleaning' to West Loop<br />
                <a href="http://www.dnainfo.com/chicago/20140528/west-loop/urban-laundry-father-son-bring-redbox-of-dry-cleaning-west-loop" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> May 2, 2014: Coral Gables News</strong> <br />
                ZoomLocker 24/7 Dry Cleaning and Laundry<br />
                <a href="http://www.communitynewspapers.com/coralgables/zoomlocker-247-dry-cleaning-and-laundry/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> April 30, 2014: San Francisco Business Times</strong> <br />
                Deliv battles Amazon for same-day delivery from malls<br />
                <a href="http://www.bizjournals.com/sanfrancisco/blog/techflash/2014/04/deliv-battles-amazon-same-day-delivery-malls.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> April 14, 2014: Downtown Project</strong> <br />
                A Bouyant Team Runs a Fresh Service<br />
                <a href="http://downtownproject.com/2014/a-buoyant-team-runs-a-fresh-service/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> April 8, 2014: American Drycleaner</strong> <br />
                Building Your Drycleaning Brand<br />
                <a href="https://americandrycleaner.com/articles/building-your-drycleaning-brand-part-1" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> March 13, 2014: San Francisco Business Times</strong> <br />
                San Francisco\'s Laundry Locker cleans up in apartment boom<br />
                <a href="http://www.bizjournals.com/sanfrancisco/blog/2014/03/san-franciscos-laundry-locker-cleans-up-in.html?page=all" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> February 10, 2014: Buzz Buzz Home</strong> <br />
                Tired of lugging your clothes to a dry cleaners? Alfred is at your service<br />
                <a href="http://news.buzzbuzzhome.com/2014/02/alfred-service.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> December 8, 2013: The Daily Domer</strong> <br />
                Alumni Make Dry Cleaning Digital<br />
                <a href="http://dailydomer.nd.edu/news/42933-alumni-make-dry-cleaning-digital/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> December 4, 2013: Inside Hook</strong> <br />
                Spin Doctors<br />
                <a href="http://www.insidehook.com/chicago/pressbox/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> November 6, 2013: Metropolitan Barcelona</strong> <br />
                Breaking from tradition: Lava Locker<br />
                <a href="http://www.barcelona-metropolitan.com/living/working/ed-hamilton%3A-lava-locker/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> November 1, 2013: Long Island Exchange</strong> <br />
                Rockville Centre Mayor Murray Welcomes Locker4Laundry to the Village<br />
                <a href="http://www.longislandexchange.com/press/2013/11/01/rockville-centre-mayor-murray-welcomes-locker4laundry-to-the-village/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 30, 2013: Pure Wow</strong> <br />
                CLEAN UP YOUR ACT<br />
                <a href="http://www.purewow.com/entry_detail/ny/7909" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 25, 2013: Business Observer</strong> <br />
                Laundry 2.0<br />
                <a href="http://www.businessobserverfl.com/section/detail/laundry-2.0/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 23, 2013: Tech Cocktail Chicagor</strong> <br />
                Pressbox Offers Dry Cleaning Like Never Before<br />
                <a href="http://tech.co/pressbox-offers-dry-cleaning-like-never-2013-10" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 22, 2013: RockvilleCentre Patch</strong> <br />
                Lockers4Laundry Ready to Wash in RVC<br />
                <a href="http://rockvillecentre.patch.com/groups/business-news/p/lockers4laundry-ready-to-wash-in-rvc" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 18, 2013: New York Times </strong> <br />
                These Cleaners Never Close<br />
                <a href="http://www.nytimes.com/2013/10/20/realestate/these-cleaners-never-close.html?_r=0&adxnnl=1&adxnnlx=1382284916-WxZrCoxxvUnDua6BmaOEUw" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 16, 2013: YourObserver.com </strong> <br />
                Whitney Beach Plaza gains first new tenants<br />
                <a href="http://www.yourobserver.com/news/longboat-key/Front-Page/1016201329732/Whitney-Beach-Plaza-gains-first-new-tenants" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 16, 2013: Bradenton Herald </strong> <br />
                Linen Locker to offer services in major markets<br />
                <a href="http://www.bradenton.com/2013/10/16/4775236/business-briefs-jess-jewelers.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> October 14, 2013: La Razon </strong> <br />
                Ropa limpia las 24 horas del dia<br />
                <a href="http://www.larazon.es/detalle_normal/noticias/3781761/ropa-limpia-las-24-horas-del-dia#.UlxD9FPkuKL" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> September 12, 2013: Long Beach Patch </strong> <br />
                Locker-Based Laundry Biz Launches in Long Beach<br />
                <a href="http://longbeach.patch.com/groups/business-news/p/lockerbased-laundry-launches-in-long-beach" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> September 10, 2013: Eureka Startups </strong> <br />
                Ahora ir a la Lavanderia y Tintoreria es tan facil como retirar dinero de un cajero automatico<br />
                <a href="http://www.eureka-startups.com/blog/2013/09/10/lavanderia-y-tintoreria-tan-facil-como-retirar-dinero-de-un-cajero-automatico/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> September 10, 2013: La Vanguardia </strong> <br />
                El lavado mas comodo<br />
                <a href="http://www.lavanguardia.com/economia/emprendedores/20130910/54382255120/el-lavado-mas-comodo.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> September 9, 2013: WGN TV </strong> <br />
                Pressbox: Chicago dry cleaning goes digital<br />
                <a href="http://wgntv.com/2013/09/10/pressbox-chicago-dry-cleaning-goes-digital/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> September 9, 2013: Larchmont Dish </strong> <br />
                MyDropOff opens two new 24/7 Locations<br />
                <a href="http://larchmontdish.com/_blog/Larchmont_Dish/post/september-9-2013/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> September 8, 2013: Chicago Tribune </strong> <br />
                Even dry cleaning is going digital<br />
                <a href="http://www.chicagotribune.com/business/ct-biz-0908-confidential-pressbox-20130908,0,1479812.column" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> August 2, 2013: LakeView Patch </strong> <br />
                Pressbox Introducing the "Redbox for Dry Cleaning" to Chicago<br />
                <a href="http://lakeview.patch.com/groups/business-news/p/pressbox-introducing-the-redbox-for-dry-cleaning-to-chicago" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> July 24, 2013: American Dry Cleaner </strong> <br />
                National Dry Cleaning Brand Announces First Franchisees<br />
                <a href="https://americandrycleaner.com/articles/national-dry-cleaning-brand-announces-first-franchisees" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> July 5, 2013: Express </strong> <br />
                Former BBC Apprentice star believes all is not lost after being fired by Lord Sugar<br />
                <a href="http://www.express.co.uk/news/showbiz/412735/Former-BBC-Apprentice-star-believes-all-is-not-lost-after-being-fired-by-Lord-Sugar" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> June 19, 2013: Fox News </strong> <br />
                Revolutionizing How You Get Your Laundry Done<br />
                <a href="http://video.foxnews.com/v/1697421215001/revolutionizing-how-you-get-your-laundry-done/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> May 5, 2013: Ad Week </strong> <br />
                This Former Hedge Fund Analyst Wants To Launder Your Clothes<br />
                <a href="http://www.adweek.com/news/advertising-branding/former-hedge-fund-analyst-wants-launder-your-clothes-149166" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> April 19, 2013: USA Today </strong> <br />
                Laundry Barons Take Dry Cleaning High Tech<br />
                <a href="http://www.usatoday.com/story/money/business/2013/04/19/high-tech-dry-cleaning-bizzie-boxes/2096003/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> April 9, 2013: Forbes </strong> <br />
                Step 1 For A Successful Startup: Identifying A Need In Your Community<br />
                <a href="http://www.forbes.com/sites/shawnoconnor/2013/04/09/step-1-for-a-successful-startup-identifying-a-need-in-your-community/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> April, 2013: Detroit Free Press </strong> <br />
                New Laundry Locker Service Aimed At Tech-savvy Urban Dwellers And Super Busy Professionals<br />
                <a href="http://www.freep.com/VideoNetwork/2310455459001/Bizzie-box-laundry-locker-demonstration?odyssey=mod|newswellvideo|text" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> March, 2013: National Clothesline </strong> <br />
                Turning It Inside Out<br />
                <a href="http://www.natclo.com/1303/profile.htm" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> February 21, 2013: Click On Detroit </strong> <br />
                Bizzie Box May Be Dry Cleaning Breakthrough For Busy People<br />
                <a href="http://www.clickondetroit.com/money/consumer/Bizzie-Box-may-be-dry-cleaning-breakthrough-for-busy-people/-/1719076/19026872/-/pmb0we/-/index.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> February 20, 2013: National Real Estate Investor </strong> <br />
                Personalized Amenity Trend Takes Off With Locker-based Delivery<br />
                <a href="http://nreionline.com/blog/personalized-amenity-trend-takes-locker-based-delivery" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> February 13, 2013: Business Insider </strong> <br />
                The 25 Most Innovative Businesses In New York City<br />
                <a href="http://www.businessinsider.com/most-innovative-businesses-in-nyc-2013-2?op=1" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>February 5, 2013: Multi Housing News</strong>
                <br />
                Reduce Resident Hassle with New bizzie Dry Cleaning Service<br />
                <a href="http://www.multihousingnews.com/product-gallery/reduce-resident-hassle-with-new-bizzie-dry-cleaning-service/1004073335.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>January 17, 2013: New York Daily News</strong>
                <br />
                New York businessman's start-up lets customers drop off and pick up laundry and dry cleaning at conveniently-placed lockers<br />
                <a href="http://www.nydailynews.com/new-york/bizman-hopes-clean-laundry-start-article-1.1240764" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>July 23, 2013: Wall Street Journal</strong>
                <br />Midtown Lunch Heats Up <br />
                <a href="http://online.wsj.com/article/SB10000872396390443570904577543223512030502.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />


                <strong> June 15, 2012: Huffington Post </strong> <br />
                Dashlocker Fomed as Banker Turned Entrepreneur Tries To Shake Up Dry-Cleaning World <br />
                <a href="http://www.huffingtonpost.com/2012/06/15/dashlocker-dry-cleaning_n_1599767.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> June 5, 2012 PRLog </strong> <br />
                NYC's Dry Cleaning, Wash &amp; Fold and Shoe Shine Go 2.0: DashLocker Launches First 24/7 Location <br />
                <a href="http://www.prlog.org/11892525-nycs-dry-cleaning-wash-fold-and-shoe-shine-go-20-dashlocker-launches-first-247-location.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>April 25, 2012: iMedia Connection</strong> <br>
                How Siri is going to completely change your job<br>
                <a href="http://www.imediaconnection.com/article_full.aspx?id=31603" target="_blank" class="greenLink">view article</a>
                    <br /> <br />

                <strong>April 17, 2012: Women 2.0</strong> <br>
                5 Ways to Achieve Work Life Balance<br>
                <a href="http://www.women2.org/5-ways-to-achieve-work-life-balance-hint-outsource-errands/" target="_blank" class="greenLink">view article</a>
                    <br /> <br />

                <strong>December 7th, 2011: Broke-Ass Stuart's</strong> <br>
                Laundry Locker: Affordable, Green Laundry Service<br>
                <a href="http://brokeassstuart.com/blog/2011/12/07/laundry-locker-affordable-green-laundry-service/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>October 20, 2011: Bentley Observer</strong> <br>
                A Fresh Idea: Arik Levy '96<br>
                <a href="http://observer.bentley.edu/issue/fall-2011/alumni-update/fresh-idea-arik-levy-96" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>July 5, 2011: Mosio</strong> <br>
                Expert Q&A: Text Messaging in Dry Cleaning and Laundry Delivery Services (Arik Levy, Laundry Locker)<br>
                <a href="http://www.mosio.com/mobileanswers/tag/arik-levy/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>June 12, 2011: Chase</strong> <br>
                Laundry Locker's CEO, Arik Levy, shares his strategies for success.<br>
                <a href="http://www.inkfromchase.com/grow-your-business/strategies-for-success/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>May 18, 2011: SF Weekly</strong> <br>
                Laundry Locker Wins 2011 Best Dry Cleaner.<br>
                <a href="http://www.sfweekly.com/bestof/2011/award/readers-poll-winners-2489590/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Dec 14, 2010: USPTO</strong> <br>
                US Patent Issued to Laundry Locker on Nov. 30 for "Handling Household Tasks" (California Inventor)<br>
                <a href="http://www.highbeam.com/doc/1P3-2211688611.html target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Sept 15, 2010: Forbes</strong> <br>
                What Google Has In Common With A Laundry Service<br>
                <a href="http://blogs.forbes.com/kymmcnicholas/2010/09/15/what-google-has-in-common-with-a-laundry-service/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Sept 9, 2010: 7x7 Magazine</strong> <br>
                Bay Area Power Couples: Arik &amp; Holly Levy of Laundry Locker<br>
                <a href="http://www.7x7.com/love-sex/bay-area-power-couples-arik-holly-levy-laundry-locker" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>May 20, 2010: SF Weekly</strong> <br>
                Laundry Locker Wins 2010 Best Dry Cleaner.<br>
                <a href="http://www.sfweekly.com/bestof/2010/award/readers-poll-winners-1983734/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Dec 30, 2008: Springwise</strong> <br>
                Laundry Locker is ranked #6 on this year's top 10 life hacks.<br>
                <a href="http://springwise.com/life_hacks/2008_this_years_top_10_life_ha/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Aug 18, 2008: Daily Candy</strong> <br>
                Laundry Locker featured in Daily Candy.<br>
                <a href="http://www.dailycandy.com/san_francisco/article/38286/Spin+Doctors" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Sep 12, 2007: KRON 4 News</strong> <br>
                Laundry Locker featured on the KRON 4 news technology segment with Gabriel Slate.<br>
                <a href="http://www.youtube.com/watch?v=2-qChq7VSSs" target="_blank" class="greenLink">view video</a>
                <br><br>
                <strong>Aug 1, 2007: Nation's First Unattended 24 x 7 Dry Cleaning and Wash &amp; Fold Facility</strong> <br>
                Overworked San Fracisco gets relief from pains of laundry<br>
                <a href="press/08012007.pdf" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>May 1, 2006: San Francisco Apartment Association Magazine</strong><br>
                A Fresh Look at Laundry Delivery<br>
                <a href="http://www.sfaa.org/0605landes.html" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Jun 1, 2005: Laundry Locker: Open for Business</strong><br>
                Service will offer San Francisco ultra convenient, 24 x 7, next-day dry cleaning and wash &amp; fold. <br>
                <a href="press/06012005.pdf" target="_blank" class="greenLink">view article</a>

                </div>
                <div class="box-aside">
                    <div class="post">
                        <h3>Schedule a Demo</h3>
                        <p>Want to see how sophisticated our software is?  Schedule a demo with our sales team by <a href="mailto:<? echo $salesEmail; ?>">sending us a quick email</a></p>
                    </div>
                    <br><br>
                    <div class="post">
                        <h3>Robust Infrastructure</h3>
                        <p>Drop Locker's software runs on Amazon Web Services, the same infrastructure that runs Amazon.com.  We have servers located around the world to ensure there is a server close to you and our systems have multiple failover protections to ensure 99% availability.</p><a class="more" href="/main/infrastructure">Read more</a>
                    </div>
                    <br />
                    <br />
                    <div class="post">
                        <h3>True Cloud Computing</h3>
                        <p>The Drop Locker platform was built ground-up to be a web based solution.  This means that you can access your system from any web device at anytime. It also ensure seemless upgrades and no server hardware.  Just load up a web browser and you are up and running.</p><a class="more" href="/main/infrastructure">Read more</a>
                    </div>
                </div>
            </div>

        </div>
        <? include ('footer.php'); ?>
    </div>
</body>
</html>
