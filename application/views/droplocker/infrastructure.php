<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DropLocker Infrastructure</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'technology';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Our Cloud Based Infrastructure</h1>
					<p>Cloud computing is all the rage, but what does it mean for you, our customer?<br/>
					<br/>
					Cloud computing just means that your software is web based. So instead of running and application on your computer, like MS Word, our application runs in your web browser.<br/>
					<br/>
					For our customers, this provides many advantages over traditional dry cleaning or laundromat software including:</p>
					<div class="table-wide">
						<h6>Why The Cloud Is Better:</h6>
						<table class="table">
							<tr class="active">
								<td class="col1">Remote Web Access</td>
								<td class="col2">You can acess your data anytime from any web device.  You ca see reports, track orders, deal with problems, all from your smartphone.</td>
							</tr>
							<tr>
								<td class="col1">No Special Hardware</td>
								<td class="col2">Unlike traditional networked business applications, our software only requires a computer with a web browser.  Our software can run on an iPhone, a $300 netbook, or your smartphone.</td>
							</tr>
							<tr class="active">
								<td class="col1">Instant Upgrades</td>
								<td class="col2">Ever had a bug with your software or a feature that you wanted added?  Traditionally that would mean a painful and expense upgrade process and you might have event needed to upgrade your hardware.  With our platform, ugrades are seemless and are deployed as fast as you can refreh the page.</td>
							</tr>
							<tr>
								<td class="col1">Customer Visibility</td>
								<td class="col2">Since your software is web-based, this means customers can access it as well (with strict security and a different view of the data of course).  This means that when your driver picks up and order out in the field, it is immediately added to the system and if the customer logs into their acount, they'll see the order listed there.</td>
							</tr>
							<tr class="active">
								<td class="col1">Smartphones</td>
								<td class="col2">Smartphones are internet devices and therefore can communicate with your software.  As your drivers progresses through his route you can real-time view his progress, all of which is automatically communicated to your customers in the form of emails and text messages.</td>
							</tr>
<tr>
								<td class="col1">Automatic Backups and Disaster Recovery</td>
								<td class="col2">Every business owner is terrified of what might happen in case of a fire, flood or natural disaster.  Our software runs on multiple servers in multiple locations, meaning it is always available.  Though we can't prevent disasters, we can make sure our software is available in case of one.</td>
							</tr>
						</table>
					</div>
					<h3>Scalability, Redundancy and Reliability</h3>
					<p>Our software runs on Amazon Web Services, which allows us to provide you with one of the most robust infrastructures in the world.  We have multiple servers located around the world.  That means you work on the server that is closest to you to improve performance, but if it goes down, you are automatically shifted to the next closest server.  It also means that we have multiple databases around the world, replicating your data to ensure you never lose anything.  And if our software slows down due to heavy loads, our technicians can bring up aditional capacity in minutes.
					<br/>
					With the Drop Locker platform, your are buying into the most robust software platform in the industry.  Just another way we are putting you leaps and bounds ahead of the competition.	
					</p>
					
					
				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Schedule a Demo</h3>
						<p>Want to see how sophisticated our software is?  Schedule a demo with our sales team by <a href="mailto:sales@droplocker.com">sending us a quick email</a></p>
					</div>
					<br><br>
					<div class="post">
						<h3>The Only Solution for Locker Based Delivery</h3>
						<p>Find out about many of the incredible features our software offers. </p><a class="more" href="/main/technology">Read more</a>
					</div>
				
				</div>
			</div>
			
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>