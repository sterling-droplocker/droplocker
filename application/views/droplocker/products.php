<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DropLocker Products</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'products';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Products</h1>
					<p>Once your business is Powered By Drop Locker, you get to choose which products you want to offer to your customers.
					<br />
					<br />
					The Drop Locker platform has many modules that are fully configurable to meet your business needs.  From service times, to pricing, to upcharges, you get to run your business the way you want to.<br />
					<br />
					<div class="table-wide">
						<h6>Currently Available Modules:</h6>
						<table class="table">
							<tr class="active">
								<td class="col1">Dry Cleaning</td>
								<td class="col2">Our dry cleaning module enables your customer to drop off their dry cleaning with you and powers the whole cleaning process.  You can process the clothes yourself or send them out to a wholesaler.  You set your pricing, service times and policies to work best for your business and your customers.</td>
							</tr>
							<tr>
								<td class="col1">Wash & Fold</td>
								<td class="col2">With our wash & fold module, you can select what options to offer your customers (wash temperatures, detergents, fabric softeners, etc...  Charge for these options and manage your production process to ensure accurate billing and full accountability.  Want to outsource the production, no problem.  With our wash & fold module your supplier just needs and internet connection and production will run like you are doing it yourself.</td>
							</tr>
							<tr class="active">
								<td class="col1">Package Delivery</td>
								<td class="col2">We all know what it's like to wait at home for a package to arrive.  With the Drop Locker solution, your customers ship their packages to you and you deliver them to the locker, ready for them to pickup at their convenience.</td>
							</tr>
							<tr>
								<td class="col1">Shoe Shine / Repair</td>
								<td class="col2">Want to offer shoe shines and repairs?  Our shoe module offers full traceability of each shoe and accurate billing.</td>
							</tr>
							<!-- <tr class="active">
								<td class="col1">Sundry Store</td>
								<td class="col2">Just about to run out of toothpaste?  No problem.  With our sundry module, you can become a virtual corner store, delivering those convenience store items for customers to pickup on their schedule.</td>
							</tr> -->
							<tr class="active">
								<td class="col1">Clothing Storage</td>
								<td class="col2">Seasons change and we don't all have walk-in closets.  With out clothing storage module, customer can drop off their clothes in a locker and you can store them at your facility, barcoded, categorized and easy to retrieve.  This is a great way to get additional dry cleaning revenue as most customers want their clothes cleaned before they have them returned.</td>
							</tr>
							<tr>
								<td class="col1">Wholesale Processing</td>
								<td class="col2">Do you have excess production capacity and want to provide wholesale services.  The software can manage all aspects of running a wholesale cleaning operation.</td>
							</tr>
						</table>
					</div>
					<div class="table-block1">
						<h5>Coming Soon:</h5>
						<table class="table">
							<tr class="active">
								<td class="col1">Advanced Alterations</td>
								<td class="col2"></td>
							</tr>
							<tr>
								<td class="col1">Restoration Cleaning</td>
								<td class="col2"></td>
							</tr>
							<tr class="active">
								<td class="col1">Supermarket Shopping</td>
								<td class="col2"></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Get Started Today!</h3>
						<p>Are you sold yet?  The Drop Locker system will have you up and running with 24/7 delivery in no time.  Be the first in your market and get a huge leg up on your competition.<br />
						<br />
						To learn more, please call us at <? echo $phone; ?> or <a href="mailto:<? echo $salesEmail; ?>">email our sales team</a>.</p>
					</div>
				</div>
			</div>
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>