<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drop Locker: Partners</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
  <link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?=get_default_gmaps_api_key();?>"></script>
	<script type="text/javascript">

	var points = {
		"sf":{"lat":"37.75470", "lng":"-122.43782", "zoom":12},
		"ny":{"lat":"42.77928", "lng":"-76.11328", "zoom":6},
	}


</script>
<script src="/js/StyledMarker.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	//var latlng = new google.maps.LatLng(37.800000, -122.357221);
	//var latlng = new google.maps.LatLng(0, 0);
        var latlng = new google.maps.LatLng(36.879621, -98.5);
	//var bounds = new google.maps.LatLngBounds();
    var myOptions = {
      zoom: 4,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
    var infowindow = new google.maps.InfoWindow();
    function add_marker(lat,lon,locationID,address,name,info, color){
        latlng = new google.maps.LatLng(lat, lon);
        //bounds.extend(latlng);
        color = '#' + color;

        //var styleIcon = new StyledIcon(StyledIconTypes.MARKER,{color:color});
        var marker = new google.maps.Marker({icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png', position:latlng, map:map});

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(info);
            infowindow.open(map,marker);
        });
    };


    var json = <?= $json_locations?>;

    $.each(json, function(i, item){
        var lat = item.lat;
        var lon = item.lon;
        //var color = item.abbreviation == 'k' ? '0099ff' : 'A4D423';
        //var target = item.abbreviation == 'k' ? '#locationTablePublic' : '#locationTable';
        var color = 'FE7569';
        var target = '#locationTable';

        add_marker(lat,lon,item.locationID,item.address, 'active', item.info, color);
        var link = '';
        if(item.locationID==483){
            link = "<a href='/'>";
         } else if(item.locationID==341){
         	link = '<a href="/main/clay" target="_blank">';
         } else if(item.locationID==186){
             link = '<a href="main/onecalifornia" target="_blank">';
         } else {
             link = '<a href="https://maps.google.com/maps?q='+item.address+' '+item.city+', '+item.state+'" target="_blank">';
         }

       
          $(target).append("<tr><td><img src='http://thydzik.com/thydzikGoogleMap/markerlink.php?&color=" + color + "' /></td><td class='bodyReg'>"+link+""+item.companyName+"<br>"+item.address+","+item.city+"</a></td></tr>");

    });
    
    //map.fitBounds(bounds);
});
</script>

</head>
<body>
	<div id="wrapper" class="sub">
		<?php
                    $active = 'partners';
                    include ('header.php');
                ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
                                        <h1>Our Partners</h1>
					<div id="map_canvas" style="width:100%; height:400px; border:1px solid #666;"></div>


					<br />

				</div>
				<div class="box-aside">
					<div class="post" style='display:none'>
						<h3>Reviews</h3>
						<p>Five Stars!!!</p>
					</div>


					<div class="post">
						<h3>Want to start your own locker based delivery business?</h3>
						<a class="more" href="/main/business">Read More<a/>
					</div>
				</div>
			</div>
		</div>

		<? include ('footer.php'); ?>
	</div>
</body>
</html>
