<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DropLocker Technology</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'technology';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>World Class Technology</h1>
					<p>When you partner with Drop Locker, your business becomes powered by the most sophisticated software in the industry (that's because we invented the industry). <br />
<br />
While our business model and software platform was built to make customer's lives easier, the Drop Locker platform has been designed ground up to manage an entire locker based delivery business. From the second a customer places an order, through order processing and delivery back to the locker, our software platform manages each area of your business. </span></p>
					<div class="table-wide">
						<h6>Standard Features:</h6>
						<table class="table">
							<tr class="active">
								<td class="col1">Customer Account<br>Management</td>
								<td class="col2">As part of the Drop Locker solution, we will build a website custom designed with your business's design for your customers to manage their account.  This is where customers can place orders, track orders, set their preferences, make payment, buy gift cards, view their Closet 2.0 and much, much more...all customized to look like your website.</td>
							</tr>
							<tr>
								<td class="col1">Route Management</td>
								<td class="col2">Sophisticated mapping software shows all your pickups and deliveries for the day.  Making it easy for your drivers to plan their routes</td>
							</tr>
							<tr class="active">
								<td class="col1">Payment Processing</td>
								<td class="col2">Drop Locker works with PayPal Payflow Pro and Authorize.Net for credit card processing.  Payments are processed on your merchant account so we do not make any money on your transactions and transactions are directly posted to your account. <br>Drop Locker software also provided a sophisticated Invoicing engine for account that you want to receive invoices.</td>
							</tr>
							<tr>
								<td class="col1">Closet 2.0</td>
								<td class="col2">Closet 2.0 functionality allows customers and you to view pictures of every item that has been cleaned, providing a tool for customers to better communicate with you.  Our patented (USPTO Patent Number 7,844,507) picture taking and annotation software is included as part of your software subscription.</td>
							</tr>
							<tr class="active">
								<td class="col1">Customer Service</td>
								<td class="col2">All the tools to manage your customer interactions are at your fingertips 24 hours a day, 7 days a week, from any web-enabled device.  You can track orders, process payments, track your drivers, manage your lockers and do everything else you would need to do using our business portal.</td>
							</tr>
							<tr>
								<td class="col1">Knowledge Sharing</td>
								<td class="col2">All Drop Locker business owners have access to our expansive knowlege base and customer forums.  Knowleged is shared among our members giving you an edge over your competition.</td>
							</tr>
							<tr class="active">
								<td class="col1">Employee Management</td>
								<td class="col2">Manage your employees, even tracking production costs to actual hours worked, in real time.  You can have employees track their timecards and manage employee schedules as well as control what aspects of the system they can view and which ones are restricted for them.</td>
							</tr>
							<tr>
								<td class="col1">Locker Management</td>
								<td class="col2">We provide all the tools to manage the lockers throughout your network.  Whether you have one location or hundreds, you can easily track and report on every locker.</td>
							</tr>
							<tr class="active">
								<td class="col1">Email and SMS Notifications</td>
								<td class="col2">Automatic email and SMS notifications can be sent to your customers as the order progresses through the process.  All notifications are customizable by you and opt in/out by the customer.</td>
							</tr>
							<tr>
								<td class="col1">Reporting</td>
								<td class="col2">Extensive reporting capabilities throughout the system with the ability to schedule reports to be emailed to you on your schedule.</td>
							</tr>
							<tr class="active">
								<td class="col1">eNewsletter Management</td>
								<td class="col2">Powerful tools for constructing and distributing electronic newsletters.</td>
							</tr>
							<tr>
								<td class="col1">Order Management</td>
								<td class="col2">Your support personnel can easily manage and track each orders with user friendly interfaces from the moment the order is placed until it is securely returned.</td>
							</tr>
							<tr class="active">
								<td class="col1">Mobile Driver App</td>
								<td class="col2">An Android based driver application eliminates errors, provides real-time visibility into order status and let's you track you drivers.  All for free and running on any Android device.</td>
							</tr>
							<tr>
								<td class="col1">Much, Much More!</td>
								<td class="col2">We have only listed a handful of features that our system includes.  Many other features such as Discounts, Gift Cards, Targeted Email Marketing, Assisted or Automated Assmbly and more are all included with your license.  <a href="mailto:sales@droplocker.com">Schedule a demo</a> with our sales team for a full tour of the software.</td>
							</tr>
						</table>
					</div>
					<div class="table-wide">
						<h5>Other Features:</h5>
						<table class="table">
							<tr class="active">
								<td class="col1">Production Managment</td>
								<td class="col2">Our production management module helps you track your productivity.  In real-time you and your production crew can view productivity.  Our software is much more feature rich than other piece counters since we track every piece at an item level, enabling you to see machine problems or rework issues.</td>
							</tr>
							<tr>
								<td class="col1">Automatic Assembly Integration</td>
								<td class="col2">Out system can work with manual tickets or heat seal barcodes.  Orders can be assembled manually, via assisted assembly or fully automated assemly with Metalprojetti integration.</td>
							</tr>
							<tr class="active">
								<td class="col1">Outsourced Production Management</td>
								<td class="col2">If you want to outsource some or all of your production, the software is here to help. Drop Locker's software helps you track costs and track down missing items.</td>
							</tr>
							<tr>
								<td class="col1">Access Control System</td>
								<td class="col2">Use our credit card based access control system to instantly give customers secure access to your location by swiping their credit card.</td>
							</tr>
						</table>
					</div>
					
				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Schedule a Demo</h3>
						<p>Want to see how sophisticated our software is?  Schedule a demo with our sales team by <a href="mailto:<? echo $salesEmail; ?>">sending us a quick email</a></p>
					</div>
					<br><br>
					<div class="post">
						<h3>Robust Infrastructure</h3>
						<p>Drop Locker's software runs on Amazon Web Services, the same infrastructure that runs Amazon.com.  We have servers located around the world to ensure there is a server close to you and our systems have multiple failover protections to ensure 99% availability.</p><a class="more" href="/main/infrastructure">Read more</a>
					</div>
					<br />
					<br />
					<div class="post">
						<h3>True Cloud Computing</h3>
						<p>The Drop Locker platform was built ground-up to be a web based solution.  This means that you can access your system from any web device at anytime. It also ensure seemless upgrades and no server hardware.  Just load up a web browser and you are up and running.</p><a class="more" href="/main/infrastructure">Read more</a>
					</div>
				</div>
			</div>
			
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>