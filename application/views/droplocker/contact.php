<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>DropLocker Contact Info</title>
        <link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
        <link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
        <script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
        <script type="text/javascript" src="/js/jquery.main.js"></script>
        <script type="text/javascript" src="/js/functions.js"></script>
        <script src='//www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
        <div id="wrapper" class="sub">
            <? $active = 'contact';
            include ('businessHeader.php'); ?>
            <div id="main">
                <div class="content-block">
                    <div class="content">
                        <h1>LICENSE THE DROP LOCKER PLATFORM</h1>
                        <h2>Start providing unmanned, 24-Hour locations and start building your business today.</h2>
                        You’ve reached the maximum profit you can make with one location, or you have home/corporate delivery routes, but you don’t want the hassle of managing multiple store fronts.  Or maybe you are handling home or corporate delivery, and want to take your business to the next level. Drop Locker is here to help.
                        <br /><br />Use our lockers to create a network of unmanned laundry locations and automate the process of pick up and drop off delivery to service those locations from one location. Grow your customer base and increase profits all while minimizing management expense. Many of our licensees can service an entire city from one base location.
                        <br /><br />We now have successful licensees in cities and countries all over the world, and if you are a successful entrepreneur or ambitious team, we want to work with you.
                        <br /><br />Contact us to learn how you can get:
                        <ul>
                            <li>A cloud-based solution for any type of delivery of services (not limited to laundry, dry cleaning, shoe repair, package delivery, retail goods, or services)</li>
                            <li>24/7 automated drop off and pick up locations with patented Locker systems. Lockers can also be placed in apartments, condos, offices, garages, grocery/retail stores, and many other locations.</li>
                            <li>State of the art delivery tracking software that cuts time, expense and minimizes human error or item misplacement.  Full software training and ongoing consulting</li>
                            <li>Apple iPhone & Google Android App, automated integration for text/email alerts, payments, subscription plans, referral system, and more</li>
                            <li>A support team with a proven track record of success including software, marketing, sales, PR and operations support</li>
                        </ul>

                        <!-- <h1>Contact Info</h1>
                        <p>If you are interesting in purchasing lockers or learing more about<br />Drop Locker's services, please contact us at:<br />
                        <ul>Drop Locker, Inc.<br />
                        1530 Custer Ave.<br />
                        San Francisco, CA 94103<br />
                        <br />
                        <? echo $phone; ?><br />
                        <a href="mailto:<? echo $salesEmail; ?>"><? echo $salesEmail; ?></a><br />
                        </ul> -->
                        <br />
                        For questions about an order, please contact your service provider.  Drop Locker does not service the lockers, we are only the software and technology supporting the operations.
                        <br />
                    </div>
                    <div class="box-aside">
                        <div class="post">
                            <h3>Get more information</h3>
                            <?= $this->session->flashdata('status_message'); ?>
                            <p>
                                <form action="/main/contact" class="email-form" id="contact_form" method="post">
                                    <fieldset>
                                        <div class="row">
                                            <div class="text-field-white">
                                                <input type="text" id='sname_input' name="name" placeholder='Name (required)' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='semail_input' name="email" placeholder='Email (required)' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='sphone_input' name="phone" placeholder='Phone (required)' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='scity_input' name="city" placeholder='City' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='sstate_input' name="state" placeholder='State / Country' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='sbusiness_type_input' name="business_type" placeholder='Current Business Type' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='scompany_input' name="company" placeholder='Company Name' value=""/>
                                            </div>
                                            <div class="text-field-white" style="margin-top:5px;">
                                                <input type="text" id='swhen_to_start_input' name="when_to_start" placeholder='When would you like to start?' value=""/>
                                            </div>
                                            <div class="text-area-white" style="margin-top:5px;">
                                                <textarea rows="6" cols="30" id='sdetails_input' name="notes" placeholder='Any other details you wish to provide'></textarea>
                                            </div>
                                            <div class="text-area-white g-recaptcha" data-sitekey="<?= $recaptcha_public_key; ?>" style="transform:scale(0.83);-webkit-transform:scale(0.83);transform-origin:0 0;-webkit-transform-origin:0 0;margin-top:5px;">
                                            </div>
                                        </div>
                                        <button type="submit" class="more" style="border:none;width: 116px;text-align: left;">Contact Me</button>
                                    </fieldset>
                                </form>
                            </p>

                            <script type="text/javascript" src="/js/jquery-validation-1.11.1/jquery.validate.js"></script>
                            <script type="text/javascript">
                                var email = '';
                                $(document).ready(function () {

                                    $("#contact_form").validate({
                                        rules: {
                                            email: {
                                                email: true,
                                                required: true
                                            },
                                            name: {
                                                required: true
                                            },
                                            phone: {
                                                required: true
                                            }
                                        },
                                        messages: {
                                            email: "Please enter a valid e-mail",
                                            name: "Please enter your name",
                                            phone: "Please enter your phone number"
                                        },
                                        errorClass: "invalid",
                                        submitHandler: function (form) {
                                            form.submit();
                                            $(":submit").attr("disabled", true);
                                        }
                                    });
                                });
                            </script>


                        </div>
                    </div>
                </div>
            </div>
            <? include ('footer.php'); ?>
        </div>
    </body>
</html>
