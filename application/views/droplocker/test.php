<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DropLocker Pricing</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/all.css" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'pricing';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Pricing</h1>
					<p>Drop Locker's pricing model is very straightfoward and cost effevtive. Our customers pay a setup fee, they buy the lockers, and then there is an ongoing monthly fee for our sofware.<br/><br><strong>One Time Setup Fee:</strong> <span>$1500</span></p>
					<div class="table-block1">
						<h6>Software Pricing <div style="float:right;">1 Year Contract</div></h6>
						<table class="table">
							<tr class="active">
								<td class="col1">10 - 100 compartments</td>
								<td class="col2">$10 per month / compartment</td>
							</tr>
							<tr>
								<td class="col1">100 - 1000 compartments</td>
								<td class="col2">$8 per month / compartment</td>
							</tr>
							<tr>
								<td class="col1">More than 1000 compartments</td>
								<td class="col2">$5 per month / compartment</td>
							</tr>
							<tr class="last">
								<td class="col1">Non-Locker Locations</td>
								<td class="col2">$.10 per month / unit</td>
							</tr>
						</table>
					</div>
					<div class="table-block1">
						<h5>Locker Pricing ( includes digital locks )<div style="float:right;">+ Shipping</div></h5>
						<table class="table">
							<tr class="active">
								<td class="col1">Metal Lockers</td>
								<td class="col2">$500 each</td>
								<td class="col3">Two Compartments</td>
							</tr>
							<tr>
								<td class="col1">Wooden Lockers</td>
								<td class="col2">$6,000</td>
								<td class="col3">Ten Compartmetns</td>
							</tr>
						</table>
					</div>
					<div class="table-block1">
						<h6>Support Services</h6>
						<h4>We do not charge for any support issues regarding outages or system problems.  Support charges are only for phone calls regading non-outage issues. Email support is always free.</h4>
						<table class="table">
							<tr class="active">
								<td class="col1">10 - 100 compartments</td>
								<td class="col2">Free Email Support / $25 Per Phone Call</td>
							</tr>
							<tr>
								<td class="col1">100 - 1000 compartments</td>
								<td class="col2">Free Email Support / Free Phone Support During Business Hours</td>
							</tr>
							<tr class="active">
								<td class="col1">More than 1000 compartments</td>
								<td class="col2">Free Email and Phone Support 24/7</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="box-aside">
					<div class="table-block2">
						<h3>Starter Package:</h3>
						<table class="table">
							<tr class="active">
								<td class="col1">Startup Fee</td>
								<td class="col2">$1500</td>
							</tr>
							<tr>
								<td class="col1">5 Metal Lockers</td>
								<td class="col2">$2500</td>
							</tr>
							<tr>
								<td class="col1">1 Year Software</td>
								<td class="col2">$1200</td>
							</tr>
							<tr class="last">
								<td class="col1">&nbsp;</td>
								<td class="col2">$5200</td>
							</tr>
						</table>
					</div>
					<div class="post">
						<h3>Fall Special</h3>
						<p>Until November 1st, anyone who buys 5 lockers, will receive 1 additional locker free.</p>
					</div>
					<br>
					<div class="table-block2">
						<h3>Other Services</h3>
						<table class="table">
							<tr>
								<td class="col1"><ul class="list-services">
								<li><a class="bullet1" href="#">Consulting</a></li>
								<li><a class="bullet2" href="#">Training</a></li>
								</ul></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>