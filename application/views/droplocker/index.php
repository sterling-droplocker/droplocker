<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drop Locker 24/7 Delivery Service</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>

</head>
<body>
	<div id="wrapper">
		<? include('header.php'); ?>
		<div id="main">
			<div class="block">
				<div class="holder">
					<div class="wrap">
						<div class="carousel">
							<div class="frame">
								<ul class="slide-list">
									<li>
										<img src="images/img_open.jpg" alt="image definition" width="471" height="340" />
										<div class="info-block">
											<h2>We're Always Open!</h2>
											<p>Frustrated that your dry cleaner is never open when you need them?  Tired of waiting at home for the UPS driver?  Well Drop Locker is the solution for you!</p>
											<p>Our business partners have lockers located across the globe, ready to service you with dry cleaning, wash & fold, package delivery, shoe shine and much more.</p>
											<p>Visit our locations page to find lockers near you.</p>
											<div class="buttons">
												<a class="register" href="/main/locations">Find Locations</a>
												<a class="more2" href="/main/how">Learn more</a>
											</div>
										</div>
									</li>
									<li>
										<img src="images/img1.jpg" alt="image definition" width="471" height="340" />
										<div class="info-block">
											<h2>Power Your Business With Drop Locker</h2>
											<p>The Drop Locker platform was built from the ground-up to support locker based delivery.  Our platform includes everything you need to get up an running, quickly, easily and extremely cost effectively.<br />
											<br />
											Whether you want to add lockers to your laundromat, give your dry cleaning customers 24/7 access to your services, or start a new locker based delivery business in your region, we have the solution for you.</p>
											<a class="register" href="/main/pricing">Pricing</a>
											<a class="more2" href="/main/business">Learn more</a>
										</div>
									</li>
									<li>
										<img src="images/img_puzzle.jpg" alt="image definition" width="471" height="340" />
										<div class="info-block">
											<h2>Patented Software</h2>
											<p>The cloud based Drop Locker Platform has been battle tested to ensure it can run your business 24 hours a day, 7 days a week.  Built using the six sigma methodology of "it's the process, not the people" we have designed a system that ensure complete visibility and accuracy throughout the entire process.</p>
											<p>When you run your business on the Drop Locker platform you are ensured that you will be at the leading edge of technology, leaving your competition in the dust.</p>
											<a class="register" href="/main/technology">Technology</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="switcher">
								<ul>
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div style="padding-bottom: 20px;" align="center">
                <a href="/main/droplocker_press"><img src="images/featuredSites.jpg" width="960" height="70" alt="Featured Sites" /></a>
            </div>
			<div class="box">
				<div class="box-holder">
					<div class="box-frame">
						<div class="box-content">
							<h2>Locations</h2>
							<div class="visual">
								<img alt="Image definition" src="images/img3.jpg" width="232" height="131" />
							</div>
							<p>Drop Locker has partnered with leading service providers across the globe. Read more about our partners...</p>
							<a class="more" href="/main/droplocker_press">Read more</a>
						</div>
						<div class="box-content">
							<h2>Our Technology</h2>
							<div class="visual">
								<img alt="Image definition" src="images/network_small.jpg" width="232" height="131" />
							</div>
							<p>World class, patented technology, transforms the customer experience and eliminates the dreaded trips to the dry cleaner.</p>
							<a class="more" href="/main/how">Read more</a>
						</div>
						<div class="box-content">
							<h2>Business Owners</h2>
							<div class="visual">
								<img alt="Image definition" src="images/img5.jpg" width="232" height="131" />
							</div>
							<p>Are you interested in adding locker based, 24/7 delivery and pickup to your business? Would you like to start a new locker based delivery business?</p>
							<a class="more" href="/main/technology">Read more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<? include('footer.php'); ?>
	</div>
</body>
</html>