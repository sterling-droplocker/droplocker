<div id="footer">
			<div class="footer-holder">
				<div class="menus">
					<h3>Main <span>Menus</span></h3>
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="/main/contact">Contact Us</a></li>
						<li><a href="/main/how">How it works</a></li>
						<!-- <li><a href="/main/locations">Locations</a></li> -->
					</ul>
					<ul>
						<li><a href="/main/business">Business owners</a></li>
						<li><a href="/main/technology">Technology</a></li>
                        <li><a href="/main/droplocker_press">Droplocker in the Press</a></li>
						<!--<li><a href="login.php">Login</a></li>-->
					</ul>
				</div>
				<div class="newsletter">
					<h3>Join Our <span>newsletter</span></h3>
					<form action="/main/newsletter" class="email-form" method='post'>
						<fieldset>
							<div class="row">
								<div class="text-field">
									<input type="text" id='e' placeholder="Email Address" name='e' />
								</div>
							</div>
							<input type="text" id='email' name='email' />
							<input type="text" id='name' name='name' />
							<input class="btn-submit" id='e_submit' type="submit" name='submit' value="Subscribe" />
						</fieldset>
					</form>
					<script type="text/javascript">
						document.getElementById('email').style.display ="none";
						document.getElementById('name').style.display ="none";
					</script>
					<p>&copy; Drop Locker, Inc. <?= date('Y');?> ALL RIGHT RESERVED.</p>
				</div>
				<div class="networks-holder">
					<!--
					<h3>Stay with <span>us</span></h3>
					<ul class="social-networks">
						<li><a class="twitter" href="#">twitter</a></li>
						<li><a class="facebook" href="#">facebook</a></li>
						<li><a class="youtube" href="#">youtube</a></li>
						<li><a class="google" href="#">google</a></li>
						<li><a class="rss" href="#">rss</a></li>
					</ul>
					-->
				</div>
			</div>
		</div>
