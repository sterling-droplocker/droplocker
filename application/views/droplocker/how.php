<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drop Locker: How it Works</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'how';
		include ('header.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>How Drop Locker Works</h1>
					<p>
					Drop Locker is the platform that powers locker based delivery across the globe.  Business owners purchase our lockers and software to run their operations.<br />
					<br />
					As a customer of any business "Powered by Drop Locker" you can access your account directly on their website or through droplocker.com.<br />
					<br />
					<h3>Watch a quick video on how the lockers work:</h3>
					<div><object width="250" height="225"><param name="movie" value="https://www.youtube.com/v/2-qChq7VSSs&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/2-qChq7VSSs&hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="286" height="215"></embed></object></div>
					<br />
					To get started using any Drop Locker location:<br />
					<ol>
					<li>Select your region.</li>
					<li>Find the nearest location "Powered by Drop Locker"</li>
					<li>Visit the company's website to register and get started.</li>
					</ol>
					<br />
					<br />

				</div>
				<div class="box-aside">
					<div class="post">
                                            <h3>Partners - <a href="/main/dlpartners">show map</a></h3>
						<?php foreach ($business as $b): ?>
                                                    <?=$b->companyName;?><br />
                                                <?php endforeach;?>
					</div>
					<br />
					<br />
					<div class="post">
						<h3>Want to start your own locker based delivery business?</h3>
						<a class="more" href="/main/business">Read More<a/>
					</div>
				</div>
			</div>
		</div>
		
		<? include ('footer.php'); ?>
	</div>
</body>
</html>