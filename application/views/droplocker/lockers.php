<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drop Locker: Lockers</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'lockers';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Lockers</h1>
					<p>We have developed such a unique locker, specific for the Drop Locker business models, that we actually have a patent on the design.  Because of our special angular design, each locker can be used to hang long dry cleaning items or store large wash & fold orders.<br>
					<br>
					Drop Locker sources our lockers and locks direct from our partners, enabling us to price our lockers below what you would pay to source them yourself.
					<br />
					<br />
					Lockers are available in metal or laminate to best suit your installation:</p>
					<br />
					<br />
				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Get Started Today!</h3>
						<p>Are you sold yet?  The Drop Locker system will have you up and running with 24/7 delivery in no time.  Be the first in your market and get a huge leg up on your competition.<br />
						<br />
						To learn more, please call us at <? echo $phone; ?> or <a href="mailto:<? echo $salesEmail; ?>">email our sales team</a>.</p>
					</div>
				</div>
			</div>
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>