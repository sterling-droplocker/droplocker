<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DropLocker Pricing</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'pricing';
		include ('businessHeader.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h1>Pricing</h1>
					<p>Drop Locker's pricing model is very straightfoward and cost effective. Our customers pay a setup fee, they buy the lockers, and then there is an ongoing monthly fee for our sofware.</p>
					<div class="table-block1">
						<h6>Software Pricing <div style="float:right;">Please <a href="mailto:sales@droplocker.com" style="color: White;">contact us</a></div></h6>
					</div>
				</div>
				<div class="box-aside">
					<!-- <div class="table-block2">
						<h3>Starter Package:<div style="float:right;">$5200</div></h3>
						<table class="table">
							<tr class="active">
								<td class="col1">Startup Fee</td>
								<td class="col2" align="right">$1500&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td class="col1">5 Metal Lockers</td>
								<td class="col2" align="right">$2500&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td class="col1">1 Year Software</td>
								<td class="col2" align="right">$1200&nbsp;&nbsp;</td>
							</tr>
						</table>
					</div> -->
                                        <!--
					<div class="post">
						<h3>Fall Special</h3>
						<p>Until November 1st, anyone who buys 5 lockers, will receive 1 additional locker free.</p>
					</div>
                                        -->
					<br>
					<h3>Other Services Offered:</h3>
					<ul class="list-services">
						<li><a class="bullet1" href="#">Consulting</a></li>
						<li><a class="bullet2" href="#">Training</a></li>
					</ul>
				</div>
			</div>
		</div>
		<? include ('footer.php'); ?>
	</div>
</body>
</html>