<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Drop Locker: Newsletter</title>
	<link media="all" rel="stylesheet" type="text/css" href="/css/droplocker/style.css" />
	<link rel="shortcut icon" href="/images/droplocker_favicon.ico" />
	<script type="text/javascript" src="/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/js/jquery.main.js"></script>
	<script type="text/javascript" src="/js/functions.js"></script>
</head>
<body>
	<div id="wrapper" class="sub">
		<? $active = 'locations';
		include ('header.php'); ?>
		<div id="main">
			<div class="content-block">
				<div class="content">
					<h2>Thank you for joining our newsletter</h2>

				</div>
				<div class="box-aside">
					<div class="post">
						<h3>Want to start your own locker based delivery business?</h3>
						<a class="more" href="/main/business">Read More<a/>
					</div>
				</div>
			</div>
		</div>
		
		<? include ('footer.php'); ?>
	</div>
</body>
</html>