<div style="float: left; display: block; padding-left: 5px; width: 370px;">
<span class="bodyReg">
<strong>Want next day dry cleaning and wash & fold at work?</strong><br>
<br>
<img src="/images/oneCaliforniaMapSmall.jpg" border="0">
<br>
<br>
<strong>We'd like to announce that Laundry Locker is now available at One California Street!
</strong><br>
<br>
Laundry Locker&reg; is changing the way San Francisco does laundry. Now you can have dry cleaning and wash & fold services, with 24 x 7 pickup and delivery only steps from your office!
<br>
<br>
No more running home to catch the dry cleaner, waiting for the delivery van or getting wet in the rain.  We've conveniently setup our lockers in the parking garage across from the attended booth; just follow our signs.  Make sure to sign up on our website first at laundrylocker.com if you're dropping of your clothes.
<br>
<br>
Keep an eye out for our lobby posters.  We'll be hosting some informational meetings soon with drinks, snacks and of course discounts on laundry.
<br>
<br>
We look forward to serving you,
<br>
Laundry Locker</span>
<br>
<br>
</div>
<div class="captionSmall blueBox">
<div style="margin-left: 10px; margin-right: 10px; margin-top: 20px;">
Items placed in a locker before 9am will be returned by 5pm the following business day - Monday thru Friday<br>
<br>
<a href="/main/dryclean" class="whiteLink">dry cleaning pricing</a><br><br>
<a href="/main/washfold" class="whiteLink">wash & fold pricing</a><br>
<br>
<a href="/register" class="whiteLink"><img src="/images/button_newUser.gif" alt="" border="0"></a>
</div></div>
<div class="imageBox" style="margin-top: 2px;">
<img src="/images/pants.JPG" width="273" height="205"/>
</div>