<script type="text/javascript">
<!--//hide
    function help() {
        window.open("bag.php", 'EANITHING', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,resizable=no,copyhistory=no,scrollbars=yes,width=550,height=600');
    }
//-->
</script>
<div class="fluid-container">
    <h1>&nbsp;FAQ's</h1>

<div id="desktop_faqs" class="">

    <div style="margin-top:20px;" class="bodyBig">

        <h2 class="h2Faq">Legal</h2>
        <a href="https://laundrylocker.com/privacy" class="headingGreen">Laundry Locker's privacy policy<br><br></a>
        


        <h2 class="h2Faq">First Time Customers</h2>

        <a href="javascript:showHide(1)" class="headingGreen">I have never used Laundry Locker before, what do I need to do?<br><br></a>
        <div id="div1" style="display:none">Laundry Locker couldn't be more simple to use.  Please see our <a href="/main/howto">how it works</a> page for step by step instructions.<br><br></div>

        <a href="javascript:showHide(2)" class="headingGreen">I don't have a locker in my building. Can I still use your service?<br><br></a>
        <div id="div2" style="display:none">Laundry Locker has hundreds of locations and many ways to use our service.  Please visit our <a href="/main/locations">locations</a> page to see all of our public locations or your can inquire about our <a href="homeDelivery.php">home delivery</a> service.<br><br></div>

        <a href="javascript:showHide(17)" class="headingGreen">Where is the nearest Laundry Locker location?<br><br></a>
        <div id="div17" style="display:none">Most of our locations are located securely inside apartment and office buildings.  We also operate <a href="/main/kiosk">public locations </a>located throughout the city.  If you are not near our public location, or a tenant of the building, you will not be able to use our locker based service.  If your building is over 40 units, you can have your property manager contact us about having lockers installed.<br><br></div>

        <a name="sms"></a>
        <a href="javascript:showHide(7)" class="headingGreen">How do I place my order using Text Messaging?<br><br></a>
        <div id="div7" style="display:none">Simply <strong>text the word LAUNDRY to MOSIO (66746)</strong>.  The system looks up your default address and places an order for you.  If you are at a locker location, you will need to text the locker number too, in the format LAUNDRY [locker #].  e.g. LAUNDRY 675<br><br></div>

        <a href="javascript:showHide(5)" class="headingGreen">Do you guarantee two day service?<br><br></a>
        <div id="div5" style="display:none">We do our best to ensure that your orders get delivered back in a timely manner.  However, we also need to ensure that we properly treat stains and press your clothes.  In most cases, orders are delivered back within two business day, but if one item is held up, we will hold the order until the next day when everything is ready.  Items such as shoe shines, sheets, pillowcases, hand washed items and other specialty items often require an extra day of processing and may delay your order. <br><br></div>

        <a href="javascript:showHide(10)" class="headingGreen">I have some special requirements, what do I need to do?<br><br></a>
        <div id="div10" style="display:none">Would you like us to use fabric softer, dry your clothes on low, use bleach or provide other special service?  No problem, just enter your requirements into your profile and we'll follow your instructions.  We may charge a small fee for some of these services.  You can <a href="mailto:sales@laundrylocker.com">send us an email</a> if you have a question about a specific request.<br><br></div>

        <a href="javascript:showHide(11)" class="headingGreen">I put my laundry in a locker, how do I pay for it?<br><br></a>
        <div id="div11" style="display:none">As soon as your order is picked up you will receive an email detailing your items and requesting payment.  Simply login to the website, select your order and pay with your credit card.  If you select the convenient automatic payment option, we will automatically charge your card for all future orders.  Please make payments promptly so we can ensure your order gets returned on time.<br><br></div>

        <a href="javascript:showHide(13)" class="headingGreen">Is my laundry safe?<br><br></a>
        <div id="div13" style="display:none">Absolutely!  Remember, only you and a Laundry Locker employee has the key to your locker.  If someone else wants to pay for your laundry, that is fine, but only the holder of the key can retrieve it.   If you like, you can easily give your key to your wife, roommate, etc. and have them pick up your laundry for you.<br><br></div>

        <a href="javascript:showHide(14)" class="headingGreen">How do I get a laundry bag?<br><br></a>
        <div id="div14" style="display:none">The first time you do laundry with us, your clothes will be returned in your <a href="javascript:help()">personal laundry bag</a>.  This bag is yours to use and keep.  Each time we receive laundry in your bag, we will know it's from you.  Please don't give your laundry bag to someone else or we may mistakenly charge your account.  If we notice that your bag is getting worn, we will replace it automatically for you.<br><br></div>

        <a href="javascript:showHide(15)" class="headingGreen">I would like Laundry Locker to automatically charge my credit card, but is it safe?<br><br></a>
        <div id="div15" style="display:none">Absolutely!  Laundry Locker has partnered with Verisign, the premier online payment processor, for this single reason.  When you enter your credit card information, we don't actually keep your credit card anywhere in our system.  We keep a reference to your last transaction and when you have a new order, we make a request to Verisign to charge you the same way you were charged last time.  Therefore, we have absolutely no trace of your credit card number anywhere in our system.<br><br></div>

        <a href="javascript:showHide(29)" class="headingGreen">It has been a couple of days, where is my order?<br><br></a>
        <div id="div29" style="display:none">Our standard turnaround time is two business days for any orders dropped off by 9am. When we pickup your order and when we deliver it back, you will receive an email confirmation.  If you didn't receive the email, we either did not pickup your order, or we picked it up and did not know it was yours.  This usually happens if you forget to place an order or enter the incorrect locker number.<br><br>Don't worry!  We will have this straigtend out in no time and it is a very easy issue to fix.  We just need you to call customer service at (415) 255-7500, letting us know your name, locker number and contact info so we can get it straightened out and have your order returned back to your locker quickly.<br><br></div>

        
        <h2 class="h2Faq">Dry Cleaning</h2>
        <p class="hr"><hr /></p>

        <a href="javascript:showHide(4)" class="headingGreen">I asked to have my shirts laundered, why was I charged the price for a blouse?<br><br></a>
        <div id="div4" style="display:none">We are able to process laundered shirts at discount pricing because there is special machinery designed for processing standard sized button down shirts with cuffs and collars.  However, shirts with special pleats, decorations or any other non-standard design elements, or shirts that are too small to fit on the machines need to be pressed by hand and will be charged the blouse price.<br><br></div>

        <a href="javascript:showHide(3)" class="headingGreen" class="headingGreen">I have shirts that I want laundered and pressed, what locker should I put them in?<br><br></a>
        <div id="div3" style="display:none">Laundered shirts should go in the dry cleaning locker.  This will allow us to return them to you on a hanger.  We will clean the items in the dry cleaning locker in accordance with the guidelines recommended by the manufacturer unless you specify otherwise.  Meaning if a shirt is supposed to be laundered, we will launder, not dry clean, it.<br><br></div>


        <h2 class="h2Faq">Wash & Fold</h2>

        <a href="javascript:showHide(51)" class="headingGreen">Do you have monthly laundry plans?<br><br></a>
        <div id="div51" style="display:none">We sure do, and they can save you lots of money!  To see our laundry plans, <a href="/main/plans">click here.</a><br><br></div>

        <a href="javascript:showHide(6)" class="headingGreen">Can you hang dry items?<br><br></a>
        <div id="div6" style="display:none">With wash & fold, unfortunately we do not have the space or time to hang dry items.  We can return them wet for you to hang dry at home or we can dry your order on low heat.  We suggest you send these items in for dry cleaning and we will clean them according to the manufacturer's specifications.  If the item specifically requires hang drying, we will hang dry them, though there is an extra cost for this and they may require an extra day to service.<br><br></div>

        <a href="javascript:showHide(8)" class="headingGreen">If I have a wash & fold order that weighs only 5 pounds, and some dry cleaning, will I be charged the 12 lb minimum wash & fold charge?<br><br></a>
        <div id="div8" style="display:none">Since we wash every customer`s clothes separately, each order is separated into colors and lights to make at least two loads (2 washers & 2 dryers) and handled by our experienced team. Hence the reason for a 12 lb minimum order size. It is fine to send in less than 12 pounds, and we will charge the 12 pound minimum for the order or deduct 12 pounds from your laundry plan balance.  This is a service that is handled separately from any dry cleaning order, and so we must charge the minimum.<br><br></div>

        <a href="javascript:showHide(9)" class="headingGreen">How much does my laundry weigh?<br><br></a>
        <div id="div9" style="display:none">Our average order is 15 to 20 pounds.  That is about two weeks worth of dirty laundry.  A full hamper is about 20 pounds.  If it is overflowing, it's about 30 pounds.  If you have a scale at home, you can weigh yourself, then weigh yourself holding your laundry and the difference is the approximate weight.<br><br></div>

        <a href="javascript:showHide(28)" class="headingGreen">Why does my Wash and Fold order say 23 items?  Isn't it supposed to be weighed? <br><br></a>
        <div id="div28" style="display:none">Yes, wash and fold orders are weighed by the pound. In our system, the word "items" equates to pounds.  In this case your order would weigh 23 pounds.<br><br></div>

        <a href="javascript:showHide(16)" class="headingGreen">I have sensitive skin and use a special detergent, can Laundry Locker do my laundry?<br><br></a>
        <div id="div16" style="display:none">Laundry Locker offers a variety of detergents to choose from.  As a registered customer, under the My Preferences section, you can configure your detergent, fabric softner, dryer sheets and other washing preferences.  However, we do no allow customers to provide us with their own products.  Because we are a delivery service, the transportation of the liquid is too risky to your clothes and the clothes of other customers.  If there is a product that you would like us to carry that you don't see currently available on our website, please 
            <?php if (isset($business_id)): ?>
            <a href="mailto:<?= get_business_meta($business_id, 'customerSupport'); ?>">send us an email</a>
            <?php endif; ?>
            and we will try our best to accommodate you.<br><br>
        </div>


        <h2 class="h2Faq">Other Questions</h2>

        <a href="javascript:showHide(32)" class="headingGreen">Where can I view your terms and conditions?<br><br></a>
        <div id="div32" style="display:none">When you register as a new customer we require you to agree to our terms and conditions.  You can view our compete terms and conditions by <a href="terms">clicking here.</a><br><br></div>

        <a href="javascript:showHide(12)" class="headingGreen">How do I change my credit card?<br><br></a>
        <div id="div12" style="display:none">If you would like to change your credit card, you can go to "My Preferences".  Select the "Credit Card" tab and enter your new credit card information.<br><br></div>

        <a href="javascript:showHide(18)" class="headingGreen">What if I am going out of town or can not pick my laundry up right away?<br><br></a>
        <div id="div18" style="display:none">Since we provide quick service, we recommend that you wait to place your clothes in the locker when you know you will be able to pick them up.  This way your neighbors and other customers can use the locker while you are away.  This helps to keep our prices low.<br><br></div>

        <a href="javascript:showHide(19)" class="headingGreen">What if the lockers I use are full?<br><br></a>
        <div id="div19" style="display:none">Laundry Locker does our best to monitor locker use to ensure that there are always enough free lockers.  If we find that a location is constantly full, we will add another locker - if the landlord / business owner approves.  We apologize for the inconvenience and hope that you use one our other many drop off locations throughout San Francisco.<br><br></div>

        <a href="javascript:showHide(20)" class="headingGreen">What if all my laundry doesn't fit in a locker?<br><br></a>
        <div id="div20" style="display:none">Our lockers are designed to hold one to two weeks worth of laundry.  If you can not fit all of your laundry into one locker then feel free to use two lockers.<br><br></div>

        <a href="javascript:showHide(21)" class="headingGreen">What if I lose my key?<br><br></a>
        <div id="div21" style="display:none">If you lose your key, please email or call us to arrange for a way for us to get your order back to you.  We will charge you a $25 lost key fee.  If you find the key, please let us know and we will refund $10 back to you upon receiving the found key.<br><br></div>

        <a href="javascript:showHide(22)" class="headingGreen">What if I don't pay?<br><br></a>
        <div id="div22" style="display:none">If you don't pay within 2 days of dropping off your clothes, the lock on the unit will be changed so that other customers can use the locker.  We will store your clean clothes at our facility and you can go to the <a href="myAccount.php">my account</a> section of our website to schedule a delivery date.  When your scheduled delivery date arrives, we will place your clothes back in the locker and install your lock back on the locker.  You can then simply pickup your clothes at your earliest convenience using your key.<br><br>
            If we don't receive payment within 30 days, your clothing will be donated to charity.  We strongly suggest you setup automatic payment to help prevent this.<br><br></div>

        <a href="javascript:showHide(27)" class="headingGreen">What happens if an item is damaged or lost?<br><br></a>
        <div id="div27" style="display:none">We take every precaution to ensure your items are not damaged or lost. In the rare circumstance that something does happen, we have a very fair claims policy.  To view all of our terms and conditions, <a href="terms">click here</a>.<br><br></div>

        <a href="javascript:showHide(23)" class="headingGreen">What if I don't have Internet access?<br><br></a>
        <div id="div23" style="display:none">If you don't have Internet access, you can call our support center and we will process your order over the telephone.  All we need is your Location, Locker Number, and Credit Card Number.  As soon as we receive your payment, and your clothes are cleaned, we will return them to the locker.<br><br></div>

        <a href="javascript:showHide(24)" class="headingGreen">I am moving out of town.  What should I do with my bags?<br><br></a>
        <div id="div24" style="display:none">We're sorry to see you go.  We can't reuse the bags for other customers, so they are yours to keep.  Please make sure you <strong>destroy the tags on the bags</strong> so that no one else can use your account.<br><br></div>

        <a href="javascript:showHide(25)" class="headingGreen">What if I don't have a credit card?<br><br></a>
        <div id="div25" style="display:none">Currently Laundry Locker only accepts payment with Credit Card.  Most ATM cards will work as well as long as they have a VISA or MasterCard logo on them.<br><br></div>

        <a href="javascript:showHide(26)" class="headingGreen">Can I store my luggage or other items in a Laundry Locker drop-off unit?<br><br></a>
        <div id="div26" style="display:none">All Laundry Locker drop-off locations are emptied several times a day.  If we find any items that Laundry Locker doesn't service we will store the items for several days and change the lock on the unit.  If you would like to reclaim your property, please contact our customer service department.<br><br></div>

    </div>
</div>

</div>