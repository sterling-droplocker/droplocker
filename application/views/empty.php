<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title><?= $title ?></title>
      <?= $javascript?>
      <?= $style?>
      <?= $_styles ?>

   </head>
   <body>

       <?= $content ?>
   </body>

</html>