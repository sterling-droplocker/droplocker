<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.9&sensor=false&key=<?=get_default_gmaps_api_key();?>"></script>
<script src="/js/StyledMarker.js" type="text/javascript"></script>
<div id="map" style="display:none"></div>
<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/howitworks/hero-24hr.jpg" class="howto_hero_pic" />

            <h1>24 hour public locations</h1>
            <div class="visible-phone" style="text-align: center">
                <?= howNav('publiclocations24hr'); ?>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-public-store.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">1. Swipe to enter</h2>
                        <p class="bodyBig concierge_p">
                            Register a <a href="/register" class="green" target="_blank">credit card here</a> to be permitted 24/7 access to all 7 stores.

                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-location-drop.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">2. Drop off and place order</h2>
                        <p class="bodyBig concierge_p">
                            Place your lanundry in any unlocked locker. Enter any 4-digit code to lock the locker.<br />
                            Place an order via our Android app or iPhone app or on our website. Select your address and locker number.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-24hr-pickup.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">3. Pickup</h2>
                        <p class="bodyBig concierge_p">
                            We'll send you the locker number and code to open the locker and retrieve your clean order. Enjoy!
                        </p>
                    </div>
                </div>
            </div>


        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= howNav('publiclocations24hr'); ?>
            </div>



            <div class="calloutBanner calloutRight" style="height: auto; margin-top: 20px">
                <h3>Locations</h3><br />

                <ul id="locationTablePublic" class="unstyled">

                </ul>

                <p class="bodyBigGray">



                    We now pick up and deliver from our public stores on the weekends.

                    Orders picked up Friday will be returned on Saturday. Orders picked up on Sunday will be returned on Monday.

                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var count = 1;
    var latlng = new google.maps.LatLng(37.800000, -122.357221);

    var myOptions = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), myOptions);
    var infowindow = new google.maps.InfoWindow();
    function add_marker(lat, lon, locationID, address, name, count, info, color) {
        latlng = new google.maps.LatLng(lat, lon);
        //bounds.extend(latlng);
        color = '#' + color;

        var styleIcon = new StyledIcon(StyledIconTypes.MARKER, {color: color, text: count});
        var marker = new StyledMarker({styleIcon: styleIcon, position: latlng, map: map, content: count});


        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(info);
            infowindow.open(map, marker);
        });
    }
    ;


    var json = <?= $json_locations ?>;

    $.each(json, function (i, item) {
        var lat = item.lat;
        var lon = item.lon;
        var color = 'A4D423';
        //item.abbreviation == 'k' ? '0099ff' : 'A4D423';
        var target = '#locationTablePublic';

        if (item.abbreviation == 'k') {
            add_marker(lat, lon, item.locationID, item.address, 'active', count.toString(), item.info, color);
            var link = '';
            if (item.locationID == 483) {
                link = "<a href='/'>";
            } else if (item.locationID == 341) {
                link = '<a href="/main/clay" target="_blank">';
            } else if (item.locationID == 186) {
                link = '<a href="main/onecalifornia" target="_blank">';
            } else {
                link = '<a href="https://maps.google.com/maps?q=' + item.address + ' ' + item.city + ', ' + item.state + '" target="_blank">';
            }

            var city = '';
            if (item.city != 'San Francisco') {
                city = ", " + item.city;
            }
            $(target).append("<li class='bodyRegItemMap'>" + item.address + " - " + link +  "Map</a></li>");
            count = count + 1;
        }

    });



</script>