<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <br>
            <h1>Business Accounts</h1>
            <div class="visible-phone" style="text-align: center">
            <?= servicesNav('business');?>
            </div>
            <br>
            <span class="bodyReg">Are you a small to mid-sized San Francisco business looking for an economical and efficient way to get your towels, sheets, pillowcases or other linens laundered?  Then Laundry Locker's wholesale laundry service is the perfect solution for you!  We service spas, hotels, furnished apartments, schools, chiropractors and other business with the highest quality product at great prices!
            </span>
            <br><br>
            <span class="bodyBold">Professional Service</span><br>
            <span class="bodyReg">We have a dedicated plant, exclusively focused on handling high quality linens, towels and blankets.  Your laundry will be:<br><br>
                <div style="margin-left: 5px;">
                    - Professionally pressed and folded (sheets and pillowcases)<br>
                    - Sanitized to meet airline industry standards<br>
                    - Delivered in heat sealed packaging for safe storage<br>
                    - Washed in specially formulated hypo-allergenic detergent
                </div>
            </span>
            <br>
            <span class="bodyBold">Free Pickup and Delivery</span><br>
            <span class="bodyReg">Laundry Locker will pickup and delivery your laundry free of charge anywhere in our <a href="/main/locations">service area</a>.  Customers are either setup on a regular delivery schedule or on an as-needed basis.  Our standard turnaround is only two days!
            </span>
            <br><br>
            <span class="bodyBold">Extremely Competitive Pricing</span><br>
            <span class="bodyReg">We charge by the piece, not the pound.  Our linens are carefully ironed and folded, yet our prices are more competitive than the competition.  And unlike our competition, there are no extra charges.  Stop bringing your items to the local laundromat and give us a try.  Please call (415) 255-7500 or <a href="mailto:sales@laundrylocker.com">email us</a> to schedule a free linen consultation.
            </span>
            <br><br>
            <span class="bodyBold">Easy Billing</span><br>
            <span class="bodyReg">Laundry Locker accepts credit card for all transactions.  That's right, no payments for 30 days and free airline miles.  What could be more convenient.
            </span>
            <br><br>
            <span class="bodyBold">Services</span><br>
            <span class="bodyReg">
                Towels - Sheets (flat and fitted) - Pillowcases - Duvet Covers - Blankets - Tablecloths - Napkins - and much more...
            </span>
            <br><br>
            <h2>Please call <?= $this->business->phone ?> or <a href="mailto:<?= $this->business->email ?>" class="greenLink">email us</a> to schedule a free consultation.</h2>
        </div>

        <div class="span4">
            <div class="hidden-phone">
            <?= servicesNav('business'); ?>
            </div>
           
            <div class="dryCleanNote">
            <div class="graySmall">
                Please call (415) 255-7500 or <a href="mailto:<?= $this->business->email ?>" class="greenLink">email us</a> to schedule a free consultation.<br><br>
                Let us show you how Laundry Locker can deliver a better product at a better price.
            </div>
            </div>
        </div>

    </div>
</div>