<div class="">
    <div class="row-fluid">
        <div class='span8 howto_content'>
            <div class="bodyBig" style="margin-top: 30px; float: left;">
                <h1>Our Delivery Services</h1><br>
                Laundry Locker's services can be used many ways.  Choose the service that works best for you:
                <br><br>
                <h2>Laundry Locker Kiosks (Retail Stores)</h2>
                Laundry Locker operates several kiosks throughout San Francisco.  Our kiosks are available to our registered customers 24 hours a day, seven days a week.  To locate a kiosk close to you, <a href="/main/kiosk"  class="greenLink">click here</a>.
                <br><br>
                <h2>Lockers in Buildings and other Public locations</h2>
                Laundry Locker has lockers located in hundreds of apartment buildings, parking garages, laundromats and office buildings throughout San Francisco.  If lockers are in your building, simply follow the <a href="/main/howto" class="greenLink">"How It Works"</a> guide to get started.
                <br><br>
                <h2>Concierge Buildings</h2>
                Laundry Locker services almost every concierge building in San Francisco.  Most locations are already setup in our system. To get started:<br>
                <ol>
                    <li><a href="/main/register" class="greenLink">Register</a> as a new customer</li>
                    <li>Leave your laundry with the concierge and put a note inside with your <strong>name and phone number</strong>. </li>
                    <li>Place an order on our website.</li>
                    <li>- If your address and/or unit number is not listed, please call (415.255.7500) or <a href="mailto:<?= get_business_meta($business_id, 'customerSupport') ?>">email</a> customer service.</li>
                </ol>
                <h2>Corner Stores and Other Public Locations</h2>
                Laundry Locker is conveniently available at many neighborhood corner stores.  To use the service at your local corner store simply leave your laundry with the front desk and then place an order on our website or via Text. <br>
                <br>
                <a href="/main/publiclocations" class="greenLink">Click here</a> to see a list of all of our public locations
                <br><br>
                <h2>@ Work</h2>
                Laundry Locker services many office buildings throughout San Francisco.  If Laundry Locker is availble at your office, simply place your order in the designated area, register on our website and place an oder.<br>
                <br>
                If you would like us to service your office, <a href="/main/atwork" class="greenLink">click here.</a>
                <br>
                <br>

            </div>
        </div>
        <div class='span4'>
            <?php
            echo howNav('work');
            echo callOut_25Percent();
            ?>
        </div>
    </div>
</div>