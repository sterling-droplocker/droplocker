<?php 
//The following code determines if the facebook buttons should open in a new tab or not, if applicable to the business.
$fb_target = "_self";
if ($business->open_facebook_register_in_new_tab) {
    $fb_target = "_blank";
}

if (!$locations) {
    $locations[''] = get_translation_for_view("missing_location", "Location %location_id% not found for business.", array('location_id' => $location_id));
    $location_id = '';
}

if (!$owners) {
    $owners[''] = get_translation_for_view("missing_owner", "Representative %owner_id% not found for business.", array('owner_id' => $owner_id));
    $owner_id = '';
}

function lb_error_alert($me)
{
    $error = $me->session->flashdata('status_message');
    if ($error) {
?>
        <div class="row">
            <div class="alert alert-danger col-12 text-center" role="alert">
            <?= $error ?>
            </div>
        </div>
<?php 
    }
}

function lb_form_error_alert($field) {
    $error = form_error($field);
    if ($error) {
?>
    <div class="alert alert-danger col-12 text-center" role="alert">
    <?= $error ?>
    </div>
<?php 
    }
}
?>
<!doctype html>
<html class="h-100"> 
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= get_translation_for_view("title", "Quick Register") ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=1024">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="/css/quick_register.css">
    <link rel="stylesheet" type="text/css" href="/css/<?= $business_subpath ?>/quick_register.css">

    <?= $_scripts ?>
    <?= $javascript?>
    <?= $style?>
    <?= $_styles ?>

</head>


<body class="h-100 main_quick_register">
<div class="page-wrapper h-100 px-5">
    <div class="row header">
        <div class="col text-center">
            <a target='_blank' href="http://<?= $business->website_url ?>/">
                <div class="logo">
                    <img class="col-md-auto" src="<?= $business->image ?>" />
                </div>
            </a>
        </div>
    </div>
    <div class="content">
        <?= lb_error_alert($this) ?>
        <form method="post">
            <div class="row">
                <div class="col text-center">
                    <p class="text-danger"><?php echo form_error('business_id'); ?></p>
                </div>
            </div>
            <?= form_hidden('business_id', $business_id) ?>
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="firstName"><?=get_translation_for_view('first_name', 'First Name')?>*</label>
                        <?= form_input(array("id" => "firstName", "name" => "firstName"), set_value('firstName'), 'required class="form-control col"') ?>
                        <?= lb_form_error_alert("firstName") ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="lastName"><?=get_translation_for_view("last_name", "Last Name")?>*</label>
                        <?= form_input(array("id" => "lastName", "name" => "lastName"), set_value('lastName'), 'required class="form-control col"') ?>
                        <?= lb_form_error_alert("lastName") ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <label class="col-md-2" for="phone"><?=get_translation_for_view("phone", "Phone Number")?>*</label>
                    <?= form_input(array("id" => "phone", "name" => "phone"), set_value('phone'), 'required class="form-control col-8"') ?>
                    
                    <div class="form-check ml-2">
                        <?= form_checkbox(array("id" => "sms_copy", "name" => "sms_copy", "checked" => true), true, set_value('sms_copy'), 'class="form-check-input"') ?>
                        <label class="form-check-label" for="sms_copy">
                            <?=get_translation_for_view("sms_text", "SMS / Text")?>
                        </label>
                    </div>
                    
                    <?= lb_form_error_alert("phone") ?>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="location_id"><?=get_translation_for_view("address", "Address")?>*</label>
                        <?= form_dropdown('location_id', $locations, set_value('location_id', $location_id), 'required class="form-control col"' . ($location_id ? ' readonly' : '') ) ?>
                        <i class="floating-icon readonly fa fa-2x fa-lock <?= ($location_id ? '' : ' d-none') ?>"></i>
                        <?= lb_form_error_alert("location_id") ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="address2"><?=get_translation_for_view("address_subtitle", "Unit / Apt #")?></label>
                        <?= form_input(array("id" => "address2", "name" => "address2"), set_value('address2'), 'class="form-control col"') ?>
                        <?= lb_form_error_alert("address2") ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="email"><?=get_translation_for_view('email', 'Email')?>*</label>
                        <?= form_input(array("id" => "email", "name" => "email"), set_value('email'), 'required class="form-control col"') ?>
                        <?= lb_form_error_alert("email") ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="email_confirm"><?=get_translation_for_view("confirm", "Confirm")?>*</label>
                        <?= form_input(array("id" => "email_confirm", "name" => "email_confirm"), set_value('email_confirm'), 'required class="form-control col" placeholder="' . get_translation_for_view('email', 'Email') . '"') ?>
                        <?= lb_form_error_alert("email_confirm") ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="password"><?=get_translation_for_view('password', 'Password')?>*</label>
                        <?= form_input(array("id" => "password", "name" => "password", "type" => "password"), '', 'required class="form-control col"') ?>
                        <?= lb_form_error_alert("password") ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="password_confirm"><?=get_translation_for_view("confirm", "Confirm")?>*</label>
                        <?= form_input(array("id" => "password_confirm", "name" => "password_confirm", "type" => "password"), '', 'required class="form-control col" placeholder="' . get_translation_for_view('password', 'Password') . '"') ?>
                        <?= lb_form_error_alert("password_confirm") ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <div class="form-row">
                            <label class="col-md-2" for="promocode"><?=get_translation_for_view("promocode", "Promotion code")?></label>
                            <?= form_input(array("id" => "promocode", "name" => "promocode"), set_value('promocode', $promocode), 'class="form-control col"' . ($promocode ? ' readonly' : '')) ?>
                            <i class="floating-icon readonly fa fa-2x fa-lock <?= ($promocode ? '' : ' d-none') ?>"></i>
                            <?= lb_form_error_alert("promocode") ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <label class="col-md-2" for="terms"></label>
                    <div class="form-check">
                        <?= form_checkbox(array("id" => "terms", "name" => "terms"), true, set_value('terms'), 'required class="form-check-input"') ?>
                        <label class="form-check-label" for="terms">
                            * <?=get_translation_for_view("accept_terms", "I agree to the %company_name% <a target='_blank' href='http://%current_url%/main/terms_and_conditions/%business_id%'>terms and conditions</a>", array('company_name' => $business->companyName, 'current_url' => $_SERVER['SERVER_NAME'], 'business_id' => $business_id,))?>
                        </label>
                        <?= lb_form_error_alert("terms") ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row justify-content-around">
                    <button type="submit" name="quick_register_submit" value="true" class="btn-secondary col-auto offset-md-2 text-uppercase register-button">
                        <?=get_translation_for_view("register", "Register")?>
                    </button>
                    <?php if (false && $business->facebook_api_key && $business->facebook_secret_key): ?>
                        <a name="register_with_facebook" id="login_with_facebook" target="<?=$fb_target?>" style="background: url('/images/icons/facebook.png') no-repeat scroll 8px center #F1F1F1; font-size: 14px; padding: 8px 8px 8px 22px; text-decoration: none;" href="<?= $facebook_url ?>">&nbsp;&nbsp; <?=get_translation_for_view("continue_facebook", "Continue with Facebook")?> </a>
                    <?php endif; ?>
                </div>
            </div>
            
            <div class="form-row representative mt-3">
                <div class="form-group col-md-6">
                    <div class="form-row">
                        <label class="col-md-4" for="owner_id"><?=get_translation_for_view('representative', 'Representative')?>*</label>
                        <?= form_dropdown('owner_id', $owners, set_value('owner_id', $owner_id), 'required class="form-control col"' . ($owner_id ? ' readonly' : '') ) ?>
                        <i class="floating-icon readonly fa fa-2x fa-lock <?= ($location_id ? '' : ' d-none') ?>"></i>
                        <?= lb_form_error_alert("owner_id") ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
</script>


</body>
</html>
