<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="visible-phone" style="text-align: center">
                <?= servicesNav('package'); ?>
            </div>
            <div class="row-fluid">
                <img src="/images/Banner_R2.jpg" style="width: 100%" />
            </div>
            <h1>Package Solutions by the Locker Experts!</h1>
            <br>

            <span class="bodyReg">
                Are you a property manager overwhelmed by the amount of packages getting delivered for your residents?
                We can help. Laundry Locker's sister company, <a href="http://luxerone.com/">Luxer One</a>, offers
                package locker and package room solutions that accept 100% of packages.
            </span>

            <br>
            <br>

            <div class="row-fluid" style="margin-bottom: 20px;">
                <a class="various iframe span6" href="https://www.youtube.com/embed/7GEwWlRAN2g?autoplay=1">
                    <img src="/images/Lockers.jpg" style="width: 100%" />
                </a>
                <a class="various iframe span6 pull-right" href="https://www.youtube.com/embed/r_nXU121l9M?autoplay=1">
                    <img src="/images/Room.jpg" style="width: 100%" />
                </a>
            </div>

            <div class="services">
                <div class="row-fluid" style="margin-bottom: 20px;">
                    <img src="/images/1-icon.jpg" class="span2" />
                    <div class="span10">
                        <strong class="grayReg">No more interruptions:</strong>
                        <span class="grayReg">Property staff is no longer involved in the process of accepting packages for
                            residents! Carriers deliver directly to lockers and residents pick up packages at their convenience.
                        </span>
                    </div>
                </div>

                <div class="row-fluid" style="margin-bottom: 20px;">
                    <img src="/images/2-icon.jpg" class="span2" />
                    <div class="span10">
                        <strong class="grayReg">Safe &amp; secure:</strong>
                        <span class="grayReg">With video surveillance and photos of every package label, Luxer One is the
                            safest way to automate your package management.
                        </span>
                    </div>
                </div>

                <div class="row-fluid" style="margin-bottom: 20px;">
                    <img src="/images/3-icon.jpg" class="span2" />
                    <div class="span10">
                        <strong class="grayReg">Hot &amp; new amenity:</strong>
                        <span class="grayReg">Convenient 24/7 package retrieval is one of the most requested amenities
                            by residents.
                        </span>
                    </div>
                </div>

                <div class="row-fluid" style="margin-bottom: 20px;">
                    <img src="/images/4-icon.jpg" class="span2" />
                    <div class="span10">
                        <strong class="grayReg">24/7 Support:</strong>
                        <span class="grayReg">Our support team is available around the clock to help any resident, carrier, or
                            property issues.
                        </span>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <h1 class="grayHuge">How it Works</h1>
                    <iframe src="https://www.youtube.com/embed/7GEwWlRAN2g" frameborder="0" allowfullscreen class="fluid-video"></iframe>
                </div>
            </div>
            <br>
        </div>
        <div class="span4">
            <div class="hidden-phone">
                <?= servicesNav('package'); ?>
            </div>
            <br>
        </div>
    </div>

    <h1 class="grayHuge">As seen as</h1>
    <div class="row-fluid media-icons">
        <img src="/images/CNBC.jpg" class="span1" />
        <img src="/images/TODAY.jpg" class="span1" />
        <img src="/images/NBC.jpg" class="span1" />
        <img src="/images/WSJ.jpg" class="span1" />
        <img src="/images/MHN.jpg" class="span1" />
        <img src="/images/NAA.jpg" class="span1" />
        <img src="/images/MFE.jpg" class="span1" />
    </div>
</div>