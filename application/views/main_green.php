<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig" style="margin-bottom: 20px">
                <h1>Green Cleaning</h1><br>
                <div class="visible-phone" style="text-align: center">
                    <?= aboutNav('green'); ?>
                </div>
                No more chemical smell, less wear and tear on your clothes, brighter colors, all with a reduced impact on the environment!
                <br>
                <br>
                Concerned about your impact on the environment? Do your part to keep our land, water, air, and yes, YOUR CLOTHES clean! Laundry Locker recently completed construction on our state of the art, 10,000 sqft, Eco-Friendly Dry Cleaning facility - the largest dry cleaning facility in San Francisco.
                <br>
                <br>
                We here at Laundry Locker have been doing our part to help the environment since our beginnings. With our ultra efficient washers and dryers, re-usable laundry bags, a paperless office,  and recycling of all packaging and hangers, we have consistently strived to reduce our carbon footprint.
                <br>
                <br>
                <br><img src="/images/GE_OnWhite.jpg" alt="" border="0" align="left">
                Laundry Locker services thousands of customers throughout San Francisco and by converting to <a href="http://www.greenearthcleaning.com/">GreenEarth</a>, the latest in Eco-Friendly Dry Cleaning technology, we will greatly reduce the use of PERC throughout the region. GreenEarth is a sand based product resulting in Odor-Free clothes that last longer, feel softer and stay brighter.  Laundry Locker is now offering boutique cleaning, that is found only at the most high-end cleaners, at a fraction of the cost.
                <br>
                <br>
                Having constructed our facility in San Francisco, we're not only doing our part to help the environment, but we are doing what we can to help the San Francisco community.  By being a locally owned business, we are employing San Franciscan's and keeping your money in the city you live in.
                <br>
                <br>
                With our many locations throughout the city and our convenient 24/7 drop off and pick up, we hope that you too, will do your part to reduce your effect on the environment by choosing Laundry Locker with GreenEarth for all of your wash and fold and dry cleaning needs! We are confident that you will find the quality of our new cleaning methods far superior to those of our competitors and even of our past track record!
            </div>
        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= aboutNav('green'); ?>
            </div>
        </div>
    </div>
</div>