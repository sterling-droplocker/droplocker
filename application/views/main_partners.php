<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8" class="bodyBig">
            <h1>Our Partners</h1><br />
            <div class="visible-phone" style="text-align: center">
                <?= aboutNav('partners'); ?>
            </div>

            <h2 class="partnersType">Discounts</h2>
            <div class="hrPricing" ></div>
            <div class="row-fluid partnersRow">
                <div class="span12">
                    <p><img src="/images/logo_zipcar.jpg" alt="" border="0"></p>
                    <h3><a href=http://www.zipcar.com/laundrylocker target=_blank class="headingGreen">Zipcar</a></h3>
                    <p class="bodyBig">Laundry Locker customers can join Zipcar for only $25 per year.</p>
                </div>
            </div>

            <div class="row-fluid partnersRow">
                <p><img src="/images/logo_lombardi.gif" alt="" border="0"></p>

                <h3><a href="http://www.lombardisports.com/" target=_blank class="headingGreen">Lombardi Sports</a></h3>
                <p class="bodyBig">Laundry Locker customers get 15% off at Lombardi Sports. <a href="/account/main/lombardiCoupon" class="greenLink">Click here</a> to print out a special coupon just for our customers.</p>
            </div>


            <h2 class="partnersType">Professional Affiliations</h2>
            <div class="hrPricing" ></div>
            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img src="/images/dli-logo.jpg" alt="" border="0">

                    <h3><a href="http://www.ifi.org/" target=_blank class="headingGreen">Drycleaning and Laundry Institute</a></h3>

                    <p class="bodyBig">DLI has been the premier international trade association for garment care professionals since 1883</p>

                </div>
            </div>
            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img src="/images/ccalogo.gif" alt="" width="151" height="150" border="0">

                    <h3><a href="http://www.calcleaners.com/" target=_blank class="headingGreen">California Cleaners Assoication</a></h3>

                    <p class="bodyBig">The California Cleaners Association (CCA) is the statewide, professional trade association for California's best dry cleaners and the companies who serve them.</p>

                </div>
            </div>


            <h2 class="partnersType">Service Providers</h2>
            <div class="hrPricing" ></div>
            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img src="/images/GE_OnWhite.jpg" border="0">             
                    <h3><a href="http://www.greenearthcleaning.com/" target=_blank class="headingGreen">GreenEarth Cleaning</a></h3>
                    <p class="bodyBig"> For decades, customers and dry cleaners had no real choice but to clean clothes in harsh, petrochemical solvents with known health and environmental risks. GreenEarth is an environmentally safe dry cleaning process that replaces petroleum-based solvents with liquid silicone, a gentle solution made from one of the earth's safest and most abundant natural resources: silica, or sand.</p>
                </div>
            </div>

            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img src="/images/logo_vaska.jpeg" alt="" width="250" height="70" border="0">              
                    <h3><a href="http://www.vaskahome.com/" target=_blank class="headingGreen">Vaska Detergents</a></h3>
                    <p class="bodyBig">Whether you're washing your high-tech workout clothing or your toddler's favorite blanket, we know that getting your laundry to look, feel, and smell clean without using chemicals that harm fabrics, people, or the planet is important to you. That's why we use Vaska laundry products' for people like you, who want exceptional textile care, but without the human and environmental costs</p>
                </div>
            </div>

            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img src="/images/logo_kndesign.gif" alt="" border="0">            
                    <h3><a href="http://www.kndesign.us/" target=_blank class="headingGreen">Kn Design</a></h3>
                    <p class="bodyBig">Branding design and marketing firm providing identity, naming, branding, collateral, packaging and interactive solutions for the premium beverage, retail, financial, telecom, technology, arts and music industries.</p>
                </div>
            </div>

            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img src="/images/withice.gif" alt="" width="169" height="94" border="0">               
                    <h3><a href="http://www.withice.com/" target=_blank class="headingGreen">withice.com</a></h3>
                    <p class="bodyBig">A collection of refugees from advertising agencies, graphic design firms, web development and new media companies.</p>
                </div>
            </div>

            <div class="row-fluid partnersRow">
                <div class="span12">
                    <img border='0' src="https://www.queensboro.com/affiliates/images/aff_box.gif" alt="Custom-embroidered logo shirts and apparel by Queensboro">           
                    <h3><a href="http://www.queensboro.com/index.html?ref_id=516385" target="_blank" class="headingGreen">Queensboro Shirt Company</a></h3>
                    <p class="bodyBig">High quality custom embroidered shirts and apparel at very reasonable prices.</p>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="hidden-phone">
                <?= aboutNav('partners'); ?>
            </div>
        </div>
    </div>
</div>

