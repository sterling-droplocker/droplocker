<!-- The following code is undocumented and unreadable *. -->

<!-- If the intended entry is to be a header, then the menu item expects the following parameter:
    header: text that represents the title for the header
If the intended entry is to be a clickable menu item, then the menu item expects the following parameters:
    link: the URL that the menu item should direct to when licked on
    icon: The image associated with the menu
    text: describes the menu item
    attributes: text that contains HTML attributes that should also be applied to the menu item
-->
<?php if($menu_items): ?>
    <div style='width:250px; text-align:left; position:absolute;left:0px; top:0px;overflow: hidden' class='slide' id='profile_sidebar'>
        <ul id='submenu' style="padding-top:0px;">
            <?php foreach ($menu_items as $key => $value): ?>
                <?php if(isset($value['header'])) : ?>
        </ul>
                <div class='menu_header'><?= $value['header']?></div>
                <ul id='submenu'>
                <?php else : ?>
                    <?php 
                    $link_array = explode("/",$value['link']);
                    $link = $link_array[sizeof($link_array)-1];
                    $link_class = ($this->uri->segment($this->uri->total_segments())==$link)?"class='active'":''; 
                    $icon = ($value['icon'])?"<img src='/images/icons/".$value['icon']."' />":""; ?>
                    <li>
                        <?php 
                            if (array_key_exists("attributes", $value)) {
                                $attributes = $value['attributes'];
                            }
                            else {
                                $attributes = "";
                            }
                        ?>
                        <a <?= $link_class?> <?= $attributes ?> href='<?= $value['link']?>'> <?= $icon ?> <?= $value['text']?></a>
                    </li>
                <?php endif; 
            endforeach; ?> 
            </ul>
    </div>

<? endif ?>