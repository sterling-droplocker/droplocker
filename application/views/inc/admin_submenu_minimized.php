<?php
/** 
 * The following view file renders the admin submenu sidebar  minimized.
 * 
 * If the intended entry is to be a header, then the menu item expects the following parameter:
    header: text that represents the title for the header
If the intended entry is to be a clickable menu item, then the menu item expects the following parameters:
    link: the URL that the menu item should direct to when licked on
    icon: The image associated with the menu
    text: describes the menu item
    attributes: text that contains HTML attributes that should also be applied to the menu item
*/
if($submenu):
    $icon = null;
?>


<script type="text/javascript">
$(document).ready(function() {
   $("#toggle_link").click(); //The follwoing statement clicks the toggle button so that the menu sidebar will automatically be minimized when the page first logs.
});
</script>
<div style='width:250px; text-align:left; position:absolute;left:0px; top:0px;overflow: hidden' class='slide' id='admin_sidebar'>
	
	<ul id='submenu' style="padding-top:5px;">
            <?php foreach ($submenu as $key => $value): ?>
                <?php if(isset($value['header'])) : ?>
        </ul>
                <div class='menu_header'><?= $value['header']?></div>
        <ul id='submenu'>
                <?php else : ?>
                    <?php 
                    $link_array = explode("/",$value['link']);
                    $link = $link_array[sizeof($link_array)-1];

                    if ($this->uri->segment($this->uri->total_segments()) == $link) {
                        $link_class="class=active";
                    }
                    else {
                        $link_class = "";
                    }
                        if (array_key_exists("attributes", $value)) {
                            $attributes = $value['attributes'];
                        }
                        else {
                            $attributes = "";
                        }
                    $icon = (isset($value['icon']))?"<img src='".$value['icon']."' />":""; ?>
                    <li><a <?= $link_class?> <?=$attributes?> href='<?= $value['link']?>'> <?= $icon ?> <?= $value['text']?></a></li>
                <?php endif; ?>
            <?php endforeach; ?> 
	</ul>
</div>
<div id='toggle_div'>
    <a class='open' id='toggle_link' href='javascript:;'><img title='close' src='/images/icons/resultset_previous.png'></a>
</div>
<?php endif ?>