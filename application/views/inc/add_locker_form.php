<script type="text/javascript">
    var units = <?= json_encode($units)?>;
    var cdnimages = "<?= CDNIMAGES ?>";
    var lockerTypes = <?= json_encode($locker_types) ?>;
    var lockTypes = <?= json_encode($lock_types) ?>;
    var lockerStatus = <?= json_encode($locker_status) ?>;
</script>

<p>
    <a href='/admin/locations/display_update_form/<?= $id ?>'><img align='bottom' src="<?= CDNIMAGES?>icons/application_edit.png" /> Edit Location</a>
    <a href='javascript:;' id='add_button'><img align='bottom' src="<?= CDNIMAGES?>icons/add.png" /> Add Lockers</a>&nbsp;&nbsp;&nbsp;
    <a href='javascript:;' id='mass_delete_button'><img align='bottom' src="<?= CDNIMAGES?>icons/cross.png" /> Make all selected lockers inactive</a>
</p>


<div id='locker_form'>
    <table class='detail_table'>
        <tr>
            <th>Locker Number</th>
            <td>
                Leave end blank if only adding one locker<br>
                <input style='width:80px;' placeholder='start' type='text' id='number_start' name='number_start' /> To <input style='width:80px;' id='number_end' placeholder="end" type='text' name='number_end' />
            </td>
        </tr>
        <tr>
            <th>Locker Number Prefix</th>
            <td>
                Specify if you want a prefix added to the locker number (i.e. "MyLocker_")<br>
                <input style='width:100px;' placeholder='insert prefix here' type='text' id='number_prefix' name='number_prefix' />
            </td>
        </tr>
            <th>Locker Style</th>
            <td>
                <select id="locker_type" name='locker_type'>
                    <?php foreach($locker_types as $locker_type) : ?>
                        <option value='<?= $locker_type->lockerStyleID?>'><?= $locker_type->name?></option>
                    <?php endforeach ; ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <th>Lock Type</th>
            <td>
                <select id="lock_type" name='lock_type'>
                    <?php 
                    foreach($lock_types as $lock_type) : 
                        $selected = "";
                        if ($lock_type->name == "Electronic") $selected = 'selected="selected"';
                    ?>
                    <option value='<?= $lock_type->lockerLockTypeID?>' <?=$selected?>><?= $lock_type->name?></option>
                    <?php endforeach ; ?>
                </select>
            </td>
        </tr>
    </table>
    <br />
    <input type="hidden" id="location_id" value="<?= $id?>"/>
    <a href='javascript:;' id='insert_button' class="button orange">Add Locker</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:;' id='cancel'><img align='bottom' src="<?= CDNIMAGES?>icons/cross.png" /> Cancel</a>
	<br />
	<br />
</div>

<table class="box-table-a" id="active_lockers">
    <thead>
        <tr>
        	<th width='10px;'><input type='checkbox' id='check_all' /></th>
            <th width='*'>Locker</th>
            <th width='200'>Style</th>
            <th width='200'>Lock Type</th>
            <th width='200'>Status</th>
            <th width='25'></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($lockers as $locker) : ?>
        <tr id="row-<?= $locker->lockerID?>">
            <td><input type='checkbox' class='mass_delete' id='delete-<?= $locker->lockerID?>' name='delete[]' /></td>
            <td><span id='lockerName-<?= $locker->lockerID ?>' class='click text'><?= $locker->lockerName ? $locker->lockerName : '-'?></span> (<a href="/admin/reports/locker_history/<?= $locker->lockerID?>" target="_blank">history</a>)</td>
            <td><span id='lockerStyle_id-<?= $locker->lockerID ?>' class='click dd-style'><?= ucwords($locker->name)?></td>
            <td><span id='lockerLockType_id-<?= $locker->lockerID ?>' class='click dd-lock'><?= ucwords($locker->lockName)?></td>
            <td><span id='lockerStatus_id-<?= $locker->lockerID ?>' class='click dd_status'><?= ucwords($locker->lockerStatus)?></td>
            <td align='center'>
                <a onclick='return verify_delete()' class="delete_button" href='/admin/locations/delete_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/cross.png' /></a>                
                <a style="display:none;" class='enable_button' id='enable-<?= $locker->lockerID?>' href='/admin/locations/enable_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/accept.png' /></a>
            </td>
        </tr>
        <? endforeach ; ?>
    </tbody>
</table>
<h2>Inactive Lockers</h2>
<table class="box-table-a" id="inactive_lockers">
    <thead>
        <tr>
            <th width='10px;'><input type='checkbox' id='check_all' /></th>
            <th width='*'>Locker</th>
            <th width='200'>Style</th>
            <th width='200'>Lock Type</th>
            <th width='200'>Status</th>
            <th width='25'></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($inactive_lockers as $locker) : ?>
        <tr id="row-<?= $locker->lockerID?>">
            <td><input type='checkbox' class='mass_delete' id='delete-<?= $locker->lockerID?>' name='delete[]' /></td>
            <td><span id='lockerName-<?= $locker->lockerID ?>' class='click text'><?= $locker->lockerName?></span> (<a href="/admin/reports/locker_history/<?= $locker->lockerID?>" target="_blank">history</a>)</td>
            <td><span id='lockerStyle_id-<?= $locker->lockerID ?>' class='click dd-style'><?= ucwords($locker->name)?></td>
            <td><span id='lockerLockType_id-<?= $locker->lockerID ?>' class='click dd-lock'><?= ucwords($locker->lockName)?></td>
            <td><span id='lockerStatus_id-<?= $locker->lockerID ?>' class='click dd_status'><?= ucwords($locker->lockerStatus)?></td>
            <td align='center'>
                <a style="display:none;" onclick='return verify_delete()' class="delete_button" href='/admin/locations/delete_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/cross.png' /></a>                
                <a class='enable_button' id='enable-<?= $locker->lockerID?>' href='/admin/locations/enable_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/accept.png' /></a>
            </td>
        </tr>
        <? endforeach ; ?>
    </tbody>
</table>
