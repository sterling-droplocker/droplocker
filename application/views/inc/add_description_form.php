<?php
//print_r($lockers);
?>

<script>
	var units = <?= json_encode($units)?>;
	var cdnimages = "<?= CDNIMAGES ?>";
	var lockerTypes = <?= json_encode($locker_types) ?>;
	var lockTypes = <?= json_encode($lock_types) ?>;
    var lockerStatus = <?= json_encode($locker_status) ?>;
</script>

<br>

<? if($lockers[0]->lockerName!='Front_Counter') :?>
<h3>Add a new drop off in this location</h3>
<form action='/admin/locations/update_description/' method='post'>
    <input style='width:90%;padding:5px;' type='text' name='description' maxlength="40" value="" />
    <input type='hidden' value='<?= $id?>' name='locationID' />
    <br />
    <span style='font-size:10'>Max Length: 40 characters</span>
    <br />
    <br />
    <input type='submit' value='Update' class='button orange' name="submit" /> 
</form>
<? else: ?>
<p>Cannot add more drop-off areas to a Corner Store</p>
<? endif; ?>
<table class="box-table-a" id="active_lockers">
    <thead>
        <tr>
        	<th width='10px;'><input type='checkbox' id='check_all' /></th>
            <th width='*'>Locker</th>
            <th width='200'>Style</th>
            <th width='200'>Lock Type</th>
            <th width='200'>Status</th>
            <th width='25'></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($lockers as $locker) : ?>
        <tr id="row-<?= $locker->lockerID?>">
        	<td><input type='checkbox' class='mass_delete' id='delete-<?= $locker->lockerID?>' name='delete[]' /></td>
            <td><span id='lockerName-<?= $locker->lockerID ?>' class='click text'><?= $locker->lockerName ? $locker->lockerName : '-'?></span> (<a href="/admin/reports/locker_history/<?= $locker->lockerID?>" target="_blank">history</a>)</td>
            <td><span id='lockerStyle_id-<?= $locker->lockerID ?>' class='click dd-style'><?= ucwords($locker->name)?></td>
            <td><span id='lockerLockType_id-<?= $locker->lockerID ?>' class='click dd-lock'><?= ucwords($locker->lockName)?></td>
            <td><span id='lockerStatus_id-<?= $locker->lockerID ?>' class='click dd_status'><?= ucwords($locker->lockerStatus)?></td>
            <td align='center'>
                <? if($lockers[0]->lockerName!='Front_Counter') :?>
                <a onclick='return verify_delete()' class="delete_button" href='/admin/locations/delete_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/cross.png' /></a>
               <? endif; ?>
                <a style="display:none;" class='enable_button' id='enable-<?= $inactive_locker->lockerID?>' href='/admin/locations/enable_locker/<?= $inactive_locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/accept.png' /></a>
            </td>
        </tr>
        <? endforeach ; ?>
    </tbody>
</table>
<h2>Inactive Lockers</h2>
<table class="box-table-a" id="inactive_lockers">
    <thead>
        <tr>
            <th width='10px;'><input type='checkbox' id='check_all' /></th>
            <th width='*'>Locker</th>
            <th width='200'>Style</th>
            <th width='200'>Lock Type</th>
            <th width='200'>Status</th>
            <th width='25'></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($inactive_lockers as $locker) : ?>
        <tr id="row-<?= $locker->lockerID?>">
            <td><input type='checkbox' class='mass_delete' id='delete-<?= $locker->lockerID?>' name='delete[]' /></td>
            <td><span id='lockerName-<?= $locker->lockerID ?>' class='click text'><?= $locker->lockerName?></span> (<a href="/admin/reports/locker_history/<?= $locker->lockerID?>" target="_blank">history</a>)</td>
            <td><span id='lockerStyle_id-<?= $locker->lockerID ?>' class='click dd-style'><?= ucwords($locker->name)?></td>
            <td><span id='lockerLockType_id-<?= $locker->lockerID ?>' class='click dd-lock'><?= ucwords($locker->lockName)?></td>
            <td><span id='lockerStatus_id-<?= $locker->lockerID ?>' class='click dd_status'><?= ucwords($locker->lockerStatus)?></td>
            <td align='center'>
                <a style="display:none;" onclick='return verify_delete()' class="delete_button" href='/admin/locations/delete_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/cross.png' /></a>                
                <a class='enable_button' id='enable-<?= $locker->lockerID?>' href='/admin/locations/enable_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/accept.png' /></a>
            </td>
        </tr>
        <? endforeach ; ?>
    </tbody>
</table>
