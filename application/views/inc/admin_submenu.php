<?
/** 
 * The following view renders the admin menu sidebar.
 * 
 * If the intended entry is to be a header, then the menu item expects the following parameter:
    header: text that represents the title for the header
If the intended entry is to be a clickable menu item, then the menu item expects the following parameters:
    link: the URL that the menu item should direct to when licked on
    icon: The image associated with the menu
    text: describes the menu item
    attributes: text that contains HTML attributes that should also be applied to the menu item
*/

$menu_current_uri = trim($this->uri->uri_string(), '/');

?>

<? if (!empty($submenu)):
    $list_open = false;
?>
<div style='width:250px; text-align:left; position:absolute;left:0px; top:0px;overflow: hidden;padding-top:5px;' class='slide' id='admin_sidebar'>
<? foreach ($submenu as $key => $value): ?>
    <? if(isset($value['header'])): ?>
        <? if ($list_open): $list_open = false; ?></ul><? endif; ?>
        <div class='menu_header'><?= $value['header']?></div>
    <? else:
        $attributes = isset($value['attributes']) ? $value['attributes'] : "";
        $link_class = strpos($menu_current_uri, trim($value['link'], '/')) === 0 ? "class=active" : "";
        $icon = (isset($value['icon']))?"<img src='".$value['icon']."' />":"";
    ?>
        <? if (!$list_open): $list_open = true; ?><ul id='submenu'><? endif; ?>
        <li>
            <a <?= $link_class ?> <?= $attributes ?> href='<?= $value['link']?>'> <?= $icon ?> <?= $value['text'] ?></a>
        </li>
    <? endif; ?>
    <? endforeach; ?>
    <? if ($list_open): $list_open = false; ?></ul><? endif; ?>
</div>
<div id='toggle_div'>
    <a class='open' id='toggle_link' href='javascript:;'><img title='close' src='/images/icons/resultset_previous.png'></a>
</div>
<? endif ?>
