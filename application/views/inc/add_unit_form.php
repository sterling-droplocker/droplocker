<script>
	var units = <?= json_encode($units)?>;
	var cdnimages = "<?= CDNIMAGES ?>";
    var lockerStatus = <?= json_encode($locker_status) ?>;
</script>

<div style='padding:5px;'>
    <a href='/admin/locations/display_update_form/<?= $id ?>'><img align='bottom' src="<?= CDNIMAGES?>icons/application_edit.png" /> Edit Location</a>
    <a href='javascript:;' id='add_button'><img align='bottom' src="<?= CDNIMAGES?>icons/add.png" /> Add Unit</a>&nbsp;&nbsp;&nbsp;
    <a href='javascript:;' id='mass_delete_button'><img align='bottom' src="<?= CDNIMAGES?>icons/cross.png" /> Delete all selected</a>
</div>

<div id='locker_form'>
    <table class='detail_table'>
        <tr>
            <th>Unit Number</th>
            <td>
            Leave end blank if only adding one unit<br>
            <input style='width:80px;' placeholder='start' type='text' id='number_start' name='number_start' /> To <input style='width:80px;' id='number_end' placeholder="end" type='text' name='number_end' /></td>
            <input type='hidden' id='locker_type' value='5'/> 
            <input type='hidden' id='lock_type' value='3'/> 
        </tr>
            <th>Unit Number Prefix</th>
            <td>
                Specify if you want a prefix added to the unit number (i.e. "Unit_")<br>
                <input style='width:100px;' placeholder='insert prefix here' type='text' id='number_prefix' name='number_prefix' />
            </td>
        </tr>
    </table>
    <br />
    <input type="hidden" id="location_id" value="<?= $id?>"/>
    <a href='javascript:;' id='insert_button' class="button orange">Add Unit</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:;' id='cancel'><img align='bottom' src="<?= CDNIMAGES?>icons/cross.png" /> Cancel</a>
<br /><br />
</div>

<table class="box-table-a" id="active_lockers">
    <thead>
        <tr>
        	<th width='10px;'><input type='checkbox' id='check_all' /></th>
            <th width='*'>Unit</th>
            <th width='200'>Status</th>
            <th align='center' width='25'>Delete</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($lockers as $locker) : ?>
            <tr id="row-<?= $locker->lockerID?>">
                <td><input type='checkbox' class='mass_delete' id='delete-<?= $locker->lockerID?>' name='delete[]' /></td>
                <td><span id='lockerName-<?= $locker->lockerID ?>' class='click text'><?= $locker->lockerName?></span></td>
                <td><span id='lockerStatus_id-<?= $locker->lockerID ?>' class='click dd_status'><?= ucwords($locker->lockerStatus)?></span></td>
                <td align='center'>
                    <a href='/admin/locations/delete_locker/<?= $locker->lockerID ?>/<?= $id ?>'><img src='<?= CDNIMAGES?>icons/cross.png' /></a>
                </td>
            </tr>
        <? endforeach ; ?>
    </tbody>
</table>

<table class="box-table-a" id="inactive_lockers">
    <thead>
        <tr>
            <th width='10px;'><input type='checkbox' id='check_all' /></th>
            <th width='*'>Unit</th>
            <th width='200'>Status</th>
            <th align='center' width='25'>Delete</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($inactive_lockers as $locker) : ?>
            <tr id="row-<?= $locker->lockerID?>">
                <td><input type='checkbox' class='mass_delete' id='delete-<?= $locker->lockerID?>' name='delete[]' /></td>
                <td><span id='lockerName-<?= $locker->lockerID ?>' class='click text'><?= $locker->lockerName?></span></td>
                <td><span id='lockerStatus_id-<?= $locker->lockerID ?>' class='click dd_status'><?= ucwords($locker->lockerStatus)?></span></td>
                <td align='center'>
                    <a style="display:none;" onclick='return verify_delete()' class="delete_button" href='/admin/locations/delete_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/cross.png' /></a>                
                    <a class='enable_button' id='enable-<?= $locker->lockerID?>' href='/admin/locations/enable_locker/<?= $locker->lockerID?>/<?= $location->locationID?>'><img src='<?= CDNIMAGES?>icons/accept.png' /></a>
                </td>
            </tr>
        <? endforeach ; ?>
    </tbody>
</table>
