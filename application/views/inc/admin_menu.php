<!-- The following code is undocumented, unreadable * -->

<!-- If the intended entry is to be a header, then the menu item expects the following parameter:
    header: text that represents the title for the header
If the intended entry is to be a clickable menu item, then the menu item expects the following parameters:
    link: the URL that the menu item should direct to when licked on
    icon: The image associated with the menu
    text: describes the menu item
    attributes: text that contains HTML attributes that should also be applied to the menu item
-->
<style type="text/css">
    li.admin_headers {
        float:right !important;
    }
    li.admin_headers a {
        background-color: #d4ffaa !important;
        color: #333 !important;
        text-shadow:none !important;
        
    }
</style>
<div id="ddcolortabs">
    <ul>
        <?php foreach ($menu as $key => $value): 
            $class = ($this->uri->segment(2)==$key || ($this->uri->segment(4)==$key&& $this->uri->segment(3)=='denied') || ($this->uri->segment(2)=='' && $key=='index'))?' id="current" ':'';
        ?>
            <!-- If the menu has the hidden value set to 'true', then we don't show that menu. -->
            <?php if (!isset($value['hidden']) || $value['hidden'] != true): ?>
                <li<?= $class?>>

                    <a <?= isset($value['attributes']) ? $value['attributes'] : "" ?> href="<?= $value['link']?>">
                        <span><?= $value['text']?></span>
                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach ?>

        <li class="admin_headers"><a href="http://droplocker.com/wiki/category/software" title="New"><span>Support</span></a></li>
        <li class="admin_headers"><a href="/admin/admin/search" title="Search"><span> Search </span></a></li>

    </ul>
</div>
<div id="ddcolortabsline"></div>
