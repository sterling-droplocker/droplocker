<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig" >
                <h1>Contact Info</h1><br>
                <div class="visible-phone" style="text-align: center">
                    <?= aboutNav('contact'); ?>
                </div>
                Phone support is available <br>8:30am to 5pm PST, Mon - Fri.<br><br>
                1530 Custer Ave.<br>
                San Francisco, CA 94124<br><br>
                phone: (415) 255-7500<br>
                fax: (415) 255-7577<br><br>
                <a href="mailto:sales@laundrylocker.com" class="whiteLink">sales@laundrylocker.com</a><br>
                <br>
                <br>
                <div style='display:none'>
                    Would you like to hear about our newest locations and services?  Please sign up for our mailing list.<br><br>
                    <form action="joinList.php" method="post">
                        <table border="0" cellspacing="5">
                            <tr>
                                <td>eMail:</td>
                                <td><input type="text" name="email" size="25"></td>
                                <td><input type="submit" value="join mailing list" class="btnLink_med" alt"join mailing list"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= aboutNav('contact'); ?>
            </div>
        </div>
    </div>
</div>