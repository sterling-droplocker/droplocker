<div class="fluid-container">
    <div class="row-fluid">

        <div class="span8">
            <br>
            <h1>Sparkling Shoe Shines &amp; Remarkable Bag Repairs!</h1>
            <br>
            <div class="visible-phone" style="text-align: center">
                <?= servicesNav('shoe'); ?>
            </div>
            <span class="bodyReg">Laundry Locker is excited to offer quality shoe repair &amp; bag services alongside our award winning dry cleaning and wash & fold service. This is not your ordinary shoe shine or bag conditioning.  Our experienced professionals use the highest quality products.  We shine &amp; condition with two coats of lotion to get your shoes back in great looking condition, and our experts can fix almost any issue with your favorite bag.  No dirty rags, no automated buffing machines, and no rush jobs.
                <br>
                <br>
                <strong>How to use:</strong>
                <br>
                <ol>
                    <li>Simply place your shoes or bag for shine/repair in any free locker (please <strong>do not</strong> place them in your wash & fold bag as this may delay your order).</li>
                    <li>Place an order online and select "Shoe Shine", "Shoe Repair", or "Other" for any bag service.</li>
                    <li>If you need a repair, please place a specific note in the notes section of the order letting us know what you need fixed.</li>
                    <li>Shines will be returned in 3-5 business days.  Repair will be returned in 7-12 business days.</li>
                </ol>
            </span>
            <div style="padding-right: 20px; padding-top:30px;">
                <img class="shoeshine-hero" src="/images/shoeShine.jpg" alt="shoe shine" border="0" align="left">
            </div>
            <div style="padding-left: 15px;">
                <br>
                <table width="90%" border="0" cellspacing="4" cellpadding="0">
                    <tr class="bodyBold">
                        <td colspan="3">Shoe Shine Pricing:<br></td>
                    </tr>
                    <tr class="bodyReg">
                        <td valign="bottom" style="border-bottom: 1px solid #999999;" colspan="2">Item</td>
                        <td valign="bottom" align="right" style="border-bottom: 1px solid #999999;">Price</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Standard Shoes</td>
                        <td></td>
                        <td align="right">$9.99</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Boots Above Mid-Calf</td>
                        <td></td>
                        <td align="right">$37.99</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Suede</td>
                        <td></td>
                        <td align="right">add $30.00</td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr class="bodyBold">
                        <td colspan="3">Shoe Repairs:<br></td>
                    </tr>
                    <tr class="bodyReg">
                        <td valign="bottom" style="border-bottom: 1px solid #999999;" colspan="2">Women's Shoes</td>
                        <td valign="bottom" align="right" style="border-bottom: 1px solid #999999;">Price</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Bronze (heels &amp; shine)</td>
                        <td></td>
                        <td align="right">$15.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Silver (soles &amp; shine)</td>
                        <td></td>
                        <td align="right">$24.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Gold (heels, soles &amp; shine)</td>
                        <td></td>
                        <td align="right">$39.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Platinum (heels, soles, leather recondition &amp; shine)</td>
                        <td></td>
                        <td align="right">$54.95</td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr class="bodyReg">
                        <td valign="bottom" style="border-bottom: 1px solid #999999;" colspan="2">Women's Boots</td>
                        <td valign="bottom" align="right" style="border-bottom: 1px solid #999999;">Price</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Bronze (heels &amp; shine)</td>
                        <td></td>
                        <td align="right">$19.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Silver (soles &amp; shine)</td>
                        <td></td>
                        <td align="right">$27.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Gold (heels, soles &amp; shine)</td>
                        <td></td>
                        <td align="right">$44.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Platinum (heels, soles, leather recondition &amp; shine)</td>
                        <td></td>
                        <td align="right">$59.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Platinum (heels, soles, leather recondition &amp; shine)</td>
                        <td></td>
                        <td align="right">$54.95</td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr class="bodyReg">
                        <td valign="bottom" style="border-bottom: 1px solid #999999;" colspan="2">Men's Shoes</td>
                        <td valign="bottom" align="right" style="border-bottom: 1px solid #999999;">Price</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Bronze (heels &amp; shine)</td>
                        <td></td>
                        <td align="right">$25.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Silver (soles &amp; shine)</td>
                        <td></td>
                        <td align="right">$39.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Gold (heels, soles &amp; shine)</td>
                        <td></td>
                        <td align="right">$64.95</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Platinum (heels, soles, leather recondition &amp; shine)</td>
                        <td></td>
                        <td align="right">$79.955</td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr class="bodyReg">
                        <td valign="bottom" style="border-bottom: 1px solid #999999;" colspan="2">Leather Reconditioning & Shines</td>
                        <td valign="bottom" align="right" style="border-bottom: 1px solid #999999;">Price</td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Shoe leather recondition &amp; shine - dark colors</td>
                        <td></td>
                        <td align="right">$22.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Shoe leather recondition &amp; shine - light colors</td>
                        <td></td>
                        <td align="right">$38.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Boot leather recondition &amp; shine - dark colors</td>
                        <td></td>
                        <td align="right">$19.80</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Boot leather recondition &amp; shine - light colors</td>
                        <td></td>
                        <td align="right">$45.00</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Handbag leather recondition &amp; shine - dark colors</td>
                        <td></td>
                        <td align="right">$49.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Handbag leather recondition &amp; shine - light colors</td>
                        <td></td>
                        <td align="right">$119.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Belt leather recondition &amp; shine - dark colors</td>
                        <td></td>
                        <td align="right">$22.95</font></td>
                    </tr>
                    <tr class="bodyReg">
                        <td>Belt leather recondition &amp; shine - light colors</td>
                        <td></td>
                        <td align="right">$34.95</font></td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                </table>

            </div>
            <span class="bodyReg">
                <br>Please note that our quality shines and repairs take time, so if you don't want the rest of your order to be delayed, please place your shoes in a separate locker.
                Boutique and designer shoes may incur a higher cost.
                <?php if (isset($business_id)): ?>
                    Please <a href="mailto:<?= get_business_meta($business_id,'customerSupport') ?>">email us</a> for a quote.
                <?php endif; ?>
                <br><br>
                For other services (including bag, suitcase, suede, or other cleaning &amp; repair), please <a href="/main/contactus">contact us.</a>
            </span>
        </div>
        <div class="span4">
            <div class="hidden-phone">
                <?= servicesNav('shoe'); ?>
            </div>
            <br>
        </div>
    </div>
</div>