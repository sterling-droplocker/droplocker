<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>LaundryLocker Mobile</title>
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
</head>
<body>

<div data-role="page">

	<div data-role="header">
		<div data-role="navbar">
			<ul>
				<li><a href="#">About Us</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="#">Login</a></li>
			</ul>
		</div><!-- /navbar -->
	</div><!-- /header -->

	<?= $page?>

	<div data-role="footer">
		<h4>Page Footer</h4>
	</div><!-- /footer -->
</div><!-- /page -->

</body>
</html>