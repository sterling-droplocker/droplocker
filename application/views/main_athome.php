<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/howitworks/hero-athome.jpg" class="howto_hero_pic" />


            <h1>Home Delivery</h1>
            <div class="visible-phone" style="text-align: center">
                <?= howNav('home'); ?>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-home-pickup.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">1. Pickup</h2>
                        <p class="bodyBig concierge_p">
                            Create an account and log in to place an order. Select "Schedule a home pickup." Choose your date and time and provide driver instructions. Our driver will arrive at the scheduled time to retrieve your order.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-order.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">2. Schedule delivery</h2>
                        <p class="bodyBig concierge_p">
                            When your laundry is clean, we'll send you an email and SMS message to schedule your return delivery. Choose a time that works for you up to 36 hours in advance.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-home-delivered.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">3. Order delivered to home</h2>
                        <p class="bodyBig concierge_p">
                            Our driver will arrive at your home during your scheduled delivery time.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12" style="text-align: center">                   
                     <a href="/account/main/view_registration" class="blue-btn">Get Started</a>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= howNav('home'); ?>
            </div>
            <div class="calloutBanner calloutRight" style="height: auto; margin-top: 20px">
                <h3>Pickup and Delivery Hours</h3>

                <p class="bodyBigGray">
                    7 Days a week, 9AM to 9PM, <br>2-hour windows
                </p>

                <p class="bodyBigGray">
                    <strong>Turnaround Time</strong><br>
                    All orders will be ready next day with exception of orders picked up after 5PM and any time on Saturday.
                </p>

                <p class="bodyBigGray">
                    <strong>Service area</strong><br>

                    San Francisco
                </p>
            </div>
        </div>
    </div>
</div>