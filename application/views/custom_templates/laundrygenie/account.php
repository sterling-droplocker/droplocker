<!doctype html>
<html lang="en-US">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Laundry Pricing | Laundry Genie Houston</title>
      <!-- Mobile Specific Metas & Favicons -->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <link rel="apple-touch-icon" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/icon-57.png">
      <link rel="apple-touch-icon" sizes="120x120" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/icon-120.png">
      <link rel="apple-touch-icon" sizes="76x76" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/icon-76.png">
      <link rel="apple-touch-icon" sizes="152x152" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/icon-152.png">
      <!-- WordPress Stuff -->
      <script>
         /* You can add more configuration options to webfontloader by previously defining the WebFontConfig with your options */
         if ( typeof WebFontConfig === "undefined" ) {
             WebFontConfig = new Object();
         }
         WebFontConfig['google'] = {families: ['Montserrat:400,700', 'Open+Sans:300,400,600,700,800,300italic,400italic,600italic,700italic,800italic&amp;subset=latin']};
         
         (function() {
             var wf = document.createElement( 'script' );
             wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.3/webfont.js';
             wf.type = 'text/javascript';
             wf.async = 'true';
             var s = document.getElementsByTagName( 'script' )[0];
             s.parentNode.insertBefore( wf, s );
         })();
      </script>
      <!-- All in One SEO Pack 2.7.2 by Michael Torbert of Semper Fi Web Design[117,148] -->
      <meta name="description"  content="Wash and fold laundry service starting at $1.59 per pound. Customizable plans for commercial service. Free laundry pickup and delivery in Houston &amp; the surrounding service area. Visit our website to learn more." />
      <link rel="canonical" href="https://www.laundrygenie.me/pricing/" />
      <script type="text/javascript" >
         window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
         ga('create', 'UA-119909616-1', 'auto');
         // Plugins
         
         ga('send', 'pageview');
      </script>
      <script async src="https://www.google-analytics.com/analytics.js"></script>
      <!-- /all in one seo pack -->
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Laundry Genie &raquo; Feed" href="https://www.laundrygenie.me/feed/" />
      <link rel="alternate" type="application/rss+xml" title="Laundry Genie &raquo; Comments Feed" href="https://www.laundrygenie.me/comments/feed/" />
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.laundrygenie.me\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
         !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='/css/laundrygenie/settings.css?ver=5.4.6.4' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         #rs-demo-id {}
      </style>
      <link rel='stylesheet' id='stylesheet-css'  href='/css/laundrygenie/style.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='js_composer_front-css'  href='/css/laundrygenie/js_composer.min.css?ver=4.12' type='text/css' media='all' />
      <link rel='stylesheet' id='shortcodes-css'  href='/css/laundrygenie/shortcodes.css?ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='responsive-css'  href='/css/laundrygenie/responsive.css?ver=4.9.8' type='text/css' media='all' />
      <script type='text/javascript' src='/js/laundrygenie/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='/js/laundrygenie/jquery-migrate.min.js?ver=1.4.1'></script>
      <script type='text/javascript' src='/js/laundrygenie/jquery.themepunch.tools.min.js?ver=5.4.6.4'></script>
      <script type='text/javascript' src='/js/laundrygenie/jquery.themepunch.revolution.min.js?ver=5.4.6.4'></script>
      <style type="text/css">
         body{ font: 400 13px Open Sans, Arial, Helvetica, sans-serif; color: #777777; line-height: 1.9;} .wrapall, .boxed-layout{ background-color: #ffffff; } body.page-template-page-blank-php{ background: #ffffff !important; } h1{ font: 28px Open Sans, Arial, Helvetica, sans-serif; color: #333333; } h2{ font: 24px Open Sans, Arial, Helvetica, sans-serif; color: #333333; } h3{ font: 20px Open Sans, Arial, Helvetica, sans-serif; color: #333333; } h4{ font: 16px Open Sans, Arial, Helvetica, sans-serif; color: #333333; } h5{ font: 16px Open Sans, Arial, Helvetica, sans-serif; color: #333333; } h6{ font: 16px Open Sans, Arial, Helvetica, sans-serif; color: #333333; } .title{ font-family: 'Open Sans', Arial, Helvetica, sans-serif; } h1 a, h2 a, h3 a, h4 a, h5 a, h6 a { font-weight: inherit; color: inherit; } h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, a:hover h1, a:hover h2, a:hover h3, a:hover h4, a:hover h5, a:hover h6 { color: #28aae1; } a{ color: #2e368f; } a:hover{ color: #28aae1; } input[type='text'], input[type='password'], input[type='email'], input[type='tel'], textarea, select { font-family: Open Sans, Arial, Helvetica, sans-serif; font-size: 13px; } #sidebar .widget h3{ font: 14px Montserrat, Arial, Helvetica, sans-serif; color: #444444; } .container .twelve.alt.sidebar-right, .container .twelve.alt.sidebar-left, #sidebar.sidebar-right #sidebar-widgets, #sidebar.sidebar-left #sidebar-widgets{ border-color: #efefef;} #topbar{ background: #f5f5f5; color: #2e368f; } #topbar a{ color: #2e368f; } #topbar a:hover{ color: #28aae1; } @media only screen and (max-width: 767px) { #topbar .topbar-col1{ background: #f5f5f5; } } /* Navigation */ #navigation > ul > li > a{ font: 400 18px Montserrat, Arial, Helvetica, sans-serif; color: #2e368f; } #navigation > ul > li:hover > a, #navigation > ul > li > a:hover { color: #28aae1; } #navigation li.current-menu-item > a:hover, #navigation li.current-page-ancestor > a:hover, #navigation li.current-menu-ancestor > a:hover, #navigation li.current-menu-parent > a:hover, #navigation li.current_page_ancestor > a:hover, #navigation > ul > li.sfHover > a { color: #28aae1; } #navigation li.current-menu-item > a, #navigation li.current-page-ancestor > a, #navigation li.current-menu-ancestor > a, #navigation li.current-menu-parent > a, #navigation li.current_page_ancestor > a { color: #28aae1; } #navigation ul li:hover{ border-color: #28aae1; } #navigation li.current-menu-item, #navigation li.current-page-ancestor, #navigation li.current-menu-ancestor, #navigation li.current-menu-parent, #navigation li.current_page_ancestor{ border-color: #28aae1; } #navigation .sub-menu{ background: #262626; } #navigation .sub-menu li a{ font: 13px Open Sans, Arial, Helvetica, sans-serif; color: #bbbbbb; } #navigation .sub-menu li a:hover{ color: #ffffff; } #navigation .sub-menu li.current_page_item > a, #navigation .sub-menu li.current_page_item > a:hover, #navigation .sub-menu li.current-menu-item > a, #navigation .sub-menu li.current-menu-item > a:hover, #navigation .sub-menu li.current-page-ancestor > a, #navigation .sub-menu li.current-page-ancestor > a:hover, #navigation .sub-menu li.current-menu-ancestor > a, #navigation .sub-menu li.current-menu-ancestor > a:hover, #navigation .sub-menu li.current-menu-parent > a, #navigation .sub-menu li.current-menu-parent > a:hover, #navigation .sub-menu li.current_page_ancestor > a, #navigation .sub-menu li.current_page_ancestor > a:hover{ color: #ffffff; } #navigation .sub-menu li a, #navigation .sub-menu ul li a{ border-color: #333333; } #navigation > ul > li.megamenu > ul.sub-menu{ background: #262626; border-color: #28aae1; } #navigation > ul > li.megamenu > ul > li { border-right-color: #333333 !important; } #navigation > ul > li.megamenu ul li a{ color:#bbbbbb; } #navigation > ul > li.megamenu > ul > li > a { color:#ffffff; } #navigation > ul > li.megamenu > ul ul li a:hover, #header #navigation > ul > li.megamenu > ul ul li.current-menu-item a { color: #ffffff !important; background-color: #333333 !important; } /* Header General */ #search-btn, #shopping-btn, #close-search-btn { color: #bbbbbb; } #search-btn:hover, #shopping-btn:hover, #close-search-btn:hover { color: #999999; } #slogan{ font: 400 20px Open Sans, Arial, Helvetica, sans-serif; color: #777777; margin-top: 26px; } /* Mobile Header */ #mobile-navigation{ background: #262626; } #mobile-navigation ul li a{ font: 13px Open Sans, Arial, Helvetica, sans-serif; color: #bbbbbb; border-bottom-color: #333333 !important; } #mobile-navigation ul li a:hover, #mobile-navigation ul li a:hover [class^='fa-'], #mobile-navigation li.open > a, #mobile-navigation ul li.current-menu-item > a, #mobile-navigation ul li.current-menu-ancestor > a{ color: #ffffff; } body #mobile-navigation li.open > a [class^='fa-']{ color: #ffffff; } #mobile-navigation form, #mobile-navigation form input{ background: #444444; color: #cccccc; } #mobile-navigation form:before{ color: #cccccc; } #mobile-header{ background: #ffffff; height: 90px; } #mobile-navigation-btn, #mobile-cart-btn, #mobile-shopping-btn{ color: #bbbbbb; line-height: 90px; } #mobile-navigation-btn:hover, #mobile-cart-btn:hover, #mobile-shopping-btn:hover { color: #999999; } #mobile-header .logo{ margin-top: 0px; } /* Header V1 */ #header.header-v1 { height: 90px; background: #ffffff; } .header-v1 .logo{ margin-top: 0px; } .header-v1 #navigation > ul > li{ height: 90px; padding-top: 35px; } .header-v1 #navigation .sub-menu{ top: 90px; } .header-v1 .header-icons-divider{ line-height: 90px; background: #efefef; } #header.header-v1 .widget_shopping_cart{ top: 90px; } .header-v1 #search-btn, .header-v1 #close-search-btn, .header-v1 #shopping-btn{ line-height: 90px; } .header-v1 #search-top, .header-v1 #search-top input{ height: 90px; } .header-v1 #search-top input{ color: #666666; font-family: Open Sans, Arial, Helvetica, sans-serif; } /* Header V3 */ #header.header-v3 { background: #ffffff; } .header-v3 .navigation-wrap{ background: #ffffff; border-top: 1px solid #efefef; } .header-v3 .logo { margin-top: 30px; margin-bottom: 30px; } /* Header V4 */ #header.header-v4 { background: #ffffff; } .header-v4 .navigation-wrap{ background: #ffffff; border-top: 1px solid #efefef; } .header-v4 .logo { margin-top: 30px; margin-bottom: 30px; } /* Transparent Header */ #transparentimage{ padding: 90px 0 0 0; } .header-is-transparent #mobile-navigation{ top: 90px; } /* Stuck */ .stuck{ background: #ffffff; } /* Titlebars */ .titlebar h1{ font: 400 24px Montserrat, Arial, Helvetica, sans-serif; color: #2e368f; } #fulltitle{ background: #f9f9f9; border-bottom: 1px dashed #efefef; } #breadcrumbs{ margin-top: 6px; } #breadcrumbs, #breadcrumbs a{ font: 300 13px Open Sans, Arial, Helvetica, sans-serif; color: #aaaaaa; } #breadcrumbs a:hover{ color: #666666; } #fullimagecenter h1, #transparentimage h1{ font: 42px Montserrat, Arial, Helvetica, sans-serif; color: #ffffff; text-transform: uppercase; letter-spacing: 1px; text-align: center; } /* Footer */ #footer .widget h3{ font: 18px Montserrat, Arial, Helvetica, sans-serif; color: #ffffff; } #footer{ color: #ffffff; border-top: 4px none #bfbfbf; } #footer{ background-color: #bfbfbf; } #footer a, #footer .widget ul li:after { color: #2e368f; } #footer a:hover, #footer .widget ul li:hover:after { color: #cccccc; } #footer .widget ul li{ border-bottom-color: #bfbfbf; } /* Copyright */ #copyright{ background: #1b1b1b; color: #777777; } #copyright a { color: #ffffff; } #copyright a:hover { color: #cccccc; } /* Color Accent */ .highlight{color:#28aae1 !important;} ::selection{ background: #28aae1; } ::-moz-selection { background: #28aae1; } #shopping-btn span{background:#28aae1;} .blog-page .post h1 a:hover,.blog-page .post h2 a:hover{color:#28aae1;} .entry-image .entry-overlay{background:#28aae1;} .entry-quote a:hover{background:#28aae1;} .entry-link a:hover{background:#28aae1;} .blog-single .entry-tags a:hover{color:#28aae1;} .sharebox ul li a:hover{color:#28aae1;} #pagination .current a{background:#28aae1;} #filters ul li a:hover{color:#28aae1;} #filters ul li a.active{color:#28aae1;} #back-to-top a:hover{background-color:#28aae1;} #sidebar .widget ul li a:hover{color:#28aae1;} #sidebar .widget ul li:hover:after{color:#28aae1;} .widget_tag_cloud a:hover,.widget_product_tag_cloud a:hover{background:#28aae1;border-color:#28aae1;} .widget_portfolio .portfolio-widget-item .portfolio-overlay{background:#28aae1;} #sidebar .widget_nav_menu ul li a:hover{color:#28aae1;} #footer .widget_tag_cloud a:hover,#footer .widget_product_tag_cloud a:hover{background:#28aae1;border-color:#28aae1;} /* Shortcodes */ .box.style-2{border-top-color:#28aae1;} .box.style-4{border-color:#28aae1;} .box.style-6{background:#28aae1;} a.button,input[type=submit],button,.minti_button{background:#28aae1;border-color:#28aae1;} a.button.color-2{color:#28aae1;border-color:#28aae1;} a.button.color-3{background:#28aae1;border-color:#28aae1;} a.button.color-9{color:#28aae1;} a.button.color-6:hover{background:#28aae1;border-color:#28aae1;} a.button.color-7:hover{background:#28aae1;border-color:#28aae1;} .counter-number{color:#28aae1;} .divider-title.align-center:after, .divider-title.align-left:after { background-color:#28aae1 } .divider5{border-bottom-color:#28aae1;} .dropcap.dropcap-circle{background-color:#28aae1;} .dropcap.dropcap-box{background-color:#28aae1;} .dropcap.dropcap-color{color:#28aae1;} .toggle .toggle-title.active, .color-light .toggle .toggle-title.active{ background:#28aae1; border-color: #28aae1;} .iconbox-style-1.icon-color-accent i.boxicon,.iconbox-style-2.icon-color-accent i.boxicon,.iconbox-style-3.icon-color-accent i.boxicon,.iconbox-style-8.icon-color-accent i.boxicon,.iconbox-style-9.icon-color-accent i.boxicon{color:#28aae1!important;} .iconbox-style-4.icon-color-accent i.boxicon,.iconbox-style-5.icon-color-accent i.boxicon,.iconbox-style-6.icon-color-accent i.boxicon,.iconbox-style-7.icon-color-accent i.boxicon,.flip .icon-color-accent.card .back{background:#28aae1;} .latest-blog .blog-item .blog-overlay{background:#28aae1;} .latest-blog .blog-item .blog-pic i{color:#28aae1;} .latest-blog .blog-item h4 a:hover{color:#28aae1;} .progressbar .progress-percentage{background:#28aae1;} .wpb_widgetised_column .widget ul li a:hover{color:#28aae1;} .wpb_widgetised_column .widget ul li:hover:after{color:#28aae1;} .wpb_accordion .wpb_accordion_wrapper .ui-state-active .ui-icon{background-color:#28aae1;} .wpb_accordion .wpb_accordion_wrapper .ui-state-active.wpb_accordion_header a{color:#28aae1;} .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header a:hover,.wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header a:hover .ui-state-default .ui-icon{color:#28aae1;} .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header:hover .ui-icon{background-color:#28aae1!important;} .wpb_content_element.wpb_tabs .wpb_tabs_nav li.ui-tabs-active{border-bottom-color:#28aae1;} .portfolio-item h4 a:hover{ color: #28aae1; } .portfolio-filters ul li a:hover { color: #28aae1; } .portfolio-filters ul li a.active { color: #28aae1; } .portfolio-overlay-icon .portfolio-overlay{ background: #28aae1; } .portfolio-overlay-icon i{ color: #28aae1; } .portfolio-overlay-effect .portfolio-overlay{ background: #28aae1; } .portfolio-overlay-name .portfolio-overlay{ background: #28aae1; } .portfolio-detail-attributes ul li a:hover{ color: #28aae1; } a.catimage:hover .catimage-text{ background: #28aae1; } /* WooCommerce */ .products li h3{font: 400 13px Open Sans, Arial, Helvetica, sans-serif; color: #777777;} .woocommerce .button.checkout-button{background:#28aae1;border-color:#28aae1;} .woocommerce .products .onsale{background:#28aae1;} .product .onsale{background:#28aae1;} button.single_add_to_cart_button:hover{background:#28aae1;} .woocommerce-tabs > ul > li.active a{color:#28aae1;border-bottom-color:#28aae1;} p.stars a:hover{background:#28aae1;} p.stars a.active,p.stars a.active:after{background:#28aae1;} .product_list_widget a{color:#28aae1;} .woocommerce .widget_layered_nav li.chosen a{color:#28aae1!important;} .woocommerce .widget_product_categories > ul > li.current-cat > a{color:#28aae1!important;} .woocommerce .widget_product_categories > ul > li.current-cat:after{color:#28aae1!important;} .woocommerce-message{ background: #28aae1; } .bbp-topics-front ul.super-sticky .bbp-topic-title:before, .bbp-topics ul.super-sticky .bbp-topic-title:before, .bbp-topics ul.sticky .bbp-topic-title:before, .bbp-forum-content ul.sticky .bbp-topic-title:before{color: #28aae1!important; } #subscription-toggle a:hover{ background: #28aae1; } .bbp-pagination-links span.current{ background: #28aae1; } div.wpcf7-mail-sent-ok,div.wpcf7-mail-sent-ng,div.wpcf7-spam-blocked,div.wpcf7-validation-errors{ background: #28aae1; } .wpcf7-not-valid{ border-color: #28aae1 !important;} .products .button.add_to_cart_button{ color: #28aae1!important; } .minti_list.color-accent li:before{ color: #28aae1!important; } .blogslider_text .post-categories li a{ background-color: #28aae1; } .minti_zooming_slider .flex-control-nav li .minti_zooming_slider_ghost { background-color: #28aae1; } .minti_carousel.pagination_numbers .owl-dots .owl-dot.active{ background-color: #28aae1; } .wpb_content_element.wpb_tour .wpb_tabs_nav li.ui-tabs-active, .color-light .wpb_content_element.wpb_tour .wpb_tabs_nav li.ui-tabs-active{ background-color: #28aae1; } .masonry_icon i{ color: #28aae1; } /* Special Font */ .font-special, .button, .counter-title, h6, .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header a, .pricing-plan .pricing-plan-head h3, a.catimage, .divider-title, button, input[type='submit'], input[type='reset'], input[type='button'], .vc_pie_chart h4, .page-404 h3, .minti_masonrygrid_item h4{ font-family: 'Montserrat', Arial, Helvetica, sans-serif; /*letter-spacing: 0px; font-weight: ;*/} .ui-helper-reset{ line-height: 1.9; } /* User CSS from Theme Options */ .hide-for-seo {display:none;} /*version 1*/ .v1-section-headline {font-size: 28px; font-weight:700; color:#2e368f;} .v1-section-headline-personal {font-size: 28px; font-weight:700; color:#f52e71;} .v1-section-headline-eco {font-size: 28px; font-weight:700; color:#4ea402;} .v1-section-headline-reversed {font-size: 24px; font-weight:600; color:#2e368f; background-color:#f5f5f5;} .v1-three-min-box {background-color:transparent;} .v1-three-min-title {color:#28aae1; font-size:24px; line-height:30px; font-weight:600; padding-top:20px; padding-bottom:4px;} .v1-three-min-title-join {color:#28aae1; font-size:24px; line-height:30px; font-weight:600; padding-bottom:4px;} .v1-three-min-text {color:#7c7c7c; font-size:16px; line-height:20px;} .v1-how-works-box h3 {color:#2e368f; font-size:24px; font-weight:700;} .v1-how-works-box {font-size:15px; min-height:160px; margin-top:-24px; padding:0 18px;} .v1-how-it-works-subtitle {color: #28aae1; text-align:center; font-size:20px; font-weight: 700;} .v1-personal-box {background-color:transparent; font-size:16px;} .v1-personal-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:20px;} .v1-eco-friendly-box ul {list-style:none; padding:0; margin:0; font-size:15px;} .v1-eco-friendly-box ul li {padding-bottom:8px;} .v1-eco-friendly-box h3 {color:#00b2d9; font-size:18px; font-weight:600; margin-bottom:12px;} .v1-eco-friendly-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:16px; line-height:24px;} .v1-eco-friendly-subtitle {font-size:15px} .v1-price-title {text-transform:uppercase; font-weight:700; font-size:32px; color:#999999; line-height:32px;} .v1-price {font-weight:400; font-size:28px; line-height:48px;} .v1-price-per-pound {font-size:12px; padding-bottom:48px;} .v1-services-box {} .v1-services-box-title {color:#28aae1; text-transform:uppercase; font-size:18px; font-weight:700;} .v1-services-box-text {font-size:14px;} .v1-customizable h3 {color:#28aae1; font-weight:700; margin:0; font-size:18px; border-bottom:1px solid #dcdcdc; padding-bottom:4px; margin-bottom:12px;} .v1-customizable p {color:#28aae1; margin:0; font-size:16px; font-weight:600;} .v1-customizable {} .v1-customizable ul {list-style:none; padding:0; margin:0;} .v1-customizable {font-size:14px;} .v1-super-easy {padding-top:24px; font-size:14px;} .v1-super-easy-title {color:#28aae1; font-size:24px; font-weight:600; padding-bottom:24px;} .v1-super-easy-text {font-size:16px;} .v1-reputation-text {font-size:15px; padding:0 24px;} .v1-reputation-highlight {color:#0eb1e1;} .v1-plans-subtext {color:#0eb1e1; font-size:16px; font-weight:600; text-align:center;} .v1-title-margin-collapse {margin-top:-32px;} .v1-price-pound-text {color:#2e368f; font-size:42px; line-height:42px; font-weight:700; margin:0;} .v1-price-per-text {color:#2e368f; font-size:12px; margin:0; padding:0;} .v1-plan-details-box {font-size:15px; margin-top:-32px;} .v1-services-subheadline {color:#0eb1e1; font-size:18px; font-weight:600; text-align:center; margin:0;} .v1-services-subheadline-subtext {text-align:center; font-size:13px; margin:0;} .v1-eco-list ul {font-size:16px; margin:0;} /*version 2*/ .v2-section-headline {font-size: 28px; font-weight:700; color:#2e368f; text-align:center;} .v2-section-headline-personal {font-size: 28px; font-weight:700; color:#f52e71;} .v2-section-headline-eco {font-size: 28px; font-weight:700; color:#4ea402;} .v2-section-headline-reversed {font-size: 24px; font-weight:600; color:#ffffff; background-color:#2e368f;} .v2-this-easy-content-row {color:#ffffff; background-color:#2e368f; font-size:16px;} .v2-this-easy-box {font-size:16px; font-weight:600; line-height:20px;} .v2-three-min-box {background-color:transparent;} .v2-three-min-title {color:#28aae1; font-size:24px; line-height:30px; font-weight:600; padding-top:20px; padding-bottom:4px;} .v2-three-min-title-join {color:#28aae1; font-size:24px; line-height:30px; font-weight:600; padding-bottom:4px;} .v2-three-min-text {color:#7c7c7c; font-size:16px; line-height:20px;} .v2-how-works-box h3 {color:#2e368f; font-size:24px; font-weight:700;} .v2-how-works-box {font-size:16px; min-height:160px; margin-top:-24px; padding:0 18px;} .v2-how-it-works-subtitle {color: #28aae1; text-align:center; font-size:20px; font-weight: 700;} .v2-personal-box {font-size:14px; line-height:20px;} .v2-eco-friendly-box ul {list-style:none; padding:0; margin:0; font-size:15px;} .v2-eco-friendly-box ul li {padding-bottom:8px;} .v2-eco-friendly-box h3 {color:#00b2d9; font-size:18px; font-weight:600; margin-bottom:12px;} .v2-eco-friendly-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:16px; line-height:24px;} .v2-eco-friendly-subtitle {font-size:15px} .v2-price-title {text-transform:uppercase; font-weight:700; font-size:32px; color:#999999; line-height:32px;} .v2-price {font-weight:400; font-size:28px; line-height:48px;} .v2-price-per-pound {font-size:12px; padding-bottom:48px;} .v2-services-box {} .v2-services-box-title {color:#28aae1; text-transform:uppercase; font-size:18px; font-weight:700;} .v2-services-box-text {font-size:14px;} .v2-customizable h3 {color:#28aae1; font-weight:700; margin:0; font-size:18px; border-bottom:1px solid #dcdcdc; padding-bottom:4px; margin-bottom:12px;} .v2-customizable p {color:#28aae1; margin:0; font-size:16px; font-weight:600;} .v2-customizable ul {list-style:none; margin-left:24px; margin-top:36px;} .v2-customizable {font-size:17px;} .v2-super-easy {padding-top:24px; font-size:14px;} .v2-super-easy-title {color:#28aae1; font-size:24px; font-weight:600; padding-bottom:24px;} .v2-super-easy-text {font-size:16px;} .v2-reputation-text {font-size:15px; padding:0 24px;} .v2-reputation-highlight {color:#0eb1e1;} .v2-plans-subtext {color:#0eb1e1; font-size:16px; font-weight:600; text-align:center;} .v2-title-margin-collapse {margin-top:-32px;} .v2-price-pound-text {color:#2e368f; font-size:42px; line-height:42px; font-weight:700; margin:0;} .v2-price-per-text {color:#2e368f; font-size:12px; margin:0; padding:0;} .v2-plan-details-box {font-size:15px; margin-top:-32px;} .v2-services-subheadline {color:#0eb1e1; font-size:18px; font-weight:600; text-align:center; margin:0;} .v2-services-subheadline-subtext {text-align:center; font-size:13px; margin:0;} .v2-eco-list ul {font-size:16px; margin:0;} /*version 3*/ .v3-section-headline {font-size: 24px; font-weight:600; color:#28aae1;} .v3-section-headline-reversed {font-size: 24px; font-weight:600; color:#28aae1; background-color:#ffffff;} .v3-three-min-box {background-color:transparent;} .v3-three-min-title {color:#28aae1; font-size:24px; line-height:30px; font-weight:600; padding-top:20px; padding-bottom:4px;} .v3-three-min-title-join {color:#28aae1; font-size:24px; line-height:30px; font-weight:600; padding-bottom:4px;} .v3-three-min-text {color:#7c7c7c; font-size:16px; line-height:20px;} .v3-how-works-box {font-size:14px; min-height:200px;} .v3-how-it-works-subtitle {color: #28aae1; text-align:center; font-size:20px; font-weight: 700;} .v3-personal-box {background-color:transparent; font-size:16px;} .v3-personal-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:20px;} .v3-eco-friendly-box ul {list-style:none; padding:0; margin:0; font-size:15px;} .v3-eco-friendly-box ul li {padding-bottom:8px;} .v3-eco-friendly-box h3 {color:#00b2d9; font-size:18px; font-weight:600; margin-bottom:12px;} .v3-eco-friendly-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:16px; line-height:24px;} .v3-eco-friendly-subtitle {font-size:15px} .v3-price-title {text-transform:uppercase; font-weight:700; font-size:32px; color:#28aae1; line-height:32px;} .v3-price {font-weight:400; font-size:28px; line-height:48px;} .v3-price-per-pound {font-size:12px; padding-bottom:48px;} .v3-services-box {} .v3-services-box-title {color:#28aae1; text-transform:uppercase; font-size:18px; font-weight:700;} .v3-services-box-text {font-size:14px;} .v3-customizable h3 {color:#28aae1; font-weight:700; margin:0; font-size:18px; border-bottom:1px solid #dcdcdc; padding-bottom:4px; margin-bottom:12px;} .v3-customizable p {color:#28aae1; font-weight:700; margin:0;} .v3-customizable {color:#28aae1;} .v3-customizable ul {list-style:none; padding:0; margin:0;} .v3-customizable {font-size:14px;} .v3-super-easy {padding-top:24px; font-size:14px;} .v3-super-easy-title {color:#28aae1; font-size:24px; font-weight:600; padding-bottom:24px;} .v3-super-easy-text {font-size:16px;} .v3-services-dc-list {padding-top:12px;} /*version 4*/ .v4-section-headline {font-size: 24px; font-weight:600; color:#ffffff; background-color:#28aae1;} .v4-section-headline-reversed {font-size: 24px; font-weight:600; color:#28aae1; background-color:#ffffff;} .v4-three-min-box {background-color:transparent;} .v4-three-min-title {color:#28aae1; font-size:20px; line-height:26px;font-weight:600; padding-top:20px; padding-bottom:4px;} .v4-three-min-title-join {color:#28aae1; font-size:20px; line-height:26px;font-weight:600; padding-bottom:4px;} .v4-three-min-text {color:#7c7c7c; font-size:16px; line-height:20px;} .v4-how-works-box {font-size:16px; background-color:transparent; background-image: url("https://www.laundrygenie.me/wp-content/uploads/2016/09/screened-background.png"); min-height:200px;} .v4-personal-box {background-color:transparent; font-size:16px;} .v4-personal-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:20px;} .v4-eco-friendly-box {background-color:transparent; font-size:16px; line-height:20px;} .v4-eco-friendly-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:18px; line-height:24px;} .v4-price-title {text-transform:uppercase; font-weight:700; font-size:32px; color:#28aae1; line-height:32px;} .v4-price {font-weight:400; font-size:28px; line-height:48px;} .v4-price-per-pound {font-size:12px; padding-bottom:48px;} .v4-services-box {background-color:transparent;} .v4-services-box-title {color:#28aae1; text-transform:uppercase; font-size:18px; font-weight:700;} .v4-services-box-text {font-size:16px;} .v4-customizable {font-size:16px;} .v4-super-easy {padding-top:24px; font-size:14px;} .v4-super-easy-title {color:#28aae1; font-size:24px; font-weight:600; padding-bottom:24px;} .v4-super-easy-text {font-size:16px;} .v4-services-dc-list {padding-top:12px;} /*version 4*/ .v5-section-headline {font-size: 24px; font-weight:600; color:#28aae1; background-color: #ffffff; line-height:60px; text-align:center;} .v5-section-headline-reversed {font-size: 24px; font-weight:600; color:#ffffff; background-color: #28aae1; line-height:60px; text-align:center;} .v5-three-min-box {background-color:transparent; max-width:360px;} .v5-three-min-title {color:#ffffff; font-size:20px; line-height:26px;font-weight:600; padding-top:20px; padding-bottom:4px;} .v5-three-min-title-join {color:#ffffff; font-size:20px; line-height:26px;font-weight:600; padding-bottom:4px;} .v5-three-min-text {color:#ffffff; font-size:16px; line-height:20px;} .v5-how-works-box {font-size:14px; background-color:transparent; background-image: url("https://www.laundrygenie.me/wp-content/uploads/2016/09/screened-background.png"); min-height:200px;} .v5-personal-box { color:#ffffff; background-color:transparent; font-size:16px;} .v5-personal-box-drop-cap {text-transform:uppercase; color:#ffffff; font-weight:700; font-size:20px;} .v5-eco-friendly-box {background-color:transparent; font-size:16px; line-height:24px;} .v5-eco-friendly-box-drop-cap {text-transform:uppercase; color:#28aae1; font-weight:700; font-size:18px; line-height:24px;} .v5-price-title {text-transform:uppercase; font-weight:700; font-size:32px; color:#28aae1; line-height:32px;} .v5-price {font-weight:400; font-size:28px; line-height:48px;} .v5-price-per-pound {font-size:12px; padding-bottom:48px;} .v5-services-box {background-color:transparent;} .v5-services-box-title {color:#ffffff; text-transform:uppercase; font-size:18px; font-weight:700;} .v5-services-box-text {font-size:16px; color:#ffffff;} /*The -2 styles for v5 need to be used if the services boxes are going to have colored icons.*/ .v5-services-box-2 {background-color: rgba(255,255,255,0.7);} .v5-services-box-text-2 {font-size:15px; color:#999999; line-height:24px;} .v5-customizable {font-size:16px;} .v5-super-easy-box {background-color:transparent; color:#ffffff; font-size:16px;} .v5-super-easy-title {font-size:20px; font-weight:600; padding-bottom:20px;} .v5-super-easy-text {font-size:16px;} .v5-services-dc-list {padding-top:12px;} .v5-testimonials {font-size:16px;} /*version 6*/ .v6-section-headline {font-size: 32px; font-weight:600; color:#0a77bc; background-color:#ffffff;} .v6-section-headline-reversed {font-size: 32px; font-weight:600; color:#ffffff; background-color:#0a77bc;} .v6-section-headline-gray-bkgnd {font-size: 32px; font-weight:600; color:#0a77bc; background-color:#f5f5f5;} .v6-personal-box {color:#999999; font-size:15px; text-align:center; padding:0 12px;} .v6-personal-box h3 {color:#28aae1; font-size:20px; font-weight:600;} .v6-this-easy-text {color:#28aae1; text-align:center; font-size:20px; line-height:22px;} .v6-subtitles {color: #999999; text-align:center; font-size:16px; font-weight: 500; text-align:center;} .v6-services-box {} .v6-services-box-title {color:#28aae1; text-transform:uppercase; font-size:18px; font-weight:700;} .v6-services-box-text {font-size:14px;} /********/ /*.pricing-plan, .pricing-plan-head and .pricing-price hook into embedded class names so they cannot carry the version designation.*/ .pricing-plan {text-transform:none; font-size:14px;} .pricing-plan-head {text-transform:none; font-size:18px;} .pricing-price {display:none;} /* callout button */ a.button.yellow { background: #053a9c; color:#ffffff; border-color:#053a9c; } a.button.yellow:hover { background: #4174d0; border-color:#4174d0;} .services-page-subheading {font-size:16px;} /*blue button - this can be changed to any color. Just select "blue" as the button color in the page editor.*/ a.button.blue{ background: #2e368f; color:#ffffff; border-color:#2e368f; } a.button.blue:hover{ background: #28aae1; border-color:#28aae1;} @media only screen and (max-width: 767px) { #topbar{ display: none !important; } }@media only screen and (max-width: 959px) { #header, .sticky-wrapper{display:none;} #mobile-header{display:inherit} }
      </style>
      <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
      <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
      <!--[if lte IE 9]>
      <link rel="stylesheet" type="text/css" href="https://www.laundrygenie.me/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
      <![endif]--><!--[if IE  8]>
      <link rel="stylesheet" type="text/css" href="https://www.laundrygenie.me/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen">
      <![endif]-->
      <meta name="generator" content="Powered by Slider Revolution 5.4.6.4 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
      <link rel="icon" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/cropped-icon-512-32x32.png" sizes="32x32" />
      <link rel="icon" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/cropped-icon-512-192x192.png" sizes="192x192" />
      <link rel="apple-touch-icon-precomposed" href="https://www.laundrygenie.me/wp-content/uploads/2018/07/cropped-icon-512-180x180.png" />
      <meta name="msapplication-TileImage" content="https://www.laundrygenie.me/wp-content/uploads/2018/07/cropped-icon-512-270x270.png" />
      <script type="text/javascript">function setREVStartSize(e){
         try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;                   
             if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})                  
         }catch(d){console.log("Failure at Presize of Slider:"+d)}
         };
      </script>
      <noscript>
         <style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>
      <link rel='stylesheet' id='custom-style-css'  href='/css/laundrygenie/bootstrap2x.css' type='text/css' media='all' />
      <script src='/bootstrap/js/bootstrap.min.js'></script>
      <script type="text/javascript">
         var $ = jQuery.noConflict();
      </script>
      <?= $_scripts ?>
      <?= $javascript ?>
      <?= $style ?>
      <?= $_styles ?>
   </head>
   <body class="page-template-default page page-id-143 smooth-scroll wpb-js-composer js-comp-ver-4.12 vc_responsive">
      <div class="site-wrapper wrapall">
         <div id="topbar" class="header-v2 clearfix">
            <div class="container">
               <div class="sixteen columns">
                  <div class="topbar-col1">
                     <a href="tel:7132532876">(713) 253-2876</a> |  <a href="mailto:hello@laundrygenie.me">hello@laundrygenie.me</a>                         
                  </div>
                  <div class="topbar-col2">
                     <div class="social-icons clearfix">
                        <ul>
                           <li><a href="https://www.facebook.com/laundrygeniehouston" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="https://www.instagram.com/laundrygenie/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <header id="header" class="header header-v1 clearfix">
            <div class="container">
               <div id="logo-navigation" class="sixteen columns">
                  <div id="logo" class="logo">
                     <a href="/"><img src="/images/laundrygenie/logo.png" alt="Laundry Genie" class="logo_standard" /></a>
                     <a href="https://www.laundrygenie.me/"><img src="/images/laundrygenie/logo-transparent.png" alt="Laundry Genie" class="logo_transparent" /></a>
                     <a href="https://www.laundrygenie.me/"><img src="/images/laundrygenie/logo-retina.png" width="260" height="80" alt="Laundry Genie" class="logo_retina" /></a>                 
                     <a href="https://www.laundrygenie.me/"><img src="/images/laundrygenie/logo-retina-transparent.png" width="260" height="80" alt="Laundry Genie" class="logo_retina_transparent" /></a>                         
                  </div>
                  <div id="navigation" class="clearfix">
                     <div class="header-icons">
                     </div>
                     <ul id="nav" class="menu">
                        <li id="menu-item-268" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-268"><a href="https://www.laundrygenie.me/services/">SERVICES</a></li>
                        <li id="menu-item-269" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-143 current_page_item menu-item-269"><a href="https://www.laundrygenie.me/pricing/">PRICING</a></li>
                        <li id="menu-item-270" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-270"><a href="https://www.laundrygenie.me/frequently-asked-questions/">FAQs</a></li>
                        <li id="menu-item-271" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-271"><a href="https://www.laundrygenie.me/sb/signup">GET STARTED!</a></li>
                        <li id="menu-item-272" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-272"><a href="/">ACCOUNT</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </header>
         <div id="mobile-header" class="mobile-header-v1">
            <div class="container">
               <div class="sixteen columns">
                  <div id="mobile-logo" class="logo">
                     <a href="https://www.laundrygenie.me/"><img src="/images/laundrygenie/logo.png" alt="Laundry Genie" class="logo_standard" /></a>
                     <a href="https://www.laundrygenie.me/"><img src="/images/laundrygenie/logo-retina.png" width="260" height="80" alt="Laundry Genie" class="logo_retina" /></a>                         
                  </div>
                  <a href="#" id="mobile-navigation-btn"><i class="fa fa-bars"></i></a>
               </div>
            </div>
         </div>
         <div id="mobile-navigation">
            <div class="container">
               <div class="sixteen columns">
                  <div class="menu-main-menu-container">
                     <ul id="mobile-nav" class="menu">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-268"><a href="https://www.laundrygenie.me/services/">SERVICES</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-143 current_page_item menu-item-269"><a href="https://www.laundrygenie.me/pricing/">PRICING</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-270"><a href="https://www.laundrygenie.me/frequently-asked-questions/">FAQs</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-271"><a href="https://www.laundrygenie.me/sb/signup">GET STARTED!</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-272"><a href="https://www.laundrygenie.me/sb/account/">LOGIN</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div id="notitlebar"></div>
         <div id="page-wrap">
            <div id="content" class="page-section nopadding">
               <div id="bootstrap2x">
                  <br/>
                  <?=$content?>
               </div>
            </div>
            <!-- end content -->
         </div>
         <!-- end page-wrap -->
         <footer id="footer">
            <div class="container">
               <div class="four columns">
                  <div id="nav_menu-2" class="widget widget_nav_menu">
                     <h3>Menu</h3>
                     <div class="menu-footer-col-1-container">
                        <ul id="menu-footer-col-1" class="menu">
                           <li id="menu-item-260" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-260"><a href="https://www.laundrygenie.me/services/">Services</a></li>
                           <li id="menu-item-795" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-143 current_page_item menu-item-795"><a href="https://www.laundrygenie.me/pricing/">Pricing</a></li>
                           <li id="menu-item-263" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-263"><a href="https://www.laundrygenie.me/frequently-asked-questions/">FAQs</a></li>
                           <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-262"><a href="https://www.laundrygenie.me/contact-us/">Contact Us</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="four columns">
                  <div id="nav_menu-4" class="widget widget_nav_menu">
                     <h3>&nbsp;</h3>
                     <div class="menu-footer-col-2-container">
                        <ul id="menu-footer-col-2" class="menu">
                           <li id="menu-item-766" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-766"><a href="https://www.laundrygenie.me/privacy/">Privacy</a></li>
                           <li id="menu-item-265" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-265"><a href="https://www.laundrygenie.me/terms-of-service/">Terms of Service</a></li>
                           <li id="menu-item-267" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-267"><a href="https://www.laundrygenie.me/sb/account/">Login</a></li>
                           <li id="menu-item-266" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-266"><a href="https://www.laundrygenie.me/sb/signup">Get Started!</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="four columns">
                  <div id="text-2" class="widget widget_text">
                     <h3>Laundry Pickup &#038; Delivery In:</h3>
                     <div class="textwidget">Houston, TX<br>
                        Bellaire, TX<br>
                        Missouri City, TX<br>
                        Richmond, TX<br>
                        Stafford, TX<br>
                        Sugar Land, TX<br>
                        Pearland, TX<br><br>
                        <a href="/sb/signup/">Enter Your Zip Code</a> To Find Out If You Qualify For Service<br><br>
                        <a href="/service-area/">Browse Our Full Service Area</a><br>
                     </div>
                  </div>
               </div>
               <div class="four columns">
                  <div id="custom_html-2" class="widget_text widget widget_custom_html">
                     <h3>Contact Info</h3>
                     <div class="textwidget custom-html-widget"><span style="font-size: 16px;">Laundry Genie</span><br>
                        <span style="font-size: 16px;">Proudly Serving Houston & The Surrounding Areas</span><br><br>
                        Phone: <a href="tel:7132532876">(713) 253-2876</a><br>
                        Email: <a href="mailto:hello@laundrygenie.me">hello@laundrygenie.me</a><br>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
         <div id="copyright" class="clearfix">
            <div class="container">
               <div class="sixteen columns">
                  <div class="copyright-text copyright-col1">
                     &copy; 2018 Laundry Genie <a href="https://www.getspringboard.com" target="_blank">Powered by Springboard</a>                                   
                  </div>
                  <div class="copyright-col2">
                     <div class="social-icons clearfix">
                        <ul>
                           <li><a href="https://www.facebook.com/laundrygeniehouston" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="https://www.instagram.com/laundrygenie/" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end copyright -->
      </div>
      <!-- end wrapall / boxed -->
      <div id="back-to-top"><a href="#"><i class="fa fa-chevron-up"></i></a></div>
      <script type='text/javascript' src='/js/laundrygenie/jquery.easing.min.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/waypoints.min.js?ver=4.12'></script>
      <script type='text/javascript' src='/js/laundrygenie/waypoints-sticky.min.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/prettyPhoto.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/isotope.pkgd.min.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/functions.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/flexslider.min.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/smoothscroll.js'></script>
      <script type='text/javascript' src='/js/laundrygenie/comment-reply.min.js?ver=4.9.8'></script>
      <script type='text/javascript' src='/js/laundrygenie/wp-embed.min.js?ver=4.9.8'></script>
      <script type='text/javascript' src='/js/laundrygenie/js_composer_front.min.js?ver=4.12'></script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
             "use strict";
             
             /* PrettyPhoto Options */
             var lightboxArgs = {            
                             animation_speed: 'fast',
                             overlay_gallery: false,
                 autoplay_slideshow: false,
                             slideshow: 5000,
                                         opacity: 0.8,
                             show_title: false,
                 social_tools: "",           deeplinking: false,
                 allow_resize: true,
                 allow_expand: false,
                 counter_separator_label: '/',
                 default_width: 1160,
                 default_height: 653
             };
             
                     /* Automatic Lightbox */
             $('a[href$=jpg], a[href$=JPG], a[href$=jpeg], a[href$=JPEG], a[href$=png], a[href$=gif], a[href$=bmp]:has(img)').prettyPhoto(lightboxArgs);
                         
             /* General Lightbox */
             $('a[class^="prettyPhoto"], a[rel^="prettyPhoto"], .prettyPhoto').prettyPhoto(lightboxArgs);
         
             /* WooCommerce Lightbox */
             $("a[data-rel^='prettyPhoto']").prettyPhoto({
                 hook: 'data-rel',
                 social_tools: false,
                 deeplinking: false,
                 overlay_gallery: false,
                 opacity: 0.8,
                 allow_expand: false, /* Allow the user to expand a resized image. true/false */
                 show_title: false
             });
         
             
             
             /* Transparent Header */
             function transparentHeader() {
                 if ($(document).scrollTop() >= 60) {
                     $('#header.header-v1').removeClass('header-transparent');
                 }
                 else {
                     $('#header.header-v1.stuck').addClass('header-transparent');
                 }
             }
                 
             /* Sticky Header */
             if (/Android|BlackBerry|iPhone|iPad|iPod|webOS/i.test(navigator.userAgent) === false) {
         
                 var $stickyHeaders = $('#header.header-v1, #header.header-v3 .navigation-wrap, #header.header-v4 .navigation-wrap');
                 $stickyHeaders.waypoint('sticky');
                 
                 $(window).resize(function() {
                     $stickyHeaders.waypoint('unsticky');
                     if ($(window).width() < 944) {
                         $stickyHeaders.waypoint('unsticky');
                     }
                     else {
                         $stickyHeaders.waypoint('sticky');
                     }
                 });
                 
                 if ($("body").hasClass("header-is-transparent")) {
                     $(document).scroll(function() { transparentHeader(); });
                     transparentHeader();
                 }
         
             }           
             
             
                 
             /* Fill rest of page */
             $('body').css({'background-color' : '#1b1b1b' });
                             
         });
      </script>
   </body>
</html>
