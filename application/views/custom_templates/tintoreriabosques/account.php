<?php
$system_lang = get_customer_business_language_key();

$lang_swap_redirect = '&r='.base64_encode($this->uri->uri_string);
$logged_in          = $this->customer_id;

$lang_switch['es'] = $logged_in ? '/account/profile/change_language?ll_swap=es'.$lang_swap_redirect : '?ll_lang=es';
$lang_switch['en'] = $logged_in ? '/account/profile/change_language?ll_swap=en'.$lang_swap_redirect : '?ll_lang=en';
?>

<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="SHORTCUT ICON" href="/images/tintoreriabosques/favicon.ico" type="image/x-icon" />
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,700,900' rel='stylesheet' type='text/css'>
        <meta name="description" content="Servicios de Tintorería, Planchaduría, Lustrado y Conservación de Vestidos en las Mejores Zonas de la Ciudad de México | Servicio a Domicilio - Tintorería Bosques DF" />
        <meta name="keywords" content="" />
        <title>Tintorería, Sastrería, Lustrado y Conservación de Prendas en DF <?= $title; ?></title>

        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

        <?= $style ?>
        <?= $_styles ?>

        <!--<script type="text/javascript" src="jquery/jquery-1.7.1.js"></script>-->
        <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
        <script src="/js/tintoreriabosques/modernizr.js"></script>
        <script type='text/javascript' src='/js/tintoreriabosques/viewport.js'></script>
        <script src='/js/tintoreriabosques/funciones.js'  type='text/javascript'></script>
        <script src='/js/tintoreriabosques/ajax.js'  type='text/javascript'></script>
        <script src='/js/tintoreriabosques/ajax.common.js'  type='text/javascript'></script>
        <script src='/js/tintoreriabosques/menu.js'  type='text/javascript'></script>
        <link rel='stylesheet' type='text/css' href='/css/tintoreriabosques/jquery.fancybox-1.3.4.css'  />
        <script src='/js/tintoreriabosques/jquery.fancybox-1.3.4.js'  type='text/javascript'></script>

  
        <link rel="stylesheet" type="text/css" href="/css/tintoreriabosques/normalize.css" />
        <link rel='stylesheet' type='text/css' href="/css/tintoreriabosques/esqueleto.css" />
        <link rel="stylesheet" type="text/css" href="/css/tintoreriabosques/esqueleto_320.css" media="all and (min-width: 240px)" />
        <link rel="stylesheet" type="text/css" href="/css/tintoreriabosques/esqueleto_640.css" media="all and (min-width: 600px)" />
        <link rel="stylesheet" type="text/css" href="/css/tintoreriabosques/esqueleto_960.css" media="all and (min-width: 960px)" />

        <link rel='stylesheet' type='text/css' href="/css/tintoreriabosques/validationhints.css" />
 
        <script type="text/javascript" src="/js/tintoreriabosques/validationhints.js"></script>
        <script type="text/javascript" src="/js/tintoreriabosques/mktSignup.js"></script>

        <!--[if lt IE 9]>
        <script src="/js/tintoreriabosques/html5shiv.js"></script>
        <![endif]-->


        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>

        <script type="text/javascript">
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute('charset', 'utf-8');
                $.src = '//v2.zopim.com/?2GpEdO0G087YwLdgXVpc2TJ5JodLB9kX';
                z.t = +new Date;
                $.
                        type = 'text/javascript';
                e.parentNode.insertBefore($, e)
            })(document, 'script');
        </script>
        <!--End of Zopim Live Chat Script-->


        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute('charset', 'utf-8');
                $.src = '//v2.zopim.com/?1iju8keIHbxrRU4yOYIvpJjNcnjS2Lj4';
                z.t = +new Date;
                $.
                        type = 'text/javascript';
                e.parentNode.insertBefore($, e)
            })(document, 'script');
        </script>
        <!--End of Zopim Live Chat Script-->


    </head>
    <body id="home" style="padding: 0;">
        <!-- Header -->
        <div class="width_100 wrapper_header">
            <div class="content">
                <div class="one_column">
                    <header>
                        <div class="one_column">
                            <div class="logo">
                                <a href="http://tintoreriabosques.com"><span><img src="/images/tintoreriabosques/ico_inicio.png"/></span><img src="/images/tintoreriabosques/logo.png"/></a>
                            </div>
                            <div class="col-tels-chat-servicio">
                                <div class="tels">
                                    <p>
                                        TELS: <a href="tel:555251-2586">(55) 5251-2586</a>
                                        <br />
                                        <a href="tel:555251-2544">(55) 5251-2544</a>
                                    </p>
                                </div>
                                <div class="chat">
                                    <a href="javascript:$zopim.livechat.window.show()">CHAT EN VIVO</a>
                                </div>
                                <div class="servicio-dom">
                                    <a href="http://tintoreriabosques.com/servicio-a-domicilio/">SERVICIO A DOMICILIO GRATIS</a>
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
            </div>
        </div>
        <div class="width_100 wrapper_menu" style="z-index:99">
            <div class="content">
                <div class="one_column header_nav">
                    <nav role="navigation">
                        <div class="cont_nav">MENÚ PRINCIPAL</div>
                        <ul class="principal_nav">
                            <li class="btn_nav ">
                                <a href="http://tintoreriabosques.com/nosotros/"><span>&nbsp;</span>NOSOTROS<span class="linea-nosotos">&nbsp;</span></a>
                            </li>
                            <li class="btn_nav ">
                                <a href="http://tintoreriabosques.com/servicios/"><span>&nbsp;</span>SERVICIOS<span class="linea-servicios">&nbsp;</span></a>
                                <ul class="servicios">
                                    <li><a href="http://tintoreriabosques.com/servicios/tintoreria/">TINTORERÍA</a></li>
                                    <li><a href="http://tintoreriabosques.com/servicios/sastreria/">SASTRERÍA</a></li>
                                    <li><a href="http://tintoreriabosques.com/servicios/lustrado/">LUSTRADO</a></li>
                                    <li><a href="http://tintoreriabosques.com/servicios/servicio-ejecutivo/">SERVICIO EJECUTIVO</a></li>
                                    <li><a href="http://tintoreriabosques.com/servicios/vestidos-de-novia/">VESTIDOS DE NOVIA</a></li>
                                </ul>
                            </li>
                            <li class="btn_nav "><a href="http://tintoreriabosques.com/servicio-a-domicilio/"><span>&nbsp;</span>SERVICIO A DOMICILIO<span class="linea-domicilio">&nbsp;</span></a></li>
                            <li class="btn_nav "><a href="http://tintoreriabosques.com/servicio-a-empresas/"><span>&nbsp;</span>SERVICIO A EMPRESAS<span class="linea-empresas">&nbsp;</span></a></li>
                            <li class="btn_nav "><a href="http://tintoreriabosques.com/sucursales/"><span>&nbsp;</span>UBICACIONES<span class="linea-ubicaciones">&nbsp;</span></a>
                                <ul class="ubicaciones">
                                    <li><a href="http://tintoreriabosques.com/sucursales/zona-norte/bellavista/">ZONA NORTE</a></li>
                                    <li><a href="http://tintoreriabosques.com/sucursales/zona-centro/lomas-plaza/">ZONA CENTRO</a></li>
                                    <li><a href="http://tintoreriabosques.com/sucursales/zona-poniente/elite-santa-fe/">ZONA PONIENTE</a></li>
                                    <li><a href="http://tintoreriabosques.com/sucursales/zona-sur/grand-pedregal/">ZONA SUR</a></li>
                                    <li><a href="http://tintoreriabosques.com/sucursales/sastrerias/espacio-esmeralda/">SASTRERÍAS</a></li>
                                    <li><a href="http://tintoreriabosques.com/sucursales/servicio-ejecutivo/lomas-plaza/">SERVICIO EJECUTIVO</a></li>
                                    <li><a href="http://tintoreriabosques.com/servicios/">TODOS LOS SERVICIOS</a></li>
                                    <li><a href="http://tintoreriabosques.com/sucursales-mas-cercanas/">ENCONTRAR LA MÁS CERCANA</a></li>
                                </ul>
                            </li>
                            <li class="btn_nav "><a href="http://tintoreriabosques.com/noticias-y-tips/"><span>&nbsp;</span>NOTICIAS Y TIPS<span class="linea-noticias">&nbsp;</span></a></li>
                            <li class="btn_nav "><a href="http://tintoreriabosques.com/contacto/"><span>&nbsp;</span>CONTACTO<span class="linea-contacto">&nbsp;</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Content -->

        <div id="main_content">
            <?= $content ?>
        </div>

        <!-- Footer -->
        <div class="width_100 wrapper_footer">
            <div class="content">
                <div class="one_column">
                    <footer>
                        <div class="one_column" style="margin-left: 0;">    
                            <div class="siguenos" style="text-align: left; width: 173px; padding: 0;">
                                <p class="siguenos-en" style="text-align: left; padding-top: 11px;">SÍGUENOS EN</p>
                                <ul class="redes-sociales" style="float: left;">
                                    <li style="width: 40px;">
                                        <a class="twitter" href="https://twitter.com/tintobosques" target="_blank" alt="Twitter Tintorer&iacute;a Bosques">&nbsp;</a>
                                    </li>
                                    <li style="width: 40px;">
                                        <a class="facebook" href="https://www.facebook.com/tintoreriabosques" target="_blank" alt="Facebook Tintorer&iacute;a Bosques">&nbsp;</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tels-footer">
                                <p>TELS: <a href="tel:555251-2586"><strong>(55) 5251-2586</strong></a>
                                    <a href="tel:555251-2544"><strong>(55) 5251-2544</strong></a>
                                </p>
                            </div>
                            <div class="registro-boletin">
                                <div class="registrate">
                                    <div style="display:none;" class="msg_email"></div>
                                    <form class="" id="frmemail" name="frmemail">
                                        <span class="hojita-boletin">&nbsp;</span>
                                        <span>
                                            <input class="cajita" type="text" name="registro_email" value="Registre su e-mail a nuestro bolet&iacute;n" class="" onclick="this.value = '';" onblur="if (this.value == '') {
                                                        this.value = 'Registre su e-mail a nuestro bolet&iacute;n'
                                                    }" />
                                            <div class="btn-enviar">
                                                <a class="enviar" href="javascript:void(0);" onclick="registroSubmit();">ENVIAR</a>
                                            </div>
                                        </span>
                                    </form>
                                </div>
                            </div>
                            <div style="display:none;">
                                <div class="garantia-light" id="content_garantia">
                                    <h2>GARANTÍA</h2>
                                    <p>
                                        <span class="icon">Tintorer&iacute;a Bosques</span> est&aacute; equipada con tecnolog&iacute;a de punta que garantiza el mejor tratamiento de limpieza y mantenimiento de tus prendas. Nuestro personal est&aacute; altamente calificado y entrenado para darte un extraordinario servicio con los m&aacute;s altos est&aacute;ndares de calidad.</p>
                                    <p>Contamos con avales y reconocimientos de las principales autoridades en el rubro de tratamiento de prendas:</p>
                                    <ul class="lista-content">
                                        <li>ITEL. Instituto Tecnol&oacute;gico Espa&ntilde;ol de Limpieza</li>
                                        <li>SIEM. Sistema de Informaci&oacute;n Empresarial Mexicano</li>
                                        <li>CANALAVA. C&aacute;mara Nacional de la Industria de Lavander&iacute;as</li>
                                        <li>IFI. Instituto de Formaci&oacute;n Integral</li>
                                    </ul>    </div>
                            </div>
                            <div style="display:none;">
                                <div class="compromiso-light" id="content_verde">
                                    <h2>COMPROMISO VERDE</h2>
                                    <p>
                                        <span class="icon">Tintorer&iacute;a Bosques</span> se preocupa constantemente por su huella en el medio ambiente, por lo que buscamos promover acciones claras que impacten en la conciencia ambiental de nuestra sociedad.</p>        <p>Desde hace m&aacute;s de 4 a&ntilde;os, mantenemos una alianza con <a href="http://www.naturalia.org.mx" target="_blank">Naturalia A.C.</a>, donde nos comprometimos a neutralizar la emisi&oacute;n de CO2 de nuestros procesos, a promover la reutilizaci&oacute;n de ganchos y a reemplazar las bolsas de pl&aacute;stico por portatrajes 100% biodegradables y 90% reciclables.</p>    </div></div><script type='text/javascript'>        jQuery(document).ready(function () {
                                                $(".info_link").fancybox({'titlePosition': 'inside', 'transitionIn': 'none', 'transitionOut': 'none'});
                                                $("#alert_link").click();
                                            });</script>
                        </div>
                        <div class="one_column">
                            <ul class="menu-footer-left">
                                <li><a href="http://tintoreriabosques.com/nosotros/">NOSOTROS</a></li>
                                <li><a href="http://tintoreriabosques.com/servicios/">SERVICIOS</a></li>
                                <li><a href="http://tintoreriabosques.com/servicio-a-domicilio/">SERVICIO A DOMICILIO</a></li>
                                <li><a href="http://tintoreriabosques.com/servicio-a-empresas/">SERVICIO A EMPRESAS</a></li>
                            </ul>
                            <ul class="menu-footer-right">
                                <li><a href="http://tintoreriabosques.com/sucursales/">UBICACIONES</a></li>
                                <li><a href="http://tintoreriabosques.com/noticias-y-tips/">NOTICIAS</a></li>
                                <li><a href="http://tintoreriabosques.com/contacto/">CONTACTO</a></li>
                                <li><a href="http://tintoreriabosques.com/preguntas-frecuentes/">PREGUNTAS FRECUENTES</a></li>
                            </ul>
                            <div class="col_chat_firma">
                                <div class="chat-footer" style="width: 100%;">
                                    <div class="chat-en-vivo">
                                        <a href="javascript:$zopim.livechat.window.show()">CHAT EN VIVO</a>
                                    </div>
                                </div>
                                <div class="aviso" style="width: 40%">
                                    <ul class="menu-footer-right" style="width: auto; padding-top: 5px;">
                                        <li><a href="http://tintoreriabosques.com/sucursales-mas-cercanas/">ENCONTRAR LA MÁS CERCANA</a></li>
                                        <li><a href="http://tintoreriabosques.com/aviso-de-privacidad.pdf" target="_blank">AVISO DE PRIVACIDAD</a></li>
                                        <li><a href="http://tintoreriabosques.com/bolsa-de-trabajo/">BOLSA DE TRABAJO</a></li>
                                    </ul>
                                </div>
                                <div style="width: 60%; float: left; padding-top: 22px; ">
                                    <div class="firma-footer">
                                        <!--    <div class="cont_txt_firma"><a href="http://www.sytyos.com" target="_blank" id="txt_firma">TODOS LOS DERECHOS RESERVADOS INTERSYTYOS S. DE R.L. DE C.V</a></div>-->
                                        <div id="firma">
                                            <div id="ico_firma"><a href="http://www.sytyos.com" target="_blank"><img src="/images/tintoreriabosques/firma_ico_sytyos.png" alt="www.sytyos.com" /></a></div>
                                            <div id="logo_firma">
                                                &nbsp;
                                            </div>
                                            <script type="text/javascript">
                                                <!--
                                                    $(document).ready(function () {
                                                    $('#ico_firma').mouseenter(function (e) {
                                                        $('#logo_firma').animate({backgroundPosition: '0px'}, 500);
                                                    }).mouseleave(function (e) {
                                                        $('#logo_firma').animate({backgroundPosition: '-195px'}, 500);
                                                    });
                                                });
                                                // -->
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>

        <? if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
        <script type="text/javascript">
                                                var _gaq = _gaq || [];
                                                _gaq.push(['_setAccount', '<?= $ga ?>']);
                                                _gaq.push(['_setDomainName', '<?= $_SERVER["HTTP_HOST"] ?>']);
                                                _gaq.push(['_trackPageview']);

                                                (function () {
                                                    var ga = document.createElement('script');
                                                    ga.type = 'text/javascript';
                                                    ga.async = true;
                                                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                                                    var s = document.getElementsByTagName('script')[0];
                                                    s.parentNode.insertBefore(ga, s);
                                                })();
        </script>
        <? endif; ?>
        <link rel="stylesheet" type="text/css" href="/css/tintoreriabosques/custom.css" />
    </body>

</html>
