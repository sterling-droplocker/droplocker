<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US" prefix="og: https://ogp.me/ns#">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US" prefix="og: https://ogp.me/ns#">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US" prefix="og: https://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <title>Dry Cleaning in Charlotte Revolutionized! Locker Revolution</title>

        <!--[if lt IE 9]>
       <script src="/js/lockerrevolution/html5.js" type="text/javascript"></script>
       <![endif]-->

        <script type="text/javascript">
            document.documentElement.className = 'js';
        </script>


        <!-- This site is optimized with the Yoast WordPress SEO plugin v2.1.1 - https://yoast.com/wordpress/plugins/seo/ -->
        <meta name="description" content="Drop off your laundry in one of our secure 24/7-accessible lockers around Charlotte and place your order. We’ll notify you when your dry cleaning is ready."/>
        <link rel="canonical" href="http://www.lockerrevolution.com/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Dry Cleaning in Charlotte Revolutionized! Locker Revolution" />
        <meta property="og:description" content="Drop off your laundry in one of our secure 24/7-accessible lockers around Charlotte and place your order. We’ll notify you when your dry cleaning is ready." />

        <meta property="og:site_name" content="Charlotte Dry Cleaning - Locker Revolution" />
        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:description" content="Drop off your laundry in one of our secure 24/7-accessible lockers around Charlotte and place your order. We’ll notify you when your dry cleaning is ready."/>
        <meta name="twitter:title" content="Dry Cleaning in Charlotte Revolutionized! Locker Revolution"/>
        <meta name="twitter:site" content="@LockerRev"/>
        <meta name="twitter:domain" content="Charlotte Dry Cleaning - Locker Revolution"/>
        <meta name="twitter:creator" content="@LockerRev"/>
        <meta itemprop="name" content="Dry Cleaning in Charlotte Revolutionized! Locker Revolution">
        <meta itemprop="description" content="Drop off your laundry in one of our secure 24/7-accessible lockers around Charlotte and place your order. We’ll notify you when your dry cleaning is ready.">
        <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/www.lockerrevolution.com\/","name":"Locker Revolution","alternateName":"LockerRevolution.com","potentialAction":{"@type":"SearchAction","target":"http:\/\/www.lockerrevolution.com\/?s={search_term}","query-input":"required name=search_term"}}</script>
        <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","name":"Locker Revolution","url":"http:\/\/www.lockerrevolution.com","logo":"http:\/\/www.lockerrevolution.com\/wp-content\/uploads\/logo_400x111.png","sameAs":["https:\/\/www.facebook.com\/LockerRevolution","https:\/\/www.linkedin.com\/company\/locker-revolution"]}</script>
        <meta name="alexaVerifyID" content="WNPYvET4GxEcF2geLckKG6qChBM" />
        <meta name="msvalidate.01" content="9F9266A296FDF7259B2B329720B3E72E" />
        <meta name="google-site-verification" content="Zy9pRPRY0y1NsbSVJd_HZltmnhBwY6PLg9UjXU3R8rE" />
        <meta name="yandex-verification" content="7fb294c0571bb14f" />
        <!-- / Yoast WordPress SEO plugin. -->
        <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <meta content="Divi v.2.3.2" name="generator"/><style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel='stylesheet' id='contact-form-7-css'  href='/css/lockerrevolution/form_styles.css' type='text/css' media='all' />
        <link rel='stylesheet' id='divi-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,700,800&#038;subset=latin,latin-ext' type='text/css' media='all' />
        <link rel='stylesheet' id='et-gf-francois-one-css'  href='https://fonts.googleapis.com/css?family=Francois+One:400&#038;subset=latin,latin-ext' type='text/css' media='all' />
        <link rel='stylesheet' id='et-gf-lato-css'  href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic&#038;subset=latin' type='text/css' media='all' />
        <link rel='stylesheet' id='divi-style-css'  href='/css/lockerrevolution/style.css' type='text/css' media='all' />
        <link rel='stylesheet' id='et-shortcodes-css-css'  href='/css/lockerrevolution/shortcodes.css' type='text/css' media='all' />
        <link rel='stylesheet' id='et-shortcodes-responsive-css-css'  href='/css/lockerrevolution/shortcodes_responsive.css' type='text/css' media='all' />
        <!--<script type='text/javascript' src='http://www.lockerrevolution.com/wp-includes/js/jquery/jquery.js?ver=1.11.2'></script>
        <script type='text/javascript' src='http://www.lockerrevolution.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>-->
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
         <link rel="stylesheet" href="/css/lockerrevolution/custom.css">
        <?= $style ?>
        <?= $_styles ?>
        <meta name="generator" content="WordPress 4.2.1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />	<style>
            a { color: #008aa6; }
            body { color: #666666; }
            .et_pb_counter_amount, .et_pb_featured_table .et_pb_pricing_heading, .et_quote_content, .et_link_content, .et_audio_content { background-color: #92c542; }
            #main-header, #main-header .nav li ul, .et-search-form, #main-header .et_mobile_menu { background-color: #ffffff; }
            #top-header, #et-secondary-nav li ul { background-color: #92c542; }
            .woocommerce a.button.alt, .woocommerce-page a.button.alt, .woocommerce button.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt, .woocommerce-page input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce-page #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page #content input.button.alt, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce-message, .woocommerce-error, .woocommerce-info { background: #92c542 !important; }
            #et_search_icon:hover, .mobile_menu_bar:before, .footer-widget h4, .et-social-icon a:hover, .comment-reply-link, .form-submit input, .et_pb_sum, .et_pb_pricing li a, .et_pb_pricing_table_button, .et_overlay:before, .entry-summary p.price ins, .woocommerce div.product span.price, .woocommerce-page div.product span.price, .woocommerce #content div.product span.price, .woocommerce-page #content div.product span.price, .woocommerce div.product p.price, .woocommerce-page div.product p.price, .woocommerce #content div.product p.price, .woocommerce-page #content div.product p.price, .et_pb_member_social_links a:hover { color: #92c542 !important; }
            .woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before, .et_pb_widget li a:hover, .et_pb_bg_layout_light .et_pb_promo_button, .et_pb_bg_layout_light .et_pb_more_button, .et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active, .et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active, .et_pb_gallery .et_pb_gallery_pagination ul li a.active, .wp-pagenavi span.current, .wp-pagenavi a:hover, .et_pb_contact_submit, .et_password_protected_form .et_submit_button, .et_pb_bg_layout_light .et_pb_newsletter_button, .nav-single a, .posted_in a { color: #92c542 !important; }
            .et-search-form, .nav li ul, .et_mobile_menu, .footer-widget li:before, .et_pb_pricing li:before, blockquote { border-color: #92c542; }
            #main-footer { background-color: #008aa6; }
            #top-menu a { color: #008aa6; }
            #top-menu li.current-menu-ancestor > a, #top-menu li.current-menu-item > a, .bottom-nav li.current-menu-item > a { color: #92c542; }
            h1, h2, h3, h4, h5, h6 { font-family: 'Francois One', Helvetica, Arial, Lucida, sans-serif; }body, input, textarea, select { font-family: 'Lato', Helvetica, Arial, Lucida, sans-serif; }	</style>
        <link rel="shortcut icon" href="/images/lockerrevolution/favicon.png" /><style type="text/css" id="custom-background-css">
            body.custom-background { background-color: #f0f0f0; }
        </style>
        <meta name="google-site-verification" content="Zy9pRPRY0y1NsbSVJd_HZltmnhBwY6PLg9UjXU3R8rE" />
        <!-- BEGIN GADWP v4.7.1 Universal Tracking - https://deconf.com/google-analytics-dashboard-wordpress/ -->
       
        <!-- END GADWP Universal Tracking -->

        <style type="text/css" id="et-custom-css">
            #footer-info {
                display: none;
                visibility: hidden;
            }

            #main-header {
                line-height: 23px;
                font-weight: 500;
                top: 0px;
                background-color: #F0F0F0 !important;
                width: 100%;
                padding: 18px 0px 0px;
                padding-top: 18px;
                padding-right-value: 0px;
                padding-bottom: 0px;
                padding-left-value: 0px;
                padding-left-ltr-source: physical;
                padding-left-rtl-source: physical;
                padding-right-ltr-source: physical;
                padding-right-rtl-source: physical;
                min-height: 43px;
                box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.1);
                transition: all 0.2s ease-in-out 0s;
                transition-property: all;
                transition-duration: 0.2s;
                transition-timing-function: ease-in-out;
                transition-delay: 0s;
                position: relative;
                z-index: 99999;
            }

            #main-content, .et_pb_section {
                background-color: #F0F0F0 !important;
            }

            a.big-button {
                font-weight: bolder !important;
                font-size: 24px;
                padding: 14px 24px;
                padding-top: 14px;
                padding-right-value: 24px;
                padding-bottom: 14px;
                padding-left-value: 24px;
                padding-left-ltr-source: physical;
                padding-left-rtl-source: physical;
                padding-right-ltr-source: physical;
                padding-right-rtl-source: physical;
                margin: 8px 6px 8px 0px;
                margin-top: 8px;
                margin-right-value: 6px;
                margin-bottom: 8px;
                margin-left-value: 0px;
                margin-left-ltr-source: physical;
                margin-left-rtl-source: physical;
                margin-right-ltr-source: physical;
                margin-right-rtl-source: physical;
                width: 200px !important;
            }

            a.smallgreen, a.biggreen {
                color: #FFFFFF !important;
                background: linear-gradient(to bottom, #90c445 0%, #80b03a 100%) repeat scroll 0% 0% transparent;
                background-color: transparent;
                background-image: linear-gradient(to bottom, #90c445 0%, #80b03a 100%);
                background-repeat: repeat;
                background-attachment: scroll;
                background-position: 0% 0%;
                background-clip: border-box;
                background-origin: padding-box;
                background-size: auto auto;
                border: 1px solid #79aa3b;
                border-top-width: 1px;
                border-right-width-value: 1px;
                border-right-width-ltr-source: physical;
                border-right-width-rtl-source: physical;
                border-bottom-width: 1px;
                border-left-width-value: 1px;
                border-left-width-ltr-source: physical;
                border-left-width-rtl-source: physical;
                border-top-style: solid;
                border-right-style-value: solid;
                border-right-style-ltr-source: physical;
                border-right-style-rtl-source: physical;
                border-bottom-style: solid;
                border-left-style-value: solid;
                border-left-style-ltr-source: physical;
                border-left-style-rtl-source: physical;
                border-top-color: #79aa3b;
                border-right-color-value: rgb(77, 171, 70);
                border-right-color-ltr-source: physical;
                border-right-color-rtl-source: physical;
                border-bottom-color: #79aa3b;
                border-left-color-value: rgb(77, 171, 70);
                border-left-color-ltr-source: physical;
                border-left-color-rtl-source: physical;
                -moz-border-top-colors: none;
                -moz-border-right-colors: none;
                -moz-border-bottom-colors: none;
                -moz-border-left-colors: none;
                border-image-source: none;
                border-image-slice: 100% 100% 100% 100%;
                border-image-width: 1 1 1 1;
                border-image-outset: 0 0 0 0;
                border-image-repeat: stretch stretch;
            }

            a.smallgreen:hover, a.biggreen:hover {
                color: #008dab !important;
            }

            #top-menu a {
                color: #008AA6;
                font-weight: bolder;
                font-size: 16px;
            }

            #top-menu a:hover {
                color: #8ec63d;
                font-weight: bolder;
                font-size: 18px;
            }

            body {
                font-size: 18px;
                background-color: #FFF;
                line-height: 1.7em;
                font-weight: 500;
                -moz-osx-font-smoothing: grayscale;
            }

            h1, h2, h3, h4, h5, h6 {
                font-weight: bolder;
            }

            h1 {
                font-size: 36px;
                color: #008dab;
            }

            h4 {
                font-size: 24px;
                color: #008dab;
            }

            .et_pb_blurb h4 a {
                text-decoration: none;
                text-decoration-color: -moz-use-text-color;
                text-decoration-line: none;
                text-decoration-style: solid;
                color: #8ac642;
            }

            .et_pb_blurb_position_left .et_pb_main_blurb_image {
                position: absolute;
                top: -3px;
                left: 0px;
                width: 32px;
            }

            .wpgmza-basic-listing-image-holder {
                display: none;
                visibility: hidden;
            }

            input[type="text"], input.text, input.title, textarea, select {
                background-color: #FFFFFF !important;
            }

            input[type=email],input[type=text], input.text, input.title, textarea, select {
                background-color: #FFFFFF;
                border: none;
                -moz-border-radius: 0 ;
                -webkit-border-radius: 0 ;
                border-radius: 0 ;
                font-size: 14px;
                color: #999 !important;
                padding: 16px ;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            .wpcf7 p:nth-child(1),.wpcf7 p:nth-child(2),.wpcf7 p:nth-child(4) {
                width: 49%;
                float: left;
            }

            .wpcf7 p:nth-child(5), textarea {
                width: 100%;
            }

            input.wpcf7-form-control.wpcf7-submit {
                font-size: 20px;
                font-weight: 500;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
                padding: 6px 20px;
                line-height: 1.7em;
                background: transparent;
                border: 2px solid;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
                -moz-transition: all 0.2s;
                -webkit-transition: all 0.2s;
                transition: all 0.2s;
                position: relative;
                margin-top: -1px;
            }

            input.wpcf7-form-control.wpcf7-submit:hover {
                padding: 6px 20px ;
                background: rgba( 0, 0, 0, 0.05 );
                border: 2px solid transparent;
            }


            @media only screen and (max-width: 480px) {
                #logo {
                    margin-bottom: 0px;
                    max-height: 72px;
                }
                .et_pb_slider {
                    display: none;
                    visibility: hidden;
                }
                .wpcf7 p:nth-child(1),.wpcf7 p:nth-child(3),.wpcf7 p:nth-child(4) {
                    width: 90%;
                    float: left;
                }
            }

        </style>
    </head>
    <body class="home page page-id-46 page-template-default custom-background et_cover_background et_header_style_centered linux et_pb_pagebuilder_layout et_full_width_page chrome">
        <div id="page-container">
            <header id="main-header" class="et_nav_text_color_dark">
                <div class="container clearfix">
                    <a href="http://www.lockerrevolution.com/">
                        <img src="/images/lockerrevolution/logo_400x111.png" alt="Charlotte Dry Cleaning - Locker Revolution" id="logo" />
                    </a>

                    <div id="et-top-navigation">
                        <nav id="top-menu-nav">
                            <ul id="top-menu" class="nav"><li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a href="http://www.lockerrevolution.com/how-it-works/">How It Works</a></li>
                                <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="http://www.lockerrevolution.com/pricing/">Pricing</a></li>
                                <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a href="http://www.lockerrevolution.com/locations/">Locations</a></li>
                                <li id="menu-item-181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181"><a href="http://www.lockerrevolution.com/coupons/">Coupons</a></li>
                                <li id="menu-item-307" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-307"><a href="http://www.lockerrevolution.com/package-lockers/">Packages Made Easy</a></li>
                                <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a href="http://www.lockerrevolution.com/faq/">FAQs</a></li>
                                <li id="menu-item-177" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-177"><a href="http://www.lockerrevolution.com/about/">Our Story</a></li>
                                <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a href="http://www.lockerrevolution.com/contact/">Contact</a></li>
                                <li id="menu-item-88" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-88"><a href="https://myaccount.lockerrevolution.com/register"><font color="#8ac642">Sign Up/Login</font></a></li>
                            </ul>					</nav>



                        <div id="et_mobile_nav_menu">
                            <a href="#" class="mobile_nav closed">
                                <span class="select_page">Select Page</span>
                                <span class="mobile_menu_bar"></span>
                            </a>
                        </div>				</div> <!-- #et-top-navigation -->
                </div> <!-- .container -->
            </header> <!-- #main-header -->

            <div id="et-main-area">
                <div id="main-content">



                    <article id="post-46" class="post-46 page type-page status-publish hentry">


                        <div class="entry-content">
                             <?= $content ?>
                        </div>


                    </article> <!-- .et_pb_post -->



                </div> <!-- #main-content -->


                <footer id="main-footer">

                    <div class="container">
                        <div id="footer-widgets" class="clearfix">
                            <div class="footer-widget"><div id="text-2" class="fwidget et_pb_widget widget_text"><h4 class="title">How It Works</h4>			<div class="textwidget"><ol>
                                            <li>Leave your clothes.</li>
                                            <li>Place your order.</li>
                                            <li>Get text and pick up clothes.</li>
                                        </ol>
                                        <br>
                                        <div align="center"><a href='http://www.lockerrevolution.com/how-it-works/' class='big-button biggreen'>Learn More</a></div></div>
                                </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="text-3" class="fwidget et_pb_widget widget_text"><h4 class="title">Find a Locker</h4>			<div class="textwidget">Find a locker near you.<br />
                                            <a href="http://www.lockerrevolution.com/locations/"><img src="/images/lockerrevolution/locations.jpg" alt="Find a locker."></a></div>
                                </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="text-4" class="fwidget et_pb_widget widget_text"><h4 class="title">Get the App</h4>			<div class="textwidget"><a name="app"></a>Download our App from Google Play.<br />
                                        <!---<br />
                                        <a href="https://itunes.apple.com"><img src="http://www.lockerrevolution.com/wp-content/uploads/itunes.png" alt="Get the app!"></a>-->
                                        <br />
                                        <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.lockerRevolution" target="_blank"><img src="/images/lockerrevolution/android.png" alt="Get the app!"></a></div>
                                </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget last"><div id="text-5" class="fwidget et_pb_widget widget_text"><h4 class="title">Connect With Us</h4>			<div class="textwidget"><br />
                                        <div class="et_pb_blurb et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_position_left">
                                            <div class="et_pb_blurb_content">
                                                <div class="et_pb_main_blurb_image">
                                                    <a target="_blank" href="https://www.facebook.com/LockerRevolution">
                                                        <span class="et-pb-icon et-waypoint et_pb_animation_off et-pb-icon-circle et-pb-icon-circle-border et-animated" style="color: #ffffff; background-color: #008aa6; border-color: #ffffff;">
                                                            
                                                        </span>
                                                    </a>
                                                </div>
                                                <h4 style="color:#FFF;">
                                                    <a target="_blank" href="https://www.facebook.com/LockerRevolution" style="color:#FFF">
                                                        <span style="color:#FFF"></span>
                                                        Facebook
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>

                                        <div class="et_pb_blurb et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_position_left">
                                            <div class="et_pb_blurb_content">
                                                <div class="et_pb_main_blurb_image">
                                                    <a target="_blank" href="https://twitter.com/LockerRev">
                                                        <span class="et-pb-icon et-waypoint et_pb_animation_off et-pb-icon-circle et-pb-icon-circle-border et-animated" style="color: #ffffff; background-color: #008aa6; border-color: #ffffff;">
                                                            
                                                        </span>
                                                    </a>
                                                </div>
                                                <h4 style="color:#FFF;">
                                                    <a target="_blank" href="https://twitter.com/LockerRev" style="color:#FFF">
                                                        <span style="color:#FFF"></span>
                                                        Twitter
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>

                                        <div class="et_pb_blurb et_pb_bg_layout_light et_pb_text_align_left et_pb_blurb_position_left">
                                            <div class="et_pb_blurb_content">
                                                <div class="et_pb_main_blurb_image">
                                                    <a target="_blank" href="https://www.linkedin.com/company/locker-revolution">
                                                        <span class="et-pb-icon et-waypoint et_pb_animation_off et-pb-icon-circle et-pb-icon-circle-border et-animated" style="color: #ffffff; background-color: #008aa6; border-color: #ffffff;">
                                                            
                                                        </span>
                                                    </a>
                                                </div>
                                                <h4 style="color:#FFF;">
                                                    <a target="_blank" href="https://www.linkedin.com/company/locker-revolution" style="color:#FFF">
                                                        <span style="color:#FFF"></span>
                                                        LinkedIn
                                                    </a>
                                                </h4>
                                            </div>
                                        </div></div>
                                </div> <!-- end .fwidget --><div id="text-6" class="fwidget et_pb_widget widget_text">			<div class="textwidget"><span style="color:#FFFFFF; font-size:10px;"><a href="http://www.lockerrevolution.com/terms/" style="color:#FFFFFF; font-size:10px;">Terms of Use</a> |  <a href="http://www.lockerrevolution.com/privacy/" style="color:#FFFFFF; font-size:10px;">Privacy Policy</a></span></div>
                                </div> <!-- end .fwidget --></div> <!-- end .footer-widget -->	</div> <!-- #footer-widgets -->
                    </div>	<!-- .container -->


                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <ul class="et-social-icons">


                            </ul>
                            <p id="footer-info">Designed by <a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a> | Powered by <a href="http://www.wordpress.org">WordPress</a></p>
                        </div>	<!-- .container -->
                    </div>
                </footer> <!-- #main-footer -->
            </div> <!-- #et-main-area -->


        </div> <!-- #page-container -->
        
        <script type='text/javascript' src='/js/lockerrevolution/jquery.form.min.js'></script>
        <!--<script type='text/javascript'>
            /* <![CDATA[ */
            var _wpcf7 = {"loaderUrl": "http:\/\/www.lockerrevolution.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif", "sending": "Sending ..."};
            /* ]]> */
        </script>-->
        <script type='text/javascript' src='/js/lockerrevolution/scripts.js'></script>
        <script type='text/javascript' src='/js/lockerrevolution/jquery.fitvids.js'></script>
        <script type='text/javascript' src='/js/lockerrevolution//waypoints.min.js'></script>
        <script type='text/javascript' src='/js/lockerrevolution/jquery.magnific-popup.js'></script>

        <script type='text/javascript' src='/js/lockerrevolution/custom.js'></script>
        <script type='text/javascript' src='/js/lockerrevolution/smoothscroll.js'></script>
    </body>
</html>