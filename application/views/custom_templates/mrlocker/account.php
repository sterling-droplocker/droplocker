<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="/css/mrlocker/styles.css" />
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>

        <?= $style ?>
        <?= $_styles ?>
    </head>
    <body>
        <div id="wrap">
        <div id="header" class="row">
            <div class="container">
            <div class="span11 offset1">
                <ul id="menu" class="unstyled">
                    <li><a href="http://www.mrlocker.com/">Home</a></li>
                    <li><a href="http://www.mrlocker.com/#!about/cm8a">Who is Mr. Locker?</a></li>
                    <li><a href="http://www.mrlocker.com/#!services/c46c">Our Services</a></li>
                    <li><a href="http://www.mrlocker.com/#!locations/c1985">Locations</a></li>
                    <li><a href="http://www.mrlocker.com/#!investors/c1jtu">Investors</a></li>
                    <li><a href="http://www.mrlocker.com/#!copy-of-who-is-mr-locker/c17qp">Get Lockers</a></li>
                    <li><a href="http://www.mrlocker.com/#!support/c1d94">Support</a></li>
                </ul>
            </div>
            </div>
        </div>
        <div id="main_content" class="row">
            <div class="container">
                <div class="spann11">
                    <?= $content; ?>
                </div>
            </div>

        </div>
        <div id="push"></div>
        </div>
        <div id="footer">
            <div class="container">
                <div class="span3 offset1">
                   
                        <ul id="contact-info" class="unstyled">
                            <li>T: 954-866-3667</li>
                            <li>E:  support@mrlocker.com</li>
                            
                        </ul>
                </div>
                <div class="span4">
                    <ul id="social" class="unstyled">
                        <li><a href="https://www.facebook.com/mrlocker247" target="_blank"><img src="/images/mrlocker/facebook.png" /></a></li>
                        <li><a href="https://instagram.com/mr.lockerdrycleaner/" target="_blank"><img src="/images/mrlocker/instagram.png" /></a></li>
                        <li><a href="https://twitter.com/Mrlockerdc" target="_blank"><img src="/images/mrlocker/twitter.png" /></a></li>
                        <li><a href="https://www.youtube.com/channel/UClYExbDMAmMI-HxtOMAqiUw" target="_blank"><img src="/images/mrlocker/youtube.png" /></a></li>
                    </ul>
                </div>
                <div class="span3">
                    <ul id="mobile-apps" class="unstyled">
                        <li><a href="https://itunes.apple.com/us/app/mr-locker/id978815060?mt=8" target="_blank"><img src="/images/mrlocker/app_store.png" /></a></li>
                        <li><a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.mrLocker&hl=en" target="_blank"><img src="/images/mrlocker/google_play.png" /></a></li>
                    </ul>

                    <div id="footer-copyright">&copy; Copyright Mr. Locker <?=date('Y');?> All rights reserved.</div>
                </div>
            </div>
        </div>
        
    </body>
</html>