<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>

        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);

    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>

        <link rel="stylesheet" href="/css/voilalaundry/style.css">



        <style>
        #header{
            width:100%;
            text-align:center;
            /*background-color:#d9edf7;*/
           /* border-bottom:1px solid #73a5bc;*/
            margin-bottom: 20px;
        }
        #inner-header{
            text-align:left;
            width:970px;
            margin:0 auto;
        }

        #footer{
            width:100%;
            text-align:center;
            height: 26px;
        }
        #inner-footer{
            text-align:left;
            width:970px;
            margin:0 auto;
        }

        .s5repeaterButtonlabel {
            display: inline-block;
            padding: 0 10px;
            color: #494848;
            transition: color 0.4s ease 0s;
        }
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            vertical-align: baseline;
            background: transparent;
        }
        user agent stylesheetp {
            display: block;
            -webkit-margin-before: 1em;
            -webkit-margin-after: 1em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
        }
        Inherited from
        Style Attribute {
            text-align: center;
        }
        Inherited from
        .s5repeaterButton {
            height: 100%;
            position: relative;
            box-sizing: border-box;
            display: inline-block;
            cursor: pointer;
            font: normal normal normal 13.5px/1.4em spinnaker,sans-serif;
        }
        a {
            cursor: pointer;
            text-decoration: none;
        }
        user agent stylesheeta:-webkit-any-link {
            color: -webkit-link;
            text-decoration: underline;
            cursor: auto;
        }
        Inherited from
        Style Attribute {
            height: 24px;
            display: inline-block;
            text-align: center;
            overflow: visible;
        }
        .s5itemsContainer {
            width: -webkit-calc(100% - 0px);
            width: calc(100% - 0px);
            white-space: nowrap;
            position: relative;
            overflow: hidden;
        }
        Inherited from
        body {
            /* line-height: 1; */
            font-size: 10px;
            font-family: Arial, Helvetica, sans-serif;
        }
        .s3 p {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        .font_9 {
            font: normal normal normal 14px/1.4em spinnaker,sans-serif;
            color: rgba(73,72,72,1);
        }

        .font_7 {
                font: normal normal normal 17px/1.4em spinnaker,sans-serif;
                color: rgba(73,72,72,1);
            }
         body {
                font: normal normal normal 12.5px/1.3em spinnaker,sans-serif
         }
        </style>

    </head>
    <body>

        <div id='header'>
            <div style="position: relative; margin: auto;top: 0px; height: 96px; width: 980px; left: 0px;" class="s4" id="SITE_HEADER" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER">
                <div id="SITE_HEADERscreenWidthBackground" class="s4screenWidthBackground" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.0" style="width: 1349px; left: -185px;"><div class="s4_bg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.0.0"></div></div><div id="SITE_HEADERcenteredContent" class="s4centeredContent" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1"><div id="SITE_HEADERbg" class="s4bg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.0"></div><div id="SITE_HEADERinlineContent" class="s4inlineContent" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1"><div id="ibrscets" data-menuborder-y="0" data-menubtn-border="0" data-ribbon-els="0" data-label-pad="0" data-ribbon-extra="0" style="position: absolute; top: 48px; height: 24px; width: 979px; left: 113px;" class="s5" data-state="center notMobile" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets" data-dropmode="dropDown"><div style="height: 24px; display: inline-block; text-align: center; overflow: visible;" id="ibrscetsitemsContainer" class="s5itemsContainer" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0"><a style="display: inherit; position: relative; box-sizing: border-box; width: 142px; height: 24px; overflow: visible;" data-listposition="center" href="http://www.voilalaundry.com/#!hiw/cani" target="_self" class="s5repeaterButton" data-state="menu  idle link notMobile" id="ibrscets0" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$0" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$0.0"><div style="text-align:center;" id="ibrscets0bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$0.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets0label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$0.0.0.0">HOW&nbsp;IT WORKS</p></div></div></a><a style="display: inherit; position: relative; box-sizing: border-box; width: 100px; height: 24px; overflow: visible;" data-listposition="center" href="http://www.voilalaundry.com/#!price-list/c1npb" target="_self" class="s5repeaterButton" data-state="menu  idle link notMobile" id="ibrscets1" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$1" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$1.0"><div style="text-align:center;" id="ibrscets1bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$1.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets1label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$1.0.0.0">SERVICES</p></div></div></a><a style="display: inherit; position: relative; box-sizing: border-box; width: 69px; height: 24px; overflow: visible;" data-listposition="center" href="http://www.voilalaundry.com/#!frequentquestions/c1vc9" target="_self" class="s5repeaterButton" data-state="menu selected idle link notMobile" id="ibrscets2" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$2" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$2.0"><div style="text-align:center;" id="ibrscets2bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$2.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets2label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$2.0.0.0">FAQS</p></div></div></a><a style="display: inherit; position: relative; box-sizing: border-box; width: 105px; height: 24px; overflow: visible;" data-listposition="center" href="http://www.voilalaundry.com/#!our-story/c2sa" target="_self" class="s5repeaterButton" data-state="menu  idle link notMobile" id="ibrscets3" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$3" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$3.0"><div style="text-align:center;" id="ibrscets3bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$3.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets3label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$3.0.0.0">ABOUT&nbsp;US</p></div></div></a><a style="display: inherit; position: relative; box-sizing: border-box; width: 99px; height: 24px; overflow: visible;" data-listposition="center" href="http://www.voilalaundry.com/#!contractinfo/c2x5" target="_self" class="s5repeaterButton" data-state="menu  idle link notMobile" id="ibrscets4" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$4" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$4.0"><div style="text-align:center;" id="ibrscets4bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$4.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets4label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$4.0.0.0">CONTACT</p></div></div></a><a style="display: inherit; position: relative; box-sizing: border-box; width: 89px; height: 24px; overflow: visible;" data-listposition="center" href="https://myaccount.voilalaundry.com/register" target="_self" class="s5repeaterButton" data-state="menu  idle link notMobile" id="ibrscets5" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$5" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$5.0"><div style="text-align:center;" id="ibrscets5bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$5.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets5label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$5.0.0.0">SIGN&nbsp;UP</p></div></div></a><a style="display: inherit; position: relative; box-sizing: border-box; width: 75px; height: 24px; overflow: visible;" data-listposition="center" href="https://myaccount.voilalaundry.com/login" target="_blank" class="s5repeaterButton" data-state="menu  idle link notMobile" id="ibrscets6" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$6" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$6.0"><div style="text-align:center;" id="ibrscets6bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$6.0.0"><p style="line-height: 24px; text-align: center;" id="ibrscets6label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.$6.0.0.0">LOGIN</p></div></div></a><a style="display: inline-block; position: absolute; box-sizing: border-box; height: 0px; overflow: hidden;" data-listposition="center" class="s5repeaterButton" data-state="menu  idle header notMobile" id="ibrscets__more__" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.7" data-originalgapbetweentextandbtn="10"><div class="s5repeaterButton_gapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.7.0"><div style="text-align:center;" id="ibrscets__more__bg" class="s5repeaterButtonbg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.7.0.0"><p style="line-height:24px;text-align:center;" id="ibrscets__more__label" class="s5repeaterButtonlabel" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.0.7.0.0.0">More</p></div></div></a></div><div id="ibrscetsmoreButton" class="s5moreButton" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.1"></div><div style="visibility: hidden; left: 461px; right: auto; bottom: auto;" data-drophposition="" data-dropalign="center" id="ibrscetsdropWrapper" class="s5dropWrapper" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.2"><div style="visibility: hidden; left: 461px; right: auto;" id="ibrscetsmoreContainer" class="s5moreContainer" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$ibrscets.2.0"></div></div></div><div style="position: absolute; top: 2px; height: 86px; width: 240px; left: 13px;" data-exact-height="NaN" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="computer.png" class="s6" data-state="noTouch" id="idnclxnw" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$idnclxnw"><a style="width: 240px; height: 86px; cursor: pointer;" href="http://www.voilalaundry.com/" target="_self" id="idnclxnwlink" class="s6link" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$idnclxnw.0"><div id="idnclxnwimg" style="width: 240px; height: 86px; position: relative;" class="s6img" data-state="loaded" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$idnclxnw.0.0"><img alt="" style="width: 240px; height: 86px; object-fit: contain;" src="/css/voilalaundry/images/7cd3e7_bab9b0cfaa5f42a29f90f5ec8ba0cf76.png_srb_p_240_86_75_22_0.50_1.20_0.00_png_srb" id="idnclxnwimgimage" class="s6imgimage" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_HEADER.1.1.$idnclxnw.0.0.0"></div></a>

                </div></div></div></div>
        </div>

        <div style='min-height:600px;'>
        <?php
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content
        ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>

        <div>
         <div id='footer'>
            <div style="position: relative; top: auto; height: 46px; width: 980px; left: 0px; bottom: 0px;" class="s2" data-state=" fixedPosition" id="SITE_FOOTER" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER"><div id="SITE_FOOTERscreenWidthBackground" class="s2screenWidthBackground" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.0" style="width: 1349px; left: -185px;"><div class="s2_bg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.0.0"></div></div><div id="SITE_FOOTERcenteredContent" class="s2centeredContent" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1"><div id="SITE_FOOTERbg" class="s2bg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.0"></div><div id="SITE_FOOTERinlineContent" class="s2inlineContent" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1"><div style="position: absolute; top: 25px; width: 980px; left: 10px;" class="s3" id="ibrtvlaa" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$ibrtvlaa"><p class="font_9" style="font-size: 12px;"><span style="font-size:12px;">&copy; 2015, Voila Laundry, LLC </span></p>
            </div></div></div></div>
        </div>
        </div>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>
    </body>


</html>