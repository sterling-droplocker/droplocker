<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/comfortcube/style.css">

    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>
</head>
<body>
<div id="fb-root"></div>
<div class="container">
    <div>
        <!-- End of facebook code -->

        <?
        //if this is using the exterior pages - make sure NOT to hide the header/footer
        // $responsive = '';
        // $uri_string = $this->uri->uri_string();
        // if( strpos($uri_string,'main/') === false ){
        $responsive = 'hidden-phone hidden-tablet';
        // }
        ?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="http://www.comfortcube.com/">
                        <img src="/images/comfortcube/ds-logo_1.png" />
                    </a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="http://www.comfortcube.com/">Home</a></li>
                            <li><a href="http://www.comfortcube.com/how-cube-works.html">How Cube Works</a></li>
                            <li><a href="http://www.comfortcube.com/clothing-care.html">Clothing Care</a></li>
                            <li><a href="http://www.comfortcube.com/developersmanagers.html">Developers/Managers</a></li>
                            <li><a href="http://www.comfortcube.com/about-cube.html">About Cube</a></li>
                            <li><a href="http://www.comfortcube.com/contact.html">Contact</a></li>  
                            <?php if (empty($this->customer)): ?>
                                <li><a href="/login">Log In</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container">
                    <div id="" class="row-fluid">
                        <div class="span12 center">
                            <div class="navbar">
                                <div class="navbar-inner-menu">
                                    <a href="http://www.comfortcube.com/">
                                        <img src="/images/comfortcube/ds-logo_1.png" class="logo">
                                    </a>
                                    <ul class="nav">
                                        <li><a class="navText" href="http://www.comfortcube.com/">Home</a></li>
                                        <li><a class="navText" href="http://www.comfortcube.com/how-cube-works.html">How Cube Works</a></li>
                                        <li><a class="navText" href="http://www.comfortcube.com/clothing-care.html">Clothing Care</a></li>
                                        <li><a class="navText" href="http://www.comfortcube.com/developersmanagers.html">Developers/Managers</a></li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle navText" data-toggle="dropdown" href="#">more...</a>
                                            <ul class="dropdown-menu">
                                                <li><a class="navText" href="http://www.comfortcube.com/about-cube.html">About Cube</a></li>
                                                <li><a class="navText" href="http://www.comfortcube.com/contact.html">Contact</a></li>
                                            </ul>
                                        </li>
                                        <?php if (empty($this->customer)): ?>
                                            <li><a class="navText" href="/login">Log In</a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" style='margin-top: 45px; background: #FFF; padding: 10px'>
            <div class="span10 center">
                <?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="footer">
                <div class="span4 map">
                    <h2>Comfort Cube Retail Location</h2>
                    <div class="wsite-map">
                        <iframe allowtransparency="true" frameborder="0" scrolling="no" style="width: 100%; height: 150px; margin-top: 10px; margin-bottom: 10px;" src="//www.weebly.com/weebly/apps/generateMap.php?map=google&amp;elementid=521154750842165802&amp;ineditor=0&amp;control=3&amp;width=auto&amp;height=150px&amp;overviewmap=0&amp;scalecontrol=0&amp;typecontrol=0&amp;zoom=15&amp;long=-87.91631910000001&amp;lat=43.0418635&amp;domain=www&amp;point=1&amp;align=1&amp;reseller=false"></iframe>
                    </div>
                </div>
                <div class="span5 testimonies">
                    <h2>What Our Clients Are Saying<br />
                        <em>"I went to my neighborhood dry cleaner one and they were closed. Thankfully I did not have
                            any clothes still there. Decided to try out Comfort Cube and it was a good decision because
                            they have made my life easier. I do not even have to worry. They provide great service"
                            - Mechanic Dan
                        </em>
                    </h2>
                </div>
                <div class="span3 contact">
                    <h2>Contact Us</h2>

                    <div class="wsite-social">
                        <span class="wsite-social wsite-social-default">
                            <a class="first-child wsite-social-item wsite-social-facebook" href="//facebook.com" target="_blank"><span class="wsite-social-item-inner"></span></a>
                            <a class="wsite-social-item wsite-social-twitter" href="//twitter.com" target="_blank"><span class="wsite-social-item-inner"></span></a>
                            <a class="wsite-social-item wsite-social-linkedin" href="//linkedin.com" target="_blank"><span class="wsite-social-item-inner"></span></a>
                            <a class="last-child wsite-social-item wsite-social-mail" href="mailto:info@email.com" target="_blank"><span class="wsite-social-item-inner"></span></a>
                        </span>
                    </div>

                    <div>
                        <form enctype="multipart/form-data" action="http://www.comfortcube.com/ajax/apps/formSubmitAjax.php" method="POST" id="form-621538308391182485" accept-charset="UTF-8" target="form-621538308391182485-target-1494261632679">
                            <div id="621538308391182485-form-parent" class="wsite-form-container" style="margin-top:10px;">
                                <ul class="formlist" id="621538308391182485-form-list">
                                    <h2 class="wsite-content-title" style="text-align:left;"><font size="4">Subscribe Today!</font></h2>

                                    <div><div class="wsite-form-field" style="margin:5px 0px 5px 0px;">
                                            <label class="wsite-form-label" for="input-861622303415840056">Email <span class="form-required">*</span></label>
                                            <div class="wsite-form-input-container">
                                                <input id="input-861622303415840056" class="wsite-form-input wsite-input wsite-input-width-370px" type="text" name="_u861622303415840056">
                                            </div>
                                            <div id="instructions-861622303415840056" class="wsite-form-instructions" style="display:none;"></div>
                                        </div></div>
                                </ul>
                            </div>
                            <div style="display:none; visibility:hidden;">
                                <input type="text" name="wsite_subject">
                            </div>
                            <div style="text-align:left; margin-top:10px; margin-bottom:10px;">
                                <input type="hidden" name="form_version" value="2">
                                <input type="hidden" name="wsite_approved" id="wsite-approved" value="approved">
                                <input type="hidden" name="ucfid" value="621538308391182485">
                                <input type="submit" style="position:absolute;top:0;left:-9999px;width:1px;height:1px"><a class="wsite-button" onclick="document.getElementById('form-621538308391182485').submit()"><span class="wsite-button-inner">Submit</span></a>
                            </div>
                        </form><iframe name="form-621538308391182485-target-1494261632679" id="form-621538308391182485-target-1494261632679" style="display: none;"></iframe>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
    <?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

    <?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
    <?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
    <?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>