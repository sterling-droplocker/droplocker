<?php 
$site_url = 'https://laundrylocker.com';
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <!-- Meta Tags -->
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title ?></title>
    <meta name="description" content="Laundry Locker">
    <meta name="keywords" content="Laundry Locker">
    <meta name="author" content="Laundry Locker">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicon -->
    <link rel="icon" href="/images/laundrylocker/favicon.ico" type="image/x-icon">

    <!-- Google Font --> 
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:700" rel="stylesheet">

    <meta name='robots' content='noindex,follow' />
    <link rel='dns-prefetch' href='//maxcdn.bootstrapcdn.com' />
    <link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
    <link rel='dns-prefetch' href='//use.fontawesome.com' />
    <link rel='dns-prefetch' href='//s.w.org' />

    <link rel='stylesheet' id='bootstrap-css'  href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' type='text/css' media='all' />

    <link rel='stylesheet' id='vendor-elegant-icon-css'  href='/css/laundrylocker/elegant-icons/style.css' type='text/css' media='all' />    

    <link rel='stylesheet' id='style-css'  href='/css/laundrylocker/style.css' type='text/css' media='all' />  
    
    <!-- https://github.com/jquery/jquery-migrate/tree/1.x-stable#readme -->
    <!-- https://github.com/jquery/jquery-migrate/blob/1.x-stable/warnings.md -->
    <!-- script type='text/javascript' src='/js/laundrylocker/jquery.js'></script -->
    <!-- script src="http://code.jquery.com/jquery-migrate-1.4.1.js" ></script -->
    
    
    <!-- https://github.com/jquery/jquery-migrate#readme -->
    <!-- https://github.com/jquery/jquery-migrate/blob/master/warnings.md -->
    <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"></script>

    <?php //compatibility layer.. This allow to use boostrap 3+ with boostrap 2x ?>
    <link rel='stylesheet' id='custom-style-css'  href='/css/laundrylocker/bootstrap2x.css' type='text/css' media='all' />

    <script src='/bootstrap/js/bootstrap.min.js'></script>        
    <script type="text/javascript">
      var $ = jQuery.noConflict();
    </script>

  <?= $_scripts ?>
  <?= $javascript ?>
  <?= $style ?>
  <?= $_styles ?>

    <link rel='stylesheet' id='custom-style-css-2'  href='/css/laundrylocker/custom.css?<?=time()?>' type='text/css' media='all' />


   <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-481555-2', 'auto', {allowLinker: true});
            ga('require', 'linker');
            ga('linker:autoLink', ['https://laundrylocker.com']);
            ga('send', 'pageview');
    </script>
</head>
<body id="top" class="home page-template page-template-homepage page-template-homepage-php page page-id-6">             
    <header class="navbar navbar-static-top sticky" id="top">
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#navbar" data-toggle="collapse" type="button"> 
                    <span class="sr-only">Toggle navigation</span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                </button> 
                <a href="/" class="navbar-brand"><img src="/images/laundrylocker/logo.png"/></a> 
            </div>
            <nav class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li > 
                        <a href="/">ACCOUNT</a>
                    </li>
                    <li > 
                        <a href="<?=$site_url?>/how-it-works/">HOW IT WORKS</a>
                    </li>
                    <li > 
                        <a href="<?=$site_url?>/pricing/">PRICING</a>
                    </li>
                    <li > 
                        <a href="<?=$site_url?>/locations/">LOCATIONS</a>
                    </li>
                    <li > 
                        <a href="<?=$site_url?>/faqs/">FAQS</a>
                    </li>
                    <li > 
                        <a href="<?=$site_url?>/contact/">CONTACT</a>
                    </li>
                    <li class="visible-xs"> 
                        <a href="<?=$site_url?>/about-us/">ABOUT US</a>
                    </li>
                    <li class="visible-xs"> 
                        <a href="<?=$site_url?>/business/">FOR BUSINESS</a>
                    </li>
                    <li class="visible-xs"> 
                        <a href="<?=$site_url?>/sustainability/">SUSTAINABILITY</a>
                    </li>
                    <li class="visible-xs"> 
                        <a href="<?=$site_url?>/rewards/">REWARDS</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="page-content" id="bootstrap2x">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pad-top-100 pad-top-50-mobile pad-bot-100 mar-bot-100 no-margin-bottom-mobile">
                    <?=$content?>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 footer-column no-margin-bottom">
                    <ul class="menu-list">
                        <li><a href="<?=$site_url?>/how-it-works/">How it Works</a></li>
                        <li><a href="<?=$site_url?>/pricing/">Pricing</a></li>
                        <li><a href="<?=$site_url?>/locations/">Locations</a></li>
                        <li><a href="<?=$site_url?>/faqs/">FAQs</a></li>
                        <li><a href="<?=$site_url?><?=$site_url?>/contact/">Contact Us</a></li>
                        <li><a href="<?=$site_url?>/terms-and-conditions/">Terms and Conditions</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 footer-column">
                    <ul class="menu-list">
                        <li><a href="<?=$site_url?>/about-us/" style="border:none;">About Us</a></li>
                        <li><a href="<?=$site_url?>/wash-fold/">Wash &amp; Fold</a></li>
                        <li><a href="<?=$site_url?>/business/">For Business</a></li>
                        <li><a href="<?=$site_url?>/sustainability/">Sustainability</a></li>
                        <li><a href="<?=$site_url?>/rewards/">Rewards</a></li>

                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 footer-column text-center">
                    <div class="row visible-xs mar-bot-35 pad-top-10">
                        <div class="col-xs-6">
                            <a target="_blank" href="https://itunes.apple.com/us/app/laundry-locker/id784630564?mt=8"><img class="img-responsive center-block img-left-desktop img-left-tablet" alt="Get it On App Store" src="/images/laundrylocker/app-store-white.svg" style="width:180px;"/></a>
                        </div>
                        <div class="col-xs-6">
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.laundrylocker"><img class="img-responsive center-block img-right-desktop img-right-tablet" alt="Get it On Play Store" src="/images/laundrylocker/google-play-white.svg" style="width:180px;"/></a>
                        </div>
                    </div>
                    <ul class="social-list">
                        <li><a target="_blank" href="https://www.facebook.com/LaundryLocker/"><i class="social_facebook"></i></a></li>
                        <li><a target="_blank" href="https://twitter.com/laundrylocker"><i class="social_twitter"></i></a></li>
                        <li><a target="_blank" href="https://plus.google.com/102336358377142524801"><i class="social_googleplus"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/laundrylocker/"><i class="social_instagram"></i></a></li>
                        <li><a target="_blank" class="not-lightbox" href="https://www.youtube.com/channel/UCAAyStUREULxvRtN9PoDJZA"><i class="fa fa-youtube "></i></a></li>
                    </ul>
                    <div class="row visible-lg visible-md visible-sm mar-top-35">
                        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-6 col-sm-offset-3">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a target="_blank" href="https://itunes.apple.com/us/app/laundry-locker/id784630564?mt=8"><img class="img-responsive center-block img-right-desktop img-right-tablet" alt="Get it On App Store" src="/images/laundrylocker/app-store-white.svg"/></a>
                                </div>
                                <div class="col-sm-6">
                                    <a target="_blank" href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.laundrylocker"><img class="img-responsive center-block img-left-desktop img-left-tablet" alt="Get it On Play Store" src="/images/laundrylocker/google-play-white.svg"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 footer-copyright">
                    <p class="mar-bot-30"><p>Founded in 2005 and voted the best dry cleaner in San Francisco, Laundry Locker Inc. also has the largest dry cleaning plant in the city. Services in San Francisco and Oakland include: Dry Cleaning, Wash &amp; Fold, Laundered Shirts, Shoe Shine, Shoe Repair, Bag Repair, Comforter Cleaning, Wedding Dress Cleaning, and more. With 350+ locations, concierge, and home delivery, we make it easy and convenient to get your laundry done. We do laundry. You do you.</p>
                </p>

                <p class="no-margin">Terms & Conditions.   © 2018 Mulberrys Garment Care. All rights reserved. | Mulberrys Garment Care. 1530 Custer Avenue, San Francisco, CA 94124</p>
            </div>
        </div>
    </div>
</footer>


<script type='text/javascript'>
    /* <![CDATA[ */
    var mPS2id_params = {"instances":{"mPS2id_instance_0":{"selector":"a[rel='m_PageScroll2id']","autoSelectorMenuLinks":"false","scrollSpeed":1000,"autoScrollSpeed":"true","scrollEasing":"easeInOutQuint","scrollingEasing":"easeOutQuint","pageEndSmoothScroll":"true","stopScrollOnUserAction":"false","layout":"vertical","offset":"215","highlightSelector":"","clickedClass":"mPS2id-clicked","targetClass":"mPS2id-target","highlightClass":"mPS2id-highlight","forceSingleHighlight":"false","keepHighlightUntilNext":"false","highlightByNextTarget":"false","appendHash":"false","scrollToHash":"true","scrollToHashForAll":"true","scrollToHashDelay":0,"disablePluginBelow":0,"adminDisplayWidgetsId":"true","adminTinyMCEbuttons":"true","unbindUnrelatedClickEvents":"false","normalizeAnchorPointTargets":"false"}},"total_instances":"1","shortcode_class":"_ps2id"};
    /* ]]> */
</script>
<script type='text/javascript' src='/js/laundrylocker/page-scroll-to-id.min.js'></script>
<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>


<script type='text/javascript' src='https://use.fontawesome.com/848352048b.js'></script>

<script type='text/javascript' src='/js/laundrylocker/scripts.js'></script>

<script type="text/javascript">
//modified to jump to 1.12: JQMIGRATE: 'ready' event is deprecated
 $(function() {
    jQuery('#interior_mobile_menu').remove();

    jQuery('.calloutBannerText').removeAttr("style");
})
</script>
<?php if (!empty($popup)) echo $popup; ?>  
</body>
</html>
