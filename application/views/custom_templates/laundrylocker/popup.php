<?php 
$html = '
<b>Great News: Laundry Locker is now Mulberrys Garment Care!</b><br/><br/>
You\'ll experience the same great service, but now with a new app.<br/><br/>

To schedule your first Mulberrys pickup <a href="https://www.mulberryscleaners.com/?_branch_match_id=584824593490492583" target="_blank">download our app</a>. Your username will be your Laundry Locker email address and you can create your password using the forgot password link. We can\'t wait to see you!<br/><br/>

Feel free to contact us at mulberrys_sf@mulberryscleaners.com or 877-814-5421 with any questions.';
?>

<style type="text/css">
/* The Modal (background) */
.modal-pop-up {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 100000; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-pop-up-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
    font-weight: normal;
}

/* The Close Button */
.modal-pop-up-close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.modal-pop-up-close:hover,
.modal-pop-up-close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>

<div id="modal-pop-up" class="modal-pop-up">

  <!-- Modal content -->
  <div class="modal-pop-up-content">
    <span class="modal-pop-up-close">&times;</span>
    <?php echo $html ?>
  </div>

</div>

<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById('modal-pop-up');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("modal-pop-up-close")[0];

    // When the user clicks on the button, open the modal 
    window.onload = function(event) {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }


    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
