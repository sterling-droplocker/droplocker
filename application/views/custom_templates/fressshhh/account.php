<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>

        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);

    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>

        <style>
        #header{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
            border-bottom:1px solid #73a5bc;
            margin-bottom:20px;
        }
        #inner-header{
            text-align:left;
            width:970px;
            margin:0 auto;
        }

        #footer{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
        }
        #inner-footer{
            text-align:left;
            width:970px;
            margin:0 auto;
        }
        </style>

    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Fredericka+the+Great|Allura|Amatic+SC|Arizonia|Averia+Sans+Libre|Cabin+Sketch|Francois+One|Jacques+Francois+Shadow|Josefin+Slab|Kaushan+Script|Love+Ya+Like+A+Sister|Merriweather|Offside|Open+Sans|Open+Sans+Condensed|Oswald|Over+the+Rainbow|Pacifico|Romanesco|Sacramento|Seaweed+Script|Special+Elite">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Montserrat:400,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:400,300,700,900">
    <link rel="stylesheet" type="text/css" href="/css/fressshhh/site.css?v=">
    <link rel="stylesheet" type="text/css" href="/css/fressshhh/css2.css?v=">
    <link rel="stylesheet" type="text/css" href="/css/fressshhh/boostrapmods.css">
    <script>
        if (typeof($sf) === "undefined") {
            $sf = {
                baseUrl: "https://img4.wsimg.com/wst/v7/WSB7_J_20151021_1409_publish-n_8087/v2",
                skin: "app",
                preload: 0,
                require: {
                    jquery: "https://img4.wsimg.com/wst/v7/WSB7_J_20151021_1409_publish-n_8087/v2/libs/jquery/jq.js",
                    paths: {
                        "wsbcore": "common/wsb/core",
                        "knockout": "libs/knockout/knockout"
                    }
                }
            };
        }
    </script>
    <script id="duel" src="/css/fressshhh/js/duel.js?appid=O3BkA5J1#TzNCa0E1SjF2Mi41Ljdwcm9k"></script>
    <script>
        define('jquery', ['jq!starfield/jquery.mod'], function(m) {
            return m;
        });
        define('appconfig', [], {
            documentDownloadBaseUrl: 'http://nebula.wsimg.com'
        });
    </script>
</head>

<body>
    <style type="text/css">
        #wsb-element-bd7b6a6d-82be-4514-b0c8-b8c6724e6272 {
            top: 78px;
            left: 621px;
            position: absolute;
            z-index: 47
        }
        #wsb-element-bd7b6a6d-82be-4514-b0c8-b8c6724e6272 .txt {
            width: 455px;
            height: 32px
        }
        #wsb-element-bc855d36-c379-4890-b999-482604be780b {
            top: 18px;
            left: 809px;
            position: absolute;
            z-index: 33
        }
        #wsb-element-bc855d36-c379-4890-b999-482604be780b .txt {
            width: 155px;
            height: 28px
        }
        #wsb-element-c7753550-0835-4cd8-b71d-171846d8b391 {
            top: 583px;
            left: 383px;
            position: absolute;
            z-index: 60
        }
        #wsb-element-c7753550-0835-4cd8-b71d-171846d8b391 .wsb-image-inner {
            padding: 0px;
            -moz-opacity: 0.05;
            -khtml-opacity: 0.05;
            opacity: 0.05
        }
        #wsb-element-c7753550-0835-4cd8-b71d-171846d8b391 .wsb-image-inner div {
            width: 236px;
            height: 236px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-c7753550-0835-4cd8-b71d-171846d8b391 img {
            position: absolute
        }
        #wsb-element-beff132d-eb5f-4bf8-bafc-3b0cf8e2a8fb {
            top: 24px;
            left: 288px;
            position: absolute;
            z-index: 225
        }
        #wsb-element-beff132d-eb5f-4bf8-bafc-3b0cf8e2a8fb .txt {
            width: 378px;
            height: 70px
        }
        #wsb-element-77afd71b-a377-4ee2-96c6-cf604a3e6b89 {
            top: 518px;
            left: 374px;
            position: absolute;
            z-index: 61
        }
        #wsb-element-77afd71b-a377-4ee2-96c6-cf604a3e6b89 .txt {
            width: 253px;
            height: 236px
        }
        #wsb-element-6941a53c-1b23-4980-8fe7-ed880132d515 {
            top: 107px;
            left: 0px;
            position: absolute;
            z-index: 17
        }
        #wsb-element-6941a53c-1b23-4980-8fe7-ed880132d515 .wsb-image-inner {}#wsb-element-6941a53c-1b23-4980-8fe7-ed880132d515 .wsb-image-inner div {
            width: 1000px;
            height: 373px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-6941a53c-1b23-4980-8fe7-ed880132d515 img {
            position: absolute
        }
        #wsb-element-424086ee-5043-44a6-bc89-4b539031c86c {
            top: 381px;
            left: 490px;
            position: absolute;
            z-index: 215
        }
        #wsb-element-424086ee-5043-44a6-bc89-4b539031c86c .txt {
            width: 492px;
            height: 54px
        }
        #wsb-element-4163ef82-5eb4-41da-9b55-fee44d199869 {
            top: 150px;
            left: 0px;
            position: absolute;
            z-index: 18
        }
        #wsb-element-4163ef82-5eb4-41da-9b55-fee44d199869 .wsb-shape {
            width: 332px;
            height: 334px;
            padding: 0px;
            background: -webkit-gradient(linear, left top, right top, color-stop(0, #fff), color-stop(1, transparent));
            background: -webkit-linear-gradient(left, #fff 0%, transparent 100%);
            background: -moz-linear-gradient(left, #fff 0%, transparent 100%);
            background: -o-linear-gradient(left, #fff 0%, transparent 100%);
            background: -ms-linear-gradient(left, #fff 0%, transparent 100%);
            background: linear-gradient(to right, #fff, transparent);
            filter: progid: DXImageTransform.Microsoft.gradient(gradientType=1, startColorstr='#ffffffff', endColorstr='#00000000');
            -ms-filter: progid: DXImageTransform.Microsoft.gradient(gradientType=1, startColorStr='#ffffffff', endColorStr='#00000000');
            box-sizing: content-box;
            -moz-box-sizing: content-box
        }
        #wsb-element-131af145-2abb-4f49-af9f-258eed39d36e {
            top: 518px;
            left: 704px;
            position: absolute;
            z-index: 59
        }
        #wsb-element-131af145-2abb-4f49-af9f-258eed39d36e .txt {
            width: 267px;
            height: 263px
        }
        #wsb-element-9ec4c108-0fd5-4bfe-b871-594af6431627 {
            top: 53px;
            left: 801px;
            position: absolute;
            z-index: 34
        }
        #wsb-element-9ec4c108-0fd5-4bfe-b871-594af6431627 .wsb-share-9ec4c108-0fd5-4bfe-b871-594af6431627 {
            width: 157px;
            height: 95px
        }
        #wsb-element-93703e64-2381-4bb3-aa6f-cc0116e62c03 {
            top: 0px;
            left: 34px;
            position: absolute;
            z-index: 67
        }
        #wsb-element-93703e64-2381-4bb3-aa6f-cc0116e62c03 .wsb-image-inner {}#wsb-element-93703e64-2381-4bb3-aa6f-cc0116e62c03 .wsb-image-inner div {
            width: 211px;
            height: 105px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-93703e64-2381-4bb3-aa6f-cc0116e62c03 img {
            position: absolute
        }
        #wsb-element-6d4436bf-794a-45f0-85ed-f69d629bbf47 {
            top: 56px;
            left: 28px;
            position: absolute;
            z-index: 35
        }
        #wsb-element-6d4436bf-794a-45f0-85ed-f69d629bbf47 {
            width: 600px;
            height: 20px
        }
        #wsb-element-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8 {
            top: 107px;
            left: 0px;
            position: absolute;
            z-index: 221
        }
        #wsb-element-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8 {
            width: 1000px;
            height: 50px
        }
        #wsb-element-5127c798-8c8f-4190-87c7-faa0ec0de142 {
            top: 104px;
            left: 37px;
            position: absolute;
            z-index: 14
        }
        #wsb-element-5127c798-8c8f-4190-87c7-faa0ec0de142 .txt {
            width: 490px;
            height: 24px
        }
        #wsb-element-50b077ac-17b4-4098-beba-31b92520cb28 {
            top: 105px;
            left: 808px;
            position: absolute;
            z-index: 11
        }
        #wsb-element-50b077ac-17b4-4098-beba-31b92520cb28 .wsb-image-inner {}#wsb-element-50b077ac-17b4-4098-beba-31b92520cb28 .wsb-image-inner div {
            width: 133px;
            height: 35px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-50b077ac-17b4-4098-beba-31b92520cb28 img {
            position: absolute
        }
        #wsb-element-2b089eff-9347-4909-a682-8891c3823aa4 {
            top: 126px;
            left: -387px;
            position: absolute;
            z-index: 22
        }
        #wsb-element-2b089eff-9347-4909-a682-8891c3823aa4 .wsb-shape {
            width: 724px;
            height: 79px;
            box-sizing: content-box;
            -moz-box-sizing: content-box
        }
        #wsb-element-0aee51e2-4e2d-440b-821d-9a6456fac238 {
            top: 34px;
            left: 945px;
            position: absolute;
            z-index: 124
        }
        #wsb-element-0aee51e2-4e2d-440b-821d-9a6456fac238 .wsb-image-inner {}#wsb-element-0aee51e2-4e2d-440b-821d-9a6456fac238 .wsb-image-inner div {
            width: 32px;
            height: 32px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-0aee51e2-4e2d-440b-821d-9a6456fac238 img {
            position: absolute
        }
        #wsb-element-bc962b5f-4767-45f1-b369-63c8f1749f2a {
            top: 480px;
            left: 0px;
            position: absolute;
            z-index: 49
        }
        #wsb-element-bc962b5f-4767-45f1-b369-63c8f1749f2a .wsb-shape {
            width: 1000px;
            height: 21px;
            box-sizing: content-box;
            -moz-box-sizing: content-box
        }
        #wsb-element-98bdd4ab-caaa-43c1-adfb-b517c7904b46 {
            top: 267px;
            left: 439px;
            position: absolute;
            z-index: 226
        }
        #wsb-element-98bdd4ab-caaa-43c1-adfb-b517c7904b46 .wsb-button {
            color: #fff;
            border: solid 1px #30a612;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            -o-border-radius: 6px;
            border-radius: 6px;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #5fcf34), color-stop(1, #1ea612));
            background: -webkit-linear-gradient(top, #5fcf34 0%, #1ea612 100%);
            background: -moz-linear-gradient(top, #5fcf34 0%, #1ea612 100%);
            background: -o-linear-gradient(top, #5fcf34 0%, #1ea612 100%);
            background: -ms-linear-gradient(top, #5fcf34 0%, #1ea612 100%);
            background: linear-gradient(to bottom, #5fcf34, #1ea612);
            filter: progid: DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr='#ff5fcf34', endColorstr='#ff1ea612');
            -ms-filter: progid: DXImageTransform.Microsoft.gradient(gradientType=0, startColorStr='#ff5fcf34', endColorStr='#ff1ea612');
            width: 110px;
            height: 51px
        }
        #wsb-element-88fb080d-54eb-480d-a66a-ebb0d2849661 {
            top: 583px;
            left: 717px;
            position: absolute;
            z-index: 58
        }
        #wsb-element-88fb080d-54eb-480d-a66a-ebb0d2849661 .wsb-image-inner {
            padding: 0px;
            -moz-opacity: 0.05;
            -khtml-opacity: 0.05;
            opacity: 0.05
        }
        #wsb-element-88fb080d-54eb-480d-a66a-ebb0d2849661 .wsb-image-inner div {
            width: 236px;
            height: 236px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-88fb080d-54eb-480d-a66a-ebb0d2849661 img {
            position: absolute
        }
        #wsb-element-6162a178-25d9-4b21-a05e-58ce179a4786 {
            top: 518px;
            left: 30px;
            position: absolute;
            z-index: 43
        }
        #wsb-element-6162a178-25d9-4b21-a05e-58ce179a4786 .txt {
            width: 264px;
            height: 209px
        }
        #wsb-element-3f671a06-c002-4615-a647-2d1545a213b5 {
            top: 583px;
            left: 46px;
            position: absolute;
            z-index: 40
        }
        #wsb-element-3f671a06-c002-4615-a647-2d1545a213b5 .wsb-image-inner {
            padding: 0px;
            -moz-opacity: 0.05;
            -khtml-opacity: 0.05;
            opacity: 0.05
        }
        #wsb-element-3f671a06-c002-4615-a647-2d1545a213b5 .wsb-image-inner div {
            width: 236px;
            height: 236px;
            position: relative;
            overflow: hidden
        }
        #wsb-element-3f671a06-c002-4615-a647-2d1545a213b5 img {
            position: absolute
        }
    </style>
    <div class="wsb-canvas body" style="background-color: #f9f9f9; background-position-x: center; background-position-y: top; background-position: center top; background-repeat: no-repeat; position: fixed; top: 0; bottom: 0; left: 0; right: 0; width: 100%; height: 100%; overflow: hidden;">
        <div class="wsb-canvas-page-container" style="position: absolute; top: 0; bottom: 0; left: 0; right: 0; width: 100%; height: 100%; overflow: auto;">
            <div id="wsb-canvas-template-page" class="wsb-canvas-page page" style="height:150px; margin: auto; width: 1000px; background-color: #ffffff; position: relative; margin-top: 0px">
                <div id="wsb-canvas-template-container" style="position: absolute;">
                    <div id="wsb-element-bd7b6a6d-82be-4514-b0c8-b8c6724e6272" class="wsb-element-text" data-type="element">
                        <div class="txt ">
                            <div style="text-align: center;"><span style="font-size:16px;" class="editor_headerCallout"></span><span style="font-size:14px;">Contact Us&nbsp;&nbsp;&nbsp;<a href="mailto:info@freSSSHHH.com" class="editor_headerCallout" target=""><span class="editor_headerCallout">info@freSSSHHH.com</span>
                                </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="wsb-element-beff132d-eb5f-4bf8-bafc-3b0cf8e2a8fb" class="wsb-element-text" data-type="element">
                        <div class="txt ">
                            <p style="text-align: center;"><span style="color:#000080;"><strong><span style="font-size:28px;"></span>
                                </strong></span><span style="color:#00FF00;"><span style="font-size:26px;"><strong>24 Hour Dry Cleaning &amp; Laundry Service</strong></span></span>
                            </p>
                            <p><span style="color:#00FF00;"><span style="font-size:26px;"></span></span>
                                <br>
                            </p>
                        </div>
                    </div>

                    <div id="wsb-element-93703e64-2381-4bb3-aa6f-cc0116e62c03" class="wsb-element-image" data-type="element">
                        <div class="wsb-image-inner ">
                            <div class="img">
                                <a href="how-it-works.html" rel="">
                                    <img src="/css/fressshhh/img/c97e2ad404423106d8d9ad55f2e61600.png" alt="" style="vertical-align:middle;width:211px;height:105px;">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="wsb-element-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8" class="wsb-element-navigation" data-type="element">
                        <div style="width: 1000px; height: 50px;" class="wsb-nav nav_theme nav-text-center nav-horizontal nav-btn-stretch wsb-navigation-rendered-top-level-container" id="wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8">
                            <style>
                                #wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8.wsb-navigation-rendered-top-level-container ul > li:hover,
                                #wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8.wsb-navigation-rendered-top-level-container ul > li:hover > a,
                                #wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8.wsb-navigation-rendered-top-level-container ul > li.active:hover,
                                #wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8.wsb-navigation-rendered-top-level-container ul > li.active > a:hover,
                                #wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8.wsb-navigation-rendered-top-level-container ul > li.active .nav-subnav li:hover,
                                #wsb-nav-62b4e741-19eb-4d3c-a77d-a2b0fd8d72d8.wsb-navigation-rendered-top-level-container ul > li.active .nav-subnav li:hover > a {
                                    background-color: !important;
                                    color: !important;
                                }
                            </style>
                            <ul class="wsb-navigation-rendered-top-level-menu ">
                                <li style="width: 16.6666666666667%" class="active"><a href="http://www.fressshhh.com/how-it-works.html" target="" data-title="How It Works" data-pageid="00000000-0000-0000-0000-000003632844" data-url="http://www.fressshhh.com/how-it-works.html">How It Works</a>
                                </li>
                                <li style="width: 16.6666666666667%"><a href="http://www.fressshhh.com/about-us.html" target="" data-title="About" data-pageid="00000000-0000-0000-0000-000502198356" data-url="http://www.fressshhh.com/about-us.html">About</a>
                                </li>
                                <li style="width: 16.6666666666667%"><a href="http://www.fressshhh.com/price-list.html" target="" data-title="Price List" data-pageid="a836d340-6508-4b85-b044-a410893eafa1" data-url="http://www.fressshhh.com/price-list.html">Price List</a>
                                </li>
                                <li style="width: 16.6666666666667%"><a href="http://www.fressshhh.com/faqs.html" target="" data-title="FAQs" data-pageid="7968112e-345f-4a6f-a2a3-382a1360fabe" data-url="http://www.fressshhh.com/faqs.html">FAQs</a>
                                </li>
                                <li style="width: 16.6666666666667%"><a href="http://www.fressshhh.com/get-fresh.html" target="" data-title="Get Fresh" data-pageid="d14aabc8-251d-417b-953f-3656ed70b25e" data-url="http://www.fressshhh.com/get-fresh.html">Get Fresh</a>
                                </li>
                                <li style="width: 16.6666666666667%"><a href="http://www.fressshhh.com/locations.html" target="" data-title="Locations" data-pageid="fc376d27-9b38-4a03-82dc-a732aa3b162b" data-url="http://www.fressshhh.com/locations.html">Locations</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="wsb-element-0aee51e2-4e2d-440b-821d-9a6456fac238" class="wsb-element-image" data-type="element">
                        <div class="wsb-image-inner ">
                            <div class="img">
                                <a href="https://www.facebook.com/myfreshcloset/timeline" target="_blank" rel="">
                                    <img src="/css/fressshhh/img/8d5e59db167763d71474ff02e5780f61" alt="" style="vertical-align:middle;width:32px;height:32px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style='min-height:600px;margin-left: 20px;margin-right: 20px;'>
            <?php
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content
            ?>
            </div>
            <div id="wsb-canvas-template-footer" class="wsb-canvas-page-footer footer" style="margin: auto; min-height:100px; height: 148px; width: 1000px; position: relative;">
                <div id="wsb-canvas-template-footer-container" class="footer-container" style="position: absolute">
                    <div id="wsb-element-bc855d36-c379-4890-b999-482604be780b" class="wsb-element-text">
                        <div class="txt ">
                            <div><span class="editor_connect">Stay Connected</span>
                            </div>
                        </div>
                    </div>
                    <div id="wsb-element-9ec4c108-0fd5-4bfe-b871-594af6431627" class="wsb-element-share">
                        <div class="wsb-share-9ec4c108-0fd5-4bfe-b871-594af6431627 ">
                            <div id="desktop-9ec4c108-0fd5-4bfe-b871-594af6431627" class="wsb-social-share"></div>
                        </div>
                        <script type="text/javascript">
                            var addthis_config = addthis_config || {};
                            addthis_config.pubid = "ra-5162fff83017c0e0";
                        </script>
                        <script type="text/javascript">
                            require(['designer/social/share/social.share'], function(share) {
                                var $element = $('#desktop-9ec4c108-0fd5-4bfe-b871-594af6431627.wsb-social-share');
                                var model = {
                                    ID: '9ec4c108-0fd5-4bfe-b871-594af6431627',
                                    mode: 'desktop',
                                    ShareFacebook: true,
                                    SharePinterest: false,
                                    ShareGoogle: true,
                                    ShareTwitter: true,
                                    ShareEmail: false,
                                    ShareMore: true,
                                    ShareTheme: 'Buttons'
                                };
                                share.render($element, model);
                            });
                        </script>
                    </div>
                    <div id="wsb-element-6d4436bf-794a-45f0-85ed-f69d629bbf47" class="wsb-element-navigation">
                        <div style="width: 600px; height: 20px;" class="wsb-nav nav_simple nav-text-center nav-horizontal nav-btn-left wsb-navigation-rendered-top-level-container" id="wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47">
                            <style>
                                #wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47.wsb-navigation-rendered-top-level-container ul > li:hover,
                                #wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47.wsb-navigation-rendered-top-level-container ul > li:hover > a,
                                #wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47.wsb-navigation-rendered-top-level-container ul > li.active:hover,
                                #wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47.wsb-navigation-rendered-top-level-container ul > li.active > a:hover,
                                #wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47.wsb-navigation-rendered-top-level-container ul > li.active .nav-subnav li:hover,
                                #wsb-nav-6d4436bf-794a-45f0-85ed-f69d629bbf47.wsb-navigation-rendered-top-level-container ul > li.active .nav-subnav li:hover > a {
                                    background-color: !important;
                                    color: !important;
                                }
                            </style>
                            <ul class="wsb-navigation-rendered-top-level-menu ">
                                <li style="width: auto" class="active"><a href="http://www.fressshhh.com/how-it-works.html" target="" data-title="How It Works" data-pageid="00000000-0000-0000-0000-000003632844" data-url="http://www.fressshhh.com/how-it-works.html">How It Works</a>
                                </li>
                                <li style="width: auto"><a href="http://www.fressshhh.com/about-us.html" target="" data-title="About" data-pageid="00000000-0000-0000-0000-000502198356" data-url="http://www.fressshhh.com/about-us.html">About</a>
                                </li>
                                <li style="width: auto"><a href="http://www.fressshhh.com/price-list.html" target="" data-title="Price List" data-pageid="a836d340-6508-4b85-b044-a410893eafa1" data-url="http://www.fressshhh.com/price-list.html">Price List</a>
                                </li>
                                <li style="width: auto"><a href="http://www.fressshhh.com/faqs.html" target="" data-title="FAQs" data-pageid="7968112e-345f-4a6f-a2a3-382a1360fabe" data-url="http://www.fressshhh.com/faqs.html">FAQs</a>
                                </li>
                                <li style="width: auto"><a href="http://www.fressshhh.com/get-fresh.html" target="" data-title="Get Fresh" data-pageid="d14aabc8-251d-417b-953f-3656ed70b25e" data-url="http://www.fressshhh.com/get-fresh.html">Get Fresh</a>
                                </li>
                                <li style="width: auto"><a href="http://www.fressshhh.com/locations.html" target="" data-title="Locations" data-pageid="fc376d27-9b38-4a03-82dc-a732aa3b162b" data-url="http://www.fressshhh.com/locations.html">Locations</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="wsb-element-5127c798-8c8f-4190-87c7-faa0ec0de142" class="wsb-element-text">
                        <div class="txt ">
                            <p><span class="editor_footer">Copyright &copy; <span itemscope="itemscope" itemtype="http://schema.org/Organization"><span itemprop="name">FRESH Dry Cleaning &amp; Laundry Services</span></span>. All rights reserved.</span>
                                <br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="view-as-mobile" style="padding:10px;position:relative;text-align:center;display:none;"><a href="#" onclick="return false;">View on Mobile</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        require(['jquery', 'common/cookiemanager/cookiemanager', 'designer/iebackground/iebackground'], function($, cookieManager, bg) {
            if (cookieManager.getCookie("WSB.ForceDesktop")) {
                $('.view-as-mobile', '.wsb-canvas-page-container').show().find('a').bind('click', function() {
                    cookieManager.eraseCookie("WSB.ForceDesktop");
                    window.location.reload(true);
                });
            }
            bg.fixBackground();
        });
    </script>


        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>

        <div>
         <div id='footer'>
            <div id='inner-footer'>
                &nbsp;
            </div>
        </div>

        <div style="text-align: right; padding:5px;">
         	<a href="https://droplocker.com" style="display: inline-block; padding: 5px;"><img style="width:189px" align="center" src="https://droplocker.com/images/logo-powered.jpg"></a>
        </div>
        </div>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>
    	<script type="text/javascript">
    	$('.container>.row>.container').addClass("offset1");
    	$('.container>.span9').removeAttr('style');
    	$('.container>.span9').addClass("offset1 span7");
    	$('.container>.span9').removeClass("span9");
    	$('.container>.row>#customer_locations').addClass("offset1");
    	</script>
    </body>


</html>
