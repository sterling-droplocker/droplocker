<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>What is Zenboxed? | ZenBoxed</title>
        <script>document.documentElement.className += " js";</script>
        <link rel='stylesheet' id='themify-font-icons-css-css'  href='/css/font-awesome-4.1.0/css/font-awesome.min.css' type='text/css' media='all' />

        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link rel='stylesheet' id='sage_css-css'  href='/css/zenboxed/main.css' type='text/css' media='all' />
        <?= $style ?>
        <?= $_styles ?>
        <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <link rel="shortcut icon" href="/images/zenboxed/favicon.ico">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <script>
           $(document).ready(function () {
                $('#account_menu').attr('style','height: auto;');
                $('.btn-navbar').click(function() {
                     $('#account_menu').slideToggle();
                });
               $('#hamburger-zen').click(function() {
                   $('#navbar-main-collapse').slideToggle();
               });
            });
        </script>
        <style type="text/css">
            body {
                font-family: Muli,sans-serif;
                font-weight: 300;
                padding-top: 50px;
                font-size: 14px;

            }

            .btn-primary, .btn-success, btn-info, .btn-primary:hover, .btn-success:hover, .btn-info:hover, .btn-primary:visited, .btn-success:visited, .btn-info:visited {
                background-color: #009bf5;


            }

            .btn-primary, .btn-success, .btn-info, .btn-danger {
                background-image: none;
                border: none;
                filter: none;
                border-radius: 0;
            }

            .btn, .btn:hover {
                text-shadow: none;
            }

            #main_content {
                margin-top: 50px;
                padding-bottom: 50px;
            }

            input, textarea, select, .uneditable-input {
                display: inline-block;

                height: 28px;
                padding: 4px;
                margin-bottom: 9px;
                font-size: 13px;
                line-height: 18px;
                color: #555555;
                background-color: #ffffff;
                border: 1px solid #cccccc;
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
            }

            #social-icons {
                margin: 0 0 9px 0;
            }
        </style>
    </head>
    <body class="page page-id-5 page-template-default what-is-zenboxed">
        <!--[if lt IE 9]>
          <div class="alert alert-warning">
            You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.      </div>
        <![endif]-->
        <header class="banner navbar-theme navbar-default navbar-fixed-top" role="banner">
            <div class="" style="max-width: 1200px; margin: 0 auto;">


                <div class="navbar-header">
                    <button type="button" id="hamburger-zen" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main-collapse">

                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://www.zenboxed.com/"><img src="/images/zenboxed/logo-01.png"></a> <!--ZenBoxed-->
                </div>

                <nav id="navbar-main-collapse" class="collapse navbar-collapse" role="navigation">
                    <ul id="menu-header" class="nav navbar-nav">
                        <li class="dropdown menu-what-is-zenboxed"><a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="http://www.zenboxed.com/what-is-zenboxed/">What is ZenBoxed? <b class="caret-theme"></b></a>
                            <ul class="dropdown-menu">
                                <li class="menu-how-it-works"><a href="http://www.zenboxed.com/what-is-zenboxed/#how-it-works">How it Works</a></li>
                                <li class="menu-features"><a href="http://www.zenboxed.com/what-is-zenboxed/#feature">Features</a></li>
                                <li class="menu-laundry-rewards"><a href="http://www.zenboxed.com/what-is-zenboxed/#rewards">Laundry Rewards</a></li>
                            </ul>
                        </li>
                        <li class="menu-services"><a href="http://www.zenboxed.com/services/">Services</a></li>
                        <li class="dropdown menu-faq"><a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="http://www.zenboxed.com/faq/">FAQ <b class="caret-theme"></b></a>
                            <ul class="dropdown-menu">
                                <li class="menu-first-time-customers"><a href="http://www.zenboxed.com/faq/#first-time-customers">First Time Customers</a></li>
                                <li class="menu-other-questions"><a href="http://www.zenboxed.com/faq/#other-services">Other Questions</a></li>
                                <li class="menu-our-process"><a href="http://www.zenboxed.com/faq/#our-process">Our Process</a></li>
                                <li class="menu-features"><a href="http://www.zenboxed.com/faq/#features">Features</a></li>
                                <li class="menu-get-zen"><a href="http://www.zenboxed.com/faq/#get-zen">Get Zen</a></li>
                                <li class="menu-spread-the-word"><a href="/faq/#spread-the-word">Spread the Word</a></li>
                            </ul>
                        </li>
                        <li class="menu-get-zen"><a href="http://www.zenboxed.com/get-zen/">Get Zen</a></li>
                        <li class="dropdown menu-about-us"><a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="http://www.zenboxed.com/about-us/">About Us <b class="caret-theme"></b></a>
                            <ul class="dropdown-menu">
                                <li class="menu-contact-us"><a href="http://www.zenboxed.com/about-us/#contact">Contact Us</a></li>
                                <li class="menu-our-story"><a href="http://www.zenboxed.com/about-us/#story">Our Story</a></li>
                                <li class="menu-testimonials"><a href="http://www.zenboxed.com/about-us/#testimonials">Testimonials</a></li>
                            </ul>
                        </li>
                        <li class="menu-locations"><a href="http://www.zenboxed.com/locations/">Locations</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="container" role="document">
            <div class="content">
                <main class="main" role="main">
                        <div id='main_content'>
                            <?=$content;?>
                        </div>
                        </main>
            </div><!-- /.content -->
        </div><!-- /.wrap -->
        <footer class="content-info" role="contentinfo">
            <div class="container">
                <section class="footer_widget1 widget text-2 widget_text">
                    <div class="textwidget"><p><span style="font-size:2.2em;">ZenBoxed Inc.</span></br><br />
                            <span style="font-size: 1.3em; line-height: 1.8">Email us: <a href="mailto:getzen@zenboxed.com">getzen@ZenBoxed.com</a></span><br />
                            <span style="font-size: 1.3em">Call us 24/7: <a href="tel:8009269">800-ZBOX (9269)</a></span><br />
                            </br></p>
                        <ul id="social-icons">
                            <li><a href="https://www.facebook.com/zenboxed"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/zenboxed"><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-apple"></i></a></li>
                            <li><a href=""><i class="fa fa-android"></i></a></li>
                        </ul>
                        <p style="font-size: 1em;">© Copyright ZenBoxed 2015</p>
                    </div>
                </section>


                <section class="footer_widget2 widget text-3 widget_text">
                    <div class="textwidget"><div style="font-size:1.1em"><a href="/legal" style="padding:0 15px;">Legal</a>   |   <a href="/about-us/#testimonials" style="padding:0 15px;">Testimonials</a>   |   <a href="/careers" style="padding:0 15px;">Careers</a></div><br>
                        <div style="padding:0 15px; font-size:1.1em; margin-top:20px;">Join our mailing list</div>
                        <span style="  max-width: 420px; font-size: .8em; padding: 5px 0 0 15px; display: inline-block;">
                            We'll send you locker related updates, news, and coupons.
                        </span>
                        <div style=" padding:0 15px; margin-top:10px;"><div role="form" class="wpcf7" id="wpcf7-f24-o1" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form name="" action="/#wpcf7-f24-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="24">
                                        <input type="hidden" name="_wpcf7_version" value="4.1.2">
                                        <input type="hidden" name="_wpcf7_locale" value="">
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f24-o1">
                                        <input type="hidden" name="_wpnonce" value="4931080953">
                                    </div>
                                    <p>
                                        <span class="wpcf7-form-control-wrap your-email">
                                            <input style="height: 47px;" type="email" name="your-email" value="you@site.com" size="35" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="list-mail" aria-required="true" aria-invalid="false"></span> <input style="color: white" type="submit" value="Go" class="wpcf7-form-control wpcf7-submit" id="list-submit"><img class="ajax-loader" src="/images/zenboxed/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;"></p>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>

                            </div>
                        </div>
						<span style="max-width: 420px;  margin-top:50px; font-size: .8em; padding: 5px 0 0 15px; display: inline-block;">Now Accepting</span>
						<img src="/images/zenboxed/visalogo-big1.png" alt="Image Description">
						<img src="/images/zenboxed/Mastercard_logo1.png" alt="Image Description">
						<img src="/images/zenboxed/Discover-logo-e1416429693676-1.png" alt="Image Description">
						<img src="/images/zenboxed/500px-american_express_logo-svg1.png" alt="Image Description">
						<img src="/images/zenboxed/Cash-on-delivery.png" alt="Image Description">
                    </div>
                </section>
            </div>
        </footer>
        <script type="text/javascript">
    	$('.container>.row>.container').addClass("offset1");
    	$('.container>.span9').removeAttr('style');
    	$('.container>.span9').addClass("offset1 span7");
    	$('.container>.span9').removeClass("span9");
    	$('.container>.row>.span7').removeClass("container");
    	$('.container>.row>#customer_locations').addClass("offset1");
    	</script>
    </body>
</html>