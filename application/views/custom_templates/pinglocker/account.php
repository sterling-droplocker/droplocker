<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="en-US"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>

    <!-- Meta Data -->
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>PingLocker | <?= $title ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>

    <!-- Favicons -->
    <link rel="icon" href="/images/pinglocker/favicon.ico" type="image/x-icon"/>
    <meta name='robots' content='noindex,follow' />

<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<!--
<link rel='stylesheet' id='ultimate-tables-style-css'  href='http://www.devpinglocker.webkrish.com/wp-content/plugins/ultimate-tables/css/ultimate-tables.css?ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='//www.devpinglocker.webkrish.com/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.1.12' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='//www.devpinglocker.webkrish.com/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.1.12' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css'  href='//www.devpinglocker.webkrish.com/wp-content/plugins/woocommerce/assets/css/woocommerce.css?ver=2.1.12' type='text/css' media='all' />
-->

<link rel='stylesheet' id='raleway-font-css'  href='/css/pinglocker/fonts/raleway/stylesheet.css?ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='montserrat-font"-css'  href='/css/pinglocker/fonts/montserrat/stylesheet.css?ver=4.0' type='text/css' media='all' />

<link rel='stylesheet' id='style-css'  href='/css/pinglocker/style.css?ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='custom-woo-css'  href='/css/pinglocker/custom-woo.css?ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='/css/font-awesome-4.1.0/css/font-awesome.min.css' type='text/css' media='all' />

<!-- SCRIPTS -->
<!--[if lt IE 9]>
  <script src="/js/pinglocker/html5shiv.js"></script>
  <script src="/js/pinglocker/respond.min.js"></script>
<![endif]-->
        
<style type="text/css" title="dynamic-css" class="options-output">
body {font-family:Droid Sans;line-height:20px;font-weight:normal;font-style:normal;color:#0000;font-size:13px; padding: 0 !important;}
.navbar .navbar-right{font-family:Raleway;line-height:20px;font-weight:Normal;font-style:normal;color:#000;font-size:13px;float:right;}
#main-nav {margin-top: 6px;}
#menu-header-menu {margin-top: 6px;}

#main_content {
    padding-top: 120px;
    padding-bottom: 50px;
}

@media (max-width: 767px) {
    #main_content {
        padding-left: 20px;
        padding-right: 20px;
    }

    .navbar-fixed-top.navbar-default {
        margin-left: 0;
        margin-right: 0;
    }
}

@media (max-width: 979px) {
    #main_content {
        padding-top: 0;
    }
}
</style>


<?= $javascript?>
<?= $_scripts;?>
<?= $style?>
<?= $_styles ?>

</head>
<body class="home page page-id-784 page-template page-template-template-home-php page_fullwidth wpb-js-composer js-comp-ver-4.2.3 vc_responsive">


<div class="navbar navbar-fixed-top navbar-default">
  <div class="navbar-inner">
    <div class="container">

    <a class="navbar-brand" href="http://www.pinglocker.com" title="">
        <img src="/images/pinglocker/pinglocker-logo.png" alt="PingLocker">
    </a>


      <a class="btn btn-navbar" data-toggle="collapse" data-target="#main-nav">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      

      <div class="nav-collapse" id="main-nav">
        <a  title="Home" href="http://www.pinglocker.com" target="_blank" class="btn btn-primary btn-small pull-right">
            <i class="icon-home icon-white"></i> HOME </a>
      </div><!--/.nav-collapse -->
    </div>
  </div>

</div>


<!-- Begin Header-->
<!--End Header-->

<section id="main_content">
<?= $content ?>
</section>

<footer id="footer">
    <div class="span12 text-center">
        <div class="back-to-top">
            <i class="fa fa-angle-double-up"></i>
        </div>
    </div>
    <div class="container text-center">
        <div class="row">
            <div class="span12 footer-logo">
                <a href="#"><h4 class="white">PingLocker</h4></a>
            </div>
        </div>
    </div>
</footer>
</body>
