<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Washbox, Inc. | <?= $title ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:700,700italic,italic,regular&amp;subset=latin" media="all" />
<link rel='stylesheet' href='/css/usewashbox/all_styles.css?v=2' type='text/css' media='all' />

<link rel="shortcut icon" href="/images/usewashbox/ee7a2dade0c55589.ico" type="image/vnd.microsoft.icon" />

<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<style type="text/css">
.span12.offset1 {
    margin-left: 120px;
    width: 940px;
}

@media (max-width: 767px) {

.span12.offset1 {
    margin-left: 0;
    width: auto;
}


}

</style>

<script type="text/javascript" src="/js/usewashbox/jquery.once.js?v=1.2"></script>
<!-- <script type="text/javascript" src="/js/usewashbox/drupal.js?n8ic8e"></script> -->
<script type="text/javascript" src="/js/usewashbox/jquery.mobilemenu.js?n8ic8e"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery(document).ready(function($) {

	$("#navigation .content > ul").mobileMenu({
		prependTo: "#navigation",
		combine: false,
		switchWidth: 760,
		topOptionText: "Select a page"
	});

	});
//--><!]]>
</script>

<script type="text/javascript" src="/js/usewashbox/jquery.hoverIntent.minified.js?n8ic8e"></script>
<script type="text/javascript" src="/js/usewashbox/sftouchscreen.js?n8ic8e"></script>
<script type="text/javascript" src="/js/usewashbox/superfish.js?n8ic8e"></script>
<script type="text/javascript" src="/js/usewashbox/supersubs.js?n8ic8e"></script>

	<?= $_scripts ?>
	<?= $javascript?>
	<?= $style?>
	<?= $_styles ?>

</head>
<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">
<div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Skip to main content</a>
</div>
<div id="drupal-wrap">
<header>
    <div class="gcontainer">
        <!-- HEADER LEFT -->
        <div id="header-left" class="four columns omega">
            <a href="http://usewashbox.com/" title="Home" rel="home" id="logo">
                <img src="/images/usewashbox/logo2-wash.png" alt="Home" />
            </a>
            <div id="name-and-slogan" class="element-invisible">
                <div id="site-name" class="element-invisible">
                    <a href="http://usewashbox.com/" title="Home" rel="home">Washbox, Inc.</a>
                </div><!--/site-name-->
            </div><!--/name-and-slogan-->
        </div><!--/header-left-->

        <!-- NAVIGATION -->
        <div id="navigation" class="twelve columns clearfix">
            <div class="menu-header">
                <div class="region region-navigation">
                    <div id="block-superfish-1" class="block block-superfish">
                        <div class="content">
                            <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-8 sf-parent-items-0 sf-single-items-8">
                                <li id="menu-1065-1" class="active-trail first odd sf-item-1 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/washbox-inc" title="Washbox, Inc." class="sf-depth-1 active">Home</a></li>
                                <li id="menu-1561-1" class="middle even sf-item-2 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/how-it-works" title="How Washbox, Inc. Works" class="sf-depth-1">How It Works</a></li>
                                <li id="menu-1574-1" class="middle odd sf-item-3 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/pricing" title="Washbox, Inc. Pricing" class="sf-depth-1">Pricing</a></li>
                                <li id="menu-1567-1" class="middle even sf-item-4 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/faqs" title="Frequently Asked Questions" class="sf-depth-1">FAQs</a></li>
                                <li id="menu-1569-1" class="middle odd sf-item-5 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/locations" title="Washbox, Inc. Locations" class="sf-depth-1">Locations</a></li>
                                <li id="menu-1563-1" class="middle even sf-item-6 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/owners-managers" title="Owners &amp; Managers at Washbox, Inc." class="sf-depth-1">Owners &amp; Managers</a></li>
                                <li id="menu-1571-1" class="middle odd sf-item-7 sf-depth-1 sf-no-children"><a href="http://usewashbox.com/contact" title="Contact Washbox, Inc." class="sf-depth-1">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--/menu-header-->
        </div><!--/navigation-->
    <div class="clear"></div>
</header>

<div id="main-section">
    <div class="gcontainer">
        <!-- MAIN CONTENT -->
        <div id="content" class="sixteen columns clearfix">
            <div id="main" class="clearfix">
                <?= $content; ?>
            </div><!--/main-->
        </div><!--/content-->
    </div>
</div>


<footer>
<div class="gcontainer">
	<div class="four columns omega">
			</div><!--/four-->
	<div class="four columns omega">
			</div><!--/four-->
    <div class="four columns omega">
			</div><!--/four-->
    <div class="four columns clearfix">
			</div><!--/four-->
	<div class="sixteen columns cleafix">
          <div class="region region-footer-full">
    <div id="block-menu-menu-footer-sitemap" class="block block-menu">


  <div class="content">
    <ul class="menu"><li class="first leaf active-trail"><a href="http://usewashbox.com/washbox-inc" title="Washbox, Inc." class="active-trail active">Home</a></li>
<li class="leaf"><a href="http://usewashbox.com/how-it-works" title="How Washbox, Inc. Works">How It Works</a></li>
<li class="leaf"><a href="http://usewashbox.com/pricing" title="Washbox, Inc. Pricing">Pricing</a></li>
<li class="leaf"><a href="http://usewashbox.com/locations" title="Washbox, Inc. Locations">Locations</a></li>
<li class="leaf"><a href="http://usewashbox.com/faqs" title="Frequently Asked Questions">FAQs</a></li>
<li class="leaf"><a href="http://usewashbox.com/owners-managers" title="Owners &amp; Managers at Washbox, Inc.">Owners &amp; Managers</a></li>
<li class="leaf"><a href="http://myaccount.usewashbox.com/login" title="Register/Login">Register/Login</a></li>
<li class="last leaf"><a href="http://usewashbox.com/contact" title="Contact Washbox, Inc.">Contact</a></li>
</ul>  </div>
</div>
<div id="block-block-7" class="block block-block">


  <div class="content">
    <p style="text-align: center;">Washbox, Inc. © 2014. All Rights Reserved. Website designed and developed by <a href="http://sitewired.net" target="_blank">Web Design Denver</a>.</p>  </div>
</div>
  </div>
    </div><!--/sixteen-->

<div class="clear"></div>
</div><!--/gcontainer-->
</footer>

</div>

<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
