<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Total Lockers</title>
        
        <!-- Bootstrap core CSS -->
        <link href="/css/totallockers/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/css/totallockers/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/totallockers/fonts.css">
        
        <link rel="stylesheet" href="/css/totallockers/bootstrap2x.css">
     
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
 
        <!-- Bootstrap core JavaScript
        ================================================== 
        -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/js/totallockers/jquery.min.js"></script>
        <script src="/js/totallockers/bootstrap.min.js"></script>
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/js/totallockers/ie10-viewport-bug-workaround.js"></script>

        <?= $_scripts ?>
        <?= $javascript ?>
        <?= $style ?>
        <?= $_styles ?>
    </head>
    <body>
        <nav class="navbar navbar-default menu-fixed" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://totalcleaners.com">
                        <img src="/images/totallockers/logo.png" height="40px" class="logo" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="menu nav navbar-nav navbar-right">
                        <li>
                            <a href="http://totalcleaners.com/#howitworks">How it works</a>
                        </li>
                        <li>
                            <a href="http://totalcleaners.com/#prices">Prices</a>
                        </li>
                        <li>
                            <a href="http://totalcleaners.com/#faq">Faq</a>
                        </li>
                        <li>
                            <a href="http://totalcleaners.com/#contact">Contact</a>
                        </li>
                        <li>
                            <a href="https:://myaccount.totalcleaners.com/login">Log in/Sign up&nbsp;</a>
                        </li>
                        <li>
                            <a href="http://totalcleaners.com/" target="_blank">Total Cleaners</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

            <div id="bootstrap2x">
                <?=$content?>
            </div>

        <footer>
                <div class="col-md-4 footer foot">
                    <p><a href="http://totalcleaners.com/property.html">Property Manager</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Package Solutions</p>
                </div>
                <div class="col-md-4 footer2">
                    <p>202-599-9090</p>
                    <p>info@totallockers.com</p>
                    <p>1160 First St NE Unit 101 Washington, DC 20002</p>
                </div>
                <div class="col-md-4 footer footer3">
                    <p>© TOTAL LOCKERS    2017    All rights reserved</p>
                </div>
            </div>
        </footer>
    </body>
</html>
