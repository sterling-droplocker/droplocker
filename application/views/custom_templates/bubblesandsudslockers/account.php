<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 ie" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 7]>	<html class="ie7 ie" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 8]>	<html class="ie8 ie" lang="en" dir="ltr"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="" lang="en" dir="ltr"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Bubbles &amp; Suds Locker <?= $title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
	<link href="/css/bubblesandsudslockers/skin.css" rel="stylesheet" media="screen" />

	<!--[if lt IE 9]><script src="/js/bubblesandsudslockers/html5.js"></script><![endif]-->
	<!--[if IE]><link rel="stylesheet" href="/css/bubblesandsudslockers/ie.css"><![endif]-->
	<!--[if lte IE 8]><script src="/js/bubblesandsudslockers/respond.js"></script><![endif]-->

	<link href="//fonts.googleapis.com/css?family=Arvo:n,b,i,bi|Open+Sans:n,b,i,bi|Oswald:n,b|Play:n,b|Yesteryear:n&subset=Latin,Cyrillic,Cyrillic-ext,Greek,Greek-ext,Latin-ext,Vietnamese" rel="stylesheet" type="text/css"  /><script type="text/javascript" src="/assets/jquery/jquery.min.js" ></script>

	<script type='text/javascript' src='/js/bubblesandsudslockers/modernizr.js'></script>
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
	<script src='/bootstrap/js/bootstrap.min.js'></script>
	<script src='/js/bubblesandsudslockers/scripts.js'></script>

	<?= $_scripts ?>
	<?= $javascript?>
	<?= $style?>
	<?= $_styles ?>

</head>

<body class="html front not-logged-in no-sidebars page-node page-node- page-node-1 node-type-page">
	<div class="body">
		<header>
			<div class="clearfix">
				<nav>
					<div class="region region-header-menu">
						<div id="block-system-main-menu" class="block block-system block-menu">
							<div class="content">
								<ul class="nav nav-pills nav-main porto-nav">
									<li><a href="http://bubblesandsudslockers.com/how" title="" class="active">How It Works</a></li>
									<li><a href="http://bubblesandsudslockers.com/services-13">Services</a></li>
									<li><a href="http://bubblesandsudslockers.com/FAQ">FAQs</a></li>
									<li><a href="http://bubblesandsudslockers.com/about">About Us</a></li>
									<li><a href="http://bubblesandsudslockers.com/locations">Locations</a></li>
									<li><a href="http://bubblesandsudslockers.com/contact" title="About us">Contact Us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</nav>
			</div>
		</header>

		<div role="main" class="main">
			<?= $content; ?>
		</div>
		<div class="clear"></div>

		<footer>
			<div class="container">
				<div class="row">
					<div class="span3">
						<div class="region region-footer-1">
							<div id="block-block-10" class="block block-block">
								<h2>ABOUT US</h2>
								<div class="content">
                                    Bubbles &amp; Suds has been providing laundry services for over 15 years. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations. Our commitment to quality and customer satisfaction has helped grow out business to 10 outstanding locations.
								</div>
							</div>
						</div>
					</div>
					<div class="span3">
						<div class="region region-footer-2">
							<div id="block-block-11" class="block block-block">
                                <h2>TESTIMONIALS</h2>
								<div class="content">
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
                                    <div class="testimonial">"The Best Laundromat in Brooklyn!"<br>- Carly T.</div>
								</div>
							</div>
						</div>
					</div>
					<div class="span6">
						<div class="region region-footer-3">
							<div id="block-block-12" class="block block-block">
								<h2>OUR CONVEIENT LOCATIONS</h2>
								<div class="content locations">
                                    <div class="row">
                                        <div class="span1">
                                            <div class="number">96</div>
                                        </div>
                                        <div class="span2">
                                            993 96TH STREET<br>
                                            BROOKLYN, NY 11209<br>
                                            (718) 555-5555<br>
                                            HOURS: 9AM - 9PM<br>
                                            24/7 LOCKERS
                                        </div>
                                        <div class="span3">
                                            <h4>SERVICES:</h4>
                                            STANDARD DROP OFF/PICKUP<br>
                                            24/7 LOCKER SERVICES<br>
                                            SELF WASH/DRY<br>
                                            DRY CLEANING
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span1">
                                            <div class="number">82</div>
                                        </div>
                                        <div class="span2">
                                            993 82ND STREET<br>
                                            BROOKLYN, NY 11209<br>
                                            (718) 555-5555<br>
                                            HOURS: 9AM - 9PM<br>
                                            24/7 LOCKERS
                                        </div>
                                        <div class="span3">
                                            <h4>SERVICES:</h4>
                                            STANDARD DROP OFF/PICKUP<br>
                                            24/7 LOCKER SERVICES<br>
                                            SELF WASH/DRY<br>
                                            DRY CLEANING
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="span1">
                                            <div class="number">77</div>
                                        </div>
                                        <div class="span2">
                                            993 77TH STREET<br>
                                            BROOKLYN, NY 11209<br>
                                            (718) 555-5555<br>
                                            HOURS: 9AM - 9PM<br>
                                            24/7 LOCKERS
                                        </div>
                                        <div class="span3">
                                            <h4>SERVICES:</h4>
                                            24/7 LOCKER SERVICES
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="span1">
                                            <div class="number">71</div>
                                        </div>
                                        <div class="span2">
                                            993 71ST STREET<br>
                                            BROOKLYN, NY 11209<br>
                                            (718) 555-5555<br>
                                            HOURS: 9AM - 9PM<br>
                                            24/7 LOCKERS
                                        </div>
                                        <div class="span3">
                                            <h4>SERVICES:</h4>
                                            24/7 LOCKER SERVICES
                                        </div>
                                    </div>
                                    
								</div>
							</div>
						</div>
					</div>
			</div>

			<div class="footer-copyright">
				<div class="container">
                    <p>Bubbles &amp; Suds Lockers © 2014</p>
				</div>
			</div>
	</footer>
	</div>

<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
