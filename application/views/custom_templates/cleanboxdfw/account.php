<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="/images/cleanboxdfw/fav.ico" sizes="16x16" />
    <link type="text/css" media="all" href="/css/cleanboxdfw/autoptimize.css" rel="stylesheet" />
    <title><?= $title ?></title>
    <meta name="description" content="Make your life easier with exclusive access to 24/7 amenities,100% ECO friendly dry cleaning , and laundry locker service in fort worth &amp; keller area 76244" />
    <link rel="canonical" href="https://cleanboxdfw.com/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Dry Cleaner &amp; Laundry Locker Service Fort Worth 100% Eco Friendly" />
    <meta property="og:description" content="Make your life easier with exclusive access to 24/7 amenities,100% ECO friendly dry cleaning , and laundry locker service in fort worth &amp; keller area 76244" />
    <meta property="og:url" content="https://cleanboxdfw.com/" />
    <meta property="og:site_name" content="Clean Box DFW" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Make your life easier with exclusive access to 24/7 amenities,100% ECO friendly dry cleaning , and laundry locker service in fort worth &amp; keller area 76244" />
    <meta name="twitter:title" content="Dry Cleaner &amp; Laundry Locker Service Fort Worth 100% Eco Friendly" />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />

    <link rel='stylesheet' id='laundry-body-typography-css' href='https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,500,700,700i,900,900i&#038;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' type='text/css' media='all' />
    <link rel='stylesheet' id='laundry-heading-typography-css' href='https://fonts.googleapis.com/css?family=Rubik:100,100i,300,300i,400,400i,500,700,700i,900,900i&#038;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' type='text/css' media='all' />
    <!--[if lt IE 9]><link rel='stylesheet' id='ie_html5shiv-css'  href='/js/cleanboxdfw/html5shiv.js?ver=c3b5f13af6e85bf71ed0bd9c4d9aa113' type='text/css' media='all' /> <![endif]-->
    <!--[if lt IE 9]><link rel='stylesheet' id='respond-css'  href='/js/cleanboxdfw/respond.min.js?ver=c3b5f13af6e85bf71ed0bd9c4d9aa113' type='text/css' media='all' /> <![endif]-->
    <link rel='stylesheet' id='redux-google-fonts-laundry_opt-css' href='https://fonts.googleapis.com/css?family=Lato%7CRubik&#038;ver=c3b5f13af6e85bf71ed0bd9c4d9aa113' type='text/css' media='all' />
    <script type='text/javascript' src='/js/cleanboxdfw/jquery.js?ver=1.12.4'></script>
    <link rel='shortlink' href='https://cleanboxdfw.com/' />

    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://cleanboxdfw.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/css/cleanboxdfw/custom.css">
    <script src='/bootstrap/js/bootstrap.min.js'></script>
    <script type="text/javascript">
      var $ = jQuery.noConflict();
    </script>
    <?= $javascript?>
    <?= $_scripts?>
    <?= $style?>
    <?= $_styles ?>
</head>

<body class="home page-template-default page page-id-701 wpb-js-composer js-comp-ver-5.2.1 vc_responsive">
    <header class="stickyHeader">
        <div class="parentContainer">
            <div class="row">
                <div class="col-sm-12 col-md-1 col-lg-3 visible-sm visible-xs">
                    <div class="box-telephone "> <address> <span class="icon icon-telephone"></span>+1 (817) 750-0055 </address></div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3 hidden-sm hidden-xs"> <address class="box-location"> 5500 North Tarrant Pkwy, Suite 100 <br>Fort Worth, TX 76244 </address></div>
                <div class="col-sm-12 col-md-7 col-lg-6 text-center">
                    <a class="logo" title="Clean Box DFW" href="https://cleanboxdfw.com/"><img class="img-responsive" src="/images/cleanboxdfw/logo.jpg" alt="Clean Box DFW"></a>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-3 hidden-sm hidden-xs">
                    <div class="box-telephone "> <address> +1 (817) 750-0055 </address></div>
                </div>
            </div>
        </div>
        <div class="box-nav">
            <div class="parentContainer">
                <div class="row">
                    <div class="center">
                        <a id="touch-menu" class="mobile-menu" href="#"> <span class="icon-menu-button-of-three-horizontal-lines"></span> </a>
                        <nav class="top-menu">
                            <ul id="menu-menu-1" class="menu">
                                <li id="nav-menu-item-3033" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-701"><a href="https://cleanboxdfw.com/" class="menu-link main-menu-link">Home</a></li>
                                <li id="nav-menu-item-3073" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://cleanboxdfw.com/about-cleanboxdfw/" class="menu-link main-menu-link">About</a></li>
                                <li id="nav-menu-item-3352" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-custom menu-item-object-custom"><a href="https://cleanboxdfw.com/laundry-services/eco-friendly/" class="menu-link main-menu-link">Eco Friendly</a></li>
                                <li id="nav-menu-item-3031" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown"><a href="https://cleanboxdfw.com/services/" class="menu-link main-menu-link">Services</a>
                                    <ul class="sub-menu">
                                        <li id="nav-menu-item-3171" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-custom menu-item-object-custom"><a href="https://cleanboxdfw.com/laundry-services/wash-fold/" class="menu-link sub-menu-link">WASH &#038; FOLD</a></li>
                                        <li id="nav-menu-item-3009" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/laundry-service/" class="menu-link sub-menu-link">Laundry services</a></li>
                                        <li id="nav-menu-item-3010" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/dry-cleaning/" class="menu-link sub-menu-link">Dry cleaning</a></li>
                                        <li id="nav-menu-item-3011" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/leather-cleaning/" class="menu-link sub-menu-link">Leather Cleaning</a></li>
                                        <li id="nav-menu-item-3012" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/wedding-gown-cleaning-preservation/" class="menu-link sub-menu-link">Wedding Gown Cleaning &#038; Preservation</a></li>
                                        <li id="nav-menu-item-3015" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/comforters-curtains-cleaning/" class="menu-link sub-menu-link">Comforters &#038; Curtains Cleaning</a></li>
                                        <li id="nav-menu-item-3014" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/professional-shirt-services/" class="menu-link sub-menu-link">Professional Shirt Service</a></li>
                                        <li id="nav-menu-item-3013" class="sub-menu-item  menu-item-odd menu-item-depth-1 menu-item menu-item-type-post_type menu-item-object-laundry_services"><a href="https://cleanboxdfw.com/laundry-services/alterations/" class="menu-link sub-menu-link">Alterations</a></li>
                                    </ul>
                                </li>
                                <li id="nav-menu-item-3030" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://cleanboxdfw.com/prices/" class="menu-link main-menu-link">Prices</a></li>
                                <li id="nav-menu-item-3132" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://cleanboxdfw.com/blog/" class="menu-link main-menu-link">Blog</a></li>
                                <li id="nav-menu-item-3029" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://cleanboxdfw.com/faq/" class="menu-link main-menu-link">FAQ</a></li>
                                <li id="nav-menu-item-3069" class="main-menu-item  menu-item-even menu-item-depth-0 menu-item menu-item-type-post_type menu-item-object-page"><a href="https://cleanboxdfw.com/contact/" class="menu-link main-menu-link">Contacts</a></li>
                                <li id="nav-menu-item-3023" class="main-menu-item  menu-item-even menu-item-depth-0 popup-link-menu menu-item menu-item-type-custom menu-item-object-custom  current-menu-item current_page_item hover"><a href="https://myaccount.cleanboxdfw.com" class="menu-link main-menu-link  page_item">My Account</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="parentContainer page page_sidebar_none">
      <article class="xxpage xxtype-page xxstatus-publish xxhentry">
        <div class="xxentry-content">
                <?php
                // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
                echo $content
                ?>
        </div>
      </article>
    </div>
    <br/>
    <footer class="site-footer">
        <div class="parentContainer">
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    <a class="logo-footer" title="Clean Box DFW" href="https://cleanboxdfw.com/"><img src="/images/cleanboxdfw/logo-bottom.jpg" alt="Clean Box DFW"></a>
                </div>
                <div class="col-xs-12 col-md-9">
                    <div class="footer-separator">
                        <div class="pull-left">
                            <div class="promo-footer"><span class="text-1">Get Started Today!</span> <span class="text-2">+1 (817) 750-0055</span></div>
                        </div>
                        <div class="pull-right">
                            <ul class="social-icon-footer">
                                <li>
                                    <a href="http://fb.me/cleanboxdfw" class="icon-facebook-logo"></a>
                                </li>
                                <li>
                                    <a href="#" class="icon-twitter-logo-silhouette"></a>
                                </li>
                                <li>
                                    <a href="#" class="icon-instagram-social-network-logo-of-photo-camera"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="parentContainer">
            <div class="row">
                <div class="col-xs-12 col-md-2 hidden-md hidden-sm hidden-xs  pull-right footer-link"><strong>CleanBoxDFW </strong><em>Dry Cleaning &amp; Laundry Locker Service</em>
                    <ul>
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-4  pull-right">
                    <div class="time-table"> 24/7 Service</div>
                </div>
                <div class="col-xs-12 col-md-3 pull-right">
                    <div class="box-location"> 5500 North Tarrant Pkwy, Suite 100 Fort Worth, TX 76244</div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="copyright-box"> © 2017 Clean Box DFW.. All Rights Reserved.</div>
                </div>
            </div>
        </div>
        <div class="bubbleContainer"></div>
    </footer>

    <script type="text/javascript" defer src="/js/cleanboxdfw/autoptimize.js"></script>
</body>
</html>