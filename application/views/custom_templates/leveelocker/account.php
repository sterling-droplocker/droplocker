<!DOCTYPE HTML>
<html lang="en-US" dir="ltr"  data-config='{"twitter":0,"plusone":0,"facebook":0,"style":"default"}'>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Levee Locker &raquo; Home &amp; Office Pickup Drop Off Laundry Service</title>
        <link rel="shortcut icon" href="">

        <link rel='stylesheet' id='collapseomatic-css-css'  href='/css/leveelocker/light_style.css?ver=1.6' type='text/css' media='all' />
        <link rel='stylesheet' id='tablepress-default-css'  href='/css/leveelocker/tablepress-combined.min.css?ver=11' type='text/css' media='all' />
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <?= $style ?>
        <?= $_styles ?>
        <script type='text/javascript' src='https://www.google.com/jsapi?ver=4.0.1'></script>
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>

        <script>
            jQuery(document).ready(function () {
                // expand-content-link renamed to expand-cnt-link for compatibility with twentyfourteen theme
                jQuery(".expand-content-link").removeClass("expand-content-link").addClass("expand-cnt-link");
                jQuery(".expand-cnt-link").click(function () {
                    jQuery(this).toggleClass("ecf_closed").parent(".exp-col-content-holder").find(".hidden-content").first().stop().slideToggle("slow").css("display", "block");
                    return false;
                });
                jQuery(".expand-cnt-link").toggleClass("ecf_closed").parent(".exp-col-content-holder").find(".hidden-content").css("display", "none");

                //images with no float styles , get floated left
                if (typeof jQuery(".hidden-content > img").attr("float") === "undefined") {
                    jQuery(".hidden-content > img:not([class])").addClass("alignleft");
                }


                jQuery(".textwidget > .exp-col-content-holder > .hidden-content > img+p").attr("style", "display:inherit !important;");

            });
        </script>
        <style>
            .expand-cnt-link { font-weight:bold; display:block; margin-bottom:.5em; }
            .expand-cnt-link:before { font-family: "ecf-icons"; content: "\e601  ";  font-size:16px; }
            .hidden-content { display:block; vertical-align:top}
            .exp-col-content-holder { margin:15px 0px 15px 0 !important; }
            .exp-col-content-holder a { display:inline; }
            .exp-col-content-holder+p, .exp-col-content-holder img+p, .expand-cnt-link+p { display:none !important; }
            .ecf_closed:before { font-family: "ecf-icons"; content: "\e600  ";  font-size:16px; }
            .hiddenContentp { margin:0 !important; }
            .hiddenContentp+p { display:none;}
            .hidden-content img { width:20%; }
            .hidden-content img.alignright { margin-right:0 !important; margin-left:10px; margin-bottom:0px; }
            .hidden-content img.alignleft { margin-left:0 !important; margin-right:10px; margin-bottom:0px; }
            .hidden-content .videoWrapper+p { margin-bottom:0; }
            @font-face {
                font-family: "ecf-icons";
                src: url("/fonts/leveelocker/ecf-icons.eot");
            }
            @font-face {
                font-family: "ecf-icons";
                src: url(data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg6v8ysAAAC8AAAAYGNtYXDL8hqdAAABHAAAADxnYXNwAAAAEAAAAVgAAAAIZ2x5ZpFu6gUAAAFgAAABZGhlYWT+7sbiAAACxAAAADZoaGVhBigD3wAAAvwAAAAkaG10eAbbADMAAAMgAAAAEGxvY2EAvABeAAADMAAAAAptYXhwAAcAOAAAAzwAAAAgbmFtZUPPHeQAAANcAAABS3Bvc3QAAwAAAAAEqAAAACAAAwQAAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAACDmAQPA/8D/wAPAAEAAAAAAAAAAAAAAAAAAAAAgAAAAAAACAAAAAwAAABQAAwABAAAAFAAEACgAAAAGAAQAAQACACDmAf//AAAAIOYA////4RoCAAEAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAIABwC+Ai8C+QAaADUAAAEUBwEGIyIvASY1ND8BJyY1ND8BNjMyFwEWFTMUBwEGIyIvASY1ND8BJyY1ND8BNjMyFwEWFQFUBv72BgcIBR0GBuHhBgYdBQgHBgEKBtsF/vUFCAcGHAYG4OAGBhwGBwgFAQsFAdsHBv72BgYdBQgHBuDhBgcIBR0GBv72BggHBv72BgYdBQgHBuDhBgcIBR0GBv72BggAAAIALAD1AmYDHQAaADUAAAEUBwEGIyInASY1ND8BNjMyHwE3NjMyHwEWFTUUBwEGIyInASY1ND8BNjMyHwE3NjMyHwEWFQJmBf71BQgHBv72BgYcBgcIBuDhBQgHBh0FBf71BQgHBv72BgYcBgcIBuDhBQgHBh0FAhIHBv72BgYBCgYHCAUdBgbh4QYGHQUI3AgF/vUFBQELBQgHBhwGBuDgBgYcBgcAAAEAAAABAADFBlcuXw889QALBAAAAAAAzrVBZAAAAADOtUFkAAAAAAJmAx0AAAAIAAIAAAAAAAAAAQAAA8D/wAAABAAAAAAaAmYAAQAAAAAAAAAAAAAAAAAAAAQAAAAAAgAAAAJJAAcCkgAsAAAAAAAKAF4AsgAAAAEAAAAEADYAAgAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAOAK4AAQAAAAAAAQASAAAAAQAAAAAAAgAOAFUAAQAAAAAAAwASACgAAQAAAAAABAASAGMAAQAAAAAABQAWABIAAQAAAAAABgAJADoAAQAAAAAACgAoAHUAAwABBAkAAQASAAAAAwABBAkAAgAOAFUAAwABBAkAAwASACgAAwABBAkABAASAGMAAwABBAkABQAWABIAAwABBAkABgASAEMAAwABBAkACgAoAHUAZQBjAGYALQBpAGMAbwBuAHMAVgBlAHIAcwBpAG8AbgAgADAALgAwAGUAYwBmAC0AaQBjAG8AbgBzZWNmLWljb25zAGUAYwBmAC0AaQBjAG8AbgBzAFIAZQBnAHUAbABhAHIAZQBjAGYALQBpAGMAbwBuAHMARwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABJAGMAbwBNAG8AbwBuAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA) format("truetype"),
                    url(data:application/font-woff;charset=utf-8;base64,d09GRgABAAAAAAUUAAsAAAAABMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABPUy8yAAABCAAAAGAAAABgDq/zK2NtYXAAAAFoAAAAPAAAADzL8hqdZ2FzcAAAAaQAAAAIAAAACAAAABBnbHlmAAABrAAAAWQAAAFkkW7qBWhlYWQAAAMQAAAANgAAADb+7sbiaGhlYQAAA0gAAAAkAAAAJAYoA99obXR4AAADbAAAABAAAAAQBtsAM2xvY2EAAAN8AAAACgAAAAoAvABebWF4cAAAA4gAAAAgAAAAIAAHADhuYW1lAAADqAAAAUsAAAFLQ88d5HBvc3QAAAT0AAAAIAAAACAAAwAAAAMEAAGQAAUAAAKZAswAAACPApkCzAAAAesAMwEJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAg5gEDwP/A/8ADwABAAAAAAAAAAAAAAAAAAAAAIAAAAAAAAgAAAAMAAAAUAAMAAQAAABQABAAoAAAABgAEAAEAAgAg5gH//wAAACDmAP///+EaAgABAAAAAAAAAAEAAf//AA8AAQAAAAAAAAAAAAIAADc5AQAAAAACAAcAvgIvAvkAGgA1AAABFAcBBiMiLwEmNTQ/AScmNTQ/ATYzMhcBFhUzFAcBBiMiLwEmNTQ/AScmNTQ/ATYzMhcBFhUBVAb+9gYHCAUdBgbh4QYGHQUIBwYBCgbbBf71BQgHBhwGBuDgBgYcBgcIBQELBQHbBwb+9gYGHQUIBwbg4QYHCAUdBgb+9gYIBwb+9gYGHQUIBwbg4QYHCAUdBgb+9gYIAAACACwA9QJmAx0AGgA1AAABFAcBBiMiJwEmNTQ/ATYzMh8BNzYzMh8BFhU1FAcBBiMiJwEmNTQ/ATYzMh8BNzYzMh8BFhUCZgX+9QUIBwb+9gYGHAYHCAbg4QUIBwYdBQX+9QUIBwb+9gYGHAYHCAbg4QUIBwYdBQISBwb+9gYGAQoGBwgFHQYG4eEGBh0FCNwIBf71BQUBCwUIBwYcBgbg4AYGHAYHAAABAAAAAQAAxQZXLl8PPPUACwQAAAAAAM61QWQAAAAAzrVBZAAAAAACZgMdAAAACAACAAAAAAAAAAEAAAPA/8AAAAQAAAAAGgJmAAEAAAAAAAAAAAAAAAAAAAAEAAAAAAIAAAACSQAHApIALAAAAAAACgBeALIAAAABAAAABAA2AAIAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEAEgAAAAEAAAAAAAIADgBVAAEAAAAAAAMAEgAoAAEAAAAAAAQAEgBjAAEAAAAAAAUAFgASAAEAAAAAAAYACQA6AAEAAAAAAAoAKAB1AAMAAQQJAAEAEgAAAAMAAQQJAAIADgBVAAMAAQQJAAMAEgAoAAMAAQQJAAQAEgBjAAMAAQQJAAUAFgASAAMAAQQJAAYAEgBDAAMAAQQJAAoAKAB1AGUAYwBmAC0AaQBjAG8AbgBzAFYAZQByAHMAaQBvAG4AIAAwAC4AMABlAGMAZgAtAGkAYwBvAG4Ac2VjZi1pY29ucwBlAGMAZgAtAGkAYwBvAG4AcwBSAGUAZwB1AGwAYQByAGUAYwBmAC0AaQBjAG8AbgBzAEcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==) format("woff");
                font-weight: normal;
                font-style: normal;
            }

            [class^="ecf-icon-"], [class*=" ecf-icon-"] {
                font-family: "ecf-icons";
                speak: none;
                font-style: normal;
                font-weight: normal;
                font-variant: normal;
                text-transform: none;
                line-height: 1;

                /* Better Font Rendering =========== */
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }
            .videoWrapper {
                position: relative;
                padding-bottom: 56.25%; /* 16:9 */
                padding-top: 25px;
                height: 0;
            }
            .videoWrapper iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

            div.menu-item div {
                font-size: 14px;
            }

            #default_account_nav ul li {
                font-size: 14px;
            }

            .btn-primary, .btn-success, .btn-info {
                background-color: #10649b;
            }

            .btn-primary:hover, .btn-success:hover, .btn-info:hover {
                background-color: #3884B5;
            }

            .btn {
                display: inline-block;
                padding: 4px 10px 4px;
                margin-bottom: 0;
                font-size: 14px;
                line-height: 18px;
                color: white;
                text-align: center;
                text-shadow: none;
                vertical-align: middle;
                cursor: pointer;
                background-color: none;
                background-image: none;

                border: none;
                border-color: none;

                border-bottom-color: none;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                filter: none;
                filter: none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
            }

            .btn-small [class^="icon-"], .btn-medium [class^="icon-"], .btn-large [class^="icon-"] {
                margin-top: 0;
            }

            @media (max-width: 767px) {
                body {
                    padding-right: 0;
                    padding-left: 0;
                }
            }


        </style>
        <!--<link rel="stylesheet" href="/wp-content/plugins/widgetkit/cache/widgetkit-fcca057f.css" />
        <script src="/wp-content/plugins/widgetkit/cache/widgetkit-5d33d62a.js"></script>-->
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <link rel="stylesheet" href="/css/leveelocker/theme.css">
        <link rel="stylesheet" href="/css/leveelocker/custom.css">
        <script src="/js/leveelocker/uikit.js"></script>
        <!--<script src="/wp-content/themes/yoo_eat_wp/warp/vendor/uikit/js/addons/autocomplete.js"></script>
        <script src="/wp-content/themes/yoo_eat_wp/warp/vendor/uikit/js/addons/search.js"></script>-->
        <script src="/js/leveelocker/sticky.js"></script>
        <!--<script src="/wp-content/themes/yoo_eat_wp/warp/js/social.js"></script>-->
        <!--<script src="/js/leveelocker/theme.js"></script>-->
    </head>

    <body class="home page page-id-30 page-template-default tm-isblog wp-front_page wp-page wp-page-30">
        <div class="header-bg">
            <div class="uk-container uk-container-header">
                <div class="tm-headerbar uk-clearfix uk-hidden-small">
                    <a class="tm-logo" href="http://leveelocker.com"><img src="/images/leveelocker/levee-locker-logo.png"></a>
                </div>

                <div class="tm-top-block tm-grid-block">
                    <nav class="tm-navbar uk-navbar" >
                        <div class="uk-container uk-container-center uk-container-nav">
                            <ul class="uk-navbar-nav uk-hidden-small">
                                <li class="uk-parent" data-uk-dropdown="{}">
                                    <a href="http://www.leveelocker.com/?page_id=1299">How it Works</a>
                                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1">
                                        <div class="uk-grid uk-dropdown-grid"><div class="uk-width-1-1">
                                                <ul class="uk-nav uk-nav-navbar">
                                                    <li class="uk-parent">
                                                        <a href="http://www.leveelocker.com/?page_id=1299">How it Works</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.leveelocker.com/?page_id=1299#features">Features</a>
                                                    </li>
                                                   <li>
                                                       <a href="http://www.leveelocker.com/faq/">FAQ</a>
                                                   </li>
                                                   <li>
                                                       <a href="http://www.leveelocker.com/contact-2/">Contact</a>
                                                   </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li data-uk-dropdown="{}"><a href="http://www.leveelocker.com/?page_id=972">Our Services</a></li>
                                <li data-uk-dropdown="{}"><a href="http://www.leveelocker.com/?page_id=792">FAQ</a></li>
                                <li data-uk-dropdown="{}"><a href="http://www.leveelocker.com/?page_id=790">About</a></li>
                                <li data-uk-dropdown="{}"><a href="http://www.leveelocker.com/?page_id=786">Locations</a></li>

                            </ul>
                            <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
                            <div id="offcanvas" class="uk-offcanvas">
                                <div class="uk-offcanvas-bar uk-offcanvas-bar-show">
                                    <!--<div class="uk-panel widget_search"><form class="uk-search" id="search-4" action="http://leveelocker.com/" method="get" role="search">
                                            <input class="uk-search-field" type="text" value="" name="s" placeholder="search...">
                                        </form>
                                    </div>-->
                                    <ul class="uk-nav uk-nav-offcanvas">
                                        <li class="uk-active"><a title="data-hover=”Scintilla" href="http://leveelocker.com/">Home</a></li>
                                        <li><a href="http://www.leveelocker.com/?page_id=790">About</a></li><li><a href="http://www.leveelocker.com/?page_id=1299">How it Works</a></li>
                                        <li><a href="http://www.leveelocker.com/?page_id=786">Locations</a></li>
                                        <li><a href="http://www.leveelocker.com/?page_id=792">FAQ</a></li>
                                        <li><a href="http://www.leveelocker.com/?page_id=786">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="uk-navbar-content uk-navbar-center uk-visible-small">
                                <a class="tm-logo-small" href="http://leveelocker.com">
                                    <img src="/images/leveelocker/logo.png" width="70%">
                                </a>
                            </div>
                        </div>

                    </nav>

                </div>
            </div>

        </div>

        <div class="tm-page">
            <div class="tm-block tm-block-light">

                <div class="uk-container uk-container-center">

                    <div class="uk-grid" data-uk-grid-match="" data-uk-grid-margin="">

                        <div class="tm-main uk-width-medium-1-1">


                            <main class="tm-content">



                                <article class="uk-article">


                                    <div id="main_content">
                                        <?= $content; ?>
                                    </div>

                                </article>



                            </main>


                        </div>


                    </div>

                </div>

            </div>


        </div>

        <div class="tm-block">
            <div class="uk-container uk-container-center">
                <footer class="tm-footer uk-text-center">

                    <div>
                        <div class="uk-panel widget_nav_menu"><ul class="uk-subnav uk-subnav-line"><li><a href="http://leveelocker.com/contact/">Contact</a></li><li><a href="http://leveelocker.com/terms-and-conditions/">Terms and Conditions</a></li></ul></div>
                        <div class="uk-panel widget_text"><br/><h3><font color="0c4c77">Inconvenient Dry Cleaning locations and hours... be gone!</h3></font>
                            <br/>
                            <img class="uk-margin-bottom" src="/images/leveelocker/ll-circle.png" width="150" height="auto" alt="Demo">
                            <br> Copyright &copy; 2015 Levee Locker<br/><a href="http://www.metrocreate.com" target="_blank">MetroCreate Studios</a></div>				</div>

                    <div>
                    </div>

                </footer>
            </div>
        </div>

        <script type='text/javascript'>
            var colomatduration = 'fast';
            var colomatslideEffect = 'slideFade';
        </script>
        <script type='text/javascript' src='/js/leveelocker/collapse.js?ver=1.5.13'></script>
    </body>
</html>