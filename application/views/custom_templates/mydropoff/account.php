<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $_scripts;?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
        
        <style>
        body{
            font-family: Hel, sans-serif;
            font-size: 14px;
            font-weight:300;
        }
        #header{
       /*     background: linear-gradient(to bottom, rgba(181,181,181,1) 0%,rgba(255,255,255,1) 38%);
            background-image: linear-gradient(rgb(181, 181, 181) 0%, rgb(255, 255, 255) 38%);
            */
            font-family: Hel, sans-serif;
            font-size: 16px;
            font-weight:300;

            -webkit-background-clip: border-box;
            -webkit-background-origin: padding-box;
            -webkit-background-size: auto;
            background-attachment: scroll;
            background-clip: border-box;
            background-color: rgba(0, 0, 0, 0);
            background-image: linear-gradient(rgb(181, 181, 181) 0%, rgb(255, 255, 255) 38%);
            background-origin: padding-box;
            background-size: auto;
            border-bottom-color: rgb(234, 234, 234);
            border-bottom-style: solid;
            border-bottom-width: 1px;
            color: rgb(68, 68, 68);
            display: block;
            height: 84px;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 0px;
            padding-bottom: 0px;
            padding-left: 0px;
            padding-right: 0px;
            padding-top: 0px;
        }
        #inner-header{
            padding-top:10px;
        }
        
        #footer{
            background: #555;
            margin: 0;
            width: 100%;
            padding: 15px 0;
        }
        #inner-footer{

        }
        
        /* specific hardcoded values for this template */
        .nav>li>a{
            padding: 2px 15px 6px 10px;
            text-decoration: none !important;
            color: rgb(0, 160, 178) !important;
            font-family: Hel, sans-serif !important;
            font-size: 16px !important;
            text-shadow: none !important; 
            list-style-image: none !important;
            list-style-position: outside !important;
            list-style-type: none !important;
        }
        .nav{
            margin-top:14px !important;
        }

        </style>
       
    </head>
    <body>
        
        <div id='header' class="row hidden-phone hidden-tablet">
            <div id="inner-header" class="container hidden-phone hidden-tablet">
                <div class="span12" class="row" style="min-height:64px;">
                <!-- using the proper nav bar structure via bootstrap -->
                   
                   <div class="span4 offset1">
                       <a class="brand" href="http://mydropoff.com/"><img src="/images/mydropoff/logo.png" alt="MyDropOff" width="74" height="63"></a>
                       <a class="brand" href="http://mydropoff.com/"><img style="" src="/images/mydropoff/logo_text.png" alt="MyDropOff" width="139" height="28"></a>
                   </div>
                
                   <div class="navbar span6">
                       <ul class="nav">
                            <li><a href="http://mydropoff.com/how-it-works/">How it Works</a></li>
                            <li><a href="http://mydropoff.com/services/">Our Services</a></li>
                            <li><a href="http://mydropoff.com/locations/">Locations</a></li>
                            <li><a href="http://mydropoff.com/faqs/">FAQ</a></li>
                       </ul>
                   </div>
                
               </div>
            </div>
        </div>
        <div class="clearfix hidden-phone"></div>
        <div style="height:auto;min-height:700px !important; padding-top:20px;" class="">
            
        <?php 
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content 
        ?>
            
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>
        
        <div>
            <div id='footer' class="container span12">
               <div id='inner-footer' class="row-fluid" style="">

                   <div class="social-footer">
                       <div class="span4 offset1">&nbsp;</div>
                       <div class="span4" style="text-align:center;">
                           <a target="_blank" href="http://www.facebook.com/sharer.php?u=http://www.mydropoff.com"><img src="/images/mydropoff/facebookshare.png" alt="MyDropOff" /></a>
                            <a href="https://plus.google.com/share?url=www.mydropoff.com"><img src="/images/mydropoff/googleshare.png" alt="MyDropOff" /></a>
                            <a href="http://twitter.com/share?text=Check%20Out&url=http://www.mydropoff.com"><img src="/images/mydropoff/twittershare.png" alt="MyDropOff" /></a>
                            <br />
                       
                            <a title="Contact" href="http://mydropoff.com/contact/" style="color:white;" >Contact</a>&nbsp;<span style="color:white;">|</span>&nbsp;<a title="Terms" href="http://mydropoff.com/term/"  style="color:white;">Terms</a>
                       </div>
                       <div class="span4">
                             <a href="https://droplocker.com"><img class="pull-right" style="width:189px;margin-right:15px;"  align="center" src="https://droplocker.com/images/droplocker_transparent_powered.png"></a> 
                       </div>
                   </div>
               </div>
           </div>
        
        </div>

        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>
    </body>
    
   
</html>
