<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/laundryboxde/style.css">
    <link rel="icon" type="image/png" href="/images/laundryboxde/favicon.png" />

    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>
    <style type="text/css">
        #footer hr {
            border-bottom-width: 1px !important;
            border-style: solid !important;
            border-color: #969696 !important;
            background-color: #969696 !important;
        }
    </style>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v3.0&appId=136635026389171&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="container">
    <div>
        <!-- End of facebook code -->

        <?
        //if this is using the exterior pages - make sure NOT to hide the header/footer
        // $responsive = '';
        // $uri_string = $this->uri->uri_string();
        // if( strpos($uri_string,'main/') === false ){
        $responsive = 'hidden-phone hidden-tablet';
        // }
        ?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="/main"><img src="/images/laundryboxde/laundry-box-logo.png" style="height:46px;" /></a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="https://www.laundrybox.de/">Home</a></li>
                            <li><a href="https://www.laundrybox.de/standorte/">Standorte</a></li>
                            <li><a href="https://www.laundrybox.de/preise/">Preise</a></li>
                            <li><a href="https://www.laundrybox.de/faq/">FAQ</a></li>
                            <li><a href="https://www.laundrybox.de/kontakt/">Kontakt</a></li>
                            <li class="login"><a href="/register">Registrieren</a></li>
                            <li class="login"><a href="/login">Anmelden</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container">
                    <div id="" class="row-fluid">
                        <div class="span10 center">
                            <div class="navbar">
                                <div class="navbar-inner-menu">
                                    <a href="https://www.laundrybox.de">
                                        <img src="/images/laundryboxde/laundry-box-logo.png" class="logo">
                                    </a>
                                    <ul class="nav">
                                        <li><a class="navText" href="https://www.laundrybox.de/">Home</a></li>
                                        <li><a class="navText" href="https://www.laundrybox.de/standorte/">Standorte</a></li>
                                        <li><a class="navText" href="https://www.laundrybox.de/preise/">Preise</a></li>
                                        <li><a class="navText" href="https://www.laundrybox.de/faq/">FAQ</a></li>
                                        <li><a class="navText" href="https://www.laundrybox.de/kontakt/">Kontakt</a></li>
                                        <li class="login"><a class="navText" href="/register">Registrieren</a></li>
                                        <li class="login"><a class="navText" href="/login">Anmelden</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" id="account-content">
            <div class="span10 center">
                <?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="span10 center" id="footerInner">
                <div class="row-fluid">
                    <div class="footerLogo hidden-tablet hidden-phone">
                        <img src="/images/laundryboxde/laundry-box-footer-logo.png?v1" />
                    </div>
                    <div class="footerLogo hidden-desktop">
                        <img src="/images/laundryboxde/laundry-box-footer-logo.png?v1" width="100%" />
                    </div>
                </div>

                    <hr id="hr">

                <div class="row-fluid">
                    <div class="span4 footerSpan">
                        <p>Laundrybox</p>
                        <p><a href="/" title="HOME">FAQ</a></p>
                        <p><a href="/agb/" title="AGB">AGB</a></p>
                        <p><a href="/kontakt/" title="Kontakt">Kontakt</a></p>
                        <p>
                        <div class="fb-like" data-href="https://www.laundrybox.de/" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                        </p>
                    </div>

                    <div class="span4 footerSpan">
                        <div>
                            <img src="/images/laundryboxde/google-play.png" />
                        </div>
                        <div>
                            <br/>
                            <img src="/images/laundryboxde/app-store.png" />
                        </div>
                        <div>
                            <p style="height:.5em">&nbsp;</p>
                            <p>Akzeptierte Zahlungmethoden:</p>
                        </div>
                        <div>
                            <p style="height:.5em">&nbsp;</p>
                            <img src="/images/laundryboxde/credit-cards.png" />
                        </div>
                    </div>

                    <div class="span4 footerSpan">
                        <p>Laundrybox GmbH</p>
                        <p>
                            <a href="mailto:info@laundrybox.de" title="info@laundrybox.de">info@laundrybox.de</a>
                        </p>
                        <p>089 52 06 14 40</p>
                        <p>Mo - Fr: 08:00 - 18:00</p>
                    </div>
                </div>
            </div>
            <div class="hidden-desktop">
                <div id="footerPrivacyMobile" class="row-fluid" style='clear:both;'>
                    <div class="span10 center">
                        <div class="row-fluid">
                            <div class="j-meta-links">
                                <a href="https://www.laundrybox.de/about/" target="_blank">Impressum</a> | 
                                <a href="https://www.laundrybox.de/j/privacy" target="_blank">Datenschutz</a> | 
                                <a href="https://www.laundrybox.de/j/cookies/policy" target="_blank">Cookie Policy</a>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-phone hidden-tablet">
            <div id="footerPrivacy" class="row-fluid" style='clear:both;'>
                <div class="span10 center">
                    <div class="row-fluid">
                        <div class="j-meta-links">
                            <a href="https://www.laundrybox.de/about/" target="_blank">Impressum</a> | 
                            <a href="https://www.laundrybox.de/j/privacy" target="_blank">Datenschutz</a> | 
                            <a href="https://www.laundrybox.de/j/cookies/policy" target="_blank">Cookie Policy</a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    try {
        var login_logout_button = document.getElementById("login_logout");
        //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
        <?php if ($facebook_user_id): ?>
        login_logout_button.textContent = "Logout";
        login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

        <?php elseif (empty($this->customer)): ?>
        login_logout_button.textContent = "Sign In";
        <?php else: ?>
        login_logout_button.textContent = "Logout";
        login_logout_button.setAttribute("href", "/logout");
        <?php endif; ?>
    } catch (err) {

    }
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>