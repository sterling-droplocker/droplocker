<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>

        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);

    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>

        <style>
        #header{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
            border-bottom:1px solid #73a5bc;
            margin-bottom:20px;
        }
        #inner-header{
            text-align:left;
            width:960px;
            margin:0 auto;
        }

        #footer{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
        }
        #inner-footer{
            text-align:left;
            width:960px;
            margin:0 auto;
        }
        #content {
            padding-bottom: 20px;
        }
        body {
            padding: 0;
        }
        #pu2702-4 {
            padding: 20px;
        }

       .calloutBannerPic {
            width: 130px;
        }

        a#maillink:link, a#maillink:visited {
            color: #FFF;
        }
        a#maillink:hover {
            color: #82BC00;
            text-decoration: none;
        }
        #kiosk_cardNumber {
            width:165px;
        }


        </style>

        <script type="text/javascript">var jslang='EN';</script>


        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="generator" content="2015.0.2.310" />
        <link rel="shortcut icon" href="/images/a-master-favicon.ico?209180445" />
        <meta name="image-hide-from-external-url" content="true" />
        <meta name="image-hide-location" content="true" />
        <meta http-equiv="X-UA-Compatible" content="chrome=IE8" />
        <!-- CSS -->
        <link href="/css/cleanbox/ModuleStyleSheets.css" type="text/css" rel="StyleSheet" />
        <link rel="stylesheet" type="text/css" href="/css/cleanbox/site_global.css" />
        <link rel="stylesheet" type="text/css" href="/css/cleanbox/master_a-master.css" />
        <link rel="stylesheet" type="text/css" href="/css/cleanbox/index.css" id="pagesheet" />

        <!--[if lt IE 9]>
          <link rel="stylesheet" type="text/css" href="/css/iefonts_index.css?457111928"/>
          <![endif]-->


    </head>
    <body>

        <div id='header' style="height: 120px;background-image: url('/css/cleanbox/images/homeback_header.jpg');">
            <div class="browser_width grpelem" id="u5496-bw" style="height: 120px;">
              <div style="height: 120px;"><!-- group -->
               <div class="clearfix" id="u5496_align_to_page" style="height: 120px;">
                <a class="anchor_item grpelem" id="services"></a>
                <div class="clearfix grpelem" id="ppu5408"><!-- column -->
                 <div class="clearfix colelem" id="pu5408"><!-- group -->
                  <img class="svg grpelem" id="u5408" src="/css/cleanbox/images/cleanbox_logo-02.svg" width="499" height="90" alt="CleanBox Laundry 24/7 Dry Cleaning" data-mu-svgfallback="/css/cleanbox/images/cleanbox_logo_blk-02_poster_.png"><!-- svg -->
                  <div class="clearfix grpelem" id="pu4687-4"><!-- column -->
                   <!-- /m_editable -->
                   <div class="clearfix colelem" id="pu5592-4"><!-- group -->
                    <a class="nonblock nontext anim_swing clearfix grpelem" id="u5592-4" href="http://www.cleanboxlaundry.com/index.html#services" style="float: left;margin-left: -50px;margin-top: 51px;" data-href="anchor:U497:U5595" data-muse-uid="U5592" data-muse-type="txt_frame"><!-- content --><p>SERVICES</p></a>
                    <a class="nonblock nontext anim_swing clearfix grpelem" id="u5593-4" href="http://www.cleanboxlaundry.com/index.html#locations" style="float: left;margin-left: -50px;margin-top: 51px;" data-href="anchor:U497:U5662" data-muse-uid="U5593" data-muse-type="txt_frame"><!-- content --><p>LOCATIONS</p></a>
                    <!-- /m_editable -->
                   </div>
                  </div>
                 </div>
                </div>
                <a class="anchor_item grpelem" id="top"></a>
               </div>
              </div>
             </div>
        </div>

        <section role="main" id="content">
        <?php
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content
        ?>
        </section>


        <div>
         <div id='footer'>
            <div id='inner-footer'>
             <div class="browser_width colelem" id="u2700-bw" style="height: 418px;">
             <div id="u2700"><!-- group -->
              <div class="clearfix" id="u2700_align_to_page">
               <div class="clearfix grpelem" id="pu2702-4"><!-- column -->
                <!-- m_editable region-id="editable-static-tag-U2702" template="index.html" data-type="html" data-ice-options="disableImageResize,link" -->
                <div class="heading2 clearfix colelem" id="u2702-4"><!-- content -->
                 <div data-muse-uid="U2702" data-muse-type="txt_frame">
                     <h2>CLEANBOX 24/7</h2>
                    </div>
                            </div>
                            <!-- /m_editable -->
                            <!-- m_editable region-id="editable-static-tag-U2703" template="index.html" data-type="html" data-ice-options="disableImageResize,link" -->
                            <div class="bodytext2 clearfix colelem" id="u2703-4" style="color: #FFF;"><!-- content -->
                                 <div data-muse-uid="U2703" data-muse-type="txt_frame">
                         <p>Allow CleanBox to take care of your dry-cleaning and laundry needs with our conveniently located lockers accessible 24/7.</p>
                        </div>
                    </div>
                    <!-- /m_editable -->
                    <div class="Footer-links colelem" id="u4742"><!-- custom html -->
                             270 Clearwater-Largo Rd. N. #C.<br>
                    Largo, FL 33770<br><br>
                    <a id="maillink" href="mailto:support@cleanboxlaundry.com?Subject=Web Query">support@cleanboxlaundry.com</a><br>
                    727-223-5891<br><br>
                    &copy;<script language="JavaScript" type="text/javascript">
                    var d=new Date();
                    yr=d.getFullYear();
                    if (yr!=2011)
                    document.write(""+yr);
                    </script>2015 CleanBox LLC. - All Rights Reserved
                    </div>

                    <!-- /m_editable -->

                   </div>
                   <div>
                   <!-- m_editable region-id="editable-static-tag-U4728" template="index.html" data-type="image" -->
                   <div class="shadow Normal-Social clip_frame grpelem" id="u4728" data-muse-uid="U4728" data-muse-type="img_frame"><!-- image -->
                    <img class="block" id="u4728_img" src="/css/cleanbox/images/android.jpg" alt="CleanBox Laundry Android App" title="CleanBox Laundry Android App" width="50" height="49" data-muse-src="/css/cleanbox/images/android.jpg">
                   </div>
                   <!-- /m_editable -->
                   <!-- m_editable region-id="editable-static-tag-U4734" template="index.html" data-type="image" -->
                   <div class="Normal-Social shadow clip_frame grpelem" id="u4734" data-muse-uid="U4734" data-muse-type="img_frame"><!-- image -->
                    <img class="block" id="u4734_img" src="/css/cleanbox/images/apple.jpg" alt="CleanBox Laundry iTunes App" title="CleanBox Laundry iTunes App" width="50" height="49" data-muse-src="/css/cleanbox/images/apple.jpg">
                   </div>
                   <!-- /m_editable -->
                   <!-- m_editable region-id="editable-static-tag-U2706" template="index.html" data-type="image" data-ice-options="clickable" data-ice-editable="link" -->
                   <a class="nonblock nontext Normal-Social shadow clip_frame grpelem" id="u2706" href="http://www.facebook.com/cleanbox247" target="_blank" data-muse-uid="U2706" data-muse-type="img_frame"><!-- image --><img class="block" id="u2706_img" src="/css/cleanbox/images/facebook.jpg" alt="Follow CleanBox Laundry on Facebook" title="Follow CleanBox Laundry on Facebook" width="50" height="49" data-muse-src="/images/facebook.jpg"></a>
                   <!-- /m_editable -->
                   </div>
                   </div>
                  </div>
                 </div>
                </div>
            </div>
        </div>
        </div>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>

    </body>


</html>
