<?php
$system_lang = get_customer_business_language_key();

$lang_swap_redirect = '&r=' . base64_encode($this->uri->uri_string);
$logged_in = $this->customer_id;

$lang_switch['fr'] = $logged_in ? '/account/profile/change_language?ll_swap=fr' . $lang_swap_redirect : '?ll_lang=fr';
$lang_switch['en'] = $logged_in ? '/account/profile/change_language?ll_swap=en' . $lang_swap_redirect : '?ll_lang=en';


?>

<!doctype html>
<!--[if lt IE 7]><html lang="fr-FR" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="fr-FR" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="fr-FR" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="fr-FR" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" class="no-js"><!--<![endif]--><html lang="fr" class="no-js"><!--<![endif]-->
        <meta charset="utf-8">
        <title>Groombox - Conciergerie par consigne</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="shortcut icon" href="/images/groombox/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="/images/groombox/icons/apple-touch-icon.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="/css/groombox/style.css?v=7">

        <script src="/js/groombox/libs/modernizr.custom.min.js"></script>
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script src='/bootstrap/js/bootstrap.min.js'></script>
        <script src='/js/groombox/scripts.js'></script>
<?= $_scripts ?>
        <?= $javascript ?>
        <?= $style ?>
        <?= $_styles ?>
        <style>
            @media screen and (min-width: 1000px) {
                #nav-main {
                    margin-top: -49px!important;
                    margin-left: 105px!important;
                    width: 100%;
                }
            }
        </style>
    </head>
    <body>
        <div id="app">
<?php if (empty($system_lang) || $system_lang == "fr") : ?>

                <header id="header">
                    <div class="layout">
                        <a href="https://groombox.fr/" class="logo-groombox">
                            <img src="/images/groombox/logo-groombox.png" alt="Groombox logo">
                        </a>
                            <div class="lang-switcher">
                                    <a href="<?= $lang_switch['fr']; ?>" title="Français"><img src="/css/groombox/images/flag-fr.png"></a>
                                    <a href="<?= $lang_switch['en']; ?>" title="Anglais"><img src="/css/groombox/images/flag-en.png"></a>
                            </div>
                        <div id="userbox">
                            <div class="mobile-hidden">
                                <a href="/login" class="button"><span>Se connecter</span></a>
                                <a href="/register" class="button"><span>Créer un compte</span></a>
                            </div>
                        </div>
                        <nav id="nav-main">
                            <ul id="menu-navigation" class="nav top-nav clearfix">
                                <li id="menu-item-2594" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2594"><a href="http://www.groombox.fr/nos-services-paris/">Services &amp; Tarifs</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-2552" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2552"><a href="http://www.groombox.fr/nos-services-paris/">Nos services à Paris</a>
                                            <ul class="sub-menu" style="display: none;">
                                                <li id="menu-item-1622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1622"><a href="http://www.groombox.fr/nos-services-paris/#pressing">Pressing</a></li>
                                                <li id="menu-item-1623" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1623"><a href="http://www.groombox.fr/nos-services-paris/#cordonnerie">Cordonnerie</a></li>
                                                <li id="menu-item-1624" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1624"><a href="http://www.groombox.fr/nos-services-paris/#retouche">Retouche</a></li>
                                                <li id="menu-item-1625" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1625"><a href="http://www.groombox.fr/nos-services-paris/#colis">Colis</a></li>
                                                <li id="menu-item-1859" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1859"><a href="http://www.groombox.fr/nos-services-paris/#housse-vip">Housse VIP</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-2553" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2553"><a href="http://www.groombox.fr/nos-services-lyon/">Nos services à Lyon</a>
                                            <ul class="sub-menu" style="display: none;">
                                                <li id="menu-item-2554" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2554"><a href="http://www.groombox.fr/nos-services-lyon/#pressing">Pressing</a></li>
                                                <li id="menu-item-2555" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2555"><a href="http://www.groombox.fr/nos-services-lyon/#cordonnerie">Cordonnerie</a></li>
                                                <li id="menu-item-2556" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2556"><a href="http://www.groombox.fr/nos-services-lyon/#retouche">Retouche</a></li>
                                                <li id="menu-item-2557" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2557"><a href="http://www.groombox.fr/nos-services-lyon/#colis">Colis</a></li>
                                                <li id="menu-item-2558" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2558"><a href="http://www.groombox.fr/nos-services-lyon/#housse-vip">House VIP</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-1626" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1626"><a href="http://www.groombox.fr/comment-ca-marche/">Comment ça marche ?</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-1627" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1627"><a href="http://www.groombox.fr/comment-ca-marche#facile">C’est facile !</a></li>
                                        <li id="menu-item-1628" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1628"><a href="http://www.groombox.fr/comment-ca-marche#pour-vos-colis">Pour vos colis</a></li>
                                        <li id="menu-item-1629" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1629"><a href="http://www.groombox.fr/comment-ca-marche#service-h24">Service 24/24 – 7j/7</a></li>
                                        <li id="menu-item-1630" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1630"><a href="http://www.groombox.fr/comment-ca-marche#carte-fidelite">Carte de fidélité</a></li>
                                        <li id="menu-item-1861" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1861"><a href="http://www.groombox.fr/comment-ca-marche#housse-vip">Housse VIP</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-1631" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1631"><a href="http://www.groombox.fr/nous-trouver-paris/">Nous trouver</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-2576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2576"><a href="http://www.groombox.fr/nous-trouver-paris/">Nous trouver à Paris</a>
                                            <ul class="sub-menu" style="display: none;">
                                                <li id="menu-item-1635" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1635"><a href="http://www.groombox.fr/nous-trouver-paris/#dans-votre-entreprise">Dans votre entreprise</a></li>
                                                <li id="menu-item-1636" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1636"><a href="http://www.groombox.fr/nous-trouver-paris/#pres-de-chez-vous">Près de chez vous</a></li>
                                                <li id="menu-item-1637" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1637"><a href="http://www.groombox.fr/nous-trouver-paris/#dans-votre-immeuble">Dans votre immeuble</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-2566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2566"><a href="http://www.groombox.fr/nous-trouver-lyon/">Nous Trouver à Lyon</a>
                                            <ul class="sub-menu" style="display: none;">
                                                <li id="menu-item-2568" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2568"><a href="http://www.groombox.fr/nous-trouver-lyon/#dans-votre-entreprise">Dans votre entreprise</a></li>
                                                <li id="menu-item-2569" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2569"><a href="http://www.groombox.fr/nous-trouver-lyon/#pres-de-chez-vous">Près de chez vous</a></li>
                                                <li id="menu-item-2570" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2570"><a href="http://www.groombox.fr/nous-trouver-lyon/#dans-votre-immeuble">Dans votre immeuble</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-1632" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1632"><a href="http://www.groombox.fr/qui-sommes-nous/">Qui sommes-nous ?</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-1638" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1638"><a href="http://www.groombox.fr/qui-sommes-nous#equipe-groombox">L’équipe Groombox</a></li>
                                        <li id="menu-item-1639" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1639"><a href="http://www.groombox.fr/qui-sommes-nous#notre-service-client">Notre service client</a></li>
                                        <li id="menu-item-1640" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1640"><a href="http://www.groombox.fr/qui-sommes-nous#technologie-groombox">Technologie Groom Box</a></li>
                                        <li id="menu-item-1641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1641"><a href="http://www.groombox.fr/qui-sommes-nous#valeurs-groombox">Nos valeurs</a></li>
                                        <li id="menu-item-1642" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1642"><a href="http://www.groombox.fr/qui-sommes-nous#nous-rejoindre">Nous rejoindre</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-1633" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1633"><a href="http://www.groombox.fr/devenez-partenaires/">Devenez partenaire</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-1643" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1643"><a href="http://www.groombox.fr/devenez-partenaires#particulier">Vous êtes un particulier</a></li>
                                        <li id="menu-item-1644" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1644"><a href="http://www.groombox.fr/devenez-partenaires#entreprise">Vous êtes une entreprise</a></li>
                                        <li id="menu-item-1645" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1645"><a href="http://www.groombox.fr/devenez-partenaires#partenaires">Nos partenaires</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-1634" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1634"><a href="http://www.groombox.fr/nous-contacter/">Nous contacter</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-1646" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1646"><a href="http://www.groombox.fr/nous-contacter#par-email">Par e-mail</a></li>
                                        <li id="menu-item-1647" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1647"><a href="http://www.groombox.fr/nous-contacter#par-telephone">Par téléphone</a></li>
                                        <li id="menu-item-1648" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1648"><a href="http://www.groombox.fr/nous-contacter#faq">F.A.Q</a></li>
                                    </ul>
                                </li>
                            </ul>


                        </nav>
                    </div>
                </header>

<?php  elseif ($system_lang == "en") : ?>
            
                <header id="header">
                    <div class="layout">
                        <a href="https://www.groombox.fr/en" class="logo-groombox">
                            <img src="/images/groombox/logo-groombox.png">
                        </a>
                            <div class="lang-switcher">
                                    <a href="<?= $lang_switch['fr']; ?>" title="Français"><img src="/css/groombox/images/flag-fr.png"></a>
                                    <a href="<?= $lang_switch['en']; ?>" title="Anglais"><img src="/css/groombox/images/flag-en.png"></a>
                            </div>
                        <div id="userbox">
                            <div class="mobile-hidden">

                                <a href="/login" class="button"><span>Log in</span></a>
                                <a href="/register" class="button"><span>Create an account</span></a>
                            </div>
                        </div>
                        <nav id="nav-main">
                            <ul id="menu-navigation" class="nav top-nav clearfix">
                                <li id="menu-item-2595" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2595"><a href="http://www.groombox.fr/en/services-prices-paris/">Services &amp; Prices</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2545" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2545"><a href="http://www.groombox.fr/en/services-prices-paris/">Services and Prices in Paris</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-213" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-213"><a href="http://www.groombox.fr/en/services-prices-paris/#dry-cleaning">Dry-cleaning</a></li>
                                                <li id="menu-item-214" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-214"><a href="http://www.groombox.fr/en/services-prices-paris/#shoe-repair">Shoe repair</a></li>
                                                <li id="menu-item-215" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-215"><a href="http://www.groombox.fr/en/services-prices-paris/#alteration">Alteration</a></li>
                                                <li id="menu-item-216" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-216"><a href="http://www.groombox.fr/en/services-prices-paris/#packages">Packages</a></li>
                                                <li id="menu-item-1872" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1872"><a href="http://www.groombox.fr/en/services-prices-paris/#vip-bag">VIP Bag</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-2546" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2546"><a href="http://www.groombox.fr/en/services-prices-lyon/">Services and Prices in Lyon</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-2547" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2547"><a href="http://www.groombox.fr/en/services-prices-lyon/#dry-cleaning">Dry-cleaning</a></li>
                                                <li id="menu-item-2548" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2548"><a href="http://www.groombox.fr/en/services-prices-lyon/#shoe-repair">Shoe repair</a></li>
                                                <li id="menu-item-2549" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2549"><a href="http://www.groombox.fr/en/services-prices-lyon/#alteration">Alteration</a></li>
                                                <li id="menu-item-2550" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2550"><a href="http://www.groombox.fr/en/services-prices-lyon/#packages">Packages</a></li>
                                                <li id="menu-item-2551" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2551"><a href="http://www.groombox.fr/en/services-prices-lyon/#vip-bag">VIP Bag</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-184"><a href="http://www.groombox.fr/en/how-does-it-work/">How does it work?</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-238" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-238"><a href="http://www.groombox.fr/en/how-does-it-work/#its-easy">It’s easy!</a></li>
                                        <li id="menu-item-217" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-217"><a href="http://www.groombox.fr/en/how-does-it-work/#for-your-packages">For your packages</a></li>
                                        <li id="menu-item-218" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-218"><a href="http://www.groombox.fr/en/how-does-it-work/#24-7-service">24/7 Service</a></li>
                                        <li id="menu-item-219" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-219"><a href="http://www.groombox.fr/en/how-does-it-work/#loyalty-program">Loyalty Program</a></li>
                                        <li id="menu-item-1873" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1873"><a href="http://www.groombox.fr/en/how-does-it-work/#vip-bag">VIP Bag</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-188" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-188"><a href="http://www.groombox.fr/en/find-us-paris/">Find us</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-2572" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2572"><a href="http://www.groombox.fr/en/find-us-paris/">Find us in Paris</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-220" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-220"><a href="http://www.groombox.fr/en/find-us-paris/#in-your-company">In your company</a></li>
                                                <li id="menu-item-221" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-221"><a href="http://www.groombox.fr/en/find-us-paris/#near-you">Near you</a></li>
                                                <li id="menu-item-222" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-222"><a href="http://www.groombox.fr/en/find-us-paris/#in-your-building">In your building</a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-2571" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2571"><a href="http://www.groombox.fr/en/find-us-lyon/">Find us in Lyon</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-2573" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2573"><a href="http://www.groombox.fr/en/find-us-lyon/#in-your-company">In your company</a></li>
                                                <li id="menu-item-2574" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2574"><a href="http://www.groombox.fr/en/find-us-lyon/#near-you">Near you</a></li>
                                                <li id="menu-item-2575" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2575"><a href="http://www.groombox.fr/en/find-us-lyon/#in-your-building">In your building</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="menu-item-187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-187"><a href="http://www.groombox.fr/en/who-are-we/">Who are we?</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-223" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-223"><a href="http://www.groombox.fr/en/who-are-we/#groombox-team">The Groombox Team</a></li>
                                        <li id="menu-item-224" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-224"><a href="http://www.groombox.fr/en/who-are-we/#our-customer-service">Our customer service</a></li>
                                        <li id="menu-item-225" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-225"><a href="http://www.groombox.fr/en/who-are-we/#groombox-technology">The Groombox Technology</a></li>
                                        <li id="menu-item-226" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-226"><a href="http://www.groombox.fr/en/who-are-we/#groombox-values">Groombox’s values</a></li>
                                        <li id="menu-item-228" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-228"><a href="http://www.groombox.fr/en/who-are-we/#join-us">Join us</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-186" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-186"><a href="http://www.groombox.fr/en/become-partner/">Become a partner</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-229" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-229"><a href="http://www.groombox.fr/en/become-partner/#private-individual">You are a private individual</a></li>
                                        <li id="menu-item-230" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-230"><a href="http://www.groombox.fr/en/become-partner/#companies">You are a company</a></li>
                                        <li id="menu-item-231" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-231"><a href="http://www.groombox.fr/en/become-partner/#partners">They are already partners</a></li>
                                    </ul>
                                </li>
                                <li id="menu-item-185" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-185"><a href="http://www.groombox.fr/en/contact-us/">Contact us</a>
                                    <ul class="sub-menu" style="display: none;">
                                        <li id="menu-item-232" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-232"><a href="http://www.groombox.fr/en/contact-us/#by-email">By e-mail</a></li>
                                        <li id="menu-item-233" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-233"><a href="http://www.groombox.fr/en/contact-us/#by-phone">By phone</a></li>
                                        <li id="menu-item-234" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-234"><a href="http://www.groombox.fr/en/contact-us/#faq">F.A.Q</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </header>

<?php endif; ?>

            <div class="no-container">
                <div id="content">
<?= $content; ?>
                </div>
            </div>

<?php if (empty($system_lang) || $system_lang === "fr") : ?>

                <footer id="footer">
                    <div class="layout">
                        <div class="pushes">
    <?= $footer ?>
                            <dl style="height: 206px;">
                                <dt>Tout sur Groom Box</dt>
                                <dd>
                                    <ul id="menu-footer" class="nav footer-nav clearfix"><li id="menu-item-190" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-190"><a href="http://www.groombox.fr/qui-sommes-nous/">Qui sommes-nous ?</a></li>
                                        <li id="menu-item-191" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191"><a href="http://www.groombox.fr/nos-services/">Nos services</a></li>
                                        <li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a href="http://www.groombox.fr/nous-trouver/">Nous trouver</a></li>
                                        <li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="http://www.groombox.fr/comment-ca-marche/">Comment ça marche ?</a></li>
                                        <li id="menu-item-189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189"><a href="http://www.groombox.fr/devenez-partenaires/">Devenez partenaires</a></li>
                                        <li id="menu-item-200" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-200"><a href="http://www.groombox.fr/mentions-legales/">Mentions légales</a></li>
                                        <li id="menu-item-203" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-203"><a href="http://www.groombox.fr/cgv-et-confidentialite/">CGV et Confidentialité</a></li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl style="height: 206px;">
                                <dt>Le Groom est bavard</dt>
                                <dd>
                                    <ul>
                                        <li><a href="http://www.groombox.fr/la-presse-en-parle/" title="La presse en parle">La presse en parle</a></li>
                                        <li><a href="http://www.groombox.fr/nous-contacter/" title="Contacter le Groom... Il vous répondra">Contactez le Groom...</a></li>
                                        <li><a href="http://www.groombox.fr/qui-sommes-nous/#nous-rejoindre" title="Rejoindre l'équipe GroomBox">Rejoindre l'équipe Groom Box</a></li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl style="height: 206px;">
                                <dt>Pour ne rien manquer</dt>
                                <dd>
                                    <ul class="social-links">
                                        <li><a href="https://www.facebook.com/pages/Groom-Box/222271777978815" title="Notre page Facebook" target="_blank"><img src="/images/groombox/icon-facebook.png"></a></li>
                                        <li><a href="https://plus.google.com/u/0/116365751725365908818/posts?cfem=1" title="Notre page Google+" target="_blank"><img src="/images/groombox/icon-gplus.png"></a></li>
                                        <li><a href="https://twitter.com/Groomboxfr" title="Notre page Twitter" target="_blank"><img src="/images/groombox/icon-twitter.png"></a></li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl style="height: 206px;">
                                <dt>Le groomblog</dt>
                                <dd>
                                    <div class="groomblog">
                                        <div class="blog-article">
                                            <a href="http://www.groombox.fr/fers-mes-chaussures-faire/">
                                                <div class="illustration"><img width="92" height="56" src="/images/groombox/blog/illustration1.jpg" class="attachment-bones-thumb-300 wp-post-image" alt="Les fers - GroomBlog"></div>
                                                <div class="article">
                                                    <div class="category">Blog</div>
                                                    <div class="separator">|</div>
                                                    <div class="date">30.03.2014</div>
                                                    <div class="title">Des fers sur mes chaussures, pour quoi faire ?</div>
                                                    <div class="intro">Semelle usée, bout de chaussures décollé … le ...</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="blog-article">
                                            <a href="http://www.groombox.fr/les-calceophiles-ces-fondus-glace/">
                                                <div class="illustration"><img width="92" height="56" src="/images/groombox/blog/illustration2.jpg" class="attachment-bones-thumb-300 wp-post-image" alt="Glaçage - Chaussures - Groom Box - Cordonnerie"></div>
                                                <div class="article">
                                                    <div class="category">Blog</div>
                                                    <div class="separator">|</div>
                                                    <div class="date">30.03.2014</div>
                                                    <div class="title">Les calcéophiles, ces fondus de glaçage !</div>
                                                    <div class="intro">« Calceo » : mot latin signifiant chaussure. « ...</div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </footer>

<?php  elseif ($system_lang === "en"): ?>

                <footer id="footer" role="contentinfo">
                    <div class="layout">
                        <div class="pushes">
    <?= $footer ?>
                            <dl style="height: 206px;">
                                <dt>All about Groom Box</dt>
                                <dd>
                                    <ul id="menu-footer" class="nav footer-nav clearfix"><li id="menu-item-190" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-190"><a href="https://www.groombox.fr/en/who-are-we/">About us</a></li>
                                        <li id="menu-item-191" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-191"><a href="https://www.groombox.fr/en/services-prices/">Services &#038; Pricing</a></li>
                                        <li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a href="https://www.groombox.fr/en/find-us/">Find us</a></li>
                                        <li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="https://www.groombox.fr/en/how-does-it-work/">How it works</a></li>
                                        <li id="menu-item-189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-189"><a href="https://www.groombox.fr/en/become-partner/">Become a partner</a></li>
                                        <li id="menu-item-200" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-200"><a href="https://www.groombox.fr/en/mentions-legales/">Legal terms</a></li>
                                        <li id="menu-item-203" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-203"><a href="https://www.groombox.fr/en/cgv-et-confidentialite/">Privacy policy</a></li>
                                    </ul>						</dd>
                            </dl>
                            <dl style="height: 206px;">
                                <dt>The Groom is talkative </dt>
                                <dd>
                                    <ul>
                                        <li><a href="http://www.groombox.fr/la-presse-en-parle/" title="La presse en parle">In the press</a></li>
                                        <li><a href="http://www.groombox.fr/contact-us/" title="Contacter le Groom... Il vous répondra">Contact the Groom</a></li>
                                        <li><a href="http://www.groombox.fr/who-are-we/#join-us" title="Rejoindre l'équipe GroomBox">Join the Groom Box Team</a></li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl style="height: 206px;">
                                <dt>Do not miss anything</dt>
                                <dd>
                                    <ul class="social-links">
                                        <li><a href="https://www.facebook.com/pages/Groom-Box/222271777978815" title="Notre page Facebook" target="_blank"><img src="https://www.groombox.fr/wp-content/themes/groombox/library/images/icon-facebook.png"></a></li>
                                        <li><a href="https://plus.google.com/u/0/116365751725365908818/posts?cfem=1" title="Notre page Google+" target="_blank"><img src="https://www.groombox.fr/wp-content/themes/groombox/library/images/icon-gplus.png"></a></li>
                                        <li><a href="https://twitter.com/Groomboxfr" title="Notre page Twitter" target="_blank"><img src="https://www.groombox.fr/wp-content/themes/groombox/library/images/icon-twitter.png"></a></li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl style="height: 206px;">
                                <dt>The GroomBloG</dt>
                                <dd>
                                    <div class="groomblog">
                                        <div class="blog-article">
                                            <a href="https://www.groombox.fr/en/cintre-recycle/">
                                                <div class="illustration"><img width="539" height="311" src="https://www.groombox.fr/wp-content/uploads/2015/03/GROOMBOX-CINTRE.jpg" class="attachment-bones-thumb-300 size-bones-thumb-300 wp-post-image" alt="GROOMBOX - CINTRE" srcset="http://www.groombox.fr/wp-content/uploads/2015/03/GROOMBOX-CINTRE-300x173.jpg 300w, http://www.groombox.fr/wp-content/uploads/2015/03/GROOMBOX-CINTRE.jpg 539w" sizes="(max-width: 539px) 100vw, 539px" /></div>
                                                <div class="article">
                                                    <div class="category">Blog</div>
                                                    <div class="separator">|</div>
                                                    <div class="date">17.03.2015</div>
                                                    <div class="title">Faites un geste pour la planète ! </div>
                                                    <div class="intro">Le Groom est écolo. Non, ce n’est pas une tocade ...</div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="blog-article">
                                            <a href="https://www.groombox.fr/en/saint-valentin-acte-6-scene-3/">
                                                <div class="illustration"><img width="500" height="333" src="https://www.groombox.fr/wp-content/uploads/2015/02/GROOMBOX-MDCE.jpg" class="attachment-bones-thumb-300 size-bones-thumb-300 wp-post-image" alt="GROOMBOX - MDCE" srcset="http://www.groombox.fr/wp-content/uploads/2015/02/GROOMBOX-MDCE-310x206.jpg 310w, http://www.groombox.fr/wp-content/uploads/2015/02/GROOMBOX-MDCE.jpg 500w" sizes="(max-width: 500px) 100vw, 500px" /></div>
                                                <div class="article">
                                                    <div class="category">Blog</div>
                                                    <div class="separator">|</div>
                                                    <div class="date">10.02.2015</div>
                                                    <div class="title">Saint-Valentin : Acte 6 Scène 3 ! </div>
                                                    <div class="intro">Tous les ans la même rengaine, elle ne veut rien, ...</div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </footer>

<?php endif; ?>        
        </div>
            <?php if (ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?= $ga ?>']);
                _gaq.push(['_setDomainName', 'dashlocker.com']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
<?php endif; ?>
    </body>
</html>
