<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);

    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>

        <style>
        #header{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
            border-bottom:1px solid #73a5bc;
            margin-bottom:20px;
        }
        #inner-header{
            text-align:left;
            width:970px;
            margin:0 auto;
        }

        #footer{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
        }
        #inner-footer{
            text-align:left;
            width:970px;
            margin:0 auto;
        }
        </style>
		<!-- favicon -->
		<link rel="shortcut icon" href="http://www.dropspotcleaner.com/wp-content/uploads/2015/10/DropSpotFavicon.png">

		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,700,800,100%7CRaleway:400,700%7CHandlee:regular&amp;subset=latin,latin-ext">

        <link rel="stylesheet" id="pe_theme_compressed-css" href="/css/dropspotcleaner/theme.min.css?ver=1441032158" type="text/css" media="all">
        <link rel="stylesheet" id="pe_theme_init-css" href="/css/dropspotcleaner/style.css?ver=1441032158" type="text/css" media="all">
        <link rel="stylesheet" id="sccss_style-css" href="/css/dropspotcleaner/detail.css?sccss=1&amp;ver=4.2.5" type="text/css" media="all">
        <link rel="stylesheet" id="microthemer-css" href="/css/dropspotcleaner/active-styles.css?ver=4.2.5" type="text/css" media="all">
        <script type="text/javascript" src="/css/dropspotcleaner/js/jquery.js?ver=1.11.2"></script>
        <script type="text/javascript" src="/css/dropspotcleaner/js/jquery-migrate.min.js?ver=1.2.1"></script>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <!-- Easy Columns 2.1.1 by Pat Friedl http://www.patrickfriedl.com -->
        <link rel="stylesheet" href="/css/dropspotcleaner/easy-columns.css" type="text/css" media="screen, projection">
        <style type="text/css">body,h1,h2,h3,h4,.chart-text span,.chart2-text span{font-family:'Open Sans';}.text-logo,h5,h6,.btn,.navbar-nav>li>a,.chart-text,.chart2-text,.footer-nav,.dropdown-menu li a{font-family:'Raleway';}.signup-handwritten{font-family:'Handlee';}</style><style type="text/css" id="pe-theme-custom-colors">.lm-failed{color:#05547b;}.sm-failed{color:#05547b;}.btn-white{color:#05547b;}.contact-content a{color:#05547b;}.post-meta span a{color:#05547b;}a{color:#05547b;}.text-color{color:#05547b;}.btn-color-ghost{color:#05547b;}* .btn-color:hover{color:#05547b;}.icon-lg:hover{color:#05547b;}.icon-lg:focus{color:#05547b;}.icon-sm:hover{color:#05547b;}.icon-sm:focus{color:#05547b;}.is-scrolling .navbar-nav>.active>a{color:#05547b;}.is-scrolling .navbar-nav>.active>a:hover{color:#05547b;}.is-scrolling .navbar-nav>.active>a:focus{color:#05547b;}.is-scrolling .navbar-nav>.current-menu-item>a{color:#05547b;}.is-scrolling .navbar-nav>.current-menu-item>a:hover{color:#05547b;}.is-scrolling .navbar-nav>.current-menu-item>a:focus{color:#05547b;}.innernav .navbar-nav>.current-menu-item>a{color:#05547b;}.innernav .navbar-nav>.current-menu-item>a:hover{color:#05547b;}.innernav .navbar-nav>.current-menu-item>a:focus{color:#05547b;}.team-img .img-icons span i:hover{color:#05547b;}.dark .btn.btn-color:hover{color:#05547b;}.footer a:hover{color:#05547b;}.bypostauthor>.comment-body >.comment-content .fn{color:#05547b;}.btn-white:hover{background-color:#05547b;}.post-password-form input[type=submit]:hover{background-color:#05547b;}.color-bg{background-color:#05547b;}.btn-color{background-color:#05547b;}* .btn-color-ghost:hover{background-color:#05547b;}.solid-bg{background-color:#05547b;}.hero-section h1>span{background-color:#05547b;}.preloader{background-color:#05547b;}.pricing-tab .ribbon .popular{background-color:#05547b;}* .fast-reg .form-control:focus{background-color:#05547b;}#commentform button{background-color:#05547b;}* .btn-white:hover{border-color:#05547b;}.post.sticky h4 a{border-color:#05547b;}.btn-color-ghost:hover{border-color:#05547b;}body .btn-color{border-color:#05547b;}.btn-color:hover{border-color:#05547b;}.pricing-tab{border-color:#05547b;}.pricing-tab h4{border-color:#05547b;}.signup-divider .form-control:focus{border-color:#05547b;}.fast-reg .form-control:focus{border-color:#05547b;}#commentform button[type]{border-color:#05547b;}</style><style type="text/css">.preloader { display: none !important;}

        .navbar-fixed-top {
          background: #1a1a1a !important;
          padding-top: 0;
          padding-bottom: 0;
          height: 110px ;
        }



        .navbar-toggle {
          color: #fff;
          padding: 0;
          margin-top: 13px;
          margin-bottom: 0;
          font-size: 52px;
        }

        .navbar-fixed-top.is-scrolling, .navbar-fixed-top.innernav {
        background: #323a45;
        background: #1a1a1a;
        border: 0;
        padding-top: 5px;
        padding-bottom: 5px;
        height: 120px;
        }

        .navbar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
          margin-left: -15px;
          margin-top: 11px !important;
          margin-bottom: 11px !important;
        }
        .pricing-tab {
          border-color: #CACACA;
          margin-top: 12px;
        border: 2px solid #CACACA;
        }


        .navbar-nav.navbar-right:last-child {
        margin-right: -15px;
        margin-top: 9px;
        }

        .is-scrolling .navbar-nav>.active>a, .is-scrolling .navbar-nav>.active>a:hover, .is-scrolling .navbar-nav>.active>a:focus, .is-scrolling .navbar-nav>.current-menu-item>a, .is-scrolling .navbar-nav>.current-menu-item>a:hover, .is-scrolling .navbar-nav>.current-menu-item>a:focus, .innernav .navbar-nav>.current-menu-item>a, .innernav .navbar-nav>.current-menu-item>a:hover, .innernav .navbar-nav>.current-menu-item>a:focus {
        color: #FFFFFF;
        background-color: transparent;
        }

        .hero-section { margin-left: 784px}

        .btn-color, .btn-color-ghost:hover {
          background: #05547b;
          color: #fff;
          border-color: #D0D0D0;

        }

        .btn-ghost {
          background: transparent;
          color: #fff;
          border-color: #D0D0D0;
        }

        .page-id-173 h1 {
          padding-top: 25px;
        text-align:center;
        }
        .footer-nav a {
          color: #fff;
          display: none;
        }

        .navbar-nav>li>a {
          font-size: 16px;
          line-height: 16px;

        }

        section#section-35.section-type-followus.newsletter {
          background-color: #111!important;
        }

        footer#footer.footer.dark-bg
        {
          background-color: #05547B!important;
        }

        .navbar-nav.navbar-right:last-child {background:#1a1a1a;
          margin-top: 20px;
          margin-left: -40px;
          padding-left: 40px;
          padding-bottom: 19px;
        }

        @media (max-width: 450px) {

            .navbar>.container .navbar-brand {
              width: 225px!important;
            }

            .navbar-nav>lu>li {

            }
        }

        .overlay {
            height: 24px!important;
            margin-bottom: 35px!important;
        }
        </style>


        <link rel="stylesheet" type="text/css" media="all" href="/css/dropspotcleaner/boostrapmods.css" />
    </head>
    <body>
         <header class="header standard " id="top">
                <div class="overlay ">
                    <nav class="navbar navbar-fixed-top is-scrolling" role="navigation">
                        <div class="container">
                            <div class="row">
                        	   <div class="col-md-12">
                                    <div class="navbar-header">
                    					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    						<i class="icon icon_menu"></i>
                    					</button>
                						<a href="http://www.dropspotcleaner.com/" class="navbar-brand img-logo scrollto">
                							<img src="/css/dropspotcleaner/img/dropspot2.png" alt="Logo">
                						</a>
                                    </div>
                                    <div class="navbar-collapse collapse" style="height: 1px;">
                                        <ul class="navigation nav navbar-right navbar-nav">
                        			         <li id="menu-item-6" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-5 current_page_item menu-item-6"><a href="http://www.dropspotcleaner.com/#section-about" class="is-external">ABOUT</a></li>
                                             <li id="menu-item-143" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-5 current_page_item menu-item-143"><a href="http://www.dropspotcleaner.com/#section-mission" class="is-external">MISSION</a></li>
                                             <li id="menu-item-135" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-5 current_page_item menu-item-135"><a href="http://www.dropspotcleaner.com/#section-howitworks" class="is-external">HOW IT WORKS</a></li>
                                             <li id="menu-item-177" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-5 current_page_item menu-item-177"><a href="http://www.dropspotcleaner.com/#section-pricing" class="is-external">PRICING</a></li>
                                             <li id="menu-item-138" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-5 current_page_item menu-item-138"><a href="http://www.dropspotcleaner.com/#section-getdropspot" class="is-external">GET DROP SPOT</a></li>
                                             <li id="menu-item-191" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-5 current_page_item menu-item-191"><a href="http://www.dropspotcleaner.com/#section-register" class="is-external">REGISTER</a></li>
                                        </ul>
                                    </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>

        <div style='min-height:600px;' id="mainContent">
            <?php
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content
            ?>
        </div>


        <div>
            <!-- div id='footer'>
                <div id='inner-footer'>
                    &nbsp;
                </div>
            </div>-->

            <footer class="footer dark-bg" id="footer">
                <div class="container">
                    <div class="wrapper-lg">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#top" class="scrollto" title="Drop Spot Cleaners"></a>
    						</div>
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="footer-nav no-margin">
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Blog</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="#">Terms</a></li>
                                        <li><a href="#">Privacy</a></li>
                                    </ul>
                                    <p class="footer-copy">Designed by <a href="http://www.nuexpression.com/" target="blank">Nu expression</a> of Winston Salem</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</footer>
        </div>

    </body>
    <script>
    $(".container>.row>.container").removeClass("container");
    $(".container>.row>.container").addClass("offset1");
    $(".container>.row>.span7").addClass("offset1");
    $(".container>.span9").addClass("offset1");
    $(".container>.span9").css("margin-left","146px");
    </script>

</html>
