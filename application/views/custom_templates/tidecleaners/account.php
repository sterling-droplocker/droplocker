<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://static.wixstatic.com/media/4bc32b_8f670db59bd641a099a8601871a2e6d5~mv2_d_1399_1539_s_2.png/v1/fill/w_136,h_150,al_c,usm_0.66_1.00_0.01,blur_2/Tide%20Cleaners%20Vertical%20Logo.png">

    <title>My Account | 24/7 Dry Cleaning and Laundry</title>

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="https://getbootstrap.com/docs/3.3/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/tidecleaners/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="https://getbootstrap.com/docs/3.3/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="https://getbootstrap.com/docs/3.3/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" />


    <?php //compatibility layer.. This allow to use boostrap 3+ with boostrap 2x ?>
    <link rel='stylesheet' id='custom-style-css'  href='/css/tidecleaners/bootstrap2x.css' type='text/css' media='all' />

    <link rel='stylesheet' id='custom-style-css-2'  href='/css/tidecleaners/custom.css?<?=time()?>' type='text/css' media='all' />

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/3.3/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/docs/3.3/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="https://getbootstrap.com/docs/3.3/assets/js/ie10-viewport-bug-workaround.js"></script>

    <script src="/js/tidecleaners/dropdown.js"></script>

    <script type="text/javascript">
      var $ = jQuery.noConflict();
    </script>

  <?= $_scripts ?>
  <?= $javascript ?>
  <?= $style ?>
  <?= $_styles ?>
</head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand myLogo" href="#">
            <img src="/images/tidecleaners/Tide_Services_Cleaners_Logo.png" />
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="https://www.tidecleaners.com/how-it-works">How It Works</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">Property Info</a>
                    <ul class="dropdown-menu">
                        <li><a href="https://www.tidecleaners.com/property-info">Dry Cleaning Lockers</a></li>
                        <li><a href="https://www.tidecleaners.com/package-solutions">Package Solutions</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">Locations</a>
                    <ul class="dropdown-menu">
                        <li><a href="https://www.tidecleaners.com/locations">Find a Location</a></li>
                        <li><a href="https://www.tidecleaners.com/request-an-install">Request an Install</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">Prices</a>
                    <ul class="dropdown-menu">
                        <li><a href="https://www.tidecleaners.com/chicago-pricing">Chicago Pricing</a></li>
                        <li><a href="https://www.tidecleaners.com/dmv-pricing">DMW Pricing</a></li>
                        <li><a href="https://www.tidecleaners.com/philadelphia-pricing">Philadelphia Pricing</a></li>
                        <li><a href="https://www.tidecleaners.com/dallas-pricing">Dallas Pricing</a></li>
                        <li><a href="https://www.tidecleaners.com/denver-pricing">Denver Pricing</a></li>
                        <li><a href="https://www.tidecleaners.com/nashville-pricing">Nashville Pricing</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">Help</a>
                    <ul class="dropdown-menu">
                        <li><a href="https://usepressbox.zendesk.com/hc/en-us">Helpcenter</a></li>
                        <li><a href="https://www.tidecleaners.com/turn-around">Turnaround</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">LOG IN / GET STARTED</a>
                    <ul class="dropdown-menu">
                        <li><a href="https://myaccount.chitidecleaners.com/">Chicago</a></li>
                        <li><a href="https://myaccount.dctidecleaners.com/">DMW</a></li>
                        <li><a href="https://myaccount.phitidecleaners.com/">Philadelphia</a></li>
                        <li><a href="https://myaccount.nashtidecleaners.com/">Nashville</a></li>
                        <li><a href="https://myaccount.dfwtidecleaners.com/">Dallas</a></li>
                        <li><a href="https://myaccount.dentidecleaners.com/">Denver</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" id="bootstrap2x">
        <br/>
        <?php echo $content; ?>
    </div><!-- /.container -->

    <footer>
    <div class="navbar navbar-inverse">
    <div class="container">
   <div class="row">
   
            <div class="col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 col-sm-6 col-xs-12">
                <ul class="footer-list">    
                    <li>
                        <a href="http://www.tidecleaners.com/">Home</a>
                    </li>
                           
                    <li>
                        <a href="http://www.tidecleaners.com/how-it-works">How it Works</a>
                    </li>
                           
                    <li>
                       <a href="http://www.tidecleaners.com/property-info">Property Info</a>
                    </li>
                    
                    <li>
                       <a href="http://www.tidecleaners.com/blog">Blog</a>
                    </li>
                    
                    <li>
                       <a href="https://usepressbox.zendesk.com/hc/en-us">Help</a>
                    </li>

                    <li>
                       <a href="http://www.tidecleaners.com/chicago-pricing">Prices</a>
                    </li>
                 </ul>
            </div>
             
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <ul class="footer-list">
                    <li>
                        <a href="http://www.pg.com/en_US/terms_conditions/index.shtml#use">Website Terms & Conditions</a>
                    </li>
                           
                    <li>
                        <a href="http://www.tidecleaners.com/terms">App Terms & Conditions</a>
                    </li>
                           
                    <li>
                       <a href="http://www.pg.com/privacy/english/privacy_notice.shtml">Privacy</a>
                    </li>
                    
                    <li>
                       <a href="http://www.tidecleaners.com/locations">Find a Location</a>
                    </li>
                    
                    <li>
                       <a href="http://www.tidecleaners.com/request-an-install">Request an Install</a>
                    </li>

                    <li>
                       <a href="http://www.tidecleaners.com/find-best-dry-cleaners">Dry Cleaner Near Me</a>
                    </li>
                 </ul>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class='icon social fb'><a href="https://www.facebook.com/tidecleaners/"><i class='fa fa-facebook'></i></a></div>
                <div class='icon social tw'><a href="https://twitter.com/tide_cleaners/"><i class='fa fa-twitter'></i></a></div>
                <div class='icon social in'><a href="https://instagram.com/tidecleaners/"><i class='fa fa-linkedin'></i></a></div>
                <br/>
            </div>            
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <a href="https://www.bbb.org/cincinnati/business-reviews/manufacturers-and-producers/the-procter-gamble-company-in-cincinnati-oh-3036/" target="_blank">
                 <img class="img-responsive center-block" src="/images/tidecleaners/accredited.png" width="119" height="auto">
                </a>
                <br/>
            </div>
            
            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a href="https://play.google.com/store/apps/details?id=com.usepressbox.pressbox&hl=en_US">
                <img src="/images/tidecleaners/icon-google-play.png" alt="Get it on Google Play" width="119" height="auto">
            </a>
            <br/><br/>
            <a href="https://itunes.apple.com/us/app/tide-cleaners/id890489860?mt=8">
                <img src="/images/tidecleaners/icon-app-store.png" alt="Download on the App Store" width="119" height="auto"/>  
            </a>
            </div>
    </div>
    </div>
    </footer>
  </body>
</html>
