<?php $styleVersion = filemtime("css/myvaletlaundry/site.css"); ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Valet Laundry | The new standard in convenience</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/css/myvaletlaundry/site.css?v=<?= $styleVersion; ?>">
    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
</head>
<body>
    <header class="clear">
         <section class="jumbotron">
            <div class="container header">
                <div class="pull-left">
                    <a href="https://www.myvaletlaundry.com/"><img src="/images/myvaletlaundry/logo.png" /></a>
                </div>
                <div class="pull-right">
                    <p class="tagline">24/7 - Dry Cleaning - Wash &amp; Fold</p>
                </div>
            </div>
            <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">

                    <!-- Inicia Menu -->
                    <ul class="nav navbar-nav">
                        <li><a href="https://www.myvaletlaundry.com">HOME</a></li>
                        <li><a href="https://www.myvaletlaundry.com">HOW IT WORKS</a></li>
                        <li><a href="https://www.myvaletlaundry.com">RATES</a></li>
                        <li><a href="https://www.myvaletlaundry.com">LOCATIONS</a></li>
                        <li><a href="https://www.myvaletlaundry.com">CONTACT US</a></li>
                    </ul>
                </div>
            </nav>
        </section>


    </header>
    <section class="main container clear">
        <div class="row">
        <br/><br/>
                <?php echo $content; ?>
        </div>
    </section>
    <br/><br/><br/>
    <footer class="clear">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <img src="/images/myvaletlaundry/logo.png" />
                    <p>200 Highland Ave, Suite 401</p>
                    <p>Needham, MA 02494</p>
                    <p>Tel: 1-617-433-8554</p>
                    <p>Email: <a href="mailto:support@myvaletlaundry.com">support@myvaletlaundry.com</a></p>
                    <p>© 2017 Valet Laundry</p>
                </div>
            </div>
        </div>


    </footer>
</body>
</html>
