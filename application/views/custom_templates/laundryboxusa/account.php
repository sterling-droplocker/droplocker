<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/laundryboxusa/style.css?v=2">

	<?= $style ?>
	<?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

	<?= $_scripts ?>
	<?= $javascript ?>
</head>
<body>
<div id="fb-root"></div>
<div class="container">
    <div>
        <!-- End of facebook code -->

		<?
		//if this is using the exterior pages - make sure NOT to hide the header/footer
		// $responsive = '';
		// $uri_string = $this->uri->uri_string();
		// if( strpos($uri_string,'main/') === false ){
		$responsive = 'hidden-phone hidden-tablet';
		// }
		?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="/main"><img src="/images/laundryboxusa/Laundry_Box_Logo_Horizontal.png?v=1" style="height:46px;" /></a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="http://washboxla.com/#steps-section">How it works</a></li>
                            <li><a href="http://washboxla.com/about">About us</a></li>
                            <li><a href="http://washboxla.com/services">Services</a></li>
                            <li><a href="http://washboxla.com/prices">Price List</a></li>
                            <li><a href="http://washboxla.com/reviews">Reviews</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container">
                    <div id="" class="row-fluid">
                        <div class="span10 center">
                            <div class="navbar">
                                <div id="upper-nav">
                                    <a href="/property">For Property Managers</a>
                                </div>
                                <div class="navbar-inner-menu">
                                    <a href="http://washboxla.com">
                                        <img src="/images/laundryboxusa/Laundry_Box_Logo_Horizontal.png?v=1" class="logo">
                                    </a>
                                    <ul class="nav">
                                        <li><a class="navText" href="http://washboxla.com/#steps-section">How it works</a></li>
                                        <li><a class="navText" href="http://washboxla.com/about">About us</a></li>
                                        <li><a class="navText" href="http://washboxla.com/services">Services</a></li>
                                        <li><a class="navText" href="http://washboxla.com/prices">Price List</a></li>
                                        <li><a class="navText" href="http://washboxla.com/reviews">Reviews</a></li>
                                        <li class="login"><a class="navText" href="https://myaccount.washboxla.com/login">Login</a></li>
                                        <li class="login"><a class="navText" href="https://myaccount.washboxla.com/register" id="signup-btn">Sign up</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" style=''>
            <div class="span10 center">
				<?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="span10 center" id="footerInner">
                <div class="row-fluid">
                    <div class="span3 footerSpan">
                        <h2 class="footerTitle">Services</h2>
                        <ul id="footerServices" class="inline">
                            <li>
                                <a href="http://washboxla.com/services" class="whiteLink">Dry Cleaning</a><br />
                                <a href="http://washboxla.com/services" class="whiteLink">Wash & Fold</a><br />
                                <a href="http://washboxla.com/services" class="whiteLink">Shoe and Boot Shine</a><br />
                                <a href="http://washboxla.com/services" class="whiteLink">Wholesale</a><br />
                                <a href="http://washboxla.com/services" class="whiteLink">Linens</a><br />
                                <a href="http://washboxla.com/services" class="whiteLink">24/7 Locations</a><br />
                                <a href="http://washboxla.com/services" class="whiteLink">Comforters</a><br />
                            </li>
                        </ul>
                    </div>

                    <div class="span3 footerSpan">
                        <h2 class="footerTitle">Sitemap</h2>
                        <ul id="footerSitemap" class="inline">
                            <li>
                                <a href="http://washboxla.com/about">About Us</a><br />
                                <a href="http://washboxla.com/services">Services</a><br />
                                <a href="http://washboxla.com/environment">Environmental</a><br />
                                <a href="http://washboxla.com/prices">Price List</a><br />
                                <a href="http://washboxla.com/reviews">Reviews</a><br />
                                <a href="http://washboxla.com/property">Property Managers</a><br />
                                <a href="http://washboxla.com/privacy">Privacy Policy</a><br />
                            </li>
                        </ul>
                    </div>


                    <div class="span3 footerSpan">
                        <p>"To provide our clients with the highest quality dry cleaning and laundry services in the
                            most convenient, secure and affordable way while being friendly to the environment and
                            helpful to our local communities."</p>
                    </div>

                    <div class="span3 footerSpan" style="border: none;">
                        <div class="footerTitle">Contact</div>
                        <a href="tel:8444458844">844.445.8844</a><br />
                        <a href="mailto:CS@washboxla.com">CS@washboxla.com</a>
                        <script type="text/javascript" src="//reviews.reviewmydrycleaner.com/embed/v4/148373323925073/5/3115711346"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
	<?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

	<?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
	<?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
	<?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>