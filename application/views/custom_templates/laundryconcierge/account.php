<?php

$base_url = 'https://laundry-concierge.com';
$display_login_buttons = false;

$is_bazinga = preg_match('#/bazinga/#i', $_SERVER['REQUEST_URI']);
$is_zipcar  = preg_match('#/zipcar/#i', $_SERVER['REQUEST_URI']);

?>
<!DOCTYPE html>
<html>
<head>
<title><?= $title ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
<link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script type="text/javascript" src='/js/functions.js'></script>
<?= $javascript?>
<?= $_scripts;?>
<?= $style?>
<?= $_styles ?>

<link href="/images/laundryconcierge/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<link rel="stylesheet" href="/css/laundryconcierge/template.css" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

<? if ($is_bazinga || $is_zipcar): ?>
<style type="text/css">
.white-bg {
    width:100%;
    height: 160px;
    background: #fff;
}

.header .logo1,
.header .logo2
{
    display: block;
    width: 250px;
    margin: 0;
}

.header .brand.logo1 {
    background-size: 250px auto;
}

.header .logo1 {
    margin-left: -250px;
    top: 35px;
    left: 50%;
}

.header .bazinga {
    background-image: url(/images/laundryconcierge/bazinga-logo-partner.png);
}

.header .zipcar {
    background-image: url(/images/laundryconcierge/zipcar-logo-partner.png);
}

.header .logo2 {
    top: 35px;
    left: 50%;
}

@media (max-width: 856px) and (min-width: 768px) {
    .header .logo1 {
        margin-left: -230px;
    }
}

@media (max-width: 767px) {
    .white-bg {
        height: 100px;
    }
    .header .brand.logo1 {
        background-position: 134px 30px;
        background-size: 114px 45px;
        top: 0;
    }
    .header .brand.logo2 {
        background-position: 10px 31px;
        background-size: 114px 45px;
        top: 0;
    }
}
</style>
<? endif; ?>

</head>
<body>

<div class="body">
    <div class="container">
        <div class="header">
            <div class="header-inner clearfix">
                <? if ($is_bazinga): ?>
                    <div class="white-bg"></div>
                    <a class="brand logo1 bazinga" href="<?= $base_url; ?>/">
                        <span class="site-title" title="Bazinga">Bazinga</span>
                    </a>
                    <a class="brand logo2" href="<?= $base_url; ?>/">
                        <span class="site-title" title="Laundry Concierge">Laundry Concierge</span>
                    </a>
                <? elseif ($is_zipcar): ?>
                    <div class="white-bg"></div>
                    <a class="brand logo1 zipcar" href="<?= $base_url; ?>/">
                        <span class="site-title" title="Zipcar">Zipcar</span>
                    </a>
                    <a class="brand logo2" href="<?= $base_url; ?>/">
                        <span class="site-title" title="Laundry Concierge">Laundry Concierge</span>
                    </a>
                <? else: ?>
                    <a class="brand pull-left" href="<?= $base_url; ?>/">
                        <span class="site-title" title="Laundry Concierge">Laundry Concierge</span>
                    </a>
                    <div class="header-search">
                        <ul class="nav menu" id="login_menu">
                    <? if (!$display_login_buttons): ?>
                            <li style="height:39px;"><a href="#">&nbsp;</a></li>
                    <? else: ?>
                            <li class="item-472"><a class="login_button" href="<?= $base_url; ?>/login"><img src="/images/laundryconcierge/login.png" alt="LogIn"></a></li>
                            <li class="item-473"><a class="signup_button" href="<?= $base_url; ?>/register"><img src="/images/laundryconcierge/signup.png" alt="Sign Up"></a></li>
                    <? endif; ?>
                        </ul>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <? if (!$is_bazinga && !$is_zipcar): ?>
        <div class="navigation">
            <ul class="nav menu nav-pills menu1">
                <li class="item-435 current active"><a href="<?= $base_url; ?>/">Home</a></li>
                <li class="item-466 parent"><a href="<?= $base_url; ?>/how-it-works">How It Works</a></li>
                <li class="item-467 parent"><a href="<?= $base_url; ?>/services">Our Prices</a></li>
                <li class="item-468 parent"><a href="<?= $base_url; ?>/faqs">FAQs</a></li>
            </ul>
            <ul class="nav menu menu2">
                <li class="item-469 parent"><a href="<?= $base_url; ?>/about-us">About Us</a></li>
                <li class="item-470"><a href="<?= $base_url; ?>/locations">Locations</a></li>
                <li class="item-471"><a href="<?= $base_url; ?>/contact-us">Contact Us</a></li>
            </ul>
        </div>
        <? endif; ?>
    </div>
</div>

<div id="main-content"><?php echo $content ?></div>

<div class="footer">
    <div class="container">
        <p class="pull-right" style="display:none;"><a href="<?= $base_url; ?>/" id="back-top">&nbsp;&nbsp;HOME</a></p>
        <ul class="nav menu">
            <li class="item-496"><a href="<?= $base_url; ?>/how-it-works">How It Works</a></li>
            <li class="item-497"><a href="<?= $base_url; ?>/services">Our Prices</a></li>
            <li class="item-498"><a href="<?= $base_url; ?>/faqs">FAQs</a></li>
            <li class="item-499"><a href="<?= $base_url; ?>/terms-and-conditions">Terms and Conditions</a></li>
        </ul>

        <div class="custom paypal_footer">
            <p><a href="https://www.laundry-concierge.com/services/prices"><img title="Paypal Verified" src="/images/laundryconcierge/paypal-verified-shadow.png" alt="Paypal Verified" width="135" height="135"></a></p>
        </div>

        <ul class="nav menu">
            <li class="item-500"><a href="<?= $base_url; ?>/about-us">About Us</a></li>
            <li class="item-501"><a href="<?= $base_url; ?>/about-us/testimonials">Testimonials</a></li>
            <li class="item-503"><a href="<?= $base_url; ?>/contact-us">Contact Us</a></li>
        </ul>

        <div class="custom_socialmedia">
            <p>Get Social with Laundry Concierge on</p>
            <p>Facebook, Twitter, LinkedIn, PIN us, YELP at Us or just send us an email:</p>
            <p><a title="Like Us on FB" href="https://www.facebook.com/pages/Laundry-Concierge/587847524569094" target="_blank"><img src="/images/laundryconcierge/facebook.png" alt="facebook" width="40" height="41"></a>&nbsp;&nbsp;<a href="https://twitter.com/LaundRConcierge" target="_blank"><img title="Tweet to Us" src="/images/laundryconcierge/twitter.png" alt="Twitter" width="49" height="38"></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/laundry-concierge-inc-" target="_blank"><img title="Link to Us" src="/images/laundryconcierge/linkedin.png" alt="LinkedIn" width="36" height="37"></a>&nbsp; &nbsp;<a title="Pin-Cierge Us!" href="http://www.pinterest.com/LaundRConcierge" target="_blank"><img src="/images/laundryconcierge/LCI-Pinterest-Logo-58p.png" alt="LCI-Pinterest-Logo-58p" width="42" height="42"></a>&nbsp;&nbsp;<a title="Yelp, CONCIERGE!" href="http://www.yelp.ca/biz/laundry-concierge-toronto-3" target="_blank"><img src="/images/laundryconcierge/Yelp-Icon%20purple.png" alt="Yelp-Icon purple" width="33" height="44"></a>&nbsp;&nbsp;<a href="mailto:info@laundry-concierge.com" target="_blank"><img title="Ask Us Something?" src="/images/laundryconcierge/email.png" alt="e-mail" width="49" height="37"></a>&nbsp;&nbsp;<a href="http://parkbench.com/canada/ontario/toronto/discoverydistrict/index.php?route=product/product&amp;product_id=131437" target="_blank"><img src="/images/laundryconcierge/park-bench-toronto-laundryconcierge-transparent.png" alt="park-bench-toronto-laundryconcierge-transparent" width="38" height="46"></a></p>
        </div>

        <div class="copyright">&copy; Laundry Concierge 2015</div>
    </div>
</div>

<script src="/bootstrap/js/bootstrap.min.js"></script>
<script>$('.dropdown-toggle').dropdown();</script>
<? if ($is_bazinga): ?>
<script>
    $('a[href="/login"]').attr('href', '/bazinga/login');
    $('a[href="/register"]').attr('href', '/bazinga/signup');
</script>
<? endif; ?>

<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', '<?php echo $ga?>']);
      _gaq.push(['_setDomainName', '<?=$this->business->website_url;?>']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
<?php endif; ?>
</body>
</html>
