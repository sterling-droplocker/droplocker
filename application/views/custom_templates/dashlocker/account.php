<!doctype html>

<html class="no-js"  lang="en-US" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="utf-8">

    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta class="foundation-mq">

    <!-- If Site Icon isn't set in customizer -->

    <link rel="pingback" href="http://www.dashlocker.com/xmlrpc.php">

    <title>Home - DashLocker</title>

    <!-- This site is optimized with the Yoast SEO plugin v3.5 - https://yoast.com/wordpress/plugins/seo/ -->
    <link rel="canonical" href="http://www.dashlocker.com/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Home - DashLocker" />
    <meta property="og:url" content="http://www.dashlocker.com/" />
    <meta property="og:site_name" content="DashLocker" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Home - DashLocker" />
    <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/www.dashlocker.com\/","name":"DashLocker","potentialAction":{"@type":"SearchAction","target":"http:\/\/www.dashlocker.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    <!-- / Yoast SEO plugin. -->

    <link rel='dns-prefetch' href='//s0.wp.com' />
    <link rel='dns-prefetch' href='//s.gravatar.com' />
    <link rel='dns-prefetch' href='//maxcdn.bootstrapcdn.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="DashLocker &raquo; Feed" href="http://www.dashlocker.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="DashLocker &raquo; Comments Feed" href="http://www.dashlocker.com/comments/feed/" />

    <link rel='stylesheet' id='fa-css-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?ver=4.6.1' type='text/css' media='all' />
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap-responsive.min.css" />
    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel='stylesheet' id='site-css-css'  href='/css/dashlocker/style.css' type='text/css' media='all' />
    <!-- This site uses the Google Analytics by MonsterInsights plugin v5.5.2 - Universal enabled - https://www.monsterinsights.com/ -->
    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');

        __gaTracker('create', 'UA-84080834-1', 'auto');
        __gaTracker('set', 'forceSSL', true);
        __gaTracker('send','pageview');

    </script>
    <!-- / Google Analytics by MonsterInsights -->
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <?= $_scripts?>
    <link rel='https://api.w.org/' href='http://www.dashlocker.com/wp-json/' />
    <link rel='shortlink' href='http://wp.me/P35sFe-6' />
    <link rel="alternate" type="application/json+oembed" href="http://www.dashlocker.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.dashlocker.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="http://www.dashlocker.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.dashlocker.com%2F&#038;format=xml" />

    <link rel='dns-prefetch' href='//v0.wordpress.com'>
    <link rel='dns-prefetch' href='//i0.wp.com'>
    <link rel='dns-prefetch' href='//i1.wp.com'>
    <link rel='dns-prefetch' href='//i2.wp.com'>
    <link rel="icon" href="/images/dashlocker/cropped-Website-Icon-1-32.png" sizes="32x32" />
    <link rel="icon" href="/images/dashlocker/cropped-Website-Icon-1-192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/images/dashlocker/cropped-Website-Icon-1-180.png" />
    <meta name="msapplication-TileImage" content="/images/dashlocker/cropped-Website-Icon-1-270.png" />

    <!-- Drop Google Analytics here -->
    <!-- end analytics -->
    <style>
        #content .btn-info, #content .btn-primary, #content .btn-danger, #content .btn-success {
            background: #2e4d8d;
            color: #FFF !important;
            border: 0;
        }

        #content .alert-info {
            color: #0a0a0a;
            background-color: rgba(74, 147, 155, 0.2);
            border: none;
            border-radius: 0;
        }

        #content .user-info-box {
            margin-top: 2em;
        }

        #default_account_nav ul {
            list-style: none;
            margin-left: 0px !important;
        }

        #content {
            width: 90%;
            margin: 0 auto;
        }

        .row .row {
            margin: 0 auto;
        }
    </style>
</head>

<!-- Uncomment this line if using the Off-Canvas Menu -->

<body class="home page page-id-6 page-template-default">

<div class="off-canvas-wrapper">

    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas position-left" id="off-canvas" data-off-canvas data-position="left">
            <ul id="menu-main-menu" class="vertical menu" data-accordion-menu><li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-36 active"><a href="http://www.dashlocker.com/">Home</a></li>
                <li id="menu-item-324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-324"><a href="http://www.dashlocker.com/laundry-services/">Laundry Services</a>
                    <ul class="vertical menu">
                        <li id="menu-item-298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-298"><a href="http://www.dashlocker.com/laundry-services/concierge-services/">Concierge Services</a></li>
                        <li id="menu-item-299" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299"><a href="http://www.dashlocker.com/laundry-services/locker-services/">Locker Services</a></li>
                        <li id="menu-item-160" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-160"><a href="http://www.dashlocker.com/laundry-services/how-we-work/">How We Work</a></li>
                        <li id="menu-item-39" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39"><a href="http://www.dashlocker.com/laundry-services/place-an-order/">Place an Order</a></li>
                    </ul>
                </li>
                <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a href="http://www.dashlocker.com/property-managers/">Property Managers</a></li>
                <li id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a href="http://www.dashlocker.com/pricing/">Pricing</a></li>
                <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-31"><a href="http://www.dashlocker.com/about/">About</a>
                    <ul class="vertical menu">
                        <li id="menu-item-197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197"><a href="http://www.dashlocker.com/about/">About Us</a></li>
                        <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://www.dashlocker.com/about/testimonials/">Testimonials</a></li>
                        <li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="http://www.dashlocker.com/about/careers/">Careers</a></li>
                        <li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="http://www.dashlocker.com/about/contact/">Contact</a></li>
                        <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="http://www.dashlocker.com/about/blog/">Blog</a></li>
                        <li id="menu-item-196" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-196"><a href="http://www.dashlocker.com/about/faq/">FAQ</a></li>
                    </ul>
                </li>
            </ul></div>

        <div class="off-canvas-content" data-off-canvas-content>

            <header class="header" role="banner">
                <div class="row expanded upper-nav">
                    <div class="row">
                        <nav role="navigation">
                            <ul id="menu-upper-nav" class="menu"><li id="menu-item-42" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-42"><a href="https://myaccount.dashlocker.com/register">Sign Up</a></li>
                                <li id="menu-item-43" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-43"><a href="https://myaccount.dashlocker.com/login">Login</a></li>
                                <li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a href="http://www.dashlocker.com/about/blog/">Blog</a></li>
                                <li id="menu-item-45" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://www.dashlocker.com/about/contact/">Contact</a></li>
                            </ul>                </nav>
                    </div>
                </div>
                <div class="row">
                    <!-- This navs will be applied to the topbar, above all content
                         To see additional nav styles, visit the /parts directory -->
                    <!-- By default, this menu will use off-canvas for small
                         and a topbar for medium-up -->

                    <div class="top-bar row" id="top-bar-menu" data-equalizer data-equalize-on="medium">
                        <div class="top-bar-left float-left" data-equalizer-watch>
                            <ul class="menu logo">
                                <li><a href="http://www.dashlocker.com"><img src="/images/dashlocker/dashlocker%20header%20logo.png" alt="DashLocker" title="DashLocker" class="logo" /></a></li>
                            </ul>
                        </div>
                        <div class="top-bar-right show-for-medium" data-equalizer-watch>
                            <ul id="menu-main-menu-1" class="vertical medium-horizontal menu" data-responsive-menu="accordion medium-dropdown"><li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-36 active"><a href="http://www.dashlocker.com/">Home</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-324"><a href="http://www.dashlocker.com/laundry-services/">Laundry Services</a>
                                    <ul class="menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-298"><a href="http://www.dashlocker.com/laundry-services/concierge-services/">Concierge Services</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-299"><a href="http://www.dashlocker.com/laundry-services/locker-services/">Locker Services</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-160"><a href="http://www.dashlocker.com/laundry-services/how-we-work/">How We Work</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-39"><a href="http://www.dashlocker.com/laundry-services/place-an-order/">Place an Order</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a href="http://www.dashlocker.com/property-managers/">Property Managers</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a href="http://www.dashlocker.com/pricing/">Pricing</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-31"><a href="http://www.dashlocker.com/about/">About</a>
                                    <ul class="menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197"><a href="http://www.dashlocker.com/about/">About Us</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://www.dashlocker.com/about/testimonials/">Testimonials</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="http://www.dashlocker.com/about/careers/">Careers</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="http://www.dashlocker.com/about/contact/">Contact</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="http://www.dashlocker.com/about/blog/">Blog</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-196"><a href="http://www.dashlocker.com/about/faq/">FAQ</a></li>
                                    </ul>
                                </li>
                            </ul>	</div>
                        <div class="top-bar-right float-right show-for-small-only">
                            <ul class="menu">
                                <li><a data-toggle="off-canvas"><i class="fa fa-bars"></i> Menu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header> <!-- end .header -->
            <div id="content">
                <?=$content?>
            </div>
            <footer class="footer" role="contentinfo">

                <div class="lower-footer">
                    <div class="row">
                        <div class="small-12 medium-3 columns">
                            <div id="text-3" class="widget widget_text">
                                <div class="textwidget"><p><img src="/images/dashlocker/DashLocker-logo_white_horizontal.png" alt="DashLocker" /></p>
                                    <p>633 3rd Avenue<br />
                                        17th Floor<br />
                                        New York, New York 10017</p>
                                    <p>Phone: <span class="mobile_tel">(718) 361-1641</span><br />
                                        email: <a href="/cdn-cgi/l/email-protection#1a696f6a6a75686e5a7e7b6972767579717f6834797577"><span class="__cf_email__" data-cfemail="83f0f6f3f3ecf1f7c3e7e2f0ebefece0e8e6f1ade0ecee">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
                                </div>
                            </div>								</div>
                        <div class="small-12 medium-3 columns">
                            <div id="nav_menu-2" class="widget widget_nav_menu"><h4 class="widgettitle">Company:</h4><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46"><a href="http://www.dashlocker.com/about/blog/">Blog</a></li>
                                        <li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="http://www.dashlocker.com/about/careers/">Careers</a></li>
                                        <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a href="http://www.dashlocker.com/about/contact/">Contact</a></li>
                                        <li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="http://www.dashlocker.com/terms/">Terms</a></li>
                                    </ul></div></div>								</div>
                        <div class="small-12 medium-3 columns">
                            <div id="text-4" class="widget widget_text"><h4 class="widgettitle">Download our App:</h4>			<div class="textwidget"><p class="center appimg"><a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities&hl=en" target="_blank"><img src="/images/dashlocker/Google_Button.png" alt="Google Play"  /></a></p><p class="center appimg"><a href="https://itunes.apple.com/us/app/droplocker/id686269581?mt=8#" target="_blank"><img src="/images/dashlocker/Apple_Button.png" alt="Apple App Store" /></a></p></div>
                            </div>								</div>
                        <div class="small-12 medium-3 columns">
                            <div id="text-5" class="widget widget_text"><h4 class="widgettitle">Follow Us:</h4>			<div class="textwidget"><ul class="follow">
                                        <li>
                                            <a href="https://www.facebook.com/DashLocker"><i class="fa fa-facebook-square"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/DashLocker"><i class="fa fa-twitter-square"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.linkedin.com/company/dashlocker"><i class="fa fa-linkedin-square"></i></a>
                                        </li>
                                    </ul></div>
                            </div>								</div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <p class="source-org copyright">&copy; 2016 <a href="http://www.dashlocker.com">DashLocker</a></p>
                        </div>
                    </div>
                </div>
            </footer> <!-- end .footer -->
        </div>  <!-- end .main-content -->
    </div> <!-- end .off-canvas-wrapper-inner -->
</div> <!-- end .off-canvas-wrapper -->



<script type='text/javascript' src='http://s.gravatar.com/js/gprofiles.js?ver=2016Sepaa'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var WPGroHo = {"my_hash":""};
    /* ]]> */
</script>
<script type='text/javascript' src='/js/dashlocker/jquery.js'></script>
<script type='text/javascript' src='/js/dashlocker/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/js/dashlocker/foundation.js'></script>
<script type='text/javascript' src='/js/dashlocker/scripts.js'></script>
<script type='text/javascript' src='http://stats.wp.com/e-201638.js' async defer></script>
<script type='text/javascript'>
    _stq = window._stq || [];
    _stq.push([ 'view', {v:'ext',j:'1:4.3.1',blog:'45630836',post:'6',tz:'-4',srv:'www.dashlocker.com'} ]);
    _stq.push([ 'clickTrackerInit', '45630836', '6' ]);
</script>
<script type="text/javascript">/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script>
</body>
</html> <!-- end page -->