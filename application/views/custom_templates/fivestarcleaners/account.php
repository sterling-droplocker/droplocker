<!DOCTYPE html>
<!--[if IE 8]> 
<html class="ie ie8" lang="en-US" prefix="og: http://ogp.me/ns#">
    <![endif]-->
    <!--[if IE 9]> 
    <html class="ie ie9" lang="en-US" prefix="og: http://ogp.me/ns#">
        <![endif]-->
        <!--[if gt IE 9]><!--> 
        <html lang="en-US" prefix="og: http://ogp.me/ns#">
            <!--<![endif]-->
            <head>
                <meta charset="UTF-8" />
                <title>Five Star Cleaners - Dry Cleaning Questions</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="pingback" href="https://myfivestarcleaners.com/xmlrpc.php" />
                <link rel="shortcut icon" href="/images/fivestarcleaners/FSC_Laundry_Favicon.png" />
                <link rel="apple-touch-icon-precomposed" href="/images/fivestarcleaners/FSC_LaundryLogo_152x152.png" />
                <style type="text/css">
                    img.wp-smiley,
                    img.emoji {
                    display: inline !important;
                    border: none !important;
                    box-shadow: none !important;
                    height: 1em !important;
                    width: 1em !important;
                    margin: 0 .07em !important;
                    vertical-align: -0.1em !important;
                    background: none !important;
                    padding: 0 !important;
                    }
                </style>
                <link rel='stylesheet' id='jquery-colorbox.css-css'  href='/css/fivestarcleaners/colorbox.css?ver=1.0' type='text/css' media='all' />
                <link rel='stylesheet' id='wpp-popup-styles-css'  href='/css/fivestarcleaners/popup-styles.css?ver=1.0' type='text/css' media='all' />
                <link rel='stylesheet' id='rs-plugin-settings-css'  href='/css/fivestarcleaners/settings.css?ver=5.4.7' type='text/css' media='all' />
                <style id='rs-plugin-settings-inline-css' type='text/css'>
                    .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.largeredbtn{font-family:"Raleway",sans-serif;font-weight:900;font-size:16px;line-height:60px;color:#fff !important;text-decoration:none;padding-left:40px;padding-right:80px;padding-top:22px;padding-bottom:22px;background:rgb(234,91,31); background:-moz-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,rgba(234,91,31,1)),color-stop(100%,rgba(227,58,12,1))); background:-webkit-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:-o-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:-ms-linear-gradient(top,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); background:linear-gradient(to bottom,rgba(234,91,31,1) 0%,rgba(227,58,12,1) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#ea5b1f',endColorstr='#e33a0c',GradientType=0 )}.largeredbtn:hover{background:rgb(227,58,12); background:-moz-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,rgba(227,58,12,1)),color-stop(100%,rgba(234,91,31,1))); background:-webkit-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:-o-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:-ms-linear-gradient(top,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); background:linear-gradient(to bottom,rgba(227,58,12,1) 0%,rgba(234,91,31,1) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#e33a0c',endColorstr='#ea5b1f',GradientType=0 )}.fullrounded img{-webkit-border-radius:400px;-moz-border-radius:400px;border-radius:400px}
                </style>
                <link rel='stylesheet' id='testimonial-rotator-style-css'  href='/css/fivestarcleaners/testimonial-rotator-style.css?ver=4.9.7' type='text/css' media='all' />
                <link rel='stylesheet' id='font-awesome-css'  href='/css/fivestarcleaners/font-awesome.min.css?ver=4.11.2.1' type='text/css' media='all' />
                <link rel='stylesheet' id='smartmag-fonts-css'  href='/css/fivestarcleaners/fonts-css.css' type='text/css' media='all' />
                <link rel='stylesheet' id='smartmag-core-css'  href='/css/fivestarcleaners/style-child.css?ver=2.6.2' type='text/css' media='all' />
                <link rel='stylesheet' id='smartmag-responsive-css'  href='/css/fivestarcleaners/responsive.css?ver=2.6.2' type='text/css' media='all' />
                <link rel='stylesheet' id='pretty-photo-css'  href='/css/fivestarcleaners/prettyPhoto.css?ver=2.6.2' type='text/css' media='all' />
                <link rel='stylesheet' id='smartmag-font-awesome-css'  href='/css/fivestarcleaners/font-awesome.min.css?ver=2.6.2' type='text/css' media='all' />
                <link rel='stylesheet' id='slb_core-css'  href='/css/fivestarcleaners/app.css?ver=2.7.0' type='text/css' media='all' />
                <link rel='stylesheet' id='csl-slplus_user_header_css-css'  href='/css/fivestarcleaners/a_gallery_style.css?ver=4.9.7' type='text/css' media='all' />
                <style id='csl-slplus_user_header_css-inline-css' type='text/css'>
                    div#map img {
                    background-color: transparent;
                    box-shadow: none;
                    border: 0;
                    max-width: none;
                    opacity: 1.0
                    }
                    div#map div {
                    overflow: visible
                    }
                    div#map .gm-style-cc > div {
                    word-wrap: normal
                    }
                    div#map img[src='/css/fivestarcleaners/iws3.png'] {
                    display: none
                    }
                    .slp_search_form .search_box {
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-between;
                    align-items: flex-start;
                    align-content: stretch
                    }
                    .slp_search_form .search_box .search_item {
                    flex: 1 1 auto;
                    display: flex;
                    align-items: flex-start;
                    justify-content: stretch;
                    margin-bottom: .25em
                    }
                    .slp_search_form .search_box .search_item label {
                    text-align: right;
                    min-width: 8em;
                    margin-right: .25em
                    }
                    .slp_search_form .search_box .search_item div {
                    flex: 1 1 auto;
                    display: flex
                    }
                    .slp_search_form .search_box .search_item #radius_in_submit {
                    text-align: right
                    }
                    .slp_search_form .search_box .search_item .slp_ui_button {
                    margin: .25em 0
                    }
                    .store_locator_plus.tagline {
                    font-size: .75em;
                    text-align: right
                    }
                    .slp_results_container .results_wrapper {
                    margin: .5em 0;
                    padding: .5em;
                    border: solid 1px lightgrey;
                    border-radius: .5em
                    }
                    .slp_results_container .results_wrapper:hover {
                    background-color: lightgrey;
                    border: solid 1px grey
                    }
                    .slp_results_container .results_wrapper .location_name {
                    font-size: 1.15em
                    }
                    .slp_results_container .results_wrapper .location_distance {
                    float: right;
                    vertical-align: text-top
                    }
                </style>
                <script type='text/javascript' src='/js/fivestarcleaners/jquery.js?ver=1.12.4'></script>
                <script type='text/javascript' src='/js/fivestarcleaners/jquery-migrate.min.js?ver=1.4.1'></script>
                <script type='text/javascript' src='/js/fivestarcleaners/jquery.colorbox-min.js?ver=1.0'></script>
                <style type="text/css" media="screen">
                    div.printfriendly a, div.printfriendly a:link, div.printfriendly a:hover, div.printfriendly a:visited {
                    text-decoration: none;
                    border: none;
                    }
                </style>
                <style type="text/css" media="screen">
                    div.printfriendly {
                    margin: 12px 12px 12px 12px;
                    position: relative;
                    z-index: 1000;
                    }
                    div.printfriendly a, div.printfriendly a:link, div.printfriendly a:visited {
                    font-size: 14px;
                    color: #6D9F00;
                    vertical-align: bottom;
                    }
                    .printfriendly a {
                    box-shadow:none;
                    }
                    .printfriendly a:hover {
                    cursor: pointer;
                    }
                    .printfriendly a img  {
                    border: none;
                    padding:0;
                    margin-right: 6px;
                    box-shadow: none;
                    -webkit-box-shadow: none;
                    -moz-box-shadow: none;
                    }
                    .printfriendly a span{
                    vertical-align: bottom;
                    }
                    .pf-alignleft {
                    float: left;
                    }
                    .pf-alignright {
                    float: right;
                    }
                    div.pf-aligncenter {
                    display: block;
                    margin-left: auto;
                    margin-right: auto;
                    text-align: center;
                    }
                </style>
                <style type="text/css" media="print">
                    .printfriendly {
                    display: none;
                    }
                </style>
                <!-- SLP Custom CSS -->
                <style type="text/css">
                    div#map.slp_map {
                    width:100%;
                    height:375px;
                    }
                    div#slp_tagline {
                    width:100%;
                    }
                </style>
                <!--[if lt IE 9]>
                <script src="https://myfivestarcleaners.com/wp-content/themes/smart-mag/js/html5.js" type="text/javascript"></script>
                <![endif]-->
                <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
                <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
                <link rel="stylesheet" href="/css/fivestarcleaners/custom.css">
                <script src='/bootstrap/js/bootstrap.min.js'></script>
                <script type="text/javascript">
                  var $ = jQuery.noConflict();
                </script>
                <?= $_scripts ?>
                <?= $javascript?>
                <?= $style?>
                <?= $_styles ?>
            </head>
            <body class="page-template page-template-page-blocks page-template-page-blocks-php page page-id-2221 right-sidebar full has-nav-dark wpb-js-composer js-comp-ver-4.11.2.1 vc_responsive">
                <div class="main-wrap">
                    <div class="top-bar">
                        <div class="wrap">
                            <section class="top-bar-content cf">
                                <div class="search">
                                    <form role="search" action="https://myfivestarcleaners.com/" method="get">
                                        <input type="text" name="s" class="query live-search-query" value="" placeholder="Search..."/>
                                        <button class="search-button" type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <!-- .search -->         
                                <div class="textwidget">
                                    <ul class="social-icons">
                                        <li><a href="https://twitter.com/FSCandLaundry" class="icon fa fa-twitter" title="Twitter"><span class="visuallyhidden">Twitter</span></a></li>
                                        <li><a href="https://www.facebook.com/pages/Five-Star-Cleaners/120088764697944?sk=wall" class="icon fa fa-facebook" title="Facebook"><span class="visuallyhidden">Facebook</span></a></li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div id="main-head" class="main-head">
                        <div class="wrap">
                            <header class="default">
                                <div class="title">
                                    <a href="https://myfivestarcleaners.com/" title="Five Star Cleaners &amp; Laundry" rel="home" class">
                                    <img src="/images/fivestarcleaners/FSC_LaundryLogo.png" class="logo-image" alt="Five Star Cleaners &amp; Laundry" data-at2x="/images/fivestarcleaners/FSC_LaundryLogo.png" />
                                    </a>        
                                </div>
                            </header>
                            <div class="navigation-wrap cf">
                                <nav class="navigation cf nav-dark">
                                    <div class>
                                        <div class="mobile" data-type="classic" data-search="1">
                                            <a href="#" class="selected">
                                            <span class="text">Navigate</span><span class="current"></span> <i class="hamburger fa fa-bars"></i>
                                            </a>
                                        </div>
                                        <div class="menu-fsc-main-container">
                                            <ul id="menu-fsc-main" class="menu">
                                                <li id="menu-item-2846" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2846"><a href="https://myfivestarcleaners.com/">Home</a></li>
                                                <li id="menu-item-2169" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2169"><a href="https://myfivestarcleaners.com/services/">Cleaning Services</a></li>
                                                <li id="menu-item-2182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2182"><a href="https://myfivestarcleaners.com/coupons/">Coupons</a></li>
                                                <li id="menu-item-2186" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2186"><a href="https://myfivestarcleaners.com/locations-contact/">Locations</a></li>
                                                <li id="menu-item-2395" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2395"><a href="http://fivestarweddinggown.com/">Wedding Gowns</a></li>
                                                <li id="menu-item-2223" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2395"><a href="https://myfivestarcleaners.com/customer-faqs/">Customer FAQs</a></li>
                                                <li id="menu-item-2512" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2221 current_page_item menu-item-2223"><a href="/account">Account</a></li>
                                            </ul>
                                        </div>
                                        <div class="mobile-menu-container">
                                            <ul id="menu-fsc-main-1" class="menu mobile-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-2846"><a href="https://myfivestarcleaners.com/">Home</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2169"><a href="https://myfivestarcleaners.com/services/">Cleaning Services</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2182"><a href="https://myfivestarcleaners.com/coupons/">Coupons</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2186"><a href="https://myfivestarcleaners.com/locations-contact/">Locations</a></li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2395"><a href="http://fivestarweddinggown.com/">Wedding Gowns</a></li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2395"><a href="https://myfivestarcleaners.com/customer-faqs/">Customer FAQs</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2221 current_page_item menu-item-2223"><a href="/account">Account</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="breadcrumbs-wrap">
                        <div class="wrap">
                            <div class="breadcrumbs"><span class="location">You are at:</span><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://myfivestarcleaners.com/"><span itemprop="title">Home</span></a></span><span class="delim">&raquo;</span><span class="current">Account</span></div>
                        </div>
                    </div>
                    <div class="main wrap cf">
                        <div class="row">
                        <?= $content ?>
                        </div>
                        <!-- .row -->
                    </div>
                    <!-- .main -->
                    <footer class="main-footer">
                        <div class="lower-foot">
                            <div class="wrap">
                                <div class="widgets">
                                    <div class="textwidget">Copyright &copy; 2018 Five Star Cleaners & Laundry. Powered by <a href="https://btycreative.com" target=blank>BTYcreative</a>.</div>
                                    <div class="menu-footer-container">
                                        <ul id="menu-footer" class="menu">
                                            <li id="menu-item-2324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2324"><a href="https://myfivestarcleaners.com/community-service/">Five Star Community Service</a></li>
                                            <li id="menu-item-2317" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2317"><a href="https://myfivestarcleaners.com/contact/">Contact</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
                <!-- .main-wrap -->
                <script>(function() {
                    function addEventListener(element,event,handler) {
                      if(element.addEventListener) {
                        element.addEventListener(event,handler, false);
                      } else if(element.attachEvent){
                        element.attachEvent('on'+event,handler);
                      }
                    }
                    
                    function maybePrefixUrlField() {
                      if(this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                        this.value = "http://" + this.value;
                      }
                    }
                    
                    var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
                    if( urlFields && urlFields.length > 0 ) {
                      for( var j=0; j < urlFields.length; j++ ) {
                        addEventListener(urlFields[j],'blur',maybePrefixUrlField);
                      }
                    }/* test if browser supports date fields */
                    var testInput = document.createElement('input');
                    testInput.setAttribute('type', 'date');
                    if( testInput.type !== 'date') {
                    
                      /* add placeholder & pattern to all date fields */
                      var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
                      for(var i=0; i<dateFields.length; i++) {
                        if(!dateFields[i].placeholder) {
                          dateFields[i].placeholder = 'YYYY-MM-DD';
                        }
                        if(!dateFields[i].pattern) {
                          dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                        }
                      }
                    }
                    
                    })();
                </script> 
                <script type='text/javascript' src='/js/fivestarcleaners/bunyad-theme.js?ver=2.6.2'></script>
                <!--[if lte IE 9]>
                <script type='text/javascript' src='https://myfivestarcleaners.com/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.2.3'></script>
                <![endif]-->
                <script type="text/javascript" id="slb_context">/* <![CDATA[ */if ( !!window.jQuery ) {(function($){$(document).ready(function(){if ( !!window.SLB ) { {$.extend(SLB, {"context":["public","user_guest"]});} }})})(jQuery);}/* ]]> */</script>
            </body>
        </html>