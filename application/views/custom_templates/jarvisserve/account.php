<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-CA" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-CA" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en-CA" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home - Jarvis 24/7 Dry-cleaning &amp; Laundry Service</title>



    
<!-- This site is optimized with the Yoast SEO plugin v3.1.2 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://jarvisserve.com/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Home - Jarvis 24/7 Dry-cleaning &amp; Laundry Service" />
<meta property="og:url" content="http://jarvisserve.com/" />
<meta property="og:site_name" content="Jarvis 24/7 Dry-cleaning &amp; Laundry Service" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Home - Jarvis 24/7 Dry-cleaning &amp; Laundry Service" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/jarvisserve.com\/","name":"Jarvis 24\/7 Dry-cleaning &amp; Laundry Service","potentialAction":{"@type":"SearchAction","target":"http:\/\/jarvisserve.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->


<link rel='stylesheet' id='dashicons-css'  href='/css/jarvisserve/dashicons.min.css' type='text/css' media='all' />


</script>

<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://jarvisserve.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://jarvisserve.com/wp-includes/wlwmanifest.xml" /> 

<link rel='shortlink' href='http://jarvisserve.com/' />

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
<link rel='stylesheet' id='main-styles-css'  href='/css/jarvisserve/main.min.css' type='text/css' media='all' />
    
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>


        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
</head>
<body class="home page page-id-4 page-template-default">
    
    <a id="top-catch" name="backtotop"></a>
    <header id="main-head">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <a href="http://jarvisserve.com">
                    <div class="logo"></div>
                    </a>
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#collapseme">
                            <svg id="svg-menu" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="24px" viewBox="0 0 25 24" style="enable-background:new 0 0 25 24;" xml:space="preserve">
                                <g>
                                    <rect style="fill:rgb(45, 179, 75);" width="25" height="6"></rect>
                                    <rect style="fill:rgb(45, 179, 75);" y="9" width="25" height="6"></rect>
                                    <rect style="fill:rgb(45, 179, 75);" y="18" width="25" height="6"></rect>
                                </g>
                            </svg>
                        </button>
                    </div>
                    <div id="collapseme" class="header-nav collapse navbar-collapse"><ul id="menu-mainnav" class="nav navbar-nav"><li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a href="http://jarvisserve.com/services/">Services</a></li>
<li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="http://jarvisserve.com/how-it-works/">How It Works</a></li>
<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="http://jarvisserve.com/pricing/">Pricing</a></li>
<li id="menu-item-16" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16"><a href="http://jarvisserve.com/locations/">Locations</a></li>
<li id="menu-item-15" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-15"><a href="http://jarvisserve.com/faq/">FAQ</a></li>
<li id="menu-item-14" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14"><a href="http://jarvisserve.com/blog/">Blog</a></li>
</ul></div>             </div>

            </div>
        </div>
    </header>


<section  class="home-section">
    <div class="container home">
       <?php echo $content; ?>
       <br/><br/>
    </div>
</section>


<section id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-lg-2">
                <picture class="footer-logo">
                    <source media="(min-width:980px)" srcset="/images/jarvisserve/jarvis-text-md-2.png">
                    <source media="(min-width:768px)" srcset="/images/jarvisserve/jarvis-text-md-2.png">
                    <source srcset="/images/jarvisserve/jarvis-text-md-2.png">
                    <img class="footer-logo" srcset="/images/jarvisserve/jarvis-text-md-2.png" alt="Jarvis at your service Logo">
                </picture>
            </div>
            <div class="col-sm-6 col-lg-8" id="footer-nav">
                <div class="middle">
                    <a class="mail" href="mailto:info@jarvisserve.com">info@jarvisserve.com</a>
                </div>
                <ul>
                    <li><a href="http://jarvisserve.com/our-story">Our Story <span class="nav-slash">&nbsp;&nbsp;/</span></a></li>
                    <li class="popmake-95"><a class="popmake-95">Get the App <span class="nav-slash">&nbsp;&nbsp;/</span></a></li>
                    <li><a class="popmake-74">Get Jarvis in Your Building <span class="nav-slash">&nbsp;&nbsp;/</span></a></li>
                    <li><a target="_blank" href="http://jarvisserve.com/wp-content/uploads/2016/02/Terms-and-Conditions.pdf">Terms and Conditions <span class="dashicons dashicons-media-document"></span><span class="nav-slash">&nbsp;&nbsp;/</span></a></li>
                    <li><a href="mailto:info@jarvisserve.com">Contact <span class="dashicons dashicons-email-alt"></span></a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-lg-2">
                <div id="sharing">
                    <span>Share on</span>
                    <ul class="social">
                        <li class="twitter"><a target="_blank" href="https://twitter.com/Jarvis_Services">Twitter</a></li>
                        <li class="facebook"><a target="_blank" href="https://www.facebook.com/jarvisserve/">Facebook</a></li>
                        <li class="instagram"><a target="_blank" href="https://www.instagram.com/jarvis_service/">Instagram</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" id="footend">
            <div class="col-sm-6">
                <div id="copyright">
                    Copyright 2016 Jarvis Services
                </div>
            </div>
            <div class="col-sm-6">
                <div id="ballistic">
                    Website design by <a target="_blank" href="http://ballisticarts.com">Ballistic Arts.</a>
                </div>
            </div>
        </div>
    </div>
</section>

    
    <div class="popmake-content"><div class="ta-center"><a href="https://itunes.apple.com/us/app/jarvis-services/id1088900278?mt=8" target="_blank" rel="attachment wp-att-97"><img class="alignnone size-full wp-image-97" src="/images/jarvisserve/app-store-badge.png" alt="app-store-badge" width="135" height="40" /></a> <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.jarvis&#038;hl=en" target="_blank" rel="attachment wp-att-98"><img class="alignnone size-full wp-image-98" src="/images/jarvisserve/play-store-badge.png" alt="play-store-badge" width="135" height="40" /></a></div>
</div>
    
</div>

    <script src="/bootstrap/js/bootstrap.min.js"></script>
<!--script type='text/javascript' src='/js/jarvisserve/script.min.js'></script-->


<div id="screen-sm-min"></div>
<div id="screen-md-min"></div>

</body>
</html>