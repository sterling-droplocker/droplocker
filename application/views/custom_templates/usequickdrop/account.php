<!DOCTYPE html>
<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="http://schema.org/Thing" lang="en-US">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>
<?= $title ?> - Quickdrop 24/7 Laundry and Dry Cleaning</title>
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
<link rel="shortcut icon" type="image/x-icon" href="/images/usequickdrop/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:100,400|Montserrat:700|Source+Sans+Pro:400"/>
<link rel="stylesheet" type="text/css" href="/css/usequickdrop/normalize.css"/>
<!--[if gte IE 9]>
<link rel="stylesheet" type="text/css" href="/css/usequickdrop/ie9.css?v=2"/>
<![endif]-->
<!--[if lt IE 9]>
    <script src="/js/usequickdrop/html5-shiv.js">
</script>
    <link rel="stylesheet" type="text/css" href="/css/usequickdrop/ie.css?v=2"/>
<![endif]-->
<!--[if !IE]> -->
    <link rel="stylesheet" type="text/css" href="/css/usequickdrop/site.css?v=2"/>
<!-- <![endif]-->
<!--
<script type="text/javascript" src="//use.typekit.net/ik/bNE_KwCqT8rNQMhzExxCgOjlKd_gX6OKBvndG_07QEwfe7XffFHN4UJLFRbh52jhWD9DZ2qhFRgcZQsKwe9uF2qXZem8FD9owUTyiaiaO1mDZWBu-AFnZY4zSfoRdhXCiWF8SWmTjhlqScN3OcFzdPUuShmUiA8R-hskdaM0SaBujW48Sagyjh90jhNlOfG0iWF8SWmTjhlqScN3O1FUiABkZWF3jAF8OcFzdPJwSY4zpe8ljPu0daZyJ6ZyZemCde97fbRKHyMMeMw6MKG4fHvgIMMjgfMfH6qJnbIbMg6eJMJ7fbKOMsMMeMS6MKG4f5w7IMMj2PMfH6qJn6IbMg6bJMJ7fbKwMsMMegI6MKG4fFJuIMIjMkMfH6qJDR9bMs6IJMJ7fbRs2UMgeMS6MKG4fFwuIMIj2PMfH6qJDD9bMs65JMJ7fbRP2UMgegI6M6.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
-->
<link rel="stylesheet" type="text/css" href="/css/usequickdrop/fonts/adelle.css"/>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<link rel="stylesheet" type="text/css" href="/css/usequickdrop/style.css?v=2"/>

    <?= $_scripts ?>
	<?= $javascript?>
	<?= $style?>
	<?= $_styles ?>
</head>
<body id="collection-540f5682e4b03c9d357c68e4" class="mobile-style-available info-page-layout-poster  info-page-content-overlay info-page-content-drop-shadow  site-drop-shadow mobile-background-image tagline-and-contact-info-hide-all site-border-none social-icon-style-round   show-category-navigation gallery-design-slideshow aspect-ratio-auto lightbox-style-dark gallery-navigation-bullets gallery-info-overlay-show-on-hover gallery-aspect-ratio-32-standard gallery-arrow-style-no-background gallery-transitions-fade gallery-show-arrows gallery-auto-crop   product-list-titles-under product-list-alignment-left product-item-size-11-square product-image-auto-crop product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing   event-thumbnails event-thumbnail-size-32-standard event-date-label event-date-label-time event-excerpts event-list-date event-list-time event-list-address event-icalgcal-links      opentable-style-light newsletter-style-dark small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-pill large-button-style-solid large-button-shape-square button-style-solid button-corner-style-pill native-currency-code-usd collection-type-page collection-layout-default collection-540f5682e4b03c9d357c68e4">
<div id="outerWrapper">
	<div id="bgOverlay"></div>
	<!--HEADER-->
	<header id="header">
		<!--MAIN NAVIGATION-->
		<!--MOBILE-->
		<nav id="mobile-navigation" data-content-field="navigation-mobileNav">
			<span id="mobile-navigation-label"></span>
			<ul>
				<li class=""><a href="http://www.usequickdrop.com/">Home</a></li>
				<li class="folder">
					<!--FOLDER-->
					<div class="folder-parent">
						<a>How It Works</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li class=""><a href="http://www.usequickdrop.com/how-it-works-1/">3 Step Process</a></li>
								<li class=""><a href="http://www.usequickdrop.com/faq/">FAQ</a></li>
								<li class=""><a href="http://www.usequickdrop.com/features/">Features</a></li>
							</ul>
						</div>
					</div>
				</li>
				<li class="folder">
					<!--FOLDER-->
					<div class="folder-parent">
						<a>Our Services</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li class="">
									<a href="http://www.usequickdrop.com/dry-cleaning/">
									Dry Cleaning
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/commercial-laundry/">
									Commercial Laundry
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/wash-and-fold/">
									Wash and Fold
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/shoe-services/">
									Shoe Service
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/package-delivery/">
									Package Delivery
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li class="folder active-folder">
					<!--FOLDER-->
					<div class="folder-parent">
						<a>About Us</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li class=" active-link">
									<a href="http://www.usequickdrop.com/mission/">
									Mission
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/vision/">
									Vision
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/locations/">
									Location
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
<? if (empty($this->customer)): ?>
				<li class="folder">
					<div class="folder-parent">
						<a>Register/Log In</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li>
									<a href="http://myaccount.usequickdrop.com/register">
									Register
									</a>
								</li>
								<li>
									<a href="http://myaccount.usequickdrop.com/login">
									Log In
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
<? endif; ?>
				<li class="">
					<a href="http://www.usequickdrop.com/contact/">Contact Us</a>
				</li>
			</ul>
		</nav>
		<nav id="main-navigation" data-content-field="navigation-mainNav">
			<ul class="cf">
				<li class="">
					<a href="http://www.usequickdrop.com/">Home</a>
				</li>
				<li class="folder">
					<!--FOLDER-->
					<div class="folder-parent">
						<a aria-haspopup="true">How It Works</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li class="">
									<a href="http://www.usequickdrop.com/how-it-works-1/">
									3 Step Process
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/faq/">
									FAQ
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/features/">
									Features
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li class="folder">
					<!--FOLDER-->
					<div class="folder-parent">
						<a aria-haspopup="true">Our Services</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li class="">
									<a href="http://www.usequickdrop.com/dry-cleaning/">
									Dry Cleaning
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/commercial-laundry/">
									Commercial Laundry
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/wash-and-fold/">
									Wash and Fold
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/shoe-services/">
									Shoe Service
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/package-delivery/">
									Package Delivery
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li class="folder active-folder">
					<!--FOLDER-->
					<div class="folder-parent">
						<a aria-haspopup="true">About Us</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li class=" active-link">
									<a href="http://www.usequickdrop.com/mission/">
									Mission
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/vision/">
									Vision
									</a>
								</li>
								<li class="">
									<a href="http://www.usequickdrop.com/locations/">
									Location
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
<? if (empty($this->customer)): ?>
				<li class="folder">
					<div class="folder-parent">
						<a aria-haspopup="true">Register/Log In</a>
						<div class="folder-child-wrapper">
							<ul class="folder-child">
								<li>
									<a href="http://myaccount.usequickdrop.com/register">
									Register
									</a>
								</li>
								<li>
									<a href="http://myaccount.usequickdrop.com/login">
									Log In
									</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
<? endif; ?>
				<li class="">
					<a href="http://www.usequickdrop.com/contact/">Contact Us</a>
				</li>
			</ul>
		</nav>
		<nav class="social" data-content-field="connected-accounts">
			<ul>
				<li>
					<a href="https://twitter.com/QuickdropATX" target="_blank" class="social-twitter" aria-label="twitter">
					</a>
				</li>
				<li>
					<a href="http://instagram.com/quickdropatx" target="_blank" class="social-instagram" aria-label="instagram">
					</a>
				</li>
				<li>
					<a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities&hl=en" target="_blank" class="social-google_play" aria-label="google_play">
					</a>
				</li>
				<li>
					<a href="https://itunes.apple.com/us/app/droplocker/id686269581?mt=8" target="_blank" class="social-itunes" aria-label="itunes">
					</a>
				</li>
			</ul>
		</nav>
	</header>
	<!--SITE TITLE OR LOGO-->
	<div id="innerWrapper">
		<div id="title-area">
			<h1 class="site-title" data-content-field="site-title">
				<a href="http://www.usequickdrop.com/">
				<img class="site-title-image" src="/images/usequickdrop/1500w.png" alt="Quickdrop 24/7 Laundry and Dry Cleaning" />
				</a>
			</h1>
			<!--SITE contact info-->
			<div class="contact-info">
				<p class="site-location">
					<a href="https://maps.google.com/maps?q=,">
					</a>
				</p>
			</div>
			<div class="site-desc">
				<p>Quickdrop is Austin's most advanced dry cleaner. We provide 24/7 access to our patented locker system conveniently located in your apartment common areas for all of your laundry and dry cleaning needs.</p>
			</div>
		</div>
		<!--CONTENT INJECTION POINT-->
		<section id="content">
			<div class="main-content-wrapper cf" data-content-field="main-content">
				<!-- CATEGORY NAV -->
				<div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1410657557979" id="page-540f5682e4b03c9d357c68e4">
					<div class="s-row sqs-row">
						<div class="col sqs-col-12">
							<div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-6925213062e262d99d16">
								<div id="main_content" class="sqs-block-content">
                                    <?= $content ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--FOOTER WITH OPEN BLOCK FIELD-->
		<footer id="footer">
			<div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Footer Content" data-type="block-field" data-updated-on="1409887287166" id="footerBlocks">
				<div class="s-row sqs-row">
					<div class="col sqs-col-12"></div>
				</div>
			</div>
		</footer>
	</div>
	<!-- end #innerWrapper -->
</div>
<!-- end #outerWrapper -->

<script>
$('#mobile-navigation-label').click(function(evt) {
    evt.preventDefault();
    $(this).next().toggle();
});

$('#main-navigation .folder-parent > a').click(function(evt) {
    evt.preventDefault();

    var li = $(this).parent().parent();
    li.toggleClass('dropdown-open');
    $('#main-navigation .dropdown-open').not(li).removeClass('dropdown-open');
});

$('#mobile-navigation .folder-parent > a').click(function(evt) {
    evt.preventDefault();

    var li = $(this).parent().parent();
    li.toggleClass('dropdown-open');
    $('#mobile-navigation .dropdown-open').not(li).removeClass('dropdown-open');
});


</script>

<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>
</body>
</html>
