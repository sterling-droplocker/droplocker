<?php
$system_lang = get_customer_business_language_key();

$lang_swap_redirect = '&r='.base64_encode($this->uri->uri_string);
$logged_in          = $this->customer_id;

$lang_switch['nl'] = $logged_in ? '/account/profile/change_language?ll_swap=nl'.$lang_swap_redirect : '?ll_lang=nl';
$lang_switch['en'] = $logged_in ? '/account/profile/change_language?ll_swap=en'.$lang_swap_redirect : '?ll_lang=en';
?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
    <link rel="shortcut icon" href="/images/bubble-stitch/favicon.ico">

    <!--[if lt IE 9]>
    <script src="/js/bubble-stitch/html5.js"></script>
    <link rel="stylesheet" href="/css/bubble-stitch/ie8.css">
    <![endif]-->
	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

    <title>Bubble&amp;Stitch - 24/7 Dry Cleaners | <?= $title ?></title>
    <meta name="description" content="We introduce a new and convenient way of cleaning your clothes. Through our proven and easy to use locker based system your able to drop off and collect your items 24/7. Register now and receive a 25% discount on your first order!"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
    <script src='/bootstrap/js/bootstrap.min.js'></script>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>

<link rel="stylesheet" href="/css/bubble-stitch/language-selector.css?v=3.3.6" type="text/css" media="all" />
<!--
<link rel='stylesheet' id='wpsl-styles-css'  href='http://www.bubble-stitch.com/wp-content/plugins/bs-store-locator/css/styles.min.css?ver=2.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-smoothness-css'  href='//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css?ver=1.11.4' type='text/css' media='all' />
<link rel='stylesheet' id='arconix-faq-css'  href='http://www.bubble-stitch.com/wp-content/plugins/arconix-faq/includes/css/arconix-faq.css?ver=1.6.1' type='text/css' media='all' />
-->
<link rel='stylesheet' id='parent-style-css'  href='/css/bubble-stitch/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='child-style-css'  href='/css/bubble-stitch/BubbleStitch/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='divi-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='et-gf-pt-sans-css'  href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&#038;subset=latin,latin-ext,cyrillic' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-css-css'  href='/css/bubble-stitch/shortcodes.css?ver=2.6.4.4' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-responsive-css-css'  href='/css/bubble-stitch/shortcodes_responsive.css?ver=2.6.4.4' type='text/css' media='all' />
<link rel='stylesheet' id='theme-customizer-css'  href='/css/bubble-stitch/theme-customizer-css.css?v=2' type='text/css' media='all' />
<!--
<script type='text/javascript' src='http://www.bubble-stitch.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
-->
<!--[if lt IE 9]><script>if(typeof String.prototype.trim !== 'function') { String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); } }</script><![endif]-->
<style type="text/css">
@media (min-width: 768px) {
#main-content > .container{
    padding: 80px 0 50px;
}

}

#default_account_nav {
    margin-bottom: 1em;
}

.navbar-fixed-top, .navbar-fixed-bottom {
    margin-right: 0;
    margin-left: 0;
}

</style>
</head>
<body class="home et_button_icon_visible et_pb_button_helper_class home-posts et_fixed_nav et_show_nav et_cover_background et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns3 et_header_style_left et_pb_pagebuilder_layout et_right_sidebar ie">
<div id="page-container">
	<header id="main-header" data-height-onload="66">
    	<div class="container clearfix et_menu_container">
    		<div class="logo_container">
    			<span class="logo_helper"></span>
    			<a href="http://www.bubble-stitch.com/en/">
    				<img src="/images/bubble-stitch/logo-bs.png" alt="Bubble &amp; Stitch" id="logo" data-height-percentage="54" data-actual-width="168" data-actual-height="72">
    			</a>
        	</div>
            <div id="et-top-navigation" data-height="66" data-fixed-height="40" style="padding-left: 198px;">
    			<nav id="top-menu-nav">
    				<ul id="top-menu" class="nav">
<?php if ($system_lang == 'en'): ?>
                        <li id="menu-item-1474" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1474"><a href="http://www.bubble-stitch.com/en/how-it-works/">How it works</a></li>
                        <li id="menu-item-1467" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1467"><a href="#">For Organizations</a>
                            <ul class="sub-menu">
                            	<li id="menu-item-1473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1473"><a href="http://www.bubble-stitch.com/en/become-partner/">Become a partner</a></li>
                            	<li id="menu-item-1463" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1463"><a href="http://www.bubble-stitch.com/en/dry-cleaning-service-at-the-office/">Service at the office</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-1465" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1465"><a href="http://www.bubble-stitch.com/en/prices/">Prices</a></li>
                        <li id="menu-item-1464" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1464"><a href="http://www.bubble-stitch.com/en/faq-2/">FAQ</a></li>
                        <li id="menu-item-1468" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1468">
                            <a href="#">About us</a>
                            <ul class="sub-menu">
                            	<li id="menu-item-1461" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1461"><a href="http://www.bubble-stitch.com/en/about-us/">About us</a></li>
                            	<li id="menu-item-1466" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1466"><a href="http://www.bubble-stitch.com/en/vacancies/">Vacancies</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-1462" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1462"><a href="http://www.bubble-stitch.com/en/contact-2/">Contact</a></li>
                        <li class="menu-item menu-item-language menu-item-language-current menu-item-has-children">
                            <a href="<?= $lang_switch['en'] ?>" ><img class="iclflag" src="/images/bubble-stitch/en.png" width="18" height="12" alt="en" title="English">English</a>
                            <ul class="sub-menu submenu-languages">
                                <li class="menu-item menu-item-language"><a href="<?= $lang_switch['nl'] ?>"><img class="iclflag" src="/images/bubble-stitch/nl.png" width="18" height="12" alt="nl" title="Nederlands <span class=&quot;icl_lang_sel_bracket&quot;>(</span>Dutch<span class=&quot;icl_lang_sel_bracket&quot;>)</span>">Nederlands <span class="icl_lang_sel_bracket">(</span>Dutch<span class="icl_lang_sel_bracket">)</span></a></li>
                            </ul>
                        </li>
<? else: ?>
                        <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a href="http://www.bubble-stitch.com/zo-werkt-het/">Zo werkt het</a></li>
                        <li id="menu-item-1208" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1208">
                            <a href="#">Voor Bedrijven</a>
                            <ul class="sub-menu">
                            	<li id="menu-item-1198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1198"><a href="http://www.bubble-stitch.com/wordt-partner-2/">Wordt partner</a></li>
                            	<li id="menu-item-1207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1207"><a href="http://www.bubble-stitch.com/stomerij-service-op-kantoor/">Service op kantoor</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a href="http://www.bubble-stitch.com/prijzen/">Prijzen</a></li>
                        <li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="http://www.bubble-stitch.com/faq/">FAQ</a></li>
                        <li id="menu-item-1209" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1209">
                            <a href="#">Over ons</a>
                            <ul class="sub-menu">
                            	<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a href="http://www.bubble-stitch.com/over-ons/">Over ons</a></li>
                            	<li id="menu-item-1206" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1206"><a href="http://www.bubble-stitch.com/vacatures/">Vacatures</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="http://www.bubble-stitch.com/contact/">Contact</a></li>
                        <li class="menu-item menu-item-language menu-item-language-current menu-item-has-children">
                            <a href="<?= $lang_switch['nl'] ?>"><img class="iclflag" src="http://www.bubble-stitch.com/wp-content/plugins/sitepress-multilingual-cms/res/flags/nl.png" width="18" height="12" alt="nl" title="Nederlands">Nederlands</a>
                            <ul class="sub-menu submenu-languages">
                                <li class="menu-item menu-item-language"><a href="<?= $lang_switch['en'] ?>"><img class="iclflag" src="http://www.bubble-stitch.com/wp-content/plugins/sitepress-multilingual-cms/res/flags/en.png" width="18" height="12" alt="en" title="English <span class=&quot;icl_lang_sel_bracket&quot;>(</span>Engels<span class=&quot;icl_lang_sel_bracket&quot;>)</span>">English <span class="icl_lang_sel_bracket">(</span>Engels<span class="icl_lang_sel_bracket">)</span></a></li>
                            </ul>
                        </li>
<? endif; ?>
                    </ul>
                </nav>

    			<div id="et_mobile_nav_menu">
                    <div class="mobile_nav closed">
                        <span class="select_page">Select Page</span>
                        <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                        <ul id="mobile_menu" class="et_mobile_menu">
                            <li id="menu-item-1474" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1474 et_first_mobile_item"><a href="http://www.bubble-stitch.com/en/how-it-works/">How it works</a></li>
                            <li id="menu-item-1467" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1467">
                                <a href="#">For Organizations</a>
                                <ul class="sub-menu">
                                	<li id="menu-item-1473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1473"><a href="http://www.bubble-stitch.com/en/become-partner/">Become a partner</a></li>
                                	<li id="menu-item-1463" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1463"><a href="http://www.bubble-stitch.com/en/dry-cleaning-service-at-the-office/">Service at the office</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-1465" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1465"><a href="http://www.bubble-stitch.com/en/prices/">Prices</a></li>
                            <li id="menu-item-1464" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1464"><a href="http://www.bubble-stitch.com/en/faq-2/">FAQ</a></li>
                            <li id="menu-item-1468" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1468">
                                <a href="#">About us</a>
                                <ul class="sub-menu">
                                	<li id="menu-item-1461" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1461"><a href="http://www.bubble-stitch.com/en/about-us/">About us</a></li>
                                	<li id="menu-item-1466" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1466"><a href="http://www.bubble-stitch.com/en/vacancies/">Vacancies</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-1462" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1462"><a href="http://www.bubble-stitch.com/en/contact-2/">Contact</a></li>
                            <li class="menu-item menu-item-language menu-item-language-current menu-item-has-children"><a href="<?= $lang_switch['en'] ?>"><img class="iclflag" src="/images/bubble-stitch/en.png" width="18" height="12" alt="en" title="English">English</a>
                                <ul class="sub-menu submenu-languages">
                                    <li class="menu-item menu-item-language"><a href="<?= $lang_switch['nl'] ?>"><img class="iclflag" src="/images/bubble-stitch/nl.png" width="18" height="12" alt="nl" title="Nederlands <span class=&quot;icl_lang_sel_bracket&quot;>(</span>Dutch<span class=&quot;icl_lang_sel_bracket&quot;>)</span>">Nederlands <span class="icl_lang_sel_bracket">(</span>Dutch<span class="icl_lang_sel_bracket">)</span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
    		</div> <!-- #et-top-navigation -->
    	</div> <!-- .container -->
    	<div class="et_search_outer">
    		<div class="container et_search_form_container">
    			<form role="search" method="get" class="et-search-form" action="http://www.bubble-stitch.com/en/">
                    <input type="search" class="et-search-field" placeholder="Search …" value="" name="s" title="Search for:">
    			</form>
    			<span class="et_close_search_field"></span>
    		</div>
    	</div>
    </header>

    <div id="et-main-area">
        <div id="main-content">
            <?= $content ?>
        </div>

<?php if ($system_lang == 'en'): ?>
<footer id="main-footer">
	<div class="container">
		<div id="footer-widgets" class="clearfix">
			<div class="footer-widget">
				<h4 class="title">Menu</h4>
                <ul id="menu-main-nav-en" class="footer-nav clearfix">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1474"><a href="http://www.bubble-stitch.com/en/how-it-works/">How it works</a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1467"><a href="#">For Organizations</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1465"><a href="http://www.bubble-stitch.com/en/prices/">Prices</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1464"><a href="http://www.bubble-stitch.com/en/faq-2/">FAQ</a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1468"><a href="#">About us</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1462"><a href="http://www.bubble-stitch.com/en/contact-2/">Contact</a></li>
                    <li class="menu-item menu-item-language menu-item-language-current menu-item-has-children">
                        <a href="<?= $lang_switch['en'] ?>"><img class="iclflag" src="/images/bubble-stitch/en.png" width="18" height="12" alt="en" title="English">English</a>
                        <ul class="sub-menu submenu-languages">
                            <li class="menu-item menu-item-language"><a href="<?= $lang_switch['nl'] ?>"><img class="iclflag" src="/images/bubble-stitch/nl.png" width="18" height="12" alt="nl" title="Nederlands <span class=&quot;icl_lang_sel_bracket&quot;>(</span>Dutch<span class=&quot;icl_lang_sel_bracket&quot;>)</span>">Nederlands <span class="icl_lang_sel_bracket">(</span>Dutch<span class="icl_lang_sel_bracket">)</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        
            <div class="footer-widget">
                <div id="text-12" class="fwidget et_pb_widget widget_text">
                    <div class="textwidget">
                        <h5>Download our app</h5>
                        <div class="clearfix">
                            <a style="float:left; padding-right:7px;" href="https://itunes.apple.com/us/app/droplocker/id686269581?mt=8" title="Download in app store">
                                <img style="height:47px; width:auto;" src="/images/bubble-stitch/get-appstore-nl.png" alt="get app in appstore">
                            </a>
                            <a style="float:left;" href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities" title="Download in app store">
                                <img style="height:47px; width:auto;" src="/images/bubble-stitch/get-google-market-nl.png" alt="get app on Google Market">
                            </a>
                        </div>
                        <br>
                        <h5>We accept the following payment options</h5>
                        <div class="clearfix">
                            <img style="float:left;height:35px; width:auto;padding:0 5px 5px 0;" src="/images/bubble-stitch/mastercard.png" alt="mastercard">  
                            <img style="float:left;height:35px; width:auto;padding:0 5px 5px 0;" src="/images/bubble-stitch/visa.png" alt="visa">  
                            <img style="float:left; height:35px; width:auto;padding:0 5px 5px 0;" src="/images/bubble-stitch/paypal.png" alt="paypal">
                        </div>
                        <br>
                        <h5>Our certificates</h5>
                        <div class="clearfix">
                            <a href="http://www.netex.nl/?P=203"><img style=" float: left; height: 34px; width: auto; margin: 0 5px 5px 0; display: block;" src="/images/bubble-stitch/netex-kwaliteitscertificaat.png" alt="Nettex Kwaliteitscertificaat"></a>
                            <a href="http://www.netex.nl/?P=203"><img style=" float: left; height: 34px; width: auto; margin: 0 5px 5px 0; display: block;" src="/images/bubble-stitch/netex-milieucertificaat.png" alt="Nettex Milieucertificaat"></a>
                            <a href="http://www.droplocker.com"><img style=" float: left; height: 34px; width: auto; margin: 0 5px 5px 0; display: block;" src="/images/bubble-stitch/droplocker.png" alt="Droplocker"></a>  
                        </div>
                    </div>
	            </div> <!-- end .fwidget -->
            </div> <!-- end .footer-widget -->
        
            <div class="footer-widget">
                <div id="text-10" class="fwidget et_pb_widget widget_text">
                    <h4 class="title">Bubble&amp;Stitch</h4>
                    <div class="textwidget">
                        Dobbeweg 18<br>
                        2254 AG Voorschoten<br>
                        <br>
                        KvK nummer: 61342939<br>
                        BTW nummer: NL 854304393B01
                        <br>
                        071-5616241<br>
                        <a href="mailto:info@bubble-stitch.com"><span class="et-pb-icon" style="opacity:1;color: #66bba7;font-size: 16px;padding-right: 5px;"></span>info@bubble-stitch.com</a><br>
                        <a href="https://www.facebook.com/pages/BubbleStitch/260227327449153?fref=ts" class="icon" title="Bubble &amp; Stitch on Facebook"><span class="et-pb-icon" style="opacity:1;color: #66bba7;font-size: 16px;padding-right: 5px;"></span>Vind ons op Facebook</a><br>
                        <a href="https://twitter.com/BubbleStitch247" title="Bubble &amp; Stitch op Twitter"><span class="et-pb-icon" style="opacity:1;color: #66bba7;font-size: 16px;padding-right: 5px;"></span>Volg ons op Twitter</a>
                    </div>
	            </div> <!-- end .fwidget -->
            </div> <!-- end .footer-widget -->
		</div>
	</div>

	<div class="footer-bottom">
		<div class="container clearfix">
			<div class="et_pb_row">
				<div class="footer-widget">
				</div>
				<div class="footer-widget">
				</div>
				<div class="footer-widget">
				</div>
			</div>
			<div class="et_pb_row"> 
			</div>
		</div>	<!-- .container -->
	</div>
</footer>
<? else: ?>
<footer id="main-footer">
    <div class="container">
	    <div id="footer-widgets" class="clearfix">
		    <div class="footer-widget">
			    <h4 class="title">Menu</h4>
				<ul id="menu-main-nav" class="footer-nav clearfix">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a href="http://www.bubble-stitch.com/zo-werkt-het/">Zo werkt het</a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1208"><a href="#">Voor Bedrijven</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a href="http://www.bubble-stitch.com/prijzen/">Prijzen</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="http://www.bubble-stitch.com/faq/">FAQ</a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1209"><a href="#">Over ons</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="http://www.bubble-stitch.com/contact/">Contact</a></li>
                    <li class="menu-item menu-item-language menu-item-language-current menu-item-has-children">
                        <a href="<?= $lang_switch['nl'] ?>"><img class="iclflag" src="/images/bubble-stitch/nl.png" width="18" height="12" alt="nl" title="Nederlands">Nederlands</a>
                         <ul class="sub-menu submenu-languages"><li class="menu-item menu-item-language"><a href="<?= $lang_switch['en'] ?>"><img class="iclflag" src="/images/bubble-stitch/en.png" width="18" height="12" alt="en" title="English <span class=&quot;icl_lang_sel_bracket&quot;>(</span>Engels<span class=&quot;icl_lang_sel_bracket&quot;>)</span>">English <span class="icl_lang_sel_bracket">(</span>Engels<span class="icl_lang_sel_bracket">)</span></a></li>
                         </ul>
                    </li>
                </ul>
            </div>
	        <div class="footer-widget"><div id="text-9" class="fwidget et_pb_widget widget_text">
                <div class="textwidget">
                    <h5>Download onze app</h5>
                    <div class="clearfix">
                        <a style="float:left; padding-right:7px;" href="https://itunes.apple.com/us/app/droplocker/id686269581?mt=8" title="Download in app store"><img style="height:47px; width:auto;" src="/images/bubble-stitch/get-appstore-nl.png" alt="get app in appstore"></a>
                        <a style="float:left;" href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities" title="Download in app store"><img style="height:47px; width:auto;" src="/images/bubble-stitch/get-google-market-nl.png" alt="get app on Google Market"></a>
                    </div>
                    <br>
                    <h5>Wij accepteren de volgende betaalmogelijkheden</h5>
                    <div class="clearfix">
                        <img style="float:left;height:35px; width:auto;padding:0 5px 5px 0;" src="/images/bubble-stitch/mastercard.png" alt="mastercard">  
                        <img style="float:left;height:35px; width:auto;padding:0 5px 5px 0;" src="/images/bubble-stitch/visa.png" alt="visa">  
                        <img style="float:left; height:35px; width:auto;padding:0 5px 5px 0;" src="/images/bubble-stitch/paypal.png" alt="paypal">
                    </div>
                    <br>
                    <h5>Onze certificaten</h5>
                    <div class="clearfix">
                        <a href="http://www.netex.nl/?P=203"><img style=" float: left; height: 34px; width: auto; margin: 0 5px 5px 0; display: block;" src="/images/bubble-stitch/netex-kwaliteitscertificaat.png" alt="Nettex Kwaliteitscertificaat"></a>
                        <a href="http://www.netex.nl/?P=203"><img style=" float: left; height: 34px; width: auto; margin: 0 5px 5px 0; display: block;" src="/images/bubble-stitch/netex-milieucertificaat.png" alt="Nettex Milieucertificaat"></a>
                        <a href="http://www.droplocker.com"><img style=" float: left; height: 34px; width: auto; margin: 0 5px 5px 0; display: block;" src="/images/bubble-stitch/droplocker.png" alt="Droplocker"></a>  
                    </div>
                </div>
            </div><!-- end .fwidget -->
        </div> <!-- end .footer-widget -->
        <div class="footer-widget">
            <div id="text-11" class="fwidget et_pb_widget widget_text">
                <h4 class="title">Bubble&amp;Stitch</h4>
                <div class="textwidget">
                    Dobbeweg 18<br>
                    2254 AG Voorschoten<br>
                    <br>
                    KvK nummer: 61342939<br>
                    BTW nummer: NL 854304393B01
                    <br>
                    071-5616241<br>
                    <a href="mailto:info@bubble-stitch.com"><span class="et-pb-icon" style="opacity:1;color: #66bba7;font-size: 16px;padding-right: 5px;"></span>info@bubble-stitch.com</a><br>
                    <a href="https://www.facebook.com/pages/BubbleStitch/260227327449153?fref=ts" class="icon" title="Bubble &amp; Stitch on Facebook"><span class="et-pb-icon" style="opacity:1;color: #66bba7;font-size: 16px;padding-right: 5px;"></span>Vind ons op Facebook</a><br>
                    <a href="https://twitter.com/BubbleStitch247" title="Bubble &amp; Stitch op Twitter"><span class="et-pb-icon" style="opacity:1;color: #66bba7;font-size: 16px;padding-right: 5px;"></span>Volg ons op Twitter</a>
                </div>
            </div> <!-- end .fwidget -->
        </div> <!-- end .footer-widget -->
        </div>
    </div>
    <div class="footer-bottom">
    	<div class="container clearfix">
    		<div class="et_pb_row">
    			<div class="footer-widget">
    			</div>
    			<div class="footer-widget">
    			</div>
    			<div class="footer-widget">
    			</div>
    		</div>
    		<div class="et_pb_row"> 
    		</div>
    	</div>	<!-- .container -->
    </div>
</footer>
<? endif; ?>


    </div>
</div>

<script type="text/javascript">
$('.mobile_menu_bar_toggle').click(function(evt) {
    $('#mobile_menu').slideToggle();
});
</script>

<? if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', '<?= $_SERVER["HTTP_HOST"] ?>']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<? endif; ?>

</body>
</html>
