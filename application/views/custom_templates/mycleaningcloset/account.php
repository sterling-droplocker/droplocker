<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 ie" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 7]>	<html class="ie7 ie" lang="en" dir="ltr"> <![endif]-->
<!--[if IE 8]>	<html class="ie8 ie" lang="en" dir="ltr"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="" lang="en" dir="ltr"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cleaning Closet | <?= $title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
	<link href="/css/mycleaningcloset/style.css" rel="stylesheet" media="screen" />

    <!--[if lt IE 9]>
        <script src="/js/soapbox/html5.js" type="text/javascript"></script>
	<![endif]-->

	<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
	<script src='/bootstrap/js/bootstrap.min.js'></script>


	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?= $_scripts ?>
	<?= $javascript?>
	<?= $style?>
	<?= $_styles ?>

<script type="text/javascript">
<!--
	function setIPhoneMenu(){
		
		jQuery('.subItem').remove();
		jQuery(".iphonNav").css("display", "none");
		jQuery(".iphonNav").addClass("iphon_navigation");
				jQuery(".iphonNav").removeClass("navigation");
				jQuery(".iphonNav > ul").attr("id","");
				jQuery(".menuImage").css("display","block");
				jQuery(".iphonNav ul li").each(function(){
					if(jQuery(this).children("ul").length >0)
					{
						jQuery(this).addClass("parentMenu");
						jQuery(this).prepend("<div class='subItem'></div>");
						jQuery(this).children("ul").addClass("subMenu");
					}
				});
				jQuery(".subMenu").css("display","none");
				jQuery(".menuImage img").unbind("click");
				jQuery(".menuImage img").click(function(){
					jQuery(".iphonNav").slideToggle(300, function(){
						if(!jQuery(this).is(":visible")){
							jQuery(this).find(".subMenu").each(function(){
								jQuery(this).css("display","none");
								jQuery(this).parent().removeClass("parentMenuActive");
							});
						}
					});
				});
				jQuery(".subItem").unbind("click");
				jQuery(".subItem").click(function(e){
					jQuery(this).parent().children(".subMenu").slideToggle(300, function(){
						if(jQuery(this).is(":visible")){
								jQuery(this).parent().parent().children(".parentMenuActive").find(".subMenu").each(function(){
										jQuery(this).slideUp(300);
										jQuery(this).css("display","none");
										jQuery(this).parent().removeClass("parentMenuActive");
									});
								jQuery(this).parent().parent().children(".parentMenuActive").removeClass("parentMenuActive");
							jQuery(this).parent().addClass("parentMenuActive");
							
						}else{
							jQuery(this).parent().removeClass("parentMenuActive");
							jQuery(this).find(".subMenu").each(function(){
								jQuery(this).css("display","none");
								jQuery(this).parent().removeClass("parentMenuActive");
								
							});
						}
					});
					
					e.stopPropagation();
				});
	}
	function setDesktopMenu(){
		jQuery(".iphonNav ul li").find(".subMenu").each(function(){
			jQuery(this).css("display","none");
			jQuery(this).parent().removeClass("parentMenuActive");
		});
		jQuery(".iphonNav").addClass("navigation");
		jQuery(".iphonNav").css("display", "block");
		jQuery(".iphonNav").removeClass("iphon_navigation");
		jQuery(".iphonNav>ul").attr("id","nav");
		jQuery(".menuImage").css("display","none");
	}
	jQuery(document).ready(function(){
		if(jQuery(window).width()<650)
		{
			setIPhoneMenu();
		}else{
			setDesktopMenu();
		}

	});
	jQuery(window).resize(function(){
		if(jQuery(window).width()<650)
		{
			setIPhoneMenu();
		}else{
			setDesktopMenu();
		}
	});
-->
</script>

</head>

<body>
<div class="header">
  <div class="head_d">
    <div class="header_outer">
      <div class="logo"><a href="/">
      <img src="/images/mycleaningcloset/logo.png" width="143" height="79" alt="Log" /></a>
      </div>
      <a href="javascript:void(0)" class="menuImage"><img src="/images/mycleaningcloset/menuIcon.png" alt="Top Menus" title="Top Menus" /></a>
      <div class="iphonNav navigation">
        <ul>
          <li class="active"><a href="javascript:void(0)" onClick="$('#home-section').animatescroll();">
          
          <img src="/images/mycleaningcloset/home-icon.png" alt="Home" class="nav_icon" />          <img src="/images/mycleaningcloset/home-icon-hover.png" alt="Home" class="nav_icon_hover" />          
          <span>Home</span></a></li>
          
                    
          <li>
          <a href="javascript:void(0)" onClick="$('#work-section').animatescroll();">
          <img src="/images/mycleaningcloset/how-work-icon.png" alt="how it works" class="nav_icon" />          <img src="/images/mycleaningcloset/how-work-icon-hover.png" alt="how it works" class="nav_icon_hover" />          <span>HOW IT WORKS</span>
          </a>
          </li>
          
          
          <li>
          <a href="javascript:void(0)" onClick="$('#service-section').animatescroll();">
          <img src="/images/mycleaningcloset/service-icon.png" alt="SERVICES" class="nav_icon" />          <img src="/images/mycleaningcloset/service-icon-hover.png" alt="SERVICES" class="nav_icon_hover" />          <span>SERVICES</span>
          </a>
          </li>
          
          
          <li>
          <a href="javascript:void(0)" onClick="$('#faq-section').animatescroll();">
       
          <img src="/images/mycleaningcloset/faq-icon.png" alt="FAQ" class="nav_icon" />          <img src="/images/mycleaningcloset/faq-icon-hover.png" alt="FAQ" class="nav_icon_hover" />          <span>FAQ</span>
          </a>
          </li>
          
          <li>
          <a href="javascript:void(0)" onClick="$('#about-section').animatescroll();">

           <img src="/images/mycleaningcloset/about-icon.png" alt="ABOUT" class="nav_icon" />          <img src="/images/mycleaningcloset/about-icon-hover.png" alt="ABOUT" class="nav_icon_hover" />          <span>ABOUT</span>
          </a>
          </li>
          
          <li>
          <a href="javascript:void(0)" onClick="$('#property-section').animatescroll();">

          <img src="/images/mycleaningcloset/property-icon.png" alt="PROPERTY" class="nav_icon" />          <img src="/images/mycleaningcloset/property-icon-hover.png" alt="PROPERTY" class="nav_icon_hover" />          <span>PROPERTY MANAGERS</span>
          </a>
          </li>
          
          <li>
          <a href="javascript:void(0)" onClick="$('#contact-section').animatescroll();">
         
          <img src="/images/mycleaningcloset/contact-icon.png" alt="CONTACT" class="nav_icon" />          <img src="/images/mycleaningcloset/contact-icon-hover.png" alt="CONTACT" class="nav_icon_hover" />          <span>CONTACT</span>
          </a>
          </li>
          
        </ul>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>


    <div role="main" id="main-content" class="main" style="overflow-x: hidden; background:#fff;padding:50px 0;">
        <?= $content; ?>
    </div>

<div class="footer">
  <div class="foo_get_touch" id="contact-section">
    <div class="footer_outer">
      <div class="get_touch_title">
      <img src="/images/mycleaningcloset/get-touch-title.png" width="366" height="38" alt="Log"></div>
      <div class="get_touch_con">
        <div class="get_touch_lt">
		<a href="/pages/popup" class="get_started cboxElement"><img src="/images/mycleaningcloset/sign-newsletter-btn.jpg" width="437" height="79" class="get_touch_btn" alt=""></a>
          <div class="foo_social">
            <ul>
              <li><a href="https://www.facebook.com/mycleaningcloset"><img src="/images/mycleaningcloset/fb-icon.png" alt=""></a></li>
              <li><a href="http://www.linkedin.com/pub/rick-hallor-jr/47/733/280" target="_blank"><img src="/images/mycleaningcloset/in-icon" alt=""></a></li>
              <li><a href="http://www.yelp.com/biz/my-cleaning-closet-santee-2"><img src="/images/mycleaningcloset/yelp-icon.png" alt=""></a></li>
              <li><a href="https://www.youtube.com/watch?v=h70GHrTt_5s"><img src="/images/mycleaningcloset/y-t-icon.png" alt=""></a></li>
            </ul>
          </div>
        </div>
        <div class="get_touch_rt">
          <ul>
            <li><img src="/images/mycleaningcloset/email-icon.png" alt=""><a href="mailto:Info@mycleaningcloset.com">Info@mycleaningcloset.com</a></li>
            <li><img src="/images/mycleaningcloset/phn-icon.png" alt="">&nbsp;
              <a href="callto:6196540774">(619)
              654-0774</a></li> 
          </ul>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="footer_d">
    <div class="footer_outer">
      <div class="footer_policy"> <a href="#">Privacy Policy</a> / <a href="#">Terms Of Use</a></div>
      <div class="footer_copyright">© 2014, <strong>My Cleaning Closet</strong> |  All  Rights Reserved. <a href="http://2pointinteractive.com" target="_blank">2Point Interactive</a>- Online marketing Agency</div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>


<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
