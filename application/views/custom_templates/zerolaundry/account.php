<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
    <head profile="http://gmpg.org/xfn/11">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
        <meta name ="format-detection" content = "telephone=no">
            <link rel="apple-touch-startup-image" href="splash-screen-320x460.png" media="screen and (max-device-width: 320px)" />
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
                <link href='https://fonts.googleapis.com/css?family=Droid+Serif:700' rel='stylesheet' type='text/css'>
                <!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]-->

                   
                    <!-- Custom styles for the demo page -->
                    <title>Zerolaundry  </title>
                    <link rel="stylesheet" href="/css/zerolaundry/style.css" type="text/css" media="screen" />
                    <link rel="icon" type="image/x-icon" href="/images/zerolaundry/favicon.ico" />
                    <link rel="stylesheet" href="/css/zerolaundry/style_main.css" type="text/css" media="screen" />
                    <link rel="stylesheet" href="/css/zerolaundry/mediaquery.css" type="text/css" media="screen" />
                   
                    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
                    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css" />
                    <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
                    <?= $_scripts; ?>
                    <?= $javascript; ?>
                    <script src="/bootstrap/js/bootstrap-collapse.js"></script>
                    <script src="/js/zerolaundry/responsive-nav.js" type="text/javascript"></script>

                    <?= $style ?>
                    <?= $_styles ?>
                    <style>
                        input, textarea, select, .uneditable-input  {
                            height: auto;
                        }

                        #main_content {
                            margin-top: 50px;
                        }

                        .btn.btn-primary, .btn.btn-success {
                            display: inline-block;
                            background: none repeat scroll 0 0 #ff7300;
                            color: #fff;
                            display: inline-block;

                            font-size: 14px;
                            font-weight: 700;
                            margin: 0 !important;
                            
                            text-shadow: none;
                            box-shadow: none;
                            border: none;
                        }

                        .btn.btn-danger {

                            display: inline-block;
                            background: none repeat scroll 0 0 #da4f49;
                            color: #fff;
                            display: inline-block;

                            font-size: 14px;
                            font-weight: 700;
                            margin: 0 !important;

                            text-shadow: none;
                            box-shadow: none;
                            border: none;
                            border-radius: 4px;

                        }

                        .btn.btn-danger:hover {
                            background-color: #bd362f;
                        }

                        .btn.btn-primary:hover, .btn.btn-success:hover {
                            background-color: #0199FE;
                        }


                        .btn.btn-primary i {
                            vertical-align: middle;
                        }

                        #account_settings select[name=default_business_language_id] {
                            margin-bottom: 40px;
                        }

                        ul.breadcrumb li a, #default_account_nav ul li a {
                            font-size: 15px;
                        }

                        .user-info-box p {
                            font-size: 15px;
                        }


                        @media (max-width: 1200px) {
                            nav.nav-collapse-0 {
                                height: 100px;
                            }
                        }

                       

                        @media (max-width: 767px) {
                            body {
                               padding-right: 0px;
                               padding-left: 0px;
                            }

                            nav.nav-collapse-0 {
                                height: 200px;
                            }

                            #main_content {
                                margin-top: 50px;
                                padding-left: 20px;
                                padding-right: 20px;
                            }
                        }



                    </style>
                    </head>
                    <body class="home page page-id-134 page-template page-template-page-templateshomepage-php">

                        <div id="page">
                            <header>
                                <div class="container">
                                    <!--<div class="nav-collapse_new signupbtn">
                                        <div class="menu-item login-btn">

                                            <a href="https://myaccount.zerolaundry.com/register" target="_blank">SIGN UP</a><span> / </span>
                                            <a href="https://myaccount.zerolaundry.com/login" target="_blank">LOG IN</a>
                                        </div>
                                    </div>-->
                                    <nav class="nav-collapse">
                                        <ul>
                                            <li id="menu-item-130" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130"><a href="http://www.zerolaundry.com/#home" data-scroll="true">Home</a></li>
                                            <li id="menu-item-137" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137"><a href="http://www.zerolaundry.com/#works" data-scroll="true">How It Works</a></li>
                                            <li id="menu-item-138" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-138"><a href="http://www.zerolaundry.com/#conciergeservice" data-scroll="true">Concierge Service</a></li>
                                            <li id="menu-item-139" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-139"><a href="http://www.zerolaundry.com/#servicespricing" data-scroll="true">Services &#038; Pricing</a></li>
                                            <li id="menu-item-136" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-136"><a href="http://www.zerolaundry.com/#laundryplans" data-scroll="true">Laundry Plans</a></li>
                                            <li id="menu-item-140" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-140"><a href="http://www.zerolaundry.com/#contact" data-scroll="true">Contact</a></li>
                                            <li id="menu-item-141" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-141"><a href="http://www.zerolaundry.com/#app" data-scroll="true">App</a></li>
                                        </ul>
                                    </nav>

                                </div><!--End header_right -->
                            </header>


                            <div id="home" class="Home-section">
                                <div id="main_content">
                                    <?= $content; ?>
                                </div>
                            </div>

                        </div><!--End of the entry -->

                        <footer>
                            <div id="app" class="app-section section">
                                <div class="container center">
                                    <nav id="navigation-menu" class="nav-collapse"><ul><li id="menu-item-198" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-198"><a href="#home" data-scroll="true">Home</a></li>
                                            <li id="menu-item-199" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-199"><a href="http://www.zerolaundry.com/#works" data-scroll="true">How It Works</a></li>
                                            <li id="menu-item-200" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-200"><a href="http://www.zerolaundry.com/#conciergeservice" data-scroll="true">Concierge Service</a></li>
                                            <li id="menu-item-201" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-201"><a href="http://www.zerolaundry.com/#servicespricing" data-scroll="true">Services &#038; Pricing</a></li>
                                            <li id="menu-item-202" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-202"><a href="http://www.zerolaundry.com/#laundryplans" data-scroll="true">Laundry Plans</a></li>
                                            <li id="menu-item-203" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-203"><a href="http://www.zerolaundry.com/#contact" data-scroll="true">Contact</a></li>
                                            <li id="menu-item-204" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-204"><a href="http://www.zerolaundry.com/#app" data-scroll="true">App</a></li>
                                        </ul></nav>
                                    <div class="bottom-icons-box">
                                        <span>
                                            <a href="https://itunes.apple.com/ca/app/zero-laundry/id963835033?mt=8" target="_blank">
                                                <img src="/images/zerolaundry/apple-icon.jpg" alt="" />
                                            </a>
                                        </span>
                                        <span>
                                            <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.zeroLaundry" target="_blank">
                                                <img src="/images/zerolaundry/android-icon.jpg" alt="" />
                                            </a>
                                        </span>
                                    </div>
                                    <div class="copyright-text">
                                        &copy; 2015 Zero Laundry Inc. All Rights Reserved.  |
                                        <a class="initialism policybtm_open" href="#policybtm"> Privacy Policy</a>  |
                                        <a class="initialism terms_content_open" href="#terms_content">Terms & Conditions</a>
                                    </div>
                                </div>
                            </div>

                            <div id="terms_content" class="well" style="max-width:44em;">
                                <button class="terms_content_close btn btn-default">X</button>
                                <h2>Terms &amp; Conditions</h2>
                                <p><strong>By using Zero Laundry\'s service, you agree to the following terms and conditions:</strong></p>

                                <p><strong>Minimum Order Size</strong></p>
                                <p>Wash &amp; Fold order minimum is 15 pounds. No minimum order size for dry cleaning.</p>
                                <p><strong>Non-payment</strong></p>
                                <p>Orders that have not been paid for within 30 days of the pickup date will be considered abandoned and all property will be donated to charity.</p>
                                <p><strong>Unclaimed items</strong></p>
                                <p>All property once returned to a locker must be removed from the locker within 48 hours.  Property not claimed after 48 hours will be removed from the locker and the lock will be changed.  Customers have up to 30 days to claim their property.  Any property not claimed within 30 days of the original pick up date will be donated to charity.</p>

                                <p>Items that have been placed in a locker and have not been claimed by any customer will be held for 30 days.  If items are unclaimed after 30 days, all property will be donated to charity.</p>
                                <p><strong>Damaged Property</strong></p>
                                <p>
                                    Zero Laundry follows the standards and policies set forth by the Fabricare Industry and the International Fabricare Institute. We exercise the utmost care in cleaning and processing garments entrusted to us and use such processes which, in our opinion, are best suited to the nature and conditions of each individual garment. Nevertheless, we cannot assume responsibility for inherent weaknesses or defects in materials which may result in tears or development of small holes in fabric that are not readily apparent prior to processing. We can not guarantee against color loss, color bleeding, and shrinkage; or against damage to weak and tender fabrics; or against damage to anscelary items such as belts, buttons, beads, ties or zipper pulls. Zero Laundry\'s liability with respect to any damaged items shall not exceed ten (10) times our charge for cleaning that garment regardless of brand or condition.
                                </p>
                                <p>Any damaged items must be reported via email to &lt;a href=&quot;mailto:\"&gt;info@zerolaundry.com</a> and returned to Zero Laundry for inspection within 5 business days. </p>

                                <p><strong>Lost Items</strong></p>
                                <p>
                                    Any lost items must be reported via email to <a href=\"mailto:support@laundrylocker.com\"></a> within 5 business days. Zero Laundry makes its best reasonable effort to track every item that we process and will review all lost items claims on a case by case basis. Any items determined to have been lost by Zero Laundry will be reimbursed in accordance with the International Fabricare Fair Claims Guide and shall not exceed ten (10) times our charge for cleaning that garment regardless of brand or condition.  Items will be considered lost 30 days after the initial claim is filed.</p>

                                <p> When leaving items in lockers, please ensure that your locker has been correctly closed and locked. Zero Laundry is not responsible for any loss or damage resulting from a failure to properly lock the locker. </p>

                                <p> When using our concierge service, Zero Laundry is not responsible for your items before they are picked up or dropped back off. It is your responsibility to ensure the safety of your items during that time. </p>

                                <p><strong>Loose Items</strong></p>
                                <p>Although, we try as hard as possible track such items, we are not responsible for loose items such as jewelry, watches, cash, detachable buttons, cufflinks, belts, broaches, stings, laces, hoods or loose items on garments, hangers, etc.. We request that customers remove these items and empty pockets prior to leaving items with us as we can not be held responsible for damage to your garments from items left in pockets (Lipstick, Gum, Pens, etc.).</p>
                                <p><strong>Personal Property</strong></p>
                                <p>Any personal property placed in a Zero Laundry locker that appears to have value will be removed by Zero Laundry and stored for 30 days.  If items are unclaimed after 30 days, all property will be donated to charity.</p>
                                <p><strong>Barcodes</strong></p>
                                <p>
                                    Zero Laundry will adhere a permanent barcode to your garments in an inconspicuous location. These barcodes are very important in helping us track your garments so that items are not lost and to ensure you are billed consistently every time. Unfortunately we can not accommodate requests not to adhere barcodes.</p>

                                <p><strong>Turnaround Time</strong></p>
                                <p>Service days and turnaround time vary by location. will make its best reasonable effort to adhere to our service schedule, however, we do not guarantee turnaround times and assume no responsibility for any damages that may occur due to a delay in service.</p>

                            </div>
                            <div id="policybtm" class="well" style="max-width:44em;">
                                <button class="policybtm_close btn btn-default">X</button>
                                <h2>Policy</h2>

                                <p><strong>What information do we collect?</strong></p>
                                <p>We collect information from you when you Submit pick-up request. </p>
                                <p>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, mailing address or phone number. You may, however, visit our site anonymously.</p>

                                <p><strong>What do we use your information for? </strong></p>
                                <p>Any of the information we collect from you may be used in one of the following ways: </p>

                                <p><strong>; To personalize your experience</strong></p>
                                <p>(your information helps us to better respond to your individual needs)</p>

                                <p><strong>; To improve our website</strong></p>
                                <p>(we continually strive to improve our website offerings based on the information and feedback we receive from you)</p>

                                <p><strong>; To improve customer service</strong></p>
                                <p>(your information helps us to more effectively respond to your customer service requests and support needs)</p>

                                <p><strong>; To process transactions</strong></p>
                                <p>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested. </p>
                                </p>; To administer a contest, promotion, survey or other site feature</p>


                                <p><strong>; To send periodic emails</p>
                                <p>The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc. </p>

                                <p><strong>How do we protect your information? </strong></p>

                                <p>We implement a variety of security measures to maintain the safety of your personal information when you place an order or enter, submit, or access  your personal information. </p>

                                <p><strong>Do we use cookies? </strong></p>

                                <p>Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information</p>

                                <p>We use cookies to understand and save your preferences for future visits and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

                                <p>If you prefer, you can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies via your browser settings. Like most websites, if you turn your cookies off, some of our services may not function properly. However, you can still place orders over the telephone or by contacting customer service.</p>

                                <p><strong>Do we disclose any information to outside parties? </strong></p>

                                <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

                                <p><strong>California Online Privacy Protection Act Compliance</strong></p>

                                <p>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>

                                <p>Childrens Online Privacy Protection Act Compliance </p>

                                <p>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>

                                <p>Online Privacy Policy Only </p>

                                <p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>

                                <p><strong>Terms and Conditions </strong></p>

                                <p>Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website athttp://www.zerolaundry.com</p>

                                <p><strong>Your Consent</strong></p>

                                <p>By using our site, you consent to our privacy policy.</p>

                                <p><strong>Changes to our Privacy Policy </strong></p>

                                <p>If we decide to change our privacy policy, we will post those changes on this page, and/or update the Privacy Policy modification date below. </p>

                                <p>This policy was last modified on 1/23/2015</p>

                                <p><strong>Contacting Us </strong></p>

                                <p>If there are any questions regarding this privacy policy you may contact us using the information below. </p>
                            </div>
                            <div id="request_content" class="well" style="max-width:44em;">
                                <button class="request_content_close btn btn-default">X</button>
                                <h2>Request Us in Your Building</h2>
                                <p>We service New Jersey&#039;s Gold Coast area and can partner with residential buildings with and without concierge services.</p><p>Offer the convenience of our technologically enabled dry cleaning and laundry service as an additional amenity to the tenants of your building.</p><p>There are no costs to the property to offer our service and/or lockers!  We service apartments of all sizes.  Our lockers are very space efficient - they only require about 2 ft x 3ft to install and do not require any electrical or internet connections. </p>
                                <p>Should your building add our service, we&#039;ll reward you with $100 credit!</p>
                                <div class="wpcf7" id="wpcf7-f213-o2" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form name="" action="/#wpcf7-f213-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                                        <div style="display: none;">
                                            <input type="hidden" name="_wpcf7" value="213" />
                                            <input type="hidden" name="_wpcf7_version" value="4.1" />
                                            <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f213-o2" />
                                            <input type="hidden" name="_wpnonce" value="a5a1866016" />
                                        </div>
                                        <div class="request-contact-left row">
                                            <label><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" id="name" aria-required="true" aria-invalid="false" placeholder="Name*" /></span></label><br />
                                            <label><span class="wpcf7-form-control-wrap lastname"><input type="text" name="lastname" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" id="lastname" aria-required="true" aria-invalid="false" placeholder="Last Name*" /></span></label>
                                        </div>
                                        <div class="row">
                                            <label><span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email input" id="email" aria-required="true" aria-invalid="false" placeholder="Email*" /></span> </label><br />
                                            <label><span class="wpcf7-form-control-wrap phone"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text input phoneinput" aria-invalid="false" placeholder="Phone" /></span></label>
                                        </div>
                                        <div class="row">
                                            <label><span class="wpcf7-form-control-wrap building_name"><input type="text" name="building_name" value="" size="40" class="wpcf7-form-control wpcf7-text input" id="building_name" aria-invalid="false" placeholder="Building Name" /></span></label><br />
                                            <label><span class="wpcf7-form-control-wrap building_address"><input type="text" name="building_address" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input" id="building_address" aria-required="true" aria-invalid="false" placeholder="Building Address*" /></span></label>
                                        </div>
                                        <div class="request-contact-right row">
                                            <label><span class="wpcf7-form-control-wrap message"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea textarea" aria-invalid="false" placeholder="Do You have anything to add?"></textarea></span> </label><label><span class="wpcf7-form-control-wrap concierge"><span class="wpcf7-form-control wpcf7-checkbox chkbox" id="concierge"><span class="wpcf7-list-item first last"><label><input type="checkbox" name="concierge[]" value="Does your building have a 24-hour concierge?" />&nbsp;<span class="wpcf7-list-item-label">Does your building have a 24-hour concierge?</span></label></span></span></span></p>
                                                <div class="submit-btn">
                                                    <label><input type="submit" value="Send Now" class="wpcf7-form-control wpcf7-submit submit" id="submit" /></label>
                                                </div>
                                             </label>
                                        </div>
                                        <div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>

                            <script>
                                $(document).ready(function () {
                                    $('#terms_content').popup();
                                    $('#policybtm').popup();
                                });
                            </script>
                        </footer>
                        <!--<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>-->
                        <script src="/js/zerolaundry/jquery.popupoverlay.js"></script>
                        <script src="/js/zerolaundry/fastclick.js" type="text/javascript"></script>
                        <script src="/js/zerolaundry/scroll.js" type="text/javascript"></script>
                        <script src="/js/zerolaundry/fixed-responsive-nav.js" type="text/javascript"></script>

                        <!-- begin olark code -->
                        <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark || (function (c) {
                                    var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load";
                                    var nt = function () {
                                        f[z] = function () {
                                            (a.s = a.s || []).push(arguments)
                                        };
                                        var a = f[z]._ = {
                                        }, q = c.methods.length;
                                        while (q--) {
                                            (function (n) {
                                                f[z][n] = function () {
                                                    f[z]("call", n, arguments)
                                                }
                                            })(c.methods[q])
                                        }
                                        a.l = c.loader;
                                        a.i = nt;
                                        a.p = {
                                            0: +new Date};
                                        a.P = function (u) {
                                            a.p[u] = new Date - a.p[0]
                                        };
                                        function s() {
                                            a.P(r);
                                            f[z](r)
                                        }
                                        f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s);
                                        var ld = function () {
                                            function p(hd) {
                                                hd = "head";
                                                return["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
                                            }
                                            var i = "body", m = d[i];
                                            if (!m) {
                                                return setTimeout(ld, 100)
                                            }
                                            a.P(1);
                                            var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o;
                                            n.style.display = "none";
                                            m.insertBefore(n, m.firstChild).id = z;
                                            b.frameBorder = "0";
                                            b.id = z + "-loader";
                                            if (/MSIE[ ]+6/.test(navigator.userAgent)) {
                                                b.src = "javascript:false"
                                            }
                                            b.allowTransparency = "true";
                                            v[j](b);
                                            try {
                                                b.contentWindow[g].open()
                                            } catch (w) {
                                                c[e] = d[e];
                                                o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';";
                                                b[k] = o + "void(0);"
                                            }
                                            try {
                                                var t = b.contentWindow[g];
                                                t.write(p());
                                                t.close()
                                            } catch (x) {
                                                b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
                                            }
                                            a.P(2)
                                        };
                                        ld()
                                    };
                                    nt()
                                })({
                                    loader: "static.olark.com/jsclient/loader0.js", name: "olark", methods: ["configure", "extend", "declare", "identify"]});
                                /* custom configuration goes here (www.olark.com/documentation) */
                                olark.identify('7178-907-10-5423');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7178-907-10-5423/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
                        <!-- end olark code -->
                        <div id="wp-auth-check-wrap" class="hidden">
                            <div id="wp-auth-check-bg"></div>
                            <div id="wp-auth-check">
                                <div class="wp-auth-check-close" tabindex="0" title="Close"></div>
                                <div id="wp-auth-check-form" data-src="http://www.zerolaundry.com/wp-login.php?palo-login=1"></div>
                                <div class="wp-auth-fallback">

                                </div>
                            </div>
                        </div>
                        <style>
                            #wp-auth-check-wrap #wp-auth-check {
                                padding-top: 0 !important
                            }
                            #wp-auth-check-wrap .wp-auth-check-close:before {
                                color:  !important;
                            }
                        </style>
                        <script type="text/javascript">
                            jQuery(document).ready(function ($) {
                                $('a[href="#pa_modal_login"]')
                                        .attr('href', 'http://www.zerolaundry.com/wp-login.php')
                                        .attr('data-palo-modal', 'http://www.zerolaundry.com/wp-login.php?palo-login=1'.replace('&amp;', '&'))
                                        .addClass('palo-modal-login-trigger')
                                        ;
                                $('a[href="#pa_modal_register"]')
                                        .attr('href', 'http://www.zerolaundry.com/wp-login.php?action=register')
                                        .attr('data-palo-modal', 'http://www.zerolaundry.com/wp-login.php?action=register&palo-login=1'.replace('&amp;', '&'))
                                        .addClass('palo-modal-login-trigger')
                                        ;
                            });
                        </script>
                        <!--[if lt IE 9]>
                            <link rel='stylesheet' id='ept-ie-style-css'  href='http://www.zerolaundry.com/wp-content/plugins/easy-pricing-tables/assets/ui/ui-ie.css?ver=4.0.1' type='text/css' media='all' />
                        <![endif]-->
                        <!--<link rel='stylesheet' id='dh-ptp-design1-css'  href='http://www.zerolaundry.com/wp-content/plugins/easy-pricing-tables/assets/pricing-tables/design1/pricingtable.css?ver=4.0.1' type='text/css' media='all' />
                        <link rel='stylesheet' id='palo-front-css'  href='http://www.zerolaundry.com/wp-content/plugins/pa-login/assets/css/front.css?ver=4.0.1' type='text/css' media='all' />
                        <link rel='stylesheet' id='dashicons-css'  href='http://www.zerolaundry.com/wp-includes/css/dashicons.min.css?ver=4.0.1' type='text/css' media='all' />
                        <link rel='stylesheet' id='wp-auth-check-css'  href='http://www.zerolaundry.com/wp-includes/css/wp-auth-check.min.css?ver=4.0.1' type='text/css' media='all' />
                       
                        <script type='text/javascript' src='http://www.zerolaundry.com/wp-content/plugins/pa-login/assets/js/frontend.js?ver=4.0.1'></script>-->
                    </body>
                    </html>
                    <!-- Dynamic page generated in 2.601 seconds. -->
                    <!-- Cached page generated by WP-Super-Cache on 2015-03-02 13:59:31 -->

                    <!-- super cache -->