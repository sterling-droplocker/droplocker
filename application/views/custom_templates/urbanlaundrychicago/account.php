<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0042)http://urbanlaundrychicago.com/?page_id=13 -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US"><head profile="http://gmpg.org/xfn/11"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
<title>UrbanLaundryChicago.com</title>
<meta name="robots" content="noodp,noydir">
<link rel="alternate" type="application/rss+xml" title="Urban Laundry Chicago » Feed" href="http://urbanlaundrychicago.com/?feed=rss2">
<link rel="alternate" type="application/rss+xml" title="Urban Laundry Chicago » Comments Feed" href="http://urbanlaundrychicago.com/?feed=comments-rss2">
            <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" id="latitude-theme-css" href="/css/urbanlaundrychicago/style.css" type="text/css" media="all">
<link rel="stylesheet" id="contact-form-7-css" href="/css/urbanlaundrychicago/styles.css" type="text/css" media="all">
<link rel="stylesheet" id="easy_table_style-css" href="/css/urbanlaundrychicago/style_1.css" type="text/css" media="all">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://urbanlaundrychicago.com/xmlrpc.php?rsd">
	
<style type="text/css" media="screen">
	#rotator {
		position: relative;
		width: 600px;
		height: 235px;
		margin: 0; padding: 0;
		overflow: hidden;
	}
</style>
	
<link rel="Shortcut Icon" href="http://urbanlaundrychicago.com/wp-content/themes/latitude/images/favicon.ico" type="image/x-icon">
<link rel="pingback" href="http://urbanlaundrychicago.com/xmlrpc.php">
<style type="text/css">#header { background: url(/images/urbanlaundrychicago/UL-Header.jpg) no-repeat !important; }</style>
<style type="text/css" id="custom-background-css">
body.custom-background { background-color: #3087d3; }
</style>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
</head>
<body class="page page-id-13 page-template-default custom-background custom-header header-image header-full-width full-width-content" style=""><div id="wrap"><div id="header"><div class="wrap"><div id="title-area"><p id="title"><a href="http://urbanlaundrychicago.com/" title="Urban Laundry Chicago">Urban Laundry Chicago</a></p><p id="description">24/7 Laundry - Simple | Quick | Clean</p></div></div></div><div id="nav"><div class="wrap"><ul id="menu-main" class="menu genesis-nav-menu menu-primary"><li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="http://urbanlaundrychicago.com/?page_id=33">Our Services</a></li>
<li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://urbanlaundrychicago.com/?page_id=11">How It Works</a></li>
<li id="menu-item-89" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89"><a href="http://urbanlaundrychicago.com/?page_id=23">Prices</a></li>
<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="http://urbanlaundrychicago.com/?page_id=27">FAQs</a></li>
<li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="http://urbanlaundrychicago.com/?page_id=25">Locations</a></li>
<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page  page_item page-item-13  menu-item-18"><a href="http://urbanlaundrychicago.com/?page_id=13">About Us</a></li>
<li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="http://urbanlaundrychicago.com/?page_id=15">Contact</a></li>
<li id="menu-item-150" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-150"><a>|</a></li>
<li id="menu-item-59" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-59"><a href="https://myaccount.urbanlaundrychicago.com/register">Sign Up</a></li>
<li id="menu-item-60" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-60"><a href="https://myaccount.urbanlaundrychicago.com/login">Log In</a></li>
</ul></div></div><div id="inner"><div class="wrap"><div id="content-sidebar-wrap"><div id="content" class="hfeed">
                
                <div class="post-13 page type-page status-publish hentry entry">
                    <div id="content_holder">
                    <?=$content;?>
                    </div>
                </div>
            
            
            </div></div></div></div>
        <div id="footer-widgets" class="footer-widgets"><div class="wrap">
        <div class="footer-widgets-1 widget-area"><div id="text-6" class="widget widget_text">
                <div class="widget-wrap"><h4 class="widget-title widgettitle">Connect</h4>
			<div class="textwidget"><a href="mailto:support@urbanlaundrychicago.com?Subject=Urban%20Laundry" target="_top">support@urbanlaundrychicago.com</a><p>
(312) 285-2449</p><p>
1167 W Madison St<br>
Chicago, IL 60607

</p></div>
		</div></div>
</div><div class="footer-widgets-2 widget-area"><div id="text-8" class="widget widget_text"><div class="widget-wrap">
            <div class="textwidget"><img class="aligncenter size-thumbnail wp-image-48" alt="folded_shirts" src="/images/urbanlaundrychicago/folded_shirts-150x150.png" width="150" height="150"></div>
		</div></div>
</div><div class="footer-widgets-3 widget-area"><div id="text-7" class="widget widget_text"><div class="widget-wrap">
            <div class="textwidget"><a href="https://www.facebook.com/GetUrbanLaundry"><img src="/images/urbanlaundrychicago/findusonfacebook.png" alt="find us on facebook" width="200" height="65" class="aligncenter size-full wp-image-149"></a></div>
		</div></div>
</div></div></div><div id="footer" class="footer"><div class="wrap"><div class="gototop"><p><a href="http://urbanlaundrychicago.com/?page_id=13#wrap" rel="nofollow">Return to top of page</a></p></div><div class="creds"><p>Copyright 2013 Urban Laundry Chicago · <a href="http://urbanlaundrychicago.com/wp-login.php">Log in</a></p></div></div></div></div>
<script type="text/javascript">
$(function($) {
    $('#content_holder').children().children().children().removeClass('offset1');
});
</script>

        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>

</body></html>
