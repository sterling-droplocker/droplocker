<!DOCTYPE html>
<html class="" lang="en" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= $title ?> | Fresh Garment Valet</title>

        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src='/bootstrap/js/bootstrap.min.js'></script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>

        <link rel='shortlink' href='http://www.freshgarmentvalet.com/' />
        <style>#responsive-menu .appendLink, #responsive-menu .responsive-menu li a, #responsive-menu #responsive-menu-title a,#responsive-menu .responsive-menu, #responsive-menu div, #responsive-menu .responsive-menu li, #responsive-menu{box-sizing: content-box !important;-moz-box-sizing: content-box !important;-webkit-box-sizing: content-box !important;-o-box-sizing: content-box !important}.RMPushOpen{width: 100% !important;overflow-x: hidden !important;height: 100% !important}.RMPushSlide{position: relative;left: 75%}#responsive-menu{position: absolute;width: 75%;left: -75%;top: 0px;background: #7f7f7f;z-index: 9999;box-shadow: 0px 1px 8px #333333;font-size: 13px !important;max-width: 999px;display: none}#responsive-menu.admin-bar-showing{padding-top: 32px}#click-menu.admin-bar-showing{margin-top: 32px}#responsive-menu #rm-additional-content{padding: 10px 5% !important;width: 90% !important;color: #FFFFFF}#responsive-menu .appendLink{right: 0px !important;position: absolute !important;border: 1px solid #7f7f7f !important;padding: 12px 10px !important;color: #FFFFFF !important;background: #7f7f7f !important;height: 20px !important;line-height: 20px !important;border-right: 0px !important}#responsive-menu .appendLink:hover{cursor: pointer;background: #7f7f7f !important;color: #FFFFFF !important}#responsive-menu .responsive-menu, #responsive-menu div, #responsive-menu .responsive-menu li,#responsive-menu{text-align: left !important}#responsive-menu .RMImage{vertical-align: middle;margin-right: 10px;display: inline-block}#responsive-menu.RMOpened{}#responsive-menu,#responsive-menu input{font-family: source sans pro !important}#responsive-menu #responsive-menu-title{width: 95% !important;font-size: 18px !important;padding: 20px 0px 20px 5% !important;margin-left: 0px !important;background: #7f7f7f !important;white-space: nowrap !important}#responsive-menu #responsive-menu-title,#responsive-menu #responsive-menu-title a{color: #FFFFFF !important;text-decoration: none !important;overflow: hidden !important}#responsive-menu #responsive-menu-title a:hover{color: #FFFFFF !important;text-decoration: none !important}#responsive-menu .appendLink,#responsive-menu .responsive-menu li a,#responsive-menu #responsive-menu-title a{transition: 1s all;-webkit-transition: 1s all;-moz-transition: 1s all;-o-transition: 1s all}#responsive-menu .responsive-menu{width: 100% !important;list-style-type: none !important;margin: 0px !important}#responsive-menu .responsive-menu li.current-menu-item > a,#responsive-menu .responsive-menu li.current-menu-item > .appendLink,#responsive-menu .responsive-menu li.current_page_item > a,#responsive-menu .responsive-menu li.current_page_item > .appendLink{background: #7f7f7f !important;color: #FFFFFF !important}#responsive-menu .responsive-menu li.current-menu-item > a:hover,#responsive-menu .responsive-menu li.current-menu-item > .appendLink:hover,#responsive-menu .responsive-menu li.current_page_item > a:hover,#responsive-menu .responsive-menu li.current_page_item > .appendLink:hover{background: #7f7f7f !important;color: #FFFFFF !important}#responsive-menu.responsive-menu ul{margin-left: 0px !important}#responsive-menu .responsive-menu li{list-style-type: none !important;position: relative !important}#responsive-menu .responsive-menu ul li:last-child{padding-bottom: 0px !important}#responsive-menu .responsive-menu li a{padding: 12px 0px 12px 5% !important;width: 95% !important;display: block !important;height: 20px !important;line-height: 20px !important;overflow: hidden !important;white-space: nowrap !important;color: #FFFFFF !important;border-top: 1px solid #7f7f7f !important;text-decoration: none !important}#click-menu{text-align: center;cursor: pointer;font-size: 13px !important;display: none;position: absolute;right: 2%;top: 5px;color: #FFFFFF;background: #7f7f7f;padding: 5px;z-index: 9999}#responsive-menu #responsiveSearch{display: block !important;width: 95% !important;padding-left: 5% !important;border-top: 1px solid #7f7f7f !important;clear: both !important;padding-top: 10px !important;padding-bottom: 10px !important;height: 40px !important;line-height: 40px !important}#responsive-menu #responsiveSearchSubmit{display: none !important}#responsive-menu #responsiveSearchInput{width: 91% !important;padding: 5px 0px 5px 3% !important;-webkit-appearance: none !important;border-radius: 2px !important;border: 1px solid #7f7f7f !important}#responsive-menu .responsive-menu,#responsive-menu div,#responsive-menu .responsive-menu li{width: 100% !important;margin-left: 0px !important;padding-left: 0px !important}#responsive-menu .responsive-menu li li a{padding-left: 10% !important;width: 90% !important;overflow: hidden !important}#responsive-menu .responsive-menu li li li a{padding-left: 15% !important;width: 85% !important;overflow: hidden !important}#responsive-menu .responsive-menu li li li li a{padding-left: 20% !important;width: 80% !important;overflow: hidden !important}#responsive-menu .responsive-menu li li li li li a{padding-left: 25% !important;width: 75% !important;overflow: hidden !important}#responsive-menu .responsive-menu li a:hover{background: #7f7f7f !important;color: #FFFFFF !important;list-style-type: none !important;text-decoration: none !important}#click-menu #RMX{display: none;font-size: 24px;line-height: 27px !important;height: 27px !important;color: #FFFFFF !important}#click-menu .threeLines{width: 33px !important;height: 27px !important;margin: auto !important}#click-menu .threeLines .line{height: 5px !important;margin-bottom: 6px !important;background: #FFFFFF !important;width: 100% !important}#click-menu .threeLines .line.last{margin-bottom: 0px !important}@media only screen and ( min-width : 0px ) and ( max-width : 800px ){#click-menu{display: block}.horizontal-menu, .ultimatummenu-2-resonsive-menu, .slicknav_menu, .ultimatum-nav, .responsive-nav-form, .thsp-sticky-header{display: none !important}}</style><style type='text/css' media='screen'>
        	h3{ font-family:"Source Sans Pro", arial, sans-serif;}
        	h4{ font-family:"Source Sans Pro", arial, sans-serif;}
        	h5{ font-family:"Source Sans Pro", arial, sans-serif;}
        	h6{ font-family:"Source Sans Pro", arial, sans-serif;}
        	li{ font-family:"Source Sans Pro", arial, sans-serif;}
        	h1{ font-family:"Lato", arial, sans-serif;}
        	h2{ font-family:"Lato", arial, sans-serif;}
        	body{ font-family:"Sanchez", arial, sans-serif;}
        	blockquote{ font-family:"Sanchez", arial, sans-serif;}
        	p{ font-family:"Sanchez", arial, sans-serif;}
        </style>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>

        <!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://www.freshgarmentvalet.com/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]--><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
        <script type="text/javascript">
            //<![CDATA[
            var pptheme = 'facebook';
            //]]>
        </script>

        <link rel='stylesheet' id='ls-google-fonts-css'  href='/css/freshgarmentvalet/fontsLato.css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
        <link rel='stylesheet' id='googlefonts-css'  href='/css/freshgarmentvalet/fontsSans.css?family=Source+Sans+Pro:300,400,700|Lato:300,400,700|Sanchez:400&subset=latin' type='text/css' media='all' />
        <link rel='stylesheet' id='theme-global-css'  href='/css/freshgarmentvalet/theme.global.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='font-awesome-css'  href='/css/freshgarmentvalet/font-awesome.min.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='font-awesome-css'  href='/css/freshgarmentvalet/formsmain.min.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ult_core_template_1-css'  href='/css/freshgarmentvalet/template_1.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='ult_core_layout_4-css'  href='/css/freshgarmentvalet/layout_4.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='js_composer_front-css'  href='/css/freshgarmentvalet/js_composer.css?ver=4.5.2a' type='text/css' media='all' />
        <link rel='stylesheet' id='template_custom_1-css'  href='/css/freshgarmentvalet/template_custom_1.css?ver=4.2.4' type='text/css' media='all' />
        <link rel='stylesheet' id='template_custom_1-css'  href='/css/freshgarmentvalet/ultimate.min.css?ver=3.11.1' type='text/css' media='all' />

        <link rel='stylesheet' id='bsf-Defaults-css'  href='/css/freshgarmentvalet/Defaults.css?ver=4.2.4' type='text/css' media='all' />
        <script type='text/javascript' src='/css/freshgarmentvalet/greensock.js?ver=1.11.8'></script>
        <script type='text/javascript' src='/css/freshgarmentvalet/jquery.js?ver=1.11.2'></script>
        <script type='text/javascript' src='/css/freshgarmentvalet/jquery-migrate.min.js?ver=1.2.1'></script>
        <script type='text/javascript' src='/css/freshgarmentvalet/holder.js?ver=1.9.0'></script>
        <script type='text/javascript' src='/css/freshgarmentvalet/child.js?ver=4.2.4'></script>


         <style type="text/css">
         #responsive-menu #responsiveSearchInput, input {
                height: 44px;
        }

        #responsive-menu .responsive-menu li a {
            padding: 0px 0px 0px 5% !important;
            font-family: source sans pro !important;
        }

        #responsive-menu .responsive-menu li li a {
            margin-top: 5%;
        }

        #responsive-menu, #responsive-menu input {
            font-family: source sans pro !important;
        }

        #kiosk_cardNumber {
            text-indent: 8em;
            height: 32px;
       }
       .kiosk-access-buttons p {
           margin-bottom: 0px;
        }

        .kiosk-access-buttons {
            position: absolute;
            bottom: 0;
            margin-left: 11em;
            top: -6px;
        }

        .kiosk-access-form {
            height: 156px;
            margin-bottom: 10px;
        }

       .on-creditcard p{
          margin-bottom: 0px;
       }

       .page-header {
            margin: 60px 0 20px;
        }

        li.menu-item {
            margin-bottom: 0px;
        }

        .ddsmoothmenuh {
            margin-top: 32px;
        }

        #responsive-menu .responsive-menu, #responsive-menu div, #responsive-menu .responsive-menu li {
            margin-bottom: 10px;
        }

         /*boostrap mods*/

        .navbar-fixed-top .navbar-inner {
            margin-top: 101px;
        }

        .btn-primary {
        	margin-top: 3px;
            margin-bottom: 3px;
            background-color: #00a9dd;
            background-image: -ms-linear-gradient(top, #23527c, #00a9dd);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#23527c), to(#00a9dd));
            background-image: -webkit-linear-gradient(top, #23527c, #00a9dd);
            background-image: -o-linear-gradient(top, #23527c, #00a9dd);
            background-image: -moz-linear-gradient(top, #23527c, #00a9dd);
            background-image: linear-gradient(to bottom, #23527c 0%,#00a9dd 100%);
            background-repeat: repeat-x;
            border-color: #808080;
            filter: progid:dximagetransform.microsoft.gradient(startColorstr='#23527c', endColorstr='#00a9dd', GradientType=0);
            filter: progid:dximagetransform.microsoft.gradient(enabled=false);
        }

        .btn-primary:hover,
        .btn-primary:active,
        .btn-primary.active,
        .btn-primary.disabled,
        .btn-primary[disabled] {
          background-color: #23527c;
          *background-color: #00a9dd;
        }

        .btn-success {
        	margin-top: 3px;
            margin-bottom: 3px;
            background-color: #00a9dd;
            background-image: -ms-linear-gradient(top, #23527c, #00a9dd);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#23527c), to(#00a9dd));
            background-image: -webkit-linear-gradient(top, #23527c, #00a9dd);
            background-image: -o-linear-gradient(top, #23527c, #00a9dd);
            background-image: -moz-linear-gradient(top, #23527c, #00a9dd);
            background-image: linear-gradient(top, #23527c, #00a9dd);
            background-repeat: repeat-x;
            border-color: #808080;
            filter: progid:dximagetransform.microsoft.gradient(startColorstr='#23527c', endColorstr='#00a9dd', GradientType=0);
            filter: progid:dximagetransform.microsoft.gradient(enabled=false);
        }

        .btn-success:hover,
        .btn-success:active,
        .btn-success.active,
        .btn-success.disabled,
        .btn-success[disabled] {
          background-color: #23527c;
          *background-color: #00a9dd;
        }

        .btn-danger {
        	margin-top: 3px;
            margin-bottom: 3px;
            background-color: #00a9dd;
            background-image: -ms-linear-gradient(top, #23527c, #00a9dd);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#23527c), to(#00a9dd));
            background-image: -webkit-linear-gradient(top, #23527c, #00a9dd);
            background-image: -o-linear-gradient(top, #23527c, #00a9dd);
            background-image: -moz-linear-gradient(top, #23527c, #00a9dd);
            background-image: linear-gradient(top, #23527c, #00a9dd);
            background-repeat: repeat-x;
            border-color: #808080;
            filter: progid:dximagetransform.microsoft.gradient(startColorstr='#23527c', endColorstr='#00a9dd', GradientType=0);
            filter: progid:dximagetransform.microsoft.gradient(enabled=false);
        }

        .btn-danger:hover,
        .btn-danger:active,
        .btn-danger.active,
        .btn-danger.disabled,
        .btn-danger[disabled] {
          background-color: #23527c;
          *background-color: #00a9dd;
        }

        .btn-info {
        	margin-top: 3px;
            margin-bottom: 3px;
            background-color: #00a9dd;
            background-image: -ms-linear-gradient(top, #23527c, #00a9dd);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#23527c), to(#00a9dd));
            background-image: -webkit-linear-gradient(top, #23527c, #00a9dd);
            background-image: -o-linear-gradient(top, #23527c, #00a9dd);
            background-image: -moz-linear-gradient(top, #23527c, #00a9dd);
            background-image: linear-gradient(top, #23527c, #00a9dd);
            background-repeat: repeat-x;
            border-color: #808080;
            filter: progid:dximagetransform.microsoft.gradient(startColorstr='#23527c', endColorstr='#00a9dd', GradientType=0);
            filter: progid:dximagetransform.microsoft.gradient(enabled=false);
        }

        .btn-info:hover,
        .btn-info:active,
        .btn-info.active,
        .btn-info.disabled,
        .btn-info[disabled] {
          background-color: #23527c;
          *background-color: #00a9dd;
        }


        a {
            color: #00a9dd;
            text-decoration: none;
        }

        a:hover {
            color: #23527c;
            text-decoration: underline;
        }

        /*
        .alert-info {
            color: ##00a9dd;
            background-color: #F1F1F1;
            border-color: #ABAAAA;
        }*/

        </style>
    </head>

    <body class="home page page-id-21 page-template-default wpb-js-composer js-comp-ver-4.5.2a vc_responsive  ut-tbs3-default ut-layout-home-template">
    <div class="clear"></div>

    <header class="headwrapper">
        <div class="ult-wrapper wrapper" id="wrapper-1">
            <div class="ult-container  container " id="container-1">
                <div class="row">
                		<div class="ult-column col-md-4 " id="col-1-1">
                			<div class="colwrapper"><div id="logo-container"><h1><a href="http://www.freshgarmentvalet.com" class="logo"><img src="/images/freshgarmentvalet/Fresh-Garment-2.svg" alt="fresh garment valet" class="img-responsive"/></a></h1></div></div>
                		</div>
                		<div class="ult-column col-md-8 " id="col-1-2">
                			<div class="colwrapper">
                            <div class="clearfix"></div>
                            <div class="widget widget_ultimatummenu inner-container">
                                <div class="ultimatum-menu-container" data-menureplacer="800">
                                        <div class="ultimatum-regular-menu">
                                            <script type="text/javascript">
                                            	//<![CDATA[
                                            	jQuery(document).ready(function($) {
                                            				ddsmoothmenu.init({
                                            			mainmenuid: "ultimatummenu-2-item",
                                            			orientation: 'h',
                                            			classname: 'ddsmoothmenuh',
                                            			contentsource: "markup"
                                            		})
                                            		$("li .menu-item").css("margin-bottom", "");
                                            	});
                                            	//]]>
                                            </script>
                                            <style> .ddsmoothmenuh ul {float: right;}</style>

                                            <div class="ultimatum-nav">
                                                <div class=" ddsmoothmenuh" id="ultimatummenu-2-item">
                	                                   <ul id="menu-main-menu" class="menu">
                	                                         <li id="menu-item-109" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-109"><a href="http://www.freshgarmentvalet.com/services/">Services</a>
                                                                <ul class="sub-menu">
                                                                	<li id="menu-item-112" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-112"><a href="http://www.freshgarmentvalet.com/how-it-works/">Locker Laundry Service</a></li>
                                                                	<li id="menu-item-113" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-113"><a href="http://www.freshgarmentvalet.com/how-it-works/#delivery">Home or Office Delivery</a></li>
                                                                	<li id="menu-item-118" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118"><a href="http://www.freshgarmentvalet.com/tailor-services/">Tailor Services</a></li>
                                                                	<li id="menu-item-132" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-132"><a href="http://www.freshgarmentvalet.com/how-it-works/#concierge">Concierge</a></li>
                                                                	<li id="menu-item-148" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-148"><a href="http://www.freshgarmentvalet.com/commercial-services/">Commercial Services</a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-12" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-12"><a href="http://www.freshgarmentvalet.com/how-it-works/">How It Works</a>
                                                                <ul class="sub-menu">
                                                                	<li id="menu-item-176" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-176"><a href="http://www.freshgarmentvalet.com/how-it-works/#rewards">Referral and Loyalty Programs</a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-135" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-135"><a href="http://www.freshgarmentvalet.com/about-us/">About Us</a>
                                                                <ul class="sub-menu">
                                                                	<li id="menu-item-11" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11"><a href="http://www.freshgarmentvalet.com/offer-fresh-garments-in-your-building/">Offer Fresh Garments in Your Building</a></li>
                                                                	<li id="menu-item-136" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-136"><a href="http://www.freshgarmentvalet.com/donate/">Donate</a></li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-10" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10"><a href="http://www.freshgarmentvalet.com/contact/">Contact</a></li>
                                                     </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ultimatum-responsive-menu">
                                            <form id="responsive-nav-ultimatummenu-2" action="" method="post" class="responsive-nav-form">
                                                <div>
                                                    <select class="responsive-nav-menu">
                                                        <option value="">Menu</option>
                                                        <option value="http://www.freshgarmentvalet.com/services/">Services</option>
                                                        <option value="http://www.freshgarmentvalet.com/how-it-works/">Locker Laundry Service</option>
                                                        <option value="http://www.freshgarmentvalet.com/how-it-works/#delivery">Home or Office Delivery</option>
                                                        <option value="http://www.freshgarmentvalet.com/tailor-services/">Tailor Services</option>
                                                        <option value="http://www.freshgarmentvalet.com/how-it-works/#concierge">Concierge</option>
                                                        <option value="http://www.freshgarmentvalet.com/commercial-services/">Commercial Services</option>
                                                        <option value="http://www.freshgarmentvalet.com/how-it-works/">How It Works</option>
                                                        <option value="http://www.freshgarmentvalet.com/how-it-works/#rewards">Referral and Loyalty Programs</option>
                                                        <option value="http://www.freshgarmentvalet.com/about-us/">About Us</option>
                                                        <option value="http://www.freshgarmentvalet.com/offer-fresh-garments-in-your-building/">Offer Fresh Garments in Your Building</option>
                                                        <option value="http://www.freshgarmentvalet.com/donate/">Donate</option>
                                                        <option value="http://www.freshgarmentvalet.com/contact/">Contact</option>
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                </div>
                           </div>
                           <div class="clearfix"></div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section role="main" id="content">
        <?= $content; ?>
        <br/>
    </section>


    <footer class="footwrapper">
        <div class="ult-wrapper wrapper " id="wrapper-7">
            <div class="ult-container  container " id="container-7">
                <div class="row">
                    <div class="ult-column col-md-4 " id="col-7-1">
                	       <div class="colwrapper">
                	           <div class="widget widget_ultimatummenu inner-container">
                    	           <script type="text/javascript">
                                    	//<![CDATA[
                                    	jQuery(document).ready(function() {
                                    		for (i=0;i<1;i++) {
                                    			setTimeout('addDot()',1000);
                                    			}
                                    		});
                                    		function addDot() {
                                    			jQuery(document).ready(function($) {
                                    				jQuery('#ultimatummenu-4-item .menu').wfMegaMenu({
                                    					rowItems: 1,
                                    					speed: 'normal',
                                    					effect: 'slide',
                                    					subMenuWidth:200
                                    				});
                                    			});
                                    	}
                                    	//]]>
                                    </script>
                                    <div class="ultimatum-nav">
                                        <div class="wfm-mega-menu" id="ultimatummenu-4-item" style="width:380px;">
                                            <ul id="menu-footer-menu" class="menu">
                                                <li id="menu-item-181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-181"><a href="http://www.freshgarmentvalet.com/terms-conditions/">Terms &amp; Conditions</a></li>
                                                <li id="menu-item-182" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-182"><a href="http://myaccount.freshgarmentvalet.com/register">Register</a></li>
                                                <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="http://myaccount.freshgarmentvalet.com/login">Login</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                           </div>
            	   </div>
                   <div class="ult-column col-md-4 " id="col-7-2">
            			<div class="colwrapper">
                			<div class="widget widget_text inner-container">
                			     <div class="textwidget">
                			         <center><h6>Copyright © Fresh Garment Valet 2015 </h6></center>
                			     </div>
                		    </div>
            		   </div>
            	   </div>
                   <div class="ult-column col-md-4 " id="col-7-3">
                       <div class="colwrapper">
                            <div class="widget widget_ultimatumsocial inner-container">
                                <div class="ult_social" style="text-align: right">
                    			     <a href="https://www.facebook.com/freshgarmentvalet" target="_blank" style="font-size:32px;line-height:32px"><i class="social-icon-circle-facebook"></i></a>
                    			     <a href="https://www.facebook.com/freshgarmentvalet" target="_blank" style="font-size:32px;line-height:32px"><i class="social-icon-circle-twitter"></i></a>
                			    </div>
                    	    </div>
                        </div>
                    </div>
    		  </div>
           </div>
        </div>
    </footer>

    <!-- Added by Responsive Menu Plugin for WordPress - http://responsive.menu -->
    <div id="responsive-menu" >
        <div id="responsive-menu-title">fresh garment valet</div>
        <div id="rm-additional-content"></div>

        <div class="menu-main-menu-container">
            <ul id="menu-main-menu-2" class="responsive-menu">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-109"><a href="http://www.freshgarmentvalet.com/services/">Services</a>
                    <ul class="sub-menu">
                    	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-112"><a href="http://www.freshgarmentvalet.com/how-it-works/">Locker Laundry Service</a></li>
                    	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-113"><a href="http://www.freshgarmentvalet.com/how-it-works/#delivery">Home or Office Delivery</a></li>
                    	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118"><a href="http://www.freshgarmentvalet.com/tailor-services/">Tailor Services</a></li>
                    	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-132"><a href="http://www.freshgarmentvalet.com/how-it-works/#concierge">Concierge</a></li>
                    	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-148"><a href="http://www.freshgarmentvalet.com/commercial-services/">Commercial Services</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-12"><a href="http://www.freshgarmentvalet.com/how-it-works/">How It Works</a>
                    <ul class="sub-menu">
                    	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-176"><a href="http://www.freshgarmentvalet.com/how-it-works/#rewards">Referral and Loyalty Programs</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-135"><a href="http://www.freshgarmentvalet.com/about-us/">About Us</a>
                    <ul class="sub-menu">
                    	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11"><a href="http://www.freshgarmentvalet.com/offer-fresh-garments-in-your-building/">Offer Fresh Garments in Your Building</a></li>
                    	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-136"><a href="http://www.freshgarmentvalet.com/donate/">Donate</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10"><a href="http://www.freshgarmentvalet.com/contact/">Contact</a></li>
            </ul>
         </div>

         <?php /*
         <form action="http://www.freshgarmentvalet.com" id="responsiveSearch" method="get" role="search">
                <input type="search" name="s" value="" placeholder="Search" id="responsiveSearchInput">
                <input type="submit" id="responsiveSearchSubmit" />
         </form>
         */ ?>
    </div>

    <!-- Added by Responsive Menu Plugin for WordPress - http://responsive.menu -->

    <div id="click-menu" class="overlay" role="button" aria-label="Responsive Menu Button">
        <div class="threeLines" id="RM3Lines">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>
    </div>
    <script>var $RMjQuery = jQuery.noConflict();$RMjQuery( document ).ready( function(){var isOpen = false;$RMjQuery( document ).on( 'click', '#click-menu', function(){$RMjQuery( '#responsive-menu' ).css( 'height', $RMjQuery( document ).height() );!isOpen ? openRM() : closeRM()});function openRM(){$RMjQuery( '#responsive-menu' ).css( 'display', 'block' );$RMjQuery( '#responsive-menu' ).addClass( 'RMOpened' );$RMjQuery( '#click-menu' ).addClass( 'click-menu-active' );$RMjQuery( '#responsive-menu' ).stop().animate({left: "0"}, 500, 'linear', function(){$RMjQuery( '#responsive-menu' ).css( 'height', $RMjQuery( document ).height() );isOpen = true})}function closeRM(){$RMjQuery( '#responsive-menu' ).animate({left: -$RMjQuery( '#responsive-menu' ).width()}, 500, 'linear', function(){$RMjQuery( '#responsive-menu' ).css( 'display', 'none' );$RMjQuery( '#responsive-menu' ).removeClass( 'RMOpened' );$RMjQuery( '#click-menu' ).removeClass( 'click-menu-active' );isOpen = false})}$RMjQuery( window ).resize( function(){$RMjQuery( '#responsive-menu' ).css( 'height', $RMjQuery( document ).height() );if( $RMjQuery( window ).width() > 800 ){if( $RMjQuery( '#responsive-menu' ).css( 'left' ) != -$RMjQuery( '#responsive-menu' ).width() ){closeRM()}}});$RMjQuery( '#responsive-menu ul ul' ).css( 'display', 'none' );$RMjQuery( '#responsive-menu .current_page_ancestor.menu-item-has-children' ).children( 'ul' ).css( 'display', 'block' );$RMjQuery( '#responsive-menu .current-menu-ancestor.menu-item-has-children' ).children( 'ul' ).css( 'display', 'block' );$RMjQuery( '#responsive-menu .current-menu-item.menu-item-has-children' ).children( 'ul' ).css( 'display', 'block' );$RMjQuery( '#responsive-menu .current_page_ancestor.page_item_has_children' ).children( 'ul' ).css( 'display', 'block' );$RMjQuery( '#responsive-menu .current-menu-ancestor.page_item_has_children' ).children( 'ul' ).css( 'display', 'block' );$RMjQuery( '#responsive-menu .current-menu-item.page_item_has_children' ).children( 'ul' ).css( 'display', 'block' );var clickLink = '<span class=\"appendLink rm-append-inactive\">▼</span>';var clickedLink = '<span class=\"appendLink rm-append-active\">▲</span>';$RMjQuery( '#responsive-menu .responsive-menu li' ).each( function(){if( $RMjQuery( this ).children( 'ul' ).length > 0 ){if( $RMjQuery( this ).find( '> ul' ).css( 'display' ) == 'none' ){$RMjQuery( this ).prepend( clickLink )}else{$RMjQuery( this ).prepend( clickedLink )}}});$RMjQuery( '.appendLink' ).on( 'click', function(){$RMjQuery( this ).nextAll( '#responsive-menu ul ul' ).slideToggle();$RMjQuery( this ).html( $RMjQuery( this ).hasClass( 'rm-append-active' ) ? '▼' : '▲' );$RMjQuery( this ).toggleClass( 'rm-append-active rm-append-inactive' );$RMjQuery( '#responsive-menu' ).css( 'height', $RMjQuery( document ).height() )});$RMjQuery( '.rm-click-disabled' ).on( 'click', function(){$RMjQuery( this ).nextAll( '#responsive-menu ul ul' ).slideToggle();$RMjQuery( this ).siblings( '.appendLink' ).html( $RMjQuery( this ).hasClass( 'rm-append-active' ) ? '▼' : '▲' );$RMjQuery( this ).toggleClass( 'rm-append-active rm-append-inactive' );$RMjQuery( '#responsive-menu' ).css( 'height', $RMjQuery( document ).height() )});$RMjQuery( '.rm-append-inactive' ).siblings( 'ul' ).css( 'display', 'none' )});</script>
    <script type='text/javascript' src='/css/freshgarmentvalet/theme.global.tbs3.min.js?ver=2'></script>
    <script type='text/javascript' src='/css/freshgarmentvalet/ddsmoothmenu.js?ver=2.38'></script>
    <script type='text/javascript' src='/css/freshgarmentvalet/js_composer_front.js?ver=4.5.2a'></script>
    <script type='text/javascript' src='/css/freshgarmentvalet/jquery.hoverIntent.min.js?ver=r7'></script>
    <script type='text/javascript' src='/css/freshgarmentvalet/jquery.dcmegamenu.1.3.3.js?ver=2.38'></script>

    <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
    <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '<?= $ga?>']);
    _gaq.push(['_setDomainName', 'freshgarmentvalet.com']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
    <?php endif; ?>

    </body>
</html>
