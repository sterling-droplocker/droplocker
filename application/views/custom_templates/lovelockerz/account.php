<!DOCTYPE html>
<html data-wf-domain="www.lovelockerz.com" data-wf-page="57c052e2357c762b1d124f26"
      data-wf-site="57c052e2357c762b1d124f25"
      class="w-mod-js w-mod-no-touch w-mod-video w-mod-no-ios wf-opensans-n3-active wf-opensans-n4-active wf-opensans-n6-active wf-opensans-n7-active wf-opensans-i8-active wf-opensans-n8-active wf-ubuntu-n7-active wf-opensans-i7-active wf-ubuntu-n4-active wf-opensans-i6-active wf-opensans-i4-active wf-ubuntu-n5-active wf-ubuntu-i7-active wf-opensans-i3-active wf-ubuntu-i5-active wf-ubuntu-i4-active wf-ubuntu-i3-active wf-ubuntu-n3-active wf-active">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap-responsive.min.css" />
    <link href="/css/lovelockerz/love-lockerz.css?v=2" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>

    <? if (!DEV): ?>
        <script type="text/javascript" src="/js/report_error.js"></script>
    <? endif; ?>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>

    <style type="text/css">
        .gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }

        .gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }

        .gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }

        @media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }

        .gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }

        a {color: #2a9ed4;}

        .ll_nav a:hover {
            text-decoration: none;
            color: #000;
        }

        html,body {font-family: Ubuntu, Helvetica, sans-serif, sans-serif;font-size: 14px;font-style: normal;font-weight: normal;}

        #content {
            font-family: Ubuntu, Helvetica, sans-serif;
            padding: 20px 0;
        }

        #content input[type=text],
        #content input[type=password] {
            line-height: 19px;
            box-sizing: content-box;
        }

        #content .alert-info {
            background-color: #ea353d;
            color: #FFF;
            border: 0;
            border-radius: 0;
        }

        #content .alert-info .btn-info,
        #register-phone .btn-info,
        #content .login_logout,
        #content .btn {
            border: 3px solid rgba(255, 255, 255, 0.317647);
            border-radius: 3px;
            background-color: #ea353d;
            background-image: none;
            font-family: Ubuntu, Helvetica, sans-serif;
            font-size: 20px;
            line-he|ight: 25px;
            text-align: center;
            letter-spacing: -1px;
            text-transform: uppercase;
            width: auto;
            padding: 6px 15px;
            box-sizing: border-box;
            min-width: 190px;
            color: #FFF;
        }

        #content .btn-navbar {
            min-width: auto;
        }

        #content ul,
        #content ol {
            padding-left: 0px;
        }

        #content .breadcrumb {
            padding: 7px 14px;
        }

        #content a{
            color: #db413d;
        }

        #content .alert-info .btn-info [class^="icon-"],
        #register-phone .btn-info [class^="icon-"],
        #content .login_logout [class^="icon-"],
        #content .btn-success [class^="icon-"]{
            margin-top: 5px;
        }

        #content .kiosk-access-form {
            height: auto;
        }

        #content .kiosk-access-buttons {
            position: static;
        }

        #content .kiosk-access-buttons p:first-child {
            display: none;
        }

        #content #update_kiosk_card_form {
            margin-bottom: 0;
        }

        .calloutBannerPic {
            float: left;
            width: 40%

        }

        .calloutBannerPic img {
            width: 100px;
            height: 100px;
        }

        .calloutBannerText {
            float: left;
            width: 50%;
            margin-left: 10%;
            text-align: left;
        }

        .calloutBannerPicc20 {
            display: none;

        }

        .calloutBannerTextc20 {
            width: 90%;
        }

        .calloutBannerTextc20 a {
            margin-top: 10px;
        }

    </style>

    <title><?= $title ?></title>
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:300,300italic,400,400italic,500,500italic,700,700italic%7COpen+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic">
    <script type="text/javascript">WebFont.load({
            google: {
                families: ["Ubuntu:300,300italic,400,400italic,500,500italic,700,700italic", "Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]
            }
        });</script>
    <link href="/images/lovelockerz/_blank.png" rel="shortcut icon" type="image/x-icon">
    <link href="/images/lovelockerz/webclip.png" rel="apple-touch-icon">
</head>
<body>
<div class="ll_nav w-nav" data-animation="default" data-collapse="medium" data-duration="400">
    <div class="nav_row1 w-row">
        <div class="w-col w-col-11 w-col-stack">
            <div class="ll_nav_wrap w-clearfix">
                <a class="w-nav-brand" href="http://www.lovelockerz.com/#">
                    <img class="ll_logo" src="/images/lovelockerz/Love-Lockerz-Logo2.png">
                </a>
                <nav class="w-nav-menu" role="navigation">
                    <a class="ll_nav_link w-nav-link" href="http://www.lovelockerz.com/#how_it_works">How It Works</a>
                    <a class="ll_nav_link w-nav-link" href="http://www.lovelockerz.com/#locations">Locations</a>
                    <a class="ll_nav_link w-nav-link" href="http://www.lovelockerz.com/about-us">About Us</a>
                    <a class="ll_nav_link w-nav-link" href="http://www.lovelockerz.com/prices">Prices</a>
                    <a class="ll_nav_link w-nav-link" href="http://www.lovelockerz.com/frequently-asked-questions">Faq</a>
                    <a class="ll_nav_link no_side_border1 w-nav-link" href="http://www.lovelockerz.com/contact">Contact</a>
                </nav>
                <div class="ll_mobile_nav w-nav-button">
                    <div class="w-icon-nav-menu"></div>
                </div>
            </div>
        </div>
        <div class="social_column w-clearfix w-col w-col-1 w-col-stack">
            <div class="social_column">
                <a class="social_element w-inline-block" href="http://www.lovelockerz.com/#">
                    <img src="/images/lovelockerz/social%20icon%20facebook.png">
                </a>
                <a class="social_element w-inline-block" href="http://www.lovelockerz.com/#">
                        <img src="/images/lovelockerz/social%20icon%20twit.png">
                </a>
            </div>
        </div>
    </div>
    <div class="w-nav-overlay" data-wf-ignore=""></div>
</div>
<!-- CONTENT (start) -->
<div id="content" class="col-sm-12 clearfix" role="main">
    <div class="inner">
        <div class="article-wrap" id="mainContentDL">
            <article>
                <?= $content ?>
            </article>
        </div>
    </div>
    <!-- .inner (end) -->
</div>
<!-- #content (end) -->
<div class="footer">
    <div class="footer1 grey_bands1" deluminate_imagetype="png"></div>
    <div class="row_padding">
        <div class="footer_row1 w-row">
            <div class="footer_column w-col w-col-4 w-col-stack">
                <div class="w-row">
                    <div class="w-col w-col-11 w-col-stack">
                        <img class="footer_logo" src="/images/lovelockerz/Love-Lockerz-Logo2.png">
                    </div>
                    <div class="w-col w-col-1 w-col-stack">
                        <div class="address alt1 descriptive_1">
                            Corporate Office<br>
                            Street Address<br>
                            City, State, Zip<br>
                            000.000.0000
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_column w-col w-col-4 w-col-stack">
                <div class="footer mini_spacer"></div>
                <a class="ll_button1 w-button white_outline1" href="http://www.lovelockerz.com/contact">Contact Us</a>
                <div class="footer_text_centered phone">(512) 382-7635</div>
            </div>
            <div class="footer_column w-col w-col-4 w-col-stack">
                <div class="starbrite_row1 w-row">
                    <div class="w-col w-col-6"><h3 class="mid_header1 starbrite1">Powered By<br>Starbrite Cleaners</h3>
                    </div>
                    <div class="starbrite_logo_column w-col w-col-6">
                        <a class="w-inline-block" href="http://www.starbritecleanerstx.com/" target="_blank">
                            <img class="starbrite_footer" sizes="(max-width: 479px) 85vw, (max-width: 767px) 180px, 140px"
                                     src="/images/lovelockerz/Happy%20Threads%20logo.png"
                                 srcset="/images/lovelockerz/Happy%20Threads%20logo-p-500x582.png 500w, /images/lovelockerz/Happy%20Threads%20logo.png 507w">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_line">
        <div class="footer_text_centered">© 2016 Love Lockerz&nbsp;</div>
        <div class="mini_spacer"></div>
        <div class="footer_text_centered">2401 Lake Austin Blvd
            Austin TX 78703
        </div>
        <a class="designworksgroup_logo w-inline-block" href="http://www.designworksgroup.com/" target="_blank">
            <img src="/images/lovelockerz/drivenby_horizontal.png">
        </a>
    </div>
</div>

<script src="/js/lovelockerz/lovelockerz.js" type="text/javascript"></script>
<script src="/bootstrap/js/bootstrap.min.js" type="text/javascript">></script>
</body>
</html>