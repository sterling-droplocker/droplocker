<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns# og: http://ogp.me/ns#">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <title>My Account - Amy&#039;s Green Cleaners</title>
      <link rel="icon" href="/images/amysgreencleaners/favicon.ico" type="image/png" />
      <!-- This site is optimized with the Yoast SEO plugin v7.4.2 - https://yoast.com/wordpress/plugins/seo/ -->
      <link rel="canonical" href="https://myaccount.amysgreencleaners.com/account/" />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Contact Us - Amy&#039;s Green Cleaners" />
      <meta property="og:url" content="https://myaccount.amysgreencleaners.com/account/" />
      <meta property="og:site_name" content="Amy&#039;s Green Cleaners" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:title" content="My Account - Amy&#039;s Green Cleaners" />
      <!-- / Yoast SEO plugin. -->
      <link rel='dns-prefetch' href='//maps.googleapis.com' />
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <meta property="og:title" content="My Account"/>
      <meta property="og:url" content="https://myaccount.amysgreencleaners.com/account/"/>
      <meta property="og:site_name" content="Amy&#039;s Green Cleaners"/>
      <meta property="og:type" content="article"/>
      <meta property="og:image" content="/images/amysgreencleaners/AGDC-logo.jpg"/>

      <!-- / Google Analytics by MonsterInsights -->
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"\/js\/amysgreencleaners\/wp-emoji-release.min.js?ver=4.8.1"}};
         !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b===c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>

      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='wpsl-styles-css'  href='/css/amysgreencleaners/styles.min.css?ver=2.2.14' type='text/css' media='all' />
      <link rel='stylesheet' id='vela-css'  href='/css/amysgreencleaners/style.css?ver=4.8.1' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css'  href='/css/amysgreencleaners/font-awesome.min.css?ver=4.3.0' type='text/css' media='all' />
      <link rel='stylesheet' id='bootstrap-css'  href='/css/amysgreencleaners/bootstrap.min.css' type='text/css' media='all' />

      <link rel='stylesheet' id='vela-theme-css'  href='/css/amysgreencleaners/greenearth.css?ver=4.8.1' type='text/css' media='all' />
      <link rel='stylesheet' id='vela-responsive-css'  href='/css/amysgreencleaners/responsive.css?ver=4.8.1' type='text/css' media='all' />
      <link rel='stylesheet' id='redux-google-fonts-wyde_options-css'  href='https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A200%2C300%2C400%2C600%2C700%2C900%2C200italic%2C300italic%2C400italic%2C600italic%2C700italic%2C900italic&#038;subset=vietnamese&#038;ver=1537456455' type='text/css' media='all' />
      <script type='text/javascript'>
         /* <![CDATA[ */
         var monsterinsights_frontend = {"js_events_tracking":"true","is_debug_mode":"false","download_extensions":"doc,exe,js,pdf,ppt,tgz,zip,xls","inbound_paths":"","home_url":"https:\/\/www.amysgreencleaners.com","track_download_as":"event","internal_label":"int","hash_tracking":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='/js/amysgreencleaners/frontend.min.js?ver=7.1.0'></script>
      <script type='text/javascript' src='/js/amysgreencleaners/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='/js/amysgreencleaners/jquery-migrate.min.js?ver=1.4.1'></script>
      <script type='text/javascript' src='/js/amysgreencleaners/modernizr.js'></script>

      <!--[if lte IE 9]>
      <link rel="stylesheet" type="text/css" href="https://www.amysgreencleaners.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.css" media="screen">
      <![endif]--><!--[if IE  8]>
      <link rel="stylesheet" type="text/css" href="/css/amysgreencleaners.com/vc-ie8.css" media="screen">
      <![endif]-->
      <style type="text/css" title="dynamic-css" class="options-output">#footer-widget{background-color:#ffffff;background-size:cover;background-position:center bottom;}#footer-bottom{background-color:#ffffff;background-size:cover;background-position:center bottom;}body{font-family:"Source Sans Pro",Arial, Helvetica, sans-serif;line-height:20px;font-weight:400;color:#333333;font-size:14px;}#header .nav-wrapper > #nav > ul > li > a{font-family:"Source Sans Pro",Arial, Helvetica, sans-serif;letter-spacing:0px;font-weight:400;}h1{font-family:"Source Sans Pro",Arial, Helvetica, sans-serif;line-height:58px;font-weight:normal;font-size:48px;}h2{font-family:"Source Sans Pro",Arial, Helvetica, sans-serif;line-height:52px;font-weight:normal;font-size:40px;}h3{font-family:"Source Sans Pro",Arial, Helvetica, sans-serif;line-height:28px;font-weight:normal;font-size:24px;}h4, h5, h6, .post-title, .post-title a,.counter-box p, .vc_pie_chart .vc_pie_chart_value, .vc_progress_bar .vc_single_bar .vc_label, .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header, .wpb_tabs_nav a{font-family:"Source Sans Pro",Arial, Helvetica, sans-serif;}</style>
      <style type="text/css" data-name="vela-color-scheme">a,
         blockquote:before, 
         .highlight,
         .top-nav > ul > li > a:hover,
         .social-icons a:hover,
         #header #nav > ul > li:hover > a,
         #header #nav > ul > li.active > a,
         #header #nav ul > li.current-menu-item > a,
         #header #nav ul > li.current-menu-ancestor > a,
         .menu-cart:hover > a,
         #search:hover,
         #header #search.active:hover,
         #header.transparent #nav > ul > li:hover > a:before,
         #header.transparent .menu-cart:hover > a:before,
         #header.transparent #search:hover:before,
         #header.fixed #nav > ul > li:hover > a,
         #header.fixed #nav > ul > li.active > a,
         #header.fixed #nav ul > li.current-menu-item > a,
         #header.fixed #nav ul > li.current-menu-ancestor > a,
         #header.fixed .nav-wrapper .menu-cart:hover > a,
         #header.fixed .nav-wrapper #search:hover,
         .post-title a:hover,
         .prev-post a:hover,
         .next-post a:hover,
         .post-meta a:hover,
         .widget a:hover,
         .post-tags a:hover,
         .related-posts a:hover,
         .comment-box h4 a:hover,
         .social-link a,
         #toTop .border,
         .background-striped .flex-direction-nav a:before,
         .wpb_content_element .wpb_tabs_nav li.ui-tabs-active a:after,
         .required,
         .call-to-action:after,
         .counter-box i,
         .heading.title-4 h2:after,
         .tp-caption .heading.title-4:after,
         .heading.title-7 h2:after,
         .tp-caption .heading.title-7:after,
         .testimonial-name a
         {
         color: #FA5C5D;
         }
         .view .post.sticky,
         #header.mobile #nav .menu,
         .wpb_toggle_title_active, 
         #content h4.wpb_toggle_title_active, 
         .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header.ui-state-active,
         .vc_tta-panel.vc_active .vc_tta-panel-heading,
         .vc_tta-color-grey.vc_tta-style-classic .vc_tta-panel.vc_active .vc_tta-panel-heading,
         .heading.title-5 h2:after,
         .tp-caption .heading.title-5:after,
         .heading.title-8 h2:after,
         .tp-caption .heading.title-8:after,
         .heading.title-10 h2:after,
         .tp-caption .heading.title-10:after {
         border-top-color: #FA5C5D;
         }
         ::-moz-selection{
         background: #FA5C5D;
         }
         ::selection {
         background: #FA5C5D;
         }
         #nav > ul > li:hover > a, 
         #nav > ul > li.active > a,
         #header.fixed #nav > ul > li:hover > a, 
         .header .menu-cart:hover > a,
         .menu-cart .dropdown-menu,
         .header #search:hover,
         .top-nav li:hover .sub-menu,
         #nav li:hover .sub-menu,
         #header.mobile #nav li.open > ul,
         blockquote,
         .heading.title-9 h2:before,
         .tp-caption .heading.title-9:after{
         border-color: #FA5C5D;
         }
         .link-button span,
         #toTop .border:before,
         #header.mobile #nav > ul > li.open > a, 
         #header.mobile #nav > ul > li.open:hover > a,
         .menu-cart .cart-items,
         #ajax-loader:before,
         #ajax-loader:after,
         .loading:before,
         .loading:after,
         .autocomplete .dropdown-menu ul > li > a:hover,
         .autocomplete .search-more a:hover, 
         .autocomplete .search-more.selected a, 
         .share-icons .dropdown-menu li a:hover,
         .flex-control-paging li a.flex-active ,
         .testimonials-slider .flex-direction-nav a,
         .counter-box span:before,
         .counter-box span:after,
         .dropcap,
         .wpb_tour .wpb_tabs_nav li.ui-tabs-active,
         .wpb_content_element .wpb_tabs_nav li.ui-tabs-active,
         .wpb_content_element .wpb_tabs_nav li:hover,
         .vc_progress_bar .vc_single_bar .vc_bar,
         .post.sticky .post-date,
         .call-to-action:before,
         .effect-goliath figure p,
         .effect-bubba figure{
         background-color: #FA5C5D;
         }
         .pagination a:hover,
         .comments-nav a:hover,
         .icon-block,
         .icon-block .border,
         input[type="submit"],
         input[type="button"],
         a.button,
         a.tp-caption.rev-btn,
         button{
         border-color:#FA5C5D;
         background-color:#FA5C5D;
         }
         #toTop:hover .border,
         input[type="submit"]:hover,
         input[type="button"]:hover,
         a.link-button,
         a.ghost-button,
         a.ghost-button:hover,
         .tp-caption a.link-button,
         .tp-caption a.ghost-button:hover,
         .heading.title-3 h2:after,
         .tp-caption .heading.title-3:after
         {
         border-color: #FA5C5D;
         color: #FA5C5D;
         }
      </style>
      <style type="text/css">
         #logo > a {
         transform: scale(1.75) !important;
         }
      </style>
      <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1455687480181{padding-top: 0px !important;}</style>
      <noscript>
         <style> .wpb_animate_when_almost_visible { opacity: 1; }</style>
      </noscript>

      <link rel='stylesheet' id='custom-style-css'  href='/css/amysgreencleaners/bootstrap2x.css' type='text/css' media='all' />
      <script src='/bootstrap/js/bootstrap.min.js'></script>
      <script type="text/javascript">
          var $ = jQuery.noConflict();
      </script>
      <?= $_scripts ?>
      <?= $javascript ?>
      <?= $style ?>
      <?= $_styles ?>
   </head>
   <body class="page-template page-template-full-width page-template-full-width-php page page-id-677 wide boxed-shadow wpb-js-composer js-comp-ver-4.7.4 vc_responsive">
      <div id="container" class="container">
         <div id="page">
            <div class="page-inner background with-overlay" style="background-color:#ffffff;">
               <div class="section-overlay">
               </div>
               <header id="header" class="header-v1 light sticky">
                  <div class="header-wrapper">
                     <div class="header">
                        <div class="container">
                           <div class="mobile-nav-icon">
                              <i class="fa fa-bars"></i>
                           </div>
                           <span id="logo">
                           <a href="https://www.amysgreencleaners.com">
                           <img class="dark-logo" src="/images/amysgreencleaners/AGDC-logo.jpg" alt="Logo" />
                           <img class="dark-sticky" src="/images/amysgreencleaners/AGDC-logo.jpg" alt="Sticky Logo" />
                           <img class="light-logo" src="/images/amysgreencleaners/AGDC-logo.jpg" alt="Logo" />
                           </a>
                           </span>
                           <div class="nav-wrapper">
                              <nav id="nav" class="nav dropdown-nav">
                                 <ul class="menu">
                                    <li id="menu-item-4778" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-4778"><a href="https://myaccount.amysgreencleaners.com/login">My Account</a></li>
                                    <li id="menu-item-4207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4207"><a href="https://www.amysgreencleaners.com/services/">Services</a></li>
                                    <li id="menu-item-4818" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4818"><a href="https://www.amysgreencleaners.com/greenearth-cleaning/">GreenEarth Cleaning</a></li>
                                    <li id="menu-item-4328" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4328"><a href="https://www.amysgreencleaners.com/cleaning-tips/">Cleaning Tips</a></li>
                                    <li id="menu-item-4344" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4344"><a href="https://www.amysgreencleaners.com/locations/">Locations</a></li>
                                    <li id="menu-item-4488" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4488"><a href="https://www.amysgreencleaners.com/coupons/">Coupons</a></li>
                                    <li id="menu-item-4558" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4558"><a href="https://www.amysgreencleaners.com/blog/">Blog</a></li>
                                    <li id="menu-item-4205" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-677 menu-item-4205"><a href="https://www.amysgreencleaners.com/contact-us/">Contact Us</a></li>
                                 </ul>
                              </nav>
                              <div id="search">
                                 <div class="search-wrapper">
                                    <form id="ajax-search-form" class="ajax-search-form clear" action="https://www.amysgreencleaners.com" method="get">
                                       <p class="search-input">
                                          <input type="text" name="s" id="keyword" value="" />
                                       </p>
                                       <button class="search-button"><i class="fa fa-search"></i></button>
                                    </form>
                                 </div>
                              </div>
                              <div class="call">
                                 <h3 style=" color:#f15c48;  padding: 0 15px;">
                                    <div id="text-2" class="widget widget_text">
                                       <div class="textwidget">
                                          <!--Have Questions? Call Us. 
                                             (970) 266-8566-->
                                       </div>
                                    </div>
                                 </h3>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </header>
               <div id="content">
                <div id="bootstrap2x">
                <br/>
                    <?=$content;?>
                </div>
                </div>
               </div>
               <!--#content-->
               <footer id="footer">
                  <div id="footer-widget" class="grid-3-col" style="">
                     <div class="container">
                        <div class="column col-md-4">
                           <div class="content">
                              <div id="text-4" class="widget widget_text">
                                 <div class="textwidget">
                                    <p><img src="/images/amysgreencleaners/AGDC-logo.jpg" /></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="column col-md-4">
                           <div class="content">
                              <div id="text-3" class="widget widget_text">
                                 <div class="textwidget">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="column col-md-4">
                           <div class="content">
                              <div id="email newsletter" class="widget eemail_widget">
                                 <h3>Sign Up:</h3>
                                 <script language="javascript" type="text/javascript" src="https://www.amysgreencleaners.com/wp-content/plugins/email-newsletter/widget/widget.js"></script>
                                 <link rel="stylesheet" media="screen" type="text/css" href="https://www.amysgreencleaners.com/wp-content/plugins/email-newsletter/widget/widget.css" />
                                 <div>
                                    <div class="eemail_caption">
                                       Receive Tips and Coupons  
                                    </div>
                                    <div class="eemail_msg">
                                       <span id="eemail_msg"></span>
                                    </div>
                                    <div class="eemail_textbox">
                                       <input class="eemail_textbox_class" name="eemail_txt_email" id="eemail_txt_email" onkeypress="if(event.keyCode==13) eemail_submit_ajax('https://www.amysgreencleaners.com/wp-content/plugins/email-newsletter/widget')" onblur="if(this.value=='') this.value='';" onfocus="if(this.value=='') this.value='';" value="" maxlength="150" type="text">
                                    </div>
                                    <div class="eemail_button">
                                       <input class="eemail_textbox_button" name="eemail_txt_Button" id="eemail_txt_Button" onClick="return eemail_submit_ajax('https://www.amysgreencleaners.com/wp-content/plugins/email-newsletter/widget')" value="Submit" type="button">
                                    </div>
                                 </div>
                              </div>
                              <div id="widget_vela_contact_box-2" class="widget widget_vela_contact_box">
                                 <div class="get_in_touch">
                                    <ul>
                                       <li class="facebook">
                                          <p><a href="http://facebook.com/AmyDryClean"target="_blank"><i class="fa fa-facebook"></i></a></p>
                                       </li>
                                       <li class="twitter">
                                          <p><a href="http://https://twitter.com/AmyDry"target="_blank"><i class="fa fa-twitter"></i></a></p>
                                       </li>
                                       <li class="youtube">
                                          <p><a href="http://instagram.com/amydryclean/"target="_blank"><i class="fa fa-instagram"></i></a></p>
                                       </li>
                                       <li class="mail">
                                          <p><a href="mailto:alanrwitty@q.com"target="_blank"><i class="fa fa-envelope"></i></a></p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="footer-bottom" class="">
                     <div class="container">
                        <div id="footer-nav" class="col-sm-6">
                           <div id="footer-hosted-by">Website Developed and Hosted by <a href="http://www.lagoon.com/" target="_blank" rel="noopener">Digital Lagoon</a>.</div>
                           <ul class="footer-menu">
                           </ul>
                        </div>
                        <div id="footer-text" class="col-sm-6"> ©2018 Amy's Green Cleaners All Rights Reserved. </div>
                     </div>
                     <div style="clear:both;"></div>
                  </div>
               </footer>
               <a id="toTop" href="#"> <span class="border"> <i class="fa fa-angle-up"></i> </span> </a>
            </div>
            <!--.page-inner-->
         </div>
         <!--#page-->
         <div id="preloader">
         </div>


         <script type='text/javascript' src='/js/amysgreencleaners/core.min.js?ver=1.11.4'></script>
         <script type='text/javascript' src='/js/amysgreencleaners/effect.min.js?ver=1.11.4'></script>
         <script type='text/javascript'>
            /* <![CDATA[ */
            var page_settings = {"siteURL":"https:\/\/www.amysgreencleaners.com","mobile_animation":"1","isPreload":"1"};
            /* ]]> */
         </script>
         <script type='text/javascript' src='/js/amysgreencleaners/plugins.js?ver=4.8.1'></script>
         <script type='text/javascript' src='/js/amysgreencleaners/page.js?ver=4.8.1'></script>
         <script type='text/javascript' src='/js/amysgreencleaners/bootstrap.min.js'></script>

         <script type='text/javascript' src='/js/amysgreencleaners/smoothscroll.js?ver=4.8.1'></script>

         <script type='text/javascript' src='/js/amysgreencleaners/wp-embed.min.js?ver=4.8.1'></script>
         <script type='text/javascript' src='/js/amysgreencleaners/js_composer_front.js?ver=4.7.4'></script>
      </div>
      <!--#container-->
   </body>
</html>
