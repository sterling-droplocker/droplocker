<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="/images/lockergenie/favicon.ico">
<title>LockerGenie | <?= $title ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">

<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<link rel='stylesheet' id='themify-framework-css'  href='/css/lockergenie/themify.framework.css?ver=2.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='themify-builder-style-css'  href='/css/lockergenie/themify-builder-style.css?ver=2.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='themify-animate-css'  href='/css/lockergenie/animate.min.css?ver=2.0.1' type='text/css' media='all' />


<link rel='stylesheet' id='easy_table_style-css'  href='/css/lockergenie/easy-table-style.css?ver=1.5.2' type='text/css' media='all' />
<link rel='stylesheet' id='easy_table_style-css'  href='/css/lockergenie/easy-table.css' type='text/css' media='all' />

<link rel='stylesheet' id='theme-style-css'  href='/css/lockergenie/flat-style.css' type='text/css' media='all' />
<link rel='stylesheet' id='theme-style-css'  href='/css/lockergenie/style.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='themify-media-queries-css'  href='/css/lockergenie/media-queries.css?ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='themify-icon-font-css'  href='/css/font-awesome-4.1.0/css/font-awesome.min.css' type='text/css' media='all' />

<!--
<link rel='stylesheet' id='magnific-css'  href='http://www.lockergenie.com/wp-content/themes/flat/themify/css/lightbox.css?ver=2.0.1' type='text/css' media='all' />
-->

<link rel='stylesheet' id='google-fonts-css'  href='https://fonts.googleapis.com/css?family=Josefin+Sans&#038;subset=latin%2Clatin-ext&#038;ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='custom-google-fonts-e4f92ddef45a02652d090f7b4ef6c4f0-css'  href='https://fonts.googleapis.com/css?family=Cabin%3Aregular&#038;subset=latin&#038;ver=4.0' type='text/css' media='all' />
<link rel='stylesheet' id='builder-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Cabin%7CCabin%7CCabin&#038;ver=4.0' type='text/css' media='all' />


<link rel='stylesheet' id='custom-css'  href='/css/lockergenie/custom.css' type='text/css' media='all' />

<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<!--
<script type='text/javascript' src='http://www.lockergenie.com/wp-content/themes/flat/themify/themify-builder/js/themify.scroll-highlight.js?ver=4.0'></script>
-->

<!-- media-queries.js -->
<!--[if lt IE 9]><script src="/js/lockergenie/respond.js"></script><![endif]-->

<!-- html5.js -->
<!--[if lt IE 9]><script src="/js/lockergenie/html5.js"></script><![endif]-->

<!--[if lt IE 9]>
<script src="/js/lockergenie/nwmatcher-1.2.5-min.js"></script>
<script type="text/javascript" src="/js/lockergenie/selectivizr-min.js"></script> 
<![endif]-->



<script>document.documentElement.className += " js";</script>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>


<style type="text/css">
.themify_builder_content-2 > .module_row_7.module_row {
	background-color: #f79f34;;
	font-family: Arial, Helvetica, sans-serif;
	color: #ffffff;;
	margin-top : 100pxpx
}

.themify_builder .text-2-7-0-0.module-text {
	font-family: Cabin;
	color: #ffffff;;
	padding-top : 50pxpx;
	margin-top : 50pxpx
}

.themify_builder .text-2-7-0-1.module-text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	color: #ffffff;
}

#main_content {
    padding: 50px 0;
}

#footer p {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 16px;
}
</style>


</head>
<body class="home page page-id-2 page-template-default skin-default webkit not-ie full_width sidebar-none no-touch">
<div id="pagewrap" class="hfeed site">

	<div id="headerwrap">

		<div id="cloud-left"></div>
		<div id="cloud-right"></div>
    
		
		<header id="header" class="section-inner">

        	
			<hgroup>
                <div id="site-logo">
                    <a href="http://www.lockergenie.com" title="LockerGenie">
                        <img src="/images/lockergenie/locker-genie-logo-revised.png" alt="LockerGenie" title="LockerGenie" />
                        <span style="display: none;">LockerGenie</span>
                    </a>
                </div>
                <div id="site-description" class="site-description">
                    <span>Magical 24-7 Dry Cleaning</span>
                </div>
            </hgroup>

			<div class="social-widget">
				
									<div class="rss"><a href="http://www.lockergenie.com/feed/">RSS</a></div>
							</div>
			<!-- /.social-widget -->

					
		</header>
		<!-- /#header -->

        <nav>
            <div id="menu-icon" class="mobile-button">
                <span><i class="fa fa-bars fa-lg"></i></span>
            </div>
            <!-- Menu -->
            <ul id="main-nav" class="main-nav pagewidth menu-name-menu">
                <li id="menu-item-2366" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-2 menu-item-has-children menu-item-2366">
                    <a href="http://www.lockergenie.com/">How It Works</a>
                    <ul class="sub-menu">
                        <li id="menu-item-2410" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2410">
                            <a href="/#three-easy-steps">Three Easy Steps</a>
                        </li>
                        <li id="menu-item-2411" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2411">
                            <a href="/#three-ways-to-order">Ways to Order</a>
                        </li>
                        <li id="menu-item-2408" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2408">
                            <a href="/#feature">Features</a>
                        </li>
                    </ul>
                </li>
                <li id="menu-item-2365" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2365">
                    <a href="http://www.lockergenie.com/pricing-services/">Pricing &amp; Services</a>
                    <ul class="sub-menu">
                        <li id="menu-item-2412" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2412">
                            <a href="/pricing-services/#dry-cleaning">Dry Cleaning</a>
                        </li>
                        <li id="menu-item-2413" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2413">
                            <a href="/pricing-services/#wash-fold">Wash &amp; Fold</a>
                        </li>
                        <li id="menu-item-2414" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2414">
                            <a href="/pricing-services/#other-services">Other Services</a>
                        </li>
                        <li id="menu-item-2418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2418">
                            <a href="/full-price-list">Full Price List</a>
                        </li>
                    </ul>
                </li>
                <li id="menu-item-2364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2364">
                    <a href="http://www.lockergenie.com/faqs/">FAQS</a>
                </li>
                <li id="menu-item-2363" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2363">
                    <a href="http://www.lockergenie.com/about-us/">Get Lockers</a>
                </li>
                <li id="menu-item-2479" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2479">
                    <a href="http://www.lockergenie.com/locations/">Locations</a>
                </li>
            </ul>
            <!-- /#main-nav -->
        </nav>

			


        				
	</div>
	<!-- /#headerwrap -->
	
	<div id="body" class="clearfix">

		

<!-- layout-container -->
<div id="layout" class="pagewidth clearfix">

		<!-- content -->
	<div id="content" class="clearfix">
    	
		
							<div id="page-2" class="type-page" itemscope itemtype="http://schema.org/Article">

			<!-- page-title -->
						<!-- /page-title -->

			<div class="page-content entry-content" itemprop="articleBody">

				<div id="themify_builder_content-2" data-postid="2" class="themify_builder_content themify_builder_content-2 themify_builder themify_builder_front">
				
			<!-- module_row -->
			<div class="themify_builder_row module_row module_row_0 clearfix fullwidth">
                <div id="main_content">
                    <?= $content ?>
                </div>
			</div>
			<!-- /module_row -->
			
			

			
			<!-- module_row -->
			<div class="themify_builder_row module_row module_row_7 clearfix fullwidth" >
				<div class="row_inner_wrapper">
					<div class="row_inner">
									<div class="col-full first tb-column">
																					
											
<!-- module text -->
<div id="text-2-7-0-0" class="module module-text text-2-7-0-0   ">
	
		
	<p> </p>
<h5 style="text-align: center;"><strong>REQUEST LOCKERS IN YOUR BUILDING</strong></h5>

	</div>
<!-- /module text -->


<!-- module text -->
<div id="text-2-7-0-1" class="module module-text text-2-7-0-1   ">
	
		
	<div style="max-width: 700px; margin: 0 auto; font-size: 14px;">
<p>Property managers, landlords, and tenants can request to have Locker Genie’s 24-hour lockers placed directly in their building. Installation is free and requires no construction.</p>
</div>
<p style="text-align: center;"><a href="http://www.lockergenie.com/about-us/#request" class="shortcode button  " style="background-color: #eeeeee;color: #2980b9;" target=""><strong>REQUEST LOCKERS</strong></a></p>

	</div>
<!-- /module text -->
									
																			</div>
									<!-- /col -->
							  

						
												
											
					</div>
					<!-- /row_inner -->
				</div>
			<!-- /row_inner_wrapper -->
			</div>
			<!-- /module_row -->
			
			
			<!-- module_row -->
			<div class="themify_builder_row module_row module_row_545a32c86295d clearfix" >
				<div class="row_inner_wrapper">
					<div class="row_inner">

						
						
						
						
						<div class="themify_builder_col col-full first last">
															
								&nbsp;							
													</div>
						<!-- /col -->

						
												
											
					</div>
					<!-- /row_inner -->
				</div>
			<!-- /row_inner_wrapper -->
			</div>
			<!-- /module_row -->
			
</div>
<!-- /themify_builder_content -->
				
				<!--  -->

			</div>
			<!-- /.post-content -->

			</div><!-- /.type-page -->
		

		
			</div>
	<!-- /content -->
    
	
</div>
<!-- /layout-container -->
	

	    </div>
	<!-- /body -->
		
	<div id="footerwrap">
    
    			<footer id="footer" class="pagewidth clearfix">
				

			
	<div class="footer-widgets clearfix">

								<div class="col4-2 first">
                                    <div id="text-2" class="widget widget_text">
                                        <h4 class="widgettitle">LOCKERGENIE</h4>
                                        <div class="textwidget">
                                            <p>600 N Westmoreland Drive<br>
                                                Orlando Fl 32805</p>
                                            <p>
                                                <a href="mailto:service@lockergenie.com?Subject=Web%20Query">service@lockergenie.com</a><br>
                                                (407) 270-6516<br>
                                                <br><a href="http://www.lockergenie.com/privacy-policy">Privacy Policy</a></p>
                                            <p>© LockerGenie <script type="text/javascript">
                                                    var date_object = new Date();
                                                    var year = date_object.getYear();
                                                    if(year < 2000) { year = year + 1900; }
                                                    document.write(year);
                                                </script>2017</p>
                                        </div>
                                    </div>
							</div>
								<div class="col4-2 ">
				<div id="themify-social-links-3" class="widget themify-social-links"><ul class="social-links horizontal">
							<li class="social-link-item facebook font-icon icon-medium">
								<a href="https://www.facebook.com/pages/LockerGenie/711024348976091" title="Facebook" ><i class="fa fa-facebook" style="color: #ffffff;background-color: #0763b8;"></i>  </a>
							</li>
							<!-- /themify-link-item -->
							<li class="social-link-item twitter font-icon icon-medium">
								<a href="https://twitter.com/lockergenie" title="Twitter" ><i class="fa fa-twitter" style="color: #ffffff;background-color: #00adef;"></i>  </a>
							</li>
							<!-- /themify-link-item -->
							<li class="social-link-item apple font-icon icon-medium">
								<a href="https://itunes.apple.com/us/app/locker-genie/id937498510?mt=8&amp;uo=4" title="Apple" ><i class="fa fa-apple" style="color: #ffffff;background-color: #662e91;"></i>  </a>
							</li>
							<!-- /themify-link-item -->
							<li class="social-link-item android font-icon icon-medium">
								<a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.lockerGenie&amp;hl=en" title="Android" ><i class="fa fa-android" style="color: #ffffff;background-color: #7cc576;"></i>  </a>
							</li>
							<!-- /themify-link-item --></ul></div><div id="text-3" class="widget widget_text">			<div class="textwidget"><p class="back-top">
				<a href="#header">
					<img src="/images/lockergenie/back-top.png" alt="Back to Top">
				</a>
			</p></div>
		</div>			</div>
		
	</div>
	<!-- /.footer-widgets -->

			<div class="footer-text clearfix">
				<div class="one"> </div>				<div class="two"> </div>			</div>
			<!-- /footer-text --> 
					</footer>
		<!-- /#footer --> 
        	</div>
	<!-- /#footerwrap -->
	
</div>
<!-- /#pagewrap -->


<!-- wp_footer -->

<script type="text/javascript">
$('#menu-icon').click(function(evt) {
    $('#main-nav').toggle();
});

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 222) {
        $("#headerwrap").addClass("fixed-header");
        $('#content').css('padding-top', '276px');
    } else {
        $("#headerwrap").removeClass("fixed-header");
        $('#content').css('padding-top', '0px');
    }
});
</script>


<? if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', '<?= $_SERVER["HTTP_HOST"] ?>']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<? endif; ?>

</body>
</html>
