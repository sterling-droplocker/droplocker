<?php
/**
 * New Business Account Template
 */
?>
<?php
$system_lang = get_customer_business_language_key();

$lang_swap_redirect = '&r=' . base64_encode($this->uri->uri_string);
$logged_in = $this->customer_id;

$lang_switch['fr'] = $logged_in ? '/account/profile/change_language?ll_swap=fr' . $lang_swap_redirect : '?ll_lang=fr';
$lang_switch['en'] = $logged_in ? '/account/profile/change_language?ll_swap=en' . $lang_swap_redirect : '?ll_lang=en';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="/css/dropgo/custom-styles.css">
        <link href="/css/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" />

        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>

        <?php if (ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo $ga ?>']);
                _gaq.push(['_setDomainName', 'dashlocker.com']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
        <?php endif; ?>
        <?= $javascript ?>
        <?= $_scripts ?>
        <?= $style ?>
        <?= $_styles ?>

        <style>
            #header{
                width:100%;
                text-align:center;
                background-color:#d9edf7;
                border-bottom:1px solid #73a5bc;
                margin-bottom:20px;
            }
            #inner-header{
                text-align:left;
                width:970px;
                margin:0 auto;
            }

            #footer{
                width:100%;
                text-align:center;
                background-color:#d9edf7;
            }
            #inner-footer{
                text-align:left;
                width:970px;
                margin:0 auto;
            }
        </style>

    </head>
    <body>

        <div  id="customer-menu" class="span10 center">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="http://dropgo.ca">
                        <img src="/images/dropgo/logo.png" />
                    </a>    

                    <div id="main_menu" class="nav-collapse">
                        <ul class="nav">
                        <? if ($system_lang=="fr"): ?>
                            <li><a href="http://dropgo.ca/fr/#section-home">Accueil</a></li>
                            <li><a href="http://dropgo.ca/fr/#section-introduction">Mode d’emploi</a></li>
                            <li><a href="http://dropgo.ca/fr/#section-services">Services</a></li>
                            <li><a href="http://dropgo.ca/fr/#section-prices">Prix</a></li>
                            <li><a href="http://dropgo.ca/fr/contact/">Contact</a></li>
                            <li><a href="http://dropgo.ca/fr/faq/">FAQ</a></li>
                            <li><a href="http://dropgo.ca/fr/#map-sec">Nous trouver</a></li>
                            <li><a href="http://dropgo.ca/fr/register/">Créer un Compte</a></li>
                            <li><a href="http://dropgo.ca/fr/login/">Se Connecter</a></li>
                        <? else: ?>
                            <li><a href="http://dropgo.ca/#section-home">Home</a></li>
                            <li><a href="http://dropgo.ca/#section-introduction">How It Works</a></li>
                            <li><a href="http://dropgo.ca/#section-services">Services</a></li>
                            <li><a href="http://dropgo.ca/#section-prices">Prices</a></li>
                            <li><a href="http://dropgo.ca/contact/">Contact</a></li>
                            <li><a href="http://dropgo.ca/faq/">FAQ</a></li>
                            <li><a href="http://dropgo.ca/#map-sec">Locations</a></li>
                            <li><a href="http://dropgo.ca/register/">Register</a></li>
                            <li><a href="http://dropgo.ca/login/">Login</a></li>
                        <? endif; ?>
                            <li>
                                  <a href="<?=($system_lang=="fr")?$lang_switch['en']:$lang_switch['fr'] ?>"><span class="fa fa-globe"></span>&nbsp;
                                    <?=($system_lang=="fr")?"English":"French" ?>
                                  </a>

                            </li>
                        </ul>
                    </div>
                   
                </div>
            </div>

        </div>
                
        <div class="content-wrapper" style='min-height:600px;'>
            <?php
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content
            ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer ?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                <? if ($system_lang=="fr"): ?>
                    Copyright <?php echo $this->business->companyName ?> <?php echo date('Y') ?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                <? else: ?>

                <? endif; ?>
                </div>
            </div>
        </div>

        <footer class="fluid-container">
                <div class="container">
                                <div class="bottom-menu text-center">
                                        <ul class="list-inline">
                                                <? if ($system_lang=="fr"): ?>
                                                    <li><a href="mailto:info@dropgo.ca">Demandez à DROP&GO</a></li>
                                                    <li>|</li>
                                                    <li><a href="http://dropgo.ca/fr/contact/">Avoir DROP&GO dans votre immeuble</a></li>
                                                    <li>|</li>
                                                    <li><a href="http://dropgo.ca/terms-and-conditions/fr/">Termes et conditions</a></li>
                                                <? else: ?>
                                                    <li><a href="mailto:info@dropgo.ca">Ask Drop&amp;Go</a></li>
                                                    <li>|</li>
                                                    <li><a href="http://dropgo.ca/contact/">Get DROP&amp;GO in your building</a></li>
                                                    <li>|</li>
                                                    <li><a href="http://dropgo.ca/terms-and-conditions/">Terms and Conditions</a></li>
                                                <? endif; ?>

                                        </ul>
                                </div>
                    <div class="copyright">
                        <? if ($system_lang=="fr"): ?>
                            <p>Copyright 2015. Tous droits réservés. DROP&GO offre le nettoyage à sec, la buanderie et les retouches avec service premium de ramassage et livraison via une application mobile à Montréal: c'est la façon simple, pratique, rapide et abordable de faire votre lavage et nettoyage. Essayez DROP&GO, inscrivez-vous au seul service de nettoyeurs intelligents de Montréal!</p>
                        <? else: ?>
                            <p>Copyright 2015. All Rights Reserved. DROP&amp;GO provides a premium app-based dry cleaning, laundry, and alterations pickup and delivery service in Montreal: it's the easy, convenient, time saving and affordable way to take care of all your laundry and dry cleaning tasks. Try DROP&amp;GO, sign up for our stress free smart cleaner service in Montreal!</p> 
                        <? endif; ?>
                    </div>
                    <p class="footer-download">
                        <? if ($system_lang=="fr"): ?>
                            <span>Téléchargez: &nbsp;&nbsp;&nbsp;</span> 
                        <? else: ?>
                            <span>Download From: &nbsp;&nbsp;&nbsp;</span> 
                        <? endif; ?>
                        
                        <a href="http://dropgo.ca/#"><i class="fa fa-android" style=" vertical-align: middle;"></i></a>&nbsp;&nbsp;
                        <a href="http://dropgo.ca/#"><i class="fa fa-apple" style=" vertical-align: middle;"></i></a>
                    </p>
                </div>
        </footer>        
        
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
            $('.dropdown-toggle').dropdown();
        </script>
    </body>


</html>
