<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <link href='https://fonts.googleapis.com/css?family=Abel|Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
        <link href="/css/zoomlocker/css" rel="stylesheet">
        <link href="/css/zoomlocker/style.css" rel="stylesheet">
        <link href="/css/zoomlocker/styles.css?v=2" rel="stylesheet">
        
        <link href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
         <link href="/css/zoomlocker/bootstrap.css" rel="stylesheet">
        <link href="/css/zoomlocker/bootstrap_other.css" rel="stylesheet">
        <link href="/css/zoomlocker/responsive.css" rel="stylesheet">
                
         <script src="/js/zoomlocker/scripts.js"></script>
          <script src="/js/zoomlocker/scripts1.js"></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'zoomlocker.com']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
        
        <meta charset="UTF-8" />
	<title>Zoomlocker</title>

</head>
 
<body class="page page-id-14 page-template page-template-page-full-width-php" style="">			
    <div class="row-fluid">
		<header role="banner">
			<div id="inner-header" class="clearfix">
				<div class="navbar navbar-fixed-top">
					<div class="navbar-inner">
						<div class="container-fluid nav-container">
							<nav role="navigation">
								<span class="resizeClass"></span>
								<a class="brand" id="logo" title="Zoom into 24/7 Dry Cleaning and Laundry!" href="http://www.zoomlocker.com/">
                                    <img src="/images/zoomlocker/logo2.png" alt="Zoom into 24/7 Dry Cleaning and Laundry!" style="width:274px;">
								</a>
								
								<a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
								</a>
								
                                <div id="main_menu" class="nav-collapse">
                                    <ul id="menu-header" class="nav">
                                        <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown">
                                            <a href="http://www.zoomlocker.com/what-is-zoomlocker/" class="dropdown-toggle disabled" data-toggle="dropdown">What is ZoomLocker?<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-176" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/what-is-zoomlocker/#how-it-works">How It Works</a></li>
                                                <li id="menu-item-178" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/what-is-zoomlocker/#features">Features</a></li>
                                                <li id="menu-item-179" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/what-is-zoomlocker/#laundry-rewards">Laundry Rewards</a></li>
                                                <li id="menu-item-180" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/what-is-zoomlocker/#faqs">FAQs</a></li>
                                                <li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown dropdown-submenu"><a href="http://www.zoomlocker.com/247-services/">Services</a>
                                                    <ul class="dropdown-menu">
                                                        <li id="menu-item-186" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/247-services/#dry-cleaning">Dry Cleaning Prices</a></li>
                                                        <li id="menu-item-187" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/247-services/#wash-fold">Wash &amp; Fold Prices</a></li>
                                                        <li id="menu-item-261" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/247-services/#plans">Wash &amp; Fold Plans</a></li>
                                                        <li id="menu-item-188" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/247-services/#shoes">Shoe Prices</a></li>
                                                        <li id="menu-item-189" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/247-services/#packages">Package Delivery Prices</a></li>
                                                    </ul>
                                                </li><!--.dropdown-->
                                            </ul>
                                        </li>
                                        <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown"><a href="http://www.zoomlocker.com/get-zoomlocker/" class="dropdown-toggle disabled" data-toggle="dropdown">Get ZoomLocker<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-191" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/get-zoomlocker/#building">In Your Building</a></li>
                                                <li id="menu-item-192" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/get-zoomlocker/#commercial">For Commercial Cleaning</a></li>
                                                <li id="menu-item-193" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/get-zoomlocker/#dry-cleaners">For Dry Cleaners</a></li>
                                                <li id="menu-item-194" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/get-zoomlocker/#other-partners">Other Partnerships</a></li><!--.dropdown-->
                                            </ul>
                                        </li>
                                        <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown"><a href="http://www.zoomlocker.com/about-us/" class="dropdown-toggle disabled" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-197" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/about-us/#contact-us">Contact Us</a></li>
                                                <li id="menu-item-198" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/about-us/#about">About ZoomLocker</a></li>
                                                <li id="menu-item-199" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://www.zoomlocker.com/about-us/#affiliations">Affiliations</a></li><!--.dropdown-->
                                            </ul>
                                        </li>
                                        <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.zoomlocker.com/locations/">Locations</a></li>
                                        <li id="menu-item-24" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://myaccount.zoomlocker.com/register">Register</a></li>
                                        <li id="menu-item-25" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="https://myaccount.zoomlocker.com/account">Log In</a></li>
                                    </ul>								
                                </div>
                            </nav>							
						</div> <!-- end .nav-container -->
					</div> <!-- end .navbar-inner -->
				</div> <!-- end .navbar -->
			</div> <!-- end #inner-header -->
		</header> <!-- end header -->

            <div></div>
            <div class="row remove-offset1" style='min-height:600px; padding-top:20px;'>
            <?php 
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content 
            ?>
            </div>
    </div>
    <div class='container-fluid hidden-phone' id="footer">
        <?php echo $footer?>
    </div>

        <footer role="contentinfo">
            <div id="inner-footer" class="clearfix">
                <div id="widget-footer" class="clearfix row-fluid">
                    <div class="row">
						<div class="span4" style="margin-left:0;">
							<h2>ZoomLocker</h2>
							<h5>166 Alahambra Circle</h5>
							<h5>Coral Gables, Fl 33134</h5>
							<h5>(786) 763-0720</h5>
							<a href="mailto:service@zoomlocker.com">service@zoomlocker.com</a>
							<p></p>
							<p><small>© Copyright ZoomLocker 2013</small></p>
							<p></p>
						</div>
						<div class="span3 social-buttons">
							<div class="row">
								<div class="span1" style="margin-right:20px;">
									<div class="square"><a href="https://www.facebook.com/pages/Zoom-Locker/438485869602131"><i class="icon-facebook-sign icon-3x"></i></a></div>
									<div class="square"><a href="http://www.linkedin.com/company/zoom-locker?trk=company_logo"><i class="icon-linkedin-sign icon-3x"></i></a></div>
								</div>
								<div class="span1">
									<div class="square"><a href="https://itunes.apple.com/us/app/zoom-locker/id798766309"><i class="icon-apple icon-3x"></i></a></div>
									<div class="square"><a href="https://plus.google.com/b/107765746748956597600/?hl=en"><i class="icon-google-plus-sign icon-3x"></i></a></div>	
								</div>
								<div class="span4">
								</div>
							</div>
						</div>

						<div class="span3">
							<h3>Join our Mailing List</h3>
							<span>Sign up here to subscribe to our mailing list. We'll send you locker related updates, news, and coupons.</span><br>
				            <div id="text-2" class="widget widget_text">			<div class="textwidget"><div class="wpcf7" id="wpcf7-f206-o2"><form action="/#wpcf7-f206-o2" method="post" class="wpcf7-form nice" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="206">
<input type="hidden" name="_wpcf7_version" value="3.7.2">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f206-o2">
<input type="hidden" name="_wpnonce" value="62b4fbc07a">
</div>
<p><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="/images/zoomlocker/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;"></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>
		</div>						</div>
					</div>
		          </div>
				</div> <!-- end #inner-footer -->
			</footer>

 <!-- end footer -->

    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script>
            $('.dropdown-toggle').dropdown();
            $(function(){
               // $('.register_page_container').addClass('span12');
            });
    </script>
    
    <style type="text/css">
        #default_account_nav ul li{
            padding-bottom:10px;
        }
        .remove-offset1 .offset1 {
            _margin-left: 0;
        }
    </style>
</body>
</html>
