<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="en-US"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->
<head>

<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width" />

<title>Easy Laundry | <?= $title ?></title>

<!--[if lt IE 9]><script src="/js/easylaundry/html5.js" type="text/javascript"></script><![endif]-->

<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<link rel='stylesheet' id='twentytwelve-fonts-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link href='http://fonts.googleapis.com/css?family=Coda+Caption:800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet' type='text/css'>
<link rel='stylesheet' id='twentytwelve-style-css'  href='/css/easylaundry/style.css?ver=3.9.2' type='text/css' media='all' />
<!--[if lt IE 9]><link rel='stylesheet' id='twentytwelve-ie-css'  href='/css/easylaundry/ie.css?ver=20121010' type='text/css' media='all' /><![endif]-->

<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<style type="text/css" id="twentytwelve-header-css">
.site-title,
.site-description {
    position: absolute;
    clip: rect(1px 1px 1px 1px); /* IE7 */
    clip: rect(1px, 1px, 1px, 1px);
}

/*
.span7.offset1,
.span12.offset1 {
    margin-left: 22px;
}
*/

#page {
    margin-top: 20px;
}

#main_content {
    padding-top: 20px;
    padding-bottom: 50px;
}

@media (max-width: 767px) {
    #main_content {
        padding-right: 20px;
        padding-left: 20px;
    }
}

</style>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>
</head>
<body class="home page page-id-19 page-template page-template-page-templatesfull-width-php full-width custom-font-enabled single-author">
<div id="page" class="hfeed site container">

<header id="masthead" class="site-header" role="banner">
    <hgroup>
        <h1 class="site-title"><a href="http://www.easylaundry.net/" title="Easy Laundry" rel="home">Easy Laundry</a></h1>
        <h2 class="site-description">Just another WordPress site</h2>

    </hgroup>
    <a href="http://www.easylaundry.net/"><img src="/images/easylaundry/watc-header.gif" class="header-image" width="960" height="197" alt=""></a>

    <nav id="site-navigation"  class="main-navigation" role="navigation">
        <div class="row nav-menu menu-menu-1-container">
            <div class="span7 offset1">
                <ul>
                    <li id="menu-item-40" class="homeimg menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-19 current_page_item menu-item-40"><a href="http://www.easylaundry.net/"></a></li><li class="sep">|</li>
                    <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-41"><a href="http://www.easylaundry.net/about-us/">About Us</a></li><li class="sep">|</li>
                    <li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="http://www.easylaundry.net/our-services/">Our Services</a></li><li class="sep">|</li>
                    <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a href="http://www.easylaundry.net/locations/">Locations</a></li><li class="sep">|</li>
                    <li id="menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-42"><a href="http://www.easylaundry.net/careers/">Careers</a></li><li class="sep">|</li>
                    <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="http://www.easylaundry.net/contact-us/">Contact Us</a></li><li class="sep">|</li>
                </ul>
            </div>
            <div class="span1">
                <a href="https://www.facebook.com/WashAroundtheClock" target="_blank" style="line-height: 27px;"><img src="/images/easylaundry/watc-facebook-nav.gif"></a>
            </div>
        </div>
    </nav><!-- #site-navigation -->
</header>

<div id="main_content">
    <?= $content ?>
</div>

</div>
<footer id="colophon" role="contentinfo">
     <div id="copyright">
        © 2014 <a href="http://www.easylaundry.net" target="_blank">Wash Around the Clock</a>
        <!-- /copyright -->
    </div>
    <div id="footnav">
        <a href="http://www.easylaundry.net/">Home</a>
        <span class="sep">|</span>
        <a href="http://www.easylaundry.net/about-us">About Us</a>
        <span class="sep">|</span>
        <a href="http://www.easylaundry.net/our-services">Our Services</a>
        <span class="sep">|</span>
        <a href="http://www.easylaundry.net/locations">Locations</a>
        <span class="sep">|</span>
        <a href="http://www.easylaundry.net/careers">Careers</a>
        <span class="sep">|</span>
        <a href="http://www.easylaundry.net/contact-us">Contact Us</a>
        <!-- /subnav -->
    </div>
</footer>

<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
