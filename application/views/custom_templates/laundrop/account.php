<?php

$system_lang = get_customer_business_language_key();

$lang_swap_redirect = '&r=' . base64_encode($this->uri->uri_string);
$logged_in = $this->customer_id;

$lang_switch['es'] = $logged_in ? '/account/profile/change_language?ll_swap=es' . $lang_swap_redirect : '?ll_lang=es';
$lang_switch['en'] = $logged_in ? '/account/profile/change_language?ll_swap=en' . $lang_swap_redirect : '?ll_lang=en';


?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="shortcut icon" href="/images/laundrop/faviconLAUNDROP.png">
<title>Laundrop | <?= $title ?></title>

<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">


<link rel="stylesheet" href="/css/laundrop/language-selector.css?v=3.1.8.2" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" media="all" href="/css/laundrop/style.css" />


<link rel='stylesheet' id='shortcodes-css'  href='/css/laundrop/shortcode.css?ver=3.9.2' type='text/css' media='all' />
<link rel='stylesheet' id='front-estilos-css'  href='/css/laundrop/estilos.css?ver=3.9.2' type='text/css' media='all' />
<link rel='stylesheet' id='cpsh-shortcodes-css'  href='/css/laundrop/shortcodes.css?ver=0.6.6' type='text/css' media='all' />

<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<script type='text/javascript' src='/js/laundrop/ddsmoothmenu.js?ver=3.9.2'></script>
<script type='text/javascript' src='/js/laundrop/custom.js?ver=3.9.2'></script>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>

<style type="text/css">
#main_content {
    padding: 0 0 50px 0;
}

@media (max-width: 767px) {
    body {
        padding-left: 0;
        padding-right: 0;
    }
    
    #main_content {
        padding-left: 20px;
        padding-right: 20px;
    }
}
</style>

<style media="print" type="text/css">
    div.faq_answer {display: block!important;}
    p.faq_nav {display: none;}
</style>

<!--[if lte IE 9]>
<style>
.feature-post .feature-box {
margin-right:10px;
}
</style>
<![endif]-->

</head>
<body>




<!-- ************************************* -->
<!-- **************** BODY *************** -->
<!-- ************************************* -->
<body class="home blog" style="background:url('');" >

<!--start Menu wrapper-->

<!-- ************************************* -->
<!-- **************** MENU *************** -->
<!-- ************************************* -->
<!-- **************** LAN *************** -->
<div class="lan_wrapper">
    <div id="lang_sel_list" class="lang_sel_list_horizontal">
        <ul>
            <li class="icl-es">
                <a href="<?= $lang_switch['es'] ?>" class="lang_sel_other">
                    <img  class="iclflag" src="/images/laundrop/es.png" alt="es" title="Español"/>&nbsp; Español
                </a>
            </li>
            <li class="icl-en">
                <a href="<?= $lang_switch['en'] ?>" class="lang_sel_sel">
                    <img  class="iclflag" src="/images/laundrop/en.png" alt="en" title="English"/>&nbsp; English
                </a>
            </li>
        </ul>
    </div>
</div>

<!--start Menu wrapper-->
<div class="menu_wrapper">
	<div class="container_24">
		<!-- **************** MENU *************** -->
		<div id="MainNav">
			<a href="#" class="mobile_nav closed"><span></span></a>
			<div id="menu" class="menu-menu1_en-container">
<? if ($system_lang == 'es'): ?>
                <ul id="menu-menu1_es" class="ddsmoothmenu"><li id="menu-item-24" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-24"><a href="http://laundrop.com/">Inicio</a></li>
                    <li id="menu-item-196" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-196"><a>¿Qué es Laundrop?</a>
                    <ul class="sub-menu">
                        <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://laundrop.com/como-funciona/">¿Cómo funciona?</a></li>
                        <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://laundrop.com/dudas-frecuentes/">Dudas frecuentes</a></li>
                        <li id="menu-item-169" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-169"><a href="http://laundrop.com/servicios/">Tarifas</a></li>
                    </ul>
                    </li>
                    <li id="menu-item-197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197"><a href="http://laundrop.com/solicita-una-taquilla/">Solicita una taquilla</a></li>
                    <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://laundrop.com/ubicaciones/">Ubicaciones</a></li>
                    <li id="menu-item-691" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-691"><a href="http://laundrop.com/promociones/">Promociones</a></li>
                </ul>
<? else: ?>
                <ul id="menu-menu1_en" class="ddsmoothmenu">
                    <li id="menu-item-531" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-531"><a href="http://laundrop.com/en/">Home</a></li>
                    <li id="menu-item-532" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-532"><a>What is Laundrop?</a>
                        <ul class="sub-menu">
                            <li id="menu-item-534" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-534"><a href="http://laundrop.com/en/how-to-use-laundrop/">How to use Laundrop?</a></li>
                            <li id="menu-item-535" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-535"><a href="http://laundrop.com/en/faqs/">FAQs</a></li>
                            <li id="menu-item-533" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-533"><a href="http://laundrop.com/en/247-service/">Price list</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-540" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-540"><a href="http://laundrop.com/en/request-lockers/">Request Lockers</a></li>
                    <li id="menu-item-537" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-537"><a href="http://laundrop.com/en/locations/">Locations</a></li>
                    <li id="menu-item-922" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-922"><a href="http://laundrop.com/en/promotions/">Promotions</a></li>
                </ul>
<? endif; ?>

</div>		</div>	
	</div>
	<!-- **************** TEL *************** -->
	<div class="tel_wrapper">
		<p>951 086 816</p>
	</div>
	
</div>
<!--End Menu wrapper-->
<div class="clear"></div>
<!-- ****************** fin ***************** -->
<div class="main-container">
<div class="container_24">
<div class="grid_24 bgimagecolor">
<!-- ************************************* -->
<!-- **************** HEADER *************** -->
<!-- ************************************* -->
<div class="header">
	<div class="grid_8 alpha">
		<div class="logo"> 
			<a href="http://laundrop.com/en/">
			<img src="/images/laundrop/logo.png" alt="Laundrop" /></a>
		</div>
	</div>
	<div class="grid_16 omega">
		
		<!-- ************************************* -->
		<!-- **************** INFO *************** -->
		<!-- ************************************* -->
		<div class="header-info">
			<img src="/images/laundrop/head-02.png" alt="" />
<? if ($system_lang == 'es'): ?>
            <p class="cell">&nbsp; tintorería y lavandería</p>
<? else: ?>
            <p class="cell">&nbsp; Dry Cleaning &amp; Laundry!</p>
<? endif; ?>
		</div>
	</div>
</div>
<div class="clear"></div>
<!-- ****************** fin ***************** -->
  
</div>
</div>
</div>

<div id="main_content"><?= $content ?></div>

<div class="clear"></div>
 <!--Start Footer-->
<div class="footer-wrapper">
   <div class="container_24">
      <div class="grid_24">
         <div class="footer">
            <div class="container_24">

<div class="grid_7 alpha">
<div class="widget_inner">
	  
		<div class="footer_logo">
			<img src="/images/laundrop/logo_White.png" alt="Laundrop Lavanderia y tintorería"/></a>
			<h2>951 086 816</h2>
		</div>	
	 
</div>
</div>


<div class="grid_6 alpha">
    <div class="widget_inner">
		<h4>Laundrop</h4>
        <div class="menu-menu2_en-container">
            <ul id="menu-menu2_en" class="menu">
<? if ($system_lang == 'es'): ?>
                <li id="menu-item-693" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-693"><a href="http://laundrop.com/quienes-somos/">¿Quiénes somos?</a></li>
                <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://laundrop.com/terminos-y-condiciones/">Términos y condiciones</a></li>
                <li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="http://laundrop.com/avisos-legales/">Aviso legal, condiciones generales de uso y política de privacidad</a></li>
                <li id="menu-item-493" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-493"><a href="http://laundrop.com/politica-de-cookies/">Política de cookies</a></li>
                <li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a href="http://laundrop.com/mapa-web/">Mapa web</a></li>
<? else: ?>
                <li id="menu-item-924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-924"><a href="http://laundrop.com/en/about-us/">About Us</a></li>
                <li id="menu-item-543" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-543"><a href="http://laundrop.com/en/terms-and-conditions/">Terms and Conditions</a></li>
                <li id="menu-item-544" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-544"><a href="http://laundrop.com/en/legal-terms/">Legal notice, terms of use and privacy policy</a></li>
                <li id="menu-item-541" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-541"><a href="http://laundrop.com/en/cookies-policy/">Cookies Policy</a></li>
                <li id="menu-item-542" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-542"><a href="http://laundrop.com/en/site-map/">Site map</a></li>
<? endif; ?>
            </ul>
        </div>
    </div>
</div>


<div class="grid_6 alpha">
    <div class="signup">
<? if ($system_lang == 'es'): ?>
		<h3><span class="titlebg">Newsletter</span></h3>
<? else: ?>
		<h3><span class="titlebg">Join our mailing list!</span></h3>
<? endif; ?>
        <div class="signuparea">
			<div class="signuparea-top"></div>
			<div class="signup-content">
<? if ($system_lang == 'es'): ?>
            <div class="signupinfo">Indícanos tu correo electrónico y te mantendremos informado de todas la novedades de Laundrop.</div>
<? else: ?>
                <div class="signupinfo">Enter your email and we will keep you informed of all the news from Laundrop</div>
<? endif; ?>
                <div class="signuplogin">
                    <form action="https://laundrop.com/" id="contactForm1" class="signupform" method="post">   
                        <input onfocus="if (this.value == 'e-mail') {this.value = '';}" onblur="if (this.value == '') {this.value = 'e-mail';}" name="email" value="e-mail" type="text" id="email" style="">
                        <input type="submit" value="" alt="" name="submit">
                        <input type="hidden" name="submitted" id="submitted" value="true">
                    </form>
                </div><!--signuplogin-->
            </div><!--signup-content-->
		<div class="signuparea-bottom"></div>
	</div><!--signuparea-->
</div><!--signup-->
	           

<!-- *********** End Signup ************ -->
</div>
</div>

<div class="grid_5 omega">
    <div class="widget_inner pos_redes">
<? if ($system_lang == 'es'): ?>
		<h3>¡Síguenos!</h3>
<? else: ?>
        <h3>Follow us!</h3>
<? endif; ?>
		<ul class="Social-links">
            <li><a href="https://www.facebook.com/laundropsl"><img src="/images/laundrop/Laundrop_en_facebook.png" alt="Facebook Icon" /></a></li>
            <li><a href="https://twitter.com/laundrop"><img src="/images/laundrop/Laundrop_en_twitter.png" alt="Twitter Icon" /></a></li>
        </ul>
	</div>
</div>
         </div>
      </div>
   </div>
</div>
<!--End Footer-->
<div class="clear"></div>
<div class="footer_bottom">
   <div class="container_24">
      <div class="grid_24">
         <div class="footer_bottom_inner"> 
		 <span class="blog-desc">				
                    Laundrop                    -
                    Lavandería y tintorería                </span>
            		<span class="copyright"><a href="http://www.laundrop.com" target="new">Diseño: Laundrop</a></span>
                    </div>
      </div>
   </div>
</div>



<? if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', '<?= $_SERVER["HTTP_HOST"] ?>']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<? endif; ?>

</body>
</html>
