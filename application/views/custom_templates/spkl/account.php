<?php
/**
 * Spkl Business Account Template
 */
header('Content-Type: text/html;charset=utf-8');
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', '<?php echo $ga?>']);
          _gaq.push(['_setDomainName', 'locker.spkl.com']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        </script>
        <?php endif; ?>
        <script src="/js/spkl/labox.reset.js"></script>
        <script src="/js/spkl/function.js"></script>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
        
        <style>
        #header{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
            border-bottom:1px solid #73a5bc;
            margin-bottom:20px;
        }
        #inner-header{
            text-align:left;
            width:970px;
            margin:0 auto;
        }
        
        #footer{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
        }
        #inner-footer{
            text-align:left;
            width:970px;
            margin:0 auto;
        }
        </style>
        <link href="/css/spkl/custom.css" rel="stylesheet">
    </head>
    <body>
    
        <div id='header'>
            <div id="inner-header">
                <h2><?php echo $business->companyName ?></h2>
            </div>
        </div>
    
        <div style='min-height:600px;'>
        <?php 
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content 
        ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>
        
        <div>
         <div id='footer'>
            <div id='inner-footer'>
                &nbsp;
            </div>
        </div>
        
        <div style="text-align: right; padding:5px;">
            <a href="https://droplocker.com" style="display: inline-block; padding: 5px;"><img style="width:189px" align="center" src="https://droplocker.com/images/logo-powered.jpg"></a>
        </div>
        </div>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
            $('.dropdown-toggle').dropdown();
        </script>
    </body>
    
   
</html>
