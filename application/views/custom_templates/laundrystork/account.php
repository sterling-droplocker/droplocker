<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'laundrystork.com']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
          
    	</script>
    	<?php endif; ?>
        
        <script type="text/javascript">
             $(function(){
                $('#site_menu img').mouseover(function(e){
                    swapMenuImage(this);
                });
                $('#site_menu img').mouseout(function(e){
                    swapMenuImage(this);
                });
             });
  
             //swap the images
             var swapMenuImage = function( el ){
                    var current = $(el).attr('src');
                    $(el).attr('src', $(el).attr('data-swap-image') );
                    $(el).attr('data-swap-image',current );
             };
        </script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
        
        <style>
        #header{
            width:100%;
            text-align:center;
            background-color:#fff;
            border-bottom:1px solid #73a5bc;
            margin-bottom:20px;
        }
        #inner-header{
/*            text-align:left;
            width:970px;
            margin:0 auto;*/
        }
        
        #footer{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
        }
        #inner-footer{
            text-align:left;
            width:970px;
            margin:0 auto;
        }
        #site_menu img{
            padding-right:0px;
            margin-right: 0px;
        }
        </style>
       
    </head>
    <body>
    
        <div id='header' class="container  span12">
            <div id="inner-header" class="row-fluid span11" style="text-align:center;">
                <div class="span4">
                    <img src="/images/laundrystork/logo.gif" alt="<?php echo $business->companyName ?>" />
                </div>
                <div class="span7" id="site_menu" style="margin-top:75px;">
                    <a href="http://www.laundrystork.com/default.aspx"><img src="/images/laundrystork/home.gif" data-swap-image="/images/laundrystork/home_hover.gif" alt="Home" title="Home" /></a>
                    <a href="http://www.laundrystork.com/our-story.aspx"><img src="/images/laundrystork/about_us.gif" data-swap-image="/images/laundrystork/about_us_hover.gif"  title="Our Story" alt="Our Story"   border="0" /></a>
                    <a href="http://www.laundrystork.com/environment.aspx"><img src="/images/laundrystork/our_environment.gif" data-swap-image="/images/laundrystork/our_environment_hover.gif"  alt="Environment"  title="Environment" /></a>
                    <a href="http://www.laundrystork.com/how-it-works.aspx"><img src="/images/laundrystork/how_it_works.gif" data-swap-image="/images/laundrystork/how_it_works_hover.gif" alt="Services"  title="How It Works" /></a>
                    <a href="http://www.laundrystork.com/rates.aspx"><img src="/images/laundrystork/new_rates.gif"  data-swap-image="/images/laundrystork/new_rates_hover.gif"  title="Prices"  alt="Prices" /></a>
                    <a href="http://www.laundrystork.com/get-storked.aspx"><img src="/images/laundrystork/rewards.gif"  data-swap-image="/images/laundrystork/rewards_hover.gif" alt="Get Storked" title="Get Storked" /></a>
                    <a href="http://www.laundrystork.com/faq.aspx"><img src="/images/laundrystork/faq.gif"  data-swap-image="/images/laundrystork/faq_hover.gif" alt="FAQ" title="FAQ" /></a>
                </div>
            </div>
        </div>
        
        <div style='min-height:600px;'>
        <?php 
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content 
        ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>
        
        <div>
         <div id='footer'>
            <div id='inner-footer'>
                &nbsp;
            </div>
        </div>
        
        <div style="text-align: right; padding:5px;">
            <a href="https://droplocker.com" style="display: inline-block; padding: 5px;"><img style="width:189px" align="center" src="/images/logo-powered.jpg"></a>
        </div>
        </div>

        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>
    </body>
</html>
