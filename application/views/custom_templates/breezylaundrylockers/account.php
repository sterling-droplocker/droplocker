<!DOCTYPE html>
<html class="" lang="en" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title ?> | Breezy Laundry Lockers</title>

<!-- General -->
<meta name="keywords"             content="Breezy Laundry Lockers, Dry Cleaning, Corporate Dry Cleaning, Laundry Lockers, ">
<meta name="description"          content="Making your life easier with exclusive access to 24/7 amenities, at your doorstep.">
<link rel="shortcut icon"         href="/images/breezylaundrylockers/logo.png">
<link rel="image_src"             href="/images/breezylaundrylockers/logo.png">

<!-- Facebook -->
<meta property="og:image"         content="/images/breezylaundrylockers/logo.png"/>
<meta property="og:url"           content="//breezylaundrylockers.com.au"/>
<meta property="og:title"         content="Breezy Laundry Lockers | Make life easy"/>
<meta property="og:description"   content="Making your life easier with exclusive access to 24/7 amenities, at your doorstep."/>
<meta property="fb:admins"        content="Your Facebook user ID"/>
<meta property="og:type"          content="website" />

<!-- Twitter -->
<meta name="twitter:card"         content="Breezy Laundry Lockers | Make life easy">
<meta name="twitter:site"         content="Twitter handle">
<meta name="twitter:title"        content="Breezy Laundry Lockers | Make life easy">
<meta name="twitter:description"  content="Making your life easier with exclusive access to 24/7 amenities, at your doorstep.">
<meta name="twitter:creator"      content="">
<meta name="twitter:image:src"    content="/images/breezylaundrylockers/logo.png">
<meta name="twitter:domain"       content="//breezylaundrylockers.com.au">

<!-- Google+ -->
<meta itemprop="name"             content="Breezy Laundry Lockers | Make life easy">
<meta itemprop="description"      content="Making your life easier with exclusive access to 24/7 amenities, at your doorstep.">
<meta itemprop="image"            content="//breezylaundrylockers.com.au/logo.png">

<!-- supports being stored on home screen (Android) -->
<meta name="mobile-web-app-capable" content="yes" />

<!-- supports being stored on home screen (Apple) -->
<meta name="apple-mobile-web-app-capable" content="yes" />

<!-- determining veiwport size for responsive design -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- changes the color of the status bar (Apple) -->
<meta name="apple-mobile-web-app-status-bar-style" content="black" />

<!-- enables format detection of phone numbers -->
<meta name="format-detection" content="telephone=no" />


<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<link href="//fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />

<link href="/css/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="/css/breezylaundrylockers/flat-ui.css" rel="stylesheet" />
<link href="/css/breezylaundrylockers/animations.css" rel="stylesheet" />
<link href="/css/breezylaundrylockers/ui-kit/ui-kit-header/css/style.css" rel="stylesheet" />
<link href="/css/breezylaundrylockers/ui-kit/ui-kit-content/css/style.css" rel="stylesheet" />
<link href="/css/breezylaundrylockers/ui-kit/ui-kit-blog/css/style.css" rel="stylesheet" />
<link href="/css/breezylaundrylockers/ui-kit/ui-kit-price/css/style.css" rel="stylesheet" />
<!-- <link href="/css/breezylaundrylockers/ui-kit/ui-kit-footer/css/style.css" rel="stylesheet" /> -->

<link href="/css/breezylaundrylockers/application.css" rel="stylesheet" />

<script type='text/javascript' src='/js/breezylaundrylockers/modernizr.js'></script>
<script src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>
<script src='/js/breezylaundrylockers/scripts.js'></script>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>

<style type="text/css">
#content {
    padding-bottom: 50px;
    margin-top: 20px;
}

@media screen and (max-width: 767px) {
#content {
    padding-bottom: 0;
}
}

</style>

</head>

<body>

<section class="contact">
    <div class="container">
        <div class="row">
            <div class="span3"><i class="fa fa-phone"></i> Customer Service: 0467 152 823</div>
            <div class="span3"><i class="fa fa-clock-o"></i> Mon. - Fri: 9am - 5pm</div>
        </div>
    </div>
</section>

<header class="header-10">
    <div class="container">
        <div class="row">

            <div class="navbar">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#main_menu.nav-collapse"></button>

                    <a class="brand" href="/"><img alt="Brand" src="/images/breezylaundrylockers/brand-e5345d12be26316e5cf4607c2776ef85.png"></a>
                    <a class="brand-mobile" href="/"><img alt="Brand mobile" src="/images/breezylaundrylockers/brand_mobile-9f49844bf11975151397f7ab9aa1eaad.png"></a>

                    <div class="nav-collapse navbar-collapse collapse" id="main_menu">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#main_menu.nav-collapse"></button>
                        <ul class="nav pull-left">
                            <li class="hidden-lg"><a href="/">Home</a></li>
                            <li><a href="http://www.breezylaundrylockers.com.au/how">How It Works</a></li>
                            <li><a href="http://www.breezylaundrylockers.com.au/pricing">Pricing</a></li>
                            <li><a href="http://www.breezylaundrylockers.com.au/owner">Owner's Corp</a></li>
                        </ul>        
                        <form class="navbar-form pull-right">
                            <a class="btn btn-success" href="mailto:contact@breezylaundrylockers.com.au">Contact</a>
                        </form>
                    </div>

            </div>
        </div>
    </div>
</header>

<section role="main" id="content">
    <?= $content; ?>
</section>

<footer>
    <div class="container">
        <p>Site by<strong><a href="//capsuledigital.com.au"> Capsule Digital</a></strong></p>
    </div>
</footer>

<script>
$('.navbar-toggle').click(function() {
    $('#main_menu').toggle();
});
</script>

<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
