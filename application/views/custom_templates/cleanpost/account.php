<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="/images/cleanpost/favicon.png"/>

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="/css/cleanpost/font-def.css">
        <link rel="stylesheet" type="text/css" href="/css/cleanpost/custom-styles.css">

        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>

        <?php if (ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo $ga ?>']);
                _gaq.push(['_setDomainName', 'dashlocker.com']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
        <?php endif; ?>
        <?= $javascript ?>
        <?= $_scripts ?>
        <?= $style ?>
        <?= $_styles ?>

        <style>
            #header{
                width:100%;
                text-align:center;
                background-color:#d9edf7;
                border-bottom:1px solid #73a5bc;
                margin-bottom:20px;
            }
            #inner-header{
                text-align:left;
                width:970px;
                margin:0 auto;
            }

            #footer{
                width:100%;
                text-align:center;
                background-color:#d9edf7;
            }
            #inner-footer{
                text-align:left;
                width:970px;
                margin:0 auto;
            }
        </style>

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3QpLkgcbXYnMBce7hDUwCZbXWzwBmfDM";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

    </head>
    <body>

        <div  id="customer-menu" class="span10 center">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="http://cleanpost.com.mx/">
                        <img src="/images/cleanpost/logo.png" />
                    </a>

                    <div id="main_menu" class="nav-collapse">
                        <ul class="nav">
                            <li><a href="http://cleanpost.com.mx/home.html#ourservice"><span>Servicios</span></a></li>
                            <li><a href="http://cleanpost.com.mx/home.html#howitworks"><span>¿Cómo funciona?</span></a></li>
                            <li><a href="http://cleanpost.com.mx/testimonials.html"><span>Testimonios</span></a></li>
                            <li><a href="http://cleanpost.com.mx/places.html"><span>Ubicaciones</span></a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>

        </div>
                
        <div class="content-wrapper" style='min-height:600px;'>
            <?php
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content
            ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer ?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName ?> <?php echo date('Y') ?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>

<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-3 col-md-4">

        <div class="bdr bdr1">
          <h1 class="ftr_hdng">CleanPost</h1>
          <ul class="menu_ftr tmar2">
            <li><a href="http://cleanpost.com.mx/about_us.html">Acerca de</a></li>
            <li><a href="http://cleanpost.com.mx/testimonials.html">Testimonios</a></li>
            <li><a href="http://cleanpost.com.mx/contact_us.html">Servicio al Cliente</a></li>
            <li><a href="http://cleanpost.com.mx/price_list.html">Lista de Precios</a></li>
            <li><a href="http://cleanpost.com.mx/rewards.html">Programa CleanPoints</a></li>
            <li><a href="http://cleanpost.com.mx/places.html">Ubicaciones</a></li>


            <div class="clear"></div>
          </ul>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="col-lg-2 col-sm-3 col-md-2">
        <div class="bdr">
          <h1 class="ftr_hdng">Información</h1>

          <ul class="menu_ftr tmar2">
            <li><a href="http://cleanpost.com.mx/property_manager.html">Pide CleanPost</a></li>
            <li><a href="http://cleanpost.com.mx/press.html">Prensa</a></li>
            <li><a href="http://cleanpost.com.mx/faq.html">Preguntas Frecuentes</a></li>
            <li><a href="http://cleanpost.com.mx/contact_us.html">Contacto</a></li>
            <li><a href="http://cleanpost.com.mx/price_list.html">Paquetes de lavandería</a></li>
          </ul>

          <div class="clear"></div>
        </div>
      </div>
      <div class="col-lg-2 col-sm-3 col-md-2">
        <div class="bdr">
          <h1 class="ftr_hdng">Legal</h1>

          <ul class="menu_ftr tmar2">
              <li><a href="http://cleanpost.com.mx/terms.html">Términos y Condiciones</a></li>
              <li><a href="http://cleanpost.com.mx/privacy.html">Aviso de Privacidad</a></li>
          </ul>
       

          <div class="clear"></div>
        </div>
      </div>
      <div class="col-lg-4 col-sm-3 col-md-4">
        <h1 class="ftr_hdng">Síguenos en:</h1>
        <a href="https://www.facebook.com/CleanPost/"><img src="/images/cleanpost/fb.png" alt="facebook" class="sociall"></a> 
        <a href="https://twitter.com/Clean_Post"><img src="/images/cleanpost/twr.png" alt="twitter" class="sociall"></a> 
        <a href="https://itunes.apple.com/es/app/cleanpost/id1049220342?mt=8"><img src="/images/cleanpost/yt.png" alt="youtube" class="sociall"></a> 
        <br>
        <br>
        <a href="https://itunes.apple.com/es/app/cleanpost/id1049220342?mt=8"><img src="/images/cleanpost/appstore.png" alt="youtube" class="sociall"></a>
        <br>
        <br>
        <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.cleanPost"><img src="/images/cleanpost/androidstore.png" alt="youtube" class="sociall"></a> 
        </div>
        
                
      </div>

    </div>
  </div>
        
        
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
            $('.dropdown-toggle').dropdown();
        </script>
    </body>


</html>
