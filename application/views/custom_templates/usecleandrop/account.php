
<!DOCTYPE html>
<!-- Microdata markup added by Google Structured Data Markup Helper. -->
<html lang="en-us">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>How It Works | CleanDrop Laundry</title>
        <meta name="description" content="CleanDrop&#039;s laundry service makes dry cleaning painless. Simply drop-of your laundry in our laundry locker, text us, and we&#039;ll do the rest!" />
        <!-- non html5 compliant --->
        <meta name="title" content="How It Works | CleanDrop Laundry" />
        <meta name="keyword" content="Laundry, Dry Cleaning" />
        <!-- ---->
        <meta name="robots" content="index, follow" />
        <meta name="google" content="notranslate" />
        <meta name="copyright" content="Copyright (c) 2014 by CleanDrop Laundry">

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-language" content="en" />

        <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
        <meta http-equiv="cache-control" content="post-check=0, pre-check=0, false" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content="-1" />
        <meta http-equiv="imagetoolbar" content="no"/>

        <!-- page specific meta info --->
        
        <link rel="shortcut icon" href="/images/usecleandrop/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/images/usecleandrop/favicon.ico" type="image/x-icon">

        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="How It Works | CleanDrop Laundry" />
        <meta property="og:description" content="CleanDrop&#039;s laundry service in San Jose, California makes dry cleaning painless. Simply drop-of your laundry in our laundry locker, text us, and we&#039;ll do the rest!" />

        <meta property="og:site_name" content="CleanDrop Laundry" />
        <meta property="article:published_time" content="2013-10-21T14:47:20+00:00" />
        <meta property="article:modified_time" content="2014-10-16T14:35:59+00:00" />



        <!-- Bootstrap -->
        <link href='//fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link href="/css/usecleandrop/styles.css" rel="stylesheet">
        <?= $style ?>
        <?= $_styles ?>
        <style>
            #main_content .span12 .offset1 {
                margin-left: 0;
            }

            #main_content {
                margin-top: 50px;
            }

            header.row {
                margin: 0 auto;
            }

            .btn {
                font-family: 'Myriad Pro', Arial;
                /*background-color: #f99b1c;*/
                color: #fff;
                /*padding: 5px 8px 2px 8px;*/
                display: inline-block;
                border-radius: 10px;
                font-weight: bold;
                font-size: 16px;
                margin-top: 20px;
                margin-bottom: 10px;
                border: none;
                text-shadow: none;
                background-image: none;
            }

            .btn:hover {
                text-shadow: none;
            }

            .btn-primary, .btn-success {
                background-color: #f99b1c;
                background-image: none;
            }

            .btn-primary:hover, .btn-success:hover {
                /*background-color: #E69322;*/
                background-color:#00A7E1; 
                background-image: none;
                text-shadow: none;
            }

            .btn-large [class^="icon-"], .btn-medium [class^="icon-"], .btn-small [class^="icon-"] {
                margin-top: 1px;
            }

            div.control-group.kiosk-access-buttons {
                margin-left: 25px;
            }

            #default_account_nav ul li, .breadcrumb li {
                font-size: 15px;
            }

            body {
                padding-right: 0;
                padding-left: 0;
            }

            header, #main_content {
                padding-left: 20px;
                padding-right: 20px;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!--<script src="//use.typekit.net/plu4fpw.js"></script>-->
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <script>
            try {
                Typekit.load();
            } catch (e) {

            }
        </script>

    </head>
    <body>
        <div class="container">
            <header class="row">
                <div class="span12 offset1">
                <div class="h-logo">
                    <a href="/">
                        <img src="/images/usecleandrop/cleandrop-full.png" width="182" height="96" class="visible-phone" />
                        <img src="/images/usecleandrop/cleandrop-full.svg" width="250" height="120" class="hidden-phone" />
                    </a>
                </div>
                <div class="h-nav">
                    <div class="service hidden-phone">
                        <img src="/images/usecleandrop/laundry-and-cleaners.png" width="329" height="15" />
                    </div>
                    <div class="hr"></div>

                    <ul class="main-nav">
                        <li class="active"><a href="http://usecleandrop.com">HOME</a></li>
                        <li><a href="http://usecleandrop.com/how-it-works#steps">HOW IT WORKS</a></li>
                        <li><a href="http://usecleandrop.com/pricing">PRICING</a></li>
                        <li><a href="http://usecleandrop.com/faq">FAQ</a></li>
                        <li><a href="http://usecleandrop.com/contact">CONTACT</a></li>
                    </ul>

                </div>
                </div>
            </header>
            <div class="dotted-bottom" style='margin-top: 50px;'>
                <?= $content; ?>
            </div>
            <footer class="row">
                <div class="span-12 offset1">
                    <div class="footer">
                        <a href="#">Copyright &copy; <span itemprop="name">CleanDrop Laundry</span>.</a>
                        <a href="#">All reserved.</a>
                        <a href="#">Terms of Use Updated Privacy Policy</a>
                    </div>
                </div>
            </footer>
        </div>

        <!--<script src="js/jquery.1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/respond.min.js"></script>-->

    </body>
</html>


