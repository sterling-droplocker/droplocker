<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Nothing is more convenient than DropnLock!</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/css/dropnlock/site.css">
    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
</head>
<body>
    <header class="clear">
         <section class="jumbotron">
            <div class="container header">
                <div class="shape_3"><p style="line-height:48px;text-align:center;margin-top:0px;margin-bottom:0px;" class="Style1">OPEN 24•7</p><p style="line-height:24px;text-align:center;margin-bottom:6px;margin-top:0px;" class="Style2">FOR YOUR CONVENIENCE</p><p style="line-height:36px;text-align:center;margin-bottom:1.5px;margin-top:0px;" class="Style3">516-804-2121</p><p style="line-height:24px;text-align:center;margin-bottom:1.5px;margin-top:0px;" class="Style4"> </p></div>
                <div class="shape_4"><p style="line-height:12px;text-align:center;margin-top:0px;margin-bottom:5.25px;" class="Style5">NOW OFFERING FREE HOME</p><p style="line-height:12px;text-align:center;margin-bottom:5.25px;margin-top:0px;" class="Style5">PICKUP AND DELIVERY</p></div>                
            </div>
            
            <div class="shape_5"><p style="line-height:18px;text-align:center;margin-top:0px;margin-bottom:5.25px;" class="Style6">3252 RAILROAD AVENUE ● WANTAGH, NY 11793</p><p style="line-height:18px;text-align:center;margin-bottom:5.25px;margin-top:0px;" class="Style6">at the LIRR Wantagh Station</p><p style="line-height:15px;text-align:center;margin-bottom:5.25px;margin-top:0px;" class="Style7">Also located inside the <span class="Style8">World Gym</span><span class="Style9">, Park Avenue, Wantagh </span></p></div>

            <div class="logo">
                <img src="/images/dropnlock/logo.png">
            </div>
        </section>


    </header>
    <section class="main container clear">
        <div class="row">
        <br/><br/>
                <?php echo $content; ?>
        </div>
    </section>
    <br/><br/><br/>
    <footer class="clear">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <p style="text-align: center;" class="text">
                            <div class="shape_6">
                                <p class="Style10">you are at<span class="Style11"> www.DropnLock.net </span>
                                <span class="Style12">Drycleaning &amp; Laundry Service</span>
                                </p>
                                <p class="Style13">©2016 www.dropnlock.com All Rights Reserved. Site designed and maintained by Siteenstein.com</p>
                            </div>
                    </p>
                </div>
            </div>
        </div>


    </footer>
</body>
</html>