<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/journal/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lora:400,400italic|Muli:normal,300,300italic,400,400italic|Playfair+Display:normalitalic"/>
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'pinglocker.com']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts;?>
        <?= $style?>
        <?= $_styles ?>
        
        <style>
        #header{
            background-color:#000;
            font-style: normal;
            font-weight: 300;
            letter-spacing: normal;
            line-height: 32.390625px; 
        }
        #inner-header{
            padding-top:12px;
            padding-bottom:12px;
            color:#cfcfcf;
            font-family: Muli, Helvetica, Arial, sans-serif;
            font-size: 14px;
            font-style: normal;
            font-weight: 300;
            padding-right:30px;
        }
        
        #inner-header span{
            padding-right:17px;
            padding-left:17px;
        }
        
        #inner-header span a{
            color: #cfcfcf;
            font-size: 14px;
        }
        
        #footer{
            width:100%;
            text-align:center;
            background-color:#fff;
            border-top: solid 1px #e6e6e6;
            text-align:center;
            font-family: Muli, Helvetica, Arial, sans-serif;
        }
        #inner-footer{
            text-align:left;
            font-family: Muli, Helvetica, Arial, sans-serif;
            color:#777777;
            width:970px;
            margin:0 auto;
            text-align:center;
        }
        a:link {text-decoration:none;}
        a:visited {text-decoration:none;}
        a:hover {text-decoration:none;color:#f09133 !important;}
        a:active {text-decoration:none;}
        
        h1{
            font-size:1.8em;
        }
        </style>
       
    </head>
    <body>
    
        <div id='header' class="row">
            <a href="http://pressboxatlanta.com"><img src="/images/pressatlanta/pressbox_logo.png" style="max-height:40px;margin-left:50px;margin-top:10px;" /></a>
            <div id="inner-header" class="pull-right">
                <span><a href="http://pressboxatlanta.com">HOME</a></span> / 
                <span><a href="http://pressboxatlanta.com/contact">CONTACT</a></span> / 
                <span><a href="http://myaccount.pressboxatlanta.com/login">LOGIN</a></span> / 
                <span><a href="http://myaccount.pressboxatlanta.com/register">CREATE ACCOUNT</a></span>
            </div>
        </div>
    
        <div style='min-height:600px;color:#000;margin-top:20px;'>
        <?php 
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content 
        ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>
        
        <div>
         <div id='footer'>
            <div id='inner-footer'>
               PRESSBOX COPYRIGHT 2013
            </div>
        </div>
        
        <div style="text-align: right; padding:5px;">
         	<a href="https://droplocker.com" style="display: inline-block; padding: 5px;"><img style="width:189px" align="center" src="https://droplocker.com/images/logo-powered.jpg"></a>
        </div>
        </div>

        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>
    </body>
</html>
