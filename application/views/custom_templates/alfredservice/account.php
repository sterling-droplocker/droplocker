<?php

$base_url = 'http://alfredservice.com';

?><!DOCTYPE html>
<html class="no-js"> 
<head>
	<meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Alfred</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=1024">

  <link rel="shortcut icon" href="/images/alfredservice/favicon.ico">
  <link rel="apple-touch-icon" href="/images/alfredservice/apple-touch-icon-144x144-precomposed.png">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
  <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
  <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        
  <link rel="stylesheet" href="/css/alfredservice/ladda.min.css">
  <link rel="stylesheet" href="/css/alfredservice/main.css?v=3">
  

  <script src="/js/alfredservice/vendor/modernizr-2.6.2.min.js"></script>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
  <script src='/js/functions.js'></script>
  <script src='/bootstrap/js/bootstrap.min.js'></script>
  <?= $_scripts ?>
  <?= $javascript?>
  <?= $style?>
  <?= $_styles ?>

</head>


<body>
  <div class="pageWrap">
   <header>

   	<div class="center">
   		<a href="<?php echo $base_url ?>/"><div class="logo left"></div></a>
 		</div><!-- center -->

    <nav>
      <div class="center">
        <div class="menu-main-container"><ul id="menu-main" class="menu"><li id="menu-item-26" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-26"><a href="<?php echo $base_url ?>/services/">Services</a>
<ul class="sub-menu">
	<li id="menu-item-33" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-33"><a href="<?php echo $base_url ?>/#things">Things Alfred Does</a></li>
	<li id="menu-item-34" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34"><a href="<?php echo $base_url ?>/#whyalfred">Why Alfred</a></li>
	<li id="menu-item-35" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-35"><a href="<?php echo $base_url ?>/#pricing">Pricing</a></li>
</ul>
</li>
<li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-27"><a href="<?php echo $base_url ?>/how/">How it Works</a>
<ul class="sub-menu">
	<li id="menu-item-36" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-36"><a href="<?php echo $base_url ?>/#works">The Way It Works</a></li>
	<li id="menu-item-37" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-37"><a href="<?php echo $base_url ?>/#customize">Customize</a></li>
	<li id="menu-item-38" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-38"><a href="<?php echo $base_url ?>/#concierge">Concierge</a></li>
</ul>
</li>
<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="<?php echo $base_url ?>/locations/">Locations</a></li>
<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="<?php echo $base_url ?>/faq-page/">FAQ</a></li>
<li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="<?php echo $base_url ?>/blog/">Blog</a></li>

</ul></div>
      </div><!-- center -->
    </nav><!-- nav -->


      

    
   </header><!-- header -->

            <div></div>
            <div class="post-2 page type-page status-publish hentry section" style='min-height:600px; padding-top:20px;'>
            <?php 
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content 
            ?>
            </div>
    
    
            <div class='container-fluid hidden-phone' id="footer">
                <?php echo $footer?>
            </div>
        </div>

	<footer class="">
		<div class="secFooter">
			<div class="center">

				<div class="priFooter">
					<div class="divider left tres">
						<a href="mailto:support@alfredservice.com">Ask <br>Alfred</a>
					</div>
					<div class="divider left tres">
						<a href="https://itunes.apple.com/ca/app/alfred-service/id799021455?mt=8" target="_blank">The <br>Alfred App</a>
					</div>
					<div class="divider left tres">
						<a href="javascript:requestServe();">Get Alfred in <br>Your Building</a>
					</div>
				</div><!-- priFooter -->

				<div class="divider left tres quattro">
					<a href="http://alfredservice.com/team/">Team</a>
				</div>

					<div class="social">
						<div class="socialHolder">
							<div class="divider left">
								<a target="_blank" class="ffsoc" href="https://twitter.com/alfredservice"><div class="svgoff twitter"></div><div class="twitter left"></div></a><!-- twitter -->
								<div class="smallSpacer left">
									<div class="howLong "></div>
									<div class="twitterdiv "></div><!-- twitterdiv -->
								</div>
							</div><!-- divider -->
							<div class="divider left">
								<a target="_blank" class="ffsoc" href="http://instagram.com/alfredservice"><div class="svgoff instagram"></div><div class="instagram left"></div></a>
								<a target="_blank" class="ffsoc" href="https://www.facebook.com/alfredservice"><div class="svgoff facebook"></div><div class="facebook left"></div></a><!-- facebook -->
							</div><!-- divider -->
						</div><!-- socialHolder -->
					</div><!-- social -->

	      <p class="copyright">&copy; 2014 Alfred Inc. All rights reserved. | <a href="http://alfredservice.com/wp-content/themes/alfredservice/Alfred_TOS.pdf" target="_blank">Terms &amp; Conditions</a></p>
	    </div><!-- center -->
		</div><!-- secFooter -->
		
			</footer><!-- footer -->

<script> var ajaxUrl = "/js/alfredservice"; </script>
<script> var ftbAmount = "5"; </script>
<!--
<script src="/js/alfredservice/vendor/jquery.transit.min.js"></script>
<script src="/js/alfredservice/vendor/jquery.validate.min.js"></script>
-->

<script src="/js/alfredservice/jquery.forthebirds.min.js"></script>
<script>
  //-- ftb twitter plugin
  $('.twitterdiv').forthebirds({
    loadtext: 'Loading Tweets...',
    transition: 'fade',
    timer: '5000',
    date: 'true',
    datelocation: 'howLong',
    fetch: ftbAmount,
    count: '1',
    avatar: 'false',
    avatarlocation: 'false',
    user: 'alfredservice'
  });

</script>

<!-- <script type="text/javascript" src="/js/alfredservice/infobox.js"></script> -->
<!-- <script src="/js/alfredservice/plugins.min.js"></script> -->
<!-- <script src="/js/alfredservice/main.min.js"></script> -->



<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?php echo $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
