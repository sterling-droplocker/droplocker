<!DOCTYPE html>
<html class="html" lang="en-GB" itemscope itemtype="http://schema.org/WebPage">
    <head>
        <meta charset="UTF-8">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <title>Drop N Lockers &#8211; Locker Services &#8211; Laundry &amp; Dry Cleaning</title>
        <meta name='robots' content='noindex,follow' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <link rel='dns-prefetch' href='//s.w.org' />



        <link rel='stylesheet' id='essential_addons_elementor-css-css'  href='/css/dropnlockerz/essential-addons-elementor.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='essential_addons_elementor-slick-css-css'  href='/css/dropnlockerz/slick.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='oceanwp-style-css'  href='/css/dropnlockerz/style.min.css?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' id='child-style-css'  href='/css/dropnlockerz/style.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='font-awesome-css'  href='/css/dropnlockerz/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
        <link rel='stylesheet' id='simple-line-icons-css'  href='/css/dropnlockerz/simple-line-icons.min.css?ver=2.4.0' type='text/css' media='all' />
        <link rel='stylesheet' id='magnific-popup-css'  href='/css/dropnlockerz/magnific-popup.min.css?ver=1.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='oceanwp-hamburgers-css'  href='/css/dropnlockerz/hamburgers.min.css?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' id='oceanwp-3dx-r-css'  href='/css/dropnlockerz/3dx-r.css?ver=1.0' type='text/css' media='all' />

        <link rel='stylesheet' id='elementor-frontend-css'  href='/css/dropnlockerz/frontend.min.css?ver=2.1.3' type='text/css' media='all' />
        <link rel='stylesheet' id='elementor-post-44-css'  href='/css/dropnlockerz/post-44.css?ver=1533544099' type='text/css' media='all' />
        <link rel='stylesheet' id='oceanwp-google-font-raleway-css'  href='//fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900%2C100i%2C200i%2C300i%2C400i%2C500i%2C600i%2C700i%2C800i%2C900i&#038;subset=latin&#038;ver=4.9.8' type='text/css' media='all' />

        <link rel='stylesheet' id='elementor-icons-css'  href='/css/dropnlockerz/elementor-icons.min.css?ver=3.6.0' type='text/css' media='all' />
        <link rel='stylesheet' id='elementor-animations-css'  href='/css/dropnlockerz/animations.min.css?ver=2.1.3' type='text/css' media='all' />
        <link rel='stylesheet' id='elementor-global-css'  href='/css/dropnlockerz/global.css?ver=1531746611' type='text/css' media='all' />
        <link rel='stylesheet' id='elementor-post-13-css'  href='/css/dropnlockerz/post-13.css?ver=1533544463' type='text/css' media='all' />
        <link rel='stylesheet' id='oe-widgets-style-css'  href='/css/dropnlockerz/widgets.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7COpen+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=4.9.8' type='text/css' media='all' />

        <style type="text/css" media="screen">
            html { margin-top: 32px !important; }
            * html body { margin-top: 32px !important; }
            @media screen and ( max-width: 782px ) {
            html { margin-top: 46px !important; }
            * html body { margin-top: 46px !important; }
            }
        </style>


        <!-- OceanWP CSS -->
        <style type="text/css">
            /* General CSS */a:hover,a.light:hover,.theme-heading .text::before,#top-bar-content >a:hover,#top-bar-social li.oceanwp-email a:hover,#site-navigation-wrap .dropdown-menu >li >a:hover,#site-header.medium-header #medium-searchform button:hover,.oceanwp-mobile-menu-icon a:hover,.blog-entry.post .blog-entry-header .entry-title a:hover,.blog-entry.post .blog-entry-readmore a:hover,.blog-entry.thumbnail-entry .blog-entry-category a,ul.meta li a:hover,.dropcap,.single nav.post-navigation .nav-links .title,body .related-post-title a:hover,body #wp-calendar caption,body .contact-info-widget.default i,body .contact-info-widget.big-icons i,body .custom-links-widget .oceanwp-custom-links li a:hover,body .custom-links-widget .oceanwp-custom-links li a:hover:before,body .posts-thumbnails-widget li a:hover,body .social-widget li.oceanwp-email a:hover,.comment-author .comment-meta .comment-reply-link,#respond #cancel-comment-reply-link:hover,#footer-widgets .footer-box a:hover,#footer-bottom a:hover,#footer-bottom #footer-bottom-menu a:hover,.sidr a:hover,.sidr-class-dropdown-toggle:hover,.sidr-class-menu-item-has-children.active >a,.sidr-class-menu-item-has-children.active >a >.sidr-class-dropdown-toggle,input[type=checkbox]:checked:before{color:#006ab6}input[type="button"],input[type="reset"],input[type="submit"],button[type="submit"],.button,#site-navigation-wrap .dropdown-menu >li.btn >a >span,.thumbnail:hover i,.post-quote-content,.omw-modal .omw-close-modal,body .contact-info-widget.big-icons li:hover i{background-color:#006ab6}.widget-title{border-color:#006ab6}blockquote{border-color:#006ab6}#searchform-dropdown{border-color:#006ab6}.dropdown-menu .sub-menu{border-color:#006ab6}.blog-entry.large-entry .blog-entry-readmore a:hover{border-color:#006ab6}.oceanwp-newsletter-form-wrap input[type="email"]:focus{border-color:#006ab6}.social-widget li.oceanwp-email a:hover{border-color:#006ab6}#respond #cancel-comment-reply-link:hover{border-color:#006ab6}body .contact-info-widget.big-icons li:hover i{border-color:#006ab6}#footer-widgets .oceanwp-newsletter-form-wrap input[type="email"]:focus{border-color:#006ab6}input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,button[type="submit"]:hover,input[type="button"]:focus,input[type="reset"]:focus,input[type="submit"]:focus,button[type="submit"]:focus,.button:hover,#site-navigation-wrap .dropdown-menu >li.btn >a:hover >span,.post-quote-author,.omw-modal .omw-close-modal:hover{background-color:#005ec4}a:hover{color:#006ab6}.page-header,.has-transparent-header .page-header{padding:30px 0 30px 0}.page-header .page-header-title,.page-header.background-image-page-header .page-header-title{color:#333333}.site-breadcrumbs,.background-image-page-header .site-breadcrumbs{color:#333333}.site-breadcrumbs ul li:after{color:#333333}.site-breadcrumbs a,.background-image-page-header .site-breadcrumbs a{color:#006ab6}.site-breadcrumbs a:hover,.background-image-page-header .site-breadcrumbs a:hover{color:#006ab6}/* Header CSS */#site-logo #site-logo-inner,.oceanwp-social-menu .social-menu-inner,#site-header.full_screen-header .menu-bar-inner,.after-header-content .after-header-content-inner{height:123px}#site-navigation-wrap .dropdown-menu >li >a,.oceanwp-mobile-menu-icon a,.after-header-content-inner >a{line-height:123px}#site-header-inner{padding:20px 0 30px 0}#site-header.has-header-media .overlay-header-media{background-color:rgba(0,0,0,0.5)}#site-logo #site-logo-inner a img,#site-header.center-header #site-navigation-wrap .middle-site-logo a img{max-width:121px}#site-header #site-logo #site-logo-inner a img,#site-header.center-header #site-navigation-wrap .middle-site-logo a img{max-height:137px}#site-navigation-wrap .dropdown-menu >li >a,.oceanwp-mobile-menu-icon a,#searchform-header-replace-close{color:#282828}#site-navigation-wrap .dropdown-menu >li >a:hover,.oceanwp-mobile-menu-icon a:hover,#searchform-header-replace-close:hover{color:#016fb3}.dropdown-menu .sub-menu,#searchform-dropdown,.current-shop-items-dropdown{border-color:#016fb3}.mobile-menu .hamburger-inner,.mobile-menu .hamburger-inner::before,.mobile-menu .hamburger-inner::after{background-color:#333333}a.sidr-class-toggle-sidr-close{background-color:#ffffff}#mobile-fullscreen{background-color:#006ab6}body .sidr a:hover,body .sidr-class-dropdown-toggle:hover,body .sidr-class-dropdown-toggle .fa,body .sidr-class-menu-item-has-children.active >a,body .sidr-class-menu-item-has-children.active >a >.sidr-class-dropdown-toggle,#mobile-dropdown ul li a:hover,#mobile-dropdown ul li a .dropdown-toggle:hover,#mobile-dropdown .menu-item-has-children.active >a,#mobile-dropdown .menu-item-has-children.active >a >.dropdown-toggle,#mobile-fullscreen ul li a:hover,#mobile-fullscreen .oceanwp-social-menu.simple-social ul li a:hover{color:#016fb3}#mobile-fullscreen a.close:hover .close-icon-inner,#mobile-fullscreen a.close:hover .close-icon-inner::after{background-color:#016fb3}body .sidr-class-mobile-searchform input,#mobile-dropdown #mobile-menu-search form input,#mobile-fullscreen #mobile-search input{border-color:#ffffff}/* Footer Widgets CSS */#footer-widgets{padding:0}#footer-widgets .footer-box a:hover,#footer-widgets a:hover{color:#ffffff}/* Footer Bottom CSS */#footer-bottom{background-color:#333333}#footer-bottom,#footer-bottom p{color:#ffffff}#footer-bottom a:hover,#footer-bottom #footer-bottom-menu a:hover{color:#006ab6}/* Typography CSS */#site-navigation-wrap .dropdown-menu >li >a,#site-header.full_screen-header .fs-dropdown-menu >li >a,#site-header.top-header #site-navigation-wrap .dropdown-menu >li >a,#site-header.center-header #site-navigation-wrap .dropdown-menu >li >a,#site-header.medium-header #site-navigation-wrap .dropdown-menu >li >a,.oceanwp-mobile-menu-icon a{font-family:Raleway;font-weight:700;font-size:14px;letter-spacing:1px;text-transform:uppercase}.page-header .page-header-title,.page-header.background-image-page-header .page-header-title{font-family:Raleway;font-weight:600;font-size:26px;letter-spacing:1.7px;text-transform:uppercase}
        </style>

        <script type='text/javascript' src='/js/dropnlockerz/jquery.js?ver=1.12.4'></script>
        <script type='text/javascript' src='/js/dropnlockerz/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='/js/dropnlockerz/share.min.js?ver=4.9.8'></script>

        <link rel='stylesheet' id='custom-style-css'  href='/css/dropnlockerz/bootstrap2x.css' type='text/css' media='all' />
        <script src='/bootstrap/js/bootstrap.min.js'></script>
        <script type="text/javascript">
          var $ = jQuery.noConflict();
        </script>
        <?= $_scripts ?>
        <?= $javascript ?>
        <?= $style ?>
        <?= $_styles ?>
    </head>
    <body class="home page-template page-template-elementor_header_footer page page-id-13 logged-in admin-bar no-customize-support wp-custom-logo oceanwp-theme fullscreen-mobile no-header-border default-breakpoint content-full-width content-max-width page-header-disabled has-breadcrumbs elementor-default elementor-template-full-width elementor-page elementor-page-13">
        <div id="outer-wrap" class="site clr">
            <div id="wrap" class="clr">
                <header id="site-header" class="center-header header-replace clr" data-height="123" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
                    <div id="site-header-inner" class="clr container closer">
                        <div id="site-logo" class="clr" itemscope itemtype="http://schema.org/Brand">
                            <div id="site-logo-inner" class="clr">
                                <a href="https://www.dropnlockerz.com/" class="custom-logo-link" rel="home" itemprop="url"><img width="240" height="237" src="/images/dropnlockerz/cropped-Drop-n-Lockerz-Logo-Colour.png" class="custom-logo" alt="Drop N Lockers" itemprop="logo" /></a>
                            </div>
                            <!-- #site-logo-inner -->
                        </div>
                        <!-- #site-logo -->
                        <div id="site-navigation-wrap" class="clr">
                            <nav id="site-navigation" class="navigation main-navigation clr" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                                <ul class="left-menu main-menu dropdown-menu sf-menu clr">
                                    <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="https://www.dropnlockerz.com/how-it-works/" class="menu-link"><span class="text-wrap">How it Works</span></a></li>
                                    <li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://www.dropnlockerz.com/faqs/" class="menu-link"><span class="text-wrap">FAQ&#8217;s</span></a></li>
                                    <li id="menu-item-193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-193"><a href="https://www.dropnlockerz.com/pricing/" class="menu-link"><span class="text-wrap">Pricing</span></a></li>
                                </ul>
                                <div class="middle-site-logo clr">
                                    <a href="https://www.dropnlockerz.com/" class="custom-logo-link" rel="home" itemprop="url"><img width="240" height="237" src="/images/dropnlockerz/cropped-Drop-n-Lockerz-Logo-Colour.png" class="custom-logo" alt="Drop N Lockers" itemprop="logo" /></a>
                                </div>
                                <ul class="right-menu main-menu dropdown-menu sf-menu clr">
                                    <li id="menu-item-43" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43"><a href="https://www.dropnlockerz.com/contact/" class="menu-link"><span class="text-wrap">Contact</span></a></li>
                                    <li id="menu-item-192" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-192">
                                        <a href="/" class="menu-link">
                                            <span class="text-wrap">
                                                <p class="reg-nav">Register
                                        </a>
                                        </span></a>
                                    </li>
                                    <li id="menu-item-191" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-191">
                                        <a href="/" class="menu-link">
                                            <span class="text-wrap">
                                                <p class="login-nav">Log In
                                        </a>
                                        </span></a>
                                    </li>
                                    <li class="search-toggle-li"><a href="#" class="site-search-toggle search-header-replace-toggle"><span class="icon-magnifier"></span></a></li>
                                </ul>
                                <div id="searchform-header-replace" class="header-searchform-wrap clr">
                                    <form method="get" action="https://www.dropnlockerz.com/" class="header-searchform">
                                        <input type="search" name="s" autocomplete="off" value="" placeholder="Type then hit enter to search..." />
                                    </form>
                                    <span id="searchform-header-replace-close" class="icon-close"></span>
                                </div>
                                <!-- #searchform-header-replace -->
                            </nav>
                            <!-- #site-navigation -->
                        </div>
                        <!-- #site-navigation-wrap -->
                        <div class="oceanwp-mobile-menu-icon clr mobile-right">
                            <a href="#" class="mobile-menu">
                                <div class="hamburger hamburger--3dx-r">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                                <span class="oceanwp-text">Menu</span>
                            </a>
                        </div>
                        <!-- #oceanwp-mobile-menu-navbar -->
                    </div>
                    <!-- #site-header-inner -->
                </header>
                <!-- #site-header -->
                <main id="main" class="site-main clr">
                    <div id="bootstrap2x"><?=$content?></div>
                    <br/>
                </main>
                <!-- #main -->
                <footer id="footer" class="site-footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
                    <div id="footer-inner" class="clr">
                        <div id="footer-widgets" class="oceanwp-row clr">
                            <div class="footer-widgets-inner container">
                                <div class="elementor elementor-44">
                                    <div class="elementor-inner">
                                        <div class="elementor-section-wrap">
                                            <section data-id="2793c14" class="elementor-element elementor-element-2793c14 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div data-id="427c5ac" class="elementor-element elementor-element-427c5ac elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-id="04cf764" class="elementor-element elementor-element-04cf764 elementor-column elementor-col-50 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section data-id="b6e7bb0" class="elementor-element elementor-element-b6e7bb0 elementor-section-stretched elementor-section-height-min-height elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div data-id="265d7ae" class="elementor-element elementor-element-265d7ae elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div data-id="49bdc80" class="elementor-element elementor-element-49bdc80 elementor-widget elementor-widget-heading" data-element_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">Get in Touch</h2>
                                                                        </div>
                                                                    </div>
                                                                    <div data-id="ba43611" class="elementor-element elementor-element-ba43611 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                                <p>Email: <a style="color: white; text-decoration: underline;" href="mailto:hello@dropnlockerz.co.uk">hello@dropnlockerz.co.uk</a></p>
                                                                                <p>Tel: 0203 368 8830</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-id="5823d7b" class="elementor-element elementor-element-5823d7b elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div data-id="4667762" class="elementor-element elementor-element-4667762 elementor-widget elementor-widget-heading" data-element_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">Find out more</h2>
                                                                        </div>
                                                                    </div>
                                                                    <div data-id="2f020e7" class="elementor-element elementor-element-2f020e7 elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
                                                                        <div class="elementor-widget-container">
                                                                            <ul class="elementor-icon-list-items">
                                                                                <li class="elementor-icon-list-item" >
                                                                                    <a href="https://www.dropnlockerz.com/how-it-works">                        <span class="elementor-icon-list-icon">
                                                                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="elementor-icon-list-text">How it Works</span>
                                                                                    </a>
                                                                                </li>
                                                                                <li class="elementor-icon-list-item" >
                                                                                    <a href="https://www.dropnlockerz.com/faqs">                        <span class="elementor-icon-list-icon">
                                                                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="elementor-icon-list-text">FAQ's</span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-id="ae8ce82" class="elementor-element elementor-element-ae8ce82 elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div data-id="9169d20" class="elementor-element elementor-element-9169d20 elementor-widget elementor-widget-heading" data-element_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">Quick Links</h2>
                                                                        </div>
                                                                    </div>
                                                                    <div data-id="4bc3c16" class="elementor-element elementor-element-4bc3c16 elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
                                                                        <div class="elementor-widget-container">
                                                                            <ul class="elementor-icon-list-items">
                                                                                <li class="elementor-icon-list-item" >
                                                                                    <a href="https://www.dropnlockerz.com/pricing">                     <span class="elementor-icon-list-icon">
                                                                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="elementor-icon-list-text">Pricing</span>
                                                                                    </a>
                                                                                </li>
                                                                                <li class="elementor-icon-list-item" >
                                                                                    <a href="https://www.dropnlockerz.com/contact">                     <span class="elementor-icon-list-icon">
                                                                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                                                                    </span>
                                                                                    <span class="elementor-icon-list-text">Get In Touch</span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div data-id="cbbbbe4" class="elementor-element elementor-element-cbbbbe4 elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <section data-id="379b4ed" class="elementor-element elementor-element-379b4ed elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
                                                                        <div class="elementor-container elementor-column-gap-default">
                                                                            <div class="elementor-row">
                                                                                <div data-id="7f0e786" class="elementor-element elementor-element-7f0e786 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div data-id="a4c40ab" class="elementor-element elementor-element-a4c40ab elementor-widget elementor-widget-image" data-element_type="image.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="elementor-image">
                                                                                                        <img width="167" height="55" src="/images/dropnlockerz/app-store-image.png" class="attachment-large size-large" alt="Download the App on Apple App Store" />                                         
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div data-id="34afc7d" class="elementor-element elementor-element-34afc7d elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div data-id="05685af" class="elementor-element elementor-element-05685af elementor-widget elementor-widget-image" data-element_type="image.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="elementor-image">
                                                                                                        <img width="167" height="55" src="/images/dropnlockerz/google-play-image.png" class="attachment-large size-large" alt="Download the App on Google Play" />                                           
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                    <div data-id="e6216a0" class="elementor-element elementor-element-e6216a0 elementor-shape-circle elementor-widget elementor-widget-social-icons" data-element_type="social-icons.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-social-icons-wrapper">
                                                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-facebook elementor-animation-pulse" href="" target="_blank">
                                                                                <span class="elementor-screen-only">Facebook</span>
                                                                                <i class="fa fa-facebook"></i>
                                                                                </a>
                                                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-twitter elementor-animation-pulse" href="" target="_blank">
                                                                                <span class="elementor-screen-only">Twitter</span>
                                                                                <i class="fa fa-twitter"></i>
                                                                                </a>
                                                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-youtube elementor-animation-pulse" href="" target="_blank">
                                                                                <span class="elementor-screen-only">Youtube</span>
                                                                                <i class="fa fa-youtube"></i>
                                                                                </a>
                                                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-pulse" href="" target="_blank">
                                                                                <span class="elementor-screen-only">Instagram</span>
                                                                                <i class="fa fa-instagram"></i>
                                                                                </a>
                                                                                <a class="elementor-icon elementor-social-icon elementor-social-icon-google-plus elementor-animation-pulse" href="" target="_blank">
                                                                                <span class="elementor-screen-only">Google-plus</span>
                                                                                <i class="fa fa-google-plus"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section data-id="a9bec6e" class="elementor-element elementor-element-a9bec6e elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div data-id="abf4a14" class="elementor-element elementor-element-abf4a14 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                                            <div class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div data-id="5d609e3" class="elementor-element elementor-element-5d609e3 elementor-widget elementor-widget-divider" data-element_type="divider.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-divider">
                                                                                <span class="elementor-divider-separator"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <section data-id="5f1642e" class="elementor-element elementor-element-5f1642e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
                                                                        <div class="elementor-container elementor-column-gap-default">
                                                                            <div class="elementor-row">
                                                                                <div data-id="c862588" class="elementor-element elementor-element-c862588 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div data-id="e28aaa6" class="elementor-element elementor-element-e28aaa6 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="elementor-text-editor elementor-clearfix">Copyright 2018 Drop N Lockerz | <a style="text-decoration: underline; color: white;" href="https://www.dropnlockerz.com/privacy-policy">Privacy Policy</a> | <a style="text-decoration: underline; color: white;" href="https://www.dropnlockerz.com/terms">Website Terms</a> | <a style="text-decoration: underline; color: white;" href="https://www.dropnlockerz.com/service=terms">Service Terms</a></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div data-id="1171015" class="elementor-element elementor-element-1171015 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
                                                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                                                        <div class="elementor-widget-wrap">
                                                                                            <div data-id="36c1dcb" class="elementor-element elementor-element-36c1dcb elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
                                                                                                <div class="elementor-widget-container">
                                                                                                    <div class="elementor-text-editor elementor-clearfix">Design by <a style="text-decoration: underline; color: white;" href="https://www.fificreative.co.uk" rel="no-follow">Fifi Creative</a></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- .container -->
                        </div>
                        <!-- #footer-widgets -->
                    </div>
                    <!-- #footer-inner -->
                </footer>
                <!-- #footer -->            
            </div>
            <!-- #wrap -->
        </div>
        <!-- #outer-wrap -->
        <a id="scroll-top" href="#"><span class="fa fa-angle-up"></span></a>
        <div id="mobile-fullscreen" class="clr">
            <div id="mobile-fullscreen-inner" class="clr">
                <a href="#" class="close">
                    <div class="close-icon-wrap">
                        <div class="close-icon-inner"></div>
                    </div>
                </a>
                <nav class="clr" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
                    <div id="mobile-nav" class="navigation clr">
                        <ul id="menu-mobile-menu" class="menu">
                            <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-13 current_page_item menu-item-37"><a href="https://www.dropnlockerz.com/">Home</a></li>
                            <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="https://www.dropnlockerz.com/how-it-works/">How it Works</a></li>
                            <li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="https://www.dropnlockerz.com/faqs/">FAQ&#8217;s</a></li>
                            <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="https://www.dropnlockerz.com/pricing/">Pricing</a></li>
                            <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="https://www.dropnlockerz.com/contact/">Contact</a></li>
                        </ul>
                    </div>
                    <div id="mobile-search" class="clr">
                        <form method="get" action="https://www.dropnlockerz.com/" class="header-searchform">
                            <input type="search" name="s" value="" autocomplete="off" />
                            <label>Type your search<span><i></i><i></i><i></i></span></label>
                        </form>
                    </div>
                </nav>
            </div>
        </div>

        <script type='text/javascript' src='/js/dropnlockerz/eael-scripts.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/fancy-text.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/countdown.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/masonry.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/load-more.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/codebird.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/doT.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/moment.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/jquery.socialfeed.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/mixitup.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/jquery.magnific-popup.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/slick.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/imagesloaded.min.js?ver=3.2.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/magnific-popup.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/lightbox.min.js?ver=1.0'></script>


        <script type='text/javascript'>
        /* <![CDATA[ */
        var oceanwpLocalize = {"isRTL":"","menuSearchStyle":"header_replace","sidrSource":null,"sidrDisplace":"","sidrSide":"right","sidrDropdownTarget":"icon","verticalHeaderTarget":"icon","customSelects":".woocommerce-ordering .orderby, #dropdown_product_cat, .widget_categories select, .widget_archive select, .single-product .variations_form .variations select","ajax_url":"https:\/\/www.dropnlockerz.com\/wp-admin\/admin-ajax.php"};
        /* ]]> */
        </script>

        <script type='text/javascript' src='/js/dropnlockerz/main.min.js?ver=1.0'></script>
        <script type='text/javascript' src='/js/dropnlockerz/wp-embed.min.js?ver=4.9.8'></script>
        <!--[if lt IE 9]>
        <script type='text/javascript' src='/js/dropnlockerz/html5.min.js?ver=1.0'></script>
        <![endif]-->
        <script type='text/javascript' src='/js/dropnlockerz/position.min.js?ver=1.11.4'></script>

        <script type="text/javascript">
          var $ = jQuery.noConflict();
        </script>
    </body>
</html>