<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="shortcut icon" href="/images/jandjlockers/logo_32.png" type="image/png" />
    <link rel="apple-touch-icon" href="/images/jandjlockers/logo_32.png" type="image/png" />

    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>
    <style type="text/css">
        body {
            padding: 0;
        }
        @font-face {
            font-family: "futura-lt";
            src: url("/fonts/jandjlockers/futura-lt.woff2") format('woff2');
        }
        .container-fluid {
            padding: 0px;
        }
        .navbar {
            background-color: #2e4a07;;
            font-family: futura-lt, "sans serif";
            padding-top: 21px;
            padding-bottom: 15px;
            margin-bottom: 0;
        }

        .navbar > .navbar-inner-menu {
            background-image: none;
            background-color: inherit;
            border: 0;
            text-align: center;
        }

        .navbar > .navbar-inner-menu > .nav {
            margin: 0 auto;
            float: none;
            display: inline-block;
        }

        .navbar .nav > li > a {
            color: #FFF;
            text-transform: uppercase;
            font-size: 14px;
            line-height: 24px;
            margin-left: 0;
            padding: 0 10px;
        }

        .navbar .nav > li > a:hover {
            color: #B0AAA9;
            text-decoration: none;
        }

        @media (min-width: 979px) {
            ul.nav li.dropdown:hover > ul.dropdown-menu {
                display: block;
                background-color: #2f2e2e;
                border-radius: 0;
                min-width: auto;
                box-shadow: none;
                top: 20px;
                margin-left: -25%;
            }

            ul.nav li.dropdown:hover > ul.luxer-one {
                margin-left: 15%;
            }

            ul.nav li.dropdown:hover > ul.dropdown-menu::after,
            ul.nav li.dropdown:hover > ul.dropdown-menu::before {
                display: none;
            }

            ul.nav li.dropdown:hover > ul.dropdown-menu a {
                color: #FFF;
                text-transform: uppercase;
            }

            ul.nav li.dropdown:hover > ul.dropdown-menu a:hover {
                background: inherit;
            }
        }

        .row-fluid .container {
            width: 100%;
        }

        .container .row {
            margin-left: 0;
        }

        #content .btn, #content .btn-info {
            color: #FFF;
            background-color: #2e4a07;
            border: 2px solid #FFF;
            padding: 5px 10px;
            border-radius: 0;
            background-image: none;
            text-transform: uppercase;
            box-shadow: none;
        }

        #content .btn:hover {
            color: #2e4a07;
            background-color: #FFF;
            border: 2px solid #2e4a07;
        }

        #content .btn:hover i {
            background-image: url("/images/jandjlockers/glyphicons-halflings-green.png");
        }

        #content {
            font-family: futura-lt, "sans serif";
            text-transform: uppercase;
            color: #2F2E2E;
            padding: 20px 0;
        }

        #content a,
        #content a:hover {
            color: #2e4a07;
            text-decoration: none;
        }

        #default_account_nav ul {
            list-style: none;
            margin-left: 0;
        }

        #content > .container > .row {
            width: 65.95744680199999%;
            *width: 65.90425531263828%;
            margin: 0 auto;
        }

        #content .container .row .span12 .container .row .span8 {
            width: 100%;
        }

        #content .container .row .span12 .container .row .span3 {
            width: 30%;
            margin-left: 3.33%;
        }

        #content .container .row .span12 .container .row .span4 {
            width: 45%;
            margin-left: 3.33%;
        }

        #content .container .row .span12 .container .row .span5 {
            width: 55%;
            margin-left: 3.33%;
        }

        #content .container .row .span7 {
            width: 65%;
            margint-left: 3.33%;
            display: inline-block;
        }

        #content .container .row .span3 {
            width: 30%;
            margint-left: 3.33%;
            display: inline-block;
        }

        #content .container .row .span3 .span2 {
            width: 100%;
        }

        #content .alert-info {
            background-color: #2e4a07;
            color: #FFF;
            border: 0;
            border-radius: 0;
        }

        #credit_card_form {
            clear: both;
            width: 100%;
        }


        #billing_info_fields div.span2 {
            width: 40%;
        }

        #credit_card_fields .span4 {
            width: 100%;
        }

        #credit_card_fields .span2 {
            width: 45%;
        }


        #credit_card_fields input, #billing_info_fields input, #billing_info_fields select {
            width: 100%;
        }

        div.control-group.span1 {
            width: 20%;
        }

        #footer {
            border-top: 5px solid #6e6968;
            padding-top: 39px;
            background-color: #2e4a07;
            color: #FFF;
        }

        #footer a img {
            margin: 5px;
        }

    </style>
</head>
<body>
<div id="fb-root"></div>
<div class="container-fluid">
    <div>
        <?
        //if this is using the exterior pages - make sure NOT to hide the header/footer
        // $responsive = '';
        // $uri_string = $this->uri->uri_string();
        // if( strpos($uri_string,'main/') === false ){
        $responsive = 'hidden-phone hidden-tablet';
        // }
        ?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                    </a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="http://www.jandjlockers.com/">Home</a></li>
                            <li><a href="http://www.jandjlockers.com/#comp-itzdxll4">How it Works</a></li>
                            <li><a href="http://www.jandjlockers.com/#comp-iu673dgz">Services & Pricing</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Details</a>
                                <ul class="dropdown-menu">
                                    <li><a href="http://www.jandjlockers.com/#comp-ifmbz7m3">About Us</a></li>
                                    <li><a href="http://www.jandjlockers.com/#comp-ihbpae3h">Locations</a></li>
                                    <li><a href="http://www.jandjlockers.com/#comp-itzdy5ld">FAQ</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Luxer One Package Lockers</a>
                                <ul class="dropdown-menu">
                                    <li><a href="http://www.jandjlockers.com/j-and-j-luxer-one">J&J And Luxer One</a></li>
                                    <li><a href="http://www.jandjlockers.com/luxer-one-how-it-works">How it Works</a></li>
                                    <li><a href="http://www.jandjlockers.com/luxer-one-lockers">Luxer One Lockers</a></li>
                                    <li><a href="http://www.jandjlockers.com/luxer-room">Luxer Room</a></li>
                                    <li><a href="http://www.jandjlockers.com/luxer-one-features">Luxer One Features</a></li>
                                    <li><a href="http://www.jandjlockers.com/luxer-one-student-housing">Student Housing</a></li>
                                    <li><a href="http://www.jandjlockers.com/luxer-one-faq">Luxer One FAQ</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Commercial</a>
                                <ul class="dropdown-menu">
                                    <li><a href="http://www.jandjlaundry.com/">J&J Commercial Services</a></li>
                                </ul>
                            </li>
                            <li><a href="/login">Sign-Up/Login</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container-fluid">
                    <div id="" class="row-fluid">
                        <div class="span12 center">
                            <div class="navbar">
                                <div class="navbar-inner-menu">
                                    <ul class="nav">
                                        <li><a class="navText navLink" href="http://www.jandjlockers.com/">Home</a></li>
                                        <li><a class="navText navLink" href="http://www.jandjlockers.com/#comp-itzdxll4">How it Works</a></li>
                                        <li><a class="navText navLink" href="http://www.jandjlockers.com/#comp-iu673dgz">Services & Pricing</a></li>
                                        <li class="dropdown">
                                            <a class="navText navLink dropdown-toggle" data-toggle="dropdown" href="#">Details</a>
                                            <ul class="dropdown-menu">
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/#comp-ifmbz7m3">About Us</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/#comp-ihbpae3h">Locations</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/#comp-itzdy5ld">FAQ</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="navText navLink" href="/main/publiclocations">Contact</a></li>
                                        <li class="dropdown">
                                            <a class="navText navLink dropdown-toggle" data-toggle="dropdown" href="#">Luxer One Package Lockers</a>
                                            <ul class="dropdown-menu luxer-one">
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/j-and-j-luxer-one">J&J And Luxer One</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/luxer-one-how-it-works">How it Works</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/luxer-one-lockers">Luxer One Lockers</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/luxer-room">Luxer Room</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/luxer-one-features">Luxer One Features</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/luxer-one-student-housing">Student Housing</a></li>
                                                <li><a class="navText navLink" href="http://www.jandjlockers.com/luxer-one-faq">Luxer One FAQ</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a class="navText navLink dropdown-toggle" data-toggle="dropdown" href="#">Commercial</a>
                                            <ul class="dropdown-menu">
                                                <li><a class="navText navLink" href="http://www.jandjlaundry.com/">J&J Commercial Services</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="navText" href="/login" class="navLink">Sign-Up/Login</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row-fluid" style=''>
            <div class="span12" id="content">
                <?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />


        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="span12" id="footerInner">
                <div class="row-fluid pagination-centered">
                    <a href="https://www.facebook.com/jandjlockers"><img src="/images/jandjlockers/facebook.png"></a>
                    <a href="https://www.instagram.com/jandjlockers"><img src="/images/jandjlockers/instagram.png"></a>
                    <a href="https://www.linkedin.com/company/j&amp;j-lockers"><img src="/images/jandjlockers/linkedin.png"></a>
                </div>
                <div class="row-fluid pagination-centered">
                    © 2016 by J&amp;J Lockers a J&amp;J Family of Companies
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
    <?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

    <?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
    <?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
    <?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>
