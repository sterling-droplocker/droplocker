<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=utf-8">
<meta charset="utf-8">
<title>Locker Drop Cleaners</title>

<meta id="view" name="viewport" content="initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes">

<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
<link rel="stylesheet" href="/css/breezylaundrylockers/font/font-awesome.css">

<link type="text/css" rel="stylesheet" href="/css/lockerdropcleaners/duda-css-foundation.min.css?version=2014-04-13T12:35:39"/>
<link href='//fonts.googleapis.com/css?family=Droid+Sans:400,400italic,700,700italic|Arvo:400,400italic,700,700italic|PT+Sans:400,700,400italic,700italic|Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic|PT+Serif:400,400italic,700,700italic|Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic|Josefin+Sans:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic|Open+Sans:300,400,600,700,800,300italic,400italic,600italic,700italic,800italic|Vollkorn:400,700,400italic,700italic|Abril+Fatface:400|Old+Standard+TT|Lobster|Sansita+One|Sanchez|Playfair+Display|Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic|Oxygen:300,400,700|Roboto:100,300,400,500,700,900,100italic,300italic,400italic,500italic,700italic,900italic|Raleway:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Yesteryear:400|Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic|Bree+Serif:400|Cabin:400,500,600,700,400italic,500italic,600italic,700italic|Lobster+Two:400,400italic,700,700italic|Amaranth:400,400italic,700,700italic|Amatic+SC:400,700|Gudea:400,700,400italic|Lora:400,400italic,700,700italic|Rokkitt:400,700|Glegoo:400|Pacifico:400|Patua+One:400|Crete+Round:400,400italic|Stalemate:400|Cookie:400|Jura:300,400,500,600|Roboto+Slab:100,300,400,700|Ubuntu+Mono:400,700,400italic,700italic|Oswald:400|Flamenco:400|Poiret+One:400|Inconsolata:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/earlyaccess/opensanshebrew.css' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/earlyaccess/alefhebrew.css' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="/css/lockerdropcleaners/duda-css-runtime-desktop-one-package.min.css?version=2014-04-13T12:35:39"/>
<link type="text/css" rel="stylesheet" href="/css/lockerdropcleaners/font-icons/dm-font/style.css?version=2014-04-13T12:35:39"/>

<link type="text/css" rel="stylesheet" href="/css/lockerdropcleaners/skin.css?v=59"/>
<link type="text/css" rel="stylesheet" href="/css/lockerdropcleaners/style.css"/>

<link rel="shortcut icon" href="/images/lockerdropcleaners/favicon_d1.ico"/>


<script type="text/javascript">
window.isMultiScreen = true;
</script>


<style id="globalCss" type="text/css">

</style>

<style id="headerCss" type="text/css">
</style>

<style id="headerDeviceCss" type="text/css">
</style>

<style id="pagestyle" type="text/css"> 

		</style>
	
<!--ie hack. set the max width of the logo image in px, otherwise you canot change its size  -->
<!--[if IE 7]>
<style>
    .fw-head.fw-logo img
    {
        max-width: 290px;
    }
    
    .dm_header .logo-div img
    {
        max-width: 290px;
    }
</style>
<![endif]-->

<!--[if IE 8]>
<style>
    .fw-head .fw-logo img
    {
        max-width: 290px;
    }
    
    .dm_header .logo-div img
    {
        max-width: 290px;
    }
    *#dm div.dmHeader
    {
        _height:90px;
        min-height:0px;
    }
    
</style>
<![endif]-->


<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>



<!--
<script type="text/javascript" src="/js/lockerdropcleaners/duda-js-runtime-package.min.js?version=2014-04-13T12:35:39"></script>

<script>
function styleImages(element) {
	
}

jQuery(document).ready(function(){
	setTimeout("styleImages()",300);
});
jQuery(window).load(function() {
	styleImages();
	setTimeout("styleImages()",2000);
	jQuery.DM.updateIOSHeight();
});
</script>
-->







<script type="text/javascript">
Parameters = {};
Parameters.AjaxContainer = "div.dmBody";
Parameters.WrappingContainer = "div.dmOuter";
Parameters.HomeUrl = "http://www.lockerdropcleaners.com/";
Parameters.SiteAlias = "dfca69aa";
Parameters.SiteId = "405875";
Parameters.SiteType = 'DUDAONE';
Parameters.IsSEOFriendlyLinks = true;
Parameters.InitialPageAlias = "home";
Parameters.IsFromScratch = "false";
Parameters.CurrentPageUrl = "null";
Parameters.IsCurrentHomePage = true;
Parameters.classicLink = "";

Parameters.AllowAjax = false;
Parameters.AfterAjaxCommand = null;

Parameters.NavigationAreaParams = {};
Parameters.NavigationAreaParams.ShowBackToHomeOnInnerPages = true;

Parameters.NavigationAreaParams.NavbarSize = 2;
Parameters.NavigationAreaParams.NavbarLiveHomePage = "http://www.lockerdropcleaners.com/";
Parameters.NavigationAreaParams.BlockContainerSelector = ".dmBody";
Parameters.NavigationAreaParams.NavbarSelector = "#dmNav:has(a)";
Parameters.NavigationAreaParams.SubNavbarSelector = "#subnav_main";
Parameters.HomeLinkText="Back To Home";
Parameters.UseGalleryModule = false;
Parameters.CurrentThemeName = "Layout Theme";
Parameters.DefaultPageAlias = "";
Parameters.LayoutVariationID = {
	mobile : 5
}; 
Parameters.LayoutID = {
	mobile : 6
};
Parameters.WidgetStyleID = null;
Parameters.IsHeaderFixed= false;
Parameters.IsHeaderSkinny= false;
Parameters.IsBfs= true;
Parameters.LayoutParams = {
	_manifestId : 10000,
	_device : "mobile"
};



				$.extend(Parameters.LayoutParams, {_navigationAnimationStyle : 'slide'});
				
			Parameters.NavigationAreaParams.MoreButtonText = "More";
		
			Parameters.NavigationAreaParams.LessButtonText = "Less";
			
			Parameters.HomeLinkText = "Home";
		

//Parameters.Charset = 'utf-8';

</script>































<script type="text/javascript">
	$(window).resize(function() {
		jQuery.DM.updateWidth();
		
		

		
	});
	$(window).bind("orientationchange",function(e) {
		jQuery.DM.updateWidth();
		jQuery.layoutManager.initLayout();
		
	});
	$(document).resize(function() {
		jQuery.DM.updateWidth();
// 		jQuery.layoutManager.initLayout();
		

		
	});
</script>

</head>




<body id="dmRoot" class="supportsFontIcons  " style="overflow:auto;padding:0;margin:0;float:none;"  >


<script>
jQuery.DM.updateWidth();
jQuery.DM.updateIOSHeight();
</script>


	
<script type="text/javascript">


	function setLoaderSize(height, width, loader) {
		loader = loader || $("body");
		var cols = loader.find(".coloumns");

		if (cols != null) {
			cols.css("height", height + "px");
			cols.css("width", width + "px");
		} else {
			for ( var i = 1; i < 13; i++) {
				loader.find("#" +'coloumn' + i).height(height);
				loader.find("#" +'coloumn' + i).width(width);
				
				/* document.getElementById('coloumn' + i).style.height = height
						+ "px";
				document.getElementById('coloumn' + i).style.width = width
						+ "px"; */
			}
		}
	}

	function setLoaderColor(colorString, loader) {
		loader = loader || $("body");
		var cols = loader.find(".coloumns");
		if (cols != null) {
			cols.css("background-color", colorString);
		} else {
			for ( var i = 1; i < 13; i++) {
				loader.find("#" +'coloumn' + i).css("background", colorString);
				//document.getElementById('coloumn' + i).style.background = colorString;
			}
		}
	}
	
	function showDefaultLoader(loader, wnd)
	{
		setLoaderSize(10, 3, loader);
		setLoaderColor("black", loader);
		loader.closest("body").css("height","100%");
		loader.css("position", "absolute").css("display", "block")
		.css("z-index", "100000")
		.css("left", ($(wnd).width() / 2) + "px")
		.css("top", ($(wnd).height() / 2) + "px");
		loader.closest("body").css("height","");
		loader.find("#imageZone").show();
	}
</script>

<div id="disabledImageZone" style="display:none;z-index:-1">
    	
    		<img id="imageZone" src="/editor/images/ajax-loader.gif" style="position:absolute;display:none">
    	
    </div>

<!-- wrap the content with a div to ensure that css created by us will override page/site css    -->
<div id="dm" class='dmwr'>
	
	
	<div class="dm_wrapper standard-var5 widgetStyle-3 standard"> 
		 <div dmwrapped="true" id="1190112199" class="dm-home-page" themewaschanged="true"> <div dm:templateorder="6" dmtemplateid="StandardLayoutMultiD" class="standardHeaderLayout dm-bfs dm-layout-home" data-buttonstyle="FLAT" data-soch="true"><!-- 	<dm:Variable dm:id="wrapperVariable" dm:target="layout" dm:attribute="class"/> --> <div id="dmStyle_outerContainer" class="dmOuter"> <div id="dmStyle_innerContainer" class="dmInner"> <div class="dmLayoutWrapper standard-var dmStandardDesktop"> <div id="desktopHeaderBox" class="dmRespRow dmRespRowStable dmRespRowNoPadding desktopHeaderBox"> <div id="sochcontainer" class="soc_hcontainer p_hfcontainer"> <div dm:templateorder="15" class="socialRow"> <div class="innerSocialRow"> <div class="socialRowBox"> <div class="dmSocialHub dmSocialHome" dmle_extension="social_hub" wr="false" data-dmtmpl="true" icon="true" surround="false"> <div class="socialHubWrapper"> <div class="socialHubInnerDiv "> <a href="#" dmle_dont_remove="target" dm_dont_rewrite_url="true"> <span class="dmSocialFacebook icon-facebook oneIcon socialHubIcon style3"></span> 
</a> 
 <a href="#" dmle_dont_remove="target" dm_dont_rewrite_url="true"> <span class="dmSocialTwitter icon-twitter oneIcon socialHubIcon style3"></span> 
</a> 
 <a href="#" dmle_dont_remove="target" dm_dont_rewrite_url="true"> <span class="dmSocialGooglePlus icon-google-plus oneIcon socialHubIcon style3"></span> 
</a> 
</div> 
</div> 
</div> 
 <div class="dmNewParagraph dmSocialParagraph u_1257064797">Call us: 786.436<span data-dmtmpl="true">.DROP</span>&nbsp;&nbsp;</div> <div class="socialRowClear"></div> 
</div> 
</div> 
</div> 
</div> 
 <div class="dmRespColsWrapper"> <div class="large-12 medium-12 small-12 dmRespCol"> <div class="dmHeader p_hfcontainer u_hcontainer desktopHeader fixedHeader fixedHeaderLimitSize dmOnlySkinny"> <div class="logoTitleWrapper"> <div class="u_logo-div logo-div dm_header" id="logo-div" style="display:none;"> <a id="dm-logo-anchor" class="u_dm-logo-anchor dm-logo-anchor" href="/"><img id="dm-logo-image" class="u_dm-logo-image dm-logo-image" src="https://irp-cdn.multiscreensite.com/dfca69aa/dms3rep/multi/desktop/lockerdroplogo-2030x376.png" dm_changed="true" data-dimensions="eyJtb2JpbGUiOnt9LCJ0YWJsZXQiOnsia2VlcFByb3AiOnRydWUsInVzZVBlcmMiOnRydWUsIndpZHRoIjoiNDklIiwiaGVpZ2h0IjoiYXV0byIsIm1heC1oZWlnaHQiOiIxMDAwcHgifSwiZGVza3RvcCI6e319" device-src-orig=""/></a> 
</div> 
 <div class="u_dm-title dm-title dm_header" id="dm-title" device-text-orig="
   MYLOGO
  ">MYLOGO</div> 
</div> 
</div> 
 <div id="upperFloatingNav" class="desktopNavWrapper  dm-bg-check"> <ul class="dmn dmLayoutNav hiddenNavPlaceHolder navPlaceHolder dmDisplay_None"></ul> 
 <ul id="upperFloatingNavigation" class="dmTablet_navNoIcons dmNavWrapper dmNavigationWithLink dmn dmLayoutNav" dmoffset="2">
 <li class=" desktopTopNav  dmHideFromNav-tablet dmHideFromNav-mobile dmHideFromNav-desktop  navItemSelectedServer navButtonLi" id=""> <span class="ButtonItemArrowUp"></span> 
 <a href="/home" animationtype="inner" class="dmUDNavigationItem_00"> <span class="navItemText">Home</span> 
</a> 
 <span class="ButtonItemArrowDown"></span> 
</li> 
 <li class=" desktopTopNav  dmHideFromNav-tablet dmHideFromNav-mobile dmHideFromNav-desktop   navButtonLi" id=""> <span class="ButtonItemArrowUp"></span> 
 <a href="/about" animationtype="inner" class="dmUDNavigationItem_01"> <span class="navItemText">About Us</span> 
</a> 
 <span class="ButtonItemArrowDown"></span> 
</li> 
 <li class=" desktopTopNav  dmHideFromNav-tablet dmHideFromNav-mobile dmHideFromNav-desktop   navButtonLi" id=""> <span class="ButtonItemArrowUp"></span> 
 <a href="/contact" animationtype="inner" class="dmUDNavigationItem_02"> <span class="navItemText">Contact</span> 
</a> 
 <span class="ButtonItemArrowDown"></span> 
</li> 
 <li class=" desktopTopNav  dmHideFromNav-tablet dmHideFromNav-mobile dmHideFromNav-desktop   navButtonLi" id=""> <span class="ButtonItemArrowUp"></span> 
 <a href="/menu" animationtype="inner" class="dmUDNavigationItem_01010199312"> <span class="navItemText">Menu</span> 
</a> 
 <span class="ButtonItemArrowDown"></span> 
</li> 
</ul> 
</div> 
 <div style="clear: both;"></div> 
</div> 
</div> 
</div> 
 <div> <div id="iscrollBody"> <div id="site_content"> <div class="dmRespRow dmRespRowStable dmRespRowNoPadding"> <div class="dmRespColsWrapper"> <div class="large-12 dmRespCol"> <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None"> <div class="titleLine display_None"><hr/></div> 
 <div id="pageTitleText"></div> 
 <div class="titleLine display_None"><hr/></div> 
</div> 
</div> 
</div> 
</div> 
 <div class="freeHeader" id="hcontainer"> <div id="hcontainer" class="u_hcontainer dmHeader p_hfcontainer"></div> 
</div> 
 <div dmwrapped="true" id="1190112199" class="dmBody u_dmStyle_template_home dm-home-page" themewaschanged="true"> <div id="allWrapper" class="allWrapper"><!-- navigation placeholders --> <div id="navWrapper" class="navWrapper"> <div id="hiddenNavPlaceHolder" class="hiddenNavPlaceHolder navPlaceHolder dmDisplay_None" navplaceholder="true"></div> 
 <div id="backToHomePlaceHolder" class="backToHomePlaceHolder navPlaceHolder" navplaceholder="true"></div> 
 <div id="newNavigationSubNavPlaceHolder" class="newNavigationSubNavPlaceHolder navPlaceHolder" navplaceholder="true"></div> 
</div> 
 <div id="dm_content" class="dmContent"> <div dm:templateorder="115" class="dmDefaultRespTmpl" id="1558298678"> <div class="u_1809251120 dmRespRow" style="text-align: center;" id="1809251120"> <div class="dmRespColsWrapper" id="1716352808"> <div class="dmRespCol small-12 medium-12 large-12" id="1622752133">
     <hr class="dmDivider" style="min-height: 2px; border:none; background:grey" id="1236338676"/>
</div> 
</div> 
</div> 

<div style="min-height: 400px;background:#fff;padding:40px 0;">
<?= $content ?>
</div> 

<div style=""></div>
</div> 

</div> 
</div> 
</div> 
</div> 



</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 
</div> 

	</div>
</div>


<script type="text/javascript" src="/js/lockerdropcleaners/duda-js-runtime-layouts-package.min.js?version=2014-04-13T12:35:39"></script>
<script type="text/javascript" src="/js/lockerdropcleaners/dmLayoutDeviceDesktop.js?version=2014-04-13T12:35:39"></script>



<script>
jQuery("img").error(function(){
		jQuery(this).hide();
	});
</script>
<div id="fb-root"></div>

<!--  site ID: 405875 -->
<!--  Alias: dfca69aa -->
<!--  device: DESKTOP -->
<!--  page ID: 32966971 -->
<!--  generated: Tue Apr 15 20:12:06 UTC 2014 -->
<!--  Server: d1-run-1 --><div class="dmPopupMask" id="dmPopupMask"></div>
<div id="dmPopup" class="dmPopup">
	<div class="dmPopupCloseWrapper"> <div class="dmPopupClose dm-icon-x2 oneIcon" onclick="dmHidePopup(event);"></div> </div> 
 	<div class="dmPopupTitle"> <span></span> Share by:</div> 
	<div class="data"></div>
</div>

<?= $footer ?>
</body>
</html>
