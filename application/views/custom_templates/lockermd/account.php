<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" >
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="format-detection" content="telephone=no">
      <title>Laundry Service - Dry Cleaners - Laundromat - Minneapolis, St Paul, MN</title>

      <meta name="description" content="The Laundry Doctor provides all of your laundry needs, wherever you are. Our most popular services - Dry Cleaning &amp; Wash, Dry, Fold come with free pick-up and delivery."/>
      <link rel="canonical" href="https://thelaundrydoctor.com/" />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Laundry Service - Dry Cleaners - Laundromat - Minneapolis, St Paul, MN" />
      <meta property="og:description" content="The Laundry Doctor provides all of your laundry needs, wherever you are. Our most popular services - Dry Cleaning &amp; Wash, Dry, Fold come with free pick-up and delivery." />
      <meta property="og:url" content="https://thelaundrydoctor.com/" />
      <meta property="og:site_name" content="Dry Cleaning &amp; Laundry Delivery Service" />
      <link rel="shortcut icon" href="/images/lockermd/favicon-32x32.png"  />
      <script type="text/javascript">
         window.abb = {};
         php = {};
         window.PHP = {};
         PHP.ajax = "https://thelaundrydoctor.com/wp-admin/admin-ajax.php";PHP.wp_p_id = "1147";var mk_header_parallax, mk_banner_parallax, mk_page_parallax, mk_footer_parallax, mk_body_parallax;
         var mk_images_dir = "https://thelaundrydoctor.com/wp-content/themes/jupiter/assets/images",
         mk_theme_js_path = "https://thelaundrydoctor.com/wp-content/themes/jupiter/assets/js",
         mk_theme_dir = "https://thelaundrydoctor.com/wp-content/themes/jupiter",
         mk_captcha_placeholder = "Enter Captcha",
         mk_captcha_invalid_txt = "Invalid. Try again.",
         mk_captcha_correct_txt = "Captcha correct.",
         mk_responsive_nav_width = 1140,
         mk_vertical_header_back = "Back",
         mk_vertical_header_anim = "1",
         mk_check_rtl = true,
         mk_grid_width = 1140,
         mk_ajax_search_option = "disable",
         mk_preloader_bg_color = "#fff",
         mk_accent_color = "#445aa2",
         mk_go_to_top =  "true",
         mk_smooth_scroll =  "false",
         mk_preloader_bar_color = "#445aa2",
         mk_preloader_logo = "";
         var mk_header_parallax = false,
         mk_banner_parallax = false,
         mk_footer_parallax = false,
         mk_body_parallax = false,
         mk_no_more_posts = "No More Posts",
         mk_typekit_id   = "",
         mk_google_fonts = [],
         mk_global_lazyload = false;
      </script>
      <link rel='stylesheet' id='font-awesome-4-css'  href='//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='core-styles-css'  href='/css/lockermd/core-styles.6.1.1.css' type='text/css' media='all' />
      <link rel='stylesheet' id='theme-options-css'  href='/css/lockermd/theme-options-production-1520563127.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mk-style-css'  href='/css/lockermd/style.css' type='text/css' media='all' />
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slide_in = {"demo_dir":"https:\/\/thelaundrydoctor.com\/wp-content\/plugins\/convertplug\/modules\/slide_in\/assets\/demos"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='/js/lockermd/jquery.js'></script>
      <script type='text/javascript' src='/js/lockermd/jquery-migrate.min.js'></script>
      <script type='text/javascript' src='/js/lockermd/webfontloader.js'></script>
      <script type='text/javascript'>
         WebFontConfig = {
             timeout: 2000
         }
         
         if ( mk_typekit_id.length > 0 ) {
             WebFontConfig.typekit = {
                 id: mk_typekit_id
             }
         }
         
         if ( mk_google_fonts.length > 0 ) {
             WebFontConfig.google = {
                 families:  mk_google_fonts
             }
         }
         
         if ( (mk_google_fonts.length > 0 || mk_typekit_id.length > 0) && navigator.userAgent.indexOf("Speed Insights") == -1) {
             WebFont.load( WebFontConfig );
         }
                 
      </script>
      <link rel='shortlink' href='https://thelaundrydoctor.com/' />
      <script type="text/javascript">
         (function(url){
             if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
             var addEvent = function(evt, handler) {
                 if (window.addEventListener) {
                     document.addEventListener(evt, handler, false);
                 } else if (window.attachEvent) {
                     document.attachEvent('on' + evt, handler);
                 }
             };
             var removeEvent = function(evt, handler) {
                 if (window.removeEventListener) {
                     document.removeEventListener(evt, handler, false);
                 } else if (window.detachEvent) {
                     document.detachEvent('on' + evt, handler);
                 }
             };
             var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
             var logHuman = function() {
                 var wfscr = document.createElement('script');
                 wfscr.type = 'text/javascript';
                 wfscr.async = true;
                 wfscr.src = url + '&r=' + Math.random();
                 (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
                 for (var i = 0; i < evts.length; i++) {
                     removeEvent(evts[i], logHuman);
                 }
             };
             for (var i = 0; i < evts.length; i++) {
                 addEvent(evts[i], logHuman);
             }
         })('//thelaundrydoctor.com/?wordfence_lh=1&hid=44A87D3575FF0004333D4AF64AEBB6B4');
      </script> 
      <!--[if lte IE 9]>
      <link rel="stylesheet" type="text/css" href="/css/lockermd/vc_lte_ie9.min.css" media="screen">
      <![endif]-->
      <?php if (ENVIRONMENT == 'production'): ?>
      <!-- BEGIN GADWP v5.3.1.1 Universal Analytics - https://deconf.com/google-analytics-dashboard-wordpress/ -->
      <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
           ga('create', 'UA-114371046-1', 'auto');
           ga('send', 'pageview');
      </script>
      <!-- END GADWP Universal Analytics -->
      </script>
      <?php endif; ?>
      <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
      <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
      <link rel="stylesheet" href="/css/lockermd/custom.css">
      <script src='/bootstrap/js/bootstrap.min.js'></script>
      <script type="text/javascript">
         var $ = jQuery.noConflict();
      </script>
      <?= $_scripts ?>
      <?= $javascript?>
      <?= $style?>
      <?= $_styles ?>
   </head>
   <body data-rsssl=1 class="home page-template-default page page-id-1147 wpb-js-composer js-comp-ver-5.4.5 vc_responsive" itemscope="itemscope" itemtype="https://schema.org/WebPage"  data-adminbar="">
      <!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
      <div id="top-of-page"></div>
      <div id="mk-boxed-layout">
         <div id="mk-theme-container" >
            <header data-height='75'
               data-sticky-height='70'
               data-responsive-height='75'
               data-transparent-skin=''
               data-header-style='1'
               data-sticky-style='fixed'
               data-sticky-offset='100%' id="mk-header-1" class="mk-header header-style-1 header-align-center js-logo-middle logo-in-middle toolbar-true menu-hover-5 sticky-style-fixed  boxed-header " role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader" >
               <div class="mk-header-holder">
                  <div class="mk-header-toolbar">
                     <div class="mk-grid header-grid">
                        <div class="mk-header-toolbar-holder">
                           <span class="header-toolbar-contact">
                              <svg  class="mk-svg-icon" data-name="mk-moon-phone-3" data-cacheid="icon-5aa7cac978251" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                 <path d="M457.153 103.648c53.267 30.284 54.847 62.709 54.849 85.349v3.397c0 5.182-4.469 9.418-9.928 9.418h-120.146c-5.459 0-9.928-4.236-9.928-9.418v-11.453c0-28.605-27.355-33.175-42.449-35.605-15.096-2.426-52.617-4.777-73.48-4.777h-.14300000000000002c-20.862 0-58.387 2.35-73.48 4.777-15.093 2.427-42.449 6.998-42.449 35.605v11.453c0 5.182-4.469 9.418-9.926 9.418h-120.146c-5.457 0-9.926-4.236-9.926-9.418v-3.397c0-22.64 1.58-55.065 54.847-85.349 63.35-36.01 153.929-39.648 201.08-39.648l.077.078.066-.078c47.152 0 137.732 3.634 201.082 39.648zm-201.152 88.352c-28.374 0-87.443 2.126-117.456 38.519-30.022 36.383-105.09 217.481-38.147 217.481h311.201c66.945 0-8.125-181.098-38.137-217.481-30.018-36.393-89.1-38.519-117.461-38.519zm-.001 192c-35.346 0-64-28.653-64-64s28.654-64 64-64c35.347 0 64 28.653 64 64s-28.653 64-64 64z"/>
                              </svg>
                              <a href="tel:(651)209-6397">(651) 209-6397 </a>
                           </span>
                           <span class="header-toolbar-contact">
                              <svg  class="mk-svg-icon" data-name="mk-moon-envelop" data-cacheid="icon-5aa7cac9787cf" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                 <path d="M480 64h-448c-17.6 0-32 14.4-32 32v320c0 17.6 14.4 32 32 32h448c17.6 0 32-14.4 32-32v-320c0-17.6-14.4-32-32-32zm-32 64v23l-192 113.143-192-113.143v-23h384zm-384 256v-177.286l192 113.143 192-113.143v177.286h-384z"/>
                              </svg>
                              <a href="mailto:sup&#112;ort&#64;&#108;o&#99;&#107;e&#114;m&#100;.c&#111;&#109;">s&#117;p&#112;&#111;rt&#64;l&#111;c&#107;&#101;rm&#100;&#46;&#99;om</a>
                           </span>
                           <div class="mk-header-social toolbar-section">
                              <ul>
                                 <li>
                                    <a class="facebook-hover " target="_blank" href="https://www.facebook.com/Laundry.Dr/">
                                       <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-facebook" data-cacheid="icon-5aa7cac978b9f" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                          <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm64.057 159.299h-49.041c-7.42 0-14.918 7.452-14.918 12.99v19.487h63.723c-2.081 28.41-6.407 64.679-6.407 64.679h-57.566v159.545h-63.929v-159.545h-32.756v-64.474h32.756v-33.53c0-8.098-1.706-62.336 70.46-62.336h57.678v63.183z"/>
                                       </svg>
                                       </i>
                                    </a>
                                 </li>
                                 <li>
                                    <a class="twitter-hover " target="_blank" href="https://twitter.com/LaundryDoctor">
                                       <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-twitter" data-cacheid="icon-5aa7cac978cf6" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                          <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm146.24 258.654c-31.365 127.03-241.727 180.909-338.503 49.042 37.069 35.371 101.619 38.47 142.554-3.819-24.006 3.51-41.47-20.021-11.978-32.755-26.523 2.923-41.27-11.201-47.317-23.174 6.218-6.511 13.079-9.531 26.344-10.407-29.04-6.851-39.751-21.057-43.046-38.284 8.066-1.921 18.149-3.578 23.656-2.836-25.431-13.295-34.274-33.291-32.875-48.326 45.438 16.866 74.396 30.414 98.613 43.411 8.626 4.591 18.252 12.888 29.107 23.393 13.835-36.534 30.915-74.19 60.169-92.874-.493 4.236-2.758 8.179-5.764 11.406 8.298-7.535 19.072-12.719 30.027-14.216-1.257 8.22-13.105 12.847-20.249 15.539 5.414-1.688 34.209-14.531 37.348-7.216 3.705 8.328-19.867 12.147-23.872 13.593-2.985 1.004-5.992 2.105-8.936 3.299 36.492-3.634 71.317 26.456 81.489 63.809.719 2.687 1.44 5.672 2.1 8.801 13.341 4.978 37.521-.231 45.313-5.023-5.63 13.315-20.268 23.121-41.865 24.912 10.407 4.324 30.018 6.691 43.544 4.396-8.563 9.193-22.379 17.527-45.859 17.329z"/>
                                       </svg>
                                       </i>
                                    </a>
                                 </li>
                                 <li>
                                    <a class="rss-hover " target="_blank" href="https://thelaundrydoctor.com/feed/">
                                       <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-rss" data-cacheid="icon-5aa7cac97a95e" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                          <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm-90.203 384.325c-20.801 0-37.663-16.861-37.663-37.663 0-20.801 16.862-37.663 37.663-37.663 20.8 0 37.663 16.862 37.663 37.663-.001 20.802-16.863 37.663-37.663 37.663zm97.299-.324c0-36.053-14.04-69.944-39.531-95.438-25.493-25.49-59.387-39.531-95.438-39.531l-.195.003v-54.029l.195-.001c104.38 0 188.998 84.616 188.998 188.997h-54.029zm98.873-.001c0-31.594-6.174-62.213-18.353-91.007-11.776-27.839-28.645-52.853-50.138-74.345-21.493-21.494-46.507-38.363-74.347-50.138-28.795-12.18-59.414-18.355-91.006-18.355l-.195.001v-54.029h.195c158.99 0 287.875 128.886 287.875 287.873h-54.031z"/>
                                       </svg>
                                       </i>
                                    </a>
                                 </li>
                                 <li>
                                    <a class="wordpress-hover " target="_blank" href="https://thelaundrydoctor.com/blog/">
                                       <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-wordpress" data-cacheid="icon-5aa7cac97aad9" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                          <path d="M351.791 244.102c0-17.582-6.335-29.748-11.728-39.209-7.204-11.705-13.952-21.615-13.952-33.342 0-13.055 9.879-25.213 23.875-25.213.606 0 1.215.071 1.822.123-25.274-23.185-58.967-37.34-95.978-37.34-49.648 0-93.344 25.49-118.767 64.088 3.343.093 6.475.167 9.14.167 14.881 0 37.899-1.825 37.899-1.825 7.64-.423 8.577 10.821.911 11.726 0 0-7.715.897-16.284 1.359l51.776 153.948 31.089-93.269-22.12-60.679c-7.662-.462-14.924-1.359-14.924-1.359-7.644-.444-6.756-12.149.906-11.726 0 0 23.463 1.825 37.429 1.825 14.858 0 37.898-1.825 37.898-1.825 7.644-.423 8.551 10.821.897 11.726 0 0-7.698.897-16.274 1.359l51.385 152.778 14.18-47.356c7.205-18.474 10.822-33.79 10.822-45.957zm-93.458 19.626l-42.666 123.928c12.734 3.738 26.211 5.771 40.163 5.771 16.539 0 32.435-2.858 47.188-8.067-.369-.599-.721-1.233-.994-1.928l-43.692-119.704zm68.966 110.397c42.243-24.622 70.66-70.404 70.66-122.853 0-24.721-6.317-47.946-17.41-68.201.642 4.535.986 9.387.986 14.62 0 14.414-2.718 30.632-10.813 50.91l-43.423 125.523zm-71.299-380.525c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm-.038 416.436c-88.478 0-160.462-71.979-160.462-160.463 0-88.478 71.983-160.462 160.462-160.462 88.483 0 160.462 71.984 160.462 160.462 0 88.483-71.98 160.463-160.463 160.463zm-142.283-158.764c0 56.258 32.708 104.907 80.117 127.921l-67.805-185.776c-7.878 17.691-12.312 37.247-12.312 57.855z"/>
                                       </svg>
                                       </i>
                                    </a>
                                 </li>
                                 <li>
                                    <a class="yelp-hover " target="_blank" href="https://www.yelp.com/biz/the-laundry-doctor-st-paul-2">
                                       <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-yelp" data-cacheid="icon-5aa7cac97ac34" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                          <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm-85.5 111.2c17.394-12.656 50.835-16.277 64.599-13.809 13.768 2.457 20.962 9.581 21.06 18.264l.781 110.677c.063 8.688-4.809 16.674-10.843 17.746-6.06 1.054-14.982-4.113-19.888-11.507l-62.75-94.892c-4.869-7.408-14.675-10.674 7.042-26.48zm-42.401 197.426c-1.588-8.887 0-51.824 0-51.824 2.645-12.331 12.668-19.866 21.352-16.686l66.734 24.489c8.678 3.18 15.081 11.467 14.224 18.45-.884 6.983-8.77 15.019-17.574 17.907l-63.173 20.688c-8.803 2.897-19.953-4.135-21.564-13.024zm130.336 92.128c-.173 8.69-15.628 17.428-24.398 14.478l-46.082-14.817c-8.75-2.95-11.731-18.56-5.73-25.202l44.071-48.907c6-6.632 16.056-11.637 22.393-11.096 6.332.541 11.351 8.065 11.178 16.783l-1.432 68.762zm122.325-57.073c-3.871 10.756-24.882 39.447-24.882 39.447-4.976 7.385-23.006 8.688-27.514 1.083l-34.866-59.062c-4.51-7.615-5.367-16.04-1.92-18.708 3.438-2.699 13.606-2.92 22.56-.544l55.26 14.708c8.956 2.378 15.233 12.291 11.363 23.075zm-6.885-83.419l-61.59 10.574c-9.108 1.887-19.829-1.596-23.853-7.767-4.022-6.149-2.406-16.634 3.622-23.234l35.615-39.262c6.027-6.606 16.718-7.55 26.063-.025 9.356 7.505 26.639 29.535 29.132 37.01 2.526 7.464 2.125 20.235-8.988 22.703z"/>
                                       </svg>
                                       </i>
                                    </a>
                                 </li>
                              </ul>
                              <div class="clearboth"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="mk-header-inner add-header-height">
                     <div class="mk-header-bg "></div>
                     <div class="mk-toolbar-resposnive-icon">
                        <svg  class="mk-svg-icon" data-name="mk-icon-chevron-down" data-cacheid="icon-5aa7cac97ae89" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
                           <path d="M1683 808l-742 741q-19 19-45 19t-45-19l-742-741q-19-19-19-45.5t19-45.5l166-165q19-19 45-19t45 19l531 531 531-531q19-19 45-19t45 19l166 165q19 19 19 45.5t-19 45.5z"/>
                        </svg>
                     </div>
                     <div class="mk-grid header-grid">
                        <div class="mk-header-nav-container one-row-style menu-hover-style-5" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" >
                           <nav class="mk-main-navigation js-main-nav">
                              <ul id="menu-primary-navigation" class="main-navigation-ul">
                                 <li id="menu-item-10917" class="menu-item menu-item-type-post_type menu-item-object-page no-mega-menu"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/24-7-laundry-lockers/">24/7 Laundry Lockers</a></li>
                                 <li id="menu-item-10916" class="menu-item menu-item-type-post_type menu-item-object-page no-mega-menu"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/laundry-pickup-delivery/">Home Delivery</a></li>
                                 <li id="menu-item-1141" class="menu-item menu-item-type-post_type menu-item-object-page no-mega-menu"><a class="menu-item-link js-smooth-scroll"  title="Pricing" href="https://thelaundrydoctor.com/laundry-services/">Pricing</a></li>
                                 <li class=" nav-middle-logo menu-item fit-logo-img add-header-height  ">
                                    <a href="https://thelaundrydoctor.com/" title="Dry Cleaning &amp; Laundry Delivery Service">
                                    <img class="mk-desktop-logo dark-logo "
                                       title="Serving Twin Cities area homes, businesses, colleges, and universities."
                                       alt="Serving Twin Cities area homes, businesses, colleges, and universities."
                                       src="/images/lockermd/lockermd-logo2-centered.png" />
                                    </a>
                                 </li>
                                 <li id="menu-item-11043" class="menu-item menu-item-type-post_type menu-item-object-page has-mega-menu"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/laundry-service-property-managers/">Property Managers</a></li>
                                 <li id="menu-item-11316" class="menu-item menu-item-type-post_type menu-item-object-page no-mega-menu"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/package-delivery/">Package Delivery</a></li>
                                 <li id="menu-item-1193" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children no-mega-menu">
                                    <a class="menu-item-link js-smooth-scroll"  target="_blank" href="https://myaccount.lockermd.com/login">Login</a>
                                    <ul style="" class="sub-menu ">
                                       <li id="menu-item-11236" class="menu-item menu-item-type-custom menu-item-object-custom"><a class="menu-item-link js-smooth-scroll"  href="https://myaccount.lockermd.com/register">Create Account</a></li>
                                    </ul>
                                 </li>
                              </ul>
                           </nav>
                        </div>
                        <div class="mk-nav-responsive-link">
                           <div class="mk-css-icon-menu">
                              <div class="mk-css-icon-menu-line-1"></div>
                              <div class="mk-css-icon-menu-line-2"></div>
                              <div class="mk-css-icon-menu-line-3"></div>
                           </div>
                        </div>
                        <div class=" header-logo fit-logo-img add-header-height  ">
                           <a href="https://thelaundrydoctor.com/" title="Dry Cleaning &amp; Laundry Delivery Service">
                           <img class="mk-desktop-logo dark-logo "
                              title="Serving Twin Cities area homes, businesses, colleges, and universities."
                              alt="Serving Twin Cities area homes, businesses, colleges, and universities."
                              src="/images/lockermd/lockermd-logo2-centered.png" />
                           </a>
                        </div>
                     </div>
                     <div class="mk-header-right">
                     </div>
                  </div>
                  <div class="mk-responsive-wrap">
                     <nav class="menu-primary-navigation-container">
                        <ul id="menu-primary-navigation-1" class="mk-responsive-nav">
                           <li id="responsive-menu-item-10917" class="menu-item menu-item-type-post_type menu-item-object-page"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/24-7-laundry-lockers/">24/7 Laundry Lockers</a></li>
                           <li id="responsive-menu-item-10916" class="menu-item menu-item-type-post_type menu-item-object-page"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/laundry-pickup-delivery/">Home Delivery</a></li>
                           <li id="responsive-menu-item-1141" class="menu-item menu-item-type-post_type menu-item-object-page"><a class="menu-item-link js-smooth-scroll"  title="Pricing" href="https://thelaundrydoctor.com/laundry-services/">Pricing</a></li>
                           <li id="responsive-menu-item-11043" class="menu-item menu-item-type-post_type menu-item-object-page"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/laundry-service-property-managers/">Property Managers</a></li>
                           <li id="responsive-menu-item-11316" class="menu-item menu-item-type-post_type menu-item-object-page"><a class="menu-item-link js-smooth-scroll"  href="https://thelaundrydoctor.com/package-delivery/">Package Delivery</a></li>
                           <li id="responsive-menu-item-1193" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                              <a class="menu-item-link js-smooth-scroll"  target="_blank" href="https://myaccount.lockermd.com/login">Login</a>
                              <span class="mk-nav-arrow mk-nav-sub-closed">
                                 <svg  class="mk-svg-icon" data-name="mk-moon-arrow-down" data-cacheid="icon-5aa7cac983c6d" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M512 192l-96-96-160 160-160-160-96 96 256 255.999z"/>
                                 </svg>
                              </span>
                              <ul class="sub-menu ">
                                 <li id="responsive-menu-item-11236" class="menu-item menu-item-type-custom menu-item-object-custom"><a class="menu-item-link js-smooth-scroll"  href="https://myaccount.lockermd.com/register">Create Account</a></li>
                              </ul>
                           </li>
                        </ul>
                     </nav>
                  </div>
               </div>
               <div class="mk-header-padding-wrapper"></div>
            </header>
            <div id="main-content">
               <?= $content ?>
            </div>
            <section id="mk-footer-unfold-spacer"></section>
            <section id="mk-footer" class="" role="contentinfo" itemscope="itemscope" itemtype="https://schema.org/WPFooter" >
               <div class="footer-wrapper mk-grid">
                  <div class="mk-padding-wrapper">
                     <div class="mk-col-1-5">
                        <section id="media_image-2" class="widget widget_media_image">
                           <img width="231" height="231" src="/images/lockermd/footer-laundry-doctor-logo-circle.png" class="image wp-image-11108  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" srcset="/images/lockermd/footer-laundry-doctor-logo-circle.png 231w, /images/lockermd/footer-laundry-doctor-logo-circle-150x150.png 150w, /images/lockermd/footer-laundry-doctor-logo-circle-120x120.png 120w" sizes="(max-width: 231px) 100vw, 231px" itemprop="image" />
                        </section>
                     </div>
                     <div class="mk-col-1-5">
                        <section id="nav_menu-7" class="widget widget_nav_menu">
                           <div class="widgettitle">Services</div>
                           <div class="menu-footer-services-container">
                              <ul id="menu-footer-services" class="menu">
                                 <li id="menu-item-10734" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10734">
                                    <a href="https://thelaundrydoctor.com/laundry/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e15d2" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       How We Do Laundry
                                    </a>
                                 </li>
                                 <li id="menu-item-10735" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10735">
                                    <a href="https://thelaundrydoctor.com/dry-cleaners/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e15d2" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Dry Cleaners
                                    </a>
                                 </li>
                                 <li id="menu-item-10919" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10919">
                                    <a href="https://thelaundrydoctor.com/24-7-laundry-lockers/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e15d2" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       24/7 Laundry Lockers
                                    </a>
                                 </li>
                                 <li id="menu-item-10918" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10918">
                                    <a href="https://thelaundrydoctor.com/laundry-pickup-delivery/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e15d2" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Free Laundry Pickup &amp; Delivery
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </section>
                     </div>
                     <div class="mk-col-1-5">
                        <section id="nav_menu-8" class="widget widget_nav_menu">
                           <div class="widgettitle">Information</div>
                           <div class="menu-footer-information-container">
                              <ul id="menu-footer-information" class="menu">
                                 <li id="menu-item-10726" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10726">
                                    <a href="https://thelaundrydoctor.com/laundry-services/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Services &amp; Pricing
                                    </a>
                                 </li>
                                 <li id="menu-item-10728" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10728">
                                    <a href="https://thelaundrydoctor.com/service-area/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Service Area
                                    </a>
                                 </li>
                                 <li id="menu-item-10727" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10727">
                                    <a href="https://thelaundrydoctor.com/faq/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       FAQ
                                    </a>
                                 </li>
                                 <li id="menu-item-11068" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11068">
                                    <a href="https://thelaundrydoctor.com/for-your-employees/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       For Your Employees
                                    </a>
                                 </li>
                                 <li id="menu-item-10724" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10724">
                                    <a href="https://thelaundrydoctor.com/laundry-service-property-managers/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       For Property Managers
                                    </a>
                                 </li>
                                 <li id="menu-item-11067" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11067">
                                    <a href="https://thelaundrydoctor.com/laundry-service-cleaning-food-service/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Cleaning and Food Service
                                    </a>
                                 </li>
                                 <li id="menu-item-11315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11315">
                                    <a href="https://thelaundrydoctor.com/package-delivery/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9e8ee8" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Package Delivery
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </section>
                     </div>
                     <div class="mk-col-1-5">
                        <section id="nav_menu-9" class="widget widget_nav_menu">
                           <div class="widgettitle">Company</div>
                           <div class="menu-footer-company-container">
                              <ul id="menu-footer-company" class="menu">
                                 <li id="menu-item-10743" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10743">
                                    <a href="https://thelaundrydoctor.com/refer-a-friend/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9f1f71" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Refer a Friend
                                    </a>
                                 </li>
                                 <li id="menu-item-10926" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10926">
                                    <a href="https://thelaundrydoctor.com/about/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9f1f71" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       About Us
                                    </a>
                                 </li>
                                 <li id="menu-item-10739" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10739">
                                    <a href="https://thelaundrydoctor.com/contact-us/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9f1f71" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Contact Us
                                    </a>
                                 </li>
                                 <li id="menu-item-10752" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10752">
                                    <a href="https://thelaundrydoctor.com/blog/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9f1f71" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Blog
                                    </a>
                                 </li>
                                 <li id="menu-item-10751" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10751">
                                    <a href="https://thelaundrydoctor.com/terms-of-service/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9f1f71" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Terms of Service
                                    </a>
                                 </li>
                                 <li id="menu-item-10750" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10750">
                                    <a href="https://thelaundrydoctor.com/privacy-policy/">
                                       <svg class="mk-svg-icon" data-name="mk-icon-angle-right" data-cacheid="icon-5aa7cac9f1f71" style=" height:14px; width: 5px; " xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 1792">
                                          <path d="M595 960q0 13-10 23l-466 466q-10 10-23 10t-23-10l-50-50q-10-10-10-23t10-23l393-393-393-393q-10-10-10-23t10-23l50-50q10-10 23-10t23 10l466 466q10 10 10 23z"></path>
                                       </svg>
                                       Privacy Policy
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </section>
                     </div>
                     <div class="mk-col-1-5">
                        <section id="contact_info-2" class="widget widget_contact_info">
                           <div class="widgettitle">Contact Us</div>
                           <ul itemscope="itemscope" itemtype="https://schema.org/Person" >
                              <li>
                                 <svg  class="mk-svg-icon" data-name="mk-moon-office" data-cacheid="icon-5aa7cac9f27d2" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M0 512h256v-512h-256v512zm160-448h64v64h-64v-64zm0 128h64v64h-64v-64zm0 128h64v64h-64v-64zm-128-256h64v64h-64v-64zm0 128h64v64h-64v-64zm0 128h64v64h-64v-64zm256-160h224v32h-224zm0 352h64v-128h96v128h64v-288h-224z"/>
                                 </svg>
                                 <span itemprop="jobTitle">The Laundry Doctor</span>
                              </li>
                              <li>
                                 <svg  class="mk-svg-icon" data-name="mk-icon-home" data-cacheid="icon-5aa7cac9f29ad" style=" height:16px; width: 14.857142857143px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1664 1792">
                                    <path d="M1408 992v480q0 26-19 45t-45 19h-384v-384h-256v384h-384q-26 0-45-19t-19-45v-480q0-1 .5-3t.5-3l575-474 575 474q1 2 1 6zm223-69l-62 74q-8 9-21 11h-3q-13 0-21-7l-692-577-692 577q-12 8-24 7-13-2-21-11l-62-74q-8-10-7-23.5t11-21.5l719-599q32-26 76-26t76 26l244 204v-195q0-14 9-23t23-9h192q14 0 23 9t9 23v408l219 182q10 8 11 21.5t-7 23.5z"/>
                                 </svg>
                                 <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress"><a href="https://www.google.com/maps/dir/''/the+laundry+doctor/data=!4m5!4m4!1m0!1m2!1m1!1s0x87f62a8fd988f94d:0x549ba8c624a9fa07?sa=X&ved=0ahUKEwiNg8GOv53ZAhVS7qwKHS_ACwYQ9RcImwEwCw">662 Selby Avenue<br/>St. Paul, MN 55104</a></span>
                              </li>
                              <li>
                                 <svg  class="mk-svg-icon" data-name="mk-icon-phone" data-cacheid="icon-5aa7cac9f2b7b" style=" height:16px; width: 12.571428571429px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1408 1792">
                                    <path d="M1408 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-52.5-3.5t-57.5-12.5-47.5-14.5-55.5-20.5-49-18q-98-35-175-83-128-79-264.5-215.5t-215.5-264.5q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47.5-12.5-57.5-3.5-52.5q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z"/>
                                 </svg>
                                 <span>(651) 209-6397</span>
                              </li>
                              <li>
                                 <svg  class="mk-svg-icon" data-name="mk-icon-envelope" data-cacheid="icon-5aa7cac9f2d59" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
                                    <path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"/>
                                 </svg>
                                 <span>
                                 <a itemprop="email" href="mailto:support&#64;lockermd.com">support&#64;lockermd.com</a></span>
                              </li>
                              <li>
                                 <svg  class="mk-svg-icon" data-name="mk-icon-globe" data-cacheid="icon-5aa7cac9f2f10" style=" height:16px; width: 13.714285714286px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1536 1792">
                                    <path d="M768 128q209 0 385.5 103t279.5 279.5 103 385.5-103 385.5-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103zm274 521q-2 1-9.5 9.5t-13.5 9.5q2 0 4.5-5t5-11 3.5-7q6-7 22-15 14-6 52-12 34-8 51 11-2-2 9.5-13t14.5-12q3-2 15-4.5t15-7.5l2-22q-12 1-17.5-7t-6.5-21q0 2-6 8 0-7-4.5-8t-11.5 1-9 1q-10-3-15-7.5t-8-16.5-4-15q-2-5-9.5-10.5t-9.5-10.5q-1-2-2.5-5.5t-3-6.5-4-5.5-5.5-2.5-7 5-7.5 10-4.5 5q-3-2-6-1.5t-4.5 1-4.5 3-5 3.5q-3 2-8.5 3t-8.5 2q15-5-1-11-10-4-16-3 9-4 7.5-12t-8.5-14h5q-1-4-8.5-8.5t-17.5-8.5-13-6q-8-5-34-9.5t-33-.5q-5 6-4.5 10.5t4 14 3.5 12.5q1 6-5.5 13t-6.5 12q0 7 14 15.5t10 21.5q-3 8-16 16t-16 12q-5 8-1.5 18.5t10.5 16.5q2 2 1.5 4t-3.5 4.5-5.5 4-6.5 3.5l-3 2q-11 5-20.5-6t-13.5-26q-7-25-16-30-23-8-29 1-5-13-41-26-25-9-58-4 6-1 0-15-7-15-19-12 3-6 4-17.5t1-13.5q3-13 12-23 1-1 7-8.5t9.5-13.5.5-6q35 4 50-11 5-5 11.5-17t10.5-17q9-6 14-5.5t14.5 5.5 14.5 5q14 1 15.5-11t-7.5-20q12 1 3-17-5-7-8-9-12-4-27 5-8 4 2 8-1-1-9.5 10.5t-16.5 17.5-16-5q-1-1-5.5-13.5t-9.5-13.5q-8 0-16 15 3-8-11-15t-24-8q19-12-8-27-7-4-20.5-5t-19.5 4q-5 7-5.5 11.5t5 8 10.5 5.5 11.5 4 8.5 3q14 10 8 14-2 1-8.5 3.5t-11.5 4.5-6 4q-3 4 0 14t-2 14q-5-5-9-17.5t-7-16.5q7 9-25 6l-10-1q-4 0-16 2t-20.5 1-13.5-8q-4-8 0-20 1-4 4-2-4-3-11-9.5t-10-8.5q-46 15-94 41 6 1 12-1 5-2 13-6.5t10-5.5q34-14 42-7l5-5q14 16 20 25-7-4-30-1-20 6-22 12 7 12 5 18-4-3-11.5-10t-14.5-11-15-5q-16 0-22 1-146 80-235 222 7 7 12 8 4 1 5 9t2.5 11 11.5-3q9 8 3 19 1-1 44 27 19 17 21 21 3 11-10 18-1-2-9-9t-9-4q-3 5 .5 18.5t10.5 12.5q-7 0-9.5 16t-2.5 35.5-1 23.5l2 1q-3 12 5.5 34.5t21.5 19.5q-13 3 20 43 6 8 8 9 3 2 12 7.5t15 10 10 10.5q4 5 10 22.5t14 23.5q-2 6 9.5 20t10.5 23q-1 0-2.5 1t-2.5 1q3 7 15.5 14t15.5 13q1 3 2 10t3 11 8 2q2-20-24-62-15-25-17-29-3-5-5.5-15.5t-4.5-14.5q2 0 6 1.5t8.5 3.5 7.5 4 2 3q-3 7 2 17.5t12 18.5 17 19 12 13q6 6 14 19.5t0 13.5q9 0 20 10t17 20q5 8 8 26t5 24q2 7 8.5 13.5t12.5 9.5l16 8 13 7q5 2 18.5 10.5t21.5 11.5q10 4 16 4t14.5-2.5 13.5-3.5q15-2 29 15t21 21q36 19 55 11-2 1 .5 7.5t8 15.5 9 14.5 5.5 8.5q5 6 18 15t18 15q6-4 7-9-3 8 7 20t18 10q14-3 14-32-31 15-49-18 0-1-2.5-5.5t-4-8.5-2.5-8.5 0-7.5 5-3q9 0 10-3.5t-2-12.5-4-13q-1-8-11-20t-12-15q-5 9-16 8t-16-9q0 1-1.5 5.5t-1.5 6.5q-13 0-15-1 1-3 2.5-17.5t3.5-22.5q1-4 5.5-12t7.5-14.5 4-12.5-4.5-9.5-17.5-2.5q-19 1-26 20-1 3-3 10.5t-5 11.5-9 7q-7 3-24 2t-24-5q-13-8-22.5-29t-9.5-37q0-10 2.5-26.5t3-25-5.5-24.5q3-2 9-9.5t10-10.5q2-1 4.5-1.5t4.5 0 4-1.5 3-6q-1-1-4-3-3-3-4-3 7 3 28.5-1.5t27.5 1.5q15 11 22-2 0-1-2.5-9.5t-.5-13.5q5 27 29 9 3 3 15.5 5t17.5 5q3 2 7 5.5t5.5 4.5 5-.5 8.5-6.5q10 14 12 24 11 40 19 44 7 3 11 2t4.5-9.5 0-14-1.5-12.5l-1-8v-18l-1-8q-15-3-18.5-12t1.5-18.5 15-18.5q1-1 8-3.5t15.5-6.5 12.5-8q21-19 15-35 7 0 11-9-1 0-5-3t-7.5-5-4.5-2q9-5 2-16 5-3 7.5-11t7.5-10q9 12 21 2 7-8 1-16 5-7 20.5-10.5t18.5-9.5q7 2 8-2t1-12 3-12q4-5 15-9t13-5l17-11q3-4 0-4 18 2 31-11 10-11-6-20 3-6-3-9.5t-15-5.5q3-1 11.5-.5t10.5-1.5q15-10-7-16-17-5-43 12zm-163 877q206-36 351-189-3-3-12.5-4.5t-12.5-3.5q-18-7-24-8 1-7-2.5-13t-8-9-12.5-8-11-7q-2-2-7-6t-7-5.5-7.5-4.5-8.5-2-10 1l-3 1q-3 1-5.5 2.5t-5.5 3-4 3 0 2.5q-21-17-36-22-5-1-11-5.5t-10.5-7-10-1.5-11.5 7q-5 5-6 15t-2 13q-7-5 0-17.5t2-18.5q-3-6-10.5-4.5t-12 4.5-11.5 8.5-9 6.5-8.5 5.5-8.5 7.5q-3 4-6 12t-5 11q-2-4-11.5-6.5t-9.5-5.5q2 10 4 35t5 38q7 31-12 48-27 25-29 40-4 22 12 26 0 7-8 20.5t-7 21.5q0 6 2 16z"/>
                                 </svg>
                                 <span><a href="https://thelaundrydoctor.com" itemprop="url">thelaundrydoctor.com</a></span>
                              </li>
                           </ul>
                        </section>
                        <section id="social-2" class="widget widget_social_networks">
                           <div class="widgettitle">Find Us Online</div>
                           <div id="social-5aa7cac9f3047" class="align-left">
                              <a href="#" rel="nofollow" class="builtin-icons light medium apple-hover" target="_blank" alt=" apple" title=" apple">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-apple" data-cacheid="icon-5aa7cac9f318e" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm11.916 104.603c12.156-12.002 32.705-20.914 49.664-21.512 2.156 16.654-5.81 33.344-17.57 45.365-11.781 12.002-31.067 21.349-50.008 20.095-2.564-16.305 6.978-33.293 17.913-43.948zm96.447 254.532c-14.069 19.19-28.616 38.303-51.594 38.678-22.605.401-29.858-19.106-55.654-19.106-25.819 0-33.916 18.731-55.259 19.507-22.176.776-39.042-20.742-53.222-39.858-28.972-39.096-51.087-110.587-21.343-158.805 14.719-23.963 41.118-39.107 69.712-39.514 21.781-.369 42.36 13.714 55.655 13.714 13.336 0 38.31-16.906 64.565-14.433 10.996.426 41.841 4.138 61.647 31.207-1.555.967-36.815 20.108-36.387 60.02.418 47.689 44.74 63.526 45.248 63.734-.406 1.135-7.091 22.616-23.366 44.856z"/>
                                 </svg>
                              </a>
                              <a href="https://www.facebook.com/Laundry.Dr/" rel="nofollow" class="builtin-icons light medium facebook-hover" target="_blank" alt=" facebook" title=" facebook">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-facebook" data-cacheid="icon-5aa7cac9f32d8" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm64.057 159.299h-49.041c-7.42 0-14.918 7.452-14.918 12.99v19.487h63.723c-2.081 28.41-6.407 64.679-6.407 64.679h-57.566v159.545h-63.929v-159.545h-32.756v-64.474h32.756v-33.53c0-8.098-1.706-62.336 70.46-62.336h57.678v63.183z"/>
                                 </svg>
                              </a>
                              <a href="https://www.google.com/search?q=the+laundry+doctor&#038;oq=the+laundry+doctor&#038;aqs=chrome.0.69i59l2j69i60l3j0.3328j0j7&#038;sourceid=chrome&#038;ie=UTF-8#lrd=0x87f62a8fd988f94d:0x549ba8c624a9fa07,1,,," rel="nofollow" class="builtin-icons light medium google-hover" target="_blank" alt=" google" title=" google">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-google" data-cacheid="icon-5aa7cac9f343d" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M257.197 103.407c-10.982 0-22.797 5.462-29.553 13.868-7.165 8.831-9.277 20.157-9.277 31.086 0 28.134 16.46 74.767 52.752 74.767 10.551 0 21.963-5.038 28.724-11.751 9.699-9.678 10.56-23.104 10.56-30.679 0-30.237-18.166-77.29-53.207-77.29zm20.684 187.364c-3.797 0-26.589.831-44.332 6.736-9.278 3.358-36.298 13.416-36.298 43.256 0 29.822 29.137 51.238 74.296 51.238 40.533 0 62.09-19.322 62.09-45.351 0-21.416-13.933-32.784-46.037-55.473-3.39-.406-5.494-.406-9.719-.406zm-21.881-297.171c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm91.541 167.789c0 31.086-17.731 45.789-35.461 59.644-5.474 5.457-11.801 11.353-11.801 20.594 0 9.246 6.326 14.264 10.949 18.068l15.209 11.746c18.565 15.547 35.449 29.849 35.449 58.803 0 39.517-38.412 79.426-111.03 79.426-61.21 0-90.778-29.001-90.778-60.094 0-15.124 7.619-36.531 32.523-51.267 26.186-15.943 61.655-18.039 80.631-19.294-5.91-7.574-12.662-15.539-12.662-28.566 0-7.152 2.135-11.355 4.244-16.375-4.685.402-9.303.831-13.523.831-44.74 0-70.075-33.207-70.075-65.964 0-19.335 8.846-40.742 27.02-56.294 24.052-19.727 52.752-23.086 75.553-23.086h86.953l-26.985 15.109h-26.205c9.709 7.977 29.986 24.779 29.986 56.719z"/>
                                 </svg>
                              </a>
                              <a href="https://www.linkedin.com/company/the-laundry-doctor" rel="nofollow" class="builtin-icons light medium linkedin-hover" target="_blank" alt=" linkedin" title=" linkedin">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-linkedin" data-cacheid="icon-5aa7cac9f359b" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm-96.612 95.448c19.722 0 31.845 13.952 32.215 32.284 0 17.943-12.492 32.311-32.592 32.311h-.389c-19.308 0-31.842-14.368-31.842-32.311 0-18.332 12.897-32.284 32.609-32.284zm32.685 288.552h-64.073v-192h64.073v192zm223.927-.089h-63.77v-97.087c0-27.506-11.119-46.257-34.797-46.257-18.092 0-22.348 12.656-27.075 24.868-1.724 4.382-2.165 10.468-2.165 16.583v101.892h-64.193s.881-173.01 0-192.221h57.693v.31h6.469v19.407c9.562-12.087 25.015-24.527 52.495-24.527 43.069 0 75.344 29.25 75.344 92.077v104.954z"/>
                                 </svg>
                              </a>
                              <a href="https://twitter.com/LaundryDoctor" rel="nofollow" class="builtin-icons light medium twitter-hover" target="_blank" alt=" twitter" title=" twitter">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-twitter" data-cacheid="icon-5aa7cac9f36df" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm146.24 258.654c-31.365 127.03-241.727 180.909-338.503 49.042 37.069 35.371 101.619 38.47 142.554-3.819-24.006 3.51-41.47-20.021-11.978-32.755-26.523 2.923-41.27-11.201-47.317-23.174 6.218-6.511 13.079-9.531 26.344-10.407-29.04-6.851-39.751-21.057-43.046-38.284 8.066-1.921 18.149-3.578 23.656-2.836-25.431-13.295-34.274-33.291-32.875-48.326 45.438 16.866 74.396 30.414 98.613 43.411 8.626 4.591 18.252 12.888 29.107 23.393 13.835-36.534 30.915-74.19 60.169-92.874-.493 4.236-2.758 8.179-5.764 11.406 8.298-7.535 19.072-12.719 30.027-14.216-1.257 8.22-13.105 12.847-20.249 15.539 5.414-1.688 34.209-14.531 37.348-7.216 3.705 8.328-19.867 12.147-23.872 13.593-2.985 1.004-5.992 2.105-8.936 3.299 36.492-3.634 71.317 26.456 81.489 63.809.719 2.687 1.44 5.672 2.1 8.801 13.341 4.978 37.521-.231 45.313-5.023-5.63 13.315-20.268 23.121-41.865 24.912 10.407 4.324 30.018 6.691 43.544 4.396-8.563 9.193-22.379 17.527-45.859 17.329z"/>
                                 </svg>
                              </a>
                              <a href="https://thelaundrydoctor.com/blog/" rel="nofollow" class="builtin-icons light medium wordpress-hover" target="_blank" alt=" wordpress" title=" wordpress">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-wordpress" data-cacheid="icon-5aa7cac9f3825" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M351.791 244.102c0-17.582-6.335-29.748-11.728-39.209-7.204-11.705-13.952-21.615-13.952-33.342 0-13.055 9.879-25.213 23.875-25.213.606 0 1.215.071 1.822.123-25.274-23.185-58.967-37.34-95.978-37.34-49.648 0-93.344 25.49-118.767 64.088 3.343.093 6.475.167 9.14.167 14.881 0 37.899-1.825 37.899-1.825 7.64-.423 8.577 10.821.911 11.726 0 0-7.715.897-16.284 1.359l51.776 153.948 31.089-93.269-22.12-60.679c-7.662-.462-14.924-1.359-14.924-1.359-7.644-.444-6.756-12.149.906-11.726 0 0 23.463 1.825 37.429 1.825 14.858 0 37.898-1.825 37.898-1.825 7.644-.423 8.551 10.821.897 11.726 0 0-7.698.897-16.274 1.359l51.385 152.778 14.18-47.356c7.205-18.474 10.822-33.79 10.822-45.957zm-93.458 19.626l-42.666 123.928c12.734 3.738 26.211 5.771 40.163 5.771 16.539 0 32.435-2.858 47.188-8.067-.369-.599-.721-1.233-.994-1.928l-43.692-119.704zm68.966 110.397c42.243-24.622 70.66-70.404 70.66-122.853 0-24.721-6.317-47.946-17.41-68.201.642 4.535.986 9.387.986 14.62 0 14.414-2.718 30.632-10.813 50.91l-43.423 125.523zm-71.299-380.525c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm-.038 416.436c-88.478 0-160.462-71.979-160.462-160.463 0-88.478 71.983-160.462 160.462-160.462 88.483 0 160.462 71.984 160.462 160.462 0 88.483-71.98 160.463-160.463 160.463zm-142.283-158.764c0 56.258 32.708 104.907 80.117 127.921l-67.805-185.776c-7.878 17.691-12.312 37.247-12.312 57.855z"/>
                                 </svg>
                              </a>
                              <a href="https://www.yelp.com/biz/the-laundry-doctor-st-paul-2" rel="nofollow" class="builtin-icons light medium yelp-hover" target="_blank" alt=" yelp" title=" yelp">
                                 <svg  class="mk-svg-icon" data-name="mk-jupiter-icon-yelp" data-cacheid="icon-5aa7cac9f397b" style=" height:24px; width: 24px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                    <path d="M256-6.4c-141.385 0-256 114.615-256 256s114.615 256 256 256 256-114.615 256-256-114.615-256-256-256zm-85.5 111.2c17.394-12.656 50.835-16.277 64.599-13.809 13.768 2.457 20.962 9.581 21.06 18.264l.781 110.677c.063 8.688-4.809 16.674-10.843 17.746-6.06 1.054-14.982-4.113-19.888-11.507l-62.75-94.892c-4.869-7.408-14.675-10.674 7.042-26.48zm-42.401 197.426c-1.588-8.887 0-51.824 0-51.824 2.645-12.331 12.668-19.866 21.352-16.686l66.734 24.489c8.678 3.18 15.081 11.467 14.224 18.45-.884 6.983-8.77 15.019-17.574 17.907l-63.173 20.688c-8.803 2.897-19.953-4.135-21.564-13.024zm130.336 92.128c-.173 8.69-15.628 17.428-24.398 14.478l-46.082-14.817c-8.75-2.95-11.731-18.56-5.73-25.202l44.071-48.907c6-6.632 16.056-11.637 22.393-11.096 6.332.541 11.351 8.065 11.178 16.783l-1.432 68.762zm122.325-57.073c-3.871 10.756-24.882 39.447-24.882 39.447-4.976 7.385-23.006 8.688-27.514 1.083l-34.866-59.062c-4.51-7.615-5.367-16.04-1.92-18.708 3.438-2.699 13.606-2.92 22.56-.544l55.26 14.708c8.956 2.378 15.233 12.291 11.363 23.075zm-6.885-83.419l-61.59 10.574c-9.108 1.887-19.829-1.596-23.853-7.767-4.022-6.149-2.406-16.634 3.622-23.234l35.615-39.262c6.027-6.606 16.718-7.55 26.063-.025 9.356 7.505 26.639 29.535 29.132 37.01 2.526 7.464 2.125 20.235-8.988 22.703z"/>
                                 </svg>
                              </a>
                           </div>
                        </section>
                     </div>
                     <div class="clearboth"></div>
                  </div>
               </div>
               <div id="sub-footer">
                  <div class=" mk-grid">
                     <span class="mk-footer-copyright">Copyright © 2018 - The Laundry Doctor. All Rights Reserved.</span>
                  </div>
                  <div class="clearboth"></div>
               </div>
            </section>
         </div>
      </div>
      <div class="bottom-corner-btns js-bottom-corner-btns">
         <a href="#top-of-page" class="mk-go-top  js-smooth-scroll js-bottom-corner-btn js-bottom-corner-btn--back">
            <svg  class="mk-svg-icon" data-name="mk-icon-chevron-up" data-cacheid="icon-5aa7caca013eb" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
               <path d="M1683 1331l-166 165q-19 19-45 19t-45-19l-531-531-531 531q-19 19-45 19t-45-19l-166-165q-19-19-19-45.5t19-45.5l742-741q19-19 45-19t45 19l742 741q19 19 19 45.5t-19 45.5z"/>
            </svg>
         </a>
      </div>
      <link rel='stylesheet' id='dashicons-css'  href='/css/lockermd/dashicons.min.css' type='text/css' media='all' />
      <style id='dashicons-inline-css' type='text/css'>
         [data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
      </style>
      <script>
         var isTest = false;
      </script>
      <script type='text/javascript' src='/js/lockermd/core-scripts.6.1.1.js'></script>
      <script type='text/javascript' src='/js/lockermd/mkhb-render.js'></script>
      <script type='text/javascript' src='/js/lockermd/mkhb-column.js'></script>
      <script type='text/javascript' src='/js/lockermd/wp-embed.min.js'></script>
      <script type='text/javascript' src='/js/lockermd/js_composer_front.min.js'></script>
      <script type='text/javascript' src='/js/lockermd/underscore.min.js'></script>
      <script type='text/javascript' src='/js/lockermd/backbone.min.js'></script>
   </body>
</html>