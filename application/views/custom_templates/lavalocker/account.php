<?php

    $system_lang = get_customer_business_language_key();

    if( $system_lang == 'ca' ){
        $lang_key = 'ca';
    }elseif( $system_lang == 'es' ){
        $lang_key = 'es';
    }elseif( $system_lang == 'en'){
        $lang_key = 'en';
    }else{
        $lang_key = 'es';
    }
    
    //now we should set the interior - register/login page associated to that
    //after they register, though, its up to them, we'll use that session to flip these accordingly
    //THE LANGUAGE SET FROM THE USER PROFILE/REGISTER SHOULD ALWAYS TAKE PRECEDENCE. SO maybe check as the last and final thing, and use that.
    
    $lava_locker_lang_keys = array( 'en'  => array( 'header' => array( 'menu1' => array('link' => 'http://www.lavalocker.es/en/price-list/', 'title' => 'Price list'),
                                                                       'menu2' => array('link' => 'http://www.lavalocker.es/en/locker-locations', 'title' => 'Locations'),
                                                                       'menu3' => array('link' => 'http://www.lavalocker.es/en/frequently-asked-questions', 'title' => 'FAQ'),
                                                                       'menu4' => array('link' => 'http://www.lavalocker.es/en/contact-us', 'title' => 'Contact us' ) ),
        
                                                    'footer' => array( 'menu1'  => array('link' => 'http://www.lavalocker.es/en/about-us/', 'title' => 'About us' ),
                                                                       'menu2'  => array('link' => 'http://www.lavalocker.es/en/lavalocker-news/', 'title' => 'Lavalocker news' ),
                                                                       'menu3'  => array('link' => 'http://www.lavalocker.es/en/terms-and-conditions/', 'title' => 'Terms and conditions'),
                                                                       'menu4'  => array('link' => '', 'title' => 'Lavalocker in Catalan'),
                                                                       'menu5'  => array('link' => '', 'title' => 'Lavalocker in Spanish'),
                                                                       'menu6'  => array('link' => '', 'title' => 'Help'),
                                                                       'menu7'  => array('link' => 'http://www.lavalocker.es/en/frequently-asked-questions/', 'title' => 'FAQs'),
                                                                       'menu8'  => array('link' => 'http://www.lavalocker.es/en/contact-us/', 'title' => 'Contact us'),
                                                                       'menu9'  => array('link' => 'http://www.lavalocker.es/en/request-lockers/', 'title' => 'Request lockers'),
                                                                       'menu10' => array('link' => '', 'title' => 'Follow us!' ) ) ),

                                    'es'  => array( 'header' => array( 'menu1' => array('link' => 'http://www.lavalocker.es/price-list/', 'title' => 'Precios'),
                                                                       'menu2' => array('link' => 'http://www.lavalocker.es/en/locker-locations', 'title' => 'Ubicaciones'),
                                                                       'menu3' => array('link' => 'http://www.lavalocker.es/en/frequently-asked-questions', 'title' => 'FAQs'),
                                                                       'menu4' => array('link' => 'http://www.lavalocker.es/en/contact-us', 'title' => 'Contáctanos' ) ),
                                        
                                                    'footer' => array( 'menu1'  => array('link' => 'http://www.lavalocker.es/quienes-somos/', 'title' => '¿Quiénes somos?'),
                                                                       'menu2'  => array('link' => 'http://www.lavalocker.es/noticias-de-lavalocker/', 'title' => 'Notícias de Lavalocker'),
                                                                       'menu3'  => array('link' => 'http://www.lavalocker.es/terminos-y-condiciones/', 'title' => 'Términos y condiciones'),
                                                                       'menu4'  => array('link' => '', 'title' => 'Lavalocker en Catalán'),
                                                                       'menu5'  => array('link' => '', 'title' => 'Lavalocker en Inglés'),
                                                                       'menu6'  => array('link' => '', 'title' => 'Ayuda'),
                                                                       'menu7'  => array('link' => 'http://www.lavalocker.es/dudas-frecuentes/', 'title' => 'Dudas frecuentes'),
                                                                       'menu8'  => array('link' => 'http://www.lavalocker.es/contacta-con-nosotros/', 'title' => 'Contáctanos'),
                                                                       'menu9'  => array('link' => 'http://www.lavalocker.es/solicita-taquillas/', 'title' => 'Solicita taquillas'),
                                                                       'menu10' => array('link' => '', 'title' => 'Síguenos!' ) ) ),
        
                                    'ca' => array( 'header' => array( 'menu1' => array('link' => 'http://www.lavalocker.es/ca/llista-de-preus/', 'title' => 'Preus'),
                                                                       'menu2' => array('link' => 'http://www.lavalocker.es/ca/localitzacions/', 'title' => 'Ubicacions'),
                                                                       'menu3' => array('link' => 'http://www.lavalocker.es/ca/dubtes-frequents/', 'title' => 'FAQs'),
                                                                       'menu4' => array('link' => 'http://www.lavalocker.es/ca/contacta-amb-nosaltres/', 'title' => 'Contacta' ) ),
                                        
                                                    'footer' => array( 'menu1'  => array('link' => 'http://www.lavalocker.es/ca/qui-som/', 'title' => 'Qui som?'),
                                                                       'menu2'  => array('link' => 'http://www.lavalocker.es/ca/noticies-de-lavalocker/', 'title' => 'Notícies de Lavalocker'),
                                                                       'menu3'  => array('link' => 'http://www.lavalocker.es/ca/termes-i-condicions/', 'title' => 'Termes i condicions'),
                                                                       'menu4'  => array('link' => '', 'title' => 'Lavalocker en Castellà'),
                                                                       'menu5'  => array('link' => '', 'title' => 'Lavalocker en Anglès'),
                                                                       'menu6'  => array('link' => '', 'title' => 'Ajuda'),
                                                                       'menu7'  => array('link' => 'http://www.lavalocker.es/ca/dubtes-frequents/', 'title' => 'Dubtes freqüents'),
                                                                       'menu8'  => array('link' => 'http://www.lavalocker.es/ca/contacta-amb-nosaltres/', 'title' => 'Contacta amb nosaltres'),
                                                                       'menu9'  => array('link' => 'http://www.lavalocker.es/ca/sol%c2%b7licita-taquilles/', 'title' => 'Sol·licita taquilles'),
                                                                       'menu10' => array('link' => '', 'title' => 'Segueix-nos!' ) ) ) );

        //redir link if lang changes                                                             
        $all_sess_data = $this->session->all_userdata();
        $lang_swap_redirect = '';
        $logged_in = false;
        if( !empty( $all_sess_data['user_id'] ) ){
            $logged_in = true;
            $lang_swap_redirect .= '&r=' . base64_encode( $this->uri->uri_string );
        }
        
        
        //finally pass the lang key to the view
      //  $content['selected_business_language_id'] = 
    ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/lavalocker/language-selector.css" type="text/css" media="all" />
        <link rel="icon" type="image/png" href="/imgages/lavalocker/favicon.png" />
        <link rel="apple-touch-icon-precomposed" href="/images/lavalocker/apple-touch-icon.png">
        <link rel="canonical" href="http://www.lavalocker.es/sample-in-app-menus/" />

        <link rel='stylesheet' id='main-style-css'  href='/css/lavalocker/app.css' type='text/css' media='all' />
        
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'lavalocker.es']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $_scripts;?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
    
        <style>
            .footer-links li{
                line-height:1.6em;
            }
            li a:link {text-decoration:none;}
            li a:visited {text-decoration:none;}
            li a:hover {text-decoration:none;}
            li a:active {text-decoration:none;}
        </style>
    </head>
    
<body class="page page-id-334 page-template page-template-sample_menu-php logged-in admin-bar no-customize-support single-author">
    <div id="in-app-header" class="hidden-phone hidden-tablet">
        <div id="in-app-inner-header" style="width: 870px;">
            <h2 class="pull-left">LavaLocker</h2>
            <ul class="in-app-menu unstyled">
                <li><a href="<?=$lava_locker_lang_keys[$lang_key]['header']['menu1']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['header']['menu1']['title'];?></a></li>
                <li><a href="<?=$lava_locker_lang_keys[$lang_key]['header']['menu2']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['header']['menu2']['title'];?></a></li>
                <li><a href="<?=$lava_locker_lang_keys[$lang_key]['header']['menu3']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['header']['menu3']['title'];?></a></li>
                <li><a href="<?=$lava_locker_lang_keys[$lang_key]['header']['menu4']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['header']['menu4']['title'];?></a></li><li>93 250 21 70</li>
            </ul>
        </div>
    </div>
    <div class="row global-wrapper"> 
        
        <div id="main" class="main-content">
            
            <div style='min-height:600px;margin-top:80px;'>
            <?php 
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content 
            ?>
        </div>
            
        </div>
    </div>
    <footer id="page-footer">
        <div class="row">
            <div class="footer-wrapper">
                <ul class="container" style="width: 870px;list-style-type: none;">
                    <li class="span2">
                        <h5 class="title">Lavalocker</h5>
                        <ul class="footer-links unstyled" style="padding-left:0px;list-style-type: none;">
                            <li><a href="<?=$lava_locker_lang_keys[$lang_key]['footer']['menu1']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu1']['title'];?></a></li>
                            <li><a href="<?=$lava_locker_lang_keys[$lang_key]['footer']['menu2']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu2']['title'];?></a></li>
                            <li><a href="<?=$lava_locker_lang_keys[$lang_key]['footer']['menu3']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu3']['title'];?></a></li>
                            
                            <?if($lang_key == 'en'){?>
                                <li><a href="<?= ( !$logged_in ) ? '?ll_lang=ca' : '/account/profile/change_language?ll_swap=ca' . $lang_swap_redirect; ?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu4']['title'];?></a></li>
                                <li><a href="<?= ( !$logged_in ) ? '?ll_lang=es' : '/account/profile/change_language?ll_swap=es' . $lang_swap_redirect; ?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu5']['title'];?></a></li>
                            <?}?>
                            
                            <?if($lang_key == 'es'){?>
                                <li><a href="<?= ( !$logged_in ) ? '?ll_lang=ca' : '/account/profile/change_language?ll_swap=ca' . $lang_swap_redirect; ?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu4']['title'];?></a></li>
                                <li><a href="<?= ( !$logged_in ) ? '?ll_lang=en' : '/account/profile/change_language?ll_swap=en' . $lang_swap_redirect; ?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu5']['title'];?></a></li>
                            <?}?>
                            
                            <?if($lang_key == 'ca'){?>
                                <li><a href="<?= ( !$logged_in ) ? '?ll_lang=es' : '/account/profile/change_language?ll_swap=es' . $lang_swap_redirect; ?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu4']['title'];?></a></li>
                                <li><a href="<?= ( !$logged_in ) ? '?ll_lang=en' : '/account/profile/change_language?ll_swap=en' . $lang_swap_redirect; ?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu5']['title'];?></a></li>
                            <?}?>
                            
                        </ul>
                    </li>
                    <li class="span2">
                        <h5 class="title"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu6']['title'];?></h5>
                        <ul class="footer-links unstyled" style="padding-left:0px;list-style-type: none;">
                            <li><a href="<?=$lava_locker_lang_keys[$lang_key]['footer']['menu7']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu7']['title'];?></a></li>
                            <li><a href="<?=$lava_locker_lang_keys[$lang_key]['footer']['menu8']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu8']['title'];?></a></li>
                            <li><a href="<?=$lava_locker_lang_keys[$lang_key]['footer']['menu9']['link'];?>"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu9']['title'];?></a></li>
                        </ul>
                    </li>
                    <li class="span2">
                        <h5 class="title"><?=$lava_locker_lang_keys[$lang_key]['footer']['menu10']['title'];?></h5>
                        <ul class="footer-links unstyled social-links" style="padding-left:0px;list-style-type: none;">
                            <li class="fb-link"><a href="https://facebook.com/lavalocker" target="_blank">Facebook</a></li>
                            <li class="tw-link"><a href="https://twitter.com/lavalocker" target="_blank">Twitter</a></li>
                            <li class="gp-link"> <a href="https://plus.google.com/u/0/b/105167012905444352155" target="_blank">Google+</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div> 
    </footer>

    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $('.dropdown-toggle').dropdown();
    </script>
</body>
</html>
