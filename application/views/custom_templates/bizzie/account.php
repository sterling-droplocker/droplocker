<?php
/**
 * Expects the following content variables
 * 
 */
?>
<!DOCTYPE html>
<html>
   <head>
       
 
       <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title><?= $title ?></title>
      <script type="text/javascript" src="/js/report_error.js"></script>

      <?php if(ENVIRONMENT == 'production' && strpos( strtolower( $_SERVER['HTTP_HOST'] ), 'laundrylocker' ) !== false ){ ?>
      <script type="text/javascript">
      </script>
      <!-- begin olark code -->
      <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
      f[z]=function(){
      (a.s=a.s||[]).push(arguments)};var a=f[z]._={
      },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
      f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
      0:+new Date};a.P=function(u){
      a.p[u]=new Date-a.p[0]};function s(){
      a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
      hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
      return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
      b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
      b.contentWindow[g].open()}catch(w){
      c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
      var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
      b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
      loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
      /* custom configuration goes here (www.olark.com/documentation) */
      olark.identify('3708-947-10-4387');/*]]>*/</script><noscript><a href="https://www.olark.com/site/3708-947-10-4387/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
      <!-- end olark code -->
    <?php } ?>
  
      <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
      <script type="text/javascript" src="/js/jquery.loading_indicator.js"> </script>
      <script type="text/javascript" src='/js/functions.js'></script>
      <?= $_scripts ?>
      <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', '<?php echo $_SERVER["HTTP_HOST"]?>']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
      <?= $javascript?>
      <?= $style?>
      <?= $_styles ?>
   
<style type="text/css">
    
.invalid {
            color: red;
        }
.laundry_plan .green {
    color:#6D9E2C;	
    background: white !important;
    border : none;
	filter: none;
}

</style>
   </head>
   <body>
        <div id="fb-root"></div>

       <!-- End of facebook code -->
      <div id="wrapper">
      	<div id="header">
            
        <!-- bizzie header -->
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />  
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">  
        <style type="text/css"> body { color:#07548B; } .nav > li > a { font-size:18px; color: #07548B; } </style>  
        <div class="row" style="background-color:#c6e29f;border-bottom: solid 8px #7B2A82;max-height:40px;margin-top:40px;"> 
                <img src="/images/bizzie/bizziebox_locker_image.png" style="max-width:140px;float:left;margin-top:-30px;margin-left:50px;"> 
                <ul class="nav nav-pills" style="margin-left:200px;"> 
                        <? $account_home_link = ''; ?>
                        <li><a href="/account">Account Home</a></li>
                        <li><a href="http://www.bizzie.com/" target="_blank">How It Works</a></li>
                        <? if( !empty( $business->show_contact_link ) ){ ?><li><a href="/account/main/contact">Contact Us</a></li><? } ?>
                </ul> 
        </div>
        <div class="clearfix"></div>      
        
        <?php if ($facebook_user_id): ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $("#login_logout").html("Logout");
                        $("#login_logout").attr("href", "<?= $facebook_logout_url?>");
                    });
                </script>
            <?php else: ?>
            <script type="text/javascript">
                    $(document).ready( function(){
                    $("#login_logout").html("Logout");
                    $("#login_logout").attr('href', "/logout");
                });
            </script>
            <?php endif; ?>
         
            
        </div>
          
          <div style='padding:0px 10px 20px 0px;'>
             <div style='padding:0px;'><?= $content ?></div>
             <br style='clear:both'>
          </div>
          
          <!-- bizzie footer -->
         <div id="footer" style='clear:both'>
             <? $bizzie_footer_logo = '/images/bizzie/v1/bizzie_logo_on_green.png';?>
             <? $custom_footer_logo = '';?>
             <!-- bizzie footer / custom footer logo --> 
             <?= $footer;?>
 
         </div>
          
         <br>
      </div>

    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script>
        $('.dropdown-toggle').dropdown();
    </script>

</body>
</html>

