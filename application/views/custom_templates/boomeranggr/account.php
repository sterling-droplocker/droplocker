<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/boomeranggr/style.css?v=1">

	<?= $style ?>
	<?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

	<?= $_scripts ?>
	<?= $javascript ?>
</head>
<body>
<div id="fb-root"></div>
<div class="container">
    <div>
        <!-- End of facebook code -->

		<?
		//if this is using the exterior pages - make sure NOT to hide the header/footer
		// $responsive = '';
		// $uri_string = $this->uri->uri_string();
		// if( strpos($uri_string,'main/') === false ){
		$responsive = 'hidden-phone hidden-tablet';
		// }
		?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="http://thinkboxsites.com/boomerang/">
                        <img src="/images/boomeranggr/logo.svg" style="height:46px;" />
                    </a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="http://thinkboxsites.com/boomerang/#services">Services</a></li>
                            <li><a href="http://thinkboxsites.com/boomerang/#how-it-works">How It Works</a></li>
                            <li><a href="http://thinkboxsites.com/boomerang/#locations">Locations</a></li>
                            <li><a href="http://thinkboxsites.com/boomerang/#pricing">Pricing</a></li>
                            <li><a href="http://thinkboxsites.com/boomerang/#faq">FAQ</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container">
                    <div id="" class="row-fluid">
                        <div class="span12 center">
                            <div class="navbar">
                                <div class="navbar-inner-menu">
                                    <a href="http://thinkboxsites.com/boomerang/">
                                        <img src="/images/boomeranggr/logo.svg" class="logo">
                                    </a>
                                    <ul class="nav">
                                        <li><a class="navText" href="http://thinkboxsites.com/boomerang/#services">Services</a></li>
                                        <li><a class="navText" href="http://thinkboxsites.com/boomerang/#how-it-works">How It Works</a></li>
                                        <li><a class="navText" href="http://thinkboxsites.com/boomerang/#locations">Locations</a></li>
                                        <li><a class="navText" href="http://thinkboxsites.com/boomerang/#pricing">Pricing</a></li>
                                        <li><a class="navText" href="http://thinkboxsites.com/boomerang/#faq">FAQ</a></li>
                                    </ul>
                                    <?php if (empty($this->customer)): ?>
                                        <ul class="login">
                                            <li><a class="navText" href="https://myaccount.boomeranggr.com/login">Login</a></li>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" style='margin-top: 120px; background: #FFF; padding: 10px'>
            <div class="span10 center">
				<?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="footer">
                <div class="footer-left">
                    <p>Want locker service in your building?</p>
                    <div class="button-wrap">
                        <a href="http://thinkboxsites.com/boomerang/#" class="btn blue">Request Service</a>
                    </div>
                </div>
                <div class="footer-right">
                    <p><a href="tel:16162024066">Call or Text 616.202.4066</a><br><a href="mailto:hello@boomeranggr.com">hello@boomeranggr.com</a></p>
                    <div class="social-wrap">
                        <ul class="reset">
                            <li><a href="#" class="facebook"></a></li>
                            <li><a href="#" class="twitter"></a></li>
                            <li><a href="#" class="instagram"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
	<?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

	<?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
	<?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
	<?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>