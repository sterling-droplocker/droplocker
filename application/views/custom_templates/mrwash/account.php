<!doctype html>
<html lang="es_CL">
<head>
	<meta charset="UTF-8">
	<title>MrWash | <?= $title ? $title : 'Servicio de Lavaseco y Lavanderia' ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
	<link rel="stylesheet" href="/css/font-awesome-4.1.0/css/font-awesome.min.css">


   	<link rel="stylesheet" href="/css/mrwash/messi.css">

	<link rel="stylesheet" href="/css/mrwash/style.css?v=1">
	<link rel="icon" href="images/mrwash/favicon.gif" type="image/x-icon" />
	<!--[if lt IE 9]>
	  <script src="/js/mrwash/html5shiv.min.js"></script>
	  <script src="/js/mrwash/respond.min.js"></script>
	<![endif]-->


	<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
	<script src='/bootstrap/js/bootstrap.min.js'></script>

	<script src="/js/mrwash/messi.min.js"></script>	
	<script>
		$(document).ready(function(){
			$('.btnmenu').click(function(){
				$(this).parent().find('ul').slideToggle(400);
			});
		});
	</script>

	<?= $_scripts ?>
	<?= $javascript?>
	<?= $style?>
	<?= $_styles ?>
</head>
<body>
	<div class="bannertop">
		<div class="container">
			<div class="row">
				<div class="span6">
					<div id="logo">
						<a href="http://mrwash.cl">
							<img src="/images/mrwash/mrwash-logo.png" class="img-responsive" alt="MrWash">
						</a>
					</div>
				</div>
				<div class="span6 text-right hidden-phone hidden-tabled" style="text-align: right;">
					<ul class="redesociales">
						<li class="facebook"><a target="_blank" href="https://www.facebook.com/mrwash.cl?fref=ts"><i class="fa fa-facebook-square"></i></a></li>
						<li class="twitter"><a target="_blank" href="https://twitter.com/mrwashchile"><i class="fa fa-twitter-square"></i></a></li>
					</ul>
					<div class="phone"><i class="fa fa-phone-square"></i>(02) 3202 6727</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="menunav">
		<div class="container">
			<div class="row">
				<div class="span12">
					<nav>
						<ul id="menu-menu" class="nav-menu">
							<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page home menu-item-33"><a href="http://mrwash.cl/">INICIO</a></li>
							<li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-58"><a href="http://mrwash.cl/como-funciona/">Cómo Funciona</a></li>
							<li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="http://mrwash.cl/nuestros-precios/">Nuestros Precios</a></li>
							<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="http://mrwash.cl/testimonios/">Testimonios</a></li>
							<li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="http://mrwash.cl/preguntas-frecuentes/">Preguntas Frecuentes</a></li>
							<li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a href="http://mrwash.cl/contactanos/">Contáctanos</a></li>
						</ul>						

						<button class="btnmenu hidden-desktop visible-phone visible-tablet">
							<span></span>
							<span></span>
							<span></span>
						</button>
					</nav>
				</div>
			</div>
		</div>
	</div>

	<div id="main_content"><?= $content ?></div>

	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="foo">
					<div class="text-center" style="float:left;  border-right: 1px solid #000;">
                                            <a href="http://mrwash.cl/terminos-y-condiciones/"><img src="/images/mrwash/webpay.jpg" alt=""></a>
					</div>
					<div class="text-center" style="float:left;">
                                            <img src="/images/mrwash/mrwash-logo-footer.png" alt="">
						<div><small>© 2015 Mr.Wash</small></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

<? if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')): ?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', '<?= $_SERVER["HTTP_HOST"] ?>']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<? endif; ?>


</body>
</html>