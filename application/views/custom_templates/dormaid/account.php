<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0070)http://www.dormaid.com/products/wash-dry-fold-laundry-and-dry-cleaning -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>Dormaid - Laundry &amp; Dry Cleaning Services</title>

<link href="/css/dormaid/dormaid_screen.css" media="screen" rel="stylesheet" type="text/css">
    <link href="/css/dormaid/dormaid_theme.css" media="screen" rel="stylesheet" type="text/css">
 <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
                <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
</head>

<body class="products products-show one-col  mac js applewebkit applewebkit_537_36 applewebkit_537 chrome chrome_31_0_1650_57 chrome_31" id="product-details">
  <div id="header">
    <div class="container">
            <ul id="main_nav">
        <li>
          <a href="http://www.dormaid.com/products/wash-dry-fold-laundry-and-dry-cleaning#">Services</a>
          <ul id="serv_drop">
            <li><a href="http://www.dormaid.com/products/dorm-room-cleaning">Dorm Cleaning</a></li>
            <li><a href="./Dormaid - Laundry & Dry Cleaning Services_files/Dormaid - Laundry & Dry Cleaning Services.html">Laundry Service</a></li>
            <li><a href="http://www.dormaid.com/t/services/beverage-delivery">Beverage Delivery</a></li>
            <li><a href="http://www.dormaid.com/products/tutoring-and-application">Tutoring &amp; Application Prep</a></li>
            
          </ul>
        </li>
        <li>
          <a href="http://www.dormaid.com/t/products">Products</a>
          <ul id="prod_drop">            
            
              <li><a href="http://www.dormaid.com/t/products/dirt-devil-vacuum-cleaners/">Dirt Devil Vacuum Cleaners</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/air-conditioners/">Air Conditioners</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/brita-water-filters/">Brita Water Filters</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/countertop-ovens/">Countertop Ovens</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/coffee-makers/">Coffee Makers</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/portable-electric-heaters/">Crane Animal Humidifiers</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/crane-animal-humidifiers/">Portable Electric Heaters</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/hepa-air-purifiers/">Honeywell Fans</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/honeywell-fans/">HEPA Air Purifiers</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/hamilton-beach-blenders/">Hamilton Beach Blenders</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/electric-kettles/">Electric Kettles</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/dorm-room-hot-plate/">Dorm Room Hot Plate</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/aerobeds/">Aerobeds</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/minifridges/">Minifridges</a></li>
            
              <li><a href="http://www.dormaid.com/t/products/toothbrush-holder-and-sanitizer/">Toothbrush Holder and Sanitizer</a></li>
            
          </ul>
        </li>
        <li><a href="http://www.dormaid.com/pages/about_us">About Us</a></li>
        <li><a href="http://www.dormaid.com/inquiries/new">Contact</a></li>
        <li><a href="http://www.dormaid.com/pages/faq">FAQ</a></li>
        <li><a href="http://blogspot.dormaid.com/">Blog</a></li>
        <li class="green"><a href="http://www.dormaid.com/pages/green">Green</a></li>
      </ul>

      <div id="logo"><a href="http://www.dormaid.com/"><img alt="Dormaid-logo" src="/images/dormaid/dormaid-logo.png"></a></div>
      <div id="top_contact_info">1-888-4-DORMAID <span><a href="mailto:support@dormaid.com">support@dormaid.com</a></span></div>
      <div id="login-bar">          
        <ul id="nav-bar">
          
  <li><a href="http://www.dormaid.com/login">Log In</a></li>


  <li class="cart-indicator"><a href="http://www.dormaid.com/orders/new">Cart</a></li>


<a name="top"> </a>
        </ul>
      </div>
    </div>
  </div>
  
  <div id="main_body">

  
    <div id="wrapper" class="container"> 
      
    
      <div id="content">
        
    
        <div style='min-height:600px;'>
        <?php 
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content 
        ?>
        </div>

      </div>
    </div>
  </div>
  <div id="footer">
  <div class="container">
    <div class="col_left">
      <h3>Contact Us <small>1-888-4-DORMAID</small></h3>
      <form action="http://www.dormaid.com/inquiries" class="footer-form" id="new_inquiry" method="post"><div style="margin:0;padding:0;display:inline"><input name="authenticity_token" type="hidden" value="UhlhqpU53eA/zUOvKNG3CoYDXwXMwfGwHyrs8S9mSF8="></div>
        <input id="inquiry_email" name="inquiry[email]" size="30" type="text" value="Your Email">
        <textarea cols="40" id="inquiry_message" name="inquiry[message]" rows="20"></textarea>
        <input class="button" id="inquiry_submit" name="commit" type="submit" value="Send">
      </form>
    </div>
    <div class="col_right">
      <h3>Deals</h3>
      <ul>
        <li><span>8-12</span>Convince a friend to sign up for a new service plan and we will give you each $30 off DormAid services! &nbsp;<a href="http://www.dormaid.com/pages/deals">more</a><br></li>
        <li><span>8-15</span>DormAid will give you $50 for ditching your current service provider. &nbsp;<a href="http://www.dormaid.com/pages/deals">more</a><br></li>
      </ul>
      
    </div>
    <div class="bottom">
      <p>Copyright © 2010, DormAid, LLC. <a href="http://www.dormaid.com/pages/terms">Terms of Use</a> <a href="http://www.dormaid.com/pages/privacy">Privacy Policy</a></p>
    </div>
  </div>
</div>   



<div id="AutocompleteContainter_832e2" style="position: absolute; z-index: 9999; top: 153px; left: 413.5px;"><div class="autocomplete-w1"><div class="autocomplete" id="Autocomplete_832e2" style="display: none; width: 500px; max-height: 300px;"></div></div></div>


<script src="/bootstrap/js/bootstrap.min.js"></script>
<script>
    $('.dropdown-toggle').dropdown();
</script>

</body>
</html>
