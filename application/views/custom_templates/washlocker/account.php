<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'washlocker.com']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
        
        <meta charset="UTF-8" />
	<title>Wash Locker</title>
	
	<link rel="stylesheet" href="/css/washlocker/main.css" />
        <link rel="stylesheet" href="/css/washlocker/main.css" />
	<link rel="stylesheet" href="/css/washlocker/normalize.css" />
        
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700' rel='stylesheet' type='text/css'>
        <style>
            /*
        #header{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
            border-bottom:1px solid #73a5bc;
            margin-bottom:20px;
        }
        #inner-header{
            text-align:left;
            width:970px;
            margin:0 auto;
        }
        
        #footer{
            width:100%;
            text-align:center;
            background-color:#d9edf7;
        }
        #inner-footer{
            text-align:left;
            width:970px;
            margin:0 auto;
        }*/
        </style>
       
</head>
 
<body>
    <div class="row-fluid">
        <div style="background-color: #3556a3;" class="hidden-phone hidden-tablet visible-desktop">
            <div id="navbar" style="background-color: #3556a3;">
		<div class="navbar-inner" style="background:none;padding:0;border:none;">
                    <ul class="nav pull-right clear" style="background-color: #3556a3;">
                        <li>
                            <a href="http://washlocker.com/services.html">Services</a>
			</li>
                        <li>
                            <a href="http://washlocker.com/how-it-works.html">How it works</a>
			</li>
			<li>
                            <a href="http://washlocker.com/locations.html">Locations</a>
			</li>
			<li>
                            <a href="http://washlocker.com/faq.html">FAQs</a>
			</li>
			<li>
                            <a href="http://washlocker.com/about.html">About us</a>
			</li>		
			<li>
                            <a href="https://myaccount.washlocker.com/register" class="btn">Sign Up</a>
			</li>		
			<li>
                            <a href="https://myaccount.washlocker.com/login" class="btn blue">Login</a>
			</li>									
                    </ul>		
                    <a href="http://washlocker.com/index.html" class="logo" style="background-color:#3556a3;"><img src="/images/washlocker/logo.png" alt="Wash Locker" /></a>		
		</div>
            </div>
        </div>


        </div>

            <div style='min-height:600px; padding-top:20px; padding-bottom: 20px;'>
            <?php 
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content 
            ?>
            </div>
    
    <div class="row-fluid">
    
            <div class='container-fluid hidden-phone' id="footer">
                <?php echo $footer?>
            </div>

    
            <div class='container-fluid visible-phone' id="footer">
                <div class='span12'>
                    <div class='well'>
                        Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                    </div>
                </div>
            </div>
        
            <div>
                <div id='footer'>
                    <div id='inner-footer'>
                        &nbsp;
                    </div>
                </div>

            </div>
    </div>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script>
            $('.dropdown-toggle').dropdown();
            $(function(){
               // $('.register_page_container').addClass('span12');
            });
    </script>
    
    <style type="text/css">
        #default_account_nav ul li{
            padding-bottom:10px;
        }
    </style>
</body>
</html>
