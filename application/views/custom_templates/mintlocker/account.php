<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        
        <link rel='stylesheet' id='google_fonts_style-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%7CCrete+Round%3A400%2C400italic&#038;ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='clear_style-css'  href='/css/mintlocker/clear.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='carouFredSel_style-css'  href='/css/mintlocker/carouFredSel.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='default_style-css'  href='/css/mintlocker/default.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='columns_style-css'  href='/css/mintlocker/columns.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='common_style-css'  href='/css/mintlocker/common.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='prettyPhoto_style-css'  href='/css/mintlocker/prettyPhoto.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='comments_style-css'  href='/css/mintlocker/comments.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='colors_style-css'  href='/css/mintlocker/colors.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='standard_wp_style-css'  href='/css/mintlocker/wp.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='typography_style-css'  href='/css/mintlocker/typography.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='main_theme_style-css'  href='/css/mintlocker/style.css?ver=3.6.1' type='text/css' media='all' />
        <link rel='stylesheet' id='responsive_style-css'  href='/css/mintlocker/responsive.css?ver=3.6.1' type='text/css' media='all' />
        
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>
        
        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>
        
        <style>
        #header{
            height:80px;
            background-color:#fff;
        }
        #inner-header{
            height:80px;
            background-color:#fff;
        }
        
        #footer{
            background-color: rgb(0,158,79);
        }
        #inner-footer{
            padding-top:20px;
            background-color: rgb(0,158,79);
            text-align:center;
        }
        #inner-footer a{
            color:#0000ff;
        }
        body, label, h1,h2,h3{
       }
        .main-menu li a{
/*            color: #19468d;*/
        }
        ul li{
/*            list-style:none;
            background:none;*/
        }
        #default_account_nav{
        }
        #default_account_nav a{
        }
        </style>
       
    </head>
    <body class="home blog">
    
        <div class="post-2 page type-page status-publish hentry section" style="background-color: #ffffff;background-repeat: no-repeat;background-position: center top;background-attachment: fixed;background-size: cover;padding-top:20px;">
        <?php 
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content 
        ?>
        </div>
        
        
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>
        
        <div>
       
        </div>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>
    </body>
    
   
</html>
