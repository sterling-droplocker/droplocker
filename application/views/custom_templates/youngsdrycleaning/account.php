<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <title>
         My Account | Young’s Dry Cleaning 
      </title>
      <link rel="shortcut icon" href="/images/youngsdrycleaning/youngs.ico" type="image/x-icon" />

      <link rel='stylesheet' id='attitude_style-css'  href='/css/youngsdrycleaning/style-site.css?ver=bfb939cc3642207d7fdee6b07b385153' type='text/css' media='all' />
      <link rel='stylesheet' id='google_fonts-css'  href='https://fonts.googleapis.com/css?family=PT+Sans%7CPhilosopher&#038;ver=bfb939cc3642207d7fdee6b07b385153' type='text/css' media='all' />

      <style type="text/css" id="custom-background-css">
         body.custom-background { background-color: #bababa; background-image: url("/images/youngsdrycleaning/sub_back3.gif"); background-position: center top; background-size: auto; background-repeat: no-repeat; background-attachment: scroll; }
      </style>


      <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
      <script src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"></script>
      <?php //compatibility layer.. This allow to use boostrap 3+ with boostrap 2x ?>
      <link rel='stylesheet' id='custom-style-css'  href='/css/youngsdrycleaning/bootstrap2x.css' type='text/css' media='all' />
      <script src='/bootstrap/js/bootstrap.min.js'></script>        
      <script type="text/javascript">
         var $ = jQuery.noConflict();
      </script>
      <?= $_scripts ?>
      <?= $javascript ?>
      <?= $style ?>
      <?= $_styles ?>
      <link rel='stylesheet' id='custom-style-css-2'  href='/css/youngsdrycleaning/custom.css?<?=time()?>' type='text/css' media='all' />
   </head>
   <body class="custom-background">
      <header id="branding" >
         <div class="container clearfix">
            <div class="hgroup-wrap clearfix">
               <section class="hgroup-right">
                  <nav id="access" class="clearfix">
                     <div class="container clearfix">
                        <ul class="root">
                           <li id="menu-item-266" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-266"><a href="https://www.youngsdrycleaning.com/contact/">Contact</a></li>
                           <li id="menu-item-609" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-609"><a href="https://www.youngsdrycleaning.com/services/garment-restoration/">Fire/Smoke Restoration</a></li>
                           <li id="menu-item-442" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-442"><a href="https://www.youngsdrycleaning.com/services/express-lockers/">Young&#8217;s Express Lockers</a></li>
                           <li id="menu-item-269" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-269"><a href="https://www.youngsdrycleaning.com/services/pickup/">Pick-Up &#038; Delivery</a></li>
                           <li id="menu-item-270" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-38 current_page_item menu-item-270"><a href="https://www.youngsdrycleaning.com/services/">Services</a></li>
                           <li id="menu-item-174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174"><a href="https://www.youngsdrycleaning.com/youngs-story/">Our Story</a></li>
                           <li id="menu-item-173" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-173"><a href="https://www.youngsdrycleaning.com/">Home</a></li>
                           <li class="default-menu"><a href="https://www.youngsdrycleaning.com" title="Navigation">Navigation</a></li>
                        </ul>
                     </div>
                     <!-- .container -->
                  </nav>
                  <!-- #access -->
               </section>
               <!-- .hgroup-right -->  
               <hgroup id="site-logo" class="clearfix">
                  <h1 id="site-title"> 
                     <img src="/images/youngsdrycleaning/header2.gif" alt="Young’s Dry Cleaning">
                     </a>
                  </h1>
               </hgroup>
               <!-- #site-logo -->
            </div>
            <!-- .hgroup-wrap -->
         </div>
         <!-- .container -->   
      </header>
      <div class="wrapper" id="bootstrap2x" style="padding-left: 1.5em;padding-right: 1.5em;height:100em;height:140em">
            <?php echo $content; ?>
      </div>
      <!-- .wrapper -->
      <br/> <br/>
      <footer id="colophon" class="clearfix">
         <div class="widget-wrap">
            <div class="container">
               <div class="widget-area clearfix">
                  <aside id="text-2" class="widget widget_text">
                     <div class="textwidget"><a href="https://www.facebook.com/Youngs-Dry-Cleaning-195369503833620/?ref=hl" target="_blank"><img src="/images/youngsdrycleaning/socialfb.png"></a>
                        <a href="
                           https://twitter.com/youngsnola" target="_blank"><img src="/images/youngsdrycleaning/socialfb.png"></a>
                        <a href="https://plus.google.com/117208520831326304920" target="_blank"><img src="/images/youngsdrycleaning/socialgp.png"></a>
                     </div>
                  </aside>
               </div>
               <!-- .widget-area -->
            </div>
            <!-- .container -->
         </div>
         <!-- .widget-wrap -->
         <div id="site-generator">
            <div class="container">
               <div class="copyright"> &copy; <?php echo date("Y"); ?> <a href="https://www.youngsdrycleaning.com/" title="Young’s Dry Cleaning" ><span>Young’s Dry Cleaning</span></a>. All Rights Reserved.</div>
               <!-- .copyright -->
               <div style="clear:both;"></div>
            </div>
            <!-- .container -->
         </div>
      </footer>
   </body>
</html>
