<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>LaundryBox Cleaners</title>
        <meta name="description" content="The HTML5 Herald">
        <meta name="author" content="SitePoint">


        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="/css/laundryboxcleaners/main.css" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Dosis:400,600|Arizonia">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Fredericka+the+Great|Allura|Amatic+SC|Arizonia|Averia+Sans+Libre|Cabin+Sketch|Francois+One|Jacques+Francois+Shadow|Josefin+Slab|Kaushan+Script|Love+Ya+Like+A+Sister|Merriweather|Offside|Open+Sans|Open+Sans+Condensed|Oswald|Over+the+Rainbow|Pacifico|Romanesco|Sacramento|Seaweed+Script|Special+Elite">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Marmelad">
        <link rel="shortcut icon" href="/images/laundryboxcleaners/icon.jpg">
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>

        <?= $style ?>
        <?= $_styles ?>


    </head>

    <body>
        
        <div id="site_wrapper" class="container">
            <div class="row">
                <h1 class="text-logo">Laundry Box Cleaners</h1>
            </div>
              <div class="row">
                <div class="span12 wsb-nav nav_theme nav-text-left nav-horizontal nav-btn-right wsb-navigation-rendered-top-level-container" id="wsb-nav-529914035">
                    <ul class="wsb-navigation-rendered-top-level-menu ">
                        <li style="">
                            <a href="http://laundryboxcleaners.com" target="" data-title="Home" data-pageid="121095" data-url="home.html">Home</a>
                        </li>
                        <li style="">
                            <a href="http://www.laundryboxcleaners.com/services.html" target="" data-title="Home" data-pageid="121095">Services</a>
                        </li>

                        <li style="">
                            <a href="http://www.laundryboxcleaners.com/contact-us.html" target="" data-title="Home" data-pageid="121095">Contact Us</a>
                        </li>
                        <li style="">
                            <a href="http://www.laundryboxcleaners.com/how-it-works.html" target="" data-title="Home" data-pageid="121095">How It Works</a>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="span12">

                    <div id="main_content">
                        <?= $content; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="span12" id="pre-footer" >
                    <strong>info@laundryboxcleaners.com &nbsp; &nbsp;&nbsp; 503-832-7022</strong>
                </div>
            </div>
            
        </div>
        <div id="footer" class="container">
            <div class="row">
                <div class="span12">
                    <ul class="unstyled" id="social-links">
                        <li><a href="#"><img src="/images/laundryboxcleaners/facebook.png" title="facebook"/></a></li>
                        <li><a href="#"><img src="/images/laundryboxcleaners/twitter.png" title="twitter"/></a></li>
                    </ul>
                </div>
            </div>
        </div>



    </body>
</html>