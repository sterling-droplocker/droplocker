
<!doctype html>
<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:website="http://ogp.me/ns/website" lang="en-US" itemscope itemtype="http://schema.org/WebPage" >

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <meta name="viewport" content="initial-scale=1">
  
  <!-- This is Squarespace. -->
<base href="">
<meta charset="utf-8" />
<title>Press Cleaners</title>
<link rel="shortcut icon" type="image/x-icon" href="/images/presscleaners/favicon.ico"/>
<link rel="canonical" href="http://www.pressatlanta.com/"/>
<meta property="og:site_name" content="Press Cleaners"/>
<meta property="og:title" content="Home"/>
<meta property="og:url" content="http://www.pressatlanta.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:image" content="/images/presscleaners/asset_1.png"/>
<meta itemprop="name" content="Home"/>
<meta itemprop="url" content="http://www.pressatlanta.com/"/>
<meta itemprop="thumbnailUrl" content="/images/presscleaners/asset_2.png"/>
<link rel="image_src" href="/images/presscleaners/asset_3.png" />
<meta itemprop="image" content="/images/presscleaners/asset_4.png"/>
<meta name="twitter:title" content="Home"/>
<meta name="twitter:image" content="/images/presscleaners/asset_5.png"/>
<meta name="twitter:url" content="http://www.pressatlanta.com/"/>
<meta name="twitter:card" content="summary"/>
<meta name="description" content="" />

<script type="text/javascript">SQUARESPACE_ROLLUPS = {};</script>
<script>(function(rollups, name) { if (!rollups[name]) { rollups[name] = {}; } rollups[name].js = ["/js/presscleaners/common-df74a0490af8af881a93-min.js"]; })(SQUARESPACE_ROLLUPS, 'squarespace-common');</script>
<script crossorigin="anonymous" src="/js/presscleaners/common-df74a0490af8af881a93-min.js"></script>
<script type="text/javascript" data-sqs-type="dynamic-assets-loader">
(function() {
    (function(a) {
        function e(b) {
            -1 !== b.indexOf(".js") ? document.write("<script " + f + 'crossorigin="anonymous" src="' + b + '">\x3c/script>') : document.write('<link rel="stylesheet" type="text/css" href="' + b + '" />')
        }
        var g = function() {
                var b = window.location.search,
                    a = b.indexOf("exclude=");
                return -1 < a && (b = b.substr(a + 8).split("&")[0]) ? b.split(",") : []
            }(),
            f;
        f = window.__beta_flag_should_defer ? 'defer="defer" ' : "";
        try {
            if (window.top != window && window.top.Squarespace && window.top.Squarespace.frameAvailable) {
                window.top.Squarespace.frameAvailable(window,
                    SQUARESPACE_ROLLUPS);
                return
            }
        } catch (h) {
            console.error(h)
        }
        for (var c in a)
            if (!(-1 < g.indexOf(c))) {
                if (a[c].js)
                    for (var d = 0; d < a[c].js.length; d++) e(a[c].js[d]);
                if (a[c].css)
                    for (d = 0; d < a[c].css.length; d++) e(a[c].css[d])
            }
    })(SQUARESPACE_ROLLUPS);
})();
</script>

<script>
Static.SQUARESPACE_CONTEXT= {
    "facebookAppId":"314192535267336",
    "rollups": {}
    ,
    "pageType":2,
    "website": {
        "id":"57444d4b7da24f12d8588542",
        "identifier":"stephen-moore-phwa",
        "websiteType":1,
        "contentModifiedOn":1467198485952,
        "cloneable":false,
        "siteStatus": {}
        ,
        "language": "en-US", "timeZone": "America/New_York", "machineTimeZoneOffset": -14400000, "timeZoneOffset": -14400000, "timeZoneAbbr": "EDT", "siteTitle": "Press Cleaners", "siteDescription": "<p>Press Cleaners</p><p>We provide quality dry cleaning and laundry service conveniently to your door. &nbsp;Pickup and delivery is free. &nbsp;</p>", "logoImageId":"5747306e01dbaed8f6e7a9da", "shareButtonOptions": {
            "6": true, "2": true, "1": true, "4": true, "5": true, "8": true, "3": true, "7": true
        }
        ,
        "logoImageUrl":"/images/presscleaners/logo2.png",
        "baseUrl":"http://www.pressatlanta.com",
        "primaryDomain":"www.pressatlanta.com",
        "typekitId":"",
        "statsMigrated":false,
        "imageMetadataProcessingEnabled":false
    }
    ,
    "websiteSettings": {
        "id":"57444d4b7da24f12d8588545",
        "websiteId":"57444d4b7da24f12d8588542",
        "subjects":[],
        "country":"US",
        "state":"GA",
        "simpleLikingEnabled":true,
        "mobileInfoBarSettings": {
            "isContactEmailEnabled": false, "isContactPhoneNumberEnabled": false, "isLocationEnabled": false, "isBusinessHoursEnabled": false
        }
        ,
        "lastAgreedTermsOfService":2,
        "defaultPostFormat":"%t",
        "commentLikesAllowed":true,
        "commentAnonAllowed":true,
        "commentThreaded":true,
        "commentApprovalRequired":false,
        "commentAvatarsOn":true,
        "commentSortType":2,
        "commentFlagThreshold":0,
        "commentFlagsAllowed":true,
        "commentEnableByDefault":true,
        "commentDisableAfterDaysDefault":0,
        "disqusShortname":"",
        "homepageTitleFormat":"%s",
        "collectionTitleFormat":"%c \u2014 %s",
        "itemTitleFormat":"%i \u2014 %s",
        "commentsEnabled":false,
        "allowSquarespacePromotion":true,
        "storeSettings": {
            "returnPolicy":null,
            "termsOfService":null,
            "privacyPolicy":null,
            "paymentSettings": {}
            ,
            "expressCheckout": false, "useLightCart": false, "showNoteField": false, "currenciesSupported": ["USD", "CAD", "GBP", "AUD", "EUR", "CHF", "NOK", "SEK", "DKK"], "defaultCurrency": "USD", "selectedCurrency": "USD", "measurementStandard": 1, "orderConfirmationInjectCode": "", "showCustomCheckoutForm": false, "enableMailingListOptInByDefault": true, "sameAsRetailLocation": false, "stripeConnected": false, "isLive": false, "storeState": 3
        }
        ,
        "useEscapeKeyToLogin":true,
        "ssBadgeType":1,
        "ssBadgePosition":4,
        "ssBadgeVisibility":1,
        "ssBadgeDevices":1,
        "pinterestOverlayOptions": {
            "mode": "disabled"
        }
    }
    ,
    "websiteCloneable":false,
    "collection": {
        "title": "Home", "id": "57461214e707ebae3e33d61e", "fullUrl": "https://www.pressatlanta.com/", "publicCommentCount": 0, "type": 10
    }
    ,
    "subscribed":false,
    "appDomain":"squarespace.com",
    "secureDomain":"/images/presscleaners/",
    "templateTweakable":true,
    "tweakJSON": {
        "outerPadding": "50px", "pagePadding": "52px", "product-gallery-auto-crop": "true", "product-image-auto-crop": "false", "topPadding": "10px"
    }
    ,
    "templateId":"50521cf884aeb45fa5cfdb80",
    "pageFeatures":[1,
    2,
    4],
    "impersonatedSession":false,
    "demoCollections":[ {
        "collectionId": "53dbedffe4b010de21d1e87e", "deleted": false
    }
    ,
    {
        "collectionId": "53dbee0fe4b010de21d1e898", "deleted": false
    }
    ,
    {
        "collectionId": "53dbee15e4b010de21d1e8a4", "deleted": false
    }
    ,
    {
        "collectionId": "53dbee1ae4b0475b8bba6e03", "deleted": false
    }
    ,
    {
        "collectionId": "53eca836e4b04a8ebc02b72f", "deleted": false
    }
    ,
    {
        "collectionId": "53dbbcb1e4b067cbb1f60e31", "deleted": false
    }
    ,
    {
        "collectionId": "53e14a0ee4b09a566104d01b", "deleted": false
    }
    ],
    "isFacebookTab":false,
    "tzData": {
        "zones":[[-300,
        "US",
        "E%sT",
        null]],
        "rules": {
            "US": [[1967, 2006, null, "Oct", "lastSun", "2:00", "0", "S"], [1987, 2006, null, "Apr", "Sun>=1", "2:00", "1:00", "D"], [2007, "max", null, "Mar", "Sun>=8", "2:00", "1:00", "D"], [2007, "max", null, "Nov", "Sun>=1", "2:00", "0", "S"]]
        }
    }
}

;	
</script>
<script type="text/javascript"> SquarespaceFonts.loadViaContext(); Squarespace.load(window);</script>

        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<link rel="stylesheet" type="text/css" href="/css/presscleaners/site.css?&filterFeatures=false"/>
  <script type="text/javascript" src="/js/presscleaners/dynamic-data.js"></script>
  <script type="text/javascript" src="/js/presscleaners/site.js"></script>
        
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src='/bootstrap/js/bootstrap.min.js'></script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
</head>

<body class="show-products-category-navigation   page-borders-thick canvas-style-normal  header-subtitle-none banner-alignment-center blog-layout-left-sidebar project-layout-left-sidebar thumbnails-on-open-page-show-all social-icon-style-round hide-social-icons hide-info-footer hide-page-banner hide-page-title   hide-article-author product-list-titles-under product-list-alignment-center product-item-size-32-standard  product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing   event-thumbnails event-thumbnail-size-32-standard event-date-label event-date-label-time event-list-show-cats event-list-date event-list-time event-list-address   event-icalgcal-links  event-excerpts  event-item-back-link      opentable-style-light newsletter-style-dark small-button-style-outline small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square button-style-solid button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd collection-type-page collection-layout-default collection-57461214e707ebae3e33d61e homepage mobile-style-available logo-image" id="collection-57461214e707ebae3e33d61e">

  <div id="canvas">

    <div id="mobileNav" class="">
      <div class="wrapper">
        <nav class="main-nav mobileNav"><ul>
    

        <li class="page-collection active-link">

          

            
              <a href="https://www.pressatlanta.com/">Home</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/process/">Process</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/dry-cleaning-delivery-atlanta-services/">Services</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/dry-cleaning-delivery-atlanta-pricing/">Pricing</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/faqs-dry-cleaning-delivery/">FAQ’s</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/about-press-atlanta-dry-cleaning-delivery/">About</a>
            

            


          

        </li>

    

        <li class="blog-collection">

          

            
              <a href="https://www.pressatlanta.com/blog/">News</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/contact-press-atlanta-dry-cleaning-delivery/">Contact</a>
            

            


          

        </li>

    

        <li class=" external-link">

          

            

            
              <a href="http://www.facebook.com/pages/Press-Atlanta/378334546110">Facebook</a>
            


          

        </li>

    

        <li class=" external-link">

          

            

            
              <a href="http://twitter.com/pressatlanta/">twitter</a>
            


          

        </li>

    

        <li class=" external-link">

          

            

            
              <a href="http://www.pressatlanta.com/feed">feed</a>
            


          

        </li>

    


    
</ul>
</nav>

      </div>
    </div>
    <div id="mobileMenuLink"><a>Menu</a></div>

    <header id="header" class="clear">

    


      
      <div class="site-info">
        <div class="site-address">Street Address</div>
        <div class="site-city-state">City, State, Zip</div>
        <div class="site-phone">Phone Number</div>
      </div>
      

      <div class="site-tag-line">
        
      </div>

      <div class="custom-info">
        <div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Header Subtitle: Custom Content" data-type="block-field" data-updated-on="1381761023501" id="customInfoBlock"><div class="row sqs-row"><div class="col sqs-col-12 span-12"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-81eee4bbda3d9efd0834"><div class="sqs-block-content"><p class="text-align-center">Your Custom Text Here</p></div></div></div></div></div>
      </div>

      <div id="lower-logo">
        <h1 class="logo" data-content-field="site-title"><a href="https://www.pressatlanta.com/"><img src="/images/presscleaners/asset_6.png" alt="Press Cleaners" /></a></h1>
      </div>
      <script>
        Y.use('squarespace-ui-base', function(Y) {
          Y.one("#lower-logo .logo").plug(Y.Squarespace.TextShrink, {
            parentEl: Y.one('#lower-logo')
          });
        });
      </script>

    
      <div id="topNav">
  <nav class="main-nav" data-content-field="navigation">
    <ul>
    

        <li class="page-collection active-link">

          

            
              <a href="https://www.pressatlanta.com/">Home</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/process/">Process</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/dry-cleaning-delivery-atlanta-services/">Services</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/dry-cleaning-delivery-atlanta-pricing/">Pricing</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/faqs-dry-cleaning-delivery/">FAQ’s</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/about-press-atlanta-dry-cleaning-delivery/">About</a>
            

            


          

        </li>

    

        <li class="blog-collection">

          

            
              <a href="https://www.pressatlanta.com/blog/">News</a>
            

            


          

        </li>

    

        <li class="page-collection">

          

            
              <a href="https://www.pressatlanta.com/contact-press-atlanta-dry-cleaning-delivery/">Contact</a>
            

            


          

        </li>

    

        <li class=" external-link">

          

            

            
              <a href="http://www.facebook.com/pages/Press-Atlanta/378334546110">Facebook</a>
            


          

        </li>

    

        <li class=" external-link">

          

            

            
              <a href="http://twitter.com/pressatlanta/">twitter</a>
            


          

        </li>

    

        <li class=" external-link">

          

            

            
              <a href="http://www.pressatlanta.com/feed">feed</a>
            


          

        </li>



    
  </ul>
  <div class="page-divider"></div>
  </nav>
</div>


    </header>

    <div class="page-divider top-divider"></div>

    <!-- // page image or divider -->
    
      

      
    

    <section id="page" class="clear" role="main" data-content-field="main-content" data-collection-id="57461214e707ebae3e33d61e" data-collection-id="57461214e707ebae3e33d61e" data-edit-main-image="Banner" >

      <!-- // CATEGORY NAV -->
      

      <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1467198267886" id="page-57461214e707ebae3e33d61e">
      		<?=$content?>
      </div>

    </section>

    <div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Home Footer Content" data-type="block-field" id="collection-57461214e707ebae3e33d61e"><div class="row sqs-row"><div class="col sqs-col-12 span-12"></div></div></div>

    <!-- <div class="page-divider bottom-divider"></div> -->

    <div class="info-footer-wrapper clear">
      <div class="info-footer">
      <div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Info Footer Content" data-type="block-field" data-updated-on="1406913522646" id="infoFooterBlock"><div class="row sqs-row"><div class="col sqs-col-12 span-12"></div></div></div>
      
        
      
      </div>
    </div>

    <footer id="footer" class="clear">
      <div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Footer Content" data-type="block-field" data-updated-on="1468263024592" id="footerBlock"><div class="row sqs-row"><div class="col sqs-col-4 span-4"><div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-yui_3_17_2_18_1464730694093_3130"><div class="sqs-block-content"><p><em>©2010-16 Press Atlanta Dry Cleaning</em><br /><em>All Rights Reserved |&nbsp;</em><a href="https://www.pressatlanta.com/privacy">Privacy</a><em>&nbsp;|&nbsp;</em><a href="https://www.pressatlanta.com/legal">Terms of Use</a></p></div></div></div></div></div>
    </footer>

  </div>

  <div></div>

  <script type="text/javascript" data-sqs-type="imageloader-bootstraper">(function() {if(window.ImageLoader) { window.ImageLoader.bootstrap(); }})();</script>
<script>Squarespace.afterBodyLoad(Y);</script>



</body>

</html>
