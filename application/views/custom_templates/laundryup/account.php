<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="LaundryUp is a 24/7 dry cleaning & laundry delivery service in Southern California. Pickup & delivery is always free.">
  <!-- Bootstrap CSS -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
  <!-- Open Sans Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  
  <link rel="stylesheet" href="/css/laundryup/bootstrap2x.css">

  <!-- Custom LaundryUp CSS -->
  <link rel="stylesheet" href="/css/laundryup/styles.css">
  <!-- Browser Tab Icon -->
  <link rel="icon" type="image/png" href="/images/laundryup/LaundryUp-Icon-Color.png" sizes="16x16">
  <link rel="icon" type="image/png" href="/images/laundryup/LaundryUp-Icon-Color.png" sizes="32x32">
  <title>LaundryUp | Southern California's 24/7 On Demand Laundry & Dry Cleaning Delivery Service</title>

  <link rel="shortcut icon" href="/images/laundryup/favicon.ico">

  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <style>
      label {
          color: #333;
      }
      .btn-send {
          font-weight: 300;
          text-transform: uppercase;
          letter-spacing: 0.1em;
          margin-bottom: 20px;
      }


      .help-block.with-errors {
          color: #ff5050;
          margin-top: 5px;
      }

      #bootstrap2x .span2 {
            width: 100px;
      }
      #bootstrap2x input, #bootstrap2x select {
            height: 1.8rem;
      }

    #bootstrap2x #update_kiosk_card_form input[type="submit"].btn {
      margin-bottom: -.1rem;
      width: 5em;
    }
      
  </style>
    <script type='text/javascript' src='/js/laundryup/jquery.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script src='/bootstrap/js/bootstrap.min.js'></script>
    <script type="text/javascript">
      var $ = jQuery.noConflict();
    </script>
    <?= $_scripts ?>
    <?= $javascript ?>
    <?= $style ?>
    <?= $_styles ?>
</head>
<body>
  <nav class="navbar sticky-top navbar-expand-sm navbar-light bg-light">
    <a class="navbar-brand" href="https://laundryup.com/index.html">
      <img src="/images/laundryup/LaundryUp-Full-Color.png" height="50" alt="LaundryUp logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
          <a class="nav-link" href="https://laundryup.com/locations.html">Locations</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://laundryup.com/index.html#howitworks">How it Works</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://laundryup.com/pricing.html">Pricing</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://laundryup.com/property_managers.html">Property Managers</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://laundryup.com/faq.html">FAQ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://laundryup.com/index.html#contact">Contact</a>
        </li>
      </ul>
      <ul class="nav justify-content-end">
        <li class="nav-item">
          <a class="nav-link" href="https://myaccount.laundryup.com/login">Log In</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://myaccount.laundryup.com/register">Register</a>
        </li>
      </ul>
    </div>
  </nav>
  <!--===== End Navigation =====-->

  <div id="bootstrap2x">
      <section class="container-fluid">
      <br/>
            <?=$content?>
      </section>
  </div>

  <footer class="container-fluid bg-lu mt-5 pb-3">
    <div class="row">
      <div class="col d-flex justify-content-around my-4">
        <a href="https://itunes.apple.com/us/app/laundryup/id1371649772?mt=8"><img class="img-fluid px-2" src="/images/laundryup/apple_256.png" alt="Apple app store badge"></a>
        <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.laundryUp"><img class="img-fluid px-2" src="/images/laundryup/google_256.png" alt="Google play store badge"></a>
      </div>
    </div>
    <div class="row d-flex justify-content-between align-items-start px-3 my-2">
      <div>
        <p>©LaundryUp. All rights reserved.</p>
      </div>
      <div>
        <a href="https://www.facebook.com/hellolaundryup" target="_blank"><i class="fab fa-facebook-f social-icon"></i></a>
        <a class="px-4" href="https://twitter.com/laundry_up" target="_blank"><i class="fab fa-twitter social-icon"></i></a>
      </div>
    </div>
  </footer>
</body>
</html>
