<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/hakuyosha/style.css?v=1">

    <link rel="icon" type="image/x-icon" href="https://irp-cdn.multiscreensite.com/b8f29ae4/site_favicon_16_1423795112791.ico">

    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>
</head>
<body>
<div id="fb-root"></div>
<div class="container">
    <div>
        <!-- End of facebook code -->

        <?
        //if this is using the exterior pages - make sure NOT to hide the header/footer
        // $responsive = '';
        // $uri_string = $this->uri->uri_string();
        // if( strpos($uri_string,'main/') === false ){
        $responsive = 'hidden-phone hidden-tablet';
        // }
        ?>

        <div class="span12 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="/main"><img src="/images/hakuyosha/hakuyosha_24_7.png" style="height:46px;" /></a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a class="navText" href="http://hakuyosha.com/">Home</a></li>
                            <li><a class="navText" href="http://hakuyosha.com/services.html">Services</a></li>
                            <li><a class="navText" href="http://hakuyosha.com/locations.html">Locations</a></li>
                            <li><a class="navText" href="http://hakuyosha.com/contact.html">Contact</a></li>
                         </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container">
                    <div id="" class="row-fluid">
                        <div class="span12 center">
                            <div class="navbar">
                                <div class="navbar-inner-menu">
                                    <a href="http://www.hakuyosha.com/">
                                        <img src="/images/hakuyosha/hakuyosha_24_7.png" class="logo">
                                    </a>
                                    <ul class="nav">
                                        <li><a class="navText" href="http://hakuyosha.com/">Home</a></li>
                                        <li><a class="navText" href="http://hakuyosha.com/services.html">Services</a></li>
                                        <li><a class="navText" href="http://hakuyosha.com/locations.html">Locations</a></li>
                                        <li><a class="navText" href="http://hakuyosha.com/contact.html">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row-fluid" style=''>
            <div class="span12 center">
                <?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="span12 center" id="footerInner">
                <div class="row-fluid">
                    <div class="span8 footerSpan awards">
                        <img src="/images/hakuyosha/hakuyosha%20awards%20newest-1000x308.jpg" alt="Best of Honolulu, Star Advertiser's Best of Hawaii Dry Cleaner awards" />
                    </div>

                    <div class="span3 footerSpan bbb">
                        <a href="http://www.bbb.org/hawaii/business-reviews/dry-cleaners/hakuyosha-international-in-honolulu-hi-577#sealclick" id="1663239626">
                            <img src="https://irp-cdn.multiscreensite.com/b8f29ae4/dms3rep/multi/mobile/blue-seal-153-100-hakuyoshainternationalinc-577-153x100.png" alt="Click for the BBB Business Review of this Dry Cleaners in Honolulu HI">
                        </a>
                    </div>
                </div>
                <div class="row-fluid">
                    <p align="center">
                        <a target"new" href="http://www.vivial.net" rel="nofollow">Built by Vivial</a> |
                        <a target="new" href="http://www.vivial.net/privacy" rel="nofollow">Privacy Policy</a>
                    </p>
                </div>
            </div>
        </div>
        </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
    <?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

    <?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
    <?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
    <?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>