<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta charset="utf-8"/>
    <title>Freshbox 24/7 | <?= $title ?></title>

<link rel="shortcut icon" href="/images/freshbox247/pfavico.ico"/>
<link rel="apple-touch-icon" href="/images/freshbox247/pfavico.ico"/>

<meta http-equiv="X-Wix-Renderer-Server" content="apu1.aus"/>
<meta http-equiv="X-Wix-Meta-Site-Id" content="72ed7c2c-62db-41b1-b3af-f5ea6fefc9e2"/>
<meta http-equiv="X-Wix-Application-Instance-Id" content="4c7e5dc1-5b8c-4f2d-90b9-4e089c1e1be6"/>
<meta http-equiv="X-Wix-Published-Version" content="72"/>
<meta http-equiv="etag" content="2f07267edac84575f07039479f62a426"/>
<meta property="og:title" content="freshbox247new"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.freshbox247.com/"/>
<meta property="og:site_name" content="freshbox247new"/>
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

<meta id="wixMobileViewport" name="viewport" content="minimum-scale=0.25, maximum-scale=1.2"/>
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">


<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Enriqueta:n,b,i,bi|&subset=latin">
<link rel="stylesheet" href="https://www.google.com/fonts#UsePlace:use/Collection:Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300">


<link rel="stylesheet" href="/css/freshbox247/style.css">

<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>

<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>

<body>

<div class="top-ruler"></div>

<div id="header">

<div class="navbar" id="header-nav">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="btn btn-navbar" data-toggle="collapse" data-target="#main-nav">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="http://freshbox247.com">
          <img src="/images/freshbox247/logo.png">
      </a>

      <div class="nav-collapse" id="main-nav">
        <ul class="nav suckerfish">
            <li><a href="http://www.freshbox247.com/">Home</a></li>
            <li><a href="http://www.freshbox247.com/#!how-it-works/c1386">How It Works</a></li>
            <li><a href="http://www.freshbox247.com/#!portfolio/cjg9">24/7 Service</a></li>
            <li><a href="http://www.freshbox247.com/#!contact/c24vq">Contact</a></li>
            <li><a href="http://www.freshbox247.com/">More</a>
                <ul>
                    <li><a href="http://www.freshbox247.com/#!get-freshbox-in-your-building/ct4d">Get Freshbox in your building</a></li>
                    <li><a href="http://www.freshbox247.com/#!price-list/c1adz">Price List</a></li>
                </ul>
            </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div> <!-- End Navbar -->

</div>

 


<div id="main_content">
<?= $content ?>
</div>

<div id="footer">
    <div class="social">
        <a href="http://www.facebook.com/freshbox247" target="_blank"><img src="/images/freshbox247/fb.png"></a>
    </div>
    <div class="copy">
        2014 by FRESHBOX DESIGN
    </div>
</div>
    
    
<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>
</body>
</html>
