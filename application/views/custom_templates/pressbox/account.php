<?php
/**
 * New Business Account Template
 */
    $temp_host_url = 'http://usepressbox.com';
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
        <script type="text/javascript" src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>

        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
         <link rel="stylesheet" media="all" href="/css/pressbox/all.css">
        <link rel="stylesheet" type="text/css" media="all" href="/css/pressbox/style.css"  />
<link rel='stylesheet' id='contact-form-7-css'  href='/css/pressbox/styles_version_3_4_2.css' type='text/css' media='all' />

        <!-- <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script> -->
        <script type="text/javascript" src='/js/functions.js'></script>
        <script type="text/javascript" src="/js/pressbox/jquery.main.js"></script>

        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', '<?= $this->business->website_url;?>']);
    	  _gaq.push(['_trackPageview']);
    	
    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts;?>
        <?= $style?>
        <?= $_styles ?>
       <style type="text/css">
           #interior_mobile_menu{
              /* display:none !important; */
           }
       </style>
        <!--[if lt IE 9]><link rel="stylesheet" media="all" href="/css/pressbox/ie.css"><![endif]-->
        <!--[if IE]><script type="text/javascript" src="/js/ie.js"></script><![endif]-->

    </head>
    <body>
        <div id="wrapper">
            <header id="header" class="hidden-phone hidden-tablet">
                <div class="header-holder open-close">
                    <h1 class="logo"><a href="<?=$temp_host_url;?>">PressBox</a></h1>
                    <a href="#" class="nav-opener">Open navigation</a>
                    <nav id="nav" class="slide">
                        <ul>
                            <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="<?=$temp_host_url;?>/how-it-works">How It Works</a></li>
                            <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="<?=$temp_host_url;?>/pricing">Pricing</a></li>
                            <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="<?=$temp_host_url;?>/sample-page/faq">Faq</a></li>
                            <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="<?=$temp_host_url;?>/locations">Locations</a></li>
                            <li id="menu-item-113" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113"><a href="<?=$temp_host_url;?>/contact">Contact</a></li>
                        </ul>
                    </nav>
                </div>

            </header>

            <section class="content-section">
            <?php 
            // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
            echo $content 
            ?>
            </section>

            <div class='container-fluid hidden-phone' id="footer">
                <?php echo $footer?>
            </div>

            <div class='container-fluid visible-phone' id="footer">
                <div class='span12'>
                    <div class='well'>
                        Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                    </div>
                </div>
            </div>
        
        </div>
            
        <div style="margin-top:10px;">
         <div id='footer'>
            <footer id="footer">
			<p>&copy; Pressbox, LLC 2013</p>
		</footer>
        </div>
        
        <div style="text-align: right; padding:5px;">
         	<a href="https://droplocker.com" style="display: inline-block; padding: 5px;"><img style="width:189px" align="center" src="https://droplocker.com/images/logo-powered.jpg"></a>
        </div>
        </div>

        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <script>
        	$('.dropdown-toggle').dropdown();
    	</script>

        <script type='text/javascript' src='/js/pressbox/scripts_version_3_4_2.js'></script>
    </body>
</html>
