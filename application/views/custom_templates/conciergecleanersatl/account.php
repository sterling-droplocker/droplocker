<?php
/**
 * New Business Account Template
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type="text/javascript" src='/js/functions.js'></script>

        <?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
        <script type="text/javascript">
    	  var _gaq = _gaq || [];
    	  _gaq.push(['_setAccount', '<?php echo $ga?>']);
    	  _gaq.push(['_setDomainName', 'dashlocker.com']);
    	  _gaq.push(['_trackPageview']);

    	  (function() {
    	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    	  })();
    	</script>
    	<?php endif; ?>
        <?= $javascript?>
        <?= $_scripts?>
        <?= $style?>
        <?= $_styles ?>


        <script src="/css/conciergecleanersatl/js/jquery-ui-1.10.3.custom.min.js"></script>
        <script src="/css/conciergecleanersatl/js/bootstrap.min.js"></script>
        <script src="/css/conciergecleanersatl/js/jssor.slider.mini.js"></script>

        <script type="text/javascript">
        (function($) { "use strict";
        			jQuery(document).ready(function() {
        				var offset = 350;
        				var duration = 500;
        				jQuery(window).scroll(function() {
        					if (jQuery(this).scrollTop() > offset) {
        						jQuery('.scroll-to-top').fadeIn(duration);
        					} else {
        						jQuery('.scroll-to-top').fadeOut(duration);
        					}
        				});
        				jQuery('.scroll-to-top').click(function(event) {
        					event.preventDefault();
        					jQuery('html, body').animate({scrollTop: 0}, duration);
        					return false;
        				})
        			});
        })(jQuery);

        jQuery("#wpadminbar").css('display','none');
        </script>
        <script type='text/javascript' src='/css/conciergecleanersatl/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
        <script type='text/javascript' src='/css/conciergecleanersatl/js/scripts.js?ver=4.2'></script>
        <script type='text/javascript' src='/css/conciergecleanersatl/js/comment-reply.min.js?ver=4.2.5'></script>
        <script type='text/javascript' src='/css/conciergecleanersatl/js/navigation.js?ver=20140711'></script>

        <style type="text/css">
            #wpadminbar{display: none !important;}
        </style>

        <link rel="stylesheet" href="/css/conciergecleanersatl/bootstrap.css">
        <link rel="stylesheet" href="/css/conciergecleanersatl/flat-ui.css">
        <link rel="stylesheet" href="/css/conciergecleanersatl/styles.css">
        <link rel="stylesheet" href="/css/conciergecleanersatl/theme-elements.css">
        <!--[if lt IE 9]>
              <script src="http://www.conciergecleanersatl.com/wp-content/themes/conciergecleanersatl/js/html5.js" type="text/javascript"></script>
        <![endif]-->

    	<script type="text/javascript">
    		window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.conciergecleanersatl.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.2.5"}};
    		!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    	</script>
    	<style type="text/css">
        img.wp-smiley,
        img.emoji {
        	display: inline !important;
        	border: none !important;
        	box-shadow: none !important;
        	height: 1em !important;
        	width: 1em !important;
        	margin: 0 .07em !important;
        	vertical-align: -0.1em !important;
        	background: none !important;
        	padding: 0 !important;
        }
        </style>
        <link rel='stylesheet' id='contact-form-7-css'  href='/css/conciergecleanersatl/styles.css?ver=4.2' type='text/css' media='all' />
        <link rel='stylesheet' id='nextend_fb_connect_stylesheet-css'  href='/css/conciergecleanersatl/facebook-btn.css?ver=4.2.5' type='text/css' media='all' />
        <link rel='stylesheet' id='rs-settings-css'  href='/css/conciergecleanersatl/settings.css?rev=4.1.4&#038;ver=4.2.5' type='text/css' media='all' />
        <link rel='stylesheet' id='rs-captions-css'  href='/css/conciergecleanersatl/dynamic-captions.css?rev=4.1.4&#038;ver=4.2.5' type='text/css' media='all' />
        <link rel='stylesheet' id='rs-plugin-static-css'  href='/css/conciergecleanersatl/static-captions.css?rev=4.1.4&#038;ver=4.2.5' type='text/css' media='all' />
        <link rel='stylesheet' id='twentytwelve-fonts-css'  href='/css/conciergecleanersatl/css.css?family=Open+Sans:400italic,700italic,400,700&#038;subset=latin,latin-ext' type='text/css' media='all' />
        <link rel='stylesheet' id='twentytwelve-style-css'  href='/css/conciergecleanersatl/styles.css?ver=4.2.5' type='text/css' media='all' />
        <!--[if lt IE 9]>
        <link rel='stylesheet' id='twentytwelve-ie-css'  href='/css/conciergecleanersatl/ie.css?ver=20121010' type='text/css' media='all' />
        <![endif]-->
        <link rel='stylesheet' id='rpt-css'  href='/css/conciergecleanersatl/rpt_style.min.css?ver=4.2.5' type='text/css' media='all' />
        <script type='text/javascript' src='/css/conciergecleanersatl/js/jquery-migrate.min.js?ver=1.2.1'></script>
        <script type='text/javascript' src='/css/conciergecleanersatl/js/jquery.themepunch.plugins.min.js?rev=4.1.4&#038;ver=4.2.5'></script>
        <script type='text/javascript' src='/css/conciergecleanersatl/js/jquery.themepunch.revolution.min.js?rev=4.1.4&#038;ver=4.2.5'></script>
        <link rel='shortlink' href='http://www.conciergecleanersatl.com/' />
        <link rel='stylesheet' id='wop-css'  href='/css/conciergecleanersatl/wop.css' type='text/css' media='all' />	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <style>
            .navbar{padding:0px !important;}
        </style>

        <link rel='stylesheet' href='/css/conciergecleanersatl/boostrapmods.css' type='text/css' media='all' />
    </head>

    <body class="home page page-id-5 page-template page-template-page-templates page-template-Homepage page-template-page-templatesHomepage-php custom-font-enabled single-author">

        <header class="navbar-fixed-top" style="height: 140px;">
          <div class="container">
            <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12 padding0 logo">
        		<a href="http://www.conciergecleanersatl.com/" class="navbar-brand"><img src="/css/conciergecleanersatl/img/Laundry-Logo.png" alt="Concierge Cleaners" /></a>
            </div>
            <div class=" navigation col-lg-9 col-md-9 col-sm-9 col-xs-12 padding0 phone-width">
              <p class="pull-right phone"><img src="/css/conciergecleanersatl/img/phone.png">404-227-1574</p>
               <ul class="sign-up">
                  <li><a href="http://myaccount.conciergecleanersatl.com/login">Login</a></li>
                  <li><a href="http://myaccount.conciergecleanersatl.com/register">Signup</a></li>
                </ul>
              <nav role="navigation" class="navbar col-lg-12 col-md-12 col-sm-12  ">
                <div class="navbar-header skrollable skrollable-between" data-200="padding: 0px 0px;" data-start="padding: 15px 0px;" style="padding:3px 0px 0px 0px;">
                  <button  data-toggle="collapse" class=" navbar-toggle collapsed btn2menu" type="button"> <span class="sr-only">Toggle navigation</span> </button>
                </div>
                <div id="navbar-collapse-01" class="navbar-collapse navbar-responsive-collapse in" style="height: 2px;">
                <div class="menu-menu-1-container">
                    <ul class="nav navbar-nav pull-right  header-ul">
                        <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://www.conciergecleanersatl.com/services/">Services</a></li>
                        <li id="menu-item-94" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-94"><a href="http://www.conciergecleanersatl.com/faqs/">FAQ&#8217;s</a></li>
                        <li id="menu-item-91" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-91"><a href="http://www.conciergecleanersatl.com/how-it-works/">How it Works</a></li>
                        <li id="menu-item-66" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><a href="http://www.conciergecleanersatl.com/about-us/">About Us</a></li>
                    </ul>
                    </div>

                </div>
              </nav>


            </div>
          </div>
        </header>

        <div style='min-height:600px;' id="mainContent">
        <?php
        // Shows the main content section, which has the sidebar... If you are wondering where the sidebar is
        echo $content
        ?>
        </div>
        <div class='container-fluid hidden-phone' id="footer">
            <?php echo $footer?>
        </div>
        <div class='container-fluid visible-phone' id="footer">
            <div class='span12'>
                <div class='well'>
                    Copyright <?php echo $this->business->companyName?> <?php echo date('Y')?>. All Rights Reserved. | <a href='/account/main/terms'>Terms</a>
                </div>
            </div>
        </div>

        <footer class="footer">
          <div class="container">
            <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-12 contacts">
              <p><span>Contact Us:</span> 770-756-9495 info@conciergecleanersatl.com. </p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
              <div class=" socail">
                <ul class="social-icons follow">
                  <li></li>
                  <li class="rss"><a href="#">RSS</a></li>
                  <li class="twitter"><a href="https://twitter.com/conciergecleans">RSS</a></li>
                  <li class="facebook"><a href="https://www.facebook.com/pages/Concierge-Cleaners/1612925368947920">RSS</a></li>
                  <li class="linkedin"><a href="#">RSS</a></li>
                  <li class=" googleplus"><a href="https://plus.google.com/+ConciergecleanersatlGA/">RSS</a></li>
                  <li class="youtube"><a href="#">RSS</a></li>
                </ul>

              </div>
            </div>
          </div>
        </footer>


    	<script type="text/javascript">
    	$('.container>.row>.container').addClass("offset1");
    	$('.container>.span9').removeAttr('style');
    	$('.container>.span9').addClass("offset1 span7");
    	$('.container>.span9').removeClass("span9");
    	$('.container>.row>.span7').removeClass("container");
    	$('.container>.row>#customer_locations').addClass("offset1");
    	</script>

    	<script type="text/javascript">
    	$(function(){
    	     var navMain = $("#navbar-collapse-01");
    		 var navBtn = $(".btn2menu");
    	     navBtn.on("click", function () {
    	         navMain.toggle();
    	     });
    	 });
    	</script>
    </body>


</html>
