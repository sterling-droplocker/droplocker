<!doctype html>
<!--[if IEMobile 7 ]> <html lang="en-US"class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html lang="en-US" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en-US" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-US" class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-US" class="no-js"><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>What is SoapBox? | SoapBox</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- media-queries.js (fallback) -->
        <!--[if lt IE 9]>
        <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!-- html5.js -->
        <!--[if lt IE 9]>
        <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href='https://fonts.googleapis.com/css?family=Abel|Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
        <link href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
        <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link rel='stylesheet' id='dws_bootstrap-css'  href='/css/soapboxdrycleaning/bootstrap_dws.css' type='text/css' media='all' />
        <link rel='stylesheet' id='dws_shortcodes-css'  href='/css/soapboxdrycleaning/shortcodes.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bootstrap-css'  href='/css/soapboxdrycleaning/bootstrap10.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bootstrap-responsive-css'  href='/css/soapboxdrycleaning/responsive.css' type='text/css' media='all' />
        <link rel='stylesheet' id='wp-bootstrap-css'  href='/css/soapboxdrycleaning/zoom-boot-style.css' type='text/css' media='all' />
        <link rel='stylesheet' id='easy_table_style-css'  href='/css/soapboxdrycleaning/style.css' type='text/css' media='all' />
        <link rel='stylesheet'  href='/css/soapboxdrycleaning/custom.css' type='text/css' media='all' />
        <!--<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">-->
        <link rel="stylesheet" type="text/css" href="/css/soapboxdrycleaning/bootstrapmods.css" />
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src="/js/soapboxdrycleaning/jquery-migrate.min.js" type="text/javascript"></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap.js"></script>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <?= $style ?>
        <?= $_styles ?>
    </head>
    <body class="page page-id-8 page-template page-template-page-full-width-php">
        <header role="banner">
            <div id="inner-header" class="clearfix">
                <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <div class="container-fluid nav-container">
                            <nav role="navigation">
                                <span class="resizeClass"></span>
                                <a class="brand" id="logo" title="Go into 24/7 Dry Cleaning and Laundry!" href="index.php">
                                    <img src="/images/soapboxdrycleaning/logo.png" alt="Go into 24/7 Dry Cleaning and Laundry!">
                                </a>
                                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </a>
                                <div class="nav-collapse">
                                    <ul id="menu-header" class="nav">
                                        <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-8 menu-item-has-children dropdown">
                                            <a href="http://soapboxdrycleaning.com/what-is-soapbox.php" class="dropdown-toggle disabled" data-toggle="dropdown">What is SoapBox?<b class="caret"></b>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-176" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item">
                                                    <a href="http://soapboxdrycleaning.com/what-is-soapbox.php#how-it-works">How It Works</a></li>
                                                <li id="menu-item-178" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item">
                                                    <a href="http://soapboxdrycleaning.com/what-is-soapbox.php#features">Features</a></li>
                                                <li id="menu-item-179" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item">
                                                    <a href="http://soapboxdrycleaning.com/what-is-soapbox.php#soapbucks-rewards">SoapBucks Rewards</a>
                                                </li>
                                                <li id="menu-item-180" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item">
                                                    <a href="http://soapboxdrycleaning.com/faqs.php">FAQs</a>
                                                </li><!--.dropdown-->
                                            </ul>
                                        </li>
                                        <li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown">
                                            <a href="http://soapboxdrycleaning.com/our-prices.php" class="dropdown-toggle disabled" data-toggle="dropdown">Our Prices<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-186" class="menu-item menu-item-type-custom menu-item-object-custom">
                                                    <a href="http://soapboxdrycleaning.com/our-prices.php#dry-cleaning">Dry Cleaning Prices</a></li>
                                                <li id="menu-item-187" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/our-prices.php#wash-fold">Wash &#038; Fold Prices</a></li> <li id="menu-item-261" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/our-prices.php#plans">Wash &#038; Fold Plans</a></li>  <li id="menu-item-189" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/our-prices.php#packages">Package Delivery Prices</a></li><!--.dropdown-->
                                            </ul>
                                        </li><li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown"><a href="http://soapboxdrycleaning.com/get-soapbox.php" class="dropdown-toggle disabled" data-toggle="dropdown">Get SoapBox<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-191" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/get-soapbox.php#building">In Your Building</a></li>  <li id="menu-item-194" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/get-soapbox.php#partner">Partner With Us</a></li><!--.dropdown-->
                                            </ul>
                                        </li><li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown"><a href="http://soapboxdrycleaning.com/about-us.php" class="dropdown-toggle disabled" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li id="menu-item-197" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/about-us.php#contact-us">Contact Us</a></li> <li id="menu-item-198" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/about-us.php#about">About SoapBox</a></li> <li id="menu-item-199" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/about-us.php#partners">Partners</a></li><!--.dropdown-->
                                            </ul>
                                        </li>
                                        <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://soapboxdrycleaning.com/location.php">Locations</a></li>
                                        <li id="menu-item-26" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://soapboxdrycleaning.com/soapbucks-rewards.php">SoapBucks Rewards</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div> <!-- end .nav-container -->
                    </div> <!-- end .navbar-inner -->
                </div> <!-- end .navbar -->
            </div> <!-- end #inner-header -->
        </header> <!-- end header -->
        <div class="container-fluid">

            <div id="content" class="clearfix">
                <div id="main" class="clearfix" role="main">
                    <article id="post-8" class="post-8 page type-page status-publish hentry clearfix" role="article">

                        <section class="post_content" id="main_content">
                            <?= $content; ?>
                        </section> <!-- end article section -->
                        <footer>
                            <p class="clearfix"></p>
                        </footer> <!-- end article footer -->
                    </article> <!-- end article -->
                </div> <!-- end #main -->
            </div> <!-- end #content -->
            <footer role="contentinfo">
                <div id="inner-footer" class="clearfix">
                    <div id="widget-footer" class="clearfix row-fluid">
                        <div class="row">
                            <div class="span3">
                                <h2><img src="/images/soapboxdrycleaning/logo.png" width="200" alt=""></h2>
                                <h5>9135 Judicial Drive Suite B</h5>
                                <h5>San Diego, California 92122</h5>
                                <h5>844-4-SOAPBOX (844-476-2726)</h5>
                                <a href="mailto:info@soapboxcleaning.com">info@soapboxcleaning.com</a>
                                <p></p>
                                <p><small>&copy; Copyright Soapbox drycleaning, Inc 2014</small></p>
                                <p></p>
                            </div>
                            <div class="span3 social-buttons">
                                <h3>SoapBox App</h3>
                                <div class="square"><a href="https://itunes.apple.com/us/app/droplocker/id686269581?mt=8"><i class="icon-apple icon-2x"></i></a></div>
                                <div class="square"><a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities&hl=en"><i class="icon-android icon-2x"></i></a></div>
                            </div>
                            <div class="span3 social-buttons">
                                <h3>Social with SoapBox</h3>
                                <div class="square"><a href="https://twitter.com/soapboxsd" target="_blank"><i class="icon-twitter icon-2x"></i></a></div>
                                <div class="square"><a href="https://www.youtube.com/channel/UCqmEqw44q4qnhHHg9ElGGHA" target="_blank"><i class="icon-youtube icon-2x"></i></a></div>
                                <div class="square"><a href="http://instagram.com/soapboxsd" target="_blank"><i class="icon-instagram icon-2x"></i></a></div>
                                <div class="square"><a class="yelp" href="http://www.yelp.com/biz/soapbox-dry-cleaning-san-diego" target="_blank"></a></div>
                                <div class="square"><a href="https://www.facebook.com/soapboxdrycleaning" target="_blank"><i class="icon-facebook-sign icon-2x"></i></a></div>
                            </div>
                            <div class="span3 green-earth-footer">
                                <img src="/images/soapboxdrycleaning/GEClean.png" width="200px" alt="">
                            </div>
                        </div>
                    </div>
                </div> <!-- end #inner-footer -->
            </footer> <!-- end footer -->
        </div> <!-- end #container -->
        <script type='text/javascript' src='/js/soapboxdrycleaning/scripts.js'></script>
    </body>
    <script>
    $(".container>.row>.container").removeClass("container");
    $(".container>.row>.container").addClass("offset1");
    $(".container>.row>.span7").addClass("offset1");
    $(".container>.span9").addClass("offset1");
    $(".container>.span9").css("margin-left","146px");
    </script>
</html>