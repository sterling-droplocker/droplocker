<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/newash/style.css">
    <link rel="stylesheet" href="/css/font-awesome-4.1.0/css/font-awesome.min.css">

    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>
</head>
<body>
<div id="fb-root"></div>
<div class="container">
    <div>
        <!-- End of facebook code -->

        <?
        //if this is using the exterior pages - make sure NOT to hide the header/footer
        // $responsive = '';
        // $uri_string = $this->uri->uri_string();
        // if( strpos($uri_string,'main/') === false ){
        $responsive = 'hidden-phone hidden-tablet';
        // }
        ?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="http://newash.mx/#home">
                        <img src="/images/newash/logo.png" />
                    </a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="http://newash.mx/#home">Inicio</a></li>
                            <li><a href="http://newash.mx/#about">Quiénes Somos</a></li>
                            <li><a href="http://newash.mx/#team">Servicios</a></li>
                            <li><a href="http://newash.mx/#nosotros">Nosotros</a></li>
                            <li><a href="http://newash.mx/#contact">Contacto</a></li>
                            <?php if (empty($this->customer)): ?>
                                <li><a href="/login">Login</a></li>
                                <li><a href="/register">Registro</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="<?= $responsive; ?>">

                <div class="container">
                    <div id="" class="row">
                        <div>
                            <div class="navbar" data-collapse="medium">
                                <div class="navbar navbar-inner-menu">
                                    <a href="http://newash.mx/#home">
                                        <img src="/images/newash/logo.png" class="logo" style="height:50px;">
                                    </a>
                                    <ul class="nav" style="margin-left:10px">
                                        <li><a class="navText" href="http://newash.mx/#home">Inicio</a></li>
                                        <li><a class="navText" href="http://newash.mx/#about">Quiénes Somos</a></li>
                                        <li><a class="navText" href="http://newash.mx/#team">Servicios</a></li>
                                        <li><a class="navText" href="http://newash.mx/#nosotros">Nosotros</a></li>
                                        <li><a class="navText" href="http://newash.mx/#contact">Contacto</a></li>
                                        <li>
                                            <div class="social-div">
                                                <a class="w-inline-block social fa fa-instagram" href="https://instagram.com/newashtintoreria" data-ix="fade-down-10" style="transition: all 0.5s ease 0s, opacity 800ms, transform 800ms; opacity: 1; transform: translateX(0px) translateY(0px);"></a>
                                                <a class="w-inline-block social fa fa-facebook" href="http://facebook.com/1567268360171366" data-ix="fade-down-11" style="transition: all 0.5s ease 0s, opacity 800ms, transform 800ms; opacity: 1; transform: translateX(0px) translateY(0px);"></a>
                                            </div>                                            
                                        </li>
                       
                                       <?php if (empty($this->customer)): ?>
                                            <li><a class="navText" href="/login">Login</a></li>
                                            <li><a class="navText" href="/register">Registro</a></li>
                                        <?php endif; ?>
                                    </ul>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" style='margin-top: 60px; background: #FFF; padding: 10px'>
            <div class="span10 center">
                <?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="footer">
                <div class="social-div in-footer">
                    <a class="w-inline-block social fa fa-instagram" href="https://instagram.com/newashtintoreria" data-ix="fade-down-10" style="transition: all 0.5s ease 0s, opacity 800ms, transform 800ms; opacity: 1; transform: translateX(0px) translateY(0px);"></a>
                    <a class="w-inline-block social fa fa-facebook" href="http://facebook.com/1567268360171366" data-ix="fade-down-11" style="transition: all 0.5s ease 0s, opacity 800ms, transform 800ms; opacity: 1; transform: translateX(0px) translateY(0px);"></a>
                </div>
                <img class="logo_footer" src="/images/newash/logo_footer.png" alt="logo_footer.png">
                <div class="footer_copyright">NEWASH | 2015</div>
            </div>
        </div>
    </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
    <?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

    <?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
    <?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
    <?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>