<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">

    <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="/css/rockymountainlaundries/style.css">
    <link rel="stylesheet" type="text/css" href="/css/rockymountainlaundries/dashicons.min.css">

    <link rel="shortcut icon" href="/images/rockymountainlaundries/rocky_mountain_laundries_favicon.png">

    <?= $style ?>
    <?= $_styles ?>
    <script type="text/javascript" src='/js/jquery-1.7.2.min.js'></script>
    <script type="text/javascript" src='/js/functions.js'></script>
    <script type="text/javascript" src="/js/jquery.loading_indicator.js"></script>
    <script type="text/javascript" src="/js/showHide.js"></script>

    <?= $_scripts ?>
    <?= $javascript ?>
</head>
<body>
<div id="fb-root"></div>
<div class="container-fluid">
    <div class="row-fluid">
        <!-- End of facebook code -->

        <?
        //if this is using the exterior pages - make sure NOT to hide the header/footer
        // $responsive = '';
        // $uri_string = $this->uri->uri_string();
        // if( strpos($uri_string,'main/') === false ){
        $responsive = 'hidden-phone hidden-tablet';
        // }
        ?>

        <div class="span10 center hidden-desktop visible-phone visible-tablet">

            <div class="navbar">
                <div class="container">

                    <a class="btn btn-navbar" data-toggle="collapse" data-target="#main_menu.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <a class="brand" href="http://www.rockymountainlaundries.com/">
                        <img src="/images/rockymountainlaundries/rocky_mountain_laundries_logo_header.png" />
                    </a>

                    <div class="nav-collapse" id="main_menu">
                        <ul class="nav">
                            <li><a href="http://www.rockymountainlaundries.com/"><i class="_mi _before dashicons dashicons-admin-home" aria-hidden="true"></i><span>Home</span></a></li>
                            <li><a href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-location-friso/"><i class="_mi _before dashicons dashicons-location" aria-hidden="true"></i><span>Frisco, CO</span></a>
                                <ul class="sub-menu">
                                    <li><a href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-friso-unlimited-wash-program/"><i class="_mi _before dashicons dashicons-products" aria-hidden="true"></i><span>Unlimited Wash Program</span></a></li>
                                </ul>
                            </li>
                            <li><a href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-location-grand-lake/"><i class="_mi _before dashicons dashicons-location" aria-hidden="true"></i><span>Grand Lake, CO</span></a></li>
                            <li><a href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-location-idaho-springs/"><i class="_mi _before dashicons dashicons-location" aria-hidden="true"></i><span>Idaho Springs, CO</span></a></li>
                            <li><a href="http://www.rockymountainlaundries.com/store/checkout/"><i class="_mi _before dashicons dashicons-cart" aria-hidden="true"></i><span>Checkout</span></a></li>
                            <li><a href="http://www.rockymountainlaundries.com/contact-rocky-mountain-laundries/"><i class="_mi _before dashicons dashicons-email-alt" aria-hidden="true"></i><span>Contact Us</span></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <div id="header" class="<?= $responsive; ?>">
            <div class="span12 <?= $responsive; ?>">

                <div class="container-fluid">
                    <div id="" class="row-fluid">
                        <div>
                            <div class="navbar" data-collapse="medium">
                                <div class="navbar navbar-inner-menu">
                                    <div class="logo_container">
                                    <a href="http://www.rockymountainlaundries.com/">
                                        <img src="/images/rockymountainlaundries/rocky_mountain_laundries_logo_header.png" class="logo">
                                    </a>
                                    </div>
                                    
                                    <div class="et-top-navigation" data-height=""90" data-fixed-height="40" style="padding-left: 298px;">

                                    <ul id="top-menu" class="nav">
                                        <li><a class="navText" href="http://www.rockymountainlaundries.com/"><i class="_mi _before dashicons dashicons-admin-home" aria-hidden="true"></i><span>Home</span></a></li>
                                        <li class="menu-item-has-children"><a class="navText" href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-location-friso/"><i class="_mi _before dashicons dashicons-location" aria-hidden="true"></i><span>Frisco, CO</span></a>
                                            <ul class="sub-menu">
                                                <li><a class="navText" href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-friso-unlimited-wash-program/"><i class="_mi _before dashicons dashicons-products" aria-hidden="true"></i><span>Unlimited Wash Program</span></a></li>
                                            </ul>
                                        </li>
                                        <li><a class="navText" href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-location-grand-lake/"><i class="_mi _before dashicons dashicons-location" aria-hidden="true"></i><span>Grand Lake, CO</span></a></li>
                                        <li><a class="navText" href="http://www.rockymountainlaundries.com/rocky-mountain-laundries-location-idaho-springs/"><i class="_mi _before dashicons dashicons-location" aria-hidden="true"></i><span>Idaho Springs, CO</span></a></li>
                                        <li><a class="navText" href="http://www.rockymountainlaundries.com/store/checkout/"><i class="_mi _before dashicons dashicons-cart" aria-hidden="true"></i><span>Checkout</span></a></li>
                                        <li><a class="navText"  href="http://www.rockymountainlaundries.com/contact-rocky-mountain-laundries/"><i class="_mi _before dashicons dashicons-email-alt" aria-hidden="true"></i><span>Contact Us</span></a></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" style='margin-top: 60px; background: #FFF; padding: 10px'>
            <div class="span10 center">
                <?= $content ?>
            </div>
        </div>

        <div class="clearfix" style="margin-top:30px;"></div>
        <br style="clear:left;" />

        <div id="footer" class="row-fluid" style='clear:both;'>
            <div class="footer">
                <div class="container clearfix">
                    <ul class="et-social-icons">

                        <li class="et-social-icon et-social-facebook">
                            <a href="#" class="icon">
                                <span>Facebook</span>
                            </a>
                        </li>
                        <li class="et-social-icon et-social-twitter">
                            <a href="#" class="icon">
                                <span>Twitter</span>
                            </a>
                        </li>
                        <li class="et-social-icon et-social-google-plus">
                            <a href="#" class="icon">
                                <span>Google</span>
                            </a>
                        </li>
                        <li class="et-social-icon et-social-rss">
                            <a href="http://www.rockymountainlaundries.com/feed/" class="icon">
                                <span>RSS</span>
                            </a>
                        </li>

                    </ul><p id="footer-info">© Copyright <span class="divibooster_year">2017</span><script>jQuery(function($){$(".divibooster_year").text(new Date().getFullYear());});</script>:&nbsp; Rocky Mountain Laundries, LLC.&nbsp; Frisco, Grand Lake &amp; Idaho Springs, Colorado.</p>					</div>	<!-- .container -->
            </div>
        </div>
    </div>
</div>
<?= $footer; ?>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var login_logout_button = document.getElementById("login_logout");
    //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
    <?php if ($facebook_user_id): ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "<?= $facebook_logout_url ?>");

    <?php elseif (empty($this->customer)): ?>
    login_logout_button.textContent = "Sign In";
    <?php else: ?>
    login_logout_button.textContent = "Logout";
    login_logout_button.setAttribute("href", "/logout");
    <?php endif; ?>
</script>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>

</body>
</html>