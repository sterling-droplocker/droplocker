<!DOCTYPE html>
<html lang="ru" dir="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>MintLocker | <?= $title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

	<link rel="stylesheet" type="text/css" href="/css/mintlocker_ru/normalize.css"/>
    <link rel='stylesheet' id='frank_stylesheet-css'  href='/css/mintlocker_ru/main.css' type='text/css' media='all' />
    <!--
	<link rel="stylesheet" type="text/css" href="/css/mintlocker_ru/fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="/css/mintlocker_ru/jquery-ui.css"/>
    -->

    <!--[if IE]>
    <link rel='stylesheet' id='frank_stylesheet_ie-css'  href='/css/mintlocker_ru/ie.css' type='text/css' media='all' />
    <![endif]-->

    <!--[if lt IE 9]>
        <script src="/js/mintlocker_ru/html5.js" type="text/javascript"></script>
        <script src="/js/mintlocker_ru/selectivizr-min.js"></script>

	<![endif]-->

    <!--[if lt IE 7]>
        <script src="/js/mintlocker_ru/ie7.js"></script>
    <![endif]-->

	<link rel="icon" type="image/x-icon" href="/images/mintlocker_ru/favicon.ico"/>
	<link rel="shortcut icon" type="image/x-icon" href="/images/mintlocker_ru/favicon.ico"/>

	<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
	<script src='/bootstrap/js/bootstrap.min.js'></script>

    <style type="text/css">
         #site-title-description {
            position: absolute !important;
            clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
            clip: rect(1px, 1px, 1px, 1px);
        }
    </style>

	<?= $_scripts ?>
	<?= $javascript?>
	<?= $style?>
	<?= $_styles ?>

</head>

<body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
	
	<div class="cyanBG"></div>

    <div id="nav-anchor" class="wrap grad">
        <nav class="big">
            <ul id="menu-menyu" class="menu">
                <li id="menu-item-223" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-223"><a href="http://mintlocker.ru/category/news/">Новости</a></li>
                <li id="menu-item-193" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item current-menu-ancestor current-menu-parent menu-item-has-children menu-item-193"><a href="/#services">Услуги</a>
                    <ul class="sub-menu">
                        <li id="menu-item-194" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-194"><a href="/#services">Химчистка</a></li>
                        <li id="menu-item-195" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-195"><a href="/#services">Стирка и глажка</a></li>
                        <li id="menu-item-196" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-196"><a href="/#services">Ремонт</a></li>
                        <li id="menu-item-266" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-266"><a href="/#services">Банк услуг</a></li>
                    </ul>
                </li>
                <li id="menu-item-161" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-161"><a href="/#howDoesItWork">Как это работает</a></li>
                <li id="menu-item-263" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-263"><a href="/#app">Мобильное приложение</a></li>
                <li id="menu-item-156" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item current-menu-ancestor current-menu-parent menu-item-has-children menu-item-156"><a href="/#about">О нас</a>
                    <ul class="sub-menu">
                        <li id="menu-item-157" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-157"><a href="/#about">О компании</a></li>
                        <li id="menu-item-158" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-158"><a href="/#about">Наши технологии</a></li>
                        <li id="menu-item-159" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-159"><a href="/#about">Пресса о нас</a></li>
                    </ul>
                </li>
                <li id="menu-item-230" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-230"><a href="http://mintlocker.ru/category/price/">Цена</a></li>
                <li id="menu-item-224" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-224"><a href="http://mintlocker.ru/category/question/">Вопросы</a></li>
                <li id="menu-item-198" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-198"><a href="/#contacts">Контакты</a></li>
            </ul>

            <div class="lang">
                <span><a class="current" href="/">RU</a></span>
                <span><a href="http://en.mintlocker.ru/">EN</a></span>
            </div>
        </nav>
    </div>
    
    <section id="header" class="wrap grad">
        <div class="topLine">
            <a class="notd" href="http://mintlocker.ru/"><img src="/images/mintlocker_ru/logo1.png" alt="MintLocker" class="ib mid logo"></a>
            <span class="ib mid social">
                <a class="notd" href="http://facebook.com" target="_blank"><span class="ib mid ic" id="fb"></span></a>
                <a class="notd" href="http://twitter.com" target="_blank"><span class="ib mid ic" id="tw"></span></a>
            </span>
            <!--span class="ib mid tagLine">
                <div>Международная сеть химчисток</div>
                <div>Более 10 лет успешной работы</div>
            </span-->
            <span class="ib mid phone">
                <div>КРУГЛОСУТОЧНО, ЕЖЕДНЕВНО</div>
                <div class="bigger">(495) 66 52-1-52</div>
            </span>
            <span class="ib mid login">
                <button class="user">
					<span class="ib mid">ВОЙТИ/Зарегистрироваться</span>
				</button>
            </span>
        </div>
    </section>

    <div role="main" id="content" class="home" style="overflow-x: hidden;">
        <div class="wrap grad">
            <div style="background: #fff; width:1000px;padding:30px 0;">
                <?= $content; ?>
            </div>
        </div>
    </div>

    <div class="wrap grad">
        <nav class="big static">
            <ul id="menu-menyu-1" class="menu">
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-223"><a href="http://mintlocker.ru/category/news/">Новости</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item current-menu-ancestor current-menu-parent menu-item-has-children menu-item-193"><a href="/#services">Услуги</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-194"><a href="/#services">Химчистка</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-195"><a href="/#services">Стирка и глажка</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-196"><a href="/#services">Ремонт</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-266"><a href="/#services">Банк услуг</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-161"><a href="/#howDoesItWork">Как это работает</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-263"><a href="/#app">Мобильное приложение</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item current-menu-ancestor current-menu-parent menu-item-has-children menu-item-156"><a href="/#about">О нас</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-157"><a href="/#about">О компании</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-158"><a href="/#about">Наши технологии</a></li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-159"><a href="/#about">Пресса о нас</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-230"><a href="http://mintlocker.ru/category/price/">Цена</a></li>
                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-224"><a href="http://mintlocker.ru/category/question/">Вопросы</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-198 nav-active"><a href="/#contacts">Контакты</a></li>
            </ul>
        </nav>
    </div>

    <section id="footer" class="wrap grad">
        <div class="bottomLine">
            <span class="ib mid logo">
                <img src="/images/mintlocker_ru/logo.png" alt="MintLocker">
            </span>
            <span class="ib mid phone">
                <div>КРУГЛОСУТОЧНО, ЕЖЕДНЕВНО</div>
                <div class="bigger">(495) 66 52-1-52</div>
                <div class="social">
                    <a class="notd" href="http://facebook.com" target="_blank"><span class="ib mid ic" id="fb"></span></a>
                    <a class="notd" href="http://twitter.com" target="_blank"><span class="ib mid ic" id="tw"></span></a>
                </div>
            </span>
            <span class="ib mid work">
                <a href="http://hh.ru" class="notd">
                    <input type="submit" value="Работать с нами">
                </a>
                <div class="castcom">
                    <a href="http://www.castcom.ru/" target="_blank"><img alt="www.castcom.ru" src="/images/mintlocker_ru/castcom.gif"></a> -
                    <a href="http://www.castcom.ru/services/web_design.html" target="_blank">веб-дизайн, изготовление сайта</a><br>
                    <a href="http://www.castcom.ru/services/seo_optimization.html" target="_blank">поисковая оптимизация сайта</a>
                </div>
            </span>
        </div>
    </section>


<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
