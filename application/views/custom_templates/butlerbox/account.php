<!DOCTYPE html>
<html class="" lang="en" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title ?> | Butlerbox</title>

<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

<script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src='/bootstrap/js/bootstrap.min.js'></script>
<script src="/css/butlerbox/sideslidemenu.min.js"></script>
<script src="/css/butlerbox/jQuery.easing.min.js"></script>
<script src="/css/butlerbox/hammer.min.js"></script>
<script src="/css/butlerbox/plugins/email-subscribers/widget/es-widget.js"></script>
<script type="text/javascript">
window.onload=function() {
	ssMenuInstance = new sideSlideMenuJS();
	ssMenuInstance.init();
}

</script>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="/css/butlerbox/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="/css/butlerbox/media-queries.css" />
<link rel="stylesheet" type="text/css" media="all" href="/css/butlerbox/slide-menu.css" />
<link href='/css/butlerbox/fonts.css?family=Roboto:400,300,400italic,700,500,500italic,700italic' rel='stylesheet' type='text/css'>
<link href='/css/butlerbox/fonts.css?family=Lato' rel='stylesheet' type='text/css'>
<link href='/css/butlerbox/fonts.css?family=Lato' rel='stylesheet' type='text/css'>
<link href="/css/butlerbox/font-awesome.min.css" rel="stylesheet" media="screen" type="text/css" />

<style type="text/css">
		#sideSlideMenu {
			background-color: #D10111;
			font-family: 'Roboto';
			font-weight: 500 ;
		}
		#sideSlideToggle {
			background-color: #D10111;
			color: #FFFFFF;
		}

		#sideSlideToggle:hover {
			color: #FFFFFF;
			background-color: #000000;
		}
		#sideSlideUl li a {
			color: #FFFFFF;
			border-bottom: 1px solid #000000;
		}
		#sideSlideUl li.ssmFCli a {
			border-top: 1px solid #000000;;
			border-bottom: 1px solid #000000;
		}

		#sideSlideUl li a:hover {
			color: #FFFFFF;
			background-color: #000000;
		}

		.ssmenuSubmenuToggle, #sideSlideFooter a {
			color: #FFFFFF;
		}
		.ssmenuSubmenuToggle:hover, #sideSlideFooter a:hover, #sideSlideFooter {
			color: #FFFFFF;
		}


</style>
<?= $_scripts ?>
<?= $javascript?>
<?= $style?>
<?= $_styles ?>

<style type="text/css">
	#kiosk_cardNumber {
        text-indent: 10em;
   }
</style>
</head>

<body>

<div id="header" class="group">
   <div class="header group">
        <div class="logo">
            <a href="http://butlerbox.us"><img src="/css/butlerbox/images/logo.png" /></a>
        </div>
<div id="sideSlideToggle" class="sideSlideSlideToggleNegative"><i class="fa fa-bars"></i></div>

        <div class="top-right group">
             <div class="shadule">
                    <a href="javascript:void" class="sh">SCHEDULE A PICK UP</a>
                <div class="foru-7">
                    <a href="javascript:void"><img src="/css/butlerbox/images/fb.png" /></a>
                    <a href="javascript:void"><img src="/css/butlerbox/images/tw.png" /></a>
                    <a href="javascript:void"><img src="/css/butlerbox/images/in.png" /></a>

                    <div class="phn">123-456-7890</div>

                </div>
             </div>
        </div>
        <div class="nav">
            <div class="menu-main-container">
                <ul id="menu-main" class="menu">
                    <li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-16"><a href="http://butlerbox.us/">Home</a></li>
                    <li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="http://butlerbox.us/about-us/">ABOUT US</a></li>
                    <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://butlerbox.us/services/">SERVICES</a></li>
                    <li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://butlerbox.us/how-it-works/">HOW IT WORKS</a></li>
                    <li id="menu-item-144" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144"><a href="http://butlerbox.us/property-managers/">PROPERTY MANAGERS</a></li>
                    <li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://butlerbox.us/faq/">FAQ</a></li>
                    <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="http://butlerbox.us/locations/">LOCATIONS</a></li>
                    <li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="http://butlerbox.us/contact-us/">CONTACT US</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section role="main" id="content">
    <?= $content; ?>
</section>


<div id="footer" class="group">
     <div id="footer-lg">
          <div class="footer group">
               <div class="quick">
                    <h1>QUICK LINKS</h1>
                    <div class="menu-main-container">
                        <ul id="menu-main-1" class="menu">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-16"><a href="http://butlerbox.us/">Home</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17"><a href="http://butlerbox.us/about-us/">ABOUT US</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://butlerbox.us/services/">SERVICES</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://butlerbox.us/how-it-works/">HOW IT WORKS</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144"><a href="http://butlerbox.us/property-managers/">PROPERTY MANAGERS</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://butlerbox.us/faq/">FAQ</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="http://butlerbox.us/locations/">LOCATIONS</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="http://butlerbox.us/contact-us/">CONTACT US</a></li>
                        </ul>
                    </div>
               </div>

               <div class="contact-details">
                    <h1>CONTACT DETAILS</h1>
                    <!--<span class="phone">123-456-7890</span>-->
                    <span class="location">BUTLERBOX</span>
                    <span class="email"><a href="mailto:orders@butlerbox.us">orders@butlerbox.us</a></span>
               </div>

               <div class="subscr">
                    <h1>SUBSCRIBE FOR UPDATES</h1>
            		<div>
            			<div class="es_caption">Get latest updates by entering  your email address</div>
            			<div class="es_msg"><span id="es_msg"></span></div>
            			<div class="es_lablebox">Email *</div>
            			<div class="es_textbox">
            				<input class="es_textbox_class" name="es_txt_email" id="es_txt_email" onkeypress="if(event.keyCode==13) es_submit_page('http://butlerbox.us')" value="" maxlength="225" type="text" placeholder="Email Address">
            			</div>
            			<div class="es_button">
            				<input class="es_textbox_button" name="es_txt_button" id="es_txt_button" onClick="return es_submit_page('http://butlerbox.us')" value="Subscribe" type="button">
            			</div>
            			<input name="es_txt_name" id="es_txt_name" value="" type="hidden">
            			<input name="es_txt_group" id="es_txt_group" value="" type="hidden">
            		</div>
                </div>

               <div class="follow">
                    <h1>FOLLOW US</h1>
                    <a href="javascript:void" class="fb">FACEBOOK</a>
                    <a href="javascript:void" class="tw">TWITTER</a>
                    <a href="javascript:void" class="inn">LINKEDIN</a>
               </div>

          </div>
    </div>
</div>

<div id="sideSlideMenu" class="sideSlideSlideNegative">


	<ul id="sideSlideUl">


	    <li class="ssmFCli" >
	        <a href="http://butlerbox.us/" class="title">
	            <span><i class="fa fa "></i>Home</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/about-us/" class="title">
	            <span><i class="fa fa "></i>ABOUT US</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/services/" class="title">
	            <span><i class="fa fa "></i>SERVICES</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/how-it-works/" class="title">
	            <span><i class="fa fa "></i>HOW IT WORKS</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/property-managers/" class="title">
	            <span><i class="fa fa "></i>PROPERTY MANAGERS</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/faq/" class="title">
	            <span><i class="fa fa "></i>FAQ</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/locations/" class="title">
	            <span><i class="fa fa "></i>LOCATIONS</span>
	        </a>


	    	    </li>


	    <li class="" >
	        <a href="http://butlerbox.us/contact-us/" class="title">
	            <span><i class="fa fa "></i>CONTACT US</span>
	        </a>



			</ul>
	<div id="sideSlideFooter">
			</div>
</div>
<div id="sideSlideToggle" class="sideSlideSlideToggleNegative"><i class="fa fa-bars"></i></div>


<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>

</body>
</html>
