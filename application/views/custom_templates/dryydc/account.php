<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width">
      <title>My Account | Dryy</title>
      <meta name="description" content="Proudly providing quality, green garment care across the DC metro area. Drop off or schedule a pickup today.">
      <link rel="stylesheet" href="/css/dryydc/style.css?id=09d56e2ebdd515493e39">
      <link rel="apple-touch-icon" sizes="180x180" href="/images/dryydc/favicons/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/images/dryydc/favicons/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/images/dryydc/favicons/favicon-16x16.png">
      <link rel="manifest" href="/images/dryydc/favicons/site.webmanifest">
      <link rel="mask-icon" href="/images/dryydc/favicons/safari-pinned-tab.svg" color="#a8dcdd">
      <link rel="shortcut icon" href="/images/dryydc/favicons/favicon.ico">
      <meta name="msapplication-TileColor" content="#a8dcdd">
      <meta name="msapplication-config" content="/images/dryydc/favicons/browserconfig.xml">
      <meta name="theme-color" content="#ffffff">
      <meta name="apple-itunes-app" content="app-id=568903335">
      <meta name="google-maps-api-key" content="AIzaSyBQc8kPbbSOa4M6nQDwIBKgBwuXbbGdSrE">

      
        <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
        <script src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"></script>

        <?php //compatibility layer.. This allow to use boostrap 3+ with boostrap 2x ?>
        <link rel='stylesheet' id='custom-style-css'  href='/css/dryydc/bootstrap2x.css' type='text/css' media='all' />

        <script src='/bootstrap/js/bootstrap.min.js'></script>        
        <script type="text/javascript">
        var $ = jQuery.noConflict();
        </script>

        <?= $_scripts ?>
        <?= $javascript ?>
        <?= $style ?>
        <?= $_styles ?>

        <link rel='stylesheet' id='custom-style-css-2'  href='/css/dryydc/custom.css?<?=time()?>' type='text/css' media='all' />

   </head>
   <body class="home tint-teal">
      <div id="app">
         <header class="header">
            <div class="container">
               <a href="/" class="logo">
                  <span class="screen-reader-text">Dryy</span>
                  <svg class="logo-svg" xmlns="http://www.w3.org/2000/svg" width="113" height="40" viewBox="0 0 112.8 40">
                     <path class="st0" d="M9.4 29h-.1c-.6 0-1.1-.5-1.1-1.1V10c0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1v16.8c1.7 0 4.2 0 5-.1 3.1-.4 5.6-1.7 7.5-4.1 1.5-1.9 2.3-4.2 2.5-7.3.4-5.2-2.8-10.3-7.4-11.8-1.7-.6-5.1-1-8.4-1H2.1v24.2h2.2c.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1H1.1c-.3 0-.5-.1-.7-.3-.3-.2-.4-.4-.4-.7V1.6C0 1 .5.5 1.1.5h8.6c3.5 0 7.1.4 9 1.1 5.5 1.8 9.4 7.8 8.9 13.9-.3 3.5-1.2 6.2-3 8.4-2.2 2.8-5.2 4.4-8.9 4.9-1.4.2-5.5.2-6.3.2zM93.5 39.5c-.2 0-.4 0-.5-.1-.5-.3-.7-.9-.4-1.4l5.1-9h-2.2c-.4 0-.8-.2-1-.6L86.4 9.7c-.1-.3-.1-.7.1-1 .2-.3.5-.5.9-.5h8.5c.4 0 .8.2 1 .6l4.7 10.6 5.5-9.2h-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1h4.2c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-7.5 12.5c-.2.3-.6.5-1 .5s-.7-.3-.9-.6l-5.2-11.9H89l7.2 16.5h3.3c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-6 10.5c-.1.6-.5.8-.9.8zM65.5 39.5c-.2 0-.4 0-.5-.1-.5-.3-.7-.9-.4-1.4l5.1-9h-2.2c-.4 0-.8-.2-1-.6L58.3 9.7c-.1-.3-.1-.7.1-1 .2-.3.5-.5.9-.5h8.5c.4 0 .8.2 1 .6l4.7 10.6 5.5-9.2h-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1h4.2c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-7.5 12.5c-.2.3-.6.5-1 .5s-.7-.3-.9-.6l-5.2-11.9H61l7.2 16.5h3.3c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-6 10.5c-.2.6-.6.8-.9.8zM42.8 29h-8c-.3 0-.5-.1-.7-.3-.2-.2-.3-.5-.3-.7V9.4c0-.6.5-1.1 1.1-1.1H38c.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1h-2.2V27h5.9V12.6c0-3.2 2.6-5.8 5.9-5.8 3.2 0 5.8 2.6 5.8 5.8s-2.6 5.8-5.8 5.8c-.6 0-1.1-.5-1.1-1.1s.5-1.1 1.1-1.1c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7c-2.1 0-3.8 1.7-3.8 3.7v15.3c0 .3-.1.5-.3.7-.1.3-.4.5-.7.5z"/>
                  </svg>
               </a>
               <button class="hamburger nav-toggle" type="button" aria-label="Navigation" aria-controls="header-nav" @click="toggleNav()" :class="{'is-active': navOpen}">
               <span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
               <nav class="nav" id="header-nav">
                  <div class="nav-inner">
                     <a href="https://dryydc.com/services" class="">Services</a>
                     <a href="https://dryydc.com/about" class="">About</a>
                     <a href="https://dryydc.com/locations" class="">Locations</a>
                     <a href="https://dryydc.com/valet" class="">Valet</a>
                     <a href="https://dryydc.com/app">Dryy Drop Lockers</a>
                     <a href="https://dryydc.com/login" class="nav-button has-arrow ">
                        Log In 
                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="6" viewBox="0 0 22 6">
                           <path d="M22 3L14.9.1l1.4 2.5H.1v.8h16.2l-1.4 2.5z"/>
                        </svg>
                     </a>
                  </div>
               </nav>
            </div>
         </header>
         <main id="main" role="main">
            <div id="bootstrap2x">
                <br/>
                <?php echo $content; ?>
                <br/>
            </div>
         </main>
         <div class="footer">
            <div class="container">
               <a href="/" class="logo">
                  <span class="screen-reader-text">Dryy</span>
                  <svg class="logo-svg" xmlns="http://www.w3.org/2000/svg" width="113" height="40" viewBox="0 0 112.8 40">
                     <path class="st0" d="M9.4 29h-.1c-.6 0-1.1-.5-1.1-1.1V10c0-.6.5-1.1 1.1-1.1.6 0 1.1.5 1.1 1.1v16.8c1.7 0 4.2 0 5-.1 3.1-.4 5.6-1.7 7.5-4.1 1.5-1.9 2.3-4.2 2.5-7.3.4-5.2-2.8-10.3-7.4-11.8-1.7-.6-5.1-1-8.4-1H2.1v24.2h2.2c.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1H1.1c-.3 0-.5-.1-.7-.3-.3-.2-.4-.4-.4-.7V1.6C0 1 .5.5 1.1.5h8.6c3.5 0 7.1.4 9 1.1 5.5 1.8 9.4 7.8 8.9 13.9-.3 3.5-1.2 6.2-3 8.4-2.2 2.8-5.2 4.4-8.9 4.9-1.4.2-5.5.2-6.3.2zM93.5 39.5c-.2 0-.4 0-.5-.1-.5-.3-.7-.9-.4-1.4l5.1-9h-2.2c-.4 0-.8-.2-1-.6L86.4 9.7c-.1-.3-.1-.7.1-1 .2-.3.5-.5.9-.5h8.5c.4 0 .8.2 1 .6l4.7 10.6 5.5-9.2h-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1h4.2c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-7.5 12.5c-.2.3-.6.5-1 .5s-.7-.3-.9-.6l-5.2-11.9H89l7.2 16.5h3.3c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-6 10.5c-.1.6-.5.8-.9.8zM65.5 39.5c-.2 0-.4 0-.5-.1-.5-.3-.7-.9-.4-1.4l5.1-9h-2.2c-.4 0-.8-.2-1-.6L58.3 9.7c-.1-.3-.1-.7.1-1 .2-.3.5-.5.9-.5h8.5c.4 0 .8.2 1 .6l4.7 10.6 5.5-9.2h-2.3c-.6 0-1.1-.5-1.1-1.1 0-.6.5-1.1 1.1-1.1h4.2c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-7.5 12.5c-.2.3-.6.5-1 .5s-.7-.3-.9-.6l-5.2-11.9H61l7.2 16.5h3.3c.4 0 .7.2.9.5.2.3.2.7 0 1.1l-6 10.5c-.2.6-.6.8-.9.8zM42.8 29h-8c-.3 0-.5-.1-.7-.3-.2-.2-.3-.5-.3-.7V9.4c0-.6.5-1.1 1.1-1.1H38c.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1h-2.2V27h5.9V12.6c0-3.2 2.6-5.8 5.9-5.8 3.2 0 5.8 2.6 5.8 5.8s-2.6 5.8-5.8 5.8c-.6 0-1.1-.5-1.1-1.1s.5-1.1 1.1-1.1c2 0 3.7-1.7 3.7-3.7s-1.7-3.7-3.7-3.7c-2.1 0-3.8 1.7-3.8 3.7v15.3c0 .3-.1.5-.3.7-.1.3-.4.5-.7.5z"/>
                  </svg>
               </a>
               <div class="footer-inner">
                  <div class="footer-left">
                     <nav class="footer-left-nav footer-left-primary">
                        <a href="https://dryydc.com/services"><span>Services</span></a>
                        <a href="https://dryydc.com/about"><span>About</span></a>
                        <a href="https://dryydc.com/locations"><span>Locations</span></a>
                        <a href="https://dryydc.com/app"><span>Dryy Drop Lockers</span></a>
                        <a href="https://dryydc.com/valet"><span>Valet</span></a>
                        <a href="https://dryydc.com/help"><span>Help &amp; FAQs</span></a>
                     </nav>
                     <nav class="footer-left-nav footer-left-secondary">
                        <a href="https://dryydc.com/about"><span>Careers</span></a>
                        <a href="https://dryydc.com/about"><span>Corporate Partnerships</span></a>
                        <a href="https://dryydc.com/privacy"><span>Privacy Policy</span></a>
                        <a href="https://dryydc.com/terms"><span>Terms and Conditions</span></a>
                     </nav>
                  </div>
                  <div class="footer-right">
                     <nav class="footer-right-social">
                        <a href="https://www.instagram.com/dryydc/" target="_blank" rel="noreferrer nofollow">
                           <span class="screen-reader-text">Instagram</span>
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                              <path d="M10 1.9c2.6 0 2.9 0 4 .1 1 0 1.5.2 1.8.3.5.2.8.4 1.1.7.3.3.6.7.7 1.1.2.5.4 1 .4 1.9 0 1 .1 1.3.1 4s0 2.9-.1 4c0 1-.2 1.5-.3 1.8-.2.5-.4.8-.7 1.1-.3.3-.7.6-1.1.7-.5.2-1 .4-1.9.4-1 0-1.3.1-4 .1S7.1 18 6 18c-1 0-1.5-.2-1.8-.3-.5-.2-.8-.4-1.1-.7-.3-.3-.6-.7-.7-1.1-.2-.5-.4-1-.4-1.9 0-1-.1-1.3-.1-4S2 7.1 2 6c0-1 .2-1.5.3-1.8.2-.5.4-.8.7-1.1.3-.3.7-.6 1.1-.7.5-.2 1-.4 1.9-.4 1.1 0 1.4-.1 4-.1m0-1.7c-2.7 0-3 0-4.1.1-1 0-1.8.2-2.4.5-.6.1-1.1.5-1.7 1-.5.6-.9 1.1-1.1 1.8-.3.6-.4 1.3-.5 2.3 0 1-.1 1.4-.1 4.1 0 2.7 0 3 .1 4.1 0 1 .2 1.8.5 2.4.3.6.6 1.2 1.1 1.7.5.5 1.1.9 1.7 1.1.6.2 1.3.4 2.4.5 1 0 1.4.1 4.1.1s3 0 4.1-.1c1 0 1.8-.2 2.4-.5.6-.3 1.2-.6 1.7-1.1.5-.5.9-1.1 1.1-1.7.2-.6.4-1.3.5-2.4 0-1 .1-1.4.1-4.1s0-3-.1-4.1c0-1-.2-1.8-.5-2.4-.3-.6-.6-1.2-1.1-1.7-.5-.5-1.1-.9-1.7-1.1-.6-.2-1.3-.4-2.4-.5H10z"></path>
                              <path d="M10 5c-2.8 0-5 2.3-5 5s2.3 5 5 5 5-2.3 5-5-2.2-5-5-5zm0 8.3c-1.8 0-3.3-1.5-3.3-3.3 0-1.8 1.5-3.3 3.3-3.3 1.8 0 3.3 1.5 3.3 3.3 0 1.8-1.5 3.3-3.3 3.3z"></path>
                              <circle cx="15.2" cy="4.8" r="1.2"></circle>
                           </svg>
                        </a>
                        <a href="https://www.facebook.com/Dryy-Garment-Care-805152479837007/" target="_blank" rel="noreferrer nofollow">
                           <span class="screen-reader-text">Facebook</span>
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                              <path d="M18.3.7H1.7c-.6 0-1 .5-1 1v16.5c0 .6.5 1 1 1h8.9V12H8.2V9.3h2.4V7.2c0-2.4 1.5-3.7 3.6-3.7 1 0 1.9.1 2.2.1v2.5h-1.5c-1.2 0-1.4.6-1.4 1.4v1.8h2.8l-.4 2.8h-2.4v7.2h4.7c.6 0 1-.5 1-1V1.7c.1-.5-.4-1-.9-1z"></path>
                           </svg>
                        </a>
                        <a href="https://www.youtube.com/channel/UCB8ufof0O6mFDnsqEfjugxg" target="_blank" rel="noreferrer nofollow">
                           <span class="screen-reader-text">YouTube</span>
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="28px" height="20px" viewBox="0 0 28 20">
                              <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->
                              <title>social-youtube</title>
                              <desc>Created with Sketch.</desc>
                              <defs></defs>
                              <g id="aeigyazmpm-social-youtube" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                 <path d="M26.9818182,3.48064516 C26.6700477,2.30746515 25.7573757,1.38892768 24.586217,1.06964809 C22.4677419,0.5 14,0.5 14,0.5 C14,0.5 5.53225806,0.5 3.41378299,1.06964809 C2.24262428,1.38892768 1.32995231,2.30746515 1.01818182,3.48064516 C0.451612903,5.60835777 0.451612903,10.0454545 0.451612903,10.0454545 C0.451612903,10.0454545 0.451612903,14.4825513 1.01818182,16.6102639 C1.32995231,17.7834439 2.24262428,18.7019814 3.41378299,19.021261 C5.53225806,19.5909091 14,19.5909091 14,19.5909091 C14,19.5909091 22.4677419,19.5909091 24.586217,19.021261 C25.7573757,18.7019814 26.6700477,17.7834439 26.9818182,16.6102639 C27.5483871,14.4825513 27.5483871,10.0454545 27.5483871,10.0454545 C27.5483871,10.0454545 27.5483871,5.60835777 26.9818182,3.48064516 Z M11.228739,14.0745601 L11.228739,6.01634897 L18.3108504,10.0454545 L11.228739,14.0745601 Z" id="aeigyazmpm-Shape" fill="#000000" fill-rule="nonzero"></path>
                              </g>
                           </svg>
                        </a>
                        <a href="https://www.linkedin.com/company/dryy-garment-care/about/" target="_blank" rel="noreferrer nofollow">
                           <span class="screen-reader-text">LinkedIn</span>
                           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                              <path d="M17.9.7H2.1C1.3.7.7 1.3.7 2v15.9c0 .7.6 1.3 1.4 1.3h15.8c.8 0 1.4-.6 1.4-1.3V2.1c0-.8-.6-1.4-1.4-1.4zM6.2 16.5H3.5V7.7h2.8v8.8zm-1.4-10c-.8 0-1.6-.7-1.6-1.6 0-.9.8-1.6 1.6-1.6.9 0 1.6.7 1.6 1.6 0 .9-.7 1.6-1.6 1.6zm11.7 10h-2.8v-4.3c0-1 0-2.3-1.4-2.3s-1.7 1.1-1.7 2.3v4.4H8V7.7h2.6v1.2c.4-.7 1.3-1.4 2.6-1.4 2.8 0 3.3 1.8 3.3 4.2v4.8z"></path>
                           </svg>
                        </a>
                     </nav>
                     <p class="footer-right-contact">
                        <a href="mailto:support@dryydc.com">support@dryydc.com</a><br>
                        <a href="tel:202.599.9090">202.599.9090</a>
                     </p>
                  </div>
               </div>
               <p class="footer-copyright">&copy; 2019 Dryy All Rights Reserved</p>
            </div>
         </div>
         <div class="nav-overlay" @click="closeNav()" @touchstart="closeNav()"></div>
      </div>
      <script async src="/js/dryydc/scripts.js?id=02e0a22abdd430b3c749"></script>
      <script src="/js/dryydc/sync.js?id=d69e8a69959dba2bb0e2"></script>                                                                                                                                         
      <script type="application/ld+json">
         {"@context":{"@vocab":"http:\/\/schema.org"},"@graph":[{"@id":"https:\/\/dryydc.com","@type":"Organization","name":"Dryy","url":"https:\/\/dryydc.com","sameAs":["https:\/\/www.instagram.com\/dryydc\/","https:\/\/www.facebook.com\/Dryy-Garment-Care-805152479837007\/","https:\/\/www.linkedin.com\/company\/dryy-garment-care\/about\/"],"contactPoint":{"@type":"ContactPoint","telephone":"+1-202.599.9090","email":"support@dryydc.com","contactType":"Customer service"}},{"@type":"DryCleaningOrLaundry","parentOrganization":{"name":"Dryy"},"name":"Union Market","address":{"@type":"PostalAddress","streetAddress":"381 Morse Street NE","addressLocality":"Washington","addressRegion":"DC","postalCode":"20002","telephone":"555.555.5555"},"image":"\/images\/dryydc\/logo.svg","priceRange":"$"},{"@type":"DryCleaningOrLaundry","parentOrganization":{"name":"Dryy"},"name":"Navy Yard","address":{"@type":"PostalAddress","streetAddress":"55 M Street SE","addressLocality":"Washington","addressRegion":"DC","postalCode":"20003","telephone":"777.777.7777"},"image":"\/images\/dryydc\/logo.svg","priceRange":"$"},{"@type":"DryCleaningOrLaundry","parentOrganization":{"name":"Dryy"},"name":"Bethesda","address":{"@type":"PostalAddress","streetAddress":"7900 Wisconsin Ave","addressLocality":"Bethesda","addressRegion":"MD","postalCode":"20814","telephone":"999.999.9999"},"image":"\/images\/dryydc\/logo.svg","priceRange":"$"}]}
      </script>
   </body>
</html>