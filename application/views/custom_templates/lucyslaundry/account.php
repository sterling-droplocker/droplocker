<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
   <head>
      <title>Lucy&#39;s Laundry | Augusta&#39;s Premier Laundromat &amp; Laundry Services | Services</title>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width" />
      <meta name="apple-mobile-web-app-title" content="Lucy's Laundry">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black">

      <link rel="stylesheet" type="text/css" href="/css/lucyslaundry/stylesheet.css" media="screen" />
      <link rel="stylesheet" type="text/css" href="/css/lucyslaundry/stylesheet.css" />
      <link rel="apple-touch-icon-precomposed" href="/images/lucyslaundry/lucys-apple-icon.png" >
      <link rel="apple-touch-startup-image" href="/images/lucyslaundry/lucys-startup-image.png">
      <link rel="shortcut icon" href="/images/lucyslaundry/lucys-icon.ico">
      <link rel="stylesheet" href="/css/lucyslaundry/foundation.css" />

      <link rel='stylesheet' id='custom-style-css'  href='/css/lucyslaundry/bootstrap2x.css' type='text/css' media='all' />

      <script src="/js/lucyslaundry/jquery.js"></script>
      <script src='/bootstrap/js/bootstrap.min.js'></script>
      <script type="text/javascript">
          var $ = jQuery.noConflict();
      </script>
      <?= $_scripts ?>
      <?= $javascript ?>
      <?= $style ?>
      <?= $_styles ?>
   </head>
   <body>
      <div id="nav-wrapper" class="fixed">
         <div id="lucy-logo" class="hide-for-small"><a href="#nav-wrapper"><img src="/images/lucyslaundry/lllogo-coin.png" width="116" height="110" alt="lucy laudromat augusta georgia"><img src="/images/lucyslaundry/lllogo-text.png" width="274" height="110" alt="Lucy's Laundry Augusta GA"></a></div>
         <div id="lucy-logo-small" class="show-for-small"><a href="#nav-wrapper"><img src="/images/lucyslaundry/lllogo-coin-small.png" width="95" height="110" alt="lucy laudromat augusta georgia"><img src="/images/lucyslaundry/lllogo-text-small.png" width="138" height="110" alt="Lucy's Laundry Augusta GA"></a></div>
         <div id="translate-bar"></div>
         <div id="menu-area">
            <nav class="top-bar">
               <ul class="title-area">
                  <li class="name">
                     <h1><a href="#">&nbsp;</a></h1>
                  </li>
                  <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
               </ul>
               <section class="top-bar-section">
                  <ul class="right">
                     <li><a href="http://lucyslaundry.com/"> Home </a>
                     </li>
                     <li><a href="http://lucyslaundry.com/index.php?page=services" class="active"> Services </a>
                     </li>
                     <li><a href="http://lucyslaundry.com/index.php?page=photos"> Photos </a>
                     </li>
                     <li><a href="http://lucyslaundry.com/index.php?page=contact"> Contact </a>
                     </li>
                  </ul>
               </section>
            </nav>
         </div>
      </div>
      <p style="height:55px;">&nbsp;</p>
      <div class="row">
         <div class="large-12 columns">
            &nbsp;
         </div>
      </div>
      <div class="row">
            <div class="large-12 columns">
                <div id="bootstrap2x"><?=$content?></div>
            </div>
      </div>
      <div class="row">
      </div>
      <footer class="row">
         <div class="large-12 columns">
            <hr />
            <div class="row">
               <div class="large-12 columns">
                  <p>© Copyright 2004-<?=date("Y")?> - Lucy's Laundry - Augusta, Georgia - All Rights Reserved</p>
               </div>
            </div>
         </div>
      </footer>
      <script src="/js/lucyslaundry/zepto.js"></script>
      <script src="/js/lucyslaundry/foundation.min.js"></script>
      <script>
         $(document).foundation();
      </script>
   </body>
</html>
