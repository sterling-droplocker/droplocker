<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
</head>
<body>

                <?php echo $content; ?>

</body>
</html>