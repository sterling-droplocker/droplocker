<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My account | One Red Locker</title>
    <link rel="shortcut icon" href="/images/oneredlocker/favicon.png">


    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

    <link rel="stylesheet" href="/css/oneredlocker/bootstrapwp.css">
 	<link rel="stylesheet" href="/css/oneredlocker/style.css">

    <script src="/js/jquery-1.7.2.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

    <link href='//fonts.googleapis.com/css?family=Quantico:400,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Cutive+Mono' rel='stylesheet' type='text/css'>
  
          <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>  
</head>
<body class="">

<div class="site_navbar site_navbar-inverse site_navbar-top " id="site_navbar-top">
        <div class="container">
		        <div class="site_navbar-header">
						<button type="button" class="site_navbar-toggle collapsed" data-toggle="collapse" data-target="#site_navbar" aria-expanded="false" aria-controls="navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="http://oneredlocker.com">
	                	<img src="/images/oneredlocker/header-lockup.svg" class="header-logo">
					</a>
		        </div>
	        
                <div id="site_navbar" class="site_navbar-collapse collapse" aria-expanded="false">
	                <ul id="menu-menu-1" class="site_nav site_navbar-nav">
		                <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://oneredlocker.com/about">About</a></li>
						<li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-37"><a href="http://oneredlocker.com/faq">FAQ</a></li>
						<li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36"><a href="http://oneredlocker.com/building-owners-managers">Owners &amp; Managers</a></li>
						<li id="menu-item-45" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="http://oneredlocker.com/contact">Contact</a></li>
						<li id="menu-item-38" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-38"><a href="https://myaccount.oneredlocker.com/login">Signup / Login</a></li>
					</ul>
				</div>           
 </div>
        
    </div>
    <div class="account_content">
    	<?php echo $content; ?>
    </div>

<footer>
	        
            <div class="get-orl">
	            <div class="container">
		            <div class="span12">
					    <h1>GET ONE RED LOCKER</h1>
		            </div>
		            <div class="span7">
					    <p>If you’d like us in your building, let us know below.<br>
						    We’ll treat you to $100.00 worth of laundry in appreciation!
					    </p>
					    <a href="mailto:support@oneredlocker.com?subject=LOVE%20TO%20SEE%20YOU%20IN%20OUR%20BUILDING!" class="button">GET O.R.L</a>
		            </div>
		            
		            <div class="span2 footer-links">
			            <h2><a href="http://oneredlocker.com/about/">About</a></h2>
			            <h2><a href="http://oneredlocker.com/faq/">FAQ</a></h2>
			            <h2><a href="http://oneredlocker.com/services/">Services</a></h2>
			            <h2><a href="http://oneredlocker.com/about#howitworks">How It Works</a></h2>
			        </div>
			        
			        <div class="span2 footer-links">
			            <h2><a href="http://oneredlocker.com/building-owners-managers/">Owners &amp; Managers</a></h2>
			            <h2><a href="http://oneredlocker.com/contact/">Contact</a></h2>
			            <h2><a href="https://myaccount.oneredlocker.com/login">Sign Up / Login</a></h2>
			        </div>
			        
			        <div class="span1 social" style="width:4%">
			            <img src="/images/oneredlocker/fb.svg">
			            <img src="/images/oneredlocker/twitter.svg">
			            <img src="/images/oneredlocker/instagram.svg">
			        </div>

		            
	            </div>
		    </div>
            
            
            <div class="sub-footer">
	            <div class="container">
		            <div class="row">
			            <div class="span6">
			                <p>© One Red Locker Inc 2016<br>
							All Rights Reserved 
			                </p>
		                </div>
		                
		                <div class="span6">
			                <div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">var ANS_customer_id="f1faa469-f747-4517-b873-80799c497585";</script> <script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js"></script><style type="text/css">
div.AuthorizeNetSeal{text-align:center;margin:0;padding:0;width:90px;font:normal 9px arial,helvetica,san-serif;line-height:10px;}
div.AuthorizeNetSeal a{text-decoration:none;color:black;}
div.AuthorizeNetSeal a:visited{color:black;}
div.AuthorizeNetSeal a:active{color:black;}
div.AuthorizeNetSeal a:hover{text-decoration:underline;color:black;}
div.AuthorizeNetSeal a img{border:0px;margin:0px;text-decoration:none;}
</style>
</div>
		                </div>
		               
	                </div>
	                
	            </div>
            </div>
            
        </footer>
    </body>
</html>