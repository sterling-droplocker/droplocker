<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Location" content="home.html">


    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fredericka+the+Great|Allura|Amatic+SC|Arizonia|Averia+Sans+Libre|Cabin+Sketch|Francois+One|Jacques+Francois+Shadow|Josefin+Slab|Kaushan+Script|Love+Ya+Like+A+Sister|Merriweather|Offside|Open+Sans|Open+Sans+Condensed|Oswald|Over+the+Rainbow|Pacifico|Romanesco|Sacramento|Seaweed+Script|Special+Elite">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato|Average">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">

    <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
    <script src='/bootstrap/js/bootstrap.min.js'></script>



    <?= $_scripts ?>
    <?= $javascript?>
    <?= $style?>
    <?= $_styles ?>




    <!-- wp_header -->

    <link rel='stylesheet' id='contact-form-7-css'  href='/css/imhuckleberry/style.css?ver=4.1' type='text/css' media='all' />
    <link rel='stylesheet' id='mappress-css'  href='/css/imhuckleberry/mappress.css?ver=2.40.7' type='text/css' media='all' />
    <link rel='stylesheet' id='themify-framework-css'  href='/css/imhuckleberry/themify.framework.css?ver=1.9.6' type='text/css' media='all' />
    <link rel='stylesheet' id='themify-builder-style-css'  href='/css/imhuckleberry/themify-builder-style.css?ver=1.9.6' type='text/css' media='all' />
    <link rel='stylesheet' id='themify-animate-css'  href='/css/imhuckleberry/animate.min.css?ver=1.9.6' type='text/css' media='all' />
    <link rel='stylesheet' id='easy_table_style-css'  href='/css/imhuckleberry/styleDefault.css?ver=1.5.2' type='text/css' media='all' />
    <link rel='stylesheet' id='theme-style-css'  href='/css/imhuckleberry/styleH.css?ver=4.2.4' type='text/css' media='all' />
    <link rel='stylesheet' id='themify-media-queries-css'  href='/css/imhuckleberry/media-queries.css?ver=4.2.4' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-css'  href='/css/imhuckleberry/fonts.css?family=Josefin+Sans&#038;subset=latin%2Clatin-ext&#038;ver=4.2.4' type='text/css' media='all' />
    <link rel='stylesheet' id='magnific-css'  href='/css/imhuckleberry/lightbox.css?ver=1.9.6' type='text/css' media='all' />
    <link rel='stylesheet' id='themify-icon-font-css'  href='/css/imhuckleberry/font-awesome.min.css?ver=1.9.6' type='text/css' media='all' />
    <script type='text/javascript' src='/css/imhuckleberry/jquery.min.js?ver=1.11.2'></script>
    <script type='text/javascript' src='/css/imhuckleberry/jquery-migrate.min.js?ver=1.2.1'></script>
    <script type='text/javascript' src='/css/imhuckleberry/themify.scroll-highlight.js?ver=4.2.4'></script>


    	<!-- media-queries.js -->
    	<!--[if lt IE 9]>
    		<script src="/js/imhuckleberry/respond.js"></script>
    	<![endif]-->

    	<!-- html5.js -->
    	<!--[if lt IE 9]>
    		<script src="/js/imhuckleberry/html5.js"></script>
    	<![endif]-->


    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">


    	<!--[if lt IE 9]>
    	<script src="/js/imhuckleberry/nwmatcher-1.2.5-min.js"></script>
    	<script type="text/javascript" src="/js/imhuckleberry/selectivizr-min.js"></script>
    	<![endif]-->

    	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    			<!--Themify Styling-->
    			<style type="text/css">
    body {

    	font-family:Roboto;
    	color: #00000;
    }

    a {

    	color: #000000;
    }

    a:hover {

    	color: #de2cc1;
    	color: rgba(222,44,193,1);
    }

    h1 {

    	font-family:Roboto;	font-weight:bold;

    }

    #site-logo img {
     	width: 250px;
    }

    #site-logo a {
     	width: 250px;
    }

    #site-description {
     	display: none;
    }

    #main-nav {
     		position:static;

    	border-bottom-color: #fff769;
    	border-bottom-color: rgba(255,247,105,1);
    }

    #main-nav a {

    	font-family:Roboto;	font-size:14px;

    	color: #a3238e;
    	color: rgba(163,35,142,1);
    	background-color: transparent;
    }

    #main-nav a:hover {

    	background-color: #00aeef;
    	background-color: rgba(0,174,239,1);
    	color: #ffffff;
    	color: rgba(255,255,255,1);
    }

    #main-nav .current_page_item a,  #main-nav .current-menu-item a {

    	background-color: #a3238e;
    	background-color: rgba(163,35,142,1);
    	color: #ffffff;
    	color: rgba(255,255,255,1);
    }

    #main-nav ul {

    	background-color: #ffffff;
    	background-color: rgba(255,255,255,1.00);
    	border-style: solid;
    	border-width: 1px;
    	border-color: #f2f2f2;
    	border-color: rgba(242,242,242,1.00);
    	padding-right: 0px;
    	padding-left: 0px;
    	margin-top: 0px;
    }

    #main-nav ul a, #main-nav .current_page_item ul a, #main-nav ul .current_page_item a, #main-nav .current-menu-item ul a, #main-nav ul .current-menu-item a {
     		font-size:.7em;
    	line-height:1em;
    	text-transform:capitalize;	text-align:center;
    	color: #a3238e;
    	color: rgba(163,35,142,1);
    }

    #main-nav ul a:hover,  #main-nav .current_page_item ul a:hover,  #main-nav ul .current_page_item a:hover,  #main-nav .current-menu-item ul a:hover,  #main-nav ul .current-menu-item a:hover {

    	background-color: #ffffff;
    	background-color: rgba(255,255,255,1.00);
    }

    #footerwrap {

    	background-color: #004a82;
    	background-color: rgba(0,74,130,1.00);
    }

    #footer {

    	padding-top: 5%;
    	padding-right: 0%;
    	padding-bottom: 2%;
    	padding-left: 0%;
    	color: #ffffff;
    	color: rgba(255,255,255,1.00);
    }

    #footer a {

    	color: #fff769;
    	color: rgba(255,247,105,1.00);
    }

    #footer a:hover {

    	color: #ffffff;
    	color: rgba(255,255,255,1);
    }

    .social-links.horizontal li, .social-links.vertical li {
        margin: 0 20px 5px 0;
    }
    @media screen and (max-width: 780px) {
        .social-widget .social-links li i {
            margin-top: -66px;

         }

         .social-widget .widget {
            float:right;
            margin-right: 10px;
         }
         #site-logo img {
             margin-left: 10px;
         }
         #content {
            padding-top: 17px;
            padding: 5px;
            margin: 10px;
        }

        input, textarea, select, input[type=search], button {
            font-size: 100%;
            font-family: 'Josefin Sans', sans-serif;
        }

        #headerwrap #main-nav {
            clear: both;
            position: absolute !important;
            width: 220px;
            z-index: 1000;
            padding: 10px 12px;
            /* display: none; */
            left: 50%;
            margin-left: -110px;
            background: #000;
            border-radius: 10px;
        }
        .container-fluid {
            padding-left: 10px;
            margin-left: 13px;
            padding-right: 10px;
        }

        .container {
            margin-left: 13px;
            margin-right: 13px;
        }
    }

    @media (min-width: 1200px) {
        .span4 {
            width: 422px;
        }
    }

    .kiosk-access-buttons {
        /* position: absolute; */
        margin-top: -18px;
        bottom: -28px;
    }

    .kiosk-access-form {
        height: 151px;
        margin-bottom: 20px;
    }

    #kiosk_cardNumber {
        width: 160px;
    }

    input, textarea, select, input[type=search], button {
        font-size: 100%;
        font-family: 'Josefin Sans', sans-serif;
    }


    </style>

</head>
<body>
	<div id="headerwrap">
		<header id="header" class="section-inner clearfix">
			<hgroup>
				<div id="site-logo"><a href="http://imhuckleberry.com" title="Huckleberry"><img src="/images/imhuckleberry/logomed.png" alt="Huckleberry" title="Huckleberry" /><span style="display: none;">Huckleberry</span></a></div>				<div class="social-widget">
					<div id="themify-social-links-3" class="widget themify-social-links"><ul class="social-links horizontal">
							<li class="social-link-item facebook font-icon icon-medium">
								<a href="https://www.facebook.com/imhuckleberry" title="Facebook" ><i class="fa fa-facebook" style="color: #ffffff;background-color: #0763b8;"></i>  </a>
							</li>
							<!-- /themify-link-item -->
							<li class="social-link-item twitter font-icon icon-medium">
								<a href="https://twitter.com/HuckleberryDMV" title="Twitter" ><i class="fa fa-twitter" style="color: #ffffff;background-color: #00adf2;"></i>  </a>
							</li>
							<!-- /themify-link-item -->
							<li class="social-link-item apple font-icon icon-medium">
								<a href="https://itunes.apple.com/us/app/droplocker/id686269581?mt=8&amp;uo=4" title="Apple" ><i class="fa fa-apple" style="color: #ffffff;background-color: #672c94;"></i>  </a>
							</li>
							<!-- /themify-link-item -->
							<li class="social-link-item youtube font-icon icon-medium">
								<a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities&amp;hl=en" title="YouTube" ><i class="fa fa-android" style="color: #ffffff;background-color: #7bc775;"></i>  </a>
							</li>
							<!-- /themify-link-item --></ul></div>				</div>
							<div id="site-description" class="site-description"><span>24/Hr Dry Cleaning &amp; WashNFold</span></div>
							<!-- /.social-widget -->
			</hgroup>

		</header>
		<nav>
			<div id="menu-icon" class="mobile-button"><span><i id="mobile-icon" class="fa fa-bars fa-lg"></i></span></div>
			<!-- Menu -->
			<ul id="main-nav" class="main-nav pagewidth"><li id="menu-item-2450" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-2450"><a href="http://imhuckleberry.com/">Home</a>
            <ul class="sub-menu">
            	<li id="menu-item-2451" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2451"><a href="http://imhuckleberry.com/#howitworks">How it Works</a></li>
            	<li id="menu-item-2452" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2452"><a href="http://imhuckleberry.com/#testimonials">Testimonials</a></li>
            	<li id="menu-item-2453" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2453"><a href="http://imhuckleberry.com/#app">Get the App</a></li>
            </ul>
            </li>
            <li id="menu-item-2383" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2383"><a href="http://imhuckleberry.com/story/">The Story</a>
            <ul class="sub-menu">
            	<li id="menu-item-2442" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2442"><a href="http://imhuckleberry.com/story/#background">Background</a></li>
            	<li id="menu-item-2433" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2433"><a href="http://imhuckleberry.com/story/#placinganorder">Placing an Order</a></li>
            	<li id="menu-item-2434" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2434"><a href="http://imhuckleberry.com/story/#features">Features</a></li>
            	<li id="menu-item-2435" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2435"><a href="http://imhuckleberry.com/story/#24kiosk">The 24/7 Kiosk</a></li>
            	<li id="menu-item-2436" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2436"><a href="http://imhuckleberry.com/story/#homedelivery">Home Delivery</a></li>
            </ul>
            </li>
            <li id="menu-item-2395" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2395"><a href="http://imhuckleberry.com/service/">The Service</a>
            <ul class="sub-menu">
            	<li id="menu-item-2437" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2437"><a href="http://imhuckleberry.com/service/#drycleaning">Dry Cleaning</a></li>
            	<li id="menu-item-2438" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2438"><a href="http://imhuckleberry.com/service/#washfold">Wash &#038; Fold</a></li>
            	<li id="menu-item-2439" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2439"><a href="http://imhuckleberry.com/service/#extras">Alterations</a></li>
            	<li id="menu-item-2440" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2440"><a href="http://imhuckleberry.com/service/#extras">Shoes</a></li>
            	<li id="menu-item-2441" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2441"><a href="http://imhuckleberry.com/service/#extras">Packages (coming soon)</a></li>
            </ul>
            </li>
            <li id="menu-item-2443" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2443"><a href="http://imhuckleberry.com/people/">The People</a>
            <ul class="sub-menu">
            	<li id="menu-item-2444" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2444"><a href="http://imhuckleberry.com/people/#team">Team</a></li>
            	<li id="menu-item-2445" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2445"><a href="http://imhuckleberry.com/people/#affiliates">Affiliates</a></li>
            	<li id="menu-item-2446" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2446"><a href="http://imhuckleberry.com/people/#questions">FAQs</a></li>
            </ul>
            </li>
            <li id="menu-item-2411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2411"><a href="http://imhuckleberry.com/community/">The Community</a>
            <ul class="sub-menu">
            	<li id="menu-item-2447" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2447"><a href="http://imhuckleberry.com/community/#press">Press Releasese</a></li>
            	<li id="menu-item-2448" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2448"><a href="http://imhuckleberry.com/community/#environment">The Environment</a></li>
            	<li id="menu-item-2449" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2449"><a href="http://imhuckleberry.com/community/#charity">Giving Back</a></li>
            </ul>
            </li>
            <li id="menu-item-2417" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2417"><a href="http://imhuckleberry.com/partner/">Partner</a></li>
            <li id="menu-item-2423" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2423"><a href="http://imhuckleberry.com/locations/">Locations</a></li>
            <li id="menu-item-2372" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2372"><a href="https://myaccount.imhuckleberry.com/login">Login/Register</a></li>
            </ul>
		</nav>
	</div>

    <div id="main-content" style="padding-top: 20px; padding-bottom:20px;">
        <?= $content ?>
    </div>



    <div id="footerwrap">
    	<footer id="footer" class="pagewidth clearfix">
        	<div class="footer-widgets clearfix">
                <div class="col4-2 first">
        		  <div id="text-2" class="widget widget_text">
        			<div class="textwidget"><h4 style="color:#fff"><strong>Huckleberry</strong></h4>
                        <p>6412 Brandon Ave<br>
                        Suite 411<br>
                        Springfield, VA 22150</p>
                        <p><a href="mailto:info@imhuckleberry.com?Subject=Web%20Query">info@imhuckleberry.com</a></p>
                        <p>© Huckleberry 2014<br>
                        <a href="/terms">Terms and Conditions</a></p>
                     </div>
            	   </div>
            	</div>
        	</div>
            <div class="footer-text clearfix">
                <div class="one"> </div>
    			<div class="two"> </div>
            </div>
		</footer>
    </div>


<script> $('#mobile-icon').click(function(){ $('#main-nav').toggle(); }); </script>
<?php if(ENVIRONMENT == 'production' && $ga = get_business_meta($this->business_id, 'google_analytics')):?>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?= $ga?>']);
_gaq.push(['_setDomainName', 'dashlocker.com']);
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<?php endif; ?>
</body>
</html>
