<!DOCTYPE html>
<html lang="en-US" class=" html_stretched responsive av-preloader-active av-preloader-enabled av-default-lightbox  html_header_top html_logo_left html_main_nav_header html_menu_right html_custom html_header_sticky html_header_shrinking_disabled html_mobile_menu_tablet html_disabled html_header_searchicon_disabled html_content_align_center html_header_unstick_top html_header_stretch html_minimal_header html_entry_id_141 ">
    <head>
        <meta charset="UTF-8" />

        <link rel="icon" href="/images/mybutlerbox/fav_icon.png" type="image/png">
        <script type="text/javascript" src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>


        <!-- google webfont font replacement -->
        <link rel='stylesheet' id='avia-google-webfont' href='//fonts.googleapis.com/css?family=Raleway%7CLato:300,400,700' type='text/css' media='all'/>
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"\/js\/mybutlerbox\/wp-emoji-release.min.js?ver=4.3.1"}};
            !function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
        </script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap-responsive.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
        <link rel='stylesheet' id='font-awesome-css'  href='/css/mybutlerbox/css/font-awesome.min.css?ver=4.3.1' type='text/css' media='all' />
        <link rel='stylesheet' id='the-social-links-style-css'  href='/css/mybutlerbox/css/style.css?ver=4.3.1' type='text/css' media='all' />
        <link rel='stylesheet' id='avia-grid-css'  href='/css/mybutlerbox/css/grid.css?ver=2' type='text/css' media='all' />
        <link rel='stylesheet' id='avia-base-css'  href='/css/mybutlerbox/css/base.css?ver=2' type='text/css' media='all' />
        <link rel='stylesheet' id='avia-layout-css'  href='/css/mybutlerbox/css/layout.css?ver=2' type='text/css' media='all' />
        <link rel='stylesheet' id='avia-scs-css'  href='/css/mybutlerbox/css/shortcodes.css?ver=2' type='text/css' media='all' />
        <link rel='stylesheet' id='avia-popup-css-css'  href='/css/mybutlerbox/css/magnific-popup.css?ver=1' type='text/css' media='screen' />
        <link rel='stylesheet' id='avia-media-css'  href='/css/mybutlerbox/css/mediaelementplayer.css?ver=1' type='text/css' media='screen' />
        <link rel='stylesheet' id='avia-print-css'  href='/css/mybutlerbox/css/print.css?ver=1' type='text/css' media='print' />
        <link rel='stylesheet' id='avia-dynamic-css'  href='/css/mybutlerbox/css/enfold.css?ver=5611f42dd7ae8' type='text/css' media='all' />
        <link rel='stylesheet' id='avia-custom-css'  href='/css/mybutlerbox/css/custom.css?ver=2' type='text/css' media='all' />
        <!-- <script type='text/javascript' src='/css/mybutlerbox/js/jquery.js?ver=1.11.3'></script>-->
        <script type='text/javascript' src='/css/mybutlerbox/js/jquery-migrate.min.js?ver=1.2.1'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/avia-compat.js?ver=2'></script>



        <script type="text/javascript" src='/js/functions.js'></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>




        <script type='text/javascript' src='/css/mybutlerbox/js/core.min.js?ver=1.11.4'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/widget.min.js?ver=1.11.4'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/mouse.min.js?ver=1.11.4'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/sortable.min.js?ver=1.11.4'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/avia.js?ver=3'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/shortcodes.js?ver=3'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/jquery.magnific-popup.min.js?ver=2'></script>
        <script type='text/javascript'>
        /* <![CDATA[ */
        var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Download File":"Download File","Download Video":"Download Video","Play\/Pause":"Play\/Pause","Mute Toggle":"Mute Toggle","None":"None","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Unmute":"Unmute","Mute":"Mute","Captions\/Subtitles":"Captions\/Subtitles"}};
        var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
        /* ]]> */
        </script>
        <script type='text/javascript' src='/css/mybutlerbox/js/mediaelement-and-player.min.js?ver=2.17.0'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/wp-mediaelement.js?ver=4.3.1'></script>
        <script type='text/javascript' src='/css/mybutlerbox/js/comment-reply.min.js?ver=4.3.1'></script>


        <style type='text/css' media='screen'>
         #top #header_main > .container, #top #header_main > .container .main_menu ul:first-child > li > a, #top #header_main #menu-item-shop .cart_dropdown_link{ height:70px; line-height: 70px; }
         .html_header_top.html_header_sticky #top #wrap_all #main{ padding-top:68px; }
        </style>
        <!--[if lt IE 9]><script src="http://mybutlerbox.com/wp-content/themes/enfold/js/html5shiv.js"></script><![endif]-->

        <style type='text/css'>
        @font-face {font-family: 'entypo-fontello'; font-weight: normal; font-style: normal;
        src: url('/css/mybutlerbox/fonts/entypo-fontello.eot?v=3');
        src: url('/css/mybutlerbox/fonts/entypo-fontello.eot?v=3#iefix') format('embedded-opentype'),
        url('/css/mybutlerbox/fonts/entypo-fontello.woff?v=3') format('woff'),
        url('/css/mybutlerbox/fonts/entypo-fontello.ttf?v=3') format('truetype'),
        url('/css/mybutlerbox/fonts/entypo-fontello.svg?v=3#entypo-fontello') format('svg');
        } #top .avia-font-entypo-fontello, body .avia-font-entypo-fontello, html body [data-av_iconfont='entypo-fontello']:before{ font-family: 'entypo-fontello'; }
        </style>

        <?= $_scripts;?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>

        <link rel="stylesheet" type="text/css" href="/css/mybutlerbox/css/bootstrapmods.css"/>
        <style type="text/css">
            .navbar-fixed-top {
                display: none!important;
            }
            .container .row {
                margin-top: 80px!important;
            }
        </style>
    </head>

    <body id="top" class="page page-id-141 page-parent page-template-default stretched raleway lato " itemscope="itemscope" itemtype="https://schema.org/WebPage" >
    	<div class='av-siteloader-wrap av-transition-enabled av-transition-with-logo'>
    	   <div class='av-siteloader-inner'>
        	   <div class='av-siteloader-cell'><img class='av-preloading-logo' src='/css/mybutlerbox/img/bb_full_logo_header-300x211.png' alt='Loading' title='Loading' />
        	       <div class='av-siteloader'>
        	           <div class='av-siteloader-extra'>
        	           </div>
                    </div>
                </div>
            </div>
        </div>
    	<div id='wrap_all'>
            <header id='header' class=' header_color dark_bg_color  av_header_top av_logo_left av_main_nav_header av_menu_right av_custom av_header_sticky av_header_shrinking_disabled av_header_stretch av_mobile_menu_tablet av_header_searchicon_disabled av_header_unstick_top av_minimal_header av_bottom_nav_disabled  av_header_border_disabled'  role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader" >
                <a id="advanced_menu_toggle" href="#" aria-hidden='true' data-av_icon='' data-av_iconfont='entypo-fontello'></a><a id="advanced_menu_hide" href="#" 	aria-hidden='true' data-av_icon='' data-av_iconfont='entypo-fontello'></a>
        		<div  id='header_main' class='container_wrap container_wrap_logo'>
                    <div class='container'>
                        <div class='inner-container'>
                            <strong class='logo'>
                                <a href='http://mybutlerbox.com/'>
                                    <img height='100' width='300' src='/images/mybutlerbox/header_logo.png' alt='ButlerBox' />
                                </a>
                            </strong>
                            <nav class='main_menu' data-selectname='Select a page'  role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" >
                                <div class="avia-menu av-main-nav-wrap">
                                    <ul id="avia-menu" class="menu av-main-nav">
                                        <li id="menu-item-142" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-1">
                                            <a href="http://mybutlerbox.com/home/#top" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Home</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a>
                                        </li>
                                        <li id="menu-item-144" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-2">
                                            <a href="http://mybutlerbox.com/home/dryclean-wash-fold-shoe-repair/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Services</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a>
                                        </li>
                                        <li id="menu-item-145" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-top-level menu-item-top-level-3">
                                            <a href="http://mybutlerbox.com/home/how-it-works/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">How it Works</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a>


                                            <ul class="sub-menu">
                                            	<li id="menu-item-143" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://mybutlerbox.com/home/prices/" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Prices</span></a></li>
                                            	<li id="menu-item-146" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="http://mybutlerbox.com/home/#faqs" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Faqs</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-235" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-4"><a href="http://mybutlerbox.com/home/#locations" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Locations</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                                        <li id="menu-item-159" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-top-level menu-item-top-level-5"><a href="http://mybutlerbox.com/home/#contact" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">Contact</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a>

                                            <ul class="sub-menu">
                                            	<li id="menu-item-367" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#aboutus" itemprop="url"><span class="avia-bullet"></span><span class="avia-menu-text">About Us</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
        					<!-- end inner-container-->
    			        </div>
    		        <!-- end container-->
    		        </div>
        		<!-- end container_wrap-->
        		</div>
                <div class='header_bg'></div>
            <!-- end header -->
            </header>

            <div id="DL_container">
                <?= $content; ?>
            </div>
            <div class='container_wrap footer_color' id='footer'>
                <div class='container'>
				    <div class='flex_column av_one_fourth  first el_before_av_one_fourth'>
				        <section id="nav_menu-2" class="widget clearfix widget_nav_menu">
				            <h3 class="widgettitle">Menu</h3>
				                <div class="menu-side-menu-container">
				                    <ul id="menu-side-menu" class="menu">
				                        <li id="menu-item-346" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-141 current_page_item menu-item-346"><a href="http://mybutlerbox.com/home/">Home</a></li>
                                        <li id="menu-item-338" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-338"><a href="http://mybutlerbox.com/home/dryclean-wash-fold-shoe-repair/">Services</a></li>
                                        <li id="menu-item-337" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-337"><a href="http://mybutlerbox.com/home/how-it-works/">How it Works</a></li>
                                        <li id="menu-item-342" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-342"><a href="http://mybutlerbox.com/home/prices/">Prices</a></li>
                                    </ul>
                                </div>
                                <span class="seperator extralight-border"></span>
                        </section>
                    </div>
                    <div class='flex_column av_one_fourth  el_after_av_one_fourth  el_before_av_one_fourth '>
                        <section id="nav_menu-5" class="widget clearfix widget_nav_menu">
                            <h3 class="widgettitle"></h3>
                                <div class="menu-client-menu-container">
                                    <ul id="menu-client-menu" class="menu">
                                        <li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"></li>
                                    </ul>
                                </div>
                                <span class="seperator extralight-border"></span>
                        </section>
                    </div>
                    <div class='flex_column av_one_fourth  el_after_av_one_fourth  el_before_av_one_fourth '>
                        <section id="simpleimage-3" class="widget clearfix widget_simpleimage">
                            <h3 class="widgettitle">Partners</h3>
    	                    <p class="simple-image">
    		                <img width="1440" height="370" src="/images/mybutlerbox/greenearth_logo_wht.png" class="attachment-full" alt="greenearth_logo_wht" />	</p>
                            <span class="seperator extralight-border"></span></section><section id="simpleimage-4" class="widget clearfix widget_simpleimage">
                            <p class="simple-image">
    		                <img width="694" height="228" src="/images/mybutlerbox/luxer_logo_wht.png" class="attachment-full" alt="luxer_logo_wht" />	</p>
                            <span class="seperator extralight-border"></span>
                        </section>
                    </div>
                    <div class='flex_column av_one_fourth  el_after_av_one_fourth  el_before_av_one_fourth '>
                        <section id="the_social_links-2" class="widget clearfix widget_the_social_links">
                            <h3 class="widgettitle">   </h3>
                                <a href="http://facebook.com/MyButlerBox" class="the-social-links tsl-circle tsl-48 tsl-default tsl-facebook" target="_blank" alt="Facebook" title="Facebook"><i class="fa fa-facebook"></i></a>&nbsp
                                <a href="" class="the-social-links tsl-circle tsl-48 tsl-default tsl-google-plus" target="_blank" alt="Google+" title="Google+"><i class="fa fa-google-plus"></i></a>&nbsp
                                <a href="http://twitter.com/MyButlerBox" class="the-social-links tsl-circle tsl-48 tsl-default tsl-twitter" target="_blank" alt="Twitter" title="Twitter"><i class="fa fa-twitter"></i></a>&nbsp
                                <a href="http://instagram.com/butlerboxla" class="the-social-links tsl-circle tsl-48 tsl-default tsl-instagram" target="_blank" alt="Instagram" title="Instagram"><i class="fa fa-instagram"></i></a>&nbsp
                                <span class="seperator extralight-border"></span>
                        </section>
                        <section id="simpleimage-2" class="widget clearfix widget_simpleimage">
        	                    <p class="simple-image">
        		                  <img width="666" height="156" src="/images/mybutlerbox/bb_logo_header.png" class="attachment-full" alt="bb_logo_header" />
        		               </p>
                          <p>818-384-1744<br />
                            info@mybutlerbox.com</p>
                            <p>5404 Whitsett ave #105<br />
                            Valley Village, CA 91607</p>

                            <span class="seperator extralight-border"></span>
                        </section>
                    </div>
				</div>
            <!-- ####### END FOOTER CONTAINER ####### -->
            </div>
        <!-- end main -->
        </div>

        <a href='#top' title='Scroll to top' id='scroll-top-link' aria-hidden='true' data-av_icon='' data-av_iconfont='entypo-fontello'><span class="avia_hidden_link_text">Scroll to top</span></a>

        <div id="fb-root"></div>

        <script type="text/javascript">
        $('.container>.row>.container').addClass("offset1");
        $('.container>.span9').removeAttr('style');
        $('.container>.span9').addClass("offset1 span7");
        $('.container>.span9').removeClass("span9");
        $('.container>.row>#customer_locations').addClass("offset1");
        $(".container>.row>.span7").addClass("offset1");
        $('.container>.row>.span7').removeClass("container");
        $(".container>.span9").css("margin-left","146px");
        </script>
    </body>
</html>
