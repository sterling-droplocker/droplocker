<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>TintoLocker - Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <script src="/js/tintolocker/modernizr-2.6.2.min.js"></script>
        <link rel="shortcut icon" href="/images/tintolocker/favicon.ico">

        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="/css/tintolocker/main.css?v=" />
        <script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src='/bootstrap/js/bootstrap.min.js'></script>
        <?= $_scripts; ?>
        <?= $javascript; ?>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>

        <?= $style ?>
        <?= $_styles ?>


        <style>
            div.alert h1 {
                font-size: 24px;
            }

            form a {
                font-size: 14px;
                vertical-align: bottom;
            }

            .btn {
                background: #31CBEC;
                border-radius: 12px;
                color: white;
                border: 1px solid #ddd;
                padding: 8px 19px;
                text-shadow: none;
                display: inline-block;
                vertical-align: middle;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.3;
                text-align: center;
                text-decoration: none;

                margin: 0;
                border: none;
                border-radius: 3px;
                box-shadow: none;
                cursor: pointer;

                padding: 9px 20px;
                height: 100%;
            }

            a.btn:active,a.btn:focus,a.btn:visited {
                color: white;
            }

            .btn-danger {
                background: #F05D5D;
            }

            label.invalid {
                font-size: 16px;
                color: #F05D5D;
            }

            .kiosk-access-buttons {
                position: absolute;
                bottom: 0;
                right: -10px;
                top: 10px;
            }

            .kiosk-access-form {
                height: 160px;
            }

            .kiosk-access-buttons p  {
                margin-bottom: 2px;
            }

            #kiosk_cardNumber {
                text-indent: 10em;
                background: transparent;
            }

            .cc-mask {

                padding: 12px;
                
            }

            .navbar ul li {
                margin-bottom: 5px;
            }

            h2 {
                font-size: 28px;
            }

            h3 {
                font-size: 20px;
            }

            .breadcrumb li {
                font-size: 14px;
            }

            #credit_card_form {
                width: 100%;
            }

            .add-on {
                padding-bottom: 25px;
            }
        </style>

      

    </head>
    <body id="especial" >
        <!--[if lt IE 7]>
            <p class="chromeframe">¡Hola! Est&#225;s utilizando un navegador obsoleto. <a href="http://browsehappy.com/">Por favor actualiza tu navegador</a> o <a href="http://www.google.com/chromeframe/?redirect=true">instala Google Chrome Frame</a> para experimentar este sitio al 100%.</p>
        <![endif]-->
        <!-- Preloader -->
        <div id="home"></div>
        <div class="container-block">

            <div class="container-block relative">
                <div class="menu-main units-container white-bg end nav-shadow relative mobile-hides" id="pin1" style="position: fixed; top: 0px;">
                    <header class="group grid-1200 relative">


                        <!--<div class="units-row text-centered menuTop unit-padding units-split">
                          <div class="unit-10"><a href="#home" class="scrollLink heavy">INICIO</a></div>
                          <div class="unit-10"><a href="#como-funciona" class="scrollLink heavy">CÓMO FUNCIONA</a></div>
                          <div class="unit-10"><a href="#servicios" class="scrollLink heavy">SERVICIOS</a></div>
                          <div class="unit-10"><a href="#acerca-de" class="scrollLink heavy">ACERCA DE</a></div>
                          <div class="unit-10"><a href="#faqs" class="scrollLink heavy">FAQs</a></div>
                          <div class="unit-10"><a href="#donde-esta" class="scrollLink heavy">DÓNDE ESTÁN</a></div>
                          <div class="unit-10"><a href="#login" class="scrollLink heavy">LOGIN</a></div>
                          <div class="unit-10"><a href="#contacto" class="scrollLink heavy">CONTACTO</a></div>
                          <div class="unit-10 text-right">
                            <a href="#"><i class="icon-facebook_circle icon1-5x blue-l"></i></a>
                            <a href="#"><i class="icon-twitter_circle icon1-5x blue-l"></i></a>
                          </div>
                        </div>-->

                        <div class="navigation-toggle unit-padding" data-tools="navigation-toggle" data-target="#menuTop">
                            <span><i class="icon-reorder icon1x cursorPointer"></i></span>
                        </div>
                        <nav class="navbar fullwidth end navigation-padding auto-margin white-bg mobile-hide" id="menuTop">
                            <ul>
                                <li><a href="http://tintolocker.com/#home" class="scrollLink medium heavy">INICIO</a></li>
                                <li><a href="http://tintolocker.com/#como-funciona" class="scrollLink medium heavy">CÓMO FUNCIONA</a></li>
                                <li><a href="http://tintolocker.com/#servicios" class="scrollLink medium heavy">SERVICIOS</a></li>
                                <li><a href="http://tintolocker.com/#acerca-de" class="scrollLink medium heavy">ACERCA DE</a></li>
                                <li><a href="http://tintolocker.com/#faqs" class="scrollLink medium heavy">FAQs</a></li>
                                <li><a href="http://tintolocker.com/#donde-esta" class="scrollLink medium heavy">DÓNDE ESTÁN</a></li>
                                <li><a href="http://tintolocker.com/#login" class="scrollLink medium heavy">LOGIN</a></li>
                                <li><a href="http://tintolocker.com/#contacto" class="scrollLink medium heavy">CONTACTO</a></li>
                                <li><a href="#"><i class="icon-facebook_circle icon1-5x blue-l"></i></a></li>
                                <li><a href="#"><i class="icon-twitter_circle icon1-5x blue-l"></i></a></li>
                            </ul>
                        </nav>



                    </header>
                </div>
            </div>
            <!-- INTRO -->
            <div class="units-container intro gray-l-bg gray-d intro-bg" data-0="background-position-y: 50%" data-750="background-position-y: -50%">
                <div class="units-row unit-padding grid-1200">
                    <div class="unit-90">
                        <div id="main_content">
                            <?= $content; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- INTRO -->



            <!-- CONTACT -->
            <div class="units-container units-padding gray-d-bg white" id="contacto">
                <div class="item-60"></div>
                <div class="units-row grid-1200 end">
                    <div class="unit-60 unit-centered text-centered">
                        <h1 class="">CONTÁCTANOS</h1>
                        <h3 class="">Si tienes preguntas sobre nuestros servicios o tu pedido,
                            envíanos tus datos en el siguiente formulario o llámanos al</h3>
                        <h2>(33) 36-41-78-56</h2>
                        <p>Pablo Neruda 4106
                            <br>Jardines Universidad
                            <br>Zapopan, Jal. CP 45110.</p>
                        <div class="item-30"></div>
                        <div id="success"></div>
                        <form id="contacto" method="post" action="" class="forms">
                            <label>
                                <input type="email" name="email"  class="width-50 centered" />
                            </label>
                            <label>
                                <input type="text" name="name"  class="width-50 centered" />
                            </label>
                            <label>
                                <textarea name="message" class="width-50 centered"  rows="5"></textarea>
                            </label>
                            <p>
                                <button type="button" id="enviar" class="btn-blue btn width-50">Contáctanos</button>
                            </p>
                        </form>
                        <div class="item-30"></div>
                        <img src="/images/tintolocker/tl_logo_h.png" alt="TintoLocker">
                        <div class="item-30"></div>
                        <a href="#" style="margin-right: 30px;"><i class="icon-facebook_circle icon3x white"></i></a>
                        &nbsp;
                        <a href="#"><i class="icon-twitter_circle icon3x white"></i></a>
                        <div class="item-30"></div>
                        <h4 class=""><a href="http://tintolocker.com/aviso-de-privacidad" class="white underlined">Aviso de privacidad</a></h4>
                        <h4 class=""><a href="http://tintolocker.com/terminos-y-condiciones" class="white underlined">Terminos y condiciones</a></h4>
                        <div class="item-30"></div>
                    </div>
                </div>
            </div>
            <!-- CONTACT -->
        </div>

        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.pjax/1.9.2/jquery.pjax.min.js"></script>
        <script src="/js/tintolocker/kube.js"></script>
        <script src="/js/tintolocker/plugins.js"></script>
        <script src="/js/tintolocker/main.js"></script>

        
    </body>
</html>