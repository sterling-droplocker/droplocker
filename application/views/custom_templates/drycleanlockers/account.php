<?php
$base_url = '//www.drycleanlockers.com';
?>
<!DOCTYPE HTML>
<html lang="en-US">

	<!-- START head -->
	<head>
	<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="pingback" href="<?php echo $base_url; ?>/xmlrpc.php" />

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/drycleanlockers/logo-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/drycleanlockers/logo-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/drycleanlockers/logo-72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/images/drycleanlockers/logo-57.png" />
	
	
	<!--[if lte IE 8]>
	<script src="/js/drycleanlockers/html5shiv.js" type="text/javascript"></script>
	<![endif]-->

		<title>Dry Clean Lockers &#8211; 24/7 Dry Cleaning</title>



        <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
        <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

		<script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <script src='/bootstrap/js/bootstrap.min.js'></script>

        <?= $_scripts ?>
        <?= $javascript?>
        <?= $style?>
        <?= $_styles ?>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel='stylesheet' href='/css/drycleanlockers/style.css?v=1' type='text/css' media='all' />
		<link rel='stylesheet' href='/css/drycleanlockers/responsive.css' type='text/css' media='all' />		

		<link rel='stylesheet'  href='/css/drycleanlockers/icomoon.css' type='text/css' media='all' />
		
		<link rel='stylesheet' href='/css/drycleanlockers/scss.css?v=2' type='text/css' media='all' />

		<link rel="stylesheet" href="/css/drycleanlockers/site.css?v=3">
	</head>
	<!-- END head -->

	
	<!-- START body -->
	<body class="home page page-id-34 page-template-default  highend-prettyphoto hb-stretched-layout wpb-js-composer js-comp-ver-4.11.1 vc_responsive" data-fixed-footer="0" itemscope="itemscope" itemtype="//schema.org/WebPage">

	
	
	
	<div id="mobile-menu-wrap">
<div class="hb-top-holder"></div><a class="mobile-menu-close"><i class="hb-icon-x"></i></a>
<nav id="mobile-menu" class="clearfix interactive">
<div class="menu-main-container"><ul id="menu-main" class="menu"><li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-34 current_page_item menu-item-58"><a href="<?php echo $base_url; ?>/">Home</a></li>
<li id="menu-item-242" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-242"><a href="<?php echo $base_url; ?>/how-it-works/">How it Works</a>
<ul class="sub-menu">
	<li id="menu-item-243" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-243"><a href="<?php echo $base_url; ?>/how-it-works/#4-easy-steps">4 Easy Steps</a></li>
	<li id="menu-item-244" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-244"><a href="<?php echo $base_url; ?>/how-it-works/#features">Features</a></li>
	<li id="menu-item-245" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-245"><a href="<?php echo $base_url; ?>/how-it-works/#rewards-program">Rewards Program</a></li>
	<li id="menu-item-246" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-246"><a href="<?php echo $base_url; ?>/how-it-works/#faq">FAQ</a></li>
</ul>
</li>
<li id="menu-item-248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-248"><a href="<?php echo $base_url; ?>/pricing/">Pricing</a>
<ul class="sub-menu">
	<li id="menu-item-250" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-250"><a href="<?php echo $base_url; ?>/pricing/#dry-cleaning">Dry Cleaning</a></li>
	<li id="menu-item-251" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-251"><a href="<?php echo $base_url; ?>/pricing/#laundry">Shirt Laundering</a></li>
	<li id="menu-item-257" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-257"><a href="<?php echo $base_url; ?>/pricing/#special-services">Special Services</a></li>
	<li id="menu-item-258" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-258"><a href="<?php echo $base_url; ?>/pricing/#household-items">Household Items</a></li>
	<li id="menu-item-252" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-252"><a href="<?php echo $base_url; ?>/pricing/#wash-fold">Wash &#038; Fold</a></li>
</ul>
</li>
<li id="menu-item-249" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-249"><a href="<?php echo $base_url; ?>/services/">Services</a>
<ul class="sub-menu">
	<li id="menu-item-253" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-253"><a href="<?php echo $base_url; ?>/services/#standard-services">Standard Services</a></li>
	<li id="menu-item-254" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-254"><a href="<?php echo $base_url; ?>/services/#special-services">Special Services</a></li>
	<li id="menu-item-255" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-255"><a href="<?php echo $base_url; ?>/services/#household-services">Household Services</a></li>
	<li id="menu-item-256" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-256"><a href="<?php echo $base_url; ?>/services/#wash-fold">Wash &#038; Fold</a></li>
</ul>
</li>
<li id="menu-item-267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-267"><a href="<?php echo $base_url; ?>/request-lockers/">Request Lockers</a></li>
<li id="menu-item-260" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-260"><a href="<?php echo $base_url; ?>/about-us/">About Us</a>
<ul class="sub-menu">
	<li id="menu-item-261" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-261"><a href="<?php echo $base_url; ?>/about-us/#contact-us">Contact Us</a></li>
	<li id="menu-item-262" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-262"><a href="<?php echo $base_url; ?>/about-us/#mission">Mission</a></li>
	<li id="menu-item-263" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-263"><a href="<?php echo $base_url; ?>/about-us/#impressive-cleaners">Impressive Cleaners</a></li>
</ul>
</li>
<li id="menu-item-265" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-265"><a href="<?php echo $base_url; ?>/packages/">Package Lockers</a></li>
</ul></div></nav>
</div>


	

	<!-- BEGIN #hb-wrap -->
	<div id="hb-wrap">

	<!-- BEGIN #main-wrapper -->
	<div id="main-wrapper" class="hb-stretched-layout hb_boxed_layout_regular with-shadow fw-100 hb-responsive nav-type-2" data-cart-url="" data-cart-count=""  data-search-header=0>

		
						<!-- BEGIN #hb-header -->
				<header id="hb-header" class=" without-top-bar">


        <!-- BEGIN #header-inner -->
        <div id="header-inner" class="nav-type-2 clearfix" role="banner" itemscope="itemscope" itemtype="//schema.org/WPHeader">

            <!-- BEGIN #header-inner-bg -->
            <div id="header-inner-bg">
                
                <!-- BEGIN .container or .container-wide -->
                <div class="container">
                    
                    <!-- BEGIN #logo -->
                    <div id="logo">
                                                <a href="<?php echo $base_url; ?>" class="image-logo">

                            
                            <span class="hb-dark-logo hb-visible-logo hb-logo-wrap">
                                <img src="/images/drycleanlockers/Dry-Cleaners-Logo-1.png" width="318" height="72" class="default" alt="Dry Clean Lockers"/>
                                
                                                                        <img src="/images/drycleanlockers/Dry-Cleaners-Logo-1.png" class="retina" width="636" height="144" alt="Dry Clean Lockers"/>
                                                                </span>

                                                    </a>

                                            </div>
                    <!-- END #logo -->


                                        </div>
                    <!-- END .container or .container-wide -->
                    <div class="clear"></div>
                    
                    
                    
                    <!-- BEGIN .main-navigation -->
                    <nav class="main-navigation minimal-skin  light-menu-dropdown clearfix" role="navigation" itemscope="itemscope" itemtype="//schema.org/SiteNavigationElement">

                                                <!-- BEGIN .container or .container-wide -->
                        <div class="container">
                        
                        <ul id="main-nav" class="sf-menu "><li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-34 current_page_item no-megamenu"><a href="<?php echo $base_url; ?>/"><span>Home</span></a></li>
<li id="menu-item-242" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children no-megamenu"><a href="<?php echo $base_url; ?>/how-it-works/"><span>How it Works</span></a>
<ul style="" class="sub-menu ">
	<li id="menu-item-243" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/how-it-works/#4-easy-steps"><span>4 Easy Steps</span></a></li>
	<li id="menu-item-244" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/how-it-works/#features"><span>Features</span></a></li>
	<li id="menu-item-245" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/how-it-works/#rewards-program"><span>Rewards Program</span></a></li>
	<li id="menu-item-246" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/how-it-works/#faq"><span>FAQ</span></a></li>
</ul>
</li>
<li id="menu-item-248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children no-megamenu"><a href="<?php echo $base_url; ?>/pricing/"><span>Pricing</span></a>
<ul style="" class="sub-menu ">
	<li id="menu-item-250" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/pricing/#dry-cleaning"><span>Dry Cleaning</span></a></li>
	<li id="menu-item-251" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/pricing/#laundry"><span>Shirt Laundering</span></a></li>
	<li id="menu-item-257" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/pricing/#special-services"><span>Special Services</span></a></li>
	<li id="menu-item-258" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/pricing/#household-items"><span>Household Items</span></a></li>
	<li id="menu-item-252" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/pricing/#wash-fold"><span>Wash &#038; Fold</span></a></li>
</ul>
</li>
<li id="menu-item-249" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children no-megamenu"><a href="<?php echo $base_url; ?>/services/"><span>Services</span></a>
<ul style="" class="sub-menu ">
	<li id="menu-item-253" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/services/#standard-services"><span>Standard Services</span></a></li>
	<li id="menu-item-254" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/services/#special-services"><span>Special Services</span></a></li>
	<li id="menu-item-255" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/services/#household-services"><span>Household Services</span></a></li>
	<li id="menu-item-256" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/services/#wash-fold"><span>Wash &#038; Fold</span></a></li>
</ul>
</li>
<li id="menu-item-267" class="menu-item menu-item-type-post_type menu-item-object-page no-megamenu"><a href="<?php echo $base_url; ?>/request-lockers/"><span>Request Lockers</span></a></li>
<li id="menu-item-260" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children no-megamenu"><a href="<?php echo $base_url; ?>/about-us/"><span>About Us</span></a>
<ul style="" class="sub-menu ">
	<li id="menu-item-261" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/about-us/#contact-us"><span>Contact Us</span></a></li>
	<li id="menu-item-262" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/about-us/#mission"><span>Mission</span></a></li>
	<li id="menu-item-263" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo $base_url; ?>/about-us/#impressive-cleaners"><span>Impressive Cleaners</span></a></li>
</ul>
</li>
<li id="menu-item-265" class="menu-item menu-item-type-post_type menu-item-object-page no-megamenu"><a href="<?php echo $base_url; ?>/packages/"><span>Package Lockers</span></a></li>
</ul>
                                                <!-- BEGIN #fancy-search -->
                        <div id="fancy-search">
                            <form id="fancy-search-form" action="<?php echo $base_url; ?>/" novalidate="" autocomplete="off">
                                <input type="text" name="s" id="s" placeholder="Type keywords and press enter" autocomplete="off">
                            </form>
                        <a href="#" id="close-fancy-search" class="no-transition"><i class="hb-icon-x"></i></a>
                        <span class="spinner"></span>
                        </div>
                        <!-- END #fancy-serach -->
                        

                                                <a href="#" id="show-nav-menu"><i class="icon-bars"></i></a>
                        

                                                <!-- END .container or .container-wide -->
                        </div>
                        
                    </nav>
                    <!-- END .main-navigation -->
                    
                                    </div>
            <!-- END #header-inner-bg -->

        </div>
        <!-- END #header-inner -->


				</header>
				<!-- END #hb-header -->

							

<!-- BEGIN #slider-section -->
<div id="slider-section" class="clearfix none" style="">
	</div>
<!-- END #slider-section -->		 
<!-- BEGIN #main-content -->
<div id="main-content">
	<div class="container">		
		<div class="row fullwidth main-row">
				<!-- BEGIN .hb-main-content -->
				<div class="col-12 hb-main-content">
					<br/>
				    <?= $content; ?>
				</div>
				<!-- END .hb-main-content -->							
			
			<!-- END #page-ID -->
		</div>
		<!-- END .row -->
	</div>
	<!-- END .container -->
</div>
<!-- END #main-content -->







<!-- BEGIN #footer OPTION light-style -->
<footer id="footer" class="dark-style footer-bg-image"  style="background-image: url(/images/drycleanlockers/footer-bg.png)">
	
	<!-- BEGIN .container -->
	<div class="container">
		<div class="row footer-row">

	<div class="col-4 widget-column no-separator"><div id="text-2" class="widget-item widget_text">			<div class="textwidget"><div class="col-12 no-margin">
<h4>DRY CLEAN LOCKERS</h4>
</div>
<div class="col-12 ">
9200 E Hampden Ave<br/>
Denver, CO 80231<br/>
(303) 209-3030<br/>
<a href="mailto: Support@DryCleanLockers.com"> Support@DryCleanLockers.com</a><br/>
</div></div>
		</div><div id="hb_soc_net_widget-2" class="widget-item hb-socials-widget">
		<ul class="social-icons light normal clearfix">
                                                    <li class="facebook">
                        <a href="//facebook.com/" original-title="Facebook" target="_blank">
                        	<i class="hb-moon-facebook"></i>
                        	<i class="hb-moon-facebook"></i>
                        </a> 
                    </li>
                                                            <li class="twitter">
                        <a href="//twitter.com/" original-title="Twitter" target="_blank">
                        	<i class="hb-moon-twitter"></i>
                        	<i class="hb-moon-twitter"></i>
                        </a> 
                    </li>
                            
		</ul>

		</div></div><div class="col-8 widget-column no-separator"><div id="text-4" class="widget-item widget_text">			<div class="textwidget"><div class="col-12 no-margin">
<h4>SITE MAP</h4>
</div>
<div class="col-3 ">
<a href="<?php echo $base_url; ?>/how-it-works"><strong>How it Works</strong></a><br/>
<a href="<?php echo $base_url; ?>/how-it-works/#4-easy-steps">4 Easy Steps</a><br/>
<a href="<?php echo $base_url; ?>/how-it-works/#features">Features</a><br/>
<a href="<?php echo $base_url; ?>/how-it-works/#rewards-program">Rewards Program</a><br/>
<a href="<?php echo $base_url; ?>/how-it-works/#faq">FAQ</a><br/>
</div>
<div class="col-3 ">
<a href="<?php echo $base_url; ?>/pricing"><strong>Pricing</strong></a><br/>
<a href="<?php echo $base_url; ?>/pricing/#dry-cleaning">Dry Cleaning</a><br/>
<a href="<?php echo $base_url; ?>/pricing/#laundry">Shirt Laundering</a><br/>
<a href="<?php echo $base_url; ?>/pricing/#special-services">Special Services</a><br/>
<a href="<?php echo $base_url; ?>/pricing/#household-items">Household Items</a><br/>
<a href="<?php echo $base_url; ?>/pricing/#wash-fold">Wash & Fold</a><br/>
</div>
<div class="col-3 ">
<a href="<?php echo $base_url; ?>/about-us"><strong>About Us</strong></a><br/>
<a href="<?php echo $base_url; ?>/about-us/#contact-us">Contact Us</a><br/>
<a href="<?php echo $base_url; ?>/about-us/#mission">Mission</a><br/>
<a href="<?php echo $base_url; ?>/about-us/#impressive-cleaners">Impressive Cleaners</a><br/>
</div>
<div class="col-3 ">
<a href="<?php echo $base_url; ?>/request-lockers"><strong>Request Lockers</strong></a><br/>
<a href="<?php echo $base_url; ?>/packages"><strong>Package Lockers</strong></a><br/>
</div></div>
		</div></div><div class="hidden widget-column no-separator"></div><div class="hidden widget-column no-separator"></div>		</div>		
	</div>
	<!-- END .container -->

</footer>
<!-- END #footer -->

<!-- BEGIN #copyright-wrapper -->
<div id="copyright-wrapper" class="normal-copyright footer-bg-image clearfix"> <!-- Simple copyright opcija light style opcija-->

    <!-- BEGIN .container -->
    <div class="container">

        <!-- BEGIN #copyright-text -->
        <div id="copyright-text">
            <p>© COPYRIGHT DRY CLEAN LOCKERS 2016
            
            </p>
        </div>
        <!-- END #copyright-text -->

        
    </div> 
    <!-- END .container -->

</div>
<!-- END #copyright-wrapper -->

</div>
<!-- END #main-wrapper -->

</div>
<!-- END #hb-wrap -->


<script type='text/javascript' src='/js/drycleanlockers/scripts.js?ver=3.2.2'></script>
<script type='text/javascript' src='/js/drycleanlockers/jquery.custom.js?ver=3.2.2'></script>

</body>
<!-- END body -->

</html>
<!-- END html -->