
<div class="fluid-container">
<div style="display: block; clear: both; margin-top:0">
<div style='text-align:center; text-shadow:1px 1px 1px #666; padding:0px;text-transform:none !important;'>
    <?
    $runTime = strtotime(convert_from_gmt_aprax(date('Y-m-d H:i:s'), "Y-m-d H:i:d"));
    $endTime = strtotime("2014-05-27 00:00:00");
    $holiday_language = '<h1 style="text-transform:none !important;">Closed on Monday, May 26 for Memorial Day</h1>';
    if($runTime < $endTime):?>
    <?= $holiday_language; ?>
    <?endif;?>
</div>
<?php

$bSelected = '';

$promo = 'HP1230';

if (!array_key_exists('laundrylocker_promo', $_COOKIE) && array_key_exists('promo', $_GET)) {
    setcookie('laundrylocker_promo', $_GET['promo'], time()+60*60*24*30, '/', 'laundrylocker.com');
}

if (array_key_exists('laundrylocker_promo', $_COOKIE)) {
    $promo = $_COOKIE['laundrylocker_promo'];
}

if (array_key_exists('promo', $_GET)) {
    $promo = $_GET['promo'];
}

switch($promo) {
    case 'AR1230':
        $bSelected = 'AR1230-LL-home-page-banner-toast.jpg';
        break;
    case 'AW1230':
        $bSelected = 'AW1230-LL-home-page-banner-locavores.jpg';
        break;
    case 'FB1230':
        $bSelected = 'FB1230-LL-home-page-banner-artisinal.jpg';
        break;
    case 'PP1230':
        $bSelected = 'PP1230-LL-home-page-banner-artisinal.jpg';
        break;
    case 'YP1230':
        $bSelected = 'YP1230-LL-home-page-banner-artisinal.jpg';
        break;
    default:
        $bSelected = 'HP1230-LL-home-page-banner-artisinal.jpg';
        break;
}

?>
<div class="row-fluid hidden">
    <div class="span12 center">
        <div class="" style="margin-bottom: 20px;">
            <a href="/account/main/view_registration?promo=<?=$promo?>">
                    <img src="/images/<?=$bSelected?>" style="width: 100%;">
            </a>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12 center">
        <div class="span8">
            <!--<div style="float:left; width: 655px;">-->
            <!--<div style="float: left; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>-->
            <iframe width="100%" height="340" src="https://www.youtube.com/embed/DKnW9nn4xro" frameborder="0" allowfullscreen></iframe>
         
            <div class="calloutBanner">

                <div class="span4 hidden-tablet hidden-phone" style="margin-top: 29px">
                    <div id="calloutLeft">
                        <h1 class="calloutH1"><a href="/main/howto">How it works</a></h1>
                        <h2 class="calloutH2" style="">4 Easy Steps</h2>
                    </div>
                </div>
                <div id="fourStepsWrapper" class="span8">

                    <ul id="fourSteps" class="inline">
                        <li class="headingBlue">
                            <img src="/images/icon_dropOff.jpg" alt="Drop Off" border="0">
                            <br>1. Drop Off
                        </li>

                        <li class="headingBlue">
                            <img src="/images/icon_placeOrder.jpg" alt="Place Order" border="0">
                            <br>2. Place <br /> Your Order
                        </li>

                        <li class="headingBlue">
                            <img src="/images/icon_getAlert.jpg" alt="Get Alert" border="0">
                            <br>3. Get <br />Notified
                        </li>

                        <li class="headingBlue fourStepsLast">
                            <img src="/images/icon_pickUp.jpg" alt="Pick UP" border="0">
                            <br>4. Pick Up
                        </li>
                    </ul>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span12"style="text-align: left; margin-bottom: 10px">
                    <h1>Our Customers Say It Best</h1>
                    <div class="bodyBig" style="padding-top: 10px; padding-bottom: 25px;">
                        <?
                        //get a random testamonial
                        $select1 = "select * from testimonial order by rand() limit 1;";
                        $query = $this->db->query($select1);
                        $line = $query->result();

                        echo '"'.substr($line[0]->testimonial,0,300);
                        if(strlen($line[0]->testimonial) > 300) { echo ' ...'; }
                        echo '"  <a href="/main/testimonials" class="greenLink"><strong>Read More</strong></a><br>
                        <div style="float:right;"> - '.$line[0]->customerName.'</div>';
                        ?>
                    </div>
                    <div class="hr"><hr /></div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6 testimonies" style="text-align: left;">
                    <h2>Named Best Dry Cleaner in San Francisco</h2>
                    <div class="graySmall">
                        Laundry Locker Named Best Dry Cleaner in San Francisco in the SF Examiner's Readers' Choice Awards
                    </div>
                    <div>
                        <a href="/main/press" class="greenLink">Read More</a>
                    </div>
                </div>
                <div class="testimonies span6 hidden-phone hidden-tablet">
                    <img src="/images/RC_winner_Logo.png" title="Examiner Readers' Choice Winner" style="margin-left: 10px; float: right;" />
                </div>
            </div>
            <br/>
            <br/>
            <div class="row-fluid">
                    <div class="span4 testimonies" style="text-align: left;">
                        <h2>Voted San Francisco's Best Dry Cleaner</h2>
                        <div class="graySmall">
                            Laundry Locker, San Francisco's #1 Eco-Friendly Dry Cleaner!
                        </div>
                        <div>
                            <a href="/main/press" class="greenLink">Read More</a>
                        </div>
                    </div>
                <div class="testimonies span8 hidden-phone hidden-tablet">
                    <img  src="/images/SFWeekly2011.gif" alt="Best of SF Weekly Winner" border="0">
                    <img src="/images/sfcc_2015.png" title="SFCC 2015" style="margin-left: 20px; float: right;" />
                </div>
            </div>
        </div>

        <div class="span4" id="rightBanners">
            <?= callOut_iphone();?>
            <?= callOut_concierge();?>
            <?= callOut_forbes();?>
        </div>
    </div>
</div>
</div>
</div>