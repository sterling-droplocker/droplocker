<!DOCTYPE html>
<html>
   <head>
       <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
      <title><?= $title ?></title>
    <script type="text/javascript" src="/js/report_error.js"></script>
   
    <?php if(ENVIRONMENT == 'production' && strpos( strtolower( $_SERVER['HTTP_HOST'] ), 'laundrylocker' ) !== false ){ ?>
    <script type="text/javascript">
    </script>
    <!-- begin olark code -->
    <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
    f[z]=function(){
    (a.s=a.s||[]).push(arguments)};var a=f[z]._={
    },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
    f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
    0:+new Date};a.P=function(u){
    a.p[u]=new Date-a.p[0]};function s(){
    a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
    hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
    return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
    b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
    b.contentWindow[g].open()}catch(w){
    c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
    var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
    b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
    loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('3708-947-10-4387');/*]]>*/</script><noscript><a href="https://www.olark.com/site/3708-947-10-4387/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
    <!-- end olark code -->
    <?php } ?>
  
      <script type="text/javascript" src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
      <script type="text/javascript" src="/js/jquery.loading_indicator.js"> </script>
      <script type="text/javascript" src='/js/functions.js'></script>
      <?= $_scripts ?>
      <?php if($ga = get_business_meta($this->business_id, 'google_analytics')):?>
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
              { (i[r].q=i[r].q||[]).push(arguments)}
              ,i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
              ga('create', '<?php echo $ga?>', 'auto');
              ga('send', 'pageview');
            </script>
    	<?php endif; ?>
      <?= $javascript?>
      <?= $style?>
      <?= $_styles ?>
   
<style type="text/css">
    
.invalid {
            color: red;
        }
.laundry_plan .green {
    color:#6D9E2C;	
    background: white !important;
    border : none;
	filter: none;
}

</style>
   </head>
   <body>
        <div id="fb-root"></div>

       <!-- End of facebook code -->
      <div id="wrapper">
      	<div id="header">

          <?= $header ?>

            
        </div>
          
          <div style='padding:0px 10px 20px 0px;'>
             <div style='padding:0px;'><?= $content ?></div>
             <br style='clear:both'>
          </div>
          
         <div id="footer" style='clear:both'>
            <?= $footer ?>
         </div>
          
         <? if( strpos( strtolower( $_SERVER['HTTP_HOST'] ), 'bizzie') === false ){ ?>
         <hr>
         <div style='text-align: right'>
         	<a href='http://droplocker.com'><img style='width:20%' align='center' src='/images/logo-powered.jpg' /></a>
         </div>
         <? } ?>
         <br>
      </div>
        <script src="/bootstrap/js/bootstrap-transition.js"></script>
        <script src="/bootstrap/js/bootstrap-alert.js"></script>
        <script src="/bootstrap/js/bootstrap-modal.js"></script>
        <script src="/bootstrap/js/bootstrap-dropdown.js"></script>
        <script src="/bootstrap/js/bootstrap-scrollspy.js"></script>
        <script src="/bootstrap/js/bootstrap-tab.js"></script>
        <script src="/bootstrap/js/bootstrap-tooltip.js"></script>
        <script src="/bootstrap/js/bootstrap-popover.js"></script>
        <script src="/bootstrap/js/bootstrap-button.js"></script>
        <script src="/bootstrap/js/bootstrap-collapse.js"></script>
        <script src="/bootstrap/js/bootstrap-carousel.js"></script>
        <script src="/bootstrap/js/bootstrap-typeahead.js"></script>
        <script type="text/javascript">
            $('.dropdown-toggle').dropdown();
                
            var login_logout_button = document.getElementById("login_logout");    
            //The following conditional determines whether or not to set the logout button to the Facebook logout or the normal LaundryLocker logout. -->
            <?php if ($facebook_user_id): ?>
                login_logout_button.textContent = "Logout";
                login_logout_button.setAttribute("href", "<?= $facebook_logout_url?>");

            <?php elseif (empty($this->customer)): ?>
                login_logout_button.textContent = "Sign In";
            <?php else: ?>
                login_logout_button.textContent = "Logout";
                login_logout_button.setAttribute("href", "/logout");
            <?php endif; ?>
    	</script>
   </body>
   
</html>

