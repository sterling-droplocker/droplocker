<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig">
                <h1>Now Hiring:</h1><br>
                <div class="visible-phone" style="text-align: center">
                    <?= aboutNav('jobs'); ?>
                </div>
                <img src="/images/hiring.jpg" alt="Now Hiring" width="100" height="130" border="0" align="right">
				<img src="/images/hiring.jpg" alt="Now Hiring" width="100" height="130" border="0" align="right">
				At Laundry Locker, we are changing the way the world does Dry Cleaning & Laundry! Our locker & software solution helps solve the issue of inconvenient hours and locations of traditional dry cleaners. Our network of over 750+ locations makes it convenient for our customers to use us 24 Hours a day, 365 days a year.  We use patented technology and industry leading software to track our customers orders, take photos of items, and even text them when it's ready for pickup.  Forbes even called us "The Most Technologically Advanced Dry Cleaner in America".
				<br>
				<br>
				We are looking for entrepreneurial people to join our team and focus on delivering value for our customers & partners. We deliver results, but we operate as a lean and collaborative startup. We're creative, dedicated people who are customers first, making collaboration and improvement a top priority.
				<br>
				<br>
				<br>
				<a href="https://laundry-locker-inc.workable.com/jobs/386152" target="_blank"><h2>Account Executive - Luxer One</h2></a>
				<br/>
				<br/>
				<a href="https://laundry-locker-inc.workable.com/jobs/390016" target="_blank"><h2>Delivery Driver</h2></a>
				<br/>
				<br/>
				<a href="https://laundry-locker-inc.workable.com/jobs/355946" target="_blank"><h2>Key Accounts Manger - Laundry Locker</h2></a>
                <br><br>
                <br><br>
                <div align="center"><h1>Change the way the world does laundry!</h1></div>
                <br>
                <br>
            </div>
        </div>
        <div class="span4">
            <div class="hidden-phone">
                <?= aboutNav('jobs'); ?>
            </div>
        </div>
    </div>
</div>