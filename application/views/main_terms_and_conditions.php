<!doctype html>
<html class="h-100"> 
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= get_translation_for_view("title", "Terms and conditions") ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=1024">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="/css/terms_and_conditions.css">
    <link rel="stylesheet" type="text/css" href="/css/<?= $business_subpath ?>/terms_and_conditions.css">

    <?= $_scripts ?>
    <?= $javascript?>
    <?= $style?>
    <?= $_styles ?>

</head>


<body class="h-100 main_terms_and_conditions">
<div class="page-wrapper h-100 px-3">
    <div class="row header">
        <div class="col text-center">
            <a href="http://<?= $business->website_url ?>/">
                <div class="logo">
                    <img src="<?= $business->image ?>" />
                </div>
            </a>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col">
                <?= $business->terms ?>
            </div>
        </div>
    </div>
</div>

<script>
</script>


</body>
</html>
