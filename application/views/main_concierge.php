<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/howitworks/hero-concierge.jpg" class="howto_hero_pic" />
            <h1>Concierge</h1>
             <div class="visible-phone" style="text-align: center">
                <?= howNav('concierge'); ?>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-concierge-pickup.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">1. Leave items with concierge</h2>
                        <p class="bodyBig concierge_p">
                            Place your dry cleaning or wash & fold items in a bag and leave it with your concierge.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-order.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">2. Place order</h2>
                        <p class="bodyBig concierge_p">
                            Place your order <a href="/register" class="green" target="_blank">via the web</a>, phone,
                            <a href="https://play.google.com/store/apps/details?id=com.laundrylocker.laundrylocker.activities.laundrylocker" class="green" target="_blank">Android</a>
                            or <a href="http://laundrylocker.loc/images/iphoneApp.png" class="green" target="_blank">iOS app</a>
                            <br /> <br />
                            Select your address, unit number, and service you are requesting (dry cleaning/wash & fold).
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-concierge-pickup.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">3. PickUp</h2>
                        <p class="bodyBig concierge_p">
                            When your laundry is ready to be picked up, we will send you a notification via the app, SMS, or email. Visit your concierge desk and retrieve your clean order.
                        </p>
                    </div>
                </div>
            </div>


        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= howNav('concierge'); ?>
            </div>
            <div class="calloutBanner calloutRight" style="height: auto; margin-top: 20px">
                <h3>First time use?</h3>
                <p class="bodyBigGray">Use any bag to drop off. Please write your Name, Phone, and email inside with type of service you are requesting.</p>
                <br />
                <br />
                <h3>Regular users</h3>
                <p class="bodyBigGray">Please use your reusable garment bag.</p>
            </div>
        </div>
    </div>
</div>