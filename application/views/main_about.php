<div class="fluid-container">
<div class="row-fluid">
    <div class="span8 bodyBig">

        <h1>About Us</h1><br>
        <div class="visible-phone" style="text-align: center">
            <?= aboutNav(); ?>
        </div>
        Launched in 2005, Laundry Locker&reg; was built with the explicit goal of providing customers with the most convenient, easy and high quality way do their laundry and dry cleaning.<br><br>
        With a patent pending on our locker-based delivery solution, Laundry Locker has eliminated the two biggest problems plaguing service industries such as dry cleaning, laundry and shoe repair - inconvenient locations and inconvenient hours.<br><br>
        With our custom designed lockers and by focusing on highly efficient operations, leading edge technology, and strong partnerships, we are providing customers with an unmatched level of service, quality, and convenience.<br><br>
        Our mission is straightforward: <br><br>

        <div align="center"><h1>Change the way the world does laundry!</h1></div>
        <br>
        <br>



    </div>
    <div class="span4">
        <div class="hidden-phone">
            <?= aboutNav(); ?>
        </div>
    </div>
</div>
</div>