<html>

    SMS Body: <textarea name="Body"></textarea><Br />
    Customer Mobile Number:<input type="text" name="From"><br />
    Business: <select id="businesses"><option>Choose Company</option></select>
    <input type="text" name="To" readonly /><br />
    <button id="run_test">test sms</button>
    <div style="margin-top:20px;border:solid 1px #ccc;" id="sms_response"></div>
</html>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript">
    var businesses = <?= json_encode( $businesses );?>;
    
    var getTestLink = '/account/sms/nexmo_claim';
    $('#run_test').click(function(){
        var body_val = $('textarea[name=Body]').val() || '';
        var from_val = $('input[name=From]').val() || '';
        var to_val = $('input[name=To]').val() || '';
        var validated = true;
        var msg = '';
        
        if( body_val == ''){
            validated = false;
            msg += 'missing body ';
        }
        
        if(from_val == '' ){
            validated = false;
            msg += 'missing customer number ';
        }
        
        if( to_val == '' ){
            validated = false;
            msg += 'missing company value ';
        }
             
        if(validated){
            $.get(getTestLink,
                     {
                         Body: body_val,
                         From: from_val,
                         To:   to_val
                     },
                     function(res){
                         $('#sms_response').html(res);
                     }
                 );
        }else{
            alert(msg);
        }
    });
    
    $(function(){
        $.each(businesses,function(business_id, businessSms ){
            $('<option value="' + businessSms.sms + '">' + businessSms.name + '</option>').appendTo('#businesses');
        });
        
        $('#businesses').change(function(e){
            $('input[name=To]').val( $(this).val() );
            console.log( $('input[name=To]').val() );
        });
        
    });
    
    /*
     * 
     * *        /*
         * NEXMO FIELDS
         * 
         * type
         * to
         * msisdn
         * network-code
         * messageId
         * message-timestamp
         * text
         * 
         * (there are other fields for concateneated inbound messages
         */
</script>