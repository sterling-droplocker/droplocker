<html>
    <head>
       <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css" />
       <link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
       <style type="text/css">
           table tr td,
           table tr th{
               font-size:12px;
               background-color:white;
           }
           
       </style>
    </head>
    
    <div class="container">
        <div class="row" style="padding:10px;">
            <div class="span10 well">
                    <h2 class="span8">SMS Flow Test</h2>
                        
                    <div class="span6  alert">
                        <strong>NOTICE:</strong> 
                        Please be advised that testing "NEXMO" will send live response messages to the customer mobile # field.
                    </div>

                    <div class="span2">
                        <table class="table table-bordered table-condensed">
                           <tr>
                               <th>Provider</th>
                               <th>Creates Claim</th>
                               <th>Sends SMS Response</th>
                           </tr>
                           <tr>
                               <td>Twilio</td>
                               <td><span style="color:green;"><strong>Yes</strong></span></td>
                               <td>No</td>
                           </tr>
                           <tr>
                               <td>Nexmo</td>
                               <td><span style="color:green;"><strong>Yes</strong></span></td>
                               <td><span style="color:green;"><strong>Yes</strong></span></td>
                           </tr>
                           <tr>
                               <td>EmailToSMS</td>
                               <td><span style="color:green;"><strong>Yes</strong></span></td>
                               <td>No</td>
                           </tr>
                       </table>
                    </div>
  
                <div class="span6" style="margin-top:10px;">
                    <div class="span2">SMS Body:</div>
                    <div class="span3"><textarea id="smsBody"></textarea></div>
                </div>

                <div class="span6">
                    <div class="span2">Customer Mobile #:</div>
                    <div class="span3"><input type="text" id="smsFrom"></div>
                </div>

                <div class="span6">
                    <div class="span2">Provider:</div>
                    <div class="span3"><select name="provider" id="provider"></select></div>
                </div>
                
                <div class="span6" style="display:none;" id="carriers">
                    <div class="span2">Carrier</div>
                    <div class="span3">
                        <select name="carrier">
                            <option value="">-- SELECT--</option>
                            <option value="31009">Alltel</option>
                            <option value="31002">ATT</option>
                            <option value="31007">Nextel</option>
                            <option value="31005">Sprint</option>
                            <option value="31004">T-Mobile</option>
                            <option value="31003">Verizon</option>
                            <option value="31008">Boost</option>
                            <option value="31011">Boost Unlimited</option>
                            <option value="31010">Virgin</option>
                        </select>
                    </div>
                </div>    
                    
                <div class="span9" id="business_row">
                    <div class="span2">Business:</div>
                    <div class="span6">
                        <select id="businesses"><option>Choose Company</option></select>
                        <input class="input-medium" type="text" id="smsTo" readonly />
                    </div>
                </div>
                <div class="span7">
                    <div class="span3"><button id="run_test" class="btn btn-large btn-primary">Test SMS Flow Response</button></div>
                </div>

                <div class="span9">
                    <div class="span6 alert alert-error" id="error_response" style="display:none;margin-top:10px;"></div>
                </div>
                
            </div>
            
            <div class="span9" id="response_from_server" style="display:none;">
                <h4>Response Text:</h4>
                <div style="margin-top:20px;" id="sms_response"></div>
            </div>
        </div>
    </div>
    
</html>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script type="text/javascript">
    var currentDate = new Date();
    var providerBusinesses = <?= json_encode( $businesses );?>;
    
    var providerInfo = { 
                        twilio:{
                            claim_url:  '/account/sms/twilio_claim',
                            to_field:   'To',
                            from_field: 'From',
                            body_field: 'Body'
                        },
                        shortcode:{
                            claim_url:  '/account/sms/bizzie_claim',
                            to_field:   'To',
                            from_field: 'From',
                            body_field: 'Body'
                        },
                        nexmo:{
                            claim_url:  '/account/sms/nexmo_claim',
                            to_field:   'to',
                            from_field: 'msisdn',
                            body_field: 'text'
                        }
                       };
    
    $('#run_test').click(function(){
        var provider_value = $('select#provider').val();
        if(provider_value == 'EmailToSMS'){
            testEmailToSms();
        }else{
            testClaim();
        }
    });
    
    
    //init
    $(function(){
        populateProviders(function(){
            $('#provider').bind('change', switchProviderBusinesses );
            var init_prov = $('#provider').val();
            populateBusiness( init_prov );
            
            $('#businesses').bind('change', switchBusiness );
        });
    });
    
    var populateProviders = function(callback){
        for(var i in providerBusinesses){
            $('<option value="' + i + '">' + i + '</option>').appendTo('#provider');
        }
        
        if(typeof callback == 'function'){
            callback();
        }
    };
    
    var populateBusiness = function( provider ){
      $('#businesses').html('');
      
      for(var b in providerBusinesses[ provider ] ){
          var pB = providerBusinesses[ provider ][ b ];
          $('<option value="' + pB.sms + '">' + pB.name + '</option>').appendTo('#businesses');
      }
      
      $('#smsTo').val( $('#businesses').val() );
    };
    
    var switchProviderBusinesses = function(event){
        var new_prov = $(this).val();
        
        if(new_prov == 'EmailToSMS'){
            
            //show carriers here
            $('#carriers').show();
            $('#business_row').hide();
            
        }else{
            //hide carriers here
            $('#carriers').hide();
            $('#business_row').show();
            populateBusiness( new_prov );
        }
    };
    
    var switchBusiness = function(event){
       // console.log( $(this).val() )
        $('#smsTo').val( $('#businesses').val() );
    };
    
    var testClaim = function(){
        $('#error_response').hide();
        $('#response_from_server').hide();
        
        var providerSelectValue = $('#provider').val();
        var providerData = providerInfo[ providerSelectValue ];
        var body_val = $('textarea#smsBody').val() || '';
        var from_val = $('input#smsFrom').val() || '';
        var to_val   = $('input#smsTo').val() || '';
        
        var validated = true;
        var msg = '';
        
        if( body_val == ''){
            validated = false;
            msg += 'Missing SMS body. ';
        }
        
        if(from_val == '' ){
            validated = false;
            msg += 'Missing customer number. ';
        }
        
        if( to_val == '' ){
            validated = false;
            msg += 'Missing company SMS number. ';
        }
             
        if(validated){
            var submitData = {};
            submitData[providerData.body_field] = body_val;
            submitData[providerData.from_field] = from_val;
            submitData[providerData.to_field] = to_val;      
                     
            $.get( providerData['claim_url'],
                     submitData,
                     function(res){
                         $('#response_from_server').fadeIn();
                         $('#sms_response').html('<pre>' + res + '</pre>');
                     }
                 );
        }else{
            $('#error_response').html('ERROR: ' + msg).fadeIn();
        }
    };
    
    var testEmailToSms = function(){
        //create carrier dropdown
        //{"id": 18432, "message" : "Locker 07", "date" : "2012-05-01", "carrier" : "ATT", "carrierId" : 31007, "phone" : "19079784496", "operator_name" : "natawski", "operator_id" : 13225}
        
        var testPostData = {
            id:11111,
            message:'',
            date: '',
            carrier:'SMSTEST',
            carrierId:11111,
            operator_name:'SMSTEST'
        };
        
        
        testPostData['message'] = $('#smsBody').val(); 
        testPostData['date'] = '2013-01-01';
        
        testPostData['phone'] = $('#smsFrom').val();
        testPostData['operator_id'] = $('select[name=carrier]').val();
        
        console.log(testPostData);
        
        $('#error_response').hide();
        $('#response_from_server').hide();
        
        $.post('/account/sms/create_claim',{JSON:JSON.stringify(testPostData)},function(res){
            var response = JSON.parse(res);
            $('#response_from_server').fadeIn();
            $('#sms_response').html('<pre>' + response.message + '</pre>');
        });
    };
</script>