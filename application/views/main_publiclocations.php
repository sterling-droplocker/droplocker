<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.9&sensor=false&key=<?=get_default_gmaps_api_key();?>"></script>
<script src="/js/StyledMarker.js" type="text/javascript"></script>
<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8 bodyBig">


            <h1>Locations</h1><br />
            Laundry Locker's current service area is outlined in blue.  All of our locker-based drop off locations are securely located inside our kiosks, office buildings and apartment buildings.
            <br /><br />
            We currently service <strong><?= $locationCount; ?></strong> buildings throughout San Francisco.  If you don't have access to a locker in your apartment or office building, you can use one of our <a href="/main/kiosk">public locations</a> or request a <a href="mailto:<?= $this->business->email ?>">locker in your building</a>.
            <br /><br />
            <span class="bodyReg"><font size="+1"><a href="http://sfbay.craigslist.org/search/apa?query=%22laundry+locker%22&srchType=A&minAsk=&maxAsk=&bedrooms=" target="_blank">View apartments for rent with Laundry Locker on site.</a></font>
                <br /><br />
                <div align="right">Click on pin for location details.</div>
                <div id="map" style="border:1px solid #3698fb;"><br /><br /><br /><br /><br />&nbsp;&nbsp;&nbsp;Loading Google Map...</div></span>
            <br /><br />

        </div>
        <div class="span4">
            <?php
            $locations = json_decode($json_locations);
            echo callOut_kiosk();
            echo callOut_landlord();
            ?>
            <div class="calloutRight"><br /><h2>Laundry Locker Stores:</h2>
                <p style="font-size: 12px; color: #666; margin: 0px 0px 10px 0px;">Open 7 days, 24 Hours</p>
                <table id='locationTablePublic'>
                    <tr>
                        <td></td>
                    </tr>
                </table>

                <h2 style="margin-top: 20px">Laundry Locker Partner Locations:</h2>
                <p style="font-size: 12px; color: #666; margin: 0px 0px 10px 0px;">Click on button for hours</p>
                <table id='locationTable'>
                    <tr>
                        <td></td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var count = 1;
    var latlng = new google.maps.LatLng(37.800000, -122.357221);

    var myOptions = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), myOptions);
    var infowindow = new google.maps.InfoWindow();
    function add_marker(lat, lon, locationID, address, name, count, info, color) {
        latlng = new google.maps.LatLng(lat, lon);
        //bounds.extend(latlng);
        color = '#' + color;

        var styleIcon = new StyledIcon(StyledIconTypes.MARKER, {color: color, text: count});
        var marker = new StyledMarker({styleIcon: styleIcon, position: latlng, map: map, content: count});


        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(info);
            infowindow.open(map, marker);
        });
    }
    ;


    var json = <?= $json_locations ?>;

    $.each(json, function (i, item) {
        var lat = item.lat;
        var lon = item.lon;
        var color = item.abbreviation == 'k' ? '0099ff' : 'A4D423';
        var target = item.abbreviation == 'k' ? '#locationTablePublic' : '#locationTable';

        add_marker(lat, lon, item.locationID, item.address, 'active', count.toString(), item.info, color);
        var link = '';
        if (item.locationID == 483) {
            link = "<a href='/'>";
        } else if (item.locationID == 341) {
            link = '<a href="/main/clay" target="_blank">';
        } else if (item.locationID == 186) {
            link = '<a href="main/onecalifornia" target="_blank">';
        } else {
            link = '<a href="https://maps.google.com/maps?q=' + item.address + ' ' + item.city + ', ' + item.state + '" target="_blank">';
        }

        var city = '';
        if (item.city != 'San Francisco') {
            city = ", " + item.city;
        }
        $(target).append("<tr><td><img src='http://thydzik.com/thydzikGoogleMap/markerlink.php?text=" + count + "&color=" + color + "' /></td><td class='bodyReg'>" + link + "" + item.companyName + "<br />" + item.address + "" + city + "</a></td></tr>");
        count = count + 1;

    });



</script>
