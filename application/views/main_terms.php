    <div style="float: left; display: block; padding-left: 5px; width: 900px; padding-top:15px;">
<h1>Laundry Locker Terms and Conditions</h1>
<br>
<span class="bodyReg">By placing your items in a Laundry Locker locker, you agree to the following terms and conditions:</span>
<br><p></p>
<span class="bodyBold">Minimum Order Size</span><br>
<span class="bodyReg">Wash & Fold order minimum is 12 pounds. No minimum order size for dry cleaning.  $15 Minimum for home delivery.<br><br></span>
<span class="bodyBold">Non-payment</span><br>
<span class="bodyReg">Orders that have not been paid for within 30 days of the pickup date will be considered abandoned and all property will be donated to charity.<br><br></span>
<span class="bodyBold">Unclaimed items</span><br>
<span class="bodyReg">All property once returned to a locker must be removed from the locker within 48 hours.  Property not claimed after 48 hours will be removed from the locker and the lock will be changed.  Customers have up to 30 days to claim their property and return the key to the locker.  Any property not claimed within 30 days of the original pick up date will be donated to charity.  Any property claimed without returning the locker key will be charged a lost key fee of $25.
<br><br>
Items that have been placed in a locker and have not been claimed by any customer will be held for 30 days.  If items are unclaimed after 30 days, all property will be donated to charity.<br><br></span>
<span class="bodyBold">Damaged Property</span><br>
<div class="bodyReg">
Laundry Locker follows the standards and policies set forth by the Fabricare Industry and the International Fabricare Institute. We exercise the utmost care in cleaning and processing garments entrusted to us and use such processes which, in our opinion, are best suited to the nature and conditions of each individual garment. Nevertheless, we cannot assume responsibility for inherent weaknesses or defects in materials which may result in tears or development of small holes in fabric that are not readily apparent prior to processing. We can not guarantee against color loss, color bleeding, and shrinkage; or against damage to weak and tender fabrics; or against damage to anscelary items such as belts, buttons, beads, ties or zipper pulls. Laundry Locker's liability with respect to any damaged items shall not exceed ten (10) times our charge for cleaning that garment regardless of brand or condition.

<p> Any damaged items must be reported via email to <a href="mailto:<?= get_business_meta($business_id, 'customerSupport')?>">support@laundrylocker.com</a> and returned to Laundry Locker for inspection within 5 business days. </p>

</div>
<span class="bodyBold">Lost Items</span><br>
<div class="bodyReg">
Any lost items must be reported via email to <a href="mailto:support@laundrylocker.com"><?= get_business_meta($business_id, 'customerSupport')?></a> within 5 business days. Laundry Locker makes its best reasonable effort to track every item that we process and will review all lost items claims on a case by case basis. Any items determined to have been lost by Laundry Locker will be reimbursed in accordance with the International Fabricare Fair Claims Guide and shall not exceed ten (10) times our charge for cleaning that garment regardless of brand or condition.  Items will be considered lost 30 days after the initial claim is filed.

<p> When leaving items in lockers, please ensure that your locker has been correctly closed and locked. Laundry Locker is not responsible for any loss or damage resulting from a failure to properly lock the locker. </p>

<p> When using our home delivery or concierge service, Laundry Locker is not responsible for your items before they are picked up or dropped back off. It is your responsibility to ensure the safety of your items during that time. </p>

<p> For our package delivery service, Laundry Locker's maximum liability will be $100.  </p>
</div>
<span class="bodyBold">Loose Items</span><br>
<span class="bodyReg">Although, we try as hard as possible track such items, we are not responsible for loose items such as jewelry, watches, cash, detachable buttons, cufflinks, belts, broaches, stings, laces, hoods or loose items on garments, hangers, etc.. We request that customers remove these items and empty pockets prior to leaving items with us as we can not be held responsible for damage to your garments from items left in pockets (Lipstick, Gum, Pens, etc.). <br><br></span>
<span class="bodyBold">Personal Property</span><br>
<span class="bodyReg">Any personal property placed in a Laundry Locker locker that appears to have value will be removed by Laundry Locker and stored for 30 days.  If items are unclaimed after 30 days, all property will be donated to charity.<br><br></span>
<span class="bodyBold">Barcodes</span><br>
<span class="bodyReg">
    Laundry Locker will adhere a permanent barcode to your garments in an inconspicuous location. These barcodes are very important in helping us track your garments so that items are not lost and to ensure you are billed consistently every time. Unfortunately we can not accommodate requests not to adhere barcodes.
</span><br><br>
<span class="bodyBold">Turnaround Time</span><br>
<span class="bodyReg">Service days and turnaround time vary by location.  Laundry Locker will make its best reasonable effort to adhere to our service schedule, however, we do not guarantee turnaround times and assume no responsibility for any damages that may occur due to a delay in service. 
<br><br></span>
<span class="bodyBold">Prices</span><br>
<span class="bodyReg">All prices subject to change without additional notice. The current prices listed at www.laundrylocker.com supersede all previous literature, postings, e-mail or other communication.
<br><br></span>
</div>