<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig">
                <h1>Recent Press</h1><br>
                <div class="visible-phone" style="text-align: center">
                    <?= aboutNav('press'); ?>
                </div>
                <strong>October 20, 2016: The San Francisco Examiner</strong> <br />
                Laundry Locker Named Best Dry Cleaner in San Francisco in the SF Examiner's Readers' Choice Awards
                <a href="http://edition.pagesuite-professional.co.uk//launch.aspx?pbid=e1dc6894-339a-4047-8a7c-291b944bf66f" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

				<strong> October 22, 2015: The San Francisco Examiner</strong> <br />
				Laundry Locker Named Best Dry Cleaner in San Francisco in the SF Examiner's Readers' Choice Awards <br />
				<a href="http://edition.pagesuite-professional.co.uk//launch.aspx?pbid=e1dc6894-339a-4047-8a7c-291b944bf66f" target="_blank" class="greenLink"> view article </a>
				<br /> <br />

                <strong> May 6, 2015: PR Newswire</strong> <br />
                New Mother's Day Gift Idea: Lavish Mom with First Class Laundry Service <br />
                <a href="http://www.prnewswire.com/news-releases/new-mothers-day-gift-idea-lavish-mom-with-first-class-laundry-service-300078366.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> March 21, 2015: The San Francisco Business Times</strong> <br />
                40 Under 40 2015: Laundry Locker CEO presses forth with business vision (Video) <br />
                <a href="http://www.bizjournals.com/sanfrancisco/print-edition/2015/03/20/40-under-40-nicholas-sanderson-laundry-locker.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> January 20, 2015: San Francisco Business Times</strong> <br />
                Day in the life: Laundry Locker delivery <br />
                <a href="http://www.bizjournals.com/sanfrancisco/blog/2015/01/laundry-locker-delivery-photos-erick-perez.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> January 16, 2015: San Francisco Business Times</strong> <br />
                There’s an app for that: The new concierge economy promises to transform urban living for the masses <br />
                <a href="http://www.bizjournals.com/sanfrancisco/print-edition/2015/01/16/tech-firms-concierge-services-mobile-apps.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> May 30, 2013: SF Weekly</strong> <br />
                Laundry Locker is named San Francisco's Best Dry Cleaner for the 4th year in a row<br />
                <a href="http://www.sfweekly.com/bestof/2013/award/readers-poll-winners-3478536/" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> February, 2013: Green Earth Cleaning </strong> <br />
                For most people, the word "locker" brings back memories of high school but for Arik Levy, it had a different meaning all together <br />
                <a href="http://www.greenearthcleaning.com/?page=LaundryLocker" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> June 19, 2012: Fox News </strong> <br />
                Revolutionizing how you get your laundry done <br />
                <a href="http://video.foxnews.com/v/1697421215001/revolutionizing-how-you-get-your-laundry-done" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> June 15, 2012: Huffington Post </strong> <br />
                Dashlocker Fomed as Banker Turned Entrepreneur Tries To Shake Up Dry-Cleaning World <br />
                <a href="http://www.huffingtonpost.com/2012/06/15/dashlocker-dry-cleaning_n_1599767.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong> June 5, 2012 PRLog </strong> <br />
                NYC's Dry Cleaning, Wash & Fold and Shoe Shine Go 2.0: DashLocker Launches First 24/7 Location <br />
                <a href="http://www.prlog.org/11892525-nycs-dry-cleaning-wash-fold-and-shoe-shine-go-20-dashlocker-launches-first-247-location.html" target="_blank" class="greenLink"> view article </a>
                <br /> <br />

                <strong>April 25, 2012: iMedia Connection</strong> <br>
                How Siri is going to completely change your job<br>
                <a href="http://www.imediaconnection.com/article_full.aspx?id=31603" target="_blank" class="greenLink">view article</a>
                <br /> <br />

                <strong>April 17, 2012: Women 2.0</strong> <br>
                5 Ways to Achieve Work Life Balance<br>
                <a href="http://www.women2.org/5-ways-to-achieve-work-life-balance-hint-outsource-errands/" target="_blank" class="greenLink">view article</a>
                <br /> <br />

                <strong>December 7th, 2011: Broke-Ass Stuart's</strong> <br>
                Laundry Locker: Affordable, Green Laundry Service<br>
                <a href="http://brokeassstuart.com/blog/2011/12/07/laundry-locker-affordable-green-laundry-service/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>October 20, 2011: Bentley Observer</strong> <br>
                A Fresh Idea: Arik Levy '96<br>
                <a href="http://observer.bentley.edu/issue/fall-2011/alumni-update/fresh-idea-arik-levy-96" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>July 5, 2011: Mosio</strong> <br>
                Expert Q&A: Text Messaging in Dry Cleaning and Laundry Delivery Services (Arik Levy, Laundry Locker)<br>
                <a href="http://www.mosio.com/mobileanswers/tag/arik-levy/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>June 12, 2011: Chase</strong> <br>
                Laundry Locker's CEO, Arik Levy, shares his strategies for success<br>
                <a href="http://www.inkfromchase.com/grow-your-business/strategies-for-success/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>May 18, 2011: SF Weekly</strong> <br>
                Laundry Locker Wins 2011 Best Dry Cleaner<br>
                <a href="http://www.sfweekly.com/bestof/2011/award/readers-poll-winners-2489590/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>December 14, 2010: USPTO</strong> <br>
                US Patent Issued to Laundry Locker on Nov. 30 for "Handling Household Tasks" (California Inventor)<br>
                <a href="http://www.highbeam.com/doc/1P3-2211688611.html target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>September 15, 2010: Forbes</strong> <br>
                What Google Has In Common With A Laundry Service<br>
                <a href="http://blogs.forbes.com/kymmcnicholas/2010/09/15/what-google-has-in-common-with-a-laundry-service/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>September 9, 2010: 7x7 Magazine</strong> <br>
                Bay Area Power Couples: Arik & Holly Levy of Laundry Locker<br>
                <a href="http://www.7x7.com/love-sex/bay-area-power-couples-arik-holly-levy-laundry-locker" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>May 20, 2010: SF Weekly</strong> <br>
                Laundry Locker Wins 2010 Best Dry Cleaner<br>
                <a href="http://www.sfweekly.com/bestof/2010/award/readers-poll-winners-1983734/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>December 30, 2008: Springwise</strong> <br>
                Laundry Locker is ranked #6 on this year's top 10 life hacks<br>
                <a href="http://springwise.com/life_hacks/2008_this_years_top_10_life_ha/" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>August 18, 2008: Daily Candy</strong> <br>
                Laundry Locker featured in Daily Candy<br>
                <a href="http://www.dailycandy.com/san_francisco/article/38286/Spin+Doctors" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>Septemper 12, 2007: KRON 4 News</strong> <br>
                Laundry Locker featured on the KRON 4 news technology segment with Gabriel Slat<br>
                <a href="http://www.youtube.com/watch?v=2-qChq7VSSs" target="_blank" class="greenLink">view video</a>
                <br><br>
                <strong>August 1, 2007: Nation's First Unattended 24 x 7 Dry Cleaning and Wash & Fold Facility</strong> <br>
                Overworked San Fracisco gets relief from pains of laundry<br>
                <a href="press/08012007.pdf" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>May 1, 2006: San Francisco Apartment Association Magazine</strong><br>
                A Fresh Look at Laundry Delivery<br>
                <a href="http://www.sfaa.org/0605landes.html" target="_blank" class="greenLink">view article</a>
                <br><br>
                <strong>June 1, 2005: Laundry Locker: Open for Business</strong><br>
                Service will offer San Francisco ultra convenient, 24 x 7, next-day dry cleaning and wash & fold <br>
                <a href="press/06012005.pdf" target="_blank" class="greenLink">view article</a>
            </div>
        </div>
        <div class="span4">
            <div class="hidden-phone">
                <?= aboutNav('press'); ?>
            </div>
        </div>
    </div>
</div>