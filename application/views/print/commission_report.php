<div align="right" style="width: 800">
<img src="<?= $invoice_logo_image ?>" alt="" border="0"><br>
<font size="-1"><?= $business->address ?>
    <br>
    <?= !empty($business->address2) ? $business->address2.'<br>' : '' ?>
    <?= $business->city ?>, <?= $business->state ?> <?= $business->zipcode ?><br>
    <?= $business->phone ?><br>
</font>
</div><br>
<br>
<?= get_translation_for_view("subject", "Dear Management,") ?> <br>
<br>
<p> <?= get_translation_for_view("body",
	"Enclosed is the commission check for %quarter% for <strong> %address% </strong>.",
	array("quarter" => $qtr, "address" => $location[0]->address)) ?> </p>
<br>
<?= get_translation_for_view("closing", "Thank you for your support,")?><br><br>
<img src="<?= $signature_image ?>" alt=""><br>
<?= $business->companyName ?><br>
<br>
<hr>
<br>
<table border="0" class="normal">
    <tr>
        <td valign="top"><?=get_translation_for_view("commission_field", "Commission:")?></td>
        <td valign="top"><?= $location[0]->commission ?></td>
    </tr>
    <tr>
        <td valign="top"><?= get_translation_for_view("commission_address_field", "Commission Address:")?></td>
        <td valign="top"><?= $location[0]->commissionAddr ?></td>
    </tr>
    <tr>
        <td valign="top"><?= get_translation_for_view("customers_to_date_field", "Customers to Date:") ?></td>
        <td valign="top"><?= $numCust[0]->numCust ?></td>
    </tr>
    <tr>
        <td valign="top"><?= get_translation_for_view("orders_to_date_field", "Orders to Date:") ?></td>
        <td valign="top"><?= $numOrd[0]->numOrd ?></td>
    </tr>
    <?php if($utilization): ?>
        <tr>
            <td valign="top"><?= get_translation_for_view("utilization_field", "Utilization:") ?></td>
            <td valign="top"><?= $utilization ?>%</td>
        </tr>
    <?php endif; ?>
</table>
<br>
<table border="1" class="normal">
    <tr>
        <td></td>
        <?php for ($i = 1; $i <= $numMonths; $i++): ?>
            <td width="75" align="right">
                <?php $selectMonth = mktime(0, 0, 0, date("m")-$i, 1,  date("Y")) ?>
                <?= date("M",$selectMonth) ?>
            </td>
        <?php endfor; ?>
    </tr>
    <tr>
        <td> <?= get_translation_for_view("total_customers_field", "# of customers who have placed orders") ?></td>
        <?php for ($i = 1; $i <= $numMonths; $i++): ?>
            <td width="75" align="right">
                <?= $totCust[$i] ?>
            </td>
        <?php endfor; ?>
    </tr>
    <tr>
        <td> <?= get_translation_for_view("total_closed_orders_field", "# of closed orders") ?> </td>
        <?php for ($i = 1; $i <= $numMonths; $i++): ?>
                <td width="75" align="right">
                    <?= $totOrd[$i] ?>
                </td>
        <?php endfor; ?>
    </tr>
    <?php if ($commissionType == "percentage"): ?>
    <tr>
        <td> <?= get_translation_for_view("total_amount_field", "Gross Revenue") ?></td>
        <?php for ($i = 1; $i <= $numMonths; $i++): ?>
            <td width="75" align="right">
                <?= format_money_symbol($business->businessID, '%.2n', $totAmt[$i]) ?>
            </td>
        <?php endfor; ?>
    </tr>
    <tr>
        <td> <?= get_translation_for_view("total_discounts_field", "Discounts") ?> </td>
        <?php for ($i = 1; $i <= $numMonths; $i++): ?>
            <td width="75" align="right">
                <?= format_money_symbol($business->businessID, '%.2n',  $totDisc[$i]) ?>
            </td>
        <?php endfor; ?>
    </tr>
    <tr>
        <td> <?= get_translation_for_view("total_revenue_field", "Total Revenue") ?> </td>
        <?php for ($i = 1; $i <= $numMonths; $i++): ?>
                <td width="75" align="right">
                        <?= format_money_symbol($business->businessID, '%.2n', $totAmt[$i] + $totDisc[$i]) ?>
                </td>
        <?php endfor; ?>
    </tr>
    <?php endif; ?>
    <tr>
        <td><?= get_translation_for_view("commission_due_field", "Commission Due") ?></td>
        <td align="right" colspan="3">
            <strong><?= format_money_symbol($business->businessID, '%.2n', $commissionDue) ?></strong>
        </td>
    </tr>
</table>
<?php if($allOrders):?>
    <br>
    <h3> <?= get_translation_for_view("all_orders_in_period_title", "Orders during this period:") ?> </h3>
    <table border="1" class="normal">
        <tr>
            <td> <?= get_translation_for_view("customer_field", "Customer") ?> </td>
            <td> <?= get_translation_for_view("order_field", "Order") ?> </td>
            <td> <?= get_translation_for_view("date_picked_up_field", "Date Picked Up") ?> </td>
            <td> <?= get_translation_for_view("date_completed_field", "Date Completed") ?> </td>
            <td> <?= get_translation_for_view("order_gross_field", "Order Gross") ?> </td>
            <td> <?= get_translation_for_view("discounts_field", "Discounts") ?> </td>
        </tr>
        <?php foreach($allOrders as $a): ?>
        <tr>
            <td> <?= getPersonName($a) ?> </td>
            <td> <a href="/admin/orders/details/<?= $a->orderID?>" target="_blank"><?= $a->orderID?></a> </td>
            <td> <?= date("m/d/Y", strtotime($a->statusDate)) ?> </td>
            <td> <?= date("m/d/Y", strtotime($a->dateCreated)) ?> </td>
            <td> <?= format_money_symbol($business->businessID, '%.2n', $totals[$a->orderID]['gross']) ?> </td>
            <td> <?= format_money_symbol($business->businessID, '%.2n', $totals[$a->orderID]['discounts']) ?> </td>
        </tr>
        <?php endforeach; ?>
    </table>

    <br>
    <h3> <?= get_translation_for_view("all_customers_title", "All customers ever:") ?> </h3>
    <table border="1" class="normal">
        <tr>
            <td> <?= get_translation_for_view("customer_field", "Customer") ?> </td>
            <td> <?= get_translation_for_view("days_since_last_order_field", "Days since last order")?> </td>
            <td> <?= get_translation_for_view("orders_placed_field", "Orders Placed") ?> </td>
            <td> <?= get_translation_for_view("total_spent_field", "$ Spent") ?> </td>
        </tr>
        <?php foreach($allCust as $a):?>
        <tr>
            <td> <?= getPersonName($a) ?> </td>

            <?php
                $date = mktime(0,0,0,date("m"),date("d"),date("Y")); //Gets Unix timestamp for current date
                $timestamp = strtotime($a->lastOrder);
                $date2 = mktime(0,0,0,date("m", $timestamp),date("d", $timestamp),date("Y", $timestamp)); //Gets Unix timestamp for current date
                $difference = $date-$date2; //Calcuates Difference
                $days2 = floor($difference /60/60/24); //Calculates Days Old
            ?>
            <td> <?= $days2 ?> </td>
            <td><?= $a->numOrd ?></td>
            <td align="right"><?= format_money_symbol($business->businessID, '%.2n', $a->ordAmt) ?></td>
        </tr>
        <?endforeach; ?>
    </table>
<?endif; ?>
</div>
