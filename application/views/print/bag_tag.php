<?php

$full_base = get_full_base_url();

?>
<!DOCTYPE html>

<html>
    
<head>
    <title> Print Bag Tag</title>
    <script type="text/javascript" src="/js/jquery-1.6.4.min.js"> </script>
    <script type="text/javascript" src="/js/jquery.cookie.js"> </script>
    <script type="text/javascript">

    $.cookie("from_print_bag_tag", "true", { path: "/"});    

     </script>
</head>

<body>
    
<link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
<div style="padding-left: 10px;width: 250px; overflow-wrap: break-word;" class="adminReg">
    <table cellspacing="4" cellpadding="4">
    <tr>
        <td align="center">
            <span style="font-size: 20px;"><br><?= substr($customer,0,22) ?>
                <? if($notes[0]->notes) { echo ' - '.$notes[0]->notes; } ?>
            </span>
        </td>
    </tr>
    <? if(!empty($default_location)){ ?>
    <tr>
        <td align="center">
            <span style="font-size: 20px;"><br>
                <?= get_translation("default_location", "print/bag_tag", array(), "Loc", $customerId) ?> : <?= $default_location ?>
            </span>
        </td>
    </tr>
    <? }; ?>
    <tr>
        <td align="center"><img src="<?= $full_base ?>/barcode.php?barcode=<?= $bag ?>&width=220&height=50"><br></div>
        </td>
    </tr>
    <tr>
        <td align="center">
            <span style="font-size: 30px; font-weight: bold;">
                <?= get_translation("message", "print/bag_tag", array(), "FREE VIP BAG", $customerId) ?>
            </span><br>
            <span style="font-size: 18px;">
                <?= get_translation("instructions", "print/bag_tag", array(), "use with every order", $customerId) ?>
                <? if ($this->business_id == 3): ?>[<?= substr($phone, -4) ?>]<? endif; ?>
            </span>
        </td>
    </tr>
    </table>
</div>

</body>
</html>
