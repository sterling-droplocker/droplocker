<?php 
enter_translation_domain("admin/orders/wf_print"); 
header('Content-Type: text/html; charset=utf-8');


$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");

?>
<!DOCTYPE html>
<? if($location->route_id == 6)
    {
    echo '<div style="FLOAT: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
        <br>
        <div align="center"><strong><font size="+2">'.__("need_by_1pm_next_day", "NEED BY 1PM NEXT DAY").'</font></strong><br><br></div>
        </div>';
    }
?>
<div style="clear: both; float: left; page-break-after:always; WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">
    <br>
    <div align="center">
        <strong><font size="+2"><?= $wf_traveller_text;?></font></strong><br><br>
    </div>
    <table width="100%" border="1" cellspacing="2" cellpadding="2" style="font-size: 10pt; font-family: Verdana;">
        <tbody>
            <tr>
                <td><font size="+1"><strong><?=__("bag_number", "Bag #"); ?></strong></font></td><td align="right"><strong><font size="+1"><?= $bag->bagNumber ?></font></strong></td>
            </tr>

            <tr>
                <td><?=__("date", "Date"); ?></td><td><?= convert_from_gmt_aprax(date("m/d/Y"), $dateFormat, $this->business_id) ?> [<?= substr(trim($customer->phone),-4) ?>]</td>
            </tr>
            <tr>
                <td><?=__("customer", "Customer"); ?></td><td><?= getPersonName($customer) ?></td>
            </tr>
            <tr>
                <td><?= weight_system_format( '', false, true );?></td><td><?= $wfPref[0]->qty ?></td>
            </tr>
            <?php

            $extra_notes = '';
            $wfPref = trim(preg_replace("#<br[ /]*>#i", "\n", $wfPref[0]->notes));
            $wfLines = explode("\n", $wfPref);
            ?>
            <? foreach($wfLines as $line):
                $cells = explode(":", $line, 2);
                if (count($cells) < 2) {
                    $extra_notes .= $line . "<br>";
                    continue;
                }
            ?>
            <tr>
                <td><?= __($cells[0], $cells[0]) ?></td>
                <td><?= $cells[1] ?></td>
            </tr>
            <? endforeach; ?>
            </tr>
            <? if (!empty($extra_notes)): ?>
            <tr>
                <td colspan="2" valign="top" align="center">
                    <strong><?= $extra_notes ?></strong>
                </td>
            </tr>
            <? endif; ?>
            <tr>
                <td colspan="2" valign="top">
                    <?=__("notes", "Notes:"); ?><br><strong><?= nl2br(trim($order->notes)) ?></strong>
                </td>
            </tr>
            <? if (!empty($otherItems)): ?>
            <tr>
                    <td valign="top"><?=__("other_items", "Other Items:"); ?></td>
                    <td>
                        <strong>
                        <? foreach($otherItems as $otherItem): ?>
                            <?= $otherItem->qty ?> - <?= $otherItem->name ?><br>
                        <? endforeach; ?>
                        </strong>
                    </td>
            </tr>
            <? endif; ?>
            <tr>
                <td><font size="+1"><strong><?=__("order_number", "Order&nbsp;#"); ?></strong></font></td><td align="right"><strong><font size="+1"><?= $order->orderID ?></font></strong></td>
            </tr>
        </tbody>
    </table>
    <table>
        <tr>
                <td>W&nbsp;________</td>
                <td>D&nbsp;________</td>
                <td>F&nbsp;________</td>
        </tr>
    </table>
</div>
