<?php 
enter_translation_domain("admin/orders/details");
$dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
?>

<? if($this->uri->segment(5) == 'captureOption'): ?>
<form action="" method="post"><input type="submit" name="changeToCaptured" value="Change To Captured"></form>
<?= $this->session->flashdata("status_message"); ?>
<? endif; ?>

<html>
<head>
    <meta charset="utf-8">
    <title>Order Receipt</title>
    <link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
    <style>
            body{
                    text-align:center;
                    background-color:#ccc;
                    padding:5px;
            }
            .number_field {
                text-align: right;
            }
            #page{
                    margin:0 auto;
                    width:800px;
                    border:1px solid #666;
                    border-top:none;
                    background-color:#fff;
                    text-align:left;
                    padding:0px;

            }
            #header{
                    margin:0 auto;
                    width:800px;
                    border:1px solid #666;
                    border-bottom:none;
                    background-color:#6699cc;
                    min-height:60px;
                    padding:0px;
            }
    </style>
</head>
	<body>
	<div id='header'>
                <?php if (isset($detailedReceipt_logo_path)): ?>
                    <img src="<?= $detailedReceipt_logo_path ?>" style="float: left; " />
                <?php endif; ?>
		<div style='float:right;padding:5px;'>
			<small style="text-align: right; color: #fff; font-size: 10px;">
			<div><?= $this->business->companyName?></div>
			<div><?= $this->business->address?> <?= $this->business->address2?></div>
			<div><?= $this->business->city?>, <?= $this->business->state?> <?= $this->business->zipcode?></div>
			<div><?= $this->business->phone?></div>
			<div><?= $this->business->email?></div>
			</small>
		</div>
		<div style='clear:both'></div>
	</div>
	<div id='page'>
	<div style='padding:5px;'>
	<h2>Detailed Receipt</h2>

	<table class='receipt_table'>
		<tr>
			<th>Customer</th>
                        <td><a href='/admin/customers/detail/<?= $customer->customerID?>' target="_blank"><?= getPersonName($customer) ?></a></td>
		</tr>
		<tr>
			<th>Order Date</th>
			<td>
                            <?= convert_from_gmt_aprax($order->dateCreated, $dateFormat, $this->business_id)?>

                        </td>
		</tr>
		<tr>
			<th>Location</th>
			<td><?= $location->address?></td>
		</tr>
		<tr>
			<th>Locker</th>
			<td><?= $locker->lockerName?></td>
		</tr>
		<tr>
			<th>Bag</th>
			<td><?= $bag->bagNumber?></td>
		</tr>
		<tr>
			<th>Order</th>
			<td><?= $order->orderID?></td>
		</tr>
		<tr>
			<th valign='top'>Ticket</th>
			<td><?= $order->ticketNum?></td>
		</tr>
		<tr>
			<th valign='top'>Assembly Notes</th>
			<td><?= $order->postAssembly?></td>
		</tr>
		<tr>
			<th valign='top'>Internal Notes</th>
			<td><?= $order->llNotes?></td>
		</tr>
		<tr>
			<th valign='top'>Notes</th>
			<td><?= $order->notes?></td>
		</tr>
	</table>
	</div>
<br />
<div style='padding:5px;'>
<h2>Order History</h2>
<table id='box-table-a'>
	<thead>
		<tr>
			<th>Status</th>
			<th>Date</th>
			<th>Employee</th>
			<th>Locker</th>
		</tr>

	</thead>
	<? if($status):foreach($status as $s):?>
	<tr>
		<td><?= $s['name']?></td>
		<td><?= convert_from_gmt_aprax($s['date'], $dateFormat, $this->business_id)?></td>
		<td><?= $s['customer']?></td>
		<td><?= $s['locker']?></td>
	</tr>
	<? endforeach; else:?>
	<tr>
		<td colspan="3">No Status History</td>
	</tr>
	<? endif;?>
</table>
</div>
<br />
<div style='padding:5px;'>
<h2>Order Items</h2>
<table id='box-table-a'>
	<thead>
		<tr>
			<th>Item</th>
			<th class="number_field">Price</th>
			<th class="number_field">Qty</th>
			<th class="number_field">Total</th>
			<th>Inventoried</th>
			<th>Barcode</th>
			<th>Location</th>
		</tr>
	</thead>
	<tbody>

	<? foreach($items as $item): ?>
		<tr>
			<td><?= $item->displayName?><?= ($item->notes!='')?"<div style='color:#000;padding-top:2px;'>".$item->notes."</div>":'';?></td>
			<td class="number_field"><?= format_money_symbol($this->business->businessID, '%.2n', $item->unitPrice)?></td>
			<td class="number_field"><?= $item->qty?></td>
			<td class="number_field"><?= format_money_symbol($this->business->businessID, '%.2n', $item->qty * $item->unitPrice);?></td>
			<td><?= convert_from_gmt_aprax($item->updated, $dateFormat, $this->business_id)?></td>
			<td><a href="/admin/orders/edit_item?orderID=<?= $order->orderID?>&itemID=<?= $item->item_id?>" target="_blank"><?= $item->barcode ?></a></td>
			<td><?= $item->supplier ?></td>
		</tr>
	<?endforeach; ?>

	<? foreach($discounts as $d): ?>
		<tr>
			<td colspan="3"><?= $d->chargeType?></td>
			<td class="number_field"><?= format_money_symbol($this->business->businessID, '%.2n', $d->chargeAmount) ?></td>
			<td colspan="3">&nbsp;</td>
		</tr>
	<?endforeach; ?>

    <? if (!$breakout_vat): ?>
        <tr>
            <td colspan="3"><?= __("subtotal","Subtotal")?></td>
            <td class="number_field"><?= format_money_symbol($this->business_id, '%.2n', $subtotal) ?> </td>
        	<td colspan="3">&nbsp;</td>
        </tr>
        <? foreach ($order_taxes as $tax): ?>
        <tr>
            <td colspan="3"><?= count($order_taxes) > 1 && $tax->name ? sprintf('%s (%s%%)', $tax->name, number_format_locale($tax->taxRate * 100, 2)) : __("tax_field", "Tax") ?> </td>
            <td class="number_field"><?= format_money_symbol($this->business_id, '%.2n', $tax->amount) ?></td>
        	<td colspan="3">&nbsp;</td>
        </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="3"><?= __("net_total","Net Total")?></td>
            <td class="number_field"><?= format_money_symbol($this->business_id, '%.2n',$net_total) ?></td>
        	<td colspan="3">&nbsp;</td>
        </tr>
    <? else: ?>
        <tr>
            <td colspan="3"><?= __("net_total","Net Total")?></td>
            <td class="number_field"><?= format_money_symbol($this->business_id, '%.2n', $net_total) ?></td>
        	<td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><?= __("subtotal","Subtotal")?></td>
            <td class="number_field"><?= format_money_symbol($this->business_id, '%.2n', $subtotal) ?> </td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <? foreach ($order_taxes as $tax): ?>
        <tr>
            <td colspan="3"><?= count($order_taxes) > 1 && $tax->name ? sprintf('%s (%s%%)', $tax->name, number_format_locale($tax->taxRate * 100, 2)) : __("tax_field", "Tax") ?> </td>
            <td class="number_field"><?= format_money_symbol($this->business_id, '%.2n', $tax->amount) ?></td>
        	<td colspan="3">&nbsp;</td>
        </tr>
        <? endforeach; ?>
    <? endif; ?>
	</tbody>
</table>
</div>
<br>
<table width='100%'>

	<tr>
		<td><a href="/admin/orders/details/<?= $order_id?>" target="_blank">Edit Order</a></td>
		<td><a href='/admin/reports/metalprogetti_history/<?= $order_id?>' target="_blank">Metalprojetti History</a></td>
	</tr>
</table>

</div>
</body>
</html>

