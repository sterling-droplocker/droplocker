<?php 
$per_page = (int)$this->input->get("per_page");
if ($per_page == 0) $per_page = 1;
?>
<style type="text/css">
p {
    font-size: .9em;
    font-family: arial;
    border-bottom:  1px solid #eee;
}

p .label {
    display: inline-block;
    width: 80px;
}

p .data {
    width: 140px;
    font-weight: bold;
}
</style>
<? 
for ( $i = $start; $i <= $end; $i++): 
    $break = "";
    if ($i%$per_page==0) $break = "page-break-after:always;";
?>

<div style="clear:both; <?=$break;?> WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;margin-top: 0">
    <p> <span class="label">Order</span>  <span class="data"><b style="font-size:2em;"><?= $orderID ?></b></span></p>
    <p> <span class="label">Customer</span>  <span span class="data"><?= $customerFullName ?></span></p>
    <p> <span class="label">Item</span> <span span class="data"><?= $i ?> out of <?= $end ?></span></p>
</div>

<? endfor; ?>