<?php
enter_translation_domain("print/temp_tag");
$full_base = get_full_base_url();

?>
<style type="text/css">
    p {
        font-size: 0.8em;
    }
</style>
<? for ( $i = $lastBag; $i <= $endBag; $i++): ?>

<div style="clear:both; page-break-after:always; width: 200px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0; margin-bottom: 20px">
    
<?php if ((bool) $business->print_temp_tag_header): ?>
    <p> <?=__("temp_tag_header1", "Name:");?>  __________________ </p>
    <p> <?=__("temp_tag_header2", "Email:");?> __________________ </p>
    <p> <?=__("temp_tag_header3", "Phone:");?> __________________ </p>
<?php endif; ?>
<?php if ($include_date != false): ?>
    <div style="text-align: center"><?= date(get_business_meta($business->businessID, "shortDateDisplayFormat"));?></div>
<?php endif; ?>
<img src="<?= $full_base ?>/barcode.php?barcode=0<?= $i ?>&height=55">
</div>

<? endfor; ?>

<?php leave_translation_domain(); ?>
