<div class="fluid-container">
    <div class="row-fluid">
        <div class="span8">
            <div class="bodyBig">
                <h1>Eco Friendly Dry Cleaning</h1>
                <br>
                <div class="visible-phone" style="text-align: center">
                    <?= servicesNav('dc');?>
                </div>
                Laundry Locker provides superb, <a href="green">eco-friendly dry cleaning</a> and laundering for your garments.  You simply toss your items in a locker and they come back clean, pressed and nicely packaged, hanging in the locker waiting for you.
                <br><br>
                We very carefully inspect each item and follow the manufacturers recommended care instructions.  If shirts or pants can be laundered, we will typically launder them, unless you specify otherwise.<br>
                <br>
                If there is a stain you would like us to try to get out, stain stickers are conveniently located inside the lockers for you to clearly identify any stains. If you would like an item handled a special way, you simply place some notes in your profile and we will take care of it.<br>
                <br>
                Damaged buttons and minor repairs will be fixed <strong>free of charge</strong> and if there is any problem with your order, we'll take care of it, quickly and hassle free!
                <br>
                <br>
                Just imagine, no more running home to make it to the dry cleaner before they close.
                <div class="hr" style="margin-top: 30px;"><hr /></div>
            </div>
            <div class="row-fluid">
                <table class="table table-condensed" id="dryCleanTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Regular Pricing</th>
                            <th>Any-time <br />Home Delivery</th>
                            <th>Scheduled On-Demand <br/> Home Delivery</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr colspan="4">
                            <th>Laundered Shirts</th>
                        </tr>
                        <tr>
                            <td>Regular</td>
                            <td class="priceCell">$2.69</td>
                            <td class="priceCell">$2.96</td>
                            <td class="priceCell">$3.23</td>
                        </tr>
                        <tr colspan="4">
                            <td></td>
                        </tr>
                        <tr colspan="4">
                            <th>Dry Cleaning</th>
                        </tr>
                        <tr>
                            <td>Tie, Scarf</td>
                            <td class="priceCell">$4.99</td>
                            <td class="priceCell">$5.49</td>
                            <td class="priceCell">$5.99</td>
                        </tr>
                        <tr>
                            <td>Blouse, Sweater</td>
                            <td class="priceCell">$6.39</td>
                            <td class="priceCell">$7.03</td>
                            <td class="priceCell">$7.67</td>
                        </tr>
                         <tr>
                            <td>Pants, Jeans, Skirt</td>
                            <td class="priceCell">$6.39</td>
                            <td class="priceCell">$7.03</td>
                            <td class="priceCell">$7.67</td>
                        </tr>
                         <tr>
                            <td>Blazer, Jacket</td>
                            <td class="priceCell">$8.99</td>
                            <td class="priceCell">$9.89</td>
                            <td class="priceCell">$10.79</td>
                        </tr>
                        <tr>
                            <td>Coat, Heavy Jacket, Dress</td>
                            <td class="priceCell">$11.69</td>
                            <td class="priceCell">$12.86</td>
                            <td class="priceCell">$14.03</td>
                        </tr>
                        <tr>
                            <td>Belts</td>
                            <td class="priceCell">$.75</td>
                            <td class="priceCell">$.83</td>
                            <td class="priceCell">$.90</td>
                        </tr>
                        <tr colspan="4">
                            <td></td>
                        </tr>
                        <tr colspan="4">
                            <th>Laundered and Pressed</th>
                        </tr>
                        <tr>
                            <td>Sheets</td>
                            <td class="priceCell">$10.99</td>
                            <td class="priceCell">$12.09</td>
                            <td class="priceCell">$13.19</td>
                        </tr>
                        <tr>
                            <td>Pillowcases</td>
                            <td class="priceCell">$4.99</td>
                            <td class="priceCell">$5.49</td>
                            <td class="priceCell">$5.99</td>
                        </tr>
                        <tr>
                            <td>Duvet Covers</td>
                            <td class="priceCell">$29.99</td>
                            <td class="priceCell">$32.99</td>
                            <td class="priceCell">$35.99</td>
                        </tr>
                        <tr colspan="4">
                            <td></td>
                        </tr>
                        <tr colspan="4">
                            <th>Special Items</th>
                        </tr>
                        <tr>
                            <td>Fancy Dresses</td>
                            <td class="priceCell">$27.50+ (call)</td>
                            <td class="priceCell">$27.50+ (call)</td>
                            <td class="priceCell">$30.00+ (call)</td>
                        </tr>
                        <tr>
                            <td>Blankets</td>
                            <td class="priceCell">$14.99</td>
                            <td class="priceCell">$16.49</td>
                            <td class="priceCell">$17.99</td>
                        </tr>
                        <tr>
                            <td>Comforters</td>
                            <td class="priceCell">$29.99</td>
                            <td class="priceCell">$32.99</td>
                            <td class="priceCell">$35.99</td>
                        </tr>
                        <tr>
                            <td>Down Comforters</td>
                            <td class="priceCell">$34.99</td>
                            <td class="priceCell">$38.49</td>
                            <td class="priceCell">$41.99</td>
                        </tr>
                        <tr>
                            <td>Vests</td>
                            <td class="priceCell">$6.39</td>
                            <td class="priceCell">$7.03</td>
                            <td class="priceCell">$7.67</td>
                        </tr>
                        <tr>
                            <td>Duvet Cover (dry clean)</td>
                            <td class="priceCell">$29.99</td>
                            <td class="priceCell">$32.99</td>
                            <td class="priceCell">$35.99</td>
                        </tr>
                        <tr>
                            <td>Handkerchief</td>
                            <td class="priceCell">$2.69</td>
                            <td class="priceCell">$2.96</td>
                            <td class="priceCell">$3.23</td>
                        </tr>
                        <tr>
                            <td>Lab Coat</td>
                            <td class="priceCell">$8.29</td>
                            <td class="priceCell">$9.12</td>
                            <td class="priceCell">$9.95</td>
                        </tr>
                        <tr>
                            <td>Napkin</td>
                            <td class="priceCell">$3.19</td>
                            <td class="priceCell">$3.51</td>
                            <td class="priceCell">$3.83</td>
                        </tr>
                        <tr>
                            <td>Shawl</td>
                            <td class="priceCell">$9.29</td>
                            <td class="priceCell">$10.22</td>
                            <td class="priceCell">$11.15</td>
                        </tr>
                        <tr>
                            <td>Sheet (dry clean)</td>
                            <td class="priceCell">$12.49</td>
                            <td class="priceCell">$13.74</td>
                            <td class="priceCell">$14.99</td>
                        </tr>
                        <tr>
                            <td>Tablecloth</td>
                            <td class="priceCell">$12.99</td>
                            <td class="priceCell">$14.29</td>
                            <td class="priceCell">$15.59</td>
                        </tr>
                        <tr>
                            <td>Tuxedo Shirt</td>
                            <td class="priceCell">$6.39</td>
                            <td class="priceCell">$7.03</td>
                            <td class="priceCell">$7.67</td>
                        </tr>
                        <tr>
                            <td>Polo (laundered short sleeve)</td>
                            <td class="priceCell">$5.69</td>
                            <td class="priceCell">$6.26</td>
                            <td class="priceCell">$6.83</td>
                        </tr>
                        <tr colspan="4">
                            <td></td>
                        </tr>
                        <tr colspan="4">
                            <th>Special Handling Fees</th>
                        </tr>
                        <tr>
                            <td>Boxed Shirts</td>
                            <td class="priceCell">$.75</td>
                            <td class="priceCell">$.83</td>
                            <td class="priceCell">$.90</td>
                        </tr>
                        <tr>
                            <td>French Cuffs</td>
                            <td class="priceCell">$.50</td>
                            <td class="priceCell">$.55</td>
                            <td class="priceCell">$.60</td>
                        </tr>
                        <tr>
                            <td>Beads / Special Decoration</td>
                            <td class="priceCell">$2.00</td>
                            <td class="priceCell">$2.20</td>
                            <td class="priceCell">$2.40</td>
                        </tr>
                        <tr>
                            <td>Silk / Rayon / Linen</td>
                            <td class="priceCell">$1.50</td>
                            <td class="priceCell">$1.65</td>
                            <td class="priceCell">$1.80</td>
                        </tr>
                        <tr>
                            <td>Hand Wash</td>
                            <td class="priceCell">$2.00</td>
                            <td class="priceCell">$2.20</td>
                            <td class="priceCell">$2.40</td>
                        </tr>
                         <tr>
                            <td>Crease on Sleeves</td>
                            <td class="priceCell">$.50</td>
                            <td class="priceCell">$.55</td>
                            <td class="priceCell">$.60</td>
                        </tr>
                        <tr>
                            <td>Leather Thrim</td>
                            <td class="priceCell">$3.00</td>
                            <td class="priceCell">$3.30</td>
                            <td class="priceCell">$3.60</td>
                        </tr>
                        <tr>
                            <td>Faux Fur</td>
                            <td class="priceCell">$10.00</td>
                            <td class="priceCell">$11.00</td>
                            <td class="priceCell">$12.00</td>
                        </tr>
                        <tr>
                            <td>Bio Hazard</td>
                            <td class="priceCell">$2.00</td>
                            <td class="priceCell">$2.20</td>
                            <td class="priceCell">$2.40</td>
                        </tr>
                        <tr>
                            <td>Bio Hazard - Soiled</td>
                            <td class="priceCell">$15.00</td>
                            <td class="priceCell">$16.50</td>
                            <td class="priceCell">$18.00</td>
                        </tr>
                        <tr colspan="4">
                            <td></td>
                        </tr>
                        <tr colspan="4">
                            <th>Leather Items (1 to 2 weeks)</th>
                        </tr>
                        <tr>
                            <td>Starts at:</td>
                            <td class="priceCell">$50.00</td>
                            <td class="priceCell">$55.00</td>
                            <td class="priceCell">$60.00</td>
                        </tr>
                        <tr>
                            <td><em>Call for Quote</em></td>
                        </tr>
                        <tr colspan="4">
                            <th>Shoes (3-5 business days)</th>
                        </tr>
                        <tr>
                            <td>Shoe Shine</td>
                            <td class="priceCell">$9.99</td>
                            <td class="priceCell">$10.99</td>
                            <td class="priceCell">$11.99</td>
                        </tr>
                        <tr>
                            <td>Other shoe repairs and services:</td>
                            <td class="priceCell" colspan=3><a href="https://laundrylocker.com/main/shoeshine">
				https://laundrylocker.com/main/shoeshine
			    </td>
                        </tr>
                    </tbody>
                </table>
                
               
               
               
               
            </div>
                <div class="bodyBig" style="margin-top: 30px; display: block; clear: both;">
                    Not listed here, no problem, we can take care of it.  We charge standard market rates for these items.  Some items may require second day service.  If a product is not listed here and you want pricing on it, just <a href="mailto:sales@laundrylocker.com">send us an email</a>.</td>
                    <br><br>
                    <strong>Alterations</strong><br>
                    Laundry Locker can provide minor repairs and alterations.  Need a hem fixed or a button replaced, no problem.  Please include detailed notes with the item you need fixed.  For most alterations, including hemming, we suggest taking your items to a tailor so they can measure you exactly and explain the different tailoring options.
                    <br><br>
                    <strong>Lost Key Fee</strong><br>
                    $25 (we'll credit your account $10 if you find the key later)
                </div>
           
        </div>

        <div class="span4">
            <div class="hidden-phone">
                <?= servicesNav('dc'); ?>
            </div>

            <div class="dryCleanNote calloutRight">
                <div class="graySmall">
                    There is no minimum for locker-based dry cleaning orders.<br><br>
                    *Rates are subject to change without notice<br><br>
                </div>
            </div>

        </div>
    </div>

</div>
