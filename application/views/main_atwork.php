
<div class="fluid-container">

    <div class="row-fluid">
        <div class="span8 bodyBig">
            <img src="/images/howitworks/hero-work.jpg" class="howto_hero_pic" />
            <h1>Reward your workforce</h1>
            <div class="visible-phone" style="text-align: center">
                <?= howNav('work'); ?>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-work-1-place.png" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">Step 1</h2>
                        <p class="bodyBig concierge_p">
                            Have Laundry Locker come directly to your office for daily pickup and deliveries. Not only do we service hundreds of apartment buildings troughout San Francisco, we also service many corporation with our "at work" service.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-work-2-van.png" style="margin-top: 30px" />
                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">Step 2</h2>
                        <p class="bodyBig concierge_p">
                            Whether you would like us to setup lockers or if you would like to designate a drop area for us, employees will appreciate the convenience, quality and service provided by San Francisco's #1 laundry delivery service. We will provide you with collateral to distribute to your employees, laundry bags and signup sheets, making the rollout seamless.

                        </p>
                    </div>
                </div>
            </div>
            <div class="row-fluid concierge_wrapper">
                <div class="span12 concierge_row">
                    <div class="span4">
                        <img class="concierge_pic" src="/images/howitworks/icon-work-3-pickup.png" />

                    </div>
                    <div class="span8">
                        <h2 class="concierge_h2">3. PickUp</h2>
                        <p class="bodyBig concierge_p">

Laundry Locker is here to work with you to make your life easier. Please contact us at (415) 255-7500 and San Francisco's #1 dry cleaner can be servicing your employees right away in a bag and leave it with your concierge.
                        </p>
                    </div>
                </div>
            </div>


        </div>

        <div class="span4">
           <div class="hidden-phone">
                <?= howNav('work'); ?>
            </div>

            <div class="calloutBanner calloutRight WorkInfoFormWrapper" style="height: auto; margin-top: 20px">
                <?php if (isset($email_sent)): ?>
                     <h3 class="green">Message Sent</h3>
                    <p> Your message has been sent. We'll be in touch soon.</p>
                <?php elseif (isset($validation_errors)): ?>
                    <?php echo $validation_errors;?>
                <?php endif; ?>
                <h3>Learn More</h3>
                <form action="" id="work_info_form" method="post">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" />
                    <label for="copmany">Company</label>
                    <input type="text" name="company" id="company" />
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" />
                    <label for="phone">Phone</label>
                    <input type="text" name="phone" id="phone" />

                    <button type="submit" class="btn btn-success btn-large">Request More Info</button>
                </form>               
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
$(document).ready(function () {

    $("#work_info_form").validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            name: {
                required: true
            },
            phone: {
                required: true
            }
        },
        messages: {
            email: "Please enter a valid e-mail",
            name: "Please enter your name",
            phone: "Please enter your phone number"
        },
        errorClass: "invalid",
        submitHandler: function (form) {
            form.submit();
            $(":submit").attr("disabled", true);
        }
    });
});
</script>
