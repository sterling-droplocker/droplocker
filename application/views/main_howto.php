
<div class="fluid-container">


    <h1>How It Works</h1>

    <div class="row">

        <div class="span12 howitworks_wrapper" style="margin-left: 30px">
            <div class="howitworks_col">
                <a href="/main/lockerlocations"><img class="howitworks_pic" src="/images/howitworks/icon-index-locations.png" /></a>
                <h3 class="howitworks">Locker Locations</h3>
                <p class="bodyBig">
                    Lockers inside building lobbies, parking garages, universities, and more. Get your dry cleaning and laundry done everywhere! <br /> <br />
                    
                </p>
                <div>
                <a href="/main/lockerlocations" class="howitworks_link">Learn More...</a>
                </div>
            </div>
            <div class="howitworks_col">
                <a href="/main/concierge"><img class="howitworks_pic" src="/images/howitworks/icon-index-concierge.png" /></a>
                <h3 class="howitworks">Concierge</h3>
                <p class="bodyBig">
                    Servicing virtually every concierge desk in San Francisco and the East Bay. <br /> <br />
                    
                </p>
                <div>
                    <a href="/main/concierge" class="howitworks_link">Learn More...</a>
                </div>
            </div>
            <div class="howitworks_col">
                <a href="/main/locations24hr"><img class="howitworks_pic" src="/images/howitworks/icon-index-24hr.png" /></a>
                <h3 class="howitworks">24hr Public Stores</h3>
                <p class="bodyBig">
                    Located throughout San Francisco's best neighborhoods, our public stores are the ATM's of dry cleaning - you can visit 24/7. <br /> <br />
                    
                </p>
                <div>
                <a href="/main/locations24hr" class="howitworks_link">Learn More...</a>
                </div>
            </div>
            <div class="howitworks_col">
                <a href="/main/athome"><img class="howitworks_pic" src="/images/howitworks/icon-index-home.png" /></a>
                <h3 class="howitworks">@ Home</h3>
                <p class="bodyBig">
                    Want pickup and delivery to your doorstep at specific time that fits your schedule? <br /> <br />
                    
                </p>
                <div>
                <a href="/main/athome" class="howitworks_link">Learn More...</a>
                </div>
            </div>
            <div class="howitworks_col">
                <a href="/main/atwork"><img class="howitworks_pic" src="/images/howitworks/icon-index-work.png" /></a>
                <h3 class="howitworks">@ Work</h3>
                <p class="bodyBig">
                    Reward your workforce and eliminate the hassle of dry cleaning and laundry for your employees. <br /> <br />
                    
                </p>
                <div>
                <a href="/main/atwork" class="howitworks_link">Learn More...</a>
                </div>
            </div>
        </div>

    </div>


    <h3 class="turnaround_title">Turnaround</h3>

    <div class="row">



        <div class="span12 howitworks_wrapper">


            <div class="howitworks_col">
                <h1 class="turnaround"><span class="hidden-desktop hidden-tablet">Locker Locations:</span> 24 Hours**</h1>
            </div>
            <div class="howitworks_col">
                <h1 class="turnaround"><span class="hidden-desktop hidden-tablet">Concierge:</span>Next day*</h1>
            </div>
            <div class="howitworks_col">
                <h1 class="turnaround"><span class="hidden-desktop hidden-tablet">24hr Public Stores:</span>Next day*</h1>
            </div>
            <div class="howitworks_col">
                <h1 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Home:</span>Next day*</h1>
            </div>
            <div class="howitworks_col">
                <h1 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Work:</span>Next day*</h1>
            </div>

        </div>

    </div>
    <h3 class="turnaround_title">
        Hours
    </h3>
    <div class="row">
        <div class="span12 howitworks_wrapper">
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">Locker Locations:</span>7am-7pm**</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">Concierge:</span>24 hours</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">24hr Public Stores:</span>24 hours</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Home:</span>9am-9pm</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Work:</span>9am-5pm</h2>
            </div>

        </div>

    </div>
    <h3 class="turnaround_title">
        Locations
    </h3>
    <div class="row">
        <div class="span12 howitworks_wrapper">
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">Locker Locations:</span>+200</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">Concierge:</span>+100</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">24hr Public Stores:</span>7</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Home:</span>San Francisco</h2>
            </div>
            <div class="howitworks_col">
                
                <h4 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Work:</span>San Francisco, Oakland, Berkeley and Emeriville</h4>
            </div>

        </div>

    </div>
    <h3 class="turnaround_title">
        Availability
    </h3>

    <div class="row">

        <div class="span12 howitworks_wrapper">
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">Locker Locations:</span>Public & Private</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">Concierge:</span>Private</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">24hr Public Stores:</span>Public</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Home:</span>All Homes Welcome</h2>
            </div>
            <div class="howitworks_col">
                <h2 class="turnaround"><span class="hidden-desktop hidden-tablet">@ Work:</span>Private</h2>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="span12">
            <p class="small_letter">
                * All orders placed in the locker by 9am will be returned the following business day with the exception of special requests. <br /><br />
                ** Hours vary depending on type of location.</p>
        </div>
    </div>
</div>
