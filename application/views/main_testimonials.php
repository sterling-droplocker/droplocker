<div class="">
    <div class="row-fluid">
        <div class="span8 bodyBig">

            <h1>Here's What Our Customers Have To Say</h1><br>
            <div class="visible-phone" style="text-align: center">
                <?= aboutNav('testimonials'); ?>
            </div>
            <? if($testimonials):
            foreach($testimonials as $t):
            ?>
            <a name="t<?php echo $t->testimonialID; ?>"></a>
            <?= $t->testimonial ?>
            <a href="<?= $t->siteLink ?>" target="_blank" class="greenLink"><strong>More&nbsp;Reviews</strong></a><br>
            <div style="float:right;"> - <?= $t->customerName ?></div><br><br>
            <?
            endforeach;
            endif;
            ?>

        </div>
        <div class="span4">
            <div class="hidden-phone">
                <?= aboutNav('testimonials'); ?>
            </div>
        </div>
    </div>
</div>