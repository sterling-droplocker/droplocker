<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\EmailLog;
use App\Libraries\DroplockerObjects\DropLockerEmail;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Employee;

/**
 * Redirects the client to the url specified in the HTTP_REFERER.
 * If the HTTP_REFERER is not set, then redirects to the specified fallback URL
 *
 * @param string $fallback_url
 * @param int $statusCode the HTTP status code
 */
function redirect_with_fallback($fallback_url, $statusCode=302)
{
    $CI = get_instance();
    if ($CI->agent->is_referral()) {
        redirect($CI->agent->referrer(), "location", $statusCode);
    } else {
        redirect($fallback_url, "location", $statusCode);
    }
}

/**
 * Retrieves the base domain of a URL
 *
 * @param string $url
 * @return mixed False if no domain is found, otherwise returns domain address
 */
function getDomain($url)
{
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }

    return false;
}

/**
 * Returns the full URL address
 *
 * @param string $path path to append to full url
 * @return string full url
 */
function get_full_url($path)
{
    if (empty($path)) {
        $path = $_SERVER["REQUEST_URI"];
    }

    $pageURL = 'http';
    if (isset($_SERVER['HTTPS']) && $_SERVER["HTTPS"] != "off") {
        $pageURL = "https";
    }
    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"];
    }

    return $pageURL . $path;
}

/**
 * Returns the full base URL
 *
 * @return string
 */
function get_full_base_url()
{
    return sprintf("%s://%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['HTTP_HOST']);
}

function get_current_branch($format = "(On branch %s)" , $force = FALSE)
{
    if ($force || DEV) {
        $branch = 'unknown';
        
        try {
            $path = file_get_contents('.git/HEAD' , TRUE);
            $comps = explode('/', $path);
            $branch = $comps[count($comps) - 1];
        } catch (Exception $exc) {
            $branch = 'unknown';
        }
        
        return sprintf($format, $branch);
    }
}

/**
 * The following function checks if the user is in developer mode by checking if the developer_mode flag is set to 1 in the user's session.
 * Note, this function assumes that the user session data is set.
 *
 * @return boolean true if the user developer mode is set, false if the user developer mode is not set
 */
function in_developer_mode()
{
    $CI = get_instance();
    if ($CI->session->userdata('developer_mode') == 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * The following function checks if the user is a superadmin.
 *
 * @return boolean true if the user is a superadmin, false if the user is not a superadmin
 */
function is_superadmin()
{
    $CI = get_instance();
    if ($CI->session->userdata('super_admin') == 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * The following function checks if the user is enabled to change current business.
 *
 * @return boolean true if the user is enabled, false if the user is not a enabled
 */
function allowed_businesses()
{
    $CI = get_instance();
    $allowed_businesses = $CI->session->userdata('allowed_businesses');
    if (!empty($allowed_businesses)) {
        if (count($allowed_businesses) == 1) {
            return false;
        }
        return $CI->session->userdata('allowed_businesses');
    }
    return false;
}

if (! function_exists("money_format")) {
    function money_format($format, $number)
    {
        $regex = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?' . '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
        if (setlocale(LC_MONETARY, 0) == 'C') {
            setlocale(LC_MONETARY, '');
        }
        $locale = localeconv();
        preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
        foreach ($matches as $fmatch) {
            $value = floatval($number);
            $flags = array(
                'fillchar' => preg_match('/\=(.)/', $fmatch[1], $match) ? $match[1] : ' ',
                'nogroup' => preg_match('/\^/', $fmatch[1]) > 0,
                'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ? $match[0] : '+',
                'nosimbol' => preg_match('/\!/', $fmatch[1]) > 0,
                'isleft' => preg_match('/\-/', $fmatch[1]) > 0
            );
            $width = trim($fmatch[2]) ? (int) $fmatch[2] : 0;
            $left = trim($fmatch[3]) ? (int) $fmatch[3] : 0;
            $right = trim($fmatch[4]) ? (int) $fmatch[4] : $locale['int_frac_digits'];
            $conversion = $fmatch[5];

            $positive = true;
            if ($value < 0) {
                $positive = false;
                $value *= - 1;
            }
            $letter = $positive ? 'p' : 'n';

            $prefix = $suffix = $cprefix = $csuffix = $signal = '';

            $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
            switch (true) {
                case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                    $prefix = $signal;
                    break;
                case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                    $suffix = $signal;
                    break;
                case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                    $cprefix = $signal;
                    break;
                case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                    $csuffix = $signal;
                    break;
                case $flags['usesignal'] == '(':
                case $locale["{$letter}_sign_posn"] == 0:
                    $prefix = '(';
                    $suffix = ')';
                    break;
            }
            if (! $flags['nosimbol']) {
                $currency = $cprefix . ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) . $csuffix;
            } else {
                $currency = '';
            }
            $space = $locale["{$letter}_sep_by_space"] ? ' ' : '';

            $value = number_format($value, $right, $locale['mon_decimal_point'], $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
            $value = @explode($locale['mon_decimal_point'], $value);

            $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
            if ($left > 0 && $left > $n) {
                $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
            }
            $value = implode($locale['mon_decimal_point'], $value);
            if ($locale["{$letter}_cs_precedes"]) {
                $value = $prefix . $currency . $space . $value . $suffix;
            } else {
                $value = $prefix . $value . $space . $currency . $suffix;
            }
            if ($width > 0) {
                $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ? STR_PAD_RIGHT : STR_PAD_LEFT);
            }

            $format = str_replace($fmatch[0], $value, $format);
        }

        return $format;
    }
}

/**
 * Retrieves all the timezones defined in PHP and formats them in an array compatiable with the form_dropdown() function in CodeIgniter.
 *
 * @return array
 */
function get_timezones_as_codeigniter_dropdown()
{
    $timezones = DateTimeZone::listIdentifiers();
    $result = array();
    foreach ($timezones as $timezone) {
        $result[$timezone] = $timezone;
    }

    return $result;
}

function checkAccess($aclRole)
{
    $CI = get_instance();

    if (! $CI->zacl->check_acl($aclRole)) {
        $sqlGetRoles = "select name from aclresources
                            join acl on acl.resource_id = aclresources.id
                            join aclroles on aclroles.aclID = type_id
                            where resource = ?
                            and action = 'allow'";
        $q = $CI->db->query($sqlGetRoles, array(
            $aclRole
        ));
        $roles = $q->result();
        foreach ($roles as $r) {
            $allowed .= '<strong>' . $r->name . '</strong> or ';
        }
        $allowed = substr($allowed, 0, - 3);

        $message = 'You must be a <strong>' . $allowed . '</strong> to have access to this page.  This page is protected by aclrole: <strong>' . $aclRole . '</strong>';
        /*
         * $CI->data['content'] = $CI->load->view('admin/admin_accessDenied', $content, true); $CI->template->write_view('content', 'inc/dual_column_left', $CI->data); $CI->template->render();
         */
        die('<h2>Access Denied</h2><br>' . $message);
    }

    return false;
}

/**
 * Returns true if the server is in bizie mode, or false otherwise.
 *
 * @return bool
 */
function in_bizzie_mode()
{
    static $in_bizzie_mode = null;
    if (is_null($in_bizzie_mode)) {
        $in_bizzie_mode = Server_Meta::in_bizzie_mode();
    }

    return $in_bizzie_mode;
}

/**
 * Returns the server base url
 *
 * @return string
 */
function get_admin_url()
{
    return 'https://' . Server_Meta::get("admin_url");
}

/**
 * Retrieves the master business.
 * If the master busines does not exist, then an error message is logged and also displayed to the user.
 *
 * @return null \App\Libraries\DroplockerObjects\Business the master business as a Droplocker Business Object, or null if the business does not exist
 * @throws \LogicException if this function is called when the server is not in Bizzie mode
 */
function get_master_business()
{
    if (in_bizzie_mode()) {
        $master_business_id = Server_Meta::get("master_business_id");
        if (! is_numeric($master_business_id)) {
            set_flash_message("error", "The master business must be set when the server is in Bizzie mode");
            trigger_error("The server is in Bizzie mode, but the master business is not set.");

            return null;
        }
        try {
            $master_business = new Business($master_business_id);

            return $master_business;
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "The master business must be set to an existing business when the server is in Bizzie Mode");
            trigger_error("The master business is set to a business that does not exist");

            return null;
        }
        if (is_null($business)) {
            if (is_superadmin()) {
                redirect("/admin/superadmin/manage_master_business");
            }
        } else {
            return $business;
        }
    } else {
        throw new \LogicException("'This function can only be called in 'bizzie' mode.");
    }
}

/**
 *
 * @param int $business_id
 *            The business for which to retrieve all roles associated with
 * @return array An array formatted to be passed into the form_dropdown() function in the CodeIgniter form helper
 * @throws \InvalidArgumentException
 */
function get_roles_as_codeigniter_dropdown($business_id)
{
    if (! is_numeric($business_id)) {
        throw new \InvalidArgumentException("'business_id' must be numeric");
    }
    $CI = get_instance();
    $CI->load->model("aclroles_model");
    $roles = $CI->aclroles_model->get(array(
        "business_id" => $business_id
    ));
    $roles_menu = array();
    foreach ($roles as $role) {
        $roles_menu[$role->aclID] = $role->name;
    }

    return $roles_menu;
}

/**
 * The following function is *, do not use.
 * gets a list of locales that are available on the server
 *
 * @example return item in the array
 *          [en_GB.utf8] => English - UNITED KINGDOM - utf8
 *
 * @return array
 */
function getLocales()
{
    /**
     * References :
     * 1.
     * http://en.wikipedia.org/wiki/ISO_3166-1#Officially_assigned_code_elements
     */
    $country_codes = array(

        'AF' => "AFGHANISTAN",
        'AL' => "ALBANIA",
        'DZ' => "ALGERIA",
        'AS' => "AMERICAN SAMOA",
        'AD' => "ANDORRA",
        'AO' => "ANGOLA",
        'AI' => "ANGUILLA",
        'AQ' => "ANTARCTICA",
        'AG' => "ANTIGUA AND BARBUDA",
        'AR' => "ARGENTINA",
        'AM' => "ARMENIA",
        'AW' => "ARUBA",
        'AU' => "AUSTRALIA",
        'AT' => "AUSTRIA",
        'AZ' => "AZERBAIJAN",
        'BS' => "BAHAMAS",
        'BH' => "BAHRAIN",
        'BD' => "BANGLADESH",
        'BB' => "BARBADOS",
        'BY' => "BELARUS",
        'BE' => "BELGIUM",
        'BZ' => "BELIZE",
        'BJ' => "BENIN",
        'BM' => "BERMUDA",
        'BT' => "BHUTAN",
        'BO' => "BOLIVIA",
        'BA' => "BOSNIA AND HERZEGOVINA",
        'BW' => "BOTSWANA",
        'BV' => "BOUVET ISLAND",
        'BR' => "BRAZIL",
        'IO' => "BRITISH INDIAN OCEAN TERRITORY",
        'BN' => "BRUNEI DARUSSALAM",
        'BG' => "BULGARIA",
        'BF' => "BURKINA FASO",
        'BI' => "BURUNDI",
        'KH' => "CAMBODIA",
        'CM' => "CAMEROON",
        'CA' => "CANADA",
        'CV' => "CAPE VERDE",
        'KY' => "CAYMAN ISLANDS",
        'CF' => "CENTRAL AFRICAN REPUBLIC",
        'TD' => "CHAD",
        'CL' => "CHILE",
        'CN' => "CHINA",
        'CX' => "CHRISTMAS ISLAND",
        'CC' => "COCOS (KEELING) ISLANDS",
        'CO' => "COLOMBIA",
        'KM' => "COMOROS",
        'CG' => "CONGO",
        'CD' => "CONGO, THE DEMOCRATIC REPUBLIC OF THE",
        'CK' => "COOK ISLANDS",
        'CR' => "COSTA RICA",
        'CI' => "COTE D'IVOIRE",
        'HR' => "CROATIA",
        'CU' => "CUBA",
        'CY' => "CYPRUS",
        'CZ' => "CZECH REPUBLIC",
        'DK' => "DENMARK",
        'DJ' => "DJIBOUTI",
        'DM' => "DOMINICA",
        'DO' => "DOMINICAN REPUBLIC",
        'EC' => "ECUADOR",
        'EG' => "EGYPT",
        'SV' => "EL SALVADOR",
        'GQ' => "EQUATORIAL GUINEA",
        'ER' => "ERITREA",
        'EE' => "ESTONIA",
        'ET' => "ETHIOPIA",
        'FK' => "FALKLAND ISLANDS (MALVINAS)",
        'FO' => "FAROE ISLANDS",
        'FJ' => "FIJI",
        'FI' => "FINLAND",
        'FR' => "FRANCE",
        'GF' => "FRENCH GUIANA",
        'PF' => "FRENCH POLYNESIA",
        'TF' => "FRENCH SOUTHERN TERRITORIES",
        'GA' => "GABON",
        'GM' => "GAMBIA",
        'GE' => "GEORGIA",
        'DE' => "GERMANY",
        'GH' => "GHANA",
        'GI' => "GIBRALTAR",
        'GR' => "GREECE",
        'GL' => "GREENLAND",
        'GD' => "GRENADA",
        'GP' => "GUADELOUPE",
        'GU' => "GUAM",
        'GT' => "GUATEMALA",
        'GN' => "GUINEA",
        'GW' => "GUINEA-BISSAU",
        'GY' => "GUYANA",
        'HT' => "HAITI",
        'HM' => "HEARD ISLAND AND MCDONALD ISLANDS",
        'VA' => "HOLY SEE (VATICAN CITY STATE)",
        'HN' => "HONDURAS",
        'HK' => "HONG KONG",
        'HU' => "HUNGARY",
        'IS' => "ICELAND",
        'IN' => "INDIA",
        'ID' => "INDONESIA",
        'IR' => "IRAN, ISLAMIC REPUBLIC OF",
        'IQ' => "IRAQ",
        'IE' => "IRELAND",
        'IL' => "ISRAEL",
        'IT' => "ITALY",
        'JM' => "JAMAICA",
        'JP' => "JAPAN",
        'JO' => "JORDAN",
        'KZ' => "KAZAKHSTAN",
        'KE' => "KENYA",
        'KI' => "KIRIBATI",
        'KP' => "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF",
        'KR' => "KOREA, REPUBLIC OF",
        'KW' => "KUWAIT",
        'KG' => "KYRGYZSTAN",
        'LA' => "LAO PEOPLE'S DEMOCRATIC REPUBLIC",
        'LV' => "LATVIA",
        'LB' => "LEBANON",
        'LS' => "LESOTHO",
        'LR' => "LIBERIA",
        'LY' => "LIBYAN ARAB JAMAHIRIYA",
        'LI' => "LIECHTENSTEIN",
        'LT' => "LITHUANIA",
        'LU' => "LUXEMBOURG",
        'MO' => "MACAO",
        'MK' => "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF",
        'MG' => "MADAGASCAR",
        'MW' => "MALAWI",
        'MY' => "MALAYSIA",
        'MV' => "MALDIVES",
        'ML' => "MALI",
        'MT' => "MALTA",
        'MH' => "MARSHALL ISLANDS",
        'MQ' => "MARTINIQUE",
        'MR' => "MAURITANIA",
        'MU' => "MAURITIUS",
        'YT' => "MAYOTTE",
        'MX' => "MEXICO",
        'FM' => "MICRONESIA, FEDERATED STATES OF",
        'MD' => "MOLDOVA, REPUBLIC OF",
        'MC' => "MONACO",
        'MN' => "MONGOLIA",
        'MS' => "MONTSERRAT",
        'MA' => "MOROCCO",
        'MZ' => "MOZAMBIQUE",
        'MM' => "MYANMAR",
        'NA' => "NAMIBIA",
        'NR' => "NAURU",
        'NP' => "NEPAL",
        'NL' => "NETHERLANDS",
        'AN' => "NETHERLANDS ANTILLES",
        'NC' => "NEW CALEDONIA",
        'NZ' => "NEW ZEALAND",
        'NI' => "NICARAGUA",
        'NE' => "NIGER",
        'NG' => "NIGERIA",
        'NU' => "NIUE",
        'NF' => "NORFOLK ISLAND",
        'MP' => "NORTHERN MARIANA ISLANDS",
        'NO' => "NORWAY",
        'OM' => "OMAN",
        'PK' => "PAKISTAN",
        'PW' => "PALAU",
        'PS' => "PALESTINIAN TERRITORY, OCCUPIED",
        'PA' => "PANAMA",
        'PG' => "PAPUA NEW GUINEA",
        'PY' => "PARAGUAY",
        'PE' => "PERU",
        'PH' => "PHILIPPINES",
        'PN' => "PITCAIRN",
        'PL' => "POLAND",
        'PT' => "PORTUGAL",
        'PR' => "PUERTO RICO",
        'QA' => "QATAR",
        'RE' => "REUNION",
        'RO' => "ROMANIA",
        'RU' => "RUSSIAN FEDERATION",
        'RW' => "RWANDA",
        'SH' => "SAINT HELENA",
        'KN' => "SAINT KITTS AND NEVIS",
        'LC' => "SAINT LUCIA",
        'PM' => "SAINT PIERRE AND MIQUELON",
        'VC' => "SAINT VINCENT AND THE GRENADINES",
        'WS' => "SAMOA",
        'SM' => "SAN MARINO",
        'ST' => "SAO TOME AND PRINCIPE",
        'SA' => "SAUDI ARABIA",
        'SN' => "SENEGAL",
        'CS' => "SERBIA AND MONTENEGRO",
        'SC' => "SEYCHELLES",
        'SL' => "SIERRA LEONE",
        'SG' => "SINGAPORE",
        'SK' => "SLOVAKIA",
        'SI' => "SLOVENIA",
        'SB' => "SOLOMON ISLANDS",
        'SO' => "SOMALIA",
        'ZA' => "SOUTH AFRICA",
        'GS' => "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
        'ES' => "SPAIN",
        'LK' => "SRI LANKA",
        'SD' => "SUDAN",
        'SR' => "SURINAME",
        'SJ' => "SVALBARD AND JAN MAYEN",
        'SZ' => "SWAZILAND",
        'SE' => "SWEDEN",
        'CH' => "SWITZERLAND",
        'SY' => "SYRIAN ARAB REPUBLIC",
        'TW' => "TAIWAN, PROVINCE OF CHINA",
        'TJ' => "TAJIKISTAN",
        'TZ' => "TANZANIA, UNITED REPUBLIC OF",
        'TH' => "THAILAND",
        'TL' => "TIMOR-LESTE",
        'TG' => "TOGO",
        'TK' => "TOKELAU",
        'TO' => "TONGA",
        'TT' => "TRINIDAD AND TOBAGO",
        'TN' => "TUNISIA",
        'TR' => "TURKEY",
        'TM' => "TURKMENISTAN",
        'TC' => "TURKS AND CAICOS ISLANDS",
        'TV' => "TUVALU",
        'UG' => "UGANDA",
        'UA' => "UKRAINE",
        'AE' => "UNITED ARAB EMIRATES",
        'GB' => "UNITED KINGDOM",
        'US' => "UNITED STATES",
        'UM' => "UNITED STATES MINOR OUTLYING ISLANDS",
        'UY' => "URUGUAY",
        'UZ' => "UZBEKISTAN",
        'VU' => "VANUATU",
        'VE' => "VENEZUELA",
        'VN' => "VIET NAM",
        'VG' => "VIRGIN ISLANDS, BRITISH",
        'VI' => "VIRGIN ISLANDS, U.S.",
        'WF' => "WALLIS AND FUTUNA",
        'EH' => "WESTERN SAHARA",
        'YE' => "YEMEN",
        'ZM' => "ZAMBIA",
        'ZW' => "ZIMBABWE"
    );

    // End of country codes

    /**
     * ISO 639-1 Language Codes
     * Useful in Locale analysis
     *
     * References :
     * 1. http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
     * 2. http://blog.xoundboy.com/?p=235
     */

    $language_codes = array(
        'en' => 'English',
        'aa' => 'Afar',
        'ab' => 'Abkhazian',
        'af' => 'Afrikaans',
        'am' => 'Amharic',
        'ar' => 'Arabic',
        'as' => 'Assamese',
        'ay' => 'Aymara',
        'az' => 'Azerbaijani',
        'ba' => 'Bashkir',
        'be' => 'Byelorussian',
        'bg' => 'Bulgarian',
        'bh' => 'Bihari',
        'bi' => 'Bislama',
        'bn' => 'Bengali/Bangla',
        'bo' => 'Tibetan',
        'br' => 'Breton',
        'ca' => 'Catalan',
        'co' => 'Corsican',
        'cs' => 'Czech',
        'cy' => 'Welsh',
        'da' => 'Danish',
        'de' => 'German',
        'dz' => 'Bhutani',
        'el' => 'Greek',
        'eo' => 'Esperanto',
        'es' => 'Spanish',
        'et' => 'Estonian',
        'eu' => 'Basque',
        'fa' => 'Persian',
        'fi' => 'Finnish',
        'fj' => 'Fiji',
        'fo' => 'Faeroese',
        'fr' => 'French',
        'fy' => 'Frisian',
        'ga' => 'Irish',
        'gd' => 'Scots/Gaelic',
        'gl' => 'Galician',
        'gn' => 'Guarani',
        'gu' => 'Gujarati',
        'ha' => 'Hausa',
        'hi' => 'Hindi',
        'hr' => 'Croatian',
        'hu' => 'Hungarian',
        'hy' => 'Armenian',
        'ia' => 'Interlingua',
        'ie' => 'Interlingue',
        'ik' => 'Inupiak',
        'in' => 'Indonesian',
        'is' => 'Icelandic',
        'it' => 'Italian',
        'iw' => 'Hebrew',
        'ja' => 'Japanese',
        'ji' => 'Yiddish',
        'jw' => 'Javanese',
        'ka' => 'Georgian',
        'kk' => 'Kazakh',
        'kl' => 'Greenlandic',
        'km' => 'Cambodian',
        'kn' => 'Kannada',
        'ko' => 'Korean',
        'ks' => 'Kashmiri',
        'ku' => 'Kurdish',
        'ky' => 'Kirghiz',
        'la' => 'Latin',
        'ln' => 'Lingala',
        'lo' => 'Laothian',
        'lt' => 'Lithuanian',
        'lv' => 'Latvian/Lettish',
        'mg' => 'Malagasy',
        'mi' => 'Maori',
        'mk' => 'Macedonian',
        'ml' => 'Malayalam',
        'mn' => 'Mongolian',
        'mo' => 'Moldavian',
        'mr' => 'Marathi',
        'ms' => 'Malay',
        'mt' => 'Maltese',
        'my' => 'Burmese',
        'na' => 'Nauru',
        'ne' => 'Nepali',
        'nl' => 'Dutch',
        'no' => 'Norwegian',
        'oc' => 'Occitan',
        'om' => '(Afan)/Oromoor/Oriya',
        'pa' => 'Punjabi',
        'pl' => 'Polish',
        'ps' => 'Pashto/Pushto',
        'pt' => 'Portuguese',
        'qu' => 'Quechua',
        'rm' => 'Rhaeto-Romance',
        'rn' => 'Kirundi',
        'ro' => 'Romanian',
        'ru' => 'Russian',
        'rw' => 'Kinyarwanda',
        'sa' => 'Sanskrit',
        'sd' => 'Sindhi',
        'sg' => 'Sangro',
        'sh' => 'Serbo-Croatian',
        'si' => 'Singhalese',
        'sk' => 'Slovak',
        'sl' => 'Slovenian',
        'sm' => 'Samoan',
        'sn' => 'Shona',
        'so' => 'Somali',
        'sq' => 'Albanian',
        'sr' => 'Serbian',
        'ss' => 'Siswati',
        'st' => 'Sesotho',
        'su' => 'Sundanese',
        'sv' => 'Swedish',
        'sw' => 'Swahili',
        'ta' => 'Tamil',
        'te' => 'Tegulu',
        'tg' => 'Tajik',
        'th' => 'Thai',
        'ti' => 'Tigrinya',
        'tk' => 'Turkmen',
        'tl' => 'Tagalog',
        'tn' => 'Setswana',
        'to' => 'Tonga',
        'tr' => 'Turkish',
        'ts' => 'Tsonga',
        'tt' => 'Tatar',
        'tw' => 'Twi',
        'uk' => 'Ukrainian',
        'ur' => 'Urdu',
        'uz' => 'Uzbek',
        'vi' => 'Vietnamese',
        'vo' => 'Volapuk',
        'wo' => 'Wolof',
        'xh' => 'Xhosa',
        'yo' => 'Yoruba',
        'zh' => 'Chinese',
        'zu' => 'Zulu'
    );

    $locale_data = array();

    // Get locales from Linux terminal command locale
    $locales = shell_exec('locale -a');

    $locales = explode("\n", $locales);

    foreach ($locales as $c => $l) {
        if (strlen($l)) {
            $parts = explode('.', $l);
            $lc = $parts[0];

            list ($lcode, $ccode) = explode('_', $lc);

            $lcode = strtolower($lcode);

            $language = $language_codes[$lcode];
            $country = $country_codes[$ccode];

            if (strlen($language) and strlen($country)) {
                // The following code is a temporary workaround for undocumented and unreadable * code that does not follow our accepted coding standards. This conditional prevents NOTICE errors from being emitted.
                if (isset($parts[1])) {
                    $locale_data[$l] = "$language - $country - {$parts[1]}";
                }
            }
        }
    }

    return $locale_data;
}

function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = str_replace(".", "", $phoneNumber);

    if (preg_match('/^\+\d(\d{3})(\d{3})(\d{4})$/', "+1" . $phoneNumber, $matches)) {
        $result = $matches[1] . '-' . $matches[2] . '-' . $matches[3];

        return $result;
    }
}

/**
 * Converts a string to a slug-formatted string.
 *
 * @param string $text Text to convert into slug
 * @param bool $lower  If true, convert to lowercase
 * @param string $char char to replace non allowed chars from slug
 * @return string
 */
function convert_to_slug($text, $lower = true, $char = '')
{
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    if ($lower) {
        // lowercase
        $text = strtolower($text);
    }

    // replace non letter or digits by -
    $text = preg_replace('#[^a-zA-Z0-9_]#', $char, $text);

    // trim
    $text = trim($text, '-');

    // remove unwanted characters
    // $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

/**
 * check that page with the users permissions.
 * Will redirect to denied page if the user does not have correct permissions. The resource name consists of segment 2 and segemnt 3 of the URL
 *
 * This will insert the new resource into the database
 * This will give admin role access
 * If the current user is an admin, they will not get redirected to denied page.
 */
function check_acl()
{
    $CI = get_instance();

    // If the user is a superadmin or an admin, then access should always be granted to a resource.
    if ($CI->session->userdata('super_admin') == 1 || $CI->zacl->get_employee_role($CI->session->userdata('buser_id')) == 'Admin') {
        return true;
    } else {
        $business_id = $CI->session->userdata('business_id');
        $segment_2 = $CI->uri->segment(2);
        $segment_3 = $CI->uri->segment(3);
        $resource = $segment_2 . "_" . $segment_3;
        $sql = "SELECT * FROM aclresources WHERE resource = ?";
        $q = $CI->db->query($sql, array(
            $resource
        ));
        if (! $q->row()) {
            $sql = "INSERT INTO aclresources (resource, description, aclgroup, default_value)
                                VALUES (?, ?, ?, 'false')";
            $CI->db->query($sql, array(
                $resource,
                $resource,
                $segment_2
            ));
            $resource_id = $CI->db->insert_id();

            // get the business admin role ID and update the acl to provide access
            $CI->load->model('aclroles_model');
            $CI->aclroles_model->update_acl(array(
                "business_id" => $business_id,
                "type_id" => $CI->zacl->getRoleID($business_id, 'Admin'),
                "resource_id" => $resource_id
            ));

            if (! $CI->zacl->get_employee_role($CI->session->userdata('buser_id')) == 'admin') {
                redirect("/admin/main/denied/orders");
            }
        }

        if (! $CI->zacl->check_acl($resource, $CI->session->userdata('buser_id'))) {
            redirect("/admin/main/denied/$segment_2/$segment_3");
        }
    }
}

/**
 * The following funciton is unknow, undocumented *, do not use.
 *
 * @return int
 */
function daysAdd()
{
    switch (date("w")) {
        case 0:
            $daysAdd = 0;
            break;
        case 1:
            $daysAdd = - 1;
            break;
        case 2:
            $daysAdd = - 2;
            break;
        case 6:
            $daysAdd = 0;
            break;
        default:
            $daysAdd = 0;
    }

    return $daysAdd;
}

/**
 * Note, do not use this * code.
 * Don't * me, bro
 *
 * @deprecated --------
 *             #######################################
 *             BEGIN *
 *             #######################################
 *
 *             required method allows a check to see if the required elements are in the options array
 *
 * @param array $required
 * @param array $data
 * @return boolean #######################################
 *         END *
 *         #######################################
 *
 */
function required($required = '', $data = '')
{
    if (gettype($data) != 'array') {
        $data = (array) $data;
    }

    if (empty($required) || empty($data))
        return false;

    foreach ($required as $field) {
        if (! isset($data[$field]) || $data[$field] == '')
            return false;
    }

    return true;
}

/**
 * takes either an array or stdClass object and return a formatted string that can be used in emails
 *
 * @param array|stdClass $array
 * @return string
 */
function formatArray($array)
{
    $str = '';
    if (sizeof($array)) {
        foreach ($array as $key => $value) {
            if ($value instanceof DateTime) {
                $value = $value->format("Y-m-d H:i:s");
            }

            $str .= "<strong>$key: </strong> " . urldecode($value) . "<br>";
        }
    }

    return $str;
}

/**
 * uses google geocoding service to get the address information
 *
 * @param string $address
 * @param string $city
 * @param string $state
 * @return array google geocode results
 *
 * @example $geo = geocode($_POST['address'], $_POST['city'], $_POST['state']);
 *          $lat = $geo['lat'];
 *          $lng = $geo['lng'];
 */
function geocode($address, $city, $state)
{
    $search = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . ",+" . urlencode($city) . ",+" . urlencode($state) . "&sensor=false&key=" . get_default_gmaps_api_key();
    
    $geo = file_get_contents($search);
    $geo = json_decode($geo);
    $res = array(
        'lat' => str_replace(',', '.', $geo->results[0]->geometry->location->lat),
        'lng' => str_replace(',', '.', $geo->results[0]->geometry->location->lng),
    );

    return $res;
}

/**
 * The following function is *, do not use
 *
 * @deprecated This function is deprecated, use convert_to_gmt_aprax().
 *             convert_to_gmt method
 *
 * @param timestamp $time
 * @param string $timezone
 * @param boolean $dst
 *            (daylight savings)
 */
function convert_to_gmt($time = '', $timezone = 'UTC', $dst = FALSE)
{
    // convert to GMT
    $time = date("c", $time);
    $time = new DateTime($time);
    $time->setTimeZone(new DateTimeZone('GMT'));
    $time = strtotime($time->format("Y-m-d H:i:s"));

    return $time;
}

/**
 * takes a date string, not a timestamp and return the gmt date string
 *
 * Example: dateString_to_gmt(date("Y-m-d H:i:s"));
 *
 * @param string $date
 */
function dateString_to_gmt($date)
{
    return date("Y-m-d H:i:s", local_to_gmt(strtotime($date)));
}

function convert_from_local_to_gmt($date, $format = "Y-m-d", $business_id = '')
{
    $CI = get_instance();

    if (empty($business_id)) {
        if (isset($CI->business)) {
            $timezone = $CI->business->timezone;
        } elseif ($CI->session->userdata("timezone")) {
            $timezone = $CI->session->userdata("timezone");
        } else {
            $timezone = "America/Los_Angeles";
        }
    } else {
        // The following conditonal checks to see if the specified business matches the business in the session, if so, then we retrieve the timezone information from the session, otherwise, we query the database to retrieve the business.
        if (isset($CI->business) && $CI->business->businessID = $business_id) {
            $timezone = $CI->business->timezone;
        } else {
            $CI->load->model("business_model");
            $business = $CI->business_model->get_by_primary_key($business_id);
            $timezone = $business->timezone;
        }
    }

    if (empty($date)) {
        $local_date = new DateTime("now", new DateTimeZone($timezone));
    } elseif ($date == "0000-00-00 00:00:00") {
        return "";
    } elseif ($date instanceof DateTime) {
        $local_date = clone($date);
    } else {
        $local_date = new DateTime($date, new DateTimeZone($timezone));
    }

    $local_date->setTimeZone(new DateTimeZone("GMT"));
    
    convert_date_format_to_japanese($format, $local_date);

    return $local_date->format($format);
}

/**
 * The following function converts a specified datetime from GMT to the business timezone.
 * Converts a datetime from GMT to the business timezone.
 *
 * @param string $date
 *            Optional. A UTC time and/or time, if empty, then the date is set to today's current time.
 * @param string $format
 *            Optional. The specified date format must be compatiable with the format specifiers for DateTime
 * @param int $business_id.
 *            If not passed, will try to use the session business_id. If no session business exists, then the time is converted to America/Los_Angeles.
 * @return string a date string in the business timezone
 */
function convert_from_gmt_aprax($date, $format = "M/D/y", $business_id = '')
{
    $CI = get_instance();
    if (empty($date)) {
        $gmt_date = new DateTime("now", new DateTimeZone("GMT"));
    } elseif ($date == "0000-00-00 00:00:00") {
        return "";
    } elseif ($date instanceof DateTime) {
        $gmt_date = clone($date);
    } else {
        $gmt_date = new DateTime($date, new DateTimeZone("GMT"));
    }

    if (empty($business_id)) {
        if (isset($CI->business)) {
            $timezone = $CI->business->timezone;
        } elseif ($CI->session->userdata("timezone")) {
            $timezone = $CI->session->userdata("timezone");
        } else {
            $timezone = "America/Los_Angeles";
        }
    } else {
        // The following conditonal checks to see if the specified business matches the business in the session, if so, then we retrieve the timezone information from the session, otherwise, we query the database to retrieve the business.
        if (isset($CI->business) && $CI->business->businessID = $business_id) {
            $timezone = $CI->business->timezone;
        } else {
            $CI->load->model("business_model");
            $business = $CI->business_model->get_by_primary_key($business_id);
            $timezone = $business->timezone;
        }
    }



    $gmt_date->setTimeZone(new DateTimeZone($timezone));

    convert_date_format_to_japanese($format, $gmt_date);

    return $gmt_date->format($format);
}

function apply_locale_date_format($date, $format = "%b %e, %Y", $business_id = null)
{
    $timezone = get_current_business_timezone($business_id);// only necesary for japanese formatting
    
    $dt = new DateTime($date, $timezone);
    
    convert_date_format_to_japanese($format, $dt);

    return strftime($format, $dt->getTimestamp());
}

function apply_date_format($date, $format = "M/D/y", $business_id = null)
{
    $timezone = get_current_business_timezone($business_id);// only necesary for japanese formatting
    
    $dt = new DateTime($date, $timezone);
    
    convert_date_format_to_japanese($format, $dt);

    return $dt->format($format);
}

function get_current_business_timezone($business_id = null)
{
    $CI = get_instance();
    $business_id = get_current_business_id($business_id);
    
    if (empty($business_id)) {
        if (isset($CI->business)) {
            $timezone = $CI->business->timezone;
        } elseif ($CI->session->userdata("timezone")) {
            $timezone = $CI->session->userdata("timezone");
        } else {
            $timezone = "America/Los_Angeles";
        }
    } else {
        // The following conditonal checks to see if the specified business matches the business in the session, if so, then we retrieve the timezone information from the session, otherwise, we query the database to retrieve the business.
        if (isset($CI->business) && $CI->business->businessID = $business_id) {
            $timezone = $CI->business->timezone;
        } else {
            $CI->load->model("business_model");
            $business = $CI->business_model->get_by_primary_key($business_id);
            $timezone = $business->timezone;
        }
    }
    
    return new DateTimeZone($timezone);
}

/**
 * The following function returns just the time from datetime.
 *
 * @param string $dateTime
 *            Assumes a date followed by a space then a time.
 * @return time from $dateTime without date.
 */
function get_time_from_datetime($dateTime) {
    if (empty($dateTime)) {
        return '';
    }

    return end(explode(' ', $dateTime, 2));
}

/**
 * The following function returns just the date from datetime.
 *
 * @param string $dateTime
 *            Assumes a date followed by a space then a time.
 * @return date from $dateTime without time.
 */
function get_date_from_datetime($dateTime) {
    if (empty($dateTime)) {
        return '';
    }

    return current(explode(' ', $dateTime, 2));
}

/**
 * Convert a date from GMT to a timezone
 *
 * @param string $date
 * @param string $timezone
 * @param string $format
 * @return string
 */
function to_timezone($date, $timezone, $format = 'Y-m-d H:i:s T') {
    $gmt_date = new DateTime($date, new DateTimeZone("GMT"));
    $gmt_date->setTimeZone(new DateTimeZone($timezone));
    return $gmt_date->format($format);
}

/**
 * The following function converts a time from GMT and specified the date format in the current locale
 * Converts a datetime from GMT to the business timezone.
 *
 * @param string $date
 *            Optional. A UTC time and/or time, if empty, then the date is set to today's current time.
 * @param string $format
 *            Optional. This ofrmat much match the format specified for strformat
 * @param int $business_id.
 *            If not passed, will try to use the session business_id. If no session business exists, then the time is converted to America/Los_Angeles.
 * @return string a date string in the business timezone
 */
function convert_from_gmt_locale($date, $format = "%b %e, %Y", $business_id = "")
{
    $CI = get_instance();

    if (empty($date)) {
        $gmt_date = new DateTime("now", new DateTimeZone("GMT"));
    } elseif ($date == "0000-00-00 00:00:00") {
        return "";
    } else {
        $gmt_date = new DateTime($date, new DateTimeZone("GMT"));
    }

    if (empty($business_id)) {
        if (isset($CI->business)) {
            $timezone = $CI->business->timezone;
        } elseif ($CI->session->userdata("timezone")) {
            $timezone = $CI->session->userdata("timezone");
        } else {
            $timezone = "America/Los_Angeles";
        }
    } else {
        // The following conditonal checks to see if the specified business matches the business in the session, if so, then we retrieve the timezone information from the session, otherwise, we query the database to retrieve the business.
        if (isset($CI->business) && $CI->business->businessID = $business_id) {
            $timezone = $CI->business->timezone;
        } else {
            $CI->load->model("business_model");
            $business = $CI->business_model->get_by_primary_key($business_id);
            $timezone = $business->timezone;
        }
    }

    $gmt_date->setTimeZone(new DateTimeZone($timezone));
    
    convert_date_format_to_japanese($format, $gmt_date);
   
    $date = strftime($format, $gmt_date->getTimestamp());
    
    return $date;
}

function convert_date_format_to_japanese(&$format, $date) 
{
    $tz = $date->getTimezone();

    if (in_array($tz->getName(), config_item('japaneseTimeZones'))) {
        $japaneseDays = config_item('japaneseDays');
        $dayOfTheWeek = $date->format('N')-1;

        foreach (array("/%a/", "/D/") as $x) {
            if (preg_match($x, $format))
            { 
                $format = preg_replace($x, $japaneseDays[$dayOfTheWeek], $format); 
            }
        }
    }
}

function translate_date($date, $customer_id = null) 
{
    $days = [
        'Sunday'    =>'Sun',
        'Monday'    =>'Mon',
        'Tuesday'   =>'Tue',
        'Wednesday' =>'Wed',
        'Thursday'  =>'Thu',
        'Friday'    =>'Fri',
        'Saturday'  =>'Sat'
    ];
    
    $months = array(
        'January'   => 'Jan', 
        'February'  => 'Feb', 
        'March'     => 'Mar', 
        'April'     => 'Apr', 
        'May'       => 'May', 
        'June'      => 'Jun', 
        'July'      => 'Jul', 
        'August'    => 'Aug', 
        'September' => 'Sep', 
        'October'   => 'Oct', 
        'November'  => 'Nov', 
        'December'  => 'Dec'
    );

    foreach($days as $long=>$short) {
        $date = str_ireplace($long, get_translation('DayLong_' . $long, "Email Macros", array(), $long, $customer_id), $date);
        $date = str_ireplace($short, get_translation('DayShort_' . $short, "Email Macros", array(), $short, $customer_id), $date);
    }

    foreach($months as $long=>$short) {
        $date = str_ireplace($long, get_translation('MonthLong_' . $long, "Email Macros", array(), $long), $date);
        $date = str_ireplace($short, get_translation('MonthShort_' . $short, "Email Macros", array(), $short), $date);
    }

    return $date;
}

/**
 * converts any date from a timezone to GMT
 * if nothing is passed, this function will return the current GMT time, even if the server timezone is set to something else
 *
 * @param string $date
 *            Optional
 * @param string $from
 *            Optional
 * @param string $format
 *            Optional
 * @return string GMT date
 */
function convert_to_gmt_aprax($date = 'now', $from = '', $format = "Y-m-d H:i:s")
{
    if (empty($from)) {

        // use the server timezone
        $dateObj = new DateTime();
        $tz = $dateObj->getTimezone();
        $from = $tz->getName();
    }

    $gmt_date = new DateTime($date, new DateTimeZone($from));

    $gmt_date->setTimeZone(new DateTimeZone("GMT"));

    convert_date_format_to_japanese($format, $gmt_date);
    
    return $gmt_date->format($format);
}

/**
 *
 * @deprecated This function is deprecated in favor of convert_from_gmt_aprax.
 * @param type $timezone
 * @param type $date
 * @param type $format
 * @return type
 */
function local_date($timezone, $date = '', $format = '')
{
    $result = convert_from_gmt_aprax($date, $format);

    return $result;
}

/**
 * set_alert uses the bootstrap css to make pretty alert messages
 * Sets flashdata to only appear on the next page load
 *
 * Classes:
 * * block - yellow
 * * error - red
 * * success - green
 * * info - blue
 *
 * Data can be a string or an array of messages. If passed an array an unordered list will display with
 * each message as a list item. string will not use a list item, just outputs the message
 *
 * @param string $class
 * @param string|array $data
 */
function set_alert($class, $data)
{
    $CI = get_instance();

    if (is_array($data)) {
        $text = '<ul>';
        foreach ($data as $d) {
            $text .= "<li>{$d}</li>";
        }
        $text .= '</ul>';
    } else {
        $text = $data;
    }

    $CI->session->set_flashdata("alert", "<div class='alert alert-$class'><button class='close' data-dismiss='alert'>x</button> " . $text . "</div>");
}

function set_alert_now($class, $data)
{
    $CI = get_instance();

    if (is_array($data)) {
        $text = '<ul>';
        foreach ($data as $d) {
            $text .= "<li>{$d}</li>";
        }
        $text .= '</ul>';
    } else {
        $text = $data;
    }

    $flashdata_key = $CI->session->flashdata_key.':old:'."alert";
    $CI->session->userdata[$flashdata_key] = "<div class='alert alert-$class'><button class='close' data-dismiss='alert'>x</button> " . $text . "</div>";
}

/**
 * Set_flash_message is used to supply various notifications to the user
 * To display the flashdata use "status_message"
 *
 * @param string $class
 *            'This is usually "error" or "success"
 * @param string $data
 *            This text message to show in the flash notification display.
 * @example $this->session->flashdata('status_message');
 *
 */
function set_flash_message($class, $data)
{
    $CI = get_instance();

    if (is_array($data)) {
        $text = '<ul>';
        foreach ($data as $d) {
            $text .= "<li>{$d}</li>";
        }
        $text .= '</ul>';
    } else {
        $text = "<ul><li>" . $data . "</li></ul>";
    }

    $CI->session->set_flashdata("status_message", "<div class='message'><div class='" . $class . "'>" . $text . "</div></div>");
}

/**
 * Set sthe flash message without needing a redirect
 * @param string $class
 * @param mixed $data
 */
function set_flash_message_now($class, $data)
{
    $CI = get_instance();

    if (is_array($data)) {
        $text = '<ul>';
        foreach ($data as $d) {
            $text .= "<li>{$d}</li>";
        }
        $text .= '</ul>';
    } else {
        $text = "<ul><li>" . $data . "</li></ul>";
    }

    $flashdata_key = $CI->session->flashdata_key.':old:'."status_message";
    $CI->session->userdata[$flashdata_key] = "<div class='message'><div class='" . $class . "'>" . $text . "</div></div>";
}

/**
 * Sets a persistent message on the view.
 *
 * @param string $class
 *            This is usually "error" or "success"
 * @param string $data
 *            The text message to display
 */
function set_static_message($class, $data)
{
    $CI = get_instance();

    if (is_array($data)) {
        $text = '<ul>';
        foreach ($data as $d) {
            $text .= "<li>{$d}</li>";
        }
        $text .= '</ul>';
    } else {
        $text = "<ul><li>" . $data . "</li></ul>";
    }

    $CI->session->set_flashdata("static_message", "<div class='static_" . $class . "'>" . $text . "</div>");
}

function message()
{
    $CI = get_instance();
    echo $CI->session->flashdata('message');
}

/**
 * Queues emails in the email table to be sent using the cron job processEmailQueue
 *
 * @param string $from
 * @param string $from_name
 * @param string $to
 *            The recipient's e-mail address
 * @param string $subject
 *            The subject of the e-mail
 * @param string $body
 *            The body of the -email
 * @param array $cc
 * @param string $filename
 *            - attachement
 * @param int $business_id
 *            - if not passed, we try to use the session business_id
 * @return int insert_id() from the emailLog table
 *
 *
 */
function queueEmail($from, $from_name, $to, $subject, $body, $cc = array(), $attachment = null, $business_id = '', $customer_id = '')
{
    $CI = get_instance();
    $email = new DropLockerEmail();

    $business_id = (empty($business_id)) ? $CI->session->userdata('business_id') : $business_id;
    if (empty($business_id)) {
        throw new Exception("business_id not passed and business_id not set in session");
    }

    if (empty($to)) {
            $error_email_data = array(
                'from' => $from,
                'from_name' => $from_name,
                'to' => $to,
                'subject' => $subject,
                'body' => $body,
                'business_id' => $business_id,
                'customer_id' => $customer_id
            );

            $error_body = "ERROR:<br>Missing 'to' field in queueEmail<BR><BR>DETAILS: <br>" . formatArray($error_email_data);
            $support_email_address = get_business_meta($business_id, 'customerSupport');

            // if for some reason we dont' have a customer support email address, dont bother, their business isn't set up properly then anyway
            if (! empty($support_email_address)) {
                send_email(SYSTEMEMAIL, SYSTEMFROMNAME, $support_email_address, 'Missing "to" field in email', $error_body);
            }

            return 0; // don't want to throw exception, but do want to let them know
                          // throw new \Exception("Missing 'to' in queueEmail");

    }

    if (! empty($customer_id)) {
        $customer = new Customer($customer_id);
    } else {
        // Was OR, but didn't use indexes... switched to UNION -dwb 2018-08-13
        $customer = $CI->db->query(
                "SELECT * FROM customer WHERE business_id = ? AND email = ?"
                . " UNION SELECT * FROM customer WHERE business_id = ? AND sms = ?"
                . " UNION SELECT * FROM customer WHERE business_id = ? AND sms2 = ?",
            array($business_id, $to, $business_id, $to, $business_id, $to))->row();
    }

    if ($customer) {
        $customer_id = $customer->customerID;
        $business = new Business($business_id); // needed for the timezone
        $email->sendDateTime = get_window_delivery_time($customer->emailWindowStart, $customer->emailWindowStop, $business->timezone);
    } else {
        $email->sendDateTime = convert_to_gmt_aprax();
    }

    // if we are missing a to field, it can cause big problems with order processing,
    // so lets send an error email but keep the flow moving


    $email->subject = $subject;
    $email->body = $body;
    $email->subject = $subject;
    $email->to = $to;
    $email->fromEmail = $from;
    $email->fromName = $from_name;
    $email->cc = serialize($cc);
    $email->dateCreated = convert_to_gmt_aprax();
    $email->business_id = $business_id;
    $email->customer_id = $customer_id;
    $email->emailType = ENVIRONMENT;
    $email->status = 'pending';
    $email->save();

    return $email->emailID;
}

/**
 * Gets an email delivery time in GMT from customer's window
 *
 * @param string $start      window start time in GMT
 * @param string $stop       window stpop time in GMT
 * @param string $timezone   business's timezone
 * @param int    $now        (optional) current timestamp, used for testing
 * @param bool   $debug      (optional) if true, prints debug info
 * @return string            the email delivery time in GMT
 */
function get_window_delivery_time($start, $stop, $timezone, $now = null, $debug = false)
{
    if (empty($now)) {
        $now = time();
    }

    // empty window, return now
    if ($start == 0 && $stop == 0) {
        return gmdate("Y-m-d H:i:s", $now);
    }

    $gmtCurrentTime   = gmdate("H:i:s", $now);
    $gmtStartTime     = $start;
    $gmtStopTime      = $stop;

    if ($gmtStopTime == 0) {
        $gmtStopTime = '23:59:59';
    }

    $localCurrentTime = to_timezone($gmtCurrentTime, $timezone, "H:i:s");
    $localStartTime   = to_timezone($gmtStartTime, $timezone, "H:i:s");
    $localStopTime    = to_timezone($gmtStopTime, $timezone, "H:i:s");

    if ($localStopTime == 0) {
        $localStopTime = '23:59:59';
    }

    if ($debug) {
        printf("             Local    (GMT)\n");
        printf("CurrentTime: %s (%s)\n", $localCurrentTime, $gmtCurrentTime);
        printf("  StartTime: %s (%s)\n", $localStartTime, $gmtStartTime);
        printf("   StopTime: %s (%s)\n", $localStopTime, $gmtStopTime);
    }

    // Base time to send later
    $startTimestamp   = strtotime("$gmtStartTime GMT", $now);

    // within the window so send immediately
    if ($localStartTime <= $localCurrentTime && $localCurrentTime < $localStopTime) {
        return gmdate("Y-m-d H:i:s", $now);
    }

    // too early
    if ($localCurrentTime < $localStartTime) {
        return gmdate("Y-m-d H:i:s", $startTimestamp);
    }

    // too late
    if ($localCurrentTime > $localStopTime) { // too late
        if ($startTimestamp < $now) {
            $startTimestamp += 86400; // add 1 day
        }

        return gmdate("Y-m-d H:i:s", $startTimestamp);
    }

    // no way we get here
    return gmdate("Y-m-d H:i:s", $now);
}

/**
 * Sends an email immediatley.
 * Mainly used when sending error reports
 *
 * @param string $from
 * @param string $from_name
 * @param string $to
 *            The recipient's e-mail address
 * @param string $subject
 *            The subject of the e-mail
 * @param string $body
 *            The body of the -email
 * @param array $cc
 * @param array $bcc
 * @param string $filename
 *            - attachement
 * @param
 *            int business_id
 * @param
 *            bool override Send an email regardless of whether or not the current environment is production
 * @return int insert_id() from the emailLog table. If the creation of the e-mail log fails, then false is returned
 *
 */
function send_email($from, $from_name, $to, $subject, $body, $cc = array(), $bcc = array(), $attachment = null, $business_id = '', $override = FALSE)
{
    $CI = get_instance();

    // Note, the following * statement is necessary to send e-mails for some reason.
    $emailObj = new \App\Libraries\DroplockerObjects\DropLockerEmail();

    if (! valid_email($from)) {
        trigger_error("Invalid 'from' address", E_USER_WARNING);
    }
    if (! valid_email($to)) {
        trigger_error("Invalid 'to' address", E_USER_WARNING);
    }

    /*
     * If we are in development environment, we only save the email for viewing in the admin section
     */
    if (ENVIRONMENT == 'production' || $override) {
        $CI->load->library('email');
        $CI->load->library('session');
        $CI->email->clear();

        $CI->email->from($from, $from_name);

        $CI->email->to($to);
        if (! is_null($subject)) {
            $CI->email->subject($subject);
        }
        $CI->email->message($body);
        $CI->email->cc($cc);
        $CI->email->bcc($bcc);
        if (! is_null($attachment)) {
            $CI->email->attach($attachment);
        }
        $CI->email->set_newline("\r\n");

        $emailLog = array();
        $emailLog['business_id'] = $CI->session->userdata("business_id");
        $emailLog['emailType'] = "live";
        $emailLog['to'] = $to;
        $emailLog['subject'] = $subject;
        $emailLog['body'] = $body;

        try {
            if (@$CI->email->send()) {
                $emailLog['status'] = "success";
            } else {
                $emailLog['status'] = 'fail';
                $emailLog['errorMessage'] = $CI->email->print_debugger();
            }
            $CI->db->insert("emailLog", $emailLog);
        } catch (\Database_Exception $e) {
            return false; // Do nothing
        } catch (\Exception $e) {
            $emailLog['status'] = 'serverError';
            $CI->db->insert("emailLog", $emailLog);
        }

        return true;
    } else {
        $email = new \App\Libraries\DroplockerObjects\DropLockerEmail();
        $email->subject = $subject;
        $email->body = $body;
        $email->to = $to;
        $email->fromEmail = $from;
        $email->fromName = $from_name;
        $email->cc = serialize($cc);
        $email->bcc = serialize($bcc);
        $email->dateCreated = date('Y-m-d H:i:s');
        if (empty($business_id)) {
            if (isset($CI->session)) {
                $email->business_id = $CI->session->userdata('business_id');
            } else {
                $email->business_id = null;
            }
        } else {
            $email->business_id = $business_id;
        }
        $email->statusNotes = "Suppressed Development Email";
        $email->emailType = ENVIRONMENT;
        $email->status = 'success';
        $email->sendDateTime = date('Y-m-d H:i:s');

        try {
            $emailID = $email->save();
        } catch (Database_Exception $e) {
            flush();
            die(var_dump($email->properties));
        }

        return true;
    }
}

function send_development_email($subject, $body)
{
    if (empty($subject)) {
        throw new \InvalidArgumentException("'subject' can not be empty");
    } elseif (empty($body)) {
        throw new \InvalidArgumentException("'body' can not be empty");
    } else {
        send_email(DEVEMAIL, "Droplocker Development", DEVEMAIL, $subject, $body, array(), array(), null);
    }
}


function format_stack_trace($stack_trace)
{
    $rtn = "";
    $count = 0;
    foreach ($stack_trace as $frame) {
        $args = "";
        if (isset($frame['args'])) {
            $args = array();
            foreach ($frame['args'] as $arg) {
                if (is_string($arg)) {
                    $args[] = "'" . $arg . "'";
                } elseif (is_array($arg)) {
                    if ($frame['function'] == "call_user_func_array") {
                        $args[] = "Array";
                    } else {
                        $args[] = sprintf("Array{%s}", serialize($arg));
                    }
                } elseif (is_null($arg)) {
                    $args[] = 'NULL';
                } elseif (is_bool($arg)) {
                    $args[] = ($arg) ? "true" : "false";
                } elseif (is_object($arg)) {
                    $args[] = get_class($arg);
                } elseif (is_resource($arg)) {
                    $args[] = get_resource_type($arg);
                } else {
                    $args[] = $arg;
                }
            }
            $args = join(", ", $args);
        }
        $rtn .= sprintf("#%s %s(%s): %s(%s)\n", $count, isset($frame['file']) ? $frame['file'] : '', isset($frame['line']) ? $frame['line'] : '', $frame['function'], $args);
        $count ++;
    }

    return $rtn;
}

/**
 * Sends an e-mail to the developers about an exception that ocurred in the application.
 *
 * @param Exception $e
 * @param string $additional_information
 *            Any additional information to include in the report.
 * @param bool $load_sql_data set to false to disable loading extra info from the database
 */
function send_exception_report($e, $additional_information = "", $load_sql_data = true)
{
    $CI = get_instance();
    $rtn = format_stack_trace($e->getTrace());

    $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
    $ip_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

    $body = "
        <pre>
            Client IP Address: {$ip_addr}
            HTTP Referer: {$referer}
            Request URI: {$_SERVER['REQUEST_URI']}
            Message: {$e->getMessage()}";

    if (get_class($e) == "Database_Exception") {
        $body .= "
            Query: {$e->get_query()}
            Error Number: {$e->get_error_number()}";
    }

    $body .= "
            Line: {$e->getLine()}
            File: {$e->getFile()}
            Trace:
                 $rtn
        </pre>
    ";

    if (isset($CI->customer_id) && $load_sql_data) {
        $CI->load->model("customer_model");
        $customers = $CI->customer_model->get_customer($CI->customer_id);
        $customer = $customers[0];

        $body .= "
            <pre>
                Customer Information
                    Name: {$customer->firstName} {$customer->lastName}
                    Phone: {$customer->phone}
                    SMS: {$customer->sms}
                    E-mail: {$customer->email}
                    Customer Address: {$customer->address}, {$customer->city} {$customer->state} {$customer->zip}
            </pre>
        ";
    }
    if (isset($CI->business)) {
        $body .= "
            <pre>
                Business Information
                    BusinessID: {$CI->business->businessID}
                    Name: {$CI->business->companyName}
                    Website: {$CI->business->website_url}
            </pre>
        ";
    }
    if (isset($CI->buser_id) && is_numeric($CI->buser_id) && $load_sql_data) {
        $employeeID = get_employee_id($CI->buser_id);

        $CI->load->model("employee_model");

        try {
            $employee = new Employee($employeeID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $employee = new stdClass();
            $employee->firstName = "Unknown";
            $employee->lastName = "Unknown";
            $employee->email = "Unknown";
        }
        $body .= "
        <pre>
            Employee Information
                Name : {$employee->firstName} {$employee->lastName}
                Email: {$employee->email}
        </pre>";
    }

    if (! empty($additional_information)) {
        $body .= "
            Additional Information
                {$additional_information}
        ";
    }
    $now = new \DateTime("now", new \DateTimeZone("America/Los_Angeles"));
    error_log($body);

    send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "Exception Report - {$now->format("Y-m-d H:i:s")} - ({$_SERVER['REQUEST_URI']})", $body);
}


/**
 * Sends a 404 error report to the Droplocker administrators
 *
 * @param string $requested_url
 *            The URL requetsed from the client
 * @param string $referred_url
 *            Optional. The URL referred to from the requested URL.
 */
function send_404_report($requested_url, $referred_url)
{
    $CI = get_instance();
    $body = "
        <pre>
        Client I.P. Address: {$CI->session->userdata['ip_address']}
        Requested URL: $requested_url
        Referred URL: $referred_url
        </pre>
    ";
    send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "404 Error Report ($requested_url)", $body, array());
}

/**
 * Sends an email to the developers about a PHP error.
 *
 * @param array $error
 *            Expects an array with the following keys:
 *            'type'
 *            'message'
 *            'file'
 *            'line'
 */
function send_error_report($error, $title = "PHP Error Report")
{
    if (!is_array($error)) {
        $error = array(
            'message' => $error,
        );
    }

     $body = "
        <pre>
            Error Type: {$error['type']}
            Message: {$error['message']}
            File: {$error['file']}
            Line: {$error['line']}
        </pre>
    ";

    if (!class_exists("\App\Libraries\DroplockerObjects\DroplockerObject", false)) {
        require dirname(__FILE__) . "/../libraries/objects/DroplockerObject.php";
    }

    if (!class_exists("\App\Libraries\DroplockerObjects\DropLockerEmail", false)) {
        require dirname(__FILE__) . "/../libraries/objects/DropLockerEmail.php";
    }

    $now = new \DateTime("now", new \DateTimeZone("America/Los_Angeles"));
    error_log($body);

    send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "$title - {$now->format("Y-m-d H:i:s")} - ({$_SERVER['REQUEST_URI']})", $body);
}

/**
 * Wrapper for queueEmail() to business's customerSupport address
 *
 * @param string $subject
 * @param string $message
 * @param int $business_id
 * @param int $customer_id
 * @return boolean int false if the email was not inserted into the database, else returns the emailID
 */
function send_admin_notification($subject, $message, $business_id = null, $customer_id = null)
{
    $CI = get_instance();
    $business_id = !empty($business_id) ? $business_id : $CI->business_id;

    if (empty($business_id)) {
        throw new Exception("business_id is not set");
    }
    $to = get_business_meta($business_id, "customerSupport");
    $emailID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $to, $subject, $message, array(), null, $business_id, $customer_id);

    return $emailID;
}

/**
 * Wrapper for queueEmail() to business's operations address
 *
 * @param string $subject
 * @param string $message
 * @param int $business_id
 * @param int $customer_id
 * @return boolean int false if the email was not inserted into the database, else returns the emailID
 */
function send_operations_notification($subject, $message, $business_id = null, $customer_id = null)
{
    $CI = get_instance();
    $business_id = !empty($business_id) ? $business_id : $CI->business_id;

    if (empty($business_id)) {
        throw new Exception("business_id is not set");
    }

    $to = get_business_meta($business_id, "operations");
    $emailID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $to, $subject, $message, array(), null, $business_id, $customer_id);

    return $emailID;
}

/**
 * The following function is *, do not use.
 *
 * @deprecated This function is useless and does not follow MVC design.
 *             check_fields helper method will unset any values that are not in the table
 *             useful for passing $_POST arrays.
 *             For example, this method will unset $_POST['submit] before doing an insert
 *
 * @param
 *            array or object $options
 * @param string $table
 * @return array of objects
 *
 */
function check_fields($options, $table)
{
    // if we have an object :: convert to an array
    if (! is_array($options)) {
        foreach ($options as $key => $value) {
            $array[$key] = $value;
        }
    } else {
        $array = $options;
    }

    $out = array();

    $CI = get_instance();
    $fields = $CI->db->list_fields($table);
    foreach ($fields as $field) {
        if (key_exists($field, $array))
            $out[$field] = $array[$field];
    }

    return $out;
}

/**
 * buser_id is pulled from the business_employee table.
 * you need to get the true employee data from that
 *
 * @param int $buser_id
 * @param
 *            int employeeID
 */
function get_employee_id($buser_id)
{
    $CI = get_instance();
    $CI->load->model('employee_model');

    return $CI->employee_model->get_employee_id($buser_id);
}

/**
 * Gets the current logged in employee
 */
function get_current_employee_id()
{
    $CI = get_instance();
    if (empty($CI->session)) {
        return null;
    }
    $CI->load->model('employee_model');

    $buser_id = $CI->session->userdata('buser_id');
    return $buser_id ? $CI->employee_model->get_employee_id($buser_id) : null;
}

/**
 * buser_id is the business_employee_id
 * So I have to get the employee id and then the name
 */
function get_employee_name($buser_id)
{
    $CI = get_instance();
    $sql = "SELECT firstName, lastName FROM employee
            JOIN business_employee b ON employeeID = employee_id
            WHERE b.ID = ?";

    $query = $CI->db->query($sql, array($buser_id));
    $user = $query->result();

    return $user ? $user[0]->firstName . " " . $user[0]->lastName : NULL;
}

/**
 * Used to get the current status of an order
 *
 * @todo put this in the orderStatus_model
 * @param int $order_id
 * @return int orderStatusOption_id
 */
function current_order_status($order_id)
{
    $CI = get_instance();
    $sql = "SELECT orderStatusOption_id FROM orders WHERE orderID = {$order_id}";
    $q = $CI->db->query($sql);
    $res = $q->row();

    return $res->orderStatusOption_id;
}

/**
 * Used to get the locker_id of an order
 *
 * @todo put this is the locker_model
 * @param int $order_id
 * @return int locker_id
 */
function get_order_locker($order_id)
{
    $CI = get_instance();
    $sql = "SELECT locker_id FROM orders WHERE orderID = {$order_id}";
    $q = $CI->db->query($sql);
    $res = $q->result();

    return $res[0]->locker_id;
}

/**
 * function to find the distance between locations given the lat and long
 * http://stackoverflow.com/questions/3349808/php-mysql-get-locations-in-radius-users-location-from-gps
 */
function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
    $lat1 = (float)$lat1;
    $lon1 = (float)$lon1;
    $lat2 = (float)$lat2;
    $lon2 = (float)$lon2;
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);
    if ($unit == "K") {
        return ($miles * 1.609344);
    } elseif ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
} // end function

/**
 * logs emails that are sent
 *
 *
 * @param unknown_type $subject
 * @param unknown_type $body
 */
function log_email($subject, $body)
{
    $CI = get_instance();
    $options['subject'] = $subject;
    $options['body'] = $body;
    $options['business_id'] = $CI->business->businessID;
    $CI->db->insert('emailLog', $options);

    return $CI->db->insert_id();
}

/**
 * write to log file.
 *
 * @param string $message
 */
function log_error($message)
{
    error_log(__METHOD__ . " : " . $message);
}

/**
 * The following undocumented function is *, do not use.
 *
 * @param type $a
 * @param type $b
 * @return int
 */
function sortLocation($a, $b)
{
    if (is_numeric($a) && is_numeric($b)) {
        if ($a == $b) {
            return 0;
        }

        return ($a < $b) ? - 1 : 1;
    } else {
        return strcasecmp($a, $b);
    }
}

/**
 * The following function is *, do not use.
 *
 * @deprecated The built in Codeigniter dropdown functionality should be used instead.
 *             outputs options for a select input
 *
 * @param array $suppliers
 */
function supplier_dropdown($suppliers, $selectedValue = '')
{
    foreach ($suppliers as $supplier) {
        if ($selectedValue == $supplier->supplierID) {
            echo "<option selected value='" . $supplier->supplierID . "'>" . $supplier->companyName . "</option>";
        } else {
            echo "<option value='" . $supplier->supplierID . "'>" . $supplier->companyName . "</option>";
        }
    }
}

/**
 * Returns countires used as an array for use in dropdowns
 *
 * @return Array
 */
function get_countries_as_array()
{
    require APPPATH . '/config/states.php';
    return $countries;
}

/**
 * Gets all states/provinces keyed to the country name
 * @return array
 */
function get_all_states_by_country_as_array()
{
    require APPPATH . '/config/states.php';
    return $states;
}

/**
 * The following function is *.
 * Do not use.
 * displays a dropdown of all the lockers
 *
 * @param int $locker_id
 */
function lockerDropdown($locker_id)
{
    $CI = get_instance();
    $sql = "select * from locker
                    join location on locationID = location_id
                    where business_id = $CI->business_id
                    order by address, lockerName";
    $lockers = $CI->db->query($sql)->result();

    ?>
<select name="locker">
    <?php foreach($lockers as $l): ?>
        <option value="<?= $l->lockerID ?>"
        <?php if($locker_id == $l->lockerID) echo ' SELECTED' ?>><?= $l->address ?> - <?= $l->lockerName ?></option>
        <?php endforeach; ?>
    </select>
<?php
}

/**
 * helper to get the uri->segment
 *
 * The argument passed is the location in the url string.
 *
 * @param int $int
 */
function seg($int)
{
    $CI = get_instance();

    return $CI->uri->segment($int);
}

/**
 * The following fucntion is *.
 * Do not use.
 * Shortcut helper for loading models
 *
 * Will work when passing the model filename with "_model" included in the $model variable or now.
 *
 * @param string $model
 * @param string $short
 */
function lm($model, $short = '')
{
    if (DEV) {
        throw new Exception("Using deprecated lm() funtion.");
    }

    $CI = get_instance();
    $model = str_replace("_model", "", $model);
    $CI->load->model($model . "_model", $short);
}

/**
 *
 *
 * update_customer_meta method is used to supply additional customer information using the customerMeta table.
 *
 * This allows us to add customer information to the database without having to alter the customer data table structure.
 * if customer_id and key exists, the value will be updated. If the value is not provided the row will be deleted. If the customer_id and key
 * does not exists, the row will be inserted
 *
 * <code>
 * update_customer_meta($this->customer_id, 'favorite_brand', 'Kmart');
 * </code>
 *
 * @param int $customer_id
 * @param string $key
 * @param string $value
 * @return int
 */
function update_customer_meta($customer_id, $key, $value = '')
{
    return update_meta('customer', $customer_id, $key, $value);
}

/**
 * Gets a single row from the customer meta table based on a key
 *
 * @param int $customer_id
 * @param string $key
 * @param string $default
 * @return string
 */
function get_customer_meta($customer_id, $key, $default = null)
{
    return get_meta('customer', $customer_id, $key, $default);
}

/**
 * The following function is *, do not use.
 *
 * @deprecated update_location_meta method is used to supply additional location information using the locationMeta table.
 *
 *             This allows us to add location information to the database without having to alter the location data table structure.
 *             if location_id and key exists, the value will be updated. If the value is not provided the row will be deleted. If the location_id and key
 *             does not exists, the row will be inserted
 *
 *             <code>
 *             update_location_meta($location_id, 'DC_min', '15'); //sets the minimum dry clean total to $15
 *             </code>
 *
 * @param int $location_id
 * @param string $key
 * @param string $value
 * @return int
 */
function update_location_meta($location_id, $key, $value = '')
{
    return update_meta('location', $location_id, $key, $value);
}

function update_server_meta($key, $value)
{
    $CI = get_instance();
    if ($value == '') {
        $CI->db->delete('server_meta', array(
            "key" => $key
        ));

        return $CI->db->affected_rows();
    }

    $q = $CI->db->get_where('server_meta', array(
        'key' => $key
    ));
    if ($res = $q->result()) {
        $CI->db->update('server_meta', array(
            'value' => $value
        ), array(
            'key' => $key
        ));

        return $CI->db->affected_rows();
    } else {
        $CI->db->insert('server_meta', array(
            'key' => $key,
            'value' => $value
        ));

        return $CI->db->insert_id();
    }
}

/**
 * The following function is *, do not use
 *
 * @param type $key
 * @return null
 */
function get_server_meta($key)
{
    $CI = get_instance();
    $q = $CI->db->get_where('server_meta', array(
        "key" => $key
    ));
    $value = $q->result();
    if (array_key_exists(0, $value)) {
        return $value[0]->value;
    } else {
        return null;
    }
}

/**
 * The following function is *, do not use.
 *
 * update_business_meta method is used to supply additional business information using the businessMeta table.
 *
 * This allows us to add business information to the database without having to alter the business data table structure.
 * if business_id and key exists, the value will be updated. If the value is not provided the row will be deleted. If the business_id and key
 * does not exists, the row will be inserted
 *
 * <code>
 * update_business_meta($business_id, 'contact', 'Matt');
 * </code>
 *
 * @param int $business_id
 * @param string $key
 * @param string $value
 * @return int
 */
function update_business_meta($business_id, $key, $value = '')
{
    return update_meta('business', $business_id, $key, $value);
}

/**
 * The following function is *.
 * do not use
 * gets a value from the businessMeta table
 *
 * @example $supportEmail = get_business_meta($this->business_id, 'customerSupport');
 * @param int $business_id
 * @param string $key
 * @param string $default
 * @return string
 */
function get_business_meta($business_id, $key, $default = null)
{
    return get_meta('business', $business_id, $key, $default);
}

function get_current_business_meta($key, $default = null)
{
    return get_business_meta(get_current_business_id(), $key, $default);
}

/**
 * The following fucntion is *, do not use.
 * get_customer_meta method gets data from the customerMeta table
 *
 * <code>
 * get_customer_meta($this->customer_id, 'favorite_brand');
 * </code>
 *
 * @param int $customer_id
 * @param string $key
 * @param string $default
 * @return string
 */
function get_meta($type, $id, $key, $default = null)
{
    $types = array(
        "customer",
        "location",
        "business"
    );
    if (! in_array($type, $types)) {
        die('Type not in Types' . __METHOD__);
    }

    $CI = get_instance();

    $row = $CI->db->query("SELECT * FROM {$type}Meta WHERE {$type}_id = ? AND `key` = ?", array($id, $key))->row();
    return $row ? $row->value : $default;
}

/**
 * The following function is *.
 * Do not use.
 * updates a meta table. This is called from any function that uses a meta table such as business or customer
 *
 * @param string $type
 * @param int $id
 * @param string $key
 * @param string $value
 */
function update_meta($type, $id, $key, $value = '')
{
    $types = array(
        "customer",
        "location",
        "business"
    );
    if (! in_array($type, $types)) {
        die('Type not in Types' . __METHOD__);
    }

    $CI = get_instance();
    if ($value == '') {
        $CI->db->delete($type . 'Meta', array(
            $type . '_id' => $id,
            "key" => $key
        ));

        return $CI->db->affected_rows();
    }

    $q = $CI->db->get_where($type . 'Meta', array(
        $type . '_id' => $id,
        'key' => $key
    ));
    if ($res = $q->result()) {
        $CI->db->update($type . 'Meta', array(
            'value' => $value
        ), array(
            $type . '_id' => $id,
            'key' => $key
        ));

        return $CI->db->affected_rows();
    } else {
        $CI->db->insert($type . 'Meta', array(
            $type . '_id' => $id,
            'key' => $key,
            'value' => $value
        ));

        return $CI->db->insert_id();
    }
}

/**
 * The following function is *, do not use.
 * helper to make writing queries faster
 *
 * @deprecated use the codeigniter function call, form example: $this->db->query
 *
 * @param string $sql
 */
function query($sql)
{
    $CI = get_instance();
    $q = $CI->db->query($sql);
    if ($CI->db->_error_message()) {
        throw new Exception($CI->db->_error_message());
    }

    return $q->result();
}

/**
 * Formats an amount of seconds into HH:mm:ss
 *
 * @param type $seconds
 * @return string
 */
function sec_to_time($seconds)
{
    $hours = floor($seconds / 3600);
    $minutes = floor($seconds % 3600 / 60);
    $seconds = $seconds % 60;

    return sprintf("%d:%02d:%02d", $hours, $minutes, $seconds);
}

/**
 * Convert a local date into a GMT date range using the current timezone
 *
 * @param string $startDate
 * @param string $endDate
 * @return array:
 */
function get_date_range($startDate, $endDate, $addTime = true)
{
    if (empty($endDate)) {
        $endDate = $startDate;
    }

    if ($addTime) {
        $start = gmdate('Y-m-d H:i:s', strtotime("$startDate 00:00:00"));
        $end = gmdate('Y-m-d H:i:s', strtotime("$endDate 23:59:59"));
    } else {
        $start = gmdate('Y-m-d H:i:s', strtotime($startDate));
        $end = gmdate('Y-m-d H:i:s', strtotime($endDate));
    }

    return array($start, $end);
}


/*
 * checks the local, and if its not US changes Lbs to Kgs, etc. pass it the amount to check the locale and spit out lbs or kgs can use this to display Lb, Lbs, Pound, Pounds, Kg, Kgs, Kilogram, Kilograms
 */
function weight_system_format($weight = '', $full_unit_name = false, $force_plural = false, $force_weight = false)
{
    $CI = get_instance();
    $CI->load->helper('inflector');
    // gets the current locale
    $locale_string = setlocale(LC_ALL, 0);

    // using abbreviation or full name
    if ($full_unit_name) {
        $metric = 'Kilogram';
        $american = 'Pound';
    } else {
        $metric = 'Kg';
        $american = 'Lb';
    }

    // default is metric, ie. MOST of the world.
    $units = $metric;

    // change it to american if we have a US locale, or none specified for some reason (falls back to US)
    if (strpos($locale_string, 'en_US') !== false) {
        $units = $american;
    } elseif (strpos($locale_string, '_US.') !== false) {
        $units = $american;
    } elseif (empty($locale_string) || strlen($locale_string) < 2) {
        $units = $american;
    }

    // if weight is passed, and it's more than 1, make it plural
    if ($force_plural || (is_numeric($weight) && $weight > 1)) {
        $units = plural($units);
    }

    if (! empty($weight) || $force_weight) {
        return $weight . ' ' . $units;
    } else {
        return $units;
    }
}

/**
 * Format currency using and let business override monetary symbol
 *
 * @param int $business_id business id
 * @param string $strformat format used for money_format function
 * @param float $number the amount
 * @param int $decimals the number of decimal places
 * @param array $options extra options
 */
function format_money_symbol($business_id, $strformat, $number, $overRide_decimals=-1, $options = array())
{
    $CI = get_instance();

    // we only allow "." as decimal symbol
    $number = str_replace(",", "", $number);
    
    $default_decimals = 2;
 
    if (!$business_id != '') {
        throw new \Exception('Missing required business_id in format_money_symbol()');
    }
 
    // cache the business monetary settings
    static $money_cache = array();
    
    if (!isset($money_cache[$business_id])) {
        $money_cache[$business_id] = array(
            'symbol'            => get_business_meta($business_id, 'money_symbol_str'),
            'position'          => get_business_meta($business_id, 'money_symbol_position'),
            'decimal_positions' => get_business_meta($business_id, 'decimal_positions', $default_decimals),
        );
    }
 
    $decimals = $overRide_decimals > -1 ? $overRide_decimals : $money_cache[$business_id]["decimal_positions"];
 
    $formatter = new \NumberFormatter($CI->business->locale, \NumberFormatter::DECIMAL); 
    $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $decimals); 
    $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $decimals); 
  
    if (!empty($options["hide_symbol"])) {
        $number = $formatter->format($number);
        return $number;
    }

    // there is not an override
    if (empty($money_cache[$business_id]['symbol'])) {
       $strformat = "%.{$decimals}n";
       return money_format($strformat, $number);
    }

    $number = $formatter->format($number);

    $symbol = $money_cache[$business_id]['symbol'];
    $position = $money_cache[$business_id]['position'];
    $price = $position ? "$symbol $number" : "$number $symbol";
    if (isset($options['shrink_money_symbol'])) {
        $price = shrink_money_symbol($price);
    }
    return $price;
}

/**
 * Format numbers using business settings
 *
 * @param int $business_id business id
 * @param float $number
 * @param int $decimals the number of decimal places
 */

function number_format_locale($number, $decimals=2)
{
    return number_format($number, $decimals);
}

/**
 * Generates the account url for a business
 * @param Object $business
 * @param string $proto
 * @param boolean $allowQA
 * @return string
 */
function get_business_account_url($business, $proto = 'https://', $allowQA = true)
{
    $extension = isset($_SERVER['SERVER_NAME']) ? pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION) : null;

    if (in_bizzie_mode()) {
        return $business->subdomain ? $proto . $business->subdomain . "." . getDomain("http://" . $_SERVER['HTTP_HOST']) : '';
    }

    if (empty($business->website_url)) {
        return $business->slug ? $proto . $business->slug.".droplocker.".$extension : '';
    }

    // let QA users to log in as a customer using the QA environment.
    if ($extension == "qa" && $allowQA) {
        // first part of domain
        return $proto .  "myaccount.". current(explode('.', $business->website_url, 2)) . '.qa';
    }

    return $proto . "myaccount.".$business->website_url;
}


function form_yes_no($name, $value = null, $separator = ' ', $extra = '')
{
    static $yes, $no = null;
    if (is_null($yes) || is_null($no)) {
        $yes = get_translation("yes", "form_yes_no", array(), "Yes");
        $no = get_translation("no", "form_yes_no", array(), "No");
    }

	$out = array();
	$out[] = sprintf('<label>%s %s</label>', $yes, form_radio($name, 1, $value, $extra));
	$out[] = sprintf('<label>%s %s</label>', $no, form_radio($name, 0, !$value, $extra));

	return implode($separator, $out);
}

function form_radios($name, $options, $default, $separator = ' ')
{
	$out = array();
	foreach($options as $key => $value) {
		$out[] = '<label>' . form_radio($name, $key, $key == $default) . ' ' . $value . '</label>' . $separator;
	}

	return implode($separator, $out);
}

function form_hours($business_id, $name, $selected = NULL)
{
    $hours = array();
    foreach (range(0, 23) as $hour) {
        $time = sprintf('%02d:00:00', $hour);
        $tz_time =  convert_from_gmt_aprax($time, "H:i", $business_id);
        $hours[$time] = $tz_time;
    }
    asort($hours);

    return form_dropdown($name, $hours, $selected);
}

function form_days($business_id, $name, $selected = NULL)
{
    $days = array();
    for($i = 1; $i <= 7; $i ++) {
        $days[$i] = date("D", strtotime("sunday +$i days"));
    }

    return form_dropdown($name, $days, $selected);
}

/**
 * Gets a random testimonial for display in the footer
 *
 * @param int $business_id
 * @return string
 */
function get_random_testimonial($business_id)
{
    $CI = get_instance();

    $out = '';
    $testimonial = $CI->db->query('SELECT * FROM testimonial WHERE business_id = ? ORDER BY RAND() LIMIT 1', array($business_id))->row();
    if ($testimonial) {
        $out = substr($testimonial->testimonial, 0, 250) . '<a href="/main/testimonials#t' . $testimonial->testimonialID . '" style="color: #fff">...</a>';
    }

    return $out;
}

/**
 * Strips characters not valid in a phone number
 * @param string $sms
 */
function clean_phone_number($sms)
{
    return preg_replace('/[^+0-9]/', '', $sms);
}

/**
 * Given a price with symbol, makes smaller money symbols that contain letters
 * @param string $string
 */
function shrink_money_symbol($price)
{
   return preg_replace('/[a-z]+/i', '<small>$0</small>', $price);
}

/**
 * Recursively remove files from dir
 * @param string $dir
 * @return bool true on success or false on failure
 */
function recursive_rmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir."/".$object)) {
                    if (!recursive_rmdir($dir."/".$object)) {
                        return false;
                    }
                } else {
                    if (!unlink($dir."/".$object)) {
                        return false;
                    }
                }
            }
        }
        return rmdir($dir);
    } else {
        return unlink($dir);
    }
}

/**
 * Prints human-readable information about a variable inside a pre-formatted HTML block
 * @param mixed $expression
 */
function pr($expression)
{
    echo '<pre>'; print_r($expression); echo '</pre>';
}


/**
 * The following function will tell if the date is a holiday for the business.
 *
 * @param string $date.  Date or time to check if is holiday for the business.
 * @param int $business_id.
 * @return true if date is a holiday for the business else false.
 */
function is_holiday($date, $business_id)
{
    $CI = get_instance();
    $findHoliday = "select 1 from holiday
                            where date = ?
                            and business_id = ?
                            and delivery = 0";
    $holiday = $CI->db->query($findHoliday, array(date("Y-m-d", strtotime($date)), $business_id))->result();
    return !empty($holiday);
}

/**
 * The following function will do a null coalesce.
 *
 * @param string $string1. 
 * @param string $string1. 
 * @return string1 if not empty else string2.
 */
function coalesce($string1, $string2) 
{
    return $string1 ? $string1 : $string2;
}

function arrayToString($arr, $propSep = ":", $listSep = ", ") {
    $comps = array();
    foreach ($arr as $key => $value) {
        $comps[] = $key . $propSep . $value;
    }

    return implode($listSep, $comps);
}

function utf8ize($d, $path = '', $log = array()) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $res = utf8ize($v, "$path/$k", $log);
            $d[$k] = $res['val'];
            
            $log = $res['log'];
        }
    } else if (is_string ($d)) {
        $new = utf8_encode($d);
        
        if ($new !== $d) {
            $log[] = __FUNCTION__ . " -> $path";
        }
        
        return array(
            'val' => $new,
            'log' => $log,
        );
    }

    return array(
        'val' => $d,
        'log' => $log,
    );
}

function cronLog($method, $res)
{
    $CI = get_instance();
    $CI->db->insert('cronLog', array('job' => $method, 'results' => serialize($res)));
}

function getMemoryInfo()
{
    $mem_limit = shorthand_to_bytes(ini_get('memory_limit'));
    
    $mu = memory_get_usage();
    $ma = memory_get_usage(true);
    $pu = memory_get_peak_usage();
    $pa = memory_get_peak_usage(true);
    
    $fma = humanFriendlySize($ma);
    $fpa = humanFriendlySize($pa);
    
    if ($mem_limit < 1) {
        $musage = $pusage = $fmusage = $fpusage = 'unknown';
    } else {
        $musage = (string)round($ma / $mem_limit, 2);
        $pusage = (string)round($pa / $mem_limit, 2);
        
        $fmusage = ($musage * 100) . "%";
        $fpusage = ($pusage * 100) . "%";
    }
    
    $memory = array(
        'summary' => "$fma ($fmusage) -> $fpa ($fpusage)",
        'limit' => array(
            'raw' => $mem_limit,
            'friendly' => humanFriendlySize($mem_limit),
        ),
        'memory' => array(
            'used' => array(
                'raw' => $mu,
                'friendly' => humanFriendlySize($mu),
            ),
            'allocated' => array(
                'raw' => $ma,
                'friendly' => humanFriendlySize($ma),
            ),
            'usage' => array(
                'raw' => $musage,
                'friendly' => $fmusage,
            ),
        ),
        'peak' => array(
            'used' => array(
                'raw' => $pu,
                'friendly' => humanFriendlySize($pu),
            ),
            'allocated' => array(
                'raw' => $pa,
                'friendly' => humanFriendlySize($pa),
            ),
            'usage' => array(
                'raw' => $pusage,
                'friendly' => $fpusage,
            ),
        ),
    );

    return $memory;
}

function shorthand_to_bytes ($size_str)
{
    switch (substr ($size_str, -1))
    {
        case 'M': case 'm': return (int)$size_str * 1048576;
        case 'K': case 'k': return (int)$size_str * 1024;
        case 'G': case 'g': return (int)$size_str * 1073741824;
        default: return $size_str;
    }
}

function humanFriendlySize($bytes, $decimals = 2) {
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');

    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor < 1) {
        $decimals = 0;
    }

    $unit = $size[$factor];
    return sprintf("%.{$decimals}f $unit", $bytes / pow(1024, $factor));
}

function secondsToHours($seconds, $showDays = false)
{
    $template = "%02d:%02d:%02d";
    $data = array();
    
    if ($showDays) {
        $grouper = 3600 * 24;
        
        $d = floor($seconds/$grouper);
        $seconds %= $grouper;
        
        $template = "%d days $template";
        $data[] = $d;
    }
    
    $grouper = 3600;
    $h = floor($seconds/$grouper);
    $seconds %= $grouper;
    $data[] = $h;

    $grouper = 60;
    $m = floor($seconds/$grouper);
    $seconds %= $grouper;
    $data[] = $m;
    $data[] = $seconds;
    
    array_unshift($data, $template);

    return call_user_func_array("sprintf", $data);
}

/**
 * until switch to php 7, array_column that supports objects and arrays
 */
function array_column_7 ($records, $col) {
    $cols = array_fill(0, count($records), $col);
    
    return array_map(function($e, $_col) {
        return is_object($e) ? $e->$_col : $e[$_col];
    }, $records, $cols);    
}

function array_columns_multi($records, $column_keys) {
    $records = stdobj_to_array($records);
    $column_keys = array_flip($column_keys);
    
    $result = array();
    foreach($records as $key => $record) {
        $result[$key] = array_intersect_key($record, $column_keys);
    }

    return $result;
}

function array_columns($record, $column_keys) {
    $record = stdobj_to_array($record);
    $column_keys = array_flip($column_keys);
    
    return array_intersect_key($record, $column_keys);
}

function array_to_stdobj($arr)
{
    if (is_array($arr)) {
        return json_decode(json_encode($arr));
    } else {
        return $arr;
    }
}

/**
 * Do not use if you want to preserve data types
 */
function stdobj_to_array($obj)
{
    if (method_exists($obj, 'to_array')) {
        return $obj->to_array();
    } else if ($obj instanceof stdClass) {
        return json_decode(json_encode($obj), true);
    } else {
        return $obj;
    }    
}

/**
  * Non "web page only" version of checkAccess, let the controller decide what to do
  * 
  * @param int|string $role : If int, the business employee ID. If string, the name of the role. If null, retrieves the roll for the logged in user.
  */
 function checkAccessForApi($aclRole, $role = null)
 {
     $CI = get_instance();
     
     $message = '';
     $access = $CI->zacl->check_acl($aclRole, $role);
     
     if (!$access) {
         $sqlGetRoles = "select name from aclresources
                             join acl on acl.resource_id = aclresources.id
                             join aclroles on aclroles.aclID = type_id
                             where resource = ?
                             and action = 'allow'";
         $q = $CI->db->query($sqlGetRoles, array(
             $aclRole
         ));
         $roles = $q->result();
         
         $role_list = implode(' or ', array_map(function($val){
             return $val->name;
         }, $roles));
         
         $message = "You must be a $role_list to have access to this feature. This feature is protected by aclrole: $aclRole";
     }
 
     return array(
         "status" => $access,
         "message" => $message,
     );
 }
 
 function trimArray($Input){
 
    if (!is_array($Input)) {
        return trim($Input);
    }
    
    return array_map('TrimArray', $Input);
}

function getSortedNameForm($fieldList, $fieldSeparator = "\n", $domain = 'global')
{
    $formTemplateKey = 'nameComponents';
    $formTemplateDefault = 'firstName||lastName';
    
    return getSortedForm($formTemplateKey, $formTemplateDefault, $fieldList, $fieldSeparator, $domain);
}

function getSortedAddressForm($fieldList, $fieldSeparator = "\n", $domain = 'global', $group_size = 1)
{
    $formTemplateKey = 'addressComponents';
    $formTemplateDefault = 'address1||address2||city||state||zip||country';
    
    return getSortedForm($formTemplateKey, $formTemplateDefault, $fieldList, $fieldSeparator, $domain, $group_size);
}

function getSortedForm($formTemplateKey, $formTemplateDefault, $fieldList, $fieldSeparator = "\n", $domain = 'global', $group_size = 1) {
    $translator = '__d';
    
    $translatorParams = array(
        $formTemplateKey,
        $formTemplateDefault,
    );
    
    switch ($domain) {
        case 'current':
            $translator = '__';
            break;
        case 'view':
            $translator = 'get_translation_for_view';
            
            $translatorParams[] = array();//substitutions
            $translatorParams[] = getCallingView();
            break;
        default:
            array_unshift($translatorParams, $domain);
            break;
    }
        
    $formTemplate = explode('||', call_user_func_array($translator, $translatorParams));

    $sortedForm = '';
    $counter = 0;
    foreach ($formTemplate as $field) {
        $sortedForm .= $fieldList[$field];
        
        $counter++;
        if ($counter % $group_size == 0) {
            $sortedForm .= $fieldSeparator;
        }
    }
    
    return $sortedForm;
}

/**
 * returns filepath of view calling the function
 */
function getCallingView()
{
    $viewPath = '';
    
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    foreach ($backtrace as $step) {
        if (getViewName($step['file'])) {
            $viewPath = $step['file'];
            break;
        }
    }

    return $viewPath;
}

/**
 * Returns the languageView_name for the given filepath
 */
function getViewName($filepath){
    //The following conditional determines which regex to use to parse the view path from the file path based on the operating system.
    // If the operating system is windows, then backward slashes are used. Otherwise, forward slashes are used.
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $regex = "#" . str_replace("/", '\\\\', APPPATH) . 'views\\\\(.*).php#';
    } else {
        $regex = "#" . APPPATH . "views/(.*).php#";
    }
    preg_match($regex, $filepath, $matches);
    $languageView_name = $matches[1];
    
    return $languageView_name;
}

function get_property_lb($o, $prop)
{
    return (is_array($o)) ? $o[$prop] : $o->$prop;
}

function getPersonName($p, $filter = true, $first_name_field = 'firstName', $last_name_field = 'lastName')
{
    if (!function_exists('no__op')) {
        function no__op($param) {return $param;};
    }
    
    $filterFcn = 'no__op';
    if ($filter) {
        if ($filter === true) {
            $filterFcn = 'ucwords';
        } else {
            $filterFcn = $filter;
        }
    }
    
    $first_name = $filterFcn(get_property_lb($p, $first_name_field));
    $last_name = $filterFcn(get_property_lb($p, $last_name_field));

    if (!$first_name && !$last_name) {
        return "";
    }
    
    return __d('global', 'person_name', '%firstName% %lastName%', array(
        'firstName'=> $first_name, 
        'lastName' => $last_name,
    ));
}

function getFormattedAddress($data, $address1_field = 'address1', $address2_field = 'address2', $city_field = 'city', $state_field = 'state', $zip_field = 'zip')
{
    $substitutions = array(
        'address1' => get_property_lb($data, $address1_field),
        'address2' => get_property_lb($data, $address2_field),
        'city' => get_property_lb($data, $city_field),
        'state' => get_property_lb($data, $state_field),
        'zip' => get_property_lb($data, $zip_field),
    );
    
    if (!count(array_filter($substitutions))) {
        return "";
    }
    
    return __d('global', 'address_format', '%address1% %address2%, %city%, %state%, %zip%', $substitutions);
}

function nameForTableHeader($format = "<th>%s</th>", $first_name_key = "first_name", $last_name_key = "last_name") 
{
    $fieldList = array();
    $fieldList['firstName'] = sprintf($format, __($first_name_key,"First Name"));
    $fieldList['lastName'] =  sprintf($format, __($last_name_key,"Last Name"));

    return getSortedNameForm($fieldList);
};

function nameForRow ($p, $format = "<td>%s</td>", $first_name_field = 'firstName', $last_name_field = 'lastName' )
{
    $fieldList = array();
    $fieldList['firstName'] = sprintf($format, get_property_lb($p, $first_name_field));
    $fieldList['lastName'] =  sprintf($format, get_property_lb($p, $last_name_field));

    return getSortedNameForm($fieldList);
};

function str_starts_with_lb($str, $prefix)
{
    return strpos($str, $prefix) === 0;
}

function str_starts_with_any_lb($str, $prefixes)
{
    $res = false;
    
    $prefixes = (array) $prefixes;
    foreach ($prefixes as $prefix) {
        if (str_starts_with_lb($str, $prefix)) {
            $res = true;
            break;
        }
    }
    
    return $res;
}

function getLocalDateTimeInfo($date_gmt, $business_id)
{
    $dateInfo = array();

    $dateInfo['iso'] = convert_from_gmt_aprax($date_gmt, 'c', $business_id);
    $dateInfo['date'] = substr($dateInfo['iso'], 0, 19);
    $dateInfo['tz'] = substr($dateInfo['iso'], -6);

    return $dateInfo;
}

function lb_flatten($arr)
{
    $result = array();
    array_walk_recursive($arr,function($v, $k) use (&$result){ $result[] = $v; });
    
    return $result;
}

function get_profiler_benchmarks()
{
    $CI =& get_instance();
    $profile = array();
    
    foreach ($CI->benchmark->marker as $key => $val)
    {
        // We match the "end" marker so that the list ends
        // up in the order that it was defined
        if (preg_match("/(.+?)_end/i", $key, $match))
        {
            if (isset($CI->benchmark->marker[$match[1].'_end']) AND isset($CI->benchmark->marker[$match[1].'_start']))
            {
                $key_friendly = ucwords(str_replace(array('_', '-'), ' ', $match[1]));
                $profile[$key_friendly] = $CI->benchmark->elapsed_time($match[1].'_start', $key);
            }
        }
    }
    
    return $profile;
}

function get_profiler_query()
{
    $query_limit = 6;
    
    $CI =& get_instance();
    
    $info = array();
    $dbs = array();
    $total_time = 0;

    // Let's determine which databases are currently connected to
    foreach (get_object_vars($CI) as $CI_object)
    {
        if (is_object($CI_object) && is_subclass_of(get_class($CI_object), 'CI_DB') )
        {
            $dbs[] = $CI_object;
        }
    }

    if (count($dbs))
    {
        foreach ($dbs as $db)
        {
            $count = 0;

            if (count($db->queries))
            {
                foreach ($db->queries as $key => $query)
                {
                    $count++;
                    $time = $db->query_times[$key];
                    $total_time += $time;

                    $info[] = array(
                        'position' => $count,
                        'weight' => 0,
                        'str_weight' => 0,
                        'time' => $time,
                        'str_time' => number_format($time, 4),
                        'query' => $query,
                        'db_server' => $db->hostname,
                        'db_name' => $db->database,
                    );
                }
            }
        }
       
        usort($info, function ($item1, $item2) {
            if ($item1['time'] == $item2['time']) return 0;
            return $item1['time'] < $item2['time'] ? 1 : -1;
        });
        
        $query_count = count($info);
        
        if ($query_limit) {
            $info = array_slice($info, 0, $query_limit);
        }
        
        foreach ($info as $key => $info_detail) {
            $weight = $info_detail['time'] / $total_time;
            $info[$key]['weight'] = $weight;
            $info[$key]['str_weight'] = sprintf("%.2f%%", $weight * 100);
        }
       
        $info = array(
            'total_time' => $total_time,
            'query_count' => $query_count,
            'detail' => $info,
        );
    }

    return $info;
}

function get_profiler_data()
{
    $CI =& get_instance();
    
    $res = array(
        'environment' => ENVIRONMENT,
        'dev' => DEV,
        'elapsed_time' => $CI->benchmark->elapsed_time(),
        'memory' => $CI->benchmark->memory_usage(),
        'benchmarks' => get_profiler_benchmarks(),
        'query' => get_profiler_query(),
    );
    
    return $res;
}

function getCallerName() 
{
    $caller = "";

    $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    $count = count($trace);
    for ($index = 0; $index < $count; $index++) {
        $call = array_shift($trace);
        if ($call["class"] != __CLASS__) {
            $caller = $call["class"] . '::' . $call["function"];
            break;
        }
    }

    return $caller;
}

function benchmark_me_start()
{
    $CI =& get_instance();
    $CI->benchmark->mark(getCallerName() . '_start');
}

function benchmark_me_end()
{
    $CI =& get_instance();
    $CI->benchmark->mark(getCallerName() . '_end');
}

/**
 * Gets the business-provided gmaps_api_key or the defalt one if none has been set
 *  */
function get_business_gmaps_api_key($business_id = null) 
{
    $business_id = get_current_business_id($business_id);
    
    if ($business_id) {
        $business_maps_api_key = get_business_meta($business_id, 'maps_api_key');
        
        if ($business_maps_api_key) {
            return $business_maps_api_key;
        }
    }
    
    return get_default_gmaps_api_key();
}

/**
 * Gets the dafault gmaps_api_key set on the configuration file
 */
function get_default_gmaps_api_key()
{
    $CI = & get_instance();
    $CI->config->load('gmaps');
    $default_key = $CI->config->item('gmaps_default_key');

    return $default_key;
}

function get_current_business_id($business_id = null) 
{
    $CI = & get_instance();

    if (!$business_id) {
        $business_id = $CI->business_id;
    }
    
    if (!$business_id) {
        $business_id = $CI->session->userdata("business_id");;
    }
    
    return $business_id;
}

function start_session($secure = true)
{
    if ($secure) {
        $old_cookie_httponly = ini_get('session.cookie_httponly'); // cookies can not be read on client side
        $old_cookie_secure = ini_get('session.cookie_secure'); // only send cookies to https domains

        enable_secure_cookies();
    }
    
    session_start();
    
    if ($secure) {
        ini_set('session.cookie_httponly', $old_cookie_httponly);
        ini_set('session.cookie_secure', $old_cookie_secure);
        
        secure_current_session_cookie();
    }
}

function enable_secure_cookies()
{
    ini_set('session.cookie_httponly', 1); // cookies can not be read on client side
    ini_set('session.cookie_secure', 1); // only send cookies to https domains
}

/**
 * If a php session existes, replace the default cookie with a secured one
 */
function secure_current_session_cookie()
{
    $current_session_id = session_id();
    if (!$current_session_id) {
        return false;
    }
    
    $params = session_get_cookie_params();
    secure_existent_cookie(
        ini_get('session.name'), 
        $current_session_id, 
        $params["path"], 
        $params["domain"]
    );    
}

function secure_existent_cookie($name, $value, $path, $domain, $expires = 0, $secure = true, $httponly = true)
{
    setcookie(
        $name, 
        $value, 
        $expires, 
        $path, 
        $domain,
        $secure,
        $httponly
    );    
}