<?php

/**
 * Sends a forgot password email to the specified customer
 * @param Object $customer
 */
function send_forgot_password_email($customer)
{
    $CI = get_instance();

    $business_id = $customer->business_id;
    $business = $CI->business_model->get_by_primary_key($business_id);
    $email = $customer->email;

    $code = md5(Now());
    $forgot_password_url = get_business_account_url($business, 'https://') . "/account/main/verify_password/{$code}/{$customer->customerID}";
    update_customer_meta($customer->customerID, 'password_verify_code', $code);

    $emailTemplate = get_emailTemplate_by_business_language_for_customer("forgot_password", $customer->customerID);
    $subject = $emailTemplate->emailSubject;
    $body =  $emailTemplate->emailText;
    $data = get_macros(array(
        "customer_id" => $customer->customerID,
        "business_id" => $business_id,
    ));
    $data["forgot_password_url"] = $forgot_password_url;

    $compiled_subject = email_replace($subject, $data);
    $compiled_body = email_replace($body, $data);

    // This email shoudl be sent immediately
    send_email(get_business_meta($business_id, 'customerSupport'), get_business_meta($business_id, 'from_email_name'), $email, $compiled_subject, $compiled_body, array(), array(), null, $business_id);

    return true;
}


/**
* Send email acknowledging the creation of a ticket
*/
    function send_ticket_system_acknowledgement($email, $supportticket_id, $business_id, $customer_id)
    {
        $CI = get_instance();

        $business = $CI->business_model->get_by_primary_key($business_id);
    
        $sql = "SELECT emailTemplate.*, emailActions.name FROM emailTemplate
             INNER JOIN emailActions ON emailActionID = emailAction_id
             WHERE emailActions.action = ? AND emailTemplate.business_id = ?";
        $emailTemplate = $CI->db->query($sql, array('ticket_system_acknowledgement', $business_id))->row();

        $subject = $emailTemplate->emailSubject;
        $body =  $emailTemplate->emailText;
        
        $options['business_id'] = $business_id;
        $data = get_macros($options);
        
        $data["supportticket_id"] = $supportticket_id;
    
        $compiled_subject = email_replace($subject, $data);
        $compiled_body = email_replace($body, $data);
    
        queueEmail(get_business_meta($business_id, 'customerSupport'), get_business_meta($business_id, 'from_email_name'), $email, $compiled_subject, $compiled_body, array(), array(), $business_id, $customer_id);
    
        return true;

    }
        

/**
 * Sends a system an email from the ticketing system
 * @param Object $customer
 */
 
  function send_ticket_system_email($customer_support, $from_email_name, $customerEmail, $ticket_id, $emailBody, $emailHistory, $business_id, $customer_id)
  {
    $CI = get_instance();

     $sql = "SELECT emailTemplate.*, emailActions.name FROM emailTemplate
             INNER JOIN emailActions ON emailActionID = emailAction_id
             WHERE emailActions.action = ? AND emailTemplate.business_id = ?";
            $emailTemplate = $CI->db->query($sql, array('ticket_system_email', $business_id))->row();

    $subject = $emailTemplate->emailSubject;
    $body =  $emailTemplate->emailText;
    $options['business_id'] = $business_id;
    $data = get_macros($options);

    $data["supportticket_id"] = $ticket_id;
    $data["email_body"] = $emailBody;
    $data["email_history"] = $emailHistory;

    $compiled_subject = email_replace($subject, $data);
    $compiled_body = email_replace($body, $data);

    return queueEmail($customer_support, $from_email_name, $customerEmail, $compiled_subject, $compiled_body, array(), null, $business_id, $customer_id);

    
}

/**
 * Sends a verify email email to the specified customer
 * @param Object $customer
 */
function send_verify_email_email($customer, $new_email)
{
    $CI = get_instance();

    $business_id = $customer->business_id;
    $business = $CI->business_model->get_by_primary_key($business_id);

    //this array will get serialized and stored in the customer table
    $email['email'] = $new_email;
    $email['date'] = date('Y-m-d');
    $email['verify'] = $code = md5(date('Y-m-d H:i:s'));

    $CI->load->model('customer_model');
    $CI->customer_model->pendingEmailUpdate = serialize($email);
    $CI->customer_model->where = array('customerID'=> $customer->customerID);
    $CI->customer_model->update();

    $verify_email_url = get_business_account_url($business, 'https://') . "/account/main/verify_new_email/{$customer->customerID}/{$code}";

    $emailTemplate = get_emailTemplate_by_business_language_for_customer("verify_email", $customer->customerID);
    $subject = $emailTemplate->emailSubject;
    $body =  $emailTemplate->emailText;
    $data = get_macros(array(
        "customer_id" => $customer->customerID,
        "business_id" => $business_id,
    ));
    $data["verify_email_url"] = $verify_email_url;

    $compiled_subject = email_replace($subject, $data);
    $compiled_body = email_replace($body, $data);

    // This email shoudl be sent immediately
    send_email(get_business_meta($business_id, 'customerSupport'), get_business_meta($business_id, 'from_email_name'), $new_email, $compiled_subject, $compiled_body, array(), array(), null, $business_id);

    return true;
}

