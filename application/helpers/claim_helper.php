<?php
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Server_Meta;

function check_order_exist($locker_id, $business_id = '')
{
    $CI = get_instance();
    $business_id = ($business_id!='')?$business_id:$CI->business_id;
    if (empty($business_id)) {
        throw new Exception("Missing business_id");
    }

    $CI->load->model('order_model');

    $options['locker_id'] = $locker_id;
    $options['exclude_orderStatusOption_id'] = array(9,10);
    $options['business_id'] = $business_id;
    $orders = $CI->order_model->get($options);
    if($orders)

    return $orders;

    return false;
}


/**
 * final check to see if the claim already exists
 * @deprecated This function is deprecated, use DroplockerObject instead.
 */
function check_claim_exists($locker_id, $multiOrder = "", $business_id='', $customer_id='')
{
    $CI = get_instance();

    $business_id = ($business_id!='')?$business_id:$CI->business_id;
    if (empty($business_id)) {
        throw new Exception("Missing business_id");
    }

    $customer_id = ($customer_id!='')?$customer_id:$CI->customer_id;
    if (empty($customer_id)) {
        throw new Exception("Missing customer_id");
    }

    if ($multiOrder == '') {
        $locker = new Locker($locker_id);
        $multiOrder = $locker->is_multi_order();
    }

    $CI->load->model('claim_model');
    $claim = $CI->claim_model->get(array('locker_id'=>$locker_id, 'business_id'=>$business_id, 'active'=>1));

    $business = new Business($business_id);

    if ($claim && $multiOrder != 1) {
        foreach ($claim as $c) {
            if ($c->customer_id == $customer_id) {
                $message = get_translation("locker_already_claimed_by_you_error_message", "business_account/orders_new_order", array("lockerName" => $locker->lockerName), "<strong>You have already claimed this locker (%lockerName%). Please check <a href='/account/orders/view_orders'>your orders</a> to see if your order has been picked up.</strong>");
            } else {
                //get claim automatic deactivation setting
                $claim_automatic_deactivation = get_business_meta($business_id, 'claim_deactivation','1');
                //if the claim is more than 48 hours old then deactivate it and place this new claim and notify customer service
                if (strtotime($c->updated) < strtotime('48 hours ago') && $claim_automatic_deactivation) {
                    $CI->claim_model->deactivate_claim($c->claimID);
                    //notify customer service that claim was superseeded
                    send_admin_notification('Claim Automatically Deactivated ['.$c->claimID.']', 'Claim ID [' . $c->claimID . '] was more than 48 hours old an another customer placed an order in the same locker so this claim was automatically deactivated', $business_id);

                    //allow claim to be placed
                    return array('status'=>'success', 'message'=>"locker is not claimed");
                } else {
                    //see if this customer has had an order picked up in the last 48 hours

                    $getOpenOrders = "select orderStatus.* from orders
                                    join orderStatus on order_id = orderID
                                    where customer_id = $c->customer_id
                                    and orderStatus.orderStatusOption_id = 1
                                    and orders.bag_id != 0
                                    and date_add(orderStatus.date, INTERVAL 48 HOUR) > now()";
                    $openOrders = $CI->db->query($getOpenOrders)->result();
                    //if so, deactivate this claim
                    if ($openOrders) {
                        if ($claim_automatic_deactivation) {
                            $CI->claim_model->deactivate_claim($c->claimID);
                            //notify customer service that claim was superseeded
                            send_admin_notification('Claim Automatically Deactivated ['.$c->claimID.']', '<a href="http://'.Server_Meta::get("admin_url").'/admin/customers/detail/'.$c->customer_id.'">This customer</a> has had an order picked up in the last 48 hours so this claim was automatically deactivated', $business_id);
                        }

                        //allow claim to be placed
                        $message = get_translation("locker_not_claimed", "business_account/orders_new_order", array("lockerName" => $locker->lockerName), "locker is not claimed");
                        return array('status'=>'success', 'message'=> $message);
                    } else {
                        $message = get_translation("locker_already_claimed_error_message", "business_account/orders_new_order", array("lockerName" => $locker->lockerName, "business_phone" => $business->phone), "Locker (%lockerName%) has already been claimed by someone else. If you think this is incorrect, please contact %business_phone% immediately.");
                    }
                }
            }
        }

        return array('status'=>"fail", 'message'=>$message);
    } else {
        return array('status'=>'success', 'message'=>"locker is not claimed");
    }

}

function pickup_date($location_id, $time = null)
{
    $CI = get_instance();

    //figure out when this order will be picked up
    $getLocation = "select * from location where locationID = $location_id";
    $location = $CI->db->query($getLocation)->result();

    //first get the cutoff time
    $business_id = $location[0]->business_id;
    $cutOff = get_business_meta($business_id, 'cutOff');
    if (empty($cutOff)) {
        send_admin_notification('No cutoff time set', 'Your business does not have a cutoff time set so we can not determine when your orers are supposed to be picked up.  Pleas set this up in Admin / Business Details', $business_id);
        $cutOff = '9:00';
    }

    //remove the : from cutoff
    $cutOff = str_replace(":", "", $cutOff);

    if (date('Gi', $time) >= $cutOff) {
        //it will get picked up tomorrow
        $origPickupDay = date('Y-m-d', strtotime('+ 1 day', $time));
    } else {
        $origPickupDay = date('Y-m-d', $time);
    }

    //now lets see if we pickup from that location on that day
    //load pickup dates into an array
    $getLocationPickupDay = "SELECT * FROM location
                                join location_serviceDay on location_id = locationID
                                where locationID = ".$location_id."
                                and location_serviceType_id = 2";
    $locationDays = $CI->db->query($getLocationPickupDay)->result();
    $pickupDay = array();
    foreach ($locationDays as $loc) {
        $pickupDay[] = $loc->day;
    }

    //if not, keep going forward until we find what day we service this location
    $foundDueMatch = 0;
    $foundPickupMatch = 0;
    for ($i = 0; $i <= 10; $i ++) {
        $newPickupDay = date("Y-m-d", strtotime("$origPickupDay + $i days"));
        if (in_array(date("N", strtotime($newPickupDay)), $pickupDay)) {
            $foundPickupMatch = 1;
            //make sure this day is not a holiday
            if (is_holiday($newPickupDay, $business_id)) {
                $foundPickupMatch = 0;
            }
        }
        if ($foundPickupMatch == 1) {
            $newPickupDate = $newPickupDay;
            break;
        }
    }
    //if we can't find a pickup date send an error
    if (!isset($newPickupDate)) {
         send_admin_notification('No pickup day set', $location[0]->address.' does not have any pickup days set and someone is trying to place an order ', $business_id);
         $newPickupDate = $origPickupDay;
    }

    return $newPickupDate;
}

//send the email to admin
function send_admin_claim_email($locker, $location, $claim_id)
{
}
