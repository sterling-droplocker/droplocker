<?php

use App\Libraries\DroplockerObjects\LanguageView;
use App\Libraries\DroplockerObjects\LanguageKey;
use DropLocker\Cache\SessionStore;

/**
 * Gets a LanguageView using its name
 *
 * Uses a cache to prevent ruring the query many times.
 * If not found one will be created.
 *
 * @param string $name
 * @return \App\Libraries\DroplockerObjects\LanguageView
 */
function get_language_view($name)
{
    $languageViewCache = SessionStore::getInstance('languageView');

    if (isset($languageViewCache[$name])) {
        return $languageViewCache[$name];
    }

    $languageView = LanguageView::search(compact('name'));
    if (empty($languageView)) {
        $languageView = LanguageView::create(compact('name'));
    }

    $languageViewCache[$name] = $languageView;

    return $languageView;
}

/**
 * Gets a LanguageKey using its name and the LanguageView name
 *
 * Uses a cache to prevent ruring the query many times.
 * If not found one will be created.
 *
 * @param string $view_name
 * @param string $key_name
 * @param string $defaultText
 * @throws \Exception when LanguageKey not found and empty $defaultText
 * @return \App\Libraries\DroplockerObjects\LanguageKey
 */
function get_language_key($view_name, $key_name, $defaultText)
{
    $languageKeyCache = SessionStore::getInstance('languageKey');

    $full_key = "$view_name::$key_name";

    if (isset($languageKeyCache[$full_key])) {
        return $languageKeyCache[$full_key];
    }

    $languageView = get_language_view($view_name);
    $languageKey = LanguageKey::search(array(
        'languageView_id' => $languageView->languageViewID,
        'name'            => $key_name
    ));

    if (empty($languageKey)) {
        if (!strlen($defaultText)) {
            throw new \Exception("There is no language key with name '$key_name' associated with language view '$view_name' and no fallback text");
        }

        $languageKey = LanguageKey::create(array(
            'languageView_id' => $languageView->languageViewID,
            'name'            => $key_name,
            'defaultText'     => $defaultText,
        ));
    }

    if (strlen($defaultText) && $languageKey->defaultText != $defaultText) {
        $languageKey->defaultText = $defaultText;
        $languageKey->save();
    }

    $languageKeyCache[$full_key] = $languageKey;

    return $languageKey;
}

/**
 * Gets a business language by ID
 *
 * Uses a cache to prevent ruring the query many times.
 *
 * @param int $business_languageID
 * @return stdClass
 */
function get_business_language($business_languageID)
{
    // should be loaded every request, because its used to expire translation cache
    static $business_languageCache = array();

    if (isset($business_languageCache[$business_languageID])) {
        return $business_languageCache[$business_languageID];
    }

    $CI = get_instance();
    $CI->load->model('business_language_model');
    $business_languageCache[$business_languageID] = $CI->business_language_model->get_by_primary_key($business_languageID);

    return $business_languageCache[$business_languageID];
}

/**
 * Gets a translation
 *
 * Uses a cache to prevent ruring the query many times.
 *
 * @param int $business_language_id
 * @param int $languageView_id
 * @param unknown $languageKey_id
 */
function get_translation_for($business_language_id, $languageView_id, $languageKey_id)
{
    $business_language = get_business_language($business_language_id);
    $prefix = "$business_language_id::{$business_language->lastUpdated}";
    $translationCache = SessionStore::getInstance($prefix);

    $cache_key = "$languageView_id::$languageKey_id";
    if (isset($translationCache[$cache_key])) {
        return $translationCache[$cache_key];
    }

    $CI = get_instance();
    $CI->load->model("languageKey_business_language_model");
    $translation = $CI->languageKey_business_language_model->get_translation_for_languageKey_in_languageView(
        $business_language->language_id,
        $business_language->business_id,
        $languageView_id,
        $languageKey_id
    );

    $translationCache[$cache_key] = $translation;

    return $translation;
}


/**
 * Determines which business language shall be used for translation
 *
 * It uses the employee session or, if specified, from the given employee ID.
 *
 * @param string $employee_id
 * @return NULL
 */
function get_employee_business_language_id($employee_id = null)
{
    $CI = get_instance();
    $CI->load->model("employee_model");
    if (is_numeric($employee_id)) {
        $CI->load->model("employee_model");
        $employee = $CI->employee_model->get_by_primary_key($employee_id);
    } elseif (isset($CI->employee)) {
        $employee = $CI->employee;
    } else {
        $employee = null;
    }

    if (!empty($employee->business_id)) {
        if (empty($CI->business) || $CI->business->businessID != $employee->business_id) {
            $business = $CI->business_model->get_by_primary_key($employee->business_id);
        } else {
            $business = $CI->business;
        }
    } elseif (!empty($CI->business)) {
        $business = $CI->business;
    } elseif (!empty($CI->business_id)) {
        $business = $CI->business_model->get_by_primary_key($CI->business_id);
    } else {
        $business = null;
    }

    if (!empty($employee->business_language_id)) {
        return $employee->business_language_id;
    }

    if (empty($business)) {
        return null;
    }

    if (!empty($business->default_business_language_id)) {
        return $business->default_business_language_id;
    }

    $CI->load->model("business_language_model");
    $first_defined_business_language = $CI->business_language_model->get_one(array("business_id" => $business->businessID));

    if ($first_defined_business_language) {
        return $first_defined_business_language->business_languageID;
    }

    return null;
}

/**
 * Determines which business language shall be used for translation
 *
 * It uses the customer session or, if specified, from the given customer ID.
 *
 * This function expects that the CI instance has the property customer set.
 * @param int $customer_id If passed, then the specified customer ID is used instead of the customer session. Otherwise, this function attempts to retrieve the customer based off of the customer property set in the $CI singleton.
 * @return mixed Returns null if there are no business languages defined for the business. Otherwise, returns the primary key of the business language entity to be used for the customer.
 */
function get_customer_business_language_id($customer_id = null)
{
    $CI = get_instance();
    $CI->load->model("business_model");
    $CI->load->model("customer_model");
    //The following conditional attempts to determine the customer associated with the translation
    if (!empty($customer_id)) {
        $customer = $CI->customer_model->get_by_primary_key($customer_id);
    } elseif (isset($CI->customer)) {
        $customer = $CI->customer;
    } else {
        $customer = null;
    }

    //The following condtiionals attempt to determine the business associated with the translation.
    if (!empty($customer->business_id)) {
        if (empty($CI->business) || $customer->business_id != $CI->business->businessID) {
            $business = $CI->business_model->get_by_primary_key($customer->business_id);
        } else {
            $business = $CI->business;
        }
    } elseif (isset($CI->business)) {
        $business = $CI->business;
    } else {
        $business = null;
    }

    if ( isset( $_GET['ll_lang'] ) ) { //check if we need to force a lang through a get var - only works if that language is supported by that business
        $get_lang = $CI->input->get(NULL, TRUE);
        $lang_tag = $get_lang['ll_lang'];
        $CI->load->model("language_model");
        $get_var_language = $CI->language_model->get_one( array( 'tag' => $lang_tag ) );

        if ( !empty( $get_var_language->languageID ) ) {
            //another - we can do a join or something
            $CI->load->model("business_language_model");
            $business_language = $CI->business_language_model->get_one(array("business_id" => $business->businessID, 'language_id' => $get_var_language->languageID ) );

            //don't interrupt the flow - if we've got a lang from the GET var, great, otherwise keep going and fall back to the other logic
            if (!empty($business_language->business_languageID)) {
                $params = array('language'=>$business_language->business_languageID);
                $CI->session->set_userdata($params);
                return $business_language->business_languageID;
            }
        }
    }

    // if customer has language, use that
    if (!empty($customer->default_business_language_id)) {
        return $customer->default_business_language_id;
    }

    // get language from last logged in user
    if (!empty($CI->session) && $session_language = $CI->session->userdata('language')) {
        return $session_language;
    }

    if (is_superadmin()) {
        return null;
    }

    // no business, can't retireve a default language
    if (empty($business)) {
        return null;
    }

    //If the customer business language ID does not exist, then default business language is used instead.
    if ($business->default_business_language_id) {
        return $business->default_business_language_id;
    }

    //If there is no default business language, then the first language defiend for the business is used, or null
    $CI->load->model("business_language_model");
    $business_language = $CI->business_language_model->get_one(array("business_id" => $business->businessID));

    if (!empty($business_language)) {
        return $business_language->business_languageID;
    }

    // no language found
    return null;
}

/**
 * Determines which business language is used for translation
 *
 * @param string $customer_id
 * @return string
 */
function get_customer_business_language_key($customer_id = null)
{
    $CI = get_instance();

    $business_lang_id = get_customer_business_language_id($customer_id);
    $CI->load->model('business_language_model');
    $language = $CI->business_language_model->get_by_primary_key($business_lang_id);

    if ($language) {
        return $language->tag;
    }

    return false;
}

/**
 * Retrieves the language translation from the specified language key.
 * Note, this function determines the correct language view based on the file path of the view file that this function is called from.
 * Note, this helper must be called from a file located in the views directory.
 * @param string $languageKey_name
 * @param string $substitutions A a key-value array that replaces any parameters in the translation. Note, translation parameters are delimited by being between two percent (%) symbols.
 * @param string $fallback_text If no language key is found, then this function will fallback to displaying this text instead.
 * @return string
 * @throws Exception Thrown if no language key is found and no default_text was supplied to this function
 */
function get_translation_for_view($languageKey_name, $fallback_text, $substitutions = array(), $filepath = null)
{
    // I inverted the order of parameters, so check for not updated views
    if (is_array($fallback_text)) {
        $tmp = $fallback_text;
        $fallback_text = $substitutions;
        $substitutions = $tmp;
    }

    if (!$filepath) {
        $backtrace = debug_backtrace();
        $filepath = $backtrace[0]['file'];
    }

    $CI = get_instance();

    //The following conditional determines which regex to use to parse the view path from the file path based on the operating system.
    // If the operating system is windows, then backward slashes are used. Otherwise, forward slashes are used.
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $regex = "#" . str_replace("/", '\\\\', APPPATH) . 'views\\\\(.*).php#';
    } else {
        $regex = "#" . APPPATH . "views/(.*).php#";
    }
    preg_match($regex, $filepath, $matches);
    $languageView_name = $matches[1];
    if (empty($languageView_name)) {
        throw new Exception("This function can only be called from a view file");
    }

    $CI->load->model("languageView_model");
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $languageView_name = str_replace("\\", "/", $languageView_name);
    }

    $languageKey = get_language_key($languageView_name, $languageKey_name, $fallback_text);

    $regex = "#^(.*)/#";
    preg_match($regex, $languageView_name, $matches);
    //The following conditional checks if the language view is associated with the admin view files, if so, then we use the logic for retrieving the business language for the current employee.
    if ($matches[1] == "admin" || $matches[1] == "print" || str_starts_with_lb($matches[1], 'admin/')) {
        $business_languageID = get_employee_business_language_id();
    } else { //If not an admin view file, we use the logic for retrieving the business language for the customer.
        $business_languageID = get_customer_business_language_id();
    }

    // Default translation
    $translation = $languageKey ? $languageKey->defaultText : "languageKey '$languageKey_name' needs to be setup";

    // If we have a $languageKey and a $business_languageID
    if (!empty($business_languageID) && !empty($languageKey)) {

        $business_language = get_business_language($business_languageID);

        // If we found the language
        if (!empty($business_language)) {
            $language_id = $business_language->language_id;
            $translation_result = get_translation_for(
                $business_languageID,
                $languageKey->languageView_id,
                $languageKey->languageKeyID
            );

            // if We found a translation
            if (!empty($translation_result->translation)) {
                $translation = $translation_result->translation;
            }
        } else {
            trigger_error(("Business Language '$business_languageID' not found"));
        }
    }

    if ($substitutions) {
        foreach ($substitutions as $substitution_key => $substitution_value) {
            $translation = str_replace("%$substitution_key%", $substitution_value, $translation);
        }
    }

    return $translation;
}


/**
 * Retreives the business defined translation from the specified language key name and language view name and customer session.
 *
 * @param string $languageKey_name The name of the language key
 * @param string $languageView_name The name of the language view
 * @param array $substitutions An array consisting of macro key names and their respective values
 * @param string $fallback_text If the language key or language view does not exist, then this text is returned from this function.
 * @param int $customer_id Use a specified customer, instead of looking at the session.
 * @param int $business_languageID If defined, will have priority over $customer_id
 * @return string
 * @throws \Exception
 */
function get_translation($languageKey_name, $languageView_name, $substitutions = array(), $fallback_text = null, $customer_id = null, $business_languageID = null)
{
    $CI = get_instance();
    $CI->load->model("languageView_model");

    //Some translations are dynamic and values could be empty
    if (empty($languageKey_name) && empty($fallback_text)) {
        return "";
    }
    
    $languageKey = get_language_key($languageView_name, $languageKey_name, $fallback_text);

    // Default translation
    $translation = $languageKey ? $languageKey->defaultText : "languageKey '$languageKey_name' needs to be setup";
    //$translation = '<span style="color:red;">'.$translation.'</span>';

    if (empty($business_languageID)) {
        $business_languageID = get_context_business_language_id($customer_id, true);
    }

    $CI->load->model("business_language_model");

    if (!empty($business_languageID)) {
        $business_language = get_business_language($business_languageID);
    } else {
        $business_language = null;
    }

    if ($business_languageID && !empty($business_language)) {

        $translation_result = get_translation_for(
            $business_languageID,
            $languageKey->languageView_id,
            $languageKey->languageKeyID
	    );

        if (!empty($translation_result->translation)) {
            $translation = $translation_result->translation;
        }
    } else {
        if (DEV && $business_languageID) {
            trigger_error("Business Language '$business_languageID' not found");
        }
        $translation = $languageKey->defaultText;
    }

    if ($substitutions) {
        foreach ($substitutions as $substitution_key => $substitution_value) {
            $translation = str_replace("%$substitution_key%", $substitution_value, $translation);
        }
    }

    return $translation;
}

function i_am_in_admin()
{
    $uri = $_SERVER['REQUEST_URI'];
    if($uri) {
        $uri = explode('/', $uri);
    }
    
    return (count($uri) > 1 && strtolower($uri[1]) === 'admin');
}

function is_employee_context()
{
    $CI = get_instance();
    return isset($CI->employee);
}

function get_context_business_language_id($entity_id, $force_customer_language = false) 
{
    if ( (i_am_in_admin() || is_employee_context()) && !$force_customer_language ) {
        return get_employee_business_language_id($entity_id);
    } else {
        return get_customer_business_language_id($entity_id);
    }
}

/**
 * Translate string using the current domain
 *
 * @param string $key the language key, if empty the slug of the text will be used
 * @param string $text the default text
 * @param array $substitutions an array of macros to replace in the text
 * @param int $customer_id Use a specified customer, instead of looking at the session
 * @return Ambigous <string, mixed>
 */
function __($key, $text, $substitutions = array(), $customer_id = null)
{
    $domain = TranslationDomain::get();
    if (empty($domain)) {
        trigger_error("Missing translation domain, you must call translation_domain() to set the domain.", E_USER_WARNING);
        return $text;
    }

    if (empty($key)) {
        $key = trim(preg_replace('/[^a-z0-9]+/i', '_', strtolower($text)), '_');
    }

    // if skipeed substitutions but set customer_id
    if (!is_array($substitutions) && preg_match('/^[0-9]+$/', $substitutions)  && empty($customer_id)) {
        $customer_id = $substitutions;
        $substitutions = array();
    }

    return get_translation($key, $domain, $substitutions, $text, $customer_id);
}

/**
 * Translate string using a specific domain
 *
 * @param string $domain the translation domain
 * @param string $key the language key, if empty the slug of the text will be used
 * @param string $text the default text
 * @param array $substitutions an array of macros to replace in the text
 * @param int $customer_id Use a specified customer, instead of looking at the session
 * @return Ambigous <string, mixed>
 */
function __d($domain, $key, $text, $substitutions = array(), $customer_id = null)
{
    if (empty($key)) {
        $key = trim(preg_replace('/[^a-z0-9]+/i', '_', strtolower($text)), '_');
    }

    // if skipeed substitutions but set customer_id
    if (!is_array($substitutions) && preg_match('/^[0-9]+$/', $substitutions)  && empty($customer_id)) {
        $customer_id = $substitutions;
        $substitutions = array();
    }

    return get_translation($key, $domain, $substitutions, $text, $customer_id);
}

function get_translation_domain()
{
	return TranslationDomain::get();
}

function enter_translation_domain($domain)
{
    TranslationDomain::push($domain);
}

function leave_translation_domain()
{
    return TranslationDomain::pop();
}

class TranslationDomain
{
    static $stack = array();

    public static function get()
    {
        return end(self::$stack);
    }

    public static function reset($domain = null)
    {
        self::$stack = array();
        if ($domain) {
            array_push(self::$stack, $domain);
        }
    }

    public static function push($domain)
    {
        array_push(self::$stack, $domain);
    }

    public static function pop()
    {
        return array_pop(self::$stack);
    }

}
