<?php

function logme($message, $email='')
{
    $message;
    $CI = get_instance();
    error_log($message, 4);

    if ($email) {
        $CI->email->to($email);
        $CI->email->from(SYSTEMEMAIL, SYSTEMFROMNAME);
        $CI->email->subject("Error in droplocker application");
        $CI->email->message($message);
        $CI->email->send();
    }
}
