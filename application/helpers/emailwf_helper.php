<?php
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Customer;

function emailWF($orderId)
{
    $CI = get_instance();

    $order = new Order($orderId);
    $cust = new Customer($order->orderID);

    // send an email to SF Coin if there is wash & fold in the order
    $wfCounter = 0;
    // see if there is wash & fold in the order items
    $query1 = "SELECT orderItem.*, bag.bagNumber, orders.notes as ordNotes, p.name
                FROM orderItem
                INNER JOIN orders on orderID = orderItem.order_id
                INNER JOIN bag on bagID = orders.bag_id
                INNER JOIN product_process on product_processID = orderItem.product_process_id
                INNER JOIN product p ON productID = product_process.product_id
                WHERE order_id = $orderId;";
    $query = $CI->db->query($query1);

    foreach ($query->result() as $line) {

        // wash and fold or comforter
        // comforter = 168,167
        // wf = 512
        // wf towels = 604
        if (in_array($line->product_process_id, array(
            167,
            168,
            512,
            604
        ))) {
            // $extra = addExtras($addOn);
            $CI->load->library('wash_fold_product');
            $preferences = $CI->wash_fold_product->get_merged_preferences($line->customer_id, $CI->session->userdata('business_id'));

            $qtyAmount = $line->qty;
            $bodyText = $bodyText . '<table border="1" cellspacing="2">
            <tr><td colspan="7"><strong>WE RECYCLE:</strong> Please place the packaging back in your laundry bag so we can recycle it. Thanks!</td></tr>
            <tr>
            <td valign="bottom" nowrap>Soap</td>
            <td valign="bottom" nowrap>Bleach</td>
            <td valign="bottom" nowrap>Softener</td>
            <td valign="bottom" nowrap>Colors</td>
            <td valign="bottom" nowrap>Whites</td>
            <td valign="bottom" nowrap>Super</td>
            <td valign="bottom" nowrap>Notes</td>
            </tr><tr><td>' . addExtras($preferences['detergent']) . '</td><td>' . addExtras($preferences['bleach']) . '</td><td>' . addExtras($preferences['fabSoft']) . '</td><td>' . addExtras($preferences['colorTemp']) . '</td><td>' . addExtras($preferences['whiteTemp']) . '</td><td>' . addExtras($preferences['superWash']) . '</td><td><strong>' . $line->ordNotes . '</strong></td>
            </tr><tr>
            <td valign="bottom" nowrap>Sheets</td>
            <td valign="bottom" nowrap>Low Dry</td>
            <td colspan="2" valign="bottom" nowrap>Loads</td>
            <td valign="bottom" nowrap>LL LBS</td>
            <td colspan="2" rowspan="2" align="right"><font size="8">' . $line->bagNumber . '</font></td>
            </tr>
            <tr>
            <td>' . addExtras($preferences['dryerSheet']) . '</td><td>' . addExtras($preferences['dryLow']) . '</td><td colspan="2">' . addExtras($preferences['loads']) . '</td><td>' . $qtyAmount . '</td></tr>
            </table><br><br>';
            $bodyText = $bodyText . $bodyText . $bodyText . $bodyText;
            $emailBagNo = $line->bagNumber;
            $laundromat = $line->supplier_id;

            $bodyText = $bodyText;
            $wfCounter = 1;
        }
    }

    if ($wfCounter > 0) {

        if ($laundromat == 12) {         // SF Coin
            $emailAddress = array(
                'sfcoinlaundry@gmail.com, rich@francis.com, oyuna@sfcoinlaundry.com, njeneer@yahoo.com'
            );
        }
        if ($laundromat == 18) {         // 4158649935 is wash-n-go fax number
            $emailAddress = array(
                '4158649935@smartfax.com'
            );
        }
        if ($laundromat == 19) {         // 5102151600 is Berkeley fax number
            $emailAddress = array(
                '5102151600@smartfax.com'
            );
        }
        if ($laundromat == 3) {         // Laundry Locker
            $emailAddress = array(
                'support@laundrylocker.com'
            );
        }

        foreach ($emailAddress as $email) {
            queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $email, 'Bag Number: ' . $emailBagNo . ' - ' . $laundromat, $bodyText, array(), null, $order->business_id);
        }
    }
}

function addExtras($addOn)
{
    $CI = get_instance();
    if ($addOn) {
        $select1 = "SELECT * FROM product WHERE productID = $addOn;";
        $select = $CI->db->query($select1);
        foreach ($select->result() as $line) {
            if ($line->sortOrder != 1) {
                $str = '<strong>' . $line->name . '</strong>';

                return $str;
            } else

                return $line->name;
        }
    } else

        return 'use default';
}
