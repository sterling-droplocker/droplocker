<?php
function get_serviceType_name($serviceTypeID)
{
    $CI = get_instance();
    $CI->load->model("servicetype_model");
    $serviceType = $CI->servicetype_model->get_by_primary_key($serviceTypeID);

    return $serviceType->name;
}
