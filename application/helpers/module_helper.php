<?php
function get_module_name($moduleID)
{
    $CI = get_instance();
    $CI->load->model("module_model");
    $module = $CI->module_model->get_by_primary_key($moduleID);

    return $module->name;
}
