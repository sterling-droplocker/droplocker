<?php

function switch_to_employee($employeeID)
{
    $CI = get_instance();
    $CI->load->model("employee_model");
    $emp_id = $employeeID;

    $options['employee_id'] = $emp_id;
    $options['business_id'] = $CI->business_id;
    if ($user = $CI->employee_model->get($options)) {
        //if a user is found, we need to get all the business information and roles for each business
        $businesses = $CI->employee_model->get_employee_businesses(array('employee_id'=>$emp_id));
        foreach ($businesses as $b) {
            if ($b->default ==1) {
                $CI->session->set_userdata('business_id', $b->businessID);
            }
            $CI->session->set_userdata('buser_id', $b->id);
        }

        $CI->session->set_userdata('employee_id', $user[0]->employeeID);
        $CI->session->set_userdata('buser_fname', $user[0]->firstName);
        $CI->session->set_userdata('buser_lname', $user[0]->lastName);
        $CI->session->set_userdata('timezone', $user[0]->timezone);
        $CI->session->set_userdata('businesses',$businesses);
        $CI->session->set_userdata('super_admin',$user[0]->superAdmin);

    }
}

function send_ticket_system_email_employee($employeeID, $ticket_id)
{
	$CI = get_instance();
	$CI->load->model("emailtemplate_model");
	$CI->load->model('employee_model');
	
	$employee = $CI->employee_model->get(array('employeeID' => $employeeID));
	
	if (!$employee) {
		return false;
	}
	
	$emailTemplate =  $CI->emailtemplate_model->get_template_by_business('ticket_system_email_employee', $CI->business_id);
	
	if (!$emailTemplate) {
		return false;
	}
	
    $subject = $emailTemplate->emailSubject;
    $body =  $emailTemplate->emailText;
    $options['business_id'] = $CI->business_id;
    $data = get_macros($options);

    $data["supportticket_id"] = $ticket_id;

    $compiled_subject = email_replace($subject, $data);
    $compiled_body = email_replace($body, $data);

	$from_email = get_business_meta($CI->business_id, 'customerSupport');
	$from_name = get_business_meta($CI->business_id, 'from_email_name');
    
	return queueEmail($from_email, $from_name, $employee[0]->email, $compiled_subject, $compiled_body, array(), null, $CI->business_id, null);
}
