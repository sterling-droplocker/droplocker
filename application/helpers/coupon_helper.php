<?php

/**
 * Returns the predefined business amount types
 * @return array
 */
function getAmountTypesInDropdownFormat()
{
    return array("percent" => "Percent", "dollars" => "Dollars");
}
/**
 * Returns the coupon types in a dropdwon format
 * @return array
 */
function getCouponTypesInDropdownFormat() {
    return array("newCustomer" => "New Customer", "inactiveCustomer" => "Inactive Customer", "orderTotal" => "Order Total Rewards", "birthdayCoupon" => "Birthday's Coupon");
}