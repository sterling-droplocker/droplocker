<?php
function is_resource_allowed($resource_name)
{
    $CI = get_instance();
    //If the user is a superadmin or an admin, then access should always be granted to a resource.
    if ($CI->session->userdata('super_admin')==1 || $CI->zacl->get_employee_role($CI->session->userdata('buser_id')) == 'Admin') {
        return true;
    } else {
        if ($CI->zacl->check_acl($resource_name, $CI->session->userdata('buser_id'))) {
            return true;
        } else {
            return false;
        }
    }
}
