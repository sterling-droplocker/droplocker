<?php
/**
 * Detect's barcode format and split the valid part.
 * Note: This is used only for bubble & stitch and Dobbi
 * @param barcode
 */
function parse_barcode($barcode)
{
	if (!is_numeric($barcode)) {
		if (preg_match("/[a-z]/i", $barcode)) {
			$barcode = substr($barcode, -7);
            $barcode = ltrim($barcode, "0");
		}
	}
	return $barcode;
}
