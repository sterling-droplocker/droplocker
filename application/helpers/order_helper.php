<?php
/**
 * Retrievesj the total of all discounts, order items, and taxes in an order
 * @param int $orderID
 * @return int
 */
function get_net_total($orderID)
{
    $CI = get_instance();
    $CI->load->model("order_model");

    return $CI->order_model->get_net_total($orderID);
}
