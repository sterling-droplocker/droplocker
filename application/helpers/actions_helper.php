<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Location_ServiceDay;

/**
 *
 * @param array $options Can consist of the following array keys
 *  customer_id
 *  business_id
 *  laundryPlan_id
 *  giftCard_id
 *  claim_id
 * @return array An array of macros consisting of macro key names and values
 * @throws Exception
 */
function get_macros($options = array())
{
    $CI = get_instance();

    $data = array();
    if (isset($options['customer_id'])) {
        $customer_id = $options['customer_id'];
        $CI->load->model('customer_model');
        $customers = $CI->customer_model->get_customer($options['customer_id']);
        $customer = $customers[0];
        $customer->fullName = ($customer->fullName=='')?"New Customer":$customer->fullName;
        $customer->Dump = print_r($CI->customer_model->get_customer_dump($options['customer_id']), 1);

        $business_language_id = get_customer_business_language_id($customer->customerID);
        $CI->load->model("business_language_model");
        $business_language = $CI->business_language_model->get_by_primary_key($business_language_id);
        $CI->load->model("language_model");
        $language = $CI->language_model->get_by_primary_key($business_language->language_id);
        setlocale(LC_TIME, $language->tag);
        // get the active customer discounts
        // This will create a table if the custoemr has discounts. If not, it is an empty string
        $discounts = \App\Libraries\DroplockerObjects\Customer::getActiveDiscounts($customer_id, $options['business_id']);
        $date_format = get_business_meta($customer->business_id, 'shortDateLocaleFormat', '%b %e, %Y');

        if (!empty($discounts)) {
            $table = "";
            foreach ($discounts as $discount) {
                $percentTypes = array(
                    "item\[\%",
                    "percent"
                );

                $isPercent = preg_match("/^".implode("|^", $percentTypes)."/", $discount->amountType);
                if ($isPercent) {
                    $amount = number_format($discount->amount, 0)." %";
                } else if ($discount->amountType == "pounds") {
                    $amount = weight_system_format($discount->amount);
                } else {
                    $amount = format_money_symbol($options['business_id'], '%.2n', $discount->amount);
                }

                if ($discount->expireDate != "null" && $discount->expireDate != '') {
                    $date = strftime($date_format, strtotime($discount->expireDate));
                    $expire_message = get_translation("discount_expire_date_message", "Discount Frequencies", array("date" => $date),"(Expires %date%)", $options['customer_id']);
                } else {
                    $expire_message = get_translation("discount_never_expires_message", "Discount Frequencies", array(), "(Never expires)", $options['customer_id']);
                }
                switch ($discount->frequency) {
                    case "every order":
                        $message = get_translation("every_order", "Discount Frequencies", array("amount" => $amount), "%amount% off every order.", $options['customer_id']) . " $expire_message";
                    break;
                    case "one time":
                        $message = get_translation("one_time", "Discount Frequencies", array("amount" => $amount), "%amount% off next order. ", $options['customer_id']) . " $expire_message";
                    break;
                    case "until used up":
                        $message = get_translation("until_used_up", "Discount Frequencies", array("amount" => $amount), "%amount% credit.", $options['customer_id']) .  " $expire_message";
                    break;
                }

                $table .= $message."<br>";
            }
            //$table .="</table>";
            $customer->customerDiscounts = $table;
        } else {
            $customer->customerDiscounts = '';
        }

        $customer->lockersLocation = '';
        if ($customer->location_id) {
            $CI->load->model('location_model');
            $customer_location = $CI->location_model->get_by_primary_key($customer->location_id);

            if ($customer_location) {
                $customer->defaultLocation = $customer_location->address;
                if ($customer_location->location) {
                    $customer->lockersLocation = get_translation("lockers_location_text", "Email Macros",
                        array(
                            'location_address' => $customer_location->address,
                            'lockers_location' => $customer_location->location
                        ),
                        "Our lockers are %lockers_location%", $customer_id);
                }
            }
        }

        // show a macro on the first order
        $customer->firstOrderMessage = '';
        $order_count = $CI->customer_model->get_customer_order_count($options['customer_id']);
        if ($order_count == 1) {
            $customer->firstOrderMessage = get_translation("customer_first_order", "Email Macros",
                array(), 'Your order has been delivered back to you in a re-usable bag barcoded with your name. It is yours to keep and use on future orders.', $customer->customerID);
        }

        $data = array_merge($data,set_keys('customer',$customer));
    }

    if (isset($options['business_id'])) {
        $CI->load->model('business_model');
        $business = new Business($options['business_id']);
        $base_url = get_business_account_url($business, 'https://', false);

        $CI->load->model('did_you_know_model');
        $business->didYouKnow = $CI->did_you_know_model->get_random_note($business->businessID);
        $business->year = date("Y");

        $CI->load->model('image_placement_model');
        $email_logo = $CI->image_placement_model->get_by_name("e-mail", $business->businessID);
        if (!empty($email_logo)) {
            $business->email_logo_url = "https://myaccount." . $business->website_url . BUSINESS_IMAGES_DIRECTORY . $email_logo->filename;
        } else {
            $business->email_logo_url = '';
        }

        $business->barcode_url = $base_url . '/barcode.php?barcode=';
        $business->qrcode_url = $base_url . '/qrcode.php?qrcode=';

        $data = array_merge($data,set_keys('business',$business));
    }

    if (isset($options['laundryPlan_id']) && !empty($options['customer_id'])) {
        $CI->load->model('businesslaundryplan_model');
        $CI->businesslaundryplan_model->businessLaundryPlanID = $options['laundryPlan_id'];
        $CI->businesslaundryplan_model->business_id = $options['business_id'];
        $plan = $CI->businesslaundryplan_model->get();

        $get_laundry_plan_end_dates_query = $CI->db->query("SELECT DATE_FORMAT(startDate, '%Y-%m-%d') as startDate, DATE_FORMAT(endDate, '%Y-%m-%d') as endDate FROM laundryPlan WHERE customer_id = {$options['customer_id']} and type='laundry_plan'");
        $get_laundry_plan_end_dates_result = $get_laundry_plan_end_dates_query->result();
        $date_format = get_business_meta($customer->business_id, 'shortDateLocaleFormat', '%b %e, %Y');
        $plan[0]->endDate = strftime($date_format, strtotime($get_laundry_plan_end_dates_result[0]->endDate));
        $plan[0]->startDate = strftime($date_format, strtotime($get_laundry_plan_end_dates_result[0]->startDate));
        $data = array_merge($data,set_keys('laundryPlan',$plan[0]));
    }

    if (isset($options['everythingPlan_id']) && !empty($options['customer_id'])) {
        $plan = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(array(
            'businessLaundryPlanID' => $options['everythingPlan_id'],
            'business_id' => $options['business_id']
        ));

        $get_laundry_plan_end_dates_query = $CI->db->query("SELECT DATE_FORMAT(startDate, '%Y-%m-%d') as startDate, DATE_FORMAT(endDate, '%Y-%m-%d') as endDate FROM laundryPlan WHERE customer_id = {$options['customer_id']} and type='everything_plan'");
        $get_laundry_plan_end_dates_result = $get_laundry_plan_end_dates_query->result();
        $date_format = get_business_meta($customer->business_id, 'shortDateLocaleFormat', '%b %e, %Y');
        $plan[0]->endDate = strftime($date_format, strtotime($get_laundry_plan_end_dates_result[0]->endDate));
        $plan[0]->startDate = strftime($date_format, strtotime($get_laundry_plan_end_dates_result[0]->startDate));
        $data = array_merge($data,set_keys('everythingPlan',$plan[0]));
    }

    if (isset($options['productPlan_id']) && !empty($options['customer_id'])) {
        $plan = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(array(
            'businessLaundryPlanID' => $options['productPlan_id'],
            'business_id' => $options['business_id']
        ));

        $get_laundry_plan_end_dates_query = $CI->db->query("SELECT DATE_FORMAT(startDate, '%Y-%m-%d') as startDate, DATE_FORMAT(endDate, '%Y-%m-%d') as endDate FROM laundryPlan WHERE customer_id = {$options['customer_id']} and type='product_plan'");
        $get_laundry_plan_end_dates_result = $get_laundry_plan_end_dates_query->result();
        $date_format = get_business_meta($customer->business_id, 'shortDateLocaleFormat', '%b %e, %Y');
        $plan[0]->endDate = strftime($date_format, strtotime($get_laundry_plan_end_dates_result[0]->endDate));
        $plan[0]->startDate = strftime($date_format, strtotime($get_laundry_plan_end_dates_result[0]->startDate));
        $data = array_merge($data,set_keys('productPlan',$plan[0]));
    }

    if (isset($options['giftCard_id'])) {
        $sql = "SELECT
                        concat(prefix,code) as code,
                        fullName,
                        email,
                        amount,
                        description,
                        sentRecipientEmail
                        FROM coupon
                        WHERE couponID = {$options['giftCard_id']}";
        $giftCard = query($sql);

        // if the giftcard was emailed to the recipient
        if ($giftCard[0]->sentRecipientEmail) {
            $giftCard[0]->sentToRecipent = get_translation("giftcard_send_to_recipient", "Email Macros", array("email" => $giftCard[0]->email), "We have sent an email to %email% telling them the great news!", $customer->customerID);
        } else {
            $giftCard[0]->sentToRecipent = "";
        }

        $data = array_merge($data,set_keys('giftCard',$giftCard[0]));
    }

    if (isset($options['claim_id'])) {
        $CI->load->model('claim_model');
        $claim = $CI->claim_model->get_claim($options['claim_id']);

        // find out if this is same day or next day
        $cutOffTime = new DateTime(get_business_meta($options['business_id'], 'cutOff'), new DateTimeZone($business->timezone));
        $current_time = new DateTime("now", new DateTimeZone($business->timezone));

        $CI->load->model("locker_model");
        $locker = new Locker($claim[0]->lockerID);

        $CI->load->model("location_model");
        $location = new Location($locker->location_id);

        $dateInterval = new \DateInterval("P1D");
        //The following code determines the anticipated serviced time.
        if ($current_time >= $cutOffTime) {
            $current_time->add($dateInterval);
        }

        $homePickup = \App\Libraries\DroplockerObjects\OrderHomePickup::search(array(
            'claim_id' => $options['claim_id'],
        ));

        $serviceDays = Location_ServiceDay::search_aprax(array(
            "location_id" => $location->locationID,
            "location_serviceType_id" => 2
        ));

        $claim[0]->tracking_code = '';
        
        if ($homePickup) {

            $date_format = get_business_meta($customer->business_id, 'standardDateLocaleFormat', '%A, %B %e');

            $time_format = get_business_meta($customer->business_id, "hourLocaleFormat", "%l:%M %p");

            $start_time  = $homePickup->windowStart;
            $end_time    = $homePickup->windowEnd;

            if (is_object($start_time) && is_object($end_time)) {
                $old_tz = date_default_timezone_set();
                date_default_timezone_set($business->timezone);

                $pickup_date         = strftime($date_format, $start_time->getTimestamp());
                $pickup_window_start = strftime($time_format, $start_time->getTimestamp());
                $pickup_window_end   = strftime($time_format, $end_time->getTimestamp());

                date_default_timezone_set($old_tz);

                $pickup_time = get_translation("pickup_time_deliv", "Email Macros",
                    array(
                        'pickup_date' => $pickup_date,
                        'pickup_time_start' => $pickup_window_start,
                        'pickup_time_end' => $pickup_window_end,
                    ),
                    "This order will be picked up at %pickup_date% between %pickup_time_start% and %pickup_time_end%",
                    $customer->customerID
                );

                $claim[0]->tracking_code = $homePickup->trackingCode;
            }
            
            // This uses an optional delivery dates
            $start_time  = $homePickup->delivery_start_time;
            $end_time    = $homePickup->delivery_end_time;

            if (!empty($start_time) && !empty($end_time)) {
                $old_tz = date_default_timezone_set();
                date_default_timezone_set($business->timezone);

                $delivery_date       = strftime($date_format, $start_time->getTimestamp());
                $delivery_start_time = strftime($time_format, $start_time->getTimestamp());
                $delivery_end_time   = strftime($time_format, $end_time->getTimestamp());

                date_default_timezone_set($old_tz);

                $claim_delivery_time = get_translation("claim_delivery_time", "Email Macros",
                    array(
                        'delivery_date'         => translate_date($delivery_date, $customer->customerID),
                        'delivery_start_time'   => $delivery_start_time,
                        'delivery_end_time'     => $delivery_end_time,
                    ),
                    "This order will be delivered at %delivery_date% between %delivery_start_time% and %delivery_end_time%",
                    $customer->customerID
                );
            }
        } elseif ($serviceDays) {

            //The following loop calculates the anticipated service time based
            // on the specified service days for the location
            for ($x = 0; $x < 10; $x++) {

                $dayFound = Location_ServiceDay::search_aprax(array(
                    "day" => $current_time->format("N"),
                    "location_id" => $location->locationID,
                    "location_serviceType_id" => 2
                ));

                if ($dayFound && !is_holiday($current_time->format('Y-m-d'), $customer->business_id)) {
                    break;
                }

                $current_time->add($dateInterval);
            }

            $old_tz = date_default_timezone_get();
            date_default_timezone_set($business->timezone);

            $date_format = get_business_meta($customer->business_id, 'standardDateLocaleFormat', '%A, %B %e');
            $locale_pickup_time = strftime($date_format, $current_time->getTimestamp());
            $locale_pickup_time = translate_date($locale_pickup_time, $customer->customerID);

            date_default_timezone_set($old_tz);

            $pickup_time = get_translation("pickup_time_known", "Email Macros", array("time" => $locale_pickup_time), "This order will be picked up <strong> %time% </strong>", $customer->customerID);

        } else {
            $pickup_time = get_translation("pickup_time_unknown", "Email Macros", array(), "This order will be picked up at the next scheduled service time", $customer->customerID);
        }

        $claim[0]->pickup_time = $pickup_time;

        $claim[0]->delivery_time = empty($claim_delivery_time)?NULL:$claim_delivery_time;

        // alias for location title
        $claim[0]->locationTitle = $claim[0]->companyName;

        $CI->load->model("locationType_model");
        $locationTypes = $CI->locationType_model->get(array("locationTypeID" => $location->locationType_id));
        $locationType = $locationTypes[0];

        //FIXME: hardcoded locationType name
        //FIXME: needs translation
        if ($locationType->name == "Kiosk" || $locationType->name == "Lockers") {
            $claim[0]->locationType = "Locker";
        } else {
            $claim[0]->locationType = "Apt";
        }
        $data = array_merge($data,set_keys('claim',$claim[0]));
    }

    if (isset($options['order_id'])) {
        $CI->load->model('order_model');
        $CI->load->model("orderitem_model");
        $CI->load->model("bag_model");

        $order_id = $options['order_id'];

        $order = array($CI->order_model->getOrderAndLocation($order_id));

        // alias for location title
        $order[0]->locationTitle = $order[0]->companyName;

        // empty macros
        $data['order_deliv_button'] = '';
        $data['order_deliv_url'] = '';
        $data['order_deliv_text'] = '';
        $data['order_deliv_sms'] = '';

        // this actions get some macro from deliv
        $deliv_actions = array(
            'ready_for_home_delivery',
            'ready_for_pickup',
            'schedule_delivery',
            'order_inventoried',
            'new_claim',
            'order_inventoried_home_delivery',
        );

        if (in_array($options['do_action'], $deliv_actions) && !$customer->invoice) {

            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($order[0]->business_id);

            $location = new Location($order[0]->location_id);
            if ($homeDeliveryService && $homeDeliveryService->canCustomerRequestHomeDeliveryToLocation($customer, $location)) {

                $deliv_url = $base_url . '/account/orders/home_delivery/' . $order_id;
                $data['order_deliv_url'] = $deliv_url;

                if (in_array($options['do_action'], array('ready_for_pickup', 'schedule_delivery', 'order_inventoried_home_delivery'))) {
                    $data['order_deliv_button'] = get_translation("order_deliv_button", "Email Macros",
                        array('deliv_url' => $deliv_url), '<a href="%deliv_url%">Have your order delivered to your home</a>', $customer->customerID);

                    $data['order_deliv_sms'] = get_translation("order_deliv_sms", "Email Macros",
                        array('deliv_url' => $deliv_url), " Have it delivered to your home or office today. Click here %deliv_url%", $customer->customerID);
                } else {
                    $data['order_deliv_button'] = get_translation("order_deliv_button_2", "Email Macros",
                        array('deliv_url' => $deliv_url), '<a href="%deliv_url%">Home Delivery</a>', $customer->customerID);
                }

                $data['order_deliv_text'] = get_translation("order_deliv_text", "Email Macros",
                    array('deliv_url' => $deliv_url), " Did you know we are now delivering orders anywhere you want them?", $customer->customerID);
            }

        }

        $items = $CI->order_model->getItems($order_id);
        $CI->load->library('table');

        $header_bgcolor  = get_business_meta($options['business_id'], 'header_bgcolor', '#6699cc');
        $header_color    = get_business_meta($options['business_id'], 'header_color', 'white');
        $row_bgcolor     = get_business_meta($options['business_id'], 'row_bgcolor', '#eaeaea');
        $row_color       = get_business_meta($options['business_id'], 'row_color', 'black');
        $row_alt_bgcolor = get_business_meta($options['business_id'], 'row_alt_bgcolor', '#eaeaea');
        $row_alt_color   = get_business_meta($options['business_id'], 'row_alt_color', 'black');

        $tmpl = array (
				'table_open'=>'<table width="100%" border="0" cellpadding="2" cellspacing="1">',
                'heading_row_start'   => '<tr style="background-color:'.$header_bgcolor.'; color: '.$header_color.';">',
                'heading_row_end'     => '</tr>',
                'heading_cell_start'  => '<th>',
                'heading_cell_end'    => '</th>',
                'row_start'           => '<tr style="background-color:'.$row_bgcolor.'; color: '.$row_color.';">',
                'row_end'             => '</tr>',
                'cell_start'          => '<td>',
                'cell_end'            => '</td>',
                'row_alt_start'       => '<tr style="background-color:'.$row_alt_bgcolor.'; color: '.$row_alt_color.';">',
                'row_alt_end'         => '</tr>',
                'cell_alt_start'      => '<td>',
                'cell_alt_end'        => '</td>',
        );
        $CI->table->set_template($tmpl);
        $CI->table->set_heading(array(
            get_translation("header_item", "Email Macros", array(), "Item", $customer->customerID),
            get_translation("header_price", "Email Macros", array(), "Price", $customer->customerID),
            get_translation("header_quantity", "Email Macros", array(), "Qty", $customer->customerID),
            get_translation("header_total", "Email Macros", array(), "Total", $customer->customerID),
        ));

        foreach ($items as $item) { //The following conditional checks whether or not the itemID is set.
            // If the itemID is set, then we produce a link to the item detail for that item. Otherwise, we output just the item's display name.
            
            $item->displayName = App\Libraries\DroplockerObjects\ProductName::getNameByBusinessLanguageId($item->displayName, $item->product_id, $business_language_id);

            if (isset($item->itemID)) {
                $item_output = "<a href='$base_url/account/profile/item_detail/{$item->itemID}'> {$item->displayName} </a>";
            } else {
                $item_output = $item->displayName;
            }

            $item_output .= '<br>'.nl2br($item->notes);
            $CI->table->add_row($item_output, array( 'data'  => format_money_symbol($customer->business_id, '%.2n', $item->unitPrice),
                    'style' => "text-align: right"),
             array( 'data'  => $item->qty,
                    'style' => "text-align: right"),
             array( "data"  => format_money_symbol($customer->business_id, '%.2n', $item->unitPrice*$item->qty),
                    "style" => "text-align: right") );
        }

        $breakout_vat = $CI->order_model->is_breakout_vat($order_id);

        if (!$breakout_vat) {
            $subtotal = $CI->order_model->get_gross_total($order_id);
            $CI->table->add_row(array('', '', get_translation("subtotal_field", "Email Macros", array(), 'Subtotal', $customer->customerID), array("data" => format_money_symbol($customer->business_id, '%.2n', $subtotal), "style" => "text-align: right")));
        }

        $CI->load->model('ordercharge_model');
        $charges = $CI->ordercharge_model->getCharges($order_id);
        foreach ($charges as $charge) {
            $CI->table->add_row (
                array (
                        $charge->chargeType,
                        array("data" => format_money_symbol($customer->business_id, '%.2n', $charge->chargeAmount), "style" => "text-align: right"),
                        array("data" => 1, "style" => "text-align: right"),
                        array("data" => format_money_symbol($customer->business_id, '%.2n', $charge->chargeAmount), "style" => "text-align: right")
                    )
            );
        }

        $net_total = $CI->order_model->get_net_total($order_id);

        $taxes = $CI->order_model->get_stored_taxes($order_id);
        $tax = 0;
        foreach ($taxes as $taxItem) {
            $tax += $taxItem['amount'];
        }

        if ($breakout_vat) {
            $sub_total = $net_total - $tax;

            $CI->table->add_row(array('', '', get_translation('total', "Email Macros", array(), "Total", $customer->customerID), array("data" => format_money_symbol($customer->business_id, '%.2n', ( $net_total ) ), "style" => "text-align: right")));
            $CI->table->add_row(array('', '', get_translation("subtotal", "Email Macros", array(), "Subtotal", $customer->customerID), array("data" => format_money_symbol($customer->business_id, '%.2n', $sub_total), "style" => "text-align: right")));
        }

        if ($tax > 0 && count($taxes) == 1) {
            $CI->table->add_row(array('', '', get_translation("tax_field", "Email Macros", array(), "Tax", $customer->customerID), array("data" => format_money_symbol($customer->business_id, '%.2n', $tax), "style" => "text-align: right")));
        } else {
            $CI->load->model("taxgroup_model");
            foreach($taxes as $taxItem) {
                $taxGroup = $CI->taxgroup_model->get_by_primary_key($taxItem['taxGroup_id']);
                if ($taxGroup) {
                    // If the group exists, use its name as label
                    $tax_label = $taxGroup->name . ':';
                } else {
                    // Generate the tax label from the tax rate
                    $taxPercent = $taxItem['taxRate'] * 100;
                    $tax_label = get_translation('tax_percent', "Email Macros", array('taxPercent' => $taxPercent), "Tax %taxPercent%%", $customer->customerID);
                }

                // add each tax line
                $CI->table->add_row(array('', '', $tax_label, array("data" => format_money_symbol($customer->business_id, '%.2n', $taxItem['amount']), "style" => "text-align: right")));
            }
        }

        if (!$breakout_vat) {
            $CI->table->add_row(array('', '', get_translation('total', "Email Macros", array(), "Total", $customer->customerID), array("data" => format_money_symbol($customer->business_id, '%.2n', ( $net_total ) ), "style" => "text-align: right")));
        }


        $table = $CI->table->generate();
        $order[0]->itemTable = $table;

        // partially delivered message
        $order[0]->partiallyDeliveredMessage = '';
        if (isset($options['orderStatusOption_id']) && $options['orderStatusOption_id'] == 11) {
            //get the message
            $sql = "SELECT note FROM orderStatus WHERE orderStatusOption_id = 12 and order_id = {$order_id} ORDER BY DATE DESC LIMIT 1";
            $message = $CI->db->query($sql)->row();
            $order[0]->partiallyDeliveredMessage = $message->note;
        }

        try {
            $customer = new Customer($order[0]->customer_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $customer = null;
        }
        $code = $customer->phone;
        // last 4 of the customers phone
        $accessCode = substr($code, strlen(trim($code))-4);


        try {
            $locker = new Locker($order[0]->locker_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $locker = null;
        }

        if (Locker::isElectronicLock($locker->lockerLockType_id)) {
            if ($customer) {
                $code = $customer->phone;
            } else {

            }

            $code = get_translation("locker_code", "Email Macros",
                array("lockerName" => $locker->lockerName, "accessCode" => $accessCode), "   New Locker number: <strong> %lockerName% </strong>. <br />    Access code: <strong>%accessCode%</strong>.", $customer->customerID);

            $sql = "SELECT locker_id, lockerName FROM orderStatus
                    INNER JOIN locker ON locker_id = lockerID
                    WHERE order_id = {$order_id} and orderStatusOption_id = 1 order by date DESC limit 1";
            $pickupLockerName = $CI->db->query($sql)->row()->lockerName;

        } else {
            $code = "";
            $pickupLockerName = $locker->lockerName;
        }

        $CI->load->model("location_model");
        $CI->load->model("locationType_model");

        $locations = $CI->location_model->get(array("locationID" => $locker->location_id));
        $location = $locations[0];

        $CI->locationType_model->options = array("locationTypeID" => $location->locationType_id);
        $locationTypes = $CI->locationType_model->get();
        $locationType = $locationTypes[0];

        //FIXME: hardcoded locationType name
        //FIXME: needs translation
        if ($locationType->name == "Kiosk" || $locationType->name == "Lockers") {
            $order[0]->lockerLimited = get_translation("limited_lockers", "Email Macros", array(), "Lockers are limited at this location and we would appreciate if you could pickup your order so that other customers can use the locker. Orders left in a locker for more than 3 business days may be removed and brought back to our office.", $customer->customerID);
        } else {
            $order[0]->lockerLimited = '';
            $order[0]->locationType = "Apt";
        }


        $date_format = get_business_meta($order[0]->business_id, 'shortDateLocaleFormat', '%b %e, %Y');


        $order[0]->pickupLockerName = $pickupLockerName;
        $order[0]->lockerAccessCode = $accessCode;
        $order[0]->lockerCode = $code;
        $order[0]->Dump = print_r($order[0], 1);
        $order[0]->dateCreated = convert_from_gmt_locale($order[0]->dateCreated, $date_format, $options['business_id']);
        $order[0]->statusDate = convert_from_gmt_locale($order[0]->statusDate, $date_format, $options['business_id']);
        $order[0]->dueDate = strftime($date_format, strtotime($order[0]->dueDate));
        $order[0]->lockerList = empty($options['lockerListDescription'])?"":$options['lockerListDescription'];
        $data = array_merge($data,set_keys('order',$order[0]));
    }


    if (isset($options['customerDiscount_id'])) {

        $customerDiscount = $CI->db->get_where('customerDiscount', array('customerDiscountID'=>$options['customerDiscount_id']))->row();

        if (empty($customerDiscount)) {
            throw new Exception("customerDiscount [".$options['customerDiscount_id']."] not found but customerDiscount_id was passed.");
        }

        if ($customerDiscount->expireDate !='null'||$customerDiscount->expireDate!='') {
            $date_format = get_business_meta($options['business_id'], 'shortDateLocaleFormat', '%b %e, %Y');

            $customerDiscount->expireDate = convert_from_gmt_locale($customerDiscount->expireDate, $date_format);
        }
        $data = array_merge($data,set_keys('customerDiscount',$customerDiscount));

    }

    if (isset($options['lockerLoot_id'])) {
        $loot = null;

        $lockerLoot = $CI->db->get_where("lockerLoot", array('lockerLootID'=>$options['lockerLoot_id']))->row();

        if (empty($lockerLoot)) {
            throw new Exception("lockerLoot Object not found but lockerLoot_id was passed");
        }

        $customer = new Customer($lockerLoot->customer_id);


        $sql = "SELECT customer_id FROM orders WHERE orderID = {$lockerLoot->order_id}";
        $childCustomer_id = $CI->db->query($sql)->row()->customer_id;

        if (!empty($childCustomer_id)) {
            $childCustomer = new Customer($childCustomer_id);
            $loot->referralCustomerName = $childCustomer->firstName." ".$childCustomer->lastName;
            $loot->referralCustomerFirstName = $childCustomer->firstName;
            $loot->referralCustomerLastName = $childCustomer->lastName;
        }


        $points = $lockerLoot->points;
        $loot->points = (float) $points;
        $loot->referrerCustomerName = $customer->firstName." ".$customer->lastName;
        $loot->referrerCustomerFirstName = $customer->firstName;
        $loot->referrerCustomerLastName = $customer->lastName;

        $data = array_merge($data,set_keys('lockerLoot',$loot));

    }

    return $data;
}


/**
 *
 * loops through all the macros and sets the values
 * @param string $category
 * @param object $data
 * @return array $out
 */
function set_keys($category,$data)
{
    $CI = get_instance();
    $fullMacros = $CI->config->item('macros');
    $macros = $fullMacros[$category];
    foreach ($macros as $key=>$value) {
        $field = explode("_", $key, 2);
        if (isset($field[1])) {
            $out[$key] = $data->$field[1];
        } else {
            $out[$key] = $data->$field[0];
        }
    }

    return $out;
}


/**
 * Replaces macros delimited by % % in a string with specified the macro keys
 * @param string $string
 * @param array $data
 * @return string
 */
function email_replace($string, $data)
{
    if (!empty($data)) {
        foreach ($data as $key => $value) {
            $string = str_replace("<!--[%".$key."%]-->", $value, $string);
            $string = str_replace("%".$key."%", $value, $string);
        }
    }

    //if has pseudo macro %shorten% for Bitly use
    $string = $string;
    $rx = "/%shorten%https?:\/\/[\da-z\.-]+\.[a-z\.]{2,6}([\/\w \.-]*)*\/?/";
    preg_match_all($rx, $string, $matches, PREG_SET_ORDER);
    if(isset($data['business_id']) && $data['business_id']!=''){
        /*Bitly config*/
        $bitly_client_id = get_business_meta($data['business_id'], 'bitly_client_id');
        $bitly_client_secret = get_business_meta($data['business_id'], 'bitly_client_secret');
        $bitly_access_token= get_business_meta($data['business_id'], 'bitly_access_token');
        $bitlyObj = new Bitly($bitly_client_id, $bitly_client_secret, $bitly_access_token);
        foreach ($matches as $match){
            $matchItem = $match[0];
            $sourceUrl = $matchItem;
            //needed to work locally and do a replace, because domain .loc will fail at bitly
            if(substr($_SERVER['HTTP_HOST'],-4) == '.loc'){
                $matchItem = str_ireplace('.loc', '.com', $matchItem);
            }
            $originalUrl = str_ireplace('%shorten%', '', $matchItem);
            $result = $bitlyObj->shorten($originalUrl);
            $string = str_ireplace($sourceUrl, $result['url'], $string);
        }
    }
    return $string;
}

/**
 * Retrieves the correct email template in the appropriate language for a customer for the specified email action name
 * @param string $emailAction_name
 * @param int $customerID
 */
function get_emailTemplate_by_business_language_for_customer($emailAction_name, $customerID)
{
    $CI = get_instance();

    $CI->load->model("emailaction_model");
    if ($CI->emailaction_model->does_emailAction_name_exist($emailAction_name)) {
        $CI->load->model("customer_model");
        $customer = $CI->customer_model->get_by_primary_key($customerID);

        $CI->load->model("business_language_model");
        $business_languages = $CI->business_language_model->get_all_for_business($customer->business_id);

        //The following conditional checks to see if there are business languages defined for this business. If so, then we attempt to find a template based on a defined business language.
        if (empty($business_languages)) {
            $sql = "SELECT emailTemplate.*, emailActions.name FROM emailTemplate
                INNER JOIN emailActions ON emailActionID = emailAction_id
                WHERE emailActions.action = ? AND emailTemplate.business_id = ?";
            $emailTemplate = $CI->db->query($sql, array($emailAction_name, $customer->business_id))->row();
        } else {
            $business_language_id = get_customer_business_language_id($customer->customerID);

            //The following query retrieves the email template for the specified businesss language
            $sql = "SELECT emailTemplate.*, emailActions.name FROM emailTemplate
                INNER JOIN emailActions on emailActionID = emailAction_id
                WHERE emailActions.action = ? AND emailTemplate.business_id = ? AND emailTemplate.business_language_id = ?";

            $get_emailTemplate_by_language_query_result = $CI->db->query($sql, array($emailAction_name, $customer->business_id, $business_language_id))->row();

            //The following conditional checks to see if there exists a business language template for the specified business language.
            //If not, then we search for a template with a 0 value for business_language_id for the specified business.
            if (empty($get_emailTemplate_by_language_query_result)) {
                $sql = "SELECT emailTemplate.*, emailActions.name FROM emailTemplate
                    INNER JOIN emailActions on emailActionID = emailAction_id
                    WHERE emailActions.action = ? AND emailTemplate.business_id = ? AND business_language_id = 0";
                $get_emailTemplate_by_no_language_query_result = $CI->db->query($sql, array($emailAction_name, $customer->business_id))->row();
                $emailTemplate = $get_emailTemplate_by_no_language_query_result;
            } else {
                $emailTemplate = $get_emailTemplate_by_language_query_result;
            }
        }

        return $emailTemplate;
    } else {
        throw new Exception("There is no Email Action with the name '$emailAction_name'");
    }
}
