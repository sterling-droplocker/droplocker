<?php
function showIssuePic($x, $y, $displayName)
{
    echo '<div class="photo">
            <a href="#">
                <span></span>
                <input type=image src=';
    switch ($displayName) {
        case 'Laundered Shirt':
            echo '/images/item_launderedShirt.jpg';
            break;
        case 'Blouse, Sweater, Shirt':
            echo '/images/item_launderedShirt.jpg';
            break;
        case 'blazer':
            echo '/images/item_blazer.jpg';
            break;
        case 'polo':
            echo '/images/item_polo.jpg';
            break;
        default:
            echo '/images/item_onePiece.jpg';
            break;
    }
    echo ' width=175 height=175>
            </a>
        </div>

        <style type="text/css">
        .photo {
            margin: 0px;
            position: relative;
            width: 175px;
            height: 175px;
        }';

        if ($x) {
            echo '
            .photo span {
                width: 36px;
                height: 37px;
                display: block;
                position: absolute;
                top: '.($y-18).'px;
                left: '.($x-18).'px;
                background: url(/images/round_marker.jpg) no-repeat;
            }';
        }
        echo '</style>';
}
