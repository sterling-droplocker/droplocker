<?php
/**
 * Not, if using this in a controller, please use 'outputJSONsuccess' instead
 * Outputs a standardized ajax success response.
 * @param array $data
 * @param string message
 */
function output_ajax_success_response($data = array(), $message = null)
{
    $CI = get_instance();
    $CI->output->enable_profiler(false);
    $CI->output->set_content_type('application/json');
    $CI->output->set_output(json_encode(array("status" => "success", "data" => $data, "message" => $message)));
    $CI->output->_display();
    exit();
}

/**
 * Note, if using this in a controller, please use 'outputJSONerror' instead
 * Outputs a standardized axjax error message
 * @param string  $message
 */
function output_ajax_error_response($message)
{
    $CI = get_instance();
    $CI->output->enable_profiler(false);
    $CI->output->set_content_type('application/json');
    $CI->output->set_output(json_encode(array("status" => "error", "message" => $message)));
    $CI->output->_display();
    exit();
}
