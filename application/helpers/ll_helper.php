<?php

function callOut_25Percent($promo_code = 'WS1230')
{

/*<div class="calloutBanner calloutRight">
                <div class="calloutBannerPic">
                    <a href="/main/concierge" style="text-decoration:none;"><img src="/images/concierge_video.gif" alt="Video: Concierge Service" width="116" height="116" border="0"></a>
                </div>
                <div class="calloutBannerText">
                    <h2>Concierge Buildings</h2>We service virtually all concierge buildings in SF.<br />
                    <a href="main/concierge" class="btnLink_short" style="padding: 3px 0px 0px 0px;">Learn More</a>
                </div>
            </div>*/

$nav = '
    <div class="calloutBanner calloutRight">
        <div class="calloutBannerPic"><img src="/images/callOut_driver.jpg" alt="Driver Delivering Clothes" border="0" style="width: 100%"></div>
        <div>
            <div class="calloutBannerText">
                <h2>25% Off Your First Order</h2>
                <div style="">Sign up now and receive 25% off your first order</div>
                <div><a href="/account/main/view_registration?promo='.$promo_code.'" class="btn btn-primary btn-small">Get Started</a></div>
            </div>

        </div>';

    $nav.='</div>';

    return $nav;
}

function callOut_discount($promo_code = 'WS1230', $amount = '$10')
{

return '<div class="calloutBanner calloutRight">
        <div class="calloutBannerPic"><img src="/images/callOut_driver.jpg" alt="Driver Delivering Clothes" border="0" style="width: 100%"></div>
        <div>
            <div class="calloutBannerText">
                <h2>'.$amount.' Off Your First Order</h2>
                <div style="">Sign up now and receive '.$amount.' off your first order</div>
                <div><a href="/account/main/view_registration?promo='.$promo_code.'" class="btn btn-primary btn-small">Get Started</a></div>
            </div>
        </div></div>';

}


function callOut_concierge()
{
/*$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 267px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:15px; margin-left:0px;" class="graySmall">
            <div style="float:left; padding-top:8px; width:117px;"><a href="/main/concierge" style="text-decoration:none;"><img src="/images/concierge_video.gif" alt="Video: Concierge Service" width="116" height="116" border="0"></a></div>
            <div style="float:left; width:149px; padding-top:8px; line-height:20px;">
                <h2>Concierge Buildings</h2>We service virtually all concierge buildings in SF.<br>
                <div style="margin-left: 25px; padding-top:5px;"><a href="main/concierge" class="btnLink_short" style="padding: 3px 0px 0px 0px;">Learn More</a></div>
            </div>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>';*/

    $nav = '<div class="calloutBanner calloutRight">
                <div class="calloutBannerPic">
                    <a href="/main/concierge" style="text-decoration:none;"><img src="/images/concierge_video.gif" alt="Video: Concierge Service" width="116" height="116" border="0"></a>
                </div>
                <div class="calloutBannerText">
                    <h2>Concierge Buildings</h2>We service virtually all concierge buildings in SF.<br />
                    <a href="main/concierge" class="btnLink_short" style="padding: 3px 0px 0px 0px;">Learn More</a>
                </div>
            </div>';

    return $nav;
}


function callOut_women20()
{
    $nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
    <div style="float: right; width: 267px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
                    <div style="position:relative; margin-top:15px; margin-left:0px;" class="graySmall">
                            <div style="float:left; padding-top:40px; width:110px; vertical-align: middle;">
                                <img src="/images/logos/women20.png" alt="Women 2.0" width="100" height="40">
                            </div>
                            <div style="float:left; width:157px; padding-top:8px; line-height:20px;">
                                    <h2>5 Ways to Achieve Work-life Balance</h2>Simplify your life with Laundry Locker. <br>
                                    <a href="http://www.women2.org/5-ways-to-achieve-work-life-balance-hint-outsource-errands/" target="_blank" class="greenLink"><strong>Read the Article</strong></a>
                            </div>
                    </div>
    </div>
    <div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>';

    return $nav;

}

function callOut_sms()
{

$nav = '<div class="calloutBanner calloutRight">
        <div class="calloutBannerPic">
            <img src="/images/callOut_iphone.png" alt="Order Via SMS">
        </div>
        <div class="calloutBannerText graySmall">
            <h2>Order Via SMS</h2>
            <div style="margin-bottom:5px; margin-top:7px;">Text LAUNDRY to MOSIO (66746) to place your order</div>
            <div style="float: right"><a href="/main/plans" class="btn btn-primary">Learn More</a></div>
        </div>
</div>';

    return $nav;
}


function callOut_shoeShine()
{
$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 267px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:15px; margin-left:0px;" class="graySmall">
            <div style="float:left; padding-top:12px; width:120px;"><img src="/images/shoeShine.jpg" alt="Sparkling Shoe and Boot Shines" width="100" height="105" border="1"></div>
            <div style="float:left; width:147px; padding-top:8px; line-height:20px;">
                <h2>Shoe Shines</h2>Sparkling shoe shines now available through Laundry Locker.<br>
                <div style="margin-left: 25px; padding-top:5px;"><a href="/main/shoeshine" class="btnLink_short" style="padding: 3px 0px 0px 0px;">Learn More</a></div>
            </div>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>';

    return $nav;
}

function callOut_vaska()
{
$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 267px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:15px; margin-left:0px;" class="graySmall">
            <div style="float:left; width:260px; padding-top:5px; line-height:19px;">
                <img src="/images/logo_vaska.jpeg" alt="" width="140" height="35" hspace="15" vspace="10" border="0" align="left">
                We are excited to partner with Vaska, the world\'s most eco-friendly laundry detergent.<br>
                <div style="margin-left: 60px; padding-top:15px;"><a href="/account/profile/preferences" class="btnLink_med" style="padding: 3px 0px 0px 0px;">Try It For Free</a></div>
            </div>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>';

    return $nav;
}

function callOut_laundryPlan()
{
$nav = '<div class="calloutBanner calloutRight">
        <div class="calloutBannerPic">
            <img src="/images/laundryLady.gif" alt="Maid Holding Clean Laundry">
        </div>
        <div class="calloutBannerText graySmall">
            <h2>$.99 Wash & Fold</h2>
            <div style="margin-bottom:5px; margin-top:7px;">Sign up for a laundry Plan with rates as low as $.99 per lb</div>
            <div style="float: right"><a href="/main/plans" class="btn btn-primary">Learn More</a></div>
        </div>
</div>';

    return $nav;
}

function callOut_collegeLaundryPlan()
{
$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="../images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 267px; height: 158px; background-image: url(../images/callOut_background.jpg); text-align: left;">
        <div style="float: left; position:relative; margin-top:20px; margin-left:1px; width:100px;">
            <img src="../images/laundryLady.gif" alt="Maid Holding Clean Laundry">
        </div>
        <div style="float: left; position:relative; margin-top:15px; margin-left:9px; width:150px;" class="graySmall">
            <h2>$99 a Semester!</h2>
            <div style="margin-bottom:10px; margin-top:7px;">Compare laundry plans starting as low as $99 per semester.</div>
            <div style="margin-left: 25px;"><a href="/main/plans/college" class="btnLink_short" style="padding: 3px 0px 0px 0px;">Learn More</a></div>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>
';

    return $nav;
}


function callOut_iphone()
{
/*$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 139px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:20px; margin-left:15px;" class="graySmall">
            <h2>Download our iPhone app</h2>
            <br>
            <a href="https://itunes.apple.com/us/app/laundry-locker/id784630564?mt=8" target="_blank"><img src="/images/iphoneApp.png" width="135" height="40" alt=""></a>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_iphone.jpg" alt="iPhone Laundry App" width="140" height="154" border="0"></div>
';*/

    $nav = '  <div class="calloutBanner calloutRight" style="text-align: center">

                <h2>Download our iPhone app</h2>
                <img id="calloutIphone" src="/images/callOut_iphone.png" width="100"/>
                <a href="https://itunes.apple.com/us/app/laundry-locker/id784630564?mt=8" target="_blank">
                    <img src="/images/iphoneApp.png" width="135" height="40" alt=""></a>

            </div>';

    return $nav;
}


function callOut_newLocation()
{
$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 100px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:35px; margin-left:15px;" class="grayBig">
            Check out our new location in the Castro
        </div>
</div>
<div style="float: right; height: 158px; margin-top:2px;"><img src="/images/callOut_pickup.jpg" alt="" width="179" height="153" border="0"></div>
';

    return $nav;
}

function callOut_landlord()
{
$nav =
    '<div class="calloutBanner calloutRight">
        <div class="calloutBannerPic"><img src="/images/callOut_pickup.jpg" alt="Lockers and Happy Customers" border="0" style="width: 100%"/></div>
        <div class="calloutBannerText grayBig">
            <p class="grayBig">Property Manager, Building Owner?</p>

            <a href="/main/locationdetail" class="btn btn-primary">Learn About Lockers</a>
        </div>
    </div>

    ';

    return $nav;
}



function callOut_kiosk()
{

$nav = '<div class="calloutBanner calloutRight">
        <div class="calloutBannerPic">
        <img src="/images/callOut_driver.jpg" alt="Driver Delivery Dry Cleaning" border="0" style="width: 100%">
        </div>
        <div class="calloutBannerText">
            <h2>No Lockers In Your Building?</h2>
            <p class="graySmall">Try one of our 24/7 kiosks </p>
            <a href="/main/kiosk" class="btn btn-primary">Learn More</a>
        </div>
</div>

';

    return $nav;
}


function callOut_forbes()
{
/*$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 267px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:15px; margin-left:0px;" class="graySmall">
            <div style="float:left; padding-top:8px; width:120px;"><a href="http://www.youtube.com/watch?v=_EUblz4WFt8" target="_blank" style="text-decoration:none;"><img src="/images/forbes.gif" alt="Video: Laundry Locker Featured on Forbes" width="116" height="116" border="0"></a></div>
            <div style="float:left; width:147px; padding-top:8px; line-height:22px;">
                <h2>Forbes Feature</h2>Laundry Locker highlighted for industry leading use of technology<br>
                <a href="http://www.youtube.com/watch?v=_EUblz4WFt8" target="_blank" class="greenLink"><strong>Watch The Video</strong></a>
            </div>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>
';*/
    $nav = '<div class="calloutBanner calloutRight">
                <div class="calloutBannerPic">
                    <a href="http://www.youtube.com/watch?v=_EUblz4WFt8" target="_blank" style="text-decoration:none;"><img src="/images/forbes.gif" alt="Video: Laundry Locker Featured on Forbes" width="116" height="116" border="0"></a>
                </div>
                <div class="calloutBannerText">
                    <h2>Forbes Feature</h2>Laundry Locker highlighted for industry leading use of technology<br>
                    <a href="http://www.youtube.com/watch?v=_EUblz4WFt8" target="_blank" class="greenLink"><strong>Watch The Video</strong></a>
                </div>
            </div>';

    return $nav;
}


function callOut_secure()
{
$nav = '<div style="float: right; height: 158px; margin-right:3px;"><img src="/images/callOut_right.jpg" alt="" border="0"></div>
<div style="float: right; width: 267px; height: 158px; background-image: url(/images/callOut_background.jpg); text-align: left;">
        <div style="position:relative; margin-top:15px; margin-left:0px;" class="graySmall">
            <div style="float:left; padding-top:13px; width:110px;"><a href="http://www.PositiveSSL.com" target="_blank" style="font-family: arial; font-size: 10px; text-decoration: none;" title="SSL Certificate Authority"><img src="/images/PositiveSSL_tl_trans.gif" alt="SSL Certificate Authority" title="SSL Certificate Authority" border="0" /><br /></a></div>
            <div style="float:left; width:155px; padding-top:1px; line-height:15px;">
We take your security very seriously, and <strong>never store your card on our site</strong>. We use secure technologies from Verisign and Comodo, two of the most trusted companies in online business.
            </div>
        </div>
</div>
<div style="float: right; height: 158px;"><img src="/images/callOut_left.jpg" alt="" border="0"></div>
';

    return $nav;
}

function callOut_c20()
{
$nav = '

        <div class="calloutBanner calloutRight">
            <div class="calloutBannerPicc20"><img src="/images/camera.gif" alt="Camera to Take Pictures of Clothing"></div>
            <div class="calloutBannerTextc20">
                <h2>We Take Pictures!</h2>
                <div class="graySmall">Experience our patented Closet 2.0 wardrobe management software</div>
                <a href="/account/profile/closet" class="btn btn-primary">View My Closet 2.0</a>
            </div>
        </div>';

    return $nav;
}

function callOut_lockerLoot($activeCust, $points)
{
$nav = '<div style="float: right; margin-right:5px; text-align: left;">
        <div style="float: right;"><img src="/images/border_bigTop.jpg" alt="" width="285" height="12" border="0"></div>
        <div style="float:right; margin-right:2px; margin-left:10px; position:relative; padding-top:7px; margin-bottom:-7px; padding-bottom:10px; width:270px; padding-left:10px; border-left: 1px solid #C0C0C0; border-right: 1px solid #C0C0C0;" class="graySmall">
            <div style="width:250px;">
                Your Locker Loot balance is:<br>
                <div style="padding-top:8px; padding-left:25px; padding-bottom:10px;"><font size="+2">'.money_format('%.2n', $points).'</font></div>';
                if ($points > 0) {
                    $nav .= '<div style="padding-left:35px;"><a href="myLockerLoot.php?redeem=1" alt="return to my account" class="btnLink_long" style="padding-top:3px;">Redeem for LL Credit</a></div>';
                }
                if ($points > 200) {
                    $nav .= '<div style="padding-left:35px;"><a href="myLockerLoot.php?redeemCash=1" alt="return to my account" class="btnLink_long" style="padding-top:3px;">Send Check For '.money_format('%.2n', $points *.75).'</a></div>';
                }

                    $nav .= 'Share your <a href="mailto:?body=http://www.laundrylocker.com/register.php?promo=CR'.$activeCust.'" class="greenLink">Referral Code</a> and get $50!<br>
                    <div style="padding-top:8px; padding-left:5px; padding-bottom:10px;">
                        <textarea cols="29" id="txtArea" onClick="SelectAll(\'txtArea\')" rows="2">http://www.laundrylocker.com/register.php?promo=CR'.$activeCust.'</textarea>
                    </div>';


            $nav .= '</div>
        </div>
        <div style="float:right;"><img src="/images/border_bigBottom.gif" alt="" width="285" height="12" border="0"></div>
        <div style="padding-left: 78px; font-size: 12px;" class="bodyReg"><a href="lockerLootTerms.php" alt="Locker Loot Terms and Conditions">Locker Loot Terms and Conditions</a></div>
    </div>
';

    return $nav;
}

function callOut_redeem()
{
$nav = '<div style="float: right; margin-right:5px; text-align: left;">
        <div style="float: right; height:12px;"><img src="/images/border_bigTop.jpg" alt="" width="285" height="12" border="0"></div>
        <div style="float: right; margin-right:2px; margin-left:10px; position:relative; padding-top:7px; margin-bottom:-7px; padding-bottom:10px; width:270px; padding-left:10px; border-left: 1px solid #C0C0C0; border-right: 1px solid #C0C0C0;" class="graySmall">
            <div style="width:180px;" class="headingGreen">Redeem a Gift Card</div>
            <div class="bodyBig" style="padding-top:20px; padding-left:20px; width:200px;" align="right">
                <form action="/account/programs/discounts" method="post">
                <div style="text-align:left">
                Code:&nbsp;<input style="width:100%" type="text" name="code">
                </div><br>
                <input type="submit" name="submit" value="Redeem" class="button blue">
                </form>
            </div>
        </div>
        <div style="float:right;"><img src="/images/border_bigBottom.gif" alt="" width="285" height="12" border="0"></div>
    </div>';

    return $nav;
}

function callout_2day()
{
    $out = '<div class="inner" style="padding:5px 15px; width:80%; float:right;">
     <div class="hr" style="margin-top: 30px; margin-bottom: 10px; float:right; width:100%;"><hr /></div>
        <div class="graySmall" style="width: 100%; float: right; margin-right:3px; padding-top:10px;">
            Two-Day Service<br><br>
            Orders placed before 9am will be returned within two business days.  Monday through Saturday.<br><br>
            Saturdays - Deliveries Only - Not available at all locations.<br><br>
        </div>
        <div class="hr" style="margin-top: 10px; margin-bottom: 10px; float:right; width:100%; margin-right:3px;"><hr /></div>
    </div>';

    return $out;
}


function servicesNav($page = '')
{
    $CI = get_instance();
    $seg1 = $CI->uri->segment(1);
    $seg2 = $CI->uri->segment(2);

$nav = '



        <div class="blueGradient calloutServices">
            <div class="homeWhiteMed" style=""><a href="/main/services" class="homeWhiteMed">Our Services</a></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/dryclean" '; if ($page == 'dc') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Dry Cleaning</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/washfold" '; if ($page == 'wf') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Wash & Fold</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/shoeshine" '; if ($page == 'shoe') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Shoe &amp; Bag Repair</a></div>
            <div class="hr"><hr /></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/packagedelivery" '; if ($page == 'package') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Package Delivery</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/businessaccounts" '; if ($page == 'business') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Business Accounts</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/specials" '; if ($page == 'specials') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Specials</a></div>
            <div class="hr"><hr /></div>
            <a href="/register" class="btn btn-success btn-large">Get Started</a>
            <a href="/account/orders/new_order" class="btn btn-primary btn-large">Place an Order</a>
        </div>';

            return $nav;
}


function aboutNav($page = '')
{
$nav = '
    <div class="blueGradient calloutServices">



            <div class="homeWhiteMed" style="margin-top: 20px;"><a href="/main/about" class="homeWhiteMed">About Us</a></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/ourtechnology" '; if ($page == 'technology') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Our Technology</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/press" '; if ($page == 'press') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Press</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/contactus" '; if ($page == 'contact') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Contact Info</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/partners" '; if ($page == 'partners') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Our Partners</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/green" '; if ($page == 'green') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Green Cleaning</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/jobs" '; if ($page == 'jobs') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Now Hiring</a></div>
            <div class="hr"><hr /></div>

     </div>';

    return $nav;
}


function myAccountNav($page = '')
{
    $CI = get_instance();
    if (!empty($CI->customer->firstName) && !empty($CI->customer->lastName)) {
        $name = sprintf("%s %s's", $CI->customer->firstName, $CI->customer->lastName);
    } else {
        $name = "My";
    }
$nav = '
    <div style="position:relative; text-align: left;">

        <div style="float: right;"><img src="/images/innerNav_right.jpg" alt="" border="0"></div>
        <div style="background-image: url(/images/innerNav_background.jpg); float: right; width: 258px; height: 300px; text-align: left;">
            <div class="homeWhiteMed" style="margin-top: 20px;"><a href="/account/" class="homeWhiteMed"> ' . $name . ' Account</a></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/account/profile/closet" '; if ($page == 'c2') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Closet 2.0</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/account/orders/view_orders" '; if ($page == 'myOrders') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>My Orders</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/plans" '; if ($page == 'plans') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Laundry Plans</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/account/profile/preferences" '; if ($page == 'preferences') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>My Preferences</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/account/programs/rewards" '; if ($page == 'lockerLoot') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Locker Loot Rewards</a></div>
            <div class="hr"><hr /></div>
        </div>
        <div style="position:relative; float: right; padding:0px; margin:0px;"><img src="/images/innerNav_left.jpg" alt="" border="0"></div>
            <div style="position: absolute; bottom:5px; left:25.9px; padding:0px; margin:0px;"><img src="/images/button_sideNavBlue.jpg" alt="" border="0"></div>
            <div style="position: absolute; bottom:5px; left:176.9px; padding:0px; margin:0px;"><img src="/images/button_sideNavGreen.jpg" alt="" border="0"></div>
            <div style="position: absolute; bottom:43px; left:50px; padding:0px; margin:0px; width:100px;"><a href="/account/profile/preferences" class="buttonWhiteText">Preferences</a></div>
            <div style="position: absolute; bottom:43px; left:211px; padding:0px; margin:0px; width:150px;"><a href="/account/orders/new_order" class="buttonWhiteText">Place an Order</a></div>
        <div style="clear: right;"></div></div>';

    return $nav;
}

function howNav($page = '')
{
$nav = '<div class="blueGradient calloutServices">
            <div class="homeWhiteMed"><a href="/main/howto" class="homeWhiteMed">How It Works</a></div>

            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/lockerlocations" '; if ($page == 'lockerlocations') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Locker Locations</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/locations24hr" '; if ($page == 'publiclocations24hr') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>24 Hour Public Locations</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/concierge" '; if ($page == 'concierge') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Concierge Buildings</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/athome" '; if ($page == 'home') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>@ Home</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/atwork" '; if ($page == 'work') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>@ Work</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="/main/lockerloot" '; if ($page == 'lockerloot') {$nav .= 'class="homeWhiteSelected"'; } else {$nav .= 'class="homeWhiteSmall"'; } $nav .= '>Locker Loot</a></div>
            <div class="hr"><hr /></div></div>';
            //<a href="/register" class="btn btn-success btn-large">Get Started</a>
        //</div>
    return $nav;
}

function wifiNav($page = '')
{
$nav = '
    <div style="text-align: left;">

        <div style="float: right;"><img src="/images/innerNav_right.jpg" alt="" border="0"></div>
        <div style="background-image: url(/images/innerNav_background.jpg); float: right; width: 258px; height: 300px; text-align: left;">
            <div class="homeWhiteMed" style="margin-top: 20px;">Learn More</div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="how.php" class="homeWhiteSmall">How Laundry Locker Works</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="locations.php" class="homeWhiteSmall">Our Locations</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="green.php" class="homeWhiteSmall">Eco-Friendly Cleaning</a></div>
            <div class="hr"><hr /></div>
            <div style="margin-top: 6px; margin-bottom: 6px;"><img src="/images/bullet.gif" alt="" border="0">&nbsp;&nbsp;<a href="specials.php" class="homeWhiteSmall">Specials</a></div>
            <div class="hr"><hr /></div>
        </div>
        <div style="float: right;position:relative;"><img src="/images/innerNav_left.jpg" alt="" border="0">
            <div style="position: absolute; top:220px; left:53px;"><img src="/images/button_greenNav.jpg" alt="" border="0"></div>
            <div class="buttonWhiteText" style="position: absolute; top:235px; left:103px; width:150px;"><a href="register.php" class="buttonWhiteText">Get Started</a>
        </div>
        </div>

        <div style="clear: right;"></div>';

    return $nav;
}

function kiosk_hours()
{
    $nav = '<div class="hr" style="margin-top: 20px; margin-bottom: 10px; float:right; width:279px; margin-right:3px;"><hr /></div>
        <div class="graySmall" style="width: 279px; float: right; margin-right:3px; padding-top:10px;">
        <u>Kiosk Hours</u><br>
        24 Hours a Day. 7 Days a Week.<br><br>
        <u>Kiosk Locations</u><br>
        145 Valencia St.<br>
        2103 Van Ness Ave.<br>
        566 Haight St.<br>
        2069 Greenwich St.<br>
        775 Filbert St.<br>

        </div>
        <div class="hr" style="margin-top: 10px; margin-bottom: 10px; float:right; width:279px; margin-right:3px;"><hr /></div>';

    return $nav;
}
