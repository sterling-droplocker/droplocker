<?php
/**
    * Validates each parameter in the client request according to the specified rule set
    *
    * Takes an array of rules, each key being the name of a request parameter and
    * the value being an array of validation rules
    *
    * The following rules are available
    *  notEmpty
    *  numeric
    *  entityMustExist
    *
    * @param array $parameters rules in format 'field' => array('rule', ...)
    * @param string type Can be 'both', 'POST', or 'GET'
 */
function validateRequest($parameters, $type = "both")
{
    $CI = &get_instance();
    $errors = array();
    foreach ($parameters as $parameterName => $rules) {
        foreach ($rules as $rule) {
            switch ($type) {
                case "both":
                    $value = $CI->input->get_post($parameterName);
                    break;
                case "POST":
                    $value = $CI->input->post($parameterName);
                    break;
                case "GET":
                    $value = $CI->input->get($parameterNname);
            }

            $value = $CI->input->get_post($parameterName);
            switch ($rule) {
                case "notEmpty" :
                    if (empty($value)) {
                        $errors[$parameterName][] = "Can not be empty";
                    }
                    break;
                case "numeric":
                    if (!is_null($value)) { //If there is a value set for this parameter, validate it. Otherwise skip this validation
                        if (!is_numeric($value)) {
                            $errors[$parameterName][] = "Must be numeric";
                        }
                    }
                    break;
                case "entityMustExist":
                    if (!is_null($value)) { //If there is a value set for this parameter, validate it. Otherwise, skip this validation rule
                        $matches = array();
                        preg_match("/(.*)_id/", $property_name, $matches);
                        $nouns = explode("_", $matches[1]);
                        $capatilized_nouns = array_map(function ($word) {
                            return ucfirst($word);
                        }, $nouns);
                        $entity_name = implode("_", $capatilized_nouns);
                        try {
                            \App\Libraries\DroplockerObjects\Validation_Exception::verify_entity_exists($this->$property_name, $entity_name);
                        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                            $errors[$parameterName][] = $validation_exception->getMessage();
                        }
                    }
                    break;

                default:
                    throw new LogicException("'$rule' does not exist");
            }
        }
    }
    if (empty($errors)) {
        return array("status" => "success");
    } else {
        return array("status" => "fail", "errors" => $errors);
    }
}
