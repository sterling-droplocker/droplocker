<?php
class MY_Exceptions extends CI_Exceptions
{
    public function __construct()
    {
        parent::__construct();
    }
    public function show_404($page = "", $log_error = TRUE)
    {
        session_start();
        $_SESSION['page'] = $page;
        if ($page == "css" || $page == "favicon.ico") {
            parent::show_404();
        } else {
            $_SESSION['referred_url'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            header("Location: /main/show_404");
        }
    }
    public function show_error($heading, $message, $template = 'error_general', $status_code = 500)
    {
        try {
            $str = parent::show_error($heading, $message, $template = 'error_general', $status_code = 500);
            throw new Exception($str);
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $trace = "<h1>Call Trace</h1><pre>". $e->getTraceAsString(). "<pre>";
            //append our stack trace to the error message
            $err = str_replace('</div>', $trace.'</div>', $msg);
            echo $err;
        }
    }

}
