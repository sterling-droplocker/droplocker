<?php

class MY_Loader extends CI_Loader
{
    protected $_my_controller_paths     = array();

    protected $_my_controllers          = array();

    // written by AJ  sirderno@yahoo.com
    public function __construct()
    {
        parent::__construct();

        $this->_my_controller_paths = array(APPPATH);
    }


    public function controller($controller, $name = '', $db_conn = FALSE)
    {
        if (is_array($controller)) {
            foreach ($controller as $babe) {
                $this->controller($babe);
            }

            return;
        }

        if ($controller == '') {
            return;
        }

        $path = '';

        // Is the controller in a sub-folder? If so, parse out the filename and path.
        if (($last_slash = strrpos($controller, '/')) !== FALSE) {
            // The path is in front of the last slash
            $path = substr($controller, 0, $last_slash + 1);

            // And the controller name behind it
            $controller = substr($controller, $last_slash + 1);
        }

        if ($name == '') {
            $name = $controller;
        }

        if (in_array($name, $this->_my_controllers, TRUE)) {
            return;
        }

        $CI = get_instance();
        if (isset($CI->$name)) {
            show_error('The controller name you are loading is the name of a resource that is already being used: '.$name);
        }

        $controller = strtolower($controller);

        foreach ($this->_my_controller_paths as $mod_path) {
            if ( ! file_exists($mod_path.'controllers/'.$path.$controller.'.php')) {
                continue;
            }

            if ($db_conn !== FALSE AND ! class_exists('CI_DB')) {
                if ($db_conn === TRUE) {
                    $db_conn = '';
                }

                $CI->load->database($db_conn, FALSE, TRUE);
            }

            if ( ! class_exists('CI_Controller')) {
                load_class('Controller', 'core');
            }

            require_once($mod_path.'controllers/'.$path.$controller.'.php');

            $controller = ucfirst($controller);

            $CI->$name = new $controller();

            $this->_my_controllers[] = $name;

            return;
        }

        // couldn't find the controller
        show_error('Unable to locate the controller you have specified: '.$controller);
    }


    /**
     * Database Loader
     *
     * @access    public
     * @param    string    the DB credentials
     * @param    bool    whether to return the DB object
     * @param    bool    whether to enable active record (this allows us to override the config setting)
     * @return    object
     */
    public function database($params = '', $return = FALSE, $active_record = NULL)
    {
        // Grab the super object
        $CI = get_instance();

        // Do we even need to load the database class?
        if (class_exists('CI_DB') AND $return == FALSE AND $active_record == NULL AND isset($CI->db) AND is_object($CI->db)) {
            return FALSE;
        }

        require_once(BASEPATH.'database/DB'.EXT);

        // Load the DB class
        $db =& DB($params, $active_record);

        $my_driver = config_item('subclass_prefix').'DB_'.$db->dbdriver.'_driver';
        $my_driver_file = APPPATH.'core/'.$my_driver.EXT;

        if (file_exists($my_driver_file)) {
            require_once($my_driver_file);
            $db = new $my_driver(get_object_vars($db));
        }

        if ($return === TRUE) {
            return $db;
        }

        // Initialize the db variable.  Needed to prevent
        // reference errors with some configurations
        $CI->db = '';
        $CI->db = $db;
    }


}
