<?php
class My_Model extends CI_Model
{
    protected $tableName;
    protected $primary_key;

    // The following properties are *, do not use.
    public $options = array();
    public $required = array();
    public $joins = array();
    public $leftjoins = array();

    /**
     * Initializes the model
     *
     * @param string $tableName override $tableName
     * @param string $primary_key override $primary_key
     */
    public function __construct($tableName='', $primary_key = '')
    {
        parent::__construct();

        if ($tableName) {
            $this->tableName = $tableName;
        }

        if ($primary_key) {
            $this->primary_key = $primary_key;
        }
    }

    /**
     * This is undocumented *, do not use.
     * @deprecated
     * @param type $name
     * @param type $value
     */
    public function __set($name, $value)
    {
        $this->options[$name] = $value;
    }
    /**
     * Retreives every row from the table.
     * @return array
     */
    public function get_all()
    {
        return $this->db->get($this->tableName)->result();
    }
    /**
     * Returns a single result from a model baseed on the primary key
     * @param int $ID the primary key value
     * @return stdClass
     */
    public function get_by_primary_key($ID)
    {
        if (is_numeric($ID)) {
            $get_by_primary_key_query = $this->db->get_where($this->tableName, array($this->primary_key => $ID));
            $result = $get_by_primary_key_query->row();

            return $result;
        } else {
            throw new \InvalidArgumentException("'ID' must be numeric");
        }
    }
    /**
     * Determines whether or not a model entity exists based on the supplied primary key value or array of criteria
     * @param mixed $criteria The primary key of the entitity or an array of WHERE parameter criteria to search for an entity by
     * @return boolean True if the entity exists, False if the entity does not exist
     */
    public function does_exist($criteria)
    {
        if (is_numeric($criteria)) {
            $result = $this->get_by_primary_key($criteria);
            if (empty($result)) {
                return false;
            } else {
                return true;
            }
        } elseif (is_array($criteria)) {
            $entity = $this->get_one($criteria);
            if (empty($entity)) {
                return false;
            } else {
                return true;
            }
        } else {
            throw new \InvalidArgumentException("'criteria' must be either the primary key of an entity or an associative array of WHERE parameters");
        }
    }
    /**
     * Returns a single row from a result set
     * @param array $options where parameters
     * @return stdClass An object that contains the properties of the first result in the query set
     */
    public function get_one($options = array(), $order_by = null)
    {
        if (!is_array($options)) {
            throw new \InvalidArgumentException("'options' must be an array");
        } else {

            foreach ($options as $key => $value) {
                if (is_array($value)) {
                    $this->db->where_in($key, $value);
                } else {
                    $this->db->where($key, $value);
                }
            }

            if (!is_null($order_by)) {
                $this->db->order_by($order_by);
            }

            $query = $this->db->get($this->tableName, 1);

            return $query->row();
        }
    }
    /**
     * Retreives data based on the supplied criteria
     * @param $options array An array of WHERE parameters
     * @param $order_by string A column to order the results
     * @param $select string The fields to SELECT
     * @param limit int
     * @param offset int
     * @return array Consist of stdclasses with properties associated with the query result columns
     */
    public function get_aprax($options = array(), $order_by = null, $select = null, $limit = null, $offset = null)
    {
        if (!is_null($order_by)) {
            $this->db->order_by($order_by);
        }
        if ($select) {
            $this->db->select($select);
        }

        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $this->db->where_in($key, $value);
            } else {
                $this->db->where($key, $value);
            }
        }

        $query = $this->db->get($this->tableName, $limit, $offset);

        return $query->result();
    }
    /**
     * Retrieves a count of the nubmer of results for a query
     * @param array $options An associate array of WHERE parameters
     * @return int The count
     */
    public function count($options = array())
    {
        foreach ($options as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->from($this->tableName);

        return $this->db->count_all_results();
    }
    /**
     * This is *, do not use or I murder your face repeatedly
     * @deprecated use get_aprax() instead
     */
    public function get()
    {
        if ($this->required) {
                if(!required($this->required, $this->options))
                die("MISSING REQUIRED FIELDS:<br>DATABASE: ".$this->tableName."<hr>REQUIRED: <pre>".print_r($this->required, true)."</pre><hr>YOU PASSED:<pre>".print_r($this->options, true)."</pre><hr>");
        }
        if ($this->options) {
            foreach ($this->options as $key => $value) {
                if (($key != 'select') && ($key != 'order_by')) {
                    if (is_array($value)) {
                        $this->db->where_in($this->tableName.".".$key,$value);
                    } else {
                        $this->db->where($this->tableName.".".$key, $value);
                    }
                }
            }
        }

        if (isset($this->options['select'])) {
                $this->db->select($this->options['select']);
        }

        if (isset($this->options['order_by'])) {
                $this->db->order_by($this->options['order_by']);
        }

        if (sizeof($this->joins)) {
                foreach ($this->joins as $j) {
                        $a = explode(" ", $j);
                        //get tablename
                        $table = $a[0];
                        //remove tablename and "on"
                        unset($a[0]);
                        unset($a[1]);
                        //rebuild the statement
                        $statement = implode(" ",$a);

                        $this->db->join($table, $statement);
                }
        }

        if (sizeof($this->leftjoins)) {
                foreach ($this->leftjoins as $j) {
                        $a = explode(" ", $j);
                        //get tablename
                        $table = $a[0];
                        //remove tablename and "on"
                        unset($a[0]);
                        unset($a[1]);
                        //rebuild the statement
                        $statement = implode(" ",$a);

                        $this->db->join($table, $statement, "LEFT");
                }
        }

        $query = $this->db->get($this->tableName);

        return $query->result();
    }
        /**
         * @deprecated
         * @return int The number of affected rows.
         */
    public function insert()
        {
            $this->db->insert($this->tableName,$this->options);

            return $this->db->insert_id();
    }
    /**
         * This is *, do not use or I murder your face repeatedly
         * @deprecated
         */
    public function update()
        {
            $where = $this->options['where'];
            unset($this->options['where']);
            $this->db->update($this->tableName, $this->options, $where);

            return $this->db->affected_rows();
    }
        /**
         * The following function deletes rows in the table by the specified where_parameters
         * @param array $where_parameters An associat5e list of parameters to be put into the WHERE part of the delete query
         * @return int The nubmer of deleted rows in the table
         */
        public function delete_aprax($where_parameters)
        {
            $this->db->delete($this->tableName, $where_parameters);

            return $this->db->affected_rows();
        }
    /**
         * This is *, do not use or I murder your face repeatedly
         * @deprecated Use delete_aprax instead.
         * @return int The number of affected rows.
         */
    public function delete()
    {
            $this->db->delete($this->tableName, $this->options);

            return $this->db->affected_rows();
    }

        /**
         * THis is *, do not use
         * @deprecated since version number
         * @param type $data
         */
    public function required($data=array())
    {
        $this->required = $data;
    }

        /**
         * This is *, do not use
         * @param type $sql
         * @return type
         */
    public function query($sql)
    {
        $q = $this->db->query($sql);

        return $q->result();
    }

    /**
         * THis is *, do not use
         * @deprecated
         * @param type $statement
         */
    public function join($statement)
    {
        if(!in_array($statement, $this->joins))
        $this->joins[] = $statement;
    }

    /**
         * This is *, do not use
         * @deprecated
         * @param type $statement
         */
    public function leftjoin($statement)
    {
        if(!in_array($statement, $this->leftjoins))
        $this->leftjoins[] = $statement;
    }

        /**
         * This is *, do not use
         * @deprecated
         */
    public function clear()
    {
        $this->options = array();
    }

        /**
         * This is * execrement, do not use
         * @deprecated
         * @param type $name
         * @return type
         */
    public function get_option($name)
    {
        return $this->options[$name];
    }

    /**
     * Inserts or Updates
     *
     * If options contains the primaryKey this will perform and update, otherwise it will do an insert
     * Use $ignore = true to generate an INSERT IGNORE statement
     *
     * @param array $options
     * @param string $ignore
     */
    public function save($options, $ignore = false)
    {
        if (!empty($options[$this->primary_key])) {
            $conditions = array($this->primary_key => $options[$this->primary_key]);
            unset($options[$this->primary_key]);
            $this->db->update($this->tableName, $options, $conditions);

            return $this->db->affected_rows();
        }

        $sql = $this->db->insert_string($this->tableName, $options);
        if ($ignore) {
            $sql = 'INSERT IGNORE' . substr($sql, 6);
        }
        $this->db->query($sql);

        return $this->db->insert_id();
    }

    /**
     * Updates all the fields
     *
     * @param fields $options
     * @param conditions $conditions
     */
    public function update_all($options, $conditions)
    {
        $this->db->update($this->tableName, $options, $conditions);
        return $this->db->affected_rows();
    }

    public function loadReportingDatabase() 
    {
        $CI = get_instance();
        $CI->reports_db = $CI->load->database("reporting", TRUE);
        if (DEV) {
            $CI->reports_db = $CI->db;
        }        
    }
    
    public function getIdBySlug($slug)
    {
        $record = $this->get_one(array(
            "slug" => $this->generateSlug($slug),
        ));

        return $record->{$this->primary_key};
    }
    
    protected function generateSlug($name)
    {
        return strtolower(str_replace(' ', '-', $name));
    }
}
