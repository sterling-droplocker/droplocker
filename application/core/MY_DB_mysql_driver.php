<?php


class MY_DB_mysql_driver extends CI_DB_mysql_driver
{

    protected $init = false;

    final public function __construct($params)
    {
        parent::__construct($params);
    }

    public function init()
    {
        parent::query("SET net_write_timeout=3600");
        $this->init = true;
    }


    /**
     * Extends the base query function to throw an exception if there is a error executing the query.
     * @param type $sql
     * @param type $binds
     * @param type $return_object
     * @return type
     * @throws \Database_Exception
     */
    public function query($sql, $binds = FALSE, $return_object = TRUE)
    {
        if (!$this->init) {
            $this->init();
        }

        $result = parent::query($sql, $binds, $return_object);

        if (!$result || $this->_error_message()) {
            throw new \Database_Exception($this->_error_message()."\n".$sql, $this->last_query(), $this->_error_number());
        }

        return $result;
    }

    final public function reset_select()
    {
        $this->_reset_select();
    }
}
