<?php

use App\Libraries\DroplockerObjects\DropLockerEmail;
use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\App;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\Newsletter;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\Upcharge;
use App\Libraries\DroplockerObjects\UpchargeSupplier;
use App\Libraries\DroplockerObjects\App\Libraries\DroplockerObjects;

/**
 * Base class for controllers
 */
class MY_Controller extends CI_Controller
{
    /**
     * Utility method for sending a json error response
     *
     * @param string $message
     * @param array $data
     */
    protected function outputJSONerror($message, $data = array())
    {
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode(array(
            "status" => "error",
            "message" => $message,
            "data" => $data)
        ));
        $this->output->_display();
        exit();
    }

    /**
     * Utility method for sending a json success response
     * @param string $message
     * @param array $data
     */
    protected function outputJSONsuccess($message, $data = array())
    {
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode(array(
            "status" => "success",
            "message" => $message,
            "data" => $data)
        ));
        $this->output->_display();
        exit();
    }

    /**
     * Validates each parameter in request according to the specified rule set
     *
     * Takes an array of rules, each key being the name of a POST parameter and
     * the value being an array of validation rules
     *
     * The following rules are available
     *  notEmpty
     *  numeric
     *  entityMustExist
     *
     * @param array $parameters rules in format 'field' => array('rule', ...)
     * @param string type Can be 'both', 'POST', or 'GET'
     */
    protected function validateRequest($parameters, $type = "both")
    {
        $errors = array();
        foreach ($parameters as $parameterName => $rules) {
            foreach ($rules as $rule) {
                switch ($type) {
                    case "both":
                        $value = $this->input->get_post($parameterName);
                        break;
                    case "POST":
                        $value = $this->input->post($parameterName);
                        break;
                    case "GET":
                        $value = $this->input->get($parameterName);
                }

                $value = $this->input->get_post($parameterName);
                switch ($rule) {
                    case "notEmpty" :
                        if (empty($value)) {
                            $errors[$parameterName][] = "Can not be empty";
                        }
                        break;
                    case "numeric":
                        if (!is_null($value)) { //If there is a value set for this parameter, validate it. Otherwise skip this validation
                            if (!is_numeric($value)) {
                                $errors[$parameterName][] = "Must be numeric";
                            }
                        }
                        break;
                    case "entityMustExist":
                        if (!is_null($value)) { //If there is a value set for this parameter, validate it. Otherwise, kip this validation rule
                            $matches = array();
                            preg_match("/(.*)_id/", $property_name, $matches);
                            $nouns = explode("_", $matches[1]);
                            $capatilized_nouns = array_map(function ($word) {
                                return ucfirst($word);
                            }, $nouns);
                            $entity_name = implode("_", $capatilized_nouns);
                            try {
                                \App\Libraries\DroplockerObjects\Validation_Exception::verify_entity_exists($this->$property_name, $entity_name);
                            } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                                $errors[$parameterName][] = $validation_exception->getMessage();
                            }
                        }
                        break;

                    default:
                        throw new LogicException("'$rule' does not exist");
                }
            }
        }
        if (empty($errors)) {
            return array("status" => "success");
        } else {
            return array("status" => "fail", "errors" => $errors);
        }

    }
}

/**
 * Base class for API controllers
 */
class MY_Api_Controller extends MY_Controller
{

    protected $client = "IPhone";
    protected $data;
    protected $business_id;
    protected $translation_domain = "mobile/api";

    public function __construct()
    {
        parent::__construct();
        $this->load->library("api");

        header("Access-Control-Allow-Headers: origin, x-requested-with, content-type");
        header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
        header("Access-Control-Allow-Origin: *");

        // copy $_POST to $_GET if it's a POST request
        if ($this->input->post() && !$this->input->get()) {
            $_GET = $this->input->post();
        }

        // log the request
        \App\Libraries\DroplockerObjects\Api_Request_Log::create();

        // detect the client type based on user agent
        if ($ua = $this->agent->agent_string()) {
            if (preg_match("#Darwin/#", $ua)) {
                $this->client = "IPhone";
            } else {
                $this->client = "Android";
            }
        }
        
        // require token parameter
        if (!$this->input->get("token")) {
            exit(json_encode(array(
                "status" => "error",
                "message" => "'token' must be passed as a GET parameter"
            )));
        }

        $token = $this->input->get("token");
        $this->load->model("app_model");
        $app = $this->app_model->get_one(array("token" => $token));

        // Exit if no app found by token
        if (empty($app)) {
            exit(json_encode(array(
                "status" => "error",
                "message" => "Application Token '$token' not found"
            )));
        }

        // Exit if app secret is empty
        if (empty($app->secret)) {
            exit(json_encode(array(
                "status" => "error",
                "message" => "There is no secret key defined for the application identified by token '$token'"
            )));
        }

        $secretKey = $app->secret;
        $output['secretKey'] = $secretKey;
        $receivedSignature = $this->input->get('signature');
        unset($_GET['signature']);

        $generatedSignature = $this->api->generateSignature($_GET, $secretKey);

        // exit if signature does not match
        if ($generatedSignature != $receivedSignature) {
            die(json_encode(array(
                'status' => 'fail', //TODO: This should be "error"
                'message' => 'Invalid Signature'
            )));
        }

        $output['data'] = $this->input->get();
        $output['token'] = $app->token;
        $output['name'] = $app->name;
        $output['dateCreated'] = $app->dateCreated;
        $output['business_id'] = $app->business_id;
        $this->data = $output;

        $this->business_id = $app->business_id;
    }
    
    protected function getSession()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$this->input->get("sessionToken")) {
            output_ajax_error_response("'sessionToken' must be passed as a GET parameter");
        }

        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Session token not found");
        }

        return $session;
    }

    /**
     * Gets the customer from the session
     * @return object
     */
    protected function getCustomer()
    {
        $session = $this->getSession();
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($session->customer_id);
        
        if (!$customer) {
            output_ajax_error_response(get_translation("no_customer_account", $this->translation_domain, array(), "The customer account does not exist", null, $this->input->get("business_languageID")));
        }

        return $customer;
    }

    protected function requiredParameters($requiredFields)
    {
        $out = array();
        foreach ($requiredFields as $requiredField) {
            $value = $this->input->get($requiredField);
            if (empty($value)) {
                output_ajax_error_response($this->getRequiredParamMessage($requiredField));
            }
            $out[$requiredField] = $value;
        }

        return $out;
    }
    
    protected function getRequiredParamMessage($field)
    {
        $fieldLabel = get_translation($field, $this->translation_domain, array(), $field, null, $this->input->get("business_languageID"));
        return get_translation("required_field", $this->translation_domain, array('field' => $fieldLabel), "'%field%' is required and can not be empty", null, $this->input->get("business_languageID"));
    }
}

/**
 * Base class for Driver API controllers
 */
class MY_Driver_Api_Controller extends MY_Controller
{
    protected $clientType = "IPhone";
    
    protected $_session;
    protected $business_id;
    protected $employee_id;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library("api");

        header("Access-Control-Allow-Headers: origin, x-requested-with, content-type");
        header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
        header("Access-Control-Allow-Origin: *");

        // copy $_POST to $_GET if it's a POST request
        if ($this->input->post() && !$this->input->get()) {
            $_GET = $this->input->post();
        }

        // log the request
        \App\Libraries\DroplockerObjects\Api_Request_Log::create();

        // detect the client type based on user agent
        if ($ua = $this->agent->agent_string()) {
            if (preg_match("#Darwin/#", $ua)) {
                $this->clientType = "IPhone";
            } else {
                $this->clientType = "Android";
            }
        }
        }
    
    protected function requiredParameters($requiredFields)
    {
        $out = array();
        foreach ($requiredFields as $requiredField) {
            $value = $this->input->get($requiredField);
            if (empty($value)) {
                output_ajax_error_response("'$requiredField' must be passed as a GET parameter and can not be empty.");
            }
            $out[$requiredField] = $value;
        }

        return $out;
    }
    
    protected function loadSession()
    {
        //very very nasty, temporary workaround to be able to show
        if ($this->get['employee_id']) {
            return $this->loadInternalSession();
        }
        
        $data = $this->requiredParameters(array(
            'sessionToken',
        ));
        
        $this->load->model('driver_apisession_model');
        $this->_session = $this->driver_apisession_model->get_one(array(
            'token' => $data['sessionToken'],
        ));

        if (!$this->_session) {
            output_ajax_error_response("Session token not found");
        } else {
            $this->employee_id = $this->_session->employee_id;
            $this->business_id = $this->_session->business_id;
        }
    }
    
    protected function loadInternalSession()
    {
        $this->load->model("employee_model");
        $employee = $this->employee_model->get(array(
            'employeeID' => $this->get['employee_id'],
        ));

        $this->employee_id = $employee[0]->employeeID;
        $this->business_id = $employee[0]->business_id;
    }
    
    /**
     * Gets the employee from the session
     * @return object
     */
    protected function getEmployee()
    {
        $this->loadSession();
        $this->load->model("employee_model");
        $employee = $this->employee_model->get(array(
            'employeeID' => $this->employee_id,
        ));
        
        if (!$employee) {
            output_ajax_error_response("The employee account does not exists");
        }

        return $employee[0];
    }

}


/**
 * Base class for Driver API controllers
 */
class MY_Mobile_Api_Controller extends MY_Controller
{
    public static $SUCCESS = 'success';
    public static $ERROR = 'error';
    
    protected $defaultSessionTimeout = 365;
    protected $time;

    //app slug
    protected $app;
    protected $isSignedApp = false;
    
    // Allows the employee to be validated by just using the employee_id request param
    protected $enableInsecureAuth = false;
    
    protected $clientType = "IPhone";
    
    protected $params;
    protected $_session;
    public $business_id; //if not, used models calling $this->business_id will fail (???)
    protected $employee_id;
    protected $employee;
    
    public function __construct()
    {
        parent::__construct();
        $this->benchmark->mark(__CLASS__ . '_start');
        
        // log the request
        \App\Libraries\DroplockerObjects\Api_Request_Log::create();

        $this->setAccessControl();
        $this->loadParams();
        $this->loadClientType();
        
        if ($this->isSignedApp) {
            $this->checkRequestSignature();
        }
    }
    
    protected function setAccessControl()
    {
        header("Access-Control-Allow-Headers: origin, x-requested-with, content-type");
        header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
        header("Access-Control-Allow-Origin: *");
    }
    
    protected function loadParams()
    {
        // copy $_POST to $_GET if it's a POST request
        if ($this->input->post() && !$this->input->get()) {
            $_GET = $this->input->post();
        }
        
        $this->params = array_map('trimArray', $this->input->get());
    }
    
    protected function loadClientType()
    {
        // detect the client type based on user agent
        $ua = $this->agent->agent_string();
        if ($ua) {
            if (preg_match("#Darwin/#", $ua)) {
                $this->client = "IPhone";
            } else {
                $this->client = "Android";
            }
        }
        
    }
    
    protected function checkRequestSignature()
    {
        $this->load->library("api");
        
        // require token parameter
        if (!$this->input->get("token")) {
            output_ajax_error_response("'token' must be passed as a GET parameter");
        }

        $token = $this->input->get("token");
        $this->load->model("app_model");
        $app = $this->app_model->get_one(array("token" => $token));

        // Exit if no app found by token
        if (empty($app)) {
            output_ajax_error_response("Application Token '$token' not found");
        }

        // Exit if app secret is empty
        if (empty($app->secret)) {
            output_ajax_error_response("There is no secret key defined for the application identified by token '$token'");
        }

        $receivedSignature = $this->input->get('signature');
        unset($_GET['signature']);

        $secretKey = $app->secret;
        $generatedSignature = $this->api->generateSignature($_GET, $secretKey);

        // exit if signature does not match
        if ($generatedSignature != $receivedSignature) {
            output_ajax_error_response('Invalid Signature');
        }

        $this->business_id = $app->business_id;
    }
    
    /*************** Tools ****************/

    protected function loadDefaults($defaults)
    {
        foreach ($defaults as $name => $default) {
            if (empty($this->params[$name])) {
                $this->params[$name] = $default;
            }
        }
    }
    
    protected function checkRequiredParameters($requiredFields, $checker = null)
    {
        if (!$checker) {
            $checker = function ($arg) {
                return empty($arg);
            };
        }
        
        foreach ($requiredFields as $requiredField) {
            if ($checker($this->params[$requiredField])) {
                output_ajax_error_response("'$requiredField' must be passed as a GET parameter and can not be empty.");
            }
        }
    }
    
    protected function checkAcl($aclRoleList)
    {
        $aclRoleList = (array) $aclRoleList;
        $this->loadEmployee();
        $business_employee_id = $this->employee->ID;
       
        foreach ($aclRoleList as $aclRole) {
            $result = checkAccessForApi($aclRole, $business_employee_id);
            
            if (!$result['status']) {
                output_ajax_error_response($result['message']);
            }
        }
    }
    
    /*************** session load ****************/

    protected function loadSession()
    {
        //very very nasty, temporary workaround to be able to show
        if ($this->enableInsecureAuth && $this->params['employee_id']) {
            return $this->loadInternalSession();
        }
        
        $this->checkRequiredParameters(array(
            'sessionToken',
        ));
        
        $this->load->model('mobile_apisession_model');
        $this->_session = $this->mobile_apisession_model->get_one(array(
            'token' => $this->params['sessionToken'],
            'app' => $this->app,
        ));

        if (!$this->_session) {
            output_ajax_error_response("Session token not found");
        } else if ($this->_session->expireDate < gmdate('Y-m-d H:i:s')) {
            output_ajax_error_response("Session token expired at " . $this->_session->expireDate);
        } else {
            $this->employee_id = $this->_session->employee_id;
            $this->business_id = $this->_session->business_id;
        }
    }
    
    protected function loadInternalSession()
    {
        $this->load->model("employee_model");
        $employee = $this->employee_model->get(array(
            'employeeID' => $this->params['employee_id'],
        ));

        $this->employee_id = $employee[0]->employeeID;
        $this->business_id = $employee[0]->business_id;
    }
    
    /**
     * Gets the employee from the session
     * @return object
     */
    protected function loadEmployee()
    {
        $this->loadSession();
        $this->load->model("employee_model");
        $this->employee = $this->getEmployeeData($this->employee_id);

        if (!$this->employee) {
            output_ajax_error_response("The employee account does not exists");
        }
        
        $this->load->model('business_model');
        $this->business = $this->business_model->get_one(array(
            'businessID' => $this->business_id,
        ));
    }
    
    protected function loadBusinessTime() 
    {
        $business = $this->business;
        
        if (!$business) {
            $business_id = $this->business_id;
            
            if (!$business_id) {
                $this->loadSession();
                $business_id = $this->business_id;
            }
            
            if ($business_id) {
                $this->load->model('business_model');
                $business = $this->business_model->get_one(array(
                    'businessID' => $business_id,
                ));
            }
        }
        
        if ($business) {
            setlocale(LC_ALL, $business->locale);
            date_default_timezone_set($business->timezone);
        } else {
            output_ajax_error_response("Unable to load time info for business");
        }
    }


    /*************** login ****************/
    
    protected function validateLoginData($employeeData)
    {
        return true;
    }

    public function login()
    {
        try {
            $this->checkRequiredParameters(array(
                'username',
                'password',
            ));

            $this->load->model('employee_model');
            $loggedData = $this->employee_model->login($this->params['username'] , $this->params['password']);

            if (!$loggedData) {
                output_ajax_error_response(get_translation("employee_validate_wrongCredentials","mobile/base", array(), 
                    "Wrong username or password", null, $this->params["business_languageID"]));
            }

            $employeeData = $this->getEmployeeData($loggedData->employeeID);
            $business_id = $employeeData->business_id;
            $employeeData->business = $this->getBusinessData($business_id);
            $this->validateLoginData($employeeData);


            $plusDays = intval($this->params["sessionTokenExpireDays"] ? $this->params["sessionTokenExpireDays"] : $this->defaultSessionTimeout);
            $expireDate = gmdate('Y-m-d H:i:s', strtotime("+$plusDays days"));

            $sessionData = $this->setupSession($business_id, $employeeData->employeeID, $expireDate);

            $employeeData->business = $this->getSessionBusinessData();

            $sessionData['employee'] = $employeeData;
            $sessionData = $this->enhanceLoginData($sessionData);

            output_ajax_success_response($sessionData, 'Employee logged in');
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    

    public function validateLogoutData() 
    {
        
    }

    public function onLogOut()
    {
        
    }
    
    public function logout()
    {
        try {
            $this->validateLogoutData();
            $this->loadEmployee();
            $this->onLogOut();
            output_ajax_success_response(array(), "Logged off successfuly");
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    protected function getEmployeeData($employee_id)
    {
        $employeeData = $this->employee_model->_get_one(array(
            'employeeID' => $employee_id,
            'include_role_data' => true,
        ));
        
        return $employeeData;
    }
    
    public function getBusiness()
    {
        $this->loadSession();
        output_ajax_success_response(array('business' => $this->getSessionBusinessData()), "Business {$this->business_id} data");
    }
    
    protected function getSessionBusinessData()
    {
        return $this->getBusinessData($this->business_id);
    }
    
    protected function getBusinessData($business_id)
    {
        $serialized = array(
            'serviceTypes',
        );
        $secret = array(
            'facebook_api_key',
            'facebook_secret_key',
        );
        
        
        $this->load->model('business_model');
        $business = $this->business_model->get_one(array(
            'businessID' => $business_id,
        ));
        
        foreach ($serialized as $s) {
            $business->$s = unserialize($business->$s);
        }
        
        foreach ($secret as $remove) {
            unset($business->$remove);
        }
        
        $this->load->model('location_model');
        $business->locations = $this->location_model->get(array(
            'select' => "locationID as id, address as label",
            
            'business_id' => $business_id,
            'status' => "active",
        ));
        
        array_unshift($business->locations, array('id' => 0, 'label' => 'Select location'));
        $location_ids = array_column_7($business->locations, 'id');
        
        $business->locations = array_combine($location_ids, $business->locations);
        
        return $business;
    }
    
    protected function enhanceLoginData($data)
    {
        return $data;
    }
    
    /**
     * Creates or renews an api session
     *
     * @param int $business_id
     * @param int $employee_id
     * @param string $expireDate
     * @return array
     */
    protected function setupSession($business_id, $employee_id, $expireDate = null)
    {
        // default expires in 24hs
        if (empty($expireDate)) {
            $expireDate = gmdate('Y-m-d H:i:s', strtotime("+{$this->defaultSessionTimeout} days"));
        }

        $this->load->model('mobile_apisession_model');

        $data = array(
            'token' => md5(uniqid(rand(0, 100000))),
            'expireDate' => $expireDate,
            'business_id' => $business_id,
            'employee_id' => $employee_id,
            'app' => $this->app,
        );

        $existingSession = $this->mobile_apisession_model->get_one(array(
            'employee_id' => $employee_id,
            'business_id' => $business_id,
            'app' => $this->app,
        ));

        if ($existingSession) {
            $data['mobile_apiSessionID'] = $existingSession->mobile_apiSessionID;
            $data['token'] = $existingSession->token;
        }

        $this->mobile_apisession_model->save($data);
        
        $this->employee_id = $employee_id;
        $this->business_id = $business_id;

        return array(
            'app' => $data['app'],
            'employee_id' => $employee_id,
            'business_id' => $business_id,
            'sessionToken' => $data['token'],
            'sessionTokenExpireDate' => $expireDate,
        );
    }
    
    protected function enhanceAjaxResponse($data = array())
    {
        $this->benchmark->mark(__CLASS__ . '_end');
        
        if ($this->params['__debug__']) {
            $data = (array) $data;

            $data['__debug'] = get_profiler_data();
            $data['__debug'] = array_merge($data['__debug'], array(
                'my_time' => $this->benchmark->elapsed_time(__CLASS__ . '_start', __CLASS__ . '_end'),
                'db_server' => $this->db->hostname,
                'db_name' => $this->db->database,
            ));

            $data['__debug']['query']['str_weight'] = sprintf("%.2f%%", $data['__debug']['query']['total_time'] / $data['__debug']['my_time'] * 100.0);
        }
        
        return $data;
    }
    
    protected function output_ajax_success_response($data = array(), $message = null)
    {
        $data = $this->enhanceAjaxResponse($data);
        $this->outputJSONsuccess($message, $data);
    }

    protected function output_ajax_error_response($message = null, $data = array())
    {
        $data = $this->enhanceAjaxResponse($data);
        $this->outputJSONerror($message, $data);      
    }

    protected function getRunningTime()
    {
        return $this->toHours(time() - $this->time);
    }
    
    protected function toHours($seconds)
    {
        $h = floor($seconds/3600);
        $seconds %= 3600;
        
        $m = floor($seconds/60);
        $seconds %= 60;
        
        return sprintf("%02d:%02d:%02d", $h, $m, $seconds);
    }
}

/**
 * Base class for account controllers
 */
class MY_Account_Controller extends MY_Controller
{
    /**
     * The current business
     *
     * This is set from subdomains.php
     * @var object
     */
    public $business;

    /**
     * The current business id
     * @var int
     */
    public $business_id;

    /**
     * The logged in customer
     * @var Customer
     */
    public $customer;

    /**
     * The logged in customer id
     * @var int
     */
    public $customer_id;

    /**
     * The path to view files
     * @var string
     */
    public $viewFileLocation;

    /**
     * The body template (with sidebar)
     * @var string
     */
    public $viewTemplate;

    /**
     * Wide body template (without sidebar)
     * @var string
     */
    public $viewWideTemplate;

    /**
     * Data passed to the template (layout)
     * @var array
     */
    public $data = array();

    /**
     * Data passed to the view
     * @var array
     */
    public $content = array();

    /**
     * Implement facebook login logic
     * @var FacebookLoginLibrary
     */
    public $facebookloginlibrary;

    /**
     * All the business's laundry plans
     * @var array
     */
    public $plans;

    /**
     * All the business's everything plans
     * @var array
     */
    public $everything_plans;

    /**
     * All the business's product plans
     * @var array
     */
    public $product_plans;

    /**
     * The locker loot name
     * @var string
     */
    public $rewards_name;

    /**
     * True if locker loot enabled for this business
     * @var bool
     */
    public $rewards;

    public function __construct()
    {
        parent::__construct();

        if (!$this->business) {
            throw new Exception("The business must be set from the subdomain library");
        }

        //This is for retrieving the GET parameters form Facebook.
        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);

        // set business_id
        $this->business_id = $this->business->businessID;

        // Load everything needed to the account display template "wrapper"
        $this->subdomain->title();
        $this->subdomain->javascript();
        $this->subdomain->style();
        $this->subdomain->header();
        $this->subdomain->footer();

        // TODO: check if we need this in every controller, I think not
        //pull the plans - check to see if we have any
        $this->load->model('businesslaundryplan_model');
        $this->plans = $this->businesslaundryplan_model->get_aprax(array(
            'business_id' => $this->business_id,
            'visible' => 1,
        ));

        $this->everything_plans = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(array(
            "business_id" => $this->business_id,
            "type"        => "everything_plan",
            "visible"     => 1
        ));

        $this->product_plans = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(array(
            "business_id" => $this->business_id,
            "type"        => "product_plan",
            "visible"     => 1
        ));

        // TODO: make this in one query, loading the LockerLootConfig object
        // load LockerLoot config
        $this->rewards = \App\Libraries\DroplockerObjects\LockerLootConfig::isActive($this->business_id);
        if (!empty($this->rewards)) {
            $this->rewards_name = \App\Libraries\DroplockerObjects\LockerLootConfig::getName($this->business_id);
        }

        $this->load->model('business_model');
        $this->load->model('customer_model');
        $this->load->helper('ll');

        // if there is a customer session, load the object
        if ($this->session->userdata('user_id')) {
            try {
                $language_by_id = array();

                $customer = new Customer($this->session->userdata("user_id"));
                $this->customer = $customer;
                $this->customer_id = $customer->customerID;
                $this->content['bouncedEmail'] = ($customer->bounced == 1 ) ? TRUE : FALSE;
                $this->data['customer'] = $this->customer;
                $this->content['customer'] = $this->customer;
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                $this->session->unset_userdata("user_id");
            }
        }

        // get the customer or business language_id
        $language_id = !empty($this->customer->default_business_language_id) ? $this->customer->default_business_language_id : $this->business->default_business_language_id;
        if ($language_id) {
            //rewrite some locales with the customer/business locale selected by the language
            $this->load->model('business_language_model');
            $language = $this->business_language_model->get_by_primary_key($language_id);

            if (!empty($language->locale)) {
                setlocale(LC_TIME, $language->locale);
            }
        }

        // stup view paths
        $this->viewFileLocation = "/business_account/";
        //bizzie sites are a bit different, so the content body is slighty diff
        if (in_bizzie_mode()) {
            $this->viewTemplate = "/business_account/bizzie_content_template";
        } else {
            $this->viewTemplate = "/business_account/content_template";
        }
        $this->viewWideTemplate = "/business_account/content_template_wide";

        if (!empty($this->business->facebook_api_key) && !empty($this->business->facebook_secret_key)) {
            // create a FacebookLogin object
            $this->load->library('facebookloginlibrary', array(
                'appId' => $this->business->facebook_api_key,
                'secret' => $this->business->facebook_secret_key,
                'business_id' => $this->business_id,
            ));

            $helper = $this->facebookloginlibrary->facebook->getRedirectLoginHelper();

            $this->content['facebook_logout_url'] = $helper->getLogoutUrl($this->facebookloginlibrary->accessToken, get_business_account_url($this->business) . '/logout?facebook_logout=yes');
            $this->content['facebook_user_id'] = $this->facebookloginlibrary->getFacebookUserId();
        }

        // pass the business to the content
        $this->content['business'] = $this->business;
        $this->data['business'] = $this->business;

        //add js files that should be applied to every business template.
        $this->template->add_js("/js/jquery-validation-1.11.1/jquery.validate.js");
        $this->template->add_js("/js/jquery-validation-1.11.1/additional-methods.js");

        // main account style
        $this->template->add_css('/css/account/style.css?v=3');
    }

    /**
     * Renders an account view (with sidebar)
     *
     * @param string $view
     * @param array $content
     */
    protected function renderAccount($view, $content)
    {
        if (strpos('/', $view) === false) {
            $view = $this->viewFileLocation . $view;
        }

        $this->renderSidebars($content);

        $this->data['content'] = $this->load->view($view, $content, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    /**
     * Renders the account sidebars
     *
     * @param array $content
     */
    protected function renderSidebars($content)
    {
        if (empty($content['skip_menu'])) {
            $this->data['sidebar'][] = $this->load->view('/business_account/account_menu', array('business' => $this->business), true);
        }

        if (!empty($content['sidebars'])) {
            foreach ($content['sidebars'] as $view => $data) {
                $this->data['sidebar'][] =  $this->load->view($view, $data, true);
            }
        }

        if ($this->business_id == 3 && empty($content['skip_kiosk_sidebars'])) {
            $this->data['sidebar'][] = $this->load->view('/business_account/callout_sms', false, true);
            $this->data['sidebar'][] = $this->load->view('/business_account/kiosk_hours', false, true);
        }

    }

    /**
     * Renders an account wide view (without sidebar)
     *
     * @param string $view
     * @param array $content
     */
    protected function renderWide($view, $content)
    {
        if (strpos('/', $view) === false) {
            $view = $this->viewFileLocation . $view;
        }

        $this->data['content'] = $this->load->view($view, $content, true);
        $this->template->write_view('content', $this->viewWideTemplate, $this->data);
        $this->template->render();
    }

    /**
     * Renders an account view using an empty layout
     *
     * @param string $view
     * @param array $content
     */
    protected function renderEmpty($view, $content)
    {
        if (strpos('/', $view) === false) {
            $view = $this->viewFileLocation . $view;
        }

        $this->template->set_template('empty');
        $this->template->write_view('content', $view, $content);
        $this->template->render();
    }

    /**
     * Checks for a customer session or redirects to /login
     */
    protected function requireLogin()
    {
        if (!$this->session->userdata('user_id')) {

            // keep the current url
            $url = $this->uri->uri_string();
            if (!empty($_GET)) {
                $url .= '?' . http_build_query($_GET);
            }
            $this->session->set_userdata('ref', $url);

            redirect('/login');
        }
    }
}

/**
 * Base class for Admin controllers
 */
class MY_Admin_Controller extends MY_Controller
{
    public $employee;
    public $employee_id;
    public $buser_id;
    public $business_id;
    public $business;
    public $pageTitle = "Business Admin";
    public $publicAccess = false;

    public function __construct()
    {
        parent::__construct();

        // Make sure app has been setup
        $serverSettings = array("mode", "email_host", "email_user", "email_password");
        foreach ($serverSettings as $name) {
            if (!\App\Libraries\DroplockerObjects\Server_Meta::get($name)) {
                redirect("/admin/setup");
            }
        }

        // load required models
        $this->load->model('business_model');
        $this->load->model('order_model');

        // check login
        if (!$this->session->userdata('buser_id')) {
            //store current URL to redirect after successfull login
            $this->session->set_flashdata('login_redirect_url', $_SERVER['REQUEST_URI']);
            redirect("/admin/login");
        }

        $this->buser_id = $this->session->userdata('buser_id');
        $this->business_id = $this->session->userdata('business_id');

        // get current business
        $this->business = $this->business_model->get_by_primary_key($this->business_id);
        if (empty($this->business)) {
            set_flash_message("error", "Business ID {$this->business_id} not found");
            redirect("/admin/login");
        }

        $this->load->model("business_employee_model");
        $business_employee = $this->business_employee_model->get_by_primary_key($this->buser_id);
        if (empty($business_employee)) {
            set_flash_message("error", "Business employee ID {$this->buser_id} not found");

            //store current URL to redirect after successfull login
            $this->session->set_flashdata('login_redirect_url', $_SERVER['REQUEST_URI']);
            redirect("/admin/login");
        }

        $this->load->model("employee_model");
        $this->employee = $this->employee_model->get_by_primary_key($business_employee->employee_id);
        $this->employee_id = $business_employee->employee_id;

        setlocale(LC_ALL, $this->business->locale);
        date_default_timezone_set($this->business->timezone);

        //The following conditional enables the built in profiler reports when the employee is set to the mode.
        if ($this->employee->profiler_mode == 1 && !$this->input->is_ajax_request()) {
            $this->output->enable_profiler(TRUE);
        }

        // check access to the controller
        if (!$this->publicAccess) {
            // redirect to denied page if the user does not have the proper permission
            // TODO: move this into controller
            check_acl();
        }

        //template and display
        $this->load->model('menus/admin_menu');
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->add_js('js/ckeditor4.4.5/ckeditor.js');
        $this->template->write('page_title', $this->pageTitle);
        $this->template->write_view('menu', 'inc/admin_menu', array('menu' => $this->admin_menu->menu));
        $this->data['submenu'] = $this->load->view('inc/admin_submenu', array("submenu" => $this->admin_menu->get_submenu()), true);
        $this->data['content'] = '';
    }

    public function index()
    {
        //find out which widgets to show
        $getWidgets = "select * from dashboard
                        left join widget on widgetID = widget_id
                        where employee_id = $this->buser_id
                        and module = 'Admin'
                        order by location";
        $result = $this->db->query($getWidgets);
        $content['widgets'] = $result->result();

        $this->load->view('admin/blank', '', true);
        $this->data['content'] = $this->load->view('admin/index', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Sets the page title
     */
    function setPageTitle($title)
    {
        $this->pageTitle = $title;
        $this->template->write('page_title', $title, true);
    }

    /**
     * Boilerplate for rendering an admin view
     * @param string $view
     * @param array $content
     */
    protected function renderAdmin($view, $content)
    {
        if (strpos($view, '/') === false) {
            $view = 'admin/' . $this->uri->segment(2) . '/' . $view;
        }

        if ($this->input->is_ajax_request()) {
            return $this->load->view($view, $content);
        }

        $this->data['content'] = $this->load->view($view, $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Render an error page
     *
     * accepts the following array keys
     * - main_title => the page main title, defaults to "Error"
     * - class => the div class name, defaults to error
     * - title => required, the error title
     * - messaage => optional error description
     *
     * @param array $content array with message to render
     * @param string $exit if true terminates script
     */
    public function adminError($content, $exit = true)
    {
        $this->renderAdmin('/admin/error', $content);
        if ($exit) {
            echo $this->output->_display();
            exit;
        }
    }

    /**
     * Redirects the user back
     *
     * @param string $url url used in case it's not a referral
     */
    protected function goBack($url = null)
    {
        if (is_null($url)) {
            $url = '/admin/' . strtolower(get_class($this));
        }

        return redirect($this->agent->is_referral() ? $this->agent->referrer() : $url);
    }

}

/**
 * Base class for cronjobs
 */
class MY_Cron_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (php_sapi_name() != 'cli') {
            die("This controller can only be accessed via shell");
        }

        $this->output->enable_profiler(FALSE);
    }

    /**
     * Gets the full command to invoke a controller
     * @param unknown $controller
     */
    protected function getControllerCall($controller)
    {
        return $_SERVER['_'] . ' ' . dirname(BASEPATH) . "/index.php $controller";
    }

    /**
     * Forks a proccess for each command and waits until all of them have finished
     *
     * If $saveOut is not empty, stdout and stderr will be saved to:
     * /tmp/$saveOut-$index.stdout and /tmp/$saveOut-$index.stderr
     *
     * @param string $commands
     * @param string $saveOut
     * @param string $pause
     */
    protected function fork($tasks, $saveOut = null, $pause = 1)
    {
        $stdout = '/dev/null';
        $stderr = '/dev/null';

        $workers = array();
        foreach ($tasks as $i => $command) {
            if ($saveOut) {
                $stdout = "/tmp/$saveOut-$i.stdout";
                $stderr  = "/tmp/$saveOut-$i.stderr";
            }

            $workers[] = shell_exec(sprintf("%s >$stdout 2>$stderr & echo $!", $command));
        }

        foreach($workers as $idx => $pid) {
            $workers[$idx] = trim($pid);
            echo "Worker $pid\n";
        }

        do {
            foreach($workers as $idx => $pid) {
                if(!posix_getpgid($pid)) {
                    unset($workers[$idx]);
                    echo "Worker: $pid DONE\n";
                }
            }
            if ($pause) {
                sleep($pause);
            }
        } while(!empty($workers));
    }

    public function daemon($method, $wait = 1)
    {
        if (!$this->isDaemonableMethod($method)) {
            $this->log("$method is not an allowed method");
            return false;
        }
        
        $args = func_get_args();
        array_shift($args);
        array_shift($args);        
        
        $label = get_class($this) . "::$method(" . implode(', ', $args) . ")";
        $this->log("Daemon started for $label");
        
        $daemonStart = time();
        $round = 0;
        
        try {
            while (true) {
                $roundStart = time();
                
                $res = call_user_func_array(array($this, $method), $args);
                if ($res) {
                    $this->log($res);
                }
                
                $round++;
                $roundTime = secondsToHours(time() - $roundStart);
                $uptime = secondsToHours(time() - $daemonStart, true);
                $memory_profile = getMemoryInfo();
                $this->log(array(
                    "message" => "Daemon for $label finished round $round. Round time: $roundTime. Uptime: $uptime. Memory usage: {$memory_profile['summary']}",
                    "memory_profile" => $memory_profile,                    
                ));
                
                sleep($wait);
            }
        } catch (Exception $e) {
            $data = array(
                'message' => "Daemon interrupted for $label",
                'error' => $e->getMessage(),
                'errorTrace' => $e->getTraceAsString(),
            );
            $this->log($data);
        }
    }
    
    /**
     * All public methods are daemonable by default
     */
    protected function isDaemonableMethod($method){
        $allowed = false;
        
        if (method_exists($this, $method))
        {
            $reflection = new ReflectionMethod($this, $method);
            if ($reflection->isPublic()) {
                $allowed = true;
            }
        };
        
        return $allowed;
    }
    
    protected function log($msg) {
        $this->db->insert('cronLog', array('job' => $this->getCallingFunction(), 'results' => serialize($msg)) );
    }
    
    protected function getCallingFunction($level = 2) {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS && DEBUG_BACKTRACE_PROVIDE_OBJECT, $level + 1);
        return get_class($trace[$level]['object']) . $trace[$level]['type'] . $trace[$level]['function'];
    }
}
