<?php

/**
 *
 * Droplocker allows businesses to use a fiew different processors.
 *
 * The business sets their processor in the admin section
 * I then use a this class as a factory to load the proper processor class file.
 *
 * The processor class file is abstract and the processors must implement 2 function
 * authorizeCard
 * makeSale
 *
 * The processor class files program to a response class with stores the transaction
 *
 * @author matt
 *
 */
class CreditCardProcessor
{

    public $processorTypes;

    public function __construct()
    {
        $this->processorTypes = array(
            'verisign' => array(
                'class' => '\DropLocker\Processor\Verisign',
                'name' => 'Verisign'
            ),
            'authorizenet' => array(
                'class' => '\DropLocker\Processor\AuthorizeNet',
                'name' => 'Authorize.net'
            ),
            'paypro' => array(
                'class' => '\DropLocker\Processor\Paypro',
                'name' => 'PayPros'
            ),
            'paypalprocessor' => array(
                'class' => '\DropLocker\Processor\PaypalPro',
                'name' => 'Paypal Payments Pro'
            ),
            'payboxdirect' => array(
                'class' => '\DropLocker\Processor\PayboxDirect',
                'name' => 'Paybox Direct Plus'
            ),
            'coinbase' => array(
                'class' => '\DropLocker\Processor\Coinbase',
                'name' => 'Coinbase'
            ),
            'cybersource' => array(
                'class' => '\DropLocker\Processor\CyberSource',
                'name' => 'CyberSource'
            ),
            'cybersource-sa' => array(
                'class' => '\DropLocker\Processor\CyberSourceSA',
                'name' => 'CyberSource Secure Acceptance'
            ),
            'conekta' => array(
                'class' => '\DropLocker\Processor\Conekta',
                'name' => 'Conekta'
            ),
            'paymentexpress' => array(
                'class' => '\DropLocker\Processor\PaymentExpress',
                'name' => 'PaymentExpress'
            ),
            'transbank' => array(
                'class' => '\DropLocker\Processor\Transbank',
                'name' => 'Transbank'

            ),
            'paygate' => array(
                'class' => '\DropLocker\Processor\Paygate',
                'name' => 'Paygate'

            ),
            'braintree' => array(
                'class' => '\DropLocker\Processor\Braintree',
                'name' => 'Braintree'
            ),
            'sonypayment' => array(
                'class' => '\DropLocker\Processor\Sonypayment',
                'name' => 'Sonypayment'
            ),
            'testprocessor' => array(
                'class' => '\DropLocker\Processor\Testprocessor',
                'name' => 'TestProcessor'
            ),
        );
    }

    /**
     * Loads the different types of processors as libraries
     *
     * @param string $type
     * @param int $business_id
     * @return DropLocker\Processor\AbstractProcessor
     */
    public function factory($type, $business_id)
    {
        $options = array();
        if (empty($type)) {
            throw new Exception("Creditcard processor is not set");
        }

        if (empty($business_id)) {
            throw new Exception("business_id is a required parameter");
        }

        if (empty($this->processorTypes[$type])) {
            throw new CreditCardProcessorException("Creditcard processor $type has not been approved by Droplocker");
        }

        return new $this->processorTypes[$type]['class'](array(
            'business_id' => $business_id
        ));
    }

    public function getProcessorsList()
    {
        $list = array();
        foreach ($this->processorTypes as $key => $value) {
            $list[$key] = $value['name'];
        }
        asort($list);

        return $list;
    }
}

class CreditCardProcessorException extends \Exception {

}

