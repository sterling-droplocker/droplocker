<?php

require_once APPPATH . 'libraries/sms_claim.php';
require_once APPPATH . 'third_party/nexmo/NexmoAccount.php';
require_once APPPATH . 'third_party/nexmo/NexmoMessage.php';
require_once APPPATH . 'third_party/nexmo/NexmoReceipt.php';

/**
 *  Manages SMS claims for Nexmo
 */
class Nexmo_claim extends Sms_claim
{
    private $nexmo_endpoint;
    private $nexmo_api_key;
    private $nexmo_api_secret;
    protected $disable_unicode = false;

    public function __construct($params)
    {
        parent::__construct($params);

        //load up API creds from the smsSettings table here
        $nexmo_settings = $this->getProviderSettings($this->business_id);

        //field names are different, but essentially the same, we only have two, apikey and apisecret
        //account_sid maps to api_key
        //auth_token maps to api_secret
        if (!empty($nexmo_settings->account_sid) && !empty($nexmo_settings->auth_token)) {
            $this->nexmo_api_key       = $nexmo_settings->account_sid;
            $this->nexmo_api_secret    = $nexmo_settings->auth_token;
            $this->business_sms_number = $nexmo_settings->number;
            $this->disable_unicode     = $nexmo_settings->disable_unicode;
        } else {
            throw new Exception("Missing API Credentials for NEXMO, business_id {$this->business_id}");
        }
    }

    protected function sendMessage($sms_number, $body)
    {
        //load API class
        $nexmo_sms = new NexmoMessage($this->nexmo_api_key, $this->nexmo_api_secret);

        $unicode = $this->disable_unicode ? false : null;

        // send the message
        $info = $nexmo_sms->sendText($sms_number, $this->business_sms_number, $body, $unicode);

        // Display an overview of the message - this won't spit out anything normally from the callback URL
        // but if running through the test tool, this will show in the output console
        echo $body . "\n\n";
        echo $nexmo_sms->displayOverview($info);
    }
}
