<?php

class Logger extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * writes to a log file.
     * Each datatable will set with values cannot be null
     *
     *
     * @param string $table
     * @param array $options
     *
     * $options Values
     * --------------------
     * status
     * employee
     * customer
     * Note
     */
    public function set($table, $options)
    {
        return $this->db->insert($table, $options);
    }

    /**
     * NOT USED!
     * use a normal data model file to get the data from a log file
     */
    public function get($table, $options)
    {
        $this->db->get($table, $options);
    }

}
