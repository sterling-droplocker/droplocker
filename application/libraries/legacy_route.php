<?php
use App\Libraries\DroplockerObjects\Order;

class Legacy_Route extends CI_Model
{
    public function remove_dups($array, $row_element)
    {
       $new_array[0] = $array[0];
       foreach ($array as $current) {
           $add_flag = 1;
           foreach ($new_array as $tmp) {
               if ($current[$row_element]==$tmp[$row_element]) {
                   $add_flag = 0; break;
               }
           }
           if ($add_flag) $new_array[] = $current;
       }

       return $new_array;
    } // end function remove_dups


    public function getRoute($route, $business_id)
    {
        //get all locations with lockers
        $selLocations = "SELECT * FROM location
        WHERE serviceType = 'daily'
        and route_id = $route
        and location.business_id = $business_id";
        $query = $this->db->query($selLocations);
        $locations = $query->result();

        //claimed orders
        $selClaims = "SELECT * FROM location
        inner join locker on locker.location_id = locationID
        inner join claim on claim.locker_id = lockerID
        where route_id = $route
        and location.business_id = $business_id
        and claim.created > NOW() - INTERVAL 6 MONTH";
        $query = $this->db->query($selClaims);
        $claims = $query->result();

        //deliveries
        $selDeliveries = "SELECT * FROM location
        inner join locker on locker.location_id = locationID
        inner join `orders` on `orders`.locker_id = lockerID
        where `orders`.orderStatusOption_id <> '10'
        and route_id = $route
        and location.business_id = $business_id
        and orders.dateCreated > NOW() - INTERVAL 6 MONTH";
        $query = $this->db->query($selDeliveries);
        $deliveries = $query->result();

        $arrayCounter = 0;

         //locations with lockers
        foreach ($locations as $l) {
            if ($l->route_id == $route or $route == '%') {
                $arrayCounter ++;
                $number = explode(" ", $l->address);
                $arrayData[$arrayCounter][0] = $number[0];
                $arrayData[$arrayCounter][1] = $l->address;
                $arrayData[$arrayCounter][2] = $l->locationID;
                $arrayData[$arrayCounter][3] = $l->accessCode;
                $arrayData[$arrayCounter][4] = $l->sortOrder;
                $arrayData[$arrayCounter][5] = $l->route_id;
                $arrayData[$arrayCounter][6] = $l->lat;
                $arrayData[$arrayCounter][7] = $l->lon;
                //huh?
                $arrayData[$arrayCounter][8] = $WFOrders[$line['id']];
                $arrayData[$arrayCounter][9] = $DCOrders[$line['id']];
            }
        }
        //pickups
        foreach ($claims as $c) {
            if ($c->route_id == $route or $route == '%') {
                $arrayCounter ++;
                $number = explode(" ", $c->address);
                $arrayData[$arrayCounter][0] = $number[0];
                $arrayData[$arrayCounter][1] = $c->address;
                $arrayData[$arrayCounter][2] = $c->locationID;
                $arrayData[$arrayCounter][3] = $c->accessCode;
                $arrayData[$arrayCounter][4] = $c->sortOrder;
                $arrayData[$arrayCounter][5] = $c->route_id;
                $arrayData[$arrayCounter][6] = $c->lat;
                $arrayData[$arrayCounter][7] = $c->lon;
                $arrayData[$arrayCounter][8] = 'pickup';
                $arrayData[$arrayCounter]['activeClaim'] = $c->active;
            }
        }
        //deliveries
        foreach ($deliveries as $d) {
            if ($d->route_id == $route or $route == '%') {
                $arrayCounter ++;
                $number = explode(" ", $d->address);
                $arrayData[$arrayCounter][0] = $number[0];
                $arrayData[$arrayCounter][1] = $d->address;
                $arrayData[$arrayCounter][2] = $d->locationID;
                $arrayData[$arrayCounter][3] = $d->accessCode;
                $arrayData[$arrayCounter][4] = $d->sortOrder;
                $arrayData[$arrayCounter][5] = $d->route_id;
                $arrayData[$arrayCounter][6] = $d->lat;
                $arrayData[$arrayCounter][7] = $d->lon;
                //huh?
                $arrayData[$arrayCounter][8] = $WFOrders[$line['dropLocation_id']];
                $arrayData[$arrayCounter][9] = $DCOrders[$line['dropLocation_id']];
            }
        }


        $arrayData = $this->remove_dups($arrayData,1);
        //array_multisort($arrayData);
        foreach ($arrayData as $key => $row) {
           $aa[$key]  = $row['0'];
           $bb[$key] = $row['1'];
           $cc[$key] = $row['2'];
           $dd[$key] = $row['3'];
           $ee[$key] = $row['4'];
           $ff[$key] = $row['5'];
           $gg[$key] = $row['6'];
           $hh[$key] = $row['7'];
        }
        array_multisort($ee, SORT_ASC, $arrayData);

        return $arrayData;
    }

    //this gets a dump of all the data needed for the drvier map
    //$route can be passed as a string of multiple routes, e.g. (1,3,7,4)
    public function route_stops_2($route, $business_id, $day, $type = 'both', $loaded_only = false)
    {
        if (empty($day)) {
            $day = date("N");
        }
        //get all locations on selected routes
        $getAllLocations = "Select * from location
                            join location_serviceDay on locationID = location_id
                            where route_id in ($route)
                            and business_id = $business_id
                            and day = $day
                            order by sortOrder,route_id";
        $query = $this->db->query($getAllLocations);
        $allLocations = $query->result();

        //get all claims on selected routes
        $getClaims = "SELECT * FROM location
                        inner join locker on locker.location_id = locationID
                        inner join claim on claim.locker_id = lockerID
                        and claim.active = 1
                        and location.business_id = $this->business_id
                        and route_id in ($route)";
        $query = $this->db->query($getClaims);
        $claims = $query->result();
        //put claims in an array by location
        foreach ($claims as $claim) {
            if (!isset($claimsArray[$claim->locationID])) {
                $claimsArray[$claim->locationID] = 0;
            }

            $claimsArray[$claim->locationID]++;
            $claimArray['claims'][$claim->locationID][$claim->claimID] = $claim;
        }

        $blankCustomerAccount = $this->business->blank_customer_id;

        $loaded_sql = $loaded_only ? 'AND loaded = 1' : '';

        //get all deliveries for selected routes
        $getDeliveries = "SELECT orderID as ordId, location.*, locker.*, customer.*, bag.*, orders.*, orderHomeDeliveryID
                            FROM location
                            JOIN locker ON (locker.location_id = locationID)
                            JOIN orders ON (orders.locker_id = lockerID)
                            LEFT JOIN customer ON (orders.customer_id = customerID)
                            LEFT JOIN bag ON (bagID = orders.bag_id)
                            LEFT JOIN orderHomeDelivery ON (orderHomeDelivery.order_id = orderID)
                            WHERE orders.orderStatusOption_id NOT IN (10, 13, 1, 2)
                            AND orders.customer_id != $blankCustomerAccount
                            AND location.business_id = $this->business_id
                            AND route_id IN ($route)
                            $loaded_sql
                            ORDER BY sortOrder, route_id";
        $query = $this->db->query($getDeliveries);
        $deliveries = $query->result();

        //put deliveries in an array
        foreach ($deliveries as $delivery) {
            $deliveryArray[$delivery->locationID][$delivery->orderID] = $delivery;

            if (!isset($deliveryArray['deliveryCount'][$delivery->locationID])) {
                $deliveryArray['deliveryCount'][$delivery->locationID] = 0;
            }
            $deliveryArray['deliveryCount'][$delivery->locationID]++;
        }

        foreach ($allLocations as $location) {
            //if this is a pickup location for this day
            if ($location->location_serviceType_id == 2 and $type != 'Deliveries') {
                //if it's daily then add it to the route
                if ($location->serviceType == 'daily') {
                    $stops['stops'][$location->locationID] = $location;
                }

                //if there is a claim at this location makes sure to add it to the route and count how many claims
                if (!empty($claimsArray[$location->locationID])) {
                    //make sure we go there
                    $stops['stops'][$location->locationID] = $location;
                    //find out how many claims we have
                    $stops['claims'][$location->locationID]['count'] = $claimsArray[$location->locationID];
                    //get claimes at this location
                    foreach ($claimArray['claims'][$location->locationID] as $claim) {
                        $stops['claims'][$location->locationID]['claims'] = $claim;
                    }
                }
            }

            //if this is a delivery location for this day
            if ($location->location_serviceType_id == 1 and $type != 'Pickups') {
                if (!empty($deliveryArray['deliveryCount'][$location->locationID])) {

                    //make sure we stop there
                    $stops['stops'][$location->locationID] = $location;

                    $this->load->model('order_model');

                    //get the orders
                    foreach ($deliveryArray[$location->locationID] as $delivery) {
                        $orderType = $this->order_model->getOrderType($delivery->orderID);

                        if (!isset($stops['deliveries'][$location->locationID]['orderTypes'][$orderType])) {
                            $stops['deliveries'][$location->locationID]['orderTypes'][$orderType] = 0;
                        }
                        $stops['deliveries'][$location->locationID]['orderTypes'][$orderType]++ ;
                        $orderData = array($delivery);

                        $stops['deliveries'][$location->locationID]['orders'][$delivery->orderID] = $orderData;
                        $stops['deliveries'][$location->locationID]['orders'][$delivery->orderID]['orderType'] = $orderType;
                    }
                }
            }
        }

        return $stops;
    }

    public function todays_stops($day, $business_id, $driver = '%', $route_id = '%')
    {
        $startTime = date('Y-m-d 00:00:01', strtotime($day)) ;
        $startTime = date('Y-m-d H:i:s', convert_to_gmt(strtotime($startTime), $this->business->timezone));
        $endTime = date('Y-m-d 00:00:01', strtotime($day . "+1 day"));
        $endTime =  date('Y-m-d H:i:s', convert_to_gmt(strtotime($endTime), $this->business->timezone));

        $getStops = "SELECT firstName, lastName, location.route_id, driverLogID, delType, sortOrder, employee_id, locationID, location.address, delTime
                    FROM driverLog
                    join employee on employeeID = driverLog.employee_id
                    join locker on lockerID = driverLog.locker_id
                    join location on locationID = locker.location_id
                    where delTime  > '$startTime'
                    and delTime < '$endTime'
                    and employee_id like '$driver'
                    and route_id like '$route_id'
                    and location.business_id = $business_id
                    order by employee_id, route_id, delTime";
        $query = $this->db->query($getStops);
        $rows = $query->result();

        return $rows;
    }

    public function driver_status($day, $business_id)
    {
        $stops = $this->todays_stops($day, $business_id);
        foreach ($stops as $stop) {
            $driver[$stop->route_id]['name'] = $stop->firstName.' '.$stop->lastName;
            $driver[$stop->route_id]['employee_id'] = $stop->employee_id;
            if ($lastStop != $stop->locationID) {
                $driver[$stop->route_id]['stopsMade'] ++;
                $driver[$stop->route_id]['lastStop'] = $stop->address ;
                $driver[$stop->route_id]['delTime'] = $stop->delTime ;
            }
            $lastStop = $stop->locationID;
        }

        return $driver;
    }

    //gets the number or orders in picked status
    public function pickups($route, $business_id, $date = '')
    {
        if($date == '') $date = date("Y-m-d");
        $getPicked = "SELECT firstName, lastName, orders.notes, orders.orderStatusOption_id
                        FROM orders
                        join locker on lockerID = orders.locker_id
                        join location on locationID = locker.location_id
                        join customer on customerID = orders.customer_id
                        where orderStatusOption_id = 1
                        and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') = '$date'
                        and location.business_id = $business_id
                        and route_id = $route;";
        $query = $this->db->query($getPicked);
        $rows = $query->result();

        return $rows;

    }

    public function getPhones($business_id)
    {
        $getPhones = "SELECT employeeUsername, employeeID, phoneNumber, firstName, lastName, business_id, dateCreated
            FROM droid_postDeliveries
            left join employee on employeeUsername = employee.username
            where business_id = $business_id
            order by ID desc
            limit 1000";

        $q = $this->db->query($getPhones);
        $phones = $q->result();
        foreach ($phones as $phone) {
            if (!$all_phones[$phone->employeeID]) {
                $all_phones[$phone->employeeID] = $phone;
            }
        }

        return $all_phones;
    }
}
