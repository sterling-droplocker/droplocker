<?php
use App\Libraries\DroplockerObjects\Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\ProductCategory;
use App\Libraries\DroplockerObjects\Customer;
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wash_Fold_Product
{
    const PRODUCT_CLASS = 'wf';
    const SERVICE_TYPE_ID = 1;
    const NAME = "Wash and Fold";

    public function __construct()
    {
        //parent::__construct();
        $this->class_slug = self::PRODUCT_CLASS;
        $this->service_type_id = self::SERVICE_TYPE_ID;
    }

    public function get_name()
    {
       return self::NAME;
    }


    public function get_service_type_id()
    {
        return self::SERVICE_TYPE_ID;
    }

    /**
     * get_merged_preferences takes the customer preferences and the business preferences and merges them with the
     * customer preferences taking priority
     *
     * @param int customer_id
     * @param int business_id
     *
     * @return array
     *
     */
    public function get_merged_preferences($customer_id, $business_id)
    {
        $CI = get_instance();

        // WF module
        $module_id = 1;

        //Get the customer preferences
        $CI->load->model('customerpreference_model');
        $customerPreferences = $CI->customerpreference_model->get_preferences($customer_id, $module_id);

        //Get the business defaults
        $CI->load->model('product_model');
        $businessPreferences  = $CI->product_model->getBusinessDefaultPreferences($business_id, $module_id);

        //Merge the customer and business preferences together, with the customer preferences taking priority
        $preferences = array_merge($businessPreferences, $customerPreferences);

        return $preferences;
    }


    /**
     * after adding a wash and fold item to the database we create order items for each
     * preference associated with the customers preferences
     *
     * cycle through the prefs and if the pref has a cost, it needs to become an orderItem from that products ID
     * if it's just a preference with no cost, it gets added to the orderItem notes
     *
     * If there is no product supplier for a product, this function will create one based on the supplier passed to the original WF orderItem
     *
     * @param int $orderItem_id
     *
     * $main_supplier_id is the supplier that provides the wash fold product
     * if the orderItems that are created have a different supplierBusinessID
     * then we leave them off and email the business owner that there is a miss-match
     * with the products and suppliers for wash fold preferences
     *
     * @return type
     */

    public function add_wash_fold_preferences($orderItem_id)
    {
        $CI = get_instance();
        $CI->load->model('orderitem_model');

        $notes = '';

        //get some info about this order
        $sqlGetVariables = "select orderItemID, customer_id, supplier_id, business_id from orderItem
                                                join orders on order_id = orderID
                                                where orderItemID = ".$orderItem_id;

        $query = $CI->db->query($sqlGetVariables);
        $vars = $query->result();
        $main_supplier_id = $vars[0]->supplier_id;

        // orderItem should always have a supplier ID. When adding preferences that have a value > 0, we will use the supplier_id for the
        // original orderItem to get the supplier for the new orderItems that will get added later in this function
        $supplier_id = ($main_supplier_id == '')?$CI->business_id:$main_supplier_id;

        //get the orderItem
        $CI->orderitem_model->clear();
        $CI->orderitem_model->orderItemID = $orderItem_id;
        $washFoldItem = $CI->orderitem_model->get();
        if (!$washFoldItem) {
                throw new Exception('No orderItem Found');
        }
        $CI->load->model('product_model');

        if (!Product::isWashFold($washFoldItem[0]->product_process_id)) {
            throw new \User_Exception("Product must be be in 'Wash and Fold' Category");
        }

        //Merge the customer and business preferences together, with the customer preferences taking priority
        $preferences = $this->get_merged_preferences($vars[0]->customer_id, $CI->session->userdata('business_id'));

        $products = array();

        // get the products
        // Loop through each preference product id and see if it has a supplier
        // if no supplier, make the selected supplier the default supplier and insert into the product_supplier table
        foreach ($preferences as $key => $value) {
            $product = '';
            $sql = "SELECT product_processID, p.productID, price, cost, supplier_id, p.name, pc.slug as category_name, p.business_id, p.displayName, p.unitOfMeasurement as unit, ps.product_supplierID, pp.active as active
                            FROM product_process pp
                        INNER JOIN product_supplier ps ON ps.product_process_id = product_processID
                            INNER JOIN product p ON p.productID = pp.product_id
                            INNER JOIN productCategory pc ON pc.productCategoryID = p.productCategory_id
                            WHERE pp.product_id = {$value} AND supplier_id = {$supplier_id}";
            $query = $CI->db->query($sql);

            // If the product has no supplier, create one now
            if (!$product = $query->row()) {
                $product_process = Product_Process::search(array('product_id'=>$value));
                $CI->load->model('product_supplier_model');
                //THe following conditional is unknown *.
                if (!$product_process) {
                    //The following try catch handler is a workaround for unknownn *.
                    try {
                        $product = new Product($value);
                    } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                        $product = null;
                    }
                    $process = new Process(0);

                    throw new \Exception("Could not find product '{$product->name}' for process '{$process->name}'.");

                }
                try {
                    $CI->product_supplier_model->product_process_id = $product_process->product_processID;
                    $CI->product_supplier_model->supplier_id = $supplier_id;
                    $CI->product_supplier_model->default = 1;
                    $CI->product_supplier_model->insert();
                } catch (Database_Exception $e) {
                    send_exception_report($e);
                    continue;
                }

                // redo the query to get the product
                $query = $CI->db->query($sql);
                $product=$query->row();

            }

            $products[] = $product;
        }

        $added_products = array();
        $failed_products = array();

        foreach ($products as $product) {
            $item_notes = '';
            $unit_price = $product->price;

            if ($unit_price > 0) {
                if (empty($product->active)) {
                    $notes .= $product->category_name . ": <b> {$product->name} </b> Is inactive, not added\n";
                    continue;
                }

                if (in_array($product->unit, array('Lbs', 'Kgs'))) {
                    $qty = $washFoldItem[0]->qty;
                } else {
                    $qty = 1;
                }

                if (date("w") == 4 and strtolower($product->category_name) == "detergent" and $product->business_id == 3) {
                        $item_notes = 'Thursday detergent upgrade promotion';
                        $unit_price = 0.00;
                }

                // create a new orderItem for each product that has a value > 0
                $res = $CI->orderitem_model->add_item(
                            $washFoldItem[0]->order_id,  // $order_id
                            0,                           // $item_id
                            $product->product_processID, // $product_process_id
                            $qty,                        // $qty
                            get_current_employee_id(),   // $employee_id
                            $supplier_id,                // $supplier_id
                            $unit_price,                 // $price
                            $item_notes                  // $notes
                        );

                if ($res['status']=='success') {
                    $added_products[] = $res['orderItemID'];
                } else {
                    $failed_products[] = $res['orderItemID'];
                }

            }
            //append to notes
            //Note, category name is actually the category slug.
            $productCategory = \App\Libraries\DroplockerObjects\ProductCategory::search(array("slug" => $product->category_name, "business_id" => $product->business_id));
            if (empty($productCategory)) {
                $notes .= "UNKNOWN PRODUCT CATEGORY\n";
            } else {
                $default_product = $productCategory->get_default_product_preference();
                if ($product->productID == $default_product->productID) {
                    $notes .= $product->category_name. ": ".$product->name."\n";
                } else {
                    $notes .= $product->category_name . ": <b> {$product->name} </b>\n";
                }
            }

        }

        //add notes to orderItem
        if ($notes != '') {
            $new_notes = $washFoldItem[0]->notes ? $washFoldItem[0]->notes . "\n" . $notes : $notes;
            $sql = "UPDATE orderItem SET notes = ? WHERE orderItemId = ?";
            $CI->db->query($sql, array($new_notes, $washFoldItem[0]->orderItemID));
        }

        //return array with newOrderItems and notes
        return array('status'=>'success', 'notes'=>$notes, "added_products"=>$added_products, "failed_products"=> $failed_products);
    }
    
    public function storeWFPReferencesAndLog($product_process, $orderItemID, $order_id, $qty)
    {
        $product = new Product($product_process->product_id);
        $productCategory = new ProductCategory($product->productCategory_id);
        if ($productCategory->slug == WASH_AND_FOLD_CATEGORY_SLUG) {
            $result = $this->add_wash_fold_preferences($orderItemID);

            //add to the WF Log is WF is added manually
            $addToLog = "insert IGNORE into wfLog
                        (order_id, weightIn, weightInCust, weightInTime, weightInIP)
                        values (?, ?, 'WF Added from Order Entry', now(), ?)";
            $this->db->query($addToLog, array($order_id, $qty, $_SERVER['REMOTE_ADDR']));
        }
    }
    
}
