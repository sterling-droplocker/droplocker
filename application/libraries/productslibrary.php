<?php

use App\Libraries\DroplockerObjects\Customer;
class ProductsLibrary
{
    /**
     * Generates the array of options for customer preferences views
     *
     * @param int $business_id
     * @param int $customer_id
     * @return array
     */
    public function getBusinessPreferences($business_id, $customer_id = null)
    {
        $products = $this->getProducts($business_id, true);

        $business_language_id = null;
        if ($customer_id) {
            $customer = new Customer($customer_id);
            $business_language_id = $customer->default_business_language_id;
        }

        $translations = $this->getTranslations($business_language_id);
        $categoriesTranslations = $this->getCategoriesTranslations($business_language_id);

        $class = array();
        $productSortOrders = array();
        foreach ($products as $i => $product) {
            $class[$product->className][$product->categorySlug]['text'] = !empty($categoriesTranslations[$product->productCategory_id]) ? $categoriesTranslations[$product->productCategory_id] : $product->categoryName;
            $class[$product->className][$product->categorySlug]['slug'] = $product->categorySlug;
            $class[$product->className][$product->categorySlug]['class_id'] = $product->module_id;
            $class[$product->className][$product->categorySlug]['parent_id'] = $product->parent_id;

            $data = array(
                'name' => $product->name,
                'displayName' => !empty($translations[$product->productID]) ? $translations[$product->productID] : $product->displayName,
                'productID' => $product->productID,
                'price' => number_format($product->price,2),
                'unit' => $product->unitOfMeasurement,
                'canDelete' => $product->canDelete
            );

            $sortOrder = $product->sortOrder;

            //DROP-4476: fix issues with repeated sortOrder in same cat.
            if (!isset($productSortOrders[$product->productCategory_id])) {
                $productSortOrders[$product->productCategory_id] = array();
            }

            if (in_array($sortOrder, $productSortOrders[$product->productCategory_id])) {
                $sortOrder++;
            }
            $productSortOrders[$product->productCategory_id][] = $sortOrder;


            $class[$product->className][$product->categorySlug]['attr'][$sortOrder] = $data;
        }

        foreach($class as $title => $modulePreferences) {
            foreach($modulePreferences as $preferenceSlug => $preference) {
                foreach ($preference['attr'] as $index => $value) {
                    $class[$title][$preferenceSlug]['options'][$value['productID']] = $this->formatLabel($value, $index, $business_language_id);
                }
            }
        }

        return $class;
    }

    /**
     * Format the business preferences for use in the phone
     *
     * @param int $business_id
     * @param string $tag
     * @return array
     */
    public function getBusinessPreferencesForAPI($business_id, $tag = null)
    {

        $business_language_id = $this->getBusinessLanguageID($business_id, $tag);

        $products = $this->getProducts($business_id, true);
        $translations = $this->getTranslations($business_language_id);
        $categoriesTranslations = $this->getCategoriesTranslations($business_language_id);

        $class = array();
        foreach ($products as $i => $product) {
            $slug = strtolower($product->categorySlug);

            $value = array(
                'displayName' => !empty($translations[$product->productID]) ? $translations[$product->productID] : $product->displayName,
                'name' => $product->name,
                'productID' => $product->productID,
                'productCategory_name' => !empty($categoriesTranslations[$product->productCategory_id]) ? $categoriesTranslations[$product->productCategory_id] : $product->categoryName,
                'productCategory_slug' => $slug,
                'productCategory_id' => $product->productCategory_id,
                'price' => number_format($product->price,2),
                'unit' => $product->unitOfMeasurement,
                'default' => $product->sortOrder == 1,
            );

            $value['displayName'] = $this->formatLabel($value, $i, $business_language_id);

            $class[$product->className][$slug][$product->sortOrder] = $value;
        }

        return $class;
    }

    /**
     * Gets the business language ID by the language name
     * @param int $business_id
     * @param string $tag
     * @return string
     */
    protected function getBusinessLanguageID($business_id, $tag)
    {
        if (empty($tag)) {
            return null;
        }

        $CI = get_instance();
        $CI->load->model('business_language_model');

        $business_language = $CI->business_language_model->findByTag($tag, $business_id);
        if ($business_language) {
            return $business_language->business_languageID;
        }

        return null;
    }

    /**
     * Formats the label with the price for a product
     *
     * @param array $value
     * @param int $index
     * @param int $business_language_id
     * @return string
     */
    protected function formatLabel($value, $index, $business_language_id = null)
    {
        $price = "";
        if ($value['price'] > 0) {
            $price = $value['unit'] == "N/A" ?
            money_format("%.2n", $value['price']) :
            sprintf("( + %s / %s )", money_format('%.2n', $value['price']), $value['unit']);
        }

        $label = trim(sprintf("%s %s", $value['displayName'], $price));
        if ($index == 1) {
            $default_label = get_translation("defaults", "Customer Preferences", array(), "(Default)", null, $business_language_id);
            $label .= " " . $default_label;
        }

        return $label;
    }

    /**
     * Gets the translated products for a business_language_id
     *
     * @param int $business_language_id
     * @return array
     */
    protected function getTranslations($business_language_id)
    {
        if (empty($business_language_id)) {
            return array();
        }

        $CI = get_instance();
        $translations = $CI->db->query("
            SELECT product_id, title
            FROM productName
            WHERE business_language_id = ?
        ", array($business_language_id))->result();

        $out = array();
        foreach ($translations as $item) {
            $out[$item->product_id] = $item->title;
        }

        return $out;
    }

    /**
     * Gets the translated product categories for a business_language_id
     *
     * @param int $business_language_id
     * @return array
     */
    protected function getCategoriesTranslations($business_language_id)
    {
        if (empty($business_language_id)) {
            return array();
        }

        $CI = get_instance();
        $translations = $CI->db->query("
            SELECT productCategory_id, title
            FROM productCategoryName
            WHERE business_language_id = ?
        ", array($business_language_id))->result();

        $out = array();
        foreach ($translations as $item) {
            $out[$item->productCategory_id] = $item->title;
        }

        return $out;
    }

    /**
     * Get the customer preferences formatted for the API responses
     *
     * @param int $customer_id
     * @throws Exception
     * @return Ambigous array
     */
    public function getCustomerPreferencesForAPI($customer_id)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be an integer.");
        }

        $CI = get_instance();

        $CI->load->model('customerpreference_model');
        $unserialized_wash_preferences = $CI->customerpreference_model->get_preferences($customer_id);

        $CI->load->model("product_model");

        $customer = new Customer($customer_id);
        $translations = $this->getTranslations($customer->default_business_language_id);

        $result = array();
        $result['preferences'] = array();
        if ($unserialized_wash_preferences) {
            foreach ($unserialized_wash_preferences as $wash_preference_name => $product_id) {
                $wash_preference_name = strtolower($wash_preference_name);
                $products = $CI->product_model->get_aprax(array("productID" => $product_id));
                if (empty($products)) {
                    $result['preferences'][$wash_preference_name]['productID'] = null;
                    $result['preferences'][$wash_preference_name]['displayName'] = null;
                } else {
                    $product = $products[0];
                    $result['preferences'][$wash_preference_name]['productID'] = $product->productID;
                    $result['preferences'][$wash_preference_name]['displayName'] = !empty($translations[$product->productID]) ? $translations[$product->productID] : $product->displayName;
                }
            }
        }

        return $result;
    }

    /**
     * Retrieves all the products for each service type
     *
     * @param int $business_id
     * @param bool $active_only
     * @return array
     */
    public function getProducts($business_id, $active_only = true)
    {
        $CI = get_instance();

        $active = $active_only ? 1 : 0;
        $products = $CI->db->query("
            SELECT `productCategory`.`module_id`, `productCategory_id`, `product`.`business_id`,
                    `module`.`slug` as classSlug, `module`.`name` as className,
                    `productCategory`.`slug` as categorySlug, `productCategory`.`name` as categoryName,
                    `productID`, `price`, `product`.`name`, `product`.`displayName`, `unitOfMeasurement`,
                    `canDelete`, `productType`, `sortOrder`, productCategory.parent_id
            FROM (`product`)
            LEFT JOIN `product_process` ON `product_process`.`product_id` = `productID`
            JOIN `productCategory` ON `productCategoryID` = `product`.`productCategory_id`
            JOIN `module` ON `module`.`moduleID` = `productCategory`.`module_id`
            LEFT JOIN `process` ON `process_id` = `processID`
            LEFT JOIN `upchargeGroup` ON `upchargeGroup_id` = `upchargeGroupID`
            WHERE `product`.`business_id` =  ?
            AND `product_process`.`active` =  ?
            AND `productType` =  'preference'
            ORDER BY productCategory.module_id, productCategory.sort_order, product.sortOrder
            ", array($business_id, $active))->result();

        return $products;
    }

}