<?php

class Timecards extends CI_Model
{
    //returns the number of hours worked by an employee for the specified date
    //note, OT is based on 8 hours in a day, not 40 hours in a week.  So if they have worked over 40 hours this week but less than 8 in the specificed day, it will show no OT
    public function todaysHours($emp_id, $dateSelected)
    {
        $todaysDetails = array(
            'startTime' => '',
            'totHrs' => '',
            'todayHrs' => '',
            'regHours' => '',
            'overTime' => ''
        );

        $cardDate = date("Y-m-d", strtotime($dateSelected));
        $getPunches = "SELECT time_to_sec(timeInOut) / 60 as minutes, timeCardDetail.*
           from timeCardDetail
           JOIN timeCard on timeCardID= timeCardDetail.timeCard_id
           join employee on employeeID = timeCard.employee_id
           join business_employee on business_employee.employee_id = employeeID
           WHERE timeCard.employee_id = '$emp_id'
           AND timeCardDetail.day = '$cardDate'
           AND business_id = $this->business_id";

        $query = $this->db->query($getPunches);
        $punches = $query->result();

        $counter = 0;
        foreach ($punches as $p) {
            $counter ++;
            if ($counter == 1) { $todaysDetails['startTime'] = $p->timeInOut; }

            if ($p->in_out == 'In') {
                $todaysDetails['totHrs'] -= $p->minutes;
            } else {
                $todaysDetails['totHrs'] += $p->minutes;
            }
        }

        //if it is greater than 0 they are clocked in
        if ($todaysDetails['totHrs'] < 0) {
            //if it's early in the morning, before 6
            if (strtotime("now") < strtotime("06:00:00")) {
                $todaysDetails['todayHrs'] = (((strtotime("now") - strtotime("yesterday")) / 60) + $todaysDetails['totHrs']) / 60;
            } else {
                $todaysDetails['todayHrs'] = (((strtotime("now") - strtotime("00:00:00")) / 60) + $todaysDetails['totHrs']) / 60;
            }
        } else {
            $todaysDetails['todayHrs'] = $todaysDetails['totHrs'] / 60;
        }

        $otHours = 0;
        $regHours = 0;
        if ($todaysDetails['todayHrs']> 8) {
            $otHours = $todaysDetails['todayHrs'] - 8;
            $regHours = 8;
        } else {
            $regHours = $todaysDetails['todayHrs'];
        }
        $todaysDetails['regHours'] = $regHours;
        $todaysDetails['overTime'] = $otHours;

        return $todaysDetails;
    }

    //returns all the hours worked for all employees on a specified date
    public function dailyHours($dateSelected)
    {
        $cardDate = date("Y-m-d", strtotime($dateSelected));
        $sqlAllClocks = "SELECT position, firstName, day, timeInOut, timeCardDetail.in_out, employeeID, time_to_sec(timeInOut) / 60 as minutes, timeCardDetail.type, timeCardID, weekOf, aclRole_id, aclroles.name as aclName
                    FROM timeCardDetail
                    join timeCard on timeCardDetail.timeCard_id = timeCardID
                    join employee on employeeID = timeCard.employee_id
                    join business_employee on business_employee.employee_id = employeeID
                    join aclroles on aclID = aclRole_id
                    where timeCardDetail.day = '$cardDate'
                    and business_employee.business_id = $this->business_id
                    order by position, day desc, employeeID, timeInOut";

        $query = $this->db->query($sqlAllClocks);
     //echo $sqlAllClocks;
        $allClocks = $query->result();

        foreach ($allClocks as $a) {
            $empHours[$a->employeeID]['timeCardID'] = $a->timeCardID;
            $empHours[$a->employeeID]['weekOf'] = $a->weekOf;
            $empHours[$a->employeeID]['name'] = $a->firstName;
            $empHours[$a->employeeID]['position'] = strtolower($a->aclName);
            if ($a->type == "Regular") {
                if ($a->in_out == 'In') { $empHours[$a->employeeID]['reg'] -= $a->minutes; } elseif ($a->in_out == 'Out') { $empHours[$a->employeeID]['reg'] += $a->minutes; }
            }
            if ($a->type == "Sick") {
                if ($a->in_out == 'In') { $empHours[$a->employeeID]['sick'] -= $a->minutes; } elseif ($a->in_out == 'Out') { $empHours[$a->employeeID]['sick'] += $a->minutes; }
            }
        }

        return $empHours;
    }

    public function dailyHoursByRole($dateFind)
    {
        $empHours = $this->dailyHours($dateFind);

        if ($empHours) {
            foreach ($empHours as $e) {
                $empReg = 0;
                $empSick = 0;
                $empOt = 0;

                if ($e['reg'] > 480) {
                    $empOt = $e['reg'] - 480;
                    $empReg = 480;
                } else { $empReg = $e['reg']; }

                $dayTime[$e['position']]['daySick'] += number_format($e['sick'] / 60, 1);
                $dayTime[$e['position']]['dayReg'] += number_format($empReg / 60, 1);
                $dayTime[$e['position']]['dayOt'] += number_format($empOt / 60, 1);
            }
        }
//var_dump($dayTime);
        return $dayTime;
    }


}
