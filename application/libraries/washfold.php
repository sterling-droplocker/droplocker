<?php
use App\Libraries\DroplockerObjects\Order;
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************
RESERVED CODES
- CR: used for customer referrals with Locker Loot program
- SR: used for customer referrals with Locker Loot program
- DR: used for customer referrals with Locker Loot program
- GF: gift cards
*/

class WashFold extends CI_Model
{
    public $CI = '';
    public $couponCode = '';
    public $customer_id = '';
    public $business_id;

    public function __construct()
    {
            $this->CI = get_instance();
    }

    /**
     *
     * @param int $bagNumber
     * @param type $lbs
     * @param int $order_id The order ID
     * @param int $business_id
     */
    public function addWashFold($bagNumber, $lbs, $order_id, $business_id)
    {
        if (!empty($bagNumber)) {
            // Make sure this bag has an order.
            $sqlOpenOrder = "SELECT orderID, orders.customer_id, orders.notes FROM orders
            inner join bag on bagID = orders.bag_id
            where bagNumber = ?
            and orderStatusOption_id in (1,2)
            and orders.business_id = ?";
            $query = $this->db->query($sqlOpenOrder, array($bagNumber, $business_id));
            $openOrder = $query->result();

            if (!$openOrder) {
                set_flash_message('error', "FAILURE: No order in PICKED or WAITING status found for bag $bagNumber");
                send_admin_notification('WF Upload Failure', "Failure in library/washfold.php - A bag number ($bagNumber) was passed that does not have an open order.");
                redirect('/admin/orders/wf_processing');
            }
            $order_id = $openOrder[0]->orderID;
            $customer_id = $openOrder[0]->customer_id;
        }
        //Set constants
        $sqlOrder = "SELECT * from orders where orderID = ?";
        $query = $this->db->query($sqlOrder, array($order_id));

        $openOrder = $query->result();

        //make sure there is not already wash and fold in the order.
        $sqlItems = "select * from orderItem
            join product_process on product_process_id = product_processID
            join product on productID = product_id
            where order_id = ?
            and productCategory_id = ?";
        $query = $this->db->query($sqlItems, array($order_id, WFCATEGORY));
        $items = $query->result();
        if ($items) {
            $dontAdd = "There is already wash & fold in this order";
        }

        //get the customer's Preferences
        $sqlCustomer = "select * from customer where customerID = ".$openOrder[0]->customer_id;
        $query = $this->db->query($sqlCustomer);
        $customer = $query->result();

        //don't add a 0 qty
        if ($lbs < 1) {
            $dontAdd = "You must enter a valid qty";
        }

        if (empty($dontAdd)) {
            //Find out the price for this item for special location or customer pricing
            //this seems to work through add_item

            //round the qty up if over .4
            $rndQty = floor($lbs);
            $remainder = $lbs - $rndQty;
            if ($remainder >= .4) {
                $rndQty += 1;
            }
            $lbs = $rndQty;

            $this->load->model("product_model");
            $defaultWF = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $business_id);

            // check that customer's defaultWF product exists
            $this->load->model("product_process_model");
            if ($customer[0]->defaultWF) {
                if ($found = $this->product_process_model->is_active($customer[0]->defaultWF)) {
                    $defaultWF = $customer[0]->defaultWF;
                } else {
                    $support_email = get_business_meta($customer[0]->business_id, 'customerSupport');
                    send_email(SYSTEMEMAIL, SYSTEMFROMNAME, $support_email, "Customer defaultWF product does not exists",
                        "<pre>
                        Customer: " . print_r($customer[0], true) . "
                        Order: " . print_r($openOrder[0], true) . "
                        </pre>"
                    );
                }
            }

            //TODO: Make this dynamic based on the logged in user.  It is hard coded now
            //the supplier is set to the same business as the person weighing in the order.
            if ($this->buser_id == 63) {
                $supplier_id = 9;
            } //rich to SF Coin
            else {
                $this->load->model("product_supplier_model");
                $default_supplier = $this->product_supplier_model->getDefault($defaultWF, $order_id);
                $supplier_id = $default_supplier->supplierID;
            }  //all else to LL

            //Add the wash & fold to the order.
            $this->load->model('orderItem_model');
            $ret = $this->orderItem_model->add_item(
                $order_id,                 // $order_id
                0,                         // $item_id
                $defaultWF,                // $product_process_id
                $lbs,                      // $qty
                get_current_employee_id(), // $employee_id
                $supplier_id               // $supplier_id
            );

            if ($ret['status'] == 'success') {
                $this->load->library('wash_fold_product');
                $ret = $this->wash_fold_product->add_wash_fold_preferences($ret['orderItemID']);
            }
            $this->load->library("discounts");
            $orders = $this->order_model->get(array('orderID'=>$order_id));
            $my_order = $orders[0];
            try {
                $result = $this->discounts->unapply_discounts($my_order);
                $result = $this->discounts->apply_discounts($my_order, true, $lbs ); //need to pass lbs here
            } catch (Exception $e) {
                set_flash_message("error", $e->getMessage());
                send_exception_report($e);
            }

            $order = new Order($order_id, false);
            //Add customer's wf notes if they have them
            if ($customer[0]->wfNotes != '') {
                $order->notes = trim($order->notes . "\n" . $customer[0]->wfNotes);
                $order->save();
            }
            //change to inventoried status
            $options['order_id'] = $order->orderID;
            $options['orderStatusOption_id'] = 3;
            $options['locker_id'] = $openOrder[0]->locker_id;
            $options['orderPaymentStatusOption_id'] = $openOrder[0]->orderPaymentStatusOption_id;
            $this->order_model->update_order($options);
        } else {
                echo 'FAILED: '.$dontAdd.'<br>';
        }
    }

}
