<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use App\Libraries\DroplockerObjects\Product;
class Shoe_Product extends Product
{
    const PRODUCT_CLASS = 'shoe';
    const SERVICE_TYPE_ID = 5;
    const NAME = "Shoe";
    public function __construct()
    {
            parent::__construct();
            $this->class_slug = self::PRODUCT_CLASS;
            $this->service_type_id = self::SERVICE_TYPE_ID;
    }
    public function get_name()
    {
        return self::NAME;
    }

    public function get_service_type_id()
    {
        return self::SERVICE_TYPE_ID;
    }
}
