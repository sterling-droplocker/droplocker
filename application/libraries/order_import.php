<?php

class Order_import extends CI_Model
{
    public function insertOrderItems($params)
    {
            //FIRST MAKE SURE THIS ORDER DOES NOT ALREADY EXIST
            if ($params['order'] <> $params['lastOrder']) {
                $sqlFindOrder = "select * from orders where ticketNum = '".$params['customer_id'].":".$params['order']."' and business_id = {$this->business_id}";
                $q = $this->db->query($sqlFindOrder);
                $findOrder = $q->row();
                //if this order already exists
                if (!empty($findOrder)) {
                    $error .= '<br><strong>ERROR: Order '.$params['order'].' skipped.  Already exists.</strong>';

                    return $error;
                } else {
                    //change this order to inventoried
                    if (isset($findOrder['orderID'])) {
                        $options['order_id'] = $findOrder['orderID'];
                        $options['orderStatusOption_id'] = 3;
                        $this->order_model->update_order($options);
                    }
                }                               
            } else { //if this is a new order
                $thisOrder = '';
                $options['customer_id'] = $param['customer_id'];
                $thisOrder = $this->order_model->new_order($param['location'], $param['locker'], 0, get_employee_id($this->session->userdata('buser_id')), '', $this->business_id, $options);
                if (!is_int($thisOrder)) {
                    throw new Exception("Did not create new order successfully." . print_r($thisOrder));
                }
                $success .= 'Order: '.$thisOrder .' Created<br>';
            }

            if ($next != 1) {
                //If we are on the first item of the order, then add the line to the notes.

                //add the line to the notes of the order
                $update1 = "update orders set notes = CONCAT(notes, '".$params["line"]."'), ticketNum = '$this->business_id:$order' where orderID = $thisOrder";

                $update = $this->db->query($update1);

                if ($params['importType'] == 'DropLocker') {
                    //get product info to import
                    $getItemInfo = "select barcode, item.product_process_id, importMapping.product_process_id as import_pp, importMapping.price
                                    from item
                                    join barcode on barcode.item_id = itemID
                                    join importMapping on item.product_process_id = importMapping.productName
                                    where itemID = ".$params['item'];
                    $q = $this->db->query($getItemInfo);
                    $itemInfo = $q->row();
                    if (empty($itemInfo)) {
                        //find out what this product is
                        $getProduct = "select barcode, item.product_process_id, product.name as productName, process.name as processName
                                        from item
                                        join product_process on product_processID = product_process_id
                                        join product on productID = product_process.product_id
                                        join process on processID = product_process.process_id
                                        join barcode on barcode.item_id = itemID
                                        where itemID = ".$params['item'];
                        $q = $this->db->query($getProduct);
                        $product = $q->row();

                        $error .= '<br><strong>ERROR: No mapping for : '.$product['productName'].' / '.$product['processName'].'  in order <a href="/admin/orders/details/'.$thisOrder.'" target="_blank">'.$thisOrder.'</a>.  Customer Service has been notified and will fix this.</strong>';
                        send_admin_notification('ISSUE ALERT: Wholesale Import Error '.strtotime("now"), 'No mapping for '.$product['productName'].' / '.$product['processName'].'<br><br>For order: <a href="http://www.droplocker.com/admin/orders/details/'.$thisOrder.'">'.$thisOrder.'</a><br><br>Add mapping <a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/wholesale_mapping/'.$customerId.'">here</a>');
                        $next = 1;
                    } else {
                        $barcode = $itemInfo['barcode'];
                        $productProcess = $itemInfo['product_process_id'];
                        $product = $itemInfo['productName'];
                        $process = $itemInfo['processName'];
                        $price = $itemInfo['price'];
                    }
                } else {
                    //get the product_process for this item
                    $productName = str_replace("'", "", trim(strtolower($productPart[0])));
                    $processName = trim(strtolower($method));
                    $description = $productPart[1];
                    $sqlGetProduct = "select product_process_id, importMapping.price, product_id, process_id
                    from importMapping
                    LEFT join product_process on product_processID = product_process_id
                    where productName = '$productName'
                    and processName = '$processName'
                    and customer_id = $_POST[customer]";
                    $q = $this->db->query($sqlGetProduct);
                    $getProduct = $q->result();
                    if ($getProduct) {
                        $productProcess = $getProduct[0]->product_process_id;
                        $productID = $getProduct[0]->product_id;
                        $processID = $getProduct[0]->process_id;
                        $price = $getProduct[0]->price;
                    } else {
                        $error .= '<br><strong>ERROR: Could not find product: '.$productPart[0].' in order <a href="/admin/orders/details/'.$thisOrder.'" target="_blank">'.$thisOrder.'</a>.  Customer Service has been notified and will fix this.</strong>';
                        send_admin_notification('ISSUE ALERT: Wholesale Import Error '.strtotime("now"), 'Could not find product: '.$productPart[0].'<br><br>For order: <a href="http://www.droplocker.com/admin/orders/details/'.$thisOrder.'">'.$thisOrder.'</a><br><br>Add mapping <a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/wholesale_mapping/'.$customerId.'">here</a>');
                        $next = 1;
                    }
                }
                if ($next <> 1) {
                    //see if the barcode exists already
                    $getBarcode = "SELECT * FROM barcode
                    join item on barcode.item_id = itemID
                    where barcode = '$barcode'";
                    $q = $this->db->query($getBarcode);
                    $barcode_existing = $q->result();
                    if (!empty($barcode_existing)) {

                        //update the product_process for this barcode
                        $updateItem = "update item set product_process_id = $productProcess
                        where itemID = ".$barcode_existing[0]->item_id;
                        $this->db->query($updateItem);
                        $itemId = $barcode_existing[0]->item_id;

                    } else {

                        //create the item
                        $success .= '<br>Creating Item for barcode '.$barcode.' as Product: '.$product.' Process: '.$cleaningProcess;

                        $options['product_process_id'] = $productProcess;
                        $options['business_id'] = $this->business_id;
                        $itemId = $this->item_model->new_item($options);


                        //create the barcode
                        $addBarcode = "INSERT INTO `barcode` (`barcode` , `item_id`, business_id)
                        VALUES ('$barcode', '$itemId', $this->business_id)";
                        $this->db->query($addBarcode);

                        //tie it to a customer
                        $addToCustomer = "INSERT INTO `customer_item` (`customer_id` , `item_id`)
                        VALUES ('$customerId', '$itemId')";
                        $this->db->query($addToCustomer);

                    }
                    //add the item to the order
                    // TODO: hardcoed $supplier_id 10
                    $itemInfo = $this->orderitem_model->add_item(
                        $thisOrder,                // $order_id
                        $itemId,                   // $item_id
                        $productProcess,           // $product_process_id
                        1,                         // $qty
                        get_current_employee_id(), // $employee_id
                        10,                        // $supplier_id
                        $price,                    // $price
                        $description              // $notes
                    );

                }
            }
            $lastOrder = $order;



        if ($skipOrder == 0) {
            //inventory last order
            $options['order_id'] = $thisOrder;
            $options['orderStatusOption_id'] = 3;
            $this->order_model->update_order($options);
        }

        $success .= '<br>Upload Complete<br>';
    }

    //returns the number of hours worked by an employee for the specified date
    //note, OT is based on 8 hours in a day, not 40 hours in a week.  So if they have worked over 40 hours this week but less than 8 in the specificed day, it will show no OT
    public function todaysHours($emp_id, $dateSelected)
    {
        $todaysDetails = array(
                'startTime'=>'',
                'totHrs'=>'',
                'todayHrs'=>'',
                'regHours'=>'',
                'overTime'=>'');
        $cardDate = date("Y-m-d", strtotime($dateSelected));
        $getPunches = "SELECT time_to_sec(timeInOut) / 60 as minutes, timeCardDetail.*
           from timeCardDetail
           JOIN timeCard on timeCardID= timeCardDetail.timeCard_id
           join employee on employeeID = timeCard.employee_id
           join business_employee on business_employee.employee_id = employeeID
           WHERE timeCard.employee_id = '$emp_id'
           AND timeCardDetail.day = '$cardDate'
           AND business_id = $this->business_id";
//          echo $getPunches; exit;
        $query = $this->db->query($getPunches);
        $punches = $query->result();

        foreach ($punches as $p) {
            $counter ++;
            if ($counter == 1) { $todaysDetails['startTime'] = $p->timeInOut; }

            if ($p->in_out == 'In') {
                $todaysDetails['totHrs'] -= $p->minutes;
            } else {
                $todaysDetails['totHrs'] += $p->minutes;
            }
        }

        //if it is greater than 0 they are clocked in
        if ($todaysDetails['totHrs'] < 0) {
            //if it's early in the morning, before 6
            if (strtotime("now") < strtotime("06:00:00")) {
                $todaysDetails['todayHrs'] = (((strtotime("now") - strtotime("yesterday")) / 60) + $todaysDetails['totHrs']) / 60;
            } else {
                $todaysDetails['todayHrs'] = (((strtotime("now") - strtotime("00:00:00")) / 60) + $todaysDetails['totHrs']) / 60;
            }
        } else {
            $todaysDetails['todayHrs'] = $todaysDetails['totHrs'] / 60;
        }

        $otHours = 0;
        $regHours = 0;
        if ($todaysDetails['todayHrs']> 8) {
            $otHours = $todaysDetails['todayHrs'] - 8;
            $regHours = 8;
        } else {
            $regHours = $todaysDetails['todayHrs'];
        }
        $todaysDetails['regHours'] = $regHours;
        $todaysDetails['overTime'] = $otHours;

        return $todaysDetails;
    }

    //returns all the hours worked for all employees on a specified date
    public function dailyHours($dateSelected)
    {
        $cardDate = date("Y-m-d", strtotime($dateSelected));
        $sqlAllClocks = "SELECT position, firstName, day, timeInOut, timeCardDetail.in_out, employeeID, time_to_sec(timeInOut) / 60 as minutes, timeCardDetail.type, timeCardID, weekOf, aclRole_id, aclroles.name as aclName
                    FROM timeCardDetail
                    join timeCard on timeCardDetail.timeCard_id = timeCardID
                    join employee on employeeID = timeCard.employee_id
                    join business_employee on business_employee.employee_id = employeeID
                    join aclroles on aclID = aclRole_id
                    where timeCardDetail.day = '$cardDate'
                    and business_employee.business_id = $this->business_id
                    order by position, day desc, employeeID, timeInOut";

        $query = $this->db->query($sqlAllClocks);
     //echo $sqlAllClocks;
        $allClocks = $query->result();

        foreach ($allClocks as $a) {
            $empHours[$a->employeeID]['timeCardID'] = $a->timeCardID;
            $empHours[$a->employeeID]['weekOf'] = $a->weekOf;
            $empHours[$a->employeeID]['name'] = $a->firstName;
            $empHours[$a->employeeID]['position'] = strtolower($a->aclName);
            if ($a->type == "Regular") {
                if ($a->in_out == 'In') { $empHours[$a->employeeID]['reg'] -= $a->minutes; } elseif ($a->in_out == 'Out') { $empHours[$a->employeeID]['reg'] += $a->minutes; }
            }
            if ($a->type == "Sick") {
                if ($a->in_out == 'In') { $empHours[$a->employeeID]['sick'] -= $a->minutes; } elseif ($a->in_out == 'Out') { $empHours[$a->employeeID]['sick'] += $a->minutes; }
            }
        }

        return $empHours;
    }

    public function dailyHoursByRole($dateFind)
    {
        $empHours = $this->dailyHours($dateFind);

        if ($empHours) {
            foreach ($empHours as $e) {
                $empReg = 0;
                $empSick = 0;
                $empOt = 0;

                if ($e['reg'] > 480) {
                    $empOt = $e['reg'] - 480;
                    $empReg = 480;
                } else { $empReg = $e['reg']; }

                $dayTime[$e['position']]['daySick'] += number_format($e['sick'] / 60, 1);
                $dayTime[$e['position']]['dayReg'] += number_format($empReg / 60, 1);
                $dayTime[$e['position']]['dayOt'] += number_format($empOt / 60, 1);
            }
        }
//var_dump($dayTime);
        return $dayTime;
    }


}
