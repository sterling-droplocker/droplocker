<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Server_Meta;
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subdomain
{
    public $CI;
    public $business;
    public $website;
    
    public $specialSubdomains = array(
        "reports",
        "android",
        "driver-api",
        "mailchimp",
        "pos-demo-api",
        "pos-test-api",
    );
    
    public $specialSubdomainsPrefix = array(
        "qa-",
    );
    
    public function __construct()
    {
        if (PHP_SAPI === 'cli') {
            return true;
        }
        $this->CI = get_instance();
        $this->CI->load->helper('url');
        $this->loadSpecialSubdomains();
        try {
            $this->parse_domain();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    private function loadSpecialSubdomains()
    {
        $config_found = $this->CI->config->load('subdomain', false, true);
        if ($config_found) {
            $specialSubdomains = (array) $this->CI->config->item('special_subdomains');
            $this->specialSubdomains = array_merge($this->specialSubdomains, $specialSubdomains);

            $specialSubdomainsPrefix = (array) $this->CI->config->item('special_subdomains_prefix');
            $this->specialSubdomainsPrefix = array_merge($this->specialSubdomainsPrefix, $specialSubdomainsPrefix);
        }
    }
    
    private function isSpecialSubdomain($subdomain)
    {
        return in_array($subdomain, $this->specialSubdomains) || 
            str_starts_with_any_lb($subdomain, $this->specialSubdomainsPrefix);
    }
    

    /**
    * parse_domain method checks the url structure for a business url and then sets the proper template
    *
    * The .htaccess strips the www. and forces to https
    *
    * @example droplocker.com - System needs to ignore
    * @example laundrylocker.com - This domain is set as a serverAlias in the Apache config file for droplocker.
    * * There is a controller that manages all the files for laundrylocker.
    * * There is no myaccount.laundrylocker.com since it is a serverAlias for droplocker. All other business domains will use myaccount as a subdomain
    * @example dashlocker.com - This is a hosted account
    * * There will be an apache config file for each business account in the conf.d directory
    */
    public function parse_domain()
    {
        //die(var_dump(debug_backtrace()));
        // load stuff
        $this->CI->load->model('business_model');
        $this->CI->load->model('website_model');
        $this->CI->load->helper('cookie');
        $this->CI->load->library('template');

        // get the elements of the domain name
        $domain_array = explode(".",$_SERVER['SERVER_NAME']);
        $size = sizeof($domain_array);
        if ($size > 3 && $domain_array[0] == 'myaccount') {
            $domain_array = explode(".",$_SERVER['SERVER_NAME'], 3);
            $size = sizeof($domain_array);
        }

        $main_domain = $domain_array[$size-2].".".$domain_array[$size-1];
        switch ($size) {
        // The following case handles the case where there is no subdomain, the subdomain is 'admin' and the server is in Bizzie mode, or the subdomain is 'reports'.
            // Note the 'reports' subdomain is a special case which is intended to refer to the dedicated reporting and cron server instance instead of the application server instance.
            // Note, the 'android' subdomain is a special case which is intended to refer to the dedicated server for handling android requests.
            case ($size < 3  || ($domain_array[0] == "admin" && $domain_array[1] == "bizzie") || $this->isSpecialSubdomain($domain_array[0]) ):
                $businesses= Business::search_aprax(array('website_url'=>$_SERVER['HTTP_HOST']));
                // if a business is found, we need to display their information
                if (empty($businesses)) {
                    if ($this->CI->router->fetch_directory() == "account/" && $this->CI->router->fetch_class() != "sms") {
                        die("No customer facing business found with website URL {$_SERVER['HTTP_HOST']}");
                    }
                } else {
                    $business = $businesses[0];
                    $this->CI->business = $business;
                    $this->website = $this->CI->website_model->get_website($business->businessID);
                    setlocale(LC_ALL, $business->locale);
                    // for private websites, set a dev_access cookie
                    if ($this->CI->uri->segment(1)=='set_cookie') {
                        $cookie = array(
                        'domain' => $_SERVER['HTTP_HOST'],
                        'name'   => 'dev_access',
                        'value'  => $this->business->businessID,
                        'expire' => '86500',
                        'path' => '/'
                        );
                        $this->CI->input->set_cookie($cookie);
                    }

                    if ($business->businessID == 3) {
                     // if this business has a custom template, load it
                        if ($this->website[0]->template) {
                            $this->CI->template->set_template($this->website[0]->template);
                        }
                    }
                }

                return true;
            break;

            /**
            * if size = 3 The domain consists of a subdomain
            *
            * Scenarios:
            * 1. The business hosts their normal account on a throd party host want to use droplocker as as their domains
            * 	  Example companyname.droplocker.com
            *
            * 2. The business wants to use their domain name with myaccount as a subdomain
            * 	  Example: myaccount.dashlocker.com
            * 	  Note that their are manual setting that need to be performed for this for both the business and in the droplocker apache configs.
            * 	  Business needs to set an A record for "myaccount" that points to droplocker IP
            * 	  the droplocker apache config file needs a server alias added for the subdomains. Example: serverAlias myaccount.dashlocker.com
            */
            case ($size == 3):
                if ($domain_array[1] == 'droplocker') {

                    // this type of subdomain will vary so we match up with the slug in the business table
                    $business = Business::search(array('slug'=>$domain_array[0]));

                    // if a business is found, we need to display their information
                    if ($business->businessID) {
                        $this->CI->business = $business;
                        $this->website = $this->CI->website_model->get_website($business->businessID);
                        setlocale(LC_ALL, $business->locale);
                        // redirect if the site is private
                        if (!get_cookie('dev_access') && $this->website[0]->status=='private') {
                            $domain = explode(".",$_SERVER['HTTP_HOST']);
                        }
                    } else {
                        throw new Exception('Business not found for: '. $_SERVER['HTTP_HOST']);
                    }

                    // if this business has a custom template, load it
                    if ($this->website[0]->template) {
                        $this->CI->template->set_template($this->website[0]->template);
                    }

                } else {
                    if ($domain_array[0] !='myaccount' && !in_bizzie_mode()) {
                        throw new Exception("Subdomain is not 'myaccount' and the server is not in bizzie mode.");
                    }
                    //If server is in Bizzie mode, then we search for the customer facing business site by the subdomain property
                    if (in_bizzie_mode()) {
                        $businesses = Business::search_aprax(array("subdomain" => $domain_array[0]));
                        if (empty($businesses)) {
                            throw new \Exception(sprintf("Could not find Bizzie business for subdomain %s", $domain_array[0]));
                        } else {
                            $business = $businesses[0];
                        }
                    } else { //Otherwise, we search based on the website_url defined for the business
                        $website_url = $domain_array[1].".".$domain_array[2];
                        if ($domain_array[2] != 'qa') {
                            $business = Business::search(array('website_url' => $website_url));
                        } else {
                            $business = Business::search(array('website_url LIKE' => $domain_array[1] . '.%'));
                        }

                        if (empty($business->businessID)) {
                            throw new \Exception("Could not find any Droplocker business with the base url '$website_url' for {$_SERVER['HTTP_HOST']}. ");
                        }
                    }


                    // if a business is found, we need to display their information
                    if ($business->businessID) {
                        setlocale(LC_ALL, $business->locale);
//                        $this->CI->lang->load("language", $business->locale);
                        // If no controller is called, we send them to the account controller
                        if ($this->CI->uri->uri_string()=='') {
                            redirect("/account");
                        }

                        $this->CI->business = $business;
                        $this->website = $this->CI->website_model->get_website($business->businessID);

                        // if this business has a custom template, load it
                        if (@$this->website[0]->template) {
                            $this->CI->template->set_template($this->website[0]->template);
                        }
                    } else {
                        throw new \Exception("An unhandled error occurred attempting to resolve the domain. Please contact Droplocker Support.");
                    }
                }

                return true;

                break; // end $size==3

            case $size > 3 :
                return true;
                break;


            default:
                redirect("http://".$main_domain);
                break;

        }

    }

    /**
     * writes the title tag in the template
     */
    public function title()
    {
        $title = isset($this->website[0]->title) ? @$this->website[0]->title : $this->business->companyName;
        $this->CI->template->write('title',$title, true);
    }

    /**
         * Note, * errors are suppressed in this file.
    * adds custom javascript files
    */
    public function javascript()
    {
        $this->CI->template->write('javascript',"\n".@$this->website[0]->javascript, true);
    }

    /**
         * Note, * notice errors are suppressed in this function.
    * adds custom CSS files
    */
    public function style()
    {
        $this->CI->template->write('style',"\n".@$this->website[0]->stylescript, true);
    }

    /**
         * Note, * notice errors are suppressed in this function.
     * Creates the header of the website.
     */
    public function header()
    {
        $this->CI->template->write('header',@$this->website[0]->header, true);
    }

    /**]
         * Note, * notice errors are suppressed in thie function.
    * Creates the footer of the website
    */
    public function footer()
    {
            preg_match('#%if_testimonial%(.*?)%/if_testimonial%#smi', @$this->website[0]->footer, $match);
            if (count($match)) {

                $testimonial = get_random_testimonial($this->website[0]->business_id);

                $s = array('%if_testimonial%', '%/if_testimonial%', '%testimonial%');
                $r = array('', '', $testimonial);
                $old = $match[0];
                $new = str_replace($s, $r, $old);

                $this->website[0]->footer = str_replace($old, $new, @$this->website[0]->footer);
            }

            $this->CI->template->write('footer',@$this->website[0]->footer, true);
    }


}
