<?php
namespace DropLocker\RestClient;
/* docs: http://docs.boxture.com/shipments/introduction */
class BoxtureAPI extends RestClient
{

    public $url = 'https://api.localexpress.nl';
    public $sandbox_url = 'https://api-qa.localexpress.nl';
    public $format = 'json';
    public $sandbox = false;
    public $assoc = false;

    public $lastRequest = NULL;
    public $api_key = NULL;

    public function setupAuth($url, $ch)
    {
        // HTTP auth, user is $api_key password empty
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key . ":");

        // Not for auth but is easy to place this extra option here
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        
        return $url;
    }

}