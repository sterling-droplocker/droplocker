<?php
namespace DropLocker\RestClient;

class DelivAPI extends RestClient
{

    public $url = 'https://api.deliv.co/';
    public $sandbox_url = 'https://sandbox.deliv.co/';
    public $version = 2;
    public $format = 'json';
    public $sandbox = false;
    public $assoc = false;

    public $lastRequest = NULL;
    public $api_key = NULL;

    /**
     * Performs a request agains the API
     *
     * @param string $uri path inside the API
     * @param string $method GET, POST, PUT, DELETE, HEAD, ...
     * @param string $params if not null, gets converted to JSON
     * @param string $http_status returns the http status code after the request
     * @return object the parsed response
     */
    public function request($uri, $method = 'GET', $params = null, &$http_status = null)
    {
        // prepend version to path
        $uri = 'v' . $this->version . $uri;

        return parent::request($uri, $method, $params, $http_status);
    }

    /**
     * Setup the API key as a parameter in the url
     *
     * @see \DropLocker\RestClient\RestClient::setupAuth()
     */
    protected function setupAuth($url, $ch)
    {
        return $url . '?api_key=' . $this->api_key;
    }

}