<?php
namespace DropLocker\RestClient;

class RickshawAPI extends RestClient
{

    public $url = 'https://gorickshaw.com';
    public $sandbox_url = 'NOT IMPLEMENTED';
    public $format = 'json';
    public $sandbox = false;
    public $assoc = false;

    public $lastRequest = NULL;
    public $api_key = NULL;

    public function setupAuth($url, $ch)
    {
        // HTTP auth, user is $api_key password empty
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key . ":");

        return $url;
    }

}