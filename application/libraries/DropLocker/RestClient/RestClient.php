<?php
namespace DropLocker\RestClient;

class RestClient
{

    public $url = null;
    public $sandbox_url = null;
    public $format = 'json';
    public $sandbox = false;
    public $assoc = false;

    public $lastRequest = NULL;
    public $api_key = NULL;

    public $ci = NULL;
    public $debug = false;
    protected $business_id = false;
    

    /**
     * Creates a RestClient
     *
     * @param string $api_key the token used to authenticate requests
     * @param bool $sandbox if true, uses sandbox url
     */
    public function __construct($api_key, $sandbox = false)
    {
        $this->api_key = $api_key;
        $this->sandbox = $sandbox;
        
        // Get the codeigniter instance
        $this->ci =& get_instance();

        // Set the business_id property for current business
        $this->setBusinessId();

        // Set debug property as true/false for current business
        $this->setDebug();
    }

    /**
     * Performs a request agains the API
     *
     * @param string $uri path inside the API
     * @param string $method GET, POST, PUT, DELETE, HEAD, ...
     * @param string $params if not null, gets converted to JSON
     * @param string $http_status returns the http status code after the request
     * @return object the parsed response
     */
    public function request($uri, $method = 'GET', $params = null, &$http_status = null)
    {
        $base_url = $this->sandbox ? $this->sandbox_url : $this->url;
        $url = $base_url . $uri;

        $body = !is_null($params) ? json_encode($params) : null;
        $response = $this->curlRequest($url, $method, $body, $http_status);
        if ($this->format == 'json') {
            $parsed = json_decode($response, $this->assoc);
        } elseif ($this->format == 'xml') {
            $parsed = $this->parseXML($response);
        } else {
            $parsed = $this->parseResponse($response);
        }

        if ($this->debug) {
            $this->debugLog($url, $body, $response);
        }

        return $parsed;
    }

    /**
     * Performs a GET request
     *
     * @param string $uri path inside the API
     * @param string $http_status returns the http status code after the request
     * @return object the parsed response
     */
    public function get($uri, &$http_status = null)
    {
        return $this->request($uri, 'GET', null, $http_status);
    }

    /**
     * Performs a POST request
     *
     * @param string $uri path inside the API
     * @param string $params if not null, gets converted to JSON
     * @param string $http_status returns the http status code after the request
     *
     * @return object the parsed response
     */
    public function post($uri, $params, &$http_status = null)
    {
        return $this->request($uri, 'POST', $params, $http_status);
    }

    /**
     * Performs a PUT request
     *
     * @param string $uri path inside the API
     * @param string $params if not null, gets converted to JSON
     * @param string $http_status returns the http status code after the request
     * @return object the parsed response
     */
    public function put($uri, $params, &$http_status = null)
    {
        return $this->request($uri, 'PUT', $params, $http_status);
    }

    /**
     * Performs a DELETE request
     *
     * @param string $uri path inside the API
     * @param string $params if not null, gets converted to JSON
     * @param string $http_status returns the http status code after the request
     * @return object the parsed response
     */
    public function delete($uri, $params = null, &$http_status = null)
    {
        return $this->request($uri, 'DELETE', $params, $http_status);
    }

    /**
     * Does the curl request
     *
     * @param string $url the url
     * @param string $method GET, POST, PUT, DELETE, HEAD, ...
     * @param string $params if not null, gets converted to JSON
     * @param string $http_status returns the http status code after the request
     * @return string the raw response
     */
    protected function curlRequest($url, $method = 'GET', $body = null, &$http_status = null)
    {
        $ch = curl_init();

        $url = $this->setupAuth($url, $ch);
        curl_setopt($ch, CURLOPT_URL, $url);

        $method = strtoupper($method);
        switch ($method) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                break;

            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
               break;

           case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
               break;

            default:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                break;
        }

        if (!is_null($body)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }

        // relax SSL for now,
        // TODO: seupt path to certs and verify peer
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);

        $headers = $this->buildHeaders();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($errno = curl_errno($ch)) {
            throw new \Exception("Curl Error: " . curl_error($ch));
        }

        curl_close($ch);

        $this->lastRequest = compact('url', 'method', 'body', 'headers', 'response', 'http_status');

        return $response;
    }

    /**
     * Generates the headers for the request
     *
     * @return array
     */
    protected function buildHeaders()
    {
        $headers = array(
            'Content-Type: application/json',
        );

        return $headers;
    }

    /**
     * Seup authorization
     *
     * Can be used to modify the URL or set curl options
     *
     * @param string $url the original url
     * @param handle $ch a curl handle
     * @return string url to be used in the request
     */
    protected function setupAuth($url, $ch)
    {
        return $url . '?api_key=' . $this->api_key;
    }

    /**
     * Used to parse the response if its not JSON or XML
     *
     * @param string $response thr raw body
     * @return object the parsed response
     */
    public function parseResponse($response)
    {
        return $response;
    }

    /**
     * Parse a XML response body
     *
     * @param string $xml
     * @return object the parsed response
     */
    protected function parseXML($xml)
    {
        $out = json_decode(json_encode((array) $xml), true);
        $filter = function($in) use (&$filter) {
            if (is_array($in))  {
                if (!empty($in['@attributes']['nil'])) {
                    return null;
                }

                if (!empty($in['@attributes']['type'])) {
                    unset($in['@attributes']);
                }

                return array_map($filter, $in);
            }
            return $in;
        };

        $out = array_map($filter, $out);
        unset($out['@attributes']);

        if (!$this->assoc) {
            $out = json_decode(json_encode($out), false);
        }

        return $out;
    }

    /**
     * Check if last request was error
     * @return boolean
     */
    public function isError()
    {
        return $this->lastRequest['http_status'] > 299;
    }

    /**
     * Check if last request was success
     * @return boolean
     */
    public function isOk()
    {
        return $this->lastRequest['http_status'] <= 299;
    }

    /**
     * Sets business_id property
     * @return NULL
     */
    protected function setBusinessId()
    {
        $business_id = $this->ci->session->userdata("business_id");
        if (empty($business_id)) {
            $this->business_id = empty($this->ci->business_id)?false:$this->ci->business_id;
        }       
    }

    /**
     * Sets debug property as true/false for current business
     * @return NULL
     */
    protected function setDebug()
    {
        if (!empty($this->business_id)) {
            $this->debug = get_business_meta($this->business_id, 'home_delivery_enable_debug', false);
        }        
    }

    /**
     * Inserts debug data into db home_delivery_log table
     * @return NULL
     */
    protected function debugLog($url, $body, $response)
    {
        try {
            $this->ci->db->query("INSERT INTO home_delivery_log(business_id, url, requestBody, requestResponse) VALUES(?, ?, ?, ?)", array($this->business_id, $url, $body, $response));
        } catch (Exception $e) {
            // we dont want to do anything here!
        }
    }
}
