<?php

namespace  DropLocker\Util;

class Lock
{
    private static $fh = array();
    private static $locks = array();

    /**
     * Acquire a lock
     * @param string $key name for the resource
     * @param bool $block if set to true the call will block
     * @return boolean
     */
    public static function acquire($key, $block = false)
    {
        self::setupHandle($key);

        if (flock(self::$fh[$key], $block ? LOCK_EX : LOCK_EX | LOCK_NB)) {
            ftruncate(self::$fh[$key], 0);
            fwrite(self::$fh[$key], getmypid() . "\n");
            fflush(self::$fh[$key]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Release an aquired lock
     * @param string $key name for the resource
     * @return boolean
     */
    public static function release($key)
    {
        self::setupHandle($key);
        return flock(self::$fh[$key], LOCK_UN);
    }

    protected function setupHandle($key)
    {
        $fkey = preg_replace('/[^a-z0-9]+/i', '_', $key);
        $filename = "/tmp/lock.$fkey";
        if (!file_exists($filename)) {
            touch($filename);
            chmod($filename, 0666);
        }

        if (empty(self::$fh[$key])) {
            self::$fh[$key] = fopen($filename, "c");
        }

        return $filename;
    }
}