<?php

namespace  DropLocker\Util;

class Random
{

    /**
     * Retrieves a random customer from the database associated with the specified business
     *
     * Added opional options array for getting more specific customers.
     *
     *
     * @param int $business_id
     * @param array $options
     * @return App\Libraries\DroplockerObjects\Customer
     * @throws Database_Exception
     */
    public static function getCustomer($business_id, $options = array())
    {
        $CI = get_instance();
        $CI->db->order_by("RAND()");
        $CI->db->where("business_id", $business_id);
        $CI->db->where('city !=', "");

        if (!empty($options)) {
            $CI->db->where($options);
        }

        $CI->db->limit(1);
        $get_random_customer_query = $CI->db->get("customer");
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_customer = $get_random_customer_query->row();
        return $random_customer;
    }

    /**
     * Retrieves a random instance of a particular class
     * @param string $class_name The class from which to instnatitate the object
     * @param where
     * @param bool $get_relationships If true, retrieves the cardinal relationships defined in the class, otherwise, does not retrieve the cardinal relationships.
     * @return \DropLocker_Object_class_path
     * @throws \Database_Exception
     */
    public static function getObject($class_name, $get_relationships = false, $where = array())
    {
        if (!is_string($class_name))
        {
            throw new \InvalidArgumentException("class_name' must be a string.");
        }

        $CI = get_instance();
        $DropLocker_Object_class_path = "App\Libraries\DroplockerObjects\\$class_name";
        $CI->db->select($DropLocker_Object_class_path::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_object_query = $CI->db->get_where($DropLocker_Object_class_path::$table_name, $where);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_object_result = $get_random_object_query->row();
        $random_object = new $DropLocker_Object_class_path($random_object_result, $get_relationships);

        return $random_object;
    }

}