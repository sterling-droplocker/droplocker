<?php

namespace DropLocker\Processor;

// ci loader forces singleton, using good all manual instanciantion
include_once(APPPATH.'libraries/paypal/Paypal_pro.php');

/*
 * This is a wrapper between the processor abstract class, and the thirdparty paypal libraries in /libraries/paypal/*
 */

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Business;

class PaypalPro extends AbstractProcessor
{

    /**
     * @var \PayPal_Pro
     */
    protected $paypal_pro_lib;
    protected $business_id;
    protected $customer_id;
    protected $api_version;
    protected $currency;
    protected $country;
    public $business;

    const PROCESSOR = "paypalprocessor";

    /**
     *
     * we load the paypal payments pro library here, and use this class
     * as a wrapper for the abstract processor class
     *
     * @param array $params
     * @throws \Exception
     */
    function __construct($params = array())
    {
        $this->api_version = '85.0';    //for dev

        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the paypal credentials');
        }

        $sandbox = DEV ? true : false;

        //to load the paypal credentials
        $business_id = $params['business_id'];

        $this->country = get_business_meta($business_id, 'paypal_country', 'US');
        $this->currency = get_business_meta($business_id, 'paypal_currency', 'USD');

        $config = array(
            'Sandbox' => $sandbox, // Sandbox / testing mode option.
            'APIUsername' => get_business_meta($business_id, 'paypal_user'), // PayPal API username of the API caller
            'APIPassword' => get_business_meta($business_id, 'paypal_password'), // PayPal API password of the API caller
            'APISignature' => get_business_meta($business_id, 'paypal_signature'), // PayPal API signature of the API caller
            'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->api_version                                       // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );

        // Show Errors
        if ($config['Sandbox']) {
            error_reporting(E_ALL);
            ini_set('display_errors', '1');
        }

        // ci loader forces singleton, using good all manual instanciantion
        $this->paypal_pro_lib = new \PayPal_Pro($config);
    }

    public function setTestMode()
    {
        $this->paypal_pro_lib->Sandbox = true;
        // Merchant API endpoints, Authentication: API Signature, Format: Name-Value Pair
        $this->paypal_pro_lib->EndPointURL = 'https://api-3t.sandbox.paypal.com/nvp';
    }

    public function setLiveMode()
    {
        $this->paypal_pro_lib->Sandbox = false;
        // Merchant API endpoints, Authentication: API Signature, Format: Name-Value Pair
        $this->paypal_pro_lib->EndPointURL = 'https://api-3t.paypal.com/nvp';
    }

    public function setExpressCheckout($returnURL, $canelURL)
    {
        $response = $this->paypal_pro_lib->SetExpressCheckout(array(
            'Payments' => array(
                array('AMT' => 0,),
            ),
            'BillingAgreements' => array(
                array('L_BILLINGTYPE' => 'MerchantInitiatedBillingSingleAgreement',),
            ),
            'SECFields' => array(
                'RETURNURL' => $returnURL,
                'CANCELURL' => $canelURL,
            ),
        ));
        $response['endpoint_url'] = $this->paypal_pro_lib->EndPointURL;

        if (!empty($response['ERRORS'])) {
            return array(
                'status' => 'FAIL',
                'message' => $response['ERRORS'][0]['L_LONGMESSAGE'],
                'error_code' => $response['ERRORS'][0]['L_ERRORCODE'],
                'response' => $response,
            );
        }

        return array(
            'status' => 'SUCCESS',
            'message' => 'Express chekout started',
            'response' => $response,
            'url' => $response['REDIRECTURL'],
        );
    }

    public function getCustomerDetails($token)
    {
        $response = $this->paypal_pro_lib->GetExpressCheckoutDetails($token);

        return $response;
    }

    protected function createBillingAgreement($token)
    {
        $response = $this->paypal_pro_lib->CreateBillingAgreement($token);
        $response['endpoint_url'] = $this->paypal_pro_lib->EndPointURL;

        if (!empty($response['ERRORS'])) {
            return array(
                'status' => 'FAIL',
                'message' => $response['ERRORS'][0]['L_LONGMESSAGE'],
                'error_code' => $response['ERRORS'][0]['L_ERRORCODE'],
                'response' => $response,
            );
        }

        return array(
            'status' => 'SUCCESS',
            'message' => 'Express chekout started',
            'response' => $response,
            'billing_agreement_id' => $response['BILLINGAGREEMENTID'],
        );
    }

    public function authorizeToken($token, $order_id = 0, $customer_id = 0, $business_id = 0)
    {
        $result = $this->createBillingAgreement($token);

        if ($result['status'] == 'SUCCESS') {
            $status = 'SUCCESS';
            $message = 'Authorization Success';
            $result_code = 1;
            $transaction_id = 0;
            $transaction_id = $result['billing_agreement_id'];
        } else {
            $status = 'FAIL';
            $message = $result['message'];
            $result_code = $result['error_code'];
        }
        
        $response = new Response($status, $result_code, $transaction_id, //refNumber
            $message, '', '', '', 'auth', $customer_id, $business_id, self::PROCESSOR, 1, '', $order_id, json_encode($result['response']));
        return $response;
    }

    public function authorizeCard($creditCard = array(), $order_id = 0, $customer_id = 0, $business_id = 0)
    {
        $result = $this->process_auth($creditCard);

        //if we have a successful authorization
        if ('SUCCESS' == $result['status']) {
            $transaction_id = $result['transaction_id'];
            $status = 'SUCCESS';
            $message = 'Authorization Success';
            $result_code = 1;

            //if the authorization failed for some reason
        } else {
            $transaction_id = 0;
            $status = 'FAIL';
            $message = $result['errors']['L_LONGMESSAGE'];
            $result_code = $result['errors']['L_ERRORCODE'];
        }

                
        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id, //refNumber
                $message, '', '', '', 'auth', $customer_id, $business_id, self::PROCESSOR, 1, '', $order_id, json_encode($result['response']));

        return $response;
    }

    public function makeSale($amount = 0, $order_id = 0, $customer_id = 0, $business_id = 0)
    {
        $this->business_id = $business_id;
        $this->customer_id = $customer_id;
        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array('customer_id' => $customer_id, 'business_id' => $business_id));
        if ($creditCard->payer_id == '') {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        $authorization_id = $creditCard->payer_id;             //the TRANSACTIONID that paypal gave us from the previous authorization
        //first do reautorization for new amount
        $result = $this->reference_transaction($authorization_id, $amount);

        //if we have a successful authorization
        if ('SUCCESS' == $result['status']) {
            $transaction_id = $result['transaction_id'];
            $status = 'SUCCESS';
            $message = 'Successful Transaction';
            $result_code = 1;

            //if the authorization failed for some reason
        } else {
            $transaction_id = 0;
            $status = 'FAIL';
            $message = $result['errors']['L_LONGMESSAGE'];
            $result_code = $result['errors']['L_ERRORCODE'];
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id, //refNumber
                $message . $this->getCreditCardSummary($creditCard), '', '', '', 'Sale', $customer_id, $business_id, self::PROCESSOR, $amount, '', $order_id, json_encode($result['response']));

        return $response;
    }

    /**
     * Run the capture
     *
     * Uses the transaction id from the initial authorization from
     * when someone setsup their credit card in the account section.
     *
     * @param string $reference_id
     * @param float $amount
     * @return array
     */
    private function reference_transaction($reference_id, $amount)
    {
        //make sure formatting is correct (ie, NOT 10.5 but 10.50)
        //might not work for international where they use a comma
        $amount = number_format($amount, 2, '.', ''); // Do not add comma, confuses MySQL

        $DRTFields = array(
            'referenceid' => $reference_id, // Required.  A transaction ID from a previous purchase, such as a credit card charage using DoDirectPayment, or a billing agreement ID
            'paymentaction' => 'Sale', // How you want to obtain payment.  Values are:  Authorization, Sale
            'ipaddress' => '', // IP address of the buyer's browser
            'reqconfirmshipping' => '', // Whether you require that the buyer's shipping address on file with PayPal be a confirmed address or not.  Values are 0/1
            'returnfmfdetails' => '', // Flag to indicate whether you want the results returned by Fraud Management Filters.  Values are 0/1
            'softdescriptor' => ''   // Per transaction description of the payment that is passed to the customer's credit card statement.
        );


        $PaymentDetails = array(
            'amt' => $amount, // Required. Total amount of the order, including shipping, handling, and tax.
            'currencycode' => $this->currency, // A three-character currency code.  Default is USD.
            'itemamt' => '', // Required if you specify itemized L_AMT fields. Sum of cost of all items in this order.
            'shippingamt' => '', // Total shipping costs for this order.  If you specify SHIPPINGAMT you mut also specify a value for ITEMAMT.
            'insuranceamt' => '',
            'shippingdiscount' => '',
            'handlingamt' => '', // Total handling costs for this order.  If you specify HANDLINGAMT you mut also specify a value for ITEMAMT.
            'taxamt' => '', // Required if you specify itemized L_TAXAMT fields.  Sum of all tax items in this order.
            'insuranceoptionoffered' => '', // If true, the insurance drop-down on the PayPal review page displays Yes and shows the amount.
            'desc' => '', // Description of items on the order.  127 char max.
            'custom' => '', // Free-form field for your own use.  256 char max.
            'invnum' => '', // Your own invoice or tracking number.  127 char max.
            'notifyurl' => '', // URL for receiving Instant Payment Notifications
            'recurring' => ''   // Flag to indicate a recurring transaction.  Values are:  Y for recurring.  Anything other than Y is not recurring.
        );


        $PayPalRequestData = array(
            'DRTFields' => $DRTFields,
            'PaymentDetails' => $PaymentDetails,
        );

        $PayPalResult = $this->paypal_pro_lib->DoReferenceTransaction($PayPalRequestData);
        $PayPalResult['endpoint_url'] = $this->paypal_pro_lib->EndPointURL;

        $status = array();
        $status['status'] = 'FAIL';
        $status['message'] = '';
        $status['response'] = $PayPalResult;

        if (!$this->paypal_pro_lib->APICallSuccessful($PayPalResult['ACK'])) {
            //paypal error array looks like this:
            //   [L_ERRORCODE] => 81247
            //   [L_SHORTMESSAGE] => Invalid Parameter
            //   [L_LONGMESSAGE] => CreditCardType : Invalid Parameter
            //   [L_SEVERITYCODE] => Error

            $status['message'] = 'ERROR';
            $status['errors'] = $PayPalResult['ERRORS'][0];
            $status['transaction_id'] = 0;
        } else {
            $status['status'] = 'SUCCESS';
            $status['transaction_id'] = $PayPalResult['TRANSACTIONID'];
            $status['message'] = 'Successful reference transaction';
        }

        return $status;
    }

    private function process_auth($creditCardData = array())
    {
        $type = 'Authorization';
        $DPFields = array(
            'paymentaction' => $type, // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
            'ipaddress' => $_SERVER['REMOTE_ADDR'], // Required.  IP address of the payer's browser.
            'returnfmfdetails' => 0     // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
        );

        $CCDetails = array(
            'creditcardtype' => $creditCardData['cardType'], // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
            'acct' => $creditCardData['cardNumber'], // Required.  Credit card number.  No spaces or punctuation.
            'expdate' => $creditCardData['expMonth'] . $creditCardData['expYear'], // Required.  Credit card expiration date.  Format is MMYYYY
            'cvv2' => $creditCardData['csc'], // Requirements determined by your PayPal account settings.  Security digits for credit card.
            'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
            'issuenumber' => ''                                                     // Issue number of Maestro or Solo card.  Two numeric digits max.
        );


        $PayerName = array(
            'salutation' => '', // Payer's salutation.  20 char max.
            'firstname' => $creditCardData['firstName'], // Payer's first name.  25 char max.
            'middlename' => '', // Payer's middle name.  25 char max.
            'lastname' => $creditCardData['lastName'], // Payer's last name.  25 char max.
            'suffix' => ''     // Payer's suffix.  12 char max.
        );

        $PayerInfo = array(
            'email' => '', // Email address of payer.
            'payerid' => '', // Unique PayPal customer ID for payer.
            'payerstatus' => '', // Status of payer.  Values are verified or unverified
            'business' => ''     // Payer's business name.
        );

        $BillingAddress = array(
            'street' => $creditCardData['address'], // Required.  First street address.
            'street2' => '', // Second street address.
            'city' => $creditCardData['city'], // Required.  Name of City.
            'state' => $creditCardData['state'], // Required. Name of State or Province.
            'countrycode' => $this->country, // Required.  Country code.
            'zip' => $creditCardData['zipcode'], // Required.  Postal code of payer.
            'phonenum' => ''     // Phone Number of payer.  20 char max.
        );


        $PaymentDetails = array(
            'amt' => '1.00', // Required.  Total amount of order, including shipping, handling, and tax.
            'currencycode' => $this->currency, // Required.  Three-letter currency code.  Default is USD.
            'itemamt' => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
            'shippingamt' => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
            'insuranceamt' => '', // Total shipping insurance costs for this order.
            'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
            'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
            'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
            'desc' => '', // Description of the order the customer is purchasing.  127 char max.
            'custom' => '', // Free-form field for your own use.  256 char max.
            'invnum' => '', // Your own invoice or tracking number
            'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
            'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
            'recurring' => ''    // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
        );



        $PayPalRequestData = array(
            'DPFields' => $DPFields,
            'CCDetails' => $CCDetails,
            'PayerInfo' => $PayerInfo,
            'PayerName' => $PayerName,
            'PaymentDetails' => $PaymentDetails,
            'BillingAddress' => $BillingAddress,
        );

        $PayPalResult = $this->paypal_pro_lib->DoDirectPayment($PayPalRequestData);
        $PayPalResult['endpoint_url'] = $this->paypal_pro_lib->EndPointURL;
        
        //remove sensitive data before logging
        unset($PayPalResult['RAWREQUEST']);
        unset($PayPalResult['REQUESTDATA']['CVV2']);
        $PayPalResult['REQUESTDATA']['ACCT'] = substr($PayPalResult['REQUESTDATA']['ACCT'], -4);
        
        $status = array();
        $status['status'] = 'FAIL';
        $status['message'] = '';
        $status['response'] = $PayPalResult;

        if (!$this->paypal_pro_lib->APICallSuccessful($PayPalResult['ACK'])) {
            //paypal error array looks like this:
            //   [L_ERRORCODE] => 81247
            //   [L_SHORTMESSAGE] => Invalid Parameter
            //   [L_LONGMESSAGE] => CreditCardType : Invalid Parameter
            //   [L_SEVERITYCODE] => Error

            $status['message'] = 'ERROR';
            $status['errors'] = $PayPalResult['ERRORS'][0];
            $status['transaction_id'] = 0;
        } else {
            $status['status'] = 'SUCCESS';
            $status['transaction_id'] = $PayPalResult['TRANSACTIONID'];
            $status['message'] = 'Successful ' . $type;
        }

        return $status;
    }

}
