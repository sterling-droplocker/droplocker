<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Business;

class CyberSourceSA extends CyberSource
{

    const PROCESSOR = 'cybersource-sa';

    public $business_id = null;

    public $mode = 'live';
    public $silent = false;
    public $locale = 'en-us';
    public $accessKey = null;
    public $secretKey = null;
    public $profileID = null;
    public $terminalID = null;
    public $merchantName = null;
    public $DMprofileID = null;
    public $orgID = null;

    public function setTestMode()
    {
        $this->mode = 'test';
    }

    public function setLiveMode()
    {
        $this->mode = 'live';
    }

    public function __construct($options)
    {
        parent::__construct($options);

        $this->accessKey = get_business_meta($this->business_id, 'cybersource_accessKey');
        $this->secretKey = get_business_meta($this->business_id, 'cybersource_secretKey');
        $this->profileID = get_business_meta($this->business_id, 'cybersource_profileID');
        $this->silent    = get_business_meta($this->business_id, 'cybersource_silent', false);
        $this->locale    = get_business_meta($this->business_id, 'cybersource_locale', 'en-us');

        // used for decision manager
        $this->terminalID   = get_business_meta($this->business_id, 'cybersource_terminalID');
        $this->merchantName = get_business_meta($this->business_id, 'cybersource_merchantName');
        $this->DMprofileID  = get_business_meta($this->business_id, 'cybersource_DMprofileID');
        $this->orgID        = get_business_meta($this->business_id, 'cybersource_orgID');
    }

    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        // this is processing the post response we got from cybersource
        if (!empty($creditCard['cybersourceResponse'])) {
            return $this->processResponse($creditCard['cybersourceResponse'], $order_id, $customer_id, $business_id);
        }

        $customer = new Customer($customer_id);

        $amount = '10.00';

        $data = array(
            'access_key' => $this->accessKey,
            'profile_id' => $this->profileID,
            'locale' => $this->locale,
            'merchant_id' => $this->merchantID,

            'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
            'unsigned_field_names' => '',

            'transaction_type' => 'authorization,create_payment_token',
            'transaction_uuid' => uniqid(),
            'currency' => $this->currency,
            'amount' => $amount,
            'reference_number' => $order_id,
            'card_type' => $this->getCardType($creditCard),
            'card_number' => $creditCard['cardNumber'],
            'card_expiry_date' => "{$creditCard['expMonth']}-{$creditCard['expYear']}",
            'card_cvn' => $creditCard['csc'],
            'bill_to_forename' => $creditCard['firstName'],
            'bill_to_surname' => $creditCard['lastName'],
            'bill_to_email' => $customer->email,
            'bill_to_address_line1' => $creditCard['address'],
            'bill_to_address_city' => $creditCard['city'],
            'bill_to_address_state' => $creditCard['state'],
            'bill_to_address_postal_code' => $creditCard['zipcode'],
            'payment_method' => 'card',
            'consumer_id' => $customer_id,
            'customer_ip_address' => $_SERVER['REMOTE_ADDR'],
            'customer_cookies_accepted' => 'true',
        );

        if ($this->country) {
            $data['bill_to_address_country'] = $this->country;
        }

        if ($this->DMprofileID) {
            $data += array(
                'merchant_defined_data1' => $this->DMprofileID,
                'merchant_defined_data2' => $this->merchantID,
                'merchant_defined_data6' => $this->terminalID,
                'merchant_defined_data7' => $this->merchantName,
                'merchant_defined_data8' => current(explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE'])),
                'device_fingerprint_id' => $this->getSessionID(), // can also be deviceFingerprintID
            );
        }


        $this->sign($data);

        $url = $this->mode == 'live' ? 'https://secureacceptance.cybersource.com/pay' : 'https://testsecureacceptance.cybersource.com/pay';
        if ($this->silent) {
            $url = $this->mode == 'live' ? 'https://secureacceptance.cybersource.com/silent/pay' : 'https://testsecureacceptance.cybersource.com/silent/pay';
        }

        $form = form_hidden($data);

        $DMHtml = $this->getDMHtml();

        // render a form that will auto submit to cybersource
        echo "<html>
                <head>
                <title>Loading...</title>
                </head>
                <body>
                    $DMHtml
                    Loading... <form method='post' action='$url' id='cybersource_form'>$form<input type='submit' value='Authorize Card' id='cybersource_submit'></form>
                    <script>window.onload = function() { document.getElementById('cybersource_form').submit(); document.getElementById('cybersource_submit').setAttribute('style', 'display:none;'); };</script>
                    <!-- END -->
                </body>
               </html>";
        exit;
    }

    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        // This instanciates the SOAP client
        if ($this->mode == 'test') {
            parent::setTestMode();
        } else {
            parent::setLiveMode();
        }

        return parent::makeSale($amount, $order_id, $customer_id, $business_id);
    }

    /**
     * Process the Cybersource response
     *
     * @param array $reply
     * @param unknown $order_id
     * @param unknown $customer_id
     * @param unknown $business_id
     * @return Response
     */
    protected function processResponse($reply, $order_id, $customer_id, $business_id)
    {
        if ($reply['decision'] == 'ACCEPT') {
            $status = 'SUCCESS';
            $refNumber = $reply['payment_token'];
        } else {
            $status = 'FAIL';
            $refNumber = '';
        }

        $response = new Response(
            $status,
            $reply['reason_code'],
            $refNumber,
            $reply['decision'],
            '',
            '',
            '',
            'auth',
            $customer_id,
            $business_id,
            self::PROCESSOR,
            0,
            '',
            $order_id
        );

        return $response;
    }

    /**
     * Sings a Secure Aceptance request
     *
     * The input array will be modified and the signature appended
     *
     * @param array $params
     * @return string
     */
    protected function sign(&$params)
    {
        if (!isset($params['signed_field_names'])) {
            $params['signed_field_names'] = '';
            $params['signed_field_names'] = implode(',', array_keys($params));
        }

        $signedFieldNames = explode(",", $params["signed_field_names"]);
        $dataToSign = array();
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . "=" . $params[$field];
        }

        $data = implode(",",$dataToSign);
        $signature = base64_encode(hash_hmac('sha256', $data, $this->secretKey, true));
        $params['signature'] = $signature;

        return $signature;
    }

    /**
     * Gets the URL for image fingerprint
     *
     * If force is true, returns always the remote url
     *
     * @param bool $force
     * @return string
     */
    protected function getImageFingerprintUrl($force = false)
    {
        $url = 'https://h.online-metrix.net/fp/clear.png';

        if ($force || $this->mode == 'live') {
            $url = '/images/cs_fp/clear.png';
        }

        return sprintf('%s?org_id=%s&session_id=%s',
            $url, $this->orgID, $this->merchantID . $this->getSessionID());
    }

    /**
     * Gets the tag for image fingerprint
     *
     * If force is true, returns always the remote url
     *
     * @param bool $force
     * @return string
     */
    protected function getImageFingerprintTag($force = false)
    {
        $url = $this->getImageFingerprintUrl($force);

        return sprintf('
            <p style="background:url(%s&amp;m=1)"></p>
            <img src="%s&amp;m=2" alt="">',
            $url, $url);
    }

    /**
     * Gets the URL for script fingerprint
     *
     * If force is true, returns always the remote url
     *
     * @param bool $force
     * @return string
     */
    protected function getScriptFingerprintUrl($force = false)
    {
        $url = 'https://h.online-metrix.net/fp/check.js';
        if ($force || $this->mode == 'live') {
            $url = '/scripts/cs_fp/check.js';
        }

        return sprintf('%s?org_id=%s&session_id=%s',
            $url, $this->orgID, $this->merchantID . $this->getSessionID());
    }

    /**
     * Gets the tag for script fingerprint
     *
     * If force is true, returns always the remote url
     *
     * @param bool $force
     * @return string
     */
    protected function getScriptFingerprintTag($force = false)
    {
        $url = $this->getScriptFingerprintUrl($force);

        return sprintf('<script src="%s" type="text/javascript"></script>', $url);
    }

    /**
     * Gets the URL for flash fingerprint
     *
     * If force is true, returns always the remote url
     *
     * @param bool $force
     * @return string
     */
    protected function getFlashFingerprintUrl($force = false)
    {
        $url = 'https://h.online-metrix.net/fp/fp.swf';
        if ($force || $this->mode == 'live') {
            $url = '/flash/cs_fp/fp.swf';
        }

        return sprintf('%s?org_id=%s&session_id=%s',
            $url, $this->orgID, $this->merchantID . $this->getSessionID());
    }

    /**
     * Gets the flash tag for fingerprint
     *
     * If force is true, returns always the remote url
     *
     * @param string $force
     * @return string
     */
    protected function getFlashFingerprintTag($force = false)
    {
        $url = $this->getFlashFingerprintUrl($force);

        return sprintf('
        <object type="application/x-shockwave-flash" data="%s" width="1" height="1" id="thm_fp">
            <param name="movie" value="%s" />
            <div></div>
        </object>', $url, $url);
    }

    /**
     * Gets the session id used for fingerprinting
     *
     * @return string
     */
    protected function getSessionID()
    {
        $CI = get_instance();
        $session_id = $CI->session->userdata('cybersource_id');
        if (!$session_id) {
            $session_id = md5(uniqid(null));
            $CI->session->set_userdata('cybersource_id', $session_id);
        }

        return $session_id;
    }

    /**
     * Returns the Decision Manager code
     *
     * If force is true, returns always the remote url
     */
    protected function getDMHtml($force = false)
    {
        if (!$this->DMprofileID) {
            return '';
        }

        $out = '';
        $out .= $this->getImageFingerprintTag($force);
        $out .= $this->getFlashFingerprintTag($force);
        $out .= $this->getScriptFingerprintTag($force);

        return $out;
    }

    public function supportsMobileAuth()
    {
        return false;
    }
}
