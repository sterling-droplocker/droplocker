<?php
namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;

class PayboxDirect extends AbstractProcessor
{

    CONST PROCESSOR = "payboxdirect";

    /**
     * Paybox Direct Api
     *
     * @var \Paybox\Direct\Api
     */
    public $api = null;

    public function setTestMode()
    {
        $this->api->sandbox = true;
    }

    public function setLiveMode()
    {
        $this->api->sandbox = false;
    }

    public function __construct($params = array())
    {
        $CI = get_instance();

        $this->business_id = (! empty($params['business_id'])) ? $params['business_id'] : $CI->business_id;
        if (empty($this->business_id)) {
            throw new \Exception("Missing Business ID");
        }

        $site = get_business_meta($this->business_id, 'paybox_site');
        $rang = get_business_meta($this->business_id, 'paybox_rang');
        $cle = get_business_meta($this->business_id, 'paybox_cle');
        $isPlus = get_business_meta($this->business_id, 'paybox_is_plus');
        $sandbox = DEV ? true : false;

        $this->api = new \Paybox\Direct\Api($site, $rang, $cle, $isPlus, $sandbox);
    }

    /**
     * Authorizes a card for use.
     *
     * @param array $card
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $amount  = '1.00';

        if ($this->api->isPlus) {
            $res = $this->authorizeCardDirectPlus($creditCard, $order_id, $customer_id, $business_id, $amount);
        } else {
            $res = $this->authorizeCardDirect($creditCard, $order_id, $customer_id, $business_id, $amount);
        }

        $status = $res['status'] == 'success' ? 'SUCCESS' : 'FAIL';
        $reference = $this->makeReference($res, $creditCard['csc']);

        $response = new Response($status,
            $res['code'],
            $reference, // save for future payments
            $res['message'],
            '',
            '',
            '',
            'auth',
            $customer_id,
            $business_id,
            self::PROCESSOR,
            $amount,
            '',
            $order_id
        );

        return $response;
    }

    /**
     * Authorizes a Card using Paybox Direct Plus API
     * @param Array $creditCard credit card info
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @param float $amount
     *
     * @return Array Paybox API response
     */
    public function authorizeCardDirectPlus($creditCard, $order_id, $customer_id, $business_id, $amount)
    {
        $reference = $this->getReference($customer_id, $business_id);

        // Already registered, use previus ref to get new authorization
        if ($reference) {
            return $this->authorizeByReference($reference, $creditCard, $order_id, $customer_id, $business_id, $amount);
        }

        return $this->registerAndAuthorize($creditCard, $order_id, $customer_id, $business_id, $amount);
    }

    /**
     * Searches for a reference in the users CreditCard
     * @param int $customer_id
     * @param int $business_id
     */
    public function getReference($customer_id, $business_id)
    {
        $creditCard = CreditCard::search(compact('customer_id', 'business_id'));
        return $creditCard ? $creditCard->payer_id : null;
    }

    /**
     * Registers a Card and athorizes it against Paybox Direct Plus
     *
     * @param Array $creditCard credit card info
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @param string $amount
     *
     * @return Array Paybox API response
     */
    public function registerAndAuthorize($creditCard, $order_id, $customer_id, $business_id, $amount)
    {
        $res = $this->api->registerSuscriber($creditCard['cardNumber'],
            $creditCard['csc'],
            $creditCard['expMonth'],
            $creditCard['expYear'],
            $amount,
            $customer_id,
            "Authorize card: customer: $customer_id, order: $order_id"
        );

        if ($res['status'] == 'error' && $res['code'] == \Paybox\Direct\Api::RESPONSE_SUSCRIBER_ALREADY_EXISTS) {
            $res = $this->api->updateSuscriber($creditCard['cardNumber'],
                $creditCard['csc'],
                $creditCard['expMonth'],
                $creditCard['expYear'],
                $amount,
                $customer_id,
                "Authorize card: customer: $customer_id, order: $order_id"
            );
        };

        return $res;
    }

    /**
     * Authorizes using a previus reference from Paybox Direct Plus
     *
     * @param string $reference
     * @param Array $creditCard credit card info
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @param float $amount
     *
     * @return Array Paybox API response
     */
    public function authorizeByReference($reference, $creditCard, $order_id, $customer_id, $business_id, $amount)
    {
        list ($call_number, $transaction, $ref_token, $cvv) = explode('|', $reference);

        return $this->api->authorizeSuscriber($ref_token,
            $cvv,
            $creditCard['expMonth'],
            $creditCard['expYear'],
            $amount,
            $customer_id,
            "Authorize card: customer: $customer_id, order: $order_id"
        );
    }

    /**
     * Authorizes using Paybox Direct (not plus)
     * @param Array $creditCard credit card info
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @param float $amount
     *
     * @return Array Paybox API response
     */
    public function authorizeCardDirect($creditCard, $order_id, $customer_id, $business_id, $amount)
    {
        return $this->api->authorize($creditCard['cardNumber'],
            $creditCard['csc'],
            $creditCard['expMonth'],
            $creditCard['expYear'],
            $amount,
            "Authorize card: customer: $customer_id, order: $order_id"
        );
    }

    /**
     * Makes a payment for a product or service
     *
     * @param decimal $amount
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        // check for comma in amount
        $amount = str_replace(',', '.', $amount);

        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(compact('customer_id', 'business_id'));
        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        list ($transaction, $call_number, $card, $cvv) = explode('|', $creditCard->payer_id);

        if ($this->api->isPlus) {
            $reference = $creditCard->payer_id;

            $creditCardData = array(
                'csc' => $cvv,
                'expMonth' => $creditCard->expMo,
                'expYear' => $creditCard->expYr,
            );

            // Get a new Authorization using the reference
            $res = $this->authorizeByReference($reference, $creditCardData, $order_id, $customer_id, $business_id, $amount);
            if ($res['status'] == 'success') {
                // Generate a new reference from the response
                $reference = $this->makeReference($res, $cvv);

                list ($call_number, $transaction, $ref_token, $cvv) = explode('|', $reference);

                // Debit the suscriber using the new created ref
                $res = $this->api->debitSuscriber($transaction, $call_number, $ref_token, $amount, $customer_id);
            }

        } else {
            // This requires that the stored reference is authorized
            $res = $this->api->debit($transaction, $call_number, $amount, "Pay Order, customer: $customer_id, order: $order_id");
        }

        $status = $res['status'] == 'success' ? 'SUCCESS' : 'FAIL';
        $reference = $this->makeReference($res, $cvv);

        if ($res['status'] == 'success') {
            $creditCard->payer_id = $reference;
            $creditCard->save();
        }

        $response = new Response($status,
            $res['code'],
            $reference,
            $res['message'] . $this->getCreditCardSummary($creditCard),
            '',
            '',
            '',
            'sale',
            $customer_id,
            $business_id,
            self::PROCESSOR,
            $amount,
            '',
            $order_id
        );

        return $response;
    }

    /**
     * Generates a reference from an API response array
     *
     * @param Array $res API successfull response
     * @param string $cvv The card CVV
     * @return string
     */
    protected function makeReference($res, $cvv)
    {
        if ($res['status'] != 'success') {
            return 'FAIL';
        }

        $reference = $res['call_number'] . '|' . $res['transaction'] . '|';
        if (isset($res['ref_token'])) {
            $reference .= $res['ref_token'];
        } else {
            // API is not plus, need to store CC number.... won't do that
        }

        $reference .= '|' . $cvv; // We need this for next payments

        return $reference;
    }
}