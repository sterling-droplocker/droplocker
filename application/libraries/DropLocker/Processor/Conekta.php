<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;

require_once APPPATH."third_party/conekta/Conekta.php";

class Conekta extends AbstractProcessor
{
    public $business_id;
    public $currency = 'USD';
    protected $private_key;
    protected $public_key;

    const PROCESSOR = "conekta";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the Conekta credentials');
        }

        $this->business_id = $params['business_id'];
        $this->currency = get_business_meta($this->business_id,'conekta_currency', 'USD');

        if (DEV || get_business_meta($this->business_id, 'conekta_mode') == 'test') {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }

        \Conekta\Conekta::setApiKey($this->private_key);
        \Conekta\Conekta::setApiVersion("2.0.0");
    }

    public function setTestMode()
    {
        $this->private_key = get_business_meta($this->business_id, 'conekta_sandbox_private_key');
        $this->public_key = get_business_meta($this->business_id, 'conekta_sandbox_public_key');
    }

    public function setLiveMode()
    {
        $this->private_key = get_business_meta($this->business_id, 'conekta_private_key');
        $this->public_key  = get_business_meta($this->business_id, 'conekta_public_key');
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace(',', '.', $amount);

        //Conekta Api needs amount to be converted to cents!
        $converted_amount = $amount * 100;

        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        try {
            $payer_details = explode("|", $creditCard->payer_id);

            $params = array(
                  "line_items" => array(
                        array(
                          "name" => "Laundry",
                          "unit_price" => $converted_amount,
                          "quantity" => 1
                        )//first line_item
                      ), //line_items
                      "currency" => $this->currency,
                      "customer_info" => array(
                        "customer_id" => $payer_details[0]
                      ), //customer_info
                      "metadata" => array("reference" => "DL Transaction"),
                      "charges" => array(
                          array(
                              "payment_method" => array(
                                "payment_source_id" => $payer_details[1],
                                      "type" => "card"
                              )//payment_method
                          ) //first charge
                      ), //charges
                      "shipping_lines" => array(array(
                        'amount' => 0,
                        'tracking_number' => $order_id,
                        'carrier' => 'Laundry',
                        'method'  => 'shippment_'.$order_id
                      )),
                      "shipping_contact" => array(
                       'receiver' => $customer->firstName.' '.$customer->lastName,
                       'phone' => empty($creditCard->phone_number)?$customer->phone:$creditCard->phone_number,
                       'between_streets' => $customer->address1,
                       'address' => array(
                          'street1' => $customer->address1,
                          'city' => empty($creditCard->city)?$customer->city:$creditCard->city,
                          'state' => empty($creditCard->state)?$customer->state:$creditCard->state,
                          'country' => 'MX',
                          'postal_code' => empty($creditCard->zip)?$customer->zip:$creditCard->zip
                        ))
            );

            $result = \Conekta\Order::create($params);

            $status         = 'SUCCESS';
            $transaction_id = $result->id;
            $message        = 'Successful Transaction';
            $result_code    = 0;
        } catch (\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->getMessage();
            $result_code    = $e->getCode();
        }
        // Response will log this transaction in the transaction table
        $response = new Response(
            $status, 
            $result_code, 
            $transaction_id, 
            $message . $this->getCreditCardSummary($creditCard), 
            '', '', '', 
            'Sale', 
            $customer_id, 
            $business_id, 
            self::PROCESSOR, 
            $amount, 
            '', 
            $order_id);

        return $response;
    }

    /**
     * Authorizes a card and stores the card token
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $this->business_id = $business_id;
        $customer          = new Customer($customer_id);

        $name = $creditCard['firstName'].' '.$creditCard['lastName'];

        $params["card"] = array(
            "name" => $name,
            "number" => $creditCard["cardNumber"],
            "cvc" => $creditCard["csc"],
            "exp_month" => $creditCard["expMonth"],
            "exp_year" => $creditCard["expYear"]
        );

        try {
            $token  = $this->create_token($params);
            if ($token->object == "error") throw new \Exception($token->message);            

            $customer_card =  \Conekta\Customer::create(
                array(
                  "name" => $name,
                  "email" => $customer->email,
                  "phone" => empty($creditCard['phone_number'])?$customer->phone:$creditCard['phone_number'],
                  "payment_sources" => array(
                    array(
                        "type" => "card",
                        "token_id" => $token->id
                    )
                  )//payment_sources
                )//customer
            );

            $status         = 'SUCCESS';
            $transaction_id = $customer_card->id."|".$customer_card->default_payment_source_id;
            $result_code    = 0;
            $message        = 'Authorization Success';
        } catch(\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->getMessage();
            $result_code    = $e->getCode();
        }

        // Response will log this transaction in the transaction table
        $response = new Response(
            $status, 
            $result_code, 
            $transaction_id, 
            $message, 
            '', 
            '', 
            '', 
            'auth', 
            $customer_id, 
            $business_id, 
            self::PROCESSOR, 
            1, 
            '', 
            $order_id);
        return $response;
    }

    /*
    *  Emulates Conekta javascript client. This allow us to retrieve card token
    *    without adding tons of javascript in frontend pages.
    *     
    *     It is based on PASO 1 (STEP 1) from this page: 
    *         https://developers.conekta.com/tutorials/card?language=php
    *
    *   dont try to use \Conekta\Token::create it doenst work anymore
    */
    protected function create_token($data)
    {
        $url = \Conekta\Conekta::$apiBase . "/tokens";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array(
            "Accept-Encoding: gzip, deflate, br",
            "Accept-Language: es",
            "Authorization: Basic " . base64_encode($this->public_key),
            "Conekta-Client-User-Agent: {\"agent\":\"Conekta JavascriptBindings/0.3.0\"}",
            "Pragma: no-cache",
            "Raisehtmlerror: false",
            "Content-Type: application/json",
            "Accept: application/vnd.conekta-v0.3.0+json",
            "Cache-Control: no-cache",
            "Authority: api.conekta.io"
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error = new \stdClass;
            $error->object = "error";
            $error->message = curl_error($ch);
            return $error;
        }
        curl_close ($ch);

        $result = json_decode($result);

        return $result;
    }
}