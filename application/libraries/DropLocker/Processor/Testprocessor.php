<?php
/*
* This is a test processor that always return SUCCESS
*/
namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;


class Testprocessor extends AbstractProcessor
{
    public $business_id;
    protected $api_endpoint;
    protected $merchant_id;
    protected $merchant_pass;
    protected $is_sandbox = false;
    protected $CI;

    const PROCESSOR = "testprocessor";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the Sony payment credentials');
        }

        $this->business_id = $params['business_id'];

        $this->CI = get_instance();
        $this->CI->load->library('encrypt');
    }

    public function setTestMode()
    {
      return true;
    }

    public function setLiveMode()
    {
      return true;
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace(',', '.', $amount); 

        $customer = new Customer($customer_id);

        /*
        if ($customer->autoPay == 0) {
          throw new \Exception('Customer is not on autoPay');
        }*/

        $creditCard = CreditCard::search(array(
          'customer_id' => $customer_id,
          'business_id' => $business_id
          ));


        $auth = $this->authorizeTransaction($creditCard, $amount);

        if (!$auth) {
          throw new \Exception("Transaction was not authorized");
        }

        $params = array(
          'OperateId'       => '1Capture',
          'CardNo'          => $creditCard->cardNumber,
          'CardExp'         => substr($creditCard->expYr, -2).$creditCard->expMo,
          'ProcessId'       => $auth['ProcessId'],
          'ProcessPass'     => $auth['ProcessPass'],
          'SalesDate'       => date("Ymd"),
          'Amount'          => $amount,
          'PayType'         => '01'
          );

        $status         = 'SUCCESS';
        $transaction_id = md5(mktime());
        $message        = 'Successful Transaction';
        $result_code    = $result["ResponseCd"];

        // Response will log this transaction in the transaction table
        $response = new Response(
          $status, 
          $result_code, 
          $transaction_id, 
          $message . $this->getCreditCardSummary($creditCard), 
          '', '', '', 
          'Sale', 
          $customer_id, 
          $business_id, 
          self::PROCESSOR, 
          $amount, 
          '', 
          $order_id);

        return $response;
      }


    protected function authorizeTransaction($creditCard, $amount)
    {
      return true;
    }

    /**
     * Authorizes a card and stores the card token
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
      $this->business_id = $business_id;

      $params = array(
        'OperateId' => '1Check',
        'CardNo' => $creditCard['cardNumber'],
        'CardExp' => substr($creditCard['expYear'], -2).$creditCard['expMonth']
      );

      /*
      $result = Array ( 
        [TransactionId] => 000008192304 
        [TransactionDate] => 20180310 
        [OperateId] => 1Check 
        [ProcessId] => b6df8d71895dcc01e6342c61d31895a0 
        [ProcessPass] => 33b2236575025ae5049015a8c26c43f7 
        [ResponseCd] => OK 
        [CompanyCd] => Y0 
        [ApproveNo] => 0001000 
      )
      */
      
      $status         = 'SUCCESS';
      $transaction_id = $result["TransactionId"];
      $result_code    = $result["ResponseCd"];
      $message        = 'Authorization Success';

      // Response will log this transaction in the transaction table
      $response = new Response(
        $status, 
        $result_code, 
        $transaction_id,  //refNumber
        $message, 
        '', 
        '', 
        '', 
        'auth', 
        $customer_id, 
        $business_id, 
        self::PROCESSOR, 
        1, 
        '', 
        $order_id);
      return $response;
    }

}
