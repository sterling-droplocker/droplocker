<?php

namespace  DropLocker\Processor;

use App\Libraries\DroplockerObjects\Order;

class Verisign extends AbstractProcessor
{
    public $user = '';
    public $vendor = '';
    public $partner = '';
    public $password = '';
    public $submiturl = null;
    public $business_id;
    public $currency = 'USD';
    public $country = 'US';

    const PROCESSOR = 'verisign';

    public function __construct($params = array())
    {
        if (empty($params['business_id'])) {
            throw new \Exception("Missing Business ID");
        }

        $this->business_id = $params['business_id'];

        $this->user     = get_business_meta($this->business_id, 'verisign_user');
        $this->vendor   = get_business_meta($this->business_id, 'verisign_vendor');
        $this->partner  = get_business_meta($this->business_id, 'verisign_partner');
        $this->password = get_business_meta($this->business_id, 'verisign_password');
        $this->currency = get_business_meta($this->business_id, 'verisign_currency', 'USD');
        $this->country  = get_business_meta($this->business_id, 'verisign_country', 'US');

        if (DEV) {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    public function setTestMode()
    {
        $this->submiturl = 'https://pilot-payflowpro.paypal.com';
    }

    public function setLiveMode()
    {
        $this->submiturl = 'https://payflowpro.paypal.com';
    }

    /**
     * authorizes a credit card
     *
     * @see \DropLocker\Processor\AbstractProcessor::authorizeCard()
     */
    public function authorizeCard($card, $order_id, $customer_id, $business_id)
    {
        $CI = get_instance();

        $this->customer_id = $customer_id;
        $this->business_id = $business_id;

        $params = array(
            'USER'       => $this->user,
            'VENDOR'     => $this->vendor,
            'PARTNER'    => $this->partner,
            'PWD'        => $this->password,
            'TENDER'     => 'C',  // C - Direct Payment using credit card
            'TRXTYPE'    => 'A',  // A - Authorization, S - Sale
            'ACCT'       => $card['cardNumber'],
            'CVV2'       => $card['csc'],
            'EXPDATE'    => str_pad($card['expMonth'], 2, '0', STR_PAD_LEFT).str_pad(substr($card['expYear'], -2), 2, '0', STR_PAD_LEFT),
            'ACCTTYPE'   => $card['cardType'],
            'AMT'        => '0.01',
            'CURRENCY'   => $this->currency,
            'NAMEONCARD' => $card['nameOnCard'],
            'STREET'     => $card['address'],
            'CITY'       => $card['city'],
            'STATE'      => $card['state'],
            'ZIP'        => $card['zip'],
            'COUNTRY'    => $this->country,
            'COMMENT1'   => "Authorizing Card",
            'INVNUM'     => $order_id,
            'ORDERDESC'  => "Authorizing Card",
            'VERBOSITY'  => 'MEDIUM',
        );

        return $this->runTransaction($params, 'auth', $customer_id, $order_id);
    }

    /**
     * credits a credit card
     * @param string $origid
     * @param decimal $amount
     */
    public function credit($origid, $amount)
    {
        $params = array(
            'USER'       => $this->user,
            'VENDOR'     => $this->vendor,
            'PARTNER'    => $this->partner,
            'PWD'        => $this->password,
            'TENDER'     => 'C',  // C - Direct Payment using credit card
            'TRXTYPE'    => 'C',  // A - Authorization, S - Sale
            'ORIGID'     => $origid,
            'AMT'        => $amount,
            'CURRENCY'   => $this->currency,
            'COMMENT1'   => 'Credit for transaction: '.$origid,
        );

        return $this->runTransaction($params, 'credit', null, null);
    }

    /**
     * makes a sale
     *
     * @see AbstractProcessor::makeSale()
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        if (empty($customer_id)) {
            throw new \Exception("Missing customer ID");
        }

        $pnref = $this->getMaxPnref($customer_id, $business_id);
        $params = array(
            'USER'       => $this->user,
            'VENDOR'     => $this->vendor,
            'PARTNER'    => $this->partner,
            'PWD'        => $this->password,
            'TENDER'     => 'C',  // C - Direct Payment using credit card
            'TRXTYPE'    => 'S',  // A - Authorization, S - Sale
            'ORIGID'     => $pnref,
            'AMT'        => $amount,
            'CURRENCY'   => $this->currency,
            'COMMENT1'   => 'Order '.$order_id,
        );


        return $this->runTransaction($params, 'sale', $customer_id, $order_id);
    }

    /**
     * Gets the most recent pnref from the transaction table for this customer.
     *
     * @param int $customer_id
     * @param int $business_id
     */
    protected function getMaxPnref($customer_id, $business_id)
    {
        // In devel use a testing payer ID
        if (DEV) {
            //DEVPAYERID = V24P1EC9F781
            return DEVPAYERID;
        }

        $CI = get_instance();
        $CI->load->model('transaction_model');
        $maxPnref = $CI->transaction_model->getMaxPnref($customer_id, $business_id);

        return $maxPnref;
    }

    /**
     * Communicates with Versign and performs a transaction
     *
     * @param array $params
     * @param string $type
     * @param int $customer_id
     * @param int $order_id
     *
     * @return \DropLocker\Processor\Response
     */
    protected function runTransaction($params, $type, $customer_id, $order_id)
    {
        $res = $this->post($params);
        $status = $res['RESULT'] == 0 ? "SUCCESS" : "FAIL";

        $response = new Response(
            $status,
            $res['RESULT'],
            $res['PNREF'],
            $res['RESPMSG'],
            $res['AVSADDR'],
            $res['AVSZIP'],
            isset($res['CVV2MATCH']) ? $res['CVV2MATCH'] : '',
            $type,
            $customer_id,
            $this->business_id,
            self::PROCESSOR,
            $params['AMT'],
            $res['AUTHCODE'],
            $order_id
        );

        return $response;
    }

    /**
     * Does a post request and parses the response
     *
     * @param array $data
     * @return array
     */
    protected function post($data)
    {
        $data = $this->getQueryString($data);

        // get data ready for API
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'curl';
        $headers[] = "Content-Type: text/namevalue"; //or text/xml if using XMLPay.
        $headers[] = "Content-Length : " . strlen ($data);  // Length of data to be passed
        $headers[] = "X-VPS-Timeout: 45";
        $headers[] = "X-VPS-Request-ID:" . microtime(true);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->submiturl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_HEADER, 1);                // tells curl to include headers in response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 90);              // times out after 90 secs
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);        // this line makes it work under https
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //adding POST data
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);        //verifies ssl certificate
        curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);       //forces closing of connection when done
        curl_setopt($ch, CURLOPT_POST, 1); 					 //data sent as POST
        $result = curl_exec($ch);
        curl_close($ch);

        $result = strstr($result, "RESULT");
        parse_str($result, $res);

        return $res;
    }

    /**
     * Encodes an array as a NVP string
     *
     * @param array $data
     * @return string
     */
    protected function getQueryString($data)
    {
        $query = array();
        foreach ($data as $key => $value) {
            $query[] = $key.'['.strlen($value).']='.$value;
        }

        return implode('&', $query);
    }
}
