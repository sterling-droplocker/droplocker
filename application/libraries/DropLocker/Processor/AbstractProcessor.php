<?php

namespace  DropLocker\Processor;

/**
 *
 * Defines the methods that are needed to work with credit cards
 * @author matt
 *
 */
abstract class AbstractProcessor
{

    /**
     * Consturcts the processor
     *
     * $params is an array with the following keys
     * - business_id: int
     *
     * @param array $params
     */
    abstract public function __construct($params);

    /**
     * Authorizes a card for use.
     *
     * @param array $card Can contain the folloiwng keys
     *  cardNumber
     *  expYear
     *  expMonth
     *  firstName
     *  lastName
     *  state
     *  zipcode
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     *
     * @return Response
     */
    abstract public function authorizeCard($creditCard, $order_id, $customer_id, $business_id);

    /**
     * Makes a payment for a product or service
     *
     * @param decimal $amount
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     *
     * @return Response
     */
    abstract public function makeSale($amount, $order_id, $customer_id, $business_id);

    /**
     * Credits a customer ... ie, refund
     *
     * @param decimal $amount
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     */
    //abstract protected function credit($amount, $customer_id, $business_id, $order_id = '');

    /**
     * Sets the processor in test mode
     */
    abstract public function setTestMode();

    /**
     * Sets the processor in live mode
     */
    abstract public function setLiveMode();

    /**
     * Retuns the processor name
     * @return string
     */
    public function getProcessor()
    {
        $class = get_class($this);
        return $class::PROCESSOR;
    }

    /**
     * Returns true if the processor can be used in the mobile app
     * @return boolean
     */
    public function supportsMobileAuth()
    {
        return true;
    }
    
    public function getCreditCardSummary($cc)
    {
        return ". {$cc->cardType} $cc->cardNumber";
    }
}
