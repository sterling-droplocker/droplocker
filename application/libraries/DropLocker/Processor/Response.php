<?php

namespace  DropLocker\Processor;

/**
 *
 * Class for handling processor responses
 * Mainly used for transaction logging.
 *
 * @author matt
 *
 */

class Response
{
    private $status;
    private $amount = 0;
    // verisign = pnref, paypro = payer_id
    private $refNumber;
    private $message;
    private $resultCode;
    private $authcode;
    private $streetMatch; //address verification response
    private $zipMatch; //zipcode verification
    //private $ivas; //not sure what this is?
    private $cvvMatch;
    private $type;
    private $customer_id;
    private $business_id;
    private $order_id;
    private $response;
    private $processor;
    private $employee_id;
    private $dump;
    private $CI;

    /**
     *
     * @param array $responseArray
     */
    function __construct($status, $result, $refNumber, $message, $streetMatch, $zipMatch, $ccvMatch, $type, $customer_id, $business_id, $processor, $amount, $authCode = '', $order_id = '', $dump = '')
    {

        if (!is_numeric($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' is required.");
        }
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' is required.");
        }
        if (!is_numeric($order_id)) {
            //throw new Exception("Missing Order ID");
        }




        $this->CI = get_instance();
        $this->status = $status;
        $this->resultCode = $result;
        $this->refNumber = $refNumber;
        $this->message = $message;
        $this->streetMatch = $streetMatch;
        $this->zipMatch = $zipMatch;
        $this->cvvMatch = $ccvMatch;
        $this->type = $type;
        $this->customer_id = $customer_id;
        $this->business_id = $business_id;
        $this->amount = $amount;
        $this->authCode = $authCode;
        $this->order_id = $order_id;
        $this->processor = $processor;
        $this->dump = $dump;



        //log the transaction
        $this->store();
    }


    /**
     * Stores the response into the database
     * @return array (status, insert_id)
     * on fail returns boolean false;
     */
    public function store()
    {
        /*
        if ($this->refNumber=='' || $this->refNumber==null) {
           //throw new Exception("Reference number was not returned from the processor");
           $this->refNumber = "N/A";
           $data['order_id'] = $this->order_id;
           $data['customer_id'] = $this->customer_id;
           $data['amount'] = $this->amount;
           send_admin_notification('Reference not returned from the processor for order ['.$this->order_id.']', "During capture of payments the response from the processor produced an error or did not get returned. In order to continue processing, we inserted the responce code as 0. <br>Please check your transaction records at the processor<br>".print_r($data, 1));
           $this->resultCode = 0;
        }
        */

        if ($this->refNumber=='' || $this->refNumber==null) {
            //throw new Exception("Reference number was not returned from the processor");
            $this->refNumber = "N/A";
        }

        $this->CI->load->model('transaction_model');
        $this->CI->transaction_model->type = $this->type;
        $this->CI->transaction_model->pnref = $this->refNumber;
        $this->CI->transaction_model->customer_id = $this->customer_id;
        $this->CI->transaction_model->business_id = $this->business_id;
        $this->CI->transaction_model->resultCode = $this->resultCode;
        $this->CI->transaction_model->message = $this->message;
        $this->CI->transaction_model->zipMatch = $this->zipMatch;
        $this->CI->transaction_model->streetMatch = $this->streetMatch;
        $this->CI->transaction_model->order_id = $this->order_id;
        $this->CI->transaction_model->amount = $this->amount;
        $this->CI->transaction_model->cscMatch = $this->cvvMatch;
        $this->CI->transaction_model->cscMatch = $this->cvvMatch;
        $this->CI->transaction_model->order_id = $this->order_id;
        $this->CI->transaction_model->processor = $this->processor;
        $this->CI->transaction_model->dump = $this->dump;

        $buser_id = $this->CI->session->userdata('buser_id');
        if (!empty($buser_id)) {
            $this->CI->transaction_model->employee_id = get_employee_id($this->CI->session->userdata('buser_id'));
        }

        if ($insert_id = $this->CI->transaction_model->insert()) {
            return array('status'=>'success', 'insert_id'=>$insert_id);
        } else {
            return false;
        }

    }

    public function get_status()
    {
        return $this->status;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function get_refNumber()
    {
        return $this->refNumber;
    }

    public function get_resultCode()
    {
        return $this->resultCode;
    }

    public function get_amount()
    {
        return $this->amount;
    }

}
