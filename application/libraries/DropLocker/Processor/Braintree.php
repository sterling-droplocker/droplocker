<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;

require_once APPPATH."third_party/braintree/Braintree.php";

class Braintree extends AbstractProcessor
{
    public $business_id;
    public $currency = 'USD';
    protected $merchant_id;
    protected $private_key;
    protected $public_key;

    const PROCESSOR = "braintree";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the Conekta credentials');
        }

        $this->business_id = $params['business_id'];
        $this->currency = get_business_meta($this->business_id,'braintree_currency', 'USD');

        if (DEV || get_business_meta($this->business_id, 'braintree_mode') == 'test') {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }

       
        \Braintree\Configuration::merchantId($this->merchant_id);
        \Braintree\Configuration::publicKey($this->private_key);
        \Braintree\Configuration::privateKey($this->public_key);
        $this->clientToken = \Braintree\ClientToken::generate();
    }

    public function setTestMode()
    {
        \Braintree\Configuration::environment('sandbox');
        $this->merchant_id = get_business_meta($this->business_id, 'braintree_sandbox_merchant_id');
        $this->private_key = get_business_meta($this->business_id, 'braintree_sandbox_private_key');
        $this->public_key = get_business_meta($this->business_id, 'braintree_sandbox_public_key');
    }

    public function setLiveMode()
    {
        \Braintree\Configuration::environment('production');
        $this->merchant_id = get_business_meta($this->business_id, 'braintree_merchant_id');
        $this->private_key = get_business_meta($this->business_id, 'braintree_private_key');
        $this->public_key  = get_business_meta($this->business_id, 'braintree_public_key');
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace(',', '.', $amount);

        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        try {
            $payer_details = explode("|", $creditCard->payer_id);

            $params = array(
              "customerId" => $payer_details[0],
              "amount" => $amount,
              "orderId" => $order_id,
              "options" => array(
                  'submitForSettlement' => True
                ),
            );

            $result = \Braintree\Transaction::sale($params);

            if (!$result->success) {
                $errors = "";
                foreach($result->errors->deepAll() AS $error) {
                    $errors .= $error->message . "<br>";
                    throw new \Exception($errors, 0);
                }
            }

            $status         = 'SUCCESS';
            $transaction_id = $result->transaction->id;
            $message        = 'Successful Transaction';
            $result_code    = 0;
        } catch (\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->getMessage();
            $result_code    = $e->getCode();
        }
        // Response will log this transaction in the transaction table
        $response = new Response(
            $status, 
            $result_code, 
            $transaction_id, 
            $message . $this->getCreditCardSummary($creditCard), 
            '', '', '', 
            'Sale', 
            $customer_id, 
            $business_id, 
            self::PROCESSOR, 
            $amount, 
            '', 
            $order_id);

        return $response;
    }

    /**
     * Authorizes a card and stores the card token
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $this->business_id = $business_id;
        $customer          = new Customer($customer_id);

        $name = $creditCard['firstName'].' '.$creditCard['lastName'];
            
        try {
            $params["card"] = array(
              'id' => $customer->customerID,
              'firstName' => $customer->firstName,
              'lastName' => $customer->lastName,
              'email' => $customer->email,
              'phone' => $customer->phone,
              //'paymentMethodNonce' => $nonce,
              'creditCard' => array(
                'number' => $creditCard['cardNumber'],
                'cvv' => $creditCard['csc'],
                'expirationDate' => $creditCard['expMonth']."/".$creditCard['expYear'],
                'billingAddress' => array(
                  'firstName' => $creditCard['firstName'],
                  'lastName' => $creditCard['lastName'],
                  'company' => 'Laundry',
                  'streetAddress' => $creditCard['address1'],
                  'locality' => $creditCard['city'],
                  'region' => $creditCard['state'],
                  'postalCode' => $creditCard['zip']
                )
              )
            );

            try {
              $bt_customer = \Braintree\Customer::find($customer->customerID);
            } catch (\Braintree\Exception\NotFound $e) {
              $bt_customer = false;
            }

            if (!empty($bt_customer->id)) {
              unset($params["card"]["id"]);
              $result = \Braintree\Customer::update($bt_customer->id, $params["card"]);
            } else {
              $result = \Braintree\Customer::create($params["card"]);
            }

            if (!$result->success) {
                $errors = "";
                foreach($result->errors->deepAll() AS $error) {
                    $errors .= $error->message . "<br>";
                    throw new \Exception($errors, 0);
                }
            }

            $status         = 'SUCCESS';
            $transaction_id = $result->customer->id."|".$result->customer->paymentMethods[0]->token;
            $result_code    = 0;
            $message        = 'Authorization Success';
        } catch(\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->getMessage();
            $result_code    = $e->getCode();
        }

        // Response will log this transaction in the transaction table
        $response = new Response(
            $status, 
            $result_code, 
            $transaction_id, 
            $message, 
            '', 
            '', 
            '', 
            'auth', 
            $customer_id, 
            $business_id, 
            self::PROCESSOR, 
            1, 
            '', 
            $order_id);
        return $response;
    }
}