<?php
/*
* Documentation are located in Jira DROP-4880
* Important docs are:
*  - e-SCOTT_Smart_TestCard.xlsx
*  - e-SCOTT Smart ConnectionSpec(BasicService)ver.1_1_12.pdf
*  - e-SCOTT Smart ConnectionSpec(CreditCardPaymentService)ver 1_3_9.pdf
*/
namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;


class Sonypayment extends AbstractProcessor
{
    public $business_id;
    protected $api_endpoint;
    protected $merchant_id;
    protected $merchant_pass;
    protected $is_sandbox = false;
    protected $CI;

    const PROCESSOR = "sonypayment";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the Sony payment credentials');
        }

        $this->business_id = $params['business_id'];
        $this->merchant_id = get_business_meta($this->business_id, 'sonypayment_merchant_id');
        $this->merchant_pass = get_business_meta($this->business_id, 'sonypayment_merchant_pass');

        if (DEV || get_business_meta($this->business_id, 'sonypayment_mode') == 'test') {
          $this->setTestMode();
        } else {
          $this->setLiveMode(); 
        }

        $this->CI = get_instance();
        $this->CI->load->library('encrypt');
    }

    public function setTestMode()
    {
      $this->api_endpoint = 'https://www.test.e-scott.jp/online/aut/OAUT002.do?';
      $TransactionDate = date("Ymd");
      $this->api_endpoint .= "MerchantId={$this->merchant_id}&MerchantPass={$this->merchant_pass}&TransactionDate=".date("Ymd");
      $this->is_sandbox = true;
    }

    public function setLiveMode()
    {
      $this->api_endpoint = 'https://www.e-scott.jp/online/aut/OAUT002.do?';
      $TransactionDate = date("Ymd");
      $this->api_endpoint .= "MerchantId={$this->merchant_id}&MerchantPass={$this->merchant_pass}&TransactionDate=".date("Ymd");
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace(',', '.', $amount); 
        $amount = number_format ( $amount , 0 , "." , "");

        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        try {
            $auth = $this->authorizeTransaction($creditCard, $amount);
            
            if (!$auth) {
              throw new \Exception("Transaction was not authorized");
            }

            $params = array(
              'OperateId'       => '1Capture',
              'CardNo'          => $this->decode($creditCard->cardNumber, $creditCard->payer_id),
              'CardExp'         => substr($creditCard->expYr, -2).$creditCard->expMo,
              'ProcessId'       => $auth['ProcessId'],
              'ProcessPass'     => $auth['ProcessPass'],
              'SalesDate'       => date("Ymd"),
              'Amount'          => $amount,
              'PayType'         => '01'
            );
           
            $result = $this->request($params);

            if (!$result) {
              throw new \Exception("Transaction was not authorized");
            }
            
            if (empty($result["ResponseCd"]) || ($result["ResponseCd"] != "OK")) {
              throw new \Exception("Transaction was not authorized");
            } 

            $status         = 'SUCCESS';
            $transaction_id = $result["TransactionId"];
            $message        = 'Successful Transaction';
            $result_code    = $result["ResponseCd"];
        } catch (\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = empty($result["TransactionId"])?0:$result["TransactionId"];
            $result_code    = empty($result["ResponseCd"])?"":$result["ResponseCd"]; 
            $message        = "Error Code: ".$result_code;   
        }
        // Response will log this transaction in the transaction table
        $response = new Response(
            $status, 
            $result_code, 
            $transaction_id, 
            $message . $this->getCreditCardSummary($creditCard), 
            '', '', '', 
            'Sale', 
            $customer_id, 
            $business_id, 
            self::PROCESSOR, 
            $amount, 
            '', 
            $order_id);

        return $response;
    }


    protected function authorizeTransaction($creditCard, $amount)
    {
        $params = array(
          'OperateId'       => '1Auth',
          'CardNo'          => $this->decode($creditCard->cardNumber, $creditCard->payer_id),
          'CardExp'         => substr($creditCard->expYr, -2).$creditCard->expMo,
          'PayType'         => '01',
          'Amount'          => $amount
        );

        $result = $this->request($params);

        if (empty($result["ResponseCd"]) || ($result["ResponseCd"] != "OK")) {
          return false;   
        }

        return $result;
    }

    /**
     * Authorizes a card and stores the card token
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
      $this->business_id = $business_id;

      $params = array(
        'OperateId' => '1Check',
        'CardNo' => $creditCard['cardNumber'],
        'CardExp' => substr($creditCard['expYear'], -2).$creditCard['expMonth']
      );

      /*
      $result = Array ( 
        [TransactionId] => 000008192304 
        [TransactionDate] => 20180310 
        [OperateId] => 1Check 
        [ProcessId] => b6df8d71895dcc01e6342c61d31895a0 
        [ProcessPass] => 33b2236575025ae5049015a8c26c43f7 
        [ResponseCd] => OK 
        [CompanyCd] => Y0 
        [ApproveNo] => 0001000 
      )
      */
      
      try {
        $result = $this->request($params);

        if (empty($result["ResponseCd"]) || ($result["ResponseCd"] != "OK")) {
          throw new \Exception("Transaction was not authorized");
        } else {
          $status         = 'SUCCESS';
          $transaction_id = $result["TransactionId"]."|".$this->encode($creditCard['cardNumber']);
          $result_code    = $result["ResponseCd"];
          $message        = 'Authorization Success';
        }
      } catch (\Exception $e) {
        $status         = 'FAIL';
        $transaction_id = empty($result["TransactionId"])?0:$result["TransactionId"];
        $result_code    = empty($result["ResponseCd"])?"":$result["ResponseCd"]; 
        $message        = "Error Code: ".$result_code; 
      }

        // Response will log this transaction in the transaction table
      $response = new Response(
        $status, 
        $result_code, 
        $transaction_id,  //refNumber
        $message, 
        '', 
        '', 
        '', 
        'auth', 
        $customer_id, 
        $business_id, 
        self::PROCESSOR, 
        1, 
        '', 
        $order_id);
      return $response;
    }

    /**
     * Encodes cardNumber
     * Note: It doesn't encodes the last four digits for security reasons
     *
     * @param string $cardNumber
     * @return string the encoded cardNumber
     */
    protected function encode($cardNumber)
    {
      $number_to_encode = substr(
        $cardNumber, 
        0, 
        strpos(
          $cardNumber, 
          substr($cardNumber, -4)
        ) 
      );
      return $this->CI->encrypt->encode($number_to_encode);
    }

    /**
     * Decodes cardNumber
     *
     * @param string $lastFour from creditCard db table
     * @param string $payer_id from creditCard db table
     * @return string the decoded cardNumber
     */
    protected function decode($lastFour, $payer_id)
    {
      $number = explode("|", $payer_id);
      return $this->CI->encrypt->decode($number[1]).$lastFour;
    }

    /**
     * Does the request
     *
     * @param string $method GET
     * @param string $params as array
     * @param string $http_status returns the http status code after the request
     * @return string the raw response
     */
    protected function request($params = array(), &$http_status = null)
    {
        $ch = curl_init();

        $api_endpoint = $this->api_endpoint;
        $api_endpoint .= '&'.http_build_query($params);

        curl_setopt($ch, CURLOPT_URL, $api_endpoint);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);

        // relax SSL for now,
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);

        $response = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($errno = curl_errno($ch)) {
            throw new \Exception("Curl Error: " . curl_error($ch));
        }

        curl_close($ch);

        parse_str($response, $result);
        return $result;
    }
}