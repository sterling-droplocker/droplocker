<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Order;

require_once APPPATH."third_party/transbank/Transbank.php";

class Transbank extends AbstractProcessor
{
    public $business_id;
    public $currency = 'USD';
    protected $private_key = '/certs/oneclick.key';
    protected $cert_file = '/certs/oneclick.crt';
    protected $url = 'https://webpay3g.transbank.cl/webpayserver/wswebpay/OneClickPaymentService?wsdl';
    protected $server_cert = '/certs/certificate_server.crt';
    protected $return_url;

    const PROCESSOR = "transbank";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the credentials');
        }

        $this->business_id = $params['business_id'];
        $this->currency = get_business_meta($this->business_id,'transbank_currency', 'USD');

        if (DEV || get_business_meta($this->business_id, 'tb_mode') == 'test') {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }

    }

    public function setTestMode()
    {
        //NOTE: Transbank change the dev certs and endpoints periodically so if these don't work you'll have to contact them
        //at soporte@transbank.cl
        $this->private_key = '/certs/oneclick.key';
        $this->cert_file  = '/certs/oneclick.crt';
        $this->server_cert = '/certs/certificate_server_dev.crt';
	$this->url = 'https://webpay3gint.transbank.cl/webpayserver/wswebpay/OneClickPaymentService?wsdl';
    }

    public function setLiveMode()
    {
        $this->private_key = get_business_meta($this->business_id, 'tb_private_key');
        $this->cert_file  = get_business_meta($this->business_id, 'tb_certificate_server');
        $this->server_cert = '/certs/certificate_server.crt';
	$this->url = 'https://webpay3g.transbank.cl/webpayserver/wswebpay/OneClickPaymentService?wsdl';
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        try {
            //check to make sure a comma doesnt break the amount
            $amount = str_replace(',', '.', $amount);
            $name = $customer->firstName.' '.$customer->lastName;

            $oneClickService =  new \Transbank($this->url, $this->private_key, $this->cert_file, $this->server_cert);

            //$oneClickInscriptionInput = new \oneClickInscriptionInput();
            $oneClickPayInput = new  \oneClickPayInput();

            $oneClickPayInput->amount = $amount;
            $oneClickPayInput->buyOrder = $this->generateBuyOrder($this->business_id);
            $oneClickPayInput->tbkUser = $creditCard->payer_id;
            $oneClickPayInput->username = $name;



            $oneClickauthorizeResponse = $oneClickService->authorize(array("arg0" => $oneClickPayInput));

            $xmlResponse = $oneClickService->soapClient->__getLastResponse();

            /*$file = fopen($_SERVER['DOCUMENT_ROOT'].'/tb_authorize.txt', 'a+');
            fwrite($file, print_r($oneClickPayInput, true));
            fwrite($file, $xmlResponse);
            fclose($file);*/

            $soapValidation = new \SoapValidation($xmlResponse, SERVER_CERT);
            if ($soapValidation->getValidationResult() == false) {
                $mailcontent = print_r($oneClickPayInput, true);
                $mailcontent.= ' ---- '.$xmlResponse;
                mail('enegri@droplocker.com', 'TB Invalid Transaction', $mailcontent);
                return $this->invalidCertificate($customer_id, $business_id, $order_id);
            }
            $oneClickPayOutput = $oneClickauthorizeResponse->return;



            //Resultado de la autorización
            $authorizationCode = $oneClickPayOutput->authorizationCode;
            $creditCardType = $oneClickPayOutput->creditCardType;
            $last4CardDigits = $oneClickPayOutput->last4CardDigits;
            $responseCode = $oneClickPayOutput->responseCode;
            $transactionId = $oneClickPayOutput->transactionId;

            if (intval($responseCode === 0)) {
                $status         = 'SUCCESS';
                $message        = 'Successful Transaction. Buy Order: '.$oneClickPayInput->buyOrder.', amount: '.$this->currency.' '.$amount.', Auth Code: '.$authorizationCode.' Date: '.date('Y-m-d h:i').' Last 4: '.$last4CardDigits.', Type of payment: Credit';
                //set_flash_message("success", $message);
                $CI = get_instance();
                $CI->load->model('transbankorder_model');
                $CI->transbankorder_model->add($order_id, $oneClickPayInput->buyOrder);
            } else {
                $status = 'FAIL';
                $message        = 'Failed Transaction. Buy Order: '.$oneClickPayInput->buyOrder;
                //set_flash_message("error", $message);
            }

            $transaction_id = $transactionId;
            $result_code    = $responseCode;

        } catch (\SoapFault $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->faulstring;
            $result_code    = $e->faultcode;
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id, $message . $this->getCreditCardSummary($creditCard), '', '', '', 'Sale', $customer_id, $business_id, self::PROCESSOR, $amount, '', $order_id);

        return $response;
    }

    /**
     * Authorizes a card and stores the card token
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
            if (isset($creditCard['responseCode'])) {
                return $this->processResponse($creditCard, $order_id, $customer_id, $business_id);
            }

            $name = $creditCard['firstName'].' '.$creditCard['lastName'];

            $expMonth = $creditCard['expMonth'];
            $expYear = $creditCard['expYear'];

            //get return url
            $ci =& get_instance();
            $controller = $ci->router->fetch_class();
            $base_url = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'];
            $params = $order_id.','.$expMonth.','.$expYear;
            $signature = $this->buildSignature($params);

            //Transbank accepts only one parameter in the query string so send all data separated by comma.
            $url_params = '?params='.$params.','.$signature;

            if ($controller == 'creditCards') {
                $responseUrl = $base_url.'/admin/creditCards/transbank_return'.$url_params;
            } else {
                $responseUrl = $base_url.'/account/profile/transbank_return'.$url_params;
            }

            $this->business_id = $business_id;
            $customer          = new Customer($customer_id);

            $oneClickService = new \Transbank($this->url, $this->private_key, $this->cert_file, $this->server_cert);
            $oneClickInscriptionInput = new \oneClickInscriptionInput();

            $oneClickInscriptionInput->username = $name;
            $oneClickInscriptionInput->email = $customer->email;
            $oneClickInscriptionInput->responseURL = $responseUrl;
            $oneClickInscriptionResponse = $oneClickService->initInscription(array("arg0" => $oneClickInscriptionInput));
            $xmlResponse = $oneClickService->soapClient->__getLastResponse();
            /*$file = fopen($_SERVER['DOCUMENT_ROOT'].'/tb_initInscription.txt', 'a+');
            fwrite($file, print_r($oneClickInscriptionInput, true));
            fwrite($file, $xmlResponse);
            fclose($file);*/
            $soapValidation = new \SoapValidation($xmlResponse, SERVER_CERT);
            if ($soapValidation->getValidationResult() == false) {
                $mailcontent = print_r($oneClickInscriptionInput, true);
                $mailcontent.= ' ---- '.$xmlResponse;
                mail('enegri@droplocker.com', 'TB Invalid Auth Transaction', $mailcontent);
                return $this->invalidCertificate($customer_id, $business_id, $order_id);
            }

            $oneClickInscriptionOutput = $oneClickInscriptionResponse->return;

            $tokenOneClick = $oneClickInscriptionOutput->token; //result token

            //if everything is ok don't return a respnse yet, first go to TransBank website so customer can input cc info
            $inscriptionURL = $oneClickInscriptionOutput->urlWebpay;
             echo "<html>
                <head>
                <title>Loading...</title>
                </head>
                <body>
                    Loading...
                    <form method='post' action='$inscriptionURL' id='tb_form'>"
                 . "<input type='hidden' id='TBK_TOKEN' name='TBK_TOKEN' value='$tokenOneClick'></form>
                    <script>window.onload = function() { document.getElementById('tb_form').submit();  };</script>
                    <!-- END -->
                </body>
               </html>";
             exit;
    }

    //this method is used when transbank redirects to LL with the respnse result
    public function finishInscription($tokenOneClick, $params, $business_id)
    {

            $params_array = explode(',', $params);

            if (!$this->validateSignature($params_array)) {
                return array('status' => 'FAIL', 'message' => 'Params may have been tampered!.');
            }

            $order_id = $params_array[0];
            $expMonth = $params_array[1];
            $expYear = $params_array[2];

            $order = new Order($order_id);

            $customer_id = $order->customer_id;
            $customer = new Customer($customer_id);
            $oneClickService = new \Transbank($this->url, $this->private_key, $this->cert_file);
            $oneClickFinishInscriptionInput = new \oneClickFinishInscriptionInput();
            $oneClickFinishInscriptionInput->token = $tokenOneClick;

            $oneClickFinishInscriptionResponse = $oneClickService->finishInscription(array( "arg0" => $oneClickFinishInscriptionInput));
            $xmlResponse = $oneClickService->soapClient->__getLastResponse();
           /* $file = fopen($_SERVER['DOCUMENT_ROOT'].'/tb_finishInscription.txt', 'a+');
            fwrite($file, print_r($oneClickFinishInscriptionInput, true));
            fwrite($file, $xmlResponse);
            fclose($file);*/
            $soapValidation = new \SoapValidation($xmlResponse, SERVER_CERT);
            if ($soapValidation->getValidationResult() == false) {
                return $this->invalidCertificate($customer_id, $business_id, $order_id);
            }

            //if signature is valid
            $oneClickFinishInscriptionOutput = $oneClickFinishInscriptionResponse->return;

            //response result
            $responseCode = $oneClickFinishInscriptionOutput->responseCode;
            $authCode = $oneClickFinishInscriptionOutput->authCode;
            $creditCardType = $oneClickFinishInscriptionOutput->creditCardType;
            $last4CardDigits = $oneClickFinishInscriptionOutput->last4CardDigits;
            $tbkUser = $oneClickFinishInscriptionOutput->tbkUser;

            $data = array(
                'responseCode' => $responseCode,
                'authCode' => $authCode,
                'cardType' => $creditCardType,
                'cardNumber' => $last4CardDigits,
                'tbkUser' => $tbkUser,
                'expMonth' => $expMonth,
                'expYear' => $expYear,
                'city' => '',
                'state' => '',
                'zipcode' => '',
                'address' => '',
                );


            $CI = get_instance();
            $CI->load->library("creditcard");
            return $CI->creditcard->authorizeCardData($data, $customer, true, $order);
    }

    public function removeUser($creditCard)
        {

        try {

            $customer = new Customer($creditCard->customer_id);
            $username = $customer->firstName.' '.$customer->lastName;

            $oneClickService = new \Transbank($this->url, $this->private_key, $this->cert_file);
            $oneClickRemoveUserInput = new \oneClickRemoveUserInput();
            $oneClickRemoveUserInput->tbkUser = $creditCard->payer_id;
            $oneClickRemoveUserInput->username =  $username;
            $removeUserResponse = $oneClickService->removeUser(array ("arg0" => $oneClickRemoveUserInput) );
            $xmlResponse = $oneClickService->soapClient->__getLastResponse();

            /*$file = fopen($_SERVER['DOCUMENT_ROOT'].'/tb_removeUser.txt', 'a+');
            fwrite($file, print_r($oneClickRemoveUserInput, true));
            fwrite($file, $xmlResponse);
            fclose($file);*/

            $soapValidation = new \SoapValidation($xmlResponse, SERVER_CERT);
            if ($soapValidation->getValidationResult() == false) {
                return false;
            }

            return $removeUserResponse->return;
        } catch (\SoapFault $e) {
            return false;
        }

    }

    public function codeReverse($order_id)
    {
        $oneClickService = new \Transbank($this->url, $this->private_key, $this->cert_file);
        $oneClickReverseInput = new \oneClickReverseInput();
        $oneClickReverseInput->buyorder= $this->getBuyOrder($order_id);
        $codeReverseOneClickResponse = $oneClickService->codeReverseOneClick(array("arg0" => $oneClickReverseInput));
        $oneClickReverseOutput = $codeReverseOneClickResponse->return;
        $xmlResponse = $oneClickService->soapClient->__getLastResponse();
        $soapValidation = new \SoapValidation($xmlResponse, SERVER_CERT);

        /*$file = fopen($_SERVER['DOCUMENT_ROOT'].'/tb_codeReverse.txt', 'a+');
        fwrite($file, print_r($oneClickReverseInput, true));
        fwrite($file, $xmlResponse);
        fclose($file);*/

        if ($soapValidation->getValidationResult() == false) {
            return false;
        }

        return $oneClickReverseOutput->reversed;
    }

    public function invalidCertificate($customer_id, $business_id, $order_id)
    {
        $status         = 'FAIL';
        $transaction_id = 0;
        $message        = 'Invalid certificate or the request has timed out. Please try again later.';
        $result_code    = 0;
        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id, $message, '', '', '', 'auth', $customer_id, $business_id, self::PROCESSOR, 0, '', $order_id);
        return $response;

    }

    public function processResponse($reply, $order_id, $customer_id, $business_id)
    {
        if ( (isset($reply['responseCode'])) && (intval($reply['responseCode']) === 0) ) {
            $status = 'SUCCESS';
            $refNumber = $reply['tbkUser'];
        } else {
            $status = 'FAIL';
            $refNumber = '';
        }

        $response = new Response($status, $reply['responseCode'], $refNumber, $status, '', '', '', 'auth', $customer_id, $business_id, self::PROCESSOR, 0, '', $order_id);
        return $response;
    }

    protected function generateBuyOrder($business_id)
    {
        $buyOrder = date('Ymdhis');
        $today = date('Y-m-d');
        $CI = get_instance();
        $sql = "select count(*) as total_transactions from transaction where business_id = ? and processor = ? AND updated >= ? and type = ?";
        $query_result  = $CI->db->query($sql, array($business_id, self::PROCESSOR , $today, 'Sale'));
        $order = $query_result->row();
        $total_orders = intval($order->total_transactions);


        if ($total_orders < 10) {
            $buyOrder.= '00'.$total_orders;
        } else if ($total_orders <= 100) {
            $buyOrder.= '0'.$total_orders;
        } else {
             $buyOrder.= $total_orders;
        }

        return $buyOrder;
    }

    function getBuyOrder($order_id)
    {
        $CI = get_instance();
        $sql = "select * from transbankOrders where order_id = ?";
        $query_result  = $CI->db->query($sql, array($order_id));
        $order = $query_result->row();
        return $order->code;
    }

    public function supportsMobileAuth()
    {
        return false;
    }

    public function buildSignature($params)
    {
        $CI = get_instance();
        $hash_salt = $CI->config->config['hash_salt'];
        return sha1($params.$hash_salt);
    }

    public function validateSignature($params_array)
    {
        if (count($params_array) != 4) {
            return false;
        }

        $signature = $params_array[3];
        unset($params_array[3]);

        $params = implode(',', $params_array);
        $signatureMatch = $this->buildSignature($params);

        return $signatureMatch == $signature;
    }
}