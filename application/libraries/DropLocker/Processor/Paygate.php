<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;

ini_set("soap.wsdl_cache_enabled", "0");

class Paygate extends AbstractProcessor
{
    public $paygate_id;
    public $paygate_password;
    public $paygate_mode;
    public $business_id;
    public $is_test = false;
    public $currency = "ZAR";
    public $soap_url = 'https://secure.paygate.co.za/PayHost/process.trans?wsdl';

    const PROCESSOR = "paygate";

    public function __construct($params)
    {
        $this->checkRequirements();

        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the Paygate credentials');
        }

        $this->business_id = $params['business_id'];
        $this->paygate_mode = get_business_meta($this->business_id, 'paygate_mode');

        if (DEV || $this->paygate_mode == "test") {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    protected function checkRequirements()
    {
        if (!class_exists('SoapClient')) {
            throw new \Exception("Paygate requires php SoapClient");
        }
    }

    public function setTestMode()
    {
        $this->is_test = true;
        $this->paygate_id = get_business_meta($this->business_id, 'paygate_sandbox_id');
        $this->paygate_secret = get_business_meta($this->business_id, 'paygate_sandbox_secret');
    }

    public function setLiveMode()
    {
        $this->paygate_id = get_business_meta($this->business_id, 'paygate_id');
        $this->paygate_secret = get_business_meta($this->business_id, 'paygate_secret');
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace( ',', '.', $amount);

        $this->business_id = $business_id;

        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        $result = $this->makeTransaction($creditCard, $customer, $order_id, false, $amount);
        
        $transaction_id = "-1";
        $reference = "-1";
        $status = "FAIL";
        $message = "FAIL";
        $auth_code = "";

        if ($result) {
            $status = "SUCCESS";
            $transaction_id = $result->TransactionId;
            $reference = $result->Reference;
            $message = $result->ResultDescription;
            $auth_code = $result->AuthCode;
        }

        // Response will log this transaction in the transaction table
        $response = new Response(
            $status,
            $transaction_id,
            $reference,
            $message . $this->getCreditCardSummary($creditCard),
            '',
            '',
            '',
            'sale',
            $customer_id,
            $this->business_id,
            self::PROCESSOR,
            $amount,
            $auth_code,
            $order_id);

        return $response;
    }

    /**
     * Authorizes a card and stores in CIM
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $amount = "1.00";
        $this->business_id = $business_id;
        $customer = new Customer($customer_id);

        $result = $this->makeTransaction($creditCard, $customer, $order_id, true, $amount);

        $transaction_id = "-1";
        $reference = "-1";
        $status = "FAIL";
        $message = "FAIL";
        $auth_code = "";
        $payer_id = "";

        if ($result) {           
            $transaction_id = empty($result->TransactionId)?null:$result->TransactionId;

            if (!empty($transaction_id)) {
                //removes the charge
                $this->voidTransaction($transaction_id);

                //get the token for this customer
                $vault_result = $this->cardVault($creditCard);
                $payer_id = empty($vault_result->VaultId)?null:$vault_result->VaultId;

                if (!empty($payer_id)) {
                    $status = "SUCCESS";
                    $reference = $result->Reference;
                    $message = $result->ResultDescription;
                    $auth_code = $result->AuthCode;
                }
            }
        }

        // Response will log this transaction in the transaction table
        $response = new Response(
            $status,
            $transaction_id,
            $payer_id,
            $message,
            '',
            '',
            '',
            'auth',
            $customer_id,
            $this->business_id,
            self::PROCESSOR,
            $amount,
            $auth_code,
            $order_id);

        return $response;
    }


    /**
     * Voids a transaction
     *
     * @param string $transaction_id
     * @return TransactionResponse
     */
    protected function voidTransaction($transaction_id)
    {
        $request = "
        <ns1:SingleFollowUpRequest>
            <ns1:VoidRequest>
                <ns1:Account>
                    <ns1:PayGateId>{$this->paygate_id}</ns1:PayGateId>
                    <ns1:Password>{$this->paygate_secret}</ns1:Password>
                </ns1:Account>
                <ns1:TransactionId>%s</ns1:TransactionId>
                <ns1:TransactionType>Settlement</ns1:TransactionType>
            </ns1:VoidRequest>
        </ns1:SingleFollowUpRequest>";

        $request = sprintf($request, $transaction_id);

        return $this->soap_request($request, "SingleFollowUp");
    }

    protected function makeTransaction($creditCard, $customer, $order_id, $authorize = false, $amount = false)
    {
        $reference = 'sale_'.$order_id;
        if ($authorize) {
            $reference = 'authorization_' . time();
        }

        $request = "
            <ns1:SinglePaymentRequest>
                <ns1:CardPaymentRequest>
                    <ns1:Account>
                        <ns1:PayGateId>{$this->paygate_id}</ns1:PayGateId>
                        <ns1:Password>{$this->paygate_secret}</ns1:Password>
                    </ns1:Account>
                    <ns1:Customer>
                        <ns1:FirstName>%s</ns1:FirstName>
                        <ns1:LastName>%s</ns1:LastName>
                        <!-- Zero or more repetitions: -->
                        <ns1:Telephone>%s</ns1:Telephone>
                        <!-- 1 or more repetitions: -->
                        <ns1:Email>%s</ns1:Email>
                    </ns1:Customer>
                    %s
                    <ns1:CVV>%s</ns1:CVV>
                    <ns1:BudgetPeriod>0</ns1:BudgetPeriod>
                    <ns1:Order>
                        <ns1:MerchantOrderId>%s</ns1:MerchantOrderId>
                        <ns1:Currency>%s</ns1:Currency>
                        <ns1:Amount>%s</ns1:Amount>
                    </ns1:Order>
                </ns1:CardPaymentRequest>
            </ns1:SinglePaymentRequest>";

        if (is_object($creditCard)) {
            $card_number = $creditCard->cardNumber;
            $expires = $creditCard->expMonth.$creditCard->expYear;
            $csc = $creditCard->csc; 
            $token = $creditCard->payer_id;
        } else {
            $card_number = $creditCard["cardNumber"];
            $expires = $creditCard["expMonth"].$creditCard["expYear"];
            $csc = $creditCard["csc"];
            $token = $creditCard["payer_id"];                
        }

        if ($authorize) {
            //First time we use card number
            $card_details = "
                    <ns1:CardNumber>%s</ns1:CardNumber>
                    <ns1:CardExpiryDate>%s</ns1:CardExpiryDate>
                    ";

            $card_details = sprintf(
                $card_details,
                $card_number,
                $expires
            );
        } else {
            //use token
            $card_details = "<VaultId>%s</VaultId>";

            $card_details = sprintf(
                $card_details,
                $token
            );
        }

        $request = sprintf(
            $request, 
            $customer->firstName,
            $customer->lastName,
            $customer->phone,
            $customer->email,
            $card_details,
            $csc,
            $reference,
            $this->currency,
            $this->convert_amount_to_cents($amount)
        );

        return $this->soap_request($request);
    }

    protected function cardVault($creditCard) 
    {

        $request = "
            <ns1:SingleVaultRequest>
                <ns1:CardVaultRequest>
                    <ns1:Account>
                        <ns1:PayGateId>{$this->paygate_id}</ns1:PayGateId>
                        <ns1:Password>{$this->paygate_secret}</ns1:Password>
                    </ns1:Account>
                    <ns1:CardNumber>%s</ns1:CardNumber>
                    <ns1:CardExpiryDate>%s</ns1:CardExpiryDate>
                </ns1:CardVaultRequest>
            </ns1:SingleVaultRequest>";

        $request = sprintf(
            $request, 
            $creditCard["cardNumber"],
            $creditCard["expMonth"].$creditCard["expYear"]
        );

        return $this->soap_request($request, "SingleVault");
    }

    protected function soap_request($request, $action = "SinglePayment") 
    {
        /**
         *  disabling WSDL cache
         */
        ini_set("soap.wsdl_cache_enabled", "0");

        /*
         * Using PHP SoapClient to handle the request
         */
        $soapClient = new \SoapClient($this->soap_url, array('trace' => 1)); //point to WSDL and set trace value to debug

        try{
            /*
             * Send SOAP request
             */
            $result = $soapClient->__soapCall(
                $action, 
                array(new \SoapVar($request, XSD_ANYXML)
            ));

        } catch(\SoapFault $sf){
            /*
             * handle errors
             */
            $result = $sf->getMessage();
        }

        if (!empty($result)) {
            if (!empty($result->CardPaymentResponse)) {
                $result = $result->CardPaymentResponse->Status;
                //var_dump($request, $result);
            } elseif (!empty($result->VoidResponse)) {
                $result = $result->VoidResponse->Status;
                //var_dump($request, $result);
            } elseif (!empty($result->CardVaultResponse)) {
                $result = $result->CardVaultResponse->Status;
                //var_dump($request, $result);
            } else {
                return false;
            }


            if (in_array($result->StatusName, array("Approved", "Completed", "Error"))) {
                //void transactions fail with test accounts that is why we accept "Error"
                if ($result->StatusName == "Error" && $action != "SingleFollowUp") {
                    return false;
                }

                return $result;
            }
        }

        return false;
    }

    /* Paygate requires amount in cents. Eg 32.10 must be send as 3210 */
    protected function convert_amount_to_cents($amount) 
    {
        $amount = str_replace( ',', '.', $amount);
        $amount = number_format($amount, 2);
        $amount = $amount * 100;
        return $amount;
    }

    protected function convert_cents_to_amount($amount) 
    {
        $amount = $amount / 100;
        $amount = number_format($amount, 2);
        $amount = str_replace( ',', '.', $amount);
        return $amount;
    }
}
