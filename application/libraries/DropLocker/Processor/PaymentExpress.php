<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\CreditCard;

class PaymentExpress extends AbstractProcessor
{
    public $business_id;
    public $currency = 'NZD';
    protected $username;
    protected $password;

    const PROCESSOR = "paymentexpress";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the credentials');
        }

        $this->business_id = $params['business_id'];
        $this->currency    = get_business_meta($this->business_id, 'pe_currency', 'NZD');

        if (DEV || get_business_meta($this->business_id, 'pe_mode') == 'test') {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    public function setTestMode()
    {
        $this->username = "LaundryLockerDev";
        $this->password  = "password12";
    }

    public function setLiveMode()
    {
        $this->username = get_business_meta($this->business_id, 'pe_username');
        $this->password = get_business_meta($this->business_id, 'pe_password');
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace(',', '.', $amount);


        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
                'customer_id' => $customer_id,
                'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        $business = new Business($business_id);

        try {

            $params = array( 'DpsBillingId' => $creditCard->payer_id,
                             'Amount' => $amount,
                             'InputCurrency' => $this->currency,
                             'TxnType' => 'Purchase',
                             'MerchantReference' => $business->companyName
                    );

            $result = $this->doRequest($params);

            if (intval($result['AUTHORIZED']) === 1) {
                $status = 'SUCCESS';
                $message = 'Authorization Success';
                $transaction_id = $result['BILLINGID'];
                $result_code = 0;
            } else {
                $status = 'FAIL';
                $message = $result['CARDHOLDERHELPTEXT'];
                $transaction_id = 0;
                $result_code = $result['RECO'];
            }

            
        } catch (\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->getMessage();
            $result_code    = $e->getCode();
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id, $message . $this->getCreditCardSummary($creditCard), '', '', '', 'Sale', $customer_id, $business_id, self::PROCESSOR, $amount, '', $order_id);
        return $response;
    }

    /**
     * Authorizes a card and stores the card token
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $this->business_id = $business_id;
        $business = new Business($business_id);

        $name   = $creditCard['firstName'].' '.$creditCard['lastName'];

        $dateExpiry = $creditCard['expMonth'].substr($creditCard['expYear'], -2);
                
        $params = array(
            'CardHolderName' => $name,
            'MerchantReference' => $business->companyName,
            'CardNumber' => $creditCard['cardNumber'],
            'DateExpiry' => $dateExpiry,
            'Amount' => '1',
            'TxnType' => 'Auth',
            'EnableAddBillCard' => 1,
            'InputCurrency' => $this->currency,
            'BillingId' => $order_id,
        );


        try {
            $result = $this->doRequest($params);
            if (intval($result['AUTHORIZED']) === 1) {
                $status = 'SUCCESS';
                $message = 'Authorization Success';
                $transaction_id = $result['DPSBILLINGID'];
                $result_code = 0;
            } else {
                $status = 'FAIL';
                $message = $result['CARDHOLDERHELPTEXT'];
                $transaction_id = 0;
                $result_code = $result['RECO'];
            }      
        } catch (\Exception $e) {
            $status         = 'FAIL';
            $transaction_id = 0;
            $message        = $e->getMessage();
            $result_code    = $e->getCode();
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id, $message, '', '', '', 'auth', $customer_id, $business_id, self::PROCESSOR, 1, '', $order_id);
        return $response;
    }

    function doRequest($params = array())
    {
        $request = '<Txn>';
        $request .= "<PostUsername>$this->username</PostUsername>"; #Insert your Payment Express Username here
        $request .= "<PostPassword>$this->password</PostPassword>"; #Insert your Payment Express Password here

        foreach ($params as $k => $v) {
            $request.="<$k>$v</$k>";
        }
        $request.='</Txn>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sec.paymentexpress.com/pxpost.aspx");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //Needs to be included if no *.crt is available to verify SSL certificates

        $result = curl_exec($ch);
        curl_close($ch);

        return $this->parseResponse($result);
    }

    function parseResponse($data)
    {
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $data, $vals, $index);
        xml_parser_free($xml_parser);

        $params = array();
        $level  = array();


        foreach ($vals as $xml_elem) {
            if ($xml_elem['type'] == 'open') {
                if (array_key_exists('attributes', $xml_elem)) {
                    list($level[$xml_elem['level']], $extra) = array_values($xml_elem['attributes']);
                } else {
                    $level[$xml_elem['level']] = $xml_elem['tag'];
                }
            }
            if ($xml_elem['type'] == 'complete') {
                $start_level = 1;


                while ($start_level < $xml_elem['level']) {
                    @$params[$level[$start_level]];
                    $start_level++;
                }
                @$params[$xml_elem['tag']] = $xml_elem['value'];
            }
        }
        return $params;
    }
}