<?php

namespace DropLocker\Processor;

require_once(APPPATH."third_party/Coinbase/Coinbase.php");

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Business;

class Coinbase extends AbstractProcessor
{

    protected $business_id;
    protected $customer_id;
    protected $sanbox = false;

    protected $clientId;
    protected $secret;
    protected $address;
    protected $currency = 'USD';
    protected $send_limit_amount = 300;

    const PROCESSOR = "coinbase";

    function __construct($params = array())
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the paypal credentials');
        }

        $sandbox = DEV ? true : false;

        $this->business_id = $params['business_id'];
        $this->address = get_business_meta($this->business_id, 'coinbase_address');
        $this->clientId = get_business_meta($this->business_id, 'coinbase_client');
        $this->secret = get_business_meta($this->business_id, 'coinbase_secret');
        $this->currency = get_business_meta($this->business_id, 'coinbase_currency', 'USD');
        $this->send_limit_amount = get_business_meta($this->business_id, 'coinbase_send_limit_amount', 300);

        if (DEV) {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    public function setTestMode()
    {
        $this->sanbox = true;
    }

    public function setLiveMode()
    {
        $this->sanbox = false;
    }

    public function oauth($returnURL)
    {
        $coinbaseOauth = new \Coinbase_OAuth($this->clientId, $this->secret, $returnURL);
        $url = $coinbaseOauth->createAuthorizeUrl("user", "send", "addresses", "send:bypass_2fa");
        $url .= '&meta[send_limit_amount]=' . $this->send_limit_amount . '&meta[send_limit_currency]=' . $this->currency;

        return array(
            'status' => 'success',
            'message' => 'Url created',
            'url' => $url,
        );
    }

    public function authorizeToken($token, $returnURL, $order_id = 0, $customer_id = 0, $business_id = 0)
    {
        try {
            $coinbaseOauth = new \Coinbase_OAuth($this->clientId, $this->secret, $returnURL);
            $tokens = $coinbaseOauth->getTokens($token);
            $coinbase = \Coinbase::withOauth($coinbaseOauth, $tokens);

            $status = 'SUCCESS';
            $message = 'Authorization Success';
            $result_code = 1;
            $refNumber = json_encode($tokens);


        } catch (\Coinbase_ApiException $e) {
            $status = 'FAIL';
            $message = $e->getMessage();
            $result_code = $e->getCode();
            $refNumber = null;
        }

        $response = new Response($status, $result_code, $refNumber,
            $message, '', '', '', 'auth', $customer_id, $business_id, self::PROCESSOR, 0, '', $order_id);
        return $response;
    }

    public function getCustomerDetails($tokens, $returnURL)
    {
        $coinbaseOauth = new \Coinbase_OAuth($this->clientId, $this->secret, $returnURL);
        $coinbase = \Coinbase::withOauth($coinbaseOauth, $tokens);

        return (array) $coinbase->getUser();
    }

    public function authorizeCard($creditCard = array(), $order_id = 0, $customer_id = 0, $business_id = 0)
    {
        throw new \Exception("Coinbase::authorizeCard() is not implemented, use Coinbase::authorizeToken()");
    }

    public function makeSale($amount = 0, $order_id = 0, $customer_id = 0, $business_id = 0)
    {
        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $business = new Business($business_id);
        $returnURL = get_business_account_url($business) . '/account/profile/coinbase_return';

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        $tokens = json_decode($creditCard->payer_id, true);

        try {

            $coinbaseOauth = new \Coinbase_OAuth($this->clientId, $this->secret, $returnURL);
            $coinbase = \Coinbase::withOauth($coinbaseOauth, $tokens);

            // Just a test to see if tokens are still valid
            try {
                $coinbase->getUser();
            } catch (\Coinbase_TokensExpiredException $e) {

                // token is expired, renew and save
                $tokens = $coinbaseOauth->refreshTokens($tokens);
                $creditCard->payer_id = json_encode($tokens);
                $creditCard->save();

                $coinbase = \Coinbase::withOauth($coinbaseOauth, $tokens);
            }

            $notes = "{$business->companyName} Order #{$order_id}";
            $userFee = '0.0002'; // for transactions less than 0.001 BTC

            // exchange rate
            $rate = $coinbase->getExchangeRate('btc', strtolower($this->currency));
            $min_amount = ceil($rate * 0.01); //min amount in $currency to get 0.001 BTC
            if ($amount > $min_amount) {
                $userFee = null; // then do not pay fee
            }

            $result = $coinbase->sendMoney($this->address, $amount, $notes, $userFee, $this->currency);

            if ($result->success) {
                $status = 'SUCCESS';
                $message = 'Successful Transaction';
                $transaction_id = $result->transaction->id;
                $result_code = 1;
            } else {
                $status = 'FAIL';
                $message = implode("\n", $result->errors);
                $result_code = 0;
                $transaction_id = $result->transaction->id;
            }

        } catch (\Coinbase_ApiException $e) {
            $status = 'FAIL';
            $message = $e->getMessage();
            $result_code = $e->getCode();
            $transaction_id = null;
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status, $result_code, $transaction_id,
                $message . $this->getCreditCardSummary($creditCard), '', '', '', 'Sale', $customer_id, $business_id, self::PROCESSOR, $amount, '', $order_id);

        return $response;
    }

    public function supportsMobileAuth()
    {
        return false;
    }
}
