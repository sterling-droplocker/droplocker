<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Business;

require_once(APPPATH."third_party/Paygateway/Paygateway.php");

class Paypro extends AbstractProcessor
{
    const TCC = 5; // secure ecommerce (6 = recurring billing)
    const INDUSTRY = "ECOMMERCE";
    const CVV = 1;
    const AVS = 8;
    const PROCESSOR = 'paypro';

    public $token;
    public $errorMessages = array();
    public $creditCardRequest;
    public $business_id;
    public $customer_id;
    public $response;
    public $type;
    public $amount;

    public function setTestMode()
    {
        $this->token = get_business_meta($this->business_id, 'paypro_test_token');
        $this->creditCardRequest->setAccountToken($this->token);
    }

    public function setLiveMode()
    {
        $this->token = get_business_meta($this->business_id, 'paypro_live_token');
        $this->creditCardRequest->setAccountToken($this->token);
    }

    public function __construct($args=array())
    {
        $CI = get_instance();

        if (empty($args['business_id'])) {
            $this->business_id = $CI->business->businessID;
        } else {
            $this->business_id = $args['business_id'];
        }

        $this->creditCardRequest = new \TransactionRequest();

        // Force test mode when in DEV
        if (DEV || get_business_meta($this->business_id, 'paypro_mode') == "test") {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    /**
    * authorizes a credit card
    *
    * @param std_class object $order
    * $order values
    * ----------------------
    * cardNumber
    * csc
    * expMonth
    * expYear
    * cardType
    * nameOnCard
    * firstName
    * lastName
    * address1
    * address2
    * city
    * state
    * zip
    * countryCode
    * orderID
    *
    * @param string $pnref
    * @param string $comment1
    * @return array
    */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {

        $this->business_id = $business_id;
        $this->customer_id = $customer_id;

        $creditCardRequest = $this->creditCardRequest;
        if (array_key_exists('firstName', $creditCard)) {
            $creditCardRequest->setBillFirstName($creditCard['firstName']);
        }

        if (array_key_exists('lastName', $creditCard)) {
            $creditCardRequest->setBillLastName($creditCard['lastName']);
        }
        if (array_key_exists('address', $creditCard)) {
            $creditCardRequest->setBillAddressOne($creditCard['address']);
        }
        //$creditCardRequest->setBillAddressTwo($creditCard['address2']);
        if (array_key_exists('zipcode', $creditCard)) {
            $creditCardRequest->setBillCity($creditCard['zipcode']);
        }
        if (array_key_exists('state', $creditCard)) {
            $creditCardRequest->setBillStateOrProvince($creditCard['state']);
        }
        $creditCardRequest->setExpireMonth($creditCard['expMonth']);
        $creditCardRequest->setExpireYear($creditCard['expYear']);
        $creditCardRequest->setCreditCardNumber($creditCard['cardNumber']);
        $creditCardRequest->setOrderID($this->getUniqId());
        $creditCardRequest->setOrderUserID($customer_id);
        $creditCardRequest->setPurchaseOrderNumber($order_id);
        $creditCardRequest->setChargeType("AUTH");

        $creditCardRequest->setChargeTotal(0.01); //Note, a charge of 1 cent is necessary for validating credit cards in the test environment
        $creditCardRequest->setManagePayerData('true');
        $creditCardRequest->setTransactionConditionCode(self::TCC);
        $creditCardRequest->setIndustry(self::INDUSTRY);
        $response = $creditCardResponse = $creditCardRequest->doTransaction();

        $result = $response->objResponseFields;

        if ($result['response_code'] == 1) {
            $status = 'SUCCESS';

        $creditCardRequest->setOrderID($result['order_id']);
        $creditCardRequest->setChargeType("VOID");
        $response = $creditCardResponse = $creditCardRequest->doTransaction();
        $refNumber = $result['order_id']."|".$result['payer_identifier'];

        } else {
            $status = 'FAIL';
            $refNumber = '';

            if (preg_match('/specified payer data is not under management/i', $result['response_code_text'])
                    && strtotime($creditCard['date']) > strtotime('-6 months')) {
                $message = '$result:<br><pre>';
                $message .= print_r($result, true);
                $message .= '</pre>';
                $message .= '<br><br>$creditCardRequest:<br><pre>';
                $message .= print_r($creditCardRequest, true);
                $message .= '</pre>';
                $message .= '<br><br>$card_payer_id:<br><pre>';
                $message .= print_r($card_payer_id, true);
                $message .= '</pre>';
                send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "authorizeCard FAIL", $message);
            }
        }


        $response = new Response($status,
                                 $result['response_code'],
                                 $refNumber, //refNumber
                                 $result['response_code_text'],
                                 '',
                                 '',
                                 '',
                                 'auth',
                                 $this->customer_id,
                                 $this->business_id,
                                 self::PROCESSOR,
                                 0,
                                 '',
                                 $order_id);

        return $response;



    }

    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        } elseif (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        } elseif (!is_numeric($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' must be numeric.");
        } elseif (!is_numeric($amount)) {
            throw new \InvalidArgumentException("'amount' must be numeric.");
        } else {
            $CI = get_instance();

            $this->business_id = $business_id;
            $this->customer_id = $customer_id;

            $customer = new Customer($customer_id);
            $creditCard = CreditCard::search_aprax(array('customer_id'=>$customer_id, 'business_id'=>$business_id));
            $creditCard = $creditCard[0];

            $refNumber = explode("|", $creditCard->payer_id);
            $card_payer_id = $refNumber[1];
            $sam = $creditCard->cardNumber;
            $mpd = 'false';

            $creditCardRequest = $this->creditCardRequest;
            $creditCardRequest->setBillFirstName($customer->firstName);
            $creditCardRequest->setBillLastName($customer->lastName);
            $creditCardRequest->setBillAddressOne($creditCard->address1);
            $creditCardRequest->setBillAddressTwo($creditCard->address2);
            $creditCardRequest->setBillCity($creditCard->zip);
            $creditCardRequest->setBillStateOrProvince($creditCard->state);
            $creditCardRequest->setExpireMonth($creditCard->expMonth);
            $creditCardRequest->setExpireYear($creditCard->expYear);
            $creditCardRequest->setPurchaseOrderNumber($order_id);
            $creditCardRequest->setChargeType("SALE");
            $creditCardRequest->setChargeTotal($amount);
            $creditCardRequest->setManagePayerData('true');
            $creditCardRequest->setTransactionConditionCode(6); //must set tcc to 6 for MPD
            $creditCardRequest->setIndustry(self::INDUSTRY);
            $creditCardRequest->setOrderID($this->getUniqId());
            $creditCardRequest->setOrderUserID($customer_id);

            if ($card_payer_id) {
                $creditCardRequest->setPayerIdentifier($card_payer_id);
            }
            if ($sam) {
                $creditCardRequest->setSecurePrimaryAccountNumber($sam);
            }
            if (isset($card_number)) {
                $creditCardRequest->setCreditCardNumber($card_number);
            }
            if (isset($card_csc)) {
                $creditCardRequest->setCreditCardVerificationNumber($card_csc);
            }

            $response = $creditCardRequest->doTransaction();
            $result = $response->objResponseFields;

            // if the credit card is authorized, store the customer in the CIM
            if ($result['response_code']==1) {
                $status = "SUCCESS";
            } else {
                $status = "FAIL";

            if (preg_match('/specified payer data is not under management/i', $result['response_code_text'])
                && strtotime($creditCard->properties['date']) > strtotime('-6 months')) {
                    $message = '$result:<br><pre>';
                    $message .= print_r($result, true);
                    $message .= '</pre>';
                    $message .= '<br><br>$creditCardRequest:<br><pre>';
                    $message .= print_r($creditCardRequest, true);
                    $message .= '</pre>';
                    $message .= '<br><br>$card_payer_id:<br><pre>';
                    $message .= print_r($card_payer_id, true);
                    $message .= '</pre>';
                    $message .= '<br><br>$creditCard:<br><pre>';
                    $message .= print_r($creditCard->properties, true);
                    $message .= '</pre>';
                    send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "authorizeCard FAIL", $message);
                }
            }

            // Response will log this transaction in the transaction table
            $response = new Response($status,
                $result['response_code'],
                $result['order_id'], //refNumber
                $result['response_code_text'] . $this->getCreditCardSummary($creditCard),
                 '',
                 '',
                 '',
                 'sale',
                $this->customer_id,
                $this->business_id,
                self::PROCESSOR,
                $amount,
                '',
                $order_id);

            return $response;
        }
    }

    /**
     * @see Processor::credit()
     */
    public function credit($amount, $customer_id, $business_id, $order_id = '')
    {
        $CI = get_instance();

        $this->business_id = $business_id;
        $this->customer_id = $customer_id;

        $customer = new Customer($customer_id);
        $creditCard = CreditCard::search_aprax(array('customer_id'=>$customer_id, 'business_id'=>$business_id));
        $creditCard = $creditCard[0];

        $refNumber = explode("|", $creditCard->payer_id);
        $card_payer_id = $refNumber[1];
        $sam = $creditCard->cardNumber;
        $mpd = 'false';

        $creditCardRequest = $this->creditCardRequest;
        $creditCardRequest->setBillFirstName($customer->firstName);
        $creditCardRequest->setBillLastName($customer->lastName);
        $creditCardRequest->setBillAddressOne($creditCard->address1);
        $creditCardRequest->setBillAddressTwo($creditCard->address2);
        $creditCardRequest->setBillCity($creditCard->zip);
        $creditCardRequest->setBillStateOrProvince($creditCard->state);
        $creditCardRequest->setExpireMonth($creditCard->expMonth);
        $creditCardRequest->setExpireYear($creditCard->expYear);
        $creditCardRequest->setPurchaseOrderNumber($order_id);
        $creditCardRequest->setChargeType("CREDIT");
        $creditCardRequest->setChargeTotal($amount);
        $creditCardRequest->setManagePayerData('true');
        $creditCardRequest->setTransactionConditionCode(6); //must set tcc to 6 for MPD
        $creditCardRequest->setIndustry(self::INDUSTRY);
        $creditCardRequest->setOrderID($this->getUniqId());
        $creditCardRequest->setOrderUserID($customer_id);

        if ($card_payer_id) {
            $creditCardRequest->setPayerIdentifier($card_payer_id);
        }
        if ($sam) {
            $creditCardRequest->setSecurePrimaryAccountNumber($sam);
        }
        if (isset($card_number)) {
            $creditCardRequest->setCreditCardNumber($card_number);
        }
        if (isset($card_csc)) {
            $creditCardRequest->setCreditCardVerificationNumber($card_csc);
        }

        $response = $creditCardRequest->doTransaction();
        $result = $response->objResponseFields;

        // if the credit card is authorized, store the customer in the CIM
        if ($result['response_code']==1) {
            $status = "SUCCESS";
        } else {
            $status = "FAIL";
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status,
                $result['response_code'],
                $result['order_id'], //refNumber
                $result['response_code_text'],
                '',
                '',
                '',
                'credit',
                $this->customer_id,
                $this->business_id,
                self::PROCESSOR,
                $amount,
                '',
                $order_id);

        return $response;
    }

    public function getUniqId()
    {
        return time() * 1000 + mt_rand(0, 999);
    }
}
