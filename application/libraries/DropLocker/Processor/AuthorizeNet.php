<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;

require_once APPPATH."third_party/authorizenet_sdk/anet_php_sdk/AuthorizeNet.php";

class AuthorizeNet extends AbstractProcessor
{
    public $at_login;
    public $at_password;
    public $at_test;
    public $business_id;
    public $country = null;
    public $send_extra = false;
    public $log = false;

    const PROCESSOR = "authorizenet";

    public function __construct($params)
    {
        if (empty($params['business_id'])) {
            throw new \Exception('need to pass business ID so we can pull the AuthorizeNet credentials');
        }

        $this->business_id = $params['business_id'];
        $this->country = get_business_meta($this->business_id, 'authorizenet_country');
        $this->send_extra = get_business_meta($this->business_id, 'authorizenet_extra', 0);

        //enable logging
        if ($this->business_id == "67") {
            $this->log = true;
        }

        if (DEV) {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    public function setTestMode()
    {
        $this->at_test = true;
        $this->at_login = AUTHORIZENET_API_LOGIN_ID;
        $this->at_password = AUTHORIZENET_TRANSACTION_KEY;
    }

    public function setLiveMode()
    {
        $this->at_test = false;
        $this->at_login = get_business_meta($this->business_id, 'api_login_id');
        $this->at_password = get_business_meta($this->business_id, 'transaction_id');
    }

    /**
     * capturefunds methods is used to capture funds ie... make a payment
     *
     */
    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        //check to make sure a comma doesnt break the amount
        $amount = str_replace( ',', '.', $amount);

        $this->business_id = $business_id;

        $customer = new Customer($customer_id);
        if ($customer->autoPay == 0) {
            throw new \Exception('Customer is not on autoPay');
        }

        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        if (empty($creditCard->payer_id)) {
            throw new \Exception("Missing Processor Reference Number. Cannot perform this transaction");
        }

        $request = new \AuthorizeNetCIM($this->at_login, $this->at_password);
        if ($this->at_test) {
            $request->setSandbox(true);
        }

        $array = explode("|",$creditCard->payer_id);
        $customerProfileId = $array[0];
        $paymentProfileId = $array[1];

        // Create Auth & Capture Transaction
        $transaction = new \AuthorizeNetTransaction;
        $transaction->amount = $amount;
        $transaction->customerProfileId = $customerProfileId;
        $transaction->customerPaymentProfileId = $paymentProfileId;

        //this is needed for INTERNATIONAL business, can be set as a setting
        $extra_options_string = 'x_invoice_num=' . $order_id;
        if ($this->send_extra) {
            $extra_options_string .= "&x_first_name=" . $customer->firstName;
            $extra_options_string .= "&x_last_name=" . $customer->lastName;
            $extra_options_string .= "&x_address=" . $creditCard->address1;
            $extra_options_string .= "&x_city=" . $creditCard->city;
            $extra_options_string .= "&x_state=" . $creditCard->state;
            $extra_options_string .= "&x_zip=" . $creditCard->zip;
            $extra_options_string .= "&x_email=" . $customer->email;
            $extra_options_string .= "&x_country=" . $this->country;
        }

        $result = $request->createCustomerProfileTransaction('AuthCapture', $transaction, $extra_options_string );
        $transactionResponse = $result->getTransactionResponse();
        $transactionId = $transactionResponse->transaction_id;

        $this->log(array(
            "method"  => "makeSale()->createCustomerProfileTransaction()",
            "params"  => array(
                'AuthCapture', 
                $transaction, 
                $extra_options_string
            ), 
            "request" => $request,
            "api_url" => "https://api.authorize.net/soap/v1/Service.asmx",
            "result"  => $result
        ));
        
        // if the credit card is authorized, store the customer in the CIM
        if ($transactionResponse->approved) {
            $status = "SUCCESS";
        } else {
            $status = "FAIL";
        }
        
        if (!empty($transactionResponse->error_message)) {
            $transactionResponse->response_reason_text .= ' ' . $transactionResponse->error_message;
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status,
                                 $transactionResponse->response_code,
                                 $transactionId, //refNumber
                                 $transactionResponse->response_reason_text . $this->getCreditCardSummary($creditCard),
                                 '',
                                 '',
                                 '',
                                 'sale',
                                 $customer_id,
                                 $this->business_id,
                                 self::PROCESSOR,
                                 $amount,
                                 '',
                                 $order_id);

         return $response;
    }

    /**
     * Authorizes a card and stores in CIM
     *
     * $creditCard values
     * ----------------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * cardType
     * nameOnCard
     * address
     * city
     * state
     * zipcode
     * countryCode
     *
     * @param array $creditCard
     * @param int $order_id
     * @param int $customer_id
     * @param int $business_id
     * @return object Response
     */
    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $this->business_id = $business_id;

        // The following conditional checks to see if we are in teh dev environment. If so, then we instantiate the AutherizeNetAIM with the Droplocker developer account.
        // Otherwise, we instantiate teh AuthorizeNetAIM account with the business account.
        $transaction = new \AuthorizeNetAIM($this->at_login, $this->at_password);
        if ($this->at_test) {
            $transaction->setSandbox(true);
        }

        $transaction->amount     = '1.00';
        $transaction->card_num   = $creditCard['cardNumber'];
        $transaction->exp_date   = $creditCard['expYear']."-".$creditCard['expMonth'];

        $customer = new Customer($customer_id);

        if ($this->send_extra) {

            $transaction->first_name = $creditCard['firstName'];
            $transaction->last_name = $creditCard['lastName'];
            $transaction->address = $creditCard['address'];

            //not all international transactions will have state, if there is one set spit it out
            if ( !empty( $creditCard['state'] ) ) {
                 $transaction->state = $creditCard['state'];
            }
            $transaction->city = $creditCard['city'];
            $transaction->zip = $creditCard['zipcode'];
            $transaction->customer_ip = $_SERVER["REMOTE_ADDR"];
            $transaction->email = $customer->email;

            // TODO: this should be setting or a $creditCard field
            $transaction->country = $this->country;
        }

        $result = $transaction->authorizeOnly();
        $message = $result->response_reason_text;
        $code = $result->response_code;

        // if the credit card is authorized, store the customer in the CIM
        if ($result->approved) {
            $this->void($result->transaction_id);
            $status = "SUCCESS";

            $profile = $this->createCustomerProfile($customer, $creditCard);

            // retry the call
            if (!$profile->isOk()) {
                $profile = $this->createCustomerProfile($customer, $creditCard);
            }

            if ($profile->isOk()) {
                $refNumber = $profile->getCustomerProfileId()."|".(string) $profile->xml->customerPaymentProfileIdList->numericString;
            } else {
                $refNumber = "";
                $status = "FAIL";
                $message = $profile->getMessageText();
                $code = $profile->getMessageCode();

                $xml = htmlspecialchars(print_r($profile->xml, true));
                if (!empty($profile->xml) && ($profile->xml instanceof \SimpleXMLElement)) {
                    $xml = htmlspecialchars($profile->xml->asXML());
                }

                $body  = 'customer_id: ' . $customer_id . '<br>';
                $body .= 'order_id: '    . $order_id  . '<br>';
                $body .= 'business_id: ' . $business_id  . '<br>';
                $body .= 'message: '     . $profile->getErrorMessage() . '<br>';
                $body .= 'XML: '         . $xml;

                $supportEmail = get_business_meta($business_id, 'customerSupport');
                send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "Authorization Error: could not create Customer Profile at AthorizeNET", $body);
            }
        } else {
            $status = "FAIL";

            if (empty($message)) {
                $message = $result->error_message;
                $code = "-1";
            }
        }

        // Response will log this transaction in the transaction table
        $response = new Response($status,
            $code,
            $refNumber, //refNumber
            $message, //otherfields if necessary $result->response_reason_code . ": " . $result->response_subcode . " " .
            '',
            '',
            '',
            'auth',
            $customer_id,
            $this->business_id,
            self::PROCESSOR,
            1.00,
            '',
            $order_id);

        return $response;

    }


    /**
     * Voids a transaction
     *
     * @param string $transaction_id
     * @return TransactionResponse
     */
    public function void($transaction_id)
    {
        $transaction = new \AuthorizeNetAIM($this->at_login, $this->at_password);
        if ($this->at_test) {
            $transaction->setSandbox(true);
        }
        $void_response = $transaction->void($transaction_id);

        return $void_response;
    }

    /**
     * Creates a customer profile (CIM)
     *
     * @param Customer $customer
     * @param array $creditCard
     * @return AuthorizeNetCIM_Response
     */
    protected function createCustomerProfile($customer, $creditCard)
    {
        // Create new customer profile
        $request = new \AuthorizeNetCIM($this->at_login, $this->at_password);
        if ($this->at_test) {
            $request->setSandbox(true);
        }

        $customerProfile = new \AuthorizeNetCustomer;
        $customerProfile->description = "Customer: ". $customer->firstName." ".$customer->lastName;
        $customerProfile->merchantCustomerId = time().rand(1,100);
        $customerProfile->email = $customer->email;

        // Add payment profile.
        $paymentProfile = new \AuthorizeNetPaymentProfile;
        $paymentProfile->customerType = "individual";
        $paymentProfile->payment->creditCard->cardNumber = $creditCard['cardNumber'];
        $paymentProfile->payment->creditCard->expirationDate = sprintf("%s-%02d", $creditCard['expYear'], $creditCard['expMonth']);
        $customerProfile->paymentProfiles[] = $paymentProfile;

        return $request->createCustomerProfile($customerProfile);
    }

     /**
     * Log $data to file
     *
     * @param array $data any data
     * @return void
     */
    protected function log($data = array())
    {    
        if ($this->log) {   
            $log = "--[".date("Y-m-d H:i:s")."]--\n";
            $log .= json_encode($data) . "\n";
            $log .= "--------------------\n";
            $filename = "/tmp/authorizenet.log";
            if (!file_exists($filename)) {
                file_put_contents($filename, $log);
                chmod($filename, 0777);
            } else {
                file_put_contents($filename, $log);
            }   
        }  
    }
}
