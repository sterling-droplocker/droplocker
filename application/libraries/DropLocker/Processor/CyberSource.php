<?php

namespace DropLocker\Processor;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Business;

require_once(APPPATH."third_party/Cybersource/CyberSource_SoapClient.php");

class CyberSource extends AbstractProcessor
{

    const PROCESSOR = 'cybersource';

    public $business_id = null;

    public $mode = 'live';
    public $merchantID = null;
    public $transactionKey = null;
    public $currency = 'USD';
    public $country = 'US';

    /**
     * @var \CyberSource_SoapClient
     */
    public $client = null;

    public function setTestMode()
    {
        $this->mode = 'test';
        $this->client = new \CyberSource_SoapClient($this->merchantID, $this->transactionKey, $this->mode);
    }

    public function setLiveMode()
    {
        $this->mode = 'live';
        $this->client = new \CyberSource_SoapClient($this->merchantID, $this->transactionKey, $this->mode);
    }

    public function __construct($options)
    {
        $this->business_id = $options['business_id'];

        $this->merchantID = get_business_meta($this->business_id, 'cybersource_merchantID');
        $this->transactionKey = get_business_meta($this->business_id, 'cybersource_transactionKey');
        $this->currency = get_business_meta($this->business_id, 'cybersource_currency');
        $this->country = get_business_meta($this->business_id, 'cybersource_country', 'US');

        // Force test mode when in DEV
        if (DEV || get_business_meta($this->business_id, 'cybersource_mode') == "test") {
            $this->setTestMode();
        } else {
            $this->setLiveMode();
        }
    }

    public function authorizeCard($creditCard, $order_id, $customer_id, $business_id)
    {
        $customer = new Customer($customer_id);

        $request = new \stdClass();

        $request->merchantID = $this->merchantID;
        $request->merchantReferenceCode = $order_id;

        // Environment Info
        $request->clientLibrary = "PHP";
        $request->clientLibraryVersion = phpversion();
        $request->clientEnvironment = php_uname();


        // Create Customer Profile
        $request->paySubscriptionCreateService = new \stdClass();
        $request->paySubscriptionCreateService->run = "true";

        // Set Customer Profile Type
        $request->recurringSubscriptionInfo = new \stdClass();
        $request->recurringSubscriptionInfo->frequency = 'on-demand';

        // Billing info
        $request->billTo = new \stdClass();
        $request->billTo->firstName = $creditCard['firstName'];
        $request->billTo->lastName = $creditCard['lastName'];
        $request->billTo->street1 = $creditCard['address'];
        $request->billTo->city = $creditCard['city'];
        $request->billTo->state = $creditCard['state'];
        $request->billTo->postalCode = $creditCard['zipcode'];

        $request->billTo->email = $customer->email;
        $request->billTo->country = $this->country;

        // Card info
        $request->card = new \stdClass();
        $request->card->accountNumber = $creditCard['cardNumber'];
        $request->card->expirationMonth = $creditCard['expMonth'];
        $request->card->expirationYear = $creditCard['expYear'];
        $request->card->cvNumber = $creditCard['csc']; // optional
        $request->card->cardType = $this->getCardType($creditCard);

        // Total Amount
        $request->purchaseTotals = new \stdClass();
        $request->purchaseTotals->currency = $this->currency;
        $request->purchaseTotals->grandTotalAmount = "0.00";

        $reply = $this->client->runTransaction($request);

        if ($reply->decision == 'ACCEPT') {
            $status = 'SUCCESS';
            $refNumber = $reply->paySubscriptionCreateReply->subscriptionID;
        } else {
            $status = 'FAIL';
            $refNumber = '';
        }

        $response = new Response(
            $status,
             $reply->reasonCode,
             $refNumber,
             $reply->decision,
             '',
             '',
             '',
             'auth',
             $customer_id,
             $business_id,
             self::PROCESSOR,
             0,
             '',
             $order_id
        );

        return $response;
    }

    public function makeSale($amount, $order_id, $customer_id, $business_id)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }

        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        }

        if (!is_numeric($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' must be numeric.");
        }

        if (!is_numeric($amount)) {
            throw new \InvalidArgumentException("'amount' must be numeric.");
        }

        $CI = get_instance();

        $customer = new Customer($customer_id);
        $creditCard = CreditCard::search(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id
        ));

        $refNumber = $creditCard->payer_id;


        $request = new \stdClass();

        $request->merchantID = $this->merchantID;
        $request->merchantReferenceCode = $order_id;

        // Environment Info
        $request->clientLibrary = "PHP";
        $request->clientLibraryVersion = phpversion();
        $request->clientEnvironment = php_uname();

        // Stored profile
        $request->recurringSubscriptionInfo = new \stdClass();
        $request->recurringSubscriptionInfo->subscriptionID = $refNumber;

        // Authorize
        $request->ccAuthService = new \stdClass();
        $request->ccAuthService->run = "true";

        // Capture
        $request->ccCaptureService = new \stdClass();
        $request->ccCaptureService->run = "true";

        // Total Amount
        $request->purchaseTotals = new \stdClass();
        $request->purchaseTotals->currency = $this->currency;
        $request->purchaseTotals->grandTotalAmount = $amount;

        $reply = $this->client->runTransaction($request);

        // if the credit card is authorized, store the customer in the CIM
        if ($reply->decision == 'ACCEPT') {
            $status = "SUCCESS";
        } else {
            $status = "FAIL";
        }

        // Response will log this transaction in the transaction table
        $response = new Response(
            $status,
            $reply->reasonCode,
            $reply->requestID,
            $reply->decision . $this->getCreditCardSummary($creditCard),
             '',
             '',
             '',
             'sale',
            $customer_id,
            $business_id,
            self::PROCESSOR,
            $amount,
            '',
            $order_id
        );

        return $response;
    }

    /**
     * Gets the correct credit card code for the card type
     *
     * @param array $creditCard
     * @return string
     */
    public function getCardType($creditCard)
    {
        $cardTypes = array(
            'Visa' => '001',
            'Mastercard' => '002',
            'Amex' => '003',
            'Discover Card' => '004',
            'Diners Club' => '005',
            'Diners Club/Carte Blanche' => '005',
            'Carte Blanche' => '006',
            'JCB' => '007',
            'enRoute' => '005',
        );

        if (isset($cardTypes[$creditCard['cardType']])) {
            return $cardTypes[$creditCard['cardType']];
        }

        return '000';
    }

}
