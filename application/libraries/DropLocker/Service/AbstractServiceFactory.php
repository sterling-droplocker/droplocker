<?php

namespace  DropLocker\Service;

abstract class AbstractServiceFactory
{

    /**
     * Gets an instance of a service configured for this business
     *
     * @param string $service
     * @param int $business_id
     * @return AbstractService
     */
    public static function getService($service, $business_id)
    {
        $services = static::getServices();

	if (!class_exists($services[$service]['class'])) {
		throw new \Exception("Missing Class {$services[$service]['class']}");
	}

        return new $services[$service]['class']($business_id);
    }

    /**
     * Get the list of services
     *
     * @return array
     */
    public static function listServices()
    {
        $list = array();
        foreach (static::getServices() as $key => $service) {
            $list[$key] = $service['name'];
        }

        return $list;
    }

    /**
     * Get the list of services
     *
     * @return array
     */
    abstract static public function getServices();
}
