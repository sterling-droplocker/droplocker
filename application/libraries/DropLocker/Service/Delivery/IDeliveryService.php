<?php

namespace  DropLocker\Service\Delivery;

abstract class IDeliveryService
{

    public $business_id = null;

    abstract public function __construct($business_id);

    /**
     * Creates a pickup for a claim
     *
     * If $drop_address is null, the default "pickup_address" will be used as destination
     *
     * @param int $claim_id ID of the claim to be delivered
     * @param string $drop_address the location where the claim will be dropped
     * @return string the pikcup id
     */
    abstract public function pickup($claim_id, $drop_address = null);

    /**
     * Creates a delivery for the order
     *
     * If $order_ids is an array, this will delivery all the orders into the same
     * delivery request.
     *
     * @param  array  $order_ids  IDs of the order to be delivered
     * @return string the delivery id
     */
    abstract public function delivery($order_ids);

    /**
     * Gets the last error
     * @return object
     */
    abstract public function getError();

    /**
     * Gets the error message from the last error
     * @return string
     */
    abstract public function getErrorMessage();

}