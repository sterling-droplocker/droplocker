<?php

namespace  DropLocker\Service\Delivery;

use DropLocker\Service\AbstractServiceFactory;

class Factory extends AbstractServiceFactory
{
    /**
     * Gets an instance of a delivery service configured for this business
     *
     * @param string $service
     * @param int $business_id
     *
     * @return \DropLocker\Service\Delivery\IDeliveryService
     */
    public static function getService($service, $business_id)
    {
        // overwritten to get proper autocompletion using doc block
        return parent::getService($service, $business_id);
    }

    /**
     * Get the list of services
     *
     * @return array
     */
    static function getServices()
    {
        return array(
            'rickshaw' => array(
                'class' => '\DropLocker\Service\Delivery\Rickshaw',
                'name' => 'Rickshaw',
            ),
        );
    }
}