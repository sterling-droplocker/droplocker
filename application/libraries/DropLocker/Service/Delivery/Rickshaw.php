<?php

namespace  DropLocker\Service\Delivery;

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\ClaimExternalPickup;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderExternalDelivery;

class Rickshaw extends \DropLocker\Service\Delivery\IDeliveryService
{

    /**
     * The rest client to Rickshaw API
     * @var \DropLocker\RestClient\RickshawAPI
     */
    protected $api = null;

    /**
     * Used to track sandbox state, since Rickshaw's API does not have a sandbox yet
     * @var bool
     */
    protected $sandbox = false;

    /**
     * Default business settings
     * @var array
     */
    protected $settings = array();

    public $lastResponse = null;

    public function __construct($business_id)
    {
        $api_key = get_business_meta($business_id, 'rickshaw_api_key');
        $this->api = new \DropLocker\RestClient\RickshawAPI($api_key);
        $this->sandbox = DEV;
        $this->business_id = $business_id;

        $this->settings = array(
            'contact_name'      => get_business_meta($business_id, 'rickshaw_contact_name'),
            'contact_phone'     => get_business_meta($business_id, 'rickshaw_contact_phone'),

            'pickup_min_time'   => get_business_meta($business_id, 'rickshaw_pickup_min_time'),
            'pickup_max_time'   => get_business_meta($business_id, 'rickshaw_pickup_max_time'),
            'pickup_address'    => get_business_meta($business_id, 'rickshaw_pickup_address'),
            'pickup_notes'      => get_business_meta($business_id, 'rickshaw_pickup_notes'),

            'delivery_min_time' => get_business_meta($business_id, 'rickshaw_delivery_min_time'),
            'delivery_max_time' => get_business_meta($business_id, 'rickshaw_delivery_max_time'),
            'delivery_address'  => get_business_meta($business_id, 'rickshaw_delivery_address'),
            'delivery_notes'    => get_business_meta($business_id, 'rickshaw_delivery_notes'),
        );
    }

    public function pickup($claim_id, $drop_address = null)
    {
        // check for a previus pickup
        $existingPickup = ClaimExternalPickup::search(array(
            'claim_id' => $claim_id,
            'disabled' => 0
        ));
        if ($existingPickup) {
            $service = $existingPickup->service;
            $date = convert_from_gmt_aprax($existingPickup->dateCreated, 'Y-m-d H:i:s');
            $id = $existingPickup->third_party_id;

            $this->lastResponse = (object) array(
                'claim_id' => array("Claim $claim_id was already sent to {$service} on {$date}, ID: {$id}"),
            );

            return false;
        }

        $claim    = new Claim($claim_id);
        $customer = new Customer($claim->customer_id);
        $locker   = new Locker($claim->locker_id);
        $location = new Location($locker->location_id);

        $orig_address = $location->address . ', ' . $location->city . ', ' . $location->state;
        $orig_address = $this->cleanupAddress($orig_address);
        $orig_name    = $customer->firstName . ' ' . $customer->lastName;
        $orig_phone   = $customer->phone;
        $orig_asset   = $this->getKeyNumber($location->accessCode);

        // Unit is stored in lockerName
        $orig_instructions  = $locker->lockerName . "\n" . $location->accessCode;

        $description = "Dirty laundry from {$customer->firstName} {$customer->lastName}. #$claim_id";
        $foreign_id = "C-$claim_id";

        $times = $this->getPickupTimes();

        $drop_address = $drop_address ? $drop_address : $this->settings['pickup_address'];

        $params = array(
            'orig_min'          => $times['min'],
            'orig_address'      => $orig_address,
            'orig_name'         => $orig_name,
            'orig_phone'        => $orig_phone,
            'orig_asset'        => $orig_asset,
            'orig_instructions' => $orig_instructions,

            'dest_max'          => $times['max'],
            'dest_address'      => $drop_address,
            'dest_name'         => $this->settings['contact_name'],
            'dest_phone'        => $this->settings['contact_phone'],
            'dest_instructions' => $this->settings['pickup_notes'],

            'description'       => $description,
            'foreign_id'        => $foreign_id,
        );

        // Rickshaw's API does not have a sandbox yet
        if ($this->sandbox) {
            $params['test_flag'] = 1;
        }

        $response = $this->api->post('/parcels', $params);
        $this->lastResponse = $response;

        // error
        if (empty($response->uuid)) {
            return false;
        }

        $externalPickup = new ClaimExternalPickup();
        $externalPickup->service = 'rickshaw';
        $externalPickup->claim_id = $claim_id;
        $externalPickup->third_party_id = $response->uuid;
        $externalPickup->dateCreated = new \DateTime();
        $externalPickup->response = json_encode($response);
        $externalPickup->drop_address = $drop_address;
        $externalPickup->disabled = 0;
        $externalPickup->save();

        return $response->uuid;
    }

    public function delivery($order_ids)
    {
        // support array of orders
        if (!is_array($order_ids)) {
            $order_ids = array($order_ids);
        }

        foreach ($order_ids as $order_id) {
            // check for a previus delivery
            $existingDelivery = OrderExternalDelivery::search(array(
                'order_id' => $order_id,
                'disabled' => 0,
            ));

            if ($existingDelivery) {
                $service = $existingDelivery->service;
                $date = convert_from_gmt_aprax($existingDelivery->dateCreated, 'Y-m-d H:i:s');
                $id = $existingDelivery->third_party_id;

                $this->lastResponse = (object) array(
                    'order_id' => array("Order $order_id was already sent to {$service} on {$date}, ID: {$id}"),
                );

                return false;
            }
        }

        // sanitize to int
        $order_ids = array_map('intval', $order_ids);
        $id_list = implode(",", $order_ids);

        // get all orders
        $orders = Order::search_aprax(array(
        	"orderID IN ($id_list)" => null
        ));

        // count by orderType
        $type_counters = array();
        foreach ($orders as $order) {
            if (!isset($type_counters[$order->orderType])) {
                $type_counters[$order->orderType] = 0;
            }
            $type_counters[$order->orderType]++;
        }

        foreach ($type_counters as $type => $count) {
            $type_counters[$type] = $count > 1 ? "$count bags of $type" : "$count bag of $type";
        }


        // pickup first order_id for the data
        $order_id = $order_ids[0];

        $order    = new Order($order_id);
        $customer = new Customer($order->customer_id);
        $locker   = new Locker($order->locker_id);
        $location = new Location($locker->location_id);


        $dest_name    = $customer->firstName . ' ' . $customer->lastName;
        $dest_phone   = $customer->phone;
        $dest_address = $location->address . ', ' . $location->city . ', ' . $location->state;
        $dest_address = $this->cleanupAddress($dest_address);
        $dest_asset   = $this->getKeyNumber($location->accessCode);

        // Unit is stored in lockerName
        $dest_instructions  = $locker->lockerName . "\n" . $location->accessCode;

        $foreign_id = "O-" . implode(", O-", $order_ids);
        $description = "Clean laundry: " . implode(", ", $type_counters) . " for {$customer->firstName} {$customer->lastName}";

        $times = $this->getDeliveryTimes();

        $params = array(
            'orig_min'          => $times['min'],
            'orig_address'      => $this->settings['delivery_address'],
            'orig_name'         => $this->settings['contact_name'],
            'orig_phone'        => $this->settings['contact_phone'],
            'orig_instructions' => $this->settings['delivery_notes'],

            'dest_max'          => $times['max'],
            'dest_address'      => $dest_address,
            'dest_name'         => $dest_name,
            'dest_phone'        => $dest_phone,
            'dest_instructions' => $dest_instructions,
            'dest_asset'        => $dest_asset,

            'description'       => $description,
            'foreign_id'        => $foreign_id,
        );

        // Rickshaw's API does not have a sandbox yet
        if ($this->sandbox) {
            $params['test_flag'] = 1;
        }

        $response = $this->api->post('/parcels', $params);
        $this->lastResponse = $response;

        // error
        if (empty($response->uuid)) {
            return false;
        }

        // save same delivery foreach order_id
        foreach ($order_ids as $order_id) {
            $externalDelivery = new OrderExternalDelivery();
            $externalDelivery->service = 'rickshaw';
            $externalDelivery->order_id = $order_id;
            $externalDelivery->third_party_id = $response->uuid;
            $externalDelivery->dateCreated = new \DateTime();
            $externalDelivery->response = json_encode($response);
            $externalDelivery->disabled = 0;
            $externalDelivery->save();
        }

        return $response->uuid;
    }

    public function getError()
    {
        if (empty($this->lastResponse)) {
            return false;
        }

        if (!empty($this->lastResponse->uuid)) {
            return false;
        }

        return empty($this->lastResponse->uuid) ? $this->lastResponse : false;
    }

    public function getErrorMessage()
    {
        if (!$error = $this->getError()) {
            return '';
        }

        $message = '';
        $errors = (array)$this->lastResponse;
        foreach ($errors as $key => $value) {
            $message .= "$key: ".implode(" ", $value)."\n";
        }

        return $message;
    }

    protected function getKeyNumber($accessCode)
    {
        $matches = array();
        if (preg_match('/keys?\s*(\d+)/i', $accessCode, $matches)) {
            return $matches[1];
        }

        return '';
    }

    protected function cleanupAddress($address)
    {
        $address = preg_replace('/\(.*\)/', '', $address);
        $address = preg_replace('/\/[^,]+/', '', $address);
        return $address;
    }

    protected function getPickupTimes()
    {
        $max = strtotime($this->settings['pickup_max_time']);
        if ($max < time()) {
            $max += 86400;
        }
        $min = strtotime($this->settings['pickup_min_time'], $max);

        $max = gmdate('Y-m-d\TH:i:sO', $max);
        $min = gmdate('Y-m-d\TH:i:sO', $min);

        return compact('min', 'max');
    }

    protected function getDeliveryTimes()
    {
        $max = strtotime($this->settings['delivery_max_time']);
        if ($max < time()) {
            $max += 86400;
        }
        $min = strtotime($this->settings['delivery_min_time'], $max);

        $max = gmdate('Y-m-d\TH:i:sO', $max);
        $min = gmdate('Y-m-d\TH:i:sO', $min);

        return compact('min', 'max');
    }

}