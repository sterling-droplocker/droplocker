<?php

namespace  DropLocker\Service\HomeDelivery;

use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\Customer;

abstract class IHomeDeliveryService
{

    public $business_id = null;

    abstract public function __construct($business_id);

    /**
     * Gets the avalialble pickup windows
     *
     * @param object $homePickup
     * @return array
     */
    abstract public function getPickupWindows($homePickup);

    /**
     * Sends the request to schedule a HomePickup
     * @param object $homePickup
     * @return App\Libraries\DroplockerObjects\OrderHomePickup
     */
    abstract public function scheduleHomePickup($homePickup);

    /**
     * Gets the avalialble delivery windows
     *
     * @param object $homeDelivery
     * @return array
     */
    abstract public function getDeliveryWindows($homeDelivery);

    /**
     * Sends the request to schedule a HomeDelivery
     *
     * @param object $homePickup
     * @return App\Libraries\DroplockerObjects\OrderHomeDelivery
     */
    abstract public function scheduleHomeDelivery($homeDelivery);

    /**
     * Get's the locations a customer can request home delivery sorted by distance
     *
     * @param object $customer
     * @return array
     */
    abstract public function getLocationAvailablesForCustomer($customer);

    /**
     * Checks is a customer is eligible for home delivery
     *
     * @param object $customer
     * @return bool
     */
    abstract public function canCustomerRequestHomeDelivery($customer);

    /**
     * Checks is a location is eligible for home delivery by a customer
     * @param object $customer
     * @param object $location
     * @return bool
     */
    abstract public function canCustomerRequestHomeDeliveryToLocation($customer, $location);

    /**
     * Gets the last error
     * @return object
     */
    abstract public function getError();

    /**
     * Gets the error message from the last error
     * @return string
     */
    abstract public function getErrorMessage();

    /**
     * Cancels a home pickup
     * @param App\Libraries\DroplockerObjects\OrderHomePickup $homePickup
     */
    abstract public function cancelPickup($homePickup);

    /**
     * Cancels a home delivery
     * @param App\Libraries\DroplockerObjects\OrderHomeDelivery $homeDelivery
     */
    abstract public function cancelDelivery($homeDelivery);

    /**
     * If found, adds the order to an existing home delivery
     *
     * @param object $order
     */
    public function addOrderToExisingHomeDelivery($order)
    {
        // search for existing home delivery object for this order
        $currentHomeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $order->orderID,
            'delivery_id != ' => '',
        ));

        // if this order has a home delivery, exit
        if ($currentHomeDelivery) {
            return array(
                'status' => 'fail',
                'message' => 'Home delivery already scheduled for this order.',
            );
        }

        $customer = new Customer($order->customer_id);

        $existingHomeDelivery = $this->getExistingHomeDelivery($order);

        // there is no previous home delivery, customer needs to schedule it
        if (!$existingHomeDelivery) {
            return array(
                'status' => 'fail',
                'message' => 'No existing home delivery found.',
            );
        }

        // get previus response
        $delivery_response = json_decode($existingHomeDelivery->delivery_response);

        // delegate to subclases to retrieve the reference ids
        $foreign_id = $this->getIdsReferenceFromRequest($delivery_response);

        // parse for already included orders in this home delivery
        $order_ids = array();
        $orders = array();
        foreach (explode(',', $foreign_id) as $id) {
            list($type,$order_id) = explode('-', $id);
            $order_ids[] = $order_id;
            $orders[] = new Order($order_id);
        }
        $order_ids[] = $order->orderID;
        $orders[] = $order;

        // get data to update the delivery request
        $description = $this->getDeliveryDescription($orders, $customer);
        $foreign_id = "O-" . implode(", O-", $order_ids);

        // get existing home delivery object for this order
        $homeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $order->orderID,
        ));

        // or create a home delivery object for this order
        if (!$homeDelivery) {
            $data = (array) $existingHomeDelivery;
            unset($data['orderHomeDeliveryID']);
            unset($data['delivery_id']);
            $data['order_id'] = $order->orderID;
            $homeDelivery = OrderHomeDelivery::create($data);
        }

        $homeDelivery = $this->updateHomeDeliveryRequest($existingHomeDelivery->delivery_id, $homeDelivery, $description, $foreign_id, $orders);

        return array(
            'status' => 'success',
            'message' => 'Order appended to existing home delivery request.',
            'data' => $homeDelivery,
        );
    }

    /**
     * Updates a home delivery to include new orders
     *
     * @param string $deliver_id
     * @param object $homeDelivery
     * @param string $description
     * @param string $foreign_id
     * @param array $orders
     */
    abstract protected function updateHomeDeliveryRequest($deliver_id, $homeDelivery, $description, $foreign_id, $orders);

    /**
     * Extracts the ids sting from a delivery response
     *
     * @param object $response
     * @return sting
     */
    abstract protected function getIdsReferenceFromRequest($response);

    /**
     * Gets an existing home delivery request for the same user
     *
     * @param object $order
     */
    protected function getExistingHomeDelivery($order)
    {
        $CI = get_instance();

        $locker = new Locker($order->locker_id);
        $dateLimit = $this->getDeliveryUpdateTimeLimit(); // Only find deliveries that can be updated

        // Search for an active home delivery for same customer and location
        $sql = "SELECT orderHomeDelivery.* FROM orderHomeDelivery
                JOIN orders on (orderID = order_id)
                WHERE orderStatusOption_id != 10
                AND service = ?
                AND orderHomeDelivery.customer_id = ?
                AND delivery_id != ''
                AND location_id = ?
                AND windowStart > ?
                ORDER BY orderHomeDeliveryID DESC";

        return $CI->db->query($sql, array(
            $this->getServiceName(),
            $order->customer_id,
            $locker->location_id,
            $dateLimit,
        ))->row();
    }

    /**
     * Gets the time limit to update requests
     *
     * Delivery requests can't be updated if their pickup window is before this time
     *
     * @return integer
     */
    protected function getDeliveryUpdateTimeLimit()
    {
        return gmdate('Y-m-d H:i:s', time() + 3600);  // Only find deliveries requests that are 1 hour away
    }

    /**
     * Gets the service name
     * @return string
     */
    public function getServiceName()
    {
        return end(explode('\\', strtolower(get_class($this))));
    }

    /**
     * Groups windows into different fee groups
     *
     * @param array $windows_by_date
     * @return array
     */
    public function splitWindowsByFees($windows_by_date)
    {
        // create groups based on the fees
        foreach ($windows_by_date as $date => $windows) {

            // working group
            $current_group = null;
            foreach ($windows as $window) {

                // if we are in a group
                if ($current_group) {
                    // if the window is outside the group's hour range, start a new group
                    if ($window['fee'] != $current_group['fee']) {

                        // save the group and reset current_group
                        $pickup_windows[] = $current_group;
                        $current_group = null;
                    }
                }

                // If we don't have a group yet, create one
                if (empty($current_group)) {
                    $current_group = array(
                        'fee' => $window['fee'],
                        'date' => $date,
                        'items' => array(),
                    );
                }

                // append window to current group
                $current_group['items'][] = $window;

            }

            // if the last formed group is not empty, save it
            if ($current_group) {
                $pickup_windows[] = $current_group;
            }
        }

        return $pickup_windows;
    }

    /**
     * Generates a description for the home delivery
     *
     * @param array $orders
     * @param Customer $customer
     * @return string
     */
    protected function getDeliveryDescription($orders, $customer)
    {
        $type_counters = array();
        foreach ($orders as $order) {
            if (!isset($type_counters[$order->orderType])) {
                $type_counters[$order->orderType] = 0;
            }
            $type_counters[$order->orderType]++;
        }

        foreach ($type_counters as $type => $count) {
            $type_counters[$type] = $count > 1 ? "$count bags of $type" : "$count bag of $type";
        }
        $description = "Delivering: " . implode(", ", $type_counters) . " for {$customer->firstName} {$customer->lastName}";

        return $description;
    }

}