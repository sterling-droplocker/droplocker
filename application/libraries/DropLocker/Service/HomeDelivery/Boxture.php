<?php
namespace  DropLocker\Service\HomeDelivery;

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\OrderStatus;

class Boxture extends \DropLocker\Service\HomeDelivery\IHomeDeliveryService
{

    /**
     * The rest client to Rickshaw API
     * @var \DropLocker\RestClient\BoxtureAPI
     */
    protected $api = null;

    /**
     * send test requests to Boxture
     * @var bool
     */
    protected $sandbox = false;

    protected $lastResponse = null;

    protected $business = null;
    protected $pickupCutOffTime = '20:00';
    protected $deliveryCutOffTime = '20:00';

    //TODO: get service_type_id from settings
    protected $pickup_service_type_id = 'e4282c5b-bb16-4ea5-92c2-62481bc0c810';
    protected $delivery_service_type_id = 'b9a78646-0609-4d33-8d0a-7ab9c07cfd14';

    public function __construct($business_id)
    {
        $this->business_id = $business_id;
        $this->business = new Business($business_id);
        $this->sandbox = get_business_meta($this->business_id, "boxture_mode") == 'test';

        $api_key = $this->sandbox?get_business_meta($business_id, 'boxture_qa_api_key'):get_business_meta($business_id, 'boxture_api_key');

        $this->api = new \DropLocker\RestClient\BoxtureAPI($api_key, $this->sandbox);
        $this->api->url = get_business_meta($business_id, 'boxture_api_url');
        $this->api->sandbox_url   = get_business_meta($business_id, 'boxture_qa_api_url');  

        $this->settings = array();
    }

    public function getError()
    {
        // or boxture lastReponse and request_response properties must be valid json strings. 
        // If not, we have an error.
        $request_response = json_decode($this->api->lastRequest['response']);
        if (
            empty($this->lastResponse) || 
            ($this->lastResponse == '') || 
            empty($request_response) || 
            ($request_response == '') ||
            $this->api->isError()
            ) {
            return "HTTP Error " . $this->api->lastRequest['http_status'];
        }

        return false;
    }

    public function getErrorMessage()
    {
        if (!$error = $this->getError()) {
            return 'Uknown error';
        }

        $message = '';
        $errors = (array)$this->lastResponse;
        foreach ($errors as $key => $value) {
            $message .= "$key: ".implode(" ", $value)."\n";
        }

        return $message;
    }

    public function canCustomerRequestHomeDelivery($customer)
    {
        $validLocations = $this->getLocationAvailablesForCustomer($customer);

        if (empty($validLocations)) {
            return false;
        }

        $this->lastResponse = $this->api->get('/address/' . urlencode("{$customer->address1}, {$customer->city}, {$customer->state}"));

        if (!empty($this->lastResponse->geocode_errors)) {
            return false;
        }

        return true;
    }

    public function canCustomerRequestHomeDeliveryToLocation($customer, $location)
    {
        // location does not have zip codes
        if (empty($location->zip_codes)) {
            return false;
        }

        // customer does not have zip code
        if (empty($customer->zip)) {
            return false;
        }

        $zipcode = $this->formatZipCode($customer->zip);
        $zipcodes = explode(",", $location->zip_codes);

        //simple search by zipcode in zipcodes array
        if (in_array($zipcode, $zipcodes)) {
            return true;
        }

        //get only first 4 numbers of zipcode and search for them in array
        if (in_array(substr($zipcode, 0, 4), $zipcodes)) {
            return true;
        }

        //no match
        return false;
    }

    public function getLocationAvailablesForCustomer($customer)
    {
        $CI = get_instance();

        $radius_query  = "SELECT * FROM location
            JOIN locker ON (location_id = locationID AND lockerStyle_id = 5 AND lockerStatus_id = 1)
            WHERE business_id = ?
            AND status = 'active'
            AND serviceType != 'not in service'
            AND (FIND_IN_SET(?, zip_codes) OR FIND_IN_SET(?, zip_codes))
        ";

        $result = $CI->db->query($radius_query, array($this->business_id, $this->formatZipCode($customer->zip), substr($customer->zip, 0, 4)))->result();

        return $result;
    }

    public function scheduleHomePickup($homePickup)
    {
        if ($homePickup->pickup_id) {
            throw new \Exception("Pickup already scheduled");
        }

        $location = new Location($homePickup->location_id);
        $customer = new Customer($homePickup->customer_id);
        $locker = new Locker($homePickup->locker_id);

        $all_windows = json_decode($homePickup->time_windows_cache);

        if (empty($all_windows->{$homePickup->pickup_window_id})) {
            throw new \Exception("boxture:Invalid Pickup Window");
        }
        $pickupWindow = $all_windows->{$homePickup->pickup_window_id};

        $orig_instructions = $homePickup->address2;
        $orig_instructions .= "\nPhone: " . $customer->phone;
        if (strlen($homePickup->notes)) {
            $orig_instructions .= "\nCustomer Notes: " . $homePickup->notes;
        }

        $claim_id = $homePickup->claim_id;

        $description = "Pickup for {$customer->firstName} {$customer->lastName}. #$claim_id";
        $foreign_id = "C-$claim_id";

        // latest acceptable dropoff time
        $dest_time = strtotime($pickupWindow->ends_at); // set to last time of the day
        $dest_max = gmdate('Y-m-d\TH:i:sO', $dest_time + 3600);


        $request["shipment"] = array(
            'human_id'          => $pickupWindow->human_id,
            'origin'            => array(
                'comments'  => $orig_instructions,
                'time_window' => array(
                    'start' => $pickupWindow->starts_at,
                    'end'   => $pickupWindow->ends_at
                ),
            ),
            'destination'       => array(
                'comments'  => '',
                'time_window' => array(
                    'start' => $pickupWindow->ends_at,
                    'end'   => $dest_max
                ),
            )
        );

        if (!empty($homePickup->quantity)) {
            $request["shipment"]["quantity"] = $homePickup->quantity;
        }

        $this->lastResponse = $this->api->post('/shipments', $request);

        if ($this->getError()) {
            $homePickup->pickup_response = json_encode($this->api->lastRequest);
            $homePickup->time_windows_cache = '';
            $homePickup->save();
            if (!empty($this->lastResponse->errors)) {
                $errors_html = $this->parseErrors($this->lastResponse->errors);
                throw new \Exception($errors_html);
            }
            throw new \Exception($this->getErrorMessage());
        }

        $homePickup->pickup_id = $this->lastResponse->shipment->id;
        $homePickup->pickup_response = $this->api->lastRequest['response'];
        $homePickup->trackingCode = $this->lastResponse->shipment->human_id;
        $homePickup->deliver_by = new \DateTime($request_date);
        $homePickup->fee = empty($pickupWindow->fee)?0:$pickupWindow->fee;
        $homePickup->service = $this->getServiceName();
        $homePickup->save();

        return $homePickup;
    }

    protected function getKeyNumber($accessCode)
    {
        $matches = array();
        if (preg_match('/keys?\s*(\d+)/i', $accessCode, $matches)) {
            return $matches[1];
        }

        return '';
    }

    protected function cleanupAddress($address)
    {
        $address = preg_replace('/\(.*\)/', '', $address);
        $address = preg_replace('/\/[^,]+/', '', $address);
        return $address;
    }

    public function getPickupWindows($homePickup)
    {

        $all_windows = $this->getAllPickupWindows($homePickup);
        return $all_windows;
    }

    protected function formatAddress($address, $address2 = false)
    {
        preg_match_all('/\d+/', $address, $matches);
        $street_number = $matches[0][0];
        $street_name = trim(str_replace($street_number, "", $address));
        if ($address2) {
            return array(
                "number" => $address2,
                "street"   => $street_name
            );
        }
        return array(
            "number" => (int)$street_number,
            "street"   => $street_name
        );
    }

    protected function getAllPickupWindows($homePickup)
    {
        $CI = get_instance();
        $quantity = $CI->input->get("quantity");

        $last_request_error = $this->getLastRequestError($homePickup);
        if ($last_request_error || $quantity) {
            $homePickup->pickup_window_id = null;
            $homePickup->time_windows_cache = null;
            $homePickup->pickup_response = null;
        }

        if (empty($homePickup->pickup_window_id)) {
            $homePickup->save();
        }

        //check if cache exists and if it is valid
        if (!empty($homePickup->time_windows_cache)) {
            $formated_windows = $this->formatByDate(json_decode($homePickup->time_windows_cache), "pickup");
            if ($formated_windows) {
                return $formated_windows;
            }
        }

        $customer = new Customer($homePickup->customer_id);
        $description = "Pickup for customer {$customer->firstName} {$customer->lastName}";
        
        $post_data = array();

        /* Pickup service address */
        $post_data["shipment_quote"]["service_type_id"] = $this->pickup_service_type_id;
        $post_data["shipment_quote"]["quantity"] = empty($quantity)?1:$quantity;
        $post_data["shipment_quote"]["origin"]["comments"] = $description;
        $post_data["shipment_quote"]["origin"]["iso_country_code"] = "NL";
        $post_data["shipment_quote"]["origin"]["locality"] = $customer->city;
        $post_data["shipment_quote"]["origin"]["postal_code"] = $this->formatZipCode($customer->zip);
        $address = $this->formatAddress($customer->address1, $customer->address2);
        $post_data["shipment_quote"]["origin"]["sub_thoroughfare"] = $address["number"];
        $post_data["shipment_quote"]["origin"]["thoroughfare"] = $address["street"];
        $post_data["shipment_quote"]["origin"]["email"] = $customer->email;
        //TODO: valid customer phone
        $post_data["shipment_quote"]["origin"]["mobile"] = trim($customer->phone);
        $post_data["shipment_quote"]["origin"]["contact"] = $customer->firstName . " " . $customer->lastName;

        //TODO: hardcoded iso_country_code
        $homeDeliveryLocation = $this->getLocationAvailablesForCustomer($customer);

        /* Pickup customer address */
        $post_data["shipment_quote"]["destination"]["iso_country_code"] = "NL";
        $post_data["shipment_quote"]["destination"]["locality"] = $homeDeliveryLocation[0]->city;
        $post_data["shipment_quote"]["destination"]["postal_code"] = $homeDeliveryLocation[0]->zipcode;

        $address = $this->formatAddress($homeDeliveryLocation[0]->address);
        $post_data["shipment_quote"]["destination"]["sub_thoroughfare"] = $address["number"];
        $post_data["shipment_quote"]["destination"]["thoroughfare"] = $address["street"];
        $post_data["shipment_quote"]["destination"]["email"] = '';
        $post_data["shipment_quote"]["destination"]["mobile"] = '';

        $this->lastResponse = $this->api->post('/shipment_quotes', $post_data);
        if (!empty($this->lastResponse->errors) || !empty($this->lastResponse->error)) {
            $errors_html = $this->parseErrors($this->lastResponse->errors);
            if (!empty($this->lastResponse->error)) {
                $errors_html = $this->lastResponse->error;
            }
            throw new \Exception($errors_html);
        }

        $service_types = $this->lastResponse->shipment_quote->service_types;

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        $windows_by_id = array();
        foreach ($service_types as $st) {
            if ($st->purpose == "dropoff") {
                continue;
            }
            foreach($st->time_windows as $tw) {
                $id = $tw->id;
                $windows_by_id[$id] = (object) array(
                    'id' => $id,
                    'starts_at' => gmdate('c', strtotime($tw->start)),
                    'ends_at' => gmdate('c', strtotime($tw->end)),
                    //'expires_at' => gmdate('c', strtotime($tw->start) - 3600 * 3),
                    //TODO: check this
                    'expires_at' => gmdate('c', strtotime($tw->end)),
                    'fee' => '',
                    'notes' => '',
                    'human_id' => $this->lastResponse->shipment_quote->human_id
                );
            }
        }

        if (!empty($windows_by_id) ) {
            uasort($windows_by_id, function($a, $b) {
                return $a->starts_at > $b->starts_at;
            });
            $homePickup->time_windows_cache = json_encode($windows_by_id);
            $homePickup->save();
        }

        $formated_windows = $this->formatByDate($windows_by_id, "pickup");
        return $formated_windows;
    }

    protected function formatByDate($all_windows, $purpose)
    {
        // group windows by date
        $windows_by_date = array();
        foreach ($all_windows as $window) {
            
            if (array_key_exists('show_to_customer', $window)) {
                if (empty($window->show_to_customer)) continue;
            }

            if (!empty($window->type)) {
                if ($purpose == "delivery" && $window->type == "pickup") {
                    continue;
                }
            }
            
            // expired window
            if (strtotime($window->expires_at) <= time()) {
                continue;
            }

            // date in the local time
            $date = convert_from_gmt_aprax($window->ends_at, 'Y-m-d', $this->business_id);

            $start = convert_from_gmt_aprax($window->starts_at, "H:i", $this->business_id);
            $end = convert_from_gmt_aprax($window->ends_at, "H:i", $this->business_id);

            $start_day = date("D", strtotime($window->starts_at));
            if ($purpose == "dropoff") {
                if (!in_array($start_day.$start."-".$end, array("Mon18:00-21:00", "Wed18:00-21:00", "Fri18:00-21:00"))) {
                    continue;
                }
            }

            // create date array if not exists.
            if (!isset($windows_by_date[$date])) {
                $windows_by_date[$date] = array();
            }

            // save window by date
            $windows_by_date[$date][] = array(
                'id' => $window->id,
                'date' => $date,
                'fee' => $window->fee,
                'notes' => $window->notes,
                'human_id' => $window->human_id,

                // local time, for the view and group calculations
                'start' => $start,
                'end' => $end,

                // GMT time, this is the one stored in DB and used in the API
                'start_time' => gmdate('c', strtotime($window->starts_at)),
                'end_time' => gmdate('c', strtotime($window->ends_at))
            );
        }

        return empty($windows_by_date)?false:$windows_by_date;
    }

    public function getDeliveryWindows($homeDelivery)
    {
        $all_windows = $this->getAllDeliveryWindows($homeDelivery);
        return $all_windows;
    }

    public function scheduleHomeDelivery($homeDelivery)
    {
        if ($homeDelivery->delivery_id) {
            throw new \Exception("Delivery already scheduled");
        }

        $order    = new Order($homeDelivery->order_id);
        $location = new Location($homeDelivery->location_id);
        $customer = new Customer($homeDelivery->customer_id);
        $locker   = new Locker($homeDelivery->locker_id);

        $accessCode = substr(trim($customer->phone), -4); // last 4 of the customers phone

        $all_windows = json_decode($homeDelivery->time_windows_cache);
        if (empty($all_windows->{$homeDelivery->delivery_window_id})) {
            $homeDelivery->time_windows_cache = '';
            $homeDelivery->save();
            throw new \Exception("Invalid Delivery Window");
        }

        $deliveryWindow = $all_windows->{$homeDelivery->delivery_window_id};

        $orders = array($order);
        $description = $this->getDeliveryDescription($orders, $customer);

        $orig_comments .= "\nOrderID: 0-" . $homeDelivery->order_id;
        $orig_comments .= "\nName: ".$this->settings['contact_name'];
        $orig_comments .= "\nPhone: " . $this->settings['contact_phone'];
        $orig_comments .= "\nNotes: " . $this->settings['delivery_notes'];
        $orig_comments .= "\nDescription: " . $description;

        $dest_comments = "Description: Delivery for {$customer->firstName} {$customer->lastName}. #".$homeDelivery->order_id;

        //Get the origin date_times from the pickup windows
        $start_origin_time = false;
        $end_origin_time = false;
        $date_time_1 = new \DateTime($deliveryWindow->starts_at);
       //die(json_encode($all_windows));
        foreach($all_windows as $tw) {
            if ($tw->type == "dropoff") continue;
            $date_time_2 = new \DateTime($tw->starts_at);
            if ( $date_time_2 < $date_time_1 ) {
                $start_origin_time = $tw->starts_at;
                $end_origin_time = $tw->ends_at;
                break;
            }
        }

        if (!$start_origin_time || !$end_origin_time) {
            throw new \Exception("Wrong origin time windows.");
        }

        $request["shipment"] = array(
            'human_id'          => $deliveryWindow->human_id,
            'comments'          => $dest_comments,
            'origin'            => array(
                'comments'      => $orig_comments,
                'time_window'   => array(
                    'start'     => $start_origin_time,
                    'end'       => $end_origin_time
                )
            ),
            'destination'       => array(
                'comments'      => $description,
                'time_window'   => array(
                    'start'     => $deliveryWindow->starts_at,
                    'end'       => $deliveryWindow->ends_at
                )
            )
        );

        if (!empty($homeDelivery->quantity)) {
            $request["shipment"]["quantity"] = $homeDelivery->quantity;
        }

        $this->lastResponse = $this->api->post('/shipments', $request);

        if ($this->getError()) {
            $homeDelivery->delivery_response = json_encode($this->api->lastRequest);
            $homeDelivery->time_windows_cache = '';
            $homeDelivery->save();

            throw new \Exception($this->getError());
        }

        $homeDelivery->delivery_id = $this->lastResponse->shipment->id;
        $homeDelivery->delivery_response = $this->api->lastRequest['response'];
        $homeDelivery->trackingCode = $this->lastResponse->shipment->human_id;
        $homeDelivery->deliveryDate = new \DateTime($request_date);
        $homeDelivery->fee = 0;
        $homeDelivery->service = $this->getServiceName();
        $homeDelivery->save();

        return $homeDelivery;
    }

    protected function getAllDeliveryWindows($homeDelivery)
    {   
        $last_request_error = $this->getLastRequestError($homeDelivery, "delivery");
        if ($last_request_error) {
            $homeDelivery->delivery_window_id = null;
            $homeDelivery->time_windows_cache = null;
            $homeDelivery->delivery_response = null;
        }

        if (empty($homeDelivery->delivery_window_id)) {
            $homeDelivery->save();
        }

        //check if cache exists and if it is valid
        if (!empty($homeDelivery->time_windows_cache)) {
            $formated_windows = $this->formatByDate(json_decode($homeDelivery->time_windows_cache), "delivery");
            if ($formated_windows) {
                return $formated_windows;
            }
        }

        $customer = new Customer($homeDelivery->customer_id);
        $description = "Pickup for customer {$customer->firstName} {$customer->lastName}";

        $post_data = array();
        $post_data["shipment_quote"]["service_type_id"] = $this->delivery_service_type_id;
        $post_data["shipment_quote"]["quantity"] = empty($homeDelivery->quantity)?1:$homeDelivery->quantity;
        $post_data["shipment_quote"]["destination"]["comments"] = $description;
        $post_data["shipment_quote"]["destination"]["iso_country_code"] = "NL";
        $post_data["shipment_quote"]["destination"]["locality"] = $customer->city;
        $post_data["shipment_quote"]["destination"]["postal_code"] = $this->formatZipCode($customer->zip);
        $address = $this->formatAddress($customer->address1, $customer->address2);
        $post_data["shipment_quote"]["destination"]["sub_thoroughfare"] = $address["number"];
        $post_data["shipment_quote"]["destination"]["thoroughfare"] = $address["street"];
        $post_data["shipment_quote"]["destination"]["email"] = $customer->email;
        //TODO: valid customer phone
        $post_data["shipment_quote"]["destination"]["mobile"] = trim($customer->phone);
        $post_data["shipment_quote"]["destination"]["contact"] = $customer->firstName . " " . $customer->lastName;

        //TODO: hardcoded iso_country_code
        $homeDeliveryLocation = $this->getLocationAvailablesForCustomer($customer);

        $post_data["shipment_quote"]["origin"]["iso_country_code"] = "NL";
        $post_data["shipment_quote"]["origin"]["locality"] = $homeDeliveryLocation[0]->city;
        $post_data["shipment_quote"]["origin"]["postal_code"] = $homeDeliveryLocation[0]->zipcode;
        $address = $this->formatAddress($homeDeliveryLocation[0]->address);
        $post_data["shipment_quote"]["origin"]["sub_thoroughfare"] = $address["number"];
        $post_data["shipment_quote"]["origin"]["thoroughfare"] = $address["street"];
        $post_data["shipment_quote"]["origin"]["email"] = '';
        $post_data["shipment_quote"]["origin"]["mobile"] = '';

        $this->lastResponse = $this->api->post('/shipment_quotes', $post_data);

        $service_types = $this->lastResponse->shipment_quote->service_types;

        if (!empty($this->lastResponse->errors) || !empty($this->lastResponse->error)) {
            $errors_html = $this->parseErrors($this->lastResponse->errors);
            if (!empty($this->lastResponse->error)) {
                $errors_html = $this->lastResponse->error;
            }
            throw new \Exception($errors_html);
        }


        $service = get_business_meta($homeDelivery->business_id, 'home_delivery_service');

        //this is just to use when avoiding deliveries on the same day as pickups
        $orderStatus = OrderStatus::search(array(
            'order_id'              => $homeDelivery->order_id,
            'orderStatusOption_id'  => 3
        ));

        //purpose: pickup|dropoff
        $windows_by_id = array();

        //$hours_to_skip = 24;

        foreach ($service_types as $st) {
            foreach($st->time_windows as $tw) {
                //avoid deliveries during first hours_to_skip after inventoried
                //$deliveryDate = new \DateTime($tw->start);
                
                //$diff = $deliveryDate->diff($orderStatus->date);
                //$diff = $diff->h + ($diff->days*24);
                $show_to_customer = true;
                //if ($diff <= $hours_to_skip) $show_to_customer = false;

                $id = $tw->id;
                $windows_by_id[$id] = (object) array(
                    'id'               => $id,
                    'starts_at'        => $tw->start, //gmdate('c', strtotime($tw->start)),
                    'ends_at'          => $tw->end, //gmdate('c', strtotime($tw->end)),
                    'expires_at'       => gmdate('c', strtotime($tw->end)),
                    'fee'              => '',
                    'notes'            => '',
                    'human_id'         => $this->lastResponse->shipment_quote->human_id,
                    'type'             => $st->purpose,
                    'show_to_customer' => $show_to_customer
                );
            }
        }

        if (!empty($windows_by_id) ) {
            uasort($windows_by_id, function($a, $b) {
                return $a->starts_at > $b->starts_at;
            });
            $homeDelivery->time_windows_cache = json_encode($windows_by_id);
            $homeDelivery->save();
        }

        $formated_windows = $this->formatByDate($windows_by_id, "delivery");
        return $formated_windows;
    }

    protected function getIdsReferenceFromRequest($response)
    {
        return true;
    }

    protected function updateHomeDeliveryRequest($deliver_id, $homeDelivery, $description, $foreign_id, $orders)
    {
        return true;
    }

    public function cancelPickup($homePickup)
    {
        return true;
    }

    public function cancelDelivery($homeDelivery)
    {
        $put_data = array(
            "shipment" => array(
                "state_event" => "cancel"
            )
        );

        $this->api->put('/shipments/'.$homeDelivery->delivery_id, $put_data);

        //we don't want to show any errors here
        return true;
    }

    protected function formatZipCode($zipcode)
    {
        $zipcode = preg_replace('/\s+/', '', $zipcode);
        return strtoupper($zipcode);
    }

    protected function parseErrors($errors)
    {
        $errors = get_object_vars($errors);
        $html = "<ul>";
        foreach($errors as $k=>$v) {
            $html .= "<li><strong>".$k.":</strong> ";
            foreach($v as $error) {
                $html .= $error."<br/>";
            }
            $html .= "</li>";
        }
        $html .= "</ul>";
        return $html;
    }

    protected function getLastRequestError($object, $type = "pickup") 
    {
        if (!empty($object->{$type."_response"})) {
            $last_request_error = json_decode($object->{$type."_response"});
            if (!empty($last_request_error->error)) {
                return true;
            }
            if (!empty($last_request_error->shipment->id)) {
                return true;
            }
        }
        return false;
    }
}