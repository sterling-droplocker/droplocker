<?php

namespace  DropLocker\Service\HomeDelivery;

use DropLocker\Service\AbstractServiceFactory;

class Factory extends AbstractServiceFactory
{
    /**
     * Get the default home delivery service configured for this business
     *
     * @param int $business_id
     *
     * @return \DropLocker\Service\HomeDelivery\IHomeDeliveryService
     */
    public static function getDefaultService($business_id)
    {
        $service = get_business_meta($business_id, 'home_delivery_service');
        if (!$service) {
            return null;
        }

        return self::getService($service, $business_id);
    }

    /**
     * Gets an instance of a home delivery service configured for this business
     *
     * @param string $service
     * @param int $business_id
     *
     * @return \DropLocker\Service\HomeDelivery\IHomeDeliveryService
     */
    public static function getService($service, $business_id)
    {
        // overwritten to get proper autocompletion using doc block
        return parent::getService($service, $business_id);
    }

    /**
     * Get the list of services
     *
     * @return array
     */
    static function getServices()
    {
        return array(
            'deliv' => array(
                'class' => '\DropLocker\Service\HomeDelivery\Deliv',
                'name' => 'Deliv',
            ),
            'droplocker' => array(
                'class' => '\DropLocker\Service\HomeDelivery\DropLocker',
                'name' => 'DropLocker',
            ),
            'rickshaw' => array(
                'class' => '\DropLocker\Service\HomeDelivery\Rickshaw',
                'name' => 'Rickshaw',
            ),
            'boxture' => array(
                'class' => '\DropLocker\Service\HomeDelivery\Boxture',
                'name' => 'Boxture',
            ),
        );
    }
}