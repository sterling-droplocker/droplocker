<?php

namespace  DropLocker\Service\HomeDelivery;

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\OrderHomePickup;

class Rickshaw extends \DropLocker\Service\HomeDelivery\IHomeDeliveryService
{

    /**
     * The rest client to Rickshaw API
     * @var \DropLocker\RestClient\RickshawAPI
     */
    protected $api = null;

    /**
     * send test requests to Rickshaw
     * @var bool
     */
    protected $sandbox = false;

    protected $lastResponse = null;

    protected $business = null;
    protected $pickupCutOffTime = '20:00';
    protected $deliveryCutOffTime = '20:00';

    public function __construct($business_id)
    {
        $this->business_id = $business_id;
        $this->business = new Business($business_id);
        $this->sandbox = DEV || get_business_meta($this->business_id, "rickshaw_mode") == 'test';

        $api_key = get_business_meta($business_id, 'rickshaw_api_key');
        $this->api = new \DropLocker\RestClient\RickshawAPI($api_key);

        $this->settings = array(
            'contact_name'     => get_business_meta($business_id, 'rickshaw_contact_name'),
            'contact_phone'    => get_business_meta($business_id, 'rickshaw_contact_phone'),
            'min_time'         => get_business_meta($business_id, 'rickshaw_min_time'),
            'max_time'         => get_business_meta($business_id, 'rickshaw_max_time'),
            'delivery_address' => get_business_meta($business_id, 'rickshaw_delivery_address'),
            'delivery_notes'   => get_business_meta($business_id, 'rickshaw_delivery_notes'),
            'pickup_address'   => get_business_meta($business_id, 'rickshaw_pickup_address'),
            'pickup_addresses' => unserialize(get_business_meta($business_id, 'rickshaw_pickup_addresses')),
            'pickup_notes'     => get_business_meta($business_id, 'rickshaw_pickup_notes'),
        );
    }

    public function getError()
    {
        if (empty($this->lastResponse) || $this->api->isError()) {
            return "HTTP Error " . $this->api->lastRequest['http_status'];
        }

        if (!empty($this->lastResponse->uuid)) {
            return false;
        }

        if (!empty($this->lastResponse->price_estimates)) {
            return false;
        }

        return empty($this->lastResponse->uuid) ? $this->lastResponse : false;
    }

    public function getErrorMessage()
    {
        if (!$error = $this->getError()) {
            return '';
        }

        $message = '';
        $errors = (array)$this->lastResponse;
        foreach ($errors as $key => $value) {
            $message .= "$key: ".implode(" ", $value)."\n";
        }

        return $message;
    }

    public function canCustomerRequestHomeDelivery($customer)
    {
        $validLocations = $this->getLocationAvailablesForCustomer($customer);

        if (empty($validLocations)) {
            return false;
        }

        $this->lastResponse = $this->api->get('/address/' . urlencode("{$customer->address1}, {$customer->city}, {$customer->state}"));
        if (!empty($this->lastResponse->geocode_errors)) {
            return false;
        }

        return true;
    }

    public function canCustomerRequestHomeDeliveryToLocation($customer, $location)
    {
        // location not avaliable for delviery
        if ($location->deliv_radius <= 0) {
            return false;
        }

        // customer does not have lat/lon
        if (empty($customer->lat)) {
            return false;
        }

        // check distance from customer to location is whithin location radius
        $distance = \distance($location->lat, $location->lon, $customer->lat, $customer->lon, 'm');

        return $distance <= $location->deliv_radius;
    }

    public function getLocationAvailablesForCustomer($customer)
    {
        $CI = get_instance();

        $radius_query  = "SELECT *, (3959 * acos(cos(radians(?)) * cos(radians(lat)) * cos(radians(lon) - radians(?)) + sin(radians(?)) * sin(radians(lat)))) AS distance
            FROM location
            JOIN locker ON (location_id = locationID AND lockerStyle_id = 5 AND lockerStatus_id = 1)
            WHERE business_id = ?
            AND status = 'active'
            AND serviceType != 'not in service'
            HAVING distance <= deliv_pickup
            ORDER BY distance ASC
        ";

        return $CI->db->query($radius_query, array($customer->lat, $customer->lon, $customer->lat,  $this->business_id))->result();
    }

    public function scheduleHomePickup($homePickup)
    {
        if ($homePickup->pickup_id) {
            throw new \Exception("Pickup already scheduled");
        }

        $location = new Location($homePickup->location_id);
        $customer = new Customer($homePickup->customer_id);
        $locker = new Locker($homePickup->locker_id);

        $all_windows = $this->getAllPickupWindows($homePickup);
        if (empty($all_windows[$homePickup->pickup_window_id])) {
            throw new \Exception("Invalid Pickup Window");
        }
        $pickupWindow = $all_windows[$homePickup->pickup_window_id];

        $orig_address = $homePickup->address1 . ', ' . $homePickup->city . ', ' . $homePickup->state;
        $orig_address = $this->cleanupAddress($orig_address);
        $orig_name    = $customer->firstName . ' ' . $customer->lastName;
        $orig_phone   = $customer->phone;
        $orig_asset   = $this->getKeyNumber($location->accessCode);

        $orig_instructions = $homePickup->address2;
        $orig_instructions .= "\nPhone: " . $customer->phone;
        if (strlen($homePickup->notes)) {
            $orig_instructions .= "\nCustomer Notes: " . $homePickup->notes;
        }

        $claim_id = $homePickup->claim_id;

        $description = "Pickup for {$customer->firstName} {$customer->lastName}. #$claim_id";
        $foreign_id = "C-$claim_id";

        // this is the window shown to customers
        $orig_min = gmdate('Y-m-d\TH:i:sO', strtotime($pickupWindow->starts_at));
        $orig_max = gmdate('Y-m-d\TH:i:sO', strtotime($pickupWindow->ends_at));

        // latest acceptable dropoff time
        $dest_time = strtotime($pickupWindow->ends_at); // set to last time of the day
        $dest_max = gmdate('Y-m-d\TH:i:sO', $dest_time + 3600);

        $request = array(
            'orig_min'          => $orig_min,
            'orig_max'          => $orig_max,
            'orig_address'      => $orig_address,
            'orig_name'         => $orig_name,
            'orig_phone'        => $orig_phone,
            'orig_asset'        => $orig_asset,
            'orig_instructions' => $orig_instructions,

            'dest_max'          => $dest_max,
            'dest_address'      => $pickupWindow->drop_address,
            'dest_name'         => $this->settings['contact_name'],
            'dest_phone'        => $this->settings['contact_phone'],
            'dest_instructions' => $this->settings['pickup_notes'],

            'description'       => $description,
            'foreign_id'        => $foreign_id,
        );

        // Rickshaw's API does not have a sandbox yet
        if ($this->sandbox) {
            $request['test_flag'] = 1;
        }

        $this->lastResponse = $this->api->post('/parcels', $request);

        if ($this->getError()) {
            $homePickup->pickup_response = $this->api->lastRequest['response'];
            $homePickup->save();

            throw new \Exception($this->getErrorMessage());
        }

        $homePickup->pickup_id = $this->lastResponse->uuid;
        $homePickup->pickup_response = $this->api->lastRequest['response'];
        //$homePickup->trackingCode = $this->lastResponse->tracking_code;
        $homePickup->deliver_by = new \DateTime($request_date);
        $homePickup->fee = $pickupWindow->fee;
        $homePickup->service = $this->getServiceName();
        $homePickup->save();

        return $homePickup;
    }

    protected function getKeyNumber($accessCode)
    {
        $matches = array();
        if (preg_match('/keys?\s*(\d+)/i', $accessCode, $matches)) {
            return $matches[1];
        }

        return '';
    }

    protected function cleanupAddress($address)
    {
        $address = preg_replace('/\(.*\)/', '', $address);
        $address = preg_replace('/\/[^,]+/', '', $address);
        return $address;
    }

    public function getPickupWindows($homePickup)
    {
        $all_windows = $this->getAllPickupWindows($homePickup);
        return $this->formatByDate($all_windows);
    }

    protected function getAllPickupWindows($homePickup)
    {
        $this->lastResponse = $this->api->post('/parcel_estimates', array(
            'orig_address' => $homePickup->address1,
        ));

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        foreach ($this->lastResponse->price_estimates as $estimate) {
            $id = gmdate('c', strtotime($estimate->orig_min));
            $windows_by_id[$id] = (object) array(
                'id' => $id,
                'starts_at' => gmdate('c', strtotime($estimate->orig_min)),
                'ends_at' => gmdate('c', strtotime($estimate->orig_max)),
                'expires_at' => gmdate('c', strtotime($estimate->orig_min) - 3600 * 3),
                'drop_address' => $this->getDropAddress($estimate->orig_min, $estimate->orig_max),
                'fee' => $estimate->orig_price,
                'notes' => '',
            );
        }

        return $windows_by_id;
    }

    protected function getDropAddress($startDate, $endDate)
    {
        $start = convert_from_gmt_aprax($startDate, 'H:i', $this->business_id);
        $end = convert_from_gmt_aprax($endDate, 'H:i', $this->business_id);

        // try to map the window to an address
        foreach ($this->settings['pickup_addresses'] as $value) {
            list($keyStart, $keyEnd) = explode('-', $value['window']);
            if ($keyStart <= $start && $end <= $keyEnd) {
                return $value['address'];
            }
        }

        // default location, not mapped
        return $this->settings['pickup_address'];
    }

    protected function formatByDate($all_windows)
    {
        // group windows by date
        $windows_by_date = array();
        foreach ($all_windows as $window) {
            // expired window
            if (strtotime($window->expires_at) <= time()) {
                continue;
            }

            // date in the local time
            $date = convert_from_gmt_aprax($window->ends_at, 'Y-m-d', $this->business_id);

            // create date array if not exists
            if (!isset($windows_by_date[$date])) {
                $windows_by_date[$date] = array();
            }

            // save window by date
            $windows_by_date[$date][] = array(
                'id' => $window->id,
                'date' => $date,
                'fee' => $window->fee,
                'notes' => $window->notes,

                // local time, for the view and group calculations
                'start' => convert_from_gmt_aprax($window->starts_at, "H:i", $this->business_id),
                'end' => convert_from_gmt_aprax($window->ends_at, "H:i", $this->business_id),

                // GMT time, this is the one stored in DB and used in the API
                'start_time' => date('Y-m-d H:i:s', strtotime($window->starts_at)),
                'end_time' => date('Y-m-d H:i:s', strtotime($window->ends_at)),
            );
        }

        return $windows_by_date;
    }

    public function getDeliveryWindows($homeDelivery)
    {
        $all_windows = $this->getAllDeliveryWindows($homeDelivery);
        return $this->formatByDate($all_windows);
    }

    public function scheduleHomeDelivery($homeDelivery)
    {
        if ($homeDelivery->delivery_id) {
            throw new \Exception("Delivery already scheduled");
        }

        $order    = new Order($homeDelivery->order_id);
        $location = new Location($homeDelivery->location_id);
        $customer = new Customer($homeDelivery->customer_id);
        $locker   = new Locker($homeDelivery->locker_id);

        $accessCode = substr(trim($customer->phone), -4); // last 4 of the customers phone

        $all_windows = $this->getAllDeliveryWindows($homeDelivery);
        if (empty($all_windows[$homeDelivery->delivery_window_id])) {
            throw new \Exception("Invalid Delivery Window");
        }
        $deliveryWindow = $all_windows[$homeDelivery->delivery_window_id];

        $dest_name    = $customer->firstName . ' ' . $customer->lastName;
        $dest_phone   = $customer->phone;
        $dest_address = $homeDelivery->address1 . ', ' . $homeDelivery->city . ', ' . $homeDelivery->state;
        $dest_address = $this->cleanupAddress($orig_address);
        $dest_asset   = $this->getKeyNumber($location->accessCode);

        $dest_instructions = $homeDelivery->address2;
        $dest_instructions .= "\nPhone: " . $customer->phone;
        if (strlen($homeDelivery->notes)) {
            $dest_instructions .= "\nCustomer Notes: " . $homeDelivery->notes;
        }

        $orders = array($order);
        $description = $this->getDeliveryDescription($orders, $customer);

        $foreign_id = "O-" . $homeDelivery->order_id;

        // earliest acceptable pickup time
        $orig_min = gmdate('Y-m-d\TH:i:sO', strtotime($deliveryWindow->ends_at) - 3600 * 3);

        // this is the time window shown to the customer
        $dest_min = gmdate('Y-m-d\TH:i:sO', strtotime($deliveryWindow->starts_at));
        $dest_max = gmdate('Y-m-d\TH:i:sO', strtotime($deliveryWindow->ends_at));

        $params = array(
            'orig_min'          => $orig_min,
            'orig_address'      => $deliveryWindow->pickup_address,
            'orig_name'         => $this->settings['contact_name'],
            'orig_phone'        => $this->settings['contact_phone'],
            'orig_instructions' => $this->settings['delivery_notes'],

            'dest_min'          => $dest_min,
            'dest_max'          => $dest_max,
            'dest_address'      => $dest_address,
            'dest_name'         => $dest_name,
            'dest_phone'        => $dest_phone,
            'dest_instructions' => $dest_instructions,
            'dest_asset'        => $dest_asset,

            'description'       => $description,
            'foreign_id'        => $foreign_id,
        );

        // Rickshaw's API does not have a sandbox yet
        if ($this->sandbox) {
            $params['test_flag'] = 1;
        }

        $this->lastResponse = $this->api->post('/parcels', $params);

        if ($this->getError() || empty($this->lastResponse->uuid)) {
            $homeDelivery->delivery_response = $this->api->lastRequest['response'];
            $homeDelivery->save();

            throw new \Exception($this->getErrorMessage());
        }

        $homeDelivery->delivery_id = $this->lastResponse->uuid;
        $homeDelivery->delivery_response = $this->api->lastRequest['response'];
        //$homeDelivery->trackingCode = $this->lastResponse->tracking_code;
        $homeDelivery->fee = $deliveryWindow->fee;
        $homeDelivery->service = $this->getServiceName();
        $homeDelivery->save();

        return $homeDelivery;
    }

    protected function getAllDeliveryWindows($homeDelivery)
    {
        $this->lastResponse = $this->api->post('/parcel_estimates', array(
            'dest_address' => $homeDelivery->address1,
        ));

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        foreach ($this->lastResponse->price_estimates as $estimate) {
            $id = gmdate('c', strtotime($estimate->dest_min));
            $windows_by_id[$id] = (object) array(
                'id' => $id,
                'starts_at' => gmdate('c', strtotime($estimate->dest_min)),
                'ends_at' => gmdate('c', strtotime($estimate->dest_max)),
                'expires_at' => gmdate('c', strtotime($estimate->dest_min) - 3600 * 3),
                'pickup_address' => $this->settings['delivery_address'],
                'fee' => $estimate->dest_price,
                'notes' => '',
            );
        }

        return $windows_by_id;
    }

    protected function getIdsReferenceFromRequest($response)
    {
        return $response->foreign_id;
    }

    protected function updateHomeDeliveryRequest($deliver_id, $homeDelivery, $description, $foreign_id, $orders)
    {
        // update Rickshaw delivery to include this order also
        $this->lastResponse = $this->api->request("/parcels/{$deliver_id}", "PATCH", array(
            'description' => $description,
            'foreign_id' => $foreign_id,
        ));

        // on error, update the home delivery and throw exception
        if ($this->getError() || empty($this->lastResponse->uuid)) {
            $homeDelivery->delivery_response = $this->api->lastRequest['response'];
            $homeDelivery->save();

            throw new \Exception($this->getErrorMessage());
        }


        // save the new home delivery object
        $homeDelivery->delivery_id = $this->lastResponse->uuid;
        $homeDelivery->delivery_response = $this->api->lastRequest['response'];
        $homeDelivery->save();

        return $homeDelivery;
    }

    public function cancelPickup($homePickup)
    {
        $this->lastResponse = $this->api->delete("/parcels/{$homePickup->pickup_id}");

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        return true;
    }

    public function cancelDelivery($homeDelivery)
    {
        $this->lastResponse = $this->api->delete("/parcels/{$homeDelivery->delivery_id}");

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        return true;
    }
}