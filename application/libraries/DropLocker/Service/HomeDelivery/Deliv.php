<?php

namespace  DropLocker\Service\HomeDelivery;

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\OrderHomePickup;

class Deliv extends \DropLocker\Service\HomeDelivery\IHomeDeliveryService
{

    /**
     * The rest client to Rickshaw API
     * @var \DropLocker\RestClient\DelivAPI
     */
    protected $api = null;

    protected $lastResponse = null;

    protected $fee = 0;

    protected $readyBy = array(
        'hour' => 6,
        'minutes' => 0,
    );

    protected $pickupWindowNotes = array();
    protected $deliveryWindowNotes = array();

    public function __construct($business_id)
    {
        $this->business_id = $business_id;

        $sandbox = DEV || (get_business_meta($this->business_id, "deliv_mode") == 'test');
        if ($sandbox) {
            $api_key = get_business_meta($this->business_id, "deliv_sandbox_key");
        } else {
            $api_key = get_business_meta($this->business_id, "deliv_api_key");
        }

        $this->api = new \DropLocker\RestClient\DelivAPI($api_key, $sandbox);
        $this->api->version = 2;

        $this->fee = get_business_meta($this->business_id, "deliv_fee");

        $readyBy = get_business_meta($this->business_id, "deliv_ready_by", "6:00");
        $parts = explode(':', $readyBy);
        $this->readyBy['hour'] = $parts[0];
        $this->readyBy['minutes'] = $parts[1];

        $this->pickupWindowNotes = unserialize(get_business_meta($this->business_id, "deliv_window_notes", serialize(array())));
    }

    public function getError()
    {
        if ($this->api->isError()) {
            return $this->lastResponse ? $this->lastResponse : true;
        }

        return false;
    }

    public function getErrorMessage()
    {
        if (!$this->api->isError()) {
            return null;
        }

        if (!empty($this->lastResponse->error)) {
            return $this->lastResponse->error;
        }

        return "HTTP Error " . $this->api->lastRequest['http_status'];
    }

    public function canCustomerRequestHomeDelivery($customer)
    {
        $validLocations = $this->getLocationAvailablesForCustomer($customer);

        return !empty($validLocations);
    }

    public function canCustomerRequestHomeDeliveryToLocation($customer, $location)
    {
        // not a deliv location or not avaliable for delviery
        if (!$location->deliv_id || $location->deliv_radius <= 0) {
            return false;
        }

        // Customer does not have lat/lon
        if (empty($customer->lat)) {
            return false;
        }

        // distance from customer to location
        $distance = \distance($location->lat, $location->lon, $customer->lat, $customer->lon, 'm');

        return $distance <= $location->deliv_radius;
    }

    public function getLocationAvailablesForCustomer($customer)
    {
        $CI = get_instance();

        $radius_query  = "SELECT *, (3959 * acos(cos(radians(?)) * cos(radians(lat)) * cos(radians(lon) - radians(?)) + sin(radians(?)) * sin(radians(lat)))) AS distance
            FROM location
            JOIN locker ON (location_id = locationID AND lockerStyle_id = 5 AND lockerStatus_id = 1)
            WHERE business_id = ?
            AND status = 'active'
            AND serviceType != 'not in service'
            AND deliv_id != ''
            HAVING distance <= deliv_pickup
            ORDER BY distance ASC
        ";

        return $CI->db->query($radius_query, array($customer->lat, $customer->lon, $customer->lat,  $this->business_id))->result();
    }

    public function scheduleHomePickup($homePickup)
    {
        if ($homePickup->pickup_id) {
            throw new \Exception("Pickup already scheduled");
        }

        $location = new Location($homePickup->location_id);
        $customer = new Customer($homePickup->customer_id);

        $all_windows = $this->getAllPickupWindows($homePickup);
        if (empty($all_windows[$homePickup->pickup_window_id])) {
            throw new \Exception("Invalid Pickup Window");
        }
        $pickupWindow = $all_windows[$homePickup->pickup_window_id];


        $end_pickup = strtotime($pickupWindow->ends_at);
        $request_date = gmdate('Y-m-d\TH:00:00\Z', $end_pickup + 3600);

        $this->lastResponse = $this->api->post('/fetches', $request = array(
            'store_id' => $location->deliv_id,
            'customer' => array(
                'first_name' => $customer->firstName,
                'last_name' => $customer->lastName,
                'phone' => $customer->phone,
                'email' => $customer->email, // optional
                'address_line_1' => $homePickup->address1,
                'address_line_2' => $homePickup->address2,
                'address_city' => $homePickup->city,
                'address_state' => $homePickup->state,
                'address_zipcode' => $homePickup->zip,
            ),
            'deliver_by' => $request_date,
            'packages' => array(
                array(
                    'name' => 'Laundry',
                    'price' => '100.00',
                ),
            ),
            'order_reference' => 'C-' . $homePickup->claim_id,
            'fetch_window_id' => $homePickup->pickup_window_id,
            'origin_comments' => $homePickup->notes,
            //'destination_comments' => $dropoff_comments,
        ));

        if ($this->getError()) {
            $homePickup->pickup_response = $this->api->lastRequest['response'];
            $homePickup->save();

            throw new \Exception($this->getErrorMessage());
        }

        $homePickup->pickup_id = $this->lastResponse->id;
        $homePickup->pickup_response = $this->api->lastRequest['response'];
        $homePickup->trackingCode = $this->lastResponse->tracking_code;
        $homePickup->deliver_by = new \DateTime($this->lastResponse->deliver_by);
        $homePickup->fee = $pickupWindow->fee;
        $homePickup->service = $this->getServiceName();
        $homePickup->save();

        return $homePickup;
    }

    public function getPickupWindows($homePickup)
    {
        $all_windows = $this->getAllPickupWindows($homePickup);
        return $this->formatByDate($all_windows);
    }

    protected function getAllPickupWindows($homePickup)
    {
        $response = $this->getPickupEstimate($homePickup);

        if (empty($response->fetch_windows)) {
            throw new \Exception("Empty pickup windows");
        }

        $windows_by_id = array();
        foreach ($response->fetch_windows as $window) {

            // skip "Anytime Windows", window is any time before ends_at
            if (empty($window->starts_at)) {
                continue;
            }

            // expired window
            if (strtotime($window->expires_at) <= time()) {
                continue;
            }

            $window->fee = $this->getPickupFee($window);
            $window->notes = $this->getPickupWindowNotes($window);

            $windows_by_id[$window->id] = $window;
        }

        return $windows_by_id;
    }

    protected function formatByDate($all_windows)
    {
        // group windows by date
        $windows_by_date = array();
        foreach ($all_windows as $window) {
            // skip "Anytime Windows", window is any time before ends_at
            if (empty($window->starts_at)) {
                continue;
            }

            // expired window
            if (strtotime($window->expires_at) <= time()) {
                continue;
            }

            // date in the local time
            $date = convert_from_gmt_aprax($window->ends_at, 'Y-m-d', $this->business_id);

            // create date array if not exists
            if (!isset($windows_by_date[$date])) {
                $windows_by_date[$date] = array();
            }

            // save window by date
            $windows_by_date[$date][] = array(
                'id' => $window->id,
                'date' => $date,
                'fee' => $window->fee,

                // local time, for the view and group calculations
                'start' => convert_from_gmt_aprax($window->starts_at, "H", $this->business_id),
                'end' => convert_from_gmt_aprax($window->ends_at, "H", $this->business_id),

                // GMT time, this is the one stored in DB and used in the API
                'start_time' => date('Y-m-d H:i:s', strtotime($window->starts_at)),
                'end_time' => date('Y-m-d H:i:s', strtotime($window->ends_at)),
                'notes' => $window->notes,
            );
        }

        return $windows_by_date;
    }

    protected function getPickupEstimate($homePickup)
    {
        // used to store the window_id
        $cache_key = 'window_id:' . $homePickup->customer_id . ':' . $homePickup->zip;

        // if set, use cached pickup windows
        if ($window_id = $this->cacheFetch($cache_key)) {
            $this->lastResponse = $this->api->get('/fetch_estimates/' . $window_id);
        }

        // if no response in cache or more than 1 hour old
        if (empty($this->lastResponse) || strtotime($this->lastResponse->deliver_by) < strtotime('-1 hour')) {
            $end_pickup = strtotime("+2 day");
            $request_date = gmdate('Y-m-d\TH:i:s\Z', $end_pickup);

            $location = new Location($homePickup->location_id);

            $this->lastResponse = $this->api->post('/fetch_estimates', array(
                'store_id' => $location->deliv_id,
                'customer_zipcode' => $homePickup->zip,
                'deliver_by' => $request_date,
            ));

            if (!empty($this->lastResponse->id)) {
                // cache response ID
                $this->cacheStore($cache_key, $this->lastResponse->id);
            }
        }

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        return $this->lastResponse;
    }

    protected function cacheStore($cache_key, $value)
    {
        $CI = get_instance();
        $CI->session->set_userdata($cache_key, $value);
    }

    protected function cacheFetch($cache_key)
    {
        $CI = get_instance();
        //return $CI->session->userdata($cache_key);
        return null;
    }

    public function getDeliveryWindows($homeDelivery)
    {
        $all_windows = $this->getAllDeliveryWindows($homeDelivery);
        return $this->formatByDate($all_windows);
    }

    public function scheduleHomeDelivery($homeDelivery)
    {
        if ($homeDelivery->delivery_id) {
            throw new \Exception("Delivery already scheduled");
        }

        $location = new Location($homeDelivery->location_id);
        $customer = new Customer($homeDelivery->customer_id);
        $order = new Order($homeDelivery->order_id);
        $accessCode = substr(trim($customer->phone), -4); // last 4 of the customers phone

        $all_windows = $this->getAllDeliveryWindows($homeDelivery);
        if (empty($all_windows[$homeDelivery->delivery_window_id])) {
            throw new \Exception("Invalid Pickup Window");
        }
        $deliveryWindow = $all_windows[$homeDelivery->delivery_window_id];

        $start_delivery = strtotime($deliveryWindow->expires_at);
        $request_date = gmdate('Y-m-d\TH:00:00\Z', $start_delivery - 3600);

        // Notes for the driver on pickup
        $description = $this->getDeliveryDescription(array($order), $customer);

        $this->lastResponse = $this->api->post('/deliveries', array(
                'store_id' => $location->deliv_id,
                'customer' => array(
                    'first_name' => $customer->firstName,
                    'last_name' => $customer->lastName,
                    'phone' => $customer->phone,
                    'email' => $customer->email, // optional
                    'address_line_1' => $homeDelivery->address1,
                    'address_line_2' => $homeDelivery->address2,
                    'address_city' => $homeDelivery->city,
                    'address_state' => $homeDelivery->state,
                    'address_zipcode' => $homeDelivery->zip,
                ),
                'ready_by' => $request_date,
                'delivery_window_id' => $homeDelivery->delivery_window_id,
                'order_reference' => 'O-' . $homeDelivery->order_id,
                'packages' => array(
                    array(
                        'name' => 'Laundry Order #' . $homeDelivery->order_id,
                        'price' => '100.00',
                    ),
                ),
                'origin_comments' => $description,
                'destination_comments' => $homeDelivery->notes,
            ));

        if ($this->getError() || empty($this->lastResponse->id)) {
            $homeDelivery->delivery_response = $this->api->lastRequest['response'];
            $homeDelivery->save();

            throw new \Exception($this->getErrorMessage());
        }

        $homeDelivery->delivery_id = $this->lastResponse->id;
        $homeDelivery->delivery_response = $this->api->lastRequest['response'];
        $homeDelivery->trackingCode = $this->lastResponse->tracking_code;
        $homeDelivery->fee = $deliveryWindow->fee;
        $homeDelivery->service = $this->getServiceName();
        $homeDelivery->save();

        return $homeDelivery;
    }

    protected function getAllDeliveryWindows($homeDelivery)
    {
        $response = $this->getDeliveryEstimate($homeDelivery);

        if (empty($response->delivery_windows)) {
            throw new \Exception("Empty delivery windows");
        }

        $windows_by_id = array();
        foreach ($response->delivery_windows as $window) {

            // skip "Anytime Windows", window is any time before ends_at
            if (empty($window->starts_at)) {
                continue;
            }

            // expired window
            if (strtotime($window->expires_at) <= time()) {
                continue;
            }

            $window->fee = $this->getDeliveryFee($window);
            $window->notes = $this->getDeliveryWindowNotes($window);
            $windows_by_id[$window->id] = $window;
        }

        return $windows_by_id;
    }

    protected function getDeliveryEstimate($homeDelivery)
    {
        // used to store the window_id
        $cache_key = 'window_id:' . $homeDelivery->order_id . ':' . $homeDelivery->zip;

        // if set, use cached pickup windows
        if ($window_id = $this->cacheFetch($cache_key)) {
            $this->lastResponse = $this->api->get('/delivery_estimates/' . $window_id);
        }

        // if no response in cache or more than 1 hour old
        if (empty($this->lastResponse) || strtotime($this->lastResponse->ready_by) < strtotime('-1 hour')) {

            // check for inventoried orders
            $order = new Order($homeDelivery->order_id);
            if ($order->orderStatusOption_id == 3) {
                $business = new Business($order->business_id);
                $date = clone($order->statusDate);
                $date->setTimeZone(new \DateTimeZone($business->timezone));
                $date->setTime($this->readyBy['hour'], $this->readyBy['minutes'], 0);
                $date->add(new \DateInterval('P1D'));

                $now = new \DateTime();
                if ($date < $now) {
                    $date = $now;
                }

                $date->setTimeZone(new \DateTimeZone("UTC"));
                $request_date = $date->format("Y-m-d\TH:i:s\Z");
            } else {
                $start_delivery_hour = "{$this->readyBy['hour']}:{$this->readyBy['minutes']}";
                $start_delivery = strtotime($start_delivery_hour);
                $request_date = gmdate('Y-m-d\TH:i:s\Z', max($start_delivery, time())); // Today
            }

            $location = new Location($homeDelivery->location_id);

            $this->lastResponse = $this->api->post('/delivery_estimates', array(
                'store_id' => $location->deliv_id,
                'customer_zipcode' => $homeDelivery->zip,
                'ready_by' => $request_date,
            ));

            if (!empty($this->lastResponse->id)) {
                // cache response ID
                $this->cacheStore($cache_key, $this->lastResponse->id);
            }
        }

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        return $this->lastResponse;
    }

    protected function getPickupFee($window)
    {
        return $this->fee;
    }

    protected function getDeliveryFee($window)
    {
        return $this->fee;
    }

    protected function getPickupWindowNotes($window)
    {
        list ($day, $hour) = explode(' ', convert_from_gmt_aprax($window->starts_at, 'w H:i', $this->business_id));

        foreach ($this->pickupWindowNotes as $windowNote) {
            list($start,$end) = explode('-', $windowNote['window']);
            if (in_array($day, $windowNote['days']) && $hour >= $start && $hour <= $end) {
                return $windowNote['notes'];
            }
        }

        return null;
    }

    protected function getDeliveryWindowNotes($window)
    {
        list ($day, $hour) = explode(' ', convert_from_gmt_aprax($window->starts_at, 'w H:i', $this->business_id));

        foreach ($this->deliveryWindowNotes as $windowNote) {
            list($start,$end) = explode('-', $windowNote['window']);
            if (in_array($day, $windowNote['days']) && $hour >= $start && $hour <= $end) {
                return $windowNote['notes'];
            }
        }

        return null;
    }

    protected function getIdsReferenceFromRequest($response)
    {
        return $response->order_reference;
    }

    protected function updateHomeDeliveryRequest($deliver_id, $homeDelivery, $description, $foreign_id, $orders)
    {
        // generate the new packages array
        $packages = array();
        foreach ($orders as $order) {
            $packages[] = array(
                'name' => 'Laundry - Order #' . $order->orderID,
                'price' => '100.00',
            );
        }

        // update Deliv delivery to include this order also
        $this->lastResponse = $this->api->put("/deliveries/{$deliver_id}", array(
            'origin_comments' => $description,
            'order_reference' => $foreign_id,
            'packages' => $packages,
        ));

        // on error, update the home delivery and throw exception
        if ($this->getError() || empty($this->lastResponse->id)) {
            $homeDelivery->delivery_response = $this->api->lastRequest['response'];
            $homeDelivery->save();

            throw new \Exception($this->getErrorMessage());
        }

        // save the new home delivery object
        $homeDelivery->delivery_id = $this->lastResponse->id;
        $homeDelivery->delivery_response = $this->api->lastRequest['response'];
        $homeDelivery->save();

        return $homeDelivery;
    }

    public function cancelPickup($homePickup)
    {
        $this->lastResponse = $this->api->delete("/fetches/{$homePickup->pickup_id}");

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        return true;
    }

    public function cancelDelivery($homeDelivery)
    {
        $this->lastResponse = $this->api->delete("/deliveries/{$homeDelivery->delivery_id}");

        if ($this->getError()) {
            throw new \Exception($this->getErrorMessage());
        }

        return true;
    }

}