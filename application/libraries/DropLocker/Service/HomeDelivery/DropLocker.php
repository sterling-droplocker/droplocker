<?php

namespace  DropLocker\Service\HomeDelivery;

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Order;

class Droplocker extends \DropLocker\Service\HomeDelivery\IHomeDeliveryService
{
    protected $lastResponse = null;

    protected $fee = 0;

    protected $readyBy = array(
        'hour' => 6,
        'minutes' => 0,
    );

    protected $pickupWindowNotes = array();
    protected $deliveryWindowNotes = array();

    protected $daysAhead = 7; // home many days to try to show, TODO: make this a setting
    protected $leadTime = 10800; // 3 hs, TODO: make this a setting
    protected $timeLimit = 604800; // 7 days in the future, TODO: make this a setting

    public function __construct($business_id)
    {
        $this->business_id = $business_id;

    }

    public function getError()
    {
        //TODO: error checking
        return false;
    }

    public function getErrorMessage()
    {
        if (!$this->api->isError()) {
            return null;
        }

        return "Unknown Error";
    }

    public function canCustomerRequestHomeDelivery($customer)
    {
        $validLocations = $this->getLocationAvailablesForCustomer($customer);

        return !empty($validLocations);
    }

    public function canCustomerRequestHomeDeliveryToLocation($customer, $location)
    {
        // Customer does not have lat/lon
        if (empty($customer->lat)) {
            return false;
        }

        $CI = get_instance();
        $CI->load->model('deliveryzone_model');

        $zones = $CI->deliveryzone_model->get_aprax(array('location_id' => $location->locationID));

        return $this->filterZonesByPoint($zones, $customer->lat, $customer->lon);
    }

    public function getLocationAvailablesForCustomer($customer)
    {
        $zones = $this->getAvailableZones($customer->lat, $customer->lon);
        $locations = array();

        $CI = get_instance();
        $CI->load->model('location_model');

        foreach ($zones as $zone) {
            $locations[] = $CI->location_model->get_by_primary_key($zone->location_id);
        }

        return $locations;
    }

    /**
     * Get all zones sourrounding a point
     * @param float $lat
     * @param float $lng
     */
    protected function getAvailableZones($lat, $lng)
    {
        $CI = get_instance();
        $CI->load->model('deliveryzone_model');

        //get zones for the business
        $zones = $CI->deliveryzone_model->get_aprax(array(
            'business_id' => $this->business_id,
        ));

        return  $this->filterZonesByPoint($zones, $lat, $lng);
    }

    /**
     * Filters an array of delivery zones by a point
     * @param array $zones
     * @param float $lat
     * @param float $lng
     */
    protected function filterZonesByPoint($zones, $lat, $lng)
    {
        $validZones = array();
        $point = (object) array('lat' => $lat, 'lng' => $lng);

        //check lat and lng in every zone
        foreach ($zones as $zone) {

            // a polygon must have at least 3 sides
            $points = json_decode($zone->points);
            if (count($points) < 3) {
                continue;
            }

            // if the polygon contains the point, then it is a valid zone
            $polygon = new GeoPolygon($points);
            if ($polygon->contains($point)) {
                $validZones[] = $zone;
            }
        }

        return $validZones;
    }

    /**
     * Get all windows for a point
     * @param float $lat
     * @param float $lng
     */
    protected function getAvailableWindows($lat, $lng)
    {
        $CI = get_instance();
        $CI->load->model('deliverywindow_model');

        $ids = array();
        foreach ($this->getAvailableZones($lat, $lng) as $zone) {
            $ids[] = $zone->delivery_zoneID;
        }

        return $CI->deliverywindow_model->getDeliveryWindowsAndZones($ids);
    }

    public function scheduleHomePickup($homePickup)
    {
        if ($homePickup->pickup_id) {
            throw new \Exception("Pickup already scheduled");
        }

        $location = new Location($homePickup->location_id);
        $customer = new Customer($homePickup->customer_id);

        //$all_windows = $this->getAllPickupWindows($homePickup);
        //$pickupWindow = $all_windows[$homePickup->pickup_window_id];

        //$end_pickup = strtotime($pickupWindow->ends_at);
        //$request_date = gmdate('Y-m-d\TH:00:00\Z', $end_pickup + 3600);

        $request = array(
            'store_id' => $location->deliv_id,
            'customer' => array(
                'first_name' => $customer->firstName,
                'last_name' => $customer->lastName,
                'phone' => $customer->phone,
                'email' => $customer->email, // optional
                'address_line_1' => $homePickup->address1,
                'address_line_2' => $homePickup->address2,
                'address_city' => $homePickup->city,
                'address_state' => $homePickup->state,
                'address_zipcode' => $homePickup->zip,
            ),
            //'deliver_by' => $request_date,
            'packages' => array(
                array(
                    'name' => 'Laundry',
                    'price' => '100.00',
                ),
            ),
            'order_reference' => 'C-' . $homePickup->claim_id,
            'fetch_window_id' => $homePickup->pickup_window_id,
            'origin_comments' => $homePickup->notes,
        );


        $homePickup->pickup_id = uniqid(null, true);
        $homePickup->pickup_response = json_encode($request);

        //$homePickup->deliver_by = new \DateTime($request_date);
        //$homePickup->fee = $pickupWindow->fee;
        $homePickup->service = $this->getServiceName();
        $homePickup->save();

        return $homePickup;
    }

    public function getPickupWindows($homePickup)
    {
        $all_windows = $this->getAllPickupWindows($homePickup);
        return $this->formatByDate($all_windows);
    }

    public function getPickupWindowByID($homePickup, $pickupWindowID)
    {
        $CI = get_instance();
        $CI->load->model('deliverywindow_model');

        $allWindows = array($CI->deliverywindow_model->get_by_primary_key($pickupWindowID));
        $formatted = $this->formatPickupWindows($homePickup, $allWindows);

        if (empty($formatted)) {
            return false;
        }

        return current($formatted);
    }

    public function getDeliveryWindowByID($homeDelivery, $deliveryWindowID)
    {
        $CI = get_instance();
        $CI->load->model('deliverywindow_model');

        $allWindows = array($CI->deliverywindow_model->get_by_primary_key($deliveryWindowID));
        $formatted = $this->formatDeliveryWindows($homeDelivery, $allWindows);

        if (empty($formatted)) {
            return false;
        }

        return current($formatted);
    }

    protected function getAllPickupWindows($homePickup)
    {
        $allWindows = $this->getAvailableWindows($homePickup->lat, $homePickup->lon);
        return $this->formatPickupWindows($homePickup, $allWindows);
    }

    protected function formatPickupWindows($homePickup, $allWindows)
    {
        //$customer = new Customer($homePickup->customer_id);
        $business = new Business($homePickup->business_id);
        $location = new Location($homePickup->location_id);

        $now = time();
        $time_limit = $now + $this->timeLimit;
        if ($location->serviceType == Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY) {
            $time_limit = $now + 86400 * 7;
        }

        $days = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        $dayOfWeek = date('w');

        $windows = array();
        foreach ($allWindows as $window) {
            for ($i = 0; $i < $this->daysAhead; $i++) {

                $day = $days[($dayOfWeek + $i) % 7];
                if (empty($window->$day)) {
                    continue; // There is not service this day
                }

                //TODO: check for capacity of that day
                $date = gmdate('Y-m-d', strtotime("$day"));
                
                if ($business->isHoliday($date)) {
                    continue;
                }
                
                $id = $window->delivery_windowID;
                $start_time = strtotime("$date {$window->start} {$business->timezone}");
                $end_time = strtotime("$date {$window->end} {$business->timezone}");
                $expire_time = $start_time - $this->leadTime;

                // window expired
                if ($expire_time < $now) {
                    continue;
                }

                // window too in the future
                if ($start_time > $time_limit) {
                    continue;
                }

                $windows[] = (object) array(
                    'id' => $id,
                    'starts_at' => gmdate('c', $start_time),
                    'ends_at' => gmdate('c', $end_time),
                    'expires_at' => gmdate('c', $expire_time),
                    'fee' => $window->fee,
                    'notes' => $window->notes,
                );
            }
        }

        return $windows;
    }

    protected function formatByDate($all_windows)
    {
        // group windows by date
        $windows_by_date = array();
        foreach ($all_windows as $window) {
            // skip "Anytime Windows", window is any time before ends_at
            if (empty($window->starts_at)) {
                continue;
            }

            // expired window
            if (strtotime($window->expires_at) <= time()) {
                continue;
            }

            // date in the local time
            $date = convert_from_gmt_aprax($window->ends_at, 'Y-m-d', $this->business_id);

            // create date array if not exists
            if (!isset($windows_by_date[$date])) {
                $windows_by_date[$date] = array();
            }

            // save window by date
            $windows_by_date[$date][] = array(
                'id' => $window->id,
                'date' => $date,
                'fee' => $window->fee,

                // local time, for the view and group calculations
                'start' => convert_from_gmt_aprax($window->starts_at, "H:i", $this->business_id),
                'end' => convert_from_gmt_aprax($window->ends_at, "H:i", $this->business_id),

                // GMT time, this is the one stored in DB and used in the API
                'start_time' => date('Y-m-d H:i:s', strtotime($window->starts_at)),
                'end_time' => date('Y-m-d H:i:s', strtotime($window->ends_at)),
                'notes' => $window->notes,
            );
        }

        return $windows_by_date;
    }

    public function getDeliveryWindows($homeDelivery)
    {
        $all_windows = $this->getAllDeliveryWindows($homeDelivery);
        return $this->formatByDate($all_windows);
    }

    public function scheduleHomeDelivery($homeDelivery)
    {
        if ($homeDelivery->delivery_id) {
            throw new \Exception("Delivery already scheduled");
        }

        $location = new Location($homeDelivery->location_id);
        $customer = new Customer($homeDelivery->customer_id);
        $order = new Order($homeDelivery->order_id);
        $accessCode = substr(trim($customer->phone), -4); // last 4 of the customers phone

        //$deliveryWindow = $this->getDeliveryWindowByID($homeDelivery, $homeDelivery->delivery_window_id);

        //$start_delivery = strtotime($deliveryWindow->expires_at);
        //$request_date = gmdate('Y-m-d\TH:00:00\Z', $start_delivery - 3600);

        // Notes for the driver on pickup
        $description = $this->getDeliveryDescription(array($order), $customer);

        $request = array(
            'store_id' => $location->deliv_id,
            'customer' => array(
                'first_name' => $customer->firstName,
                'last_name' => $customer->lastName,
                'phone' => $customer->phone,
                'email' => $customer->email, // optional
                'address_line_1' => $homeDelivery->address1,
                'address_line_2' => $homeDelivery->address2,
                'address_city' => $homeDelivery->city,
                'address_state' => $homeDelivery->state,
                'address_zipcode' => $homeDelivery->zip,
            ),
            //'ready_by' => $request_date,
            'delivery_window_id' => $homeDelivery->delivery_window_id,
            'order_reference' => 'O-' . $homeDelivery->order_id,
            'packages' => array(
                array(
                    'name' => 'Laundry Order #' . $homeDelivery->order_id,
                    'price' => '100.00',
                ),
            ),
            'origin_comments' => $description,
            'destination_comments' => $homeDelivery->notes,
        );

        $homeDelivery->delivery_id = uniqid(null, true);
        $homeDelivery->delivery_response = json_encode($request);
        $homeDelivery->service = $this->getServiceName();
        $homeDelivery->save();

        return $homeDelivery;
    }

    protected function getAllDeliveryWindows($homeDelivery)
    {
        $allWindows = $this->getAvailableWindows($homeDelivery->lat, $homeDelivery->lon);
        return $this->formatDeliveryWindows($homeDelivery, $allWindows);
    }

    protected function formatDeliveryWindows($homeDelivery, $allWindows)
    {
        //$customer = new Customer($homeDelivery->customer_id);
        $business = new Business($homeDelivery->business_id);
        $location = new Location($homeDelivery->location_id);

        $now = time();
        $time_limit = $now + $this->timeLimit;
        if ($location->serviceType == Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY) {
            $time_limit = $now + 86400 * 7;
        }

        $days = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        $dayOfWeek = date('w');

        $windows = array();
        foreach ($allWindows as $window) {
            for ($i = 0; $i < $this->daysAhead; $i++) {

                $day = $days[($dayOfWeek + $i) % 7];
                if (empty($window->$day)) {
                    continue; // There is not service this day
                }

                //TODO: check for capacity of that day
                $date = gmdate('Y-m-d', strtotime("$day"));

                $id = $window->delivery_windowID;

                $start_time = strtotime("$date {$window->start} {$business->timezone}");
                $end_time = strtotime("$date {$window->end} {$business->timezone}");
                $expire_time = $start_time - $this->leadTime;

                // window expired
                if ($expire_time < $now) {
                    continue;
                }

                // window too in the future
                if ($start_time > $time_limit) {
                    continue;
                }

                $windows[] = (object) array(
                    'id' => $id,
                    'starts_at' => gmdate('c', $start_time),
                    'ends_at' => gmdate('c', $end_time),
                    'expires_at' => gmdate('c', $expire_time),
                    'fee' => $window->fee,
                    'notes' => $window->notes,
                );
            }
        }

        return $windows;
    }

    protected function getIdsReferenceFromRequest($response)
    {
        return $response->order_reference;
    }

    protected function updateHomeDeliveryRequest($deliver_id, $homeDelivery, $description, $foreign_id, $orders)
    {
        // generate the new packages array
        $packages = array();
        foreach ($orders as $order) {
            $packages[] = array(
                'name' => 'Laundry - Order #' . $order->orderID,
                'price' => '100.00',
            );
        }

        // TODO: update Deliv delivery to include this order also

        return $homeDelivery;
    }

    public function cancelPickup($homePickup)
    {
        //TODO: canel the home pickup, maybe send emails

        return true;
    }

    public function cancelDelivery($homeDelivery)
    {
        //TODO: canel the home delivery, maybe send emails

        return true;
    }

}


class GeoPolygon
{
    public $points = array();

    public function __construct($points = array())
    {
        $this->points = $points;
    }

    public function contains($point)
    {
        $crossed = 0;
        $p1 = $this->points[0];
        $n = count($this->points);

        for ($i=1; $i <= $n; $i++) {
            $p2 = $this->points[$i % $n];

            $min_lng = min($p1->lng, $p2->lng);
            $max_lng = max($p1->lng, $p2->lng);
            $max_lat = max($p1->lat, $p2->lat);

            if ($point->lng > $min_lng && $point->lng <= $max_lng && $point->lat <= $max_lat && $p1->lng != $p2->lng) {
                $xinters = ($point->lng - $p1->lng) * ($p2->lat - $p1->lat) / ($p2->lng - $p1->lng) + $p1->lat;
                if ($p1->lat == $p2->lat || $point->lat <= $xinters) {
                    $crossed++;
                }
            }
            $p1 = $p2;
        }
        // if the number of edges we passed through is even, then it's not in the poly.
        return $crossed % 2 != 0;
    }
}