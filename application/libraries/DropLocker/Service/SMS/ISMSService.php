<?php

namespace  DropLocker\Service\SMS;

abstract class ISMSService
{
    /**
     * The business id
     * @var int
     */
    public $business_id = null;

    /**
     * The business SMS settings
     * @var object
     */
    public $settings = null;

    /**
     * Constructs the object and loads settings
     *
     * @param int $business_id
     */
    public function __construct($business_id)
    {
        $this->business_id = $business_id;
        $this->settings = $this->getSMSSettings();
    }

    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     */
    abstract public function sendMessage($to, $message);

    /**
     * Get the SMS configuraition for this business
     *
     * @return object
     */
    protected function getSMSSettings()
    {
        $CI = get_instance();
        $sms_query = $CI->db->get_where('smsSettings', array(
            'business_id' => $this->business_id
        ));

        return $sms_query->row();
    }

    /**
     * Gets the last error
     * @return object
     */
    abstract public function getError();

    /**
     * Gets the error message from the last error
     * @return string
     */
    abstract public function getErrorMessage();

}