<?php

namespace  DropLocker\Service\SMS;

class Twilio extends \DropLocker\Service\SMS\ISMSService
{
    protected $api = null;
    protected $lastError = null;

    public function __construct($business_id)
    {
        parent::__construct($business_id);

        require_once APPPATH . 'third_party/Twilio/Twilio.php';
        $this->api = new \Services_Twilio($this->settings->account_sid, $this->settings->auth_token);
    }

    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     * @return bool
     */
    public function sendMessage($to, $message)
    {
        $this->lastError = null;
        try {
            $this->api->account->messages->sendMessage($this->settings->number, $to, $message);
        } catch (\Services_Twilio_RestException $e) {
            $this->lastError = $e;
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->lastError;
    }

    public function getErrorMessage()
    {
        $e = $this->lastError;

        return "Error " . $e->getStatus() . ": " . $e->getMessage(). " Info: " . $e->getInfo() . " Code: " . $e->getCode();
    }
}