<?php

namespace  DropLocker\Service\SMS;

class Nexmo extends \DropLocker\Service\SMS\ISMSService
{

    protected $api = null;
    protected $lastError = null;

    public function __construct($business_id)
    {
        parent::__construct($business_id);

        require_once APPPATH . 'third_party/nexmo/NexmoMessage.php';
        $this->api = new \NexmoMessage($this->settings->account_sid, $this->settings->auth_token);

    }

    /**
     * Sends an SMS message
     *
     * @param string $to
     * @param string $message
     */
    public function sendMessage($to, $message)
    {
        $this->lastError = null;

        $info = $this->api->sendText($to, $this->settings->number, $message);
        if ($info->messagecount) {
            return true;
        }

        $this->lastError = $info;
        return false;

    }

    public function getError()
    {
        return $this->lastError;
    }

    public function getErrorMessage()
    {
        return $this->api->displayOverview($this->lastError);
    }

}