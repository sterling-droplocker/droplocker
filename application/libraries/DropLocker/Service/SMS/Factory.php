<?php

namespace  DropLocker\Service\SMS;

use DropLocker\Service\AbstractServiceFactory;

class Factory extends AbstractServiceFactory
{
    /**
     * Gets an instance of a sms service configured for this business
     *
     * @param string $service
     * @param int $business_id
     *
     * @return \DropLocker\Service\SMS\ISMSService
     */
    public static function getService($service, $business_id)
    {
        // overwritten to get proper autocompletion using doc block
        return parent::getService($service, $business_id);
    }

    /**
     * Get the list of services
     *
     * @return array
     */
    static function getServices()
    {
        return array(
            'twilio' => array(
                'class' => '\DropLocker\Service\SMS\Twilio',
                'name' => 'Twilio',
            ),
            'nexmo' => array(
                'class' => '\DropLocker\Service\SMS\Nexmo',
                'name' => 'Nexmo',
            ),
        );
    }
}