<?php

namespace  DropLocker\Export;


class QuickBooks
{
    public function getIIF($invoices)
    {
        $out = array();
        $out[] = "!TRNS|TRNSID|TRNSTYPE|DATE|ACCNT|NAME|CLASS|AMOUNT|DOCNUM|MEMO|CLEAR|TOPRINT|ADDR1|ADDR2|ADDR3|ADDR4|ADDR5|DUEDATE|TERMS|PAID|SHIPDATE|INVTITLE";
        $out[] = "!SPL|SPLID|TRNSTYPE|DATE|ACCNT|NAME|CLASS|AMOUNT|DOCNUM|MEMO|CLEAR|QNTY|PRICE|INVITEM|PAYMETH|TAXABLE|REIMBEXP|EXTRA";
        $out[] = "!ENDTRNS";

        $i = 0;
        foreach ($invoices as $invoice) {
            $i++;

            $date       = date("m/d/Y", strtotime($invoice['date']));
            $customer   = $invoice['customer'];
            $total      = number_format($invoice['total'], 2, '.', '');
            $number     = $invoice['number'];
            $address    = $invoice['address'];
            $title      = $invoice['title'];

            //       !TRNS|TRNSID|TRNSTYPE|DATE   |ACCNT              |NAME       |CLASS|AMOUNT  |DOCNUM   |MEMO|CLEAR|TOPRINT|ADDR1     |ADDR2|ADDR3|ADDR4|ADDR5|DUEDATE|TERMS  |PAID|SHIPDATE|INVTITLE
            $out[] = "TRNS|{$i}  |INVOICE |{$date}|Accounts Receivable|{$customer}|     |{$total}|{$number}|    |N    |N      |{$address}|     |     |     |     |{$date}|{$date}|N   |        |{$title}";

            foreach ($invoice['items'] as $item) {
                $i++;

                $description = $item['description'];
                $amount      = number_format($item['amount'], 2, '.', '');
                $price       = number_format($item['price'], 2, '.', '');
                $quantity    = $item['quantity'];
                $taxable     = !empty($item['taxable']) ? 'Y' : 'N';
                $account     = !empty($item['account']) ? $item['account'] : 'Income Account';
                $name        = !empty($item['name']) ? $item['name'] : '';
                $item        = !empty($item['item']) ? $item['item'] : 'Sales Item';


                //       !SPL|SPLID|TRNSTYPE|DATE   |ACCNT     |NAME   |CLASS|AMOUNT    |DOCNUM       |MEMO          |CLEAR|QNTY        |PRICE   |INVITEM|PAYMETH|TAXABLE   |REIMBEXP|EXTRA";
                $out[] = "SPL|{$i} |INVOICE |{$date}|{$account}|{$name}|     |-{$amount}|{$invoice_id}|{$description}|N    |-{$quantity}|{$price}|{$item}|       |{$taxable}|N       |NOTHING";
            }
            $i++;

        }

        $out[] = 'ENDTRNS';

        $out = implode("\r\n", $out) . "\r\n";
        $out = preg_replace('#[ ]+\|#', "|", $out);
        $out = str_replace("|", "\t", $out);
        return $out;
    }
}
