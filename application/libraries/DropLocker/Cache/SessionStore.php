<?php
namespace DropLocker\Cache;

class SessionStore implements \ArrayAccess
{

    public $key = null;
    protected static $instances = array();

    public function __construct($key)
    {
        if (session_id() === '') {
            start_session();
        }
        $this->key = $key;

        if (! isset($_SESSION[$this->key])) {
            $_SESSION[$this->key] = array();
        }
    }

    public static function getInstance($key)
    {
        if (! isset(self::$instances[$key])) {
            self::$instances[$key] = new SessionStore($key);
        }

        return self::$instances[$key];
    }

    public function offsetExists($index)
    {
        return isset($_SESSION[$this->key][$index]);
    }

    public function offsetGet($index)
    {
        return isset($_SESSION[$this->key][$index]) ? $_SESSION[$this->key][$index] : null;
    }

    public function offsetSet($index, $value)
    {
        if (is_null($index)) {
            $_SESSION[$this->key][] = $value;
        } else {
            $_SESSION[$this->key][$index] = $value;
        }
    }

    public function offsetUnset($index)
    {
        unset($_SESSION[$this->key][$index]);
    }

    public function getContents()
    {
        return $_SESSION[$this->key];
    }
}