<?php
use App\Libraries\DroplockerObjects\Business;

/* library for orders exports / imports */
class Export_orders
{

    protected $_ci;

    public $start_date = '0000-00-00 00:00:00'; // if you send these dates through in the params, make sure they're formatted like this
    public $end_date = '9999-99-99 99:99:99';

    public $business_id = null;

    public $routes = false;

    public function __construct()
    {
        $this->_ci = get_instance();
    }


    public function export_to_file( $params = array() )
    {
        /* Check for required params */
        $this->business_id = $params['origin_business_id'];
        if (empty($this->business_id)) {
            throw new Exception('Missing origin business ID');
        } else {
            $this->business = new Business($params['origin_business_id']);
        }

        if (empty($params['start_date'])) {
            throw new Exception('start_date is required');
            
        } 

        if (empty($params['end_date'])) {
            throw new Exception('end_date is required');
        }

        $this->start_date = $params['start_date'];
        $this->end_date = $params['end_date'];

        if (! empty($params['routes'])) {
            $this->routes = $params['routes'];
        }

        /* Get orders */
        $orders = $this->pullOrders();
        
        /* Generate and download file */
        $this->download_as_file($orders);
    }

    protected function pullOrders($automat_ref = true)
    {
        $this->_ci->db->select("customer.firstName, customer.lastName, customer.phone, customer.email,
            location.address AS address1, location.address2, location.city, location.state, location.zipcode as zip, location.route_id, location.sortOrder,
            orders.*, bagNumber, bagID");
        $this->_ci->db->from("orders");
        $this->_ci->db->join('bag', 'bagID = orders.bag_id');
        $this->_ci->db->join('customer', 'customer.customerID = orders.customer_id');
        $this->_ci->db->join('locker', 'lockerID = orders.locker_id');
        $this->_ci->db->join('location', 'locationID = locker.location_id');
        $this->_ci->db->join('orderStatus', 'orders.orderID = orderStatus.order_id');
        $this->_ci->db->order_by("route_id ASC, sortOrder ASC, orders.customer_id ASC");
        $this->_ci->db->group_by("orders.orderID");
        // if we have dates use them
        if (! empty($this->end_date)) {
            $this->_ci->db->where("orderStatus.date <= ", $this->end_date);
        }
        if (! empty($this->start_date)) {
            $this->_ci->db->where("orderStatus.date >= ", $this->start_date);
        }
        $this->_ci->db->where("orderStatus.orderStatusOption_id", 3);
        $this->_ci->db->where_in("orders.orderStatusOption_id", array(3, 7));
        if (!empty($this->routes)) {
            $this->_ci->db->where_in("route_id", $this->routes);
        }
        $this->_ci->db->where("orders.orderType", "DC");
        $this->_ci->db->where("orders.business_id", $this->business_id);

        $orders_query = $this->_ci->db->get();

        $orders = $orders_query->result();
        $all_order_data = array();
        if (! empty($orders)) {

            $last_route = -1;
            $last_stop = 0;

            foreach ($orders as $order) {
                // first see if its already in the automat
                $automat_check_query = $this->_ci->db->select('automatID')
                    ->from('automat')
                    ->where('order_id', $order->orderID)
                    ->where('slot !=', '')
                    ->get();
                $automat_check = $automat_check_query->row();

                // if its in the automat, skip it
                if (! empty($automat_check)) {
                    continue;
                }

                $this->_ci->db->select('item.*, orderItem.*, picture.*, barcode.barcode');
                $this->_ci->db->join('item', 'item.itemID = orderItem.item_id', 'left');
                $this->_ci->db->join('picture', 'item.itemID = picture.item_id', 'left');
                $this->_ci->db->join('barcode', 'barcode.item_id = item.itemID', 'left');

                $order_items_query = $this->_ci->db->get_where('orderItem', array(
                    'order_id' => $order->orderID
                ));
                $order_items = $order_items_query->result();

                if (! empty($order_items)) {
                    $all_order_data[$order->orderID]['order'] = $order;

                    if ($last_route != $order->route_id) {
                        $last_route = $order->route_id;
                        $last_stop = 0;
                    }

                    $all_order_data[$order->orderID]['order']->route_stop = ++$last_stop;


                    foreach ($order_items as $order_item) {

                        $this->_ci->db->select('product_process.*, product.*, process.name as process_name ');
                        $this->_ci->db->join('product', 'product.productID = product_process.product_id');
                        $this->_ci->db->join('process', 'process.processID = product_process.process_id');
                        $prod_details_query = $this->_ci->db->get_where('product_process', array(
                            'product_processID' => $order_item->product_process_id
                        ));
                        $prod_details = $prod_details_query->row();

                        $all_order_data[$order->orderID]['orderItems'][$order_item->orderItemID] = $order_item;

                        if (! empty($prod_details)) {
                            $all_order_data[$order->orderID]['orderItemsDetail'][$order_item->orderItemID] = $prod_details;
                        }
                    }

                    /*
                    if ($this->loadBags) {
                        $all_order_data[$order->orderID]['orderItems'][$order->bagID] = (object) array(
                            'itemID' => $order->bagID,
                            'barcode' => $order->bagNumber,
                            'qty' => 1,
                            'unitPrice' => '0.00',
                        );

                        $all_order_data[$order->orderID]['orderItemsDetail'][$order->bagID] = (object) array(
                            'name' => 'Bag',
                        );
                    }
                    */

                }
            }
        }

        return $all_order_data;
    }

    public function download_as_file( $data = array() ) 
    {
        $filename = $this->business->slug."_".date("m-d-Y H:i").".txt";
        header('Content-type: text/tab-separated-values');
        header("Content-Disposition: attachment;filename=".$filename."");
        foreach ($data as $oid=>$details) {
            foreach ($details["orderItems"] as $item) {
                $line = array(
                    $item->order_id,
                    empty($item->barcode)?"":$item->barcode,
                    $details["orderItemsDetail"][$item->orderItemID]->process_name,
                    $details["orderItemsDetail"][$item->orderItemID]->name.": ".$details["orderItemsDetail"][$item->orderItemID]->displayName
                );
                echo implode("\t",$line)."\n";
            }
        }
        die();
    }
}