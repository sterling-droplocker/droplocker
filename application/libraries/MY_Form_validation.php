<?php

class MY_Form_validation extends CI_Form_validation
{
    public function does_product_process_exist($product_processID)
    {
        $this->CI->load->model("product_process_model");
        if ($this->CI->product_process_model->does_exist($product_processID) === true) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Product Process ID does not exist");

            return false;
        }
    }
    /**
     * Checks if the specified Product Category exists in the database
     * @param int $productCategoryID
     * @return boolean
     */
    public function does_productCategory_exist($productCategoryID)
    {
        $this->CI->load->model("productCategory_model");
        if ($this->CI->productCategory_model->does_exist($productCategoryID) === true) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Product Category ID '$productCategoryID' does not exist");

            return false;
        }
    }
    /**
     * Checks if the specified Email Action exists in the database
     * @param int $emailActionID
     * @return boolean
     */
    public function does_emailAction_exist($emailActionID)
    {
        $this->CI->load->model("emailactions_model");
        if ($this->CI->emailactions_model->does_exist($emailActionID)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Email Action ID '$emailActionID' does not exist");

            return false;
        }
    }
    /**
     * Checks if the specified order exists in the database
     * @param int $orderID
     * @return boolean
     */
    public function does_order_exist($orderID)
    {
        $this->CI->load->model("order_model");
        if ($this->CI->order_model->does_exist($orderID)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Order ID '$orderID' does not exist");

            return false;
        }
    }
    /**
     * Checks if the specified language exists in the database.
     * @param int $languageID the primary key of the language
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public function does_language_exist($languageID)
    {
        if (!is_numeric($languageID)) {
            throw new \InvalidArgumentException("'languageID' must be numeric");
        }
        $this->CI->load->model("language_model");
        if ($this->CI->language_model->does_exist($languageID)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Language ID '$languageID' does not exist");

            return false;
        }
    }

    /**
     * Checks if the specified language view exists in the database.
     * @param int $languageViewID the primary key of the language view
     * @return boolean
     * @throws \InvalidArgumentException if 'languageViewID' is not numeric
     */
    public function does_languageView_exist($languageViewID)
    {
        if (!is_numeric($languageViewID)) {
            throw new \InvalidArgumentException("'languageViewID' must be numeric");
        }
        $this->CI->load->model("languageView_model");
        if ($this->CI->languageView_model->does_exist($languageViewID)) {
            return true;
        } else {
            $this->set_message("does_languageView_exist", "Langauge View ID '$languageViewID' does not exist");

            return false;
        }

    }
    /**
     * Checks to see if the specified business exists in the database.
     * @param int $businessID the priamry key of the business entity
     * @return boolean
     * @throws \InvalidArgumentException thrown if 'businessID' is not numeric
     */
    public function does_business_exist($businessID)
    {
        if (!is_numeric($businessID)) {
            throw new \InvalidArgumentException("'businessID' must be numeric");
        }
        $this->CI->load->model("business_model");
        if ($this->CI->business_model->does_exist($businessID)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Business ID '$businessID' does not exist");

            return false;
        }
    }

    public function does_reminder_exist($reminderID)
    {
        if (is_numeric($reminderID)) {
            $this->CI->load->model("reminder_model");
            if ($this->CI->reminder_model->does_exist($reminderID)) {
                return true;
            } else {
                $this->set_message(__FUNCTION__, "Rmeinder ID '$reminderID' does not exist");

                return false;
            }
        } else {
            throw new \InvalidArgumentException("'reminderID' must be numeric");
        }
    }
    public function does_reminder_email_exist($reminder_emailID)
    {
        if (is_numeric($reminder_emailID)) {
            $this->CI->load->model("reminder_email_model");
            if ($this->CI->reminder_email_model->does_exist($reminder_emailID)) {
                return true;
            } else {
                $this->set_message(__FUNCTION__, "Reminder Email ID '$reminder_emailID' does not exist");
            }
        } else {
            throw new \InvalidArgumentException("'reminder_emailID' must be numeric");
        }
    }
    /**
     * Checks to see if the specified business language key exists in the database.
     * @param int $business_languageID
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public function does_business_language_exist($business_languageID)
    {
        if (!is_numeric($business_languageID)) {
            throw new \InvalidArgumentException("'business_languageID' must be numeric");
        }
        $this->CI->load->model("business_language_model");
        if ($this->CI->business_language_model->does_exist($business_languageID)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Business Language ID '$business_languageID' does not exist");

            return false;
        }
    }
    /**
     * Checks to see if the specified language key exists in the database
     * @param int $languageKeyID the primary key of the language entity
     * @return boolean
     * @throws \InvalidArgumentException thrown if 'languageKeyID' is not numeric
     */
    public function does_languageKey_exist($languageKeyID)
    {
        if (!is_numeric($languageKeyID)) {
            throw new \InvalidArgumentException("'languageKeyID' must be numeric");
        }
        $this->CI->load->model("languageKey_model");
        if ($this->CI->languageKey_model->does_exist($languageKeyID)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "Language key ID '$languageKeyID' does not exist");

            return false;
        }
    }
    /**
     * Verifies that the specified language key can be deleted as according to the business rules.
     * @param int $languageKeyID
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public function is_languageKey_deletable($languageKeyID)
    {
        if (!is_numeric($languageKeyID)) {
            throw new \InvalidArgumentException("'languageKeyID' must be numeric");
        }
        $this->CI->load->model("languageKey_business_language_model");
        $languageKey_business_language_count = $this->CI->languageKey_business_language_model->count(array("languageKey_id" => $languageKeyID));
        if ($languageKey_business_language_count > 0) {
            $this->CI->load->model("languageKey_model");
            $languageKey = $this->CI->languageKey_model->get_by_primary_key($languageKeyID);
            $this->set_message(__FUNCTION__, "Can not delete '{$languageKey->name}' because there are $languageKey_business_language_count business translations associated with this language key");

            return false;
        }
    }
    /**
     * Checks if the sepcified language view is deletable as according to business rules.
     * A language view is deletable only if there are no associated language keys.
     * @param int $languageViewID the primary key of the language view
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public function is_languageView_deletable($languageViewID)
    {
        if (!is_numeric($languageViewID)) {
            throw new \InvalidArgumentException("'langagueViewID' must be numeric");
        }
        $this->CI->load->model("languageView_model");
        $languageView = $this->CI->languageView_model->get_by_primary_key($languageViewID);
        $this->CI->load->model("languageKey_model");
        $languageKeys_count = $this->CI->languageKey_model->count(array("languageView_id" => $languageViewID));
        if ($languageKeys_count > 0) {
            $this->set_message("is_languageView_deletable", "Can not delete language view '$languageView->name' because there are $languageKeys_count language keys associated with this view.");

            return false;
        } else {
            return true;
        }
    }
    /**
     * Verifies that the specified language tag is not already defined in the language table.
     * @param string $tag
     * @return boolean True if the language tag does not already exist, False if the language tag already exists
     */
    public function is_language_tag_unique($tag)
    {
        $this->CI->load->model("language_model");
        $languages_count = $this->CI->language_model->count(array("tag" => $tag));
        if ($languages_count > 0) {
            $this->set_message(__FUNCITON__, "There is already a language with tag '$tag' that exists");

            return false;
        } else {
            return true;
        }
    }
    /**
     * Verifies that the specified language view name is not already defined in the language view table.
     * @param true $name
     * @return boolean False if the language view name exists, true of the language view name does not already exist
     */
    public function is_languageView_name_unique($name)
    {
        $this->CI->load->model("languageView_model");
        $languageViews = $this->CI->languageView_model->get_aprax(array("name" => $name));
        if (empty($languageViews)) {
            return true;
        } else {
            $this->set_message(__FUNCTION__, "A language view with the name '$name' already exists");

            return false;
        }
    }
    /**
     * Verifies that the specified language is deletable as according to business rules.
     * @param int $languageID
     * @return boolean
     * @throws \InvalidArgumentException
     */
    public function is_language_deletable($languageID)
    {
        if (!is_numeric($languageID)) {
            throw new \InvalidArgumentException("'languageID' must be numeric");
        } else {
            $this->CI->load->model("languageKey_business_language_model");
            $languageKey_business_language_count = $this->CI->languageKey_business_language_model->count(array("language_id" => $languageID));

            $this->CI->load->model("language_model");
            $language = $this->CI->language_model->get_by_primary_key($languageID);
            if ($languageKey_business_language_count > 0) {

                $this->set_message(__FUNCTION__, "Can not delete language '{$language->tag}' because there are $languageKey_business_language associated with this language");

                return false;
            }
            $this->CI->load->model("business_language_model");
            $business_language_count = $this->CI->business_language_model->count(array("language_id" => $languageID));
            if ($business_language_count > 0) {
                $this->set_message(__FUNCTION__, "Can not delete language '{$language->tag}' because there are $business_language_count businsesses associated with this language");

                return false;
            }

            return true;
        }
    }
}
