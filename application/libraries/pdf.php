<?php

/**
 * This class does not follow MVC standards, do not extend this class further.
 */
class Pdf extends CI_Model
{
    public function invoice($html, $invNumber, $business, $action, $customer_id, $customFields, $font_set = 'ascii')
    {       
        $this->load->library("tcpdfcustom");

        $this->tcpdfcustom->setFontSubsetting(true);

        $font_set = $this->tcpdfcustom->font_set[$font_set];

        // set document information
        $this->tcpdfcustom->SetCreator(PDF_CREATOR);
        $this->tcpdfcustom->SetAuthor('Drop Locker');
        $this->tcpdfcustom->SetTitle('Invoice');
        $this->tcpdfcustom->SetSubject('Invoice');
        $this->tcpdfcustom->SetKeywords('Drop Locker, invoice, PDF');

        // set default header data
        //logo_withspinclean.gif
        $this->load->model("image_placement_model");
        $image = $this->image_placement_model->get_by_name("invoice", $business->businessID);
        $image_file_path = FCPATH . "images/logos/{$image->filename}";
        $size = getimagesize($image_file_path);

        $image_ratio = $size[1] / max($size[0], 1);
        $image_width = round(20 / $image_ratio);


        //$address = "test";
        $address = $business->address." ".$business->address2."\n".$business->city.", ".$business->state." ".$business->zipcode."\nPhone: ".$business->phone."\n";
        $address .= $business->email."\n".$business->website_url."\n";
        
        if (!empty($customFields)) {
            foreach ($customFields as $field) {
                $address.= $field->title.': '.$field->value."\n";
            }
        }

        $companyName = $business->companyName;

        $invoice_right_header = get_business_meta($this->business_id, "invoice_right_header", "");
        if (!empty($invoice_right_header)) {
            $address = $invoice_right_header;
            $companyName = "";
            $this->tcpdfcustom->setIsHtmlHeaderAddress(true);
        }

        //$pdf->setRTL(true);
        $this->tcpdfcustom->SetHeaderData($image_file_path, $image_width, $companyName, $address);

        // set header and footer fonts
        $this->tcpdfcustom->setHeaderFont(Array($font_set['PDF_FONT_NAME_MAIN'], '', PDF_FONT_SIZE_MAIN));
        $this->tcpdfcustom->setFooterFont(Array($font_set['PDF_FONT_NAME_DATA'], '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $this->tcpdfcustom->SetDefaultMonospacedFont($font_set['PDF_FONT_MONOSPACED']);

        //set margins
        $this->tcpdfcustom->SetMargins(PDF_MARGIN_LEFT, '50', PDF_MARGIN_RIGHT);
        $this->tcpdfcustom->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->tcpdfcustom->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $this->tcpdfcustom->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $this->tcpdfcustom->SetFont($font_set['dejavusans'], '', 14, '', true);

        // ---------------------------------------------------------

        enter_translation_domain('admin/reports_invoice_pdf');

        $this->tcpdfcustom->AddPage();
        $this->tcpdfcustom->setRTL(false);

        $invoice_pdf_message = get_business_meta($this->business_id, "invoice_pdf_message", "");
        if (!empty($invoice_right_header)) {
            $y = $this->tcpdfcustom->getY();
            $this->tcpdfcustom->SetFont($font_set['dejavusans'], '', 10, '', true);
            $this->tcpdfcustom->writeHTMLCell(0, '', '', $y, $invoice_pdf_message, 0, 0, 0, true, 'J', true);
            $this->tcpdfcustom->Ln(20);
        }

        $this->tcpdfcustom->SetFont($font_set['helvetica'], 'BI', 16);

        $bill_to = __("bill_to", "BILL TO", $customer_id);
        $invoice = __("invoice", "INVOICE", $customer_id);

        $this->tcpdfcustom->Cell(0, '', $bill_to, 0, 0, 'L', 0, '', 0);
        $this->tcpdfcustom->Cell(0, '', $invoice, 0, 1, 'R', 0, '', 0);

        //get customer info
        $getCustomer = "SELECT invoicing.*, invoice.* FROM invoice
                                        JOIN orders ON orderID = order_id
                                        JOIN invoicing ON invoicing.customer_id = orders.customer_id
                                        WHERE invoiceNumber = '$invNumber'
                                        LIMIT 1;";
        $query = $this->db->query($getCustomer);
        $customerInfo = $query->result();

        $date_format = get_business_meta($business->businessID, "shortDateDisplayFormat");

        if ($invNumber) {
            $invNumber = $customerInfo[0]->invoiceNumber;
            $invDate= convert_from_gmt_aprax($customerInfo[0]->dateInvoiced, $date_format, $business->businessID);
            $invTime = strtotime($customerInfo[0]->dateInvoiced);
        } else {
            $invNumber = strtotime("now");
            $invDate =  convert_from_gmt_aprax('now', $date_format, $business->businessID);
            $invTime = time();
        }

        $dueDate = str_replace('Net ', '', $customerInfo[0]->terms);
        if ($dueDate == 'Credit Card') {
            $dueDate = __("paid_by_credit_card", "PAID - by credit card", $customer_id);
        } elseif ($dueDate == 'Upon Receipt') {
            $dueDate  = __("upon_receipt", "Upon Receipt", $customer_id);
        } else {
            $dueDate = date($date_format, strtotime('+'.$dueDate . ' Days', $invTime));
        }

        $invTerms       = $customerInfo[0]->terms;
        if (!empty($customerInfo[0]->terms)) {
            $invTerms   = __($customerInfo[0]->terms, $customerInfo[0]->terms, $customer_id);
        }
        $invoice_date   = __("invoice_date"  , "Invoice Date:"  , $customer_id);
        $invoice_number = __("invoice_number", "Invoice Number:", $customer_id);
        $invoice_terms  = __("invoice_terms" , "Invoice Terms:" , $customer_id);
        $due_date       = __("due_date"      , "Due Date:"      , $customer_id);

        $this->tcpdfcustom->SetFont($font_set['helvetica'], '', 12);
        $billingAddr = $customerInfo[0]->billingName."\n".$customerInfo[0]->address."\n";
        $this->tcpdfcustom->MultiCell(100, 1, $billingAddr, 0, 'L', 0, 0, '', '', true, 0, false);
        $this->tcpdfcustom->SetFont($font_set['helvetica'], 'B', 12);
        $this->tcpdfcustom->MultiCell(40, 1, $invoice_date."\n".$invoice_number."\n".$invoice_terms."\n".$due_date, 0, 'L', 0, 0, '', '', true, 0, false);
        $this->tcpdfcustom->SetFont($font_set['helvetica'], '', 12);
        $this->tcpdfcustom->MultiCell(0, 1, $invDate."\n".$invNumber."\n".$invTerms."\n".$dueDate, 0, 'R', 0, 1, '', '', true, 0, false, true);

        $this->tcpdfcustom->SetFont($font_set['helvetica'], '', 8);

        $this->tcpdfcustom->writeHTML($html, 1, 0, 1, 0 ,'L');

        leave_translation_domain();

        $filename = "{$invNumber}.pdf";

        if ($action == 'save') {
            $this->tcpdfcustom->Output($filename, 'I');
        } elseif ($action == 'email') {
            $filename = sys_get_temp_dir()."/".$filename;
            $this->tcpdfcustom->Output($filename, 'F');
            return $filename;
        }
    }

}
