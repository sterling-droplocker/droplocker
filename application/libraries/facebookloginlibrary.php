<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;

class FacebookLoginLibrary
{

    /**
     * The Facebook API
     *
     * @var Facebook
     */

    public $facebook;
    public $business_id;
    public $business;
    public $appId;
    public $accessToken;

    const DEFAULT_GRAPH_VERSION = 'v2.8';

    public function __construct($params)
    {
        require_once APPPATH . "third_party/facebook-php-sdk-5/src/Facebook/autoload.php";
        require_once APPPATH . "third_party/facebook-php-sdk-5/src/Facebook/PersistentData/CodeigniterPersistentDataHandler.php";

        if (!isset($params['appId'])) {
            throw new \InvalidArgumentException("Missing appId");
        }

        if (!isset($params['secret'])) {
            throw new \InvalidArgumentException("Missing secret");
        }

        if (empty($params['business_id'])) {
            throw new \InvalidArgumentException("Missing business_id");
        }

        $this->facebook = new \Facebook\Facebook(array(
            'app_id' => $params['appId'],
            'app_secret' => $params['secret'],
            'default_graph_version' => self::DEFAULT_GRAPH_VERSION,
            'persistent_data_handler' => new CodeigniterPersistentDataHandler(),
            'allowSignedRequest' => false
        ));

        $this->business_id = $params['business_id'];
        $this->appId = $params['appId'];
    }

    public function getAccessToken($redirect_uri = "")
    {
        $helper = $this->facebook->getRedirectLoginHelper();
        try {
            $this->accessToken = $helper->getAccessToken($redirect_uri);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    public function getLoginUrl($redirect = null)
    {
        $helper = $this->facebook->getRedirectLoginHelper();

        $params = array("scope" => "email");
        if (!$redirect) {
            $redirect = base_url(uri_string());
        }

        return $helper->getLoginUrl($redirect, $params);
    }

    public function getLogoutUrl($next)
    {
        return $this->facebook->getLogoutUrl(array(
            "next" => $next
        ));
    }

    public function getFacebookUserId()
    {
        //return $this->facebook->getUser();
    }

    public function loginOrCreateCustomer($redirect_uri = "")
    {
        $this->getAccessToken($redirect_uri);

        if (empty($_REQUEST['code'])) {
            return array(
                'status' => 'error',
                'message' => get_translation("facebook_missing_token", "facebook_errors", array(), "An error ocurred while trying to access Facebook API"),
                'data' => 'Missing Facebook token.'
            );
        }

        //try to get fb user

        try {
            $response = $this->facebook->get('/me?fields=id,email,first_name,last_name', $this->accessToken);
            $user = $response->getGraphObject()->asArray();

        } catch (FacebookApiException $e) {

            // clear facebook cookie that got stuck and throws this exception
            $cookie_name = 'fbsr_' . $this->facebook->getAppId();
            setcookie($cookie_name, "", strtotime("-1 year"), "/", $_SERVER["HTTP_HOST"]);

            return array(
                'status' => 'error',
                'message' => get_translation("facebook_error", "facebook_errors", array(), "An error ocurred while trying to access Facebook API"),
                'data' => 'Error ' . $e->getCode() . ':' . $e->getMessage()
            );
        }

        return $this->loginOrCreateCustomerWithFacebookId($user['id'], $user);
    }

    public function loginOrCreateCustomerWithFacebookId($facebook_id, $user)
    {
        // search customer by facebook_id
        $customer = Customer::search(array(
            "facebook_id" => $facebook_id,
            "business_id" => $this->business_id
        ));

        if ($customer) {
            return array(
                'status' => 'success',
                'message' => get_translation("facebook_loggedin_notice", "business_account/main_index", array(), "Logged in through your Facebook Account"),
                'data' => array(
                    'customer' => $customer->to_array(),
                    'firstTime' => false
                )
            );
       }

       // if no customer, create one with facebook data
       if (!$customer && empty($user['email'])) {
           return array(
               'status' => 'error',
               'message' => get_translation("facebook_missing_email", "business_account/main_index", array(), "Missing email field"),
               'data' => 'Missing email field from Facebook response.'
           );
       }

        // search customer by email
        $customer = Customer::search(array(
            "email" => $user['email'],
            "business_id" => $this->business_id
        ));

        // if found, update it's facebook_id
        if ($customer) {
            $customer->facebook_id = $facebook_id;
            $customer->save();

            return array(
                'status' => 'success',
                'message' => get_translation("facebook_merged_success", "business_account/main_index", array(), "Merged your existing LaundryLocker account with your Facebook account"),
                'data' => array(
                    'customer' => $customer->to_array(),
                    'firstTime' => false
                )
            );
        }


        $CI = get_instance();
        $CI->load->model('customer_model');

        $newCustomer_id = $CI->customer_model->new_customer($user['email'], uniqid(), $this->business_id, "", "facebook");
        if (! $newCustomer_id) {
            throw new Exception("Failed to create new Customer");
        }

        $business = new Business($this->business_id);

        $customer = new Customer($newCustomer_id);
        $customer->firstName = $user['first_name'];
        $customer->lastName = $user['last_name'];

        if (!empty($user['hometown']['name'])) {
            $customer->city = current(explode(",", $user['hometown']['name']));
        }

        $customer->facebook_id = $facebook_id;
        $customer->default_business_language_id = $business->default_business_language_id;

        $all_states = get_all_states_by_country_as_array();
        $states = $all_states['usa'];

        $state_abbreviation = array_search($location[1], $states);
        if (! empty($state_abbreviation)) {
            $customer->state = $state_abbreviation;
        }

        $customer->save();

        return array(
            'status' => 'success',
            'message' => get_translation("facebook_signup_success", "business_account/main_index", array(), "Successfully signed up through Facebook"),
            'data' => array(
                'customer' => $customer->to_array(),
                'firstTime' => true
            )
        );
    }
}