<?php
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Zacl
{
    // Set the instance variable
    public $CI;

    public function __construct()
    {
        $this->CI = get_instance();
        try {
            $business_employee = new Business_Employee($this->CI->session->userdata('buser_id'));
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("Error", "Could not find business employee ID '{$this->CI->session->userdata('buser_id')}' in {$this->CI->db->database}");
            trigger_error("Could not find business employee ID {$this->CI->session->userdata('buser_id')} in {$this->CI->db->database}");
            $this->CI->session->unset_userdata('buser_id');
            redirect("/admin/login");
        }

        if (in_bizzie_mode() && is_superadmin()) {
            $master_business = get_master_business();
            $business_id = $master_business->businessID;
        } else {
            $business_id = $this->CI->session->userdata('business_id');
        }

        set_include_path(get_include_path() . PATH_SEPARATOR . BASEPATH . "application/libraries");
        require_once(APPPATH . '/libraries/Zend/Acl.php');
        //require_once(APPPATH . '/libraries/Zend/Acl/Role.php');
        //require_once(APPPATH . '/libraries/Zend/Acl/Resource.php');
        $this->acl = new Zend_Acl();

        // Set the default ACL
        $this->acl->addRole(new Zend_Acl_Role('default'));

        $aclresources = $this->CI->db->get('aclresources')->result();
        foreach ($aclresources AS $aclresource) {
            if (!$this->acl->has($aclresource->resource)) {
                $this->acl->add(new Zend_Acl_Resource($aclresource->resource));
            }

            if ($aclresource->default_value == 'true') {
                $this->acl->allow('default', $aclresource->resource);
            }
        }

        // Get the ACL for the roles
        $this->CI->db->order_by("roleorder", "ASC");

        //The following conditional checks to see if we need to find all acl role types, or only the acl role type for the current user. We only need all the aclrole types if we are viewing the all the employee roles or the roles for a particular employee.
        if ($this->CI->uri->uri_string == "admin/employees/roles" || $this->CI->uri->segment(3) == "employee_roles" || $this->CI->uri->uri_string == "admin/admin/update_role_ajax") {
            $query = $this->CI->db->get_where('aclroles',array('business_id'=>$business_id));
        } else {
            $query = $this->CI->db->get_where('aclroles',array('business_id'=>$business_id, 'aclID' => $business_employee->aclRole_id));
        }

        $aclRoles  = $query->result();

        foreach ($aclRoles AS $aclRole) {
            $role = (string) $aclRole->name;
            $this->acl->addRole(new Zend_Acl_Role($role), 'default');

            // get the ACL for roles
            $this->CI->db->join('aclresources', 'acl.resource_id = aclresources.id');
            $this->CI->db->where('type', 'role');
            $this->CI->db->where('type_id', $aclRole->aclID);
            $this->CI->db->where('acl.business_id',$business_id);
            $acls = $this->CI->db->get('acl')->result();

            foreach ($acls AS $acl) {
                if ($acl->action == "allow") {
                    try {
                        $this->acl->allow($role, $acl->resource);
                    } catch (Exception $e) {
                        trigger_error(var_export($e), E_USER_WARNING);
                    }
                } else {
                    try {
                        $this->acl->deny($role, $acl->resource);
                    } catch (Exception $e) {
                        trigger_error(var_export($e), E_USER_WARNING);
                    }
                }
            }

            //The following conditional checks to see if we need to find all acl role types, or only the acl role type for the current user. We only need all the aclrole types if we are viewing the all the employee roles or the roles for a particular employee.
            if ($this->CI->uri->uri_string == "admin/employees/roles" || $this->CI->uri->segment(3) == "employee_roles" || $this->CI->uri->uri_string == "admin/admin/update_role_ajax") {
                $userquery = $this->CI->db->get_where('business_employee', array('aclRole_id'=>$aclRole->aclID, 'business_id'=>$business_id));
            } else {
                $userquery = $this->CI->db->get_where('business_employee', array('aclRole_id'=>$aclRole->aclID, 'business_id'=>$business_id, 'employee_id' => $this->CI->session->userdata('employee_id')));
            }

            $userquery_result = $userquery->result();

            foreach ($userquery_result AS $userrow) {
                $this->acl->addRole(new Zend_Acl_Role($userrow->ID), $role);

                $this->CI->db->join('aclresources', 'acl.resource_id = aclresources.id');
                $this->CI->db->where('type', 'user');
                $this->CI->db->where('acl.business_id',$business_id);
                $this->CI->db->where('type_id', $userrow->ID);
                $usersubquery = $this->CI->db->get('acl');
                $usersubquery_result = $usersubquery->result();
                foreach ($usersubquery_result AS $usersubrow) {
                    if ($usersubrow->action == "allow") {
                        $this->acl->allow($userrow->ID, $usersubrow->resource);
                    } else {
                        $this->acl->deny($userrow->ID, $usersubrow->resource);
                    }
                }
            }
        }
    }


    /**
     * get_employee_role method gets the role name for the employee
     *
     * A logged in employee has a buser_id set as a cookie. The buser_id is mapped in the business_employee table
     *
     * @example $role = $this->zacl->get_employee_role($this->session->userdata('buser_id));
     *
     * @param int $business_employee_id
     * @return string role
     */
    public function get_employee_role($business_employee_id)
    {


        // If the user is not logged in yet, just return true
        if ($business_employee_id=='') {
            return true;
        } elseif (!is_numeric($business_employee_id)) {
            throw new \InvalidArgumentException("'business_employee_id' must be numeric.");
        }

        $CI = get_instance();
        $sql = "SELECT * FROM business_employee
                        INNER JOIN aclroles a ON a.aclID = business_employee.aclRole_id
                        WHERE ID = ?";
        $query = $CI->db->query($sql, array($business_employee_id));
        $row = $query->row();
        if (!empty($row)) {
            return $row->name;
        } else {
            return null;
        }
    }

    /**
     * The the id of the role
     *
     *
     *
     * @example $role = $this->zacl->get_employee_role($this->session->userdata('buser_id));
     *
     * @param int $business_id
     * @param string $roleName
     * @return int aclID
     */
    public function getRoleID($business_id, $roleName='')
    {
        // If the user is not logged in yet, just return true
        if ($business_id=='') {
            throw new Exception("Missing Business ID");
        }

        $CI = get_instance();
        $sql = "SELECT * FROM aclroles
                WHERE business_id = {$business_id}
                AND name = '{$roleName}'";
        $query = $CI->db->query($sql);
        $row = $query->row();

        return $row->aclID;
    }


    /**
     * Function to check if the current or a preset employee role has access to a resource
     *
     * @example $this->zacl->check_acl('new_order');
     * The above example will check to see if the current logged in employee has access to the resource "new_order"
     *
     * Almost all controllers in the admin section use a helper called check_acl() that created a resource for each controller/function
     *
     * @param string $resource
     * @param int|string $role If int, the business employee ID. If string, the name of the role. If null, retrieves the roll for the logged in user.
     * @return boolean True if the user has acess, false if the user does not have access
     */
    public function check_acl($resource, $role=null)
    {
        $CI = get_instance();
        if (is_null($role)) {
            $user_role = $this->get_employee_role($CI->session->userdata('buser_id'));
        } elseif (is_string($role)) {
            $user_role = $role;
        } else {
            $user_role = $this->get_employee_role($role);
        }
        // if the resource no exists, or we are not trying to protect the resource
        if (!$this->acl->has($resource)) {
            return true;
        }

        try {
            $access = $this->acl->isAllowed($user_role, $resource);
        } catch (Zend_Exception $e) {
            $access = false;
        }

        return $access;
    }

    /**
     * Sends a request to the operations email for a business
     *
     * test for this function is in the tests/zacl folder
     *
     * @param string $page
     * @param string $resource
     * @param string $post
     * @param int buser_id optional
     * @param int $business_id
     * @throws Exception is the business_id or buser_id is not set
     * @return int emailLogID
     */
    public function requestPermission($page, $resource, $post, $buser_id='', $business_id='')
    {
        $CI = get_instance();

        $buser_id = (empty($buser_id))?$CI->session->userdata('buser_id'):$buser_id;
        if (empty($buser_id)) {
            throw new Exception("Missing buser_id from session");
        }

        $business_id = (empty($business_id))?$CI->session->userdata('business_id'):$business_id;
        if (empty($business_id)) {
            throw new Exception("Missing business_id from session");
        }

        $employee_id = get_employee_id($CI->session->userdata('buser_id'));
        $employee = new Employee($employee_id);
        $employeeName = $employee->firstName." ".$employee->lastName;
        $body =  "Employee: ". $employee->firstName." ".$employee->lastName."<br>";
        $body .=  "Employee Email: ". $employee->email."<br>";
        $body .= "Role: " . $this->get_employee_role($CI->session->userdata('buser_id')). "<br>";
        $body .= "Page: " . $post['page'] . "<br>";
        $body .= "Resource: ". $post['page'] . "_" . $post['resource'] . "<br>";
        $body .= "requested full url: " . $post['getValues'] . "<br><br>";
        $body .= "<a href='https://droplocker.com/admin/employees/roles'>Click here to manage roles</a>";
        $to = (get_business_meta($business_id, 'operations')=='')?get_business_meta($business_id, 'customerSupport'):get_business_meta($business_id, 'operations');

        return queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $to, 'Request permission to resource', $body, array($employee->email), null, $business_id);

    }
}
