<?php
use DropLocker\Util\Lock;
use App\Libraries\DroplockerObjects\Droid_PostDeliveries;
use App\Libraries\DroplockerObjects\Droid_PostPickup;

require_once(APPPATH . "third_party/firebase/src/firebaseLib.php");

class DriverAppSyncLibrary extends CI_Model
{
    
    protected $commandQueueNode = '/DriverAppUpdates';
    
    const VERSION = '3.0';
    private $version = 3.0;
    
    protected $apiTimeout = 20;
    protected $last_sync_meta = 'driver_app_last_sync_id';
    protected $next_sync_meta = 'driver_app_next_sync_date';
    protected $sync_time_meta = 'driver_app_sync_time';
    
    protected $journal_sync_max_entries = 100;
    protected $firebase_timeout = 20;
    protected $firebase_retries = 10;

    protected $defaultSyncTime = "6:00";

    public function __construct() {
        parent::__construct();
        $this->loadFirebaseConfig();
    }
    
    public function loadFirebaseConfig() {
        $this->config->load('firebase');
        $this->firebase_timeout = $this->config->item('firebase_timeout');
        $this->firebase_retries = $this->config->item('firebase_retries');
        $this->journal_sync_max_entries = $this->config->item('journal_sync_max_entries');
    }

    /* 
     * Update data related to a specific entity (order or claim) at entity level 
     */
    public function syncEntity($day, $table, $id)
    {
        try {
            if (true || !ob_get_level()) { //force separate log
                ob_start();
            }

            $data = $this->getDataFromOrder($day, $table, $id);        
            if (!$data) {
                $this->out("No routes found for day $day for $table $id. Skipping...");
                $res['success'] = true;
            } else {
                $this->out("Found $table $id on " . $this->arrayToString($data));
                
                $business_id = $data['business_id'];
                $businessRoutes = (array)$data['routes'];

                $path = array(
                    '___first',
                    '___first',
                    array(
                        'node' => 'windows',
                        'id' => '___first',
                    ),
                    '___first',
                    array(
                        'node' => $table,
                        'id' => $id,
                    )
                );

                $res = $this->sync($day, $business_id, $businessRoutes, $path, $this->getFilter($table, $id));

                if ($res['success']) {
                    $this->out("$table $id found on route {$businessRoutes[0]} for business $business_id on day $day processed succesfully");
                } else {
                    $this->out("ERROR: Something went wrong.");
                }
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();

            return array(
                'success' => FALSE,
                'message' => "Error while updating $table $id on day $day. Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        $res['table'] = $table;
        $res['id'] = $id;
        
        $log = $this->getOutputLog();
        $res['log'] = $log;
        
        return $res;
    }
    
    public function syncDriverNote($day, $table, $id, $business_id)
    {
        try {
            if (true || !ob_get_level()) { //force separate log
                ob_start();
            }

            $path = array(
                '___first',
                '___first',
                array(
                    'node' => $table,
                    'id' => $id,
                )
            );

            $res = $this->sync('from_driver_note', $business_id, null, $path, $this->getFilter($table, $id));

            if ($res['success']) {
                $this->out("$table $id found for business $business_id processed succesfully");
            } else {
                $this->out("ERROR: Something went wrong.");
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();

            return array(
                'success' => FALSE,
                'message' => "Error while updating $table $id on day $day. Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        
        $res['table'] = $table;
        $res['id'] = $id;
        
        $log = $this->getOutputLog();
        $res['log'] = $log;
        
        return $res;
    }
                    
    
    public function syncRouteForBusiness($day, $business_id, $route, $callback = null, $callbackParams = array())
    {
        $previousBuffering = ob_get_level();
        
        $time = time();
        $success = TRUE;
        $message = "";
        
        try {
            if (!$previousBuffering) { 
                ob_start();
            }
            
            $path = array($business_id, $route);
            $res = $this->sync($day, $business_id, $route, $path);
            if ($res['success']) {
                unset($res['node']);
                $message = "Route $route for business $business_id for day $day updated succesfully";
                $this->out($message);
            } else {
                $success = FALSE;
                $message = "ERROR: Something went wrong.";
                $this->out($message);
            }
        } catch (Exception $e) {
            $success = FALSE;
            $message = "Error while updating route $route for business $business_id on day $day. Error: {$e->getMessage()}";
            $errorTrace = $e->getTrace();
        }
        
        $memory_profile = getMemoryInfo();
        
        $responseData = array(
            'business_id' => $business_id, 
            'route' => $route, 
            'time' => $this->toHours(time() - $time),
            'memory' => $memory_profile['summary'],
            'day' => $day, 
            'success' => $success,
            'message' => $message,
            'log' => $this->getOutputLog(), 
            'detail' => $res,
            'errorTrace' => $errorTrace,
            "memory_profile" => $memory_profile,
        );
        
        $summmary = array(
            'success' => $success,
            'message' => $message,
        );
        
        if (is_callable($callback))  {
            $callbackParams[] = $responseData;
            call_user_func_array ($callback, $callbackParams);
        } else {
            $summmary = $responseData;
        }
        
        if ($previousBuffering) {
            ob_start();
        }
        
        return $summmary;
    }
    
    public function syncLocationRoute($day, $table, $id, $business_id, $setup)
    {
        try {
            if (true || !ob_get_level()) { //force separate log
                ob_start();
            }
            
            $eventIdx = array_search('update_route', $setup['event']);
            $old = $setup['oldData'][$eventIdx];
            $new = $setup['newData'][$eventIdx];
            
            $filter = array(
                'locationID' => $id,
                'no_customer_subscription' => true, //location fields do not affect customer subscription stops
                'no_time_window' => true, //if delivery window for location, location route not used
            );
            
            $firebaseNodes = $this->getFirebaseTree($day, $business_id, (array) $new, $filter);

            if (count($firebaseNodes)) {
                $paths = $this->getPathsForStopsInRoute($firebaseNodes);
                $res = $this->syncNodes($firebaseNodes, $paths);
            } else {
                $this->out("No data to send");
                $res['success'] = true;
            }

            if ( !$res['success'] && ! isset($res['message'])) {
                $res['message'] =  __FUNCTION__ . ": Something when wrong";
            }

            if ($res['success']) {
                $this->out("$table $id found for business $business_id processed succesfully");
                
                $this->load->model('location_model');
                $deliveryWindows = $this->location_model->getDeliveryWindowsForLocation($id);
                $this->out(count($deliveryWindows) . " delivery windows found for location $id");
                
                if (!count($deliveryWindows)) {
                    $this->removeLocationStops($business_id, $old, $id);
                    $this->moveCompletedLocations($business_id, $id, $old, $new);
                } else {
                    $this->out("Ignoring location route changes");
                }
                
            } else {
                $this->out("ERROR: Something went wrong.");
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();

            return array(
                'success' => FALSE,
                'message' => "Error while updating $table $id on day $day. Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        
        $res['table'] = $table;
        $res['id'] = $id;
        
        $log = $this->getOutputLog();
        $res['log'] = $log;
        
        return $res;
    }

    public function removeLocationStops($business, $route, $id) 
    {
        $this->out(">>> Removing stops from old route $route", false, 1);
        
        $removeList = array();
        
        $this->outDisable();
        $windows = $this->getNode("/business/business-$business/routes/route-$route/windows");
        $this->outEnable();
        
        foreach ($windows as $windowIdx => $window) {
            foreach ($window['stops'] as $stopIdx => $stop) {
                if($stop['locationID'] == $id && $stop['stopType'] !== 'Delivery_CustomerSubscription') {
                    $removeList[] = $stop['node_path'];
                }
            }
        }
        
        $res = $this->removeNode($removeList);
        
        return $res;
    }
    
    public function moveCompletedLocations($business, $id, $old, $new) 
    {
        $this->out(">>> Moving completed location from route $old to route $new", false, 1);
        
        $removeList = array();
        $addList = array();
        
        $oldStopsPath = "/business/business-$business/completed_locations/routes/route-$old/windows";
        $newStopsPath = "/business/business-$business/completed_locations/routes/route-$new/windows";
        
        $this->outDisable();
        $windows = $this->getNode($oldStopsPath);
        $this->outEnable();
        
        foreach ($windows as $windowIdx => $window) {
            foreach ($window['stops'] as $stopIdx => $stop) {
                $relPath = "$windowIdx/stops/$stopIdx";
                if($stop['locationID'] == $id && $stop['stopType'] !== 'Delivery_CustomerSubscription') {
                    $stop['-route_changed_from'] = $old;
                    $stop['-route_changed_gmt_time'] = gmdate("Y-m-d H:i:s");
                    
                    $removeList[] = "$oldStopsPath/$relPath";
                    $addList[] = array(
                        'path' => "$newStopsPath/$relPath",
                        'node' => $stop,
                    );
                }
            }
        }
        
        $this->removeNode($removeList);
        
        if (count($addList)) {
            array_unshift($addList, array(
                'path' => "/business/business-$business/completed_locations/routes/route-$new/isCompleteRoute",
                'node' => true,
            ));
        }
        
        foreach ($addList as $add) {
            $this->sendToFirebaseWithRetry($add['path'], $add['node']);
        }
    }
    
    protected function getPathsForStopsInRoute($firebaseNodes){
        $paths = array();

        $businessIdx = $this->getFirstKey($firebaseNodes['business']);
        $routeIdx = $this->getFirstKey($firebaseNodes['business'][$businessIdx]['routes']);

        $routePath = array(
            $this->getIdFromKey($businessIdx),
            $this->getIdFromKey($routeIdx),
        );

        $route = $firebaseNodes['business'][$businessIdx]['routes'][$routeIdx];

        foreach ($route['windows'] as $windowIdx => $window) {
            $windowPath = array_merge(
                $routePath,
                array(
                    array(
                        'node' => 'windows',
                        'id' => $this->getIdFromKey($windowIdx),
                    ),
                )
            );

            foreach ($window['stops'] as $stopIdx => $stop) {
                $paths[] = array_merge(
                    $windowPath,
                    array(
                        $this->getIdFromKey($stopIdx),
                    )
                );
            }
        }
        
        return $paths;
    }
    
    protected function getIdFromKey($key)
    {
        return substr($key, strpos($key, '-') + 1);
    }
    
    /* 
     * Update data related to a specific entity (order or claim) at a route level 
     */
    public function syncRouteForEntity($day, $table, $id)
    {
        try {
            if (!ob_get_level()) { 
                ob_start();
            }

            $data = $this->getDataFromOrder($day, $table, $id);        
            if (!$data) {
                $this->out("No routes found for day $day for $table $id. Skipping...");
                $res['success'] = true;
            } else {
                $this->out("Found $table $id on " . $this->arrayToString($data));

                $business_id = $data['business_id'];
                $businessRoutes = (array)$data['routes'];

                $path = array($business_id, $businessRoutes[0]);

                $res = $this->sync($day, $business_id, $businessRoutes, $path);

                if ($res['success']) {
                    $this->out("Route {$businessRoutes[0]} for business $business_id for day $day containing $table $id updated succesfully");
                } else {
                    $this->out("ERROR: Something went wrong.");
                }
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();

            return array(
                'success' => FALSE,
                'message' => "Error while updating routes on day $day for $table $id. Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        
        $log = $this->getOutputLog();
        $res['log'] = $log;
        
        return $res;
    }
    
    public function syncBusinesses($business_id = null, $day = null, $routes = null, $force = false, $callback = null, $callbackParams = array())
    {
        $time = time();
        
        if ($business_id) {
            return $this->syncBusiness($business_id, $day, $routes, $force, $callback, $callbackParams);
        }
        
        $this->load->model('business_model');
        $businessList = $this->business_model->get_all_active_ids_for_driver_app();
        $businessCount = count($businessList);
        
        $results = array(
            'time' => '',
            'memory' => '',
            'success' => true,
            'items' => array(),
        );
        
        foreach ($businessList as $business_id) {
            $res = $this->syncBusiness($business_id, $day, null, $force, $callback, $callbackParams);
            
            $results['success'] = $results['success'] && $res['success'];
            $results['items'][] = $res;
        }
        
        $time = $this->toHours(time() - $time);
        $memory_profile = getMemoryInfo();
        
        $results['time'] = $time;
        $results['memory'] = $memory_profile['summary'];
        $results['message'] = $results['success'] ? "$businessCount businesses processed correctly ($time)" : "Error while processing data for $businessCount businesses ($time)";
        $results['memory_profile'] = $memory_profile;
        
        return $results;
    }
    
    /* 
     * Update data related to a specific business
     * Updates the whole business node, if routes list provided, it will only contain those routes
     */
    public function syncBusiness($business_id, $day = null, $routes = null, $force = false, $callback = null, $callbackParams = array())
    {
        $totalUpdate = empty($routes);
        
        $time = time();
        $success = TRUE;
        $message = "";
        $detail= array();
        $log = array();
        
        try {
            if (!ob_get_level()) { 
                ob_start();
            }
            
            $this->setBusinessTime($business_id);
                    
            $nextSync = get_business_meta($business_id, $this->next_sync_meta, 0);
            $nextSyncTime = strtotime($nextSync);
            $now = strtotime("now");
            $timeLeft = $nextSyncTime - $now;
            if ($force || !$nextSync || $timeLeft < 0) {
                if (!$routes) {
                    $routes = $this->getRoutes($business_id);
                }

                if (!strlen(trim($day))) {
                    $day = $this->getTodayDayNumber($business_id);
                }

                $this->load->model('driver_app_journal_model');
                $lastChangeId = $this->driver_app_journal_model->getLastRecordId();
                $this->out("Last change registered on the system : $lastChangeId");
                
                $log = $this->getOutputLog();
                foreach ($routes as $route) {
                    $routeRes = $this->syncRouteForBusiness($day, $business_id, $route, $callback, $callbackParams);
                    $success = $success && $routeRes['success'];
                    $detail[] = $routeRes;
                }
                if (!ob_get_level()) { 
                    ob_start();
                }

                if ($success && $totalUpdate) {
                    $this->out("Next time business $business_id will process changes newer than: $lastChangeId");
                    update_business_meta($business_id, $this->last_sync_meta, $lastChangeId);
                    $this->updateBusinessSyncTime($business_id);
                    
                    $this->removeNode("/business/business-$business_id/completed_locations");
                    $this->updateBusinessSyncInfo($business_id, $day);
                } else if ($success) {
                    $this->out("Partial sync for business $business_id finished correctly. Routes: " . implode(', ', $routes));
                } else {
                    $message = "ERROR: Something went wrong.";
                    $this->out($message);
                }
            } else {
                $message = "Business $business_id already in sync. Next sync: $nextSync (in " . $this->toHours($timeLeft). "). Skipping...";
                $this->out($message);
            }
        } catch (Exception $e) {
            $success = FALSE;
            $message = "Error while updating business $business_id on day $day . Error: {$e->getMessage()}";
            $errorTrace = $e->getTrace();
        }
        
        $log = array_merge($log, $this->getOutputLog());
        $memory_profile = getMemoryInfo();
        
        $responseData = array(
            'business_id' => $business_id, 
            'time' => $this->toHours(time() - $time),
            'memory' => $memory_profile['summary'],
            'day' => $day, 
            'success' => $success,
            'message' => $message,
            'log' => $log, 
            'detail' => $detail,
            'errorTrace' => $errorTrace,
            "memory_profile" => $memory_profile,
        );
        
        $summmary = array(
            'success' => $success,
            'message' => $message,
        );
        
        if (is_callable($callback))  {
            $callbackParams[] = $responseData;
            call_user_func_array ($callback, $callbackParams);
        } else {
            $summmary = $responseData;
        }
        
        return $summmary;
    }

    protected function updateBusinessSyncInfo($business_id, $day) {
        $dateFormat = "Y-m-d H:i:s";
        $date_gmt = gmdate($dateFormat);
        $date_business = convert_from_gmt_aprax(null, $dateFormat, $business_id);
        
        $info = array();
        $info['-last_sync__gmt_time'] = $date_gmt;
        $info['-last_sync__local_time'] = $date_business;
        $info['-sync_for_day'] = $day;
        $info['-is_partial_sync'] = null;
        
        $path = "/business/business-$business_id";
        
        $this->updateNode($path, $info);
    }
    
    protected function updateBusinessSyncTime($business_id)
    {
        $tomorrow = date('Y-m-d', strtotime('+ 1 day'));
        $syncTyme = get_business_meta($business_id, $this->sync_time_meta, $this->defaultSyncTime);
        $nextSync = "$tomorrow $syncTyme";
        
        update_business_meta($business_id, $this->next_sync_meta, $nextSync);
        
        $this->out("Next sync for business $business_id: $nextSync");
    }
    
    protected function sync($day, $business_id, $businessRoutes, $path, $filters = array())
    {
        $treeInfo = array();
        $firebaseNodes = array();
        
        if (isset($filters['driverNote_id'])) {
            $firebaseNodes = $this->getNotesTree($business_id, $filters);
        } else if (!count($businessRoutes)) {
            $this->out("No routes provided for business $business_id. Skipping...");
        } else {
            $firebaseNodes = $this->getFirebaseTree($day, $business_id, $businessRoutes, $filters);
        }
        
        $treeInfo = $this->syncNode($firebaseNodes, $path);
        
        return $treeInfo;
    }
    
    protected function syncNode($firebaseNodes, $path)
    {
        $treeInfo = array(
            'nodeUpdated' => FALSE,
        );
        
        if (count($firebaseNodes)) {
            try {
                $treeInfo = $this->getSubtree($firebaseNodes, $path);

                $this->out("Sending to Firebase");
                $response = $this->sendToFirebaseWithRetry($treeInfo['path'], $treeInfo['node']);
                $this->out("Done");

                $treeInfo['success'] = $response !== FALSE;
                
                if ($treeInfo['success']) {
                    $treeInfo['nodeUpdated'] = TRUE;
                }
            } catch (Exception $exc) {
                $treeInfo['success'] = false;
                $treeInfo['message'] = $exc->getMessage();
            }
        } else {
            $this->out("No data to send");
            $treeInfo['success'] = true;
        }
        
        if ( !$treeInfo['success'] && ! isset($treeInfo['message'])) {
            $treeInfo['message'] =  __FUNCTION__ . ": Something when wrong";
        }
        
        return $treeInfo;
    }
    
    protected function syncNodes($firebaseNodes, $paths)
    {
        $this->out("Syncronyzing " . count($paths) . " nodes");
        $res = array (
            'success' => true,
            'nodes' => array(),
        );
        
        foreach ($paths as $path) {
            $treeInfo = $this->syncNode($firebaseNodes, $path);
            unset($treeInfo['node']);
            $res['nodes'][] = $treeInfo;
            $res['success'] = $res['success'] && $treeInfo['success'];
        }
                
        return $res;
    }

    protected function getNotesTree($business_id, $filters = array()) {
        $this->setBusinessTime($business_id);
        
        $firebaseNodes = array();
        $this->load->library('routelibrary');
        
        $this->out("Retrieving data for driver notes for business $business_id. " . (count($filters) ? "Filtered for " . $this->arrayToString($filters) : ""));
        
        $driverNotes = $this->routelibrary->getDriverNotes(null, $business_id, $filters);
        $driverNotes = json_decode(json_encode($driverNotes), true);
        
        if (count($driverNotes)) {
            $firebaseNotes = $this->processDriverNotes($driverNotes);
            $firebaseNodes = $this->processStops($driverNotes, $business_id, $firebaseNotes, 'from_driver_notes', true);
        } else {
            $this->out(__FUNCTION__ . ": No data found for note {$filters['driverNote_id']} on business $business_id.");
        }
        
        $this->out("Finished generating.");
        
        return $firebaseNodes;
    }

    protected function getFirebaseTree($day, $business_id, $businessRoutes, $filters = array()) 
    {
        $this->setBusinessTime($business_id);
        
        $type = 'both';
        $loaded = false;
        //$updateSort = $type == 'both';
        $updateSort = false;  
        
        $businessRoutes = (array)$businessRoutes;
        $firebaseNodes = array();
        
        $isPartialSync = count($filters) ? TRUE : NULL;
        $this->out("Retrieving data for day '$day' for business $business_id. " . ($isPartialSync ? "Filtered for " . $this->arrayToString($filters) : ""));
        $this->load->library('routelibrary');
        
        $this->out("Loading routes (" . implode(', ', $businessRoutes) . ") for business $business_id.");
        
        $stops = $this->routelibrary->getStops($businessRoutes, $business_id, $day, $type, $loaded, $updateSort, $filters);
        
        $driverNotes = $this->routelibrary->getDriverNotes($businessRoutes, $business_id);
        
        $stops = json_decode(json_encode($stops), true);
        $driverNotes = json_decode(json_encode($driverNotes), true);
        
        if (count($stops)) {
            $firebaseNotes = $this->processDriverNotes($driverNotes);
            $firebaseNodes = $this->processStops($stops, $business_id, $firebaseNotes, $day, $isPartialSync);
        } else {
            $this->out(__FUNCTION__ . ": No data found.");
        }
        
        $this->out("Finished generating.");
        
        return $firebaseNodes;
    }


    protected $firebaseModelPath = array(
        array(
            'node' => 'business',
            'label' => 'business',
        ),
        array(
            'node' => 'routes',
            'label' => 'route',
        ),
        array(
            'windows' => array(
                'node' => 'windows',
                'label' => 'window',
            ),
            'driverNotes' => array(
                'node' => 'notes',
                'label' => 'note',
            ),
        ),
        array(
            'node' => 'stops',
            'label' => 'stop',
        ),
        array(
            'orders' => array(
                'node' => 'deliveries',
                'label' => 'delivery',
            ),
            'claim' => array(
                'node' => 'pickups',
                'label' => 'pickup',
            ),
        ),
    );
    
    protected $firebaseSearchStep = 2;
    
    protected function getSubtree($firebaseNodes, $pathComp)
    {
        $walkComps = $this->getTreeWalk($firebaseNodes, $pathComp);
        $this->outDeeper();
        $walkComps = $this->validateTreeWalk($walkComps);
        $this->outShallower();
        
        //getSubtree
        $subTree = $firebaseNodes;
        foreach ($walkComps as $step) {
            $subTree = $subTree[$step];
        }
        
        $walk = implode('/', $walkComps);
        
        return array(
            'path' => $walk,
            'node' => $subTree,
        );
    }
    
    protected function getTreeWalk($node, $pathComp) {
        $walkComps = array();
        
        foreach ($pathComp as $key => $id) {
            $nodeSetup = $this->firebaseModelPath[$key];
            
            if (is_array($this->getFirst($nodeSetup))) {
                $nodeSetup = $nodeSetup[$id['node']];
                $id = $id['id'];
            }
            
            $nodeName = $nodeSetup['node'];
            
            if ($id === '___first') {
                $nodeId = $this->getFirstKey($node[$nodeName]);
                
                if(count(array_keys($node[$nodeName])) > 1) {
                    $this->out("Warning: multiple elements found on $nodeName. Retrieving first element anyway ($nodeId)");
                }
            } else {
                $nodeId = $nodeSetup['label'] . '-' . $id;
            }
            
            $walkComps[] = $nodeName;
            $walkComps[] = $nodeId;
            
            $node = $node[$nodeName][$nodeId];
        }
        
        return $walkComps;
    }
    
    protected function validateTreeWalk($walkComps)
    {
        $lastMising = array();
        
        while (count($walkComps) > 1) {
            $currentWalk = implode('/', $walkComps);
            
            if(!$this->nodeExists($currentWalk)) {
                $this->out("Data not found for path '$currentWalk', keep looking upstairs");
                
                if (count($walkComps) >= $this->firebaseSearchStep) {
                    $lastMising = array();
                    for($i = 0; $i < $this->firebaseSearchStep; $i++) {
                        $lastMising[] = array_pop($walkComps);
                    }
                }
            } else {
                $this->out("Data found for path '$currentWalk', let's insert here");
                break;
            }
       }
        
        return array_merge($walkComps, array_reverse($lastMising));
    }


    protected function nodeExists($path)
    {
        $test = 'node_path';
        
        $this->outDisable();
        $this->out("Checking if data exist on $path");
        $node = $this->getNodeShallow($path);
        $this->outEnable();
        
        if ($this->firebaseError) {
            throw new Exception(__METHOD__ . " ($path) : Firebase error while retrieving node");
        }
        
        return isset($node[$test]);
    }
    
    protected function getFilter($table, $id) {
        $filter = array(
            'order_id' => -1,
            'claim_id' => -1,
            'driverNote_id' => null,
            'nonEmpty' => true,
        );
        
        if ($table === "orders") {
            $filter['order_id'] = $id;
        } else if ($table === "claim") {
            $filter['claim_id'] = $id;
        } else if ($table === "driverNotes") {
            $filter['driverNote_id'] = $id;
            unset($filter['nonEmpty']);
        };
        
        return $filter;
    }
    
    protected function getFirstKey($arr) {
        $keys = array_keys($arr);
        return $keys[0];
    }

    protected function getFirst($arr) {
        return $arr[$this->getFirstKey($arr)];
    }
    
    protected function getDataFromOrder($day, $table, $id) 
    {
        $dayField = $this->getDayName($day);
        
        if ($day == 0) {
            // Stores 7 in location_serviceDay for Sunday
            $day = 7;
        }
        
        
        if ($table === "orders") {
            $idField = "orderID";
        } else if ($table === "claim") {
            $idField = "claimID";
        } else {
            return null;
        };
        
        $sql = "SELECT DISTINCT t.business_id, "
            . "COALESCE(subscr_dw.route, loc_dw.route, IF(location_serviceDayID, l.route_id, NULL) ) routes, "
            . "lck.location_id "
            . "FROM $table t "
            . "
                LEFT JOIN delivery_customerSubscription subscr ON (subscr.customer_id = t.customer_id) 
                LEFT JOIN delivery_zone subscr_dz ON (subscr_dz.delivery_zoneID = subscr.delivery_zone_id) 
                LEFT JOIN delivery_window subscr_dw ON (subscr_dw.delivery_zone_id = subscr_dz.delivery_zoneID AND subscr_dw.$dayField > 0)
                
                LEFT JOIN locker lck ON (t.locker_id = lockerID)        
                LEFT JOIN location l ON (lck.location_id = l.locationID)
                LEFT JOIN location_serviceDay ON (l.locationID = location_serviceDay.location_id AND location_serviceDay.day = ?)
                LEFT JOIN delivery_zone loc_dz ON (l.locationID = loc_dz.location_id)
                LEFT JOIN delivery_window loc_dw ON (loc_dz.delivery_zoneID = loc_dw.delivery_zone_id AND loc_dw.$dayField > 0)
            "        

            . "WHERE $idField = ?";
        
        $CI = get_instance();
        $info = $CI->db->query($sql, array($day, $id))->result_array();

        if (isset($info[0]) && strlen($info[0]['routes']) ) {
            $res = $info[0];
        } else {
            $res = FALSE;
        }
        
        return $res;
    }
    
    protected function getRoutes($business_id)
    {
        $this->load->model('location_model');

        $businessRoutes = $this->location_model->get_routes($business_id);
        $businessRoutes = json_decode(json_encode($businessRoutes), true);
        $businessRoutes = array_map(function ($elem) { return $elem['route_id']; }, $businessRoutes);
        
        return $businessRoutes;
    }

    /**
     * From route library
     * Converts a day from numeric representation into text representation
     *
     * @param int $day
     * @return string
     */
    protected function getDayName($day)
    {
        $days = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
        $dayName = $days[$day];
        return $dayName;
    }

    protected function processDriverNotes($driverNotes) {
        $dateFormat = "Y-m-d H:i:s";
        $date_gmt = gmdate($dateFormat);
        
        if (count($driverNotes)) {
            $date_business = convert_from_gmt_aprax(null, $dateFormat, $driverNotes[0]['business_id']);
        }
        
        $firebaseNotes = array();
        
        foreach ($driverNotes as $driverNote) {
            $businessKey = 'business-' . $driverNote['business_id'];
            $routeKey = 'route-' . $driverNote['route_id'];
            $noteKey = "note-" . $driverNote['driverNotesID'];
            $node_path = "business/$businessKey/routes/$routeKey/notes/$noteKey";
            
            $firebaseNotes[$routeKey][$noteKey] = array(
                'refUid' => 'stop-Location-' . $driverNote['locationID']. '-' . $this->clean($driverNote['address'].$driverNote['address2']),
                'id' => $driverNote['driverNotesID'],
                'type' => 'location',
                'object_id' => $driverNote['locationID'],
                'text' => $driverNote['note'],
                'sender_id' => $driverNote['createdBy'],
                'sender' => $driverNote['firstName'] . ' ' . $driverNote['lastName'],
                'date' => $driverNote['created'],
                'business_id' => $driverNote['business_id'],
                'route_id' => $driverNote['route_id'],
                
                'node_path' => $node_path,
                '-last_sync__gmt_time' => $date_gmt,
                '-last_sync__local_time' => $date_business,
            );
        }
        
        return $firebaseNotes;
    }

    protected function processStops($stops, $business_id, $firebaseNotes, $day, $isPartial = NULL) {
        $firebaseNodes = array();
        
        $dateFormat = "Y-m-d H:i:s";
        $date_gmt = gmdate($dateFormat);
        $date_business = convert_from_gmt_aprax(null, $dateFormat, $business_id);
        
        foreach ($stops as $stop) {
            $node_path = 'business';
            
            $businessKey = 'business-' . $business_id;
            $node_path .= "/$businessKey";
            $firebaseNodes[$businessKey]['id'] = $business_id;
            $firebaseNodes[$businessKey]['-last_sync__gmt_time'] = $date_gmt;
            $firebaseNodes[$businessKey]['-last_sync__local_time'] = $date_business;
            $firebaseNodes[$businessKey]['-sync_for_day'] = $day;
            $firebaseNodes[$businessKey]['node_path'] = $node_path;
            $firebaseNodes[$businessKey]['-is_partial_sync'] = $isPartial;
            $firebaseNodes[$businessKey]['-node_path_comps'] = array(
                'businessKey' => $businessKey,
            );
            
            $routeKey = 'route-' . $stop['route'];
            $node_path .= "/routes/$routeKey";
            $firebaseNodes[$businessKey]['routes'][$routeKey]['id'] = $stop['route'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['-last_sync__gmt_time'] = $date_gmt;
            $firebaseNodes[$businessKey]['routes'][$routeKey]['-last_sync__local_time'] = $date_business;
            $firebaseNodes[$businessKey]['routes'][$routeKey]['-sync_for_day'] = $day;
            $firebaseNodes[$businessKey]['routes'][$routeKey]['notes'] = !empty($firebaseNotes[$routeKey]) ? $firebaseNotes[$routeKey] : array();
            $firebaseNodes[$businessKey]['routes'][$routeKey]['node_path'] = $node_path;
            $firebaseNodes[$businessKey]['routes'][$routeKey]['-is_partial_sync'] = $isPartial;
            $firebaseNodes[$businessKey]['routes'][$routeKey]['-node_path_comps'] = array(
                'businessKey' => $businessKey,
                'routeKey' => $routeKey,
            );
            
            if (isset($stop['driverNotesID'])) {
                $this->out("Driver note data detected on stop ({$stop['driverNotesID']}). Skipping the rest of the tree");
                continue;
            }
            
            $windowKey = 'window-' . ($stop['timeWindow'] ? $stop['timeWindow'] : 'all_day');
            $node_path .= "/windows/$windowKey";
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['delivery_windowID'] = $stop['delivery_windowID'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['orderHomeDeliveryID'] = $stop['orderHomeDeliveryID'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['orderHomePickupID'] = $stop['orderHomePickupID'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['timeWindow'] = $stop['timeWindow'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['windowStart'] = $stop['windowStart'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['windowEnd'] = $stop['windowEnd'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['localWindowStart'] = $stop['localWindowStart'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['localWindowEnd'] = $stop['localWindowEnd'];
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['node_path'] = $node_path;
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['-node_path_comps'] = array(
                'businessKey' => $businessKey,
                'routeKey' => $routeKey,
                'windowKey' => $windowKey,
            );
            
            $stopKey = 'stop-' . $stop['type'] . '-' . $stop[lcfirst($stop['type']) . 'ID']. '-' . $this->clean($stop['address1'].$stop['address2']) ;
            $node_path .= "/stops/$stopKey";
            
            $stop['address1'] = "{$stop['address1']} {$stop['address2']}";
            $stop['address2'] = "{$stop['city']} {$stop['state']} {$stop['zip']}";
            $stop['address'] = "{$stop['address1']} {$stop['address2']}";
            $stop['customerName'] = "{$stop['firstName']} {$stop['lastName']}";
            $stop['customerPhone'] = $stop['phone'];
            $stop['stopType'] = $stop['type'];
            
            $stop['ignore_pickups_to_complete'] = strtolower($stop['serviceType']) === 'recurring home delviery';
            $stop['ignore_deliveries_to_complete'] = false;
            $stop['ignore_notes_to_complete'] = false;
            
            $stop['uid'] = $stopKey;
            $stop['node_path'] = $node_path;
            $stop['-last_sync__gmt_time'] = $date_gmt;
            $stop['-last_sync__local_time'] = $date_business;
            $stop['-sync_for_day'] = $day;
            $stop['-is_partial_sync'] = $isPartial;
            $stop['-node_path_comps'] = array(
                'businessKey' => $businessKey,
                'routeKey' => $routeKey,
                'windowKey' => $windowKey,
                'stopKey' => $stopKey,
            );
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['stops'][$stopKey] = $stop;
            
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['stops'][$stopKey]['deliveries'] = array();
            foreach ($stop['deliveries'] as $delivery) {
                $deliveryKey = 'delivery-' . $delivery['orderID'];
                $delivery['node_path'] = "$node_path/deliveries/$deliveryKey";
                $delivery['-last_sync__gmt_time'] = $date_gmt;
                $delivery['-last_sync__local_time'] = $date_business;
                $delivery['-sync_for_day'] = $day;
                $delivery['-is_partial_sync'] = $isPartial;
                $delivery['stopSortOrder'] = $stop['sortOrder'];
                $delivery['-node_path_comps'] = array(
                    'businessKey' => $businessKey,
                    'routeKey' => $routeKey,
                    'windowKey' => $windowKey,
                    'stopKey' => $stopKey,
                    'deliveryKey' => $deliveryKey,
                );
                $delivery['accessCode'] = substr(trim($delivery['phone']), -4);
                $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['stops'][$stopKey]['deliveries'][$deliveryKey] = $delivery;
            }
            
            $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['stops'][$stopKey]['pickups'] = array();
            foreach ($stop['pickups'] as $pickup) {
                $pickupKey = 'pickup-' . $pickup['claimID'];
                $pickup['node_path'] = "$node_path/pickups/$pickupKey";
                $pickup['-last_sync__gmt_time'] = $date_gmt;
                $pickup['-last_sync__local_time'] = $date_business;
                $pickup['-sync_for_day'] = $day;
                $pickup['-is_partial_sync'] = $isPartial;
                $pickup['-node_path_comps'] = array(
                    'businessKey' => $businessKey,
                    'routeKey' => $routeKey,
                    'windowKey' => $windowKey,
                    'stopKey' => $stopKey,
                    'pickupKey' => $pickupKey,
                );
                $pickup['accessCode'] = substr(trim($pickup['phone']), -4);
                $firebaseNodes[$businessKey]['routes'][$routeKey]['windows'][$windowKey]['stops'][$stopKey]['pickups'][$pickupKey] = $pickup;
            }
        }
            
        return array('business' => $firebaseNodes);
    }
    
    //=================================================================================
    
    /**
     * Firebase request queue ==> Driver Api
     */
    public function requestQueueToApi()
    {
        $requestCount = 0;
        $time = time();
        
        try {
            if (!ob_get_level()) { 
                ob_start();
            }

            $queue = $this->getRequestQueue();
            $this->out("Found " . count($queue) . " request lists to process");

            if ($queue) {
                foreach ($queue as $employee => $list) {
                    $this->out("Processing " . count($list) . " request for $employee");
                    foreach ($list as $signature => $command) {
                        $sendResult = $this->sendToDriverApi($command);
                        if ($sendResult) {
                            $this->out($sendResult['message']);
                        }

                        $removeResult = $this->removeFromQueue($employee, $signature);
                        $requestCount++;
                    }
                }
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();
            return array(
                'success' => FALSE,
                'message' => "Error while executing processRequestQueue. Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        
        $time = $this->toHours(time() - $time);
        $memory_profile = getMemoryInfo();
        $log = $this->getOutputLog();
        
        return array(
            'time' => $time,
            'memory' => $memory_profile['summary'],
            'count' => $requestCount,
            'success' => TRUE,
            'message' => "Request queue processed",
            'data' => array (
                'log' => $log,
                "memory_profile" => $memory_profile,
            ),
        );
    }
    
    protected function sendToDriverApi($command)
    {
        try {
            $url = $command['api_url'] . $command['urn'] . "?" . http_build_query($command['params']);
            $this->out("Executing $url");
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->apiTimeout);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->apiTimeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            
            $return = curl_exec($ch);
            
            if( !$return) 
            { 
                $this->out("The following error ocurred while trying to curl: " . curl_error($ch));
            } else {
                $return = json_decode($return, true);
            }
        } catch (Exception $e) {
            $this->out("The following exception ocurred while trying to curl: " . $e->getMessage());
            $this->out('Error Trace: ' . $e->getTraceAsString());

            $return = null;
        }
        
        curl_close($ch);

        return $return;
    }

    /**
     * Retrieves the list of driver app request to be sent to driver api 
     */
    protected function getRequestQueue()
    {
        return $this->sendToFirebaseWithRetry($this->commandQueueNode, array(), 'get');
    }
    
    protected function removeFromQueue($employee, $signature)
    {
        return $this->removeNode("{$this->commandQueueNode}/$employee/$signature");
    }
    
    //=================================================================================
    
    public function driverJournalToFirebase($business_id = null, $day = null, $callback = null, $callbackParams = array())
    {
        $time = time();
        if ($business_id) {
            return $this->businessDriverJournalToFirebase($business_id, $day);
        }
        
        $this->load->model('business_model');
        $businessList = $this->business_model->get_all_active_ids_for_driver_app();
        $businessCount = count($businessList);
        
        $results = array(
            'time' => '',
            'memory' => '',
            'success' => true,
            'items' => array(),
        );
        
        foreach ($businessList as $item_id) {
            $res = $this->businessDriverJournalToFirebase($item_id, $day, $callback, $callbackParams);
            
            $results['success'] = $results['success'] && $res['success'];
            $results['data'][] = $res;
        }
        
        $time = $this->toHours(time() - $time);
        $memory_profile = getMemoryInfo();
        
        $results["time"] = $time;
        $results["memory"] = $memory_profile['summary'];
        
        $results['message'] = $results['success'] ? "$businessCount businesses processed correctly." : "Error while processing data for $businessCount businesses.";
        $results["memory_profile"] = $memory_profile;
        
        return $results;
    }
    
    /* 
     * driver_app_journal table ==> Firebase
     * Update data related to a specific entity (order or claim)
     * It will update routes for all entities that have changed since previous sync
     */
    public function businessDriverJournalToFirebase($business_id, $day = null, $callback = null, $callbackParams = array())
    {
        $time = time();
        
        $success = TRUE;
        $errorTrace = "";
        
        $res = array();
        $lastSeen = '';
        $lastChangeId = '';
        
        try {
            if (!ob_get_level()) { 
                ob_start();
            }
            
            if($this->getBusiness($business_id)) {
                if (!strlen(trim($day))) {
                    $day = $this->getTodayDayNumber($business_id);
                }

                $lastSeen = get_business_meta($business_id, $this->last_sync_meta, 0);
                $this->out("Last change seen by business $business_id : $lastSeen");

                $this->load->model('driver_app_journal_model');

                $lastChangeId = $this->driver_app_journal_model->getLastRecordId();
                $this->out("Last change registered on the system : $lastChangeId");

                $entries = $this->driver_app_journal_model->getPendingChanges($business_id, $lastSeen);
                $entries_count = count($entries);
                
                $this->out("Detected $entries_count pending changes for business $business_id");
                
                if ($entries_count > $this->journal_sync_max_entries) {
                    update_business_meta($business_id, $this->next_sync_meta, 0);
                    $this->out("Too many entries (more than {$this->journal_sync_max_entries}). Scheduled complete business $business_id refresh for next round");
                } else {
                    $res = $this->syncEntites($day, $entries);

                    if ($res) {
                        $this->out("Next time business $business_id will process changes newer than: $lastChangeId");
                        update_business_meta($business_id, $this->last_sync_meta, $lastChangeId);
                    } else {
                        $this->out("ERROR: Something went wrong. Next time business $business_id will try to process changes newer than: $lastSeen");
                    }
                }
            } else {
                $message = "No business found with id $business_id. Skipping...";
                $this->out($message);
            }
            
        } catch (Exception $e) {
            $success = FALSE;
            $message = "Error while updating routes for $business_id on day $day . Error: {$e->getMessage()}";
            $errorTrace = $e->getTrace();
        }
        
        $log = $this->getOutputLog();
        $time = $this->toHours(time() - $time);
        $memory_profile = getMemoryInfo();
        
        $message = isset($message) ? $message : "Finished update for business $business_id on day $day. Time: $time. Memory: {$memory_profile['summary']}";

        $responseData = array(
            'business_id' => $business_id, 
            'time' => $time,
            'memory' => $memory_profile['summary'],
            'day' => $day, 
            'entityCount' => isset($res['entityCount']) ? $res['entityCount'] : $entries_count,
            'entityUpdated' => isset($res['entityUpdated']) ? $res['entityUpdated'] : 0,
            'success' => $success,
            'message' => $message,
            'processedFrom' => $lastSeen, 
            'processedTo' => $lastChangeId,
            'log' => $log, 
            'detail' => $res,
            'errorTrace' => $errorTrace,
            "memory_profile" => $memory_profile,
        );
        
        $summmary = array(
            'success' => $success,
            'message' => $message,
        );
        
        if (is_callable($callback))  {
            $callbackParams[] = $responseData;
            call_user_func_array ($callback, $callbackParams);
        } else {
            $summmary['data'] = $responseData;
        }
        
        return $summmary;
    }

    protected function syncEntites($day, $entries)
    {
        $results = array(
            'success' => true,
            'entityCount' => 0,
            'entityUpdated' => 0,
            'entities' => array(),
        );

        foreach ($entries as $change) {
            if ($change['tableName'] == 'location' && in_array('update_route', $change['event'])) {
                $res = $this->syncLocationRoute($day, $change['tableName'], $change['id'], $change['business_id'], $change);
            } else if ($change['tableName'] == 'driverNotes') {
                $res = $this->syncDriverNote($day, $change['tableName'], $change['id'], $change['business_id']);
            } else {
                $res = $this->syncEntity($day, $change['tableName'], $change['id']);
            }
            $results['success'] = $results['success'] && $res['success'];
            
            unset($res['node']);
            $results['entities'][] = $res;
            $results['entityCount']++;
            
            if ($res['nodeUpdated']) {
                $results['entityUpdated']++;
            }
        }

        return $results;
    }

    protected function syncRoutesForEntites($day, $entries)
    {
        $sucess = true;
        
        $outdated = array();
        
        foreach ($entries as $change) {
            $data = $this->getDataFromOrder($day, $change['tableName'], $change['id']);
            
            if (!$data) {
                $this->out("No routes found for day $day for {$change['tableName']} {$change['id']}. Skipping...");
            } else {
                $outdated[$data['business_id']][$data['routes']] = $data['routes'];
            }
        }
        
        $this->out("Total of businesses that needs to be updated for day $day: " . count($outdated));
        
        foreach ($outdated as $business_id => $routes) {
            foreach ($routes as $route) {
                $this->out("Updating route $route for business $business_id for day $day");
                
                $path = array($business_id, $route);
                $syncResult = $this->sync($day, $business_id, $route, $path);
                $sucess = $sucess && $syncResult['success'];
            }
        }
        
        return $sucess;
    }
    
    //=================================================================================
    
    public function getTodayDayNumber($business_id)
    {
        $this->setBusinessTime($business_id);
        return date("w");
    }
    
    //=================================================================================
    
    public function droidToDroplocker($business_id = null, $employee_id = null) {
        $time = time();
        
        $pickups = $this->processPickups($business_id, $employee_id);
        $orders = $this->processOrders($business_id, $employee_id);
        
        $time = $this->toHours(time() - $time);
        $memory_profile = getMemoryInfo();
        
        return array(
            'time' => $time,
            'memory' => $memory_profile['summary'],
            'pickup_count' => $pickups['count'],
            'orders_count' => $orders['count'],
            'pickups' => $pickups,
            'orders' => $orders,
            'success' => $pickups['success'] && $orders['success'],
            "memory_profile" => $memory_profile,
        );
    }
    
    /**
     * Adapted from processOrders on controllers\admin\mobile_2_6
     */
    protected function processOrders($business_id = null, $employee_id = null)
    {
        $completed = 0;
        $skipped = 0;
        $failed = 0;
            
        try {
            if (!ob_get_level()) { 
                ob_start();
            }

           $this->out("Processing pending orders on Droid tables");

           $filter = array(
                'status'=>'pending', 
                'process_status' => 'not_processed', 
                'version' => $this->version,
            );

            if (!$business_id) {
                $this->out("Processing all businesses");
            } else {
                $this->out("Processing business $business_id");
                $filter['business_id'] = $business_id;
            }

            if ($employee_id) {
                $this->out("Processing only for employee $employee_id");
                $filter['employee_id'] = $employee_id;
            } else {
                $this->out("Processing all employees");
            }

            $this->load->model('droidpostdeliveries_model');
            $deliveries = $this->droidpostdeliveries_model->get($filter);

            if (!$deliveries) {
                $this->out('No orders found');
            } else {
                $this->out("Found " . count($deliveries). " orders to process");
            }

            foreach ($deliveries as $delivery) {
                $droid_postDelivery = new Droid_PostDeliveries($delivery->ID);
                if ($droid_postDelivery->process_status == "not_processed") { //Note, we verify that the devliery has still not been processed after retrieving the initial result set.
                    $droid_postDelivery->process_status = "processing";
                    $droid_postDelivery->save();

                    $res = $this->droidpostdeliveries_model->processDelivery($delivery);
                    
                    $droid_postDelivery->process_status = "processed";
                    $droid_postDelivery->save();

                    $this->out("Processed delivery for order " . $delivery->order_id);
                    if ($res === true) {
                        $this->out("Something went wrong, check droid table for additional detail");
                        $failed++;
                    } else {
                        $completed++;
                    }
                } else {
                    $this->out("WARNING: Pending droid_delivery {$delivery->ID} on status '{$droid_postDelivery->process_status}'. Skipping...");
                    $skipped++;
                    //send_admin_notification("Duplicate process delivery call from phone ({$droid_postDelivery->{Droid_PostDeliveries::$primary_key}})", print_r($droid_postDelivery,1));
                }
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();
            return array(
                'success' => FALSE,
                'message' => "Error while executing " . __METHOD__ . ". Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        
        $log = $this->getOutputLog();
        
        return array(
            'success' => TRUE,
            'message' => "Pending orders on Droid tables: $completed completed, $failed failed, $skipped skipped",
            'count' => $completed + $failed + $skipped,
            'data' => array (
                'log' => $log,
            ),
        );
    }

    /**
     * Adapted from processPickups on controllers\admin\mobile_2_6
     */
    protected function processPickups($business_id = null, $employee_id = null)
    {
        $completed = 0;
        $skipped = 0;
        $failed = 0;
            
        try {
            if (!ob_get_level()) { 
                ob_start();
            }

           $this->out("Processing pending pickups on Droid tables");

           $filter = array(
                'status'=>'pending', 
                'version' => $this->version,
            );

            if (!$business_id) {
                $this->out("Processing all businesses");
            } else {
                $this->out("Processing business $business_id");
                $filter['business_id'] = $business_id;
            }

            if ($employee_id) {
                $this->out("Processing only for employee $employee_id");
                $filter['employee_id'] = $employee_id;
            } else {
                $this->out("Processing all employees");
            }

            $this->load->model('droidpostpickups_model');

            $key = "pickups_{$business_id}_{$employee_id}_" . date('YmdHi');
            $this->out("Acquired lock for $key");

            $pickups = $this->droidpostpickups_model->get($filter);

            if (!$pickups) {
                $this->out('No pickups found');
            } else {
                $this->out("Found " . count($pickups). " pickups to process");
            }
            
            foreach ($pickups as $pickup) {
                $droid_postPickup = new Droid_PostPickup($pickup->ID);
                //The following conditional verifies that another process from the faulty Android App is not already processing or has already processed this pickup.
                if ($droid_postPickup->process_status == "not_processed") {
                    $droid_postPickup->process_status = "processing";
                    $droid_postPickup->save();

                    $res = $this->droidpostpickups_model->processPickup($pickup);

                    $droid_postPickup->process_status = "processed";
                    $droid_postPickup->save();

                    $this->out("Processed pickup for bag " . $pickup->bagNumber);
                    if ($res === true) {
                        $this->out("Something went wrong, check droid table for additional detail");
                        $failed++;
                    } else {
                        $this->out("New order id: $res");
                        $completed++;
                    }
                } else {
                    $this->out("WARNING: Pending droid_pickup {$pickup->ID} on status '{$droid_postPickup->process_status}'. Skipping...");
                    $skipped++;
                }
            }
        } catch (Exception $e) {
            $log = $this->getOutputLog();
            return array(
                'success' => FALSE,
                'message' => "Error while executing " . __METHOD__ . ". Error: {$e->getMessage()}",
                'log' => $log,
                'errorTrace' => $e->getTrace(),
            );
        }
        
        $log = $this->getOutputLog();
        
        return array(
            'success' => TRUE,
            'message' => "Pending pickups on Droid tables: $completed completed, $failed failed, $skipped skipped",
            'count' => $completed + $failed + $skipped,
            'data' => array (
                'log' => $log,
            ),
        );
}

    //=================================================================================
    
    protected function getNode($path)
    {
        return $this->sendToFirebaseWithRetry($path, null, 'get');
    }

    protected function getNodeShallow($path)
    {
        return $this->sendToFirebaseWithRetry($path, null, 'get', array('shallow' => 'true'));
    }

    protected function removeNode($paths)
    {
        $res = array();
        $paths = (array)$paths;
        
        foreach ($paths as $path) {
            $res[] = $this->sendToFirebaseWithRetry($path, array(), 'delete');
        }
        
        return $res;
    }

    protected function updateNode($path, $info) {
        return $this->sendToFirebaseWithRetry($path, $info, 'update');
    }
    
    protected $firebaseError = false;

    protected function sendToFirebaseWithRetry($path, $value, $action = 'set', $options = array())
    {
        $this->firebaseError = false;
        
        if ($action === 'set' && !empty($value)) {
            $value = $this->sanitizeForFirebase($value);
        }
        
        $timeout = $this->firebase_timeout;
        $retries = $this->firebase_retries;
        
        $this->out("Using firebase instance: " . $this->config->item('firebaseUrl'));
        $this->out("Sending '$action' to $path ( " . $this->humanFriendlySize(strlen(json_encode($value))) . " )");
        
        $this->Firebase = new \Firebase\FirebaseLib($this->config->item('firebaseUrl'), $this->config->item('firebaseDBSecret'));
        $this->Firebase->setTimeout($timeout);
        
        $try = 0;
        for ($i = 0; $i < $retries; $i++) {

            if ($i) {
                $this->out("Retry $i of $retries (waiting $timeout seconds) ...", true);
            }

            $res = $this->Firebase->$action($path, $value, $options);
            if (json_last_error() !== JSON_ERROR_NONE) {
                $this->out("Error during data encoding for firebase: " . json_last_error_msg(), true);
            }
            
            if ($res) {
                $res = json_decode($res, true);
                
                if(isset($res['error'])) {
                    $this->out("Error: {$res['error']}", true);
                } else {
                    $this->out("Success.");
                    return $res;
                }
            } else {
                $this->out("No response from firebase", true);
            }
        }

        if (empty($res) || isset($res['error'])) {
            $this->out('ERROR: too many failed requests', true);
            $this->firebaseError = true;
            return FALSE;
        }
    }
    
    protected function sanitizeForFirebase($value)
    {
        json_encode($value);
        
        if (json_last_error() === JSON_ERROR_UTF8) {
            $this->out(__FUNCTION__ . ": executing", true);
            
            $utfRes = utf8ize($value);
            $value = $utfRes['val'];

            foreach ($utfRes['log'] as $log) {
                $this->out($log, true);
            }
        }

        return $value;
    }
    
    protected function arrayToString($arr) {
        $comps = array();
        foreach ($arr as $key => $value) {
            $comps[] = "$key:$value";
        }
        
        return implode(", ", $comps);
    }
    
    protected $outDeep = 0;
    
    protected function outDeeper(){
        $this->outDeep++;
    }
    
    protected function outShallower(){
        $this->outDeep--;
    }
    
    protected $outDisabled = false;
    
    protected function outEnable(){
        $this->outDisabled = false;
    }
    
    protected function outDisable(){
        $this->outDisabled = true;
    }
    
    protected function out($msg, $force = false, $deep = 0)
    {
        if ($this->outDisabled && !$force) {
            return;
        }
        
        $sep = "--";
        $tab = "";
        for($i=0;$i<$this->outDeep + $deep;$i++){$tab .= $sep;};
        
        echo gmdate('Y-m-d H:i:s') . " $sep$tab $msg<br />\n";
    }
    
    protected function getOutputLog()
    {
        $log = ob_get_contents();
        ob_end_clean();

        $log = explode("\n", str_replace('<br />', '', $log));
        
        return $log;
    }
    
    protected function getBusiness($business_id) {
        $this->load->model('business_model');
        return $this->business_model->get_by_primary_key($business_id);
    }


    protected function setBusinessTime($business_id) 
    {
        $this->load->model('business_model');
        $business = $this->business_model->get_by_primary_key($business_id);
        
        setlocale(LC_ALL, $business->locale);
        date_default_timezone_set($business->timezone);
    }
    
    protected function clean($str) {
        return preg_replace('/[^a-z\d]/i', '', $str);
    }
    
    protected function humanFriendlySize($bytes, $decimals = 2) {
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor < 1) {
            $decimals = 0;
        }
        
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    protected function toHours($seconds)
    {
        $h = floor($seconds/3600);
        $seconds %= 3600;
        
        $m = floor($seconds/60);
        $seconds %= 60;
        
        return sprintf("%02d:%02d:%02d", $h, $m, $seconds);
    }
}
