<?php
namespace App\Libraries\DroplockerObjects;

class TimeCard extends DroplockerObject
{
    public static $table_name = "timeCard";
    public static $primary_key = "timeCardID";


    public function validate()
    {
    }

    public function processEntry($timeCardEntry)
    {
        $CI = get_instance();

        if (empty($timeCardEntry->inout)) {
            throw new Exception("Missing inout variable. Should be either in or out");
        }

        if (empty($timeCardEntry->clockTime)) {
            throw new Exception("missing time for clock in/out");
        }

        if (!$timeCardEntry->employee_id) {
            throw new Exception("Missing employee_id");
        }

        $defaultWeekStart = get_business_meta($this->business_id, 'timecards_start_of_week', 'Mon');
        
        //If today is defaultWeekStart, this is the weekOf
        $date = convert_from_gmt_aprax($timeCardEntry->clockTime, "Y-m-d H:i:s", $timeCardEntry->business_id);
        if (date("D", strtotime($date)) == $defaultWeekStart) {
            $weekOf = date("Y-m-d", strtotime($date));
        } else {
            for ($i = 1; $i <= 7; $i++) {
                if (date("D", strtotime("$date -$i day")) == $defaultWeekStart) {
                    $weekOf = date("Y-m-d", strtotime("$date -$i day"));
                }
            }
        }

        //find out if this customer has a timecard for this week yet
        $sql = "SELECT timeCard.* FROM timeCard
        where employee_id = ?
        and weekOf = ?;";

        //if there is not already a timecard
        $query = $CI->db->query(
            $sql,
            array(
                $timeCardEntry->employee_id,
                $weekOf
            )
        );

        $numRows = $query->num_rows();

        if ($numRows == 0) {
            //create one
            $insertCard1 = "INSERT INTO timeCard (employee_id, weekOf, status) VALUES (?, ?, 'Preliminary');";
            
            $CI->db->query(
                $insertCard1,
                array(
                    $timeCardEntry->employee_id,
                    $weekOf
                    )
                );

            $timeCard_id = $CI->db->insert_id();
        } else {
            $timecard = $query->row();
            $timeCard_id = $timecard->timeCardID;
        }


        $timeInOut = date("H:i:s", strtotime($date));
        $day = date("Y-m-d", strtotime($date));

        //mysql 5+ now reserves inOUt so I changed inOut to in_out
        $insert1 = "INSERT INTO timeCardDetail (timeCard_id, type, timeInOut, in_out, day, timestamp, ip)
        VALUES (?, 'Regular', ?, ?, ?, ?, 'phone');";
        $CI->db->query(
            $insert1,
            array(
                $timeCard_id,
                $timeInOut,
                ucfirst($timeCardEntry->inout),
                $day,
                $timeCardEntry->clockTime
                )
            );

        if ($insert_id = $CI->db->insert_id()) {
            $message = "Posting to timecard [{$insert_id}] (".$timeCardEntry->inout.") was a success";
            $status = 'success';
        } else {
            $message = "Fail Posting to timecard (".$timeCardEntry->inout.")";
            $status = 'fail';
        }

        $droidTimeCard = new \App\Libraries\DroplockerObjects\DroidPostTimeCard($timeCardEntry->droid_postTimeCardsID);
        $droidTimeCard->status = $status;
        $droidTimeCard->statusNotes = $message;
        $droidTimeCard->save();
    }
}
