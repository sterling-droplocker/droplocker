<?php

namespace App\Libraries\DroplockerObjects;
class Location_ServiceDay extends DroplockerObject
{
    public static $table_name = "location_serviceDay";
    public static $primary_key = "location_serviceDayID";
    public static $has_one = array("Location_ServiceType", "Location");
    protected $rules = array(
        "day" => array("numeric", "required"),
        "location_serviceType_id" => array("numeric", "required", "entity_must_exist"),
        "location_id" => array("numeric", "required", "entity_must_exist")
    );
    public static function create($day, $location_id, $location_serviceType_id)
    {
        $location_serviceDay = new Location_ServiceDay();
        $location_serviceDay->day = $day;
        $location_serviceDay->location_id = $location_id;
        $location_serviceDay->location_serviceType_id = $location_serviceType_id;
        $location_serviceDay->save();

        return $location_serviceDay;
    }
}
