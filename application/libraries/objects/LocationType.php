<?php
namespace App\Libraries\DroplockerObjects;
class LocationType extends DroplockerObject
{
    public static $table_name = "locationType";
    public static $primary_key = "locationTypeID";

    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
    }

    /**
     * @deprecated This functionality should exist in the locationtype model instance, as the Droplocekr Objects should only be used for writing data and not just data retrieval.
     * Retrieves all the availble location types.
     * @return array A codeigniter-form menu compatiable array of the location.
     */

    public static function get_all()
    {
        $locationTypes = self::search_aprax(array(), false);
        $result = array();
        foreach ($locationTypes as $locationType) {
            $result[$locationType->{self::$primary_key}] = $locationType->name;
        }

        return $result;
    }
}
