<?php
namespace App\Libraries\DroplockerObjects;

class Customer extends DroplockerObject
{
    public static $table_name = "customer";
    public static $primary_key = "customerID";
    public static $belongsTo = array("Customer_Location");
    public static $has_many = array("Bag", "Order");

    public $rules = array(
        "email" => array("required"=>  array("userMessage" => "Please enter your email address"))
    );
    /**
     * The following function is *, do not use.
     * @deprecated Use DroplockerObjects relationship functionality instead.
    * returns the customers bags
    */
    public function getBags()
    {
        $CI = get_instance();
        $query = $CI->db->get_where('bag', array('customer_id'=> $this->{self::$primary_key}));

        return $query->result();
    }


    /**
     * The following function is *, do not use.
     * gets the credit card for the customer
     * @deprecated Use the get_customer_card in the Credit Card model instead.
     * @return array of Objects
     */
    public function getCreditCard()
    {
        $CI = get_instance();

        return $CI->db->get_where('creditCard', array('customer_id'=> $this->{self::$primary_key}))->row();
    }

    /**
     * unsubscribes a user from the emails
     * @return int
     */
    public function unsubscribe()
    {
        $CI = get_instance();
        $sql = "update customer set noEmail = 1 where customerID = ".$this->{self::$primary_key};
        $result = $CI->db->query($sql);

        return true;
    }


    /**
     * The following fucntion is *, do not use.
     * gets the customers active discounts
     * @deprecated This funcitonality should be in the Customer model, not the Dropocker Objects
     * @param int customer_id
     * @return array of objects
     */
    public static function getActiveDiscounts($customer_id, $business_id)
    {
        $CI = get_instance();
        $sql = "SELECT * FROM customerDiscount
                WHERE customer_id = $customer_id
                AND business_id = $business_id
                AND active = 1
                AND description != 'Laundry Plan'";

        $discounts = $CI->db->query($sql)->result();

        return $discounts;
    }

    /**
     * The following function is *
     * Gets the items from a closet
     * @deprecated Functions that only return data without the intent of modifying it should be delegated to the model class
     * @return array of objects
     */
    public function closet()
    {
        $CI = get_instance();
        $CI->load->model('item_model');
        $items = $CI->item_model->get_closet($this->{self::$primary_key});

        return $items;
    }


    public function lockers()
    {
        $CI = get_instance();
        //get the customer locations
        $CI->load->model('location_model');
        $options['customer_id'] = $this->{self::$primary_key};
        $options['business_id'] = $this->business_id;
        $options['select'] = "locationID,(SELECT name from locationType where locationTypeID = locationType_id) as locationType, (SELECT count(lockerID) from locker where location_id = locationID) as total,lockerType,description,locationType_id,serviceType, address,address2,city,state,zipcode, lat, lon as lng";
        $options['sort'] = array('dateCreated', 'desc');
        $options['with_location_type'] = true;
        $options['exclude_serviceTypes'] = Location::getHomeDeliveryServiceTypes();

        $locations = $CI->location_model->get($options);

        $response = new \stdClass();
        foreach ($locations as $location) {

            $lockers = Locker::search_aprax(array('location_id'=>$location->locationID), FALSE);
            $count = 0;
            foreach ($lockers as $locker) {
                $lockType = new LockerLockType($locker->lockerLockType_id);
                $lockerStyle = new LockerStyle($locker->lockerStyle_id);
                $lockerStatus = new LockerStatus($locker->lockerStatus_id);
                $properties['lockerID'] = $locker->lockerID;
                $properties['lockerName'] = $locker->lockerName;
                $properties['lockerLockType'] = $lockType->name;
                $properties['lockerStyle'] = $lockerStyle->name;
                $properties['lockerStatus'] = $lockerStatus->name;
                $location->lockers[] = $properties;
                $count++;
                if ($count == 5) {
                    break;
                }
            }
            $response->Location[] = $location;
        }

        return $response;
    }

    /**
     * Add a location to a customer account
     *
     * @param int $location_id
     * @return boolean
     */
    public function addLocation($location_id)
    {
        if (empty($location_id)) {
            throw new \Exception("Missing location_id");
        }

        $CI = get_instance();
        $CI->load->model('customer_location_model');
        $CI->customer_location_model->customer_id = $this->{self::$primary_key};
        $CI->customer_location_model->location_id = $location_id;
        if (!$CI->customer_location_model->get()) {
            $CI->customer_location_model->insert();

            return TRUE;
        }

        return FALSE;
    }

    /**
    * Removes a location from a customer account
    *
    * @param int $location_id
    * @return boolean
    */
    public function removeLocation($location_id)
    {
        if (empty($location_id)) {
            throw new \Exception("Missing location_id");
        }

        $CI = get_instance();
        $CI->load->model('customer_location_model');
        $CI->tomer_location_model->delete(array('customer_id'=>$this->{self::$primary_key}, 'location_id'=>$location_id));
        if($CI->db->affected_rows())

            return TRUE;
        else
            return FALSE;

    }

    /**
    * The following function is *, do not use.
    * return an array of customer preferences. If the customer does not have any preferences set, return an empty array
    * @deprecated THis functionality should use the Customer model, not the Droplocker Objects
    * @return array $customerPreferences
    */
    public function getPreferences()
    {
        $customerPreferencesString = $this->preferences;
        $customerNotes = $this->customerNotes;
        $starchOnShirts_id = $this->starchOnShirts_id;

        $customerPreferences = array();

        //if the customer has preferences set, show them. If not, show the normal business preferences
        if ($customerPreferencesString != '') {
            $customerPreferences = unserialize($customerPreferencesString);

            foreach ($customerPreferences as $key => $customerPref) {
                unset( $customerPreferences[ $key ] );
                $new_key = strtolower($key);
                $new_key = str_replace('-', '', $new_key);
                $new_key = str_replace(' ', '', $new_key);
                $customerPreferences[ $new_key ] = $customerPref;
            }
        }

        $preferences_and_settings = array('preferences'=>$customerPreferences, 'customerNotes'=>$customerNotes, 'starchOnShirts_id'=>$starchOnShirts_id);

        return $preferences_and_settings;

    }

    /**
     * The following implementation of the save function provides special handlign for the default wash and fold and the password properties of a customer.
     * Teh password is hashed using the * design md5 algoroithm because the password value is saved into the database.
     */
    public function save()
    {
        $CI = get_instance();
        $this->validate();

        //Note, every customer should have a default wash and fold, if none is specified, then we pull the default product process for the b usiness.
        if (empty($this->defaultWF)) {
            $CI->load->model("product_model");
            $this->defaultWF = $CI->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);
        }

        $current_customer = new Customer($this->{self::$primary_key});
        //The following conditional checks if we need to update the customer password. If so, then we make sure to apply the md5 encryption algorithm before saving the password into the database
        if (!empty($this->password) && $this->password != $current_customer->password) {
            $this->password = md5($this->password);
        }
        
        $this->lastUpdated = convert_to_gmt_aprax();

        $id = parent::save();

        return $id;
    }
    /**
     * A customer can not be deleted if he or she has associated orders and/or associated active claims.
     * @throws Validation_Exception
     */
    public function validate_delete()
    {
        parent::validate_delete();
        $CI = get_instance();
        $CI->load->model("order_model");
        $orders_count = $CI->order_model->count(array("customer_id" => $this->{self::$primary_key}));
        if ($orders_count > 0) {
            throw new Validation_Exception("Can not delete customer because they have $orders_count orders");
        }
        $CI->load->model("claim_model");
        $active_claims_count = $CI->claim_model->count(array("customer_id" => $this->{self::$primary_key}, "active" => 1));
        if ($active_claims_count > 0) {
            throw new Validation_Exception("Can not delete customer because they have $active_claims_count active claims");
        }
    }
    /**
     * Validates the propertes of the customer object.
     * @throws Validation_Exception
     */
    public function validate()
    {
        try {
            $business = new Business($this->business_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found");
        }
        if (isset($this->email)) {
            // The following conditional checks to see that there does not already exist an customer with the same email associated with the business if we are creating anew customer entity.
            if (isset($this->{Customer::$primary_key})) {
                $customers = Customer::search_aprax(array("email" => $this->email, "business_id" => $this->business_id, "customerID !=" => "{$this->{Customer::$primary_key}}"));
            }
            if (!empty($customers)) {
                $existing_customer = $customers[0];
                throw new Validation_Exception("Customer '{$existing_customer->firstName} {$existing_customer->lastName} ({$existing_customer->{Customer::$primary_key}}) already has the email '{$this->email}' in business '{$business->companyName}'");
            }
        }
        if (empty($this->facebook_id)) {
            if (empty($this->password)) {
                throw new Validation_Exception("'password' can not be empty.");
            }
        } else {
            if (!is_numeric($this->facebook_id)) {
                throw new Validation_Exception("facebook_id must be numeric");
            }
        }
        if (!empty($this->default_business_language_id)) {
            if (is_numeric($this->default_business_language_id)) {
                Validation_Exception::verify_entity_exists($this->default_business_language_id, "Business_Language");
            } else {
                throw new Validation_Exception("'customer_id' must be numeric");
            }
        }
        parent::validate();
    }

    /**
     * @deprecated this functionality belongs in the model, not the Droplocker Objects
     * @return \App\Libraries\DroplockerObjects\Order
     * @throws \BadMethodCallException
     */
    public function get_last_order()
    {
        if (!is_numeric($this->customerID)) {
            throw new \BadMethodCallException("'customerID' must be numeric.");
        }
        $CI = get_instance();
        $CI->db->select_max("orderID", "orderID");
        $CI->db->where("customer_id", $this->customerID);
        $CI->db->where("bag_id !=", 0);
        $get_max_order_query = $CI->db->get("orders");
        $get_max_order_query_result = $get_max_order_query->row();
        $order = new Order($get_max_order_query_result->orderID, false);

        return $order;
    }
}
