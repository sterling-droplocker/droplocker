<?php
namespace App\Libraries\DroplockerObjects;

class Product_Process extends DroplockerObject
{
    public static $table_name = "product_process";
    public static $primary_key = "product_processID";

    public static $has_one = array("Product", "Process");

    /**
     * Saves the object into the database.
     * Note, 'dateCreated' is a ready only field that can not be changed.
     */
    public function save()
    {
        unset($this->dateCreated);
        $id = parent::save();
        $saved_product = new Product_Process($this->{self::$primary_key});
        $this->dateCreated = $saved_product->dateCreated;

        return $id;
    }
    /**
     * Returns the default supplier for this product process.
     * @return \App\Libraries\DroplockerObjects\Supplier
     */
    public function get_default_supplier()
    {
        $CI = get_instance();
        $CI->load->model("product_supplier_model");
        $result = $CI->product_supplier_model->getDefault($this->product_processID);
        $default_supplier = new Supplier($result->supplier_id);

        return $default_supplier;
    }

    public function validate()
    {
        if (!is_numeric($this->product_id)) {
            throw new Validation_Exception("'product_id' must be numeric.");
        }
        try {
            new Product($this->product_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Product ID '{$this->product_id}' not found.");
        }

        /*
         * We have many products that have process_id of 0.
         * I refactored this validation to allow for updaating the prices for products that have process = 0
         */
        if (is_numeric($this->process_id)) {
            //throw new Validation_Exception("'process_id' must be numeric.");
            if ($this->process_id != 0) {
                try {
                    new Process($this->process_id);
                } catch (NotFound_Exception $e) {
                    throw new Validation_Exception("Process ID '{$this->process_id}' not found.");
                }
            }
        }

        $CI = get_instance();
        $CI->load->model("product_process_model");
        if (!isset($this->{self::$primary_key})) {
            $existing_product_processes = $CI->product_process_model->get_aprax(array("product_id" => $this->product_id, "process_id" => $this->process_id));
            if (!empty($existing_product_processes)) {
                throw new Validation_Exception("Process '{$this->process_id}' already exists for '{$this->product_id}");
            }
        }


    }

}
