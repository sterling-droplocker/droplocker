<?php
namespace App\Libraries\DroplockerObjects;
class Supplier extends DroplockerObject
{
    public static $table_name="supplier";
    public static $primary_key="supplierID";

    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            new Business($this->business_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found in database.");
        }
        if (!is_numeric($this->supplierBusiness_id)) {
            throw new Validation_Exception("'supplierBusiness_id' must be numeric.");
        }

        try {
            new Business($this->supplierBusiness_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->supplierBusiness_id}' not found in database");
        }
    }

    /**
     * gets the suppliers for a business
     *
     * @param int $business_id Optional - if not passed, will try to use the session business_id
     * @throws Exception business_id not found
     * @return array of stdClass objects
     */
    public static function getBusinessSuppliers($business_id = '')
    {
        $CI = get_instance();
        $business_id = (empty($business_id))?$CI->session->userdata('business_id'):$business_id;

        if (empty($business_id)) {
            throw new \Exception("Business_id was not passed and business_id in the session is not set");
        }

        $CI->load->model('supplier_model');
        $CI->supplier_model->business_id = $business_id;
        $CI->supplier_model->join("business ON business.businessID = supplier.supplierBusiness_id");

        return $CI->supplier_model->get();
    }
}
