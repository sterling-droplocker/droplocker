<?php
namespace App\Libraries\DroplockerObjects;
class CreditCard extends DroplockerObject
{
    public static $table_name = "creditCard";
    public static $primary_key = "creditCardID";

    protected $rules = array(
        "cardNumber" => array("required"),
        "customer_id" => array("numeric", "required", "entity_must_exist"),
        "expMo" => array("required"),
        "expYr" => array("required"),
        "processor" => array("required")
    );

    /**
     * Determines if the given month and year are expired, based on the current server time
     * @param int $expMo The two digit month
     * @param int $expYr The Two digit year
     * @return bool TRUE if expired, FALSE if not expired
     */
    public static function isExpired($expMo, $expYr)
    {
        if (!is_numeric($expMo)) {
            throw new Validation_Exception("'expMo' must be numeric");
        }
        if (!is_numeric($expYr)) {
            throw new Validation_Exception("'expYr' must be numeric");
        }
        // the card expires the first second of the next month printed on the card
        $expires = mktime(0, 0, 0, intval($expMo) + 1, 1, intval($expYr));

        return $expires <= time();
    }

    public static function clean($cc_no)
    {
        // Remove non-numeric characters from $cc_no
        return ereg_replace ('[^0-9]+', '', $cc_no);
    }

    public static function identify($cc_no)
    {
         //$cc_no = $this::clean($cc_no);

        // Get card type based on prefix and length of card number
        if (preg_match ('/^4(.{12}|.{15})$/', $cc_no)) {
            return 'Visa';
        }

        if (preg_match ('/^5[1-5].{14}$/', $cc_no)) {
            return 'Mastercard';
        }

        if (preg_match ('/^3[47].{13}$/', $cc_no)) {
            return 'Amex';
        }

        if (preg_match ('/^3(0[0-5].{11}|[68].{12})$/', $cc_no)) {
            return 'Diners Club/Carte Blanche';
        }

        if (preg_match ('/^6011.{12}$/', $cc_no)) {
            return 'Discover Card';
        }

        if (preg_match ('/^(3.{15}|(2131|1800).{11})$/', $cc_no)) {
            return 'JCB';
        }

        if (preg_match ('/^(2(014|149).{11})$/', $cc_no)) {
            return 'enRoute';
        }

        return 'unknown';
    }

    /**
     * The following function is undocumented *.
     * @deprecated This functinality does not belong in the Droplocker Objects
     *
     * #################################
     * BEGIN *
     * #################################
     * Authorizes and saves the card for the customer
     *
     * @param int $customer_id
     * @param array $creditCard
     * @throws \Exception
     * @return Ambigous <multitype:, multitype:string , multitype:string multitype:string  , unknown>
     *
     * #################################
     * END *
     * #################################
     */
    /** The following is Adam Prax's attempt at documenting this function
     *
     * @param int $customer_id
     * @param type $creditCard
     * @return array
     *  status: "success" or "fail"
     *  message:
     * @throws \Exception
     */
    public function authorize($customer_id, $creditCard = array(), $test = false)
    {
        $CI = get_instance();

        /* ************************* */
        /* Begin * */
        /* ************************* */
        $customer = new Customer($customer_id, false);
        $business = new Business($customer->business_id, false);
        $this->customer_id = $customer->customerID;
        $this->business_id = $business->businessID;

        if (empty($this->creditCard)) {
            $result = $this->setCard($creditCard);
            if ($result['status'] == 'fail') {
                throw new \Exception("Credit Card not set properly. Missing: <br>".print_r($result['message'], 1));
            }
        }

        //if the card is still missing... throw stuff
        if (empty($this->creditCard)) {
            throw new \Exception('missing the credit card values');
        }

        if (CreditCard::isExpired($creditCard['expMonth'], $creditCard['expYear'])) {
            $result = array();
            $result['status'] = 'error';
            $result['order_id'] = $order_id;
            $result['message'] = "Credit card is expired";

            return $result;
       }

        $CI->load->library('creditcardprocessor');
        try {
            $this->processor = get_business_meta($business->businessID, 'processor');
            $processor = $CI->creditcardprocessor->factory($this->processor, $business->businessID);
        } catch (Exception $e) {
            send_admin_notification("Credit card Processor not configured properly for ".$business->companyName, "Credit card Processor not configured properly for ".$business->companyName.". Customers for this business cannot authorize their cards");
        }

        $CI->load->model('order_model');
        $options['customer_id'] = $customer_id;

        // The following code is a hackish workaround for validating that the prepayment locker ID exists so I don't have to rewrite the entire * code.
        $CI->load->model("locker_model");
        /* ************************* */
        /* End * */
        /* ************************* */
        if (is_numeric($business->prepayment_locker_id) && $CI->locker_model->does_exist($business->prepayment_locker_id)) {

            $locker = new Locker($business->prepayment_locker_id);

            /* ************************* */
            /* Begin * */
            /* ************************* */
            $authorizationOrder = Order::create(array(
                "customer_id" => $customer_id,
                "locker_id" => 0, // set to $business->returnToOfficeLockerId in the order
                "orderPaymentStatusOption_id" => 1,
                "orderStatusOption_id" => 3,
                "notes" => "Authorizing Card",
                "business_id" => $business->businessID,
                "bag_id" => null,
            ));
            $order_id = $authorizationOrder->orderID;


            if (empty($order_id)) {
                throw new \Exception("Order was not created");
            }

            if ($test) {
                $processor->setTestMode();
            }
            $response = $processor->authorizeCard($this->creditCard, $order_id, $customer->customerID, $business->businessID);
            $this->refNumber = $response->get_refNumber();
            if ($response->get_status() == "SUCCESS") {
                $card = $this::search_aprax(array('customer_id' => $this->customer_id), FALSE);
                if (!$card) {
                    $card = new CreditCard();
                } else {
                    $card = $card[0];
                }
                $card->cardNumber = substr($this->creditCard['cardNumber'], -4);
                $card->payer_id = $this->refNumber;
                $card->customer_id = $this->customer_id;
                $card->business_id = $this->business_id;
                $card->expMo = $this->creditCard['expMonth'];

                $card->expYr = $this->creditCard['expYear'];
                if (array_key_exists('address', $creditCard)) {
                    $card->address1 = $this->creditCard['address'];
                }
                if (array_key_exists('state', $creditCard)) {
                    $card->state = $this->creditCard['state'];
                }
                if (array_key_exists('zipcode', $creditCard)) {
                    $card->zip = $this->creditCard['zipcode'];
                }
                if (array_key_exists('city', $creditCard)) {
                    $card->city = $this->creditCard['city'];
                }
                $card->processor = $this->processor;
                $card->cardType = $this->identify($this->creditCard['cardNumber']);
                $card->save();

                //update the order and orderstatus
                $CI->order_model->update(array('statusDate' => dateString_to_gmt(date("Y-m-d H:i:s")), 'orderStatusOption_id'=>10, 'orderPaymentStatusOption_id'=>3), array('orderID'=>$order_id));
                $CI->load->model('orderpaymentstatus_model');
                $CI->orderpaymentstatus_model->order_id = $order_id;
                $CI->orderpaymentstatus_model->orderPaymentStatusOption_id = 3;
                $CI->orderpaymentstatus_model->insert();

                $result['status'] = 'success';
                $result['order_id'] = $order_id;
                $result['creditCardID'] = $card->creditCardID;
                $result['refNumber'] = $card->payer_id;
                $result['message'] = $response->get_message();

            } else {
                // delete the order
                $CI->order_model->options = array('orderID'=>$order_id);
                $CI->order_model->delete();

                $result['status'] = 'error';
                $result['order_id'] = $order_id;
                $result['message'] = $response->get_message();
            }
            /* ************************* */
            /* End * */
            /* ************************* */
        } else {
            $result = array("status" => "error", "message" => "The prepayment locker '{$business->prepayment_locker_id}' does not exist for '{$business->companyName}'");
        }

        return $result;

    }



    /**
     * This method will make sure tha the credit card ids filled with proper values
     *
     * reuired post input
     * ----------------
     * cardNumber
     * csc
     * expMonth
     * expYear
     * fullName
     * firstName
     * lastName
     * address
     * state
     * city
     * zipcode
     *
     * @param array $postData
     * @return array - status and message
     */
    public function setCard($postData)
    {
        if (empty($postData)) {
            throw new \Exception('Missing postData variables');
        }

        $requiredCardValues = (array('cardNumber', 'csc', 'expMonth', 'expYear'));
        foreach ($requiredCardValues as $key=>$value) {
            if (!key_exists($value, $postData) || empty($postData[$value])) {
                $errors[] = "Missing $value";
            }
        }

        // if there are any missing values, return the error and do not try to process
        if (!empty($errors)) {
            return array('status'=>'fail', 'message'=>$errors, 'requiredCardValues'=>$requiredCardValues);
        }

        // remove any non-numeric characters
        $postData['cardNumber'] = preg_replace('/[^0-9]/', '', $postData['cardNumber']);

        $cardType = $this->identify($postData['cardNumber']);
        $creditCard['cardNumber'] = $postData['cardNumber'];
        $creditCard['csc'] = $postData['csc'];
        $creditCard['expMonth'] = $postData['expMonth'];
        $creditCard['expYear'] = $postData['expYear'];
        $creditCard['cardType'] = $cardType;

        // if  passed from card, we will try to use the customer infomra
        if (array_key_exists("fullName", $postData)) {
            $creditCard['nameOnCard'] = $postData['fullName'];
        }
        if (array_key_exists('firstName', $postData)) {
            $creditCard['firstName'] = $postData['firstName'];
        }
        if (array_key_exists('lastName', $postData)) {
            $creditCard['lastName'] = $postData['lastName'];
        }
        if (array_key_exists('address', $postData)) {
            $creditCard['address'] = $postData['address'];
        }
        if (array_key_exists('state', $postData)) {
            $creditCard['state'] = $postData['state'];
        }
        if (array_key_exists('city', $postData)) {
            $creditCard['city'] = $postData['city'];
        }

        if (array_key_exists('zipcode', $postData)) {
            $creditCard['zipcode'] = $postData['zipcode'];
        }

        $creditCard['countryCode'] = 'us';

        $this->creditCard = $creditCard;

        return  array('status'=>'success', 'message'=>'card values set');

    }

}
