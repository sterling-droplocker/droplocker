<?php
namespace App\Libraries\DroplockerObjects;

class Api_Request_Log extends DroplockerObject
{
    public static $table_name = "api_request_log";
    public static $primary_key = "api_request_logID";
    protected $rules = array(
        "requestType" => array("required"),
        "url" => array("required"),
    );

    public static function create() {
        $api_request_log = new Api_Request_Log();
        $CI = get_instance();
        $api_request_log->url = @$_SERVER['HTTP_HOST'] . @$_SERVER['REQUEST_URI'];
        if ($CI->input->post()) {
            $api_request_log->requestType = "POST";
            $api_request_log->requestBody = file_get_contents("php://input");
        } else {
            $api_request_log->requestType = "GET";
        }

        $api_request_log->clientAddress = $CI->input->ip_address();
        $api_request_log->serverAddress = $_SERVER['SERVER_ADDR'];
        $api_request_log->hostName = gethostname();
        $api_request_log->userAgentString = $CI->agent->agent_string();
        $api_request_log->save();
        return $api_request_log;
    }
}
