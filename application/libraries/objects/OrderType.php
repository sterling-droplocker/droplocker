<?php

namespace App\Libraries\DroplockerObjects;

class OrderType extends DroplockerObject
{
    public static $table_name = "orderType";
    public static $primary_key = "orderTypeID";

    /**
     * gets the image for the orderType.
     * If this is used outside of droplocker, ie. the iPhone, you will
     * need to append the url
     *
     * $orderTypeSlug possible values
     * --------------
     * DC
     * WF
     * Other
     * SS
     * SR
     * LS
     *
     * @param string $orderTypeSlug
     * @return string|null image path and filename or null
     */
    public static function getImage($orderTypeSlug)
    {
        $obj = \App\Libraries\DroplockerObjects\OrderType::search_aprax(array('slug'=>$orderTypeSlug));
        if (sizeof($obj)==1) {
            return "/images/".$obj[0]->image;
        } else {
            return null;
        }
    }


    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
    }
}
