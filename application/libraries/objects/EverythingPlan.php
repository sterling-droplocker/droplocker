<?php
namespace App\Libraries\DroplockerObjects;
class EverythingPlan extends DroplockerObject
{
    public static $primary_key = "laundryPlanID";
    public static $table_name = "laundryPlan";


    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new \App\Libraries\DroplockerObjects\Customer($this->customer_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Customer ID {$this->customer_id} not found");
        }
        try {
            new \App\Libraries\DroplockerObjects\Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found");
        }
        if (empty($this->plan)) {
            throw new Validation_Exception("'plan' can not be empty");
        }
        if (!is_numeric($this->price)) {
            throw new Validation_Exception("'price' must be numeric");
        }
    }

    /**
     * checks to see if the customer is going to upgrade after the enddate of the current plan
     */
    public static function is_upgrading($customer_id)
    {
        $CI = get_instance();
        if (!is_numeric($customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }
        $sql = "SELECT renew, laundryPlanID FROM laundryPlan WHERE customer_id = {$customer_id} and active = 1 and type = 'everything_plan'";
        $query = $CI->db->query($sql);
        if ($query->num_rows()==1) {
            return false;
        }

        foreach ($query->result() as $plan) {
            if ($plan->renew == 1) {
                return $plan->laundryPlanID;
            }
        }

        return false;
    }

    /**
     * gets the wash and fold price.
     *
     * @param int $customer_id
     * @param int $business_id
     * @return float
     */
    public function getDefaultPrice($customer_id, $business_id)
    {
        $CI = get_instance();

        // viewing this page from a public page, ie... no cookies set yet
        $CI->load->model('product_model');
        $defaultWF = $CI->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $business_id);

        $sql = "SELECT price FROM product_process pp WHERE pp.product_processID = ?";
        $defaultWfPrice = $CI->db->query($sql, array($defaultWF))->row();

        if ( !empty( $customer_id ) ) {

            // get the price fror wash and fold for the customer
            $sql = "SELECT price FROM customer c
                    INNER JOIN product_process pp ON pp.product_processID = defaultWF
                    WHERE customerID = " . $customer_id;
            $customerWfPrice = $CI->db->query($sql)->row();
        }

        if ( empty( $customerWfPrice->price ) || $customerWfPrice->price < 0.01  ) {
            return (float) $defaultWfPrice->price;
        } else {
            return (float) $customerWfPrice->price;
        }
    }

    /**
     * get all active plans for given business
     */
    public static function get_active_plans($business_id)
    {
        $CI = get_instance();
        if (!is_numeric($business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }

        $sql = "SELECT laundryPlanID, customerID, firstName, lastName, description, startDate, businessLaundryPlan_id, laundryPlanID
                FROM (laundryPlan)
                JOIN customer ON customerID = customer_id
                WHERE laundryPlan.active =  1
                AND laundryPlan.business_id =  ?
                AND laundryPlan.type = 'everything_plan' ORDER BY concat(firstName, lastName) ASC";

        return $CI->db->query($sql, array($business_id))->result();
    }

    public static function getActiveEverythingPlan($customer_id)
    {
        $CI = get_instance();
        $CI->db->where('customer_id', $customer_id);
        $CI->db->where('type', 'everything_plan');
        $CI->db->where('active', 1);
        $results = $CI->db->get(self::$table_name)->result();

        return $results;
    }
}
