<?php
namespace App\Libraries\DroplockerObjects;

class Order extends DroplockerObject
{
    public static $has_many = array("OrderItem", "OrderCharge", "Transaction", "OrderStatus"); //An order has many items
    public static $has_one = array("Customer", "Locker", "Bag");

    public static $table_name = "orders";
    public static $primary_key = "orderID";

    protected $rules = array(
        "orderPaymentStatusOption_id" => array("entity_must_exist", "numeric"),
        "business_id" => array("required", "entity_must_exist", "numeric"),
        "customer_id" => array("required", "entity_must_exist", "numeric"),
        "orderStatusOption_id" => array("entity_must_exist", "numeric"),
        "bag_id" => array("numeric", "entity_must_exist")
    );
    
    const CREATE_ERROR_BAG_NUMBER_MAX = 1001; 
    const CREATE_ERROR_BAG_NUMBER_NOT_FOUND = 1002; 
    const CREATE_ERROR_BAG_NUMBER_OPEN_ORDER = 1003; 
    const CREATE_ERROR_CUSTOMER_ON_HOLD = 1004; 
    const CREATE_ERROR_MULTIPLE_CLAIMS_ON_LOCKER = 1005; 
    const CREATE_ERROR_GET_ORDER_DETAILS = 1006;
    
    const SOURCE_POS_APP = 'pos_app';
    
    public function save()
    {
        //If the order is not associated with a locker, then we assign the order to the business specified return to office locker.
        $business = new Business($this->business_id);
        if (empty($this->locker_id)) {
            $this->locker_id = $business->returnToOfficeLockerId;
        }
        if (empty($this->{self::$primary_key})) {
            $this->dateCreated = new \DateTime($business->timezone);
        }
        parent::save();
    }
    /**
     * checks to see if the current order was delivered to a locker that has a previous delivery
     * with a different customer within the hour.
     *
     * This function will send an email to customer supprt if there are recent orders in the locker
     *
     * @return int $emailID
     */
    public function checkRecentDeliveryToSameLockerDifferentCustomer()
    {
        $CI = get_instance();
        $emailID = null;

        // if this is not a locker (ie. front_counter, prepayment)
        $locker = new Locker($this->locker_id);
        $location = new Location($locker->location_id);
        if ($location->getLocationType() != 'lockers') {
            return true;
        }


        $business_id = $this->business_id;

        $sql = "SELECT locker_id, orderID, statusDate, customer_id
                FROM orders
                WHERE locker_id = {$this->locker_id}
                AND statusDate >= date_add(NOW(), INTERVAL -1 HOUR)
                AND orderStatusOption_id IN (9,10)
                AND business_id = {$this->business_id}
                AND orderID != {$this->{self::$primary_key}}
                AND customer_id != {$this->customer_id}";
        $recentOrders = $CI->db->query($sql)->result();

        if ($recentOrders) {

            $body = "Warning: An Order was delivered to a locker that has a delivery within an hour by a different customer<br>";
            $body .= "Order [<a href='https://droplocker.com/admin/orders/details/{$this->orderID}'>{$this->orderID}</a>] ";
            $body .= "<h3>Recent Orders in the same locker and different customer</h3>";
            $body .= "<ul>";
            foreach ($recentOrders as $recentOrder) {

                $body .= "<li>Order [<a href='https://droplocker.com/admin/orders/details/{$recentOrder->orderID}'>{$recentOrder->orderID}</a>] - Customer[$recentOrder->customer_id] - statusDate = ".convert_from_gmt_aprax($recentOrder->statusDate, STANDARD_DATE_FORMAT, $business_id)."</li>";

            }
            $body .= "</ul>";

            $emailID = send_admin_notification('Warning: An Order was delivered to a locker that has a delivery within an hour by a different customer', $body, $business_id);

        }

        return $emailID;
    }



    /**
     * @deprecated The following function is *.
     * Get the orderType for the order
     *
     * @return string
     */
    public function getOrderType()
    {
        $CI = get_instance();
        $orderType = 'Other';
        $CI->load->model("orderItem_model");
        $CI->orderItem_model->order_id = $this->orderID;
        $orderItems = $CI->orderItem_model->get();

        if ($orderItems) {
            foreach ($orderItems as $item) {
                $sql = "SELECT product.name, productCategory.module_id FROM product
                                JOIN productCategory ON productCategoryID=productCategory_id
                                INNER JOIN product_process ON product_id = productID
                                WHERE product_processID = ".$item->product_process_id;
                $query = $CI->db->query($sql);
                $product = $query->row();
                if (stristr($product->name, 'whsl') || stristr($product->name, 'linen')) {
                    $orderType = 'Wholesale';
                }

                if ($product->module_id == WFMODULE) {
                    $orderType = 'WF';
                    break;
                }

                if (in_array($product->module_id, array(2,3))) {
                    $orderType = 'DC';
                }

            }
        }

        return $orderType;
    }

    /**
     * Adds an item to a redo order
     *
     * @param stdClass $item
     * @param int $business_id
     * @param int $qty
     * @param int $supplier_id
     * @param int $price
     * @throws \Exception
     */
    public function addRedoItem($item, $business_id, $qty = 1, $supplier_id = '', $price = 0)
    {
        $CI = get_instance();
        $redo_id = get_business_meta($this->business_id, 'default_redo_id');

        // TODO: WTF? This belongs int the controller
        if (empty($redo_id)) {
            set_flash_message('error', "Business must have a default redo product before making redo orders");
            redirect("/admin/admin/defaults");
        }

        $supplier = '';
        $CI->load->model("orderitem_model");
        $pp = new \App\Libraries\DroplockerObjects\Product_Process($item->product_process_id);

        $product_processID = $pp->product_processID;
        if (empty($product_processID)) {
            throw new \Exception("Missing product process");
        } else {
            $supplier = $pp->get_default_supplier();
        }
        $itemObj = new \App\Libraries\DroplockerObjects\Item($item->itemID);

        //TODO: add an $employee_id paramter instead of using get_current_employee_id()
        $insert_id = $CI->orderitem_model->add_item(
            $this->orderID,             // $order_id
            $item->itemID,              // $item_id
            $redo_id,                   // $product_process_id
            $qty,                       // $qty
            get_current_employee_id(),  // $employee_id
            $supplier->supplierID,      // $supplier_id
            $price,                     // $price
            $itemObj->getProductName()  // $notes
        );
    }

    /**
     * @deprecated This function does not follow coding standsards and the purpose of the Droplocekr Objects and should be in the orderItem model.
     * Gets all the order items in the order
     *
     * @return array of codeigniter query objects, each with the following properties
     *  qty
     *  unitPrice
     *  subTotal
     *  notes
     *  item_id
     *  updated
     *  displayName
     *  orderItemID
    */
    public function getItems()
    {
        $CI = get_instance();
        $sql = "SELECT orderItem.qty, orderItem.unitPrice, (orderItem.qty * orderItem.unitPrice) as subTotal, orderItem.item_id, orderItem.notes, orderItem.updated, product.displayName,orderItemID  FROM orderItem
                LEFT JOIN product_process ON product_process.product_processID = orderItem.product_process_id
                LEFT JOIN product ON product.productID = product_process.product_id
                WHERE orderItem.order_id = ". $this->orderID;

        $items = $CI->db->query($sql)->result();

        return $items;
    }

    /**
     * The following function is *, do not use
     * get the total for the order
     * @deprecated Use get_subtotal in hte order model instead
     * The total is defined as the tthe combination of the order item total and any order charges/discountsw
     * @return float
    */
    public function getTotal()
    {
        $CI = get_instance();
        $CI->load->model("orderItem_model");
        $totals = $CI->orderItem_model->get_item_total($this->orderID);

        return (float) $totals['total'];
    }

    /**
     * The following function is unknown *.
     * @return type
     */
    public function getInventoryEmployee()
    {
        $CI = get_instance();
        $sql = "SELECT firstName, lastName, employeeID from employee
                INNER JOIN orderStatus on employeeID = employee_id
                WHERE order_id = {$this->{self::$primary_key}}
                AND orderStatusOption_id=3";
        $employee = $CI->db->query($sql)->row();

        return $employee;
    }

    /**
     * Retrieves the total amount for the order without any discount.gr
     * @deprecated Use get_gross_total in the order model instead
     * @return float
     */
    public function get_gross_total()
    {
        $CI = get_instance();
        $CI->load->model("orderItem_model");
        $totals = $CI->orderItem_model->get_item_total($this->orderID);

        return (float) $totals['subtotal'];
    }

    /**
     * Retreives the total discounts amount in the order.
     * @deprecated, use get_discount_total in the order model instead
     * @return float
     */
    public function get_discount_total()
    {
        $CI = get_instance();
        $CI->load->model("orderItem_model");
        $totals = $CI->orderItem_model->get_item_total($this->orderID);

        return (float) $totals['discounts'];
    }
    /**
     * Returns the total cost to the business for an order
     * @return float The total expense for the business to for an order
     */
    public function get_cost_total()
    {
        $CI = get_instance();
        $cost = 0;
        $CI->load->model("orderItem_model");
        $CI->orderItem_model->order_id = $this->orderID;
        $items = $CI->orderItem_model->get();
        if ($items) {
            foreach ($items as $item) {
                $cost += $item->cost * $item->qty;
            }
        } else {
            $cost = 0;
        }

        return (float) $cost;
    }

    /**
     * This is *, do not use.
     * Gets all the charges for an order
     * @deprecated This functionality doe snot belong in the Droplocker Objects.
     * @return array of objects
    */
    public function getCharges()
    {
        $CI = get_instance();
        $CI->load->model("orderCharge_model");
        $CI->orderCharge_model->order_id = $this->orderID;

        return $CI->orderCharge_model->get();
    }

    /**
     * This is *, do not use.\
     * @deprecated This does not belong in the Droplocker Objects. Use get_discount_total() in the order model instead.
    * Gets all the charge total
    *
    * If $orderCharges are not passed, use the order objects orderID
    * to get all the charges
    *
    * @param array $orderCharges
    * @return float|false
    */
    public function getChargesTotal($orderCharges = '')
    {
        if (!isset($orderCharges)) {
            $orderCharges = $this->getCharges();
        }
        foreach ($orderCharges as $charge) {
            $total +=  $charge->chargeAmount;
        }

        return $total;
    }


    public function validate()
    {
        //Note, an order is allowed to have a locker_id attribute value of '0'.
        if (is_numeric($this->locker_id) && $this->locker_id != 0) {
            try {
                new Locker($this->locker_id);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("Locker ID '{$this->locker_id}' not found.");
            }
        } else {
            throw new Validation_Exception("'locker_id' must be numeric.");
        }
        if (!empty($this->dueDate) && strtotime($this->dueDate) === false && $this->dueDate != '0000-00-00') {
            throw new Validation_Exception("Invalid due date '{$this->dueDate}'");
        }
        parent::validate();
    }
    
    public static function isPosOrder($order)
    {
        return $order->source === self::SOURCE_POS_APP;
    }
}
