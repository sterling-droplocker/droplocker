<?php

namespace App\Libraries\DroplockerObjects;
class OrderStatus extends DroplockerObject
{
    public static $table_name = "orderStatus";
    public static $primary_key = "orderStatusID";
    public static $has_one = array("OrderStatusOption", "Employee", "Locker", "Order");

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
    }

    public function validate()
    {
        if (!is_numeric($this->order_id)) {
            throw new Validation_Exception("'order_id' must be numeric.");
        }
        try {
            new Order($this->order_id);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Order ID '{$this->order_id}' not found in database.");
        }
        if (!is_numeric($this->orderStatusOption_id)) {
            throw new Validation_Exception("'orderStatusOption' must be numeric.");
        }
        if (isset($this->employee_id)) {
            if (!is_numeric($this->employee_id)) {
                throw new Validation_Exception("'employee_id' must be numeric.");
            }
            try {
                new Employee($this->employee_id);

            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("Employee ID '$this->employee_id' not found in database.");
            }
        }
    }
    public static function search_aprax($options, $get_relationships = false, $order_by = null, $limit = false)
    {
        return parent::search_aprax($options, $get_relationships, $order_by, $limit);
    }
}
