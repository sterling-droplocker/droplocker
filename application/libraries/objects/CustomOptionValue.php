<?php
namespace App\Libraries\DroplockerObjects;

class CustomOptionValue extends DroplockerObject
{

    public static $table_name = "customOptionValue";
    public static $primary_key = "customOptionValueID";
    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "customOption_id" => array("numeric", "required", "entity_must_exist"),
        "value" => array("required"),
    );

    public static $belongsTo = array("CustomOption");

    /**
     * Stores current order to detect changes
     *
     * @var int
     */
    protected $currentOrder = null;

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
        if ($id) {
            $this->currentOrder = $this->sortOrder;
        }
    }

    public function validate()
    {
        parent::validate();

        $options = array(
            'value' => $this->value,
            'business_id' => $this->business_id,
            'customOption_id' => $this->customOption_id
        );
        if ($this->customOptionValueID) {
            $options['customOptionValueID !='] = $this->customOptionValueID;
        }
        if (CustomOptionValue::search_aprax($options)) {
            throw new Validation_Exception("'value' should be unique");
        }
        return true;
    }

    protected function beforeSave()
    {
        $CI = get_instance();
        $CI->load->model('customoptionvalue_model');

        $max_sortOrder = $CI->customoptionvalue_model->getMaxSortOrder($this->business_id, $this->customOption_id);

        // If the sortOrder is no set or greater than the next sortOrder, use the next sortOrder
        if (empty($this->sortOrder) || $this->sortOrder > ($max_sortOrder + 1)) {
            $this->sortOrder = $max_sortOrder + 1;
        }

        // if updating, sortOrder can't be greater than max_sortOrder
        if ($this->sortOrder > $max_sortOrder && $this->customOptionID) {
            $this->sortOrder = $max_sortOrder;
        }

        return true;
    }

    protected function afterSave($created)
    {
        $CI = get_instance();
        if ($created || $this->currentOrder != $this->sortOrder) {
            $CI->load->model('customoptionvalue_model');
            $CI->customoptionvalue_model->updateSortOrder($this->business_id, $this->customOption_id, $this->customOptionValueID, $this->sortOrder, $this->currentOrder);
        }

        if ($this->default) {
            $CI->load->model('customoptionvalue_model');
            $CI->customoptionvalue_model->updateDefault($this->business_id, $this->customOption_id, $this->customOptionValueID, $this->default);
        }
    }

    protected function afterDelete()
    {
        $CI = get_instance();

        // fix sort order in values
        $CI->load->model('customoptionvalue_model');
        $CI->customoptionvalue_model->updateSortOrder($this->business_id, $this->customOption_id, $this->customOptionValueID, 0, $this->sortOrder);

        // delete my translations
        $CI->load->model('customoptionvaluename_model');
        $CI->customoptionvaluename_model->delete_aprax(array(
            'customOptionValue_id' => $this->customOptionValueID
        ));

    }

    public function getValueNames()
    {
        $out = array();
        $found = CustomOptionValueName::search_aprax(array(
            'customOptionValue_id' => $this->customOptionValueID,
        ));

        foreach ($found as $CustomOptionValue) {
            $out[$CustomOptionValue->business_language_id] = $CustomOptionValue;
        }

        return $out;
    }

    public function saveValueNames($names)
    {
        $valueNames = $this->getValueNames();
        $out = array();
        foreach ($names as $businesLanguageID => $title) {
            if (isset($valueNames[$businesLanguageID])) {
                $valueName = $valueNames[$businesLanguageID];
            } else {
                $valueName = new CustomOptionValueName();
                $valueName->customOptionValue_id = $this->customOptionValueID;
                $valueName->business_language_id = $businesLanguageID;
            }
            $valueName->name = $title;
            $valueName->save();

            $out[$businesLanguageID] = $valueName;
        }

        return $out;
    }

}
