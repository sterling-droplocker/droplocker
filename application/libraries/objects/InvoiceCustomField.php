<?php
namespace App\Libraries\DroplockerObjects;

class InvoiceCustomField extends DroplockerObject
{

    public static $table_name = "invoiceCustomField";
    public static $primary_key = "invoiceCustomFieldID";
    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "name" => array("required"),
    );

    public static $has_many = array("InvoiceCustomFieldValue");

    /**
     * Stores current order to detect changes
     *
     * @var int
     */
    protected $currentOrder = null;

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
        if ($id) {
            $this->currentOrder = $this->currentOrder;
        }
    }

    public function validate()
    {
        parent::validate();

        $options = array(
            'name' => $this->name,
            'business_id' => $this->business_id
        );
        if ($this->invoiceCustomFieldID) {
            $options['invoiceCustomFieldID !='] = $this->invoiceCustomFieldID;
        }
        if (InvoiceCustomField::search_aprax($options)) {
            throw new Validation_Exception("'name' should be unique");
        }

        return true;
    }

    protected function beforeSave()
    {
        $CI = get_instance();
        $CI->load->model('invoiceCustomField_model');

        $max_sortOrder = $CI->invoiceCustomField_model->getMaxSortOrder($this->business_id);

        // If the sortOrder is no set or greater than the next sortOrder, use the next sortOrder
        if (empty($this->sortOrder) || $this->sortOrder > ($max_sortOrder + 1)) {
            $this->sortOrder = $max_sortOrder + 1;
        }

        // if updating, sortOrder can't be greater than max_sortOrder
        if ($this->sortOrder > $max_sortOrder && $this->invoiceCustomFieldID) {
            $this->sortOrder = $max_sortOrder;
        }

        return true;
    }

    protected function afterSave($created)
    {
        $CI = get_instance();

        if ($created || $this->currentOrder != $this->currentOrder) {
            $CI->load->model('invoiceCustomField_model');
            $CI->invoiceCustomField_model->updateSortOrder($this->business_id, $this->invoiceCustomFieldID, $this->sortOrder, $this->currentOrder);
        }
    }

    protected function afterDelete()
    {
        $CI = get_instance();

        // fix sort order in options
        $CI->load->model('invoicecustomfield_model');
        $CI->invoicecustomfield_model->updateSortOrder($this->business_id, $this->invoiceCustomFieldID, 0, $this->sortOrder);

        // delete my translations
        $CI->load->model('invoicecustomfieldvalue_model');
        $CI->invoicecustomfieldvalue_model->delete_aprax(array(
            'invoiceCustomField_id' => $this->invoiceCustomFieldID
        ));
    }

    public function getValues()
    {
        /*return InvoiceCustomFieldValue::search_aprax(array(
            'invoiceCustomField_id' => $this->invoiceCustomFieldID
        ));*/
        $out = array();
        $found = InvoiceCustomFieldValue::search_aprax(array(
            'invoiceCustomField_id' => $this->invoiceCustomFieldID
        ));

        foreach ($found as $InvoiceCustomField) {
            $out[$InvoiceCustomField->business_language_id] = $InvoiceCustomField;
        }

        return $out;

    }

    public function saveValues($titles, $values)
    {
        $currentValues = $this->getValues();
        $out = array();

        foreach ($titles as $businessLanguageID => $title) {
            if (isset($currentValues[$businessLanguageID])) {
                $invoiceCustomFieldValue = $currentValues[$businessLanguageID];
            } else {
                $invoiceCustomFieldValue = new InvoiceCustomFieldValue();
                $invoiceCustomFieldValue->invoiceCustomField_id = $this->invoiceCustomFieldID;
                $invoiceCustomFieldValue->business_language_id = $businessLanguageID;
            }
            $invoiceCustomFieldValue->title = $title;
            $invoiceCustomFieldValue->value = $values[$businessLanguageID];
            $invoiceCustomFieldValue->save();

            $out[$businessLanguageID] = $invoiceCustomFieldValue;
        }

        return $out;
    }

}