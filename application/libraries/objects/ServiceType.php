<?php
namespace App\Libraries\DroplockerObjects;
class ServiceType extends DroplockerObject
{
    public static $table_name = "serviceType";
    public static $primary_key = "serviceTypeID";

    public static function updateDisplayName($business_id, $names)
    {
        $CI = get_instance();
        $CI->db->query("DELETE FROM serviceTypeDisplayName WHERE business_id = {$business_id}");

        foreach ($names as $serviceType_id=>$name) {
            self::saveDisplayName($business_id, $serviceType_id, $name);
        }
    }

    public static function saveDisplayName($business_id, $serviceType_id, $name)
    {
        $CI = get_instance();
        $CI->db->query("INSERT INTO serviceTypeDisplayName (business_id, serviceType_id, name) VALUES (?, ?, ?)",
            array($business_id, $serviceType_id, $name));
    }

}
