<?php
namespace App\Libraries\DroplockerObjects;
class BusinessProductPlan extends DroplockerObject
{
    public static $primary_key = "businessLaundryPlanID";
    public static $table_name = "businessLaundryPlan";

    /**
     *
     * @param int $source_business_id
     * @param int $destination_business_id
     * @throws \Exception
     */
    public static function copy_from_source_business_to_destination_business($source_business_id, $destination_business_id)
    {
        NotFound_Exception::does_exist($source_business_id, "Business");
        NotFound_Exception::does_exist($destination_business_id, "Business");

        if ($source_business_id == $destination_business_id) {
            throw new \Exception("The destination business can not be the same as the source business");
        }
        $destination_businessProductPlans = self::search_aprax(array("business_id" => $destination_business_id));
        foreach ($destination_businessProductPlans as $destination_businessProductPlan) {
            $destination_businessProductPlan->delete();
        }
        $source_businessProductPlans = self::search_aprax(array("business_id" => $source_business_id));
        foreach ($source_businessProductPlans as $source_businessProductPlan) {
            $destination_businessProductPlan = $source_businessProductPlan->copy();
            $destination_businessProductPlan->business_id = $destination_business_id;
            $destination_businessProductPlan->save();
        }
    }
    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new \App\Libraries\DroplockerObjects\Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found");
        }
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }
        if (!is_numeric($this->credit_amount)) {
            throw new Validation_Exception("'credit amount' must be numeric");
        }
    }
}
