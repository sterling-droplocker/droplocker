<?php
namespace App\Libraries\DroplockerObjects;
class DeliveryNotice extends DroplockerObject
{
    public static $table_name = "deliveryNotice";
    public static $primary_key = "deliveryNoticeID";

    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new \App\Libraries\DroplockerObjects\Business($this->business_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found");
        }
    }
}
