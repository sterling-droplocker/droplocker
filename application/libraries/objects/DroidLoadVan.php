<?php
namespace App\Libraries\DroplockerObjects;

class DroidLoadVan extends \stdClass
{

    /**
     * updates the order to have the status of loaded on van
     *
     * @param int $order_id
     * @param dateTime $deliveryTime
     * @param string $bagNumber
     * @param string $username
     * @param int $employee_id
     * @paramint $business_id
     * @return string SUCCESS|$order_id
     *
     */
    public static function loadVan($order_id, $deliveryTime, $bagNumber, $username, $employee_id, $business_id)
    {
        $CI = get_instance();

        if (!$order_id) {
            throw new \Exception("Missing order id");
        }

        //just add to the history table. DO NOT UPDATE THE ORDER STATUS IN THE ORDER TABLE
        $insert1 = "insert into orderStatus
        (order_id, orderStatusOption_id, date, customer_userName, employee_id)
        values ('$order_id', '14', '$deliveryTime', '$username', '$employee_id');";
        $insertVan = $CI->db->query($insert1);
        $insert_id = $CI->db->insert_id();

        $sql =   "update orders set loaded = 1 where orderID = '$order_id';";
        $update = $CI->db->query($sql);
        $affected_rows = $CI->db->affected_rows();
        if ($affected_rows) {
            $message = "Post to van updated order";
        } else {
            $message = "Post to van did not update order";
        }

        // Update the transaction
        $CI->load->model('driver_log_model');
        $options = array();
        $options['order_id'] = $order_id;
        $options['employee_id'] = $employee_id;
        $options['delTime'] = $deliveryTime;
        $options['delType'] = 'loadVan';
        $options['bagNumber'] = $bagNumber;
        $options['business_id'] = $business_id;
        $CI->driver_log_model->insertTransaction($options);

        return json_encode(array('status'=>'SUCCESS', 'order_id'=>$order_id, 'order_status_id' => $insert_id));


    }

}
