<?php
namespace App\Libraries\DroplockerObjects;
class CustomerDiscount extends DroplockerObject
{
    public static $table_name = "customerDiscount";
    public static $primary_key = "customerDiscountID";

    public function save()
    {
        if (array_key_exists("expireDate", $this->properties) && empty($this->expireDate)) {
            unset($this->properties['expireDate']);
        }
        parent::save();
    }
    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }

        try {
            new Customer($this->customer_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Customer ID {$this->customer_id} not found in database");
        }

        if (!is_numeric($this->amount)) {
            throw new Validation_Exception("'amount' must be numeric.");
        }

        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }

        try {
            new Business($this->business_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found in database");
        }

        if ($this->combine != 0 && $this->combine != 1) {
            throw new Validation_Exception("'combine' must be 1 or 0");
        }

        // detect specials amount types
        if (substr($this->amountType, 0, 6) == 'item[%') {
            $amountType = "itemPercent";
        } elseif (substr($this->amountType, 0, 5) == 'item[') {
            $amountType = "item";
        } else {
            $amountType = $this->amountType;
        }

        if (!array_key_exists($amountType, Coupon::$amountTypes)) {
            throw new Validation_Exception("amountType '{$this->amountType}' is not a valid amount type.");
        }

        if (!array_key_exists($this->frequency, Coupon::$frequencies)) {
            throw new Validation_Exception("'{$this->frequency}' is not a valid frequency.");
        }
    }

    /**
     * Adds a customer discount to a customer
     * @param int $custId
     * @param float $amount
     * @param String $amountType
     * @param String $frequency
     * @param String $description
     * @param String $extendedDesc
     * @param String $expireDate
     * @return int|boolean -- if success, will return the insert_id
     */
    public static function addDiscount($custId, $amount, $amountType, $frequency, $description='', $extendedDesc='', $expireDate='')
    {
        $CI = get_instance();

        if (empty($custId)) {
            throw new \Exception("Missing customerID (custId)");
        }

        $customer = new Customer($custId);
        //make sure not to double add a discount
        $select1 = "select * from customerDiscount
        where customer_id = $custId
        and extendedDesc = '$extendedDesc'";
        $discounts = $CI->db->query($select1)->result();

        if (!$discounts) {

            if ($amountType == 'lockerloot') {
                //add discount as Locker Loot
                $insert1 = "insert into lockerLoot
                set points =  '$amount',
                customer_id = '$custId',
                updated = now()";
                if ($description) {
                    $insert1 .= ", description = " . $CI->db->escape( $description );
                }

                $insert = $CI->db->query($insert1);

                $insert2 = "insert into customerDiscount
                set customer_id = '$custId',
                business_id = {$customer->business_id},
                amount = '$amount',
                amountType = '$amountType',
                frequency = '$frequency',
                description = " . $CI->db->escape( $description ) . ",
                extendedDesc = " . $CI->db->escape( $extendedDesc ) . ",
                active = 0,
                origCreateDate = now(),
                updated = now();";
                $insert = $CI->db->query($insert2);
            } else {
                $insert1 = "insert into customerDiscount
                            set customer_id = '$custId',
                            business_id = {$customer->business_id},
                            amount = '$amount',
                            amountType = '$amountType',
                            frequency = '$frequency',
                            description = " . $CI->db->escape( $description ) . ",
                            extendedDesc = " . $CI->db->escape( $extendedDesc ) . ",
                            active = 1,
                            origCreateDate = now(),
                            updated = now()";

                if ($expireDate) {
                    $expireDate = date("Y-m-d", strtotime($expireDate));
                    $insert1 .= ", expireDate = '$expireDate'";
                }

                $insert = $CI->db->query($insert1);

            }

            return $CI->db->insert_id();

        } else {
            return false;
        }

    }

}
