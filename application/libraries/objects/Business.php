<?php
namespace App\Libraries\DroplockerObjects;
class Business extends DroplockerObject
{
    public static $table_name = "business";
    public static $primary_key = "businessID";
    public static $has_one = array("Location");
    public static $belongsTo = array("Business_Employee");

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
    }
    public function validate()
    {
        if (empty($this->companyName)) {
            throw new Validation_Exception("'companyName' can not be empty.");
        }
        if (!is_numeric($this->blank_customer_id)) {
            throw new Validation_Exception("'blank_customer_id' can not be empty.");
        }
        if (empty($this->timezone)) {
            throw new Validation_Exception("'timezone' can not be empty.");
        }
    }
    public function save()
    {
        $this->validate();
        $default_report_cut_off_time = new \DateTime($this->default_report_cut_off_time);
        $this->default_report_cut_off_time = $default_report_cut_off_time->format("H:i");

        return parent::save();
    }
    /**
     * The following function copies all of the base coupons into a particular business.
     */
    public function apply_all_coupons()
    {
        $coupons = Coupon::search_aprax(array("base_id" => 0));
        foreach ($coupons as $coupon) {
            $business_coupon = $coupon->copy();
            $business_coupon->base_id = $coupon->{Coupon::$primary_key};
            $business_coupon->business_id = $this->{self::$primary_key};
            $business_coupon->save();
        }
    }
    /**
     * creates a new business
     * @deprecated
     */
    public function AddNewBusiness($postData)
    {
        // make sure we don't have a business object instantiatied
        if (array_key_exists('businessID', $this->properties)) {
            throw new \Exception("Instance of business object found");
        }

        $CI = get_instance();
        $timezones = \DateTimeZone::listIdentifiers();

        $this->website_url = $postData['domainName'];
        $this->companyName = $postData['companyName'];
        $this->address = $postData['address'];
        $this->address2 = $postData['address2'];
        $this->city = $postData['city'];
        $this->state = $postData['state'];
        $this->zipcode = $postData['zipcode'];
        $this->phone = $postData['phone'];
        $this->email = $postData['email'];
        $this->timezone = $timezones[$postData["timezone"]];
        $this->serviceTypes = $postData['serviceTypes'];
        $this->blank_customer_id = 0; // needed to get past validation

        $this->slug = preg_replace('/[^a-z0-9]/i', '_', trim($postData['companyName']));
        $business_id = $this->save();

        $CI->load->model('business_model');
        $CI->business_model->copyAcl($business_id);


        $customer = new Customer();
        $customer->firstName = "Blank";
        $customer->lastName = "Customer";
        $customer->business_id = $business_id;
        $customer->email = "blankcustomer@".$this->website_url;
        $customer->phone = $this->phone;
        $customer->username = 'blankCustomer'.$this->slug;
        $customer->password = md5(date("Y-m-d H:i:s").rand(1,10000));
        $customer->address1 = $this->address;
        $customer->city = $this->city;
        $customer->state = $this->state;
        $customer->zip = $this->zipcode;
        $customer->permAssemblyNotes = "NOT A CUSTOMER DO NOT SEND OUT";
        $blankCustomer_id = $customer->save();

        $employee = new Employee();
        $employee->firstName = "Admin";
        $employee->lastName = "User";
        $employee->email = "superadmin@".$this->website_url;
        $employee->llPhone = $this->phone;
        $employee->username = 'superadmin'.$this->slug;
        $employee->password = md5("=-09\][p");
        $employee->address = $this->address;
        $employee->manager_id = 1;
        $employee->city = $this->city;
        $employee->state = $this->state;
        $employee->zipcode = $this->zipcode;
        $adminEmployee_id = $employee->save();

        //get the admin role for this business
        $adminAclRole = \App\Libraries\DroplockerObjects\AclRoles::search_aprax(array('business_id'=>$business_id, 'name'=>'Admin'));

        // make the employee that was just created and admin
        $businessEmployee = new \App\Libraries\DroplockerObjects\Business_Employee();
        $businessEmployee->business_id = $business_id;
        $businessEmployee->employee_id = $adminEmployee_id;
        $businessEmployee->aclRole_id = $adminAclRole[0]->aclID;
        $businessEmployee->default = 1;
        $businessEmployee->save();

        // Need to create a default wash and fold

        $this->blank_customer_id = $blankCustomer_id;
        $this->save();

        return $business_id;

    }

    /**
     * gets the full account domain
     * example: myaccount.dashlocker.com
     *
     * @param int $business_id
     * @return string
     */
    public static function getAccountDomain($business_id)
    {
        $business = new Business($business_id);

        if ( in_bizzie_mode() ) {
            return $business->subdomain . ".bizzie.com";
        } else {
            return "myaccount.".$business->website_url;
        }
    }

    /**
     * Returns the next valid barcode for the business.
     * @return int
     * @throws Exception
    */
    public function get_next_barcode()
    {
        $CI = get_instance();
        $CI->db->select("barcode");
        $CI->db->order_by("barcode", "DESC");
        $CI->db->limit(1);
        $get_last_barcode_query = $CI->db->get_where("barcode", array("business_id" => $this->{self::$primary_key}));
        if ($CI->db->_error_message()) {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $last_barcode = $get_last_barcode_query->row();

        return $last_barcode ? (int) $last_barcode->barcode + 1 : 1;
    }
    /**
     * Returns the defualt wash and fold preferences for each category for the specified business.
     * @param int $business_id
     * @return array Each key is the product categoyr ID and each value is the product ID.
     * @throws \Database_Exception
     */
    public function get_default_wash_and_fold_preferences($business_id)
    {
        $CI = get_instance();
        $business = new Business($business_id);
        $query = "
            SELECT productCategory_id, productID
            FROM product
            JOIN productCategory ON productCategory_id=productCategoryID
            WHERE
                sortOrder = (
                    SELECT MIN(sortOrder)
                        FROM product AS product2
                        JOIN productCategory ON productCategory_id=productCategoryID
                    WHERE
                        product2.productCategory_id = product.productCategory_id AND productCategory.module_id=1 AND business_id={$business->businessID}
                ) AND productCategory.module_id=1 AND product.business_id =3
            GROUP BY productCategory_id
        ";
        $get_business_wash_and_fold_defaults_query = $CI->db->query($query);
        if ($CI->db->_error_message()) {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $get_business_wash_and_fold_defaults_query_result = $get_business_wash_and_fold_defaults_query->result();

        return $get_business_wash_and_fold_defaults_query_result;
    }


    /**
     * The following function is  . Do not use.
     * @deprcated Droplocker Objects should not be used for just retrieving data
     *
     * ---------------------------------
     * BEGIN
     * ---------------------------------
     *
     * gets the locker for return to office
     *
     * ---------------------------------
     * END
     * ---------------------------------
     */
    public function getReturnToOfficeLocker()
    {
        $locker = new Locker($this->returnToOfficeLockerId);
        $location = $locker->find('location_id');
        $result = array('location_id'=>$location->locationID, 'locker_id'=>$locker->lockerID);

        return $result;
    }
    
    public function isHoliday($date) {
        return is_holiday($date, $this->businessID);
    }

}
