<?php
namespace App\Libraries\DroplockerObjects;

interface ReminderInterface
{
    /**
     * required function that gets the reminder object
     */
    public function setReminder();

    /**
     * the function is requeres to make a database call and get an array of reminder objects
     */
    public function performQuery();


    /**
     * sets the array for the job
     *
     * job[] = array('reminderID'=>1,'business_id'=>3, array(1,2,3,4))
     */
    public function getJobs();


}
