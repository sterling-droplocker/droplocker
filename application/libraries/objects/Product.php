<?php
namespace App\Libraries\DroplockerObjects;
class Product extends DroplockerObject
{
    public static $table_name = "product";
    public static $primary_key = "productID";
    public static $has_one = array("ProductCategory", "Business");

    public static $product_image_path = '/images/logos/';

    public static function isWashFold($productProcessID)
    {
        $CI = get_instance();
        $sql = "SELECT productCategory.slug FROM product_process
                INNER JOIN product ON productID = product_id
                INNER JOIN productCategory on productCategory_id = productCategoryID
                WHERE product_processID = " . $productProcessID;
        $productCategory = $CI->db->query($sql)->row();

        return ($productCategory->slug == "wf")?TRUE:FALSE;
    }
    /**
     * Copies all product categories, products, and productt processes from one business to another.
     *
     * @param int $source_business_id
     * @param int $destination_business_id
     * @throws \LogicExceptionÏ
     */
    public static function copy_from_source_business_to_destination_business($source_business_id, $destination_business_id)
    {
        $CI = get_instance();
        if ($source_business_id == $destination_business_id) {
            throw new \LogicException("The source business can not be the same as the destination business");
        } else {
            $destination_products = self::search_aprax(array("business_id" => $destination_business_id));
            foreach ($destination_products as $destination_product) {
                $destination_product->delete();
            }
            $destination_productCategories = ProductCategory::search_aprax(array("business_id" => $destination_business_id));
            foreach ($destination_productCategories as $destination_productCategory) {
                $destination_productCategory->delete();
            }
            $destination_upchargeGroups = UpchargeGroup::search_aprax(array("business_id" => $destination_business_id));
            foreach ($destination_upchargeGroups as $destination_upchargeGroup) {
                $destination_upchargeGroup->delete();
            }
            $source_products = self::search_aprax(array("business_id" => $source_business_id));

            $new_productCategory_lookup = array();
            $new_upchargeGroup_lookup = array();
            foreach ($source_products as $source_product) {
                $destination_product = $source_product->copy();
                $destination_product->business_id = $destination_business_id;

                if (!empty($source_product->productCategory_id)) {
                    $source_productCategory = new ProductCategory($source_product->productCategory_id);
                    if (array_key_exists($source_productCategory->{ProductCategory::$primary_key}, $new_productCategory_lookup)) {
                        $destination_product->productCategory_id = $new_productCategory_lookup[$source_productCategory->{ProductCategory::$primary_key}]->{ProductCategory::$primary_key};
                    } else {
                        $destination_productCategory = $source_productCategory->copy();
                        $destination_productCategory->business_id = $destination_business_id;
                        $destination_productCategory->save();
                        $new_productCategory_lookup[$source_productCategory->{ProductCategory::$primary_key}] = $destination_productCategory;
                        $destination_product->productCategory_id = $destination_productCategory->{ProductCategory::$primary_key};
                    }
                }
                if (!empty($source_product->upchargeGroup_id)) {
                    $source_upchargeGroup = new UpchargeGroup($source_product->upchargeGroup_id);
                    if (array_key_exists($source_upchargeGroup->{UpchargeGroup::$primary_key}, $new_upchargeGroup_lookup)) {
                        $destination_product->upchargeGroup_id = $new_upchargeGroup_lookup[$source_upchargeGroup->{UpchargeGroup::$primary_key}]->{UpchargeGroup::$primary_key};
                    } else {
                        $destination_upchargeGroup = $source_upchargeGroup->copy();
                        $destination_upchargeGroup->business_id = $destination_business_id;
                        $destination_upchargeGroup->save();
                        $new_upchargeGroup_lookup[$source_upchargeGroup->{UpchargeGroup::$primary_key}] = $destination_upchargeGroup;
                        $destination_product->upchargeGroup_id = $destination_upchargeGroup->{UpchargeGroup::$primary_key};
                    }
                }
                $destination_product->save();

                $source_product_processes = Product_Process::search_aprax(array("product_id" => $source_product->{self::$primary_key}));
                foreach ($source_product_processes as $source_product_process) {
                    $destination_product_process = $source_product_process->copy();
                    $destination_product_process->product_id = $destination_product->{self::$primary_key};
                    $destination_product_process->taxGroup_id = null;
                    $destination_product_process->taxable = 0;
                    $destination_product_process->save();
                }
            }
            $CI->load->model("productcategory_model");
            if ($CI->productcategory_model->does_exist(array("slug" => "wf", "business_id" => $destination_business_id)) === false ) {
                ProductCategory::createWashandFold($destination_business_id);
            }
        }
    }

    /**
    * @deprecated This function is deprecated, use the DroplockerObject relationships funcitonality instead.
    * get the name of a product from the product process
    *
    * @param int $product_process_id
    * @return array product name and product displayName
    */
    public static function getName($product_process_id)
    {
        if (!is_numeric($product_process_id)) {
            throw new Validation_Exception("'product_process_id' must be numeric.");
        }
        $CI = get_instance();

        $sql = "SELECT name, displayName FROM product
            INNER JOIN product_process pp ON pp.product_id = productID
            WHERE product_processID = {$product_process_id}";
        $query = $CI->db->query($sql);
        $product = $query->row();

        return array('name'=>$product->name, 'displayName'=>$product->displayName);
    }

    /**
     * Validates the product's properties.
     * @throws Exception
     */
    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
        if (empty($this->displayName)) {
            throw new Validation_Exception("displayName' can not be empty.");
        }
        if (!is_numeric($this->productCategory_id)) {
            throw new Validation_Exception("'productCategory_id' must be numeric.");
        }
        try {
            NotFound_Exception::does_exist($this->productCategory_id, "ProductCategory");
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Product Category ID '{$this->productCategory_id}' not found in database.");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            NotFound_Exception::does_exist($this->business_id, "Business");
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found in database.");
        }
        if (isset($this->upchargeGroup_id)) {
            if (!is_numeric($this->upchargeGroup_id)) {
                throw new Validation_Exception("'upchargeGroup_id' must be numeric");
            }
        }
    }

    public function delete()
    {
        parent::delete();
        $product_processes = Product_Process::search_aprax(array("product_id" => $this->{self::$primary_key}));
        foreach ($product_processes as $product_process) {
            $product_process->delete();
        }
    }

    /**
     * Get the translations for this product
     *
     * @return array
     */
    public function getProductNames()
    {
        $out = array();
        $found = ProductName::search_aprax(array(
            'product_id' => $this->productID
        ));

        foreach ($found as $productName) {
            $out[$productName->business_language_id] = $productName;
        }

        return $out;
    }

    /**
     * Saves the transtalions for a product
     *
     * $names is an array with $businesLanguageID as key and title as value
     *
     * @param array $names
     * @return array
     */
    public function saveProductNames($names)
    {
        $productNames = $this->getProductNames();
        $out = array();
        foreach ($names as $businesLanguageID => $title) {
            if (isset($productNames[$businesLanguageID])) {
                $productName = $productNames[$businesLanguageID];
                if (empty($title)) {
                    $productName->delete();
                    continue;
                }

            } else {
                if (empty($title)) {
                    continue;
                }

                $productName = new ProductName();
                $productName->product_id = $this->productID;
                $productName->business_language_id = $businesLanguageID;
            }
            $productName->title = $title;
            $productName->save();

            $out[$businesLanguageID] = $productName;
        }

        return $out;
    }

}
