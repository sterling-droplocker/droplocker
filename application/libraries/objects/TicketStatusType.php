<?php
namespace App\Libraries\DroplockerObjects;

class TicketStatusType extends DroplockerObject
{
    public static $table_name = "ticketStatusType";
    public static $primary_key = "ticketStatusTypeID";

    public function validate()
    {
        if (empty($this->description)) {
            throw new Validation_Exception("description is required.");
        }
    }
}
