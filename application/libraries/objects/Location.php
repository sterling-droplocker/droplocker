<?php
namespace App\Libraries\DroplockerObjects;

class Location extends DroplockerObject
{
    public static $table_name = "location";
    public static $primary_key = "locationID";
    public static $has_many = array("Locker");
    public static $has_one = array("LocationType");

    const SERVICE_TYPE_ON_DEMAND                 = "on demand";
    const SERVICE_TYPE_DAILY                     = "daily";
    const SERVICE_TYPE_NOT_IN_SERVICE            = "not in service";
    const SERVICE_TYPE_UNATTENDED_KEY_DELIVERY   = "unattended key delivery";
    const SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY = "time window home delivery";
    const SERVICE_TYPE_RECURRING_HOME_DELVIERY   = "recurring home delviery";
    const SERVICE_TYPE_ON_DEMAND_HOME_DELIVERY   = "on demand home delivery";

    /**
     * Lists all the available service types
     *
     * @return string[]
     */
    static public function listServiceTypes()
    {
        return array(
            self::SERVICE_TYPE_ON_DEMAND                 => "On Demand",
            self::SERVICE_TYPE_DAILY                     => "Daily",
            self::SERVICE_TYPE_NOT_IN_SERVICE            => "Not in Service",
            self::SERVICE_TYPE_UNATTENDED_KEY_DELIVERY   => "Unattended Key Delivery",
            self::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY => "Time Window Home Delivery",
            self::SERVICE_TYPE_RECURRING_HOME_DELVIERY   => "Recurring Home Delviery",
            self::SERVICE_TYPE_ON_DEMAND_HOME_DELIVERY   => "On Demand Home Delivery",
        );
    }

    /**
     * Get all homde delivery types
     *
     * @return string[]
     */
    static public function getHomeDeliveryServiceTypes()
    {
        return array(
            self::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY,
            self::SERVICE_TYPE_UNATTENDED_KEY_DELIVERY,
            self::SERVICE_TYPE_RECURRING_HOME_DELVIERY,
            self::SERVICE_TYPE_ON_DEMAND_HOME_DELIVERY
        );
    }

    /**
     * @deprecated
     */
    public function setLocationType()
    {
        $CI = get_instance();
        $sql = "SELECT name as locationTypeName, lockerType FROM locationType
                        WHERE locationTypeID = {$this->locationType_id}";
        $query = $CI->db->query($sql);
        $results = $query->result();
        foreach ($results[0] as $k=>$v) {
                $this->$k = $v;
        }
    }

    /**
     * gets the locationType for a location
     *
     * @return string lockerType
     */
    public function getLocationType()
    {
        $CI = get_instance();
        $sql = "SELECT name, lockerType FROM locationType
        WHERE locationTypeID = {$this->locationType_id}";

        return $CI->db->query($sql)->row()->lockerType;

    }

    /**
     * gets the locationType for a location
     *
     * @return string name
     */
    public function getLocationTypeName()
    {
        $CI = get_instance();
        $sql = "SELECT name, lockerType FROM locationType
        WHERE locationTypeID = {$this->locationType_id}";

        return $CI->db->query($sql)->row()->name;

    }

    /**
     * @deprecated THis function is deprecated. Do not use.
     */
    public function is_multi_order()
    {
        $sql = "SELECT lockerType FROM locationType
                        INNER JOIN location ON locationType_id = locationTypeID
                        WHERE locationID = ".$this->locationID;
        $result = query($sql);
        if($result[0]->lockerType == 'lockers')

                return false;

        return true;
    }


    /**
     * The following function is *. Do not use.
     * @deprecated This functionality belongs in the locker model, not the Droplocker Object namespace.
     * gets all the lockers in this location
     */
    public function lockers()
    {
        $lockers = Locker::search_aprax(array(
            'location_id' => $this->{self::$primary_key},
            "locker.lockerName !=" => "InProcess",
            "lockerStyle_id NOT IN (7, 9)" => null,
            "locker.lockerStatus_id" => 1
        ), false, "locker.lockerName");

        foreach ($lockers as $locker) {
            $lockType = new LockerLockType($locker->lockerLockType_id);
            $lockerStyle = new LockerStyle($locker->lockerStyle_id);
            $lockerStatus = new LockerStatus($locker->lockerStatus_id);
            $properties['lockerID'] = $locker->lockerID;
            $properties['lockerName'] = $locker->lockerName;
            $properties['lockerLockType'] = $lockType->name;
            $properties['lockerStyle'] = $lockerStyle->name;
            $properties['lockerStatus'] = $lockerStatus->name;
            $lockersArray[] = $properties;
        }

        return $lockersArray;
    }


    /**
     * checks to see if there are any active lockers in the locations.
     * @return boolean
     */
    public function isActive()
    {
        $CI = get_instance();
        $sql = "SELECT count(*) as total from locker WHERE location_id = ".$this->{self::$primary_key}."
                and lockerStatus_id = 1";
        $lockers = $CI->db->query($sql)->row();
        if ($lockers->total > 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
    * @deprecated
    * This function is depcreated. Do not use.
    * get the InProcess locker for a location
    *
    * @param int $location_id
    * @return boolean|object false if not found
    */
    public function getInProcessLocker($location_id)
    {
        $CI = get_instance();
        $sql = "SELECT * FROM locker WHERE lockerName = 'InProcess'
                            AND location_id = {$location_id}";
        $query = $CI->db->query($sql);

        if ($query->num_rows()==0) {
            return false;
        }

        $locker = $query->row();

        return $locker;
    }
    /**
     * Validates the location object's properties.
     * @throws Exception
     */
    public function validate()
    {
        $CI = get_instance();
        if (empty($this->city)) {
            throw new Validation_Exception("'city' can not be empty.");
        }
        if (empty($this->address)) {
            throw new Validation_Exception("'address' can not be empty.");
        }
        if (empty($this->locationType_id)) {
            throw new Validation_Exception("'locationType_id' can not be empty.");
        }
        try {
            new LocationType($this->locationType_id);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Location type ID '{$this->locationType_id}' not found in database.");
        }
        if (empty($this->serviceType)) {
            throw new Validation_Exception("'serviceType' can not be empty.");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            new Business($this->business_id);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->businsess_id}' not found in database.");
        }
        $CI->load->model("location_model");
        if (empty($this->{self::$primary_key})) {
            $existing_locations_count = $CI->location_model->count(array("city" => $this->city, "state" => $this->state, "address" => $this->address, "address2" => $this->address2, "business_id" => $this->business_id));
            if ($existing_locations_count > 0) {
                throw new Validation_Exception("There is already an existing location with city '{$this->city}', state '{$this->state}', address '{$this->address}', address2 '{$this->address2}' for business ID {$this->business_id}");
            }
        }
    }

    /**
     * @see \App\Libraries\DroplockerObjects\DroplockerObject::save()
     *
     * If this is an insert we check for the location type and create lockers
     */
    public function save()
    {
        $CI = get_instance();
        parent::save();
        if ($this->{self::$primary_key}) {
             parent::save();
             $locationID = $this->{self::$primary_key};
        } else {
            $locationID = parent::save();
            $location = new Location($locationID);

            $lockerType = $location->getLocationType();
            $locationTypeName = $location->getLocationTypeName();

            if ($lockerType == 'description') {
                $CI->load->model('locker_model');

                // if corner store add a locker called "Front_Counter"
                if ($locationTypeName == 'Corner Store') {
                    $name = "Front_Counter";
                } else {
                    $name = $locationTypeName;
                }
                $CI->locker_model->insert(  array('location_id'=>$locationID,
                'lockerName' => $name,
                "lockerStyle_id" => 8,
                "lockerLockType_id" => 3));

            } elseif ($lockerType == 'lockers') {
                try {
                    Locker::createInProcessLocker($locationID);
                } catch (\Database_Exception $e) {
                    trigger_error("inProcess locker already exists for location {$location->{Location::$primary_key}}", E_USER_NOTICE);
                }

            }
        }

        return $locationID;
    }

}
