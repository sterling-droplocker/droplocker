<?php
namespace App\Libraries\DroplockerObjects;
/**
 * This table defines how the web URLs are accessible for each business.
 * The properties are as follows:
 *  resource string THe name of the resource. In most cases, this is the URL to the controller and action, with the slashes in the URL replaced with underscores.
 *  description A human readable description of the resource
 *  aclgroup string A human readable meaning of what a partciular resource is categorized under
 *  aclgrouporder unint Unknown * property
 *  default_value string Can be 'false' or 'true'
 *  business_id uint Unknown * property. This does not appear to be actually used
 */
class AclResource extends DroplockerObject
{
    public static $table_name = "aclresources";
    public static $primary_key = "id";
    public function validate()
    {
        if (empty($this->resource)) {
            throw new Validation_Exception("'resource' can not be empty");
        }
    }
}
