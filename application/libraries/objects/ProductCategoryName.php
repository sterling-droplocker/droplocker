<?php
namespace App\Libraries\DroplockerObjects;

class ProductCategoryName extends DroplockerObject
{
    public static $table_name = "productCategoryName";
    public static $primary_key = "productCategoryNameID";

    public static $belongsTo = array("ProductCategory");

    public function validate()
    {
        parent::validate();

        $options = array(
            'productCategory_id' => $this->productCategory_id,
            'business_language_id' => $this->business_language_id
        );

        if ($this->productCategoryNameID) {
            $options['productCategoryNameID !='] = $this->productCategoryNameID;
        }

        if (ProductCategoryName::search_aprax($options)) {
            throw new Validation_Exception("('productCategory_id', business_language_id') should be unique");
        }

        return true;
    }

}