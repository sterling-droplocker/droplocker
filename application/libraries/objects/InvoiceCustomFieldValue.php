<?php
namespace App\Libraries\DroplockerObjects;

class InvoiceCustomFieldValue extends DroplockerObject
{

    public static $table_name = "invoiceCustomFieldValue";
    public static $primary_key = "invoiceCustomFieldValueID";

    public static $belongsTo = array("InvoiceCustomField");

    protected $rules = array(
        "invoiceCustomField_id" => array("numeric", "required"),
        "business_language_id" => array("numeric", "required"),
        "title" => array("required"),
        "value" => array("required"),
    );

    public function validate()
    {
        parent::validate();

        $options = array(
            'invoiceCustomField_id' => $this->invoiceCustomField_id,
            'business_language_id' => $this->business_language_id
        );
        if ($this->invoiceCustomFieldValueID) {
            $options['invoiceCustomFieldValueID !='] = $this->invoiceCustomFieldValueID;
        }
        if ($this->search_aprax($options)) {
            throw new Validation_Exception("Invoice and Language should be unique");
        }

        return true;
    }

}