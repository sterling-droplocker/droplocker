<?php

namespace App\Libraries\DroplockerObjects;
class Website extends DroplockerObject
{
    public static $primary_key = "websiteID";
    public static $table_name = "website";

    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new Business($this->business_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found");
        }
    }
}
