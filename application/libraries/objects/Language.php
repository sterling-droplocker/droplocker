<?php
namespace App\Libraries\DroplockerObjects;
class Language extends DroplockerObject
{
    public static $table_name = "language";
    public static $primary_key = "languageID";

    /**
     * This is a convenience method for creating a new Language entity.
     * @param string $tag
     * @param string $description
     * @return \App\Libraries\DroplockerObjects\Language
     */
    public static function create($tag, $description)
    {
        $language = new Language();
        $language->tag = $tag;
        $language->description = $description;
        $language->save();

        return $language;
    }
    public function validate()
    {
        if (empty($this->tag)) {
            throw new Validation_Exception("'tag' can not be empty");
        }
        if (empty($this->description)) {
            throw new Validation_Exception("'description' can not be empty");
        }
    }
}
