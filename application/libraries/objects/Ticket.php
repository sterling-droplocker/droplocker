<?php
namespace App\Libraries\DroplockerObjects;

class Ticket extends DroplockerObject
{
    public static $table_name = "ticket";
    public static $primary_key = "ticketID";


    public function validate()
    {
    }

    /**
     * function that sends all the tickets for a business to the subscribers.
     * Mainly used in the cron
     *
     * @param object $subscriber
     * @param array $status
     * @return int
     */
    public function sendEmail($subscriber, $status)
    {
        $tickets = $this->getTickets($subscriber->business_id, $status);
        $body = $this->formatEmail($tickets);

        $emails = unserialize($subscriber->subscribers);
        $to = '';
        foreach ($emails as $email) {
            if ($email!='' && valid_email($email)) {
                $to .= $email.",";
            }
        }
        $emailID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, rtrim($to, ","), $subscriber->subject, $body, $cc, null, $subscriber->business_id);

        return $emailID;
    }


    public function sendDigest($business_id, $subscriber)
    {
        //get this person's email address
        if ($subscriber == 0) {
            $to = get_business_meta($business_id, 'customerSupport');
        } else {
            $assignee = new Employee($subscriber);
            $to = $assignee->email;
        }

        //send them a dump of their tickers
        $tickets = $this->getEmployeeTickets($business_id, $subscriber);
        $body = $this->formatEmail($tickets);

        if (isset($to)) {
            $emailID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, rtrim($to, ","), 'Your Open Support Tickets ['.date("m/d/y").']', $body, null, null, $business_id);
        }

        return $emailID;
    }



    /**
     * static function for adding a new ticket.
     * There are to many arguments so I left the argument as an array
     *
     * $post values
     * -------------------
     * title
     * description
     * status (open|urgent|closed)
     * dateUpdated dateTime
     * order_id (optional)
     * customer_id (optional)
     * location_id (optional)
     * locker_id (optional)
     * bag (optional)
     * ticketProblem_id (optional)
     *
     * @param int $business_id
     * @param int $employee_id
     * @param array $_POST
     */
    public static function addTicket($business_id, $employee_id, $post)
    {
        $CI = get_instance();

        if (!is_numeric($business_id)) {
            throw new \Exception("Missing business_id");
        }

        $ticket = new \App\Libraries\DroplockerObjects\Ticket();
        $ticket->business_id = $business_id;
        $ticket->order_id = $post['order_id'];
        $ticket->customer_id = $post['customer_id'];
        $ticket->employee_id = $employee_id;
        $ticket->location_id = $post['location_id'];
        $ticket->locker_id = $post['locker_id'];
        $ticket->bag_id = $post['bag'];
        $ticket->title = addslashes($post['title']);
        $ticket->issue = addslashes($post['description']);
        $ticket->dateUpdated = date('Y-m-d H:i:s');
        $ticket->status_id = $post['statusid'];
        $ticket->assignedTo = $post['assignedTo'];
        $insert_id = $ticket->save();

        if ($insert_id) {

            $employee = new Employee($ticket->employee_id);
            $body = "New Ticket has been created by: ".$employee->firstName." ".$employee->lastName."<br></br>";
            $body .= $ticket->title."<br>";
            $body .= $ticket->issue."<br>";
            $body .= "<a href='https://droplocker.com/admin/tickets/edit_ticket/".$insert_id."'>View Ticket</a>";

            //send an email to whomever it was assinged to
            if ($ticket->assignedTo == 0) {
                $to = get_business_meta($business_id, 'customerSupport');
            } else {
                $assignee = new Employee($ticket->assignedTo);
                $to = $assignee->email;
            }
            $emailID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $to, 'New '.$ticket->status.' Support Ticket ['.$insert_id.']', $body);
        }

        return $insert_id;
    }


    public function addTicketNote()
    {
    }

    /**
     * gets tickets for a business
     * @param int $business_id
     * @param array $status
     * @return array of objects
     */
    public function getTickets($business_id = '', $status = array())
    {
        $CI = get_instance();
        if (!empty($status)) {
            $CI->db->where_in('ticket.status', $status);
        }

        if (is_numeric($business_id)) {
            $CI->db->where('ticket.business_id', $business_id);
        }
        $CI->db->select("ticketID,business_id, firstName, lastName, address, lockerName, ticket.status, employee_id, ticket.dateCreated, issue, title, bag_id, locker_id, locationID");
        $CI->db->join('bag', 'bagID = bag_id','left');
        $CI->db->join('locker', 'lockerID = ticket.locker_id','left');
        $CI->db->join('location', 'locationID = ticket.location_id','left');
        $CI->db->join('customer', 'customerID = ticket.customer_id', 'left');
        $query = $CI->db->get(self::$table_name);
        $lastQuery = $CI->db->last_query();

        return $query->result();
    }

    /**
     * gets a ticket
     *
     * @param int $ticket_id
     * @return object
     *
     */
    public static function getTicket($ticket_id)
    {
        $CI = get_instance();

        if (!is_numeric($ticket_id)) {
            throw new \Exception("Missing ticket_id");
        }

        $CI->db->where('ticketID', $ticket_id);
        $CI->db->select("ticketID,ticket.business_id, firstName, lastName, customerID, order_id, address, lockerName, ticket.ticketstatus_id,ticket.priority, employee_id, ticket.dateCreated, issue, title, bagNumber, locker_id, locationID, nextActionDate,customerEmail,customerName,customerPhone,ticket.dateUpdated,ticket.dateCreated,ticket.customer_id, ticket.ticketType_id");
        $CI->db->join('bag', 'bagID = bag_id','left');
        $CI->db->join('locker', 'lockerID = ticket.locker_id','left');
        $CI->db->join('location', 'locationID = ticket.location_id','left');
        $CI->db->join('customer', 'customerID = ticket.customer_id', 'left');
        $query = $CI->db->get(self::$table_name);
        $lastQuery = $CI->db->last_query();

        return $query->row();
    }

        /**
     * gets all tickets for this employee in open status
     *
     * @param int $business_id
     * @param int $employee_id
     * @return object
     *
     */
    public static function getEmployeeTickets($business_id, $employee_id)
    {
        $CI = get_instance();
        $CI->db->select("ticketID,firstName, ticketProblem.name as problemName, lastName, customerID, order_id, address, lockerName, ticket.ticketStatus_id, employee_id, ticket.dateCreated, issue, title, bag_id, locker_id, locationID, ticketProblem_id, assignedTo, ticketStatus.description as ticketStatusDescription");
        $CI->db->where('ticket.business_id', $business_id);
        $CI->db->where('ticket.assignedTo', $employee_id);
        $CI->db->where('ticketStatus.description !=', 'Closed');
  
        $CI->db->join('bag', 'bagID = bag_id','left');
        $CI->db->join('locker', 'lockerID = ticket.locker_id','left');
        $CI->db->join('location', 'locationID = ticket.location_id','left');
        $CI->db->join('customer', 'customerID = ticket.customer_id', 'left');
        $CI->db->join('ticketProblem', 'ticketProblemID = ticket.ticketProblem_id', 'left');
        $CI->db->join('ticketStatus', 'ticketStatusID = ticket.ticketStatus_id');
       
        $query = $CI->db->get(self::$table_name);
        //$lastQuery = $CI->db->last_query();

        return $query->result();
    }

    /**
     * Adds a note to a ticket
     *
     * @param int $ticket_id
     * @param int $employee_id
     * @param string $description
     * @param string $status
     * @param string $systemNotes
     * @return int
     */
    public static function addNote($ticket_id, $employee_id, $description, $status, $systemNotes = '',$noteType="Internal",$actionTime='')
    {
        $CI = get_instance();

        if (!is_numeric($ticket_id)) {
            throw new \Exception("Missing ticket_id");
        }

        $options['ticket_id'] = $ticket_id;
        $options['description'] = $description;
        $options['employee_id'] = $employee_id;
        $options['status'] = $status;
        $options['expectedDate'] = $actionTime;
        $options['systemNotes'] = $systemNotes;
        $options['noteType'] = $noteType;
        $insert_id = $CI->db->insert('ticketNote', $options);

        return $insert_id;
    }

    /**
     * formats the email
     *
     * @param array $tickets
     * @return string
     */
    public function formatEmail($tickets)
    {
        //put together the body for email
        $body = "<h2>Open Tickets</h2><br>";
        if ($tickets) {
            foreach ($tickets as $t) {
                $body .= "<div style='padding:5px; margin:5px; width:100%; border:1px solid #eaeaea'>".$t->ticketStatusDescription." <a href='https://droplocker.com/admin/tickets/edit_ticket/".$t->ticketID."'>".$t->title."</a><br/>".$t->issue."</div>";
            }
        } else {
            $body .= "<br><br>There are no open tickets";
        }

        return $body;
    }

    /**
     *
     */
    public function getFullDetails($options = array())
    {
        if (isset($options['ticket.business_id'])) {
            $this->db->where('ticket.business_id',$options['ticket.business_id']);
        }
        if (isset($options['ticketID'])) {
            $this->db->where('ticketID', $options['ticketID']);
        }
        if (isset($options['status'])) {
            $this->db->where('ticket.status', $options['status']);
        }
        if (isset($options['where_in_status'])) {
            $this->db->where_in('ticket.status', $options['where_in_status']);
        }
        if (isset($options['select'])) {
            $this->db->select($options['select']);
        }
        if (isset($options['order_by'])) {
            $this->db->order_by($options['order_by'], 'desc');
        }

        $this->db->join('bag', 'bagID = bag_id','left');
        $this->db->join('locker', 'lockerID = ticket.locker_id','left');
        $this->db->join('location', 'locationID = ticket.location_id','left');
        $this->db->join('customer', 'customerID = ticket.customer_id', 'left');


        $query = $this->db->get('ticket');

        return $query->result();
    }


}
