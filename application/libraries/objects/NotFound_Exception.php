<?php
namespace App\Libraries\DroplockerObjects;
/**
 * This exception should be thrown by Droplocker objects for the case where an attempt at instnatiating an object is made with
an ID that does not exist in the database.
 */
class NotFound_Exception extends \Exception
{
    /**
     * Determines if the primary key value exists in a database table
     * @param int $id
     * @param string $class The Droplocker Object class
     * @return boolean Returns true if the primary key value exists
     * @throws NotFound_Exception If the primary key value does not exist in the table, then this exceptioon is thrown
     */
    public static function does_exist($id, $droplockerObject_class)
    {
        $CI = get_instance();

        $class = sprintf("\App\Libraries\DroplockerObjects\%s", $droplockerObject_class);

        if (!is_numeric($id)) {
            throw new \InvalidArgumentException("'id' must be numeric");
        }
        $CI->db->where($class::$primary_key, $id);
        $result = $CI->db->get($class::$table_name);
        if (empty($result)) {
            throw new NotFound_Exception("{$class::$table_name} ID $id not found");
        } else {
            return true;
        }
    }
}
