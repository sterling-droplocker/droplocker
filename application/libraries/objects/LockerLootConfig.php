<?php
namespace App\Libraries\DroplockerObjects;

class LockerLootConfig extends DroplockerObject
{
    public static $table_name = "lockerLootConfig";
    public static $primary_key = "lockerLootConfigID";


    public function validate()
    {
       if (!is_numeric($this->percentPerOrder)) {
           throw new Validation_Exception("'percentPerOrder' must be numeric.");
       }
       if (!is_numeric($this->redeemPercent)) {
           throw new Validation_Exception("'redeemPercent' must be numeric.");
       }
       if (!is_numeric($this->minRedeemAmount)) {
           throw new Validation_Exception("'minRedeemAmount' must be numeric.");
       }
       if (!is_numeric($this->referralAmountPerReward)) {
           throw new Validation_Exception("'referralAmountPerReward' must be numeric.");
       }
       if (!is_numeric($this->referralNumberOrders)) {
           throw new Validation_Exception("'referralNumberOfOrders' must be numeric");
       }
       if (!is_numeric($this->referralOrderAmount)) {
           throw new Validation_Exception("'referralOrderAmount' must be numeric.");
       }
       if (empty($this->programName)) {
           throw new Validation_Exception("'programName' can not be empty.");
       }
       if (!is_numeric($this->business_id)) {
           throw new Validation_Exception("'business_id' must be numeric");
       }
       try {
           new Business($this->business_id);
       } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
           throw new Validation_Exception("Business ID '{$this->business_id} does not exist");
       }
    }

    /**
     * static function that checks for all configuration settings in order to enable locker loot for a business
     *
     * @param int $business_id
     * @return boolean
     */
    public static function isActive($business_id)
    {
        $CI = get_instance();

        $config = $CI->db->query("SELECT * FROM lockerLootConfig WHERE business_id = {$business_id}")->row();

        if (empty($config)) {
            return false;
        } elseif ($config->lockerLootStatus != "active") {
            return false;
        }

        return true;
    }

    public static function getName($business_id)
    {
        $CI = get_instance();

        $name = $CI->db->query("SELECT programName FROM lockerLootConfig WHERE business_id = {$business_id}")->row();

        return $name->programName;
    }


}
