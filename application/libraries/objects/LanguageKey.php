<?php
namespace App\Libraries\DroplockerObjects;
class LanguageKey extends DroplockerObject
{
    public static $table_name = "languageKey";
    public static $primary_key = "languageKeyID";
    public static $belongsTo = array("LanguageView");

    public function validate()
    {
        $CI = get_instance();
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }

        if (empty($this->defaultText)) {
            throw new Validation_Exception("'defaultText' can not be empty");
        }
        if (is_numeric($this->languageView_id)) {
            Validation_Exception::verify_entity_exists($this->languageView_id, "LanguageView");
        } else {
            throw new Validation_Exception("'languageView_id' can not be empty");
        }

        $CI->load->model("languageKey_model");
        $existing_language_key_count = $CI->languageKey_model->count(array(
            "name" => $this->name,
            "languageView_id" => (int) $this->languageView_id,
            "languageKeyID !=" => (int) $this->languageKeyID,
        ));

        if ($existing_language_key_count > 0) {
            throw new Validation_Exception("A language key with the name '{$this->name}' in the same view '{$this->languageView_id}' already exists");
        }

    }
    public function validate_delete()
    {
        parent::validate_delete();

        $CI = get_instance();
        $CI->load->model("languageKey_business_language_model");
        $languageKeys_count = $CI->languageKey_business_language_model->count(array("languageKey_id" => $this->{self::$primary_key}));
        if ($languageKeys_count > 0) {
            throw new Validation_Exception("Can not delete '{$this->name}' ({$this->{self::$primary_key}}) because there are $languageKeys_count business languages keys associated with this language key");
        }
    }
}
