<?php
namespace App\Libraries\DroplockerObjects;

class Upcharge extends DroplockerObject
{
    public static $table_name = "upcharge";
    public static $primary_key = "upchargeID";


    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }

        if (!is_numeric($this->upchargePrice)) {
            throw new Validation_Exception("'upchargePrice' must be numeric.");
        }

        if (!is_numeric($this->upchargeGroup_id)) {
            throw new Validation_Exception("'upchargeGroup_id' must be numeric.");
        }

    }

    /**
     * This funciton is *, do not use or I murder you in the face
     * --------------------------------------
     * adds a new upcharge with an upcharge group and inserts in the upcharge Supplier table
     * @deprecated This functionality does not follow Droplocker Object coding standards
     * @param int $upchargeGroup_id
     * @param String $name
     * @param float $upchargePrice
     * @param int $supplier_id
     * @param float $cost
     * @param int $everyOrder
     * @return int insert_id
     */
    public function addUpcharge($upchargeGroup_id, $name, $upchargePrice, $supplier_id, $cost)
    {
        $CI = get_instance();
        $sql = "INSERT IGNORE INTO upcharge (upchargeGroup_id, name, upchargePrice, everyOrder, priceType) VALUES ('".mysql_real_escape_string($upchargeGroup_id)."', '".mysql_real_escape_string($name)."', '".mysql_real_escape_string($upchargePrice)."', '".mysql_real_escape_string($everyOrder)."', '".mysql_real_escape_string($priceType)."')";
        $CI->db->query($sql);

        if ($insert_id = $CI->db->insert_id()) {
            $sql = "INSERT INTO upchargeSupplier (supplier_id, cost, `default`, upcharge_id) VALUES (".mysql_real_escape_string($supplier_id).", '".mysql_real_escape_string($cost)."', 1, ".$insert_id.")";
            $CI->db->query($sql);
        }

        return $insert_id;
    }


    /**
     * @deprecated This does not belong in the Droplocker objects
     * gets the default supplier for an upcharge
     *
     * @return Object UpchargeSupplier
     */
    public function getDefaultSupplier()
    {
        $suppliers = \App\Libraries\DroplockerObjects\UpchargeSupplier::search_aprax(array('upcharge_id'=>$this->{self::$primary_key}, 'default'=>1));

        return $suppliers[0];
    }

    /**
     * @deprecated This does not belong in the Droplocker ojbects.
     *
     * This will get all the suppliers and the costs
     *
     * This returns all suppliers, even if they are not in the upcharge Table
     *
     * $return Supplier Values
     * ----------------------
     * supplier_id
     * companyName
     * cost
     * default
     *
     */
    public function getSuppliers()
    {
        $CI = get_instance();
        $sql = "SELECT business_id FROM upchargeGroup
                WHERE upchargeGroupID = ".$this->upchargeGroup_id;
        $row = $CI->db->query($sql)->row();

        $suppliers = \App\Libraries\DroplockerObjects\Supplier::getBusinessSuppliers($row->business_id);
        foreach ($suppliers as $supplier) {
            $upchargeSupplier = \App\Libraries\DroplockerObjects\UpchargeSupplier::search_aprax(array('upcharge_id'=>$this->{self::$primary_key}, 'supplier_id'=>$supplier->supplierID));
            $returnSupplier['supplier_id'] = $supplier->supplierID;
            $returnSupplier['companyName'] = $supplier->companyName;
            $returnSupplier['cost'] = isset($upchargeSupplier[0]->cost)?$upchargeSupplier[0]->cost:0;
            $returnSupplier['default'] = isset($upchargeSupplier[0]->default)?$upchargeSupplier[0]->default:0;

            $returnSuppliers[] = $returnSupplier;
        }

        return $returnSuppliers;
    }

    /**
     * deletes the upcharge and the upcharge supplier
     */
    public function delete()
    {
        if ($deletedRows = parent::delete()) {

            $suppliers = \App\Libraries\DroplockerObjects\UpchargeSupplier::search_aprax(array('upcharge_id'=>$this->{self::$primary_key}));
            if ($suppliers) {
                foreach ($suppliers as $supplier) {
                     $supplier->delete();
                }
            }

        }

        return $deletedRows;
    }



}
