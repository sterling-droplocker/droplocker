<?php
namespace App\Libraries\DroplockerObjects;
class OrderPaymentStatusOption extends DroplockerObject
{
    public static $table_name = "orderPaymentStatusOption";
    public static $primary_key = "orderPaymentStatusOptionID";

    public function validate()
    {
        if (!$this->name) {
            throw new Validation_Exception("'name' can not be empty.");
        }
    }
}
