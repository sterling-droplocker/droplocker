<?php


namespace App\Libraries\DroplockerObjects;

class Delivery_CustomerSubscription extends DroplockerObject
{
    public static $table_name = "delivery_customerSubscription";
    public static $primary_key = "delivery_customerSubscriptionID";
}
