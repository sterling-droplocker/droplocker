<?php
namespace App\Libraries\DroplockerObjects;

class LockerLoot extends DroplockerObject
{
    public static $table_name = "lockerLoot";
    public static $primary_key = "lockerLootID";

    private $lockerLootConfig;
    private $customer;
    private $order;
    private $errorMessage;

    private $lockerLootID;
    private $orderTotal;
    private $referralParent;
    private $customerTotalOrdersPlaced;


    /**
     * applies locker loot to a customer
     * Main process for giving a customer a reward
     */
    public function applyReward()
    {
        // applys a reward to the customer that placed the order
        $this->lockerLootID = $this->insertLockerLoot($this->order->orderID, $this->points, $this->customer->customerID, $this->lockerLootConfig->percentPerOrder . "% Rebate" );

        // set the referral parent for the customer
        $this->setReferralParent();

        // set the total orders this customer has placed with the referral parent customer
        $this->setCustomerTotalOrdersPlaced();
        if ($this->customerTotalOrdersPlaced < $this->lockerLootConfig->referralNumberOrders && $this->referralParent && $this->orderTotal >= $this->lockerLootConfig->referralOrderAmount) {

            $lockerLootID = $this->applyReferralParentRewards();

            $action_array = array('do_action' => 'locker_loot_reward', 'order_id'=>$this->order->orderID, 'lockerLoot_id'=>$lockerLootID, 'customer_id'=>$this->referralParent, 'business_id'=>$this->order->business_id);
            \Email_Actions::send_transaction_emails($action_array);
        }

        return $this->lockerLootID;

    }



    /**
     * sets all the objects needed for lockerLoot
     * This function also verifies that a customer and order are valid for locker loot (ie. orderTotal > 0)
     *
     * ErrorMessages:
     * ------------
     * - LockerLoot for this business is not active
     * - Cannot apply lockerloot to a wholesale customer
     * - Customer has already received reward for this order
     * - The order total is 0
     * - Points are zero - cannot apply lockerloot to this order
     *
     * @param object $lockerLootConfig
     */
    public function setLockerLootConfig($lockerLootConfig, $customer, $order)
    {
        // reset the error array in case we are in a loop
        $this->errorMessage = array();

        //$this->lockerLootConfig = $this->validateLockerLootConfig($lockerLootConfig);
        $this->lockerLootConfig = $lockerLootConfig;
        try {
            $this->customer = $this->validateCustomer($customer);
            $this->order = $this->validateOrder($order);
        
        } catch (Exception $e) {
            $this ->errorMessage[] = 
                "Exception (" . $e->getMessage() . ") caught while applying lockerloot to order: " . $order->orderID . " order.customerID = " . $order->customer_id;
        }

        if ($this->lockerLootConfig->lockerLootStatus != 'active') {
            $this->errorMessage[] = "LockerLoot for this business is not active";
        }

        if ($customer->type == 'wholesale') {
            $this->errorMessage[] = "Cannot apply lockerloot to a wholesale customer";
        }

        $this->setOrderTotal();
        if ($this->orderTotal <= 0) {
            $this->errorMessage[] = "The order total is 0";
        }

        $this->calculatePoints();
//        if ($this->points <= 0) {
//            $this->errorMessage[] = "Points are zero - cannot apply lockerloot to this order";
//        }

        $this->deleteLockerLoot();

        // has customer already received rewards?
        // if ($this->hasAlreadyReceivedRewardForOrder()) {
            //$this->errorMessage[] = "Customer has already received reward for this order";
        //}

    }


    /**
     * verifies that we have a proper customer object
     * We only check for customerID and type (no discounts to wholesale customers)
     *
     * @param object $customer
     * @throws \Exception
     * @return object
     */
    public function validateCustomer($customer)
    {
        if (!is_numeric($customer->customerID)) {
            throw new \Exception("Customer object is missing customerID. " . print_r($customer, true));
        }

        if (empty($customer->type)) {
            throw new \Exception("Customer object is missing customer type");
        }

        return $customer;
    }


    /**
     * verifies that we have a valid order object
     * We only require the orderID
     *
     * @param object $order
     * @throws \Exception
     * @return object
     */
    public function validateOrder($order)
    {
        if (!is_numeric($order->orderID)) {

            throw new \Exception("Order object is missing orderID");
        }

        return $order;
    }

    /**
     * sets the private variable for orderTotal
     *
     * @uses orderitem_model->get_item_total($order_id)
     * @return float
     */
    public function setOrderTotal()
    {
        if (empty($this->order->orderID)) {
            throw new \Exception("Missing orderID");
        }

        $CI = get_instance();
        $CI->load->model('orderitem_model');
        $total = $CI->orderitem_model->get_item_total($this->order->orderID);

        $this->orderTotal = $total['total'];

        return $this->orderTotal;

    }

    /**
    * gets the locker loot points for an order
    * I added 2 optional arguments so i could test this function without having to setup this class.
    * If the arguments are not passed, I use the private variables for this class
    *
    * @param float $orderTotal
    * @return float
    */
    public function calculatePoints($orderTotal = 0, $percentPerOrder = 0)
    {
        $orderTotal = ($orderTotal==0)?$this->orderTotal:$orderTotal;
        $percentPerOrder = ($percentPerOrder==0)?$this->lockerLootConfig->percentPerOrder:$percentPerOrder;

        if ($orderTotal <= 0) {
            $this->points = 0;

            return 0;
        }

        $this->points = $orderTotal * ($percentPerOrder / 100);

        return $this->points;
    }


    /**
     * checks to see if the customer has already received rewards for this order
     *
     * @return boolean
     */
    public function hasAlreadyReceivedRewardForOrder()
    {
        $CI = get_instance();
        $sql = "select count(*) as totalFound from lockerLoot where order_id = {$this->order->orderID} and customer_id = '{$this->customer->customerID}'";
        $result = $CI->db->query($sql)->row();
        if ($result->totalFound > 0) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * delete rows in the lockerLoot table based on order_id
     */
    public function deleteLockerLoot()
    {
        $CI = get_instance();
        $sql = "delete from lockerLoot where order_id = {$this->order->orderID}";
        $CI->db->query($sql);
    }


    /**
     * sets the private variable 'customerTotalOrdersPlaced' the number of referral rewards already applied for the customer
     *
     * @return boolean FALSE if the referralParent is not set
     */
    public function setCustomerTotalOrdersPlaced()
    {
        $CI = get_instance();

        if (empty($this->referralParent)) {
            return false;
        }

        if (empty($this->customer->customerID)) {
            throw new \Exception("missing customerID");
        }

        $sql = "SELECT orders.customer_id, count(orders.customer_id) as totalOrders
                FROM lockerLoot
                JOIN orders on orderID = lockerLoot.order_id
                WHERE lockerLoot.customer_id = ?
                AND orders.customer_id = ?";

        $result = $CI->db->query($sql, array($this->referralParent, $this->customer->customerID))->row();

        $this->customerTotalOrdersPlaced = isset($result->totalOrders)?$result->totalOrders:0;
    }



    /**
     * sets the private variable for referralParent -  the customer that referred the customer for this order
     *
     * @return NULL|int - will return null if the customer is an employee, else it will return the customer_id
     */
    public function setReferralParent()
    {
        $CI = get_instance();
        if (empty($this->customer->customerID)) {
            throw new \Exception("missing customerID");
        }

        $sql = "SELECT customer.referredBy as customer_id, refer.type
        FROM customer
        join customer as refer on refer.customerID = customer.referredBy
        WHERE customer.customerID = {$this->customer->customerID};";
        $referrer = $CI->db->query($sql)->row();

        if (empty($referrer->customer_id)) {
            return null;
        }
        // no discounts to employees

        if (isset($referrer->type) && ($referrer->type == 'delivery' || $referrer->type == 'employee')) {
            return null;
        }

        $this->referralParent = $referrer->customer_id;

    }

    /**
     * inserts a row into the lockerloot table for the customer and the order
     *
     * @return int insert_id()
     */
    public function insertLockerLoot($order_id, $points, $customer_id, $description)
    {
        $CI = get_instance();

        /*
         * NOTE: I tried to use the lockerLoot object, but points were always null???
         */
        $insert = "INSERT into lockerLoot (order_id, points, customer_id, description)
        VALUES (?, ?, ?, ?);";
        $CI->db->query($insert, array($order_id, $points, $customer_id, $description));

        return $CI->db->insert_id();
    }

    /**
     * inserts a row into the lockerloot table for the customer's referral parent and the order
     *
     * @return int insert_id()
     */
    public function applyReferralParentRewards()
    {
//        if (empty($this->points)) {
//            throw new \Exception('points are empty');
//        }

        $description = "$".$this->lockerLootConfig->referralAmountPerReward." per first ".$this->lockerLootConfig->referralNumberOrders." orders referral";

        return $this->insertLockerLoot($this->order->orderID, $this->lockerLootConfig->referralAmountPerReward, $this->referralParent, $description);

    }

    /**
     * If there are any error messages, this function will return them as a string
     * If the errormessages are in an array I convert them to a string
     *
     * @return string|NULL
     */
    public function _error_message()
    {
        if (!empty($this->errorMessage)) {
            if (is_array($this->errorMessage)) {
                $str = '';
                foreach ($this->errorMessage as $error) {
                    $str .= $error.", ";
                }

                return rtrim($str, ", ");
            } else {
                return $this->errorMessage;
            }
        }

        return null;
    }


    public function validate()
    {
       if (!is_numeric($this->customer_id)) {
           throw new Validation_Exception("'customer_id' must be numeric.");
       }
       try {
           new Customer($this->customer_id, false);
       } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
           throw new Validation_Exception("Customer ID {$this->customer_id} not found in database.");
       }

    }

}
