<?php
namespace App\Libraries\DroplockerObjects;

class AuthorizationLog extends DroplockerObject
{
    public static $table_name = "authorizationLog";
    public static $primary_key = "authorizationLogID";
    
    /**
     * The following override prevents any authorization logs from actualy being set in the database.
     * @param array $properties
     * @return boolean
     */
    public static function create($properties) 
    {
        return false;
    }
}
