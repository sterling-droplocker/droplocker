<?php

namespace App\Libraries\DroplockerObjects;

class App extends DroplockerObject
{
    public static $table_name = "app";
    public static $primary_key = "appID";

    protected $rules = array(
        "business_id" => array("required", "entity_must_exist"),
        "description" => array("required"),
        "keywords"    => array("required"),
        "support_url" => array("required"),
        "name"        => array("required"),
    );
}
