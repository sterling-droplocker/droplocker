<?php
namespace App\Libraries\DroplockerObjects;
/**
 * The properties are defined as follows
 *  aclRole_id uint a foreign key reference to an ACL Role in the aclroles table
 */
class Business_Employee extends DroplockerObject
{
    public static $table_name = "business_employee";
    public static $primary_key = "ID";
    public static $has_one = array("Employee");
    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }

        try {
            new Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found in database.");
        }

        if (!is_numeric($this->employee_id)) {
            throw new Validation_Exception("'employee_id' must be numeric.");
        }

        try {
            new Employee($this->employee_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Employee ID '{$this->employee_id}' not found in database.");
        }
    }
}
