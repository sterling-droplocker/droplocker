<?php
namespace App\Libraries\DroplockerObjects;

class CustomOptionName extends DroplockerObject
{

    public static $table_name = "customOptionName";
    public static $primary_key = "customOptionNameID";
    protected $rules = array(
        "customOption_id" => array("numeric", "required", "entity_must_exist"),
        "title" => array("required"),
    );

    public static $belongsTo = array("CustomOption");

    public function validate()
    {
        parent::validate();

        $options = array(
            'customOption_id' => $this->customOption_id,
            'business_language_id' => $this->business_language_id
        );
        if ($this->customOptionNameID) {
            $options['customOptionNameID !='] = $this->customOptionNameID;
        }
        if (CustomOptionName::search_aprax($options)) {
            throw new Validation_Exception("('customOption_id', business_language_id') should be unique");
        }

        return true;
    }

}
