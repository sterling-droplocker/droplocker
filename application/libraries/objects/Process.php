<?php
namespace App\Libraries\DroplockerObjects;
class Process extends DroplockerObject
{
    public static $table_name = "process";
    public static $primary_key = "processID";


    /**
        * get the name of a product from the product process
        *
        * @param int $product_process_id
        * @return array product name and product displayName
        */
    public static function getName($product_process_id)
    {
        if (!is_numeric($product_process_id)) {
            throw new Exception("'product_process_id' must be numeric.");
        }
        $CI = get_instance();

        $sql = "SELECT name, slug FROM process
                        INNER JOIN product_process pp ON pp.process_id = processID
                        WHERE product_processID = {$product_process_id}";
        $query = $CI->db->query($sql);
        $process = $query->row();

        return array('name'=>$process->name, 'slug'=>$process->slug);
    }
    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
        if (empty($this->slug)) {
            throw new Validation_Exception("'slug' can not be empty.");
        }
    }
}
