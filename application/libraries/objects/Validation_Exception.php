<?php
namespace App\Libraries\DroplockerObjects;
/**
 * This exception sould be thrown in the 'validate' function of each Droplocker object when a validation rule fails.
 *
 * @author ariklevy
 */
class Validation_Exception extends \Exception
{
    private $userMessage; //This is a custom message intended for displaying a friendly message to the user
    public function __construct($message, $userMessage = NULL, $code = NULL, $previous = NULL)
    {
        $this->userMessage = $userMessage;
        parent::__construct($message, $code, $previous);
    }
    public function getUserMessage() {
        return $this->userMessage;
    }
    /**
     * The following function is a short hand function for throwing a validation exception if attempting to set a property to the primary key of another entity
     * @param int $primary_key
     * @param int $class_name a valid Droplocker Object Class
     * @throws Validation_Exception
     */
    public static function verify_entity_exists($primary_key, $class_name)
    {
        try {
            \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($primary_key, $class_name);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception($e->getMessage());
        }
    }
}
