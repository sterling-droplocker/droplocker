<?php
namespace App\Libraries\DroplockerObjects;

class UpchargeSupplier extends DroplockerObject
{
    public static $table_name = "upchargeSupplier";
    public static $primary_key = "upchargeSupplierID";


    public function validate()
    {

    }

    public static function deleteUpchargeSuppliers($upcharge_id)
    {
        $CI = get_instance();

        $sql = "DELETE FROM upchargeSupplier WHERE upcharge_id = {$upcharge_id}";
        $CI->db->query($sql);
    }

}
