<?php
namespace App\Libraries\DroplockerObjects;
class LockerLockType extends DroplockerObject
{
    const KEY = 1;
    const ELECTRONIC = 2;
    const NONE = 3;

    public static $table_name = "lockerLockType";
    public static $primary_key = "lockerLockTypeID";

    public function validate()
    {
        throw new \Exception("Not Implemented.");
    }
}
