<?php

namespace App\Libraries\DroplockerObjects;

class Module extends DroplockerObject
{
    public static $table_name = "module";
    public static $primary_key = "moduleID";
    /**
     * Retrieves the categories for the specified business for the current module.
     * @param int $business_id
     * @return \App\Libraries\DroplockerObjects\Category
     * @throws \InvalidArgumentException
     * @throws \Database_Exception
     */
    public function get_categories($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException($business_id);
        }
        $CI = get_instance();
        $CI->db->select("productCategory_id");
        $CI->db->group_by("productCategory_id");
        $CI->db->where("business_id", $business_id);
        $get_categories_for_module_query = $CI->db->get(self::$table_name);
        if ($CI->db->_error_message()) {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $categories = $get_categories_for_module_query->result();

        $result = array();
        foreach ($categories as $category) {
            $result[] = new Category($category->productCategory_id);
        }

        return $result;
    }
    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
    }
}
