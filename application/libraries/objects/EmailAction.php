<?php
namespace App\Libraries\DroplockerObjects;

class EmailAction extends DroplockerObject
{
    public static $table_name = "emailActions"; //Note, * incorrectly named the table a plural noun instead of a singular noun.
    public static $primary_key = "emailActionID";
    public static $foreign_key = "emailAction_id";
    public function validate()
    {
        if (empty($this->action)) {
            throw new Validation_Exception("'action' can not be empty.");
        }
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
        if (empty($this->description)) {
            throw new Validation_Exception("'description' ca not be empty.");
        }
    }
}
