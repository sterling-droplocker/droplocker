<?php

namespace App\Libraries\DroplockerObjects;

class Product_Supplier extends DroplockerObject
{
    public static $table_name = "product_supplier";
    public static $primary_key = "product_supplierID";


    public function validate()
    {
        if (!is_numeric($this->product_process_id)) {
            throw new Validation_Exception("'product_process_id' must be numeric.");
        }
        try {
            new App\Libraries\DroplockerObjects\Product_Process($this->product_process_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Product Process ID '{$this->product_process_id}' does not exist.");
        }
        if (!is_numeric($this->supplier_id)) {
            throw new Validation_Exception("'supplier_id' must be numeric.");
        }
        try {
            new App\Libraries\DroplockerObjects\Supplier($this->supplier_id);

        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Supplier ID '{$this->supplier_id}' does not exist..");
        }

    }
}
