<?php
namespace App\Libraries\DroplockerObjects;
/**
 * The properties are defined as follows
 *  type_id uint A foreign key defined The ACL role defined in the aclRoles table. Note, this column name does not conform to coding standards.
 *  type string Can be 'role' or 'user'
 *  resource_id uint A foreign key to an ACL Resource defined in the aclresource table
 *  action string Can be 'allow' or 'deny'
 */
class Acl extends DroplockerObject
{
    public static $table_name = "acl";
    public static $primary_key = "id"; //Note, the name of the primary key does not conform to coding standards.
    public function validate()
    {
        if (!is_numeric($this->type_id)) {
            throw new Validation_Exception("'type_id' must be numeric");
        }
        try {
            $aclrole = new AclRoles($this->type_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("'type_id' {$this->type_id} not found in aclroles table");
        }
        if ($aclrole->business_id != $this->business_id) {
            throw new Validation_Exception("The acl role does not belong to the same business");
        }
        if (!is_numeric($this->resource_id)) {
            throw new Validation_Exception("'resource_id' must be numeric.");
        }
        try {
            $aclResource = new AclResource($this->resource_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("'resource_id' {$this->resource_id} not found in aclresource table");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            new Business($this->business_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found in database");
        }
    }
}
