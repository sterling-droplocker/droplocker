<?php
namespace App\Libraries\DroplockerObjects;

class StarchOnShirts extends DroplockerObject
{
    public static $table_name = "starchOnShirts";
    public static $primary_key = "starchOnShirtsID";

    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }
    }
}
