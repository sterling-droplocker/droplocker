<?php
namespace App\Libraries\DroplockerObjects;

class CustomOption extends DroplockerObject
{

    public static $table_name = "customOption";
    public static $primary_key = "customOptionID";
    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "name" => array("required"),
    );

    public static $has_many = array("CustomOptionValue");

    /**
     * Stores current order to detect changes
     *
     * @var int
     */
    protected $currentOrder = null;

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
        if ($id) {
            $this->currentOrder = $this->currentOrder;
        }
    }

    public function validate()
    {
        parent::validate();

        $options = array(
            'name' => $this->name,
            'business_id' => $this->business_id
        );
        if ($this->customOptionID) {
            $options['customOptionID !='] = $this->customOptionID;
        }
        if (CustomOption::search_aprax($options)) {
            throw new Validation_Exception("'name' should be unique");
        }

        return true;
    }

    protected function beforeSave()
    {
        $CI = get_instance();
        $CI->load->model('customoption_model');

        $max_sortOrder = $CI->customoption_model->getMaxSortOrder($this->business_id);

        // If the sortOrder is no set or greater than the next sortOrder, use the next sortOrder
        if (empty($this->sortOrder) || $this->sortOrder > ($max_sortOrder + 1)) {
            $this->sortOrder = $max_sortOrder + 1;
        }

        // if updating, sortOrder can't be greater than max_sortOrder
        if ($this->sortOrder > $max_sortOrder && $this->customOptionID) {
            $this->sortOrder = $max_sortOrder;
        }

        return true;
    }

    protected function afterSave($created)
    {
        $CI = get_instance();

        if ($created || $this->currentOrder != $this->currentOrder) {
            $CI->load->model('customoption_model');
            $CI->customoption_model->updateSortOrder($this->business_id, $this->customOptionID, $this->sortOrder, $this->currentOrder);
        }
    }

    protected function afterDelete()
    {
        $CI = get_instance();

        // fix sort order in options
        $CI->load->model('customoption_model');
        $CI->customoption_model->updateSortOrder($this->business_id, $this->customOptionID, 0, $this->sortOrder);

        // delete my translations
        $CI->load->model('customoptionname_model');
        $CI->customoptionname_model->delete_aprax(array(
            'customOption_id' => $this->customOptionID
        ));

        // delete translations for my values
        $CI->load->model('customoptionvaluename_model');
        foreach ($this->getValues() as $value) {
            $CI->customoptionvaluename_model->delete_aprax(array(
                'customOptionValue_id' => $value->customOptionValueID
            ));
        }

        // delete my values
        $CI->load->model('customoptionvalue_model');
        $CI->customoptionvalue_model->delete_aprax(array(
            'customOption_id' => $this->customOptionID
        ));
    }

    public function getValues()
    {
        return CustomOptionValue::search_aprax(array(
            'customOption_id' => $this->customOptionID
        ));
    }

    public function getOptionNames()
    {
        $out = array();
        $found = CustomOptionName::search_aprax(array(
            'customOption_id' => $this->customOptionID
        ));

        foreach ($found as $CustomOptionName) {
            $out[$CustomOptionName->business_language_id] = $CustomOptionName;
        }

        return $out;
    }

    public function saveOptionNames($names)
    {
        $optionNames = $this->getOptionNames();
        $out = array();
        foreach ($names as $businesLanguageID => $title) {
            if (isset($optionNames[$businesLanguageID])) {
                $optionName = $optionNames[$businesLanguageID];
            } else {
                $optionName = new CustomOptionName();
                $optionName->customOption_id = $this->customOptionID;
                $optionName->business_language_id = $businesLanguageID;
            }
            $optionName->title = $title;
            $optionName->save();

            $out[$businesLanguageID] = $optionName;
        }

        return $out;
    }

}
