<?php
namespace App\Libraries\DroplockerObjects;
class ApiSession extends DroplockerObject
{
    public static $table_name = "apiSession";
    public static $primary_key = "apiSessionID";

    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric");
        }
        try {
            NotFound_Exception::does_exist($this->customer_id, "Customer");
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Customer ID '{$this->customer_id}' not found");
        }

        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            NotFound_Exception::does_exist($this->business_id, "Business");
        } catch (NotFound_Exception $e) {
            throw new Valdiation_Exception("Business ID '{$this->business_id}' not found");
        }
        if (empty($this->token)) {
            throw new Validation_Exception("'token' can not be empty");
        }
    }
}
