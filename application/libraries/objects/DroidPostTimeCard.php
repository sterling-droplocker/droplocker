<?php
namespace App\Libraries\DroplockerObjects;

class DroidPostTimeCard extends DroplockerObject
{
    public static $table_name = "droid_postTimeCards";
    public static $primary_key = "droid_postTimeCardsID";


    public function validate()
    {
    }

    /**
     * gets all the pending timecard entries
     *
     * @param int $limit
     * @return array of objects
     */
    public static function getPending($limit = 50)
    {
        $CI = get_instance();
        $sql = "SELECT * FROM droid_postTimeCards WHERE status = 'pending' LIMIT {$limit}";

        return $CI->db->query($sql)->result();
    }

}
