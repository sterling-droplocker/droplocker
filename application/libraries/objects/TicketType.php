<?php
namespace App\Libraries\DroplockerObjects;

class TicketType extends DroplockerObject
{
    public static $table_name = "ticketType";
    public static $primary_key = "ticketTypeID";



    public function validate()
    {
        if (empty($this->description)) {
            throw new Validation_Exception("description is required.");
        }

        if (empty($this->roles)) {
            throw new Validation_Exception("You need to select at least one role.");
        }
    }
}
