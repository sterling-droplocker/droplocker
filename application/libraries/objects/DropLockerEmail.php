<?php
namespace App\Libraries\DroplockerObjects;

use DropLocker\Util\Lock;

/**
 * The following class is   and does not follow coding standards. Do not use.
 */
class DropLockerEmail extends DroplockerObject
{
    public static $table_name = "email";
    public static $primary_key = "emailID";

    private $devEmails;
    private $noDev;
    private $sendAllEmails;
    private $emailCronStatus;
    private $manualEmailProcessing;
    private $emailLimit;

    public function __construct($emailID = '')
    {
        parent::__construct($emailID);

        // set up emails for problems
        $this->devEmails = DEVEMAIL;

        $this->setEmailSettings();
    }

    public function validate()
    {
    }

    public function save()
    {
        //force the the customer_id attribute to numeric to avoid MySQL query error.
        if (!is_numeric($this->customer_id)) {
            $this->customer_id = 0;
        }
        parent::save();
    }

    /**
     * function that gets the email history for a customer with both sms and email
     *
     * @param object $customer
     * @return array of objects
     */
    public function getCustomerEmailsAndSMS($customer, $limit = '')
    {
        $CI = get_instance();

        $CI->db->where('customer_id', $customer->customerID);
        $CI->db->or_where('to', $customer->email);
        $CI->db->where('business_id', $customer->business_id);

        $CI->db->limit($limit);
        $CI->db->order_by('sendDateTime', 'DESC');
        $query = $CI->db->get('email');

        return $query->result();
    }

    /**
     * this funciton is called throughout the email sending process.
     * The settings are maintained in the settings table
     *
     * This function sets private variables for this class
     */
    public function setSettings($settingObject = null)
    {
        if (!$settingObject) {
            $this->sendAllEmails  = \App\Libraries\DroplockerObjects\Settings::getSendAllEmails();
            $this->emailCronStatus  = \App\Libraries\DroplockerObjects\Settings::getEmailCronStatus();
            $this->manualEmailProcessing  = \App\Libraries\DroplockerObjects\Settings::getManualEmailProcessing();
            $this->manualEmailProcessing = 0; // force this, because its magically changin it value in prod
            $this->emailLimit  = \App\Libraries\DroplockerObjects\Settings::getEmailLimit();
        } else {
            // this allows me to load a mock settings object for testing
            // not implemented yet
        }
    }

    /**
     * gets all pending emails.
     *
     * @param int $business_id
     *            (optional)
     * @param int $limit
     *            (optional)
     * @return array of objects
     */
    public static function getAllPendingEmails($business_id = '', $limit = '')
    {
        $CI = get_instance();

        $withBusiness = ($business_id != '') ? " AND business_id = {$business_id}" : "";
        $withLimit = ($limit != '') ? " LIMIT {$limit}" : "";

        $sql = "SELECT emailID, subject, email.business_id, companyName, `to`, sendDateTime, dateCreated
        FROM email
        INNER JOIN business ON businessID = email.business_id
        WHERE status = 'pending'
        {$withBusiness}
        ORDER BY dateCreated DESC
        {$withLimit}";

        return $CI->db->query($sql)->result();
    }

    /**
     * gets the count of all pending emails in queue.
     * If the $max argument is met, an email is sent to the develoment team
     *
     * @param int $max
     * @return int
     */
    public function getPendingCount($max = 100)
    {
        $CI = get_instance();

        $count = $CI->db->query('SELECT count(*)  as total
                FROM email
                WHERE sendDateTime < now()
                and status="pending"')->row();

        // Send an email to development if the queue is over $max
        if ($count->total >= $max) {
            send_email(SYSTEMEMAIL, SYSTEMFROMNAME, $this->devEmails, "Email queue has {$count->total} pending emails", "Email queue has {$count->total} pending emails");
        }

        return $count->total;
    }

    /**
     * gets the count of emails that have been in pending for over a time period.
     * If any emails are found, an email is sent to the development team and update the email rows
     * to 'fail'
     *
     * @param int $hours
     * @return int
     */
    public function getOldEmailCount($hours = 48)
    {
        $CI = get_instance();

        $oldEmailCount = $CI->db->query('SELECT count(*) as total
                FROM email
                WHERE status="pending"
                AND dateCreated <= DATE_ADD(NOW(), INTERVAL -' . $hours . ' HOUR) ')->row();

        // Send an email to development if there are emails in the queue that were made over a day ago
        if ($oldEmailCount->total >= 1) {

            // need to update the status to fail to prevent this email from getting resent
            $oldEmails = $CI->db->query('SELECT *
                    FROM email
                    WHERE status="pending"
                    AND dateCreated <= DATE_ADD(NOW(), INTERVAL -'.$hours.' HOUR) ')->result();

            $body = "<table width='100%'>";
            $body .= "<tr>";
            $body .= "<th>TO</th><th>EMAILID</th>";
            $body .= "</tr>";
            foreach ($oldEmails as $oldEmail) {
                $body .= "<tr>";
                $body .= "<td>{$oldEmail->to}</td><td>{$oldEmail->emailID}</td>";
                $body .= "</tr>";
            }
            $body .= "</table>";

            send_email(SYSTEMEMAIL, SYSTEMFROMNAME, $this->devEmails, "There are {$oldEmailCount->total} emails over 1 day old in the queue", $body);

            // update the emails to fail
            $CI->db->query('UPDATE email
                    SET status = "fail", statusNotes = "email set to fail due to status of pending over 1 days"
                    WHERE status="pending"
                    AND dateCreated <= DATE_ADD(NOW(), INTERVAL -24 HOUR) ');
        }

        return $oldEmailCount->total;
    }

    /**
     * queries the email table and sends the emails
     *
     * @return array
     */
    public function processEmails()
    {
        $CI = get_instance();

        $this->setSettings();

        if ($this->emailCronStatus == 0) {
            error_log("Email Cron Status has been set to 0. Can not continue");
            return false;
        }

        if ($this->manualEmailProcessing == 1) {
            error_log("Manual Email Processing has been set to 1. Can not continue");
            return false;
        }

        if (! Lock::acquire('processEmails')) {
            error_log("Lockfile present, can not execute");
            return false;
        }

        $results = array();
        $this->getPendingCount();
        $this->getOldEmailCount();

        $withTime = 'AND sendDateTime <= NOW()';
        if ($this->sendAllEmails) {
            $withTime = '';
        }

        $emails = $CI->db->query("SELECT * FROM email WHERE
            status = 'pending' $withTime
            ORDER BY dateCreated
            LIMIT {$this->emailLimit}")->result();

        foreach ($emails as $email) {
            $result = array();
            $email = new DropLockerEmail($email->emailID);

            // need to check if this is an SMS or typical email
            // (SMS may need to go through twilio or nexmo)
            $fromName = strtolower(trim($email->fromName));
            switch ($fromName) {
                case 'twilio':
                case 'nexmo':
                    $results[] = $this->sendSMS($email, $fromName);
                break;

                default:
                    $results[] = $email->send();
            }
        }

        return $results;
    }

    /**
     * Sends a SMS
     *
     * @param Email $email
     * @param string $provider
     * @return array
     */
    protected function sendSMS($email, $provider)
    {
        $sent_status = $email->status;

        //check the environment, don't send out SMS's if its on dev
        $email->emailType = 'development';
        if (ENVIRONMENT == "production") {
            $email->emailType = 'production';
        }

        //double check that we have a number
        if (empty($email->to) || !is_numeric($email->to)) {

            return array(
                'emailID' => $email->emailID,
                'status'=> $sent_status,
                'to' => $email->to,
                'subject' => $email->subject
            );
        }

        //only send if its production
        if ($email->emailType == 'production') {

            $sms = \DropLocker\Service\SMS\Factory::getService($provider, $email->business_id);
            if ($sms->sendMessage($email->to, $email->body)) {
                $sent_status = 'success';
                $email->sentDateTime = convert_to_gmt_aprax();
            } else {
                $sent_status = 'fail';
                $email->errorMessage = $sms->getErrorMessage();
            }

        } else {
            //dev suppress
            $sent_status = 'success';
            $email->sentDateTime = convert_to_gmt_aprax();
        }

        $email->status = $sent_status;
        $email->save();

        return array(
            'emailID' => $email->emailID,
            'status'=> $sent_status,
            'to' => $email->to,
            'subject' => $email->subject
        );
    }

    /**
     * manually sends an array of emails
     *
     * $email array is an array of emailID's
     *
     * @param array $emailArray
     * @return array
     */
    public function manualProcessEmails($emailArray)
    {
        $CI = get_instance();

        $CI->db->where_in('emailID', $emailArray);
        $query = $CI->db->get('email');
        $emails = $query->result();

        foreach ($emails as $email) {

            $CI->db->where('emailID', $email->emailID);
            $CI->db->select('status');
            $query = $CI->db->get('email');
            $status = $query->row();

            if ($status->status != 'pending') {
                continue;
            }

            $email = new DropLockerEmail($email->emailID);

            //need to check if this is an SMS or typical email
            // (SMS may need to go through twilio or nexmo)
            $fromName = strtolower(trim($email->fromName));
            switch ($fromName) {
            	case 'twilio':
            	case 'nexmo':
            	    $results[] = $this->sendSMS($email, $fromName);   //send the SMS
                break;

                default: $results[] = $email->send();
            }
        }

        return $results;
    }


    /**
     * Simply gets the total number of emails sent for a business
     *
     * @param string $email
     * @param int $business_id
     * @throws \Exception
     */
    public static function count($email, $customer_id = '', $business_id = '')
    {
        $CI = get_instance();

        $search_customer_id = (!empty($customer_id))?" OR customer_id = ". $customer_id:'';

        $sql = "SELECT count(*) as total FROM email WHERE `to` = ? $search_customer_id AND business_id = ?";
        $query = $CI->db->query($sql, array($email, $business_id));
        $total = $query->row();

        return $total->total;
    }

    /**
     * sets the email settings for each business.
     * Each business can use their own amazon ses servers
     *
     * @param unknown $business_id
     * @throws \Exception
     */
    public function setEmailSettings()
    {
        $CI = get_instance();

        if (Server_Meta::get('amazon_ses_username') == '' || Server_Meta::get('amazon_ses_password') == '') {
            trigger_error("Amazon ses username and password is not set properly for the server", E_USER_WARNING);
            //throw new \Exception("Amazon ses username and password is not set properly for the server");
        }

        $config['smtp_host'] = 'ssl://email-smtp.us-east-1.amazonaws.com';
        $config['smtp_user'] = get_server_meta('amazon_ses_username');
        $config['smtp_pass'] = get_server_meta('amazon_ses_password');
        $config['protocol'] = 'smtp';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'UTF-8';
        $config['wordwrap'] = FALSE;
        $config['smtp_port'] = '465';
        $config['mailtype'] = 'html';

        $CI->email->initialize($config);
    }


    public function testSend($to, $from, $fromName, $subject, $body)
    {
        $CI = get_instance();
        $this->setEmailSettings();

        $CI->email->subject($subject);
        $CI->email->message($body);
        $CI->email->to($to);
        $CI->email->from($from, $fromName);
        $CI->email->set_newline("\r\n");
        $CI->email->send();
        echo $CI->email->print_debugger();
    }


    /**
     * The following function is unknown  . Do not use
     * @deprecated
     * ----------------------------------
     * BEGIN
     * ----------------------------------
     *
     * sends the email
     *
     * The result array consists of:
     * -------------------
     * emailID
     * status
     * to
     * subject
     *
     * @throws Exception
     * @return array of results
     *
     * ----------------------------------
     * END
     * ----------------------------------
     */
    private function send()
    {
        $CI = get_instance();


        $CI->email->clear();

        /*
         * IN development, we should never be here. Just an added protection to sending dev emails to customers.
         */
        if (ENVIRONMENT == "production") {

            $CI->email->from($this->fromEmail, $this->fromName);
            $to = $this->to;
            $subject = $this->subject;
            $body = $this->body;
            $cc = unserialize($this->cc);
            $bcc = unserialize($this->bcc);

            if (empty($to)) {
                //throw new \Exception("Missing 'to' email address.<BR>". $this->emailID);
                return true;    //temp hack - skip emails
            }

            $CI->email->to($to);
            if (!is_null($subject)) {
                $CI->email->subject($subject);
            }
            $CI->email->message($body);
            $CI->email->cc($cc);
            $CI->email->bcc($bcc);
            if (!empty($this->attachment)) {
                $CI->email->attach($this->attachment);
            }
            $CI->email->set_newline("\r\n");

            if (!$CI->email->send()) {
                $this->status = 'fail';
                $this->sentDateTime = date('Y-m-d H:i:s');
                $this->errorMessage = $CI->email->print_debugger();
                $this->save();
            } else {
               $this->status = 'success';
               $this->sentDateTime = date('Y-m-d H:i:s');
               $this->save();
            }

        } else {

            $to = $this->to;
            $subject = $this->subject;
            $this->status = 'success';
            $this->sentDateTime = date('Y-m-d H:i:s');
            $this->save();
        }

        return array('emailID'=>$this->emailID, 'status'=>$this->status, 'to'=>$to, 'subject'=>$subject);
    }
}
