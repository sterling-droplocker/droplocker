<?php
namespace App\Libraries\DroplockerObjects;

class CronLog extends DroplockerObject
{
    public static $table_name = "cronLog";
    public static $primary_key = "cronLogID";

    public function getAdminLog() {
        $CI = get_instance();

        $sql = "SELECT * FROM cronLog ORDER BY dateCreated DESC LIMIT 30";
        return $CI->db->query($sql)->result();
    }

}