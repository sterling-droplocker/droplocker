<?php

namespace App\Libraries\DroplockerObjects;
class TriggerOrderItemRemoved extends DroplockerObject
{
    public static $table_name = "triggerOrderItemRemoved";
    public static $primary_key = "triggerOrderItemRemovedID";

    public function validate()
    {
        if (!is_numeric($this->orderItem_id)) {
            throw new Validation_Exception("'orderItem_id' must be numeric");
        }
    }
}
