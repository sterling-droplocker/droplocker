<?php
namespace App\Libraries\DroplockerObjects;
class AlertItem extends DroplockerObject
{
    public static $table_name = "alertItem";
    public static $primary_key = "alertItemID";

    public static function get($business_id)
    {
        $CI = get_instance();
        $sql = "SELECT alertItem.*, product.name FROM alertItem
                INNER JOIN product ON product_id = productID
                Where alertItem.business_id = {$business_id}";

        return $CI->db->query($sql)->result();
    }
    /**
     *
     * @return boolean
     * @throws Validation_Exception
     */
    public function validate()
    {

    }

}
