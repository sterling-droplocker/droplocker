<?php
namespace App\Libraries\DroplockerObjects;

class Server_Meta extends DroplockerObject
{
    public static $table_name = "server_meta";
    public static $primary_key = "server_metaID";

    private static $modes = array("droplocker", "bizzie"); //This variable stores the valid modes.
    private static $keys = array("mode", "email_host", "email_user", "email_password", "master_business_id", "sms_keyword", "amazon_ses_username", "amazon_ses_password"); //This variable stores the valid keys that can be stored in the database
    public function validate()
    {
        if ($this->key == "mode") {
            if (!in_array($this->value, self::$modes)) {
                throw new Validation_Exception(sprintf("'mode' must be one of the following values: %s", implode(",", self::$modes)));
            }
        }
        if (empty($this->key)) {
            throw new Validation_Exception("'key' can not be empty");
        }
        if (!in_array($this->key, self::$keys)) {
            throw new Validation_Exception(sprintf("Invalid key '{$this->key}'. 'key' must be one of the following values: %s", implode(",", self::$keys)));
        }

        if (empty($this->value)) {
            throw new Validation_Exception("'value' can not be empty");
        }
    }

    /**
     * Retrieves an Server_Meta object by its key nhame
     * @param string The key name
     * @return string $key 'false' if no match found, otherwise returns the value for the key
     */
    public static function get($key)
    {
        $keys = self::search_aprax(array("key" => $key));
        if (empty($keys)) {
            return false;
        } else {
            return $keys[0]->value;
        }
    }
    /**
     * Sets the server mode
     * @param string $value
     */
    public static function set_server_mode($value)
    {
        self::set("mode", $value);
    }

    public static function set_master_business($business_id)
    {
        try {
            new Business($business_id);


        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '$business_id' does not exist in database");
        }
        self::set("master_business_id", $business_id);
    }
    /**
     * Sets a server meta setting. If the setting does not already exist, then it creates the setting
     * @param string $key_name
     * @param string $value
     */
    public static function set($key_name, $value)
    {
        $key = self::get($key_name);
        $keys = self::search_aprax(array("key" => $key_name));
        if (empty($keys)) {
            $key = null;
        } else {
            $key = $keys[0];
        }

        if ($key instanceOf \App\Libraries\DroplockerObjects\Server_Meta) {

            $key->value = $value;
        } else {
            $key = new Server_meta();
            $key->key = $key_name;
            $key->value = $value;
        }
        $key->save();
    }

    public static function get_valid_modes_as_codeigniter_dropdown()
    {
        $modes_dropdown = array();
        foreach (self::$modes as $mode) {
            $modes_dropdown[$mode] = $mode;
        }

        return $modes_dropdown;
    }
    public static function get_valid_keys()
    {
        return self::$keys;
    }

    public static function in_bizzie_mode()
    {
        return self::get("mode") == "bizzie";
    }
}
