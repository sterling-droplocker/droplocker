<?php
namespace App\Libraries\DroplockerObjects;
class LanguageView extends DroplockerObject
{
    public static $table_name = "languageView";
    public static $primary_key = "languageViewID";
    public static $has_many = array("LanguageKey");

    public function validate()
    {
        $CI = get_instance();
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }
        $CI->load->model("languageView_model");
        $existing_language_views_count = $CI->languageView_model->count(array(
            "name" => $this->name,
            "languageViewID !=" => (int) $this->languageViewID,
        ));

        if ($existing_language_views_count > 0) {
            throw new Validation_Exception("A language view with the name '{$this->name}' already exists");
        }
    }
}
