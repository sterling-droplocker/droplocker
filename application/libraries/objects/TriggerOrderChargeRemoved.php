<?php

namespace App\Libraries\DroplockerObjects;
class TriggerOrderChargeRemoved extends DroplockerObject
{
    public static $table_name = "triggerOrderChargeRemoved";
    public static $primary_key = "triggerOrderChargeRemovedID";

    public function validate()
    {
        if (!is_numeric($this->orderCharge_id)) {
            throw new \InvalidArgumentException("'orderCharge_id' must be numeric");
        }
    }
}
