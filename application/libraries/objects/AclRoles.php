<?php
namespace App\Libraries\DroplockerObjects;
/**
 * @deprecated The class name of a derivation of a Droplocker Object must be a singular noun.
 */
class AclRoles extends DroplockerObject
{
    public static $table_name = "aclroles";
    public static $primary_key = "aclID";

    public function validate()
    {
    }

}
