<?php
namespace App\Libraries\DroplockerObjects;
class OrderStatusOption extends DroplockerObject
{
    public static $table_name = "orderStatusOption";
    public static $primary_key = "orderStatusOptionID";

    public function validate()
    {
        if (!$this->name) {
            throw new Validation_Exception("'name' can not be empty.");
        }
    }
}
