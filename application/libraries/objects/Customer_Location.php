<?php
namespace App\Libraries\DroplockerObjects;
class Customer_Location extends DroplockerObject
{
    public static $table_name = "customer_location";
    public static $primary_key = "ID";
    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");

        }
        try {
            new Customer($this->customer_id);

        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Customer ID '{$this->customer_id}' not found in database.");
        }

        if (!is_numeric($this->location_id)) {
            throw new Validation_Exception("'location_id' must be numeric.");
        }
        try {
            new Location($this->location_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Location ID '{$this->location_id}' not found in database.");
        }
    }
}
