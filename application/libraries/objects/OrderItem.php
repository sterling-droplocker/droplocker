<?php
namespace App\Libraries\DroplockerObjects;
class OrderItem extends DroplockerObject
{
    public static $table_name = "orderItem";
    public static $primary_key = "orderItemID";

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
    }
    public function validate()
    {
        if (!is_numeric($this->item_id)) {
            throw new Validation_Exception("'item_id' must be numeric.");
        }
        try {
            new Item($this->item_id);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Item ID '{$this->item_id}' not found in database.");
        }
        if (!is_numeric($this->supplier_id)) {
            throw new Validation_Exception("'supplier_id' must be numeric.");
        }
        try {
            new Supplier($this->supplier_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Supplier '{$this->supplier_id}' not found in database");
        }

        if (!is_numeric($this->order_id)) {
            throw new Validation_Exception("'order_id' must be numeric");
        }
        try {
            new Order($this->order_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Order ID '{$this->order_id}' not found in database");
        }

        if (!is_numeric($this->product_process_id)) {
            throw new Validation_Exception("'product_process_id' must be numeric");
        }

        try {
            new Product_Process($this->product_process_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Product Process 'id' not found in database.");
        }
    }
}
