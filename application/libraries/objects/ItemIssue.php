<?php

namespace App\Libraries\DroplockerObjects;
class ItemIssue extends DroplockerObject
{
    public static $table_name = "itemIssue";
    public static $primary_key = "itemIssueID";

    public static $issueTypes = array("Repair" => "Repair", "Other" => "Other", "Stain" => "Stain");
    public static $statuses = array("Opened"  => "Opened", "Closed" => "Closed");
    public static $frontBacks = array("Front" => "Front", "Back" => "Back");

    public function validate()
    {
        if (!is_numeric($this->item_id)) {
            throw new Validation_Exception("'item_id' must be numeric");
        }
        try {
            new Item($this->item_id);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Item ID '{$this->item_id}' not found");
        }

        if (empty($this->frontBack)) {
            throw new Validation_Exception("'frontBack' can not be empty.");
        }

        if (!in_array($this->status, self::$statuses)) {
            throw new Validation_Exception("Invalid status value");
        }

    }
}
