<?php
namespace App\Libraries\DroplockerObjects;

class Product_Location_Price extends DroplockerObject
{
    public static $table_name = "product_location_price";
    public static $primary_key = "product_location_priceID";
    public function validate()
    {
        if (!is_numeric($this->product_process_id)) {
            throw new Validation_Exception("'product_process_id' must be numeric.");
        }
        try {
            new Product_Process($this->product_process_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Product Process ID '{$this->product_process_id}' not found");
        }
        if (!is_numeric($this->location_id)) {
            throw new Validation_Exception("'location_id' must be numeric.");
        }
        if (empty($this->price)) {
            throw new Validation_Exception("'price' can not be empty");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' can not be empty");
        }
        try {
            new Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found");
        }
    }
}
