<?php
namespace App\Libraries\DroplockerObjects;
class ReminderLog extends DroplockerObject
{
    public static $table_name = "reminderLog";
    public static $primary_key = "reminderLogID";
}
