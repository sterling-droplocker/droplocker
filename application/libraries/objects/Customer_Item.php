<?php

namespace App\Libraries\DroplockerObjects;

class Customer_Item extends DroplockerObject
{
    public static $table_name = "customer_item";
    public static $primary_key = "customer_itemID";
    public static $has_one = array("Customer");

    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }
        try {
            new Customer($this->customer_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Customer ID '{$this->customer_id}' not found.");
        }
        try {
            new Item($this->item_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Item ID '{$this->item_id}' not found.");
        }
    }

}
