<?php
namespace App\Libraries\DroplockerObjects;
class BusinessSetting extends DroplockerObject
{
    public static $primary_key="businessSettingID";
    public static $table_name = "businessSetting";
    public static $foreignKey = "businessSetting_id";
    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            new Business($this->businesss_id, false);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' does not exist");
        }
        if (empty($this->setting)) {
            throw new Validation_Exception("'setting' can not be empty.");
        }
    }
}
