<?php

namespace App\Libraries\DroplockerObjects;
class TaxGroup extends DroplockerObject
{
    public static $table_name = "taxGroup";
    public static $primary_key = "taxGroupID";

    protected $rules = array (
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "name" => array("required"),
    );

}
