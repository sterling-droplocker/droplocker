<?php

namespace App\Libraries\DroplockerObjects;
class TaxTable extends DroplockerObject
{
    public static $table_name = "taxTable";
    public static $primary_key = "id";


    protected $rules = array (
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "zipcode" => array("required"),
        "taxRate" => array("numeric", "required")
    );

}
