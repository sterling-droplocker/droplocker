<?php
namespace App\Libraries\DroplockerObjects;
class LockerStatus extends DroplockerObject
{
    const IN_SERVICE = 1;
    const ON_HOLD_FOR_CUSTOMER = 2;
    const REMOVED = 4;
    const BROKEN = 5;
    const INACTIVE = 6;

    public static $table_name = "lockerStatus";
    public static $primary_key = "lockerStatusID";
    public function validate()
    {
        throw new \Exception("Not Implemented.");
    }
}
