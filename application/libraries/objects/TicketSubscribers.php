<?php
namespace App\Libraries\DroplockerObjects;

class TicketSubscribers extends DroplockerObject
{
    public static $table_name = "ticketSubscribers";
    public static $primary_key = "ticketSubscribersID";


    public function validate()
    {
    }

    /**
     *
     * updates the ticketSubscribers table
     *
     * if we find an invalid email, we return an array:
     * ----------
     * status = fail
     * message = Invalid Email
     *
     * @param array $emails
     * @param string $subject
     * @param int $business_id
     *
     * @return array|int
     */
   public static function updateSubscribers($emails, $subject, $business_id)
   {
        $CI = get_instance();

       if (!is_numeric($business_id)) {
           throw new \Exception("missing business_id");
       }

       if ($subject=="") {
           $error = array('status'=>'fail', 'message'=>"Missing Subject");

           return $error;
       }

       $CI->load->helper('email');
       foreach ($emails as $email) {
           if (!valid_email(trim($email))) {
               $error = array('status'=>'fail', 'message'=>"Invalid Email: ".$email);

               return $error;
           }
       }

       $subscribers = TicketSubscribers::search_aprax(array('business_id'=>$business_id));
       if (!isset($subscribers)) {
           $subscribers = new TicketSubscribers();
       } else {
           $subscribers = $subscribers[0];
       }
       $subscribers->business_id = $business_id;
       $subscribers->subscribers = serialize($emails);
       $subscribers->subject = mysql_escape_string($subject);

       return $subscribers->save();

   }
}
