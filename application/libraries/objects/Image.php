<?php
namespace App\Libraries\DroplockerObjects;
class Image extends DroplockerObject
{
    public static $primary_key = "imageID";
    public static $table_name = "image";

    public function validate()
    {
        if (empty($this->filename)) {
            throw new Validation_Exception("'filename' can not be empty");
        }
        if (empty($this->path)) {
            throw new Validation_Exception("'path' can not be empty");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new Business($this->business_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found in database");
        }
    }
}
