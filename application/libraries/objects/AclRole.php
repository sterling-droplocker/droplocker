<?php

/**
 * The properties of an AclRole are as follows:
 *  name The name of the role, as specified by the business
 *  roleorder int Unknown * parameter
 *  business_id uint
 *  custom int Unknown * parameter
 * @author aprax
 */
class AclRole extends DroplockerObject
{
    public static $table_name = "aclroles";
    public static $primary_key = "aclID"; //Note, the primary key name does not conform to coding standards.
}
