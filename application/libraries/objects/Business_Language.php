<?php
namespace App\Libraries\DroplockerObjects;
class Business_Language extends DroplockerObject
{
    public static $table_name = "business_language";
    public static $primary_key = "business_languageID";
    public static $has_one = array("Language");

    /**
     * Convenience function for quickly creating a new business language
     * @param int $business_id
     * @param int $language_id
     * @return \App\Libraries\DroplockerObjects\Business_Language
     */
    public static function create($business_id, $language_id)
    {
        $business_language = new Business_Language();
        $business_language->business_id = $business_id;
        $business_language->language_id = $language_id;
        $business_language->locale = "";
        $business_language->save();

        return $business_language;
    }

    public function validate()
    {
        $CI = get_instance();
        $CI->load->model("business_language_model");

        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        if (!is_numeric($this->language_id)) {
            throw new Validation_Exception("'language_id' must be numeric");
        }
        Validation_Exception::verify_entity_exists($this->business_id, "Business");
        Validation_Exception::verify_entity_exists($this->language_id, "Language");

        $business_languages = $CI->business_language_model->get_aprax(array(
            "business_id" => $this->business_id,
            "language_id" => $this->language_id,
            "business_languageID !=" => intval($this->business_languageID),
        ));

        if (!empty($business_languages)) {
            $CI->load->model("business_model");
            $business = $CI->business_model->get_by_primary_key($this->business_id);
            $CI->load->model("language_model");
            $language = $CI->language_model->get_by_primary_key($this->language_id);
            throw new Validation_Exception("Language '{$language->tag} - {$language->description}' ({$language->languageID}) is already defined for business {$business->companyName} ({$business->businessID})");
        }

    }
    public function validate_delete()
    {
        $business = new Business($this->business_id);
        if ($business->default_business_language_id == $this->{self::$primary_key}) {
            throw new Validation_Exception("Can not delete the default business language");
        }
        $CI = get_instance();
        $CI->load->model("reminder_email_model");
        $reminder_emails_for_business_language_count = $CI->reminder_email_model->count(array("business_language_id" => $this->{self::$primary_key}));
        if ($reminder_emails_for_business_language_count > 0) {
            throw new Validation_Exception("Can not delete because there are $reminder_emails_for_business_language_count reminder emails associated with this business language");
        }
    }
}
