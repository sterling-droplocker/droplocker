<?php
namespace App\Libraries\DroplockerObjects;
class LaundryPlan extends DroplockerObject
{
    public static $table_name = "laundryPlan";
    public static $primary_key = "laundryPlanID";

    /**
     * @deprecated THis is not functionality that belongs in the Droplocker Objects
     */
    public static function getActiveLaundryPlan($customer_id)
    {
        $CI = get_instance();
        $CI->db->where('customer_id', $customer_id);
        $CI->db->where('type', 'laundry_plan');
        $CI->db->where('active', 1);
        $results = $CI->db->get(self::$table_name)->result();

        return $results;
    }

    public function validate()
    {
    }
}
