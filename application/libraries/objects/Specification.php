<?php
namespace App\Libraries\DroplockerObjects;

class Specification extends DroplockerObject
{
    public static $table_name = "specification";
    public static $primary_key = "specificationID";


    public function validate()
    {
        if (empty($this->description)) {
            throw new Validation_Exception("'description' can not be empty.");
        }

        if (!is_numeric($this->everyOrder)) {
            throw new Validation_Exception("'everyOrder' can not be empty.");
        }
    }


}
