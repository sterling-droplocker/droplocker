<?php
namespace App\Libraries\DroplockerObjects;
/**
 * Note, this status is incorrectly named a plural noun instead of a singular noun.
 */
class Droid_PostDeliveries extends DroplockerObject
{
    public static $process_statuses = array("not_processed", "processing", "processed");
    public static $table_name = "droid_postDeliveries";
    public static $primary_key = "ID";

    public function validate()
    {
        if (!in_array($this->process_status, self::$process_statuses)) {
            throw new Validation_Exception("Invalid process status '{$this->process_status}'");
        }

    }
}
