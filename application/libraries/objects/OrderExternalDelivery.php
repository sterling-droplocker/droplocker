<?php
namespace App\Libraries\DroplockerObjects;

class OrderExternalDelivery extends DroplockerObject
{
    public static $table_name = "orderExternalDelivery";
    public static $primary_key = "orderExternalDeliveryID";
    public static $belongsTo = array("Order");
}