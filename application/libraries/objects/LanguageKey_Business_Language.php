<?php
namespace App\Libraries\DroplockerObjects;
class LanguageKey_Business_Language extends DroplockerObject
{
    public static $table_name = "languageKey_business_language";
    public static $primary_key = "languageKey_business_languageID";

    public static $belongsTo = array("Language");
    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "languageKey_id" => array("numeric", "required", "entity_must_exist"),
        "language_id" => array("numeric", "required", "entity_must_exist"),
        "translation" => array("required")
    );
    /**
     * The following static method is a convience method for creating a new language key business language entity.
     * @param int $business_id
     * @param int $language_id
     * @param int $languageKey_id
     * @param string $translation
     * @return \App\Libraries\DroplockerObjects\LanguageKey_Business_Language
     */
    public static function create($business_id, $language_id, $languageKey_id, $translation)
    {
        $languageKey_business_language = new LanguageKey_Business_Language();
        $languageKey_business_language->business_id = $business_id;
        $languageKey_business_language->language_id = $language_id;
        $languageKey_business_language->languageKey_id = $languageKey_id;
        $languageKey_business_language->translation = $translation;
        $languageKey_business_language->save();

        return $languageKey_business_language;
    }
    /**
     *
     * @throws Validation_Exception
     */
    public function validate()
    {
        parent::validate();
        $CI = get_instance();
        if (empty($this->{self::$primary_key})) {
            $CI->load->model("languageKey_business_language_model");
            $languageKey_business_language = $CI->languageKey_business_language_model->get_one(array("business_id" => $this->business_id, "languageKey_id" => $this->languageKey_id, "language_id" => $this->language_id));
            if (!empty($languageKey_business_language)) {
                $CI->load->model("language_model");
                $CI->load->model("business_model");
                $CI->load->model("languageKey_model");
                $language = $CI->language_model->get_by_primary_key($this->language_id);
                $business = $CI->business_model->get_by_primary_key($this->business_id);
                $languageKey = $CI->languageKey_model->get_by_primary_key($this->languageKey_id);
                throw new Validation_Exception(sprintf("A translation for language key '%s' (%s) already exists for business '%s' (%s) with language '%s' (%s)", $languageKey->name, $languageKey->languageKeyID, $business->name, $business->businessID, $language->tag, $language->languageID));
            }
        }
    }

    /**
     * Validate if the same substitutions exists in defaultText and translation
     *
     * @param string $defaultText
     * @param string $translation
     *
     * @return boolean
     */
    public function validate_substitutions($defaultText, $translation)
    {
        preg_match_all("/%(.+?)%/", $defaultText, $defaultTextMatches);
        preg_match_all("/%(.+?)%/", $translation, $translationMatches);

        sort($defaultTextMatches[0]);
        sort($translationMatches[0]);

        return ($defaultTextMatches[0] == $translationMatches[0]);
    }
}
