<?php
namespace App\Libraries\DroplockerObjects;
class Reminder extends DroplockerObject
{
    public static $table_name = "reminder";
    public static $primary_key = "reminderID";

    public static $amountTypes = array("percent" => "Percent", "dollars" => "Dollars");
    public static $couponTypes = array("newCustomer", "inactiveCustomer", "orderTotal", "birthdayCoupon");
    public static $statuses = array("paused", "active");

    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "description" => array("required"),
        "amount" => array("numeric"),
        "maxAmt" => array("numeric")
    );
    /**
     * The following fucntion extension deletes any associated reminder emails entities in addition to deleting the reminder itself.
     */
    public function delete()
    {
        $associated_reminder_emails = Reminder_Email::search_aprax(array("reminder_id" => $this->{self::$primary_key}));
        array_map(function ($reminder) { $reminder->delete();}, $associated_reminder_emails);
        parent::delete();
    }
    /**
     * Validates the data for this reminder
     * @throws Validation_Exception
     * @throws User_Exception If the business ID, couponType, days, and orders for a new reminder match for an existing reminder
     */
    public function validate()
    {
        if (is_null($this->orders)) {
            $this->orders = 0;
        }
        if (is_null($this->days)) {
            $this->days = 0;
        }
        parent::validate();
        //The following conditional checks to make sure that there isn't an existing reminder iwth the same values if we are tyring to create a new reminder.
        if (empty($this->{Reminder::$primary_key})) {
            $existing_reminders = self::search_aprax(array("business_id" => $this->business_id, "couponType" => $this->couponType, "days" => $this->days, "orders" => $this->orders));
            if (!empty($existing_reminders)) {
                $business = new Business($this->business_id);
                $existing_reminder = $existing_reminders[0];
                throw new Validation_Exception("Reminder '{$existing_reminder->description}' ({$existing_reminder->reminderID}) already has the same coupon type '{$this->couponType}', number of days '{$this->days}', and number of orders '{$this->orders}' for '{$business->companyName}' ({$business->businessID})");
            }
        }
        if (!in_array($this->couponType, self::$couponTypes)) {
            throw new Validation_Exception("Unknown coupon type {$this->couponType}");
        }

    }
}
