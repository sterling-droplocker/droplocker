<?php
namespace App\Libraries\DroplockerObjects;

//Note, each class that extends this class must define the public static properties
abstract class DroplockerObject
{
    /**
     * This stores the child's table name. This is set from the public static
     * declaration in the extended class
     *
     * @var string
     */
    private $table_name = null;

    /**
     * This stores the child's primary key name. This is set from the public static
     * declaration in the extended class
     *
     * @var string
     */
    private $primary_key = null;

    /**
     * This stores the table schema, when needed will be read from the database if empty
     *
     * @var array
     */
    protected static $schema = array();


    /**
     * This stores the object's properties, which are t he columns from the table in the
     * database specified in $table_name.
     *
     * @var array
     */
    public $properties = array();

    /**
     * The extended class can derive the following static arrays that stores the other
     * objects that the child class has a cardinal relationship to.
     *
     * Note, the extended class must declare this variables as public and static so that
     * search_aprax can work as a static method.
     *
     * A child class defines its cardinal relations as an array in each of the following
     * public static variables:
     *
     *  * has_many
     *  * has_one
     *  * belongsTo
     *
     * For example, suppose we are defining the relations for the class User. The relationships
     * are as follows:
     *
     * Class User Extends DroplockerObject {
     *     public static $table_name = "user";
     *     public static $primary_key = "userID";
     *
     *     //User hasMany Comments, ..., ... 	comment.user_id
     *     public static $has_many = array("Comment", "...", ...);
     *
     *     //User hasOne Token, ..., ... 	token.apple_id  This means that the key is in the other table
     *     public static $has_one = array("Token", "...", ...);
     *
     *     //User belongsTo Group, ..., ...	user.group_id This means that the key is in the same table
     *     public static $belongsTo = array("Group", "...", ...);
     * }
     *
     * Relationships are accessible as an array. This variable is populated through $has_many, $has_one,
     * and $belongsTo static variables set in the extended class.
     *
     * If the relationship has only once result, or is a has_one relatipnship, it's result is stored as
     * the first index in the array.
     *
     * If there are multiple entities in the result, each entity is stored as an index in the array.
     * Example:
     *  $user = new User(1, true);
     *  $user_token = $user->relationships['Token'][0];
     */
    public $relationships;

    /**
     * The following rules exist:
     *
     *  * numeric : The value for the entity's property must be numeric
     *  * required : The value of the entity's property can not be empty
     *  * entity_must_exist : The primary key of an entity referenced by a foreign key must exist in the database. Note, the specified key must be in the format [model_name]_id. For example: "customer_id"
     *
     * Rules are specified as an array with a key, that refers to a property of the class, and an array of rules.
     * An additional parameter, 'userMessage', can be passed as an option array to store a user friendly error message
     * Example:
     *  protected $rules = array(
     *      "business_id" => array("numeric", "required", "entity_must_exist"),
     *      "languageKey_id => array(
     *          "numeric" => array(
     *              "userMessage" => "Please enter a password"
     *          ),
     *          "required",
     *          "entity_must_exist"
     *      ),
     *      "translation" => array("required")
     *  );
     */
    protected $rules = array();

    /**
     * Referece to the controller, soon to be removed
     * @var \CI_Controller
     */
    protected $CI;

    /**
     * The following property keeps track of which entity properties were set to new values.
     *
     * @var array
     */
    private $changed = array();

    /**
     * Constructor
     *
     * @param mixed $id the primary key ID, if empty, creates a new object.
     * @param get_relationships bool
     * @throws Exception
     * @throws LogicException
     */
    public function __construct($id = null, $get_relationships = false)
    {
        $object = null;
        $class = get_called_class();

        if ($id instanceof \stdClass) {
            $object = $id;
            $id = $id->{$class::$primary_key};
        }

        try {
            if (!is_numeric($id) && $get_relationships == true) {
                throw new \LogicException("Can not retrieve relationships without ID.");
            }
        } catch (\LogicException $e) {
            send_exception_report($e);
            $get_relationships = false;
        }

        $CI = get_instance();

        if (!isset($class::$table_name)) {
            throw new \Exception("The static variable 'table_name' must be defined.");
        }
        if (!isset($class::$primary_key)) {
            throw new \Exception("The static variable 'primary_key' must be defined.");
        }

        $this->table_name = $class::$table_name;
        $this->primary_key = $class::$primary_key;
        $this->properties = array();

        if ($id === '0') {
            throw new NotFound_Exception("$class ID cannot be 0");
        }


        if (empty(self::$schema[$this->table_name])) {
            foreach ($CI->db->field_data($this->table_name) as $column) {
                static::$schema[$this->table_name][$column->name] = $column;
            }
        }

        // If we don't have an id, stop here
        if (empty($id)) {
            return;
        }

        if (!is_numeric($id)) {
            throw new \InvalidArgumentException("ID must be numeric.");
        }

        // If the object already exists in the database, then we populate the object's
        // properties with the data already in the database.

        if (empty($object)) {
            $query = $CI->db->query("SELECT * FROM `{$this->table_name}` WHERE `{$this->primary_key}` = ?", array($id));
            $object = $query->row();
        }

        if (empty($object)) {
            throw new NotFound_Exception("{$this->table_name} {$this->primary_key} '$id' not found.");
        }

        //The following loop sets the properties to the column names and column values in the database.
        foreach ($object as $field => $value) {
            $this->properties[$field] = $value;

            //The following conditional will set any initialized datetime or timestamp to GMT timezone,
            // because that's the expected timezone that should be set in the database.
            if (in_array(static::$schema[$this->table_name][$field]->type, array("datetime", "timestamp"))) {

                // If date is empty, set the field to null
                if (empty($value) || $value == "0000-00-00 00:00:00" || $value == "0000-00-00") {
                    $this->properties[$field] = null;
                    continue;
                }

                $this->properties[$field] = new \DateTime($value, new \DateTimeZone("GMT"));

                //The following statements will set the timezone of a particular date time value to the timezone
                // specified in the user session or the default timezone set in the server, if there is no user
                // session timezone found.
                if (isset($CI->session) && $user_timezone = $CI->session->userdata("timezone")) {
                    $timezone = new \DateTimeZone($user_timezone);
                    $this->properties[$field]->setTimezone($timezone);
                } else {
                    $this->properties[$field]->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                }
            }
        }

        if ($get_relationships) {
            $this->relationships = array();

            //Note, the "orders" table is incorrectly called "orders" instead of "order", this conditional handles this problem.
            if ($this->table_name == "orders") {
                $this->table_name = "order";
            }

            if (isset($class::$has_many) && is_array($class::$has_many)) {

                foreach ($class::$has_many as $has_many) {
                    $has_many_class = sprintf("%s\%s", __NAMESPACE__, $has_many);
                    if ($this->table_name == "orders") {
                        $this->relationships[$has_many] = $has_many_class::search_aprax(array("order_id" => $this->{$this->primary_key}), false);
                    } else {
                        $this->relationships[$has_many] = $has_many_class::search_aprax(array("{$this->table_name}_id" => $this->{$this->primary_key}), false);
                    }
                }
            }
            //This means that the foreign key to the current table is in the other table
            if (isset($class::$belongsTo) && is_array($class::$belongsTo)) {
                foreach ($class::$belongsTo as $belongsTo) {
                    $belongsTo_class = sprintf("%s\%s", __NAMESPACE__, $belongsTo);
                    //The following statement accomodates a * design error where he named the orders table plural instead of singlar. In contradiciotion to our accepted naming convention.
                    if ($class::$table_name == "orders") {
                        $this->relationships[$belongsTo] = $belongsTo_class::search_aprax(array(sprintf("%s_id", "order") => $this->{$this->primary_key}), false);
                    } else {
                        $this->relationships[$belongsTo] = $belongsTo_class::search_aprax(array(sprintf("%s_id", $class::$table_name) => $this->{$this->primary_key}), false);
                    }
                }

            }
            //This means that the foreign key to the other table is in the current table
            if (isset($class::$has_one) && is_array($class::$has_one)) {
                foreach ($class::$has_one as $has_one) {
                    $has_one_class = sprintf("%s\%s", __NAMESPACE__, $has_one);
                    try {
                        if (isset($has_one_class::$foreign_key)) {
                            $foreign_key = $has_one_class::$foreign_key;
                        } else {
                            if (strstr($has_one, "_")) { //Name has underscores
                                $foreign_key = sprintf("%s_id", strtolower($has_one));
                            } else { //Name is camelcase {
                                $foreign_key = sprintf("%s_id", lcfirst($has_one));
                            }
                        }
                        if (!empty($this->$foreign_key)) {
                            $this->relationships[$has_one][] = new $has_one_class($this->$foreign_key, false);
                        }
                    } catch (NotFound_Exception $e) {
                        trigger_error("Has One relationship $has_one_class {$this->$foreign_key} not found");
                    }

                }
            }

            if ($this->table_name == "order") {
                $this->table_name = "orders";
            }
        }
    }

    /**
    * Magic setter for object variables
    * @param string $key
    * @param string|int $value
    */
    public function __set($key, $value)
    {
        if (is_object($value) || is_array($value) || is_null($value)) {
            $this->properties[$key] = $value;
        } else {
            $this->properties[$key] = trim($value);
        }
        $this->changed[$key] = $this->properties[$key];

    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        } else {
            return null;
        }

    }
    public function __isset($key)
    {
        if (null === $this->__get($key)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the object's database properties as an array.
     * @return array
     */
    public function to_array()
    {
        return $this->properties;
    }
    /**
     * Saves the properties of an object into the database.
     * If the primary key is not set, then a new row is inserted into the database. Otherwise, an existing row in the database is updated.
     * @return int the primary key of the saved row
     */
    public function save()
    {
        if (!$this->beforeSave()) {
            return false;
        }

        $CI = get_instance();

        $this->validate();
        $query_data = array();
        foreach ($this->changed as $property_name => $value) {
            if ($value instanceOf \DateTime) {
                $date = clone $value;
                $date->setTimezone(new \DateTimeZone("GMT"));
                $query_data[$property_name] = $date->format("Y-m-d H:i:s");
            } else {
                $query_data[$property_name] = $value;
            }
        }
        $this->changed = array();
        //If the object already has a primary key, then we assume that the data already exists in the database.
        if ($this->{$this->primary_key}) {
            $CI->db->update($this->table_name, $query_data, array($this->primary_key => $this->{$this->primary_key}));

            if ($result = $CI->db->affected_rows()) {
                $this->afterSave(false);
            }

            return $result;

        } else { //Otherwise, we insert new data into the database.
            unset($query_data[$this->primary_key]);
            $CI->db->insert($this->table_name, $query_data);

            $this->{$this->primary_key} = $CI->db->insert_id();
            if ($this->{$this->primary_key}) {
                $this->afterSave(true);
            }

            return $this->{$this->primary_key};
        }

    }

    /**
     * Before Save Callback
     *
     * This callback is called before the save() operation and can prevent it by returning false
     *
     * @return boolean
     */
    protected function beforeSave() {

        return true;
    }

    /**
     * After Save Callback
     *
     * This callback is called after a successful save() operation
     *
     * @param boolean $created true when a new record is created
     */
    protected function afterSave($created) {
    }

    /**
     * The following function is *. Do not use.
     * @deprecated use save() instead
     */
    public function update()
    {
        $CI = get_instance();

        //send_exception_report(new \BadMethodCallException("This usage of this function is deprecated and the code should be changed to use the Droplocker Relationships."));
        if (isset($this->{$this->primary_key})) {
            $CI->db->update($this->table_name, $this, array($this->primary_key => $this->properties[$this->primary_key]));

            return $CI->db->affected_rows();
        } else {
            throw new Exception("The primary key must be set before updating.");
        }
    }

    /**
     * The following method contains validation logic that should pass before deleting an object.
     * @throws Validation_Exception
     */
    public function validate_delete()
    {
        if (!isset($this->primary_key)) {
            throw new Validation_Exception("The primary key must be set before deleting");
        }
    }

    public function delete()
    {
        $CI = get_instance();
        $this->validate_delete();
        if (!$this->beforeDelete()) {
            return false;
        }

        $CI = get_instance();

        $CI->db->delete($this->table_name, array($this->primary_key => $this->{$this->primary_key}));
        if ($result = $CI->db->affected_rows()) {
            $this->afterDelete();
        }
        return $result;
    }

    /**
     * Before Delete Callback
     *
     * This callback is called before the delete() operation and can prevent it by returning false
     *
     * @return boolean
     */
    protected function beforeDelete() {
        return true;
    }

    /**
     * After Save Callback
     *
     * This callback is called after a successful delete() operation
     */
    protected function afterDelete() {
    }

    /**
     * @deprecated
     * create another object based on one column
     * must be a foreign key column (even though we don't have foreign keys)
     * example: locker_id in order table or location_id in locker table
     *
     * @param string $var
     */
    function find($var){
//        send_exception_report(new \BadMethodCallException("This usage of this function is deprecated and the code should be changed to use the Droplocker Relationships."));
        try {
            $class = ucfirst(str_replace("_id", "", $var));
            $class_path = sprintf("%s\%s", __NAMESPACE__, $class);

            return new $class_path($this->$var, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            return null;
        }
    }

    /**
     * This funciton is  . Do not use.
    * @deprecated use the Droplocker Relationships instead.
    *  the table that is passed as an argument HAS TO have a
    * many to one relationship with the calling object
    *
    * @param string $tableName
    */
    public function getData($tableName)
    {
        $CI = get_instance();

//        send_exception_report(new \BadMethodCallException("This usage of this function is deprecated and the code should be changed to use the Droplocker Relationships."));
        $CI = get_instance();
        $primary_key_name = $this->primary_key;
        $new_primary_key_name = str_replace("ID", "_id", $primary_key_name);
        $sql = "SELECT * FROM {$tableName} WHERE {$new_primary_key_name} = '{$this->{$this->primary_key}}'";
        $query = $CI->db->query($sql);
        if ($CI->db->_error_message()) {
            throw new Exception($CI->db->_error_message());
        }
        $result = $query->result();

        return $result;
    }

    /**
     * Searches for one row
     *
     * This returns only one row. If you need to get multiple rows use ::search_aprax()
     *
     * @param array $options
     * @param bool get_relationships If true, retrieves cardinal relationships
     * @param string order_by The name of the column to order by
     *
     * @return DroplockerObject or boolean false
     */
    public static function search($options, $get_relationships = false, $order_by = null)
    {
        $CI = get_instance();
        if (!is_array($options)) {
            throw new \Exception("'options' must be an array.");
        }

        $class = get_called_class();

        if (!empty($options)) {
            $CI->db->limit(1);

            if ($order_by) {
                $CI->db->order_by($order_by);
            }

            $query = $CI->db->get_where($class::$table_name, $options);
            $row = $query->row();
            return $row ? new $class($row, $get_relationships) : false;
        }

        return false;
    }

    /**
     * Searches for data within a particular table for a particular class.
     *
     * @param array $options
     * @param bool get_relationships If true, retrieves cardinal relationships
     * @param string order_by The name of the column to order by
     *
     * @return array An array consisting of object instances of the class for any resulting rows
     * @throws Exception
     */
    public static function search_aprax($options, $get_relationships = false, $order_by = null, $limit = false)
    {
        $CI = get_instance();
        $CI->db->reset_select();

        if (!is_array($options)) {
            throw new \InvalidArgumentException("'options' must be an array.");
        }
        $class = get_called_class();

        if ($order_by) {
            $CI->db->order_by($order_by);
        } else {
            if (isset($class::$primary_key)) {
                $CI->db->order_by($class::$primary_key, "ASC");
            }
        }

        if ($limit) {
            $CI->db->limit($limit);
        }

        $query = $CI->db->get_where($class::$table_name, $options);

        $rows = $query->result();
        $result = array();
        foreach ($rows as $row) {
            try {
                $result[] = new $class($row, $get_relationships);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                //If the item for some reason suddenly no longer exists, we just continue on as normally.
            }
        }

        return $result;
    }
    /**
     * Copies the properties of the object into a new object.
     * Note, the primary key is removed so that when saved, a new row would be inserted into the database.
     * @return \App\Libraries\DroplockerObjects\DroplockerObject
     */
    public function copy()
    {
        $copy = clone $this;
        $copy->changed = $copy->properties;
        $copy->{$this->primary_key} = null;

        return $copy;
    }
    /**
     * Updates the properties of an DroplockerObject with the latest changes in the database
     */
    public function refresh()
    {
        if (isset($this->relationships)) {
            $this->__construct($this->{$this->primary_key}, true);
        } else {
            $this->__construct($this->{$this->primary_key}, false);
        }
    }
    /**
     * Retreives the relationships and refreshses the properties of a DroplockerObject. This function is useful if the relationships were not retreived at the time the class was instantiated.
     */
    public function get_relationships()
    {
        $this->__construct($this->{$this->primary_key}, true);
    }

    /**
     * This function is the validation callback for saving the properties of an entity
     *  @return void if validation suceeds, throws a Validation_Exception if validation fails.
     * @throws Validation_Exception Thrown if the user is not a superadmin and the busines_id property's value of the entity matches the user session's business ID if the user is not a superadmin.
     * @throws InvalidArgumentException Thrown if a rule is not recognized
     */
    public function validate()
    {
        $CI = get_instance();
        if (isset($CI->business->businessID) && !is_superadmin()) {
            if (isset($this->business_id) && $this->business_id != $CI->business->businessID) {
                throw new Validation_Exception("$this->table_name  does not belong to the current business in the user's session");
            }

        }

        //The following loop processes the specified property rules for the entity
        foreach ($this->rules as $property_name => $property_rules) {
            foreach ($property_rules as $rule_name => $options) {
                if (is_string($options)) {
                    $rule = $options;
                    $options = array();
                } else if (is_array($options)) {
                    $rule = $rule_name;
                } else {
                    throw new \InvalidArgumentException("'options' must be a string or an array");
                }

                // method name is validateRuleName
                $method = 'validate' . str_replace(' ', '', ucwords(preg_replace('/[\s_]+/', ' ', $rule)));
                if (method_exists($this, $method)) {
                    $this->$method($property_name, $options);
                } else {
                    $class = get_class($this);
                    throw new \Exception("There is no validation rule named '$rule', add $class::$method()");
                }
            }
        }
    }

    /**
     * Validation rule for rule 'numeric'
     *
     * @param string $property_name
     * @param array $options
     * @throws Validation_Exception
     * @return boolean
     */
    protected function validateNumeric($property_name, $options)
    {
        if (!empty($this->$property_name)) {
            if (!is_numeric($this->$property_name)) {
                $userMessage = isset($options['userMessage']) ? $options['userMessage'] : null;
                throw new Validation_Exception("'$property_name' must be numeric", $userMessage);
            }
        }
        return true;
    }

    /**
     * Validation rule for rule 'entity_must_exist'
     *
     * @param string $property_name
     * @param array $options
     * @throws Validation_Exception
     * @return boolean
     */
    protected function validateEntityMustExist($property_name, $options)
    {
        if (is_numeric($this->$property_name)) {
            $matches = array();
            preg_match("/(.*)_id/", $property_name, $matches);
            $nouns = explode("_", $matches[1]);
            $capatilized_nouns = array_map(function ($word) {return ucfirst($word);}, $nouns);
            $entity_name = implode("_", $capatilized_nouns);
            Validation_Exception::verify_entity_exists($this->$property_name, $entity_name);
        }
        return true;
    }

    /**
     * Validation rule for rule 'required'
     *
     * @param string $property_name
     * @param array $options
     * @throws Validation_Exception
     * @return boolean
     */
    protected function validateRequired($property_name, $options)
    {
        if (empty($this->$property_name)) {
            $userMessage = isset($options['userMessage']) ? $options['userMessage'] : null;
            throw new Validation_Exception("'$property_name' can not be empty", $userMessage);
        }
        return true;
    }

    /**
     * Validation rule for rule 'unique'
     *
     * @param string $property_name
     * @param array $options
     * @throws Validation_Exception
     * @return boolean
     */
    public function validateUnique($property_name, $options)
    {
        $search = array(
            $property_name => $this->$property_name,
        );

        $unique = $property_name;
        if (!empty($options['fields'])) {
            foreach ($options['fields'] as $field) {
                $options[$field] = $this->$field;
                $unique .= ',' . $field;
            }
        }

        $class = get_class($this);
        $primaryKey = $class::$primary_key;
        if ($this->$primaryKey) {
            $search["$primaryKey !="] = $this->$primaryKey;
        }


        if ($class::search_aprax($search)) {
            $userMessage = isset($options['userMessage']) ? $options['userMessage'] : null;
            throw new Validation_Exception("'$unique' should be unique", $userMessage);
        }

        return true;

    }

    /**
     *  Should take an array of data structures an insert them after running the validate on each
     *  Use this to run bulk inserts without creating new objects, return all sucessfully inserted objects
     *  $insert_objects is an array of all objects to be inserted
     *  $return_on_success will return the objects that have been successfully inserted (with a 'new')
     */
    public static function bulk_insert( $insert_objects = array() , $return_on_sucess = false )
    {
        $CI = get_instance();                           //get the CI instance
        $total_received = count( $insert_objects );     //count how many came in
        $total_added = 0;                               //track the amount added
        $return_objects = array();                      //if theyw ant objects back we'll use this

        //insert the objects
        $class = get_called_class();

        foreach( $insert_objects as $insert_object ) {
            $CI->db->insert( $class::$table_name , $insert_object );

            $total_added += $CI->db->affected_rows();

        }

        //return all relevant data
        return array('sent' => $total_received, 'added' => $total_added , 'objects' => $return_objects );
    }


    /**
     * Convenience function create quickly creating a new derived droplocker object class instance
     * @param array $properties
     * @return \App\Libraries\DroplockerObjects\class
     */
    public static function create($properties) {
        $class = get_called_class();
        $droplockerObject = new $class();
        $droplockerObject->setProperties($properties, false);
        $droplockerObject->save();
        return $droplockerObject;
    }
    /**
     * Sets the properies of the object the values of the keys in the arroay
     * @param array $properties
     * @param bool save If true, will save the changes to the database
     */
    public function setProperties($properties, $save = TRUE)
    {
        $CI = get_instance();
        foreach ($properties as $key => $value) {
            if (!$CI->db->field_exists($key, $this->table_name)) {
                throw new \LogicException("{$this->table_name} has no column called '$key'");
            }
            $this->__set($key, $value);
        }

        if ($save) {
            $this->save();
        }
    }
    
    public function getId()
    {
        return $this->{$this->primary_key};
    }
}
