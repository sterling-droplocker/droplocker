<?php
namespace App\Libraries\DroplockerObjects;
class Image_Placement extends DroplockerObject
{
    public static $table_name = "image_placement";
    public static $primary_key = "image_placementID";
    public function validate()
    {
        if (!is_numeric($this->image_id)) {
            throw new Validation_Exception("'image_id' must be numeric.");
        }
        try {
            new Image($this->image_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Image ID '$this->image_id' not found");
        }

        if (!is_numeric($this->placement_id)) {
            throw new Validation_Exception("'placement_id' must be numeric");
        }

        try {
            new Placement($this->placement_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Placement ID '{$this->placement_id}' not found");
        }
    }
}
