<?php
namespace App\Libraries\DroplockerObjects;

class Day extends DroplockerObject
{
    public static $table_name = "day";
    public static $primary_key = "DayID";

    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }

        return true;
    }
}
