<?php
namespace App\Libraries\DroplockerObjects;
class EmailTemplate extends DroplockerObject
{
    public static $table_name = "emailTemplate";
    public static $primary_key = "emailTemplateID";
    public static $has_one = array("EmailAction");

    protected $rules = array("business_language_id" => array("entity_must_exist"));

    public function validate()
    {
        parent::validate();
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            new \App\Libraries\DroplockerObjects\Business($this->business_id, false);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found in database.");
        }
        if (!is_numeric($this->emailAction_id)) {
            throw new Validation_Exception("'emailAction_id' must be numeric.");
        }
        Validation_Exception::verify_entity_exists($this->emailAction_id, "EmailAction");
        $CI = get_instance();
        $CI->load->model("business_language_model");
        $business_language = $CI->business_language_model->get_by_primary_key($this->business_language_id);
        if ($business_language->business_id != $this->business_id) {
            throw new Validation_Exception("The business language specified in the email template is not associated to the business associated to the email template");
        }
    }
    /**
     *
     * @param int $source_business_id
     * @param int $destination_business_id
     * @throws \Exception
     */
    public static function copy_from_source_business_to_destination_business($source_business_id, $destination_business_id)
    {
        NotFound_Exception::does_exist($source_business_id, "Business");
        NotFound_Exception::does_exist($destination_business_id, "Business");

        if ($destination_business_id == $source_business_id) {
            throw new \Exception("The desintation business can not be the same as the source business");
        }

        $CI = get_instance();

        $destination_emailTemplates = self::search_aprax(array("business_id" => $destination_business_id));
        foreach ($destination_emailTemplates as $destination_emailTemplate) {
            $destination_emailTemplate->delete();
        }

        $destination_business_languages = Business_Language::search_aprax(array("business_id" => $destination_business_id));
        $source_emailTemplates = self::search_aprax(array("business_id" => $source_business_id));
        $CI->load->model("business_language_model");

        if (empty($destination_business_languages)) {
            $source_business_language = $CI->business_language_model->get_by_primary_key($source_emailTemplate->business_language_id);
            $destination_business_language = Business_Language::create($destination_business_id, $source_business_language->language_id);
            $destination_business_languages = array($destination_business_language);
        }

        foreach ($destination_business_languages as $destination_business_language) {
            foreach ($source_emailTemplates as $source_emailTemplate) {
                $destination_emailTemplate = $source_emailTemplate->copy();
                $destination_emailTemplate->business_id = $destination_business_id;
                $destination_emailTemplate->business_language_id = $destination_business_language->{Business_Language::$primary_key};
                $destination_emailTemplate->save();
            }
        }

    }
}
