<?php
namespace App\Libraries\DroplockerObjects;
class OrderPaymentStatus extends DroplockerObject
{
    public static $table_name = "orderPaymentStatus";
    public static $primary_key= "orderPaymentStatusID";
    
    protected $rules = array(
        "order_id" => array("numeric", "required", "entity_must_exist"),
        "orderPaymentStatusOption_id" => array("numeric", "required", "entity_must_exist")
    );
}
