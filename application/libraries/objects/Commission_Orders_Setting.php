<?php
namespace App\Libraries\DroplockerObjects;

class Commission_Orders_Setting extends DroplockerObject
{
    public static $table_name = "commission_orders_setting";
    public static $primary_key = "commission_orders_settingID";
    public function validate()
    {
        if (!is_numeric($this->employee_id)) {
            throw new Validation_Exception("'employee_id' must be numeric");
        }
        try {
            new Employee($this->employee_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Employee ID '{$this->employee_id}' not found in database.");
        }
        if (!is_numeric($this->index)) {
            throw new Validation_Exception("'index' must be numeric.");
        }
        if (!is_numeric($this->amount)) {
            throw new Validation_Exception("'amount' must be numeric.");
        }
    }
}
