<?php
namespace App\Libraries\DroplockerObjects;

class ItemHistory extends DroplockerObject
{
    public static $table_name = "itemHistory";
    public static $primary_key = "itemHistoryID";
    public function validate()
    {
        if (!is_numeric($this->item_id)) {
            throw new Validation_Exception("'item_id' must be numeric.");
        }
        try {
            new Item($this->item_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Item ID '{$this->item_id}' does not exist in database.");
        }
        if (empty($this->description)) {
            throw new Validation_Exception("'description' can not be empty.");
        }
    }
}
