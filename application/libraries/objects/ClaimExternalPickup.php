<?php
namespace App\Libraries\DroplockerObjects;

class ClaimExternalPickup extends DroplockerObject
{
    public static $table_name = "claimExternalPickup";
    public static $primary_key = "claimExternalPickupID";
    public static $belongsTo = array("Claim");
}