<?php
namespace App\Libraries\DroplockerObjects;

class CustomOptionValueName extends DroplockerObject
{

    public static $table_name = "customOptionValueName";
    public static $primary_key = "customOptionValueNameID";
    protected $rules = array(
        "customOptionValue_id" => array("numeric", "required", "entity_must_exist"),
        "name" => array("required"),
    );

    public static $belongsTo = array("CustomOption");

    public function validate()
    {
        parent::validate();

        $options = array(
            'customOptionValue_id' => $this->customOptionValue_id,
            'business_language_id' => $this->business_language_id
        );
        if ($this->customOptionValueNameID) {
            $options['customOptionValueNameID !='] = $this->customOptionValueNameID;
        }
        if (CustomOptionValueName::search_aprax($options)) {
            throw new Validation_Exception("('customOptionValue_id', business_language_id') should be unique");
        }

        return true;
    }

}
