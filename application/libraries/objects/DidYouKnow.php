<?php

namespace App\Libraries\DroplockerObjects;
class DidYouKnow extends DroplockerObject
{
    public static $table_name = "didYouKnow";
    public static $primary_key = "didYouKnowID";
    public function validate()
    {
        if (empty($this->note)) {
            throw new Validation_Exception("'note' can not be empty");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new Business($this->business_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found in database");
        }
        if (isset($this->base_id)) {
            $master_business = get_master_business();
            $didYouKnows = self::search_aprax(array("business_id" => $master_business->businessID, "didYouKnowID" => $this->base_id));
            if (empty($didYouKnows)) {
                throw new Validation_Exception("There is no {$this::$primary_key} referred to by the base ID");
            }
        }
    }
}
