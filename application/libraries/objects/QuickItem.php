<?php
namespace App\Libraries\DroplockerObjects;

class QuickItem extends DroplockerObject
{
    public static $table_name = "quickItem";
    public static $primary_key = "quickItemID";
    public static $has_one = array("Product", "Process");
    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }
        if (!is_numeric($this->product_id)) {
            throw new Validation_Exception("'product_id' must be numeric.");
        }

        if (!is_numeric($this->process_id)) {
            throw new Validation_Exception("'process_id' must be numeric.");
        }
    }

}
