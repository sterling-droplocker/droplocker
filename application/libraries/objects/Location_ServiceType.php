<?php

namespace App\Libraries\DroplockerObjects;
class Location_ServiceType extends DroplockerObject
{
    public static $table_name = "location_serviceType";
    public static $primary_key = "location_serviceTypeID";
    public static $belongsTo = array("Location_ServiceDay");
    public function validate()
    {
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
    }
}
