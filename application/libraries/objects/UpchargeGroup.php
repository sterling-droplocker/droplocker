<?php
namespace App\Libraries\DroplockerObjects;

class UpchargeGroup extends DroplockerObject
{
    public static $table_name = "upchargeGroup";
    public static $primary_key = "upchargeGroupID";


    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist")
    );

    /**
     * @deprecated This functionality does not belong in this class and does not following coding standards.
     * adds a new a upcharge group
     * @param String $name
     * @param int $business_id
     * @return int insert_id
     */
    public function addGroup($name, $business_id)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException("'name' must not be empty.");
        }

        $CI = get_instance();

        $sql = "INSERT IGNORE INTO upchargeGroup (name, business_id) VALUES ('".mysql_real_escape_string($name)."', ".$business_id.")";
        $CI->db->query($sql);

        return $CI->db->insert_id();
    }

    /**
     * @deprecated This functionality does not belong in this class and does not follow coding standards.
     * gets all the upcharges in a group with the supplier
     */
    public function getGroupUpcharges()
    {
        $CI = get_instance();

        $sql = "SELECT upcharge.name, upcharge.upchargePrice, upcharge.priceType, upcharge.upchargeID, business.companyName, upchargeSupplier.supplier_id, upchargeSupplier.cost, upcharge.everyOrder FROM upcharge
                LEFT JOIN upchargeSupplier ON upchargeID = upcharge_id AND `default` = 1
                LEFT JOIN supplier ON supplierID = supplier_id
                LEFT JOIN business ON supplierBusiness_id = businessID
                WHERE upchargeGroup_id = ?
                ORDER BY upcharge.name ASC";

        return $CI->db->query($sql, array($this->upchargeGroupID))->result();
    }


    public static function copy_from_source_business_to_destination_business($source_business_id, $destination_business_id)
    {
        NotFound_Exception::does_exist($source_business_id, "Business");
        NotFound_Exception::does_exist($destination_business_id, "Business");

        if ($destination_business_id == $source_business_id) {
            throw new \Exception("The desintation business can not be the same as the source business");
        } else {
            $destination_upchargeGroups = self::search_aprax(array("business_id" => $destination_business_id));
            foreach ($destination_upchargeGroups as $destination_upchargeGroup) {
                $destination_upchargeGroup->delete();
            }

            $source_upchargeGroups = self::search_aprax(array("business_id" => $source_business_id));
            foreach ($source_upchargeGroups as $source_upchargeGroup) {
                $destination_upchargeGroup = $source_upchargeGroup->copy();
                $destination_upchargeGroup->business_id = $destination_business_id;
                $destination_upchargeGroup->save();
                $source_upcharges = Upcharge::search_aprax(array("upchargeGroup_id" => $source_upchargeGroup->{self::$primary_key}));
                foreach ($source_upcharges as $source_upcharge) {
                    $destination_upcharge = $source_upcharge->copy();
                    $destination_upcharge->upchargeGroup_id = $destination_upchargeGroup->{self::$primary_key};
                    $destination_upcharge->save();
                }
            }
        }
    }
    /**
     * The following function deletes all upcharges in addition to the upcharge group to be deleted.
     */
    public function delete()
    {
        $upcharges = Upcharge::search_aprax(array("upchargeGroup_id" => $this->{self::$primary_key}));
        foreach ($upcharges as $upcharge) {
            $upcharge->delete();
        }
        parent::delete();
    }
}
