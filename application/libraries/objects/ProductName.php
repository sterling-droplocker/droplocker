<?php
namespace App\Libraries\DroplockerObjects;

class ProductName extends DroplockerObject
{
    public static $table_name = "productName";
    public static $primary_key = "productNameID";

    public static $belongsTo = array("Product");

    public function validate()
    {
        parent::validate();

        $options = array(
            'product_id' => $this->product_id,
            'business_language_id' => $this->business_language_id
        );

        if ($this->productNameID) {
            $options['productNameID !='] = $this->productNameID;
        }

        if (ProductName::search_aprax($options)) {
            throw new Validation_Exception("('product_id', business_language_id') should be unique");
        }

        return true;
    }

    public static function getNameByBusinessLanguageId($fallback_name, $product_id, $business_language_id)
    {
        try {
            $productNames = ProductName::search_aprax(array(
                'product_id' => $product_id
                ));

            foreach ($productNames as $productName) {
                if ($productName->business_language_id = $business_language_id) {
                    return $productName->title;
                    break;
                }
            }
        } catch (Exception $e) {
                //Nothing to do... Continue as always.
        }

        return $fallback_name;
    }

}
