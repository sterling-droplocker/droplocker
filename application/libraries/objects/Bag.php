<?php
namespace App\Libraries\DroplockerObjects;
class Bag extends DroplockerObject
{
    public static $table_name = "bag";
    public static $primary_key = "bagID";

    public function __construct($bagID = "", $get_relationships = false)
    {
        parent::__construct($bagID, $get_relationships);
        if (empty($bagID)) {
            $this->bagNumber = self::get_new_bagNumber();
        }
    }

    public static $has_one = array("Customer");

    /**
    * gets the max bag number from the settings table
    *
    * @return int the max bagnumber
    */
    public static function get_max_bagNumber()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings where setting='lastBarcode';";
        $setting = $CI->db->query($sql)->row();

        return $setting->value;
    }

    /**
     * Generated a valid new, unused bag number.
     * @return int The new bagnumber
     */
    public static function get_new_bagNumber()
    {
        $CI = get_instance();
        $new_bagNumber = Bag::get_max_bagNumber()+1;
        Bag::set_max_bagNumber($new_bagNumber);

        return $new_bagNumber;
    }
    /**
     * sets the max barcode in the settings table
     *
     * @param int $barcode@
     * @throws Exception for missing barcode or passed barcode is less than current barcode
     * @return int affected_rows
     */
    public static function set_max_bagNumber($barcode)
    {
        $CI = get_instance();
        if (empty($barcode)) {
            throw new \InvalidArgumentException("Missing barcode");
        }

        $originalBarcode = Bag::get_max_bagNumber();
        if ($originalBarcode >= $barcode) {
            throw new \InvalidArgumentException("The new barcode can not be less than the current max barcode");
        }

        $sql = "UPDATE settings set value = ? where setting = 'lastBarcode'";
        $CI->db->query($sql, array($barcode));

        return $CI->db->affected_rows();
    }


    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }
        try {
            new Customer($this->customer_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Customer ID '{$this->customer_id}' does not exist in the database.");
        }
        if (!is_numeric($this->bagNumber)) {
            throw new Validation_Exception("'bagNumber' must be numeric.");

        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        try {
            new Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found in database.");
        }
    }
}
