<?php
namespace App\Libraries\DroplockerObjects;
class BusinessLaundryPlan extends DroplockerObject
{
    public static $primary_key = "businessLaundryPlanID";
    public static $table_name = "businessLaundryPlan";

    /**
     *
     * @param int $source_business_id
     * @param int $destination_business_id
     * @throws \Exception
     */
    public static function copy_from_source_business_to_destination_business($source_business_id, $destination_business_id)
    {
        NotFound_Exception::does_exist($source_business_id, "Business");
        NotFound_Exception::does_exist($destination_business_id, "Business");

        if ($source_business_id == $destination_business_id) {
            throw new \Exception("The destination business can not be the same as the source business");
        }
        $destination_businessLaundryPlans = self::search_aprax(array("business_id" => $destination_business_id));
        foreach ($destination_businessLaundryPlans as $destination_businessLaundryPlan) {
            $destination_businessLaundryPlan->delete();
        }
        $source_businessLaundryPlans = self::search_aprax(array("business_id" => $source_business_id));
        foreach ($source_businessLaundryPlans as $source_businessLaundryPlan) {
            $destination_businessLaundryPlan = $source_businessLaundryPlan->copy();
            $destination_businessLaundryPlan->business_id = $destination_business_id;
            $destination_businessLaundryPlan->save();
        }
    }
    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new \App\Libraries\DroplockerObjects\Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID {$this->business_id} not found");
        }
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty");
        }
        if (!is_numeric($this->pounds)) {
            throw new Validation_Exception("'pounds' must be numeric");
        }
    }

    /**
     * Searches for one row
     *
     * This returns only one row. If you need to get multiple rows use ::search_aprax()
     *
     * @param array $options
     * @param bool get_relationships If true, retrieves cardinal relationships
     * @param string order_by The name of the column to order by
     *
     * @return DroplockerObject or boolean false
     */
    public static function search($options, $get_relationships = false, $order_by = null)
    {
        $CI = get_instance();
        if (!is_array($options)) {
            throw new \Exception("'options' must be an array.");
        }

        $class = get_called_class();

        if (!empty($options)) {
            $options["type"] = "laundry_plan";
            
            $CI->db->limit(1);

            if ($order_by) {
                $CI->db->order_by($order_by);
            }

            $query = $CI->db->get_where($class::$table_name, $options);
            $row = $query->row();
            return $row ? new $class($row, $get_relationships) : false;
        }

        return false;
    }

    /**
     * Searches for data within a particular table for a particular class.
     *
     * @param array $options
     * @param bool get_relationships If true, retrieves cardinal relationships
     * @param string order_by The name of the column to order by
     *
     * @return array An array consisting of object instances of the class for any resulting rows
     * @throws Exception
     */
    public static function search_aprax($options, $get_relationships = false, $order_by = null, $limit = false)
    {
        $CI = get_instance();
        $CI->db->reset_select();

        if (!is_array($options)) {
            throw new \InvalidArgumentException("'options' must be an array.");
        }

        $options["type"] = "laundry_plan";

        $class = get_called_class();

        if ($order_by) {
            $CI->db->order_by($order_by);
        } else {
            if (isset($class::$primary_key)) {
                $CI->db->order_by($class::$primary_key, "ASC");
            }
        }

        if ($limit) {
            $CI->db->limit($limit);
        }

        $query = $CI->db->get_where($class::$table_name, $options);

        $rows = $query->result();
        $result = array();
        foreach ($rows as $row) {
            try {
                $result[] = new $class($row, $get_relationships);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                //If the item for some reason suddenly no longer exists, we just continue on as normally.
            }
        }

        return $result;
    }
}
