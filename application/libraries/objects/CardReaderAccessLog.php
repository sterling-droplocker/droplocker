<?php
namespace App\Libraries\DroplockerObjects;

class CardReaderAccessLog extends DroplockerObject
{
    public static $primary_key = "cardReaderAccessLogID";
    public static $table_name = "cardReaderAccessLog";

}
