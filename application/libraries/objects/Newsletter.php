<?php
namespace App\Libraries\DroplockerObjects;
class Newsletter extends DroplockerObject
{
    public static $table_name = "newsletter";
    public static $primary_key = "newsletterID";

    public function __construct($id = '', $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
    }
    public function validate()
    {

    }

    public function fillQueue($customerType, $business_id, $newsletter_id)
    {
        $CI = get_instance();
        if ($customerType ==  "allCustomers") {
            $sql = "SELECT * FROM customer WHERE business_id = {$business_id} AND noEmail != 1 AND inactive != 1 ORDER BY customerID DESC;";
        } else {
            $getFilter = "select * from filter where filterID = $customerType";
            $filter = $CI->db->query($getFilter)->row();
            $sql = $filter->query . ' AND noEmail != 1 AND inactive != 1 ORDER BY customerID DESC';
        }

        $customers = $CI->db->query($sql)->result();
        foreach ($customers as $customer) {
            $insert = array('customer_id'=>$customer->customerID,
                            'newsletter_id'=>$newsletter_id);
            $inserts[] = $insert;
        }

        $CI->db->insert_batch("newsletterQueue", $inserts);

        return array('insert_id'=>$CI->db->insert_id(), 'totalRows'=>sizeof($inserts));

    }
}
