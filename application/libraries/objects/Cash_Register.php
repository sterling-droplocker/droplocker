<?php

namespace App\Libraries\DroplockerObjects;
class Cash_Register extends DroplockerObject
{
    public static $table_name = "cash_register";
    public static $primary_key = "cash_registerID";

}
