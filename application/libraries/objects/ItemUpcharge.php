<?php
namespace App\Libraries\DroplockerObjects;
class ItemUpcharge extends DroplockerObject
{
    public static $table_name = "itemUpcharge";
    public static $primary_key = "itemUpchargeID";
    public static $has_one = array("Item", "Product");

    public function __construct($id = '', $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
    }
    public function validate()
    {
       if (!is_numeric($this->item_id)) {
           throw new Validation_Exception("'item_id' must be numeric.");
       }
       if (!is_numeric($this->product_id)) {
           throw new Validation_Exception("'product_id' must be numeric.");
       }
       try {
           new Item($this->item_id, false);
       } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
           throw new Validation_Exception("Item ID '{$this->item_id}' not found in database.");
       }
       try {
           new Product($this->product_id);
       } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
           throw new Validation_Exception("Product ID '{$this->product_id}' not found in database.");
       }
    }
}
