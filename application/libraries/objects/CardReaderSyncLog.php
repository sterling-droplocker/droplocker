<?php
namespace App\Libraries\DroplockerObjects;

class CardReaderSyncLog extends DroplockerObject
{
    public static $primary_key = "cardReaderSyncLogID";
    public static $table_name = "cardReaderSyncLog";

}
