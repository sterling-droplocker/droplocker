<?php
namespace App\Libraries\DroplockerObjects;

class TicketStatus extends DroplockerObject
{
    public static $table_name = "ticketStatus";
    public static $primary_key = "ticketStatusID";



    public function validate()
    {
        if (empty($this->description)) {
            throw new Validation_Exception("description is required.");
        }

        if (empty($this->responseTime)) {
            throw new Validation_Exception("responseTime is required.");
        }
    }
}
