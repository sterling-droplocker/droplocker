<?php
namespace App\Libraries\DroplockerObjects;
class Testimonial extends DroplockerObject
{
    public static $table_name = "testimonial";
    public static $primary_key = "testimonialID";

    public function __construct($testimonialID = '')
    {
        parent::__construct($testimonialID);
    }

    public function validate()
    {
        //throw new Exception("Not Implemented.");
    }

    /**
     * lists all the testimonials for a business
     *
     * @param unknown_type $business_id
     * @throws Exception
     */
    public function listAll($business_id='')
    {
        $CI = get_instance();
        $business_id = (empty($business_id))?$CI->session->userdata('business_id'):$business_id;
        if (empty($business_id)) {
            throw new \InvalidArgumentException("Missing business ID");
        }

        $CI->db->order_by('testimonialID', 'DESC');
        $CI->db->where(array('business_id'=>$business_id));

        return $CI->db->get('testimonial')->result();

    }
}
