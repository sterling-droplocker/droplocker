<?php
namespace App\Libraries\DroplockerObjects;
class Transaction extends DroplockerObject
{
    public static $table_name = "transaction";
    public static $primary_key = "transactionID";
    public static $has_one = array("Customer", "Business", "Order");

    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric");
        } else {
            try {
                new Customer($this->customer_id, false);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("Customer ID '{$this->customer_id}' not found in database.");
            }
        }

        if (!is_numeric($this->order_id)) {
            throw new Validation_Exception("'order_id' must be numeric.");
        } else {
            try {
                new Order($this->order_id, false);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("Order ID '{$this->order_id}' not found in database.");
            }

        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        } else {
            try {
                new Business($this->business_id, false);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("Business ID '{$this->business_id}' not found in database.");
            }
        }

        if (empty($this->pnref)) {
            throw new Validation_Exception("'pnref' can not be empty.");
        }
        if (empty($this->type)) {
            throw new Validation_Exception("'type' can not be empty.");
        }
        if (!is_int((int) $this->resultCode)) {
            throw new Validation_Exception("'resultCode' must be an integer.");
        }

        if (empty($this->message)) {
            throw new Validation_Exception("'message' can not be empty");
        }
    }

    /**
     * $days = the number of days to go back in history. In case a customer calls and says they were double charged
     *
     * @param int $customer_id
     * @param int $days
     */
    public function getDoubleCharges($business_id, $days = 5)
    {
        $CI = get_instance();
        $business = new Business($business_id);
        $sql = "SELECT count(order_id) as charges, transaction.amount, firstName, lastName, order_id, customerID, updated
                FROM transaction
                INNER JOIN customer on customer_id = customerID
                WHERE transaction.business_id = {$business_id}
                AND resultCode in (1,0)
                AND updated >= date_sub(NOW(), INTERVAL {$days} DAY)
                GROUP BY order_id, customer_id
                HAVING count(order_id) > 1
                ORDER BY updated DESC";

        return $CI->db->query($sql)->result();

    }
}
