<?php
namespace App\Libraries\DroplockerObjects;
class Coupon extends DroplockerObject
{
    public static $table_name = "coupon";
    public static $primary_key = "couponID";

    public static $amountTypes = array(
        "percent" => "Percent",
        "dollars" => "Dollars",
        "pounds" => "Pounds",
        "item" => "Item $",
        "itemPercent" => "Item %",
        "product_plan_percent" => "product_plan_percent",
        "product_plan" => "product_plan"
    );

    public static $frequencies = array(
        "every order" => "Every order",
        "one time" => "One time",
        "until used up" => "Until used up",
        "all customers" => "All Customers"
    );

    public static $first_time_fields = array(
        "0" => "Any order",
        "1" => "First Order"
    );

    public static $has_one = array("Recurring_Type", "Business");

    protected $rules = array(
        "prefix" => array("required"),
        "code" => array("required"),
        "business_id" => array("required")
    );

    public static function get_amountTypes()
    {
        $amountTypes = array();
        foreach (self::$amountTypes as $key => $value) {
            $amountTypes[$key] = get_translation($key, "Coupons Amount Types", array(), $value);
        }

        return $amountTypes;
    }

    public static function get_frequencies()
    {
        $frequencies = array();
        foreach (self::$frequencies as $key => $value) {
            $frequencies[$key] = get_translation($key, "Coupons Frequencies", array(), $value);
        }

        return $frequencies;
    }

    public static function get_first_time_fields()
    {
        $first_time_fields = array();

        foreach (self::$first_time_fields as $key => $value) {
            $first_time_fields[$key] = get_translation($value, "Coupons First Time", array(), $value);
        }

        return $first_time_fields;
    }

    public static function get_recurring_types()
    {
        $CI = get_instance();
        $CI->load->model("recurring_type_model");
        $recurring_types = $CI->recurring_type_model->get_recurring_types_as_codeigniter_dropdown();

        foreach ($recurring_types as $key => $value) {
            $recurring_types[$key] = get_translation($value, "Coupons Recurring Types", array(), ucfirst($value));
        }

        return $recurring_types;
    }

    public function save()
    {
        $id = parent::save();

        return $id;
    }

    public function validate()
    {
        if (!empty($this->startDate)) {
            if (!$this->startDate instanceOf \DateTime) {
                throw new Validation_Exception("'startDate' must be instance of DateTime");
            }
        }
        if (!empty($this->endDate)) {
            if (!($this->endDate instanceOf \DateTime)) {
                throw new Validation_Exception("'endDate' must be instance of DateTime");
            }
        }
        /*
        if (!array_key_exists($this->amountType, self::$amountTypes)) {
            throw new Validation_Exception("amountType '{$this->amountType}' is not a valid amount type.");
        }
         *
         */

        $CI = get_instance();
        //The following statements check to see if we are setting the amount type to item percent or item dollar. If so, then we check to see if there are no customer discounts already associated with the coupon. If there are customer discounts already associated with the coupon, then we throw a validation exception indicating that we can not change the amount type because of the existing customer discounts.
        $old_coupon = new Coupon($this->{self::$primary_key});
        $old_amountType = $old_coupon->amountType;
        $new_amountType = $this->amountType;
        if ($old_amountType != $new_amountType) {
            if ($this->amountType == "item" || $this->amountType == "itemPercent") {
                $CI->load->model("customerdiscount_model");
                $customerDiscounts_count = $CI->customerdiscount_model->count(array("couponCode" => $this->prefix . $this->code));
                if ($customerDiscounts_count > 0) {
                    throw new Validation_Exception("Can not sent amount type from '$old_amountType' to '$new_amountType' because there are $customerDiscounts_count customer discounts associated with this coupon");
                }
            }
        }
        /*
        if (!array_key_exists($this->frequency, self::$frequencies)) {
            throw new Validation_Exception("'{$this->frequency}' is not a valid frequency.");
        }
         *
         */
        if (!is_numeric($this->amount)) {
            throw new Validation_Exception("'amount' must be numeric.");
        }
        /* 
        if ($this->amount < 0) {
            throw new Validation_Exception("'amount' can not be less than 0");
        }
        */
        parent::validate();
    }

}
