<?php
namespace App\Libraries\DroplockerObjects;
class ProductCategory extends DroplockerObject
{
    public static $table_name = "productCategory";
    public static $primary_key = "productCategoryID";

    /**
     * Creates the special Wash and Fold product category.
     * @param int $businessID
     * @return \App\Libraries\DroplockerObjects\ProductCategory
     * @throws \LogicException
     */
    public static function createWashandFold($businessID)
    {
        $CI = get_instance();
        $CI->load->model("productcategory_model");
        if ($CI->productcategory_model->does_exist(array("slug" => "wf", "business_id" => $businessID))) {
            throw new \LogicException("Wash and Fold product category already exists");
        } else {
            $washfold_productCategory = new ProductCategory();
            $washfold_productCategory->name = "Wash and Fold";
            $washfold_productCategory->slug = "wf";
            $washfold_productCategory->business_id = $businessID;
            $washfold_productCategory->module_id = 1;
            $washfold_productCategory->save();

            return $washfold_productCategory;
        }
    }
    public function validate()
    {
        if (empty($this->slug)) {
            throw new Validation_Exception("'slug' can not be empty.");
        }
        if (empty($this->name)) {
            throw new Validation_Exception("'name' can not be empty.");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");

        }
        if (!is_numeric($this->module_id)) {
            throw new Validation_Exception("'module_id' must be numeric");
        }
        try {
            NotFound_Exception::does_exist($this->module_id, "Module");
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Module ID '{$this->module_id}' does not exist in database");
        }
        try {
            NotFound_Exception::does_exist($this->business_id, "Business");
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' does not exist in database.");
        }
    }
    /**
     * Returns the default product for this category.
     * @return \App\Libraries\DroplockerObjects\Product
     * @throws Database_Exception
     */
    public function get_default_product_preference()
    {
        $CI = get_instance();
        $CI->db->select("productID");
        $CI->db->where("business_id", $this->business_id);
        $CI->db->where("productCategory_id", $this->productCategoryID);
        $CI->db->order_by("sortOrder");
        $CI->db->limit(1);
        $get_default_product_preference_query = $CI->db->get(Product::$table_name);

        $get_default_product_preference_result = $get_default_product_preference_query->row();
        $product = new Product($get_default_product_preference_result->productID);

        return $product;
    }
    /**
     * Returns all the products associated with the current product category
     * @return array An array of Product objects
     */
    public function get_products()
    {
        $products = Product::search_aprax(array("productCategory_id" => $this->{self::$primary_key}));

        return $products;
    }


    /**
     * Get the translations for this product category
     *
     * @return array
     */
    public function getProductCategoryNames()
    {
        $out = array();
        $found = ProductCategoryName::search_aprax(array(
            'productCategory_id' => $this->productCategoryID
        ));

        foreach ($found as $productCategoryName) {
            $out[$productCategoryName->business_language_id] = $productCategoryName;
        }

        return $out;
    }

    /**
     * Saves the transtalions for a product category
     *
     * $names is an array with $businesLanguageID as key and title as value
     *
     * @param array $names
     * @return array
     */
    public function saveProductCategoryNames($names)
    {
        $productCategoryNames = $this->getProductCategoryNames();
        $out = array();
        foreach ($names as $businesLanguageID => $title) {
            if (isset($productCategoryNames[$businesLanguageID])) {
                $productCategoryName = $productCategoryNames[$businesLanguageID];
                if (empty($title)) {
                    $productCategoryName->delete();
                    continue;
                }

            } else {
                if (empty($title)) {
                    continue;
                }

                $productCategoryName = new ProductCategoryName();
                $productCategoryName->productCategory_id = $this->productCategoryID;
                $productCategoryName->business_language_id = $businesLanguageID;
            }

            //var_dump($productCategoryName); die();

            $productCategoryName->title = $title;
            $productCategoryName->save();

            $out[$businesLanguageID] = $productCategoryName;
        }

        return $out;
    }
}
