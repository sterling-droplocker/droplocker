<?php
namespace App\Libraries\DroplockerObjects;

class CardReader extends DroplockerObject
{
    public static $primary_key = "cardReaderID";
    public static $table_name = "cardReader";

    protected $rules = array(
        "location_id" => array("numeric", "required", "entity_must_exist", "unique"),
        "address" => array("required"),
        "port" => array("numeric", "required"),
        "masterPort" => array("numeric", "required"),
        "password" => array("required"),
    );

    public static function listModes()
    {
        return array(
            "registered" => "Open to Registered",
            "open" => "Open To All",
            "window" => "Window"
        );
    }

    public static function listModels()
    {
        return array(
            '8000' => '8000'
        );
    }
}
