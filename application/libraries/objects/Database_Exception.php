<?php
class Database_Exception extends Exception
{
    protected $message; //Stores the raw error message from the database provider
    private $query; //Stores the raw SQL query
    private $error_code; //Stores the SQL error code
    public function __construct($message, $query, $error_code)
    {
        $this->message = $message;
        $this->query = $query;
        $this->error_code = $error_code;
        parent::__construct($message);
    }
    /**
     * Returns the raw SQL query that was made
     * @return string
     */
    public function get_query()
    {
        return $this->query;
    }
    /**
     * Returns the SQL error number from the failed query
     * @return int
     */
    public function get_error_number()
    {
        return $this->error_code;
    }
}
