<?php

namespace App\Libraries\DroplockerObjects;
class KioskAccess extends DroplockerObject
{
    public static $table_name = "kioskAccess";
    public static $primary_key = "kioskAccessID";

    public function validate()
    {
        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }
        if (!is_numeric($this->cardNumber)) {
            throw new Validation_Exception("'cardNumber' must be numeric");
        }
        if (!is_numeric($this->expMo)) {
            throw new Validation_Exception("'expMo' must be numeric");
        }
        if (!is_numeric($this->expYr)) {
            throw new Validation_Exception("'expYr' must be numeric.");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
    }
}
