<?php
namespace App\Libraries\DroplockerObjects;

class Location_Neighborhood extends DroplockerObject
{
    public static $table_name = "location_neighborhood";
    public static $primary_key = "location_neighborhoodID";
    public static $has_one = array("Neighborhood");

    protected $rules = array(
        "business_id" => array("numeric", "required", "entity_must_exist"),
        "location_id" => array("numeric", "required", "entity_must_exist"),
        "neighborhood" => array("required")
    );
}
