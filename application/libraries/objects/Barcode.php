<?php
namespace App\Libraries\DroplockerObjects;
class Barcode extends DroplockerObject
{
    public static $table_name = "barcode";
    public static $primary_key = "barcodeID";
    public static $has_one = "Item";

    /**
    * gets the active order for a barcode (item)
    *
    * @param int $business_id
    * @throws Exception
    * @return int $order_id
    */
    public function getActiveOrder($business_id = "")
    {
        $CI = get_instance();
        $business_id = ($business_id=='')?$CI->session->userdata('business_id'):$business_id;

        if (!$this->{self::$primary_key}) {
            throw new \Exception(self::$primary_key . " can not be empty.");
        }

        if (empty($business_id)) {
            throw new \Exception('Missing business ID');
        }

        $CI->db->select_max("order_id", "order_id");
        $CI->db->join("orderItem", "orderItem.item_id=barcode.item_id");
        $CI->db->join("orders", "orderID=order_id");
        $CI->db->where("barcodeID", $this->{self::$primary_key});
        $CI->db->where("orders.business_id", $business_id);
        $CI->db->where_not_in("orders.orderStatusOption_id", array(9,10));

        $result = $CI->db->get(self::$table_name);
        if ($CI->db->_error_message()) {
            throw new \Exception($CI->db->_error_message());
        }
        $line = $result->row();

        return $line->order_id;
    }


    /**
    * gets the most recent order for a barcode (item)
    *
    * @param int $business_id Optional, if missing will try to use the session
    * @throws Exception
    * @return int $order_id
    */
    public function getMostRecentOrder($business_id = '')
    {
        $CI = get_instance();
        $business_id = ($business_id=='')?$CI->session->userdata('business_id'):$business_id;

        if (empty($business_id)) {
            throw new \Exception('Missing business ID');
        }

        $CI->db->select_max("order_id", "order_id");
        $CI->db->join("orderItem", "orderItem.item_id=barcode.item_id");
        $CI->db->join("orders", "orderID=order_id");
        $CI->db->where("barcodeID", $this->{self::$primary_key});
        $CI->db->where("orders.business_id", $business_id);

        $query = $CI->db->get(self::$table_name);

        if ($CI->db->_error_message()) {
            throw new \Exception($CI->db->_error_message());
        }

        $line = $query->row();

        return $line->order_id;
    }


    /**
        * gets the customers for a barcode (item)
        *
        * @param int $business_id Optional, if not set will try to use the session business_id
        * @throws Exception
        * @return array of objects
        */
    public function getCustomer($business_id = '')
    {
        $CI = get_instance();
        $business_id = ($business_id=='')?$CI->session->userdata('business_id'):$business_id;

        if (empty($business_id)) {
            throw new \InvalidArgumentException('Missing business ID');
        }

        $CI->db->join("customer_item", "customer_item.item_id=barcode.item_id");
        $CI->db->join("customer", "customerID=customer_item.customer_id");
        $CI->db->where("barcodeID", $this->{self::$primary_key});
        $CI->db->where("barcode.business_id", $business_id);
        $query = $CI->db->get(self::$table_name);


        if ($CI->db->_error_message()) {
            throw new \Exception($CI->db->_error_message());
        }

        return $query->result();
    }
    /**
     *
     * @return boolean
     * @throws Validation_Exception
     */
    public function validate()
    {
        $CI = get_instance();
        $CI->load->model("barcodelength_model");

        if (!is_numeric($this->barcode)) {
            throw new Validation_Exception("Barcode must be numeric.");
        }
        if (!$CI->barcodelength_model->is_valid_barcode_length($this->barcode, $this->business_id)) {
            throw new Validation_Exception("Barcode is not valid length.");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("business_id must be numeric.");
        } else {
            try {
                $business = new Business($this->business_id);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("Business ID {$this->business_id} does not exist in the database.");
            }

            if (empty($business)) {
                throw new Validation_Exception("Business not found.");
            }
        }
        try {
            $item = new Item($this->item_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("item ID Not found in database.");
        }
    }

}
