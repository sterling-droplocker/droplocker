<?php

namespace App\Libraries\DroplockerObjects;

class OrderHomeDelivery extends DroplockerObject
{
    public static $table_name = "orderHomeDelivery";
    public static $primary_key = "orderHomeDeliveryID";

    public function validate()
    {
        if (!is_numeric($this->order_id)) {
            throw new Validation_Exception("'order_id' must be numeric.");
        }

        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }

        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }
    }
}
