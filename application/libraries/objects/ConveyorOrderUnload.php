<?php
namespace App\Libraries\DroplockerObjects;

class ConveyorOrderUnload extends DroplockerObject
{
    public static $has_one = array("Order");

    public static $tableName = 'conveyorOrderUnload';
    public static $primary_key = 'conveyorOrderUnloadID';

    protected $rules = array(
        "order_id" => array("required", "entity_must_exist", "numeric")
    );

}
