<?php
namespace App\Libraries\DroplockerObjects;
class EmailLog extends DroplockerObject
{
    public static $table_name = "emailLog";
    public static $primary_key = "emailLogID";
}
