<?php
namespace App\Libraries\DroplockerObjects;
class Employee extends DroplockerObject
{
    public static $table_name = "employee";
    public static $primary_key = "employeeID";
    public static $belongsTo = array("Business_Employee");

    public function __construct($employeeID = '', $get_relationships = false)
    {
    	
        parent::__construct($employeeID, $get_relationships);

        if ($get_relationships && !empty($employeeID) && !empty($this->commission)) {
            $this->relationships['Named_Accounts'] = Customer::search_aprax(array("owner_id" => $this->{self::$primary_key}), false);
        }
    }

    /**
     * gets the name of the employee
     * @param int $employee_id
     */

    public static function getName($employee_id)
    {
       $employee = new \App\Libraries\DroplockerObjects\Employee($employee_id);

       return $employee->firstName. " ".$employee->lastName;
    }
    /**
    * Gets the business_employee_id
    *
    * @param int $business_id
    * @return int buser_id
    */
    public function getBusinessEmployeeID($business_id='')
    {
        $CI = get_instance();
        $business_id = ($business_id=='')?$CI->session->userdata('business_id'):$business_id;
        if ($business_id == '') {
            throw new \Exception("Missing business ID");
        }

        $id = $this->employeeID;
        $row = $CI->db->get_where('business_employee', array('business_id'=>$business_id, 'employee_id'=>$id))->row();

        return $row->ID;
    }
    /**
     * The following function ensures that the password is hashed with MD5 before saving it to the database if the password is new.
     */
    public function save()
    {
        $existing_employee = new Employee($this->{self::$primary_key});
        if ($this->password != $existing_employee->password) {
            $this->password = md5($this->password);
        }

       return  parent::save();
    }
    public function validate()
    {
        if (!empty($this->manager_id) && !is_numeric($this->manager_id)) {
            throw new Validation_Exception("'manager_id' must be numeric.");
        }
        if (empty($this->email)) {
            throw new Validation_Exception("'email' can not be empty.");
        }

        $CI = get_instance();
        $CI->load->model("employee_model");

        //The following conditional ensures that we are not adding a a new employee with the same e-mail as an existing employee.
        if (empty($this->employeeID)) {
            $CI->db->where("email", $this->email);
            $CI->db->from("employee");
            if ($CI->db->count_all_results() > 0) {
                throw new Validation_Exception("An employee with email '{$this->email}' already exists");
            }
        }

        if (empty($this->password)) {
            throw new Validation_Exception("'password' can not be empty.");
        }
    }
    
    public function getBusinessId()
    {
        $CI = get_instance();
        $row = $CI->db->get_where('business_employee', array(
            'default' => 1, 
            'employee_id' => $this->employeeID)
        )->row();
        
        return $row->business_id;
    }

}
