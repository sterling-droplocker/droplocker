<?php
namespace App\Libraries\DroplockerObjects;

class OrderCharge extends DroplockerObject
{
    public static $table_name = "orderCharge";
    public static $primary_key = "orderChargeID";

    public function validate()
    {
        if (!is_numeric($this->order_id)) {
            throw new Validation_Exception("'order_id' must be numeric.");
        }
    }
}
