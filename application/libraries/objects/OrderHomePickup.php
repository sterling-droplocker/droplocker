<?php

namespace App\Libraries\DroplockerObjects;

class OrderHomePickup extends DroplockerObject
{
    public static $table_name = "orderHomePickup";
    public static $primary_key = "orderHomePickupID";

    public function validate()
    {
        if (!is_numeric($this->order_id)) {
            throw new Validation_Exception("'order_id' must be numeric.");
        }

        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }

        if (!is_numeric($this->customer_id)) {
            throw new Validation_Exception("'customer_id' must be numeric.");
        }
    }

    function cancel()
    {
        if ($this->pickup_id) {
            $service = \DropLocker\Service\HomeDelivery\Factory::getService($this->service, $this->business_id);
            $service->cancelPickup($this);
        }
    }

}
