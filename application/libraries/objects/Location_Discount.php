<?php

namespace App\Libraries\DroplockerObjects;
class Location_Discount extends DroplockerObject
{
    public static $table_name = "location_discount";
    public static $primary_key = "locationDiscountID";
    public static $has_one = array("Location");
    protected $rules = array(
        "location_id" => array("numeric", "required", "entity_must_exist")
    );

}
