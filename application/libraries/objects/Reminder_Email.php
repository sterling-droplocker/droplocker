<?php

namespace App\Libraries\DroplockerObjects;

class Reminder_Email extends DroplockerObject
{
    public static $table_name = "reminder_email";
    public static $primary_key = "reminder_emailID";

    protected $rules = array(
        "business_language_id" => array("numeric", "required", "entity_must_exist"),
        "reminder_id" => array("numeric", "required", "entity_must_exist"),
        "subject" => array("required"),
        "body" => array("required")
    );
    /**
     * This is a convenience function for creating a new reminder email.
     * @param int $business_language_id
     * @param int $reminder_id
     * @param string $subject
     * @param string $body
     */
    public static function create($business_language_id, $reminder_id, $subject, $body)
    {
        $reminder_email = new Reminder_Email();
        $reminder_email->business_language_id = $business_language_id;
        $reminder_email->reminder_id = $reminder_id;
        $reminder_email->subject = $subject;
        $reminder_email->body = $body;
        $reminder_email->save();
    }
    public function validate()
    {
        parent::validate();
        $CI = get_instance();
        $CI->load->model("reminder_model");
        $reminder = $CI->reminder_model->get_by_primary_key($this->reminder_id);
        $CI->load->model("business_language_model");
        $business_language = $CI->business_language_model->get_by_primary_key($this->business_language_id);
        if ($reminder->business_id != $business_language->business_id) {
            $CI->load->model("business_model");
            $business_language_business = $CI->business_model->get_by_primary_key($business_language->business_id);
            $reminder_business = $CI->load->business_model->get_by_primary_key($reminder);
            throw new Validation_Exception("The business associated with the business '{$business_language_business->companyName}' language does not match the business '{$reminder_business->companyName}' associatd with the reminder");
        }
    }
}
