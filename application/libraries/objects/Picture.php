<?php
namespace App\Libraries\DroplockerObjects;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Picture
 *
 * @author ariklevy
 */
class Picture extends DroplockerObject
{
    public static $table_name = "picture";
    public static $primary_key = "pictureID";

    public function validate()
    {
        if (!is_numeric($this->item_id)) {
            throw new Exception("Item ID must be numeric.");
        }
        try {
            new Item($this->item_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Item ID '{$this->item_id}' not found in database.");
        }

        if (empty($this->file)) {
            throw new Validation_Exception("'file' can not be empty.");
        }

        if (!is_numeric($this->employee_id)) {
            throw new Validation_Exception("'employee_id' can not be empty.");
        }

        try {
            new Employee($this->employee_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Employee ID '{$this->employee_id}' not found in database.");
        }
    }
}
