<?php
namespace App\Libraries\DroplockerObjects;
class DriverNotes extends DroplockerObject
{
    public static $table_name = "driverNotes";
    public static $primary_key = "driverNotesID";

    public function validate()
    {
    }

    /**
     * Static function to get driver notes
     *
     * @param int $business_id
     * @return array of objects
     */
    public static function view($business_id='', $active = 1, $limit='')
    {
        $CI = get_instance();

        $sql = (!empty($business_id))?" and driverNotes.business_id = $business_id ":"";
        $limit = (!empty($limit))?" LIMIT {$limit}":"";

        $sqlNotes = "SELECT location.route_id,
        location.address as locationAddress,
        location.companyName,
        note,
        driverNotesID as notesId,
        employeeID,
        driverNotes.created,
        (SELECT count(*) FROM droid_postDriverNotes WHERE note_id = driverNotesID) as driverResponseCount
        from driverNotes
        join location on locationID = driverNotes.location_id
        left join employee on employeeID = driverNotes.createdBy
        where active = $active
        $sql
        order by created desc
        {$limit}";
        $result = $CI->db->query($sqlNotes)->result();

        return $result;


    }

    /**
     * gets all the responses from the drivers
     *
     * @return array of objects
     */
    public function driverResponses()
    {
        $CI = get_instance();
        $sql = "SELECT * FROM droid_postDriverNotes where note_id = {$this->{self::$primary_key}}";

        return $CI->db->query($sql)->result();
    }
}
