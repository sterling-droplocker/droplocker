<?php

namespace App\Libraries\DroplockerObjects;
class Cash_Register_Record extends DroplockerObject
{
    public static $table_name = "cash_register_record";
    public static $primary_key = "cash_register_recordID";

}
