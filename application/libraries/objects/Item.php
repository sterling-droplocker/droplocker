<?php
namespace App\Libraries\DroplockerObjects;


class Item extends DroplockerObject
{
    public static $table_name = "item";
    public static $primary_key = "itemID";

    public static $belongsTo = array("Picture", "Barcode", "ItemHistory", "ItemIssue", "ItemUpcharge", "OrderItem", 'Customer_Item');
    public static $has_one = array("Product_Process");

    /**
     * The following method is *, do not use.
     * @deprecated Use the models for retrieving data, not the Droplocker Objects.
     * Returns the picture URL for a particular item.
     * @return string|null Returns a string if the picture exists, or null if the picture doe snot exist.
     */
    public function getPicture()
    {
        $CI = get_instance();
        $CI->load->model("picture_model");
        $primary_key = $this->primary_key;
        $CI->picture_model->item_id = $this->{self::$primary_key};
        $file = $CI->picture_model->get();

        return isset($file[0]->file)?$file[0]->file:'';
    }

    /**
     * The following method is *. Do not use.
     * @deprecated use the model for retrieving data, not the Droplocker Objects.
     * @return type
     */
    public function getCustomer()
    {
        $CI = get_instance();
        $primary_key = $this->primary_key;

        $sql = "SELECT customer.* from customer
                    INNER JOIN customer_item ci ON ci.customer_id = customerID
                    WHERE ci.item_id = {$this->{self::$primary_key}}";
        $query= $CI->db->query($sql);

        return $query->result();
    }

    /**
     * The following method is *, do not use.
     * @deprecated Use the models for retrieving data, not the Droplocker Objects.
     * gets all the orders that this item was in
     *
     * @return array of objects
     */
    public function getOrdersWithItem()
    {
        if (empty($this->{self::$primary_key})) {
            throw new \Exception("Missing primary key for item");
        }

        $CI = get_instance();
        $sql = "SELECT orderID, dateCreated
                FROM orderItem
                INNER JOIN orders ON orders.orderID = orderItem.order_id
                WHERE orderItem.item_id = ?
                ORDER BY dateCreated DESC";

        $query = $CI->db->query($sql, array($this->{self::$primary_key}));

        return $query->result();

    }

    /**
     * The following method is *. Do not use.
     * @deprecated Use the models for retrieving data, not the Droplocker Objects.
     * gets the product name for the item
     *
     * @return string
     */
    public function getProductName()
    {
        $CI = get_instance();
        $sql = "SELECT product.name FROM item
                INNER JOIN product_process ON product_processID = item.product_process_id
                INNER JOIN product ON productID = product_process.product_id
                WHERE itemID = {$this->itemID}";
        $result = $CI->db->query($sql)->row();

        return $result->name;
    }

    public function validate()
    {
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric");
        }
        try {
            new Business($this->business_id);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Business ID '{$this->business_id}' not found in database.");
        }
    }

}
