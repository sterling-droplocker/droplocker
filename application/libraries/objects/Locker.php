<?php
namespace App\Libraries\DroplockerObjects;
class Locker extends DroplockerObject
{
    public static $table_name = "locker";
    public static $primary_key = "lockerID";
    public static $has_one = array("Location", "LockerStatus", "LockerStyle", "LockerLockType");
    public static $belongsTo = array("Order");

    public function __construct($id = null, $get_relationships = false)
    {
        parent::__construct($id, $get_relationships);
    }
    /**
     * Determines if the current locker is capable of taking multiple orders.
     * @return boolean
     */
    public function is_multi_order()
    {
        $CI = get_instance();
        $sql = "SELECT lockerType FROM locationType
            INNER JOIN location ON locationType_id = locationTypeID
            INNER JOIN locker ON location_id = locationID
            WHERE lockerID = ? AND lockerStyle_id != 9";
        $row = $CI->db->query($sql, array($this->lockerID))->row();

        if(in_array($row->lockerType,array('lockers'))) {
            return false;
        }

        return true;
    }

    public function validate()
    {
        if (empty($this->lockerName)) {
            throw new Validation_Exception("'lockerName' can not be empty.");
        }
        if (!is_numeric($this->lockerStatus_id)) {
            throw new Validation_Exception("'lockerStatus_id' must be numeric.");
        }

        try {
            new LockerStatus($this->lockerStatus_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Locker Status ID '{$this->lockerStatus_id}' does not exist in the database.");
        }

        if (!is_numeric($this->lockerStyle_id)) {
            throw new Validation_Exception("'lockerStyle_id' must be numeric.");
        }
        try {
            new LockerStyle($this->lockerStyle_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Locker Style ID '{$this->lockerStyle_id}' not found in database.");
        }
        if (!is_numeric($this->lockerLockType_id)) {
            throw new Validation_Exception("'lockerLockType_id' must be numeric.");
        }
        try {
            new LockerLockType($this->lockerLockType_id, false);
        } catch (NotFound_Exception $e) {
            throw new Validation_Exception("Locker Lock Type ID '{$this->lockerLockType_id}' not found in database.");
        }

    }

    /**
     * Do not use this function any further. This function does not follow coding standards and does not belong in the Droplocker objects. Please reipmplemnt this function in the Locker model.
     *
     * checks a location for inProcess locker.
     *
     * @param int $location_id
     * @param boolean $create
     * @param boolean $delete
     */
    public static function checkForInProcessLocker($location_id, $create=false, $delete=false)
    {
        // If there
        $inProcessLocker = Locker::search_aprax(array('lockerName'=>'inProcess', 'location_id'=>$location_id));
        if ($inProcessLocker) {
            if (sizeof($inProcessLocker)>1) {
                throw new \Exception("Location [{$location_id}] has more than one inProcess Locker ");
            }

            if ($delete) {
                foreach ($inProcessLocker as $l) {
                    $l->delete();
                }
            }

            return true;
        }

        // There is no inProcess locker in this location
        // Check to see if there are any electronic lockers
        $lockers = Locker::search_aprax(array('location_id' => $location_id, 'lockerLockType_id'=> LockerLockType::ELECTRONIC, 'lockerStatus_id'=> LockerStatus::IN_SERVICE));
        if ($lockers) {
            if ($create) {
                self::createInProcessLocker($location_id);

                return true;
            }

            return false;

        }

        throw new \Exception("InProcess locker search failed");
    }

    /*
     * Creates an inProcess locker
     *
     * @param int location_id
     * @return int insert_id()
    */
    public static function createInProcessLocker($location_id)
    {
        if (empty($location_id)) {
            throw new Exception('Missing location_id');
        }

        $newLocker = new Locker();
        $newLocker->location_id = $location_id;
        $newLocker->lockerName = 'InProcess';
        $newLocker->lockerStyle_id = 8;
        $newLocker->lockerLockType_id = 2;
        $newLocker->lockerStatus_id = 1;

        return $newLocker->save();
    }

    public static function isElectronicLock($lockerLockTypeID)
    {
        $lockerLockType = LockerLockType::search_aprax(array(
            "lockerLockTypeID" => $lockerLockTypeID, 
            "isElectronicLock" => 1
        ));

        if(empty($lockerLockType)) {
            return false;
        }

        return true;
    }
}
