<?php
namespace App\Libraries\DroplockerObjects;
class Settings extends DroplockerObject
{
    public static $table_name = "settings";
    public static $primary_key = "settingsID";

    public function validate()
    {
        //throw new Exception("Not Implemented.");
    }

    /**
     * static function to get the sendAllEmails value
     */
    public static function getSendAllEmails()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'SendAllEmails'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

    /**
     * static function to get the sendAllEmails value
     */
    public static function getEmailCronStatus()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'emailCronStatus'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

    /**
     * static function to get the sendAllEmails value
     */
    public static function getManualEmailProcessing()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'manualEmailProcessing'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

    /**
     * static function to get the sendAllEmails value
     */
    public static function getEmailLimit()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'emailLimit'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }
	 
	 /**
     * static function to get the Customer Service email username value
     */
    public static function getCsEmailUser()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'csEmailUser'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

	 /**
     * static function to get the Customer Service email password value
     */
    public static function getCsEmailPassword()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'csEmailPassword'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

	 /**
     * static function to get the Customer Service email host value
     */
    public static function getCsEmailHost()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'csEmailHost'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

	 /**
     * static function to get the Customer Service email port value
     */
    public static function getCsEmailPort()
    {
        $CI = get_instance();
        $sql = "SELECT value FROM settings WHERE setting = 'csEmailPort'";
        $result  = $CI->db->query($sql)->row();

        return $result->value;
    }

    /*
    *
    * Static function used to set the sendAllEmails setting
    */
    public static function setSendAllEmails($status)
    {
        $CI = get_instance();
        $sql = "UPDATE settings SET value='$status' WHERE setting = 'SendAllEmails'";
        $CI->db->query($sql);

        return $CI->db->affected_rows();
    }


    /**
     * Static function used to set the emailCronStatus setting
     */
    public static function setEmailCronStatus($status)
    {
        $CI = get_instance();
        $sql = "UPDATE settings SET value='$status' WHERE setting = 'emailCronStatus'";
        $CI->db->query($sql);

        return $CI->db->affected_rows();
    }

    /**
     * Static function used to set the manualEmailProcessing setting
     */
    public static function setManualEmailProcessing($status)
    {
        $CI = get_instance();
        $sql = "UPDATE settings SET value='$status' WHERE setting = 'manualEmailProcessing'";
        $CI->db->query($sql);

        return $CI->db->affected_rows();
    }

    /**
     * Static function used to set the manualEmailProcessing setting
     */
    public static function setEmailLimit($limit)
    {
        $CI = get_instance();
        $sql = "UPDATE settings SET value='$limit' WHERE setting = 'emailLimit'";
        $CI->db->query($sql);

        return $CI->db->affected_rows();
    }

}
