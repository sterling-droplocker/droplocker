<?php
namespace App\Libraries\DroplockerObjects;
class LockerStyle extends DroplockerObject
{
    const WASH_AND_FOLD = 1;
    const DRY_CLEAN = 2;
    const COMBO = 3;
    const ANGLED_LOCKER = 4;
    const APARTMENT = 5;
    const LOCATION_DESCRIPTION = 6;
    const IN_PROCESS = 7;
    const OTHER = 8;
    const HIDDEN = 9;

    public static $table_name = "lockerStyle";
    public static $primary_key = "lockerStyleID";

    public function validate()
    {
        throw new \BadMethodCallException("Not Implemented.");
    }
}
