<?php
namespace App\Libraries\DroplockerObjects;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MaintenanceLog
 *
 * @author ariklevy
 */
class MaintenanceLog extends DroplockerObject
{
    public static $table_name = "maintenanceLog";
    public static $primary_key = "maintenanceLogID";
    public static $has_one = array("Employee");
    public function validate()
    {
        if (!$this->datePerformed instanceOf \DateTime) {
            throw new Validation_Exception("'dateCreated must be instance of DateTime");
        }
        if (!is_numeric($this->employee_id)) {
            throw new Validation_Exception("'employee_id' must be numeric.");
        }
        if (!is_numeric($this->business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        if (!Business_Employee::search_aprax(array("business_id" => $this->business_id, "employee_id" => $this->employee_id))) {
            throw new Validation_Exception("Employee ID '{$this->employee_id}' does not belong to business ID '{$this->business_id}'");
        }
        if (empty($this->machine)) {
            throw new Validation_Exception("'machine' can not be empty.");

        }
        if (empty($this->workPerformed)) {
            throw new Validation_Exception("'workPerformed' can not be empty.");
        }
    }
}
