<?php
namespace App\Libraries\DroplockerObjects;
use App\Libraries\DroplockerObjects\OrderHomePickup;

class Claim extends DroplockerObject
{
    public static $table_name = "claim";
    public static $primary_key = "claimID";
    public static $has_one = array("Customer", "Locker");

    public function validate()
    {
        if (is_numeric($this->locker_id)) {
            try {
                $locker = new Locker($this->locker_id);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("locker ID does not exist in database.");
            }
        } else {
            throw new Validation_Exception("'locker_id' must be numeric.");
        }
        if (is_numeric($this->business_id)) {
            try {
                $business = new Business($this->business_id);
            } catch (NotFound_Exception $e) {
                throw new Validation_Exception("business does not exist in the database.");
            }
        } else {
            throw new Validation_Exception("'business_id' must be numeric");
        }
    }

    /**
     * @ Do not use this undocumented function that does not follow coding standards.
     * *************************************
     * BEGIN *
     * *************************************
     *
     * performs all the checks to claim a locker
     *
     * Options Values
     * ---------------
     * notes
     * service_type_serialized
     *
     *
     * @param int $locker_id
     * @param int $customer_id
     * @param int $business_id
     * @param array $options
     * @return array status, message, claimID (on success will return claimID)
     *
     * ************************************
     * END
     * ************************************
     */
    public function newClaim($locker_id, $customer_id, $business_id, $options = array())
    {
        $CI = get_instance();
        $CI->load->helper('claim_helper');

        $locker = new Locker($locker_id, FALSE);
        $location = new Location($locker->location_id, FALSE);
        $business = new Business($business_id, FALSE);
        $customer = new Customer($customer_id, FALSE);

        // Require profile to have first name, last name, and phone
        if (empty($customer->properties['firstName']) || empty($customer->properties['lastName']) || empty($customer->properties['phone'])) {
            return array('status' => 'error', 'message' => get_translation("first_last_phone_required", "claim_errors",
                array("customer_support" => get_business_meta($business_id, "customerSupport")),
                "You must complete your profile with first name, last name, and phone. If you think this is in error, please call us at %customer_support%", $customer_id));
        }

        $checkorder = check_order_exist($locker->lockerID, $business_id);
        $multiOrder = $locker->is_multi_order();

        //if there IS an order AND the locker IS NOT multiorder
        if ($checkorder && !$multiOrder) {

            foreach ($checkorder as $o) {
                $order = new Order($o->orderID);
                if (($customer_id == $order->customer_id)) {
                    $message = get_translation("already_opened_order_view_order_error", "claim_errors",
                        array("customer_support" => get_business_meta($business_id, "customerSupport")),
                        "You already have an open order in this locker. To view your open orders, go to <a href='/account/orders/view_orders/'>My Orders</a>.  If you think this is in error, please call us at %customer_support%", $customer_id);
                } elseif (strlen($order->customer_id) == 0 || $order->customer_id == 0) {

                    $message = get_translation("already_opened_order_error", "claim_errors",
                        array("customer_support" => get_business_meta($business_id, "customerSupport")),
                        "You already have an open order in this locker. If you think this is in error, please call us at %customer_support%", $customer_id);

                    //use this order
                    $bag = new Bag($order->bag_id);

                    //bag found but the bag has no customer_id
                    if (empty($bag->customer_id)) {
                        $bag->customer_id = $customer_id;
                        $bag->save();
                    } elseif ($bag->customer_id != $customer_id) {
                        $message = get_translation("bag_mismatch_error", "claim_errors",
                            array("customer_support" => get_business_meta($business_id, "customerSupport")),
                            "The bag in this locker does not belong to you. If you think this is in error, please call %customer_support%", $customer_id);

                        return array('status'=>'error', 'message'=>$message);
                    }

                    $service_type_serialized = $options['service_type_serialized'];
                    if ($service_type_serialized) {
                        $type_array = unserialize($service_type_serialized);
                        $CI->load->model('serviceType_model');
                        if ($serviceTypes = $CI->serviceType_model->get(array('serviceTypeID'=>$type_array))) {
                            $str = '';
                            foreach ($serviceTypes as $serviceType) {
                                $str .= "- ".$serviceType->name."<br>";
                            }
                        }
                    }
                    $service_type = rtrim($str, ', ');

                    $order->customer_id = $customer_id;
                    $order->notes = "Order Notes: ".$options['notes']."<br>".$service_type."<br>".$order->notes;

                    $order->save();
                    $orderIDs[] = $order->orderID;

                    queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($business_id, "customerSupport"), 'Order ('.$order->orderID.')
                                in Locker '.$locker->lockerName.' in location '.$location->address.'
                                has been claimed', 'Order ('.$order->orderID.') in Locker '.$locker->lockerName.'
                                in location '.$location->address.' has been claimed by '.$customer->firstName.' '.$customer->lastName, array(), null, $business_id);
                } else {
                    //some other customers order is in the locker
                    $message = get_translation("another_user_order_error", "claim_errors",
                        array("customer_support" => get_business_meta($business_id, "customerSupport")),
                        "There is another customer's order currently in this locker. If you think this is in error, please call %customer_support%", $customer_id);
                }

                return array('status'=>'error', 'message'=>$message);
            }

            if (sizeof($orderIDs) == 1) {
                $CI->load->library('discounts');
                $applyRes = $CI->discounts->apply_discounts($order);
                //set_flash_message('info', "Order Details");
                //set_alert('error', $message);
                //redirect('/account/orders/order_details/'.$orderIDs[0]);
                return array('status'=>'error', 'message'=>$message);

            } elseif (sizeof($orderIDs) > 1) {

                $message = get_translation("unpaid_orders_error", "claim_errors",
                    array(),
                    "You now have multiple orders to pay for. Please go to <a href='/account/orders/view_orders/'>My Orders</a> to pay for them.", $customer_id);

                return array('status'=>'error', 'message'=>$message);
            }
        }
        // perform checks on the claim
        $results = check_claim_exists($locker->lockerID, $locker->is_multi_order(), $business_id, $customer_id);
        if ($results['status'] == 'fail') {
            return array('status'=>'error', 'message'=>$results['message']);
        }

        // Check the claims limit if the customer is not the blank_customer
        if ($business->blank_customer_id != $customer_id
            &&  !self::checkOrderLimit($customer_id, $business_id, $response)) {
            return array('status'=>'error', 'message'=> $response);
        }

        //need to claim the locker
        $CI->load->model('claim_model');
        $claim['customer_id'] = $customer_id;
        $claim['locker_id'] = $locker_id;
        $claim['business_id'] = $business_id;
        $claim['order_notes'] = @$options['notes'];
        $claim['orderType'] =  $options['service_type_serialized'];
        if ($claim_id = $CI->claim_model->new_claim($claim)) {
            $action = "new_claim";
            
            if (!$this->isHomePickup($customer_id, $business_id, $locker->location_id)) {
                $action_array = array('do_action' => $action, 'customer_id'=>$customer_id,'claim_id'=>$claim_id, 'business_id'=>$business_id, 'order_id' => @$order->orderID);

                \Email_Actions::send_transaction_emails($action_array);
            }

            // This lacks translation, but it doesn't seems to be used
            $message = 'claim ID: '.$claim_id;

            return array('status'=>'success', 'claimID'=>$claim_id, 'message'=>$message);
        } else {
            $message = get_translation("undefined_error", "claim_errors", array(), "Error claiming locker");
            return array('status'=>'error', 'message'=>$message);
        }
    }

    /**
     * Checks that the customer has not exceeded the claims limit
     *
     * In case of error, this function will set an localized error message
     * in the $response paramteter.
     *
     * @param int $customer_id
     * @param int $business_id
     * @param string $response OUT: the localized error message
     * @return boolean True if client has not exceeded the claims limit
     */
    public static function checkOrderLimit($customer_id, $business_id, &$response = null)
    {
        $CI = get_instance();

        $order_limit = get_business_meta($business_id, 'order_limit', 3);
        if (!empty($order_limit)) {
            $CI->load->model('claim_model');
            $active_claims = count($CI->claim_model->get_claims_by_customer($customer_id));
            if ($active_claims >= $order_limit) {
                $business = new Business($business_id);
                $response = get_translation("too_many_claims_error", "claim_errors",
                    array(
                            "order_limit" => $order_limit,
                            "active_claims" => $active_claims,
                            "customer_support" => get_business_meta($business_id, "customerSupport"),
                            "website_url" => $business->website_url,
                            "phone" => $business->phone,
                            "customer_id" => $customer_id,
                    ),
                    "As a security measure, you can only place %order_limit% orders. If you really used more than 3 lockers, please email the additional locker numbers to %customer_support%", $customer_id);

                return false;
            }
        }

        return true;
    }

    /**
     * Finds a HomePickup object for the customer
     *
     * @param int $claim_id
     * @return boolean
     */
    protected function isHomePickup($customer_id, $business_id, $location_id)
    {
        $service = get_business_meta($business_id, 'home_delivery_service');

        $homePickup = OrderHomePickup::search(array(
            'claim_id' => 0,
            'location_id' => $location_id,
            'customer_id' => $customer_id,
            'service' => $service,
        ));

        return empty($homePickup)?false:true;
    }
}
