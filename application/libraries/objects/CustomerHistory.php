<?php
namespace App\Libraries\DroplockerObjects;
class CustomerHistory extends DroplockerObject
{
    public static $table_name = "customerHistory";
    public static $primary_key = "customerHistoryID";
    protected $rules = array(
        "customer_id" => array("required", "numeric", "entity_must_exist"),
        "note" => array("required")
    );

    public function store($customer_id, $business_id, $historyType, $note)
    {
        $this->customer_id = $customer_id;
        $this->business_id = $business_id;
        $this->historyType = $historyType;
        $this->timestamp = date('Y-m-d H:i:S');
        $this->note = $note;

        return $this->save();
    }
}
