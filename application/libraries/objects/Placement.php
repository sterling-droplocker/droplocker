<?php
namespace App\Libraries\DroplockerObjects;
class Placement extends DroplockerObject
{
    public static $table_name = "placement";
    public static $primary_key = "placementID";

    public function validate()
    {
        if (empty($this->place)) {
            throw new Validation_Exception("'place' can not be empty");
        }
    }
}
