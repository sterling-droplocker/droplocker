<?php
/**
 * Order Library
 *
 */
class Order
{
    /**
     * Captures any uncaptured orders the specified customer
     *
     * Sends an email to Customer Support associated with the business for that customer if any orders are captured
     * @param int $customer_id
     * @return array
     *  (
     *      additionalOrders => array(),
     *      captureResults => array(
     *          array(
     *              status => string success|fail
     *              order_id => int
     *              message => string
     *              amount => float
     *          ), ...
     *      }
     *  )
     */
    public function captureOrdersForCustomer($customer_id)
    {
        if (!is_numeric($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' must be numeric");
        }
        $CI = get_instance();
        $CI->load->model("customer_model");
        $customer = $CI->customer_model->get_by_primary_key($customer_id);
        if (empty($customer)) {
            throw new \InvalidArgumentException("Customer '$customer_id' does not exist");
        }

        if ($customer->invoice) {
            return array(
                'additionalOrders' => array(),
                'captureResults' => array(),
            );
        }

        $CI->load->model("order_model");
        // An order is considered uncaptured if the payment status is not captured and the status is NOT the following Picked Up, Waitng for Service, Inventoried, Washing, Washed, or Loaded on Van.
        $uncapturedOrders = $CI->db->query("SELECT orderID FROM orders WHERE customer_id = ? AND orderPaymentStatusOption_id <> 3 AND orderStatusOption_id NOT IN (1, 2, 3, 5, 6, 14)", array($customer_id))->result();
        $captureResults = array();

        // do not capture orders for customer on invoicing
        if (!empty($uncapturedOrders) && !$customer->invoice) {
            $subject = "Customer '{$customer->firstName} {$customer->lastName} ({$customer->customerID})' entered a new credit card and was charged for the following orders";
            $body = "Customer: {$customer->firstName} {$customer->lastName} [<a href='https://{$_SERVER['HTTP_HOST']}/admin/customers/detail/$customer_id'> $customer_id </a>] <br /> <br />";
            $body .= "Orders captured after customer authorizes new card: <br />";
            foreach ($uncapturedOrders as $uncapturedOrder) {
                $captureResults[] = $lastResponse = $CI->order_model->capture($uncapturedOrder->orderID, $customer->business_id);
                $statusText = @$lastResponse['status'] == 'success' ? 'SUCCESS' : 'ERROR: ' . $lastResponse['message'];
                $body .= "<a href='https://{$_SERVER['HTTP_HOST']}/admin/orders/details/{$uncapturedOrder->orderID}'> {$uncapturedOrder->orderID} $statusText </a> <br />";
            }

            send_admin_notification($subject, $body, $customer->business_id);
        }

        $results = array('additionalOrders' => $uncapturedOrders, 'captureResults' => $captureResults);
        return $results;
    }
}
