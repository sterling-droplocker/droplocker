<?php

class CardReaderLibrary extends CI_Model
{

    public $sockets = array();

    /**
     * Gets a socket connection
     *
     * @param string $address
     * @param int $port
     * @return \DropLocker\Stream\Client
     */
    public function connect($address, $port)
    {
        $key = "$address:$port";

        if (!empty($this->sockets[$key])) {
            return $this->sockets[$key];
        }

        $this->sockets[$key] = new \DropLocker\Stream\Client($address, $port);

        return $this->sockets[$key];
    }

    public function disconnect($address, $port)
    {
        $key = "$address:$port";

        if (!empty($this->sockets[$key])) {
            $this->sockets[$key]->close();
        }

        unset($this->sockets[$key]);
    }

    public function sendCommand($address, $port, $command)
    {
        $socket = $this->connect($address, $port);
        $socket->write($command . "\r");
        $socket->setTimeout(5);
        $ret = $socket->read();

        return $ret;
    }

    public function setMode($address, $port, $mode, $windowStart = null, $windowEnd = null)
    {
        try {
            $socket = $this->connect($address, $port);

            $current_mode = $mode;

            switch ($mode) {
                case 'registered':
                    $command = 'X0750';
                break;

                case 'open':
                    $command = 'X0751';
                break;

                case 'window':
                    //$command = 'X0750';
                    //$this->setSchedule($address, $port, range(1, 7), $windowStart, $windowEnd);

                    $now = date('H:i:s');
                    if ($now < $windowStart || $now > $windowEnd) {
                        $command = 'X0750';
                        $mode = 'window:registered';
                        $current_mode = 'registered';
                    } else {
                        $command = 'X0751';
                        $mode = 'window:open';
                        $current_mode = 'open';
                    }
                break;

            }
            $socket->write($command . "\r");  // set the mode
            $res = $socket->read();

            return array(
                'result' => 'success',
                'message' => "Mode Set to {$mode}",
                'data' => $current_mode,
            );

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port",
            );
        }
    }

    public function setSchedule($address, $port, $days, $windowStart, $windowEnd)
    {
        try {
            $socket = $this->connect($address, $port);

            $command = 'SC';
            $socket->write($command . "\r");  // clear schedules
            sleep(1);

            $start = date('Hi', strtotime("$windowStart"));
            $end = date('Hi', strtotime("$windowEnd"));

            foreach ($days as $i => $day) {
                $n = 200 - $day;
                $command = "SA{$n}{$day}{$start}{$end}";

                $socket->write($command . "\r");  // Add Schedule
                sleep(1);

                if ($n < 200) {
                    $prev = $n + 1;

                    $command = "SS{$prev}{$n}";
                    $socket->write($command . "\r");  // Associate Schedule
                    sleep(1);
                }
            }

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port",
            );
        }
    }

    public function open($address, $port)
    {
        try {
            $socket = $this->connect($address, $port);

            $command = "!05"; // opens door for 5 seconds
            $socket->write($command . "\r");
            sleep(1);

            return array(
                'result' => 'success',
                'message' => "Door opened",
            );

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port",
            );
        }
    }

    public function reset($address, $port)
    {
        try {
            $socket = $this->connect($address, $port);

            $command = "\00U"; // factory defaults
            $socket->write($command . "\r");
            $socket->read();

            $command = 'cd'; // removed all records
            $socket->write($command . "\r");
            $socket->read();

            $command = '\08'; // reset the record length
            $socket->write($command . "\r");
            $socket->read();

            // set the time on the reader
            $thisTime = date("ymdHisN");
            $command = "+$thisTime";
            $socket->write($command . "\r");
            $socket->read();

            return array(
                'result' => 'success',
                'message' => "Reader Reset",
            );

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port",
            );
        }
    }

    public function reboot($address, $port, $password)
    {
        try {
            $socket = $this->connect($address, $port);

            $command = "PASS{$password}"; // Login
            $socket->write($command . "\r");
            $res = $socket->read();

            if (!strstr($res, 'OK')) {
                return array(
                    'result' => 'error',
                    'message' => "Login failed: '$res'",
                    'data' => $res,
                );
            }

            $command = 'REBOOT'; // Reboot
            $socket->write($command . "\r");
            $res = $socket->read();

            return array(
                'result' => 'success',
                'message' => "Reader Reboot",
                'data' => $res,
            );

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port",
            );
        }
    }

    public function getLog($address, $port)
    {
        try {
            $socket = $this->connect($address, $port);

            $data = array();

            $command = 'l';
            $socket->write($command . "\r");

            $socket->setTimeout(10);
            $res = $socket->read();

            if (strstr($res, 'END')) {
                $thisTime = date("ymdHisN");
                $command = "+$thisTime";
                $socket->write($command . "\r");

                $res = $socket->read();

                return array(
                    'result' => 'success',
                    'message' => "No Data in Access Log to Upload: $res",
                    'data' => array(),
                );
            }

            // loop through all the card swipes
            while (! strstr($res, 'END')) {
                // example return string 112112014421081121402

                if (!strstr($res, 'NOK')) {

                    $offset = 0;
                    if (strlen(trim($res, "\n\r ")) > 21) {
                        $offset = 4;
                    }

                    $date = preg_replace('/([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})/', '20\1-\2-\3 \4:\5:\6', substr($res, $offset + 1, 12));
                    $accessTime = strtotime($date);

                    $data[] = array(
                        'accessTime' => gmdate('Y-m-d H:i:s', $accessTime),
                        'card' => substr($res, $offset + 13, 4),
                        'exp' => substr($res, $offset + 17, 4),
                        'success' => substr($res, $offset + 0, 1) == 0 ? 1 : 0, // 0 at reader means success
                        'raw' => $res,
                    );
                }

                $command = 'l';
                $socket->write($command . "\r");
                $socket->setTimeout(10);
                $res = $socket->read();
            }

            // set the time on the reader$res = $socket->read();
            $thisTime = date("ymdHisN");
            $command = "+$thisTime";
            $socket->write($command . "\r");
            $res = $socket->read();

            $counter = count($data);

            return array(
                'result' => 'success',
                'message' => "Log file uploaded {$counter} Records",
                'data' => $data,
            );

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port while getting log file.",
                'data' => $data,
            );
        }
    }

    public function addCards($address, $port, $cards)
    {
        if (!$cards) {
            return array(
                'result' => 'success',
                'message' => "No new cards to add",
            );
        }

        try {
            $socket = $this->connect($address, $port);

            $errors = array();
            $cardsAdded = array();
            foreach ($cards as $card) {
                $cardRecord = $card->cardNumber . $card->expYr . str_pad($card->expMo, 2, "0", STR_PAD_LEFT);
                $command = 'AN000' . $cardRecord;

                $socket->write($command . "\r");
                $socket->setTimeout(10);

                $res = $socket->read();

                if (strstr($res, 'A')) {
                    $cardsAdded []= $cardRecord;
                } else {
                    $errors[] = array(
                        'cardRecord' => $cardRecord,
                        'response' => $res,
                    );
                }
            }

            $count = count($cardsAdded);
            $message = "$count cards added.";
            if ($errors) {
                $count = count($errors);
                $message .= " Errors adding $count cards.";
            }

            return array(
                'result' => 'success',
                'message' => $message,
                'data' => compact('errors', 'cardsAdded'),
            );

        } catch (\DropLocker\Stream\ConnectException $e) {
            return array(
                'result' => 'error',
                'message' => "Could not establish connecttion to reader at $address:$port",
            );
        } catch (\DropLocker\Stream\WriteException $e) {

            $this->disconnect($address, $port);

            return array(
                'result' => 'error',
                'message' => "Failed to send command at $address:$port while getting log file.",
                'data' => compact('errors', 'cardsAdded'),
            );
        }
    }

    public function getCards($business_id, $from = null)
    {
        $params = array($business_id);
        $dateCondition = '';

        if ($from) {
            $dateCondition = "AND updated >= ?";
            $params[] = $from;
        }

        $getCards = "SELECT distinct customer_id, cardNumber, expYr, expMo
            FROM kioskAccess
            join customer on customerID = kioskAccess.customer_id
            where customer.business_id = ?
            $dateCondition
            order by cardNumber";

        $q = $this->db->query($getCards, $params);
        $cards = $q->result();

        return $cards;
    }

}
