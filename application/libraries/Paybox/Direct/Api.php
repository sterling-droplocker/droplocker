<?php
namespace Paybox\Direct;

/**
 * Paybox Direct API
 *
 * @author Dardo Sordi <dardosordi@gmail.com>
 */
class Api
{

    const PAYBOX_DIRECT = "00103";
    const PAYBOX_DIRECT_PLUS = "00104";

    const REQUEST_AUTH = "00001"; // Authorization only
    const REQUEST_DEBIT = "00002"; // Debit (Capture)
    const REQUEST_AUTH_CAPTURE = "00003"; // Authorization + Capture
    const REQUEST_CREDIT = "00004"; // Credit
    const REQUEST_CANCEL = "00005"; // Cancel
    const REQUEST_CHECK = "00011"; // Check if a transaction exists
    const REQUEST_NO_AUTH = "00012"; // Transaction without authorization request
    const REQUEST_UPADTE = "00013"; // Update the amount of a transaction
    const REQUEST_REFUND = "00014"; // Refund
    const REQUEST_INQUIRY = "00017"; // Inquiry

    /* This block is only for Direct Plus */
    const REQUEST_AUTH_SUSCRIBER = "00051"; // Authorization only on a subscriber Only Direct Plus
    const REQUEST_DEBIT_SUSCRIBER = "00052"; // Debit on a subscriber Only Direct Plus
    const REQUEST_AUTH_CAPTURE_SUSCRIBER = "00053"; // Authorization + Capture on a subscriber Only Direct Plus
    const REQUEST_CREDIT_SUSCRIBER = "00054"; // Credit on a subscriber Only Direct Plus
    const REQUEST_CANCEL_SUSCRIBER = "00055"; // Cancel of a transaction on a subscriber Only Direct Plus
    const REQUEST_REGISTER_SUSCRIBER = "00056"; // Register new subscriber Only Direct Plus
    const REQUEST_UPDATE_SUSCRIBER = "00057"; // Update an existing subscriber Only Direct Plus
    const REQUEST_DELETE_SUSCRIBER = "00058"; // Delete a subscriber Only Direct Plus
    const REQUEST_AUTH_ONLY = "00061"; // Authorization only

    const RESPONSE_SUCCESS = '00000';
    const RESPONSE_CONNECTION_FAILED = '00001';
    const RESPONSE_COHERENCE_ERROR = '00002';
    const RESPONSE_PAYBOX_ERROR = '00003';
    const RESPONSE_INVALID_CARD_NUMBER = '00004';
    const RESPONSE_INVALID_QUESTION_NUMBER = '00005';
    const RESPONSE_ACCESS_REFUSED = '00006';
    const RESPONSE_INVALID_DATE = '00007';
    const RESPONSE_INVALID_EXPIRATION_DATE = '00008';
    const RESPONSE_INVALID_OPERATION = '00009';
    const RESPONSE_UNKNOWN_CURRENCY = '00010';
    const RESPONSE_INCORRECT_AMOUNT = '00011';
    const RESPONSE_INVALID_REFERENCE = '00012';
    const RESPONSE_UNSUPPORTED_VERSION = '00013';
    const RESPONSE_INCOHERENT_REQUEST = '00014';
    const RESPONSE_REFERENCE_ERROR = '00015';
    const RESPONSE_SUSCRIBER_ALREADY_EXISTS = '00016';
    const RESPONSE_SUSCRIBER_DOES_NOT_EXISTS = '00017';
    const RESPONSE_TRANSACTION_NOT_FOUND = '00018';
    const RESPONSE_RESERVED = '00019';
    const RESPONSE_MISSING_CVV = '00020';
    const RESPONSE_UNAUTHORIZED_CARD = '00021';
    const RESPONSE_THRESHOLD_REACHED = '00022';
    const RESPONSE_CARDHOLDER_ALREADY_SEEN = '00023';
    const RESPONSE_COUNTRY_CODE_FILTERED = '00024';
    const RESPONSE_CARDHOLDER_NOT_AUTHENTICATED = '00040';
    const RESPONSE_CONNECTION_TIMEOUT = '00097';
    const RESPONSE_INTERNAL_CONNECTION_ERROR = '00098';
    const RESPONSE_INCOHERENT_RETURN = '00099';
    const RESPONSE_PAYMENT_REJECTED = '00100';

    /**
     * If true, use Paybox Direct Plus API
     *
     * @var Boolean
     */
    public $isPlus = false;

    /**
     * If true, use sandbox
     *
     * @var Boolean
     */
    public $sandbox = false;

    /**
     * Paybox site number
     *
     * @var String
     */
    public $site = null;

    /**
     * Paybox rang
     *
     * @var String
     */
    public $rang = null;

    /**
     * Paybox password
     *
     * @var String
     */
    public $cle = null;

    /**
     * Production server URL
     *
     * @var String
     */
    public $mainURL = 'https://ppps.paybox.com/PPPS.php';

    /**
     * Secondary server URL
     *
     * @var String
     */
    public $secondaryURL = 'https://ppps1.paybox.com/PPPS.php';

    /**
     * Test server URL
     *
     * @var String
     */
    public $testURL = 'https://preprod-ppps.paybox.com/PPPS.php';

    /**
     * Currency
     *
     * @var String
     */
    public $currency = 'EUR';

    /**
     * Maps currency name to ISO 4217 code
     *
     * @var Array
     */
    public $currencyMap = array(
        'EUR' => '978',
        'USD' => '840',
        'CFA' => '952'
    );

    /**
     * Activity string
     *
     * @var String
     */
    public $activity = 'internet-payment';

    /**
     * Used to map activities
     *
     * @var array
     */
    public $activityMap = array(
        'not-specified' => '020',
        'telephone' => '021',
        'mail' => '022',
        'minitel' => '023',
        'internet-payment' => '024',
        'recurring-payment' => '027',
    );

    public function __construct($site = null, $rang = null, $cle = null, $isPlus = false, $sandbox = false)
    {
        $this->site = $site;
        $this->rang = $rang;
        $this->cle = $cle;
        $this->isPlus = $isPlus;
        $this->sandbox = $sandbox;
    }

    public function authorize($cardNumber, $cvv, $ccExpMo, $ccExpYr, $amount, $description = null, $data = array())
    {
        $data['TYPE'] = self::REQUEST_AUTH;
        $data['DEVISE'] = $this->getCurrencyCode();
        $data['PAYS'] = ''; // Get card country
        $data['TYPECARTE'] = ''; // Get card type
        $data['REFERENCE'] = $description ? $description : "Authorize Card [$customer]";

        $data['MONTANT'] = round($amount * 100); // amount in cents
        $data['PORTEUR'] = $cardNumber; // card number
        $data['CVV'] = $cvv;
        $data['DATEVAL'] = $this->formatExpires($ccExpMo, $ccExpYr);

        if (empty($data['ACTIVITE'])) {
            $data['ACTIVITE'] = $this->activityMap[$this->activity];
        }

        $response = $this->doRequest($data);

        return $response;
    }

    public function debit($transaction, $call_number, $amount, $description = null, $data = array())
    {
        $data['TYPE'] = self::REQUEST_DEBIT;
        $data['DEVISE'] = $this->getCurrencyCode();
        $data['REFERENCE'] = $description ? $description : "Debit Card [$customer]";
        $data['PAYS'] = ''; // Get card country

        $data['MONTANT'] = round($amount * 100); // amount in cents
        $data['NUMAPPEL'] = $call_number;
        $data['NUMTRANS'] = $transaction;

        $response = $this->doRequest($data);

        return $response;
    }

    public function authorizeSuscriber($cardNumber, $cvv, $ccExpMo, $ccExpYr, $amount, $customer, $description = null, $data = array())
    {
        $data['TYPE'] = self::REQUEST_AUTH_SUSCRIBER;
        $data['DEVISE'] = $this->getCurrencyCode();
        $data['PAYS'] = ''; // Get card country
        $data['TYPECARTE'] = ''; // Get card type
        $data['REFABONNE'] = $customer;
        $data['REFERENCE'] = $description ? $description : "Authorize Card [$customer]";

        $data['MONTANT'] = round($amount * 100); // amount in cents
        $data['PORTEUR'] = $cardNumber; // card number
        $data['CVV'] = $cvv;
        $data['DATEVAL'] = $this->formatExpires($ccExpMo, $ccExpYr);


        if (empty($data['ACTIVITE'])) {
            $data['ACTIVITE'] = $this->activityMap[$this->activity];
        }

        $response = $this->doRequest($data);

        return $response;
    }

    public function registerSuscriber($cardNumber, $cvv, $ccExpMo, $ccExpYr, $amount, $customer, $description = null, $data = array())
    {
        $data['TYPE'] = self::REQUEST_REGISTER_SUSCRIBER;
        $data['DEVISE'] = $this->getCurrencyCode();
        $data['PAYS'] = ''; // Get card country
        $data['TYPECARTE'] = ''; // Get card type
        $data['REFERENCE'] = $description ? $description : "Register [$customer]";

        $data['MONTANT'] = round($amount * 100); // amount in cents
        $data['REFABONNE'] = $customer; // customer ID
        $data['PORTEUR'] = $cardNumber; // card number
        $data['CVV'] = $cvv;
        $data['DATEVAL'] = $this->formatExpires($ccExpMo, $ccExpYr);

        if (empty($data['ACTIVITE'])) {
            $data['ACTIVITE'] = $this->activityMap[$this->activity];
        }

        $response = $this->doRequest($data);

        return $response;
    }

    public function updateSuscriber($cardNumber, $cvv, $ccExpMo, $ccExpYr, $amount, $customer, $description = null, $data = array())
    {
        $data['TYPE'] = self::REQUEST_UPDATE_SUSCRIBER;
        $data['DEVISE'] = $this->getCurrencyCode();
        $data['PAYS'] = ''; // Get card country
        $data['TYPECARTE'] = ''; // Get card type
        $data['REFERENCE'] = $description ? $description : "Register [$customer]";

        $data['MONTANT'] = round($amount * 100); // amount in cents
        $data['REFABONNE'] = $customer; // customer ID
        $data['PORTEUR'] = $cardNumber; // card number
        $data['CVV'] = $cvv;
        $data['DATEVAL'] = $this->formatExpires($ccExpMo, $ccExpYr);

        if (empty($data['ACTIVITE'])) {
            $data['ACTIVITE'] = $this->activityMap[$this->activity];
        }

        $response = $this->doRequest($data);

        return $response;
    }

    public function debitSuscriber($transaction, $call_number, $ref_token, $amount, $customer, $description = null, $data = array())
    {
        $data['TYPE'] = self::REQUEST_DEBIT_SUSCRIBER;
        $data['DEVISE'] = $this->getCurrencyCode();
        $data['REFERENCE'] = $description ? $description : "Debit Card [$customer]";

        $data['MONTANT'] = round($amount * 100); // amount in cents
        $data['REFABONNE'] = $customer; // customer ID
        $data['NUMAPPEL'] = $call_number;
        $data['NUMTRANS'] = $transaction;
        $data['PORTEUR'] = $ref_token;

        if (empty($data['ACTIVITE'])) {
            $data['ACTIVITE'] = $this->activityMap[$this->activity];
        }

        $response = $this->doRequest($data);

        return $response;
    }

    protected function formatExpires($ccExpMo, $ccExpYr)
    {
        return sprintf('%02d%02d', $ccExpMo, $ccExpYr % 100); // expires MMYY
    }

    protected function doRequest($data = array())
    {

        /* Auth */
        $data['SITE'] = $this->site;
        $data['RANG'] = $this->rang;
        $data['CLE'] = $this->cle;

        /* Boilerplate */
        $data['VERSION'] = $this->isPlus ? self::PAYBOX_DIRECT_PLUS : self::PAYBOX_DIRECT;
        $data['DATEQ'] = date('dmYHis');
        $data['NUMQUESTION'] = $this->generateRequestNumber();
        if (! isset($data['REFERENCE'])) {
            $data['REFERENCE'] = uniqid("", true);
        }

        $url = $this->sandbox ? $this->testURL : $this->mainURL;
        $query = null;
        $response = $this->postRequest($url, $data, $query);

        // only retry if no in sandbox
        if ($response === false && !$this->sandbox) {
            $response = $this->postRequest($url = $this->secondaryURL, $data, $query);
        }

        if (empty($response)) {
            $response = array(
            	'status' => 'error',
                'code' => '-1',
                'message' => 'Network Error',
                'data' => array(),
                'response' => $response,
            );
        } else {
            $response = $this->parseResponse($response);
        }

        $response['request'] = $data;
        $response['url'] = $url;
        $response['query'] = $url;

        return $response;
    }

    protected function parseResponse($response) {

        $data = array();
        parse_str(utf8_encode($response), $data);

        $output = array();

        if ($data['CODEREPONSE'] == self::RESPONSE_SUCCESS) {
            $output['status'] = 'success';
        } else {
            $output['status'] = 'error';
        }

        $output['code'] = $data['CODEREPONSE'];
        $output['message'] = $data['COMMENTAIRE'];
        $output['data'] = $data;
        $output['response'] = $response;

        if (!empty($data['NUMAPPEL'])) {
            $output['call_number'] = $data['NUMAPPEL'];
        }

        if (!empty($data['NUMTRANS'])) {
            $output['transaction'] = $data['NUMTRANS'];
        }

        if (!empty($data['AUTORISATION'])) {
            $output['authorization'] = $data['AUTORISATION'];
        }

        if (!empty($data['PORTEUR'])) {
            $output['ref_token'] = $data['PORTEUR'];
        }

        return $output;
    }

    protected function postRequest($url, $data, &$query = null)
    {
        $data = array_map('rawurlencode', $data);
        $query = http_build_query($data);

        $ch = curl_init($url);
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $query,
            CURLOPT_HTTPHEADER => array(
                'Accept-Charset: utf-8',
                'Content-Type: application/x-www-form-urlencoded; charset=utf-8'
            ),
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_VERBOSE => false,
        );
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpCode > 400) {
            return false;
        }

        return $result;
    }

    protected function generateRequestNumber()
    {
        // seconds since midnight
        $secs = (date('G') * 3600) + (date('i') * 60) + date('s');
        return $secs * 1000 + rand(0, 999);
    }

    protected function getCurrencyCode()
    {
        return isset($this->currencyMap[$this->currency]) ? $this->currencyMap[$this->currency] : $this->currency;
    }
}