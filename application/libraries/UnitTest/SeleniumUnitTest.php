<?php

require_once("{$GLOBALS["application_folder"]}/libraries/webdriver/WebDriver/Driver.php");
require_once("{$GLOBALS["application_folder"]}/libraries/webdriver/WebDriver/WebElement.php");
require_once("{$GLOBALS["application_folder"]}/libraries/webdriver/WebDriver.php");

use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\ProductCategory;
use App\Libraries\DroplockerObjects\EmailLog;
use App\Libraries\DroplockerObjects\Website;

abstract class SeleniumUnitTest extends PHPUnit_Framework_TestCase {
    protected $CI;
    protected $business_id;
    protected $driver;
    protected $domain_extension;
    protected $default_implicit_wait;
    protected $browsers;
    protected $selenium_server_port;
    protected $branch_name; //This is the name of the Git branch
    protected $developer_email; //This is the email to send Selenium Error reports to.
    protected $prepayment_locker_location;
    protected $prepayment_locker;
    protected $default_wash_fold_product;
    protected $default_wash_fold_productCategory;
    protected $root_path;
    protected $website;
    protected $base_url;
    protected $default_language;
    protected $default_business_language;

    /**
     * Sends an email immediatley. Mainly used when sending error reports
     * @param string $from
     * @param string $from_name
     * @param string $to The recipient's e-mail address
     * @param string $subject The subject of the e-mail
     * @param string $body The body of the -email
     * @param array $cc
     * @param array $bcc
     * @param string $attachement A PNG image that contains a screensho tof the browser at the time of failure
     * @return int insert_id() from the emailLog table. If the creation of the e-mail log fails, then false is returned
     * 
     */
    public function send_selenium_error_report($from, $from_name, $to, $subject, $body, $cc=array(), $bcc=array(), $attachment = null)
    {
        $CI =& get_instance();

        if (empty($to))
        {
            throw new \InvalidArgumentException("'to' can not be empty.");
        }
        if (empty($from))
        {
            throw new \InvalidArgumentException("'from' can not be empty");
        }

        $emailLog = new EmailLog();
        $emailLog->to = $to;
        $emailLog->subject = $subject;
        $emailLog->body = $body;
        $emailLog->save();

        $attachment_path = $this->root_path . "/attachments/{$emailLog->{EmailLog::$primary_key}}.png";
        if (!file_put_contents($attachment_path, $attachment))
        {
            trigger_error("Could not save selenium screenshot atachment to '$attachment_path", E_USER_WARNING);
        }

        $emailLog->attachment = "/attachments/{$emailLog->{EmailLog::$primary_key}}.png";
        $emailLog->save();

        $CI->load->library('email');
        $CI->load->library('session');
        $CI->email->clear();
        $CI->email->from($from, $from_name);

        $CI->email->to($to);
        if (!is_null($subject))
        {
            $CI->email->subject($subject);
        }
        $CI->email->message($body);
        $CI->email->cc($cc);
        $CI->email->bcc($bcc);
        if (!is_null($attachment))
        {
            $CI->email->attach($attachment);
        }
        $CI->email->set_newline("\r\n");

        if(@$CI->email->send())
        {
            // Do nothing
        }
        else
        {
            trigger_error($CI->email->print_debugger(), E_USER_WARNING);
            //throw new \Exception($CI->email->print_debugger());
        }

        return true; 
    }    
    
    
    /**
     * The following statementtells Selenium to wait for any JQuery initiated AJAX queries to complete.
     * @return void
     */

    public function wait_for_ajax_to_complete() {
        for ($x = 0; $x < $this->default_implicit_wait/1000; $x++) {
            $result = $this->driver->execute_js_sync("return jQuery.active == 0");
            $body = $result['body'];

            //Note, piece of shit json_decode() is broken.;
            preg_match("%\"value\":(\w+),%", $body, $matches);


            if ($matches[1] == "true") {
                return;
            } else {
                sleep(1);
            }
        }
        sleep(1);
    }

    /**
     * Removes all extra spaces, tags, and whitespace around the string.
     * @param type $string 
     * @return string
     */
    public function prepare_string_for_search($string) {
        $prepared_string = trim(strip_tags(preg_replace("/\s\s+/", " ", $string)));
        return $prepared_string;
    }

    /**
     * The following constructor creates the harcoded test business with business ID 3, and also extracts the configuraiton data from the phpunit XML file.
     * @param bool $bizzie_test_mode Wheter or not to read the BIZZIE configuration values in the phpunit.xml
     * @throws \Exception
     */
    public function __construct($bizzie_test_mode = false) 
    {
        $this->default_implicit_wait = 1000;
        $this->CI = & get_instance();
        
        $this->business_id = 3;
        
        //The following conditional loads the bizzie test database if the bizzie test mode is enabled.
        if ($bizzie_test_mode)
        {
            $this->database_identifier = getenv("BIZZIE_MODE_DATABASE_IDENTIFIER");
            if (empty($this->database_identifier))
            {
                throw new \Exception("'BIZZIE_MODE_DATABASE_IDENTIFIER' must be set in phpunit.xml");
            }
        }
        else
        {
            $this->database_identifier = getenv("DATABASE_IDENTIFIER");
            if (empty($this->database_identifier))
            {
                throw new \Exception("'DATABASE_IDENTIFIER' must be set in phpunit.xml");
            }
        }
        $this->CI->load->database($this->database_identifier, TRUE);
        
        //The following conditional loads the domain extension for the bizzie web directory if bizzie test mode is enabled.
        if ($bizzie_test_mode) {
            $this->domain_extension = getenv("BIZZIE_MODE_DOMAIN_EXTENSION");
            if (empty($this->domain_extension)) {
                throw new \Exception("'BIZZIE_MODE_DOMAIN_EXTENSION' must be set in phpunit.xml");
            }    
        }
        else {
            $this->domain_extension = getenv("DOMAIN_EXTENSION");
            if (empty($this->domain_extension)) {
                throw new \Exception("'DOMAIN_EXTENSION' must be set in phpunit.xml");
            }
        }
        $this->base_url = "http://droplocker." . $this->domain_extension;
        //The following conditional loads the applicaiton directgory web path if bizzie test mode is enabled.
        if ($bizzie_test_mode)
        {
            $this->root_path = getenv("BIZZIE_MODE_ROOT_PATH");
            if (empty($this->root_path))
            {
                throw new \Exception("'BIZZIE_MODE_ROOT_PATH' must be set in phpunit.xml");
            }
        }
        else 
        {
            $this->root_path = getenv("ROOT_PATH");
            if (empty($this->root_path))
            {
                throw new \Exception("'ROOT_PATH' must be set in phpunit.xml");
            }
        }

        $this->CI->load->model("business_model");

        $this->server = getenv("SELENIUM_SERVER_ADDRESS");
        if (empty($this->server)) 
        {
            throw new \Exception("'SELENIUM_SERVER_ADDRESS' must be set in phpunit.xml");
        }
        $this->selenium_server_port = getenv("SELENIUM_SERVER_PORT");

        if (empty($this->selenium_server_port)) 
        {
            throw new \Exception("'SELENIUM_SERVER_PORT' must be set in phpunit.xml");
        }

        $this->branch_name = getEnv("BRANCH_NAME");
        if (empty($this->branch_name)) 
        {
            throw new \Exception("'BRANCH_NAME' must be set in phpunit.xml");
        }

        $this->developer_email = getEnv("DEVELOPER_EMAIL");
        if (empty($this->developer_email)) 
        {
            throw new \Exception("'DEVELOPER_EMAIL' must be set in phpunit.xml");
        }

        
        $this->browsers = explode(",", getenv("BROWSERS"));
        if (empty($this->browsers)) 
        {
            throw new \Exception("'BROWSERS' must be set in phpunit.xml");
        }
        //$browsers = array("internet explorer");

        Server_meta::set("mode", "droplocker");
        Server_meta::set("email_host", "test");
        Server_Meta::set("email_user", "testuser");
        Server_Meta::set("email_password", "testemailpassword");
    }

    /**
     * Note, the following setUp function sets the business to be defined as 'American', with a country property value of'USA'.
     */
    protected function setUp() {
        parent::setUp();

        $this->business_id = 3;
        try 
        {
            $this->business = new Business($this->business_id);
        } 
        catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) 
        {
            $this->business = new Business();
        }
        $this->business->companyName = "Laundry Locker";
        $this->business->slug = "laundrylocker";
        $this->business->city = "San Francisco";
        $this->business->state = "CA";
        $this->business->zipcode = "94517";
        $this->business->address = "1530 Custer Ave";
        $this->business->businessID = $this->business_id;
        $this->business->phone = "415) 255-7500";
        $this->business->paymentToken = "3374397FCE62500F388AEDFF8D56AB6DCFC1259444B0C3F6E8A7492787DA5AE49A56BB50CDF186E798";
        $this->business->timezone =" America/Los_Angeles";
        $this->business->store_access = 1;
        $this->business->wash_fold_tracking = 1;
        $this->business->locale = "en_US.utf8";
        $this->business->blank_customer_id=2884;
        $this->business->subdomain = "laundrylocker";
        $this->business->serviceTypes = 'a:6:{i:1;a:4:{s:14:"serviceType_id";s:1:"1";s:6:"active";i:1;s:7:"service";s:13:"Wash and Fold";s:11:"displayName";s:14:"Wash and Fold ";}i:2;a:4:{s:14:"serviceType_id";s:1:"2";s:6:"active";i:1;s:7:"service";s:9:"Dry Clean";s:11:"displayName";s:9:"Dry Clean";}i:4;a:4:{s:14:"serviceType_id";s:1:"4";s:6:"active";i:1;s:7:"service";s:5:"Other";s:11:"displayName";s:5:"Other";}i:5;a:4:{s:14:"serviceType_id";s:1:"5";s:6:"active";i:1;s:7:"service";s:10:"Shoe Shine";s:11:"displayName";s:30:"Shoe Shine (3-5 Business Days)";}i:6;a:4:{s:14:"serviceType_id";s:1:"6";s:6:"active";i:1;s:7:"service";s:16:"Laundered Shirts";s:11:"displayName";s:16:"Laundered Shirts";}i:7;a:4:{s:14:"serviceType_id";s:1:"7";s:6:"active";i:1;s:7:"service";s:11:"Shoe Repair";s:11:"displayName";s:32:"Shoe Repair (5-10 Business Days)";}}';
        $this->business->subdomain = "laundrylocker";
        $this->business->slug = "laundrylocker";
        $this->business->website_url = "";
        $this->business->country = "usa";
        $this->business->template = "";
        $this->prepayment_location = UnitTest::create_random_location($this->business_id);
        $this->prepayment_locker = UnitTest::create_random_locker($this->prepayment_location->{Location::$primary_key});
        $this->business->prepayment_locker_id = $this->prepayment_locker->{Locker::$primary_key};
        
        $this->default_language = UnitTest::create_random_language();
        $this->default_business_language = UnitTest::create_random_business_language($this->business->businessID, $this->default_language->languageID);
        
        $this->business->save();
        //Note, the following statement sets the test buisness to have a primry key of 3.
        $this->CI->db->query("Update business SET businessID=3 WHERE businessID=?", $this->business->businessID); 
        $this->business->businessID = $this->business_id;

        //The following statements create the default wash and fold product categoryfor the business if they do not exist already for the test business.
        $productCategories = ProductCategory::search_aprax(array("slug" => "wf", "business_id" => $this->business_id));
        if (empty($productCategories))
        {
            $this->default_wash_fold_productCategory = new ProductCategory();
            $this->default_wash_fold_productCategory->name = "Wash and Fold";
            $this->default_wash_fold_productCategory->slug = "wf";
            $this->default_wash_fold_productCategory->business_id = $this->business_id;
            $this->default_wash_fold_productCategory->module_id = 1; //This is the Wash and Fold Service Type ID
            $this->default_wash_fold_productCategory->save();
        }
        else
        {
            $this->default_wash_fold_productCategory = $productCategories[0];
        }
        
        //The following statements create the default wash and fold product  for the business.
        $products = Product::search_aprax(array("productCategory_id" => $this->default_wash_fold_productCategory->{ProductCategory::$primary_key}));
        if (empty($products))
        {
            $this->default_wash_fold_product = new Product();
            $this->default_wash_fold_product->name = 'default wash and fold name';
            $this->default_wash_fold_product->displayName = 'Default wash and fold product display name';
            $this->default_wash_fold_product->business_id = $this->business_id;
            $this->default_wash_fold_product->module_id = 1;
            $this->default_wash_fold_product->sortOrder = 1;
            $this->default_wash_fold_product->productType = "preference";
            $this->default_wash_fold_product->productCategory_id = $this->default_wash_fold_productCategory->{ProductCategory::$primary_key};
            $this->default_wash_fold_product->save();
            UnitTest::create_random_product_process($this->business_id, $this->default_wash_fold_product->{Product::$primary_key});
        }
        else
        {
            $this->default_wash_fold_product = $products[0];
        }
                
        //$business->facebook_api_key = "";
        //$business->facebook_secret_key = "";
        $this->business->save();

        //The following statements create the website code for the test business.
        $websites = App\Libraries\DroplockerObjects\Website::search_aprax(array("business_id" => $this->business_id));
        if (empty($websites))
        {
            $this->website = new Website();
            $this->website->business_id = $this->business->businessID;
        }
        else
        {
            $this->website = $websites[0];
        }
        $this->website->template = "laundrylocker";
        $this->website->title = "Laundry Locker :. San Francisco Green Dry Cleaning and Laundry Delivery Service";
        $this->website->javascript = '
            <script type="text/javascript" src="https://laundrylocker.com/js/anylinkmenu.js"></script>
            <script type="text/javascript" src="https://laundrylocker.com/js/showHide.js"></script>

            <script type="text/javascript">anylinkmenu.init("menuanchorclass")</script>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, "script", "facebook-jssdk"));</script>            
        ';
        $this->website->stylescript = '
            <link rel="stylesheet" type="text/css" href="https://laundrylocker.com/css/laundrylocker/style.css"/>
            <link rel="shortcut icon" href="/images/favicon.ico" />
        ';
        $this->website->header = '
            <div id="wrapper_back">
            <div style="position:relative; float: left; padding-top:13px; padding-bottom:13px;"><a href="https://laundrylocker.com"><img src="https://laundrylocker.com/images/logo.jpg" alt="" border="0"></a></div>

            <div style="float: right; position: relative; width: 300px; height: 53px; padding-top: 20px;">
                <div style="float: right; height: 53px;"><img src="/images/signIn_right.jpg" alt="" border="0"></div>
                <div style="float: right; width: 150px; height: 53px; background-image: url(/images/signIn_background.jpg);  text-align: center;">
                    <img style="margin-top: 12px;" src="/images/button_signIn.jpg" alt="" border="0">
                    <div style="position: absolute; left:185px; top: 36px;">
                    <a href="/login" id="login_logout" class="buttonWhiteText">Sign In</a>

            </div>
            <div style="padding-top:16px;" class="fb-like" data-href="https://www.facebook.com/LaundryLocker" data-send="false" data-layout="button_count" data-width="0" data-show-faces="true" data-font="arial"></div>
                </div>
                <div style="float: right; height: 53px;"><img src="/images/signIn_left.jpg" alt="" border="0"></div>
                <div style="float: right; width: 250px; text-align:right; padding-top:3px; padding-right:150px;"><a href="/register" class="bodyBold">Get Started For Free!</a></div></div>
            <div style="clear: both;"></div>

            <div style="height: 38px; text-align: center;">
                   <img src="/images/nav_left.jpg" style="float: left;" />
                <div align="center" style="float: left; background-image: url(https://laundrylocker.com/images/nav_background.jpg); height: 38px; text-align: center; width: 954px;">

                    <div style="float: left;" class="navText"><a href="/account" class="navLink">My Account</a></div>
                    <div style="float: left;"><img src="https://laundrylocker.com/images/nav_div.jpg" alt="" border="0"></div>
                    <div style="float: left;" class="navText"><a href="/main/services" class="navLink">Our Services</a></div>
                    <div style="float: left;"><img src="https://laundrylocker.com/images/nav_div.jpg" alt="" border="0"></div>
                    <div style="float: left;" class="navText"><a href="/main/howto" class="navLink">How it Works</a></div><div style="float: left;"><img src="https://laundrylocker.com/images/nav_div.jpg" alt="" border="0"></div>
                    <div style="float: left;" class="navText"><a href="/main/faqs" class="navLink">FAQ\'s</a></div>
                    <div style="float: left;"><img src="https://laundrylocker.com/images/nav_div.jpg" alt="" border="0"></div>
                    <div style="float: left;" class="navText"><a href="/main/publiclocations" class="navLink">Locations</a></div>
                    <div style="float: left;"><img src="https://laundrylocker.com/images/nav_div.jpg" alt="" border="0"></div>
                    <div style="float: left;" class="navText"><a href="/main/about" class="navLink">About Us</a></div>    

                </div>
                    <img src="/images/nav_right.jpg" style="float: right;" />
            </div>
            <br />

            ';
        $this->website->footer = '
            <div style="clear:both"></div>
            <div style="float:none; background-image: url(/images/footer_border.jpg); height: 20px; width: 970px;"></div>
            <div style="float:none; width: 970px; height: 300px; background-color: #12447B;">
                            <div style="float:left; margin-left:30px; padding-top: 30px; width:300px; text-align: left;">
                                    <div class="homeWhiteMed" style="padding-bottom:15px;">Services</div>
                                    <div style="float: left;" class="homeWhiteSmall">
                                            <a href="/main/dryclean" class="whiteLink">Dry Cleaning</a><br>
                                            <a href="/main/washfold" class="whiteLink">Wash & Fold</a><br>
                                            <a href="/main/shoeshine" class="whiteLink">Shoe and Boot Shine</a><br>
                                            <a href="/main/wholesale" class="whiteLink">Wholesale</a><br>
                                            <a href="/main/dryclean" class="whiteLink">Linens</a>
                                    </div>
                                    <div style="float: left; padding-left:30px;" class="homeWhiteSmall">
                                            <a href="/main/locations" class="whiteLink">24/7 Locations</a><br>
                                            <a href="/main/dryclean" class="whiteLink">Comforters</a><br>
                                            <a href="/main/storage" class="whiteLink">Clothing Storage</a><br>
                                            <a href="/main/homedelivery" class="whiteLink">Home Delivery</a>
                                    </div>
                            </div>
                            <div style="float:left; background-image: url(/images/hr_vert.gif); background-repeat: repeat-y; height: 270px; width: 5px;"></div>
                            <div style="float:left; margin-left:30px; padding-top: 30px; margin-right: 30px; width:250px; text-align: left;">
                                    <div class="homeWhiteMed" style="padding-bottom:15px;">Testimonial</div>
                                    <div style="float: left;" class="homeWhiteSmall">
                                    %if_testimonial% %testimonial% %/if_testimonial%				<br><br><a href="/main/testimonials" class="greenLink">read more testimonials</a>
                                    </div>
                            </div>
                            <div style="float:left; background-image: url(/images/hr_vert.gif); background-repeat: repeat-y; height: 270px; width: 5px;"></div>
                            <div style="float:right; margin-right:30px; padding-top: 30px; width:250px; text-align: left;">
                                    <div class="homeWhiteMed" style="padding-bottom:15px;">Contact</div>
                                    <div style="float: left;" class="homeWhiteSmall">
                                            1530 Custer Ave.<br>
                                            San Francisco, CA 94124<br>
                                            Ph. 415.255.7500<br>
                                            Fax 415.255.7577<br>
                                            Email: <a href="mailto:support@laundrylocker.com" style="color: white; text-decoration: none;">support@laundrylocker.com</a><br>

                    <br><a name="fb_button" id="fb_share" href="http://www.facebook.com/pages/Laundry-Locker/228625619189" target="_blank" type="button"><img src="/images/facebook_white.png" width="32" height="34" alt="" border="0"></a><a href="https://twitter.com/laundrylocker" target="_blank"><img src="/images/twitter_white.png" alt="" width="32" height="34" border="0"></a>	
                                    <br>
                                    </div>
                            </div>
                            <div style="clear: right;"></div>
                            <div align="right" class="homeWhiteSmall" style="float:right; text-align: right; font-size: 10px; padding-top: 20px; padding-right:20px;">2012 Laundry Locker, Inc.</div>

                    </div>            

            ';
        
        $this->website->save();
        
        
        if ($this->CI->db->hostname == "droplockerbizzie.cvtvvbrpfazq.us-east-1.rds.amazonaws.com") 
        {
            die("Refusing to run tests on Production server.");
        }
    }

    protected function tearDown() 
    {    
        /**
        if ($this->default_wash_fold_productCategory instanceOf \App\Libraries\DroplockerObjects\ProductCategory)
        {
            $this->default_wash_fold_productCategory->delete();
        }
        if ($this->default_wash_fold_product instanceOf \App\Libraries\DroplockerObjects\Product)
        {
            $this->default_wash_fold_product->delete();
        }
         * 
         */
        if ($this->prepayment_locker instanceOf \App\Libraries\DroplockerObjects\Locker)
        {
            $this->prepayment_locker->delete();
        }
        if ($this->prepayment_location instanceOf \App\Libraries\DroplockerObjects\Location)
        {
            $this->prepayment_location->delete();
        }
        parent::tearDown();
    }

    /**
     * Override to run the test across all specified browsers.
     *
     * @return mixed
     * @throws RuntimeException
     */
    public function runBare() {
        echo ("\n <<<Running {$this->getName()}...>>>\n");

        foreach ($this->browsers as $browser) {
            $this->browser = $browser;
            //The following loop will make multiple attempts, if necessary, to create a browser instance.
            $number_of_attempts = 0;
            while(true) {
                try  {
                    $this->driver = WebDriver_Driver::InitAtHost($this->server, $this->selenium_server_port, $browser);
                    $this->driver->load("http://droplocker.{$this->domain_extension}/admin");
                    break;
                }
                catch (PHPUnit_Framework_Exception $e) {
                    if ($number_of_attempts < 20) {
                        throw $e;
                        break;
                    }
                    ++$number_of_attempts;
                    sleep(1);
                }
            }

            $this->driver->set_implicit_wait($this->default_implicit_wait);
            //The following try-catch statement catches any errors or exceptions thrown by the test, logss it to e-mail, and then throws the exception back to PHPUnit
            try {
                parent::runBare();
            } 
            catch (\Exception $selenium_test_exception)  {
                try {
                    printf("The alert text (if any) is as follows: %s \n", $this->driver->get_alert_text());
                }
                catch (Exception $get_alert_exception) {
                    echo("No Alert text \n");
                }
                //The following try catch statement attempts to get a screenshot of the browser. If getting a screen fails, then we send an e-mail with no screenshot.
                $body = "
                    <pre>
                        URL: {$this->driver->get_url()}


                        {$selenium_test_exception->getMessage()}
                        {$selenium_test_exception->getFile()} : {$selenium_test_exception->getLine()} "
                        . htmlspecialchars($selenium_test_exception->xdebug_message) . "

                    </pre>
                ";
               
                try {
                    $image = $this->driver->get_screenshot();
                    if (getenv("SUPPRESS_SEND_SELENIUM_ERROR_REPORTS") == "yes") {
                        echo "Selenium error report supressed, not logging error or sending e-mail \n";
                    }
                    else {
                        echo "Sending and logging Selenium Error report \n";
                        $this->send_selenium_error_report(SYSTEMEMAIL, SYSTEMFROMNAME, $this->developer_email, "Selenium Error (Branch: {$this->branch_name}) (Test Name: {$this->getName()}) (Browser: {$this->browser})", $body, array(), array(), $image);
                    }
                    //
                    $this->driver->load("http://droplocker.{$this->domain_extension}/admin/index/logout");
                } 
                catch (\Exception $get_screenshot_exception) {
                    echo "Could not save screenshot : {$get_screenshot_exception->getMessage()} \n";
                    try {
                        send_email(SYSTEMEMAIL, SYSTEMFROMNAME, $this->developer_email, "Selenium Error (Branch: {$this->branch_name}) (NO SCREENSHOT) (Test Name: {$this->getName()}) (Browser: {$this->browser})", $body, array(), array(), null, null, true);
                    }
                    catch (\Exception $send_email_exception) {
                        echo "Could not send email: {$send_email_exception->getMessage()} \n";
                    }
                }
                $this->driver->quit();
                throw $selenium_test_exception;
            }
            $this->driver->load("http://droplocker.{$this->domain_extension}/admin/index/logout");
            $this->driver->quit();
        }
    }

}

?>
