<?php
require_once 'PHPUnit/Autoload.php';

use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\LockerStatus;
use App\Libraries\DroplockerObjects\LockerType;
use App\Libraries\DroplockerObjects\LockerLockType;
use App\Libraries\DroplockerObjects\LockerStyle;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\ItemIssue;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\OrderStatusOption;
use App\Libraries\DroplockerObjects\OrderStatus;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Transaction;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\OrderItem;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Supplier;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\QuickItem;
use App\Libraries\DroplockerObjects\Process;
use App\Libraries\DroplockerObjects\StarchOnShirts;
use App\Libraries\DroplockerObjects\BusinessLaundryPlan;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Customer_Item;
use App\Libraries\DroplockerObjects\ProductCategory;
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\Image;
use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\OrderCharge;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\LanguageKey_Business_Language;
use App\Libraries\DroplockerObjects\LanguageKey;
use App\Libraries\DroplockerObjects\Language;
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\EmailAction;
use App\Libraries\DroplockerObjects\EmailTemplate;
use App\Libraries\DroplockerObjects\Business_Language;
use App\Libraries\DroplockerObjects\LanguageView;
class UnitTest extends PHPUnit_Framework_TestCase {
    protected $CI;
    protected $business_id; //All tests will run against this business.
    protected $url;
    protected $business;
    
    public static $paypro_test_credit_card = "4217651111111119"; //When testing the paypro gateway, this credit card number should be used when procesing a transaction.
    public static $paypro_test_credit_card_expiration_month = 12;
    public static $paypro_test_credit_card_expiration_year = 14;
    const droidVersion = 'mobile_2_4';

    public function __construct() {
        parent::__construct();
        $this->business_id = 3;
        $this->business = new Business($this->business_id, false);
    
        $database_identifier = getenv("DATABASE_IDENTIFIER");
        
        if (empty($database_identifier))
        {
            throw new \Exception("'DATABASE_IDENTIFIER' must be set in phpunit.xml");
        }
        $this->CI = & get_instance();
        $this->CI->db = $this->CI->load->database($database_identifier, TRUE);
        date_default_timezone_set("GMT");
              
    }
    
    public function setUp() {
        parent::setUp();
        
        if ($this->CI->db->hostname == "droplockerbizzie.cvtvvbrpfazq.us-east-1.rds.amazonaws.com") {
            die("Refusing to run tests on Production server.");
        }
        
        //The following statements retrieve the domain extension set in phpunit.xml.
        $this->domain_extension = getenv("DOMAIN_EXTENSION");
        if (empty($this->domain_extension)) {
            throw new Exception("'DOMAIN_EXTENSION' must be set in phpunit.xml");
        }
        
        $this->url = "http://droplocker.".getenv("DOMAIN_EXTENSION");
        Server_Meta::set("amazon_ses_username", "asdf");
        Server_Meta::set("amazon_ses_password", "asdf");
    }
    /**
     * Creates a random email Tempalte
     * @param type $business_id
     * @return \EmailTemplate
     */
    public function create_random_emailTemplate($business_id) {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }
        $emailAction = self::create_random_emailAction();
        $emailTemplate = new EmailTemplate();
        
        $emailTemplate->business_id = $business_id;
        $emailTemplate->emailAction_id = $emailAction->emailActionID;
        $emailTemplate->emailSubject = self::get_random_word();
        $emailTemplate->emailText = self::get_random_word();
        $emailTemplate->bodyText = self::get_random_word();
        $emailTemplate->smsSubject = self::get_random_word();
        $emailTemplate->smsText = self::get_random_word();
        $emailTemplate->adminSubject = self::get_random_word();
        $emailTemplate->adminText = self::get_random_word();
        $emailTemplate->bcc = self::get_random_word();
        $emailTemplate->base_id = rand(1,1000);
        $emailTemplate->save();
        $emailTemplate->refresh();
        return $emailTemplate;        
    }
    /**
     * Creates a random email Action
     * @return \EmailAction
     */
    public function create_random_emailAction() {
        $emailAction= new EmailAction();
        $emailAction->action = self::get_random_word();
        $emailAction->description = self::get_random_word();
        $emailAction->name = self::get_random_word();
        $emailAction->subject = self::get_random_word();
        $emailAction->body = self::get_random_word();
        $emailAction->bodyText = self::get_random_word();
        $emailAction->adminSubject = self::get_random_word();
        $emailAction->adminBody = self::get_random_word();
        $emailAction->smsSubject = self::get_random_word();
        $emailAction->smsText = self::get_random_word();
        $emailAction->save();
        return $emailAction;
    }
    /**
     * Creates a random reminder
     * @param int $business_id
     * @return \Reminder
     */
    public function create_random_reminder($business_id) {
        $reminder = new Reminder();
        $reminder->business_id = $business_id;
        $couponTypes = Reminder::$couponTypes;
        $reminder->couponType = $couponTypes[array_rand($couponTypes)];
        $reminder->days = rand(1,30);
        $reminder->orders = rand(1,10);
        $reminder->expireDays = rand(1,30);
        $reminder->subject = self::get_random_word(3);
        $reminder->body =  self::get_random_word(rand(1,50));
        $reminder->noDiscount = rand(0,1);
        $reminder->description = self::get_random_word(rand(1,20));
        $reminder->status = self::get_random_word('active');
        $reminder->base_id = rand(1,10000);
        $reminder->save();
        $reminder->refresh();
        return $reminder;
    }
    
    /**
     * Creates a random order charge
     * @param int orderID
     * @return \App\Libraries\DroplockerObjects\OrderCharge
     */
    public static function create_random_orderCharge($orderID) {
        $CI = &get_instance();
        $orderCharge = new OrderCharge();
        
        $orderCharge->order_id = $orderID;
        $orderCharge->chargeType = "asdf";
        $orderCharge->chargeAmount = 5;
        $CI->load->model("order_model");
        $order = $CI->order_model->get_by_primary_key($orderID);
        $customerDiscount = self::create_random_customerDiscount($order->business_id);
        $orderCharge->customerDiscount_id = $customerDiscount->customerDiscountID;
        $orderCharge->save();
        return $orderCharge;
    }
    
    /**
     * Creates a random customer discount.
     * @param int $business_id
     * @return \App\Libraries\DroplockerObjects\CustomerDiscount
     */
    public static function create_random_customerDiscount($business_id =3) {
        $customer = self::create_random_customer_on_autopay();
        $customerDiscount = new CustomerDiscount();
        $customerDiscount->customer_id = $customer->customerID;
        $customerDiscount->amount = 5;
        $customerDiscount->frequency = "one time";
        $customerDiscount->active = 1;
        $customerDiscount->business_id = $business_id;
        $customerDiscount->amountType = "dollars";
        $customerDiscount->frequency = "every order";
        $customerDiscount->save();
        return $customerDiscount;
    }
     /**
     * Creates a random customer item.
     * @param int $customer_id
     * @param int $item_id
     * @return \App\Libraries\DroplockerObjects\Customer_Item
     * @throws Database_ExceptionCreates a random customer item 
     */
    public static function create_random_customer_item($customer_id,$item_id)
    {
        $customer_item = new Customer_Item();
        $customer_item->customer_id = $customer_id;
        $customer_item->item_id = $item_id;
        try {
            $customer_item->save();
        }
        catch (\Database_Exception $e) {
            if ($e->get_error_number() == 1062) { //If the customer item already exists in the database, we log the error and continue.
                trigger_error("Customer item with customer ID {$customer_item->customer_id} and item ID {$customer_item->item_id} already exists");
                $customer_items = Customer_Item::search_aprax(array("customer_id" => $customer_id, "item_id" => $item_id));
                return $customer_items[0];
            }
            else {
                throw $e;
            }
        }
        return $customer_item;
    }
    
    /**
     * Creates a random product location price
     * @param int $business_id
     * @param int $product_process_id
     * @param int $location_id
     * @return \App\Libraries\DroplockerObjects\Product_Location_Price
     */
    public static function create_random_product_location_price($business_id, $product_process_id, $location_id) {
        $product_location_price = new \App\Libraries\DroplockerObjects\Product_Location_Price;
        $product_location_price->price = rand(1,60);
        $product_location_price->product_process_id = $product_process_id;
        $product_location_price->location_id = $location_id;
        $product_location_price->business_id = $business_id;
        $product_location_price->save();
        return $product_location_price;
    }
    
    /**
     * Creates a random business language.
     * @param int $business_id
     * @param int $language_id
     * @return \App\Libraries\DroplockerObjects\Business_Language
     */
    public static function create_random_business_language($business_id, $language_id) {
        return Business_Language::create($business_id, $language_id);
    }
    /**
     * Returns a random number of characters
     * @param int $number_of_characters
     * @param int $charset
     * @return string 
     */
    public static function get_random_string($number_of_characters, $charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") {
        $charset_length = strlen($charset);
        $random_string = "";
        for ($x = 0; $x < $number_of_characters; $x++)
        {
            $random_string .= substr($charset, rand(0, $charset_length), 1);
        }
        return $random_string;
    }
    /**
     * Creates a random LanguageKey Business Language entity.
     * @param int $incoming_business_id
     * @param int $incoming_languageKey_id
     * @param int $incoming_language_id
     * @return \App\Libaries\DroplockerObjects\LanguageKey_Business_Language
     */
    public static function create_random_languageKey_business_language($incoming_business_id = null, $incoming_languageKey_id = null, $incoming_language_id = null, $incoming_translation = null) {
        if (is_null($incoming_business_id)) {
            $business = self::create_random_business();
            $business_id = $business->{Business::$primary_key};
        }
        else {
            $business_id = $incoming_business_id;
        }
        if (is_null($incoming_languageKey_id)) {
            $languageKey = self::create_random_languageKey();
            $languageKey_id = $languageKey->{LanguageKey::$primary_key};
        }
        else {
            $languageKey_id = $incoming_languageKey_id;
        }
        if (is_null($incoming_language_id)) {
            $language = self::create_random_language();
            $language_id = $language->languageID;
        }
        else {
            $language_id = $incoming_language_id;
        }
        if (is_null($incoming_translation)) {
            $translation = self::get_random_word(rand(1,20));
        }
        else {
            $translation = $incoming_translation;
        }
        
        $languageKey_business_language = LanguageKey_Business_Language::create($business_id, $language_id, $languageKey_id, $translation);
        return $languageKey_business_language;
    }
    
    /**
     * Creates a random language key entity
     * @param int $incoming_languageView_id
     * @return \App\Libraries\DroplockerObjects\languageKey
     */
    public static function create_random_languageKey($incoming_languageView_id = null, $incoming_default_text = null) {
        $name = self::get_random_word(1);
        if (is_null($incoming_default_text)) {
            $defaultText = self::get_random_word(3);
        }
        else {
            $defaultText = $incoming_default_text;
        }
        
        if (is_null($incoming_languageView_id)) {
            $languageView = self::create_random_languageView();
            $languageView_id = $languageView->{LanguageView::$primary_key};
        }
        else {
            $languageView_id = $incoming_languageView_id;
        }
        $languageKey = LanguageKey::create(compact('name', 'defaultText', 'languageView_id'));
        return $languageKey;
    }
    /**
     * Creates a language view entity with random properties
     * @return \App\Libraries\DroplockerObjects\LanguageView
     */
    public static function create_random_languageView() {
        $name = self::get_random_string(5);
        $languageView = LanguageView::create(compact('name'));
        return $languageView;
    }
    /**
     * Creates a language entity with random properties
     * @return \App\Libaries\DroplockerObjects\Language
     */
    public static function create_random_language() {
        $tag = self::get_random_string(2);
        $description = self::get_random_word(2);
        $existing_languages = Language::search_aprax(array("tag" => $tag));
        if (empty($existing_languages)) {
            $language = Language::create($tag, $description);
        }
        else {
            $language = $existing_languages[0];
        }
        return $language;
    }
    /**
     * Creates a random barcode
     * @param int $business_id THe business that the item shall be associated with
     * @param int $item_id Optional. The item that the barcode shall be associated with.
     * @param int customer_id Optional. The customer ID associated to create a customer Item
     * @return \App\Libraries\DroplockerObjects\Barcode 
     */
    public static function create_random_barcode($business_id = 3, $item_id = null, $customer_id = null)
    {
        $barcode_number = rand(11111111, 99999999);
        
        $existing_barcodes = Barcode::search_aprax(array("barcode" => $barcode_number));
        foreach ($existing_barcodes as $existing_barcode)
        {
            $existing_barcode->delete();
        }
        $barcode = new Barcode();
        if (is_null($item_id))
        {
            $item = self::create_random_item(null, $business_id);
            $item->business_id = $business_id;
            $item->save();
            $barcode->item_id = $item->itemID;
        }
        else
        {
            $barcode->item_id = $item_id;
        }
        
        if (!is_null($customer_id))
        {
            $customer = new Customer($customer_id);
            $customer_item = new Customer_Item();
            $customer_item->customer_id = $customer->{Customer::$primary_key};
            $customer_item->item_id = $barcode->item_id;
            $customer_item->save();
        }
        
        $barcode->business_id = $business_id;
        $barcode->barcode = $barcode_number;
        $barcode->save();
        return $barcode;
    }
    
    /**
     * Creates a new business
     * @return \App\Libraries\DroplockerObjects\Business
     */
    public static function create_random_business()
    {
        $business = new Business();
        $business->companyName = self::get_random_word(2);
        $business->blank_customer_id = 0;
        $business->timezone = "America/Los_Angeles";
        $business->serviceTypes = 'a:6:{i:1;a:4:{s:14:"serviceType_id";s:1:"1";s:6:"active";i:1;s:7:"service";s:13:"Wash and Fold";s:11:"displayName";s:27:"Wash and Fold ($15 Minimum)";}i:2;a:4:{s:14:"serviceType_id";s:1:"2";s:6:"active";i:1;s:7:"service";s:9:"Dry Clean";s:11:"displayName";s:9:"Dry Clean";}i:4;a:4:{s:14:"serviceType_id";s:1:"4";s:6:"active";i:1;s:7:"service";s:5:"Other";s:11:"displayName";s:5:"Other";}i:5;a:4:{s:14:"serviceType_id";s:1:"5";s:6:"active";i:1;s:7:"service";s:10:"Shoe Shine";s:11:"displayName";s:32:"Shoe Shine (3 - 5 Business Days)";}i:6;a:4:{s:14:"serviceType_id";s:1:"6";s:6:"active";i:1;s:7:"service";s:16:"Laundered Shirts";s:11:"displayName";s:16:"Laundered Shirts";}i:7;a:4:{s:14:"serviceType_id";s:1:"7";s:6:"active";i:1;s:7:"service";s:11:"Shoe Repair";s:11:"displayName";s:32:"Shoe Repair (5-10 Business Days)";}}';
        $business->save();
        return $business;
    }
    
    /**
     * Retrieves a random hardcoded crease ID.
     * @return string
     */
    public static function get_random_crease()
    {
        //Note, the crease types are hardcoded into the item edit view.
        $crease_ids = array("no", "center", "none");
        $crease_id = $crease_ids[array_rand($crease_ids)];
        return $crease_id;
    }
    /**
     * Creates a randomly generated business laundry plan.
     * @param int $business_id THe business that the laundry plan is associated with
     * @return \App\Libraries\DroplockerObjects\BusinessLaundryPlan
     */
    public static function create_random_businessLaundryPlan($business_id)
    {
        $businessLaundryPlan = new BusinessLaundryPlan();
        $businessLaundryPlan->business_id = $business_id;
        $businessLaundryPlan->name = UnitTest::get_random_word(3);
        $businessLaundryPlan->caption = UnitTest::get_random_word(5);
        $businessLaundryPlan->description = UnitTest::get_random_word(5);
        $businessLaundryPlan->price = rand(1,10);
        $businessLaundryPlan->days = rand(15,75);
        $businessLaundryPlan->pounds = rand(30,200);
        $businessLaundryPlan->save();
        return $businessLaundryPlan;
    }
    
    public static function create_random_image($business_id)
    {
        $image = new Image();
        $image->filename = self::get_random_word(1);
        $image->path = self::get_random_word(1);
        $image->business_id = $business_id;
        $image->save();
        return $image;
    }
    
    /**
     * Creates a random quick item
     * @param int $business_id
     * @return \App\Libraries\DroplockerObjects\QuickItem
     */
    public static function create_random_quickItem($business_id)
    {
        $quickItem = new QuickItem();
        $quickItem->business_id = $business_id;
        $quickItem->name = self::get_random_word(rand(1,5));
        $product = self::create_random_product($business_id);
        $quickItem->product_id = $product->{Product::$primary_key};
        $process = self::get_random_object("Process");
        $quickItem->process_id = $process->{Process::$primary_key};
        $starch = self::get_random_object("StarchOnShirts");
        $quickItem->starch_id = $starch->{StarchOnShirts::$primary_key};
        $quickItem->crease_id = self::get_random_crease();
        $quickItem->notes = self::get_random_word(rand(1,30));
        $quickItem->color = "white";
        $quickItem->save();
        return $quickItem;
    }
    
    /**
    * Creates an new, unpaid, empty order with a status of 'picked up'
    * 
    * if $customer_id is not passed, a customer will be created on autopay
    * if a $locker_id is not passed, a location and a locker in that location is created
    * 
    * @param int $business_id the business for which the order will be associated with
    * @param int $customer_id an optional customer to associate the new order to. If no customer ID is specified, then this function creates a new customer
    * @param int $locker_id an optional locker to associate the new locker to. If No locker is specified, then this function creates a new locker.
    * @return \App\Libraries\DroplockerObjects\Order 
    */
    public static function create_random_order($business_id = 3, $customer_id = null, $locker_id = null)
    {
        $order = new Order();
        if (is_null($customer_id))
        {
            $customer = self::create_random_customer_on_autopay($business_id);
        }
        else
        {
            $customer = new Customer($customer_id);
        }
        $order->customer_id = $customer->{Customer::$primary_key};
        
        $bag = self::create_random_bag($business_id, $customer->customerID);

        $bag->get_relationships();
        $order->bag_id = $bag->{Bag::$primary_key};
        $order->business_id = $business_id;
        $order->dateCreated = new DateTime();
        if ($locker_id == null)
        {
            $location = self::create_random_location($business_id);
            $locker = self::create_random_locker($location->locationID, $business_id);
            $order->locker_id = $locker->{Locker::$primary_key};
        } 
        else 
        {    
            $order->locker_id = $locker_id;
        }
        
        $order->orderStatusOption_id = 1;
        $order->orderPaymentStatusOption_id = 1;
        $order->save();
        $order->get_relationships();
        return $order;
        
    }
    
    /**
     * Creates a random order item for an order that has a random price and a quantity of "1".
     * @param int $orderID 
     */
    public static function create_random_orderItem($orderID)
    {
        if (!is_numeric($orderID)) {
            throw new \InvalidArgumentException("'orderID' must be numeric");
        }
        $orderItem = new OrderItem();

        $order = new Order($orderID);
        $supplier = self::get_random_object("Supplier", FALSE, array("business_id" => $order->business_id));
        $item = self::create_random_item(0, $order->business_id);
        $product_process = self::create_random_product_process($order->business_id);
        
        $orderItem->item_id = $item->{Item::$primary_key};
        $orderItem->product_process_id = $product_process->{Product_Process::$primary_key};
        $orderItem->order_id = $order->{Order::$primary_key};
        $orderItem->supplier_id = $supplier->{Supplier::$primary_key};
        $orderItem->qty = 1;
        $orderItem->unitPrice = rand(1,30);
        
        $orderItem->save();
        return $orderItem;
    }    

    /**
     * creates an item, and adds a barcode to the item
     * if you pass a customer_id, the item will get added to the customer
     * 
     * @param int $customer_id
     * @param int $business_id
     * @return Item object
     * 
     */
    public static function create_random_item($customer_id='', $business_id=3)
    {
      $CI =& get_instance();

       
       $product_process = self::create_random_product_process($business_id);
       $item = new Item();
       $item->product_process_id = $product_process->{Product_Process::$primary_key};
       $item->business_id = $business_id;
       $item->save();

       
        // if customer was passed, add the item to the customer
       if(!empty($customer_id)){
           $sql = "INSERT INTO customer_item (customer_id, item_id) VALUES ({$customer_id}, {$item->{Item::$primary_key}});";
           $CI->db->query($sql);
       }
        
       $itemObj = new Item($item->{Item::$primary_key}, true);
       return $itemObj;
    }
    
    
    /**
     * creates a new product process
     * 
     * @param int business_id default(3)
     * @param int product_id The product to associate the product process with. If none is specified, then a new product is created as well
     * @param int $process_id default(1)
     
     * @return Product_Process Object
     */
    public static function create_random_product_process($business_id = 3, $product_id = null, $process_id = null) {
        if (is_null($product_id)) {
            $product = UnitTest::create_random_product($business_id); 
            //Note, the create_random_product function s also expected to create a product process as well.
            $product_processes = Product_Process::search_aprax(array("product_id" => $product->{Product::$primary_key}), true);
            if (empty($product_processes)) {
                throw new \Exception("Could not find product process for randomly created product");
            }
            else {
                $product_process = $product_processes[0];
                
                return $product_process;
            }
            
        }
        else {
            $product = new Product($product_id);
            $product_process = new \App\Libraries\DroplockerObjects\Product_Process();

            $product_process->product_id = $product->{Product::$primary_key};

            if ($process_id == null)
            {
                $process_id = 1;
            }

            $price = rand(1,75);
            $product_process->process_id = $process_id;

            $product_process->price = $price;
            $product_process->taxable = 0;
            $product_process->active = 1;
            $product_process->dateCreated = date("Y-m-d H:i:s");
            try
            {
                $product_process->save();
            }
            catch (Database_Exception $create_product_process_exception)
            {
                //If there is already a product process, then we simply reeuse that one.
                if ($create_product_process_exception->get_error_number() == 1062)
                {
                    $product_processes = Product_Process::search_aprax(array("product_id" => $product->productID, "process_id" => $process_id));
                    if (empty($product_processes))
                    {
                        throw new \Exception("Could not find duplicate product process");
                    }
                    else
                    {
                        $product_process = $product_processes[0];
                    }
                }
                else
                {
                    throw $create_product_process_exception;
                }
            }

            $product_process->get_relationships();

            return $product_process;
        }
    }
    
    /**
     * Creates a random bag
     * @param int $business_id The business that the bag shall be associated withy
     * @param int $customer_id Optional, an existing customer that the bag shall be associated with
     * @return \App\Libraries\DroplockerObjects\Bag
     * @throws \InvalidArgumentException
     */
    public static function create_random_bag($business_id, $customer_id = null)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }
        //The following statement verifies that the business exists in the database
        $business = new Business($business_id);
        
        if (is_null($customer_id))
        {
            $customer = self::create_random_customer_on_autopay($business_id);
        }
        else
        {
            $customer = new Customer($customer_id);
        }
        $bag = new Bag();
        $bag->customer_id = $customer->{Customer::$primary_key};
        $bag->bagNumber = Bag::get_new_bagNumber();
        $bag->business_id = $business->{Business::$primary_key};
        try
        {
            $bag->save();
        }
        catch (Database_Exception $create_bag_exception)
        {
            if ($create_bag_exception->get_error_number() == 1062)
            {
                $bag->bagNumber = Bag::get_new_bagNumber();
                $bag->save();
            }
            else
            {
                throw $create_bag_exception;
            }
        }
        $bag->get_relationships();
        return $bag;
    }
    
    /**
     * creates a new product
     * 
     * @param int $business_id
     * @param \App\Libraries\DroplockerObjects\ProductCategory productCategory_id If null, then a new product category is created for the product
     * @return \App\Libraries\DroplockerObjects\Product
     */
    public static function create_random_product($business_id=3, $productCategory = null) {
        $CI =& get_instance();
        $CI->load->model('product_model');

        $words = self::get_random_word(2);
        if (is_null($productCategory))
        {
            $productCategory = UnitTest::create_random_productCategory($business_id);
        }
        else if (!($productCategory instanceof \App\Libraries\DroplockerObjects\ProductCategory)) {
            throw new \InvalidArgumentException("'productCategory' must be instance of \App\Libraries\DroplockerObjects\ProductCategory");
        }
        $product = new Product();
        $product->name = $words;
        $product->unitOfMeasurement = "qty";
        $product->displayName = $words;
        $product->productType = 'product';
        $product->sortOrder = 0;
        $product->productCategory_id = $productCategory->{ProductCategory::$primary_key};
        $product->business_id = $business_id;
        $product->save();
        $product->get_relationships();
       
        try
        {
            self::create_random_product_process($business_id, $product->{Product::$primary_key});
        }
        catch (Database_Exception $e)
        {
            if ($e->get_error_number() == 1062)
            {
                //If a product process already exists for the product, we simply ignore the error and continue.
            }
            else
            {
                throw $e;
            }
        }

        return $product;
    }
    /**
     * Creates a random wash and fold service type product category
     * @param int $business_id
     * @throws \InvalidArgumentException
     */
    public static function create_random_productCategory($business_id, $module_id = 1)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $productCategory = new ProductCategory();
        $productCategory->name = self::get_random_word(1);
        $productCategory->note = self::get_random_word(5);
        $productCategory->slug = self::get_random_word(1);
        $productCategory->business_id = $business_id;
        $productCategory->module_id = $module_id;
        $productCategory->save();
        return $productCategory;
    }
    /**
     * Creates a random coupon for the specified business
     * @param int $business_id
     * @return \App\Libraries\DroplockerObjects\Coupon
     * @throws \InvalidArgumentException
     */
    public static function create_random_coupon($business_id)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric");
            
        }
        $business = new Business($business_id);
        $coupon = new Coupon();
        $coupon->prefix = self::get_random_string(2);
        $coupon->description = self::get_random_word(1);
        $coupon->amountType = array_rand(Coupon::$amountTypes,1);
        $coupon->frequency = array_rand(Coupon::$frequencies,1);
        $coupon->code = rand(1000,9999);
        $coupon->amount = rand(1,100);
        $coupon->business_id = $business->{Business::$primary_key};
        $coupon->save();
        return $coupon;
    }
    /**
     * Creates a random active locker for testing
     * @param int $location_id the location to associate the locker with
     * @return \App\Libraries\DroplockerObjects\Locker 
     */
    public static function create_random_locker($location_id, $get_relationships = true)
    {
        if (!is_numeric($location_id))
        {
            throw new \InvalidArgumentException("'location_id' must be numeric");
        }
        $locker = new Locker();
        
        $locker->location_id = $location_id;
        $locker->lockerName = rand(1,9999999);
        
        $existing_lockers = Locker::search_aprax(array("lockerName" => $locker->lockerName));
        foreach ($existing_lockers as $existing_locker)
        {
            $existing_locker->delete();
        }

        $lockerStyle = self::get_random_object("LockerStyle");
        $lockerLockType = self::get_random_object("LockerLockType");
        $locker->lockerStatus_id = 1; //Note, a locker status of 1 is expected to be an active locker.
        $locker->lockerStyle_id = $lockerStyle->{LockerStyle::$primary_key};
        $locker->lockerLockType_id = $lockerLockType->{LockerLockType::$primary_key};

        $locker->save();
        $locker->get_relationships();
        return $locker;
    }
    /**
     * Creates a non-random active locker for testing
     * @param int $location_id the location to associate the locker with
     * * @param int $locker_style 
     * * @param int $locker_lock_type 
     * @return \App\Libraries\DroplockerObjects\Locker 
     */
    public static function create_nonrandom_locker($location_id, $locker_style = 1, $locker_lock_type = 1, $get_relationships = true)
    {
        if (!is_numeric($location_id))
        {
            throw new \InvalidArgumentException("'location_id' must be numeric");
        }
        $locker = new Locker();
        
        $locker->location_id = $location_id;
        $locker->lockerName = rand(1,9999999);
        
        $existing_lockers = Locker::search_aprax(array("lockerName" => $locker->lockerName));
        foreach ($existing_lockers as $existing_locker)
        {
            $existing_locker->delete();
        }

        $locker->lockerStatus_id    = 1; //Note, a locker status of 1 is expected to be an active locker.
        $locker->lockerStyle_id     = $locker_style;
        $locker->lockerLockType_id  = $locker_lock_type;

        $locker->save();
        $locker->get_relationships();
        return $locker;
    }    
    /**
     * Creates a random customer on autopay for testing
     * Note, every customer is saved with the password "webdriver"
     * @return \App\Libraries\DroplockerObjects\Customer
     */
    public static function create_random_customer_on_autopay($business_id = 3)
    {
        $email = "webdriver_" . self::get_random_word() . "@testing.com";
        
        $customers = Customer::search_aprax(array("email" => $email, "business_id" => $business_id));
        foreach ($customers as $customer)
        {
            $customer->delete();
        }
        $customer = new Customer();
        $customer->email = $email;
        $customer->password = "webdriver";
        $customer->firstName = self::get_random_word();
        $customer->lastName = self::get_random_word();
        $customer->address1 = sprintf("%s %s %s", rand(1,999), self::get_random_word(), self::get_random_word());
        $customer->address2 = sprintf("%s %s %s", rand(1,999), self::get_random_word(), self::get_random_word());
        $customer->city = "San Francisco";
        $customer->state = "CA";
        $customer->zip = "94122";
        $customer->phone = "907-488-3221";
        $customer->sms = "9074882231@example.com";
        $customer->business_id=$business_id;
        $customer->autoPay=1;
        $customer->preferences = 'a:11:{s:9:"colorTemp";s:3:"123";s:9:"whiteTemp";s:3:"125";s:9:"detergent";s:3:"255";s:6:"bleach";s:3:"259";s:7:"fabSoft";s:3:"132";s:10:"dryerSheet";s:3:"134";s:6:"dryLow";s:3:"142";s:9:"superWash";s:3:"143";s:5:"loads";s:3:"153";s:14:"starchOnShirts";s:1:"2";s:5:"notes";s:17:"Do not box shirts";}';
        $customer->save();
                
        $creditCard = new CreditCard();
        $creditCard->customer_id = $customer->{Customer::$primary_key};
        $creditCard->business_id = $customer->business_id;
        $creditCard->cardType = "Visa";
        $creditCard->cardNumber = "4111";
        $creditCard->expMo = 2;
        $creditCard->expYr = 2022;
        //$creditCard->csc=437;
        $creditCard->payer_id= "1358786820|0000013c-5e01-8d7c-0000-000078f8b3e5" ;
        $creditCard->save(); 
        $customer->refresh();
        return $customer;
    }
    /**
     * Creates a random transaction for an order.
     * @param int $order_id 
     * @return App\Libraries\DroplockerObjects\Transaction
     */
    public static function create_random_transaction($order_id)
    {
        
        $order = new Order($order_id, false);
        $transaction = new Transaction();
        $transaction->customer_id = $order->customer_id;
        $transaction->resultCode = 0;
        $transaction->message = "Approved";
        $transaction->order_id = $order->{Order::$primary_key};
        $transaction->zipMatch = "Match";
        $transaction->cscMatch = "Service Not Requested";
        $transaction->business_id = $order->business_id;
        $transaction->processor = "verisign";
        $transaction->amount = "5.00";
        $transaction->pnref = md5(time());
        $transaction->resultCode = 0;   
        $transaction->type = "Auth";
        $transaction->save();
        return $transaction;
    }
    
    /**
     * Creates a random order status for an order
     * @param int $order_id
     * @return \OrderStatus 
     */
    public static function create_random_orderStatus($order_id)
    {
        $order = new Order($order_id, false);
        $employee = self::get_random_employee($order->business_id);
        $orderStatusOption = self::get_random_orderStatusOption();
        
        $orderStatus = new OrderStatus();
        $orderStatus->order_id = $order->{Order::$primary_key};
        $orderStatus->orderStatusOption_id = $orderStatusOption->{OrderStatusOption::$primary_key};
        $orderStatus->employee_id = $employee->{Employee::$primary_key};
        $location = self::create_random_location($order->business_id);
        $locker = self::create_random_locker($location->locationID);
        $orderStatus->locker_id = $locker->lockerID;
        $orderStatus->save();
        return $orderStatus;
    }
    /**
     * Creates a random active location
     * @param int $business_id The business to associate the location with
     * 
     * @return \App\Libraries\droplockerObjects\Location
     */
    public static function create_random_location($business_id)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }
        $location = new Location();
        $location->city = self::get_random_word();
        $location->address = "123 " . self::get_random_word();
        $location->state = "CA";
        $location->locationType_id = 1; //Note,we do not want to create an electronic locker, since that requires an inprocess Locker.
        $location->serviceType = "daily";
        $location->business_id = $business_id;
        $location->public = 'Yes';
        $location->status = "active";
        $location->companyName = self::get_random_word(rand(1,3));
        $location->save();
        
        //Note, every location requires an inProcess locker. If the location does not already have an inProcess locker associated with it, then one is created.
        $standard_locker = self::create_nonrandom_locker($location->{Location::$primary_key});
        $inProcess_lockers = Locker::search_aprax(array("location_id" => $location->{Location::$primary_key}, "lockerName" => "InProcess"));
        if (empty($inProcess_lockers))
        {
            $inProcess_locker = self::create_random_locker($location->{Location::$primary_key});
            $inProcess_locker->lockerName = "InProcess";
            $inProcess_locker->lockerStyle_id = 7;
            $inProcess_locker->save();            
        }
        $location->get_relationships();
        return $location;
    }
    
    /**
     *
     * @param int $number_of_words
     * @return string Each word is separated by a space
     */
    public static function get_random_word($number_of_words=1)
    {
        exec("shuf -n $number_of_words /usr/share/dict/words", $words);
        if (empty($words)) //If the above command fails, we try another way.
        {
            $words = array();
            for ($x = 0; $x < $number_of_words; $x++)
            {
                $words[] = system('sed `perl -e "print int rand(99999)"`"q;d" /usr/share/dict/words');
            }
        }
        $fullWord = join(" ", $words);
        // remove the apostraphes
        return str_replace("'", "", $fullWord);
    }
    
    
    
    public function runBare()
    {
        echo ("\n <<<Running {$this->getName()}...>>>\n");
        parent::runBare();
    }
    
    public function testUnitTestSanity()
    {
        $this->assertEquals(true,true);
    }
    /**
     * The following fucntion retrieves a random product process from the database
     * @return type A database query result
     * @throws Exception 
     */
    protected function get_random_product_process()
    {
        $CI = &get_instance();
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_product_process_query = $CI->db->get("product_process");
        if ($CI->db->_error_message())
        {
            throw new Exception($CI->db->_error_message());
        }
        $random_product_process = $get_random_product_process_query->row();
        return $random_product_process;
    }
    /**
     * Retrieves a random order status option from the database.
     * @return \App\Libraries\DroplockerObjects\OrderStatusOption
     * @throws Database_Exception 
     */
    public static function get_random_orderStatusOption()
    {
        $CI = &get_instance();
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_orderStatusOption_query = $CI->db->get("orderStatusOption");
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->_last_query(), $CI->db->_error_number());
        }
        $random_orderStatusOption_result = $get_random_orderStatusOption_query->row();
        $random_orderStatusOption = new \App\Libraries\DroplockerObjects\OrderStatusOption($random_orderStatusOption_result->orderStatusOptionID);
        return $random_orderStatusOption;
    }
    /**
     * Retrieves a random customer from the database associated with the specified business
     * 
     * Added opional options array for getting more specific customers.
     * 
     * 
     * @param int $business_id
     * @param array $options
     * @return App\Libraries\DroplockerObjects\Customer
     * @throws Database_Exception 
     */
    public static function get_random_customer($business_id, $options = array())
    {
        $CI = &get_instance();
        $CI->db->order_by("RAND()");
        $CI->db->where("business_id", $business_id);
        
        if(isset($options['inactive'])){
            $CI->db->where('inactive', $options['inactive']);
        }
        if(isset($options['autoPay'])){
            $CI->db->where('autoPay', $options['autoPay']);
        }
        if(isset($options['invoice'])){
            $CI->db->where('invoice', $options['invoice']);
        }
        
        $CI->db->where('city !=', "");
        
        $CI->db->limit(1);
        $get_random_customer_query = $CI->db->get("customer");
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_customer = $get_random_customer_query->row();
        return $random_customer;
    }
    
    /**
     * gets a random supplier for a business
     * 
     * @param int  $business_id default = 3
     * @throws Exception "Business has not suppliers"
     * @return Supplier Object
     */
    function get_random_supplier($business_id = 3){
        $suppliers = \App\Libraries\DroplockerObjects\Supplier::getBusinessSuppliers($business_id);
        if($suppliers){
            return $suppliers[rand(0, sizeof($suppliers)-1)];
        } else {
            throw new Exception("Business has no suppliers");
        }
    }
    /**
     * Retrieves a random employee from the database with the specified business ID
     * @param int $business_id
     * @return Employee
     * @throws Database_Exception 
     */
    public static function get_random_employee($business_id) {
        $CI = &get_instance();
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_employee_query = $CI->db->get("employee");
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $get_random_employee_result = $get_random_employee_query->row();
        $random_employee = new App\Libraries\DroplockerObjects\Employee($get_random_employee_result->{App\Libraries\DroplockerObjects\Employee::$primary_key});
        return $random_employee;
    }
    
    
    /**
     * gets a random completed order
     * 
     * @param int $business_id
     * @return stdClass Object
     */
    function get_random_complete_order($business_id = 3){
        $sql = "SELECT orderID, customer_id,
        (SELECT count(*) FROM locker WHERE lockerID = orders.locker_id) as lockerCount
        FROM (`orders`)
        LEFT JOIN `customer` ON `customerID`=`customer_id`
        LEFT JOIN `orderCharge` ON `orderCharge`.`order_id`=`orderID`
        WHERE `orders`.`business_id` = {$business_id}
        AND `orders`.`bag_id` IS NOT NULL
        AND `orders`.bag_id != 0
        AND `customer_id` IS NOT NULL
        AND `orders`.`customer_id` != 0
        AND `customer`.`hold` = 0
        AND `orderPaymentStatusOption_id` != 3
        -- AND customerDiscount_id IS NOT NULL
        AND `orderStatusOption_id` = 10
        AND dateCreated != '0000-00-00 00:00:00'
        ORDER BY RAND()
        LIMIT 1";
        
        $order = $this->CI->db->query($sql)->row();
        return $order;
    }
    
    /**
     * Retrieves a rfandom location type from the database.
     * @return \App\Libraries\DroplockerObjects\LocationType
     * @throws Database_Exception 
     */
    public static function get_random_locationType() {
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\LocationType::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_locationType_query = $CI->db->get(\App\Libraries\DroplockerObjects\LocationType::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_locationType_result = $get_random_locationType_query->row();
        $random_locationType = new App\Libraries\DroplockerObjects\LocationType($random_locationType_result->{\App\Libraries\DroplockerObjects\LocationType::$primary_key});
        return $random_locationType;
    }
    
    /**
     * Retreives a random barcode
     * @return \App\Libraries\DroplockerObjects\Barcode
     * @throws Database_Exception 
     */
    public static function get_random_barcode($business_id, $get_relationships = false)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\Barcode::$primary_key);
        $CI->db->order_by("RAND()");
        
        $CI->db->join('item', 'barcode.item_id=item.itemID');
        $CI->db->join('product_process', 'item.product_process_id=product_processID');
        $CI->db->join('product', 'product_process.product_id=product.productID');

        $CI->db->where("barcode.business_id", $business_id);
        $CI->db->limit(1);
        $get_random_barcode_query = $CI->db->get(App\Libraries\DroplockerObjects\Barcode::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_barcode_result = $get_random_barcode_query->row();
        $random_barcode = new App\Libraries\DroplockerObjects\Barcode($random_barcode_result->{App\Libraries\DroplockerObjects\Barcode::$primary_key}, $get_relationships);
        return $random_barcode;
    }
    
    /**
     * Retreives a random transaction
     * @param type $business_id
     * @return \App\Libraries\DroplockerObjects\Transaction
     * @throws \InvalidArgumentException
     * @throws Database_Exception 
     */
    public static function get_random_transaction($business_id)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\Transaction::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->where("business_id", $business_id);
        $CI->db->limit(1);
        $get_random_transaction_query = $CI->db->get(App\Libraries\DroplockerObjects\Transaction::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_transaction_result = $get_random_transaction_query->row();
        $random_transaction = new \App\Libraries\DroplockerObjects\Transaction($random_transaction_result->{\App\Libraries\DroplockerObjects\Transaction::$primary_key}, true);
        return $random_transaction;
    }
    
    public static function get_random_product($business_id)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\Product::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->where("business_id", $business_id);
        $CI->db->limit(1);
        $get_random_product_query = $CI->db->get(App\Libraries\DroplockerObjects\Product::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_product_result = $get_random_product_query->row();
        $random_product = new \App\Libraries\DroplockerObjects\Product($random_product_result->{\App\Libraries\DroplockerObjects\Product::$primary_key}, true);
        return $random_product;       
    }

    public static function get_random_productCategory($business_id)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\ProductCategory::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->where("business_id", $business_id);
        $CI->db->limit(1);
        $get_random_productCategory_query = $CI->db->get(App\Libraries\DroplockerObjects\ProductCategory::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_productCategory_result = $get_random_productCategory_query->row();
        $random_productCategory = new \App\Libraries\DroplockerObjects\ProductCategory($random_productCategory_result->{\App\Libraries\DroplockerObjects\ProductCategory::$primary_key});
        return $random_productCategory;       
    }
    /**
     * Retrieves a random module from the database.
     *
     * @return \App\Libraries\DroplockerObjects\Module
     * @throws \InvalidArgumentException
     * @throws \Database_Exception 
     */
    public static function get_random_module()
    {
        
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\Module::$primary_key);
        $CI->db->order_by("RAND()");
        //$CI->db->where("business_id", $business_id);
        $CI->db->limit(1);
        $random_module_result = $CI->db->get(App\Libraries\DroplockerObjects\Module::$table_name)->row();
        
      
        $random_module = new \App\Libraries\DroplockerObjects\Module($random_module_result->{\App\Libraries\DroplockerObjects\Module::$primary_key});
        return $random_module;               
    }
    
    /**
     * Selects a random location and a random locker in that location.
     * 
     * location object returned includes 
     * locationID, route_id, lockerTotal, foundInProcessLocker, lockerID
     * 
     * $type options = kiosk, lockers, concierge, home delivery, corner store, offices, wholesale location, undefined
     * @deprecated
     * @param string $type
     * @return object
     */
    function getRandomLockerInLocation($type="kiosk", $business_id=3){
        
        $CI =& get_instance(); 
        
        $sql = "SELECT locationID, route_id,
        (SELECT COUNT(*) from locker WHERE location_id = locationID) as lockerTotal,
        (SELECT count(*) from locker WHERE location_id = locationID AND lockerName = 'inProcess') as foundInProcessLocker
        FROM location
        INNER JOIN locationType ON locationTypeID = locationType_id
        WHERE locationType.name = '".ucwords($type)."'
        AND locationID NOT IN (483, 387) 
        AND location.business_id = {$business_id}
        HAVING lockerTotal > 5 AND foundInProcessLocker > 0
        ORDER BY rand() LIMIT 1";
        $get_location_query_result = $CI->db->query($sql);
        
        $location = $get_location_query_result->row();
        
        $sql = "SELECT lockerID FROM locker WHERE location_id = {$location->locationID} AND lockerName != 'InProcess' AND lockerStatus_id = 1  ORDER BY RAND() LIMIT 1";
        $locker = $CI->db->query($sql)->row();
        
        $location->lockerID = $locker->lockerID;
        
        return $location;
    }
    
    
    /**
     * gets a random claim.
     *
     * Since customers, locations and lockers might have been deleted we need to do an inner join to make sure they exist
     *
     * @return claim object
     */
    function get_random_claim(){
        $sql = "SELECT * FROM claim
        INNER JOIN locker ON lockerID = claim.locker_id
        INNER JOIN location ON locationID = locker.location_id
        INNER JOIN customer on claim.customer_id = customerID
        WHERE active = 1 ORDER BY rand() LIMIT 1";
        $claim = $this->CI->db->query($sql)->row();
        return $claim;
    }
    
    /**
     * adds a laundryplan to a customer
     * 
     * @param unknown $customer_id
     * @return int laundryPlan_id
     */
    function addLaundryPlanToCustomer($customer_id){
        $plan = $this->get_random_laundryPlan();
        
        // delete the customers laundryplans
        $this->CI->load->model('customerdiscount_model');
        $this->CI->customerdiscount_model->clear();
        $this->CI->customerdiscount_model->customer_id = $customer_id;
        $this->CI->customerdiscount_model->description = "Laundry Plan";
        $this->CI->customerdiscount_model->delete();
        
        // add the laundryplan
        $this->CI->load->model('laundryplan_model');
        $this->CI->load->model('order_model');
        $options['customer_id'] = $customer_id;
        $options['business_id'] = $this->business_id;
        $options['plan_id'] = $plan->businessLaundryPlanID;
        $result = $this->CI->order_model->new_laundry_plan_order($options);
        
        return $result['laundryplan_id'];
    }
    
    /**
     * gets a random laundryplan
     * 
     * @return stdClass Object from businessLaundryPlan table
     */
    function get_random_laundryPlan(){
        $sql = "SELECT * FROM businessLaundryPlan WHERE business_id = {$this->business_id} AND grouping = '' ORDER BY rand() LIMIT 1";
        return $this->CI->db->query($sql)->row();
    }
    
    
    /**
     * adds a customer discount to a customer
     * 
     * @param int $customer_id
     * @param float $amount
     * @param string $amountType
     * @param string $frequency
     * @param string $description
     * @param string $extendedDesc
     * @param date $expireDate
     * @return int|boolean customerDiscountID|FALSE
     */
    function addCustomerDiscountToCustomer($customer_id, $amount = "20", $amountType="dollars", $frequency="one time", $description="test discount", $extendedDesc="test discount", $expireDate=''){
       return \App\Libraries\DroplockerObjects\CustomerDiscount::addDiscount($customer_id, $amount, $amountType, $frequency, $description, $extendedDesc, $expireDate, 0 ,0);
    }

    

    
    /**
     * adds an item to a customer account
     * 
     * Used insert ignore in the query so the return variable might be null if the insert failed.
     * The reason why it would fail is that the customer already has the item in there account
     * 
     * @param int $item_id
     * @param int$customer_id
     * @return int|null insert_id()
     */
    function addItemToCustomer($item_id, $customer_id){
        $CI =& get_instance();
        $sql =  "INSERT IGNORE INTO customer_item (item_id, customer_id) VALUES ($item_id, $customer_id)";
        $CI->db->query($sql);
        return $CI->db->insert_id();
    }
    
    /**
     * gets a mock creditcard for testing
     * 
     * returns a creditCard array
     * creditCard array keys:
     * --------------
     * firstName
     * lastName
     * address1
     * address2 - empty
     * zip
     * city
     * state
     * expMonth
     * expYear
     * cardNumber
     * 
     * The address is hardcoded becuase if passing a new customer, there is no address set for the customer
     * 
     * @param Customer Object $customer
     * @param string $creditCardNumber defualt is the paypro test card number
     * @return array
     */
    function get_mock_creditCard($customer, $creditCardNumber = '4217651111111119'){
        $creditCard = array();
        $creditCard['firstName'] = $customer->firstName;
        $creditCard['lastName'] = $customer->lastName;
        $creditCard['address'] = '611 jean st';
        $creditCard['address2'] = '';
        $creditCard['zipcode'] = 94610;
        $creditCard['city'] = 'Oakland';
        $creditCard['state'] = 'CA';
        $creditCard['expMonth'] = 10;
        $creditCard['expYear'] = 2020;
        $creditCard['cardNumber'] = $creditCardNumber;
        return  $creditCard;
    }
    
    /**
     * authorized a card and then saves the creditcard in the creditcard table for the customer that is passed in the arguments.
     * The response object saves the auth transaction and you can search it by customer_id
     * 
     * Use the function get_mock_card to generate the creditCard array
     * 
     * @param array $creditCardArray
     * @param int $customer_id
     * @param int $business_id
     * @return Response Object
     */
    function authorizeCard($creditCardArray, $customer_id, $business_id = 3, $processorName='paypro'){
        $CI = & get_instance();
        $CI->load->library('creditcardprocessor');
        $processor = $CI->creditcardprocessor->factory($processorName, $business_id);
        $response = $processor->authorizeCard($creditCardArray,9999999,$customer_id, $business_id);
        
        if($response->get_status()=="SUCCESS"){
            // save the card
            // save the card
            $cardType = CreditCard::identify($creditCardArray['cardNumber']);
            $options['cardNumber'] = $creditCardArray['cardNumber'];
            $options['csc'] = '123';
            $options['expMo'] = $creditCardArray['expMonth'];
            $options['expYr'] = $creditCardArray['expYear'];
            $options['cardType'] = $cardType;
            $options['address1'] = $creditCardArray['address'];
            $options['state'] = $creditCardArray['state'];
            $options['city'] = $creditCardArray['city'];
            $options['zip'] = $creditCardArray['zipcode'];
            $options['payer_id'] = $response->get_refNumber();
            $options['customer_id'] = $customer_id;
            $options['business_id'] = $business_id;
            $options['processor'] = $processorName;
        
            $CI->load->model('creditcard_model');
            $CI->creditcard_model->save_card($options);
        }
        
        return $response;
    }
    
    
    /**
     * This will authorize a test credit card and assign the credit card to a customer 
     * 
     * @param array $creditCard
     * @param object $customer
     * @return int creditCardID
     */
    function addCreditCardToCustomer($customer){
        
        $this->CI->load->model('creditcard_model');
        $this->CI->creditcard_model->delete_card($customer->customerID, $customer->business_id);
        
        $this->CI->load->library('creditcardprocessor');
        $processor = get_business_meta($customer->business_id, 'processor');
        $processor = $this->CI->creditcardprocessor->factory($processor, $customer->business_id);
        
        // creditcard
        // NOTE that I use hardcoded customer info here. It's OK
        // All I need is a reference number... the CC info is not necessary
        $creditCard = array();
        $creditCard['firstName'] = 'Matt';
        $creditCard['lastName'] = 'Dunlap';
        $creditCard['address1'] = '611 jean st';
        $creditCard['address2'] = '';
        $creditCard['zip'] = 94610;
        $creditCard['state'] = 'CA';
        $creditCard['expMonth'] = 10;
        $creditCard['expYear'] = 2014;
        $creditCard['cardNumber'] = '4217651111111119';
        $response = $processor->authorizeCard($creditCard,9999999,$customer->customerID, $customer->business_id);
        
        if($response->get_status()=="SUCCESS") {
            $cardType = CreditCard::identify($creditCard['cardNumber']);
            $options['cardNumber'] = $creditCard['cardNumber'];
            $options['payer_id'] = $response->get_refNumber();
            $options['customer_id'] = $customer->customerID;
            $options['business_id'] = $customer->business_id;
            $options['expMo'] = $creditCard['expMonth'];
            $options['expYr'] = $creditCard['expYear'];
            $options['address1'] = $creditCard['address1'];
            $options['address2'] = $creditCard['address2'];
            $options['state'] = $creditCard['state'];
            $options['zip'] = $creditCard['zip'];
            $options['processor'] = $processor->getProcessor();
            $options['cardType'] = $cardType;
            $creditCardID = $this->CI->creditcard_model->save_card($options);
        } else {
            $creditCardID = null;
        }
        return $creditCardID;
        
    }
    
    /**
     * expires a session token for testing the api
     * 
     * @param String $sessionToken
     */
    function expireSessionDate($sessionToken){
        $this->CI->db->query("UPDATE apiSession set expireDate = date_add(NOW(), INTERVAL -1 Day) WHERE token = '$sessionToken'");
    }
    
    
    /**
     * clears active claims in a locker
     * 
     * @param object $locker
     */
    function clearActiveClaimInLocker($locker)
    {
        $CI = &get_instance();
        $sql = "UPDATE claim SET active = 0 WHERE locker_id = {$locker->lockerID}";
        $CI->db->query($sql);
    }
    
    
    /**
     * creates a new claim in the locker provided
     * 
     * @param object $locker
     * @param object $location
     * @param object $customer
     * @return int claimID
     */
    function createClaim($locker, $location, $customer){
    
        // make sure we have no active claims in this locker
        $this->clearActiveClaimInLocker($locker);
    
        // make a single claim for a customer
        $this->CI->load->model("claim_model");
        $options['customer_id'] = $customer->customerID;
        $options['business_id'] = $customer->business_id;
        $options['locker_id'] = $locker->lockerID;
        $options['orderType'] = "a:1:{i:0;i:1;}";
        $options['order_notes'] = "ORDER TYPE: Dry Clean";
        return $this->CI->claim_model->new_claim($options);
    }
    
    /**
     * performs a GET request using curl
     * NOTE: The url should be the full url including the querystring
     * 
     * @param string $url
     * @return mixed
     */
    function curl_get($url){
        
        echo "\nCurl_get: {$url}\n";
        
        $process = curl_init();
        curl_setopt($process, CURLOPT_URL, $url);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 0);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($process);
        if (curl_error($process))
        {
            throw new \Exception(curl_error($process));
        } 
        
        echo $response;
        
        curl_close($process);
        
        return json_decode($response);
        
    }
    
    /**
     * Retrieves a random location from the database
     * @return \App\Libraries\DroplockerObjects\Location
     * @throws \Database_Exception 
     */
    public static function get_random_location($where = array())
    {
        $CI = &get_instance();
        $CI->db->select(App\Libraries\DroplockerObjects\Location::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_location_query = $CI->db->get_where(App\Libraries\DroplockerObjects\Location::$table_name, $where);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_location_result = $get_random_location_query->row();
        $random_location = new \App\Libraries\DroplockerObjects\Location($random_location_result->{\App\Libraries\DroplockerObjects\Location::$primary_key}, true);
        return $random_location;               
    }
    /**
     * Retrieves a random locker from the database
     * Optionally you can request a random locker from a specific location and exclude
     * the inProcess locker
     * 
     * @param int $business_id default(3)
     * @param int $location_id default(empty)
     * @param bool $excludeInProcessLocker default(TRUE)
     * 
     * @return \App\Libraries\DroplockerObjects\Locker
     * @throws \Database_Exception 
     */
    public static function get_random_locker($business_id=3, $location_id = '', $excludeInProcessLocker = TRUE)
    {
 
        $CI = &get_instance();
        $CI->db->select(Locker::$primary_key);
        $CI->db->join("location", "location.locationID = locker.location_id");
        $CI->db->where("business_id", $business_id);
        
        // get a locker in a specific location
        if(!empty($location_id)){
            $CI->db->where('locker.location_id', $location_id);
        }
        
        //exclude the in Process locker
        if(!$excludeInProcessLocker){
            $CI->db->where("lockerName != ", "InProcess");
        }
        
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_locker_query = $CI->db->get(Locker::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_locker_result = $get_random_locker_query->row();
        $random_locker = new Locker($random_locker_result->{Locker::$primary_key}, true);
        return $random_locker;
    }
    
    
    function get_random_locker_loot($business_id = 3){
        $CI = &get_instance();
        return $CI->db->query("SELECT lockerLootID from lockerLoot WHERE description like '%orders referral' ORDER BY RAND() limit 1")->row();
    }
    

    
    /**
     * Retrieves a random locker style from the database
     * @return \App\Libraries\DroplockerObjects\Locker
     * @throws \Database_Exception 
     */
    public static function get_random_lockerStyle ()
    {
        
        $CI = &get_instance();
        $CI->db->select(LockerStyle::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_lockerStyle_query = $CI->db->get(LockerStyle::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_lockerStyle_result = $get_random_lockerStyle_query->row();
        $random_lockerStyle = new Locker($random_lockerStyle_result->{LockerStyle::$primary_key});
        return $random_lockerStyle;
    }    
    /**
     * Retrieves a random instance of a particular class
     * @param string $class_name The class from which to instnatitate the object
     * @param where
     * @param bool $get_relationships If true, retrieves the cardinal relationships defined in the class, otherwise, does not retrieve the cardinal relationships.
     * @return \DropLocker_Object_class_path
     * @throws \Database_Exception 
     */
    public static function get_random_object($class_name, $get_relationships = false, $where = array())
    {
        if (!is_string($class_name))
        {
            throw new \InvalidArgumentException("class_name' must be a string.");
        }
        $CI = &get_instance();
        $DropLocker_Object_class_path = "App\Libraries\DroplockerObjects\\$class_name";
        $CI->db->select($DropLocker_Object_class_path::$primary_key);
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_object_query = $CI->db->get_where($DropLocker_Object_class_path::$table_name, $where);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_object_result = $get_random_object_query->row();
        $random_object = new $DropLocker_Object_class_path($random_object_result->{$DropLocker_Object_class_path::$primary_key}, $get_relationships);
        return $random_object;
    }
    /**
     * Retrieves a random ItemIssue that is associated with an item, order item, and an order.
     * @param int $business_id
     * @param bool $get_relationships If true, retrieves the cardinal relationships defined in the class, otherwise, does not retrieve the cardinal relationships.
     * @return array
     * @throws \Database_Exception 
     */
    public static function get_random_itemIssue($business_id, $get_relationships = false)
    {
        $CI = &get_instance();
        $CI->db->select("ItemIssueID, itemID, orderItemID, orderID");
        $CI->db->join("item", "item.itemID=ItemIssue.item_id");
        $CI->db->join("orderItem", "orderItem.item_id=item.itemID");
        $CI->db->join("order", "order.orderID=orderItem.order_id");
        $CI->db->where("business_id", $business_id);
        $CI->db->order_by("RAND()");
        $CI->db->limit(1);
        $get_random_object_query = $CI->db->get_where(ItemIssue::$table_name);
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->last_query(), $CI->db->_error_number());
        }
        $random_itemIssue_result = $get_random_object_query->row();
        return $random_itemIssue_result;
    }    
    
    
    /**
     * gets a random item
     * @deprecated Do not use, use create_random_item() instead
     * @param int $business_id
     * @return object item
     */
    function get_random_item($business_id = 3){
        $CI = & get_instance();
        $get_random_item_query = "SELECT itemID from item
        INNER JOIN barcode ON item_id = itemID
        WHERE item.business_id = {$business_id} 
        AND product_process_id > 10 ORDER BY rand() LIMIT 1";
        $get_random_item_result = $CI->db->query($get_random_item_query);
        $item = $get_random_item_result->row();
        $random_item = new \App\Libraries\DroplockerObjects\Item($item->itemID, true);
        return $random_item;
    }
    
    /**
     * creates a coupon 
     * 
     * @param String $prefix
     * @param String $code
     * @param float $amount
     * @param String $description
     * @param int $combine (0,1)
     * @param String $amount_type (dollars, percent)
     * @param String $frequency (until used up, one time)
     * @param int $business_id
     * @return \App\Libraries\DroplockerObjects\Coupon
     */
    function create_coupon($prefix, $code, $amount, $description, $combine, $amount_type, $frequency, $business_id=3){
        
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        
        $coupon = new \App\Libraries\DroplockerObjects\Coupon();
        $coupon->prefix = $prefix;
        $coupon->code = $code;
        $coupon->amount = $amount;
        $coupon->description = $description;
        $coupon->business_id = $business_id;
        $coupon->endDate = $endDate;
        $coupon->combine = $combine;
        $coupon->amountType = $amount_type;
        $coupon->frequency = $frequency;
        
        $existing_coupons = \App\Libraries\DroplockerObjects\Coupon::search_aprax(array("prefix" => $coupon->prefix, "code" => $coupon->code));
        array_map(function($existing_coupon) {
            $existing_coupon->delete();
        }, $existing_coupons);
        
        return new \App\Libraries\DroplockerObjects\Coupon($coupon->save());

    }
    
    /**
     * Retrieves a random coupon for a particular business.
     * @param type $business_id
     * @param type $get_relationships
     * @return \App\Libraries\DroplockerObjects\Coupon
     * @throws \Database_Exception 
     */
    public static function get_random_active_coupon($business_id, $get_relationships = false)
    {
        if (!is_numeric($business_id))
        {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $CI = &get_instance();
        $CI->load->model("coupon_model");
        $active_coupons = $CI->coupon_model->get_active_coupons($business_id);
        
        $random_coupon_result = $active_coupons[array_rand($active_coupons,1)];
        
        $random_coupon = new \App\Libraries\DroplockerObjects\Coupon($random_coupon_result->couponID, $get_relationships);
        return $random_coupon;
    }
    /**
     * Searches for data within the Location table. Only returns locations with active lockers that do not have the name "InProcess".
     * @param array $options An array of WHERE parameters, can consist of columsn in the Location table or the Lockers table.
     * @param bool get_relationships If true, retrieves cardinal relationships, otherwise, does not retrieve cardinal relationships.
     * @return array An array consisting of object instances of the class for any resulting rows
     * @throws Exception 
    */
    public static function search_locations_with_lockers($options, $get_relationships = true)
    {
        $CI = &get_instance();
        
        if (!is_array($options))
        {
            throw new Exception("'options' must be an array.");
        }
        
        $CI->db->join("locker", "locker.location_id = location.locationID AND lockerName != 'InProcess'");
        $CI->db->where("lockerStatus_id", 1);
        $CI->db->group_by("locationID");
        $get_valid_locations_query = $CI->db->get_where(Location::$table_name, $options);
        
        if ($CI->db->_error_message())
        {
            throw new \Database_Exception($CI->db->_error_message(), $CI->db->_error_message(), $CI->db->_error_number());
        }
        $rows = $get_valid_locations_query->result();

        $locations = array();     
        foreach ($rows as $row)
        {
            $locations[] = new Location($row->{Location::$primary_key}, $get_relationships);
        }
        return $locations;   
    }
    /**
     * The following function creates a random employeel.
     * @param int $business_id
     * @param string $password
     * @return \App\Libraries\DroplockerObjects\Business_Employee
     */
    public function create_random_employee($business_id, $password = "webdriver") {
        $employee = new Employee();
        $employee->password = $password;
        $word = self::get_random_word(1);
        $employee->email = "webdriver_$word@example.com";
        $employee->save();
        $business_employee = new Business_Employee();
        $business_employee->business_id = $business_id;
        $business_employee->employee_id = $employee->{Employee::$primary_key};
        $business_employee->save();
        return $employee;
    }
}

?>
