<?php
use App\Libraries\DroplockerObjects\Customer;
class CustomerUnitTest extends SeleniumUnitTest
{

    protected $test_customer;
    
    public function setUp() {
        parent::setUp();
        $this->test_customer = new Customer();
        
        $word = UnitTest::get_random_word();
        $this->test_customer->email = "webdriver_$word@testing.com";
        
        $customers = Customer::search_aprax(array("email" => $this->test_customer->email));
        foreach ($customers as $customer)
        {
            $customer->delete();
        }
        
        $this->password = "webdriver";
        $this->test_customer->business_id = $this->business_id;
        $this->test_customer->password = $this->password;
        $this->test_customer->firstName = "webdriver_$word";
        $this->test_customer->lastName = "webdriver_$word";
        $this->test_customer->lastName = "webdriver_$word";
        $this->test_customer->address1 = UnitTest::get_random_word(3);
        $this->test_customer->city = UnitTest::get_random_word(2);
        $this->test_customer->state = "AK";
        $this->test_customer->zip = "99705";
        $this->test_customer->phone = "907-978-9999";
        $this->test_customer->sms = "907-978-9999";
        $this->test_customer->save();
        
        $this->test_customer->password = "webdriver"; //Note, we need to keep the unhashed value of the password in order to use it for loggin in.
        $this->base_url = "http://laundrylocker.droplocker.{$this->domain_extension}";
        $this->driver->load($this->base_url);
    }

    /**
     * Logs in using the customer interface.
     * @param string $email
     * @param string $password 
     */
    public function login($email = NULL, $password = NULL) 
    {
        if (is_null($email) || is_null($password))
        {
            $email = $this->test_customer->email;
            $password = $this->test_customer->password;
        }
        //$this->driver->get_element("id=login_logout")->click();
        $this->driver->get_element("name=username")->send_keys($email);
        $this->driver->get_element("name=password")->send_keys($password);
        $this->driver->get_element("name=password")->submit();

        sleep(5);
        //$this->driver->get_element("css=div.success")->assert_text("Welcome Back"); //This code verifies that the "Welcome Back" flash message is present when an existing customer logs in.
    }
    
    public function tearDown() {
        if ($this->test_customer instanceOf \App\Libraries\DroplockerObjects\Customer)
        {
            $this->test_customer->delete();
        }
        parent::tearDown();
    }
}

?>
