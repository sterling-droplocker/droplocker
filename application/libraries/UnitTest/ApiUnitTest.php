<?php
use \App\Libraries\DroplockerObjects\ApiSession;
use \App\Libraries\DroplockerObjects\Customer;

class ApiUnitTest extends UnitTest
{
    protected $token;
    protected $sessionToken;
    protected $webserviceURL;
    protected $secretKey;
    protected $business_id;
    protected $curl_connection;
    protected $test_customer;
    public function setUp()
    {
        parent::setUp();
        $this->token = 'da49bbfde282bfc603423478c0509bc9';
        $this->sessionToken = md5(UnitTest::get_random_word(1));
        $this->webserviceURL = "http://droplocker.{$this->domain_extension}/api/v1/";
        $this->secretKey = '$2a$07$ur8utmesi0056trinY8oru1cxOPbEA0SMmBwWTct2QFsqLnQcq9JC';
        $this->business_id = 3;
        
        $this->CI->db->delete("apiSession", array("token" => $this->sessionToken));
        
        $this->curl_connection = curl_init();
        $this->CI->load->model("apisession_model");
        
        $this->test_customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        $apiSession = new ApiSession();
        $apiSession->business_id = $this->business_id;
        $apiSession->customer_id = $this->test_customer->{Customer::$primary_key};
        $apiSession->token = $this->sessionToken;
        $apiSession->save();

        curl_setopt($this->curl_connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl_connection, CURLOPT_HTTPGET, true);
    }
    /**
     * creates a signature based on the app secret and data passed from the app. IF the signatures match we have a valid app
     * 
     * @param array $data
     * @param string $secretKey
     * @return string signature
    */
    public function generateSignature($data,$secretKey)
    {
        //sort data array alphabetically by key
        ksort($data);
        //combine keys and values into one long string
        $dataString = strtolower(rawurlencode(urldecode(http_build_query($data, @PHP_QUERY_RFC3986))));


        //generate signature using the SHA256 hashing algorithm
        $signature = hash_hmac("sha256",$dataString,$secretKey);
        
        return $signature;
    }
}

?>
