<?php
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
/**
 * 
 */
abstract class EmployeeUnitTest extends SeleniumUnitTest
{
    protected $business_employee; //This is the test employee created for each test.
    protected $password; //This is the plaintext password for the test employee.
    protected $employee;
    /**
     *
     * @param WebDriver_Driver $driver 
     * @param bool $bizzie_mode If to use the bizzie mode configuration
     */
    public function __construct($driver = null, $bizzie_mode = false) {
        parent::__construct($bizzie_mode);
        if ($driver instanceOf WebDriver_Driver)
        {
            $this->driver = $driver;
        }
    }
    
    
    
    public function setUp() {
        parent::setUp();
        $this->password = "webdriver";
        //The following statements create a test employee.
        $this->employee = new Employee();
        $this->employee->firstName = "webdriver";
        $this->employee->lastName = "webdriver";
        $word = UnitTest::get_random_word();
        $this->employee->email = "webdriver_$word@example.com";
        $this->employee->password = $this->password;
        $this->employee->createdBy = 12;
        $this->employee->manager_id = 12;
        $this->employee->type = "non exempt";
        $this->employee->empStatus = 1;
        $this->employee->profiler_mode = 1;
        $this->employee->superadmin = 1;
        //The following statements find and delete any employee with the existing e-mail address.
        
        $this->CI->load->model("employee_model");
        $this->CI->employee_model->delete_aprax(array("email" => $this->employee->email));
        
        $this->employee->save();
        //The following statements preexisitng business employee ID that associated to the test employee ID.
        
        $this->CI->load->model("business_employee_model");
        $this->CI->business_employee_model->delete_aprax(array("employee_id" => $this->employee->{Employee::$primary_key}));

        $this->business_employee = new Business_Employee();
        $this->business_employee->business_id = $this->business_id;
        $this->business_employee->employee_id = $this->employee->employeeID;
        $this->business_employee->aclRole_id = 41;
        $this->business_employee->default=1;
        $this->business_employee->save();        
    }
    /**
     * 
     * Logs in as an employee to the Droplocker Admin application. 
     * Note, this function expects that the employee already exists in the database.
     * @param email The employee's email; if null, then the default employee credentials are used to login
     * @param password The employee's password; if null, then the default employee credentials are used to login
     */
    public function login($email = null, $password = null) {
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/login");
    
        if (is_null($email) || is_null($password))
        {
            $email = $this->employee->email;
            $password = $this->password;
            $firstName = $this->employee->firstName;
            $lastName = $this->employee->lastName;
        }
        else 
        {
            $employees = Employee::search_aprax(array("email" => $email));
            if (empty($employees))
            {
                throw new \Exception("Could not find employee with the specified email '$email'");
            }
            else
            {
                $employee = $employees[0];
            }
            $firstName = $employee->firstName;
            $lastName = $employee->lastName;
        }
        
        $this->driver->get_element("name=email_or_username")->send_keys($email);
        $this->driver->get_element("name=password")->send_keys($password);
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("id=employee_name")->assert_text(sprintf("%s %s", trim($firstName), trim($lastName)));
        
    }    
    
    public function tearDown() {
        parent::tearDown();
        /**
        if ($this->employee instanceOf Employee)
        {
            $this->employee->delete();
        }
        if ($this->business_employee instanceof Business_Employee)
        {
            $this->business_employee->delete();
        }
         */
    }
}

?>
