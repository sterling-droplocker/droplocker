<?php
use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\AclRoles;
/**
 * The following test suite verifies functionality of operations in Superadmin when the server is in Bizzie mode
 */
class BizzieSuperAdminUnitTest extends EmployeeUnitTest
{
    protected $master_business;
    protected $slave_business;
    protected $slave_employee;
    protected $slave_business_employee;
    protected $database_identifier;
    protected $domain_extension;
    
    public function __construct() {
        parent::__construct(false, true);
        Server_Meta::set_server_mode("bizzie");
        Server_Meta::set_master_business($this->business_id);
    }
    public function setUp()
    {
        parent::setUp();
        Server_Meta::set_server_mode("bizzie");
        Server_Meta::set_master_business($this->business_id);
        
        Server_Meta::set("master_business_id", $this->business_id);
        $this->master_business = new Business($this->business_id);
        $this->employee->superAdmin = 1;
        $this->employee->save();
        
        $this->slave_business = UnitTest::create_random_business();
        $this->CI->business_model->copyAcl($this->slave_business->{Business::$primary_key}, $this->master_business->{Business::$primary_key});
                
        $this->slave_employee = new Employee();
        $this->slave_employee->firstName = "webdriver_slave";
        $this->slave_employee->lastName = "webdriver_slave";
        $word = UnitTest::get_random_word();
        $this->slave_employee->email = "webdriver_slave_$word@example.com";
        $this->slave_employee->password = $this->password;
        $this->slave_employee->createdBy = 12;
        $this->slave_employee->manager_id = 12;
        $this->slave_employee->type = "non-exampt";
        $this->slave_employee->profiler_mode = 1;
        $this->slave_employee->empstatus = 1;
        
        $this->CI->load->model("employee_model");
        $this->CI->employee_model->delete(array("email" => $this->slave_employee->email));
        
        $this->slave_employee->save();
        
        $this->slave_business_employee = new Business_Employee();
        $this->slave_business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->slave_business_employee->employee_id = $this->slave_employee->{Employee::$primary_key};
        $aclroles = AclRoles::search_aprax(array("business_id" => $this->slave_business->{Business::$primary_key}, 'name' => 'admin'));
        if (empty($aclroles))
        {
            throw new \Exception("Could not find the admin acl role for the slave business");
        }
        $aclrole = $aclroles[0];
        $this->slave_business_employee->aclRole_id = $aclrole->aclID;
        
        $this->slave_business_employee->default = 1;
        $this->slave_business_employee->save();
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin");
    }
    /**
     * Logs in as a Bizzie franchisee user
     */
    protected function login_as_slave_admin()
    {
        $this->login($this->slave_employee->email, $this->password);
    }
    /**
     * Logs in as a bizzie superadmin. 
     */
    protected function login_as_bizzie_superadmin()
    {
        $this->login();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
    }
    
    public function tearDown()
    {
        parent::tearDown();
        /**
        if ($this->slave_employee instanceof \App\Libraries\DroplockerObjects\Employee)
        {
            $this->slave_employee->delete();
        }
        if ($this->slave_business_employee instanceof \App\Libraries\DroplockerObjects\Business_Employee)
        {
            $this->slave_business_employee->delete();
        }
         * 
         */
    }
}

?>
