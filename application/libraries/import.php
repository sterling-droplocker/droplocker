<?php

class import extends CI_Model
{
    public function findImportedOrder($order_id, $customer_id)
    {
        $sqlFindOrder = "select *
                            from orders
                            where ticketNum = '".$customer_id.":".$order_id."'
                            and business_id = $this->business_id";
        $q = $this->db->query($sqlFindOrder);
        $findOrder = $q->row();
        return $findOrder;
    }


    public function insertOrderItems($params)
    {
        //see if there is already an order created
        $findOrder = $this->findImportedOrder($params['order'], $params['customer_id']);
        //if there is not already an order create it
        if (empty($findOrder)) {
            $thisOrder = '';
            $options['customer_id'] = $params['customer_id'];
            $options['orderStatusOption_id'] = 1;
            $thisOrder = $this->order_model->new_order($params['location'], $params['locker'], 0, get_employee_id($this->session->userdata('buser_id')), '', $this->business_id, $options);
            if (!is_int($thisOrder)) {
                throw new Exception("Did not create new order successfully." . print_r($thisOrder, true));
            }
            $success .= '<br>SUCCESS: Order '.$thisOrder .' Created';
        } else {
            //if this order is not in picked up status, skip it
            if ($findOrder->orderStatusOption_id != 1) {
                $success .= '<br><strong>ERROR: Order '.$params['order'].' skipped.  Already exists.</strong>';
                //echo $success;

                return $success;
            }
            $thisOrder = $findOrder->orderID;
        }

        if ($next != 1) {
            //add the line to the notes.
            $line = implode($params["line"], ' | ');
            //add the line to the notes of the order
            $update1 = "update orders set notes = CONCAT(notes, '".$line."
'), ticketNum = '$params[customer_id]:$params[order]' where orderID = $thisOrder";

            $update = $this->db->query($update1);

            if ($params['importType'] == 'DropLocker') {
                //get product info to import
                $getItemInfo = "select barcode, item.product_process_id, importMapping.product_process_id as import_pp, importMapping.price
                                from item
                                join barcode on barcode.item_id = itemID
                                join importMapping on item.product_process_id = importMapping.productName
                                where itemID = ".$params['item'];
                $q = $this->db->query($getItemInfo);
                $itemInfo = $q->row();
                if (empty($itemInfo)) {
                    //find out what this product is
                    $getProduct = "select barcode, item.product_process_id, product.name as productName, process.name as processName
                                    from item
                                    join product_process on product_processID = product_process_id
                                    join product on productID = product_process.product_id
                                    join process on processID = product_process.process_id
                                    join barcode on barcode.item_id = itemID
                                    where itemID = ".$params['item'];
                    $q = $this->db->query($getProduct);
                    $product = $q->row();

                    $success .= '<br><strong>ERROR: No mapping for : '.$product->productName.' / '.$product->processName.'  in order <a href="/admin/orders/details/'.$thisOrder.'" target="_blank">'.$thisOrder.'</a>.  Customer Service has been notified and will fix this.</strong>';
                    send_admin_notification('ISSUE ALERT: Wholesale Import Error '.strtotime("now"), 'No mapping for '.$product->productName.' / '.$product->processName.'<br><br>For order: <a href="http://www.droplocker.com/admin/orders/details/'.$thisOrder.'">'.$thisOrder.'</a><br><br>Add mapping <a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/wholesale_mapping/'.$customerId.'">here</a>');
                    $next = 1;
                } else {
                    $barcode = $itemInfo->barcode;
                    $productProcess = $itemInfo->product_process_id;
                    $product = $itemInfo->productName;
                    $process = $itemInfo->processName;
                    $price = $itemInfo->price;
                    $description = '';
                }
            } else {
                //get the product_process for this item
                $barcode = $params["barcode"];
                $productPart = $params["productPart"];
                $productName = str_replace("'", "", trim(strtolower($params["product"])));
                $processName = trim(strtolower($params["method"]));
                $description = $productPart[1];
                $sqlGetProduct = "select product_process_id, importMapping.price, product_id, process_id
                from importMapping
                LEFT join product_process on product_processID = product_process_id
                where productName = '{$productName}'
                and processName = '{$processName}'
                and customer_id = {$_POST[customer]}";

                $q = $this->db->query($sqlGetProduct);
                $getProduct = $q->result();
                if ($getProduct) {
                    $productProcess = $getProduct[0]->product_process_id;
                    $productID = $getProduct[0]->product_id;
                    $processID = $getProduct[0]->process_id;
                    $price = $getProduct[0]->price;
                } else {
                    $error = '<br><strong>ERROR: Could not find product: '.$productName.' in order <a href="/admin/orders/details/'.$thisOrder.'" target="_blank">'.$thisOrder.'.</a> &nbsp;Check your import mapping settings <a href="/admin/orders/wholesale_mapping">Go</a></strong>';
                    return $error;
                }
            }
            if ($next <> 1) {
                //see if the barcode exists already
                $getBarcode = "SELECT * FROM barcode
                join item on barcode.item_id = itemID
                where barcode = '$barcode' and item.business_id='".$this->business_id."'";
                $q = $this->db->query($getBarcode);
                $barcode_existing = $q->result();
                if (!empty($barcode_existing)) {

                    //update the product_process for this barcode
                    $updateItem = "update item set product_process_id = $productProcess
                    where itemID = ".$barcode_existing[0]->item_id." and item.business_id='".$this->business_id."'";
                    $this->db->query($updateItem);
                    $itemId = $barcode_existing[0]->item_id;

                } else {

                    //create the item
                   // $success .= '<br>Creating Item for barcode '.$barcode.' as Product: '.$product.' Process: '.$cleaningProcess;

                    $options['product_process_id'] = $productProcess;
                    $options['business_id'] = $this->business_id;
                    $itemId = $this->item_model->new_item($options);


                    //create the barcode
                    $addBarcode = "INSERT INTO `barcode` (`barcode` , `item_id`, business_id)
                    VALUES ('$barcode', '$itemId', $this->business_id)";
                    $this->db->query($addBarcode);

                    //tie it to a customer
                    $addToCustomer = "INSERT INTO `customer_item` (`customer_id` , `item_id`)
                    VALUES ('".$params['customer_id']."', '$itemId')";
                    $this->db->query($addToCustomer);
                }
                //add the item to the order
                //TODO: hardcoded supplier_id 10
                $this->load->model('orderitem_model');
                $itemInfo = $this->orderitem_model->add_item(
                    $thisOrder,                // $order_id
                    $itemId,                   // $item_id
                    $productProcess,           // $product_process_id
                    1,                         // $qty
                    get_current_employee_id(), // $employee_id
                    10,                        // $supplier_id
                    $price,                    // $price
                    $description               // $notes
                );

            }
        }

       // echo $success;
        return $success;
    }

}
