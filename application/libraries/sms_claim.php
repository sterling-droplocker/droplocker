<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\DropLockerEmail;
use DropLocker\Util\PhoneNumber;

/**
 * Base class for handling SMS claims
 *
 * Possible MAIN actions
 * 1 - placing an order at a location w/locker ID - typical
 * 2 - link SMS to email in customer data
 *
 * Possible 'gotchas'
 * 1 - no default location set
 * 2 - number NOT linked to email address
 */
abstract class Sms_claim
{
    protected $business = null;
    protected $business_id = null;
    protected $customer_id = null;
    protected $sms_keyword = '';
    protected $business_sms_number = '';
    protected $support_email = '';
    protected $support_from_name = '';

    /**
     * Initialize SMS claim library
     *
     * $params can have this keys:
     * - business_id
     * - sms_keyword
     * - business_sms_number
     *
     * @param array $params
     */
    public function __construct($params)
    {
        $CI = get_instance();
        $CI->load->model('customer_model');
        $CI->load->model('claim_model');
        $CI->load->helper('text');

        // this sets the business id for the sms claim
        if (!empty($params['business_id'])) {
            $this->setupBusiness($params['business_id']);
        }

        if (!empty($params['sms_keyword'])) {
            $this->sms_keyword = $params['sms_keyword'];
        }

        if (!empty($params['business_sms_number'])) {
            $this->business_sms_number = $params['business_sms_number'];
        }
    }

    /**
     * Setups the business parameters
     *
     * @param int $business_id
     */
    protected function setupBusiness($business_id)
    {
        $this->business = new Business($business_id);
        $this->business_id = $this->business->businessID;

        // set the support email
        $this->support_email = get_business_meta($this->business_id, 'customerSupport');
        $this->support_from_name = get_business_meta($this->business_id, 'from_email_name');

        //TODO: this was probably used for translation, need to check
        $CI = get_instance();
        $CI->business = $this->business;
        $CI->business_id = $this->business->businessID;
    }

    /**
     * Sends the response to the customer
     *
     * @param string $sms_number
     * @param array $message_data
     * @param string $response
     * @param bool $error
     */
    public function send_process_response($sms_number, $message_data, $response, $error = false)
    {
        if ($error) {
            $this->notifyCustomerSupport($sms_number, $message_data, $response, true);
        }

        // log the request
        $this->logRequest($message_data['original_message'], $sms_number, $response);

        // send the SMS message
        $this->sendMessage($sms_number, $response);

        // log the SMS message to email history
        $this->logHistory($sms_number, $response);

        exit;
    }

    /**
     * Sends a SMS message
     *
     * @param string $sms_number
     * @param string $body
     */
    abstract protected function sendMessage($sms_number, $body);

    /**
     * Process the incoming SMS
     *
     * @param string $sms_number
     * @param string $sms_body
     */
    public function process_message($sms_number, $sms_body)
    {
        //figure out what to do with the message
        $parsed_body = $this->parse_sms_body($sms_body);

        switch ($parsed_body['action']) {
            case 'place_order':
                $this->attempt_sms_order($sms_number, $parsed_body);
                break;

            case 'link_email':
                $this->attempt_link_email($sms_number, $parsed_body);
                break;

            case 'help':
                $this->help($sms_number, $parsed_body);
                break;

            case 'opt_out':
                $this->opt_out($sms_number, $parsed_body);
                break;

            case 'apply_coupon':
                $this->apply_coupon($sms_number, $parsed_body);
                break;

            case 'thank_you':
                $this->thank_you($sms_number, $parsed_body);
                break;

            case 'apply_notes':
                $this->apply_notes($sms_number, $parsed_body);
                break;
            default:
                $this->parsing_error($sms_number, $parsed_body);
                break;
        }
    }

    /**
     * Takes the body of the message and return an array of it's details
     *
     * @param array sms_body
     * @return array
     */
    public function parse_sms_body($sms_body)
    {
        //decode incase for somereason the body is escaped
        $sms_body = urldecode($sms_body);
        $sms_body = trim($sms_body);

        $parsed_output = array(
            'email' => null,
            'locker' => null,
            'action' => null,
            'customer_sms_number' => null,
            'coupon' => null,
            'original_message' => $sms_body
        );

        // if empty, its an error
        if (empty($sms_body)) {
            $parsed_output['action'] = 'error';

            return $parsed_output;
        }

        // is there is a valid email address, this is as an attempt to link an account
        $emails = array();
        if (preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+[_a-zA-Z0-9-]/i", $sms_body, $emails)) {
            $parsed_output['email']  = $emails[0][0];
            $parsed_output['action'] = 'link_email';

            return $parsed_output;
        }

        // begin check if is there any forbidden word
        $forbiddenWords = str_ireplace(', ', ',', get_business_meta($this->business_id, 'sms_forbidden_words'));
        $forbiddenWords = explode(',', $forbiddenWords);
        usort($forbiddenWords, function ($a, $b) {
            return strlen($b) - strlen($a);
        });

        $clean = trim(str_ireplace($forbiddenWords, '', $sms_body));
        if (empty($clean)) {
            $parsed_output['action'] = 'thank_you';

            return $parsed_output;
        }

       //check if is there any note
       if (preg_match("/^notes?[:, ]*(.*)/is", $sms_body, $matches)) {
           $parsed_output['notes']  = trim($matches[1]);
           $parsed_output['action'] = 'apply_notes';
           return $parsed_output;
       }

        // check for help and opt out requests
        $common_words = array('stop', 'end'); // required to be the first word
        $unsuscribe_words = array('quit', 'cancel', 'unsubscribe', 'stopall',); // can be at any place

        $message_parts = explode(' ', strtolower(trim($sms_body)));
        foreach ($message_parts as $i => $word) {

            // check for a help request
            if ($word == 'help') {
                $parsed_output['action'] = 'help';

                return $parsed_output;
            }

            // check for a opt out request
            // common words only if are the first word, unsuscribe words work in any place
            if (($i == 0 && in_array($word, $common_words)) || in_array($word, $unsuscribe_words)) {
                $parsed_output['action'] = 'opt_out';

                return $parsed_output;
            }

            if ($i == 0 && $word == 'code') {
                if (!empty($message_parts[$i + 1])) {
                    $parsed_output['action'] = 'apply_coupon';
                    $parsed_output['coupon'] = $message_parts[$i + 1];

                    return $parsed_output;
                }
            }
        }

        // finally, if we got in here the customer is trying to place an order
        $parsed_output['action'] = 'place_order';

        // try to find the locker number in the message
        // any digits appearing correspond to the order, lockerName
        $parsed_output['locker'] = preg_replace("/[^0-9]/", '', $sms_body);
        $match = array();
        if (preg_match_all('/([0-9]+)/', $sms_body, $match)) {
            $parsed_output['locker'] = $match[1][0];
        }

        return $parsed_output;
    }

    /**
     * Handles the case where a we failed to parse the SMS body
     *
     * @param string $sms_number
     * @param array $parsed_body
     */
    public function parsing_error($sms_number, $parsed_body)
    {
        //TODO there is a typo in the language key
        $response = $this->getTranslation("error_parsin_response", "There was a problem placing your SMS order, place your order at %website_url% or call %phone%. Thanks!");

        $this->send_process_response($sms_number, $parsed_body, $response);

        $error_message = 'Customer sent a SMS that was not readable or not formatted properly';
        $this->sendErrorToAdmin($sms_number, $parsed_body, $error_message);
    }

    /**
     * Sends the help message to the customer
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function help($sms_number, $message_data)
    {
        $response = $this->getTranslation("help_text", "%companyName% Alerts: Help at %website_url% or %phone%. Msg&data rates may apply. Text STOP to cancel.");

        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Handles the customer opt out
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function opt_out($sms_number, $message_data)
    {
        // this is the customer referenced from the SMS number (if it's linked up)
        $customer = $this->checkCustomerNumber($sms_number, $message_data);

        // remove customer SMS number
        $customer->sms = '';
        $customer->save();

        $response = $this->getTranslation("opt_out_success", "You are unsubscribed from %companyName% Alerts. No more messages will be sent. Help at %website_url% or %phone%.");

        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Handles the customer case of thank you in the message
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function thank_you($sms_number, $message_data)
    {
        $response = $this->getTranslation("thank_you_success", "This is an automated message. Please do not respond. Thank you.");

        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Handles when a note is sent by the customer
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function apply_notes($sms_number, $message_data)
    {
        $customer = $this->checkCustomerNumber($sms_number, $message_data);

        $CI = get_instance();
        $CI->load->model('claim_model');

        //get last open claim
        $active_claims = $CI->claim_model->get_claims_by_customer($customer->customerID);

        if (empty($active_claims)) {
            $response = $this->getTranslation("updated_claim_notes_fail", "Sorry, You don't have any open claim to add the note.");
            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        $active_claim = $active_claims[0];
        if (!strlen($message_data['notes'])) {
            $response = $this->getTranslation("empty_claim_notes_fail", "Notes message empty or invalid. Please try again.");
            $this->send_process_response($sms_number, $message_data, $response);
        }

        //update claim
        $message_to_add = convert_from_gmt_aprax('', "Y-m-d H:i:d", $this->business_id) . " - " . $message_data['notes'];
            $notes = strlen($active_claim->notes) ? $active_claim->notes . "\n" . $message_to_add : $message_to_add;
        $CI->claim_model->save(array(
            'claimID' => $active_claim->claimID,
            'notes' => $notes,
        ));
        $response = $this->getTranslation("updated_claim_notes_success", "Notes added to your claim.");
        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Applies coupon to customer account
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function apply_coupon($sms_number, $message_data)
    {
        // this is the customer referenced from the SMS number (if it's linked up)
        $customer = $this->checkCustomerNumber($sms_number, $message_data);

        $code = $message_data['coupon'];

        $CI = get_instance();
        $CI->load->library('coupon');
        $response = $CI->coupon->applyCoupon($code, $customer->customerID, $this->business_id);

        if ($response['status']=='fail') {
            $response = $response['error'];
        } else {
            $response = $response['message'];
        }

        //TODO: the coupon library should not respond with HTML!
        $response = strip_tags($response);

        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Links the SMS number to the customer account
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function attempt_link_email($sms_number, $message_data)
    {
        $customer = $this->getCustomerByEmail($message_data['email']);

        // customer found
        if ($customer) {
            // add this SMS number to this customer's account
            $customer->sms = $sms_number;
            if ($customer->phone == '') {
                $customer->phone = $sms_number;
            }
            $customer->save();

            // respond that they can now place an order, but only show the keyword if one is set
            if ($this->sms_keyword) {
                $response = $this->getTranslation("link_email_success_keyword", "SMS number: %sms_number% has been linked to account: %email%. You may now place your order by texting the word %sms_keyword%.", array(
                    "sms_number" => $sms_number,
                    "email" => $message_data['email'],
                    "sms_keyword" => $this->sms_keyword,
                ));
            } else {
                $response = $this->getTranslation("link_email_success", "SMS number: %sms_number% has been linked to account: %email%. You may now place your order.", array(
                    "sms_number" => $sms_number,
                    "email" => $message_data['email'],
                ));
            }

            $this->send_process_response($sms_number, $message_data, $response);
        }

        // check to see if we are missing the word 'create' in the message
        if (stripos($message_data['original_message'], 'create') === false) {
            $response = $this->getTranslation("link_email_failure", "Sorry, we could not find an account for %email%.  To create an account please text CREATE %email%.", array(
                "email" => $message_data['email'],
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }


        //if we have the the word 'create' in the message

        //create an account with random password
        //TODO: email them their password or reset link?
        $new_temp_pass               = substr(md5(strtotime('now')), 0, 10);
        $new_customer                = new Customer();
        $new_customer->password      = $new_temp_pass;
        $new_customer->sms           = $sms_number;
        $new_customer->phone         = $sms_number;
        $new_customer->firstName     = 'SMS';
        $new_customer->lastName      = 'USER';
        $new_customer->email         = $message_data['email'];
        $new_customer->business_id   = $this->business_id;
        $new_customer->signup_method = "SMS";
        $new_customer->default_business_language_id = $this->business->default_business_language_id;

        $newCustomer_id = $new_customer->save();

        $response = $this->getTranslation("account_created_via_sms", "We have created your account! You can now place your order by texting the locker number.");
        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Place a claim using a SMS
     *
     * $message_data should be generated using Sms_claim::parse_sms_body()
     *
     * @param string $sms_number
     * @param array $message_data
     */
    public function attempt_sms_order($sms_number, $message_data)
    {
        // this is the customer referenced from the SMS number (if it's linked up)
        $customer = $this->checkCustomerNumber($sms_number, $message_data);

        // check if the customer exceeded the business's order limit
        $this->checkClaimLimit($sms_number, $message_data, $customer);

        // try to use the locker number to find the location they're at
        $lockers_matching_name = $this->getLockersByName($message_data['locker']);

        //if there is not an unique locker by that name, check default location
        if (count($lockers_matching_name) != 1) {

            // customer's default location
            $default_location = $this->checkDefaultLocation($sms_number, $message_data, $customer->customerID);

            //is this is a locker/kiosk location?
            if ($default_location->lockerType == "lockers") {
                $this->claimAtLockersLocation($sms_number, $message_data, $customer, $default_location);
            } else {
                $this->claimAtNonLockersLocation($sms_number, $message_data, $customer, $default_location);
            }
        }

        //there is only one locker by that name, use it for the claim
        $new_location = $lockers_matching_name[0];

        // check locker and place the claim
        $this->makeClaimAtLockersLocaiton($sms_number, $message_data, $customer, $new_location, $new_location);
    }

    /**
     * Make checks for the locker and finally place the claim
     *
     * @param string $sms_number
     * @param array $message_data
     * @param Customer $customer
     * @param object $locker
     * @param object $location
     */
    protected function makeClaimAtLockersLocaiton($sms_number, $message_data, $customer, $locker, $location)
    {
        //check to see if the locker has already been claimed
        $this->checkMultipleClaimLocker($sms_number, $message_data, $locker, $location);

        //check if the locker is active
        $this->checkLockerIsActive($sms_number, $message_data, $locker, $location);

        //update the customer default location
        $customer->location_id = $location->locationID;
        $customer->save();

        //place the claim
        $this->makeClaim($sms_number, $message_data, $customer, $locker, $location);
    }

    /**
     * Creates the claim after all validations passed
     *
     * @param string $sms_number
     * @param array $message_data
     * @param Customer $customer
     * @param object $locker
     * @param object $location
     * @return string
     */
    protected function makeClaim($sms_number, $message_data, $customer, $locker, $location)
    {
        $CI = get_instance();
        $CI->load->model('claim_model');

        // from serviceType table, serviceTypeID 11 = SMS
        $orderType = serialize(array(11));

        // this succeeds or throws an exception
        $claim_id = $CI->claim_model->new_claim(array(
            "customer_id" => $customer->customerID,
            "locker_id"   => $locker->lockerID,
            "business_id" => $customer->business_id,
            "orderType"   => $orderType
        ));

        try {
            Email_Actions::send_transaction_emails(array(
            'do_action'   => 'new_claim',
            'customer_id' => $customer->customerID,
            'claim_id'    => $claim_id,
            'business_id' => $customer->business_id
            ));
        } catch (Exception $e) {
            // if sending the new claim e-mail fails, we still want to continue processing the order normally.
            send_exception_report($e, "Customer SMS Number: {$customer->sms}. Customer ID: {$customer->customerID}");
        }

        $response = $this->getTranslation("place_claim_success", "Hi %firstName% %lastName%, your order was successfully placed in locker %lockerName% at location %address%.", array(
            "firstName" => $customer->firstName,
            "lastName" => $customer->lastName,
            "lockerName" => $locker->lockerName,
            "address" => $location->address,
            "title" => $location->companyName,
        ));

        $this->send_process_response($sms_number, $message_data, $response);
    }

    /**
     * Check that a customer does not exceed the business's order limit
     *
     * @param string $sms_number
     * @param array $message_data
     * @param Customer $customer
     */
    protected function checkClaimLimit($sms_number, $message_data, $customer)
    {
        $order_limit = get_business_meta($this->business_id, 'order_limit', 3);

        // no limit
        if (!$order_limit) {
            return true;
        }

        $CI = get_instance();
        $CI->load->model('claim_model');

        $active_claims = count($CI->claim_model->get_claims_by_customer($customer->customerID));
        if ($active_claims >= $order_limit) {
            $response = $this->getTranslation("too_many_claims_error", "As a security measure, you can only place %order_limit% orders. If you really used more than 3 lockers, please email the additional locker numbers to %customer_support%", array(
                "order_limit" => $order_limit,
                "active_claims" => $active_claims,
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        return true;
    }

    /**
     * Check if the locker can take a claim
     *
     * @param string $sms_number
     * @param array $message_data
     * @param object $locker
     * @param object $location
     * @return boolean
     */
    protected function checkMultipleClaimLocker($sms_number, $message_data, $locker, $location)
    {
        $locker = new Locker($locker->lockerID);

        //does this specific locker support multiple claims?
        if ($locker->is_multi_order()) {
            return true;
        }

        $CI = get_instance();

        //check to see if the locker has already been claimed and if it only supports a single claim
        if ($CI->claim_model->does_claim_exist($locker->lockerID, $this->business_id)) {
            $response = $this->getTranslation("locker_already_claimed_error", "The locker %lockerName% at location %shortened_default_location_name% has already been claimed, please choose another locker.", array(
                "lockerName" => $locker->lockerName,
                "shortened_default_location_name" => character_limiter($location->address, 20),
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        return true;
    }

    /**
     * Check if the locker is active
     *
     * @param string $sms_number
     * @param array $message_data
     * @param object $locker
     * @param object $location
     * @return boolean
     */
    protected function checkLockerIsActive($sms_number, $message_data, $locker, $location)
    {
        if ($locker->lockerStatus_id == 1) {
            return true;
        }

        $response = $this->getTranslation("locker_non_active", "The locker %lockerName% at location %short_location_name% is not active, please choose another locker.", array(
            "lockerName" => $locker->lockerName,
            "short_location_name" => $shortened_default_location_name
        ));

        $this->send_process_response($sms_number, $message_data, $response, true);
    }

    /**
     * Processes a claim placed at a lockers location
     *
     * @param string $sms_number
     * @param array $message_data
     * @param Customer $customer
     * @param object $default_location
     */
    protected function claimAtLockersLocation($sms_number, $message_data, $customer, $default_location)
    {
        //if we dont have a locker number for this location
        if (empty($message_data['locker'])) {

            // if this string is longer than 20 characters, send automated response for "This is an automated system..."
            if (strlen($message_data['original_message']) >= 20) {
                $response = $this->getTranslation("unreadable_sms_message", "This is an automated system, if you have a special request please contact customer service.");
            } else {
                $response = $this->getTranslation("default_location_has_lockers_error", "Your default location (%shortened_default_location_name%) has lockers so we need you to text us the locker number. (e.g. '452')", array(
                    "shortened_default_location_name" => character_limiter($default_location->address, 20),
                ));
            }

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        //check that the locker exists in that location
        $locker = $this->checkLockerExistsInLocation($sms_number, $message_data, $message_data['locker'], $default_location);

        // check the locker and place the claim
        $this->makeClaimAtLockersLocaiton($sms_number, $message_data, $customer, $locker, $default_location);
    }

    /**
     * Processes a claim placed at a non-lockers location
     *
     * @param string $sms_number
     * @param array $message_data
     * @param Customer $customer
     * @param object $default_location
     */
    protected function claimAtNonLockersLocation($sms_number, $message_data, $customer, $default_location)
    {
        // make sure we only allow one claim at a non-lockers location
        $this->checkNoOpenClaims($sms_number, $message_data, $customer);

        //Note, we deterine the right locker and location by the last order placed by the customer.
        $last_order_data = $this->checkLastOrder($sms_number, $message_data, $customer->customerID);

        // check to see if the location is still active
        $location = new Location($last_order_data['location']->locationID);

        if (!$location->isActive()) {
            $response = $this->getTranslation("inactive_location_error", "Sorry, but the location [%short_location_name%] is not active. Please call %phone% or go to %website_url% to place your order.", array(
                "short_location_name" => character_limiter($location->address, 20),
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        //check to see if the locationType of the last order matches the customer's default location.
        if ($last_order_data['location']->locationID != $customer->location_id) {
            $response = $this->getTranslation("default_location_last_order_location_mismatch_error", "Your default location (%default_location_shortname%) does not match the location of your last order (%last_location_shortname%). Please place your order at %website_url%", array(
                "default_location_shortname" => character_limiter($default_location->address, 20),
                "last_location_shortname" => character_limiter($location->address, 20),
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        // check to see if locker/unit is still active. 
        // $lockerStatusID == 1 means "In Service" 
        $locker = new Locker($last_order_data['order']->locker_id);
        if ($locker->lockerStatus_id != "1") {
            $response = $this->getTranslation("inactive_locker_error", "Sorry, but the locker [%short_locker_name%] is not active. Please call %phone% or go to %website_url% to place your order.", array(
                "short_locker_name" => character_limiter($locker->lockerName, 20),
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        //have everythign we need, all checks out, make the claim
        $response = $this->makeClaim($sms_number, $message_data, $customer, $last_order_data['locker'], $location);
    }

    /**
     * Checks that the customer does not have open claims
     *
     * @param string $sms_number
     * @param array $message_data
     * @param customer $customer
     * @return bool
     */
    protected function checkNoOpenClaims($sms_number, $message_data, $customer)
    {
        $CI = get_instance();
        $CI->load->model('claim_model');

        $active_claims = $CI->claim_model->get_claims_by_customer($customer->customerID);
        if ($active_claims) {
            $response = $this->getTranslation("already_open_claim", "You already have an open order. If you need to place additional orders go to %website_url% or call %phone%.");

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        return true;
    }

    /**
     * Checks to see if a SMS number is on file or not
     *
     * @param string $sms_number
     * @param array $message_data
     * @return Customer
     */
    protected function checkCustomerNumber($sms_number, $message_data)
    {
        if (empty($sms_number)) {
            throw new Exception("Empty sms number");
        }

        $stripped_number = preg_replace('/[^0-9]/', '', $sms_number);

        // if found using the number, just return
        if ($customer = $this->getCustomerBySms($stripped_number)) {
            $this->customer_id = $customer->customerID;

            return $customer;
        }

        // gets an array of numbers without the prefix
        $numbers = PhoneNumber::stripPrefix($stripped_number);

        // try to find the customer using the prefixless numbers
        foreach ($numbers as $number) {
            // if customer is found, update its sms number and return
            if ($customer = $this->getCustomerBySms($number)) {

                // if the phone number is the same as SMS, update it also
                if ($customer->sms == $customer->phone) {
                    $customer->phone = $sms_number;
                }

                // update the customer to use the number we just found
                $customer->sms = $sms_number;
                $customer->save();

                $this->customer_id = $customer->customerID;

                return $customer;
            }
        }

        //if we dont have a customer, the account is not linked by SMS number
        $response = $this->getTranslation("phone_not_linked_to_account_error", "Sorry, this phone is not linked to your account. To activate it, please text us your email address.");

        $this->send_process_response($sms_number, $message_data, $response, true);
    }

    /**
     * Gets a customer by its email number
     *
     * @param string $number
     * @return Customer
     */
    protected function getCustomerByEmail($email)
    {
        // check if customer already exists in this business
        $customer = Customer::search(array(
            "email" => $email,
            "business_id" => $this->business_id,
        ));

        return $customer;
    }

    /**
     * Gets a customer by its SMS number
     *
     * @param string $number
     * @return Customer
     */
    protected function getCustomerBySms($number)
    {
        $CI = get_instance();

        $customer_query = $CI->db->query(
            "SELECT * FROM customer WHERE (sms LIKE ? OR sms2 LIKE ?) AND business_id = ?",
            array("%$number%", "%$number%", $this->business_id)
       );

        $row = $customer_query->row();

        return $row ? new Customer($row) : false;
    }

    /**
     * Gets the locker and location of the last order placed by the customer
     *
     * If the customer never placed an order, then an error message is sent back to the
     * customer stating that the first need to place an order first.
     *
     * @param string $sms_number
     * @param array $message_data
     * @param inst $customer_id
     * @return array
     */
    protected function checkLastOrder($sms_number, $message_data, $customer_id)
    {
        $last_order = $this->getLastOrderData($customer_id);

        if (empty($last_order['order']) || empty($last_order['location'])) {
            $response = $this->getTranslation("first_time_order_error", "Unfortunately we can't take this order via SMS.  Please place your order at %website_url% or call %phone%.  Thanks!'");

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        return $last_order;
    }

    /**
     * Retrieves the last order locker and location using the customer_id
     *
     * @param unknown $customer_id
     * @return array
     */
    protected function getLastOrderData($customer_id)
    {
        $CI = get_instance();

        $last_order = array();

        //get the details of the last order
        $CI->db->select("locker_id");
        $CI->db->where("customer_id", $customer_id);
        $CI->db->where('bag_id !=', 0);
        $CI->db->limit(1);
        $CI->db->order_by("dateCreated", "DESC");
        $get_last_order_query = $CI->db->get("orders");
        $last_order['order'] = $get_last_order_query->row();

        //no order? then forget it
        if (empty($last_order['order'])) {
            return false;
        }

        $CI->db->select("lockerID, location_id, lockerName");
        $get_last_locker_query = $CI->db->get_where("locker", array("lockerID" => $last_order['order']->locker_id));
        $last_order['locker'] = $get_last_locker_query->row();

        $CI->db->select("locationID, address");
        $get_last_location_query = $CI->db->get_where("location", array("locationID" => $last_order['locker']->location_id));
        $last_order['location'] = $get_last_location_query->row();

        if (!empty($last_order)) {
            return $last_order;
        } else {
            return false;
        }
    }

    /**
     * Checks to see if the customer has a default location set up
     *
     * @param string $sms_number
     * @param array $message_data
     * @param int $customer_id
     * @return object
     */
    protected function checkDefaultLocation($sms_number, $message_data, $customer_id)
    {
        $location = $this->getDefaultLocation($customer_id);

        //no default location set, send them message with details
        if (!$location) {
            $response = $this->getTranslation("no_default_location_error", "You don't have a default location set up yet. Please place an order at %website_url% or call %phone%. Thanks!");

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        return $location;
    }

    /**
     * Gets the cutstomer's default location
     *
     * @param int $customer_id
     * @return object
     */
    protected function getDefaultLocation($customer_id)
    {
        $CI = get_instance();
        return $CI->db->query("SELECT locationType.lockerType, location.*
                                    FROM customer
                                    JOIN location ON (location.locationID = customer.location_id)
                                    JOIN locationType ON (locationType.locationTypeID = location.locationType_id)
                                    WHERE customerID = ?", array($customer_id))->row();
    }

    /**
     * Check that the locker exists in the given location
     *
     * @param string $sms_number
     * @param array $message_data
     * @param string $locker_name
     * @param object $location
     * @return object
     */
    protected function checkLockerExistsInLocation($sms_number, $message_data, $locker_name, $location)
    {
        $locker = $this->getLockerAtLocation($location->locationID, $locker_name);

        if (!$locker) {
            $response = $this->getTranslation("locker_not_found_error", "Sorry, we could not find Locker %locker% at %shortened_default_location_name%.  Please place your order at %website_url% or call %phone%. Thanks!", array(
                "locker" => $message_data['locker'],
                "shortened_default_location_name" => character_limiter($location->address, 20),
            ));

            $this->send_process_response($sms_number, $message_data, $response, true);
        }

        return $locker;
    }

    /**
     * Checks to see if locker exists in location or not
     *
     * @param string $location_id
     * @param string $locker_name
     * @return object
     */
    protected function getLockerAtLocation($location_id, $locker_name)
    {
        $CI = get_instance();

        $get_locker_query = $CI->db->query("SELECT * FROM locker WHERE location_id = ? AND lockerName = ?", array($location_id, $locker_name));
        $locker = $get_locker_query->row();

        return $locker;
    }

    /**
     * Get all the location/joined-lockers matching that name exactly
     *
     * @param string $lockerName
     * @return array
     */
    protected function getLockersByName($lockerName)
    {
        // remove leading zero from the search string, in case the
        // customer sent an invalid string or a partner created lockers under
        // 3 characters
        $lockerName = ltrim($lockerName, '0');

        $CI = get_instance();

        if (empty($lockerName)) {
            return array();
        }

        $CI->db->from('locker');
        $CI->db->where('locker.lockerName', $lockerName);
        $CI->db->where('locker.lockerStatus_id', 1);
        $CI->db->where('location.business_id', $this->business_id);
        $CI->db->join('location', 'location.locationID = locker.location_id');
        $locker_check_query = $CI->db->get();

        return $locker_check_query->result();
    }

    /**
     * Stores the incoming SMS messages and their responses
     *
     * @param string $message
     * @param string $sms_number
     * @param string $response
     */
    protected function logRequest($message, $sms_number, $response)
    {
        $CI = get_instance();

        $CI->db->insert("orderIncoming", array(
            "content"     => $message,
            "fromAddr"    => $sms_number,
            "response"    => $response,
            "customer_id" => $this->customer_id,
       ));
    }

    /**
     * Notifies customer support that something went wrong during the claim
     *
     * @param string $sms_number
     * @param array $message_data
     * @param string $response
     */
    protected function notifyCustomerSupport($sms_number, $message_data, $response, $error = false)
    {
        $body = "<pre>
            Phone Number: $sms_number\n";
        if ($error) {
            $body .= "Type: ERROR\n";
        }
        $body .= "Message: " . print_r($message_data, true) . "
            $response </pre>";

        send_email($this->support_email, $this->support_from_name, $this->support_email, "SMS Claim Error",
            $body);
    }

    /**
     * Notifies the business that something is wrong with their setup or there was a major error
     *
     * @param string $sms_number
     * @param array $message_data
     * @param string $error_message
     */
    protected function sendErrorToAdmin($sms_number, $message_data, $error_message)
    {
        send_email(SYSTEMEMAIL, SYSTEMFROMNAME, SYSTEMEMAIL, "SMS Claim Error",
            "<pre>
            Phone Number: $sms_number
            Message: " . print_r($message_data, true) . " {$error_message} </pre>");
    }

    /**
     * Gets the provider settings
     *
     * @param int $business_id
     * @return object
     */
    protected function getProviderSettings($business_id)
    {
        $CI = get_instance();

        $number_query = $CI->db->get_where('smsSettings', array('business_id' => $business_id));
        $business_sms_settings = $number_query->row();

        return $business_sms_settings;
    }

    /**
     * Shortcut for translating strings
     *
     * @param string $languageView_name
     * @param string $fallback_text
     * @param array $substitutions
     * @return string
     */
    protected function getTranslation($languageKey_name, $fallback_text, $substitutions = array())
    {
        $substitutions = array_merge(array(
                'customer_support' => $this->support_email,
                'companyName' => $this->business->companyName,
                'website_url' => $this->business->website_url,
                'phone' => $this->business->phone
            ), $substitutions);

        return get_translation($languageKey_name, "SMS Claim", $substitutions, $fallback_text, $this->customer_id);
    }

    /**
     * Stores the incoming SMS response into customer email history
     *
     * @param $sms_number
     * @param $response
     */
    public function logHistory($sms_number, $response)
    {
        $email = new DropLockerEmail();
        $email->body = $response;
        $email->to = $sms_number;
        $email->cc = serialize(array());
        $email->fromEmail = 'twilio';
        $email->fromName = $this->support_from_name;
        $email->dateCreated = date('Y-m-d H:i:s');
        $email->sendDateTime = date('Y-m-d H:i:s');
        $email->business_id = $this->business_id;
        $email->status = 'success';
        $email->emailType = ENVIRONMENT;
        $email->customer_id = $this->customer_id;

        $email->save();
    }
}
