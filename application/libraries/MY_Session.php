<?php
/**
 * Description of MY_Session
 *
 * @author ariklevy
 */
class MY_Session extends CI_Session
{
    var $cookie_httponly = FALSE;

    public function __construct($params = array())
    {
        $CI =& get_instance();
        $this->cookie_httponly = (isset($params['cookie_httponly'])) ? $params['cookie_httponly'] : $CI->config->item('cookie_httponly');
        
        parent::__construct($params);

        $buser_id = $this->userdata('buser_id');

        if (is_numeric($buser_id)) {
            try {
                $business_employee = new \App\Libraries\DroplockerObjects\Business_Employee($buser_id, true);
            } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                return;
            }
            $employee = $business_employee->relationships['Employee'][0];

            if ($employee->session_expiration) {
                $params['sess_expiration'] = $employee->session_expiration;
            }

            if ($employee->one_session) {
                $params['sess_match_useragent'] = TRUE;
                $params['sess_match_ip'] = TRUE;
            }
            parent::__construct($params);
            parent::sess_write();
        }

    }
    /**
     * Update an existing session
     * This is a workaround for Codeigniter Issue#154 https://github.com/EllisLab/CodeIgniter/issues/154
     * @access    public
     * @return    void
    */
    public function sess_update()
    {
       // skip the session update if this is an AJAX call! This is a bug in CI; see:
       // https://github.com/EllisLab/CodeIgniter/issues/154
       // http://codeigniter.com/forums/viewthread/102456/P15
       if ( !($this->CI->input->is_ajax_request()) ) {
           parent::sess_update();
       }
        // skip the session update if this is an AJAX call! This is a bug in CI; see:
        // https://github.com/EllisLab/CodeIgniter/issues/154
        // http://codeigniter.com/forums/viewthread/102456/P15
        if ( !(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || ( stripos($_SERVER['REQUEST_URI'], 'live/request') != 0 ) ) {
                parent::sess_update();
        }
    }

	// --------------------------------------------------------------------

	/**
	 * Write the session cookie
         * Note: overrided to add support for cookie_httponly
	 *
	 * @access	public
	 * @return	void
	 */
	function _set_cookie($cookie_data = NULL)
	{
		if (is_null($cookie_data))
		{
			$cookie_data = $this->userdata;
		}

		// Serialize the userdata for the cookie
		$cookie_data = $this->_serialize($cookie_data);

		if ($this->sess_encrypt_cookie == TRUE)
		{
			$cookie_data = $this->CI->encrypt->encode($cookie_data);
		}
		else
		{
			// if encryption is not used, we provide an md5 hash to prevent userside tampering
			$cookie_data = $cookie_data.md5($cookie_data.$this->encryption_key);
		}

		$expire = ($this->sess_expire_on_close === TRUE) ? 0 : $this->sess_expiration + time();

		// Set the cookie
		setcookie(
					$this->sess_cookie_name,
					$cookie_data,
					$expire,
					$this->cookie_path,
					$this->cookie_domain,
					$this->cookie_secure,
                                        $this->cookie_httponly
				);
	}

}
