<?php
use App\Libraries\DroplockerObjects\Business;
/* library for Fabricare exports / imports (from responses) */

class FabricareLibrary
{
    protected $_ci;
    protected $start_date = '0000-00-00 00:00:00';  //if you send these dates through in the params, make sure they're formatted like this
    protected $end_date   = '9999-99-99 99:99:99';
    protected $business_id;
    protected $bag_number = 0;
    private $file_path = null;
    public $save_on_server = false;

    public function __construct( $params = array() )
    {
        $this->_ci = get_instance();

        $this->business_id = $params['business_id'];
        if ( empty( $this->business_id ) ) {
            throw new Exception('Missing Business ID');
        } else {
            $this->business = new Business( $params['business_id'] );
            $this->file_path = "/home/droplocker/integration/" . $this->business_id . "/Import.XML";
        }

        if ( !empty( $params['file_path'] ) ) {
            $this->file_path = $params['file_path'];
        }

        if ( !empty( $params['start_date'] ) ) {
            $this->start_date = $params['start_date'];
        }
        if ( !empty( $params['end_date'] ) ) {
            $this->end_date = $params['end_date'];
        }
        if ( !empty( $params['bag_number'] ) ) {
            $this->bag_number = $params['bag_number'];
        }

        //check to see if our start/end dates are set by the POST
        $start_post = $this->_ci->input->post('start_date');
        $end_post   = $this->_ci->input->post('end_date');
        if ( !empty( $start_post ) ) {
            $this->start_date = date('Y-m-d H:i:s', strtotime( $start_post ) );
        }
        if ( !empty( $end_post ) ) {
            $this->end_date   = date('Y-m-d H:i:s', strtotime( $end_post ) );
        }
    }

    //orders - array of objects
    //as file if you wanna pop the header out, otherwise just echos
    public function export($as_file = false, $automat_ref = true, $save_on_server = false)
    {
        if (!empty($this->bag_number)) {
            $orders = $this->pullBag();
        } else {
            $orders = $this->pullOrders( $automat_ref );    //get the orders
        }

        $import_xml = $this->getXML( $orders, $as_file );             //get the XML

        $file_path = DEV ? "/tmp/" : "/home/droplocker/";
        $file_path .= 'fabricare/' . $this->business_id . '/';

        if (!is_dir($file_path)) {
            mkdir($file_path, 0777, true);
        }

        $file_path .= gmdate('Y-m-d_H.i.s_');

        file_put_contents($file_path . 'Import.xml', $import_xml);

        //if as file is flipped to true then we'll let them save it
        if ( !empty( $as_file ) ) {
            header("Content-type: text/xml; charset=utf-8");
            $filename = 'Fabricare_export_' . date('YmdHis');
            header("Content-Disposition: attachment; filename='$filename'");
            echo $import_xml;
        } elseif ( !empty( $save_on_server ) ) {

            if ( !empty( $this->file_path ) ) {
                $file_handler = fopen($this->file_path, 'w') or die("can't open file. location: " . $this->file_path );
                fwrite($file_handler, $import_xml);
                fclose($file_handler);
            }
        } else {
            header("Content-type: text/xml; charset=utf-8");
            echo $import_xml;
        }

        exit;
    }

    private function is_rejected($response)
    {
        $response = strtolower( $response );
        if ( strpos( $response, 'rejected' ) !== false ) {
            $reason_array = explode( ',', $response );

            return @$reason_array[1];
        }
        if ( strpos( 'accepted', $response ) !== false ) {
            return false;
        }

        return false;
    }

    public function parseResponse($response_xml_string)
    {
        $ResponseStream = simplexml_load_string( $response_xml_string );

        foreach ($ResponseStream as $NewOrder) {
            $customer_rejected = $this->is_rejected( (string) $NewOrder->CustomerResponse );
            $invoice_rejected  = $this->is_rejected( (string) $NewOrder->InvoiceResponse );
            $order_id = $NewOrder->InvoiceID;

            $items = $NewOrder->Item;
            if (!$customer_rejected) {
                echo 'Accepted Customer:' . $NewOrder->OrderCustomerID . "\n\n";
            } else {
                echo 'Rejected Customer' . $NewOrder->OrderCustomerID . " because:" . $customer_rejected . "\n\n";
            }
            if (!$invoice_rejected) {
                echo 'Accepted Invoice' . $NewOrder->InvoiceID . "\n\n";
            } else {
                echo 'Rejected Invoice ' . $NewOrder->InvoiceID . " because:" . $invoice_rejected . "\n\n";
            }

            $bulk_automat_data = array();
            $reject_flag  = false;

            foreach ($items as $item) {
                $automat_data = array();
                $garmentSKU   = (string) $item->GarmentSKU;
                $itemResponse = (string) $item->ItemResponse;
                $heatsealID   = (string) $item->HeatsealID;

                $item_rejected = $this->is_rejected( $itemResponse );
                if (!$item_rejected) {

                    echo '  Accepted Item ' . $garmentSKU . "\n\n";

                    $automat_data['order_id'] = $order_id;
                    $automat_data['garcode'] = $heatsealID;
                    $automat_data['gardesc'] = "Imported to Fabricare Manager";
                    $automat_data['fileDate'] = convert_to_gmt_aprax( date("H:i:s", strtotime('now') ), $this->business->timezone);

                    $bulk_automat_data[] = $automat_data;
                } else {

                    $reject_flag = true;
                    echo '  Rejected Item with GarmentSKU: ' . $item->GarmentSKU;
                }
            }

            //if the reject_flag was still false, then insert, otherwise forget it
            if (!$reject_flag) {
                foreach ($bulk_automat_data as $bulk_insert) {
                    $this->_ci->db->insert('automat', $bulk_insert );
                }

            } else {
                //SEND EMAIL HERE WITH DETAILS
                $operations_email = get_business_meta( $this->business_id, 'operations' );
                $email_text = $NewOrder->asXML();
                send_email( SYSTEMEMAIL, SYSTEMFROMNAME, $operations_email, 'Fabricare response rejected for order: ' . $order_id, $email_text );
            }

        }

        return true; //fin
    }

    private function pullBag()
    {
        $this->_ci->db->select( "customer.firstName, customer.lastName, customer.address1, customer.address2, customer.city, customer.state, customer.zip, customer.phone, customer.email, orders.*" );
        $this->_ci->db->from("orders");
        $this->_ci->db->join('customer', 'customer.customerID = orders.customer_id');
        $this->_ci->db->where("orders.business_id", $this->business_id);
        $orders_query = $this->_ci->db->get();

        $orders = $orders_query->result();
    }

    private function pullOrders($automat_ref = true)
    {
        $this->_ci->db->select( "customer.firstName, customer.lastName, customer.address1, customer.address2, customer.city, customer.state, customer.zip, customer.phone, customer.email, orders.*" );
        $this->_ci->db->from("orders");
        $this->_ci->db->join('customer', 'customer.customerID = orders.customer_id');

        $suppliers = get_business_meta($this->business_id, 'fabricare_suppliers');
        if (!empty($suppliers)) {
            $suppliers = unserialize($suppliers);
        }

        if ($suppliers) {
            $this->_ci->db->join('orderItem', 'order_id = orderID');
            $this->_ci->db->where_in('supplier_id', $suppliers);
            $this->_ci->db->group_by('orderID');
        }

        //if we have dates use them
        if ( !empty( $this->end_date ) ) {
            $this->_ci->db->where("orders.dateCreated <= ", $this->end_date );
        }
        if ( !empty( $this->start_date ) ) {
            $this->_ci->db->where("orders.dateCreated >= ", $this->start_date );
        }

        $this->_ci->db->where_in( "orderStatusOption_id", array(3,7) );
        $this->_ci->db->where("orders.business_id", $this->business_id);

        $orders_query = $this->_ci->db->get();
        $orders = $orders_query->result();

        $all_order_data = array();
        if ( !empty( $orders ) ) {

            foreach ($orders as $order) {

                //first see if its already in the automat
                $automat_check_query = $this->_ci->db->select('automatID')->from('automat')->where('order_id',$order->orderID)->where('slot !=','')->get();
                $automat_check = $automat_check_query->row();

                //if its in the automat, skip it
                if ( !empty( $automat_check ) ) {
                    continue;
                }

                $all_order_data[ $order->orderID ]['order'] = $order;
                $this->_ci->db->select( 'item.*, orderItem.*, picture.*, barcode.barcode' );
                $this->_ci->db->join( 'item', 'item.itemID = orderItem.item_id' );
                $this->_ci->db->join( 'picture', 'item.itemID = picture.item_id', 'LEFT' );
                $this->_ci->db->join( 'barcode', 'barcode.item_id = item.itemID' );

                $order_items_query = $this->_ci->db->get_where('orderItem', array( 'order_id' => $order->orderID ) );
                $order_items = $order_items_query->result();

                if ( !empty( $order_items ) ) {

                    foreach ($order_items as $order_item) {

                        $this->_ci->db->select('product_process.*, product.* ');
                        $this->_ci->db->join( 'product', 'product.productID = product_process.product_id' );
                        $prod_details_query = $this->_ci->db->get_where( 'product_process', array('product_processID' => $order_item->product_process_id ) );
                        $prod_details = $prod_details_query->row();

                        $all_order_data[ $order->orderID ]['orderItems'][ $order_item->orderItemID ] = $order_item;

                        if ( !empty( $prod_details ) ) {
                            $all_order_data[ $order->orderID ]['orderItemsDetail'][ $order_item->orderItemID ] = $prod_details;
                        }


                        $specials = $this->_ci->db->query("SELECT * FROM item_specification
                                                            JOIN specification ON (specification_id = specificationID)
                                                            WHERE item_id = ?;",
                        array($order_item->itemID))->result();


                        $all_order_data[ $order->orderID ]['orderItemSpecials'][ $order_item->orderItemID ] = $specials;
                    }
                }
            }
        }

        return $all_order_data;
    }

    private function getXML( $all_order_data = array(), $as_file = false )
    {
        $xml = simplexml_load_string('<?xml version="1.0" encoding="utf-8"?><TransactionStream></TransactionStream>');

        $ready_by = get_business_meta($this->business_id, 'fabricare_ready_by');
        $service_days = get_business_meta($this->business_id, 'fabricare_service_days');

        if (!empty($service_days)) {
            $service_days = unserialize($service_days);
        }

        //go through each order
        foreach ($all_order_data as $orderID => $order_data) {

            //create a bizzieorder child
            if ( !empty( $order_data['order']) ) {
                $order_details = $order_data['order'];
                $newOrder = $xml->addChild('NewOrder');

                $newOrder->addChild( 'OrderCustomerID', $order_details->customer_id );
                $newOrder->addChild( 'LastName', htmlspecialchars( $order_details->lastName ) );
                $newOrder->addChild( 'FirstName', htmlspecialchars( $order_details->firstName ) );
                $newOrder->addChild( 'Address1', htmlspecialchars( $order_details->address1 ) );
                $newOrder->addChild( 'Address2', htmlspecialchars( $order_details->address2 ) );
                $newOrder->addChild( 'City', htmlspecialchars( $order_details->city ) );
                $newOrder->addChild( 'State', htmlspecialchars( $order_details->state ) );
                $newOrder->addChild( 'PostalCode', htmlspecialchars( $order_details->zip ) );

                $comment       = $newOrder->addChild( 'Comment' );
                $reminder      = $newOrder->addChild( 'Reminder', htmlspecialchars( $order_details->notes ) );
                $is_tax_exempt = $newOrder->addChild( 'IsTaxExempt', false);
                $package       = $newOrder->addChild( 'Package' );
                $starch        = $newOrder->addChild( 'Starch' );//customer.starch

                $newOrder->addChild( 'Phone', htmlspecialchars( $order_details->phone ) );
                $newOrder->addChild( 'Email', htmlspecialchars( $order_details->email ) );


                $due_date_time = strtotime($order_details->dueDate);
                $due_date_time -= 86400 * $ready_by; // remove ready_by days

                // if service days is set
                if (!empty($service_days)) {
                    $dow = date('w', $due_date_time);
                    $dow = $dow ? $dow : 7;

                    // if no service in that day, keep going back
                    while(!in_array($dow, $service_days)) {
                        $due_date_time -= 86400;
                        $dow = date('w', $due_date_time);
                        $dow = $dow ? $dow : 7;
                    }
                }


                $invoice_id   = $newOrder->addChild( 'InvoiceID', $order_details->orderID );
                $invoice_date = $newOrder->addChild( 'InvoiceDate', date('c', strtotime( $order_details->dateCreated ) ) );
                $due_date     = $newOrder->addChild( 'DueDate', date('c', $due_date_time ) );
                $invoice_memo = $newOrder->addChild( 'InvoiceMemo', htmlspecialchars( $order_details->postAssembly ) );

            }

            if ( !empty( $order_data['orderItems'] ) ) {
                $order_items = $order_data['orderItems'];

                foreach ($order_items as $order_item) {
                    //item fields
                    $item = $newOrder->addChild( 'Item' );
                    $item->addChild( 'GarmentSKU', htmlspecialchars($order_item->product_process_id) ); //PRODUCT PROCESS ID

                    //check for descriptors
                    $descriptors = array();
                    if ( !empty( $order_item->color ) ) {
                        $descriptors[] = $order_item->color;
                    }

                    if ( !empty( $order_item->size ) ) {
                        $descriptors[] = $order_item->size;
                    }
                    if ( !empty( $order_item->manufacturer ) ) {
                        $descriptors[] = $order_item->manufacturer;
                    }
                    $descriptors_string = '';
                    $descriptors_string = implode( ',', $descriptors );
                    if ( !empty( $descriptors_string ) && $descriptors_string!='null' ) {
                        $item->addChild( 'Descriptors', str_replace( "&", ' and ', $descriptors_string) );
                    } else {
                        $item->addChild( 'Descriptors');
                    }


                    $item->addChild( 'PhotoURL', $order_item->file ? htmlspecialchars('https://s3.amazonaws.com/LaundryLocker/' . $order_item->file) : '' );
                    $item->addChild( 'HeatsealID', htmlspecialchars($order_item->barcode) ); //barcode
                    $item->addChild( 'FlagTag', htmlspecialchars($order_data[ 'orderItemsDetail' ][ $order_item->orderItemID ]->notes ) ); // item.note
                    $item->addChild( 'Quantity', htmlspecialchars($order_item->qty) );
                    $item->addChild( 'Pieces', htmlspecialchars($order_item->qty) );
                    $item->addChild( 'Amount', '0' ); //orderitem.price

                    //item specifications
                    $flags = array();
                    foreach ($all_order_data[$orderID]['orderItemSpecials'][$order_item->orderItemID] as $special) {
                        $flags[] = $special->description;
                    }
                    $flags = implode(', ', $flags);

                    $item->addChild( 'FlagTag', htmlspecialchars($flags) );

                }
            }

        }

        if (DEV) {
            $dom = dom_import_simplexml($xml)->ownerDocument;
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            return $dom->saveXML();
        }

        return $xml->asXML();   //return the XML string data
    }

}
