<?php

require_once APPPATH . 'third_party/tcpdf_6_0_077/tcpdf.php';


class TCPDFcustom extends TCPDF
{
    public $is_html_header_address = false;

    public $font_set = array(
        'ascii'     => array(
            'helvetica'             => 'helvetica',
            'PDF_FONT_NAME_MAIN'    => PDF_FONT_NAME_MAIN,
            'PDF_FONT_NAME_DATA'    => PDF_FONT_NAME_DATA,
            'PDF_FONT_MONOSPACED'   => PDF_FONT_MONOSPACED,
            'dejavusans'            => 'dejavusans'
        ),
        'non-ascii' => array(
            'helvetica'             => 'cid0jp',
            'PDF_FONT_NAME_MAIN'    => 'cid0jp',
            'PDF_FONT_NAME_DATA'    => 'cid0jp',
            'PDF_FONT_MONOSPACED'   => 'cid0jp',
            'dejavusans'            => 'cid0jp'
        )
    );

    public function __construct($params = null)
    {
        if (!empty($params)) {
            extract($params);
            parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        } else {
            parent::__construct();
        }
    }

    public function setIsHtmlHeaderAddress($is_html_header_address = false)
    {
        $this->setIsHtmlHeaderAddress = $is_html_header_address;
    }

    public function getIsHtmlHeaderAddress()
    {
        return $this->setIsHtmlHeaderAddress;
    }

    /**
     * The following function adjusts the header function to right align the header title and text.
     */
    public function Header()
    {
        $ormargins = $this->getOriginalMargins();
        $headerfont = $this->getHeaderFont();
        $headerdata = $this->getHeaderData();
        if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
            $this->Image(K_PATH_IMAGES.$headerdata['logo'], $this->GetX(), $this->getHeaderMargin(), $headerdata['logo_width']);
            $imgy = $this->getImageRBY();
        } else {
            $imgy = $this->GetY();
        }
        $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
        // set starting margin for text data cell
        if ($this->getRTL()) {
            $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
        } else {
            $header_x = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
        }

        $this->SetTextColor(0, 0, 0);
        // header title
        $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
        $this->SetX($header_x);
        $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, 'R', 0, '', 0);
        // header string
        $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
        $this->SetX($header_x);

        //MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

        $this->MultiCell(0, $cell_height, $headerdata['string'], 0, 'R', 0, 1, '', '', true, 0, $this->getIsHtmlHeaderAddress());
        
        // print an ending header line
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
        if ($this->getRTL()) {
                $this->SetX($ormargins['right']);
        } else {
                $this->SetX($ormargins['left']);
        }
        $this->Cell(0, 0, '', 'T', 0, 'C');

    }
}
