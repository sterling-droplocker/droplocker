<?php

require_once APPPATH . 'libraries/sms_claim.php';

/**
 *  Manages SMS claims for Twilio
 */
class Twilio_claim extends Sms_claim
{
	private $use_api = false;

    public function __construct($params)
    {
 		parent::__construct($params);
 		if (!empty($params["use_api"])) {
 			if ($params["use_api"]) {
 				$this->use_api = true;
 			}
 		}
	}

    protected function sendMessage($sms_number, $body)
    {
    	if (!$this->use_api) {
	        //this is TwiML - how twilio handles automated SMS responses when our endpoint is set
	        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
	        $output .= '<Response>';
	        $output .= '<Message>' . $body . '</Message>';
	        $output .= '</Response>';

	        echo $output;
    	} else {
        	$sms = \DropLocker\Service\SMS\Factory::getService("twilio", $this->business_id);
        	$sms->sendMessage($sms_number, $body);   		
    	}
    }

}
