<?php

class Units extends CI_Model
{
    const TYPE_DISTANCE = 'distance';
    
    const DISTANCE_FEET = 'feet';
    const DISTANCE_KM = 'km';
    const DISTANCE_MILES = 'miles';

    protected $units = array(
        self::TYPE_DISTANCE => array (
            self::DISTANCE_FEET => array (
                'label' => 'feet',
                'conversions' => array (
                    
                )
            ),
            self::DISTANCE_KM => array (
                'label' => 'km',
                'conversions' => array (
                    self::DISTANCE_FEET => 3280.84,
                )
            ),
            self::DISTANCE_MILES => array (
                'label' => 'miles',
                'conversions' => array (
                    self::DISTANCE_FEET => 5280,
                    self::DISTANCE_KM => 1.609,
                )
            ),
        ) 
    );
    
    public function __construct()
    {
        parent::__construct();
    }

    public function get_distance_units_for_dropdown()
    {
        return $this->get_units_for_dropdown(self::TYPE_DISTANCE);
    }

    public function get_units_for_dropdown($type = self::TYPE_DISTANCE)
    {
        $list = array();
        
        foreach ($this->units[$type] as $key => $data) {
            $list[$key] = $this->get_unit_label($key, $type);
        }
        
        return $list;
    }
    
    public function get_unit_label($unit, $type = self::TYPE_DISTANCE)
    {
        $default = $this->units[$type][$unit]['label'];
        return get_translation("unit_{$type}_{$unit}", "units", array(), $default);
    }
    
    public function get_distance_unit_label($unit)
    {
        return $this->get_unit_label($unit, self::TYPE_DISTANCE);
    }
    
    public function convert_distance($val, $from, $to) {
        return $this->convert($val, $from, $to, self::TYPE_DISTANCE);
    }
    
    /*
     * todo: if necesary someday, make system more flexible to handle linear, non linear and custom conversions
     * 
     **/
    public function convert($val, $from, $to, $type = self::TYPE_DISTANCE) {
        $factor = 1;
        
        $direct = $this->units[$type][$from]['conversions'][$to];
        $inverse = $this->units[$type][$to]['conversions'][$from];
        if ($direct) {
            $factor = $direct;
        } else if ($inverse) {
            $factor = 1 / $inverse;
        }
        
        return $val * $factor;
    }

}
