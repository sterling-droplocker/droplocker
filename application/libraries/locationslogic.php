<?php
class Locationslogic
{
    /**
     *
     * @var CI_Controller
     */
    protected $CI;

    public function __construct( $params = array() )
    {
        $this->CI = get_instance();
        $this->CI->load->model('location_model');

    }

// this performs a search by location, distance (via lat/lon)
// this is our radius search - use the google query
//  [example of the haversine formula] SELECT id, ( 3959 * acos( cos( radians(37) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-122) ) + sin( radians(37) ) * sin( radians( lat ) ) ) ) AS distance FROM markers HAVING distance < 25 ORDER BY distance LIMIT 0 , 20;
//  found this: https://developers.google.com/maps/articles/phpsqlsearch_v3
//
    public function radius_search($lat = '', $lon = '', $distance_in_miles = 1, $business_id = 0, $public = '', $private = '', $limit = 500)
    {
        //we can always optimize this further if need be
        //ie, draw a box around, do a subselect, this way i've cut out most of the results already
        //or we can do a sub select by state, etc. etc.

        //check to make sure geocoding isn't using commas
        $lat = str_replace(',','.',$lat);
        $lon = str_replace(',','.',$lon);
        $distance_in_miles = str_replace(',','.',$distance_in_miles);

        // location types to exclude
        $exclude = implode(",", $this->getExcludedLocationTypes($business_id));

        $radius_query  = "SELECT locationID, ( 3959 * acos( cos( radians(" . $lat . ") ) * cos( radians( lat ) ) * cos( radians( lon ) - radians(" . $lon . ") ) + sin( radians(" . $lat . ") ) * sin( radians( lat ) ) ) ) AS distance_in_miles ";
        $radius_query .= " FROM location WHERE ( ";
        $radius_query .= " business_id='" . $business_id . "' AND status='active' AND serviceType != 'not in service' AND locationType_id NOT IN ($exclude)";

        if ( !empty( $public ) ) {
            $radius_query .= " AND public='" . $public . "'";
        }

        $radius_query .= ") ";
        $radius_query .= " HAVING ( distance_in_miles <= " . $distance_in_miles . " OR distance_in_miles IS NULL ) ORDER BY distance_in_miles LIMIT " . $limit;
        $locations_in_radius = $this->CI->db->query( $radius_query )->result();
        
        $this->CI->load->library('units');
        $location_distance_display_unit = get_current_business_meta('location_distance_display_unit', Units::DISTANCE_MILES);
        $location_distance_display_unit_label = $this->CI->units->get_distance_unit_label($location_distance_display_unit);
        
        //check if the locations has available lockers
        $this->CI->load->model('location_model');
        foreach ($locations_in_radius as $key=>$result) {
            $locations_in_radius[$key]->distance = $this->CI->units->convert_distance($locations_in_radius[$key]->distance_in_miles, Units::DISTANCE_MILES, $location_distance_display_unit);
            $locations_in_radius[$key]->distance_unit = $location_distance_display_unit;
            $locations_in_radius[$key]->distance_unit_label = $location_distance_display_unit_label;

            $availableLockers = $this->CI->location_model->getActiveLockerCount($result->locationID);
             if ($availableLockers == 0) {
                unset($locations_in_radius[$key]);
            }
        }

        return $locations_in_radius;
    }

    //search for locations by keyword/address??
    public function location_search($search_term = '', $business_id = 0)
    {
        //try to parse address and geocode it, then run radius search?

        $output = array( 'status' => 0, 'result' => array() );

        //GOOOOOOGLE
        $search_request_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode( $search_term ) . "&sensor=false&key=" . get_default_gmaps_api_key();
        $geo = file_get_contents( $search_request_url );
        $geo_results = json_decode($geo);

        if ($geo_results->status == "OK") {
            $first_result = $geo_results->results[0];

            $result_lat = $first_result->geometry->location->lat;
            $result_lon = $first_result->geometry->location->lng;

            $result_coords = array( 'lat' => $result_lat, 'lon' => $result_lon );
            $output['result'] = $result_coords;
            $output['status'] = 1;
        }

        return $output;
    }

//
//        //this returns the businesses
    public function business_neighborhoods($business_id = 0)
    {
        $content_neighborhood_query = $this->CI->db->query('SELECT DISTINCT neighborhood, location_id FROM location_neighborhood WHERE business_id=' . $business_id );
        $businessNeighborhoods = $content_neighborhood_query->result();
        $client_formatted_neighborhoods = array();
        foreach ($businessNeighborhoods as $businessNeigh) {
            $client_formatted_neighborhoods[ $businessNeigh->location_id ][] = $businessNeigh;
        }

        return $client_formatted_neighborhoods;
    }

    public function city_locations($business_id = 0)
    {
        $client_formatted_locations = array();
        $displayLocations = $this->locations($business_id);

        foreach ($displayLocations as $displayLoc) {
            $client_formatted_locations[ $displayLoc->city ][ $displayLoc->locationID ] = array(
                'locationID'    => $displayLoc->locationID,
                'address'       => $displayLoc->address,
                'lockerType'    => $displayLoc->name,
                'city'          => $displayLoc->city,
                'state'         => $displayLoc->state,
                'lat'           => $displayLoc->lat,
                'lon'           => $displayLoc->lon,
                'zipcode'       => $displayLoc->zipcode,
                'serviceType'   => $displayLoc->serviceType,
                'public'        => $displayLoc->public
            );
        }

        return $client_formatted_locations;
    }
//
    public function neighborhoods($business_id = 0)
    {
        $content_neighborhood_query = $this->CI->db->query('SELECT DISTINCT neighborhood, location_id FROM location_neighborhood WHERE business_id=' . $business_id );
        $businessNeighborhoods = $content_neighborhood_query->result();
        $client_formatted_neighborhood_keys = array();
        foreach ($businessNeighborhoods as $businessNeigh) {
            $client_formatted_neighborhood_keys[ $businessNeigh->neighborhood ][] = $businessNeigh->location_id;
        }

        return $client_formatted_neighborhood_keys;
    }

    public function locations($business_id = 0)
    {
        $exclude = implode(",", $this->getExcludedLocationTypes($business_id));

        $displayLocations = $this->CI->location_model->get(array(
            'business_id' => $business_id,
            'status' => 'active',
            'serviceType !=' => 'not in service',
            "locationType_id NOT IN ($exclude)" => null,
            'with_active_locker_count' => true,
            'having' => 'count(lockerID) > 0', // must have active lockers
            'select' => 'locationID, locationType_id, address, location, city, state, public, companyName, hours, lat, lon',
       ));

        $this->CI->load->model('locationType_model');
        $type_map = array();
        foreach ($this->CI->locationType_model->get_all() as $locationType) {
            $type_map[$locationType->locationTypeID] = $locationType->name;
        }

        $output = array();
        $translation_cache = array();

        foreach ($displayLocations as $location) {
            $key = isset($type_map[$location->locationType_id]) ? strtolower($type_map[$location->locationType_id]) : '';
            if (!isset($translation_cache[$key])) {
                $translation_cache[$key] = $key ? get_translation( $key , 'Location Types', array(), $type_map[$location->locationType_id]) : $key;
            }
            $location->name = $translation_cache[$key];
            $output[$location->locationID] = $location;
        }

        return $output;
    }

    public function public_locations($business_id)
    {
        $exclude = implode(",", $this->getExcludedLocationTypes($business_id));

        $results = $this->CI->location_model->get(array(
            'business_id' => $business_id,
            'status' => 'active',
            'serviceType !=' => 'not in service',
            "locationType_id NOT IN ($exclude)" => null,
            'select' => 'locationID, lat, lon',
            'public' => 'yes',
        ));

        //check if the locations has available lockers
        $this->CI->load->model('location_model');
        foreach ($results as $key=>$result) {
            $availableLockers = $this->CI->location_model->getActiveLockerCount($result->locationID);
             if ($availableLockers == 0) {
                unset($results[$key]);
            }
        }

        return $results;

    }

    public function getExcludedLocationTypes($business_id)
    {
        $exclude = array(8); // undefined
        if (get_business_meta($business_id, "hide_home_delivery_locations")) {
            $exclude[] = 4; // home delivery
        }

        return $exclude;
    }

}
