<?php

class Production_metrics extends CI_Model
{
    //This function gets the amount of wash & fold done by employee for a specified date
    public function getWFDataByEmp($getDate, $cleaner)
    {
        $rangeStart = $getDate . ' 09:00:00';
        $rangeEnd = date("Y-m-d H:i:s", strtotime($rangeStart . "+ 1 day"));

        $rangeStart =  date("Y-m-d H:i:s", convert_to_gmt(strtotime($rangeStart), $this->business->timezone));
        $rangeEnd =  date("Y-m-d H:i:s", convert_to_gmt(strtotime($rangeEnd), $this->business->timezone));

        //TODO: using product.name like '%wash%' instead of product category
        $sqlCountByEmp = "select sum(orderItem.qty) as lbs, count(orderItem.order_id) as bags, supplier_id as cleaner_id, wfLog.foldCust
                    FROM orderItem
                    join product_process on (product_processID = orderItem.product_process_id)
                    join product on (productID = product_process.product_id)
                    join orders on (orderID = orderItem.order_id)
                    left join wfLog on (wfLog.order_id = orderID)
                    join locker on (lockerID = orders.locker_id)
                    join location on (locationID = locker.location_id)
                    join bag on (bagID = orders.bag_id)
                    where product.name like '%wash%'
                    and supplier_id = ?
                    and wfLog.weightOutTime > ?
                    and wfLog.weightOutTime < ?
                    group by foldCust";
        $query = $this->db->query($sqlCountByEmp, array($cleaner, $rangeStart, $rangeEnd));

        return $query->result();
    }

    public function getPcsPerHr($selectDay)
    {
        $totPcs = 0;

        //get total hours
        $sqlTotHrs = "SELECT day, inOut, sum(time_to_sec(timeInOut) / 60) as minutes
                FROM timeCardDetail
                join timeCard on timeCardID = timeCardDetail.timeCard_id
                join employee on employeeID = timeCard.employee_id
                WHERE day = '$selectDay'
                group by inOut";
        $query = $this->db->query($sqlTotHrs);
        $totHrs = $query->result();

        foreach ($totHrs as $t) {
            if ($t->inOut == "In") { $inCount = $t->minutes; }
            if ($t->inOut == "Out") { $outCount = $t->minutes; }
        }
        $totHrs = ($outCount - $inCount);

        //get the pcs per hour for that date
        $sqlDatePcs = "SELECT sum(qty) as pcs
                FROM orderStatus
                inner join orderItem on orderItem.order_id = orderStatus.order_id
                join orders on orderID = orderStatus.order_id
                WHERE orderStatus.orderStatusOption_id = 3
                and left(date,10) = '$selectDay'
                and orders.business_id = 4";
        $query = $this->db->query($sqlDatePcs);
        $datePcs = $query->result();
        $totPcs = $datePcs[0]->pcs;

        $pcsPerHr['pcs'] = $totPcs;
        $pcsPerHr['hrs'] = $totHrs/60;

        return $pcsPerHr;
    }

    //Arik - I have no idea what this is for. Probably old timecard stuff
    public function getEligability($selectDay, $empId)
    {
        $eligable = 1;
        $sqlMinutes = "SELECT min(timeInOut) as timeIn, (time_to_sec(timeInOut) / 60) as minutes, type
                    FROM timeCardDetail
                    join timeCard on timeCardID = timeCardDetail.timeCard_id
                    WHERE day = '$selectDay'
                    and employee_id = $empId
                    group by timeCardID";
        $query = $this->db->query($sqlMinutes);
        $minutes = $query->result();
        if ($minutes[0]->type <> "Regular") {
            $eligable = 0;
        }
        if ($empId == "1118" and $minutes[0]->minutes > 515) { //william
            $eligable = 0;
        } elseif ($empId == "1211" and $minutes[0]->minutes > 725 and date("l", strtotime($timeIn) == "Monday")) { //patty
            $eligable = 0;
        } elseif ($minutes[0]->minutes > 575) {
            $eligable = 0;
        }

        return $eligable;
    }

    public function pphPressing($employee_id, $startDate = null, $endDate = null, $detail = false)
    {
        if (!$startDate) {
            $startDate = date("Y-m-d");
        }

        $includeHours = (strlen($startDate) < 16);

        list($startTime, $endTime) = get_date_range($startDate, $endDate, $includeHours);

        $getItems = "select ihi.*, (select ih.rework
                                    from itemHistory ih
                                    where ih.item_id = ihi.item_id
                                    and ih.creator != ihi.creator
                                    and ih.rework = 1
                                    and ih.date >= ?
                                    and ih.date <= ?
                                    limit 1) as reworked
                        from itemHistory ihi
                        where date >= ?
                        and date <= ?
                        and creator = ?
                        order by date asc";
        $query = $this->db->query($getItems, array($startTime, $endTime, $startTime, $endTime, $employee_id));

        $items = $query->result();
        $counter = 0;
        $rework = 0;
        $elapsedTime = '';

        foreach ($items as $i) {
            $counter++;
            if ($detail) {
                $pph['items'][$counter]['id'] = $i->item_id;
                $pph['items'][$counter]['dateStamp'] = $i->date;
                $pph['items'][$counter]['description'] = $i->description;
            }

            //elapsed time is only the time they spent pressing
            if ($counter == 1) {
                $lastItem = $i->date;
            }

            if (strtotime($i->date) - strtotime($lastItem) < 500) {
                $elapsedTime += strtotime($i->date) - strtotime($lastItem);
            }
            $lastItem = $i->date;

            //get number of reworked items
            if ($i->reworked == 1) {
                $rework++;
            }
        }
        $pph['rework'] = $rework;
        $pph['itemCount'] = $counter;
        $pph['elapsedTime'] = $elapsedTime;
        $pph['userUpcharges'] = 0;

        $pph['pph'] = 0;
        if ($elapsedTime) {
            $pph['pph'] = $counter/($elapsedTime/60/60);
        }

        return $pph;
    }

    public function pphInventory($empId, $startDate = null, $endDate = null, $details = false)
    {

        if (!$startDate) {
            $startDate = date("Y-m-d");
        }

        $includeHours = (strlen($startDate) < 16);

        list($startTime, $endTime) = get_date_range($startDate, $endDate, $includeHours);


        $params = array($startTime, $endTime);
        $sql_employee = "";

        if ($empId) {
            $sql_employee = "AND employee_id = ?";
            $params[] = $empId;
        }

        //get the orders inventoried
        $userOrders = "SELECT *
            FROM orderStatus
            WHERE date >= ? AND date <= ?
                AND orderStatusOption_id = 3
                $sql_employee
                ORDER BY date";

        //get order entry errors
        $entryErrors = "SELECT count(orderStatusID) AS errors
                FROM orderStatus
                WHERE note LIKE 'Order Entry Error%'
                AND date >= ? AND date <= ?
                $sql_employee";

        $query = $this->db->query($entryErrors, $params);
        $entryErrors = $query->result();
        $pph['entryErrors'] = $entryErrors[0]->errors;

        //get all the items from the day
        $this->load->model('orderItem_model');
        $allItems = $this->orderItem_model->todays_items($this->business_id, $startTime, $endTime, false);

        $allCount = 0;
        $totUpcharges = 0;
        foreach ($allItems as $a) {
                $itemsArray[$a->order_id][$a->item_id]["updated"] = $a->updated;
                $itemsArray[$a->order_id][$a->item_id]["barcode"] = $a->barcode;
                $upchargeArray[$a->item_id] = $a->upcharges;
                $totUpcharges += $a->upcharges;
                $allCount++;
        }

        //now loop through the users orders to get a total
        $query = $this->db->query($userOrders, $params);
        $userOrders = $query->result();


        $counter = 0;
        $elapsedTime = 0;
        $userUpcharges = 0;
        $lastItem = null;

        $ordersAdded = array();

        foreach ($userOrders as $u) {

            if (in_array($u->order_id, $ordersAdded)) {
                continue;
            }

            $ordersAdded[] = $u->order_id;

            if (!empty($itemsArray[$u->order_id])) {
                //loop through all the items for this order
                foreach ($itemsArray[$u->order_id] as $key => $value) {
                    $updated = $value["updated"];
                    $barcode = $value["barcode"];

                    //elapsed time is only time spent checking in clothes
                    if (!$lastItem) {
                        $lastItem = $updated;
                    }
                    $counter++;
                    if ($counter == 1) {
                        $lastItem = $updated;
                    }

                    if (strtotime($updated) - strtotime($lastItem) < 500 and strtotime($updated) - strtotime($lastItem) > 0) {
                        $elapsedTime += strtotime($updated) - strtotime($lastItem);
                    }

                    $lastItem = $updated;

                    //see if there were any upcharges on this item
                    $userUpcharges += $upchargeArray[$key];

                    if ($details) {
                        $itemArray['order_id'] = $u->order_id;
                        $itemArray['updated'] = $updated;
                        $itemArray['upcharge'] = $upchargeArray[$key];
                        $itemArray['item_id'] = $key;
                        $itemArray['barcode'] = $barcode;
                        $pph['details'][] = $itemArray;
                    }
                }
            }
        }

        $pph['allItems'] = $allCount;
        $pph['itemCount'] = $counter;
        $pph['elapsedTime'] = $elapsedTime;
        $pph['totUpcharges'] = $totUpcharges;
        $pph['userUpcharges'] = $userUpcharges;
        if ($elapsedTime) {
            $pph['pph'] = $counter/($elapsedTime/60/60);
        }

        return $pph;
    }

    public function pphPictures($empId, $startDate = null, $endDate = null)
    {
        if (!$startDate) {
            $endDate = date("Y-m-d");
        }

        list($startTime, $endTime) = get_date_range($startDate, $endDate);

        //get the pictures taken by this employee
        $pictures = "SELECT *
                        FROM picture
                        WHERE
                            taken >= ?
                            AND taken <= ?
                            AND employee_id = ?
                            ORDER BY taken;";

        $query = $this->db->query($pictures, array($startTime, $endTime, $empId));
        $pictures = $query->result();

        $lastItem = null;
        $elapsedTime = 0;
        $counter = 0;
        foreach ($pictures as $p) {
            //elapsed time is only time spent
            if (!$lastItem) {
                $lastItem = $p->taken;
            }

            $counter++;
            if ($counter == 1) {
                $lastItem = $p->taken;
            }

            if (strtotime($p->taken) - strtotime($lastItem) < 500 and strtotime($p->taken) - strtotime($lastItem) > 0) {
                $elapsedTime += strtotime($p->taken) - strtotime($lastItem);
            }
            $lastItem = $p->taken;
        }

        $pph['allItems'] = $pictures;
        $pph['itemCount'] = $counter;
        $pph['elapsedTime'] = $elapsedTime;
        if ($elapsedTime) {
            $pph['pph'] = $counter/($elapsedTime/60/60);
        }

        return $pph;
    }


}
