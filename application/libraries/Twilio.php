<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Name:  Twilio
 *
 * Author: Ben Edmunds
 *		  ben.edmunds@gmail.com
 *         @benedmunds
 *
 * Location:
 *
 * Created:  03.29.2011
 *
 * Description:  Modified Twilio API classes to work as a CodeIgniter library.
 *               Added additional helper methods.
 *               Original code and copyright are below.
 *
 *
 */
class Twilio
{
    protected $_ci;
    protected $_twilio;
    protected $mode;
    protected $account_sid;
    protected $auth_token;
    protected $api_version;
    protected $number;

    /*
     * db business_sms values should come through here in the $params
     */
    function __construct( $params = array() )
    {
        //initialize the CI super-object
        $this->_ci = get_instance();

        //get settings from config
        $this->mode        = $params['mode'];
        $this->account_sid = $params['account_sid'];
        $this->auth_token  = $params['auth_token'];
        $this->api_version = $params['api_version'];
        $this->number      = $params['number'];


        //initialize the client
        $this->_twilio = new Services_Twilio($this->account_sid, $this->auth_token);
    }

    //for use with codeigniter, so we can reinstantiate later
    public function initialize( $params = array() )
    {
            //get settings from config
            $this->mode        = $params['mode'];
            $this->account_sid = $params['account_sid'];
            $this->auth_token  = $params['auth_token'];
            $this->api_version = $params['api_version'];
            $this->number      = $params['number'];

            //initialize the client
            $this->_twilio = new Services_Twilio($this->account_sid, $this->auth_token);
    }

    /**
     * __call
     *
     * @desc Interface with rest client
     *
     */
    public function __call($method, $arguments)
    {
        if (!method_exists( $this->_twilio, $method) ) {
            throw new Exception('Undefined method Twilio::' . $method . '() called');
        }

        return call_user_func_array( array($this->_twilio, $method), $arguments);
    }

    /**
     * Send SMS
     *
     * @desc Send a basic SMS
     *
     * @param <int> Phone Number
     * @param <string> Text Message
     */
    public function sms($from, $to, $message)
    {
        return $this->_twilio->account->messages->sendMessage($from, $to, $message);
    }

}

require(APPPATH . 'third_party/Twilio/Twilio.php');
