<?php
/**
 * must pass order object
 * must call unapply_discounts first then apply_discounts
 */

class Discounts
{
    public $order;
    public $order_items = array();
    public $applyDiscount = 1;
    public $message = '';
    public $_ci;
    public $last_inserted_location_minimum_charge_id = null;
    /**
        * if this order has already been captured, don't apply discounts
        */
    public function __construct()
    {
        $this->_ci = get_instance();
    }

    public function check_order_status()
    {
        if ($this->order->orderPaymentStatusOption_id == 3 && $this->order->pre_paid_amount <= 0) {
            return false;
        }

        return true;
    }

    /**
        * used to remove all discounts
        * @param Order $order
        * @return type
        * @throws Exception
        */
    public function unapply_discounts($order)
    {
        $CI = get_instance();
        $this->order = $order;

        $business_id = $order->business_id;
        if (!$this->check_order_status()) {
            throw new \Exception("Payment has been captured for this order. Can not change discounts.");
        }

        $sql = "SELECT * from orderCharge
                        WHERE order_id = ".$this->order->orderID." and customerDiscount_id is null;";
        $query = $CI->db->query($sql);
        $orderCharges = $query->result();
        if ($orderCharges) {
            $this->applyDiscount = 0;
            $this->message .= 'Discounts can not be reapplied to orders with manual discounts created.  Please remove the manual discounts and try again.';

            queueEmail($CI->business->email, get_business_meta($order->business_id, "from_email_name"), get_business_meta($order->business_id, "operations"), 'Discount NOT Reapplied', "Discounts tried to be reapplied to an order with manual discounts created.  Please investigate this immediately.<br>Order <a href='http://{$_SERVER['HTTP_HOST']}/admin/orders/details/{$this->order->orderID}'>" .$this->order->orderID.'</a>', array(), null, $business_id);
            set_flash_message("error", "Discounts can not be reapplied to orders with manual discounts created.  Please remove the manual discounts and try again.");
        } else {
            //get the charges that have been applied to the order
            $sql = "SELECT * from orderCharge
                            WHERE order_id = ".$this->order->orderID.";";
            //die($sql);
            $query = $CI->db->query($sql);
            $orderCharges = $query->result();

            if ($orderCharges) {
                $this->applyDiscount = 0;
                foreach ($orderCharges as $c) {
                    //customerDiscount_id == '0' for stuff like $15 minumums
                    if ($c->customerDiscount_id == '0') {
                        $sql = "delete from orderCharge
                                        WHERE orderChargeID = ".$c->orderChargeID;
                        //die($sql);
                        $CI->db->query($sql);
                        $this->message .= 'Discount Deleted: '.$c->chargeType.' for '.format_money_symbol($business_id, '%.2n', $c->chargeAmount).'<br>';
                        $this->applyDiscount = 1;
                    } else {
                        //find original discount
                        $sql = "SELECT * from customerDiscount
                                        WHERE customerDiscountID = $c->customerDiscount_id";
                        //die($sql);
                        $query = $CI->db->query($sql);
                        $customerDiscounts = $query->result();
                        if ($customerDiscounts[0]->frequency == 'every order' or $customerDiscounts[0]->frequency == 'one time') {
                            //delete the credit from the order before we reapply a new one
                            $sql = "DELETE from orderCharge
                                            WHERE orderChargeID = ".$c->orderChargeID;
                            $CI->db->query($sql);
                            $this->message .= 'Discount Deleted: '.$c->chargeType.' for '.format_money_symbol($business_id, '%.2n', $c->chargeAmount).'<br>';

                            //reactivate the one time discount.
                            if ($customerDiscounts[0]->frequency == 'one time') {
                                $sql = "UPDATE customerDiscount
                                                    SET active = 1
                                                    WHERE customerDiscountID = ".$customerDiscounts[0]->customerDiscountID;
                                $CI->db->query($sql);
                            }

                            $this->applyDiscount = 1;
                        } elseif ($customerDiscounts[0]->frequency == 'until used up') {
                            if (strpos($customerDiscounts[0]->amountType, "product_plan") !== false) {
                                return array('status'=>'success');
                            }
                            //create a new discount for the amount that was applied to this order
                            $desc = 'Total amount changed on order '.$this->order->orderID.' so new credit created.';
                            $refundAmt = $customerDiscounts[0]->amountApplied * -1;
                            $descriptionR = 'Correcting Entry ('.$customerDiscounts[0]->description.')';
                            
                            $sql = "INSERT INTO customerDiscount
                                    SET customer_id = ".$customerDiscounts[0]->customer_id.",
                                    amount = '".$refundAmt."',
                                    amountType = '".$customerDiscounts[0]->amountType."',
                                    frequency = '".$customerDiscounts[0]->frequency."',
                                    description = " . $CI->db->escape($descriptionR) . ",
                                    extendedDesc = " . $CI->db->escape($desc) . ",
                                    business_id = $business_id,
                                    active = 1,
                                    origCreateDate = '".$customerDiscounts[0]->origCreateDate."',
                                    updated = now()";
                            if (!$customerDiscounts[0]->expireDate) {
                                $sql .= ", expireDate = NULL;";
                            } else {
                                $sql .= ", expireDate = '".$customerDiscounts[0]->expireDate."';";
                            }
                            //die($sql);
                            $CI->db->query($sql);

                            $sql = "DELETE FROM orderCharge
                                            WHERE orderChargeID = ".$c->orderChargeID;
                            $CI->db->query($sql);
                            $this->message .= 'Until Used Up Discount Deleted: '.$c->chargeType.' for '.format_money_symbol($business_id, '%.2n', $c->chargeAmount).'<br>';
                            $this->applyDiscount = 1;
                        } else {
                            $this->message .= 'Problem reapplying discount. The current discount is of an unknown type. Tech Support has been contacted.<br>';

                            send_admin_notification('Discount NOT Reapplied', 'A discount tried to be reapplie but failed becauase the discount type is unknown.  Please fix this manually.<br>Order <a href="http://'.$this->business->website_url.'/admin/orders/details/'.$this->order->orderID.'</a>');
                        }
                    }
                }
            }

            if (sizeof($orderCharges) > 1) {
                queueEmail($CI->business->email, get_business_meta($order->business_id, "from_email_name"), get_business_meta($order->business_id, "operations"), 'Double check multiple discounts', "Double check this order to make sure multiple discounts are being reapplied correctly. <br>Order <a href='http://droplocker.com/admin/orders/details/{$this->order->orderID}'> {$this->order->orderID} </a>", array(), null, $business_id);

                return array("status"=>'success', "warning"=>"Double check multiple discounts.");
            }
        }

        return array('status'=>'success');
    }

    private function getWashAndFoldMinimum($business_id)
    {
        $this->_ci->db->select('wf_min_amount, wf_min_type');
        $wf_min_query = $this->_ci->db->get_where('business', array( 'businessID' => $business_id ));
        $wf_pre_data =  $wf_min_query->row();

        //sanity checking
        if (empty($wf_pre_data) || empty($wf_pre_data->wf_min_type) || empty($wf_pre_data->wf_min_amount)) {
            return false;
        } else {
            return $wf_pre_data;
        }
    }

    private function getLocationMinimum($location_id, $order_type = "dc")
    {
        $min_amount = $order_type . '_min_amount';
        $min_type = $order_type . '_min_type';
        $this->_ci->db->select("$min_amount, $min_type");
        
        $location_minimum = $this->_ci->db->get_where(
            'location',
            array(
                'locationID' => $location_id
                )
            );
        
        $location_minimum = $location_minimum->row();

        //sanity checking
        if (empty($location_minimum) || empty($location_minimum->{$min_amount}) || empty($location_minimum->{$min_type})) {
            return false;
        } else {
            return array(
                "minimum" => $location_minimum->{$min_amount},
                "type"    => $location_minimum->{$min_type}
                );
        }
    }

    public function getOrderMinimum($business_id, $order_type)
    {
        $order_type = strtolower($order_type);
        $min_amount_field = $order_type.'_min_amount';
        $min_type_field = $order_type.'_min_type';

        $min_query = $this->_ci->db->get_where('business', array( 'businessID' => $business_id ));
        $pre_data =  $min_query->row();

        //sanity checking
        if (empty($pre_data) || empty($pre_data->$min_type_field) || empty($pre_data->$min_amount_field)) {
            return false;
        } else {
            return $pre_data;
        }
    }

    private function getCustomerDiscounts($customer_id)
    {
        $sql = "SELECT * FROM customerDiscount
                    WHERE customer_id = " . $customer_id . "
                    and active = '1'
                    and amountType like '%item%'";
        $query = $this->_ci->db->query($sql);
        $customerDiscounts = $query->result();

        return $customerDiscounts;
    }

    private function getCustomerDiscountMaxedOut($customer_id, $coupon_code, $order_id)
    {
        $select10 = "SELECT sum(amountApplied) * -1 as amtApplied FROM customerDiscount
                                where customer_id = " . $customer_id . "
                                and couponCode = '" . $coupon_code . "'
                                and order_id <> " . $order_id;
        $query10 = $this->_ci->db->query($select10);

        return $query10->result();
    }

    private function getEveryOrderDiscounts($customer_id)
    {
        //next do every order ers
        $query1 = "SELECT * FROM customerDiscount
            WHERE customer_id = " . $customer_id . "
            and active = '1'
            and frequency = 'every order'
            and amountType not like '%item%' ";
        if ($only_negative) {
            $query1 .= "and amount < 0 ";
        }
        $query1 .= "ORDER BY combine, origCreateDate";

        return $this->_ci->db->query($query1)->result();
    }

    private function getAllCustomersDiscounts($business_id)
    {
        //next do every order ers
        $query1 = "SELECT * FROM coupon
            WHERE business_id = ?
            and frequency = 'all customers'
            and amountType not like '%item%' 
            and startDate <= curdate()
            and ( (endDate >= curdate()) OR (endDate is null) )";

        $result = $this->_ci->db->query($query1, $business_id)->result();
        return $result;
    }

    private function getUntilUsedUpDiscounts($customer_id)
    {
        //finally do until used up discounts
        $query1 = "SELECT * FROM customerDiscount
                WHERE customer_id = " . $customer_id . "
                and active = '1'
                and frequency = 'until used up'
                and amountType not like '%item%'
            ORDER BY combine, origCreateDate";

        $query = $this->_ci->db->query($query1);

        return $query->result();
    }

    private function getOneTimeOrderItems($order_id, $product_id, $amount)
    {
        $sql = "select * from orderItem
            join product_process on product_processID = product_process_id
            WHERE order_id = " . $order_id . "
            and product_id = " . $product_id . "
            and unitPrice <> '". $amount . "'";

        $query = $this->_ci->db->query($sql);
        $orderItems = $query->result();

        return $orderItems;
    }

    private function getOneTimeDiscounts($customer_id)
    {
        $query1 = "SELECT * FROM customerDiscount
                WHERE customer_id = '{$customer_id}'
                and active = '1'
                and frequency = 'one time'
                and amountType not like '%item%'
                and amountType != 'pounds'
                ORDER BY amountType, combine, origCreateDate";

        return $this->_ci->db->query($query1)->result();
    }

    private function updateOneTimeOrderItems($amount, $description, $order_item_id)
    {
        $sql = "update orderItem set unitPrice = ?,
                notes = ?
                WHERE orderItemID = ?";

        return $this->_ci->db->query($sql, array($amount, $description, $order_item_id));
    }

    private function updateOneTimeCustomerDiscount($order_id, $customer_discount_id)
    {
        $sql= "update customerDiscount
               set active = 0,
               order_id = " . $order_id . ",
               updated = now()
               where customerDiscountID = " . $customer_discount_id;

        return $this->_ci->db->query($sql);
    }

    private function updateCustomerDiscount($order_id, $customer_discount_id, $setToInactive = false)
    {
         if (!$setToInactive) return;

        $update1 = "update customerDiscount
                    set active = 0,
                    order_id = " . $order_id . ",
                    updated = now()
                    where customerDiscountID = " . $customer_discount_id;

        return $this->_ci->db->query($update1);
    }

    private function getDryCleanOrderItems($order_id, $process_id = 0)
    {
        //loop through all items in the order of this class
        $sql = "select orderItemID, unitPrice, orderItem.notes
                from orderItem
                join product_process on product_processID = product_process_id
                join product on productID = product_process.product_id
                join productCategory on productCategoryID = productCategory_id
                WHERE order_id = '" . $order_id . "'
                and productCategory.module_id = 3";

        if ($process_id) {
            $sql .= " and product_process.process_id = '$process_id'";
        }

        $query = $this->_ci->db->query($sql);
        $orderItems2 = $query->result();

        return $orderItems2;
    }

    private function updateOrderItemPrice($price, $order_item_id)
    {
        $sql = "update orderItem set unitPrice = '$price' WHERE orderItemID = $order_item_id";

        return $this->_ci->db->query($sql);
    }

    private function getMatchingOrderItems($order_id, $product_id)
    {
        //get matching orderItems
        $getItems = "select * from orderItem
                        join product_process on product_processID = product_process_id
                        WHERE order_id = '".$order_id."'
                        and product_id = $product_id";

        $query = $this->_ci->db->query($getItems);

        return $query->result();
    }

    private function updateOrderItemShirt($order_item_id)
    {
        $update1 = "update orderItem
                        set unitPrice = unitPrice - .30,
                        notes = concat('$2.14 shirt with each item of dry cleaning. ', notes)
                        where orderitemID = " . $order_item_id;

        return  $this->_ci->db->query($update1);
    }

    private function getWeightDiscounts($customer_id)
    {
        //don't apply this minimum to laundry plan customers. The minimum is assessed later.
        $query1 = "SELECT * FROM customerDiscount
                        WHERE customer_id = " . $customer_id . "
                        and active = '1'
                        and amountType = 'pounds'";

        $query = $this->_ci->db->query($query1);

        return $query->result();
    }

    private function getWashAndFoldWeightDiscounts($customer_id)
    {
        $query2 = "SELECT * FROM customerDiscount
            WHERE customer_id = " . $customer_id . "
            and active = '1'
            and amountType = 'pounds'
            order by customerDiscountID desc";

        $query2 = $this->_ci->db->query($query2);

        return $query2->result();
    }

    private function getRemainingWeight($customer_id)
    {
        $select5 = "select sum(amount) as totLbs from customerDiscount WHERE customer_id = '{$customer_id}'
                        and active = '1'
                        and amountType = 'pounds'";
        $select5 = $this->_ci->db->query($select5);

        return $select5->result();
    }

    private function getWashAndFoldItems($order_id)
    {
        //get wash and fold products
        $sql = "SELECT orderItem.*
            FROM orderItem
            JOIN product_process ON product_processID = product_process_id
            JOIN product ON (productID = product_id)
            JOIN productCategory ON (productCategoryID = productCategory_id)
            WHERE order_id = ? AND slug = ?";

        $query = $this->_ci->db->query($sql, array($order_id, WASH_AND_FOLD_CATEGORY_SLUG));

        return $query->result();
    }

    private function insertOrderCharge($order_id, $charge_type, $charge_amount, $customer_discount_id = 0, $isLocationMinimum = false)
    {
        if ($charge_amount != 0 && $charge_type != null) {
            $charge_amount = str_replace(',', '.', $charge_amount);   //switch commas for decimals for international cases
            $insert1 = "INSERT into orderCharge
                                    set order_id = " . $order_id . ",
                                    chargeType = " . $this->_ci->db->escape($charge_type) . ",
                                    chargeAmount = '{$charge_amount}',
                                    customerDiscount_id = " . $customer_discount_id . ",
                                    updated = now();";

            $return = $this->_ci->db->query($insert1);
            
            if ($isLocationMinimum) {
                $this->last_inserted_location_minimum_charge_id = $this->_ci->db->insert_id();
            }

            return $return;
        }
    }

    private function getOrderLocation($order_id)
    {
        $query1 = "select location.*
                                from orders
                                join locker on lockerID = orders.locker_id
                                join location on locationID = locker.location_id
                                where orderID = " . $order_id;

        $query = $this->_ci->db->query($query1);

        return $query->row();
    }

    private function getLocationDiscount($location_id)
    {
        $location_query = $this->_ci->db->get_where('location_discount', array('location_id' => $location_id ));

        return $location_query->row();
    }

    private function getOrderLocationType($order_id)
    {
        $query1 = "select locationType_id
                                from orders
                                join locker on lockerID = orders.locker_id
                                join location on locationID = locker.location_id
                                where orderID = " . $order_id . ";";

        $query = $this->_ci->db->query($query1);

        return $query->row();
    }

    private function setCustomerDiscountToInactive($order_id, $amount_applied, $customer_discount_id)
    {
        $amount_applied = str_replace(',', '.', $amount_applied);
        //update to inactive
        $update1 = "update customerDiscount
                set active = 0,
                order_id = {$order_id},
                amountApplied = $amount_applied,
                updated = now()
                where customerDiscountID = {$customer_discount_id}";

        return $this->_ci->db->query($update1);
    }

    private function getOrderTotalWeight($order_id, $business_id)
    {
        $query = 'SELECT qty FROM orderItem
                  JOIN product_process ON product_process.product_processID = orderItem.product_process_id
                  JOIN product ON product.productID = product_process.product_id
                  WHERE orderItem.order_id = ?
                  AND product.productCategory_id = ( SELECT productCategoryID as productCategory_id from productCategory where productCategory.slug = ? AND productCategory.business_id =?)';

        $order_weight_query =  $this->_ci->db->query($query, array($order_id, WASH_AND_FOLD_CATEGORY_SLUG, $business_id));
        $order_weight = $order_weight_query->row();

        if (empty($order_weight->qty)) {
            return 0;
        }

        return $order_weight->qty;
    }

    private function getHomeDeliveryCharge($business_id)
    {
        $this->_ci->db->select('home_delivery_charge_type, home_delivery_charge_amount');
        $homedeliveryquery = $this->_ci->db->get_where('business', array( 'businessID' => $business_id ));

        return $homedeliveryquery->row();
    }

    private function getOrderTotal($order_id)
    {
        $this->_ci->load->model('orderitem_model');
        $res = $this->_ci->orderitem_model->get_item_total($order_id);

        return $res['total'];
    }
    /**
        * used to apply discounts
    */
    /**
     *
     * @param Order $order
     * @param bool $unapply
     * @return type
     * @throws Exception
     */
    public function apply_discounts($order, $unapply=true, $lbs = 0)
    {
        if ($unapply) {
            $unres = $this->unapply_discounts($order);

            if ($unres['status']!='success') {
                return array("status"=>"fail", "message"=>"Unapply discounts has returned with an error");
            }
        }

        $CI = get_instance();
        $this->order = $order;
        $business_id = $order->business_id;

        if (!$this->check_order_status()) {
            throw new Exception("Payment has been captured for this order. Cannot change discounts");
        }
        if (empty($this->order->customer_id)) {
            throw new Exception("Can not apply discounts because there is no customer for this order");
        }

        //get the order location
        $location = $this->getOrderLocation($this->order->orderID);

        //location discount - see if one exists
        $location_discount = $this->getLocationDiscount($location->locationID);

        // Note, if the order does not have a customer with it, then there is no need to apply any customer discounts.
        if ($this->applyDiscount == 1) {

            //if this customer has any expired discounts, make them inactive
            $CI->load->model('customerdiscount_model');
            $CI->customerdiscount_model->expireCustomerDiscount($this->order->business_id, $this->order->customer_id);


            // get the order Total
            $CI->load->model('orderitem_model');
            $ordTotal = $this->getOrderTotal($this->order->orderID);

            if ($ordTotal > 0) {
                $customerDiscounts = $this->getCustomerDiscounts($this->order->customer_id);

                //if there are no customer discounts, the function exits.
                //TODO remove the large if statement and just return.
                if ($customerDiscounts) {
                    //For each coupon (from flow chart)
                    foreach ($customerDiscounts as $cd) {
                        $setToInactive = false;

                        //line 129 in applyDiscounts.php
                        $productId = explode("[", $cd->amountType);
                        $productId = substr($productId[1], 0, -1);
                        $is_percent = false;
                        if (substr($productId, 0, 1) == '%') {
                            $is_percent = true;
                            $productId = substr($productId, 1);
                        }

                        $notice = '';

                        //if it is a class e.g. DC
                        if (substr($productId, -2) == 'dc') {
                            $orderItems2 = $this->getDryCleanOrderItems($this->order->orderID, $cd->process_id);
                            foreach ($orderItems2 as $o) {
                                //if it is a percent off
                                if ($is_percent) {
                                    if ($cd->frequency == "every order" and $o->notes != '') {
                                        //notify that discount could not be reapplie
                                        $notice .= "Order Item ID ".$o->orderItemID." on order <a href='http://{$_SERVER['HTTP_HOST']}/admin/orders/details/{$this->order->orderID}'>".$this->order->orderID."</a> did not get an every order percent off discount because it had a note and it is therefore assumed that this discount was being reapplied again.<br><br>";
                                        $newPrice = $o->unitPrice;
                                    } else {
                                        $newPrice = number_format($o->unitPrice * ((100 - $cd->amount) * .01), 2, '.', '');
                                    }
                                } else {
                                    $newPrice = $cd->amount;
                                }

                                //add discount to item notes, preserving old notes
                                $item_notes = trim(trim($o->notes) . "\n" . $cd->description);

                                $this->updateOneTimeOrderItems($newPrice, $item_notes, $o->orderItemID);

                                    $setToInactive = true;
                            }
                        } else {
                            if ($cd->frequency == "one time") {
                                $orderItems = $this->getOneTimeOrderItems($this->order->orderID, $productId, $cd->amount);
                                if ($orderItems) {
                                    foreach ($orderItems as $orderItem) {
                                        $newPrice = $cd->amount;
                                        if ($is_percent) {
                                            $newPrice = number_format($orderItem->unitPrice * ((100 - $cd->amount) * .01), 2, '.', '');
                                        }

                                        //add discount to item notes, preserving old notes
                                        $item_notes = trim(trim($orderItem->notes) . "\n" . $cd->description);

                                        //apply the discount to this order item
                                        $this->updateOneTimeOrderItems($newPrice, $item_notes, $orderItem->orderItemID);
                                    }
                                    //set the discount to inactive
                                    $this->updateOneTimeCustomerDiscount($this->order->orderID, $cd->customerDiscountID);

                                    $setToInactive = true;
                                }
                            } else {
                                //get matching orderItems
                                $orderItems2 = $this->getMatchingOrderItems($this->order->orderID, $productId);
                                foreach ($orderItems2 as $o) {
                                    $newPrice = $cd->amount;

                                    if ($is_percent) {
                                        if ($cd->frequency == "every order" && stripos($o->notes, $cd->description) !== false) {
                                            //notify that discount could not be reapplie
                                            $notice .= "Order Item ID ".$o->orderItemID." on order <a href='http://{$_SERVER['HTTP_HOST']}/admin/orders/details/{$this->order->orderID}'>".$this->order->orderID."</a> did not get an every order percent off discount because it had a note and it is therefore assumed that this discount was being reapplied again.<br><br>";
                                            $newPrice = $o->unitPrice;
                                        } else {
                                            $newPrice = number_format($o->unitPrice * ((100 - $cd->amount) * .01), 2, '.', '');
                                        }
                                    }

                                    //add discount to item notes, preserving old notes
                                    $item_notes = trim(trim($o->notes) . "\n" . $cd->description);

                                    $this->updateOneTimeOrderItems($newPrice, $item_notes, $o->orderItemID);

                                    $setToInactive = true;
                                }
                            }
                        }


                        if ($cd->frequency != 'every order') {
                            $this->updateCustomerDiscount( $this->order->orderID, $cd->customerDiscountID, $setToInactive );
                        }

                        if (!empty($notice)) {
                            queueEmail($CI->business->email, get_business_meta($order->business_id, "from_email_name"), get_business_meta($order->business_id, "operations"), 'Discount NOT Reapplied', $notice, array(), null, $business_id);
                        }

                    }
                }
                // end of customer Discounts

                //get the new order total
                $ordTotal = $this->getOrderTotal($this->order->orderID);

                //don't give this to wholesale accounts
                $CI->load->model('customer_model');
                $CI->customer_model->customerID = $this->order->customer_id;
                $customer = $CI->customer_model->get();
                $discCust = $customer[0];

                //don't apply this minimum to laundry plan customers. The minimum is assessed later.
                $weighted_discounts = $this->getWeightDiscounts($this->order->customer_id);


                //order types that have a minimum amount setting
                $orderTypes = array('wf', 'dc');
                $CI->load->model('order_model');
                $orderType = strtolower($CI->order_model->getOrderType($this->order->orderID));
                //apply minimum charges

                if ((!$weighted_discounts) && (in_array($orderType, $orderTypes))) {

                    //does this business have a minimum setting
                    $min_data = $this->getOrderMinimum($this->order->business_id, $orderType);

                    $min_type_field  = $orderType.'_min_type';
                    $min_amount_field  = $orderType.'_min_amount';

                    $set_location_minimum_id = false;
                    //determine what the order minimum is
                    if (!empty($location->$min_type_field) && !empty($location->$min_amount_field)) {
                        $order_min['type'] = $location->$min_type_field;
                        $order_min['amount'] =  $location->$min_amount_field;
                        $order_min['text'] = get_translation('location_minimum', 'Discount Minimum', array(), '%s Location Minimum', $this->order->customer_id);
                        $set_location_minimum_id = true;
                    } elseif (!empty($min_data->$min_type_field) && !empty($min_data->$min_amount_field)) {
                        $order_min['type'] = $min_data->$min_type_field;
                        $order_min['amount'] =  $min_data->$min_amount_field;
                        $order_min['text'] =  get_translation('minimum', 'Discount Minimum', array(), '%s Minimum', $this->order->customer_id);
                    }

                    if (!empty($order_min['type']) and $order_min['amount'] > 0) {
                        $chargeAmount = 0;
                        //if its pounds then then orderType = 'wf'
                        if ($order_min['type'] == 'pounds') {
                            $total_order_weight = $this->getOrderTotalWeight($this->order->orderID, $business_id) ;

                            //if total_order_weight is empty, there's no wfLog record, so check to see if weight was passed
                            if (empty($total_order_weight) && !empty($lbs)) {
                                $total_order_weight = $lbs;
                            }

                            if ($total_order_weight < $order_min['amount']) {
                                //create the weight charge here per lb diff
                                $products = $this->getWashAndFoldItems($this->order->orderID);

                                if (count($products)) {
                                    $weight_diff = $order_min['amount'] - $total_order_weight;
                                    $chargeAmount = $weight_diff * $products[0]->unitPrice;
                                    $order_min_text = sprintf($order_min['text'], weight_system_format($order_min['amount']));
                                }
                            }
                        } elseif ($order_min['type'] == 'dollars') {
                            if ($ordTotal < $order_min['amount']) {
                                $chargeAmount = $order_min['amount'] - $ordTotal;
                                $order_min_text = sprintf($order_min['text'], format_money_symbol($business_id, '%.2n', $order_min['amount']));
                            }
                        }

                        if ($chargeAmount) {
                            $this->insertOrderCharge($this->order->orderID, $order_min_text, $chargeAmount, 0, $set_location_minimum_id);
                        }
                    }
                }
                //get the new order total
                $ordTotal = $this->getOrderTotal($this->order->orderID);

                // SPECIAL PROMOTION

                $specialPromo = 0;

                $noMore = null;
                
                if (!empty($location_discount) && ($location_discount->percent > 0 || !empty($location_discount->dollar_amount))) {
                    if (strtotime('now') <= strtotime($location_discount->expire_date)) { //check if still valid
                        $specialPromo = 1;      //mark as specialPromo

                        $chargeDesc = $location_discount->description;  //discount description
                        if ($location_discount->combine == 0) {         //can we combine with other discounts?
                             $noMore = 1;
                        }

                        if ($location_discount->percent > 0) {
                            $chargeAmount = $ordTotal * (- ($location_discount->percent / 100)) ;
                        }

                        if (!empty($location_discount->dollar_amount)) {
                            $only_apply_if_minimum_is_not_met = $location_discount->only_apply_if_minimum_is_not_met;
                            
                            if (!empty($only_apply_if_minimum_is_not_met)) {
                                if (!empty($this->last_inserted_location_minimum_charge_id)) {
                                    $sql = "delete from orderCharge
                                                    WHERE order_id = ? and orderChargeID = ?";
                                    $query = $CI->db->query(
                                        $sql, 
                                        array(
                                            $this->order->orderID, 
                                            $this->last_inserted_location_minimum_charge_id
                                            )
                                        );
                                }

                                $location_minimum = $this->getLocationMinimum($location_discount->location_id, $orderType);

                                if (!empty($location_minimum) && $location_minimum['type'] == "dollars") {
                                    //get the new order total
                                    $ordTotal = $this->getOrderTotal($this->order->orderID);
                                    if ($ordTotal < $location_minimum['minimum']) {
                                        $chargeAmount = $location_discount->dollar_amount ;
                                    }
                                }
                            } else {
                                $chargeAmount = $location_discount->dollar_amount ;
                            }
                        }
                    }
                }

                if ($specialPromo == 1) {
                    $this->insertOrderCharge($this->order->orderID, $chargeDesc, $chargeAmount);

                    //get the new order total
                    $ordTotal = $this->getOrderTotal($this->order->orderID);
                    //no more promotions on top of this one.
                }
                //END SPECIAL PROMOTION
                if ($noMore != 1) {
                    $method = $this->getOrderLocationType($this->order->orderID);
                    if (strtolower($method->locationType_id) == HOMEDELIVERYTYPE) {
                        $home_delivery_charge = $this->getHomeDeliveryCharge($this->order->business_id);
                        if (!empty($home_delivery_charge->home_delivery_charge_type) && !empty($home_delivery_charge->home_delivery_charge_amount)) {
                            //make this 10% dynamic, should be able to do amount or %
                            if ($home_delivery_charge->home_delivery_charge_type == 'percentage' && $home_delivery_charge->home_delivery_charge_amount > 0) {
                                $chargeAmount = ($home_delivery_charge->home_delivery_charge_amount / 100) * $ordTotal;
                                $this->insertOrderCharge($this->order->orderID, 'Home Delivery Surcharge (' . $home_delivery_charge->home_delivery_charge_amount . '%)', $chargeAmount);
                            }

                            if ($home_delivery_charge->home_delivery_charge_type == 'dollars' && $home_delivery_charge->home_delivery_charge_amount > 0) {
                                $chargeAmount = $home_delivery_charge->home_delivery_charge_amount;
                                $this->insertOrderCharge($this->order->orderID, 'Home Delivery Surcharge', $chargeAmount);
                            }
                        }
                    }

                    //get new order total
                    $ordTotal = $this->getOrderTotal($this->order->orderID);

                    //if this is a WF order, see if the customer is on a laundry plan and use the plan first
                    //first see if the wash and fold item is in the order items
                    $wfItem = $this->getWashAndFoldItems($this->order->orderID, $this->order->business_id);

                    $laundryPlansWithExpiredPrice = $this->getCustomerLaundryPlansWithExpiredPrice($this->order->customer_id);

                    $wf_min_data = $this->getWashAndFoldMinimum($this->order->business_id);
                    //determine what the order minimum is
                    if ($location->wf_min_type == 'pounds' && !empty($location->wf_min_amount)) {
                        $order_wf_min['type'] = $location->wf_min_type;
                        $order_wf_min['amount'] =  $location->wf_min_amount;
                        $order_wf_min['text'] = get_translation('location_discount_pounds', 'Discount Minimum', array(), 'Location minimum of %s applied.', $this->order->customer_id);
                    } elseif ($location->wf_min_type == 'dollars' && !empty($location->wf_min_amount)) {
                        $order_wf_min['type'] = $location->wf_min_type;
                        $order_wf_min['amount'] =  $location->wf_min_amount;
                        $order_wf_min['text'] =  get_translation('location_discount_dollars', 'Discount Minimum', array(), 'Location minimum of %s applied.', $this->order->customer_id);
                    } elseif ($wf_min_data->wf_min_type == 'pounds' && !empty($wf_min_data->wf_min_amount)) {
                        $order_wf_min['type'] = $wf_min_data->wf_min_type;
                        $order_wf_min['amount'] =  $wf_min_data->wf_min_amount;
                        $order_wf_min['text'] =  get_translation('discount_pounds', 'Discount Minimum', array(), 'Minimum of %s applied.', $this->order->customer_id);
                    } elseif ($wf_min_data->wf_min_type == 'dollars' && !empty($wf_min_data->wf_min_amount)) {
                        $order_wf_min['type'] = $wf_min_data->wf_min_type;
                        $order_wf_min['amount'] =  $wf_min_data->wf_min_amount;
                        $order_wf_min['text'] =  get_translation('discount_dollars', 'Discount Minimum', array(), 'Minimum of %s applied.', $this->order->customer_id);
                    }

                    foreach ($wfItem as $i => $w) {
                        $credit = $this->getWashAndFoldWeightDiscounts($this->order->customer_id);

                        //TODO TEST THIS LAUNDRY PLANS
                        foreach ($credit as $c) {

                            // skip if lbs are zero
                            if ($w->qty <= 0) {
                                continue;
                            }

                            //determine amount of credit
                            if ($w->qty <= $c->amount) {
                                $freeLbs = $w->qty;
                            } else {
                                $freeLbs = $c->amount;
                            }

                            $postDescription = '';

                            //decrease the number of pounds of credit left
                            //figure out the minimum
                            if ($order_wf_min['type'] == 'dollars') {
                                $postDescription = sprintf($order_wf_min['text'], format_money_symbol($this->order->business_id, '%.2n', $order_wf_min['amount']));

                                //divide dollars by weight
                                $minLbs = $order_wf_min['amount'] / $wfItem[0]->unitPrice;
                            } elseif ($order_wf_min['type'] == 'pounds') {
                                $minLbs = $order_wf_min['amount'];
                                $postDescription = sprintf($order_wf_min['text'], weight_system_format($order_wf_min['amount'], false, true));
                            }

                            if ($freeLbs <= $minLbs) {
                                //remove minLbs from their laundry plan
                                $amtApplied = $minLbs * -1;
                            } else {
                                $amtApplied = $freeLbs * -1;
                            }

                            //in case there are multiple discounts we need to make sure the qty takes into account discounts already applied
                            $w->qty = $w->qty - $c->amount;
                            $wfItem[$i] = $w;

                            //update to inactive
                            $update = $this->setCustomerDiscountToInactive($this->order->orderID, $amtApplied, $c->customerDiscountID);
                            //create a new discount for any remaing credit
                            $remainder = $c->amount + $amtApplied;

                            if ($remainder > 0) {
                                $desc = $cd->extendedDesc;
                                $insert1 = "insert into customerDiscount
                                    set customer_id = '$c->customer_id',
                                    amount = '$remainder',
                                    amountType = '$c->amountType',
                                    business_id = $business_id,
                                    frequency = '$c->frequency',
                                    description = ".$CI->db->escape($c->description).",
                                    extendedDesc = ".$CI->db->escape($desc).",
                                    active = 1,
                                    combine = 1,
                                    origCreateDate = '$c->origCreateDate',
                                    updated = now()";
                                if (!$c->expireDate) {
                                    $insert1 .= ", expireDate = NULL;";
                                } else {
                                    $insert1 .= ", expireDate = '$c->expireDate';";
                                }
                                $insert = $CI->db->query($insert1);
                            }

                            //get total remaining pounds
                            $leftOver = $this->getRemainingWeight($this->order->customer_id);

                            $chargeDescription = get_translation('charge_description', 'Discount Laundry Plan', array(
                                'remainder' => weight_system_format(number_format($leftOver[0]->totLbs, 0, '.', ''), false, true, true),
                            ), 'Laundry Plan (%remainder% Remaining).', $this->order->customer_id);

                            $chargeDescription.= ' ' . $postDescription;
                            $creditAmount = $freeLbs * $w->unitPrice * -1;

                            //create an order discount
                            $this->insertOrderCharge($this->order->orderID, $chargeDescription, $creditAmount, $c->customerDiscountID);
                        }

                        foreach ($laundryPlansWithExpiredPrice as $laundryPlan) {
                            // skip if lbs are zero
                            if ($w->qty <= 0) {
                                continue;
                            }

                            $freeLbs = $w->qty;
                            $price = $w->unitPrice - $laundryPlan->expired_price;

                            $chargeDescription = get_translation('charge_description_expired', 'Discount Laundry Plan', array(
                                'free_lbs' => weight_system_format(number_format($freeLbs, 2)),
                                'expired_price' => format_money_symbol($this->order->business_id, '%.2n', $laundryPlan->expired_price),
                            ), 'Laundry Plan Expired Price (%free_lbs% @%expired_price%).', $this->order->customer_id);

                            $creditAmount = $freeLbs * $price * -1;
                            //create an order discount
                            $this->insertOrderCharge($this->order->orderID, $chargeDescription, $creditAmount, 0);
                        }
                    }

                    //The following variable determines whether or not to try to apply discounts. If 'skip' is true, then we reached a coupon that is not combinable, and thus should not continue with the logic for applying any other discounts.
                    $skip = false;
                    $counter = 0; //The following variable keeps track of how many discounts we have applied so far.
                    //get new order total
                    $ordTotal = $this->getOrderTotal($this->order->orderID);


                    //if it is greater than 0 see if the customer has any discounts
                    //first apply any one-time discounts
                    $one_time_discounts = $this->getOneTimeDiscounts($this->order->customer_id);

                    if (!$skip) {
                        foreach ($one_time_discounts as $index => $one_time_discount) {
                            if ($one_time_discount->combine == 0 && $counter > 0) {
                                $skip = true;
                                break;
                            }
                            if ($ordTotal > 0) {
                                //special case coupons
                                if (strtoupper(substr($one_time_discount->couponCode, 0, 2)) =='SR') {
                                    if ($ordTotal >= 40) {
                                        $one_time_discount->amountType = 'dollars';
                                        $one_time_discount->amount = '20';
                                    } else {
                                        $one_time_discount->amountType = 'percent';
                                        $one_time_discount->amount = '25';
                                    }
                                }
                                if ($one_time_discount->amountType == 'dollars') {
                                    if ($one_time_discount->amount < $ordTotal) {
                                        $creditAmount = $one_time_discount->amount;
                                    } else {
                                        $creditAmount = $ordTotal;
                                    }
                                } else { //start else percent

                                    $creditAmount = ($one_time_discount->amount/100) * $ordTotal;
                                    if ($one_time_discount->maxAmt > 0) {
                                        $clampedCreditAmount = max(0, min($one_time_discount->maxAmt, $creditAmount));
                                        $creditAmount = $clampedCreditAmount;
                                    }

                                    //if there is a max, only apply that amount
                                    if (stristr($one_time_discount->couponCode, 'CR')) {
                                        $select9 = "select *
                                            from coupon
                                            where prefix = LEFT('$one_time_discount->couponCode',2)
                                            and business_id = ?;";
                                    } else {
                                        $select9 = "select *
                                            from coupon
                                            where concat(coupon.prefix, coupon.code) = '".$one_time_discount->couponCode."'
                                            and business_id = ?;";
                                    }
                                    $query9 = $CI->db->query($select9, array($business_id));
                                    $line9 = $query9->result();
                                    //NEED - if it is a duplicate discount, still make sure it doesn't exceed max.
                                    //get total of redit amount with this coupon code
                                    //SELECT sum(amountApplied) as amtApplied FROM prod.customerDiscount where customer_id = 6944 and couponCode = 'WS1212'
                                    //then add that to $creditAmount
                                    if ($line9[0]->maxAmt <> 0) {
                                        //TODO TEST THIS
                                        //see if this customer has already maxed out this coupon on other orders
                                        $line10 = $this->getCustomerDiscountMaxedOut($one_time_discount->customer_id, $one_time_discount->couponCode, $this->order->orderID);

                                        if ($line9[0]->maxAmt <= ($line10[0]->amtApplied + $creditAmount)) {
                                            $creditAmount = $line9[0]->maxAmt - $line10[0]->amtApplied;
                                        }
                                    }
                                } //end else percent

                                $creditAmount = $creditAmount * -1;

                                $ordTotal = $ordTotal + $creditAmount;
                                //insert into credit
                                $chargeTypeDesc = $one_time_discount->description;
                                if ($one_time_discount->couponCode) {
                                    $chargeTypeDesc .= " (".$one_time_discount->couponCode.")";
                                }

                                $this->insertOrderCharge($this->order->orderID, $chargeTypeDesc, $creditAmount, $one_time_discount->customerDiscountID);
                                //update to inactive
                                $this->setCustomerDiscountToInactive($this->order->orderID, $creditAmount, $one_time_discount->customerDiscountID);

                                if ($one_time_discount->amountType == 'percent') {
                                    $select5 = "select orders.*, firstName, lastName
                                            from orders
                                            join customer on customerID = orders.customer_id
                                            where orders.customer_id = ".$one_time_discount->customer_id."
                                            and orderStatusOption_id = 1";
                                    //die($select5);
                                    $select5 = $CI->db->query($select5);
                                    $rows = $select5->result();
                                    if (sizeof($rows) > 1) {
                                        $line22 = $rows;
                                        $desc = $one_time_discount->extendedDesc;
                                        $insert1 = "INSERT into customerDiscount
                                            set customer_id = ".$one_time_discount->customer_id.",
                                            business_id = $business_id,
                                            amount = '".$one_time_discount->amount."',
                                            amountType = '".$one_time_discount->amountType."',
                                            frequency = '".$one_time_discount->frequency."',
                                            description = ".$CI->db->escape($one_time_discount->description).",
                                            extendedDesc = ".$CI->db->escape($desc).",
                                            couponCode = '".$one_time_discount->couponCode."',
                                            active = 1,
                                            origCreateDate = now()";
                                        if (!$one_time_discount->expireDate) {
                                            $insert1 .= ", expireDate = NULL;";
                                        } else {
                                            $insert1 .= ", expireDate = '".$one_time_discount->expireDate."';";
                                        }
                                        $insert = $CI->db->query($insert1);

                                        queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($business_id, "customerSupport"), 'duplicate discount created', 'duplicate discount on order <a href="http://'.$_SERVER['HTTP_HOST'].'/delivery.php?order='.$this->order->orderID.'&receipt=1">'.$this->order->orderID.'</a> for '.$line22[0]->firstName.' '.$line22[0]->lastName, array(), null, $business_id);
                                    }
                                }
                            }
                            $counter++;
                            if ($one_time_discount->combine == 0) {
                                $skip = true;
                                break;
                            }
                        }
                    }

                    if (!$skip) {
                        //next do every order ers
                        $every_order_discounts = $this->getEveryOrderDiscounts($this->order->customer_id);

                        foreach ($every_order_discounts as $index => $every_order_discount) {
                            if ($every_order_discount->combine == 0 && $counter > 0) {
                                $skip = true;
                                break;
                            }

                            if ($ordTotal > 0) {
                                if ($every_order_discount->amountType == 'dollars') {
                                    if ($every_order_discount->amount < $ordTotal) {
                                        $creditAmount = $every_order_discount->amount;
                                    } else {
                                        $creditAmount = $ordTotal;
                                    }
                                } else {
                                    // Apply percentage discount
                                    $amount = ($every_order_discount->amount/100) * $ordTotal;
                                    // If there is a max amount set, clamp the total credited amount to the max amount value
                                    if ($every_order_discount->maxAmt > 0) {
                                        $clampedCreditAmount = max(0, min($every_order_discount->maxAmt, $amount));
                                        $creditAmount = $clampedCreditAmount;
                                    } else {
                                        $creditAmount = $amount;
                                    }
                                }

                                $creditAmount = $creditAmount * -1;
                                $ordTotal = $ordTotal + $creditAmount;
                                //add the parentheses if there is a coupon code
                                $post_description_string = '';
                                if (!empty($every_order_discount->couponCode)) {
                                    $post_description_string .= '(' . $every_order_discount->couponCode . ')';
                                }

                                $this->insertOrderCharge(
                                    $this->order->orderID,
                                                          $every_order_discount->description . " " . $post_description_string,
                                                          $creditAmount,
                                                          $every_order_discount->customerDiscountID
                                );
                            }

                            //testing of employer match process
                            if (stristr($every_order_discount->description, "match")) {
                                queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($business_id, 'customerSupport'), 'Employer Match Discount Just Created', 'An order with an employer match was just created.  Please make sure this gets charged to the right account. <a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/details/'.$this->order->orderID.'">'.$this->order->orderID.'</a>', array(), null, $business_id);
                            }
                            $counter++;
                            if ($every_order_discount->combine == 0) {
                                $skip = true;
                                break;
                            }
                        }
                    }


                    if (!$skip) {
                        //finally do until used up discounts
                        $until_used_up_customerDiscounts = $this->getUntilUsedUpDiscounts($this->order->customer_id);

                        foreach ($until_used_up_customerDiscounts as $index => $until_used_up_customerDiscount) {
                            if ($until_used_up_customerDiscount->combine == 0 && $counter > 0) {
                                $skip = true;
                                break;
                            }
                            if ($ordTotal > 0) {
                                $skipRest = 0;

                                $everything_plan = $this->getPlanForDiscount($this->order, $until_used_up_customerDiscount);

                                $product_plan = $this->getPlanForDiscount($this->order, $until_used_up_customerDiscount, "product_plan");

                                if ($until_used_up_customerDiscount->amountType == 'dollars') {
                                    if ($until_used_up_customerDiscount->amount < $ordTotal) {
                                        $creditAmount = $until_used_up_customerDiscount->amount;
                                    } else {
                                        $creditAmount = $ordTotal;
                                    }
                                    
                                    $remainder = $until_used_up_customerDiscount->amount - $creditAmount;
                                    $chargeDescription = get_translation('until_used_up_remaining', "Discount Until Used Up", array(
                                        'remainder' => format_money_symbol($this->order->business_id, '%.2n', $remainder),
                                    ), '(%remainder% Remaining)');
                                } elseif ($until_used_up_customerDiscount->amountType == 'percent') {
                                    $creditAmount = ($until_used_up_customerDiscount->amount/100) * $ordTotal;
                                    // If there is a max amount set, clamp the total credited amount to the max amount value
                                    if ($until_used_up_customerDiscount->maxAmt > 0) {
                                        $clampedCreditAmount = max(0, min($until_used_up_customerDiscount->maxAmt, $creditAmount));
                                        $creditAmount = $clampedCreditAmount;
                                    }
                                    $chargeDescription = $until_used_up_customerDiscount->description;
                                } elseif ($until_used_up_customerDiscount->amountType == 'product_plan') {
                                    if (!empty($product_plan->productsForDiscount)) {
                                        $creditAmount = 0;
                                        //apply discount to all products of the order that are included in the plan
                                        if ((int)$until_used_up_customerDiscount->amount >= $product_plan->productsForDiscount->quantity) {
                                            $creditAmount = $product_plan->productsForDiscount->amount;
                                            $remainder = (int)$until_used_up_customerDiscount->amount - $product_plan->productsForDiscount->quantity;
                                        //the order has more products included in the plan than the ones available in the discount..
                                        //We need to apply the discount only to a subgroup of the products in the order
                                        } elseif ((int)$until_used_up_customerDiscount->amount < $product_plan->productsForDiscount->quantity && (int)$until_used_up_customerDiscount->amount > 0) {
                                            $i = 0;
                                            foreach ($product_plan->productsForDiscount->products as $p) {
                                                for ($i; $i <= $p->qty; $i++) {
                                                    //discount has no more remaining products
                                                    //but the order still has products included in the product plan.
                                                    //We need to apply an expired discount percent to these products.
                                                    if ($i >= (int)$until_used_up_customerDiscount->amount) {
                                                        $creditAmount += ($product_plan->expired_discount_percent/100) * $p->unitPrice;
                                                    } else {
                                                        $creditAmount += $p->unitPrice;
                                                    }
                                                }
                                            }
                                            $remainder  = 0;
                                        } else {
                                            $creditAmount = $ordTotal;
                                        }

                                        $remainder = ($remainder <= 0)?0:$remainder;

                                        $chargeDescription = get_translation('until_used_up_remaining', "Discount for Product Plan", array(
                                            'remainder' => (int)$remainder,
                                        ), '(%remainder% Products Remaining)');
                                    } else {
                                        $skipRest = 1;
                                    }
                                } elseif ($until_used_up_customerDiscount->amountType == 'product_plan_percent') {
                                    if (!empty($product_plan->productsForDiscount)) {
                                        $creditAmount = 0;
                                        $creditAmount = ($until_used_up_customerDiscount->amount/100) * $product_plan->productsForDiscount->amount;
                                        $chargeDescription = $until_used_up_customerDiscount->description;
                                    } else {
                                        $skipRest = 1;
                                    }
                                } else {
                                    $skipRest = 1;
                                }

                                if ($skipRest == 0) {
                                    $creditAmount = $creditAmount * -1;
                                    $ordTotal = $ordTotal + $creditAmount;

                                    $this->message .= 'order total is: '.$ordTotal;

                                    $this->insertOrderCharge($this->order->orderID, $chargeDescription, $creditAmount, $until_used_up_customerDiscount->customerDiscountID);
                                    //update to inactive
                                    $this->setCustomerDiscountToInactive($this->order->orderID, $creditAmount, $until_used_up_customerDiscount->customerDiscountID);

                                    //create a new discount for any remaining credit
                                    $desc = $until_used_up_customerDiscount->extendedDesc;

                                    if ($remainder > 0) {
                                        $insert1 = "insert into customerDiscount
                                                set customer_id = ".$until_used_up_customerDiscount->customer_id.",
                                                amount = '".$remainder."',
                                                amountType = '".$until_used_up_customerDiscount->amountType."',
                                                frequency = '".$until_used_up_customerDiscount->frequency."',
                                                description = ".$CI->db->escape($until_used_up_customerDiscount->description).",
                                                business_id = $business_id,
                                                extendedDesc = ".$CI->db->escape($desc).",
                                                combine = '{$until_used_up_customerDiscount->combine}',
                                                active = 1,
                                                origCreateDate = '".$until_used_up_customerDiscount->origCreateDate."',
                                                updated = now()";
                                        if (!$until_used_up_customerDiscount->expireDate) {
                                            $insert1 .= ", expireDate = NULL;";
                                        } else {
                                            $insert1 .= ", expireDate = '".$until_used_up_customerDiscount->expireDate."';";
                                        }
                                        $insert = $CI->db->query($insert1);
                                    } elseif ($everything_plan) {
                                        //if amount < 0 insert a new discount using expired_discount_percent
                                        $insert1 = "insert into customerDiscount
                                                set customer_id = ".$until_used_up_customerDiscount->customer_id.",
                                                amount = '".$everything_plan->expired_discount_percent."',
                                                amountType = 'percent',
                                                frequency = 'every order',
                                                description = ".$CI->db->escape($until_used_up_customerDiscount->description).",
                                                business_id = $business_id,
                                                extendedDesc = ".$CI->db->escape($desc).",
                                                combine = '{$until_used_up_customerDiscount->combine}',
                                                active = 1,
                                                origCreateDate = '".$until_used_up_customerDiscount->origCreateDate."',
                                                updated = now()";
                                        if (!$until_used_up_customerDiscount->expireDate) {
                                            $insert1 .= ", expireDate = NULL;";
                                        } else {
                                            $insert1 .= ", expireDate = '".$until_used_up_customerDiscount->expireDate."';";
                                        }
                                        $insert = $CI->db->query($insert1);

                                        //deactivate other discounts related with this everything plan
                                        if ($insert) {
                                            $sql = "UPDATE customerDiscount SET active = 0 WHERE customer_id = ? and description LIKE '%Everything Plan%' AND active = 1 and amountType = 'dollars'";
                                            $CI->db->query($sql, array($this->order->customer_id));
                                        }
                                    } elseif ($product_plan) {
                                        //if amount <= 0 insert a new discount using expired_discount_percent
                                        $insert1 = "insert into customerDiscount
                                                set customer_id = ".$until_used_up_customerDiscount->customer_id.",
                                                amount = '".$product_plan->expired_discount_percent."',
                                                amountType = 'product_plan_percent',
                                                frequency = 'every order',
                                                description = ".$CI->db->escape($until_used_up_customerDiscount->description).",
                                                business_id = $business_id,
                                                extendedDesc = ".$CI->db->escape($desc).",
                                                combine = '{$until_used_up_customerDiscount->combine}',
                                                active = 1,
                                                origCreateDate = '".$until_used_up_customerDiscount->origCreateDate."',
                                                updated = now()";
                                        if (!$until_used_up_customerDiscount->expireDate) {
                                            $insert1 .= ", expireDate = NULL;";
                                        } else {
                                            $insert1 .= ", expireDate = '".$until_used_up_customerDiscount->expireDate."';";
                                        }
                                        $insert = $CI->db->query($insert1);

                                        //deactivate other discounts related with this product plan
                                        if ($insert) {
                                            $sql = "UPDATE customerDiscount SET active = 0 WHERE customer_id = ? and description LIKE '%Prodcut Plan%' AND active = 1 and amountType = 'product_plan'";
                                            $CI->db->query($sql, array($this->order->customer_id));
                                        }
                                    }
                                }
                            }

                            $counter++;
                            if ($until_used_up_customerDiscount->combine == 0) {
                                $skip = true;
                                break;
                            }
                        }
                    }

                    /* Apply "all customers" coupons.
                    * These coupons need to be applied to all customers
                    */
                    $discounts = $this->getAllCustomersDiscounts($this->order->business_id);

                    foreach ($discounts as $index => $discount) {
                        if ($ordTotal > 0) {
                            if ($discount->amountType == 'dollars') {
                                if ($discount->amount < $ordTotal) {
                                    $creditAmount = $discount->amount;
                                } else {
                                    $creditAmount = $ordTotal;
                                }
                            } else {
                                // Apply percentage discount
                                $amount = ($discount->amount/100) * $ordTotal;
                                // If there is a max amount set, clamp the total credited amount to the max amount value
                                if ($discount->maxAmt > 0) {
                                    $clampedCreditAmount = max(0, min($discount->maxAmt, $amount));
                                    $creditAmount = $clampedCreditAmount;
                                } else {
                                    $creditAmount = $amount;
                                }
                            }

                            $creditAmount = $creditAmount * -1;
                            $ordTotal = $ordTotal + $creditAmount;
                            //add the parentheses if there is a coupon code
                            $post_description_string = '';
                            if (!empty($discount->couponCode)) {
                                $post_description_string .= '(' . $discount->couponCode . ')';
                            }

                            $this->insertOrderCharge(
                                $this->order->orderID,
                              $discount->description . " " . $post_description_string,
                              $creditAmount,
                              $discount->couponID
                            );
                        }
                        $counter++;
                    }
                    /*-----*/
                } else {
                    return array('status'=>'success', "message"=>"No Customer Discounts");
                }
            }//end ordTotal > 0
            else { //orderTotal is 0

                return array('status'=>'success', "message"=>"end1");
            }
        } else {
            return array('status'=>'success', "message"=>"end2");
        }
        //end apply_discounts
        return array('status'=>'success', "message"=>"Order was finalized");
    }

    public function getCustomerLaundryPlansWithExpiredPrice($customer_id, $type = "laundry_plan")
    {
        return $this->_ci->db->query("SELECT * FROM laundryPlan where customer_id = ? AND active = 1 AND expired_price > 0 and type='".$type."'", array($customer_id))->result();
    }

    //Check if this discount is part of an everything/product plan
    private function getPlanForDiscount($order, $discount, $type="everything_plan")
    {
        if (empty($order)) {
            return false;
        }

        $customer_id = $order->customer_id;
        if (empty($customer_id)) {
            return false;
        }

        //The user can only have one type of plan. So, we choose the first one.
        $sql = "SELECT * FROM laundryPlan WHERE customer_id = ? AND type = ? AND active = 1 ORDER BY laundryPlanID DESC";

        $query = $this->_ci->db->query($sql, array($customer_id, $type));
        $plan = $query->result();

        $plan = empty($plan)?false:$plan[0];

        if ($plan) {
            $description = "Everything Plan";
            if ($type="product_plan") {
                $description = "Product Plan";

                $product_ids = json_decode($plan->products);
                $products = $this->getOrderItemsByProductIds($order, $product_ids);
                $plan->productsForDiscount = $products;
            }

            //discount was created by an everything/product plan
            if (stripos($discount->description, $description) !== false) {
                return $plan;
            }
        }

        return false;
    }

    private function getOrderItemsByProductIds($order, $product_ids = array())
    {
        $CI = get_instance();
        $sql = "SELECT 
                    qty, unitPrice, qty*unitPrice as total
                FROM orderItem
                    LEFT JOIN product_process ON product_processID = product_process_id
                    LEFT JOIN product ON (productID = product_id)
                    LEFT JOIN productCategory ON (productCategoryID = productCategory_id)
                WHERE 
                    order_id = ? ";
        //-1 means all producs
        if (!in_array("-1", $product_ids)) {
            $sql .= "AND product_processID IN ('".implode("','", $product_ids)."')";
        }

        $query = $CI->db->query(
            $sql,
            array($order->orderID)
        );
        $rows = $query->result();

        if (!empty($rows)) {
            $result = new stdClass;
            $result->products = $rows;
            $result->quantity = 0;
            $result->amount = 0;
            foreach ($rows as $r) {
                $result->quantity += $r->qty;
                $result->amount += $r->total;
            }
        }

        return empty($rows)?false:$result;
    }
}
