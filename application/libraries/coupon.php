<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************
RESERVED CODES
- CR: used for customer referrals with Locker Loot program
- SR: used for customer referrals with Locker Loot program
- GF: gift cards
*/

class Coupon
{
    public $CI = '';
    public $couponCode = '';
    public $customer_id = '';
    public $business_id;

    public function __construct()
    {
        $this->CI = get_instance();
    }

    public function __set($var, $value)
    {
        $this->$var = $value;
    }

    public function __get($var)
    {
        return $this->$var;
    }



    /**
        * @deprecated This function is deprecated in for the apply_coupon functino in the coupon model.
        * applyCoupon method calls applyCouponCust();
        *
        * In the constructor of this class, I have a magic setter if yuo want to set the customer_id, Business_id and couponCode before calling this
        *
        * @param string $code
        * @param int $customer_id
        * @param int $business_id
        */
    public function applyCoupon($couponCode, $customer_id, $business_id)
    {

        $this->customer_id = $customer_id;
        $this->business_id = $business_id;
        $this->couponCode = $couponCode;

        if (!$this->customer_id) {
            throw new \InvalidArgumentException("You must set customer ID");
        }

        if (!$this->couponCode) {
            throw new \InvalidArgumentException("You must set couponCode");
        }

        return $this->applyCouponCust();
    }


    /**
     *
     * @return string
     * @throws Exception
     */
    public function applyCouponCust()
    {
    if (!$this->customer_id) {
            throw new \Exception("You must set customer ID");
        }

        if (!$this->couponCode) {
            throw new \Exception("You must set couponCode");
        }
        $business = new \App\Libraries\DroplockerObjects\Business($this->business_id, false);


    if ($this->couponCode) {
            $CI = $this->CI;
            $couponCode = $this->couponCode;

            //trim out any special characters
            $couponCode = preg_replace("/[^a-zA-Z0-9\s]/", "", $couponCode);

            /* Commented out so we can use numeric prefixes in coupons
            //if no prefix is added, try it as a gift card
            if (is_numeric(substr($couponCode, 0 , 2))) {
                $couponCode = 'GF'.$couponCode;
            }
            */

            //always check to make sure that this customer has not used this coupon before
            $CI->load->model('customerdiscount_model');
            $CI->customerdiscount_model->customer_id = $this->customer_id;
            $CI->customerdiscount_model->business_id = $this->business_id;
            $CI->customerdiscount_model->couponCode = $couponCode;
            $discount = $CI->customerdiscount_model->get();
            if ($discount) {
                return array('status'=>'fail', 'error'=>get_translation("already_used_coupon_error","business_account/programs_discounts", array(), "Sorry, you have already entered this coupon and it is pending in your account", $this->customer_id));
            }

            //check to see if this is a valid coupon code
            //TODO: This returns a coupon object... try to see if we can use this object instead of
            // doing more queries

            $prefix = substr($couponCode, 0,2);
            if (strcasecmp('CR', $prefix) == 0 || strcasecmp('SR', $prefix) == 0) {
                $couponSql = "select * from coupon where business_id = {$this->business_id} AND prefix = LEFT('$couponCode',2)";

                //check to make sure the customer exists
                $uid = substr(trim($couponCode), 2);
                $sql = "SELECT * FROM customer WHERE customerID = '$uid' and business_id = $this->business_id";
                $customer =  $CI->db->query($sql)->result();

                //don't let a customer refer themselves
                if ($uid == $this->customer_id) {
                    return array('status'=>'fail', 'error'=>get_translation("self_referral_error","business_account/programs_discounts", array(), "Sorry, you can not refer yourself.", $this->customer_id));
                } else {
                    // no customer was found with this id
                    if (!$customer) {
                        return array('status'=>'fail', 'error'=>get_translation("invalid_promo_code", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, the promotional code you entered is invalid.  If you think this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
                    } else {
                        // add referred by for lockerloot
                        $sql = "UPDATE customer set referredBy = '$uid' WHERE customerID = $this->customer_id";
                        $CI->db->query($sql);
                    }
                }
            } else {
                $couponSql = "select * from coupon where business_id = {$this->business_id} AND concat(coupon.prefix, coupon.code) = '$couponCode'";
            }

            // Add coupon endDate validation
            $couponSql .= " AND (endDate >= '" . convert_to_gmt_aprax() . "'
                                    OR endDate IS NULL 
                                    OR endDate = '0000-00-00 00:00:00')";

            $coupons = query($couponSql);

            if (!$coupons) {
                return array('status'=>'fail', 'error'=>get_translation("invalid_promo_code", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, the promotional code you entered is invalid.  If you think this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
            }
            $expireDate = $coupons[0]->endDate;

            if (sizeof($coupons) == 1) {  //if only one row was returned, see what the code is
                if ($coupons[0]->redeemed == 1) {
                    return array('status'=>'fail', 'error'=>get_translation("certificate_aleady_redeemed", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, this gift certificate has already been redeemed. If you think this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
                }

                if ($coupons[0]->firstTime == 1) {
                    //If this only applies to the first order make sure they don't have any closed orders already
                    //don't count the orders for payment authorization.
                    $sql = "SELECT * from orders
                                            WHERE business_id = {$this->business_id} AND  customer_id = $this->customer_id
                                            AND orderStatusOption_id = 10
                                            AND bag_id <> 0";
                    $orders = query($sql);

                    if ($orders) {
                        return array('status'=>'fail', 'error'=>get_translation("invalid_promo_code_first_time", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, you can not use a first time user promotional code. You already have completed orders. If you think this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
                    }

                    //If this only applies to the first order make sure they don't have another first time code in their account
                    $sql = "SELECT couponCode, firstTime
                            FROM customerDiscount
                            LEFT JOIN coupon on concat(coupon.prefix, coupon.code) = customerDiscount.couponCode
                            WHERE customerDiscount.business_id = {$this->business_id}
                            AND active = 1
                            AND  customerDiscount.customer_id = $this->customer_id;";
                    $customerDiscount = query($sql);

                    if ($customerDiscount) {
                        foreach ($customerDiscount as $d) {

                            if ($d->firstTime == 1) {
                                return array('status'=>'fail', 'error'=>get_translation("alredy_entered_promo_code", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, you have already entered a first time user promotional code.  If this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.'));
                            }

                            if (stristr(strtoupper($d->couponCode), 'CR') or stristr(strtoupper($d->couponCode), 'SR')) {
                                return array('status'=>'fail', 'error'=>get_translation("alredy_entered_promo_code", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, you have already entered a first time user promotional code.  If this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.'));
                            }
                        }
                    }

                }

                if (strcasecmp($coupons[0]->prefix.$coupons[0]->code,trim($couponCode)) == 0 ) {
                    $couponId = $coupons[0]->couponID;
                }
                //this is a wildcard coupon and further querying needs to be done
                else if ($coupons[0]->code == '*') {
                    //check to make sure this customer has not used this wildcard code before
                    $sql = "select *
                        FROM customerDiscount
                        WHERE business_id = {$this->business_id}
                        AND customer_id = {$this->customer_id}
                        AND left(couponCode,2) = left('$couponCode',2);";
                    $result = query($sql);

                    if ($result) { //this customer has used this code before

                        return array('status'=>'fail', 'error'=>get_translation("already_pending", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, you have already entered this coupon and it is pending in your account. Please, contact customer service at <a href="mailto:%email%">%email%</a> if you think there is an issue.', $this->customer_id));
                    }

                    $couponId = $coupons[0]->couponID;
                } else {
                    return array('status'=>'fail', 'error'=>get_translation("invalid_promo_code", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, the promotional code you entered is invalid.  If you think this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
                }
            } else {
                //run another query to find the exact coupon match
                $sql = "select *
                                from coupon
                                where business_id = {$this->business_id} AND '$couponCode' = concat(prefix, code);";
                $coupon = query($sql);
                $coupon = $coupon[0];
                if ($coupon) {
                    $couponId = $coupon->couponID;
                } else {
                    return array('status'=>'fail', 'error'=>get_translation("invalid_promo_code", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, the promotional code you entered is invalid.  If you think this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
                }
            }

            //now we have a coupon ID.  Get the coupon details.
            $sql = "select *
                from coupon
                where business_id = {$this->business_id} AND couponID = $couponId;";
            $query = $CI->db->query($sql);
            $couponDetails = $query->result();
            $couponDetails = $couponDetails[0];

            //Make sure this coupon has started and that it hasn't ended.
            if (strtotime($couponDetails->startDate) != '') {
                if (strtotime($couponDetails->startDate) > strtotime(date("m/d/y"))) {
                    return array('status'=>'fail', 'error'=>get_translation("not_started","business_account/programs_discounts", array(), "Sorry, the promotional code you entered has not started yet.", $this->customer_id));
                }
                if (isset($couponDetails->endDate) and strtotime($couponDetails->endDate) < strtotime(date("Y-M-D"))) {
                    return array('status'=>'fail', 'error'=>get_translation("already_ended","business_account/programs_discounts", array(), "Sorry, the promotional code you entered has already ended.", $this->customer_id));
                }
            }

            //TODO remove the special coupons and create a function to allow each business to add their own coupons
            //if this was a Groupons Gift Card, do not let the same customer use multiple gift cards
            if ($couponDetails->description == '$50 Groupons Gift Certificate' or $couponDetails->description == '$50 Living Social Gift Certificate') {
                $sql = "select * from customerDiscount where description = '$couponDetails->description' and customer_id = '$this->customer_id'";
                $query = $CI->db->query($sql);
                $res = $query->result();
                if ($res) {
                        return array('status'=>'fail', 'error'=>"Sorry, the Groupons promotion is limited to one coupon per customer.  However, we encourage you to give this Groupon to one of your friends as a gift.");
                }
                //also, deactivate any other active discounts
                //$update1 = "update customerDiscount set active = 0 where customer_id = '$customer->id'";
                //$update = $GLOBALS['db']->query($update1);
            }
            //if this was a Gilt City Coupons give them 20 lbs of WF too
            if ($couponDetails->description == 'Gilt City $20 and 20 Lbs') {
                //manually add the WF
                $newCode = $couponCode . 'WF';
                $sql = "insert
                    into customerDiscount
                    (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode)
                    values ('$customer->id','20','pounds', 'until used up', '$couponDetails->description', '$couponDetails->description', '1', now(), now(), '$newCode')";
                $query = $CI->db->query($sql);
            }

            //if this was a SFWeekly Coupon give them 30 lbs of WF too
            if ($couponDetails->description == 'SF Weekly $15 and 30 Lbs') {
                //manually add the WF
                $newCode = $couponCode . 'WF';
                $sql = "insert
                    into customerDiscount
                    (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, business_id)
                    values ('$customer->id','30','pounds', 'until used up', '$couponDetails->description', '$couponDetails->description', '1', now(), now(), '$newCode', $this->business_id)";
                $query = $CI->db->query($sql);
            }

            //load up the details of this coupon
            $amount = $couponDetails->amount;
            $amountType = $couponDetails->amountType;
            $frequency = $couponDetails->frequency;
            $description = "{$couponDetails->description} - {$couponCode}";
            $extendedDesc = $couponDetails->extendedDesc;
            $maxAmount = $couponDetails->maxAmt;
            $process_id = $couponDetails->process_id;

            $combine = isset($couponDetails->combine)?$couponDetails->combine:0;

            if (strtoupper($couponCode) == 'FP4589') {
                $expireDate = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")+31, date("Y")));

            }


            //if no expire date, set it to null
            if (!$expireDate || $expireDate == ("0000-00-00 00:00:00")) {
                $expireDate = null;
            }


            // Mow find out if this customer has an order that is not closed or just picked up
            // (won't have any items on it) that has not been captured yet.
            // NOTE: This will apply discount to the most recent orders first (order by orderID DESC)
            $sql = "select *
                from `orders`
                where business_id = ($this->business_id) AND customer_id = $this->customer_id
                and orderStatusOption_id not in (1, 10)
                and orderPaymentStatusOption_id = 1
                order by orderID DESC;";
            $order = $CI->db->query($sql)->result();

            $discount_applied = array();

            // HACK ALERT----
            // This is poor structure below, so we're doing this quick loop to pull out orders that fail
            // the > 0 conditional yet should still be saved for future use
            // screen these out before hitting the bad conditional below
            $CI->load->model('orderitem_model');
            foreach ($order as $k => $ord) {    //make sure that these have net totals greater than zero, if not unset it
                $ord_total_array = $CI->orderitem_model->get_item_total( $ord->orderID );
                if ($ord_total_array['total'] == 0) {
                    unset($order[$k]);
                }
            }

            // Make sure the coupon is not for items
            if ($order and stristr($amountType, 'item') == FALSE and $amountType !== 'pounds') {   //this is a terrible conditional
                // need to set the customerDiscount remaining amount before
                // trying to apply to each order.
                $customer_discount_remaining_amount = $amount;

                foreach ($order as $o) {

                    // customer discount is 0
                    if ($customer_discount_remaining_amount <= 0) {
                        break; // nothing left in the coupon so get out of loop
                    }

                    if ($combine != 1) {
                        $sql = "select * from customerDiscount where business_id = ($this->business_id) AND order_id = $o->orderID";
                        $query = $CI->db->query($sql);
                        $res = $query->result();
                        if ($res) {
                            //if this coupon can't be combined, put it in their account for future use.
                            return array('status'=>'fail', 'error'=>get_translation("not_combinable", "business_account/programs_discounts", array("email" => $business->email), 'Sorry, this order already has a discount applied to it and the promotional code you entered may not be combined with other offers.  If you feel this is an error, please contact customer service at <a href="mailto:%email%">%email%</a>.', $this->customer_id));
                        }
                    }

                    $total_array = $CI->orderitem_model->get_item_total($o->orderID);
                    $orderTotal = ($total_array['total'])?$total_array['total']:0;

                    if ($orderTotal > 0) {

                    //if the credit is a percent, apply it to all open orders
                        if ($amountType == 'percent') {
                            $creditAmount = ($amount/100) * $orderTotal;
                            //if there is a max, only apply that amount
                            if ($maxAmount < $creditAmount and $maxAmount <> 0) {
                                $creditAmount = $maxAmount;
                            }

                            $creditAmount = $creditAmount * -1;
                            $creditAmount = number_format($creditAmount, 2, '.', '');

                            $sql = "INSERT into orderCharge set order_id = '$o->orderID', chargeType = '$description', chargeAmount = '$creditAmount', updated = now();";
                            $query = $CI->db->query($sql);

                            //create a transaction in discount table so that this coupon can not be reapplied
                            $sql = "insert
                                            into customerDiscount
                                            (customer_id, amount, amountType, maxAmt, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, order_id, amountApplied, business_id, combine)
                                            values ('$this->customer_id','$amount','$amountType', '$maxAmount', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '0', now(), now(), '$couponCode', '$o->orderID', '$creditAmount', $this->business_id, '$combine')";
                            $query = $CI->db->query($sql);
                            $insert_id = $CI->db->insert_id();

                            $discount_applied[] = array('order_id'=>$o->orderID, 'credit_amount'=>$creditAmount);
                        }

                        //if the credit is an amount, apply it until the credit is done
                        //TODO fix this bug:
                        // customer has 2 orders with no items. one order is for a gift card.
                        // this results in bad message
                        if ($amountType == 'dollars') {
                            if ($customer_discount_remaining_amount < $orderTotal) {
                                $creditAmount = $customer_discount_remaining_amount;
                            } else {
                                $creditAmount = $orderTotal;
                                $remainder = $customer_discount_remaining_amount - $orderTotal;
                            }

                            $creditAmount = $creditAmount * -1;
                            $creditAmount = number_format($creditAmount, 2, '.', '');
                            $sql = "INSERT into orderCharge set order_id = '$o->orderID', chargeType = '$description', chargeAmount = '$creditAmount', updated = now();";
                            $query = $CI->db->query($sql);


                            $sql = "insert
                            into customerDiscount
                            (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, order_id, amountApplied, business_id, combine)
                            values ('$this->customer_id','$customer_discount_remaining_amount','$amountType', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '0', now(), now(), '$couponCode', '$o->orderID', '$creditAmount', $this->business_id, '$combine')";
                            $query = $CI->db->query($sql);

                            $insert_id = $CI->db->insert_id();

                            $discount_applied[] = array('order_id'=>$o->orderID, 'credit_amount'=>$creditAmount);

                        }

                    } // end if orderTotal > 0

                    // if this is a one time coupon sent the $customer_discount_remaining_amount to 0
                    if ($frequency == "one time") {
                        $customer_discount_remaining_amount = 0;
                        break; // might as well stop the loop here and continue
                    }

                    if ($remainder > 0) {
                        $customer_discount_remaining_amount = $remainder;
                    } else {
                        $customer_discount_remaining_amount = 0;
                    }

                    $remainder = number_format($remainder, 2, '.', '');

                } // end of order = $o foreach


                //if this is an until used up coupon, put the remainder in as a credit on future orders
                if ($frequency == 'until used up') {
                    $sql = "insert
                            into customerDiscount
                            (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, combine)
                            values ('$this->customer_id','$remainder','$amountType', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '1', now(), now(), '$couponCode', '$combine')";
                    $query = $CI->db->query($sql);
                    $insert_id = $CI->db->insert_id();
                }

                if ($frequency == 'every order') {

                    $amount = number_format($amount, 2, '.', '');

                    //if this is an every order coupon, create a credit for future orders.
                    //create a transaction in discount table so this coupon will be applied to the next order
                    if ($expireDate) {
                        $sql = "insert
                                into customerDiscount
                                (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, expireDate, combine)
                                values ('$this->customer_id','$amount','$amountType', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '1', now(), now(), '$couponCode', '$expireDate', '$combine')";
                    } else {
                        $sql = "insert
                                into customerDiscount
                                (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, combine)
                                values ('$this->customer_id','$amount','$amountType', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '1', now(), now(), '$couponCode', '$combine')";
                    }
                    $query = $CI->db->query($sql);
                    $insert_id = $CI->db->insert_id();
                    $array = array('status'=>'success', 'message' => get_translation("successful_coupon_redemption_message", "business_account/programs_discounts", array("description" => $description), 'A credit for %description% has been placed in your account and will automatically be applied to your next order.'));

                }
            } else {

                $amount = number_format($amount, 2, '.', '');

                //if there are no open orders, create a credit for future orders.
                //create a transaction in discount table so this coupon will be applied to the next order
                if ($expireDate) {
                    $sql = "insert
                            into customerDiscount
                            (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, expireDate, business_id, combine, process_id)
                            values ('$this->customer_id','$amount','$amountType', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '1', now(), now(), '$couponCode', '$expireDate', $this->business_id, '$combine', '$process_id')";
                } else {
                    $sql = "insert
                            into customerDiscount
                            (customer_id, amount, amountType, frequency, description, extendedDesc, active, origCreateDate, updated, couponCode, business_id, combine, process_id)
                            values ('$this->customer_id','$amount','$amountType', '$frequency', '".mysql_real_escape_string($description)."', '".mysql_real_escape_string($extendedDesc)."', '1', now(), now(), '$couponCode', $this->business_id, '$combine', '$process_id')";
                }
                $query = $CI->db->query($sql);
                $insert_id = $CI->db->insert_id();

            }


            //if this was a gift card, make it inactive so that it can not be used again.
            if (stristr($couponCode, 'GF')) {
                //set coupon inactive.
                $sql = "update coupon set redeemed = 1 where concat(prefix, code) = '$couponCode'";
                $update = $CI->db->query($sql);
            }

            if ($discount_applied) {
                $discount_message = "<ul>";
                foreach ($discount_applied as $da) {
                        $discount_message .= "<li>Credit applied to order: ".$da['order_id']." for ".format_money_symbol($this->business_id, '%.2n', $da['credit_amount'])."</li>";
                }
                $discount_message .= "</ul>";
            }
            
            if ($amountType=='dollars') {
                $amount = format_money_symbol($this->business_id, '%.2n', $amount);
            } elseif ($amountType == 'pounds') {
                $amount = weight_system_format($amount);
            } else {
                $amount = number_format($amount, 0)."%";
            }

            $message = (sizeof($discount_applied)>0) ?
				get_translation("successful_coupon_redemption_order_applied_message", "business_account/programs_discounts", array("amount" => $amount, "discount_message" => $discount_message), "A credit for %amount% has been placed in your account.<br> %discount_message%", $this->customer_id)
				: get_translation("successful_coupon_redemption_message", "business_account/programs_discounts", array("description" => $description), 'A credit for %description% has been placed in your account and will automatically be applied to your next order.', $this->customer_id);

            $array = array('status'=>'success', 'message'=>$message);
    }

    return $array;
}

}
