<?php

use App\Libraries\DroplockerObjects\Customer;

/**
 * Formats and sends and emails to customers for actions
 */
class Email_Actions
{
    public function __construct()
    {
        // Removing the following lines makes all cronjobs stop working
        $CI = get_instance();
        $CI->load->model('emailactions_model');
    }

    /**
     * Sends email to customers
     *
     * Expects that the 'options' parameter contain a customer ID and a business ID.
     * business_id int
     * customer_id int
     * do_action string The email action name. e.g. 'ready_for_pickup'
     *
     * Can take at least the following optional option keys
     * order_id int
     * claim_id int
     */
    public static function send_transaction_emails($options)
    {

        if (! array_key_exists('business_id', $options)) {
            throw new \InvalidArgumentException("'business_id' must be defined in the options array.");
        }

        if (! array_key_exists("customer_id", $options)) {
            throw new \InvalidArgumentException("'customer_id' must be defined in the options array");
        }

        $business_id = $options['business_id'];
        $customer_id = $options['customer_id'];

        if (! is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }

        $action = $options['do_action'];
        $macro_options = $options;

        $CI = get_instance();
        $CI->load->model("customer_model");

        $customer = $CI->customer_model->get_by_primary_key($customer_id);
        if (empty($customer)) {
            return false;
        }

        // use custom template if passed in the option array
        if (empty($options['emailTemplate'])) {
            $emailTemplate = get_emailTemplate_by_business_language_for_customer($action, $customer_id);
        } else {
            $emailTemplate = $options['emailTemplate'];
            unset($options['emailTemplate']);
        }

        // Exit out of this function if there is no email tempalte found for the above criteria.
        $CI->load->model('emailactions_model');
        if (!$emailTemplate || $CI->emailactions_model->is_blank($emailTemplate)) {
            return;
        }

        $data = get_macros($macro_options);

        self::send_sms($customer, $emailTemplate->smsText, $options['business_id'], $data);
        self::send_email($customer, $emailTemplate, $options['business_id'], $data);

        // The following query fetches a single defined admin alert for the action.
        // This hackery is to workaround the new internationalization functionality.
        $sql = "SELECT emailTemplate.adminText, emailTemplate.adminSubject, emailTemplate.adminRecipients
                FROM emailTemplate
                INNER JOIN emailActions ON (emailActionID = emailAction_id)
                WHERE emailActions.action = ?
                AND emailTemplate.business_id = ?
                AND adminText IS NOT NULL
                AND adminText != ''";

        $adminAlert_emailTemplate = $CI->db->query($sql, array(
            $action,
            $business_id
        ))->row();

        if ($adminAlert_emailTemplate) {
            Email_Actions::send_adminAlert($adminAlert_emailTemplate, $options['business_id'], $data);
        }
    }

    /**
     * Sends an email to business admin
     *
     * If the adminRecipients is set in the database, it will come in the form of
     * array(
     *     'system' => array(),
     *     'custom' => $string
     * );
     *
     * The database stores the array as a serialized string
     * The system emails are set in the business settings and stored in the business_meta table.
     * The custom emails are any email the business owner wants to add to receive the admin notification
     *
     * emailTemplate - Must have the properties:
     *  - 'adminText'
     *  - 'adminSubject'
     *  - 'adminRecipients'
     *
     * $data contains the replacement for macros
     *
     * @param stdClass emailTemplate
     * @param int business_id
     * @param array data
     * @throws Exception if there is no customer support email set in the business_meta table
     */
    public static function send_adminAlert($emailTemplate, $business_id, $data)
    {
        if ($emailTemplate->adminSubject == '' || $emailTemplate->adminText == '') {
            return;
        }

        $subject    = email_replace($emailTemplate->adminSubject, $data);
        $body       = email_replace($emailTemplate->adminText, $data);
        $allEmails  = array();

        if (!empty($emailTemplate->adminRecipients)) {
            $recipients = unserialize($emailTemplate->adminRecipients);

            $system = array();
            if (!empty($recipients['system'])) {
                $system = $recipients['system'];
            }

            $customEmails = array();
            if (!empty($recipients['custom'])) {
                $customEmails = explode(",", $recipients['custom']);
            }

            $allEmails = array_merge($system, $customEmails);

        }

        if (empty($allEmails)) {
            $allEmails[] = get_business_meta($business_id, 'customerSupport');

            if (empty($allEmails[0])) {
                throw new Exception("business emails are not configured properly. Missing customer support email address");
            }
        }

        foreach ($allEmails as $to) {
            if (! empty($to)) {
                //in case I have more than one address
                $emailsArray = explode(',', $to);
                foreach ($emailsArray as $splittedTo){
                    queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $splittedTo, $subject, $body, array(), null, $business_id);
                }
            }
        }
    }

    /**
     * Sends a gift card notification email to the recipient of a gift card
     *
     * @param int $customerID
     *            The customer who purchased the gift card for the recipient
     * @param string $recipientEmail
     * @param int $couponID
     *            The primary key of the gift card coupon
     * @throws Exception
     * @throws \InvalidArgumentException
     * @throws LogicException
     */
    public static function send_giftCard_recipient_email($customerID, $recipientEmail, $couponID)
    {
        $CI = get_instance();
        $CI->load->helper('email');
        $CI->load->model("customer_model");
        $CI->load->model("coupon_model");

        $customer = $CI->customer_model->get_by_primary_key($customerID);
        if (empty($customer)) {
            throw new Exception("sending customer ID '$customerID' does not exist");
        }

        if (! $CI->coupon_model->does_exist($couponID)) {
            throw new Exception("Gift card coupon ID '$couponID' does not exist");
        }

        if (! valid_email($recipientEmail)) {
            throw new \InvalidArgumentException("'recipientEmail' must be a valid email");
        }

        $emailTemplate = get_emailTemplate_by_business_language_for_customer("buy_giftCard_recipient", $customer->customerID);

        if (empty($emailTemplate->emailSubject)) {
            throw new LogicException("There must be a subject defined for the giftCard recipient email template");
        }

        if (empty($emailTemplate->emailText)) {
            throw new LogicException("There must be a body defined for the gift card recipient email template.");
        }

        $macro_values = get_macros(array(
            "business_id" => $customer->business_id,
            "customer_id" => $customer->customerID,
            "giftCard_id" => $couponID
        ));

        $subject = email_replace($emailTemplate->emailSubject, $macro_values);
        $body = email_replace($emailTemplate->emailText, $macro_values);

        $from_email_name = get_business_meta($customer->business_id, 'from_email_name');
        $customer_support = get_business_meta($customer->business_id, 'customerSupport');
        // if we have a comma in the from field, lets just pull the first email
        $customer_support = current(explode(',', $customer_support));

        queueEmail($customer_support, $from_email_name, $recipientEmail, $subject, $body, array(), null, $customer->business_id);
    }

    /**
     * Formats and sends an email to the customer
     *
     * $emailTemplate must have the following properties
     * - subject
     * - body
     *
     * $data contains the replacement for macros
     *
     * @param stdClass $customer
     * @param stdClass $emailTemplate
     * @param int $business_id
     * @param array $data
     */
    public static function send_email($customer, $emailTemplate, $business_id, $data)
    {
        if (empty($customer)) {
            throw new \InvalidArgumentException("'customer' must be a codeigniter query result of a customer entity");
        }

        if (empty($emailTemplate->emailSubject) || empty($emailTemplate->emailText)) {
            return;
        }

        //need  send the business_id
        $data['business_id'] = $business_id;

        $subject = email_replace($emailTemplate->emailSubject, $data);
        $body = email_replace($emailTemplate->emailText, $data);

        $from_email_name = get_business_meta($customer->business_id, 'from_email_name');
        $customer_support = get_business_meta($customer->business_id, 'customerSupport');
        // if we have a comma in the from field, lets just pull the first email
        $customer_support = current(explode(',', $customer_support));


        queueEmail($customer_support, $from_email_name, $customer->email, $subject, $body, array(), null, $business_id);
    }

    /**
     * Formats and sends a SMS to the customer
     *
     * $data contains the replacement for macros
     *
     * @param object $customer
     * @param string $smsText
     * @param array $data
     */
    static function send_sms($customer, $smsText, $business_id, $data)
    {
        $suppressed = false;

        // if there is no message, bounce out
        if (empty($smsText)) {
            return null;
        }

        //need  send the business_id
        $data['business_id'] = $business_id;

        $body = email_replace($smsText, $data);
        $body = strip_tags($body);

        // check to see if the business is setup to use twilio or something else
        $business_sms_settings = self::getSmsSettings($business_id);

        $customerSupport = get_business_meta($business_id, 'customerSupport');
        // if we have a comma in the from field, lets just pull the first email
        $customerSupport = current(explode(',', $customerSupport));

        // if we have a custom SMS Provider, send the SMS's with that
        if (! empty($business_sms_settings->provider)) {

            $sms = clean_phone_number($customer->sms);
            $sms2 = clean_phone_number($customer->sms2);

            $provider = $business_sms_settings->provider;

            // send the outgoing SMS into the emailqueue
            if (! empty($sms)) {
                queueEmail($customerSupport, $provider, $sms, null, $body, array(), null, $business_id);
            }

            if (! empty($sms2)) {
                queueEmail($customerSupport, $provider, $sms2, null, $body, array(), null, $business_id);
            }

            // if there is no business setting, or the provider is NOT twilio or nexmo run it the normal way
        } else {

            $from_email_name = get_business_meta($business_id, 'from_email_name');

            if (!empty($customer->sms)) {

                if (valid_email($customer->sms)) {
                    queueEmail($customerSupport, $from_email_name, $customer->sms, null, $body, array(), null, $business_id);
                } else {
                    // if the sms is not valid, we delete it from the customer account
                    $customer = new Customer($customer->customerID);
                    $customer->sms = '';
                    $customer->save();
                }
            }

            if (!empty($customer->sms2)) {

                if (valid_email($customer->sms2)) {
                    queueEmail($customerSupport, $from_email_name, $customer->sms2, null, $body, array(), null, $business_id);
                } else {
                    // if the sms is not valid, we delete it from the customer account
                    $customer = new Customer($customer->customerID);
                    $customer->sms2 = '';
                    $customer->save();
                }
            }


        }
    }

    /**
     *
     * @param type $business_id
     * @return type
     */
    protected static function getSmsSettings($business_id = 0)
    {
        $CI = get_instance();

        $sms_query = $CI->db->get_where('smsSettings', array(
            'business_id' => $business_id
        ));

        return $sms_query->row();
    }
}
