<?php
use App\Libraries\DroplockerObjects\Business;
/* library for Fabricare exports / imports (from responses) */
class Spot
{

    protected $_ci;

    public $start_date = '0000-00-00 00:00:00'; // if you send these dates through in the params, make sure they're formatted like this
    public $end_date = '9999-99-99 99:99:99';

    public $business_id = null;
    public $bag_number = 0;
    public $serviceID = null;

    public $accountKey = null;
    public $deviceName = null;
    public $userName = null;
    public $priceTable = null;
    public $customerPrefix = '';

    public $save_on_server = false;

    public $url = 'https://servicesdev.spotpos.com/x';

    public $loadBags = false;
    public $addSplit = false;

    public $routes = false;

    public function __construct($params = array())
    {
        $this->_ci = get_instance();

        $this->business_id = $params['business_id'];
        if (empty($this->business_id)) {
            throw new Exception('Missing Business ID');
        } else {
            $this->business = new Business($params['business_id']);
        }

        $this->loadBags = get_business_meta($this->business_id, 'loadBags');
        $this->addSplit = get_business_meta($this->business_id, 'spot_add_split_item_count');


        if (! empty($params['url'])) {
            $this->url = $params['url'];
        }

        if (! empty($params['accountKey'])) {
            $this->accountKey = $params['accountKey'];
        }

        if (! empty($params['deviceName'])) {
            $this->deviceName = $params['deviceName'];
        }

        if (! empty($params['userName'])) {
            $this->userName = $params['userName'];
        }

        if (! empty($params['priceTable'])) {
            $this->priceTable = $params['priceTable'];
        }

        if (! empty($params['start_date'])) {
            $this->start_date = $params['start_date'];
        }

        if (! empty($params['end_date'])) {
            $this->end_date = $params['end_date'];
        }

        if (! empty($params['bag_number'])) {
            $this->bag_number = $params['bag_number'];
        }

        if (! empty($params['customerPrefix'])) {
            $this->customerPrefix = $params['customerPrefix'];
        }

        // check to see if our start/end dates are set by the POST
        $start_post = $this->_ci->input->post('start_date');
        $end_post = $this->_ci->input->post('end_date');
        if (! empty($start_post)) {
            $this->start_date = date('Y-m-d H:i:s', strtotime($start_post));
        }
        if (! empty($end_post)) {
            $this->end_date = date('Y-m-d H:i:s', strtotime($end_post));
        }

        if (! empty($params['routes'])) {
            $this->routes = $params['routes'];
        }
    }

    // orders - array of objects
    public function export($split = false, &$count = 0)
    {
        $orders = $this->pullOrders(); // get the orders
        $count = count($orders);

        return $this->getXML($orders, $split); // get the XML
    }

    protected function processCustomerItem($item, $rItem)
    {
        $tag = $item->getName();
        switch ($tag) {
            case 'CustomerSave':
                return array(
                   'status' => 'success',
                   'message' => (string) $item,
                   'id' => (string)$item['CustomerID'],
                );

            case 'Error':
                $out = array(
                   'status' => 'error',
                   'message' => (string) $item,
                   'id' => (string)$item['CustomerID'],
                   'request' => $rItem->asXML(),
                );

                $matches = array();
                if (preg_match("#Invalid '([^']+)' value#", $out['message'], $matches)) {
                    $out['message'] .= ": '" . $rItem->{$matches[1]} . "'";
                }

                return $out;

            default:
                throw new Exception("SPOT response key unknown: '$tag'");
                break;
        }
    }

    protected function processInvoiceItem($item, $rItem)
    {
        $tag = $item->getName();
        switch ($tag) {
            case 'InvoiceSave':
                return array(
                   'status' => 'success',
                   'message' => (string) $item,
                   'id' => (string)$item['ExternalID'],
                );
                break;

            case 'Error':
                return  array(
                   'status' => 'error',
                   'message' => (string) $item,
                   'id' => (string)$item['ExternalID'],
                   'request' => $rItem->asXML(),
                );
                break;

            default:
                throw new Exception("SPOT response key unknown: '$tag'");
                break;
        }
    }

    public function parseResponse($response, $request)
    {
        $ResponseStream = simplexml_load_string($response);
        $RequestStream = simplexml_load_string($request);

        $invoices = array();
        $customers = array();
        foreach ($RequestStream as $item) {
            $tag = $item->getName();
            switch ($tag) {
            	case 'Customer': $customers[(string)$item->CustomerID] = $item; break;
            	case 'Invoice': $invoices[(string)$item->ExternalID] = $item; break;
            	default: throw new Exception("SPOT request unknown tag: '$tag'");
            }
        }

        $customersStatus = array();
        $InvoiceStatus = array();

        foreach ($ResponseStream as $item) {

            if ($item['CustomerID']) {
                $rItem = $customers[(string)$item['CustomerID']];
            } else {
                $rItem = $invoices[(string)$item['ExternalID']];
            }

            if (empty($rItem)) {
                throw new Exception("SPOT response unknown item: " . $item->asXML());
            }
            $tag = $rItem->getName();

            switch ($tag) {
                case 'Customer': $customersStatus[] = $this->processCustomerItem($item, $rItem); break;
                case 'Invoice': $InvoiceStatus[] = $this->processInvoiceItem($item, $rItem); break;
                default: throw new Exception("SPOT response unknown tag: '$tag'");
            }
        }

        foreach ($InvoiceStatus as $item) {
            $order_id = $item['id'];

            if ($item['status'] == 'success') {
                //echo 'Accepted Invoice ' . $item['id'] . "<br>";

                $invoice_id = $item['id'];
                $invoice = $invoices[$invoice_id];

                foreach ($invoice->Items[0] as $item) {
                    $automat_data = array();
                    $automat_data['order_id'] = $item['id'];
                    $automat_data['gardesc'] = "Imported into SPOT";
                    $automat_data['garcode'] = (string)$item->HSLKey;
                    $automat_data['fileDate'] = convert_to_gmt_aprax(date("H:i:s", time()), $this->business->timezone);
                    $this->_ci->db->insert('automat', $automat_data);
                }
            } else {
                //echo 'Rejected Invoice ' . $item['id'] . " because: " . $item['message']. "<br>";
            }
        }

        return array(
            'invoices' => $InvoiceStatus,
            'customers' => $customersStatus,
        );
    }

    public function pullOrders($automat_ref = true)
    {
        $this->_ci->db->select("customer.firstName, customer.lastName, customer.phone, customer.email,
            location.address AS address1, location.address2, location.city, location.state, location.zipcode as zip, location.route_id, location.sortOrder,
            orders.*, bagNumber, bagID");
        $this->_ci->db->from("orders");
        $this->_ci->db->join('bag', 'bagID = orders.bag_id');
        $this->_ci->db->join('customer', 'customer.customerID = orders.customer_id');
        $this->_ci->db->join('locker', 'lockerID = orders.locker_id');
        $this->_ci->db->join('location', 'locationID = locker.location_id');
        $this->_ci->db->join('orderStatus', 'orders.orderID = orderStatus.order_id');
        $this->_ci->db->order_by("route_id ASC, sortOrder ASC, orders.customer_id ASC");
        $this->_ci->db->group_by("orders.orderID");
        // if we have dates use them
        if (! empty($this->end_date)) {
            $this->_ci->db->where("orderStatus.date <= ", $this->end_date);
        }
        if (! empty($this->start_date)) {
            $this->_ci->db->where("orderStatus.date >= ", $this->start_date);
        }
        $this->_ci->db->where("orderStatus.orderStatusOption_id", 3);
        $this->_ci->db->where_in("orders.orderStatusOption_id", array(3, 7));
        if (!empty($this->routes)) {
            $this->_ci->db->where_in("route_id", $this->routes);
        }
        $this->_ci->db->where("orders.business_id", $this->business_id);

        $orders_query = $this->_ci->db->get();

        $orders = $orders_query->result();

        $all_order_data = array();
        if (! empty($orders)) {

            $last_route = -1;
            $last_stop = 0;

            foreach ($orders as $order) {

                // first see if its already in the automat
                $automat_check_query = $this->_ci->db->select('automatID')
                    ->from('automat')
                    ->where('order_id', $order->orderID)
                    ->where('slot !=', '')
                    ->get();
                $automat_check = $automat_check_query->row();

                // if its in the automat, skip it
                if (! empty($automat_check)) {
                    continue;
                }

                $this->_ci->db->select('item.*, orderItem.*, picture.*, barcode.barcode');
                $this->_ci->db->join('item', 'item.itemID = orderItem.item_id', 'left');
                $this->_ci->db->join('picture', 'item.itemID = picture.item_id', 'left');
                $this->_ci->db->join('barcode', 'barcode.item_id = item.itemID', 'left');

                $order_items_query = $this->_ci->db->get_where('orderItem', array(
                    'order_id' => $order->orderID
                ));
                $order_items = $order_items_query->result();

                if (! empty($order_items)) {
                    $all_order_data[$order->orderID]['order'] = $order;

                    if ($last_route != $order->route_id) {
                        $last_route = $order->route_id;
                        $last_stop = 0;
                    }

                    $all_order_data[$order->orderID]['order']->route_stop = ++$last_stop;


                    foreach ($order_items as $order_item) {

                        $this->_ci->db->select('product_process.*, product.* ');
                        $this->_ci->db->join('product', 'product.productID = product_process.product_id');
                        $prod_details_query = $this->_ci->db->get_where('product_process', array(
                            'product_processID' => $order_item->product_process_id
                        ));
                        $prod_details = $prod_details_query->row();

                        $all_order_data[$order->orderID]['orderItems'][$order_item->orderItemID] = $order_item;

                        if (! empty($prod_details)) {
                            $all_order_data[$order->orderID]['orderItemsDetail'][$order_item->orderItemID] = $prod_details;
                        }
                    }

                    if ($this->loadBags) {
                        $all_order_data[$order->orderID]['orderItems'][$order->bagID] = (object) array(
                            'itemID' => $order->bagID,
                            'barcode' => $order->bagNumber,
                            'qty' => 1,
                            'unitPrice' => '0.00',
                        );

                        $all_order_data[$order->orderID]['orderItemsDetail'][$order->bagID] = (object) array(
                            'name' => 'Bag',
                        );
                    }

                }
            }
        }

        return $all_order_data;
    }

    public function getXML($all_order_data = array(), $split = false)
    {
        $xml = simplexml_load_string('<?xml version="1.0" encoding="utf-8"?><SPOTData></SPOTData>');


        $this->_ci->load->model('business_model');
        $business = $this->_ci->business_model->get_by_primary_key($this->business_id);

        $this->_ci->load->model('orderType_model');
        $orderTypes = $this->_ci->orderType_model->get_formatted_slug_name();

        $this->_ci->load->model("order_model");
        $this->_ci->load->model("orderCharge_model");

        $businessMap = array();

        // go through each order
        foreach ($all_order_data as $orderID => $order_data) {

            if (empty($order_data['order'])) {
                continue;
            }


            $order_details = $order_data['order'];

            // customer fields
            $full_name = array(
                htmlspecialchars($order_details->lastName),
                htmlspecialchars($order_details->firstName)
            );
            $full_name = implode(', ', $full_name);
            $customer = $xml->addChild('Customer');

            $customer->addChild('WorkstationName', $this->deviceName);
            $customer->addChild('UserName', $this->userName);
            $customer->addChild('CustomerID', $this->customerPrefix . $order_details->customer_id);
            $customer->addChild('CustomerType', 'I');
            //$customer->addChild('Title');
            $customer->addChild('FullName', $full_name);

            // remove non-numbers and grab only the last 10 numbers (usually to remove the country code "1"
            $phone = substr(preg_replace('/[^0-9]+/', '', $order_details->phone), -10);

            $customer->addChild('Phone', $phone);

            $customer->addChild('Address1', htmlspecialchars($order_details->address1));
            $customer->addChild('Address2', htmlspecialchars($order_details->address2));

            $customer->addChild('City', $order_details->city ? $order_details->city : $business->city);
            $customer->addChild('State', strtoupper($order_details->state ? $order_details->state : $business->state));
            $customer->addChild('Zip', $order_details->zip ? $order_details->zip : $business->zipcode);

            //$customer->addChild('Phone', preg_replace('/[^0-9]+/', '', $order_details->phone));
            $customer->addChild('Email', $order_details->email);
            $customer->addChild('VIPFlag', 0);
            //$customer->addChild('PriceLevel');
            $customer->addChild('Reminder');
            $customer->addChild('ReferralSource');

            // invoice fields
            $invoice = $xml->addChild('Invoice');
            $invoice->addChild('WorkstationName', $this->deviceName);
            $invoice->addChild('UserName', $this->userName);
            $invoice->addChild('CustomerID', $this->customerPrefix . $order_details->customer_id);
            $invoice->addChild('CreatedDateTime', date('c', strtotime($order_details->dateCreated))); // international formatting
            $invoice->addChild('PromisedDateTime', date('c', strtotime($order_details->dueDate)));
            $invoice->addChild('PriceTable', $this->priceTable);
            $invoice->addChild('DepartmentGroup', isset($orderTypes[$order_details->orderType]) ? $orderTypes[$order_details->orderType] : 'Other');
            $invoice->addChild('ExternalID', $order_details->orderID);

            $comment = "Route: {$order_details->route_id} Stop: {$order_details->route_stop}";

            if ($this->addSplit) {
                $comment .= " Split Items Count: " . $order_details->split;
            }

            // TODO: probably a good idea to add "Assembly Notes: " before this
            $comment .= ' ' . trim(str_replace(array("\r\n", "\n", "\r"), ' ', $order_details->postAssembly));

            $invoice->addChild('Comment', $comment);

            $discountAmount = 0;
            $discountReasons = array();

            $this->_ci->load->model("orderCharge_model");
            $orderCharges = $this->_ci->orderCharge_model->get_aprax(array("order_id" => $order_details->orderID));
            foreach ($orderCharges as $orderCharge) {
                $discountAmount += $orderCharge->chargeAmount;
                $discountReasons[] = trim($orderCharge->chargeType);
            }
            $discountAmount = number_format($discountAmount, 2);
            $discountReason = str_replace(array("\r\n", "\r", "\n"), ' ', implode(' - ', $discountReasons));

            $invoice->addChild('DiscountAmount', $discountAmount);
            $invoice->addChild('DiscountReason', $discountReason);

            $invoice->addChild('EnviroChargeType', 'N');
            $invoice->addChild('EnviroChargeRate', 0);
            $invoice->addChild('EnviroChargeAmount', 0);

            $tax = number_format($this->_ci->order_model->get_tax_total($order_details->orderID), 2);
            $invoice->addChild('Tax', $tax);

            if (! empty($order_data['orderItems'])) {

                $items = $invoice->addChild('Items'); // add items ot the invoice
                $order_items = $order_data['orderItems'];

                $i = 0;
                foreach ($order_items as $item_id => $order_item) {

                    $category = 'Dry Cleaning';
                    $name = isset($order_data['orderItemsDetail'][$item_id]) ? $order_data['orderItemsDetail'][$item_id]->name : '';

                    // ITEM LOOP
                    //$item = $items->addChild('Item');
                    $item = $items->addChild("x$i");
                    $i++;

                    $item->addChild('ExternalID', htmlspecialchars($order_item->itemID));
                    $item->addChild('HSLKey', htmlspecialchars($order_item->barcode)); // barcode
                    $item->addChild('Department', $category);
                    $item->addChild('Category', $category);
                    $item->addChild('Item', htmlspecialchars($name));
                    $item->addChild('Quantity', htmlspecialchars($order_item->qty));
                    $item->addChild('PieceCount', htmlspecialchars($order_item->qty));
                    $item->addChild('Price', htmlspecialchars($order_item->unitPrice));
                    //$item->addChild('Taxable');
                    $item->addChild('Comment');
                    $item->addChild('ConveyorContext', 'H');
                    $item->addChild('ProductionPoints');

                    // check for descriptors
                    $descriptors = array();
                    if (! empty($order_item->color)) {
                        $descriptors[] = $order_item->color;
                    }

                    if (! empty($order_item->size)) {
                        $descriptors[] = $order_item->size;
                    }
                    if (! empty($order_item->manufacturer)) {
                        $descriptors[] = $order_item->manufacturer;
                    }
                    //
                    // $descriptors_string = '';
                    // if ( !empty( $descriptors ) ) {
                    // $descriptors_string = implode( ',', $descriptors );
                    // }
                }
            }
        }

        $xml_prefix = '<?xml version="1.0" encoding="utf-8"?>';
        if ($split) {
            $out = array('customers' => array(), 'invoices' => array());
            foreach ($xml->Customer as $customer) {
                $out['customers'][] = $xml_prefix . $customer->asXML();
            }

            foreach ($xml->Invoice as $invoice) {
                $out['invoices'][] = $xml_prefix . $invoice->asXML();
            }
            return $out;
        }

        return $xml->asXML(); // return the XML string data
    }

    public function saveData($xml, &$request = null)
    {
        $request = $this->createDataRequest($xml);
        return $this->curlRequest($request);
    }

    public function createDataRequest($payload)
    {
        $base64payload = base64_encode($payload);

        $request = '<Request TaskMode="S" Format="Raw">';
        $request .= '<Task Key="Task" Mode="S">';
        $request .= '<Method Key="X" Name="SPOT.RemoteServices.Service:SPOT.RemoteServices.Service:SPOTDataSaveXMLBase64">';
        $request .= '<dataXmlBase64>'.$base64payload.'</dataXmlBase64>';
        $request .= '</Method>';
        $request .= '</Task>';
        $request .= '</Request>';

        return $request;
    }

    public function saveInvoice($xml)
    {
        $request = $this->createInvoiceRequest($xml);
        return $this->curlRequest($request);
    }

    public function createInvoiceRequest($payload)
    {
        $base64payload = base64_encode($payload);

        $request = '<Request TaskMode="S" Format="Raw">';
        $request .= '<Task Key="Task" Mode="S">';
        $request .= '<Method Key="X" Name="SPOT.RemoteServices.Service:SPOT.RemoteServices.Service:InvoiceSaveXMLBase64">';
        $request .= '<invoiceXmlBase64>'.$base64payload.'</invoiceXmlBase64>';
        $request .= '</Method>';
        $request .= '</Task>';
        $request .= '</Request>';

        return $request;
    }

    public function saveCustomer($xml)
    {
        $request = $this->createCustomerRequest($xml);
        return $this->curlRequest($request);
    }

    public function createCustomerRequest($payload)
    {
        $base64payload = base64_encode($payload);

        $request = '<Request TaskMode="S" Format="Raw">';
        $request .= '<Task Key="Task" Mode="S">';
        $request .= '<Method Key="X" Name="SPOT.RemoteServices.Service:SPOT.RemoteServices.Service:CustomerSaveXMLBase64">';
        $request .= '<customerXmlBase64>'.$base64payload.'</customerXmlBase64>';
        $request .= '</Method>';
        $request .= '</Task>';
        $request .= '</Request>';

        return $request;
    }

    public function curlRequest($body)
    {
        $headers = $this->buildHeaders();

        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);

        //relax SSL for now
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS,  $body);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if($errno = curl_errno($ch)) {
            throw new \Exception("Curl Error: " . curl_error($ch));
        }

        curl_close($ch);

        return $response;
    }

    public function buildHeaders()
    {
        $headers = array(
            'Content-Type: user/xml',
            'ServiceID: ff0ea611-e5b6-4989-9857-4806a4357358',
            "AccountKey: $this->accountKey",
            "DeviceName: $this->deviceName",
        );

        return $headers;
    }

}
