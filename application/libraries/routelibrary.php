<?php

use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\DroplockerObject;
use App\Libraries\DroplockerObjects\Business;

class RouteLibrary
{
    protected $noOrderLocation = array();
    protected $noClaimLocation = array();

    
    /**
     * Gets all the routes pickups and deliveries
     *
     * It will return an array of stops each having this structure:
     *
     * {
     *   pickups: [],
     *   deliveries: [],
     *   sortOrder: int,
     *   sortOrder_id: int,
     *   type: 'location|home-delivery'
     *   route: int,
     *   lat: float,
     *   lon: float,
     *   address1: string,
     *   address2: string,
     *   city: string,
     *   state: string,
     *   accessCode: string,
     *   timeWindow: {},
     * }
     *
     *
     * @param array $routes
     * @param int $business_id
     * @param int $day
     * @param string $stops_type
     * @param string $loaded_only
     * @param bool $update
     * @param array $filters key-value for filterType-filterValue (supported claim_id and order_id)
     * @return array
     */
    public function getStops($routes, $business_id, $day, $stops_type = 'both', $loaded_only = false, $update = false, $filters = array())
    {
        $this->noClaimLocation = array();
        $this->noOrderLocation = array();
        
        $business = new Business($business_id);
        $stops = array();
        
        if (!$filters['no_customer_subscription']) {
            // process route subscriptions
            $customers_in_routes = $this->getCustomersInRoutes($business_id, $routes, $day);
            foreach ($customers_in_routes as $customer) {
                $stops = $this->getStopForCustomerInRoute($business, $customer, $stops, $loaded_only, $stops_type, $filters);
            }
        }

        $locations_in_routes =  $this->getLocationsInRoutes($business_id, $routes, $day, $filters);
        
        // get orders
        if ($stops_type == 'both' || $stops_type == 'deliveries') {
            foreach ($locations_in_routes as $location) {
                // get all the orders at this location and the real pickup address
                $stops = $this->processOrders($business, $location, $day, $stops, $loaded_only, $filters);
            }
        }

        // Get claims
        if ($stops_type == 'both' || $stops_type == 'pickups') {
            foreach ($locations_in_routes as $location) {
                // get all the claims at this location and the real pickup address
                $stops = $this->processClaims($business, $location, $day, $stops, $filters);
            }
        }
        
        $stops = $this->processDailyOrNoteLocations($business_id, $stops, $stops_type);

        usort($stops, function($a, $b) {
            return $a->sortOrder - $b->sortOrder;
        });

        if ($update) {
            $sortOrder = 0;
            foreach ($stops as $key => $value) {
                ++$sortOrder;

                if ($sortOrder != $stops[$key]->sortOrder) {
                    $stops[$key]->sortOrder = $sortOrder;
                        $this->updateSortOrder($stops[$key]->type, $stops[$key]->sortOrder_id, $stops[$key]->sortOrder);
                }
            }

            usort($stops, function($a, $b) {
                return $a->sortOrder - $b->sortOrder;
            });
        }

        return $stops;
    }

    /**
     * Get all the driver notes from selected routes
     *
     * @param array $routes
     * @param int $business_id
     * @return array
     */
    public function getDriverNotes($routes, $business_id, $filters = array())
    {
        $route_filter_sql = '';
        if ($routes) {
            $route_sql = implode(',', array_map('intval', $routes));
            $route_filter_sql = "HAVING route_id IN ($route_sql)";
        }
        
        $filter_data = array(
            $business_id
        );
        
        $note_filter_sql = '';
        if (isset($filters['driverNote_id'])) {
            $note_filter_sql = 'AND driverNotes.driverNotesID = ?';
            $filter_data[] = $filters['driverNote_id'];
        }
        
        $sql = "SELECT driverNotes.*, location.*, e.firstName, e.lastName, route_id route 
        FROM driverNotes
        JOIN location ON (locationID = driverNotes.location_id)
        LEFT JOIN employee e ON (employeeID = driverNotes.createdBy)
        WHERE active = 1
        AND location.business_id = ? 
        $note_filter_sql 
        $route_filter_sql 
        ORDER BY driverNotes.created DESC";

        $CI = get_instance();
        return $CI->db->query($sql, $filter_data)->result();
    }

    /**
     * Gets a stop by type and id
     *
     * @param string $type
     * @param int $id
     * @throws Exception
     * @return DroplockerObject
     */
    public function getStop($type, $id)
    {
        switch ($type) {
            case 'Location':
            case 'OrderHomePickup':
            case 'OrderHomeDelivery':
            case 'Delivery_CustomerSubscription':
                $type = 'App\\Libraries\\DroplockerObjects\\' . $type;
                $object = new $type($id);
                return $object;

            default:
                throw new Exception("Unknown class $type");
        }
    }

    /**
     * Updates a stop sortOrder
     *
     * @param string $type
     * @param int $id
     * @param int $sortOrder
     */
    public function updateSortOrder($type, $id, $sortOrder)
    {
        $object = $this->getStop($type, $id);
        $object->sortOrder = $sortOrder;
        $object->save();

        switch ($type) {
            case 'Location':
            case 'OrderHomePickup':
            case 'OrderHomeDelivery':
                break;


            case 'Delivery_CustomerSubscription':

                $CI = get_instance();
                $sql = 'UPDATE orderHomePickup
                        JOIN claim ON (claim_id = claimID)
                        SET sortOrder = ?
                        WHERE orderHomePickup.customer_id = ?
                        AND active = 1';
                $CI->db->query($sql, array($sortOrder, $object->customer_id));

                $sql = 'UPDATE orderHomeDelivery
                        JOIN orders ON (order_id = orderID)
                        SET sortOrder = ?
                        WHERE orderHomeDelivery.customer_id = ?
                        AND orderStatusOption_id NOT IN (10, 13, 1, 2)';
                $CI->db->query($sql, array($sortOrder, $object->customer_id));

                break;

            default:
                throw new Exception("Unknown class $type");
        }

    }

    /**
     * Gets all the locationID in a route
     *
     * @param int $business_id
     * @param array $routes
     * @param int $day
     * @return array
     */
    protected function getLocationsInRoutes($business_id, $routes, $day, $filters = array())
    {
        $dayField = $this->getDayName($day);
        
        if ($day == 0) {
            // Stores 7 in location_serviceDay for Sunday
            $day = 7;
        }
        
        $location_id_filter = '';
        if ($filters['locationID']) {
            $location_id_filter = " AND location.locationID = {$filters['locationID']} ";
        }

        $no_time_window_filter = '';
        if ($filters['no_time_window']) {
            $no_time_window_filter = " AND delivery_window.delivery_windowID IS NULL ";
        }

        // Get all the locationID in this route via location route or home delivery window route
        // A location can be asociated to several routes (one per time window)
        $route_sql = implode(',', array_map('intval', $routes));
        $sql = "SELECT DISTINCT locationID,
        COALESCE(route, route_id) AS route_id

        FROM location
        LEFT JOIN location_serviceDay ON (locationID = location_serviceDay.location_id)
        LEFT JOIN delivery_zone ON (locationID = delivery_zone.location_id)
        LEFT JOIN delivery_window ON (delivery_zoneID = delivery_window.delivery_zone_id)

        WHERE location.business_id = ?
        AND (location_serviceDay.day = ? OR delivery_window.$dayField > 0)
        AND location.status = 'active'
        $location_id_filter
        $no_time_window_filter
        HAVING route_id IN ($route_sql)
        ORDER BY sortOrder ASC
        ";
        
        $CI = get_instance();
        return $CI->db->query($sql, array($business_id, $day))->result();
    }

    /**
     * Converts a day from numeric representation into text representation
     *
     * @param int $day
     * @return string
     */
    protected function getDayName($day)
    {
        // sunday can be 0 or 7.
        $days = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
        $dayName = $days[$day];
        return $dayName;
    }

    /**
     * Gets the time frame to search delivery windows for the selected day
     * @param int $day
     * @return array
     */
    protected function getTimeWindowsFromDay($day)
    {
        $dayName = $this->getDayName($day);

        $timeStart   = strtotime("$dayName 00:00:00");
        $timeEnd     = strtotime("$dayName 23:59:59");

        $windowStart = gmdate('Y-m-d H:i:s', $timeStart);
        $windowEnd   = gmdate('Y-m-d H:i:s', $timeEnd);

        return array($windowStart, $windowEnd);
    }
        
    /**
     * Gets all the orders for delivery in every stop
     *
     * @param Business $business
     * @param stdClass $location
     * @param int $day
     * @param array $stops
     * @param bool $loaded_only
     * @return array
     */
    protected function processOrders($business, $location, $day, $stops, $loaded_only, $filters = array())
    {
        $business_id = $business->businessID;
        $blankCustomerAccount = $business->blank_customer_id;
        $orders = $this->getOrdersForLocation($business_id, $location->locationID, $day, $loaded_only, $blankCustomerAccount, $filters);
        
        if (!count($orders) && !isset($filters['order_id'])) {
            $this->noOrderLocation[] = $location->locationID;
        }
        
        foreach ($orders as $order) {

            if ($order->orderHomeDeliveryID) {
                $window_start = $order->windowStart;
                $window_end = $order->windowEnd;
                $local_window_start = convert_from_gmt_aprax($window_start, 'H:i', $business_id);
                $local_window_end = convert_from_gmt_aprax($window_end, 'H:i', $business_id);
                $time_window =  $local_window_start . '-' . $local_window_end;
            } else {
                $window_start = 'All Day';
                $window_end   = '';
                $time_window = '';
                $local_window_start = '';
                $local_window_end = '';
            }

            // create the key for this stop
            $type = $order->orderHomeDeliveryID ? 'OrderHomeDelivery' : 'Location';

            $stop_key = sprintf('%s/%s/%s', $location->route_id, $order->address1, $order->address2);

            // add the stop if not exists
            if (empty($stops[$stop_key])) {
                $stops[$stop_key] = (object) array(
                    'pickups'             => array(),
                    'deliveries'          => array(),
                    'type'                => $type,
                    'sortOrder'           => $order->sortOrder,
                    'sortOrder_id'        => $order->sortOrder_id,
                    'serviceType'         => $order->serviceType,
                    'locationType'        => $order->locationType,
                    'route'               => $location->route_id,
                    'lat'                 => $order->lat,
                    'lon'                 => $order->lon,
                    'address1'            => $order->address1,
                    'address2'            => $order->address2,
                    'zip'                 => $order->zip,
                    'city'                => $order->city,
                    'state'               => $order->state,
                    'deliveryNotes'       => $order->delivery_notes,
                    'windowStart'         => $window_start,
                    'windowEnd'           => $window_end,
                    'localWindowStart'    => $local_window_start,
                    'localWindowEnd'      => $local_window_end,
                    'timeWindow'          => $time_window,
                    'locationID'          => $order->locationID,
                    'accessCode'          => $order->accessCode,
                    'orderHomeDeliveryID' => $order->orderHomeDeliveryID,
                    'firstName'           => $order->firstName,
                    'lastName'            => $order->lastName,
                    'phone'               => $order->phone,
                    'customer_id'         => $order->customer_id,
                );
            }

            // add the claim to the pickups
            $stops[$stop_key]->deliveries[] = (object) array(
                'orderID'     => $order->orderID,
                'customer_id' => $order->customer_id,
                'dateCreated' => $order->dateCreated,
                'locker_id'   => $order->locker_id,
                'location_id' => $order->locationID,
                'pickupDate'  => $order->deliveryDate,
                'orderType'   => $order->orderType,
                'firstName'   => $order->firstName,
                'lastName'    => $order->lastName,
                'phone'       => $order->phone,
                'loaded'      => $order->loaded,
                'payment'     => $order->orderPaymentStatusOption_id,
                'address1'    => $order->address1,
                'address2'    => $order->address2,
                'route'       => $location->route_id,
                'items'       => $order->items,
                'llNotes'     => $order->llNotes,
                'notes'       => $order->notes,
                'bagNumber'   => $order->bagNumber,
                'lockerName'  => $order->lockerName,
                'orderPosition'  => $order->orderPosition,
                'loadedDate'  => $order->loadedDate,
                'loadedBy'    => $order->loadedBy,
                'assemblyNotes'  => $order->postAssembly,
                'orderStatusOption_id'  => $order->orderStatusOption_id,
                'dueDate'     => $order->dueDate,
                'alertNotes'  => $order->alertNotes,
            );
        }

        return $stops;
    }
    
    /**
     * Gets all the orders for pickup in every stop
     *
     * @param Business $business
     * @param stdClass $location
     * @param int $day
     * @param array $stops
     * @return array
     */
    protected function processClaims($business, $location, $day, $stops, $filters = array())
    {
        $business_id = $business->businessID;
        $claims = $this->getClaimsForLocation($business_id, $location->locationID, $day, $filters);

        if (!count($claims) && !isset($filters['claim_id'])) {
            $this->noClaimLocation[] = $location->locationID;
        }
        
        foreach ($claims as $claim) {

            if ($claim->orderHomePickupID) {
                $window_start = $claim->windowStart;
                $window_end = $claim->windowEnd;
                $local_window_start = convert_from_gmt_aprax($window_start, 'H:i', $business_id);
                $local_window_end = convert_from_gmt_aprax($window_end, 'H:i', $business_id);
                $time_window = $local_window_start . '-' . $local_window_end;
            } else {
                $window_start = 'All Day';
                $window_end = '';
                $time_window = '';
                $local_window_start = '';
                $local_window_end = '';
            }

            // create the key for this stop
            $type = $claim->orderHomePickupID ? 'OrderHomePickup' : 'Location';

            $stop_key = sprintf('%s/%s/%s', $location->route_id, $claim->address1, $claim->address2);

            // add the stop if not exists
            if (empty($stops[$stop_key])) {
                $stops[$stop_key] = (object) array(
                    'pickups'           => array(),
                    'deliveries'        => array(),
                    'type'              => $type,
                    'sortOrder'         => $claim->sortOrder,
                    'sortOrder_id'      => $claim->sortOrder_id,
                    'serviceType'       => $claim->serviceType,
                    'route'             => $location->route_id,
                    'locationType'      => $claim->locationType,
                    'lat'               => $claim->lat,
                    'lon'               => $claim->lon,
                    'address1'          => $claim->address1,
                    'address2'          => $claim->address2,
                    'zip'               => $claim->zip,
                    'city'              => $claim->city,
                    'state'             => $claim->state,
                    'pickupNotes'       => $claim->pickup_notes,
                    'windowStart'       => $window_start,
                    'windowEnd'         => $window_end,
                    'localWindowStart'  => $local_window_start,
                    'localWindowEnd'    => $local_window_end,
                    'timeWindow'        => $time_window,
                    'locationID'        => $claim->locationID,
                    'accessCode'        => $claim->accessCode,
                    'orderHomePickupID' => $claim->orderHomePickupID,
                    'firstName'         => $claim->firstName,
                    'lastName'          => $claim->lastName,
                    'phone'             => $claim->phone,
                    'customer_id'       => $claim->customer_id,
                );
            }

            // add the claim to the pickups
            $stops[$stop_key]->pickups[] = (object) array(
                'claimID'     => $claim->claimID,
                'customer_id' => $claim->customer_id,
                'locker_id'   => $claim->locker_id,
                'location_id' => $claim->locationID,
                'pickupDate'  => $claim->pickupDate,
                'firstName'   => $claim->firstName,
                'lastName'    => $claim->lastName,
                'phone'       => $claim->phone,
                'lockerName'  => $claim->lockerName,
                'orderType'   => unserialize($claim->orderType),
                'notes'       => $claim->notes,
                'address1'    => $claim->address1,
                'address2'    => $claim->address2,
            );
        }

        return $stops;
    }

    /**
     * Gets all the orders for pickup in a single stop
     *
     * @param int $business_id
     * @param int $location_id
     * @param int $day
     * @param bool $loaded_only
     * @param int $blankCustomerAccount
     * @return array
     */
    protected function getOrdersForLocation($business_id, $location_id, $day, $loaded_only, $blankCustomerAccount, $filters = array())
    {
        $filters['location_id'] = $location_id;
        $filters['business_id'] = $business_id;
        
        return $this->getOrdersForManifest($day, $loaded_only, $blankCustomerAccount, $filters);
    }
    
    protected function getOrdersForManifest($day, $loaded_only, $blankCustomerAccount, $filters)
    {
        $loaded_sql = $loaded_only ? 'AND loaded = 1' : '';
        list($timeStart, $timeEnd) = $this->getTimeWindowsFromDay($day);
        
        $filter_data = array(
            $blankCustomerAccount,
            //Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY,
            //$timeStart,  now showing up on manifest if pastdue. dwb
            $timeEnd,
            Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY,
        );
        
        $location_filter_sql = '';
        if (isset($filters['location_id'])) {
            $location_filter_sql = 'AND locker.location_id = ?';
            $filter_data[] = $filters['location_id'];
        }

        $business_filter_sql = '';
        $filter_by_due_date = '';
        if (isset($filters['business_id'])) {
            $business_filter_sql = 'AND orders.business_id = ?';
            $filter_data[] = $filters['business_id'];
            
            if (get_business_meta($filters['business_id'], "limit_deliveries_by_due_date")) {
                $filter_by_due_date = "AND orders.duedate <= '" . convert_from_gmt_locale("", "%Y-%m-%d", $filters['business_id']) . "'";  
            }
        }
        
        $order_filter_sql = '';
        if (isset($filters['order_id'])) {
            $order_filter_sql = 'AND orders.orderID = ?';
            $filter_data[] = $filters['order_id'];
        }

        $sql = "SELECT orders.*, location.serviceType, locationID, orderHomeDeliveryID,
                       COALESCE(orderHomeDelivery.notes, location.accessCode) AS accessCode,
                                              CASE WHEN location.serviceType = 'time window home delivery' THEN
                           COALESCE(orderHomeDelivery.address1, customer.address1)
                       ELSE 
                          COALESCE(orderHomeDelivery.address1, location.address)
                       END AS address1,
                       CASE WHEN location.serviceType = 'time window home delivery' THEN
                           COALESCE(orderHomeDelivery.address2, customer.address2)
                       ELSE 
                          COALESCE(orderHomeDelivery.address2, location.address2)
                       END AS address2,
                       COALESCE(orderHomeDelivery.zip, location.zipcode) AS zip,
                       COALESCE(orderHomeDelivery.city, location.city) AS city,
                       COALESCE(orderHomeDelivery.state, location.state) AS state,
                       COALESCE(orderHomeDelivery.lat, location.lat) AS lat,
                       COALESCE(orderHomeDelivery.lon, location.lon) AS lon,
                       COALESCE(orderHomeDelivery.sortOrder, location.sortOrder) AS sortOrder,
                       COALESCE(orderHomeDeliveryID, locationID) AS sortOrder_id,
                       orderHomeDelivery.deliveryDate AS deliveryDate,
                       orderHomeDelivery.windowStart,
                       orderHomeDelivery.windowEnd,
                       orderHomeDelivery.notes AS delivery_notes,
                       customer.firstName, customer.lastName, customer.phone,
                       locationType.name locationType,
                       locker.lockerName,
                       bag.bagNumber,
                       assembly.position orderPosition,
                       loadedStatus.date loadedDate,
                       CONCAT(loaderEmployee.firstName, ' ', loaderEmployee.lastName) loadedBy,
                       COUNT(orderItemID) AS items
            FROM orders
            JOIN locker ON (lockerID = orders.locker_id)
            JOIN location ON (locationID = locker.location_id)
            LEFT JOIN locationType ON (locationTypeID = location.locationType_id)
            LEFT JOIN bag ON (bagID = orders.bag_id)
            JOIN customer ON (customerID = orders.customer_id)
            LEFT JOIN orderHomeDelivery ON (orderID = orderHomeDelivery.order_id)
            LEFT JOIN orderItem ON (orderID = orderItem.order_id)
            LEFT JOIN businessMeta AS requireCustomerSelection 
              ON orders.business_id = requireCustomerSelection.business_id and 
                 `key` = 'hd_time_window_require_customer_delivery_selection'
            LEFT JOIN assembly ON (orderID = assembly.order_id)
            LEFT JOIN orderStatus loadedStatus ON (orderID = loadedStatus.order_id AND loadedStatus.orderStatusOption_id = '14')
            LEFT JOIN employee loaderEmployee ON (loaderEmployee.employeeID = loadedStatus.employee_id)
            
            WHERE orders.orderStatusOption_id NOT IN (10, 13, 1, 2)
            AND orders.customer_id != ?
            AND (
                    orderHomeDelivery.windowEnd <= ? OR
                    location.serviceType != ? OR 
                    requireCustomerSelection.value = '0'
                )
            $location_filter_sql
            $business_filter_sql
            $order_filter_sql
            $filter_by_due_date
            $loaded_sql
            GROUP BY orderID
            ";

        $CI = get_instance();
        $orders = $CI->db->query($sql, $filter_data)->result();

        return $orders;
    }

    /**
     * Gets all the claims for pickup in a single stop
     *
     * @param int $business_id
     * @param int $location_id
     * @param int $day
     * @return array
     */
    protected function getClaimsForLocation($business_id, $location_id, $day, $filters = array())
    {
        $filters['location_id'] = $location_id;
        $filters['business_id'] = $business_id;
        
        
        return $this->getClaimsForManifest($day, $filters);
    }
    
    protected function getClaimsForManifest($day, $filters)
    {
        list($timeStart, $timeEnd) = $this->getTimeWindowsFromDay($day);
        
        $filter_data = array(
            Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY,
            $timeStart,
            $timeEnd,
            Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY,
        );
                
        $location_filter_sql = '';
        if (isset($filters['location_id'])) {
            $location_filter_sql = 'AND locker.location_id = ?';
            $filter_data[] = $filters['location_id'];
        }
        
        $business_filter_sql = '';
        if (isset($filters['business_id'])) {
            $business_filter_sql = 'AND claim.business_id = ?';
            $filter_data[] = $filters['business_id'];
        }
        
        $claim_filter_sql = '';
        if (isset($filters['claim_id'])) {
            $claim_filter_sql = 'AND claim.claimID = ?';
            $filter_data[] = $filters['claim_id'];
        }
        
        $sql = "SELECT claim.*, location.serviceType, locationID, orderHomePickupID,
                       COALESCE(orderHomePickup.notes, location.accessCode) AS accessCode,
                       COALESCE(orderHomePickup.address1, location.address) AS address1,
                       COALESCE(orderHomePickup.address2, location.address2) AS address2,
                       COALESCE(orderHomePickup.zip, location.zipcode) AS zip,
                       COALESCE(orderHomePickup.city, location.city) AS city,
                       COALESCE(orderHomePickup.state, location.state) AS state,
                       COALESCE(orderHomePickup.lat, location.lat) AS lat,
                       COALESCE(orderHomePickup.lon, location.lon) AS lon,
                       COALESCE(orderHomePickup.sortOrder, location.sortOrder) AS sortOrder,
                       COALESCE(orderHomePickupID, locationID) AS sortOrder_id,
                       COALESCE(orderHomePickup.pickupDate, claim.pickupDate) AS pickupDate,
                       orderHomePickup.windowStart,
                       orderHomePickup.windowEnd,
                       orderHomePickup.notes AS pickup_notes,
                       locationType.name locationType,
                       locker.lockerName,
                       customer.firstName, customer.lastName, customer.phone
            FROM claim
            JOIN locker ON (lockerID = claim.locker_id)
            JOIN location ON (locationID = locker.location_id)
            LEFT JOIN locationType ON (locationTypeID = location.locationType_id)
            JOIN customer ON (customerID = claim.customer_id)
            LEFT JOIN orderHomePickup ON (claimID = orderHomePickup.claim_id)
            WHERE claim.active = 1
            AND location.serviceType != ?
            AND (
                    (orderHomePickup.windowStart >= ? AND orderHomePickup.windowEnd <= ?) OR
                    (orderHomePickupID IS NULL AND location.serviceType != ?)
                )
            $location_filter_sql
            $business_filter_sql
            $claim_filter_sql
            ";

        $CI = get_instance();
        $claims = $CI->db->query($sql, $filter_data)->result();

        return $claims;
    }

    /**
     * Gets all the customers with subscriptions to a route
     *
     * @param int $business_id
     * @param array $routes
     * @param int $day
     * @return array
     */
    protected function getCustomersInRoutes($business_id, $routes, $day)
    {
        $dayField = $this->getDayName($day);

        // Get all the locationID in this route via location route or home delivery window route
        $route_sql = implode(',', array_map('intval', $routes));

        $sql = "SELECT
                        dz.location_id AS locationID,
                        dc.notes AS accessCode,
                        COALESCE(dc.address1, c.address1) AS address1,
                        COALESCE(dc.address2, c.address2) AS address2,
                        COALESCE(dc.zip, c.zip) AS zip,
                        COALESCE(dc.city, c.city) AS city,
                        COALESCE(dc.state, c.state) AS state,
                        COALESCE(dc.lat, c.lat) AS lat,
                        COALESCE(dc.lon, c.lon) AS lon,
                        dc.sortOrder,
                        dw.start as windowStart,
                        dw.end as windowEnd,
                        dw.notes AS delivery_notes,
                        dw.route as route_id,
                        dw.delivery_windowID,
                        dz.delivery_zoneID,
                        dc.delivery_customerSubscriptionID,
                        c.firstName,
                        c.lastName,
                        c.phone,
                        c.customerID as 'customer_id'
                FROM delivery_window dw
                JOIN delivery_zone dz ON dz.delivery_zoneID = dw.delivery_zone_id
                JOIN delivery_customerSubscription dc ON dc.delivery_zone_id = dz.delivery_zoneID
                JOIN customer c ON c.customerID = dc.customer_id
                WHERE dw.route IN ($route_sql)
                AND dw.$dayField > 0
                AND c.business_id = ?;";

        $CI = get_instance();
        $result = $CI->db->query($sql, array($business_id))->result();
        return $result;
    }

    /**
     * Generates the stop for a customer in a route
     *
     * @param Business $business
     * @param stdClass $customer
     * @param array $stops
     * @param bool $loaded_only
     * @param string $stops_type
     */
    protected function getStopForCustomerInRoute($business, $customer, $stops, $loaded_only, $stops_type, $filters = array())
    {
        $business_id = $business->businessID;
        $blankCustomerAccount = $business->blank_customer_id;

        $window_start = gmdate('Y-m-d H:i', strtotime("{$customer->windowStart} {$business->timeZone}"));
        $window_end = gmdate('Y-m-d H:i', strtotime("$customer->windowEnd {$business->timeZone}"));
        $local_window_start = convert_from_gmt_aprax($window_start, 'H:i', $business_id);
        $local_window_end = convert_from_gmt_aprax($window_end, 'H:i', $business_id);
        $time_window =  $local_window_start . '-' . $local_window_end;

        // create the key for this stop
        $stop_key = sprintf('%s/%s/%s', $customer->route_id, $customer->address1, $customer->address2);

            // add the stop if not exists
        if (empty($stops[$stop_key])) {
            $stops[$stop_key] = (object) array(
                'pickups'             => array(),
                'deliveries'          => array(),
                'type'                => 'Delivery_CustomerSubscription',
                'sortOrder'           => $customer->sortOrder,
                'sortOrder_id'        => $customer->delivery_customerSubscriptionID,
                'serviceType'         => Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY,
                'route'               => $customer->route_id,
                'lat'                 => $customer->lat,
                'lon'                 => $customer->lon,
                'address1'            => $customer->address1,
                'address2'            => $customer->address2,
                'zip'                 => $customer->zip,
                'city'                => $customer->city,
                'state'               => $customer->state,
                'deliveryNotes'       => $customer->delivery_notes,
                'windowStart'         => $window_start,
                'windowEnd'           => $window_end,
                'localWindowStart'    => $local_window_start,
                'localWindowEnd'      => $local_window_end,
                'timeWindow'          => $time_window,
                'locationID'          => $customer->locationID,
                'accessCode'          => $customer->accessCode,
                'orderHomeDeliveryID' => '',
                'firstName'           => $customer->firstName,
                'lastName'            => $customer->lastName,
                'phone'               => $customer->phone,
                'customer_id'         => $customer->customer_id,
                'delivery_windowID'   => $customer->delivery_windowID,
                'delivery_zoneID'     => $customer->delivery_zoneID,
                'Delivery_CustomerSubscriptionID' => $customer->delivery_customerSubscriptionID,
                );

            if ($stops_type == 'both' || $stops_type == 'pickups') {
                // get all the claims at this location and the real pickup address
                $claims = $this->getClaimsForCustomerInRoute($business_id, $customer->customer_id, $filters);

                // add the claim to the pickups
                foreach ($claims as $claim) {
                    $stops[$stop_key]->pickups[] = (object) array(
                        'claimID'     => $claim->claimID,
                        'customer_id' => $claim->customer_id,
                        'locker_id'   => $claim->locker_id,
                        'location_id' => $claim->locationID,
                        'pickupDate'  => $claim->pickupDate,
                        'firstName'   => $customer->firstName,
                        'lastName'    => $customer->lastName,
                        'phone'       => $customer->phone,
                        'lockerName'  => $claim->lockerName,
                        'orderType'   => unserialize($claim->orderType),
                        'notes'       => $claim->notes,
                        'address1'    => $customer->address1,
                        'address2'    => $customer->address2,
                    );
                }
            }

            if ($stops_type == 'both' || $stops_type == 'deliveries') {
                // get all the orders fro this customer
                $orders = $this->getOrdersForCustomerInRoute($business_id, $customer->customer_id, $filters);

                // add the claim to the pickups
                foreach ($orders as $order) {
                    $stops[$stop_key]->deliveries[] = (object) array(
                        'orderID'     => $order->orderID,
                        'customer_id' => $order->customer_id,
                        'dateCreated' => $order->dateCreated,
                        'locker_id'   => $order->locker_id,
                        'location_id' => $order->locationID,
                        'pickupDate'  => $order->deliveryDate,
                        'orderType'   => $order->orderType,
                        'firstName'   => $customer->firstName,
                        'lastName'    => $customer->lastName,
                        'phone'       => $customer->phone,
                        'loaded'      => $order->loaded,
                        'payment'     => $order->orderPaymentStatusOption_id,
                        'address1'    => $customer->address1,
                        'address2'    => $customer->address2,
                        'route'       => $customer->route_id,
                        'items'       => 0,
                        'llNotes'     => $order->llNotes,
                        'notes'       => $order->notes,
                        'bagNumber'   => $order->bagNumber,
                        'lockerName'  => $order->lockerName,
                        'orderPosition'  => $order->orderPosition,
                        'loadedDate'  => $order->loadedDate,
                        'loadedBy'    => $order->loadedBy,
                        'assemblyNotes'  => $order->postAssembly,
                        'orderStatusOption_id'  => $order->orderStatusOption_id,
                        'dueDate'     => $order->dueDate,
                        'alertNotes'  => $order->alertNotes,
                    );
                }
            }
        }
        
        if (!empty($filters['nonEmpty']) && !count($stops[$stop_key]->pickups) && !count($stops[$stop_key]->deliveries)) {
            unset($stops[$stop_key]);
        }

        return $stops;
    }

    /**
     * Gets all the claims for pickup from customer_id
     *
     * @param int $business_id
     * @param int $customer_id
     * @return array
     */
    protected function getClaimsForCustomerInRoute($business_id, $customer_id, $filters = array())
    {
        $filter_data = array(
            $customer_id, 
            $business_id
        );
        
        $claim_filter_sql = '';
        if (isset($filters['claim_id'])) {
            $claim_filter_sql = 'AND c.claimID = ?';
            $filter_data[] = $filters['claim_id'];
        }
        
        $sql = "SELECT
                    c.claimID,
                    c.customer_id,
                    c.locker_id,
                    ohp.location_id as 'locationID',
                    c.pickupDate,
                    locker.lockerName,
                    c.orderType,
                    c.notes
                FROM
                    orderHomePickup ohp
                        JOIN
                    claim c ON c.claimID = ohp.claim_id
                    LEFT JOIN locker ON (lockerID = c.locker_id)
                WHERE
                    c.customer_id = ?
                    AND c.business_id = ?
                    AND c.active = 1 
                    $claim_filter_sql
                    ";

        $CI = get_instance();
        return $CI->db->query($sql, $filter_data)->result();
    }

    /**
     * Gets all the orders for delivery to a customer
     *
     * @param int $business_id
     * @param int $customer_id
     * @return array
     */
    protected function getOrdersForCustomerInRoute($business_id, $customer_id, $filters = array())
    {
        $filter_data = array(
            $customer_id, 
            $business_id
        );
        
        $order_filter_sql = '';
        if (isset($filters['order_id'])) {
            $order_filter_sql = 'AND o.orderID = ?';
            $filter_data[] = $filters['order_id'];
        }
        
        $sql = "SELECT
                    o.orderID,
                    o.customer_id,
                    o.locker_id,
                    ohd.location_id as 'locationID',
                    ohd.deliveryDate,
                    o.orderType,
                    o.dateCreated,
                    o.loaded,
                    o.orderPaymentStatusOption_id,
                    o.llNotes,
                    o.notes,
                    o.postAssembly,
                    o.orderStatusOption_id,
                    bag.bagNumber,
                    locker.lockerName,
                    assembly.position orderPosition,
                    loadedStatus.date loadedDate,
                    o.dueDate,
                    o.alertNotes, 
                    CONCAT(loaderEmployee.firstName, ' ', loaderEmployee.lastName) loadedBy
                FROM
                    orderHomeDelivery ohd
                        JOIN
                    orders o ON o.orderID = ohd.order_id
                    LEFT JOIN bag ON (bagID = o.bag_id)
                    LEFT JOIN locker ON (lockerID = o.locker_id)
                    LEFT JOIN assembly ON (orderID = assembly.order_id)
                    LEFT JOIN orderStatus loadedStatus ON (orderID = loadedStatus.order_id AND loadedStatus.orderStatusOption_id = '14')
                    LEFT JOIN employee loaderEmployee ON (loaderEmployee.employeeID = loadedStatus.employee_id)
                    
                WHERE o.customer_id = ?
                AND o.orderStatusOption_id NOT IN (10, 13, 1, 2)
                AND o.business_id = ?
                $order_filter_sql
                    
                GROUP BY orderID
                ";

        $CI = get_instance();
        return $CI->db->query($sql, $filter_data)->result();
    }

    /**
     * Gets all the routes for the business
     *
     * @param int $business_id
     * @return array
     */
    public function getRoutesForBusiness($business_id)
    {
        $sql = "SELECT DISTINCT
                    route_id
               FROM
                    location 
               WHERE 
                    business_id = ?";

        $CI = get_instance();
        return $CI->db->query($sql, array($business_id))->result();
    }

    /**
     * add all the daily locations or note locations with no asociated orders or claims
     *
     * @param int $business_id
     * @param array $stops
     * @param string $stops_type
     * @return array
     */
    protected function processDailyOrNoteLocations($business_id, $stops, $stops_type) {
        $noDataLocations = array();
        if ($stops_type == 'both') {
            $noDataLocations = array_intersect($this->noOrderLocation, $this->noClaimLocation);
        } else if ($stops_type == 'deliveries') {
            $noDataLocations = $this->noOrderLocation;
        } else if ($stops_type == 'pickups') {
            $noDataLocations = $this->noClaimLocation;
        }
        
        if (count($noDataLocations)) {
            $locations = $this->getDailyOrNoteLocations($business_id, $noDataLocations);

            foreach ($locations as $location) {

                //assuming: location with no order/claim asociated, are all day locations
                $window_start = 'All Day';
                $window_end = '';
                $time_window = '';
                $local_window_start = '';
                $local_window_end = '';

                // create the key for this stop
                $type = 'Location';

                $stop_key = sprintf('%s/%s/%s', $location->route_id, $location->address1, $location->address2);

                // add the stop if not exists
                if (empty($stops[$stop_key])) {
                    $stops[$stop_key] = (object) array(
                        'pickups'           => array(),
                        'deliveries'        => array(),
                        'type'              => $type,
                        'sortOrder'         => $location->sortOrder,
                        'sortOrder_id'      => $location->sortOrder_id,
                        'serviceType'       => $location->serviceType,
                        'route'             => $location->route_id,
                        'locationType'      => $location->locationType,
                        'lat'               => $location->lat,
                        'lon'               => $location->lon,
                        'address1'          => $location->address1,
                        'address2'          => $location->address2,
                        'zip'               => $location->zip,
                        'city'              => $location->city,
                        'state'             => $location->state,
                        'windowStart'       => $window_start,
                        'windowEnd'         => $window_end,
                        'localWindowStart'  => $local_window_start,
                        'localWindowEnd'    => $local_window_end,
                        'timeWindow'        => $time_window,
                        'locationID'        => $location->locationID,
                        'accessCode'        => $location->accessCode,
                        'firstName'         => '',
                        'lastName'          => '',
                        'phone'             => '',
                        'customer_id'       => '-1',
                    );
                }

            }
        }

        return $stops;
    }

    /**
     * Gets all the daily locations or locations with notes with no asociated orders or claims
     *
     * @param int $business_id
     * @param array $noDataLocations
     * @return array
     */
    protected function getDailyOrNoteLocations($business_id, $noDataLocations)
    {
        $noDataLocationsList = implode(', ', array_unique($noDataLocations));
        
        $sql = "SELECT distinct location.serviceType, locationID, route_id,
                       location.accessCode AS accessCode,
                       location.address AS address1,
                       location.address2 AS address2,
                       location.zipcode AS zip,
                       location.city AS city,
                       location.state AS state,
                       location.lat AS lat,
                       location.lon AS lon,
                       location.sortOrder AS sortOrder,
                       locationID AS sortOrder_id,
                       locationType.name locationType
            FROM location
            LEFT JOIN locationType ON locationTypeID = location.locationType_id
            LEFT JOIN driverNotes ON locationID = driverNotes.location_id AND driverNotes.active = 1
            WHERE location.business_id = ?
            AND locationID in ($noDataLocationsList) 
            AND (location.serviceType = 'daily' OR driverNotes.driverNotesID IS NOT NULL)
            ";

        $CI = get_instance();
        $locations = $CI->db->query($sql, array(
            $business_id,
        ))->result();

        return $locations;
    }

    /**
     * Gets all the routes for customer
     *
     * @param int $customer_id
     * @param int $business_id
     * @return array
     */
    public function getRoutesForCustomer($customer_id, $business_id)
    {
        $sql = "SELECT 
                    DISTINCT c.customerID, 
                    GROUP_CONCAT(l.route_id SEPARATOR ',') AS routes 
                FROM 
                    customer c 
                    JOIN customer_location cl ON cl.customer_id = c.customerID
                    JOIN location l ON l.locationID = cl.location_id 
                WHERE c.business_id = ? 
                    AND c.customerID = ?
                GROUP BY 
                    customerID";

        $CI = get_instance();
        $result = $CI->db->query($sql, array($business_id, $customer_id))->result();
        
        if (empty($result[0])) return array();

        $routes = explode(',', $result[0]->routes);

        return $routes;
    }
}
