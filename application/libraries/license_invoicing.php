<?php

class License_invoicing extends CI_Model
{

    public function showInvoice($invoiceNumber)
    {
        if (!$invoiceNumber) {
            set_flash_message('error', "No Invoice ID Passed");
        } else {
            $getInvoice = "select *
                            from licenseInvoice
                            join business on business_id = businessID
                            where licenseInvoiceID = $invoiceNumber";
            $q = $this->db->query($getInvoice);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $content['invoice'] = $invoice = $q->result();

            $getLockers = "select *
                            from licenseInvoiceDetail
                            left join locker on lockerID = detailValue
                            left join location on location_id = locationID
                            left join locationType on locationTypeID = locationType_id
                            where licenseInvoice_id = $invoiceNumber
                            and detailType = 'locker_id'
                            order by location.address, lockerName";
            $q = $this->db->query($getLockers);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $content['lockers'] = $lockers = $q->result();
            $content['lockerCount'] = 0;
            $content['lockerTotal'] = 0;

            foreach ($lockers as $l) {
                $content['lockerCount'] ++;
                $content['lockerTotal'] += $l->price;
            }

            $getTrans = "select *
                              from licenseInvoiceDetail
                              join orders on orderID = detailValue
                              join locker on locker_id = lockerID
                              join location on location_id = locationID
                              join locationType on locationTypeID = locationType_id
                              join customer on customer_id = customerID
                              where licenseInvoice_id = $invoiceNumber
                              and detailType = 'order_id'
                              order by orders.dateCreated desc";
            $q = $this->db->query($getTrans);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $content['tran'] = $tran = $q->result();
            $content['tranCount'] = 0;
            $content['tranTotal'] = 0;
            foreach ($tran as $t) {
                $content['tranCount'] ++;
                $content['tranTotal'] += $t->price;
            }

            $getRevenues = "select *
                            from licenseInvoiceDetail
                            where licenseInvoice_id = $invoiceNumber
                            and detailType = 'revenue'";
            $q = $this->db->query($getRevenues);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $content['revenues'] = $revenues = $q->result();
            $content['revenuesCount'] = 0;
            $content['revenuesTotal'] = 0;
            foreach ($revenues as $l) {
                $content['revenuesCount'] ++;
                $content['revenuesTotal'] += $l->price;
            }

            $getMin = "select *
                              from licenseInvoiceDetail
                              where licenseInvoice_id = $invoiceNumber
                              and detailType = 'minimum'";
            $q = $this->db->query($getMin);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $content['minimum'] = $minimum = $q->result();
            $content['totalDue'] = $content['tranTotal'] + $content['lockerTotal'] + $content['revenuesTotal'];
            if (!empty($minimum[0]->price)) {
                $content['totalDue'] += $minimum[0]->price;
            }
        }

        return $content;
    }

}
