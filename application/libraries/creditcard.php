<?php
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderPaymentStatus;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CustomerHistory;
use App\Libraries\DroplockerObjects\KioskAccess;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\AuthorizationLog;

/**
 * CreditCard Library
 */
class CreditCard
{
    /**
     * Uses the key values stored in the POST body for performaing an authorization
     * @param type $customer_id If not passed, this function will search for and attempt to use the key 'customer_id' in the POST body
     * @throws \InvalidArgumentException Thrown if no customer ID is psecified in the function arguments and no 'customer_id' key in the POST obdy
     */
    public function authorizePOST($customer_id = null)
    {
        $CI = get_instance();
        $errorResult = array("status" => "error");
        if (is_null($customer_id)) {
            if (!($customer_id = $CI->input->post("customer_id"))) {
                $errorResult['message'] = ("'customer_id' must be passed as an argument or exist in the POST body");
            }
        }
        $enableKioskAccess = $CI->input->post("kioskAccess");

        // remove any non-numeric characters
        $cardNumber = preg_replace('/[^0-9]/', '', $CI->input->post("cardNumber"));
        if (!is_numeric($cardNumber)) {
            $errorResult['message'] = "'cardNumber' must be numeric";
        }
        else if (!is_numeric($expirationMonth = $CI->input->post("expMo"))) {
            $errorResult['message'] = "'expMo' must be numeric";
        }

        else if (!is_numeric($expirationYear = $CI->input->post("expYr"))) {
            $errorResult['message'] = "'expYr' must be numeric";
        }
        $firstName = $CI->input->post("firstName");
        $lastName = $CI->input->post("lastName");
        $address = $CI->input->post("address1");
        $city = $CI->input->post("city");
        $state =  $CI->input->post("state");
        $zipcode = $CI->input->post("zip");
        $csc = $CI->input->post("csc");
        $phone_number = empty($CI->input->post("phone_number"))?'':$CI->input->post("phone_number");

        $countryCode = $CI->input->post("countryCode");

        if (empty($errorResult['message'])) {
            return $this->authorize($customer_id, $enableKioskAccess, $cardNumber, $expirationMonth, $expirationYear, $firstName, $lastName, $address, $city, $state, $zipcode, $countryCode, $csc, NULL, $phone_number);
        } else {
            return $errorResult;
        }
    }

    /**
     * Authorizes a customer's new credit card.
     * If authorization is succesful, the new credit card will replace any existing credit cfard
     *
     * @param int $customer_id
     * @param bool $enableKioskAccess If true, will make the credit card also available for kiosk access by creating a kioskAccess entry
     * @param int $cardNumber
     * @param int $expirationMonth In two digit format
     * @param int $expirationYear in four digit format
     * @param string $firstName Billign first name
     * @param string $lastName Billing last name
     * @param string $address Billing address
     * @param string $city Billing city
     * @param string $state Billing State
     * @param string $zipcode Billing Zip Code
     * @param string $countryCode Billing Country code
     * @param int $csc
     * @param bool testMode If true, then the authorization is done using the payment processor's test mode. If false, then the uathorization is done as a live transac tion.
     * @return array
     *  (
     *      status => success | error,
     *      message => string
     *      data => array(
     *          additionalOrders => array(
     *          ),
     *          captureResults => {
     *              array(
     *                  status => string success|fail,
     *                  order_id => int,
     *                  message => string,
     *                  amount => float
     *              )
     *          }
     *      }
     *  )
     */
    public function authorize($customer_id, $enableKioskAccess, $cardNumber, $expirationMonth, $expirationYear, $firstName, $lastName, $address, $city, $state, $zipcode, $countryCode = "us", $csc, $testMode = NULL, $phone_number = NULL, $is_one_time = 0)
    {
        //Get the customer current credit card
        $currentCreditCard = \App\Libraries\DroplockerObjects\CreditCard::search(array(
            "customer_id" => $customer_id,
        ));

        if ($is_one_time && !empty($currentCreditCard)) {
            return array(
                "status" => "error",
                "message" => get_translation('one_time_card_already_on_file', "business_account/profile_billing", array(), "Can not use one-time credit card. You already have a card on file ", $customer_id),
            );
        }
        
        $expirationMonth = str_pad($expirationMonth, 2, "0", STR_PAD_LEFT);

        if (\App\Libraries\DroplockerObjects\CreditCard::isExpired($expirationMonth, $expirationYear)) {
            $expire_date = sprintf('%02s/%d', $expirationMonth, $expirationYear);
            return array(
                "status" => "error",
                "message" => get_translation("expired_credit_card_authorization", "business_account/profile_billing", array("expire_date" => $expire_date), "credit card is expired", $customer_id),
            );
        }

        try {
            $customer = new Customer($customer_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
            throw new CreditCardProcessorException($notFound_exception->getMessage);
        }


        $cardType = \App\Libraries\DroplockerObjects\CreditCard::identify($cardNumber);

        //Note, due to sloppy programming in the processor classes, we supress notice errors.
        $data = array(
                "firstName" => $firstName,
                "lastName" => $lastName,
                "cardNumber" => $cardNumber,
                "address" => $address,
                "city" => $city,
                "state" => $state,
                "csc" => $csc,
                "expMonth" => $expirationMonth,
                "expYear" => $expirationYear,
                "cardType" => $cardType,
                "nameOnCard" => "$firstName $lastName",
                "countryCode" => $countryCode,
                "zipcode" => $zipcode,
                "phone_number" => $phone_number,
                "is_one_time" => $is_one_time,
        );

        return $this->authorizeCardData($data, $customer, $enableKioskAccess, null, $testMode);
    }

    /**
     * Authorizes a credit card using the data
     *
     * This is the implementation, called by the other methods.
     *
     * $data array needs the following keys:
     * - cardNumber
     * - expMonth
     * - expYear
     * - cardType
     * - city
     * - state
     * - zipcode
     *
     * @param array $data
     * @param Customer $customer
     * @param bool $enableKioskAccess
     * @param Order $order
     * @param bool $testMode
     * @return array
     */
    public function authorizeCardData($data, $customer, $enableKioskAccess, $order = null, $testMode = null)
    {
        $is_one_time = $data['is_one_time'];
        
        //Get the customer current credit card
        $currentCreditCard = \App\Libraries\DroplockerObjects\CreditCard::search(array(
            "customer_id" => $customer->customerID,
        ));

        if ($is_one_time && !empty($currentCreditCard)) {
            return array(
                "status" => "error",
                "message" => get_translation('one_time_card_already_on_file', "business_account/profile_billing", array(), "Can not use one-time credit card. You already have a card on file ", $customer->customerID),
            );
        }
        
        $CI = get_instance();

        // Create authorization order if not given
        if (!$order) {
            $order = Order::create(array(
                "customer_id" => $customer->customerID,
                "locker_id" => 0, // set to $business->returnToOfficeLockerId in the order
                "orderPaymentStatusOption_id" => 1,
                "orderStatusOption_id" => 3,
                "notes" => "Authorizing Card",
                "business_id" => $customer->business_id,
                "bag_id" => null,
            ));
        }

        // Store a log of the attempted authorization.
        $log = AuthorizationLog::create(array_merge(array(
            "customer_id" => $customer->customerID,
            "order_id" => $order->orderID,
            "business_id" => $customer->business_id,
            "rawData" => serialize($data)
        ), $data));

        // autorize card

        $creditCardProcessor = $this->getCardProcessor($customer->business_id);
        
        $response = @$creditCardProcessor->authorizeCard(
            $data, $order->orderID, $customer->customerID, $customer->business_id
        );

        // If the authorization fails, delete the order and return an error response.
        if ($response->get_status() != "SUCCESS") {
            $order->delete();

            return array(
                "status" => "error",
                "message" => $response->get_message()
            );
        }

        // Set the authorization order status option to completed and payment status to captured
        $order->setProperties(array(
            "statusDate" => date("Y-m-d H:i:s"),
            "orderStatusOption_id" => 10,
            "orderPaymentStatusOption_id" => 3
        ));

        // Create a new order payment status of 'complete' for the authorization order.
        $orderPaymentStatus = OrderPaymentStatus::create(array(
            "order_id" => $order->orderID,
            "orderPaymentStatusOption_id" => 3
        ));

        // Save the credit card
        
        $creditCardData = array(
            "cardNumber" => substr($data['cardNumber'], -4),
            "expMo" => $data['expMonth'],
            "expYr" => $data['expYear'],
            "cardType" => $data['cardType'],
            "customer_id" => $customer->customerID,
            "address1" => $data['address'],
            "state" => $data['state'],
            "city" => $data['city'],
            "zip" => $data['zipcode'],
            "business_id" => $customer->business_id,
            "payer_id" => $response->get_refNumber(),
            "processor" => get_business_meta($customer->business_id, 'processor'),
            "phone_number" => $data['phone_number'],
            "is_one_time" => $is_one_time,
        );
        
        //Get the customer credit card
        $creditCard = \App\Libraries\DroplockerObjects\CreditCard::search(array(
            "customer_id" => $customer->customerID
        ));

        // If the customer does not have any new credit cards, then we create a new Credit Card entity.
        if (empty($creditCard)) {
            $creditCard = new \App\Libraries\DroplockerObjects\CreditCard();
        }
        
        $creditCard->setProperties($creditCardData);

        $note = "Successful authorization";
        
        if ($is_one_time) {
            $note .= ". One time use credit card.";
        } else {
            //Remove any payment hold on the customer account.
            $customer->internalNotes = str_replace("ON PAYMENT HOLD", "", $customer->internalNotes);
            if ($customer->hold == 1) {
                $customer->hold = 0;
                $note .= ", removed payment hold from customer account";
            }
            $customer->autoPay = 1;
            $customer->save();
        }

        // Store the information that we authorized a new card for the customer.
        $customerHistory = CustomerHistory::create(array(
            "note" => $note,
            "customer_id" => $customer->customerID,
            "business_id" => $customer->business_id,
            "historyType" => "Internal Note"
        ));
        
        if (!$is_one_time) {
            // enable kiosk access
            if ($enableKioskAccess) {
                $CI->load->model("kioskaccess_model");
                $CI->kioskaccess_model->update_access(
                    $customer->customerID,
                    $customer->business_id,
                    substr($data['cardNumber'], -4),
                    $data['expMonth'],
                    $data['expYear']
                );
            }

            $CI->load->library("order");
            $results = $CI->order->captureOrdersForCustomer($customer->customerID);
        }
        
        if ($is_one_time) {
            $msg = "Authorized credit card for one time use for customer {$customer->firstName} {$customer->lastName} ({$customer->customerID})";
        } else {
            $msg = "Authorized credit card and attempted to capture all uncaptured orders for customer {$customer->firstName} {$customer->lastName} ({$customer->customerID})";
        }
        
        return array(
            "status" => "success",
            "message" => $msg,
            "data" => $results
        );
    }

    /**
     * Gets the credit card processor for the business
     *
     * @param int $business_id
     * @param bool $testMode
     * @throws CreditCardProcessorException
     * @return DropLocker\Processor\AbstractProcessor
     */
    public function getCardProcessor($business_id, $testMode = null)
    {
        $CI = get_instance();
        $CI->load->library('creditcardprocessor');
        $processor = get_business_meta($business_id, 'processor');
        try {

            $creditCardProcessor = $CI->creditcardprocessor->factory($processor, $business_id);
            if (is_null($testMode) && DEV) {
                $testMode = TRUE;
            }
            if ($testMode) {
                $creditCardProcessor->setTestMode();
            }

        } catch (CreditCardProcessorException $creditCardProcessorException) {
            $business = new Business($business_id);

            //If an credit card processor exception occurs, we send an admin notfication email.
            send_admin_notification("Credit card Processor not configured properly for {$business->companyName}", "Credit card Processor not configured properly for {$business->companyName}. Customers for this business cannot authorize their cards. <br />" . $creditCardProcessorException->getMessage());
            throw $creditCardProcessorException;
        }

        return $creditCardProcessor;
    }
}
