<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use App\Libraries\DroplockerObjects\LockerLoot;

class LockerLoot
{
    private $lockerLootObj;

    public function __construct($lockerLootObj='')
    {
        if (empty($lockerLootObj)) {
            $this->lockerLootObj = new LockerLoot();
        } else {
            $this->lockerLootObj = $lockerLootObj;
        }

    }


    /**
     * applies locker loot to a customer
     */
    public function applyReward()
    {
    }


}
