<?php

class PrinterLibrary
{
    /**
     * Note, the following function is undocumented, unknown *.
     * The business login is in the view of this controller because it was copied over from the old site
     *
     * @todo update this function so the business logic is not in the view
     *
     * @param string $code
     * @param string $short
     */
    public function _print_barcode_receipt($code, $short, $assembly = '')
    {
        //if ($code == 'tags') {
        //    redirect('/admin/admin/print_temp_tags?number=24');
        //    return;
        // }

        $CI = get_instance();

        $request['order'] = '';
        $request['routes'] = '';
        $request['barcode'] = $code;
        $request['showYesterday'] = '';
        $request['short'] = $short;
        $request['dc'] = '';
        $request['orderDate'] = '';
        //$request['partial'] = 1;
        //$request['item'] = '';
        //$request['reason'] = '';
        //$request['assembly'] = $assembly;

        $data['request'] = $request;
        $CI->load->view('admin/print_receipts', $data);

    }

    /**
     * Note, the following function is undocumented, unknown *.
     * @param type $data
     * @return type
     */
    public function print_receipts($data)
    {
        $CI = get_instance();

        return $CI->load->view('admin/print_receipts', $data, true);
    }
    
    /**
    * Refactored from  /admin/printer/print_order_receipt/
    * 
    * Prints the receipt for a particular order.
    *
    * When generating a partial receipt, you must pass the following parameters as a POST rueqest
    *  @param partial int  The nubmer of items witheld
    *  @param item string Any user-specified details about why the order was partialed
    *  @param reason string A user-specified reason why the order was partialed.
    * Note that if the 'partial' parameter is passed, this function will also change the order status to 'Partially Delivered'
    */
    public function get_order_receipt($order_id = null, $business_id = null, $customer_id = null, $partial = array(), $show_payment = FALSE)
    {
        $CI = get_instance();
        
        $message = '';
       
        if (!$business_id) {
            $business_id = $CI->business_id;
        }

        $orderObj = new \App\Libraries\DroplockerObjects\Order($order_id);
        if ($orderObj->locker_id == 0) {
            $message .= "No Receipt Reason: ";
            $message .= $orderObj->notes;

            return array(
                'success' => false,
                'message' => $message,
                'order_id' => $order_id,
            );
        }

        $request['order'] = $order_id;
        $request['routes'] = '';
        $request['barcode'] = '';
        $request['showYesterday'] = '';
        $request['short'] = 0;
        $request['dc'] = '';
        $request['orderDate'] = '';

        //These parametrs are for generating a partial receipt.
        $request['partial'] = $partial["partial"];
        $request['item'] = $partial["item"];
        $request['reason'] = $partial["reason"];

        $data['request'] = $request;

        //If the partial parameter is passed, update the order status.
        $CI->load->model('orderstatus_model');
        $CI->load->model('order_model');
        if ($partial["partial"]) {
            $order = $CI->order_model->get(array("orderID" => $order_id, "business_id" => $business_id));
            //Note, order status option 12 is expected to be "Out for partial delivery".
            $CI->orderstatus_model->update_orderStatus(array(
                "order_id" => $order_id,
                "orderStatusOption_id" => 12,
                "locker_id" => $order[0]->locker_id,
                "employee_id" => get_current_employee_id(),
                "note" => $partial["reason"],
            ));
        }

        //this is only used by boxture service
        $service = get_business_meta($business_id, 'home_delivery_service');
        if (!empty($service)) {
            $homeDelivery = \App\Libraries\DroplockerObjects\OrderHomeDelivery::search(array(
                'order_id' => $order_id,
                'service'  => $service,
            ));
            if (!empty($homeDelivery->delivery_response)) {
                $object = json_decode($homeDelivery->delivery_response);
                if (!empty($object->shipment->lines)) {
                    $data['shipment'] = $object->shipment->lines;
                }
            }
        }

        $data['order'] = new \App\Libraries\DroplockerObjects\Order($order_id);
        
        $data['show_payment'] = $show_payment;
        if ($show_payment) {
            $CI->load->model('transaction_model');
            $data['order_payment'] = $CI->transaction_model->getLastOrderSale($order_id);
        }
        
        return array(
            'success' => true,
            'message' => get_translation("order_receipt_data", "admin/print_receipts", array('order_id' => $order_id), "Generated receipt for order %order_id%", $customer_id),
            'data' => $this->print_receipts($data),
            'order_id' => $order_id,
        );
    }
    
    public function get_orders_receipt_list($order_id_list = array(), $business_id = null, $customer_id = null, $partial = array(), $show_payment = FALSE)
    {
        $success = false;
        
        $message_list = array();
        $data = array(
            'details' => array(),
            'receipt_orders' => array(),
            'error_orders' => array(),
        );
        
        foreach ($order_id_list as $order_id) {
            $receipt_info = $this->get_order_receipt($order_id, $business_id, $customer_id, $partial, $show_payment);
            $data['details'][$order_id] = $receipt_info;
            
            if ($receipt_info['success']) {
                //if we get to generate at least one receipt, success 
                $success = true;
                $data['receipt_orders'][] = $order_id;
            } else {
                $data['error_orders'][] = $order_id;
            }
        }
        
        if ($data['receipt_orders']) {
            $message_list[] = "Receipts generated for orders: " . implode(', ', $data['receipt_orders']) . ".";
        } else {
            $message_list[] = "No receipts have been generated.";
        };
        
        if ($data['error_orders']) {
            $message_list[] = "Could not generate receipts for orders: " . implode(', ', $data['error_orders']) . ".";
            
            foreach ($data['error_orders'] as $no_receipt_order) {
                $message_list[] = "Order $no_receipt_order: {$data['details'][$no_receipt_order]['message']}";
            }
        } 
        
        return array(
            'success' => $success,
            'message' => implode("\n", $message_list),
            'data' => $data,
        );
    }
}
