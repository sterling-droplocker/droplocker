<?php

require_once APPPATH . "third_party/mailchimp-api/MailChimp.php";
require_once APPPATH . "third_party/mailchimp-api/Batch.php";

class Mailchimplibrary
{

    public $business;
    public $api_key;
    public $enabled;
    public $list_name;
    public $list_info;
    public $last_update;
    public $api;
    public $valid;
    public $batchMembers;

    /**
     * Mailchimplibrary constructor.
     *  Required params:
     * - business
     * - api_key
     * - enabled
     * - list_name
     * - last_update
     * @param $params
     */
    public function __construct($params)
    {
        $this->business = $params['business'];
        $this->business_id = $this->business->businessID;
        $this->api_key = $params['api_key'];
        $this->enabled = $params['enabled'];
        $this->list_name = trim($params['list_name']);
        $this->last_update = $params['last_update'];

        $this->valid = false;

        if ($this->enabled) {
            try {
                $this->api = new \DrewM\MailChimp\MailChimp($this->api_key);

                $validateKey = $this->api->get('/');
                $this->valid = empty($validateKey['status']) || ($validateKey['status'] <= 400) ? 1 : 0;
                $this->list_info = $this->get_list_info();
            } catch (Exception $exception) {
                error_log("[Mailchimp API] - Invalid API key: ({$this->api_key}) for Business ID: {$this->business_id}");
            }
        }
    }

    /**
     * Get list stats from mailchimp
     *
     * @return array
     */
    public function get_list_info()
    {
        $results = array(
            'subscribed' => 0,
            'unsubscribed' => 0
        );
        $lists = $this->api->get('/lists');

        $list = array_pop(
            array_map(function($list) {
                return array(
                    'subscribed' => $list['stats']['member_count'],
                    'unsubscribed' => $list['stats']['unsubscribe_count'],
                    'list_id' => $list['id']
                );
            }, array_filter($lists['lists'], function($list) {
                return $list['name'] == $this->list_name;
            }))
        );

        return $list;
    }

    /**
     * Create droplocker list on mailchimp with business information
     *
     * @param $language
     * @return array
     */
    public function createDroplockerList($language)
    {
        $list = $this->api->post('lists', array(
            'name' => $this->list_name,
            'contact' => array(
                'company' => ($this->business->companyName) ? $this->business->companyName : '',
                'address1' => ($this->business->address) ? $this->business->address : '',
                'address2' => ($this->business->address2) ? $this->business->address2 : '',
                'city' => ($this->business->city) ? $this->business->city : '',
                'state' => ($this->business->state) ? $this->business->state : '',
                'country' => Business_Model::countryToISO3166($this->business->country),
                'zip' => ($this->business->zipcode) ? $this->business->zipcode : '',
            ),
            'permission_reminder' => 'You subscribed to ' . $this->business->companyName,
            'campaign_defaults' => array(
                'from_email' => get_business_meta($this->business->businessID, 'customerSupport'),
                'from_name' => get_business_meta($this->business->businessID, 'from_email_name'),
                'subject' => $this->business->companyName . ' List',
                'language' => $language->tag
            ),
            'email_type_option' => false
        ));

        $this->logRequest();

        $this->list_info = $this->get_list_info();

        return $list;
    }

    /**
     * Creates mailchimp batch job to update member data
     *
     * @param array $customers
     */
    public function createBatchUpdateMembers($customers)
    {
        $processed = 0;
        $batch = $this->api->new_batch();

        $customerIDs = array();
        foreach ($customers as $customer) {
            $email = $this->getCustomerEmail($customer->email);
            $memberId = md5(strtolower($email));

            $merge_fields = array(
                'FNAME' => ($customer->firstName) ? $customer->firstName : '',
                'LNAME' => ($customer->lastName) ? $customer->lastName : ''
            );
            foreach ($this->mergeFields as $fieldName => $fieldType) {
                $mailchimpField = strtoupper(substr($fieldName, 0, 10));
                $merge_fields[$mailchimpField] = ($customer->{$fieldName}) ? $customer->{$fieldName} : '';
            }

            unset($customer->email);

            $batch->put(
                $customer->customerID,
                "/lists/{$this->list_info['list_id']}/members/$memberId",
                array(
                    'email_address' => $email,
                    'status' => ($customer->noEmail || $customer->inactive) ? 'unsubscribed' : 'subscribed',
                    'merge_fields' => $merge_fields
                )
            );

            $customerIDs[] = $customer->customerID;
            $processed++;
        }
        $this->batchMembers = $processed;
        $batch->execute();
        $this->logRequest();

        $this->updateCustomerMailchimpSyncDate($customerIDs);
        return $processed;
    }

    /**
     * Create mailchimp custom fields
     *
     */
    public function createMergeFields()
    {
        array_walk($this->mergeFields, function($fieldType, $fieldName) {
           $this->api->post("/lists/{$this->list_info['list_id']}/merge-fields", array(
                'tag' => $fieldName,
                'name' => $fieldName,
                'type' => $fieldType,
                'required'  => false,
                'public' => false
            ));

            $this->logRequest();
        });
    }

    /**
     * set customer mailchimp_sync_date
     */
    protected function updateCustomerMailchimpSyncDate($customerIDs = array())
    {
        if (!empty($customerIDs)) {
            $CI = get_instance();
            $CI->db->query(
                'UPDATE customer SET mailchimp_sync_date = ? WHERE customerID IN ('.implode(",", $customerIDs).')', 
                array(convert_to_gmt_aprax())
            );
        }
    }

    /**
     * Log mailchimp request URL and responses.
     */
    public function logRequest()
    {
        $request = $this->api->getLastRequest();
        $response = $this->api->getLastResponse();

        $CI = get_instance();
        $CI->load->model('mailchimplog_model');

        $CI->mailchimplog_model->business_id = $this->business_id;
        $CI->mailchimplog_model->requestUrl = $request['url'];
        $CI->mailchimplog_model->response = $response['body'];
        $CI->mailchimplog_model->batchMembers = ($this->batchMembers) ? $this->batchMembers : null;
        $CI->mailchimplog_model->insert();
    }


    public function getCustomerEmail($email)
    {
        if (ENVIRONMENT == "development") {
            $email = md5($email) . '@droplocker.com';
        }

        return $email;
    }


    /**
     * Gets amount of pending members update on mailchimp
     *
     * @return int
     */
    public function getPendingBatches() {
        $batches = $this->api->get('/batches', array(
            'status' => 'pending,preprocessing,started',
            'fields' => 'batches.id',
            'count' => '300'
        ));

        if ($batches['batches']) {
            $filters = array();
            foreach ($batches['batches'] as $batch) {
                $filters[] = " response LIKE '%\"id\":\"" . $batch['id'] . "\"%' ";
            }

            $sql = "SELECT SUM(batchMembers) AS totalPending FROM mailchimpLog WHERE " .
                implode($filters, "OR");

            $CI = get_instance();
            $query = $CI->db->query($sql);

            $result = $query->result();
        }

        return isset($result) ? $result[0]->totalPending : 0;
    }

    /**
     * @var array database fields with mailchimp merge field type
     */
    private $mergeFields = array(
        'customerID' => 'text',
        'address1' => 'text',
        'lastOrder' => 'date',
        'location_id' => 'number',
        'route_id' => 'number',
        'avgOrderLapse' => 'number',
        'zip' => 'text',
        'kioskAccess' => 'number',
        'inactive' => 'number',
        'sex' => 'text',
        'totGross' => 'number',
        'visits' => 'number',
        'signupDate' => 'date',
        'orderCount' => 'number',
        'business_id' => 'number',
        'firstOrder' => 'date',
        'city' => 'text',
        'state' => 'text',
        'ip' => 'text',
        'birthday' => 'date',
        'planStatus' => 'text'
    );
}