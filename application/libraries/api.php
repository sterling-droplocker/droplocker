<?php
use App\Libraries\DroplockerObjects\App;
use App\Libraries\DroplockerObjects\ApiSession;

class API
{
    private $CI;
    private $data;

    public function __construct()
    {
        $this->CI = get_instance();

        $this->data = $this->CI->input->get();
    }

    /**
     * creates a signature based on the app secret and data passed from the app. IF the signatures match we have a valid app
     *
     * @param array $data
     * @param string $secretKey
     * @return string signature
     */
    public function generateSignature($data,$secretKey)
    {
        //sort data array alphabetically by key
        ksort($data);

        //combine keys and values into one long string
        $dataString = strtolower(rawurlencode(urldecode(http_build_query($data, 2))));

        $signature = hash_hmac("sha256",$dataString,$secretKey);

        return $signature;
    }


    /**
     * The following function is *, do not use.
     * return an failed message to the app
     *
     * this will terminate processing after the json object is echoed
     *
     * @param string $message
     * @deprecated  the ajax helper function output_ajax_error_response
     */
    public function responseError($message)
    {
        die(json_encode(array('status'=>'fail', 'message'=>$message)));
    }
    /**
     * @deprecated Use the ajax helper function output_ajax_error_response
     * Displays a formatted error response.
     * @param string $message
     */
    public function output_api_error_response($message)
    {
        $CI = get_instance();
        $CI->output->set_content_type('application/json');
        $CI->output->set_output(json_encode(array('status' => 'error', 'message' => $message)));

        return;
    }
    /**
     *
     * Displays a formatted success JSON response.
     * @deprecqated Use the ajax helper function output_api_success_response
     * @param array $data
     * @param string $message
     */
    public function output_api_success_response($data = array(), $message = null)
    {
        $CI = get_instance();
        $CI->output->set_content_type('application/json');
        $CI->output->set_output(json_encode(array('status' => 'success', 'data' => $data, "message" => $message)));

        return;
    }

    /**
     * This is * d not use.
     * @deprecated use the ajax helper fucntion output_api_success_response
     *
     * return a message to the app
     *
     * This will echo the response
     *
     * @param string|array $response
     */
    public function response($response)
    {
        $result['status'] = 'success';
        if (is_string($response)) {
            $result['message'] = $response;
        }
        if (is_array($response) || is_object($response)) {
            foreach ($response as $key=>$value) {
                $result[$key] = $value;
            }
        }

        if (empty($response)) {
            die();
        } else {
            die(json_encode($result));
        }

    }

    /**
     * validates the customers session token
     *
     * @param string $token
     * @return ApiSession
     */
    public function validateSessionToken($token)
    {
        if (empty($token)) {
            return false;
        }

        $apiSession = current(ApiSession::search_aprax(array("token" => $token)));

        return $apiSession ? $apiSession : false;

    }

}
