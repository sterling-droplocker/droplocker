<?php

class LanguageKey_Model extends MY_Model
{
    protected $tableName = 'languageKey';
    protected $primary_key = 'languageKeyID';

    /**
     * Returns all the language keys for the specified language view.
     * @param int $languageViewID
     * @return array
     * @throws \InvalidArgumentException
     */
    public function get_all_for_languageView($languageViewID)
    {
        $this->load->model("languageView_model");
        if ($this->languageView_model->does_exist($languageViewID)) {
            $languageKeys = $this->get_aprax(array("languageView_id" => $languageViewID));

            return $languageKeys;
        } else {
            throw new \InvalidArgumentException("languageViewID $languageViewID does not exist");
        }
    }
}
