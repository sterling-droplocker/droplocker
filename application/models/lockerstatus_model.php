<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class LockerStatus_Model extends My_Model
{
    protected $tableName = 'lockerStatus';
    protected $primary_key = 'lockerStatusID';

    /**
     * returns an array for processing
     */
    public function simple_array()
    {
        $this->db->select('lockerStatusID, name');
        $q = $this->db->get('lockerStatus');
        $res = $q->result();
        foreach ($res as $r) {
            $a[$r->lockerStatusID] = $r->name;
        }

        return $a;
    }

}
