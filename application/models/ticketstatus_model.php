<?php

class TicketStatus_Model extends My_Model
{
    protected $tableName   = 'ticketStatus';
    protected $primary_key = 'ticketStatusID';

    // returns closed status for business
    public function getClosedStatusId($business_id)
    {
        $query = $this->db->query("SELECT ticketStatusID 
                           FROM ticketStatus ts, ticketStatusType tst
                           WHERE 
                           ts.ticketStatusType_id = tst.ticketStatusTypeID AND
                           business_id = $business_id AND
                           tst.description = 'Closed' ");
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }


        $data = $query->result();
        if (sizeof($data) > 0) {
            return $data[0]->ticketStatusID;
        } else {
            return 0;
        }
    }

    // returns open status for business
    public function getOpenStatusId($business_id)
    {
        $query = $this->db->query("SELECT ticketStatusID 
                           FROM ticketStatus ts, ticketStatusType tst
                           WHERE 
                           ts.ticketStatusType_id = tst.ticketStatusTypeID AND
                           business_id = $business_id AND
                           tst.description = 'Open' ");
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        $data = $query->result();
        if (sizeof($data) > 0) {
            return $data[0]->ticketStatusID;
        } else {
            return 0;
        }
    }

    // returns open status for business
    public function getNewStatusId($business_id)
    {
        $query = $this->db->query("SELECT ticketStatusID 
                           FROM ticketStatus ts, ticketStatusType tst
                           WHERE 
                           ts.ticketStatusType_id = tst.ticketStatusTypeID AND
                           business_id = $business_id AND
                           tst.description = 'InitialState' ");
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        $data = $query->result();
        if (sizeof($data) > 0) {
            return $data[0]->ticketStatusID;
        } else {
            return 0;
        }
    }

    // return all non new ticket status
    // aka all status that a ticket can be moved into
    public function get_non_new_status($business_id)
    {

        $newStatusId = $this->getNewStatusId($business_id);
        $query       = $this->db->query("SELECT ticketStatusID, description
                                   FROM ticketStatus ts
                                   WHERE 
                                   ticketStatusId != $newStatusId AND
                                   business_id = $business_id
                                   ");
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $query->result();
    }

    function getWithStatusTypes($business_id)
    {
        $sql = 'SELECT t.*,ts.description as ticketStatusTypeDescription FROM '.$this->tableName.' t ';
        $sql.= 'INNER JOIN ticketStatusType ts ON t.ticketStatusType_id = ts.ticketStatusTypeID ';
        $sql.= 'WHERE t.business_id = '.$business_id;
        $query = $this->db->query($sql);

        return $query->result();
     

    }
}