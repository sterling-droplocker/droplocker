<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class TicketLabel_Model extends My_Model
{
    protected $tableName = 'ticketLabel';
    protected $primary_key = 'ticketLabelID';

    public function get($options = array())
    {
        if (isset($options['business_id'])) {
            $this->db->where('business_id', $options['business_id']);
        }

        $query = $this->db->get('ticketLabel');

        return $query->result();

    }

}
