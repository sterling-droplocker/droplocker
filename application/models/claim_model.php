<?php
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Claim;

class Claim_Model extends My_Model
{
    protected $tableName = 'claim';
    protected $primary_key = 'claimID';

    /**
    * get_claim_by_location method finds a claim by location ID and locker Name
    * in the claim table we only store the lockerID
    * Delivery men only see the lockername so we have to find the claimID with a couple joins
    *
    * @param int location_id
    * @param int locker_id
    * @return array of objects
    */
    public function get_claim_by_location($location_id, $locker_id)
    {
        $this->load->model('locker_model');
        $locker = $this->locker_model->get(array('location_id'=>$location_id, 'lockerName'=>$locker_id,'select'=>'lockerID'));
        $this->locker_id = $locker[0]->lockerID;
        $claim = $this->get();

        return $claim[0];
    }

    /**
     * Retrieves all claims for the specified locker
     * @deprecated This function is deprecated.
     *
     * @param int $locker_id
     * @return array An array of claim objects
     * @throws Exception
     */
    public function get_claims_by_locker($locker_id)
    {
        if (!is_numeric($locker_id)) {
            throw new Exception("'locker_id' must be an integer.");
        }
        $this->db->select("*");
        $this->db->where(array("locker_id" => $locker_id, 'active' => 1));
        $claims = $this->db->get("claim");

        return $claims->result();
    }

    /**
     * Retrieves all claims for the specified customer
     *
     * @param int $customer_id
     */
    public function get_claims_by_customer($customer_id, $order_by = array())
    {
        $this->db->select("*");
        if (!empty($order_by)) {
            $this->db->order_by($order_by["column"], $order_by["direction"]);
        }
        $this->db->where(array("customer_id" => $customer_id, 'active' => 1));
        $claims = $this->db->get("claim");

        return $claims->result();
    }

    /**
    * get the customer that made the claim
    *
    * @param int $claimID
    * @return array of objects (customer)
    */
    public function get_claim_customer($claimID)
    {
            $this->db->join("customer", "customerID=customer_id");
            $this->db->where('claimID', $claimID);
            $this->db->select("customerID, firstName, lastName");
            $query = $this->db->get('claim');

            return $query->result();
    }

    /**
    * when processing an order, we need to de-activate the original claim on a locker
    * Only orders that have a customer ID will de-activate the claim.
    * This function sets active to 0 and updates the updated column
    *
    * This method simply sets the active field to 0
    * I could have called it using ->update() but it is
    * easier to identify in the controllers
    *
    * @param int $claimID
    * @return int affected_rows = 1
    */
    public function deactivate_claim($claimID, $employee_id = null)
    {
        $options = array();
        $options['updated'] = convert_to_gmt_aprax();
        $options['active'] = 0;
        $options['employee_id'] = $this->get_claim_employee_id($claimID, $employee_id);
        
        return $this->update_all($options, array("claimID" => $claimID));
    }
    
    private function get_claim_employee_id($claimID, $employee_id)
    {
        if (!empty($employee_id)) {
            return $employee_id;
        }
        
        $employee_id = intval($this->session->userdata("employee_id"));
        if (!empty($employee_id)) {
            return $employee_id;
        }
        
        $buser_id = $this->session->userdata('buser_id');
        if ($buser_id) {
            $employee_id = get_employee_id($buser_id);
            if (!empty($employee_id)) {
                return $employee_id;
            }       
        }
        
        $query = $this->db->query('SELECT employee_id FROM droid_postPickups WHERE claim_id = ? LIMIT 0,1', array($claimID));
        $droid_pickup = $query->result();
        if (!empty($droid_pickup)) {
            return $droid_pickup[0]->employee_id;
        }
        
        return null;
    }

    /**
     * Deactivates a customer's active claims
     * @param int $customer_id
     */
    public function deactivate_customer_claims($customer_id)
    {
        return $this->update_all(array(
            'active' => 0,
            'updated' => gmdate('Y-m-d H:i:s')
        ), array(
            'customer_id' => $customer_id,
            'active' => 1,
        ));
    }

    /**
     * Gets a claim using memcache. If memcache is not installed, this performs just like $this->get
     * when passing only the claimID
     *
     * If no_cache is set, this method will not even try to use memcache. It will just do a normal db call
     *
     *
     * @param int $claim_id
     * @return multitype:
     */
    public function get_claim($claim_id)
    {
        $sql = "SELECT claimID,
                        lockerID,
                        locationID,
                        lockerName,
                        companyName,
                        address,
                        address2,
                        city,
                        state,
                        zipcode,
                        claim.updated,
                        claim.created,
                        claim.active,
                        claim.notes
                        FROM claim
                        INNER JOIN locker ON lockerID = locker_id
                        INNER JOIN location on locationID = location_id
                        WHERE claimID = ".$claim_id;


        $q = $this->db->query($sql);

        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $q->result();
    }


    /**
    * overrides get
     * Note that this funciton will always search ONLY for active claims.
    *
    * Options Values
    * ----------------------
    * business_id
    * active = 0 or 1
    * claimID
    * customer_id
    * select 		//used for select statement
    * order_by
    * with_customer //joins the customer table
    * with_locker 	//joins the locker table
    *
    * Required Values
    * ------------------
    * business_id
    *
    * @param array $options
    * @throw Exception missing required option values
    * @return array of objects
    */
    public function get($options = array())
    {
        if (!required(array('business_id'), $options)) {
                throw new Exception("Business ID is required");
        }

        // options['active'] will override the following db setting
        $this->db->where("active", 1);

        if (isset($options['business_id'])) {$this->db->where('claim.business_id',$options['business_id']);}
        if (isset($options['active'])) {$this->db->where('claim.active',$options['active']);}
        if (isset($options['claimID'])) {$this->db->where('claimID',$options['claimID']);}
        if (isset($options['customer_id'])) {$this->db->where('customer_id',$options['customer_id']);}
        if (isset($options['locker_id'])) {$this->db->where('locker_id',$options['locker_id']);}
        if (isset($options['select'])) {$this->db->select($options['select']);}
        if (isset($options['order_by'])) {$this->db->order_by($options['order_by']);}


        if (isset($options['with_customer'])) {
            $this->db->join('customer', 'customerID = claim.customer_id');
        }

        if (isset($options['with_locker'])) {
            $this->db->join("locker", "lockerID = claim.locker_id");
            $this->db->join("location", "locationID = locker.location_id");
            $this->db->join("locationType", "location.locationType_id = locationType.locationTypeID");
        }

        $query = $this->db->get('claim');

        return $query->result();

    }

    public function get_inactive($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->db->select("serviceType, address, lockerName, customerID, firstName, lastName, updated, claimID, order_notes");
        $this->db->join("customer", "customer.customerID = claim.customer_id");
        $get_inactive_claims_query = $this->db->get_where($this->tableName, array("business_id" => $business_id, "active" => 0));

        return $get_inactive_claims_query->result();

    }

    /** creates a new claim
        *
        * $options values
        * ------------------
        * customer_id
        * locker_id
        * business_id
        * orderType
        * order_notes
        *
        * Required Values
        * ----------------
        * locker_id
        * business_id
        * customer_id
        *
        * @param array $options
        * @return int claim_id
        */
    public function new_claim($options = array())
    {
        if (!required(array('locker_id'), $options)) {
            throw new Exception("Missing Locker ID");
        }

        if (!isset($options['business_id'])) {
            $options['business_id'] = $this->business_id;

            if (empty($options['business_id'])) {
                throw new Exception("Missing business id");
            }
        }

        if (!isset($options['customer_id'])) {
            $options['customer_id'] = $this->customer_id;

            if (empty($options['customer_id'])) {
                throw new Exception("Missing customer id");
            }
        }

        $locker_id = $options['locker_id'];
        $locker = new Locker($locker_id, FALSE);

        $business_id = $options['business_id'];
        $business = new Business($business_id, FALSE);

        $notes = '';
        if (!empty($options['orderType'])) {
            $this->load->model('servicetype_model');
            $orderTypes = unserialize($options['orderType']);
            $serviceTypes = $this->servicetype_model->get(array('serviceTypeID'=>$orderTypes));
            $businessServiceType = unserialize($business->serviceTypes);
            $str = '';
            foreach ($serviceTypes as $serviceType) {
                $name = $serviceType->name;
                if (!empty($businessServiceType[$serviceType->serviceTypeID])) {
                    $name = $businessServiceType[$serviceType->serviceTypeID]['displayName'];
                }
                $str .= $name . ", ";
            }
            $notes .= get_translation("order_type", "customer_notes", array(), "ORDER TYPE:", $options['customer_id']) . ' ' . rtrim($str, ", ")."\n";
        }

        //get the notes
        if (array_key_exists("order_notes", $options)) {
            $notes .= get_translation("this_order_only", "customer_notes", array(), "THIS ORDER ONLY:", $options['customer_id']) . ' ' . $options['order_notes']."\n";
        }

        if (array_key_exists("recurring_claim_notes", $options)) {
            $notes = $options['recurring_claim_notes']."\n";
            unset($options['recurring_claim_notes']);
        }

        unset($options['order_notes']);
        $options['notes'] = $notes;

        $options['created'] = $options['updated'] = gmdate("Y-m-d H:i:s");

        $this->db->insert('claim',$options);

        return $this->db->insert_id();
    }

    /**
     * Checks if a claim exists in a particular locker.
     * @param int $locker_id
     * @param int $business_id
     * @return type
     */
    public function does_claim_exist($locker_id, $business_id)
    {
        if (!is_numeric($locker_id)) {
            throw new Exception("'locker_id' must be an integer.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be an integer.");
        }
    $claim = $this->get(array('locker_id'=>$locker_id, 'business_id'=>$business_id, 'active'=>1));

    if ($claim) {
            return true;
    } else {
            false;
    }

    }



    /**
     * Searches for active claimed orders by the customer last name, customer first name, and/or claim address for the specified business
     *
     * @param string $firstName the customer's first name
     * @param string $lastName the customer's last name
     * @param string $address the customer's street address
     * @return array an array of claim objects
     */
    public function search($firstName = "", $lastName = "", $address = "", $business_id, $active = 1)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->db->select("location.locationID, location.address, location.serviceType, claimID, claim.locker_id, lockerName, claim.updated, customerID,firstName, lastName, claim.notes as order_notes, customer.customerNotes as customer_notes, claim.created, location.route_id, claim.orderType, customer.email, customer.phone, customer.address1");
        $this->db->join("locker", "locker.lockerID=claim.locker_id");
        $this->db->join("location", "location.locationID=locker.location_id");
        $this->db->join("customer", "claim.customer_id=customer.customerID", "left");

        if (!empty($firstName)) {
            $this->db->like("customer.firstName", $firstName);
        }
        if (!empty($lastName)) {
            $this->db->like("customer.lastName", $lastName);
        }
        if (!empty($address)) {
            $this->db->like("location.address", $address);
        }

        $this->db->where("claim.business_id", $business_id);
        $this->db->where("claim.active", $active);
        $this->db->order_by("updated DESC");
        $search_claims_query = $this->db->get("claim");

        return $search_claims_query->result();
    }

    /**
     * Gets the customer active claims
     *
     * @param int $customer_id
     * @param int $business_id
     * @return array an array of claim objects
     */
    public function getCustomerActiveClaims($customer_id, $business_id)
    {
        $sql = 'SELECT claimID, lockerName, 
                    COALESCE(orderHomePickup.address1, location.address) AS address, claim.updated, claim.notes, 
                    orderHomePickup.pickupDate, orderHomePickup.windowStart, orderHomePickup.windowEnd
                FROM (claim)
                JOIN locker ON (lockerID = claim.locker_id)
                JOIN location ON (locationID = locker.location_id)
                LEFT JOIN orderHomePickup ON (claimID = orderHomePickup.claim_id)
                WHERE claim.active =  1
                AND claim.customer_id =  ?
                AND claim.business_id =  ?';

        $claims = $this->db->query($sql, array($customer_id, $business_id))->result();
        $claims["windowStart"] = convert_from_gmt_aprax($claims["windowStart"], $date_format, $business_id);
        return $claims;
    }
    
    public function getLockerName($claim_id)
    {
        $claim = new Claim($claim_id, FALSE);
        $locker = new Locker($claim->locker_id, FALSE);
        
        return $locker->lockerName;
    }
}
