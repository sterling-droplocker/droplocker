<?php

class Email_Model extends My_Model
{
    protected $tableName = 'email';
    protected $primary_key = 'emailID';

    /**
     * The following function returns the total number of emails for a particular business.
     * @param int $business_id
     * @return array
     */
    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $email_count = $this->db->query("SELECT COUNT(emailID) AS total FROM {$this->tableName} WHERE business_id = ?", array($business_id))->row();

        return $email_count->total;
    }
}
