<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class TicketProblem_Model extends My_Model
{
    protected $tableName = 'ticketProblem';
    protected $primary_key = 'ticketProblemID';

    public function get($options = array())
    {
        if (isset($options['order_by'])) {$this->db->order_by($options['order_by'], 'asc');}
        $query = $this->db->get('ticketProblem');

        return $query->result();

    }

}
