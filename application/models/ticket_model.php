<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Ticket_Model extends My_Model
{
    protected $tableName = 'ticket';
    protected $primary_key = 'ticketID';

    /**
     *
     */
    public function get($options = array())
    {
        if (isset($options['ticket.business_id'])) {$this->db->where('ticket.business_id',$options['ticket.business_id']);}
        if (isset($options['ticketID'])) {$this->db->where('ticketID', $options['ticketID']);}
        if (isset($options['status'])) {$this->db->where('ticket.status', $options['status']);}
        if (isset($options['not_status'])) {$this->db->where_not('ticket.status', $options['where_not']);}
        if (isset($options['where_in_status'])) {$this->db->where_in('ticket.status', $options['where_in_status']);}
        if (isset($options['select'])) {$this->db->select($options['select']);}
        if (isset($options['order_by'])) {$this->db->order_by($options['order_by'], 'desc');}

        $this->db->join('bag', 'bagID = bag_id','left');
        $this->db->join('locker', 'lockerID = ticket.locker_id','left');
        $this->db->join('location', 'locationID = ticket.location_id','left');
        $this->db->join('customer', 'customerID = ticket.customer_id', 'left');


        $query = $this->db->get('ticket');

        return $query->result();
    }

    public function update($options, $where)
    {
        $this->db->update('ticket', $options, $where);

        return $this->db->affected_rows();
    }

    public function delete($options = array())
    {
        $this->db->delete('ticket', $options);
        if ($this->db->affected_rows()) {
            $this->db->query("DELETE FROM ticketNote WHERE ticket_id = {$options['ticketID']}");
        }
    }

}
