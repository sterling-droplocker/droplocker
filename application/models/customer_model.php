<?php
use App\Libraries\DroplockerObjects\Customer;
class Customer_Model extends MY_Model
{
    protected $tableName = 'customer';
    protected $primary_key = 'customerID';

    /**
     * Retrieves all the orders for the specified customer
     * @param int $customerID
     * @param string $orderBy
     * @param string $orderDirection (DESC, ASC)
     * @param int $limit - number of rows to return.
     * @param int $offset - starting row.
     * @return array
     * @throws \Database_Exception
     */
    public function getOrderHistory($customerId, $orderBy = "", $orderDirection = "", $limit = 0, $offset = 0)
    {
        $sql = "
            SELECT orderID, dateCreated, bagNumber, bag.notes, orderPaymentStatusOption_id, name, 0 isClaim
            FROM `orders`
            LEFT OUTER JOIN bag
                ON orders.bag_id=bag.bagID
            LEFT OUTER JOIN orderStatusOption
                ON orderStatusOption.orderStatusOptionID=orderStatusOption_id
            WHERE `orders`.`customer_id` = ?
            UNION
            SELECT claimID, created, null, null, null, 'Active', 1 isClaim
            FROM claim
            WHERE customer_id = ? and active = 1
        ";
        
        if (!empty ($orderBy)) {
            $sql .= "ORDER BY $orderBy $orderDirection";
        }
        
        if ( $limit > 0 ) {
           $sql .= " LIMIT $offset,$limit";
        }
        
        $query = $this->db->query($sql, array($customerId, $customerId));
        
        return $query->result();
    }
    
    /**
     * Retrieves the count of orders for the specified customer
     * @param int $customerID
     * @return int
     * @throws \Database_Exception
     */
    public function getOrderHistoryCount($customerId)
    {
        $sql = "
            SELECT COUNT(orderId) orderCount
            FROM
            (
                SELECT orderId
                FROM orders
                WHERE orders.customer_id = ?
                UNION 
                SELECT claimId AS orderId
                FROM claim
                WHERE customer_id = ? 
                  AND active = 1
            ) main
        ";
        
        $query = $this->db->query($sql, array($customerId, $customerId));
        
        $results = $query->result();
        
        return $results[0]->orderCount;
    }
    
    
    
    /**
     * @deprecated Use getOrderHistory instead.
     * Retrieves all the orders for the specified customer
     * @param int $customerID
     * @param bool $exclude_authorization_transactions Exclude any authorization transaction orders from the result set
     * @return array
     * @throws \Database_Exception
     */
    public function get_order_history($customerID, $exclude_authorization_transactions = false)
    {
        if (!is_numeric($customerID)) {
            throw new \InvalidArgumentException("'customer_id' must be numeric");
        }
        $this->db->select("orderID, orders.business_id, dateCreated, bagNumber, bag.notes, orderPaymentStatusOption_id, orderStatusOption_id, name");
        $this->db->join("bag", "orders.bag_id=bag.bagID", 'left');
        $this->db->join("orderStatusOption", "orderStatusOption.orderStatusOptionID=orderStatusOption_id", "left");

        if ($exclude_authorization_transactions) {
            $this->db->join("transaction", "orderID=transaction.order_id", "left");
            $this->db->where("transaction.type !=", 'auth');
        }
        $this->db->where("orders.customer_id", $customerID);
        $this->db->order_by("orderID", 'DESC');
        $get_order_history_query = $this->db->get("orders");
        $orders = $get_order_history_query->result();
        $this->load->model('order_model');
        foreach ($orders as $index => $order) {
            $orders[$index]->order_type = $this->order_model->getOrderType($order->orderID);
        }

        return $orders;
    }
    /**
     * Deletes a customer from the database if they have no orders.
     * @deprecated Use the delete function for the Droplocker Object instead.
     *
     * @param int $customerID
     * @param int $business_id
     * @return int The number of rows deleted
     * @throws Exception
     */
    public function delete($customerID, $business_id)
    {
        if (!is_numeric($customerID)) {
            throw new Exception("'customerID' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        //The following query checks to see if the customer has any orders.
        $this->db->select("orderID");
        $this->db->join("orders", "orders.customer_id = customer.customerID");
        $this->db->where(array("customer.customerID" => $customerID, "orders.business_id" => $business_id));
        $this->db->from($this->tableName);
        if ($this->db->count_all_results() > 0) {
            throw new Exception("Can not delete customer because this customer has orders.");
        }

        return $this->db->delete($this->tableName, array("customerID" => $customerID, "business_id" => $business_id));
    }
    /**
     * Retrieves the total number of real orders for a particular customer
     * @param int $customer_id
     * @return int
     */
    public function get_customer_order_count($customerID)
    {
        if (!is_numeric($customerID)) {
            throw new Exception("'customerID' must be numeric.");
        }
        $this->db->select("COUNT(orderID) AS total");
        $this->db->where(array(
            "customer_id" => $customerID,
            "bag_id !=" => 0,
        ));


        $order_count_query = $this->db->get("orders");
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
        $order_counts = $order_count_query->result();

        return $order_counts[0]->total;

    }
    /**
     * Returns the creation date of the most recent order placed by the specified customer.
     * @param int $customerID
     * @return string Returns an empty string if the customer has no recent order
     * @throws Exception
     */
    public function get_last_order_date($customerID)
    {
        if (!is_numeric($customerID)) {
            throw new Exception("'customerID' must be numeric.");
        } else {
            $this->db->select("dateCreated");
            $this->db->where("customer_id", $customerID);
            $this->db->where("bag_id != ", 0);
            $this->db->order_by("dateCreated", "DESC");
            $this->db->limit(1);
            $orders_query = $this->db->get("orders");
            $last_order = $orders_query->row();

            return $last_order ? $last_order->dateCreated : null;
        }
    }
    /**
    * Logs in a customers
    *
    * A customer can login with either email or username
    *
    * @param string loginString - either email or username
    * @param string password
    * @param int business_id
    * @return object
    */
    public function login($loginString, $password, $business_id)
    {
        $login = $loginString;

        $sql = "SELECT customerID FROM customer WHERE password = ?
                    AND business_id = ?
                    AND (username = ? OR email = ?);";

        $query = $this->db->query($sql, array($password,$business_id, $login, $login));
        $customer = $query->row();

        return $customer;
    }

    /**
     * The following function is  . Do not use.
    * @deprecated This functionality should exist in the save function of the Customer Droplocker Object.
    *
    * Adds a new customer to the application
    *
    * @param astring $email
    * @param string $password
    * @param int $business_id
    * @param string (Optional) $how
    * @param string (Optional) $signup_method
    * @throws Exception
    * @return int insert_id()
    */
    public function new_customer($email, $password, $business_id, $how = "", $signup_method = null)
    {
        if (empty($email)) {
            throw new Exception("'email' can not be empty.");
        }
        if (empty($password)) {
            throw new Exception("'password' can not be empty.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }

        $this->load->model('product_model');
        $defaultWF = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $business_id);
        $this->db->insert('customer', array(
            "email" => $email,
            "defaultWF" => $defaultWF,
            "business_id" => $business_id,
            "how" => $how,
            "signup_method" => $signup_method,
            'password' => md5($password),
            'ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']:""
        ));
        $insert_id =  $this->db->insert_id();

        return $insert_id;
    }


    /**
     * Returns all customer fields.
     *
     * @param int $customerID If not specified, then the client's customer ID is used.
     * @return Customer a customer object
     */
    public function get_customer_dump($customerID)
    {
        $this->db->select("*");
        $this->db->where("customerID", $customerID);
        $customer_query = $this->db->get("customer");
        $customer_query_result = $customer_query->result();

        return $customer_query_result[0];
    }

    /**
     * gets the total number of customers or total number of customer in a business
     *
     * @param int $bid Optional
     * @return int
     */
    public function customerCount($business_id = '')
    {
        $business_id = ($business_id!='')?$business_id:$this->business_id;
        $getCustCount = "SELECT count(customerID) as total FROM customer where business_id = $this->business_id";
        $q = $this->db->query($getCustCount);
        $total = $q->result();

        return $total[0]->total;
    }

    /**
     * gets the total number of customers or total number of customer who have placed an order in a business
     *
     * @param int $bid Optional
     * @return int
     */
    public function customerCountOrder($business_id = '')
    {
        $business_id = ($business_id!='')?$business_id:$this->business_id;
        $getCustCount = "select count(distinct customer_id) as total from orders where business_id = $this->business_id";
        $q = $this->db->query($getCustCount);
        $total = $q->result();

        return $total[0]->total;
    }

    /**
     * @deprecated Use get_by_primary_key in MY_Model instead
     * Retrieves the customer's information.
     *
     * @param int $customer_id If no parameter is passed, then we search by using the client user's customer ID.
     * @return array An array containing Customer model objects
     */
    public function get_customer($customer_id)
    {
        if ($customer_id == '') {
            throw new Exception("Missing customer ID");
        }

        $sql_string = "SELECT location_id, customer.business_id,customerID, email, firstName, lastName, concat(firstName, ' ', lastName) as fullName, customer.address1 as address, customer.address2, customer.city, customer.state, customer.zip, customer.phone, customer.lat, lon, username, sms, customerNotes, internalNotes, permAssemblyNotes, creditCard.cardNumber, creditCard.expMo, creditCard.expYr, pendingEmailUpdate, invoice FROM customer LEFT JOIN creditCard ON creditCard.customer_id = customer.customerID WHERE customerID = ";

        $query = $this->db->query($sql_string.$customer_id);
        $customers = $query->result();
        foreach ($customers as $key => $customer) {
            $customers[$key]->fullName = getPersonName($customer);
        }
        
        return $customers;
    }

    /**
     * Update customer notes
     *
     * If notes exists, put a log in customer history
     *
     * @param int $customer_id
     * @param string $customerNotes
     */
    public function update_customerNotes($customer_id, $customerNotes)
    {
        $customer = new Customer($customer_id);

        // if saving new notes and the customer has old notes
        if ($customer->customerNotes && $customer->customerNotes != $customerNotes) {

            // save old notes in the customer history
            $this->load->model('customerhistory_model');
            $this->customerhistory_model->insert($customer->customerNotes, $customer_id,
                'Customer Note', $customer->business_id);
        }

        $customer->customerNotes = $customerNotes;
        $customer->save();
    }

    /**
     * updatePermAssemblyNotes method will add a note to the customers perm Assembly Notes. If the
     * customer already has a note, that note is archived in the customerHistory table
     *
     * @param string $note
     * @param int $customer_id
     * @param int $business_id
     * @return int affected_rows
     */
    public function updatePermAssemblyNotes($note, $customer_id, $business_id)
    {
        $customer = new Customer($customer_id);
        $oldNote = $customer->permAssemblyNotes;
        $customer->permAssemblyNotes = $note;
        $id = $customer->save();

        if ($id && $oldNote!='') {
            $this->load->model('customerhistory_model');
            $insert_id = $this->customerhistory_model->insert($oldNote, $customer_id, 'Assembly Note', $business_id);
        }

        return $id;
    }

    public function update_internalNotes($customer_id, $notes, $business_id)
    {
        $field = 'internalNotes';
        $customer = new Customer($customer_id);
        
        if ($customer->business_id != $business_id) {
            throw new Exception("Customer #$customer_id does not belong to business $business_id");
        }

        // if saving new notes and the customer has old notes
        if ($customer->$field && $customer->$field != $notes) {

            // save old notes in the customer history
            $this->load->model('customerhistory_model');
            $this->customerhistory_model->insert($customer->$field, $customer_id,
                $field, $customer->business_id);
        }

        $customer->$field = $notes;
        $customer->save();
    }
    
    /**
     * simply uses a get, but I have to use like so I added a method
     *
     * @param string $word
     * @param integer $business_id
     * @return array of Objects
     */
    public function autocomplete($word, $business_id)
    {
        $word = '%'.$word.'%';
        $sql = "SELECT firstName, lastName, customerID, address1, city, state, zip, phone, email FROM customer WHERE CONCAT(TRIM(firstName), ' ', TRIM(lastName)) like ?
                AND business_id = ? AND email != '' limit 20";
        $query = $this->db->query($sql, array($word, $business_id));

        return $query->result();
    }

    /**
     * Customizable version of autocomplete
     */
    public function autocomplete_full($word, $business_id)
    {
        $separator = '~';
        
        $search_fields = array(
            'customerID',
            'firstName',
            'lastName',
            'phone',
            'email',
        );
        
        $search_list = implode(', ', array_map(function($val){
            return "TRIM($val)";
        }, $search_fields));
        
        $word = '%' . preg_replace('!\s+!', $separator, $word) . '%';
        $sql = "SELECT firstName, lastName, customerID, address1, city, state, zip, phone, email FROM customer 
                WHERE CONCAT_WS('$separator', $search_list) like ?
                AND business_id = ? AND email != '' limit 20";
        $query = $this->db->query($sql, array($word, $business_id));

        return $query->result();
    }

    /**
     * returns the customers locker loot balance
     *
     * @param unknown_type $customer_id
     * @return float
     */
    public function get_locker_loot_balance($customer_id)
    {
        $this->load->model('lockerLoot_model');

        return $this->lockerLoot_model->get_balance($customer_id);
    }

    /**
     * The following function is *, do not use.
     * @deprecated This function is undocumented and any uses of it should be removed.
        * get_business_customers gets customers for a business
        * I made this because there are many different ways a customer
        * can be connected to a business.
        *
        * Currently this only gets customers that have a current connection to a location for the business_id
        *
        * Options Values
        * -------------------
        * business_id
        * limit
        * offset
        * search_string
        *
        * @param array $options
        * @return array of objects
        */
    public function get_business_customers($options = array())
    {
        if (!required(array('business_id'), $options)) {
                return array('status'=>'fail', "message"=>'Missing required field');
        }

        $this->db->where('customer.business_id',$options['business_id']);
        
        if (isset($options['only_active'])) {
            $this->db->where('customer.inactive','0');
        }
        
        if (isset($options['select'])) {$this->db->select($options['select']);}

        //$this->db->join('customer_location', 'customer_location.customer_id = customer.customerID');
        //$this->db->join('location', 'location.locationID = customer_location.location_id');
        //$this->db->distinct('customer.username');

        if (isset($options['sort'])) {$this->db->order_by($options['sort']);}

        if (isset($options['with_orders'])) {
                $this->db->join('orders', 'orders.customer_id = customerID', 'left');
        }

        if (!empty($options["with_search"])) {
            $options["with_search"] = $this->db->escape_like_str($options["with_search"]);
            $this->db->where("(email LIKE '".$options["with_search"]."%' OR firstName LIKE '".$options["with_search"]."%' OR lastName LIKE '".$options["with_search"]."%')");
        }

        if (isset($options['limit'])) {
                $offset = (isset($options['offset']))?$options['offset']:0;
                $this->db->limit($options['limit'], $offset);
        }

        $query = $this->db->get('customer');

        return $query->result();

    }


    /**
     * get_order_customer gets a customer by joining an order
     *
     * @param int $order_id
     * @return array of objects
     */
    public function get_order_customer($order_id)
    {
        $this->db->where('orderID', $order_id);
        $this->db->join("orders", 'orders.customer_id = customer.customerID');
        $query = $this->db->get('customer');

        return $query->result();
    }

    /**
    * It's possible for more then 1 customer to be connected to an item.
    *
    * @param int $item_id
    * @param array of objects
    */
    public function get_item_customers($item_id)
    {

        // first we need to see if this item is owned by more then one person
        $this->load->model('customer_item_model');
        $this->customer_item_model->item_id = $item_id;
        $customers = $this->customer_item_model->get();

        if ($customers) {
            foreach ($customers as $c) {
                $array[] = $c->customer_id;
            }
        }
        //print_r($customers);
        $this->db->where_in('customerID', $array);
        $query = $this->db->get('customer');

        return $query->result();
    }

    /**
     * Options Values
     * -----------------------
     * firstName
     * lastName
     * address
     * phone
     * username
     * email
     * business_id
     *
     * @param array $options
     * @return array of stdClass objects
     */
    public function search_customer($options = array())
    {
        $options = array_map('trim', $options);
        if (array_key_exists("customerID", $options)) {
            $this->db->where("customerID", $options['customerID']);
        }
        if (array_key_exists("firstName", $options)) {
            $this->db->like("firstName", $options['firstName']);
        }
        if (array_key_exists("lastName", $options)) {
            $this->db->like("lastname", $options['lastName']);
        }
        if (array_key_exists("address", $options)) {
            $this->db->like("address1", $options['address']);
        }

        if (array_key_exists("phone", $options)) {
            $this->db->like("phone", $options['phone']);
        }
        if (array_key_exists("sms", $options)) {
            $this->db->like("sms", $options['phone']);
        }

        if (array_key_exists("phone_or_sms", $options)) {
            $number = $this->db->escape('%' . $options['phone_or_sms'] . '%');
            $this->db->where("(phone LIKE $number OR sms LIKE $number OR sms2 LIKE $number)");
        }

        if (array_key_exists("username", $options)) {
            $this->db->like("username", $options['username']);
        }
        if (array_key_exists("email", $options)) {
            $this->db->like("email", $options["email"]);
        }
        if (array_key_exists("customerNotes", $options)) {
            $this->db->like("customerNotes", $options["customerNotes"]);
        }

        if (array_key_exists("business_id", $options)) {
            $this->db->where("customer.business_id", $options['business_id']);
        }

        $this->db->where("email != ", "");
        $this->db->order_by("firstName");
        $this->db->order_by("lastName");
        $this->db->limit(100);
        $get_customers_query = $this->db->get("customer");

        return $get_customers_query->result();
    }

    /**
     * returns the total number or orders for a specified customer
     *
     * @param int $customer_id
     * @return int
     */
    public function get_order_count($customer_id='')
    {
        $this->db->select("count(customer_id) as ordCount");
        $this->db->where("customer_id = $customer_id and bag_id <> 0");
        $query = $this->db->get('orders');
        $t = $query->result();

        return $t[0]->ordCount;
    }

    /**
     * returns the total value of all Completed orders for a specified customer
     *
     * @param int $customer_id
     * @return decimal
     */
    public function get_order_total($customer_id)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        $this->db->select("sum(closedGross) as gross, sum(closedDisc) as disc");
        $this->db->where("customer_id = $customer_id and bag_id <> 0");
        $query = $this->db->get('orders');
        $t = $query->result();

        return $t[0];
    }

    /**
     * @depreacted This functionality belongs in the credit card model
     * returns the customer's credit card info
     *
     * @param int $customer_id
     * @return array of objects
     * @todo update function to return a single row, not an array
     */
    public function get_creditcard($customer_id='')
    {
        $this->load->model('creditcard_model');
        $this->creditcard_model->select='cardNumber, expMo, expYr';
        $this->creditcard_model->customer_id = $customer_id;

        return $this->creditcard_model->get();
    }

    /**
     * returns the customer's last order
     *
     * @param int $customer_id
     * @return array of objects
     */
    public function get_lastOrder($customer_id='')
    {
        $sqlLastOrder = "SELECT orders.*, address, lockerType
            FROM orders
            join locker on lockerID = locker_id
            join location on locationID = location_id
            join locationType on locationTypeID = locationType_id
            WHERE orderID = (select Max(orderID) from orders WHERE `customer_id` = $customer_id and bag_id <> 0)";
        $query = $this->db->query($sqlLastOrder);

        return $query->row();
    }



    /**
     * @deprecated This function is deprecated, use the customer history model instead
     *
     * returns all the customer's history
     *
     * @param int $customer_id
     * @return array of objects
     */
    public function get_history($customer_id)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        $this->load->model('customerhistory_model');
        $this->customerhistory_model->customer_id = $customer_id;
        $this->customerhistory_model->business_id = $this->business_id;
        $this->db->join('employee', 'employee_id = employeeID');

        return $this->customerhistory_model->get();
    }

    /**
    * The following function is *, do not use.
     * @deprecated This funciton is undocumented and any uses should be refactored.
    * pulled from old code. Used in the admin/mobile functions
    *
    * @param int $customer_id
    * @return array $info
    */
    public function orderInfo($customer_id)
    {
        $sql = "select
            count(distinct orderID) as numOrd,
            sum(closedGross) as ordAmt,
            sum(closedDisc) as discAmt,
            max(orders.dateCreated) as lastOrder
            from orders
            where customer_id  = $customer_id
            and bag_id > 1
            group by customer_id;";
        $select = $this->db->query($sql);
        $res = $select->result();
        $res = @$res[0];

        if (isset($res->lastOrder)) {
            $date = mktime(0,0,0,date("m"),date("d"),date("Y")); //Gets Unix timestamp for current date
            $timestamp = strtotime($res->lastOrder);
            $date2 = mktime(0,0,0,date("m", $timestamp), date("d", $timestamp), date("Y", $timestamp)); //Gets Unix timestamp for current date
            $difference = $date-$date2; //Calcuates Difference
            $days2 = floor($difference /60/60/24); //Calculates Days Old
            $info['daysSince'] = $days2.' days ago';
        } else {
            $info['daysSince'] = 'NO ORDERS';
        }
        
        $info['ordAmt']     = @$res->ordAmt;
        $info['discAmt']    = @$res->discAmt;
        $info['numOrd']     = @$res->numOrd;
        $info['lastOrder']  = @$res->lastOrder;
        if ($info['daysSince'] == 'NO ORDERS') {
            $info['lastOrder']  = '-';
        }
        
        return $info;
    }

    /**
     * The following function is *, do not use.
     * @deprecated This functionality of the Customer Droplocker object shoudl be used instead.
     * @param int $id The customer ID
     * @param array $properties
     */
    public function update_properties($id, $properties)
    {
        $this->db->update_batch($this->tableName, $properties, $id);
    }

    /**
     * checks to see if the customer has a location set.
     * The customer cannot place an order if they do not have any locations
     *
     * @param int $customer_id
     * @return boolean
     */
    public function isDefaultLocationSet($customer_id)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }

        $customers = $this->get_customer($customer_id);
        $customer = $customers[0];

        if ((empty($customer->location_id))) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Checks to see if the customer profile is complete.
     * A customer cannot place a new order if their profile is not complete
     *
     * @param int $customer_id
     * @throw excption if customer is not found
     * @return boolean
     */
    public function isProfileComplete($customer_id)
    {
        if (!is_numeric($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' must be numeric.");
        }
        $sql = 'SELECT firstName, lastName, phone
                FROM customer
                WHERE customerID = '.$customer_id;
        $query = $this->db->query($sql);
        $profile = $query->row();
        if (!$profile) {
            throw new Exception("Customer not found");
        }

        if (!required(array('firstName', 'lastName', 'phone'), $profile)) {
            return FALSE;
        } else {
            return TRUE;
        }

        return FALSE;
    }

    public function isOnPaymentHold($customer_id)
    {
        $onHoldStatusId = 7;

        $q = "SELECT count(*) qty FROM orders WHERE customer_id = ? AND orderStatusOption_id = ?";
        $onHoldOrders = $this->db->query($q, array($customer_id, $onHoldStatusId))->result();

        return $onHoldOrders[0]->qty > 0;
    }
    
    public function getCustomerForBusiness($customer_id, $business_id)
    {
        $success = true;
        $message = '';
        $customer = null;
        
        try {
            $customer = new Customer($customer_id, false);
            
            $this->load->model('creditCard_model');
            $customer->creditCard = $this->creditCard_model->getPublicInfo($customer_id, $business_id);
        } catch (Exception $e) {
            $success = false;
            $message = "Customer [$customer_id] does not exist";
        }

        if ($customer->business_id != $business_id) {
            $success = false;
            $message = "This customer does not belong to your business.";
        }

        return array(
            'success' => $success,
            'message' => $message,
            'data' => $customer,
        );
    }
    
}
