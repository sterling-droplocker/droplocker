<?php

class ItemPreference_Model extends My_Model
{
    protected $tableName = 'itemPreference';
    protected $primary_key = 'itemPreferenceID';

    /**
     * Save the item preferences
     *
     * preferences should be an array of product_id.
     *
     * @param int $item_id
     * @param array $preferences
     */
    public function save_preferences($item_id, $preferences)
    {
        // delete old preferences
        $this->delete_aprax(array('item_id' => $item_id));

        foreach ($preferences as $name => $product_id) {

            // check if active and can be selected
            $query = $this->db->query("SELECT productCategory_id
                FROM product
                JOIN product_process ON (productID = product_id)
                JOIN productCategory ON (productCategoryID = productCategory_id)
                WHERE product_id = ?
                AND productType = 'preference'
                AND product_process.active = 1", array($product_id));

            // skip if not found
            if (!$category = $query->row()) {
                throw new Exception("$product_id is not a valid option for an item preference.");
            }

            $this->save(array(
                'item_id' => $item_id,
                'productCategory_id' => $category->productCategory_id,
                'product_id' => $product_id,
                'updated' => gmdate('Y-m-d H:i:s')
            ));
        }
    }


    /**
     * Gets the productID for each item preference
     *
     * @param int $item_id
     * @return array
     */
    public function get_preferences($item_id)
    {
        $sql = "SELECT productCategory.slug, itemPreference.product_id
            FROM itemPreference
            JOIN product_process USING (product_id)
            JOIN productCategory ON (productCategoryID = productCategory_id)
            WHERE item_id = ?
                AND product_process.active = 1";

        $query = $this->db->query($sql, array($item_id));
        $products = $query->result();

        $preferences = array();
        foreach ($products as $product) {
            $preferences[$product->slug] = $product->product_id;
        }

        return $preferences;
    }


    /**
     * Gets the product name for each item preference
     *
     * @param int $item_id
     * @return array
     */
    public function get_preferences_names($item_id)
    {
        $sql = "SELECT productCategory.name AS categoryName, product.displayName
            FROM itemPreference
            JOIN productCategory ON (productCategoryID = productCategory_id)
            JOIN product ON (productID = itemPreference.product_id)
            JOIN product_process USING (product_id)
            WHERE item_id = ?
                AND product_process.active = 1";

        $query = $this->db->query($sql, array($item_id));
        $products = $query->result();

        $preferences = array();
        foreach ($products as $product) {
            $preferences[$product->categoryName] = $product->displayName;
        }

        return $preferences;
    }


    /**
     * Gets the product for each item preference
     *
     * @param int $item_id
     * @return array
     */
    public function get_preferences_data($item_id)
    {
        $sql = "SELECT productCategory.name AS categoryName, product.*, product_process.price
            FROM itemPreference
            JOIN productCategory ON (productCategoryID = productCategory_id)
            JOIN product ON (productID = itemPreference.product_id)
            JOIN product_process USING (product_id)
            WHERE item_id = ?
                AND product_process.active = 1";

        $query = $this->db->query($sql, array($item_id));
        $products = $query->result();

        $preferences = array();
        foreach ($products as $product) {
            $preferences[$product->categoryName] = $product;
        }

        return $preferences;
    }

}
