<?php
use App\Libraries\DroplockerObjects\Customer;
class DataDumpsLog_Model extends MY_Model
{
    protected $tableName = 'dataDumpsLog';
    protected $primary_key = 'dataDumpsLogID';

    public function get_by_business($options = array())
    {
        if (!required(array('business_id'), $options)) {
            throw new Exception("Missing business_id required field");
        }

        $this->db->join('employee', 'employee_id=employeeID', 'INNER');
        $this->db->join('business', 'business_id=businessID', 'INNER');
        $this->db->where($this->tableName.'.business_id',$options['business_id']);


        if (isset($options['limit'])) {
            $offset = isset($options['offset']) ? $options['offset']:0;
            $this->db->limit($options['limit'], $offset);
        }

        $query = $this->db->get('dataDumpsLog');
        return $query->result();
    }

    public function countTotal($business_id = null)
    {
        $business_id = ($business_id != null) ? $business_id:$this->business_id;
        $count = "SELECT count(dataDumpsLogID) as total FROM dataDumpsLog where business_id = $this->business_id";
        $q = $this->db->query($count);
        $total = $q->result();
        return $total[0]->total;
    }

    public function save($options = array())
    {
        $this->options = $options;
        return $this->insert();
    }
}
