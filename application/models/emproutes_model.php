<?php
class EmpRoutes_Model extends My_Model
{
    protected $tableName = 'empRoutes';
    //TODO: we don't have support for multi column primary key
    protected $primary_key = array('employee_id', 'route_id');

}
