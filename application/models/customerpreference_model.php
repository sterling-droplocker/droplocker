<?php

class CustomerPreference_Model extends My_Model
{
    protected $tableName = 'customerPreference';
    protected $primary_key = 'customerPreferenceID';

    /**
     * Save the customer preferences
     *
     * preferences should be an array of product_id.
     *
     * @param int $customer_id
     * @param array $preferences
     */
    public function save_preferences($customer_id, $preferences)
    {
        // delete old preferences
        $this->delete_aprax(array('customer_id' => $customer_id));

        foreach ($preferences as $name => $product_id) {

            // check if active and can be selected
            $query = $this->db->query("SELECT productCategory_id
                FROM product
                JOIN product_process ON (productID = product_id)
                JOIN productCategory ON (productCategoryID = productCategory_id)
                WHERE product_id = ?
                AND productType = 'preference'
                AND product_process.active = 1", array($product_id));

            // skip if not found
            if (!$category = $query->row()) {
                throw new Exception("$product_id is not a valid option for a customer preference.");
            }

            $this->save(array(
                'customer_id' => $customer_id,
                'productCategory_id' => $category->productCategory_id,
                'product_id' => $product_id,
            ));
        }
    }


    /**
     * Gets the productID for each customer preference in a module
     *
     * @param int $customer_id
     * @param int $module_id
     * @return array
     */
    public function get_preferences($customer_id, $module_id = null)
    {
        $sql = "SELECT productCategory.slug, customerPreference.product_id
            FROM customerPreference
            JOIN product_process USING (product_id)
            JOIN productCategory ON (productCategoryID = productCategory_id)
            WHERE customer_id = ?
                AND product_process.active = 1
                ";

        $params = array($customer_id);

        if ($module_id) {
            $sql .= "AND productCategory.module_id = ?";
            $params[] = $module_id;
        }

        $query = $this->db->query($sql, $params);
        $products = $query->result();

        $preferences = array();
        foreach ($products as $product) {
            $preferences[$product->slug] = $product->product_id;
        }

        return $preferences;
    }

    /**
     * Gets the product name for each customer preference
     *
     * @param int $customer_id
     * @param int $module_id
     * @return array
     */
    public function get_preferences_names($customer_id, $module_id = null)
    {
        $sql = "SELECT productCategory.name AS categoryName, product.displayName
            FROM customerPreference
            JOIN product_process USING (product_id)
            JOIN productCategory ON (productCategoryID = productCategory_id)
            JOIN product ON (productID = customerPreference.product_id)
            WHERE customer_id = ?
                AND product_process.active = 1
                ";

        $params = array($customer_id);

        if ($module_id) {
            $sql .= "AND productCategory.module_id = ?";
            $params[] = $module_id;
        }

        $query = $this->db->query($sql, $params);
        $products = $query->result();

        $preferences = array();
        foreach ($products as $product) {
            $preferences[$product->categoryName] = $product->displayName;
        }

        return $preferences;
    }

    /**
     * Gets the product name for each customer preference that matches a productCategory
     *
     * @param int $customer_id
     * @param int $parent_id
     * @return array
     */
    public function get_preferences_names_for_cateogry($customer_id, $parent_id, $module_id = 3)
    {
        $data = $this->get_preferences_data_for_category($customer_id, $parent_id, $module_id);

        return array_map(function ($a) { return $a->displayName; }, $data);
    }

    /**
     * Gets the product for each customer preference that matches a productCategory
     *
     * @param int $customer_id
     * @param int $parent_id
     * @return array
     */
    public function get_preferences_data_for_category($customer_id, $parent_id, $module_id = 3)
    {
        $sql = "SELECT productCategory.name AS categoryName, product.*, product_process.price
            FROM customerPreference
            JOIN product_process USING (product_id)
            JOIN productCategory ON (productCategoryID = productCategory_id)
            JOIN product ON (productID = customerPreference.product_id)
            WHERE customer_id = ?
                AND product_process.active = 1
                AND (productCategory.parent_id IN (0, ?) OR productCategory.parent_id IS NULL)
                AND productCategory.module_id = ?
                ";
        $params = array($customer_id, $parent_id, $module_id);

        $query = $this->db->query($sql, $params);
        $products = $query->result();

        $preferences = array();
        foreach ($products as $product) {
            $preferences[$product->categoryName] = $product;
        }

        return $preferences;
    }

}
