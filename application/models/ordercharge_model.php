<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class OrderCharge_Model extends My_Model
{
    protected $tableName = 'orderCharge';
    protected $primary_key = 'orderChargeID';

        /**
         *Retreives all the charges for a particular order
         * @param int $order_id
         * @return array An array consisting of the charge type and charge amount for each charge.
         */
        function getCharges($order_id)
        {
            $this->select="chargeType,chargeAmount";
            $this->order_id = $order_id;

            return $this->get();
        }

}
