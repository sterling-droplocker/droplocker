<?php

class Image_Model extends My_Model
{
    protected $tableName = 'image';
    protected $primary_key = 'imageID';

    /**
     * Retrieves a single image by the specified ID and business.
     * @deprecated
     * @param type $imageID
     * @param type $business_id
     * @return Image_Model
     */
    public function get_by_id($imageID, $business_id)
    {
        $get_image_query = $this->db->get_where($this->tableName, array("imageID" => $imageID, "business_id" => $business_id));
        $images = $get_image_query->result();

        return $images[0];
    }

//
    /**
     * Retrieves all the images for the specified buisness that are assigned to a particular placement.
     * @param int $business_id The ID of the business from which retrieve the logo placement
     * @return array Image ID is the key, and all placements associated with each image are an array within
     */
    public function get_business_placements($business_id)
    {
        $placements= $this->db->query("SELECT image_placement.image_id,image_placement.placement_id FROM image INNER JOIN image_placement ON image.imageID = image_placement.image_id WHERE business_id=$business_id");

        $result = array();
        foreach ($placements->result() as $placement) {
            $result[$placement->image_id][] = $placement->placement_id;
        }

        return $result;
    }
    /**
     * Returns all the images associated with a particular business
     * @param int $business_id The business ID
     * @return array
     */
    public function get_images($business_id)
    {
        $images = $this->db->query("SELECT * FROM images WHERE business_id=$business_id");

        return $images->result();
    }
    /**
     * Returns the image assigned to a placement for a particular business.
     * @param string $name The name of the placement
     * @return string The image's URL path, or null if there is no image associated with the placement
     */
    public function get_placement_path($place, $business_id = NULL)
    {
        if (!$business_id) {
            $business_id = $this->business_id;
        }
        
       $this->db->select("filename");
       $this->db->join("image_placement", "image.imageID=image_placement.image_id");
       $this->db->join("placement", "placement.placementID=image_placement.placement_id");
       $this->db->where("placement.place", $place);
       $this->db->where("business_id", $business_id);
       $get_image_placement_query = $this->db->get("image");

       $image = $get_image_placement_query->result();
       if (!empty($image)) {
           return sprintf("%s/%s", "/images/logos", $image[0]->filename);
       } else {
           return null;
       }

    }

    /**
     * Returns an array of images with no placement assigned.
     * @param int $business_id.
     * @return array
     */
    public function get_images_without_placement($business_id)
    {
        $sql = "SELECT * FROM image i
                  LEFT JOIN image_placement ip ON i.imageID = ip.image_id
                  WHERE i.business_id = ?
                  AND ip.image_id IS NULL";

        return $this->db->query($sql, array($business_id))->result();
    }
}
