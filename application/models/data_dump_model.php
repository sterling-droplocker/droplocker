<?php
class Data_Dump_Model extends MY_Model
{
    private $reporting_database = null;
    private $selectedOptions = array();

    public function dump_by_location($data = array(), $business)
    {
        $this->reporting_database = $this->load->database("reporting", true);
        if (DEV) {
            $this->reporting_database = $this->db;
        }

        $date_y = $data['end_year'];
        $date_m = $data['end_month'];
        $current_time = mktime(0, 0, 0, $data['end_month'], date('d'), $data['end_year']);

        for ($i = 0; $i <= $data["numMonths"] -1; $i = $i + $data["increment"]) {
            $totComm = 0;

                //show the right heading
                switch ($data["grouping"]) {
                    case 'quarter':

                    $month = $date_m - $i;
                    $time1 = mktime(0, 0, 0, $month -0, 1,  $date_y);
                    $time2 = mktime(0, 0, 0, $month -1, 1,  $date_y);
                    $time3 = mktime(0, 0, 0, $month -2, 1,  $date_y);

                    $date1 = date("Y-n", $time1);
                    $date2 = date("Y-n", $time2);
                    $date3 = date("Y-n", $time3);

                    $this->selectedOptions["selectQuarter"] = "'$date1', '$date2', '$date3'";
                    $data['headers'][$i] = date("M 'y", $time1)." to ".date("M 'y", $time3);
                    break;

                    case 'week':
                    $end = date("m/d/y", strtotime("this sunday", $current_time));
                    $endUnix = date("Y-m-d 23:59:59", strtotime("this sunday", $current_time));

                    $start = date("m/d/y", strtotime("$end - 7 days"));
                    $startUnix = date("Y-m-d 00:00:00", strtotime("$endUnix - 7 days"));

                    $dayJump = $i * 7;
                    $start = date("m/d/y", strtotime("$start - $dayJump days"));
                    $startUnix = date("Y-m-d 00:00:00", strtotime("$startUnix - $dayJump days"));

                    $end = date("m/d/y", strtotime("$end - $dayJump days"));
                    $endUnix = date("Y-m-d 23:59:59", strtotime("$endUnix - $dayJump days"));
                    
                    $this->selectedOptions["startUnix"] = $startUnix;
                    $this->selectedOptions["endUnix"] = $endUnix;
                    $this->selectedOptions["selectWeek"] = date("YW", strtotime($end));
                    $data['headers'][$i] = $end." - ".$start;

                    break;

                    case '4week':
                    $end = date("m/d/y", strtotime("this sunday", $current_time));
                    $start = date("m/d/y", strtotime("$end - 28 days"));

                    $dayJump = $i * 28;
                    $start = date("m/d/y", strtotime("$start - $dayJump days"));
                    $end = date("m/d/y", strtotime("$end - $dayJump days"));

                    $w1 = date("YW", strtotime($end));
                    $w2 = date("YW", strtotime("$end - 7 days"));
                    $w3 = date("YW", strtotime("$end - 14 days"));
                    $w4 = date("YW", strtotime("$end - 21 days"));

                    $this->selectedOptions["selectRange"] = $w1.','.$w2.','.$w3.','.$w4;
                    $data['headers'][$i] = $end." - ".$start;
                    break;

                    case 'month':
                    $time1 = mktime(0, 0, 0, $date_m - $i, 1,  $date_y);
                    $this->selectedOptions["selectMonth"] = date("Y-n", $time1);
                    $data['headers'][$i] = date("M 'y", $time1);
                    break;
                }

            $firstCustomers = $this->getFirstCustomers($business, $data["grouping"]);

            foreach ($firstCustomers as $firstCustomer) {
                $data['firstCustomers'][$i] = empty($data['firstCustomers'][$i])?0:$data['firstCustomers'][$i];
                $data['firstCustomers'][$i] = $data['firstCustomers'][$i] + $firstCustomer->firstCust;
                $data['custFirstCategory'][$firstCustomer->reportingClass][$i] = $firstCustomer->firstCust;
            }

            $signups = $this->getSignups($business, $data["grouping"]);
            $data['signups'][$i] = $signups[0]->firstCust;

            $unique = $this->getUniqueCustomers($business, $data["grouping"]);
            $data['uniqueCusts'][$i] = $unique[0]->customers;

            $allData = $this->getAllData($business, $data["grouping"]);

            foreach ($allData as $a) {
                if (!empty($data['locList'])) {
                    if ($data['locList'] != $a->reportingClass) {
                        continue;
                    }
                }

                $data['title'][$a->locationID] = substr($a->companyName, 0, 35);
                $data['address'][$a->locationID] = substr($a->address, 0, 20);
                $data['units'][$a->locationID] = $a->units;
                $data['route'][$a->locationID] = $a->route_id;
                $data['zipcode'][$a->locationID] = $a->zipcode;

                    //get locker count
                    $totLockers = 0;
                $lockerCount = $this->getLockerCount($business, $a->locationID);
                foreach ($lockerCount as $lc) {
                    $data['lockers'][$a->locationID][$lc->type] = $lc->numLock;
                    $types = array('WF', 'DC', 'Combo', 'Unknown', 'Electronic');
                    if (in_array($lc->type, $types)) {
                        $data['totLockers'] = empty($data['totLockers'])?0:$data['totLockers'];
                        $data['totLockers'] += $lc->numLock;
                        $totLockers = $data['totLockers'];
                    }
                }

                $data['repClass'][$a->locationID] = $a->reportingClass;
                $data['serviceType'][$a->locationID] = $a->serviceType;
                if ($a->lowTarget > 0 or $a->highTarget > 0) {
                    $data['target'][$a->locationID] = number_format($a->lowTarget / 1000, 1, '.', '').'K/'.number_format($a->highTarget / 1000, 1, '.', '').'K';
                } else {
                    $data['target'][$a->locationID] = $a->custom1;
                }

                    //calculate utilization
                    $util = 0;
                $utilMeas = '';

                    /* TODO: get formula to calculate utilization by lockers */
                    $data['utilization'][$a->locationID] = "-";
                if ($a->serviceMethod == 'Lockers') {
                    /*
                        $utilMeas = $a->numOrds;
                        if($totLockers > 0)
                            $util = $a->numOrds/($totLockers * 12) * 100;
                        else
                            $util = 0;
                        */
                } else {
                    if ($a->units != 0) {
                        $util = $a->numCust/$a->units * 100;
                        $data['utilization'][$a->locationID] = $a->numCust .'<br/>('.number_format($util, 2, '.', '').'% )';
                    }
                }


                //store the monthly revenue per location
                $data['location'][$a->locationID][$i] = $a->totOrds;
                $data['gross'][$i] = empty($data['gross'][$i])?0:$data['gross'][$i];
                $data['gross'][$i] = $data['gross'][$i] + $a->totOrds;
                $data['tax'][$i] = empty($data['tax'][$i])?0:$data['tax'][$i];
                $data['tax'][$i] += !empty($a->tax) ? $a->tax:0;
                $data['numOrders'][$i] = empty($data['numOrders'][$i])?0:$data['numOrders'][$i];
                $data['numOrders'][$i] += $a->numOrds;
                $data['custCountCategory'][$a->reportingClass][$i] = empty($data['custCountCategory'][$a->reportingClass][$i])?0:$data['custCountCategory'][$a->reportingClass][$i];
                $data['custCountCategory'][$a->reportingClass][$i] += $a->numCust;
                $data['countNumOrders'][$a->reportingClass][$i] = empty($data['countNumOrders'][$a->reportingClass][$i])?0:$data['countNumOrders'][$a->reportingClass][$i];
                $data['countNumOrders'][$a->reportingClass][$i] += $a->numOrds;
                $data['reportingClass'][$a->reportingClass][$i] = empty($data['reportingClass'][$a->reportingClass][$i])?0:$data['reportingClass'][$a->reportingClass][$i];
                $data['reportingClass'][$a->reportingClass][$i] += $a->totOrds;
            }

            if (!empty($this->selectedOptions["selectMonth"])) {
                $data['selectMonth'] = $this->selectedOptions["selectMonth"];
            }

            $discounts = $this->getDiscounts($business, $data["grouping"]);
            foreach ($discounts as $d) {
                $data['discounts'][$i] = empty($data['discounts'][$i])?0:$data['discounts'][$i];
                $data['discounts'][$i] = $data['discounts'][$i] + $d->charges;
            }

            $data['gross'][$i] = empty($data['gross'][$i])?0:$data['gross'][$i];
            $data['net'][$i] = $data['gross'][$i] + $data['discounts'][$i];
        }

                //data that is compared to prior month
        for ($i = 0; $i <= $data["numMonths"] -1; $i++) {
            $priorMonth = $i + 1;
            $data['firstCustomers'][$i] = empty($data['firstCustomers'][$i])?0:$data['firstCustomers'][$i];
            $data['uniqueCusts'][$i] = empty($data['uniqueCusts'][$i])?0:$data['uniqueCusts'][$i];
            $data['uniqueCusts'][$priorMonth] = empty($data['uniqueCusts'][$priorMonth])?0:$data['uniqueCusts'][$priorMonth];
            $data['lostCusts'][$i] = $data['firstCustomers'][$i] - ($data['uniqueCusts'][$i] - $data['uniqueCusts'][$priorMonth]);

            if (!empty($data['custCountCategory'])) {
                foreach ($data['custCountCategory'] as $class => $value) {
                    $data['custFirstCategory'][$class][$i] = empty($data['custFirstCategory'][$class][$i])?0:$data['custFirstCategory'][$class][$i];
                    $data['custCountCategory'][$class][$priorMonth] = empty($data['custCountCategory'][$class][$priorMonth])?0:$data['custCountCategory'][$class][$priorMonth];
                    $data['lostCustCategory'][$class][$i] = $data['custFirstCategory'][$class][$i] - ($data['custCountCategory'][$class][$i] - $data['custCountCategory'][$class][$priorMonth]);
                }
            }

            if ($data['uniqueCusts'][$priorMonth]) {
                $data['retentionRate'][$i] = (1-($data['lostCusts'][$i] / $data['uniqueCusts'][$priorMonth]));
                if (!empty($data['custCountCategory'])) {
                    foreach ($data['custCountCategory'] as $class => $value) {
                        $data['retentionRateCategory'][$class][$i] = (1-($data['lostCustCategory'][$class][$i] / $data['custCountCategory'][$class][$priorMonth]));
                    }
                }

                if (!empty($data['uniqueCusts'][$i]) && !empty($data['retentionRate'][$i])) {
                    $data['lifetime'][$i] = ($data['net'][$i] / $data['uniqueCusts'][$i])*($data['retentionRate'][$i]/(1-$data['retentionRate'][$i]));
                }
            }
        }

        $results = array();
        if (!empty($data["reportingClass"])) {
            foreach ($data["reportingClass"] as $key => $value) {
                for ($i = 0; $i <= $data["numMonths"] -1; $i = $i + $data["increment"]) {
                    $results["totals"][$key][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $value[$i]);
                }
            }
        }

        for ($i = 0; $i <= $data["numMonths"] -1; $i = $i + $data["increment"]) {
            $data["gross"][$i] = empty($data["gross"][$i])?0:$data["gross"][$i];
            $results["totals"]["totals"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["gross"][$i]);
            
            $data["discounts"][$i] = empty($data["discounts"][$i])?0:$data["discounts"][$i];
            $results["totals"]["discounts"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["discounts"][$i]);
            
            $data["net"][$i] = empty($data["net"][$i])?0:$data["net"][$i];
            $results["totals"]["totals_w_discounts"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["net"][$i]);
            
            $data["signups"][$i] = empty($data["signups"][$i])?0:$data["signups"][$i];
            $results["totals"]["signups"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["signups"][$i]);
            
            $data["uniqueCusts"][$i] = empty($data["uniqueCusts"][$i])?0:$data["uniqueCusts"][$i];
            $results["totals"]["uniqueCusts"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["uniqueCusts"][$i]);
            
            $data["firstCustomers"][$i] = empty($data["firstCustomers"][$i])?0:$data["firstCustomers"][$i];
            $results["totals"]["firstCustomers"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["firstCustomers"][$i]);
            
            $data["lostCusts"][$i] = empty($data["lostCusts"][$i])?0:$data["lostCusts"][$i];
            $results["totals"]["lostCusts"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["lostCusts"][$i]);
            
            $data["numOrders"][$i] = empty($data["numOrders"][$i])?0:$data["numOrders"][$i];
            $results["totals"]["numOrders"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["numOrders"][$i]);
            
            $data["retentionRate"][$i] = empty($data["retentionRate"][$i])?0:$data["retentionRate"][$i];
            $results["totals"]["retentionRate"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["retentionRate"][$i]);
            
            $data["lifetime"][$i] = empty($data["lifetime"][$i])?0:$data["lifetime"][$i];
            $results["totals"]["lifetime"][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $data["lifetime"][$i]);
        }

        $data["location"] = empty($data["location"])?array():$data["location"];
        foreach ($data["location"] as $key => $l) {
            $results["locations"][$data["title"][$key]]["title"] = $data["title"][$key];
            $results["locations"][$data["title"][$key]]["address"] = $data["address"][$key];
            $results["locations"][$data["title"][$key]]["units"] = $data["units"][$key];
            $results["locations"][$data["title"][$key]]["lockers"] = isset($data["lockers"][$key]) ? array_sum($data["lockers"][$key]) : 0;
            $results["locations"][$data["title"][$key]]["type"] = $data["repClass"][$key];
            $results["locations"][$data["title"][$key]]["frequency"] = $data["serviceType"][$key];
            $results["locations"][$data["title"][$key]]["utilization_rate"] = $data["utilization"][$key];
            $results["locations"][$data["title"][$key]]["route"] = $data["route"][$key];
            $results["locations"][$data["title"][$key]]["zipcode"] = $data["zipcode"][$key];
            $results["locations"][$data["title"][$key]]["custom1"] = isset($data["target"][$key]) ? $data["target"][$key] : "";

            for ($i = 0; $i <= $data["numMonths"] -1; $i = $i + $data["increment"]) {
                if ($l[$i]) {
                    if (empty($this->selectedOptions["selectMonth"]
                            )) {
                        $results["locations"][$data["title"][$key]][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $l[$i]);
                    } else {
                        $results["locations"][$data["title"][$key]][$data["headers"][$i]] = format_money_symbol($business->businessID, '%.2n', $l[$i]);
                    }
                }
            }
        }
        return $results;
    }

    protected function getFirstCustomers($business, $grouping)
    {
        $getFirstCustomers = "select count(customer_id) as firstCust, reportingClass from(
        SELECT distinct `orders`.customer_id, min(orders.dateCreated) as firstOrder, locationType.name as reportingClass
        FROM `orders`
        join locker on lockerID = orders.locker_id
        join location on locker.location_id = locationID
        join locationType on locationType_id = locationTypeID
        where bag_id <> 0
        and orders.business_id = $business->businessID
        group by customer_id) t2 ";

        switch ($grouping) {
            case 'quarter':
            $getFirstCustomers .= "where concat(year(convert_tz(firstOrder, 'GMT', '".$business->timezone."')),'-', month(convert_tz(firstOrder, 'GMT', '".$business->timezone."'))) in (".$this->selectedOptions["selectQuarter"]."
            )";
            break;
            case 'month':
            $getFirstCustomers .= "where concat(year(convert_tz(firstOrder, 'GMT', '".$business->timezone."')),'-', month(convert_tz(firstOrder, 'GMT', '".$business->timezone."'))) = '".$this->selectedOptions["selectMonth"]."
            '";
            break;
            case 'week':
            $getFirstCustomers .= "where convert_tz(firstOrder, 'GMT', '".$business->timezone."') >= '".$this->selectedOptions["startUnix"]."
            ' and convert_tz(firstOrder, 'GMT', '".$business->timezone."') <= '".$this->selectedOptions["endUnix"]."
            '";
            break;
            case '4week':
            $getFirstCustomers .= "where yearweek(convert_tz(firstOrder, 'GMT', '".$business->timezone."')) in (".$this->selectedOptions["selectRange"]."
            )";
            break;
        }
        $getFirstCustomers .= " group by reportingClass";

        $q = $this->reporting_database->query($getFirstCustomers);
        return $q->result();
    }

    protected function getSignups($business, $grouping)
    {
        //find out how many signups there were
        $getSignups = "select count(customerId) as firstCust
        from customer
        where business_id = $business->businessID ";

        switch ($grouping) {
            case 'quarter':
            $getSignups .= "and concat(year(convert_tz(signupDate, 'GMT', '".$business->timezone."')),'-', month(convert_tz(signupDate, 'GMT', '".$business->timezone."'))) in (".$this->selectedOptions["selectQuarter"]."
            )";
            break;
            case 'month':
            $getSignups .= "and concat(year(convert_tz(signupDate, 'GMT', '".$business->timezone."')),'-', month(convert_tz(signupDate, 'GMT', '".$business->timezone."'))) = '".$this->selectedOptions["selectMonth"]."
            '";
            break;
            case 'week':
            $getSignups .= "and (convert_tz(signupDate, 'GMT', '".$business->timezone."') >= '".$this->selectedOptions["startUnix"]."
            ' AND convert_tz(signupDate, 'GMT', '".$business->timezone."') <= '".$this->selectedOptions["endUnix"]."
            ')";
            break;
            case '4week':
            $getSignups .= "and yearweek(convert_tz(signupDate, 'GMT', '".$business->timezone."')) in (".$this->selectedOptions["selectRange"]."
            )";
            break;
        }

        $q = $this->reporting_database->query($getSignups);
        return $q->result();
    }

    protected function getUniqueCustomers($business, $grouping)
    {
        //find out how many unique customers
        $getUnique = "select count(distinct customer_id) as customers
        from orders
        where orders.business_id = $business->businessID
        and bag_id <> 0 ";

        switch ($grouping) {
            case 'quarter':
            $getUnique .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$business->timezone."'))) in (".$this->selectedOptions["selectQuarter"]."
            )";
            break;
            case 'month':
            $getUnique .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$business->timezone."'))) = '".$this->selectedOptions["selectMonth"]."
            '";
            break;
            case 'week':
            $getUnique .= "and (convert_tz(dateCreated, 'GMT', '".$business->timezone."') >= '".$this->selectedOptions["startUnix"]."
            ' and convert_tz(dateCreated, 'GMT', '".$business->timezone."') <= '".$this->selectedOptions["endUnix"]."
            ')";
            break;
            case '4week':
            $getUnique .= "and yearweek(convert_tz(dateCreated, 'GMT', '".$business->timezone."')) in  (".$this->selectedOptions["selectRange"]."
            )";
            break;
        }

        $q = $this->reporting_database->query($getUnique);
        return $q->result();
    }

    protected function getAllData($business, $grouping)
    {
        //main query
        $getAllData = "SELECT location.address, route_id, locationID, companyName, count(distinct `orders`.customer_id) as numCust, sum(orderItem.qty * orderItem.unitPrice) as totOrds, count(distinct orderID) as numOrds, location.units, locationID, serviceType, locationType.name as serviceMethod, locationType.name as reportingClass, lowTarget, highTarget, custom1, location.zipcode 
        FROM orders
        JOIN locker on lockerID = orders.locker_id
        JOIN location on locationID = locker.location_id
        JOIN orderItem on orderItem.order_id = orderID
        join locationType on locationTypeID = locationType_id ";

        switch ($grouping) {
            case 'quarter':
            $getAllData .= "where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."'))) in (".$this->selectedOptions["selectQuarter"]."
            )";
            break;
            case 'month':
            $getAllData .= "where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."'))) = '".$this->selectedOptions["selectMonth"]."
            '";
            break;
            case 'week':
            $getAllData .= "where (convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."') >= '".$this->selectedOptions["startUnix"]."
            '  and convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."') <= '".$this->selectedOptions["endUnix"]."
            ') ";
            break;
            case '4week':
            $getAllData .= "where yearweek(convert_tz(orders.dateCreated, 'GMT', '".$business->timezone."')) in (".$this->selectedOptions["selectRange"]."
            )";
            break;
        }

        $getAllData .= "
        and orders.business_id = $business->businessID
        and bag_id <> 0
        group by address
        order by totOrds desc";

        $q = $this->reporting_database->query($getAllData);
        return $q->result();
    }

    protected function getLockerCount($business, $location_id)
    {
        $getLockerCount = "SELECT location_id, lockerLockType.name as type, count(lockerLockType_id) as numLock
        FROM locker
        join lockerLockType on lockerLockTypeID = lockerLockType_id
        join location on locationID = location_id
        WHERE location_id = $location_id
        and location.business_id = $business->businessID
        and lockerStatus_id = 1
        group by location_id, lockerLockType_id";

        $q = $this->reporting_database->query($getLockerCount);
        return $q->result();
    }

    protected function getDiscounts($business, $grouping)
    {
        $getDiscounts = "SELECT sum(chargeAmount) as charges
        FROM orderCharge
        join orders on orderID = order_id
        where chargeType <> 'basic authorization'
        and orders.business_id = $business->businessID ";

        switch ($grouping) {
            case 'quarter':
            $getDiscounts .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$business->timezone."'))) in (".$this->selectedOptions["selectQuarter"]."
            )";
            break;
            case 'month':
            $getDiscounts .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$business->timezone."'))) = '".$this->selectedOptions["selectMonth"]."
            '";
            break;
            case 'week':
            $getDiscounts .= "and (convert_tz(dateCreated, 'GMT', '".$business->timezone."') >= '".$this->selectedOptions["startUnix"]."
            ' and convert_tz(dateCreated, 'GMT', '".$business->timezone."') <= '".$this->selectedOptions["endUnix"]."
            ') ";
            break;
            case '4week':
            $getDiscounts .= "and yearweek(convert_tz(dateCreated, 'GMT', '".$business->timezone."')) in (".$this->selectedOptions["selectRange"]."
            )";
            break;
        }

        $q = $this->reporting_database->query($getDiscounts);
        return $q->result();
    }

    public function exportCSV($data, $headers = true)
    {
        if (empty($data)) {
            return false;
        }

        $lines = array(
            "headers" => array(),
            "rows" => array()
        );
        
        foreach ($data as $row) {
            foreach($row as $k=>$v) {
                if (!in_array($k, $lines["headers"])) {
                    $lines["headers"][] = $k; 
                }
            }
            $lines["rows"][] = $row;
        }

        $fout = fopen('php://output', 'w');
        if ($headers) {
          fputcsv($fout, $lines["headers"]); 
        }

        foreach ($lines["rows"] as $r) {
            fputcsv($fout, $r);
        }
        fclose($fout);

        return count($data);
    }

    public function exportXML($data)
    {
        if (empty($data)) {
            return false;
        }

        $writer = new XmlWriter();
        $writer->openURI('php://output');
        $writer->startDocument('1.0', 'utf-8');
        $writer->startElement('Data');
        $writer->setIndent(true);

        $writer->startElement("items");
        foreach($data as $d) {
            $writer->startElement("item");
            foreach ($d as $h=>$v) {
                $writer->startElement("property");
                $writer->writeElement("name", iconv("UTF-8", "UTF-8//IGNORE", $h));
                $writer->writeElement("value", iconv("UTF-8", "UTF-8//IGNORE", $v));
                $writer->endElement(); 
            }
            $writer->endElement();
        }
        $writer->endElement(); //items
        $writer->endElement(); //data
        $writer->flush();

        return count($data);
    }

    public function exportJSON($data)
    {
        echo json_encode($data);
        return count($data);
    }
}
