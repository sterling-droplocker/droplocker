<?php
class FollowupCallScript_Model extends My_Model
{
    protected $tableName = 'followupCallScript';
    protected $primary_key = 'followupCallScriptID';

    /**
     *
     * @param type $text
     * @param type $followupCallType_id
     * @param type $business_id
     * @return int The nubmer of rows updated in the table
     * @throws Exception
     */
    public function update($text, $followupCallType_id, $business_id)
    {
        if (!is_numeric($followupCallType_id)) {
            throw new Exception("'followupCallType_id' must be numeric.");
        }

        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $get_followup_call_script_query = $this->db->get_where($this->tableName, array("followupCallType_id" => $followupCallType_id, "business_id" => $business_id));

        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
        if ($get_followup_call_script_query->result()) {
            $this->db->where(array("business_id" => $business_id, "followupCallType_id" => $followupCallType_id));

            return $this->db->update($this->tableName, array("text" => $text));
        } else {
            return $this->db->insert($this->tableName, array("text" => $text, "business_id" => $business_id, "followupCallType_id" => $followupCallType_id));
        }
    }

    /**
     * Retrieves a particular follow up script for a buisness.
     * @param type $followupCallType_id
     * @param type $business_id
     * @return type
     * @throws Exception
     */
    public function get($followupCallType_id, $business_id)
    {
        if (!is_numeric($followupCallType_id)) {
            throw new Exception("'followupCallType_id' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->db->select("text");
        $get_followup_call_script_query = $this->db->get_where($this->tableName, array("followupCallType_id" => $followupCallType_id, "business_id" => $business_id ));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
        $get_followup_call_script_result = $get_followup_call_script_query->result();
        $followup_call_script = $get_followup_call_script_result[0]->text;

        return $followup_call_script;
    }
}
