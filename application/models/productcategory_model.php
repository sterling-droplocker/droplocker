<?php

class ProductCategory_Model extends My_Model
{
    protected $tableName = 'productCategory';
    protected $primary_key = 'productCategoryID';

    /**
     * Retrieves all the product categories for the specified business
     * @param int business_id
     * @param bool exclude_wash_and_fold_products If true, then any wproduct categories whose slug is 'wf'
     * @return array
     */
    public function get_all($business_id, $exclude_wash_and_fold_products = false)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }
        $this->db->select("productCategoryID, name, slug");
        $this->db->where("business_id", $business_id);
        if ($exclude_wash_and_fold_products) {
            $this->db->where("slug !=", "wf");
            $this->db->where("module_id !=", WFMODULE);
        }
        $this->db->order_by("name");
        $categories_query = $this->db->get("productCategory");
        $categories = $categories_query->result();

        return $categories;
    }

    /**
     * Retrieves all the product categories in a particular business in a codeigniter compatible dropdown format for the create item page.
     * Note, the first index is 'show_all', which is intended to be a selector that shows all products regardless of category.
     * Note, this action exlucdes any product categories with the slug 'wf' or have a service type ID that is the defined wash and fold modlue ID constant
     * Note, this function excludes any product categories with no associated products.
     * @param int $business_id the business from which to retrieve all products from
     * @return array
     */
    public function get_all_as_codeigniter_dropdown($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }
        App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");

        $get_productCategories_query = $this->db->query("
            SELECT productCategory.name,productCategoryID
            FROM productCategory
            JOIN product ON product.productCategory_id = productCategoryID
            JOIN product_process ON product_id = productID
            WHERE productCategory.business_id = ?
                AND product.business_id = ?
                AND slug != 'wf'
                AND productCategory.module_id NOT IN (?, ?)
                AND productType = 'product'
                AND product_process.active = 1
            GROUP BY productCategoryID
            ORDER BY productCategory.name;
        ", array($business_id, $business_id, WFMODULE, UPCHARGEMODULE));
        $productCategories = $get_productCategories_query->result();
        $result = array("show_all" => "-- SHOW ALL --");
        foreach ($productCategories as $productCategory) {
            $result[$productCategory->productCategoryID] = $productCategory->name;
        }

        return $result;
    }
    /**
    * insert method inserts a product category into the database
    */
    public function insert($options = array())
    {
        $options = check_fields($options,'productCategory');
        $this->db->insert($this->tableName, $options);

        return $this->db->insert_id();
    }

    /**
     * Retrieves all the product categories and products within each category
     *
     * The output structure is in the following format:
     *  {
     *      productCategory1: {
     *          productID1: {
     *              name,
     *              displayName,
     *              process_name,
     *              productID,
     *              category_name,
     *              upchargeGroup_id,
     *              productCategory_id
     *          },
     *          productID2: {
     *              ...
     *          },
     *      productCategory2: {
     *          ...
     *      },
     *      ...
     *  }
     *
     * @param int $business_id
     * @return array
     */
    public function get_productCategories_and_products($business_id)
    {
        $sql = "SELECT product.name, product.displayName, process.name AS process_name,
                        productID, productCategory.name AS category_name, upchargeGroup_id,
                        productCategory_id
                FROM product JOIN productCategory ON productCategory_id = productCategoryID
                JOIN product_process ON product_id=productID
                LEFT JOIN process ON process_id=processID
                WHERE product_process.active = 1
                AND product.business_id = ?
                AND productType = 'product'
                AND productCategory.slug != 'wf'
                AND productCategory.module_id NOT IN (?, ?)
                GROUP BY product.productID
                ORDER BY product.name, productCategoryID";

        $query = $this->db->query($sql, array($business_id, WFMODULE, UPCHARGEMODULE));
        $products =  $query->result();

        $categories_products = array();
        foreach ($products as $index => $product) {
            //Create a special category called 'show_all' that contains all the products
            $categories_products["show_all"][$index] = $product;
            $categories_products[ $product->productCategory_id ][ $index ] = $product;
        }

        return $categories_products;
    }

    public function get_as_codeigniter_dropdown($business_id, $module_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }

        if (!is_numeric($module_id)) {
            throw new Exception("'module_id' must be numeric");
        }

        $result = $this->get_aprax(compact("business_id", "module_id"));
        $categories = array();
        foreach ($result as $category) {
            $categories[$category->productCategoryID] = $category->name;
        }

        return $categories;
    }

    /**
     * Generates the data for the admin section to manage products
     *
     * @param int $module_id
     * @param int $business_id
     * @param string $type
     * @return array
     */
    public function get_productCategories_with_products($module_id, $business_id, $type = null)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }

        if (!is_numeric($module_id)) {
            throw new Exception("'module_id' must be numeric");
        }

        $params = array($business_id, $module_id, $business_id);

        $typeQuery = '';
        if ($type) {
            $typeQuery = 'AND product.productType = ?';
            $params[] = $type;
        }

        $sql = "SELECT base_id, productCategory.module_id, productCategoryID,
                product.business_id, module.slug AS module_slug, module.name AS module_name,
                productCategory.slug AS categorySlug, productCategory.name AS categoryName,
                productCategory.parent_id, productID, product_process.price, product.name,
                product.displayName, unitOfMeasurement, canDelete, productType, product_processID,
                processID, product_process.active, product.sortOrder, process.name AS process,
                upchargeGroup.name AS upchargeGroupName, taxGroup_id, taxGroup.name AS taxGroupName,
                product_supplier.cost AS supplier_cost, companyName AS supplier, supplierID,
                lastItem.updated AS last_used, product.width, product.image_id, i.filename image
            FROM product
            JOIN productCategory ON (productCategoryID = product.productCategory_id)
            JOIN module ON (module.moduleID = productCategory.module_id)
            LEFT JOIN product_process ON (product_process.product_id = productID)
            LEFT JOIN process ON (process_id = processID)
            LEFT JOIN upchargeGroup ON (upchargeGroup_id = upchargeGroupID)
            LEFT JOIN taxGroup ON (taxGroup_id = taxGroupID)
            LEFT JOIN product_supplier ON (product_supplier.product_process_id = product_processID AND `default` = 1)
            LEFT JOIN supplier ON (supplier_id = supplierID AND supplier.business_id = ?)
            LEFT JOIN business ON (businessID = supplierBusiness_id)
            LEFT JOIN image i ON imageID = product.image_id

            LEFT JOIN (
            SELECT product_process_id, updated
                FROM orderItem
                GROUP BY product_process_id DESC
                ORDER BY product_process_id, updated DESC
            ) AS lastItem ON (lastItem.product_process_id = product_processID)

            WHERE productCategory.module_id =  ?
            AND product.business_id =  ?
            $typeQuery
            ORDER BY productCategory.sort_order, product.name, process.name
            ";

        $products = $this->db->query($sql, $params)->result();

        $productCategories = array();
        foreach ($products as $product) {
            $productCategories[$product->categorySlug]['categoryName'] = $product->categoryName;
            $productCategories[$product->categorySlug]['categorySlug'] = $product->categorySlug;
            $productCategories[$product->categorySlug]['module_id'] = $product->module_id;
            $productCategories[$product->categorySlug]['productCategoryID'] = $product->productCategoryID;
            $productCategories[$product->categorySlug]['parent_id'] = $product->parent_id;

            $productCategories[$product->categorySlug]['products'][$product->product_processID] = array(
                'base_id' => $product->base_id,
                'product_processID' => $product->product_processID,
                'process' => $product->process,
                'displayName' => $product->displayName,
                'name' => $product->name,
                'productID' => $product->productID,
                'price' => $product->price,
                'unit' => $product->unitOfMeasurement,
                'productType' => $product->productType,
                'canDelete' => $product->canDelete,
                'active' => $product->active,
                'upchargeGroupName' => $product->upchargeGroupName,
                'taxGroup_id' => $product->taxGroup_id,
                'taxGroupName' => $product->taxGroupName,
                'last_used' => $product->last_used,
                'sortOrder' => $product->sortOrder,
                'supplier' => $product->supplier,
                'supplierID' => $product->supplierID,
                'supplier_cost' => $product->supplier_cost,
                'width' => $product->width,
                'image_id' => $product->image_id,
                'image' => $product->image,
            );
        }

        if ($type != 'product') {
            // display the products according to their sort order
            foreach ($productCategories as $categorySlug => $data) {
                usort($productCategories[$categorySlug]['products'], function($a, $b) {
                    return $a['sortOrder'] - $b['sortOrder'];
                });
            }
        }

        return $productCategories;
    }

    /**
     * Generates the list of empty categories
     *
     * @param int $business_id
     * @param int $module_id
     * @param string $type
     * @return array
     */
    public function get_productCategories_with_no_products($business_id, $module_id, $type = null)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }

        if (!is_numeric($module_id)) {
            throw new Exception("'module_id' must be numeric");
        }

        $params = array();

        $typeQuery = '';
        if ($type) {
            $typeQuery = 'AND product.productType = ?';
            $params[] = $type;
        }

        $params[] = $module_id;
        $params[] = $business_id;

        $sql = "SELECT productCategory.*
                FROM productCategory
                LEFT JOIN product ON (productCategory_id = productCategoryID $typeQuery)
                WHERE productID IS NULL
                AND productCategory.module_id = ?
                AND productCategory.business_id = ?
                ORDER BY productCategory.sort_order
            ";

        return $this->db->query($sql, $params)->result();
    }

}
