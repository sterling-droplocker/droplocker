<?php
class QuickItem_Model extends My_Model
{
    protected $tableName = 'quickItem';
    protected $primary_key = 'quickItemID';
    
    public function getListForBusiness($business_id)
    {
        //copied from quick items admin controller, this belongs here
        
        $sql = "SELECT quickItemID,
            quickItemID id,
            quickItem.name, 
            quickItem.name label, 
            product.name AS product_name, 
            process.name AS process_name, 
            quickItem.business_id, 
            quickItem.dateCreated 
            
            FROM quickItem
            LEFT JOIN product ON product_id=productID
            LEFT JOIN process ON process_id=processID
            WHERE quickItem.business_id = ?";

        return $this->db->query($sql, array($business_id))->result();
    }
}
