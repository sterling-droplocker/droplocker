<?php
/**
 *
 * Location Services Model
 *
 *
 */
class Location_services_Model extends MY_Model
{
    protected $tableName = 'location_services';
    protected $primary_key = 'location_servicesID';
    private $reporting_database = null;

    public function add_to_location($locationID, $serviceTypeID, $businessID)
    {
        $this->remove_from_location($locationID, $serviceTypeID, $businessID);

        $sql = "INSERT INTO ".$this->tableName." VALUES('', ?, ?, ?)";
        if (!$this->db->query($sql, array($locationID, $serviceTypeID, $businessID))) {
            return false;
        }

        return true;
    }

    public function remove_from_location($locationID, $serviceTypeID, $businessID)
    {
        $sql = "DELETE ls FROM ".$this->tableName." AS ls JOIN location l ON ls.location_id = l.locationID WHERE ls.location_id=? AND ls.serviceType_id=? AND ls.business_id=? AND l.status <> 'deleted'";
        $result = $this->db->query($sql, array($locationID, $serviceTypeID, $businessID));
        if ($locationID == "1566") {
            echo $this->db->last_query();die();
        }
        if (!$result) {
            return false;
        }

        return true;
    }

    public function in_use($serviceTypeID, $businessID)
    {
        $sql = "SELECT ls.location_servicesID FROM ".$this->tableName." AS ls JOIN location l ON  ls.location_id = l.locationID WHERE ls.serviceType_id=? AND ls.business_id=? AND l.status <> 'deleted' LIMIT 0,1";
     
        $query = $this->db->query($sql, array($serviceTypeID, $businessID));

        if (empty($query->num_rows())) return false;
        
        return true;
    }

    /**
    * update function
    *
    * @param array $options
    * @param array $where
    * @return int affected_rows
    */
    public function update($options = array(), $where = array())
    {
        $this->db->update('location_services', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * delete function
     *
     * @param array $where
     * @return int affected_rows
     */
    public function delete($where = array())
    {
        $this->db->delete('location_services', $where);

        return $this->db->affected_rows();
    }

    /**
     * add_customer_location makes a connection between a customer and a location
     *
     * Option Values
     * ---------------------
     * customer_id
     * location_id
     *
     * @param array $options
     * @return int insert_id
     */
     function add_customer_location($options = array())
     {

         $keys = array();
         $quotes = array();
         $values = array();
         foreach ($options as $key => $value) {
                $keys[] = $key;
                $quotes[] = '?';
                $values[] = $value;
         }

         $keys = implode(', ', $keys);
         $quotes = implode(', ', $quotes);

         $sql = "INSERT IGNORE INTO customer_location ($keys) VALUES ($quotes);";
         $this->db->query($sql, $values);

         return $this->db->insert_id();
     }
     
    public function getForLocation($location_id, $business_id)
    {
        $this->load->model('business_model');
        $businessServices = $this->business_model->get_aprax(array(
            'businessID' => $business_id,
        ), NULL
        , 'serviceTypes');
        
        $businessServices = unserialize($businessServices[0]->serviceTypes);
        
        $this->db->select('serviceType.*');
        $this->db->join('serviceType', "serviceTypeID = location_services.serviceType_id");
        $this->db->where(array(
            'location_id' => $location_id,
            'location_services.business_id' => $business_id,
            'visible' => 1,
        ));
        $query = $this->db->get('location_services');
        
        $location_services = $query->result();
        
        $services_array = array();
        foreach ($location_services as $ls) {
            $name = $ls->name;
            if ($businessServices[$ls->serviceTypeID]['displayName']) {
                $name = $businessServices[$ls->serviceTypeID]['displayName'];
            };
            
            $services_array[$ls->serviceTypeID] = $name;
        }

        return $services_array;
    }

}