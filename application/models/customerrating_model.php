<?php

class CustomerRating_Model extends My_Model
{
    protected $tableName = 'customerRating';
    protected $primary_key = 'customerRatingID';

    public function save_customer_rating($customer_id, $rating, $comment = null, $order_id = null)
    {
        if (empty($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' should be a valid customer id");
        }

        if ($rating < 1 || $rating > 5) {
            throw new \InvalidArgumentException("'rating' should be a integer from 1 to 5");
        }

        $this->archive_customer_ratings($customer_id);

        return $this->save(array(
            'customer_id' => $customer_id,
            'rating' => $rating,
            'comment' => $comment,
            'order_id' => $order_id,
            'created' => gmdate('Y-m-d H:i:s'),
        ));
    }

    public function archive_customer_ratings($customer_id)
    {
        return $this->update_all(array(
            'archived' => 1,
        ), array(
            'customer_id' => $customer_id,
        ));
    }

}