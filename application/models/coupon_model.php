<?php

class Coupon_Model extends My_Model
{
    protected $tableName = 'coupon';
    protected $primary_key = 'couponID';

    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $coupons_count = $this->db->query("SELECT COUNT(couponID) as total FROM {$this->tableName} WHERE business_id=?", array($business_id))->row();

        return $coupons_count->total;
    }
    /**
        *
        * @param int $couponID
        * @param string $new_date
        * @throws Exception
        */
    public function set_endDate($couponID, $date)
    {
        if (!is_numeric($couponID)) {
            throw new Exception("'couponID' must be numeric.");
        }
        $endDate = new DateTime($new_date);

        $this->db->where(array("couponID" => $couponID));
        $this->db->update($this->tableName, array("endDate" => $endDate->format("Y-m-d")));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
    }

    public function set_prefix($couponID, $prefix)
    {
        if (!is_numeric($couponID)) {
            throw new Exception("'couponID' must be numeric.");
        }
        if (empty($prefix)) {
            throw new Exception("'prefix' can not be empty.");
        }
        $this->db->where(array("couponID" => $couponID));
        $result = $this->db->update($this->tableName, array("prefix" => $prefix));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
    }

    public function set_code($couponID, $code)
    {
        if (!is_numeric($couponID)) {
            throw new Exception("'couponID' must be numeric.");
        }
        if (empty($code)) {
            throw new Exception("'code' can not be empty.");
        }
        $this->db->where(array("couponID" => $couponID));
        $result = $this->db->update($this->tableName, array("code" => $code));

        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $result;
    }
    public function set_amount($couponID, $amount)
    {
        if (!is_numeric($couponID)) {
            throw new Exception("'couponID' must be numeric.");
        }
        if (empty($amount)) {
            throw new Exception("'amount' can not be empty.");
        }
        $this->db->where(array("couponID" => $couponID));

        $result = $this->db->update($this->tableName, array("amount" => $amount));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $result;
    }

    public function set_frequency($couponID, $frequency)
    {
        if (!is_numeric($couponID)) {
            throw new Exception("'couponID' must be numeric.");
        }
        if (empty($frequency)) {
            throw new Exception("'frequency' can not be empty.");
        }
        $this->db->where(array("couponID" => $couponID));

        $result = $this->db->update($this->tableName, array("frequency" => $frequency));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $result;
    }
    /**
        *
        * Makes a new coupon and updates the order with status of 10 (completed)
        * the order is amde before the
        *
        * @param float $amount
        * @param int $order_id
        * @param string $cardNumber
        * @param int $business_id
        * @param string $to_email
        * @param string $fullName
        */
    public function new_gift_card($amount, $order_id, $cardNumber, $business_id, $to_email, $fullName, $sentRecipientEmail = "unknown")
    {
        if (empty($sentRecipientEmail)) {
            $sentRecipientEmail = "Unknown * bug";
        }
        $cardNumber = ($cardNumber!='')? $cardNumber : time() ;
        $insert1 = "insert into coupon (prefix, amount, amountType, frequency, description, extendedDesc, code, combine, firstTime, business_id, email, fullName, order_id, sentRecipientEmail)
                                values ('GF', ?,  'dollars', 'until used up', ?, ?, ?, 1, 0, ?, ?, ?, ?, ?)";
        $query = $this->db->query($insert1, array($amount, "$amount Gift Certificate", "Gift Certificate: Order $order_id", $cardNumber, $business_id, $to_email, $fullName, $order_id, $sentRecipientEmail));
        $coupon_id = $this->db->insert_id();

        return $coupon_id;
    }

    /**
        * To be done
        * @param type $prefix
        * @param type $code
        * @param type $customer_id
        * @param type $business_id
        */
    public function add_existing_coupon($prefix, $code, $customer_id, $business_id)
    {
        throw new Exception("Not implemented.");
    }

    /**
        * Returns the description attribute of the coupon.
        * @param string  $promo_code The code and prefix of the coupon
        * @return string
        * @throws Exception
        */
    public function get_discount_value_as_string($promo_code, $business_id)
    {
        if (empty($promo_code)) {
            throw new Exception("'promo_code' can not be empty.");
        }
        if (empty($business_id)) {
            throw new Exception("'business_id' can not be empty.");
        }

        $prefix = substr($promo_code, 0,2);
        $code = substr($promo_code, 2);
        $this->db->select("amount, amountType, description");
        if ($prefix == "CR") { //Note, CR is used for locker loot and has no code.
            $get_discount_value_query = $this->db->get_where($this->tableName, array("prefix" => $prefix, 'business_id' => $business_id));
        } else {
            $get_discount_value_query = $this->db->get_where($this->tableName, array("prefix" => $prefix, "code" => $code, 'business_id' => $business_id));
        }
        $discount_values = $get_discount_value_query->result();
        $discount_value = $discount_values[0];

        return $discount_value->description;
    }

    /** Returns all the active coupons in the database
     *
     * @param int $business_id
     * @return array
     * @throws \InvalidArgumentException
     * @throws \Database_Exception
     */
    public function get_active_coupons($business_id = null)
    {

        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        new \App\Libraries\DroplockerObjects\Business($business_id); //This statement checks to see if the business exists, will throw NotFound_Exception if the business does not exist.

        $today = new \DateTime("GMT");

        $query = "
            SELECT coupon.*, 
            employee.firstName, 
            employee.lastName, 
            CONCAT(prefix, code) promocode
            FROM coupon
            LEFT JOIN employee ON created_by = employeeID 
            WHERE
                coupon.business_id = ?
                AND (coupon.endDate >= ? OR coupon.endDate IS NULL OR coupon.endDate = '0000-00-00 00:00:00')
                AND prefix != 'GF'
            ORDER BY prefix, code DESC
        ";
        $get_active_coupons_query = $this->db->query($query, array($business_id, $today->format("Y-m-d")));

        $active_coupons = $get_active_coupons_query->result();

        return $active_coupons;
    }
    /**
     * Retrieves all the inactive coupons for a particular businesses
     * @param int $business_id
     * @return array
     * @throws \InvalidArgumentException
     * @throws \Database_Exception
     */
    public function get_inactive_coupons($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $query = "
            SELECT 
            coupon.*, 
            employee.firstName, 
            employee.lastName 
            FROM coupon
            LEFT JOIN employee ON created_by = employeeID 
            WHERE
                coupon.business_id = ?
                AND (coupon.endDate < ? AND coupon.endDate IS NOT NULL AND coupon.endDate != '0000-00-00 00:00:00' )
                AND prefix != 'GF'
            ORDER BY prefix, code DESC
        ";
        $today = new DateTime("GMT");
        $get_inactive_coupons_query = $this->db->query($query, array($business_id, $today->format("Y-m-d")));

        $inactive_coupons = $get_inactive_coupons_query->result();

        return $inactive_coupons;
    }
    /**
     * Retrieves the active coupons for the specified business. Note that the result excludes gift card coupons.
     * @param int $business_id
     * @return array An codeigniter menu formatted array.
     * @throws \App\Libraries\Droplocker_Objects\NotFound_Exception
     * @throws \InvalidArgumentException
     * @throws \Database_Exception
     */
    public function get_active_coupons_as_codeigniter_menu($business_id)
    {

        $active_coupons = $this->get_active_coupons($business_id);

        foreach ($active_coupons as $active_coupon) {
            if (is_null($active_coupon->endDate)) {
                $endDate = "NEVER";
            } else {
                $endDate = new DateTime($active_coupon->endDate);
                $endDate = $endDate->format("m/d/y");
            }
            $codeigniter_menu_result[$active_coupon->prefix . $active_coupon->code] = sprintf("%s%s - %s (Expires: %s)", $active_coupon->prefix, $active_coupon->code, $active_coupon->description, $endDate);
        }

        return $codeigniter_menu_result;
    }

    public function get_active_coupons_list($business_id)
    {
        $active_coupons = $this->get_active_coupons($business_id);

        foreach ($active_coupons as $active_coupon) {
            if (is_null($active_coupon->endDate)) {
                $endDate = "NEVER";
            } else {
                $endDate = new DateTime($active_coupon->endDate);
                $endDate = $endDate->format("m/d/y");
            }
            
            $active_coupon->label = get_translation('promocode_label', 'coupon/manage', array('promocode' => $active_coupon->promocode, 'description' => $active_coupon->description, 'expires' => $endDate), "%promocode% - %description% (Expires: %expires%)");
        }

        return $active_coupons;
    }

}
