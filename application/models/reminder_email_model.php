<?php

class Reminder_Email_Model extends MY_Model
{
    protected $tableName = 'reminder_email';
    protected $primary_key = 'reminder_emailID';

    /**
     * Retrieves all reminder emails for the specified reminder by business language
     * @param int $reminder_id
     * @return array
     */
    public function get_all_for_reminder_by_business_language($reminder_id)
    {
        if (is_numeric($reminder_id)) {
            $this->load->model("reminder_model");
            $reminder = $this->reminder_model->get_by_primary_key($reminder_id);
            if (empty($reminder)) {
                throw new Exception("Reminder ID '$reminder_id' does not exist");
            } else {
                $this->load->model("business_language_model");

                $reminder_emails_for_reminder_by_business_language = array();

                $business_languages = $this->business_language_model->get_aprax(array("business_id" => $reminder->business_id));
                foreach ($business_languages as $business_language) {
                    $reminder_emails_for_reminder_by_business_language[$business_language->business_languageID] = array();
                }

                $reminder_emails = $this->db->query("SELECT reminder_emailID, reminder_email.subject, reminder_email.body, business_language_id FROM reminder_email RIGHT JOIN business_language ON business_language_id=business_languageID WHERE reminder_id=?", array($reminder_id))->result();

                foreach ($reminder_emails as $reminder_email) {
                    $reminder_emails_for_reminder_by_business_language[$reminder_email->business_language_id] = array("reminder_emailID" => $reminder_email->reminder_emailID, "subject" => $reminder_email->subject, "body" => $reminder_email->body);
                }

                return $reminder_emails_for_reminder_by_business_language;
            }
        } else {
            throw new \InvalidArgumentException("'reminder_id' must be numeric");
        }
    }
}
