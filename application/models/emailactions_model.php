<?php
use App\Libraries\DroplockerObjects\EmailTemplate;
/**
 * Note, * incorrectly named the corresponding table as a plural noun, instead of a singular noun.
 * @deprecated ALl the custom * functions in this class are depreacted, use the model 'emailaction_model', which is correctly named, instead. If necessary, copy the function from this deprecated class to the correctly named email aciton model.
 */
class EmailActions_Model extends My_Model
{
    protected $tableName = 'emailActions';
    protected $primary_key = 'emailActionID';

    /**
     * gets all the email actions for the transactions
     */
    public function get_actions($options = array())
    {
        $query = $this->db->query("SELECT emailactionID, action, name, description FROM emailActions");

        return $query->result();
    }

    /**
     * gets a single email action to perform a transactional email
     */
    public function get_action($options = array())
    {
        if (isset($options['actionEmailID'])) {$this->db->where('emailActionID', $options['emailActionID']);}
        if (isset($options['action'])) {$this->db->where('action', $options['action']);}
        if (isset($options['function'])) {$this->db->where('function', $options['function']);}
        if (isset($options['select'])) {$this->db->select($options['select']);}

        $this->db->join("emailTemplate", 'emailTemplate.emailAction_id = emailActionID', 'left');
        $query = $this->db->get('emailActions');

        return $query->result();
    }

    /**
     * @deprecated use get_by_primary_key defined in MY_Model
     * @param type $emailActionID
     * @return type
     * @throws Exception
     */
    public function get_by_id($emailActionID)
    {
        if (!is_numeric($emailActionID)) {
            throw new Exception("'emailActionID' must be numeric.");
        }
        $query = $this->db->get_where($this->tableName, array("emailActionID" => $emailActionID));
        $actions = $query->result();

        return $actions[0];
    }

    /**
     * @deprecated
     *
    * gets the custom email or default for an email action
    *
    * @param unknown_type $business_id
    */
   public function getBusinessActions($business_id)
   {
       if (!is_numeric($business_id)) {
           throw new \InvalidArgumentException("'$business_id' must be numeric.");
       }
       $sql = "SELECT emailTemplateID, emailActionID, emailActions.body as defaultEmail, emailActions.description, emailActions.adminBody as defaultAdmin, emailActions.smsText as defaultText, emailActions.name, emailActions.orderStatusOption_id,
               emailTemplate.emailText as customEmail, emailTemplate.smsText as customText, emailTemplate.adminText as customAdmin,
               orderStatusOption.name as statusName
           FROM emailActions
           LEFT JOIN orderStatusOption on orderStatusOption.orderStatusOptionID = orderStatusOption_id
           LEFT JOIN emailTemplate ON emailTemplate.emailAction_id = emailActionID AND business_id = ?";
       $query = $this->db->query($sql, array($business_id));
       $actions =  $query->result();
       foreach ($actions as $v) {
           $emails[$v->emailActionID]['emailTemplateID'] = $v->emailTemplateID;
           $emails[$v->emailActionID]['name'] = $v->name;
           $emails[$v->emailActionID]['status'] = $v->statusName;
           $emails[$v->emailActionID]['sms_found'] = $this->_found($v->customText, $v->defaultText);
           $emails[$v->emailActionID]['email_found'] = $this->_found($v->customEmail, $v->defaultEmail);
           $emails[$v->emailActionID]['admin_found'] = $this->_found($v->customAdmin, $v->defaultAdmin);
           $emails[$v->emailActionID]['description'] = $v->description;
       }

       return $emails;
   }

    /**
     * This function is deprecated, do not use.
     * Gets a single emailAction
     *
     * @param int $business_id
     * @param int $emailActionID
     */
    public function getEmailTemplate($business_id, $emailActionID)
    {
        $sql = "SELECT emailActionID, emailTemplateID, d.bodyText as defaultEmailText, d.body as defaultEmail, d.subject as defaultEmailSubject, d.adminBody as defaultAdmin, d.adminSubject as defaultAdminSubject, d.smsText as defaultText, d.smsSubject as defaultSmsSubject, d.name, d.orderStatusOption_id,
            c.bodyText as customerEmailText, c.emailText as customEmail, c.emailSubject as customEmailSubject, c.smsText as customText, c.smsSubject as customSmsSubject, c.adminText as customAdmin, c.adminSubject as customAdminSubject,
            c.adminRecipients
            FROM emailActions d
            LEFT JOIN emailTemplate c ON c.emailAction_id = emailActionID AND business_id = {$business_id}
            WHERE emailActionID = {$emailActionID}";

        $query = $this->db->query($sql);
        $action = $query->row();

        return $action;
    }


    /**
    * Updates an email template
    * @deprecated Use the Droplocker Object EmailTemplate instead.
    *
    * @param int $business_id
    * @param string $message
    * @param string $subject
    * @param int $emailAction_id
    * @param string $type
    * @param string $adminRecipients
    * @return array (status, message)
    */
    public function updateTemplate($business_id, $message, $subject, $emailAction_id, $type, $adminRecipients='')
    {
        $subjectColumn = $type."Subject";
        $textColumn = $type."Text";

        $template = EmailTemplate::search(array('business_id'=>$business_id, 'emailAction_id'=>$emailAction_id));

        if ($template->emailTemplateID) {
            $template = new EmailTemplate($template->emailTemplateID);
        } else {
            $template = new Template();
            $template->business_id = $business_id;
            $template->emailAction_id = $emailAction_id;
            $res = $template->save();
        }

        $template->$subjectColumn = $subject;
        $template->$textColumn = $message;
        $template->adminRecipients = $adminRecipients;
        $res = $template->save();

        $status = ($res) ?'success':'error';
        $message = ($res) ?'Updated Action Template':'Error Updating Action Template';

        return array('status'=>$status, 'message'=>$message);
    }

    /**
     * @deprecated Unknown, undocumented *.
     * @param object $email
     */
    public function is_blank($email)
    {
        if ($this->_found($email->emailText)!='') {return false;}
        if ($this->_found($email->smsText)!='') {return false;}
        if ($this->_found($email->adminText)!='') {return false;}
        if ($this->_found($email->emailSubject)!='') {return false;}
        if ($this->_found($email->smsSubject)!='') {return false;}
        if ($this->_found($email->adminSubject)!='') {return false;}

        return true;
    }


    /**
     * @deprecated Unknown, Undocumented *.
     *
     * ###################################
     * BEGIN * BABBLE
     * ###################################
     *
     * @param unknown_type $custom
     * @param unknown_type $default
     *
     * ###################################
     * END * BABBLE
     * ###################################
     */
    public function _found($custom = '', $default = '')
    {
        $flag = '';
        if (!empty($custom)) {
            $flag = "C";
        } elseif (!empty($default)) {
            $flag = "D";
        }

        return $flag;
    }
}
