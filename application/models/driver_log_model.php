<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Driver_Log_Model extends My_Model
{
    protected $tableName = 'driverLog';
    protected $primary_key = 'driverLogID';

    public function getTransactions($employee_id, $logDate, $business, $withClockInOut = false)
    {
        // Adjust logDate to business timezone, begin and end datetime.
        $begin_date = convert_from_local_to_gmt($logDate . " 00:00:00", $format = "Y-m-d H:i:s");
        $end_date   = convert_from_local_to_gmt($logDate . " 23:59:59", $format = "Y-m-d H:i:s");

        $sql = "SELECT firstName,
                        lastName,
                        employee_id,
                        delTime,
                        delType,
                        location.address,
                        lockerName,
                        lat,
                        lon,
                        route_id,
                        order_id,
                        locker_id
            FROM driverLog
            join locker on lockerID = driverLog.locker_id
            join location on locationID = locker.location_id
            join employee on employeeID = employee_id
            where delTime >= ?
            AND delTime <= ?
            AND delType !='pending'
            and employee_id = ?
            order by delTime";

        if ($withClockInOut) {
            $sql = "SELECT * FROM
                        (SELECT 
                            e.firstName, 
                            e.lastName, 
                            tc.employee_id, 
                            tcd.timestamp as delTime, 
                            '' as delType, 
                            tcd.in_out as address,
                            '' as lockerName,
                            '' as lat,
                            '' as lon,
                            '' as route_id,
                            '' as order_id,
                            '' as locker_id
                        FROM
                            timeCard tc
                                JOIN
                            timeCardDetail tcd ON tcd.timeCard_id = tc.timeCardID
                                JOIN
                            employee e on e.employeeID = tc.employee_id
                        WHERE
                            tc.employee_id = ?
                                AND tcd.day = ?
                        UNION
                        ".$sql.") as x
                        ORDER BY x.delTime;";
            $query = $this->db->query(
                    $sql, 
                    array($employee_id, $logDate, $begin_date, $end_date, $employee_id)
                );
        } else {
            $query = $this->db->query(
                    $sql, 
                    array($begin_date, $end_date, $employee_id)
                );            
        }

        $transactions = $query->result();

        return $transactions;
    }

    /**
    * This function gets the driver's performance based on their id and a date
    * option values
    * --------------------------------
    * employee_id
    * date
    * driverLog (this is an optional value.  This is if you have already done the query)
    */
    public function getMetrics($options=array())
    {
        if (isset($options['employee_id']) and isset($options['date'])) {
            $options['driverLog'] = $this->getTransactions($options['employee_id'], $options['date']);
        }

        if (isset($options['driverLog'])) {
            foreach ($options['driverLog'] as $t) {
                if (isset($options['withClockInOut'])) {
                    if (in_array($t->address, array("In", "Out"))) continue;
                }
                $counter++;
                if ($priorStop <> $t->address) { $stopCounter ++; }
                if ($t->delType == "pickup") { $pickupCounter ++; }
                if ($t->delType == "delivery") { $deliveryCounter ++; }
                $priorStop = $t->address;
                if ($counter == 1) { $firstStopTime = $t->delTime; }
                $lastStopTime = $t->delTime;
            }
            if ($stopCounter > 0) {
                $ret['stopCounter'] = $stopCounter;
                $ret['pickupCounter'] = $pickupCounter;
                $ret['deliveryCounter'] = $deliveryCounter;
                $ret['totalTime'] = (strtotime($lastStopTime) - strtotime($firstStopTime)) / 3600;
                $ret['stopsPerHour'] = $stopCounter / $ret['totalTime'];
                $ret['tranPerHour'] = ($pickupCounter + $deliveryCounter)/$ret['totalTime'];

                return $ret;
            }
        }

        return array('status'=>'Error', 'message'=>'Required values not set for getMetrics');
    }

    /**
     * inserts a driver transaction in the driverLog table
     *
     * Mainly used in the mobile controller to help manage duplicate orders
     *
     * @param array or object $options
     * @return int insert_id()
     */
    public function insertTransaction($options = array())
    {
       $this->db->insert('driverLog',$options);

       return $this->db->insert_id();
    }

    /**
     * (non-PHPdoc)
     * @see My_Model::get()
     */
    public function get($options = array())
    {
        $query = $this->db->get_where('driverLog', $options);

        return $query->result();
    }


    /**
     * (non-PHPdoc)
     * @see My_Model::update()
     */
    public function update($options = array(), $where)
    {
        if (empty($where)) {
            throw new Exception("Missing where clause for update");
        }

        $this->db->update('driverLog', $options, $where);

        return $this->db->affected_rows();
    }

}
