<?php

class Business_Language_Model extends MY_Model
{
    protected $tableName = 'business_language';
    protected $primary_key = 'business_languageID';

    /**
     * Finds a language by the specified tag
     * @param string $tag
     * @return type
     */
    public function findByTag($tag, $business_id)
    {
        if (empty($tag)) {
            throw new InvalidArgumentException("'tag' can not be empty");
        }
        if (!is_numeric($business_id)) {
            throw new InvalidArgumentException("'business_id' must be numeric");
        }
        $this->db->join("language", "business_language.language_id=language.languageID");
        return $this->db->get_where($this->tableName, array("language.tag" => $tag, 'business_id' => $business_id))->row();
    }
    /**
     * Retrieves an instance of te business language model and also retrieves the associated language properties as well.
     * @param int $business_languageID
     * @return stdClass
     */
    public function get_by_primary_key($business_languageID)
    {
        $this->db->join("language", "language.languageID=business_language.language_id");
        $business_language = $this->db->get_where($this->tableName, array($this->primary_key => $business_languageID))->row();

        return $business_language;
    }
    /**
     * Retrieves all the defined business languages for a particular business
     * @param int $business_id
     * @return array An array of stdClasses from the result of using a Codeigniter active record query
     */
    public function get_all_for_business($business_id, $custoner_id = null)
    {
        $this->db->join("language", "language.languageID=business_language.language_id");
        $business_languages = $this->db->get_where($this->tableName, array("business_language.business_id" => $business_id))->result();

        foreach ($business_languages as $business_language) {
            $business_language->translatedDescription = get_translation($business_language->description, "Languages", array(), $business_language->description, $custoner_id);
        }

        return $business_languages;
    }
    /**
     *
     * @param int $business_id
     * @return array Formatted for use with the form_dropdown function in the Codeingitoer form helper
     * @throws \InvalidArgumentException
     */
    public function get_all_as_codeigniter_dropdown($business_id, $custoner_id = null)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }

        $business_languages = $this->get_all_for_business($business_id, $custoner_id);

        $business_languages_dropdown = array();
        foreach ($business_languages as $business_language) {
            $business_languages_dropdown[$business_language->business_languageID] = $business_language->translatedDescription;
        }

        return $business_languages_dropdown;
    }
}
