<?php

class Module_Model extends My_Model
{
    protected $tableName = 'module';
    protected $primary_key = 'moduleID';

    /**
     * Retrieves all modules by the specified name
     * @param string $name The name of the module
     * @return array
     * @throws Exception
     */
    public function get_module_by_name($name)
    {
        if (empty($name)) {
            throw new Exception("'name' can not be empty");
        }
        $query_result = $this->db->get_where($this->tableName, array("name" => $name));

        return $query_result->result();
    }

}
