<?php

class ItemHistory_Model extends My_Model
{
    protected $tableName = 'itemHistory';
    protected $primary_key = 'itemHistoryID';

    /**
    * Adds a logo entry for a particular item.
    * @param int $item_id
    * @param string $description
    */
    public function add($item_id, $description, $user_id)
    {
        $this->options = array("description" => $description, "item_id" => $item_id, "creator" => $user_id);
        parent::insert();
    }

    /**
     * Returns the history for a particular item.
     * @param int $item_id
     * @return array An array containing stdClass objects
     * @throws ExceptioN
     */
    public function get_history($item_id)
    {
        if (!is_numeric($item_id)) {
            throw new Exception("'item_id' must be numeric.");
        }
        $this->db->join("employee", "employeeID=creator", "left");
        $this->db->order_by("date", "desc");
        $history = $this->db->get_where($this->tableName, array("item_id" => $item_id));

        return $history->result();

    }
}
