<?php
class TicketStatusType_Model extends My_Model
{
    protected $tableName = 'ticketStatusType';
    protected $primary_key = 'ticketStatusTypeID';


    public function get_all_as_codeigniter_dropdown() {
        $types = $this->db->get("ticketStatusType")->result();
        $types_dropdown = array();
        foreach ($types as $t) {
            $types_dropdown[$t->ticketStatusTypeID] = $t->description;
        }

        return $types_dropdown;
    }

}