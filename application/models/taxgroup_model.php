<?php

class TaxGroup_Model extends MY_Model
{
    protected $tableName = 'taxGroup';
    protected $primary_key = 'taxGroupID';

    public function get_as_codeigniter_dropdown($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }

        $result = $this->get_aprax(array('business_id' => $business_id));

        $taxGroups = array();
        foreach ($result as $taxGroup) {
            $taxGroups[$taxGroup->taxGroupID] = $taxGroup->name;
        }

        return $taxGroups;
    }

    /**
     *
     * @param int $orderID
     * @return float
     */
    public function get_tax_rate($orderID, $taxGroupID)
    {
        $sql = "SELECT taxRate FROM orders
            JOIN locker ON lockerID = orders.locker_id
            JOIN location ON locationID = locker.location_id
            JOIN taxTable ON taxTable.zipcode = location.zipcode AND taxTable.business_id = orders.business_id
            WHERE orderID = ?
            AND taxGroup_id = ?";

        $query = $this->db->query($sql, array($orderID, $taxGroupID));
        $rate = $query->row();

        return $rate ? $rate->taxRate : 0;
    }

}
