<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class ItemIssue_Model extends My_Model
{
    protected $tableName = 'itemIssue';
    protected $primary_key = 'itemIssueID';

    public function checkItemIssues($item_id, $order_id, $product_id, $process_id)
    {
        if ($item_id !=0 ) {
            $this->item_id = $item_id;
            $this->status = 'Opened';
            $issues = $this->get();
            
            if ($issues) {
                return array(
                    'success' => false,
                    'message' => 'Issues on item',
                    'redirect' => "/admin/orders/item_issues_confirmation/$item_id/$order_id/$product_id/$process_id",
                );
            }
        }
        
        return array(
            'success' => true,
            'message' => '',
        );
    }
    
}
