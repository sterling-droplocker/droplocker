<?php

class ItemOption_Model extends My_Model
{
    protected $tableName = 'itemOption';
    protected $primary_key = 'itemOptionID';

    /**
     * Save the item options
     *
     * options should be an array of product_id.
     *
     * @param int $item_id
     * @param array $options
     */
    public function save_options($item_id, $options)
    {
        // delete old options
        $this->delete_aprax(array('item_id' => $item_id));

        foreach ($options as $name => $value) {

            $this->save(array(
                'item_id' => $item_id,
                'name' => $name,
                'value' => $value,
                'updated' => gmdate('Y-m-d H:i:s')
            ));
        }
    }


    /**
     * Gets the value for each item option
     *
     * @param int $item_id
     * @return array
     */
    public function get_options($item_id)
    {
        $sql = "SELECT name, value
            FROM itemOption
            WHERE item_id = ?";

        $query = $this->db->query($sql, array($item_id));
        $values = $query->result();

        $options = array();
        foreach ($values as $value) {
            $options[$value->name] = $value->value;
        }

        return $options;
    }

}
