<?php

class Payment_Method_Model extends My_Model
{
    protected $tableName = 'payment_method';
    protected $primary_key = 'payment_methodID';
}
