<?php

use App\Libraries\DroplockerObjects\AclResource;

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class AclRoles_Model extends My_Model
{
    protected $tableName = 'aclroles';
    protected $primary_key = 'aclID';

    /**
     * Retrieves the number of acl roles for a particular business
     * @param int $business_id
     * @return int
     */
    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $aclRoles_count = $this->db->query("SELECT COUNT(aclID) AS total FROM {$this->tableName} WHERE business_id = ?", array($business_id))->row();

        return $aclRoles_count->total;
    }

    /**
     * Retrieves all the ACL roles for a particular business in a codeigniter dropdown compatiable format.
     * @param int $business_id
     * @return array
     */
    public function get_all_as_codeigniter_dropdown($business_id)
    {
        $result = array();
        $aclRoles = $this->get_aprax(array("business_id" => $business_id), 'name ASC');
        foreach ($aclRoles as $aclRole) {
            $result[$aclRole->aclID] = $aclRole->name;
        }

        return $result;
    }

    /**
     * Retrieves all the ACL roles for a particular business in a codeigniter dropdown compatiable format.
     * @param int $business_id
     * @return array
     */
    public function get_roles_name_array($business_id)
    {
        $result = array();
        $aclRoles = $this->get_aprax(array("business_id" => $business_id), 'name ASC');
        foreach ($aclRoles as $aclRole) {
            $result[strtolower($aclRole->name)] = $aclRole->name;
        }

        return $result;
    }

    /**
     * gets roles from the aclroles table
     *
     * Options Values
     * -----------------------
     * business_id
     *
     * @param array $options
     * @return array of objects
     */
    public function get($options = array())
    {
        if (isset($options['business_id'])) {$this->db->where('business_id', $options['business_id']);}
        $this->db->order_by('name');

        $query = $this->db->get('aclroles');

        return $query->result();
    }

    /**
     * get_role_groups method gets role groups
     *
     * Options Values
     * --------------------
     * business_id
     *
     * @param array $options
     * @return array of objects
     */
    public function get_resources($options = array())
    {
        $this->db->order_by('aclgroup');
        $query = $this->db->get('aclresources');

        return $query->result();
    }

    /**
     * update_acl method updates or inserts a new acl for a role/resource
     *
     * Options Values
     * ---------------------
     * business_id
     * resource
     * type    -- if not set, defualts to role
     * type_id -- if not set, default to the role_id
     *
     * @param array $options
     * @return string "allow", if the setting was changed to "allow", "deny" if the setting was changed to "deny"
     */
    public function update_acl($options = array())
    {
        $query = $this->db->get_where('acl', $options);
        $result = $query->result();

        // The following conditional determines if we are updating an existing resource or adding a new resource.
        if ($result) {
            //If the resource was already set to allow, then we set it to deny, otherwise, if the resource was set to deny, then we set it to allow.
            $options['action'] = $action = ($result[0]->action=='allow')?"deny":"allow";
            $this->db->update('acl', $options, array('id'=>$result[0]->id));
        } else {
            // if we are setting a user type, we need to see if they are allowed or denied by default and then set the opposite
            if ($options['type']=='user') {
                $aclResource = new AclResource($options['resource_id']);
                $options['action'] = ($this->zacl->check_acl($aclResource->resource, $options['type_id']))?'deny':'allow';
            } else {
                $options['action'] = 'allow';
            }
            $this->db->insert('acl',$options);
        }

        return $options['action'];
    }
}
