<?php
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
class DroidPostPickupReminders_Model extends My_Model
{
    protected $tableName = 'droid_postPickupReminders';
    protected $primary_key = 'ID';

    /**
     * inserts a postPickup from the droid to the database
     *
     * Required Options Values
     * ----------------------
     * transaction_id
     * bagNumber
     * location_id
     * lockerName
     * employee_id
     * business_id
     *
     * @param array $options
     * @throws Exception for missing required option values
     * @return int insert_id
     */
    public function insert($options = array())
    {
        $required = array('transaction_id',
                          'bagNumber',
                          'location_id',
                          'lockerName',
                          'employee_id',
                          'business_id');

        if (!required($required, $options)) {
            throw new Exception("Missing required option values. <br>Required: <br>". print_r($required, 1)."<br>Sent: <br>".print_r($options, 1));
        }

        // rename FRONT to Front_Counter
        $options['lockerName'] = ($options['lockerName'] == 'FRONT')?'Front_Counter':$options['lockerName'];

        // check to see if the transaction exists... if it do, then just return that id
        if (!$id = $this->get($options)) {
            $this->db->insert('droid_postPickupReminders', $options);
            $id = $this->db->insert_id();
        }

        return $id;
    }


    /**
     *
     * Updates a row in the  table
     *
     * @param array $options
     * @param array $where
     * @throws Exception for missing where statement
     * @return int affected_rows
     */
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where statement");
        }

        $this->db->update('droid_postPickupReminders', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * gets data from the table
     *
     * $options values
     * ----------------
     * transaction_id
     * status
     *
     * @param array $options
     * @return array of objects
     */
    public function get($options = array())
    {
        if (isset($options['transaction_id'])) {$this->db->where('transaction_id',$options['transaction_id']);}
        if (isset($options['status'])) {$this->db->where('status',$options['status']);}
        if (isset($options['version'])) {$this->db->where('version',$options['version']);}

        $query = $this->db->get('droid_postPickupReminders');

        return $query->result();
    }



    /**
    * Processes a single pickup reminder
    *
    * @param stdclass $reminder
    * @throws Exception
    */
    public function processPickupReminder($reminder)
    {
        $where = array('ID'=>$reminder->ID); // used for updating the droid_postPickups table
        $defaultOptions = array('status'=>'complete');

        try {

            if (empty($reminder->bagNumber)) {
                throw new Exception("Missing bag number");
            }

            //get the location
            $sql = "SELECT * FROM locker
                JOIN location on locationID = locker.location_id
                WHERE locker.lockerName = '$reminder->lockerName'
                AND location_id = $reminder->location_id;";

            $query = $this->db->query($sql);
            $location = $query->row();

            if (!$location->locationID) {

                $body = "Could not find locker location on pickup reminder";
                $body .= "<br><br>".formatArray($reminder)."<br>";
                send_admin_notification("Could not find locker location on pickup reminder", $body, $reminder->business_id);

                $statusNotes[] = "Could not find locker location on pickup reminder";
                $options = $defaultOptions;
                $options['statusNotes'] = serialize($statusNotes);
                $options['methodCalled'] = __METHOD__;
                $this->update($options,$where);

                return;
            }

            //get the customer from the bag
            $sql = "SELECT customer.*, bagID FROM customer join bag on bag.customer_id = customerID where bagNumber = $reminder->bagNumber AND customer.business_id = {$reminder->business_id}";
            $customer = $this->db->query($sql)->row();
            $bagID = $customer->bagID;

            // We found a customer
            if ($customer->customerID) {

                // check to see if there has been a recent pickup reminder email sent to them
                // I check to see if one has been sent within the last 8 hours
                $sql = "SELECT customerHistoryID FROM customerHistory
                    WHERE customer_id = {$customer->customerID}
                    AND historyType = 'Pickup Reminder'
                    AND timestamp > DATE_ADD(NOW(),INTERVAL -8 HOUR)";
                $history = $this->db->query($sql)->row();

                if ($history->customerHistoryID) {

                    $statusNotes[] = "Customer has already received this reminder within the last 8 hours";
                    $options = $defaultOptions;
                    $options['statusNotes'] = serialize($statusNotes);
                    $options['methodCalled'] = __METHOD__;
                    $this->update($options,$where);

                    return;
                }


                // save to customer history
                $this->load->model('customerhistory_model');
                if (!$this->customerhistory_model->insert('Pickup Reminder Sent', $customer->customerID,'Pickup Reminder', $reminder->business_id, $reminder->employee_id)) {
                    throw new Exception('Pickup Reminder Error', 'There was an error saving pickup reminder to the customer history table. <br><br>Customer: '.$customer->CustomerID.'<br>BagNumber: '.$reminder->bagNumber);
                }

            } else {
                //no customer found
                $subject = "Customer Not Found on pickup reminder";
                $error = 'Customer was not found: <br><br>'.formatArray($reminder);
            }

            // if there is an error, we need to clear out the phone and NOT send an email to the customer
            if ($error) {

                $bodyText = $error;
                send_admin_notification($subject, $bodyText, $reminder->business_id);

                $statusNotes[] = "Customer not found";
                $options = $defaultOptions;
                $options['statusNotes'] = serialize($statusNotes);
                $options['methodCalled'] = __METHOD__;
                $this->update($options,$where);

                return;

            } else {

                $order = $this->_getOrderFromLocker($reminder->lockerName, $reminder->location_id, $bagID, $reminder->business_id);
                if (empty($order)) {
                    $location =
                    $subject = "Pickup reminder notification to customer failed - No Order Found";
                    $body = "Trying to send a pickup reminder to ". getPersonName($customer)." [<a href='https://droplocker.com/admin/customers/detail/{$customer->customerID}'>{$customer->customerID}</a>].<br><br>";
                    $body .= "The order is found based on the lockerName and location id. Please investigate why the order was not found.";
                    $body .= "<br>Full Reminder Details: <br>".formatArray($reminder)."<br>";
                    queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($reminder->business_id, 'customerSupport'), $subject, $body, array(), null, $reminder->business_id);

                    $statusNotes[] = "Order not found. Failed sending pickup reminder to customer";
                    $options = $defaultOptions;
                    $options['statusNotes'] = serialize($statusNotes);
                    $options['methodCalled'] = __METHOD__;
                    $this->update($options,$where);

                    return false;
                }

                $action_array = array('do_action' => 'pickup_reminder', 'customer_id'=>$customer->customerID, 'order_id'=>$order->orderID, 'business_id'=>$reminder->business_id);
                Email_Actions::send_transaction_emails($action_array);
            }


            $statusNotes[] = "Succesfully sent pickup reminder";
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;
            $this->update($options,$where);

        } catch (Exception $e) {
            send_exception_report($e);
        }
    }


    /**
    * gets the order by searching with the lockerName and location_id
    *
    * @param string $lockerName
    * @param int $location_id
    * @param int $business_id
    * @return stdClass object
    */
    public function _getOrderFromLocker($lockerName, $location_id, $bagID, $business_id)
    {
        $locker = Locker::search(array('lockerName'=>$lockerName, "location_id"=>$location_id));
        $sql = "SELECT *, orders.dateCreated FROM orders
                  WHERE locker_id = {$locker->lockerID}
                  AND bag_id = {$bagID}
                  AND orderStatusOption_id = 10
                  AND orders.business_id = {$business_id}
                  ORDER BY orderID desc limit 1";
        $order = $this->db->query($sql)->row();

        return $order;
    }


}
