<?php

class DropLocation_Model extends MY_Model
{
    public function __construct($tableName = "dropLocation", $primary_key = "dropLocationID")
    {
        parent::__construct($tableName, $primary_key);
    }

    /**
     * Gets an array of Drop Locations for this business for use in dropdopwns
     *
     * @param int $business_id
     * @return array
     */
    public function get_list($business_id)
    {
        $locations = $this->get_aprax(array(
            'business_id' => $business_id,
        ));

        $list = array();
        foreach ($locations as $location) {
            $list[$location->dropLocationID] = $location->address;
        }

        return $list;
    }

    /**
     * Gets the dropLocationID for the default Drop Location
     *
     * @param int $business_id
     * @return int
     */
    public function get_default($business_id)
    {
        $location = $this->get_one(array(
            'business_id' => $business_id,
            'default' => 1,
        ));

        return $location ? $location->dropLocationID : null;
    }

}