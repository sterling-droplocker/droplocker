<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class StarchOnShirts_Model extends My_Model
{
    protected $tableName = 'starchOnShirts';
    protected $primary_key = 'starchOnShirtsID';

}
