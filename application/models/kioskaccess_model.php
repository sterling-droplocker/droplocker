<?php
use App\Libraries\DroplockerObjects\CreditCard;
/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class KioskAccess_Model extends My_Model
{
    protected $tableName = 'kioskAccess';
    protected $primary_key = 'kioskAccessID';

    /**
     * Gets the card on file for a customer
     *
     * @param int $customer_id
     * @param int $business_id
     * @return array
     */
    public function get_kiosk_access($customer_id, $business_id)
    {
        return $this->db->get_where('kioskAccess', array(
            'customer_id' => $customer_id,
            'business_id' => $business_id,
        ))->result();
    }

    /**
     * Determines id the customer has kiosk access for the given card number.
     *
     * Returns True if the customer has kiosk access for the given card, False
     * if the customer does not have kiosk access for the given card.
     *
     * This uses only the last 4 digits of the credit card number.
     *
     * @param int $card_number
     * @param int $customer_id
     * @param int $business_id
     * @return bool
     */
    public function has_kiosk_access($cardNumber, $customer_id, $business_id)
    {
        $cardNumber = substr($cardNumber, -4);

        $kioskAccess = $this->db->get_where($this->tableName, array(
            "cardNumber" => $cardNumber,
            "customer_id" => $customer_id,
            "business_id" => $business_id
        ), 1)->row();

        return !empty($kioskAccess);
    }

    /**
     * Setups kiosk access for a customer
     *
     * Only last 4 numbers of $cardNumber will be stored.
     * $expYr and $expMo are stored as a 2 chars.
     *
     * @param int $customer_id
     * @param int $business_id
     * @param int $cardNumber
     * @param int $expMo
     * @param int $expYr
     * @return bool
     */
    public function update_access($customer_id, $business_id, $cardNumber, $expMo, $expYr)
    {
        $cardNumber = substr($cardNumber, -4);
        $expMo = sprintf("%02d", substr($expMo, -2));
        $expYr = sprintf("%02d", substr($expYr, -2));

        // delete old access
        $this->db->delete('kioskAccess', array('customer_id' => $customer_id));

        // insert new access
        $this->db->insert('kioskAccess', array(
            'cardNumber' => $cardNumber,
            'expMo' => $expMo,
            'expYr' => $expYr,
            'business_id' => $business_id,
            'customer_id' => $customer_id,
        ));

        // update customer as having access
        $this->db->update('customer', array(
            'kioskAccess' => 1
        ), array(
            'customerID' => $customer_id
        ));

        // update the business
        $this->set_kiosks_to_update($business_id);

        return true;
    }

    /**
     * Removes kiosk access from a customer
     *
     * @param int $customer_id
     * @param int $business_id
     */
    public function remove_access($customer_id, $business_id)
    {
        // delete access
        $this->db->delete('kioskAccess', array('customer_id' => $customer_id));

        // update customer as not having access
        $this->db->update('customer', array(
            'kioskAccess' => 0
        ), array(
            'customerID' => $customer_id
        ));

        // update the business
        $this->set_kiosks_to_update($business_id);
    }

    /**
     * Updates a business setting with the count of kiosks that need update
     *
     * @param int $business_id
     */
    public function set_kiosks_to_update($business_id)
    {
        //set a business_meta to the number of kiosks
        $getNumKiosks = "select count(locationID) as numKiosk
                            from location
                            where business_id = ?
                            and locationType_id = 1";
        $query = $this->db->query($getNumKiosks, array($business_id));
        $kiosks = $query->row();

        update_business_meta($business_id, "kioskLeftToUpdate", $kiosks->numKiosk);
    }
}
