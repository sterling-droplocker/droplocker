<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Assembly_Model extends My_Model
{
    protected $tableName = 'assembly';
    protected $primary_key = 'assemblyID';

    /**
     * gets the max position from the assembly table
     *
     * @param int $route_id
     * @param int $business_id
     * @throws Exception missing business id
     * @return int
     *
     */
    public function getMaxPosition($route_id, $business_id)
    {
        if (empty($business_id)) {
            throw new \InvalidArgumentException("Missing business ID");
        }

        if (!is_numeric($route_id)) {
            throw new \InvalidArgumentException('Missing route id');
        }

        $sql = "select max(position) as lastPos from assembly where route_id = $route_id and business_id = $business_id";
        $query = $this->db->query($sql);

        if (!$query) {
            throw new Exception($this->db->_error_message());
        }

        $row = $query->row();

        return $row->lastPos;
    }

    /**
     * inserts into the assembly table
     *
     * @param int $business_id
     * @param int $route_id
     * @param int $order_id
     * @param int $position
     * @param dateTime $printDate
     * @return int insert_id
     */
    public function insert($business_id, $route_id, $order_id, $position, $printDate)
    {
       $sql = "insert into assembly (business_id, route_id, order_id, position, printDate)
                    values ($business_id, $route_id, $order_id, $position, '$printDate');";
       $this->db->query($sql);

       return $this->db->insert_id();
    }

}
