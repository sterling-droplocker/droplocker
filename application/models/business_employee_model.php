<?php
class Business_Employee_Model extends My_Model
{
    protected $tableName = 'business_employee';
    protected $primary_key = 'ID';

    /**
     *
     */
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where clause");
        }

        $this->db->update('business_employee', $options, $where);

        return $this->db->affected_rows();
    }



}
