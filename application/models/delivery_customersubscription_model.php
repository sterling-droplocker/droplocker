<?php

class Delivery_CustomerSubscription_Model extends My_Model
{
    protected $tableName = 'delivery_customerSubscription';
    protected $primary_key = 'delivery_customerSubscriptionID';

    public function get_subscriptions($customer_id)
    {
        $sql = "SELECT *, delivery_customerSubscription.notes
                FROM delivery_customerSubscription
                LEFT JOIN delivery_zone ON (delivery_customerSubscription.delivery_zone_id = delivery_zoneID)
                LEFT JOIN delivery_window ON (delivery_window.delivery_zone_id = delivery_zoneID)
                WHERE customer_id = ?
            ";

        $rows = $this->db->query($sql, array($customer_id))->result();

        $out = array();
        foreach ($rows as $row) {
            $id = $row->delivery_customerSubscriptionID;
            if (!isset($out[$id])) {
                $out[$id] = (object) array(
                    'delivery_customerSubscriptionID' => $row->delivery_customerSubscriptionID,
                    'customer_id' => $row->customer_id,
                    'route' => $row->route,
                    'address1' => $row->address1,
                    'address2' => $row->address2,
                    'city' => $row->city,
                    'state' => $row->state,
                    'zip' => $row->zip,
                    'lat' => $row->lat,
                    'lon' => $row->lon,
                    'delivery_zone_id' => $row->delivery_zone_id,
                    'business_id' => $row->business_id,
                    'location_id' => $row->location_id,
                    'sortOrder' => $row->sortOrder,
                    'notes' => $row->notes,
                    'windows' => array(),
                );
            }

            $day_indexes = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
            $days = array();
            foreach ($day_indexes as $key) {
                if ($row->$key) {
                    $days[] = ucfirst($key);
                }
            }

            $out[$id]->windows[$row->delivery_windowID] = (object) array(
                'delivery_windowID' => $row->delivery_windowID,
                'route' => $row->route,
                'start' => $row->start,
                'end' => $row->end,
                'days' => $days,
            );

        }

        return $out;
    }

    public function get_subscriptions_array($customer_id)
    {
        $subscriptions = $this->get_subscriptions($customer_id);

        $out = array();
        foreach ($subscriptions as $id => $subscription) {
            $windows = array();
            foreach ($subscription->windows as $window) {
                $start = preg_replace('/:00$/', '', $window->start);
                $end = preg_replace('/:00$/', '', $window->end);
                $days = implode('-', $window->days);
                $windows[] = sprintf("%s-%s %s", $start, $end, $days);
            }

            $windows = implode(', ', $windows);
            $out[$id] = sprintf("Route %s, %s", $subscription->route, $windows);
        }

        return $out;
    }

}
