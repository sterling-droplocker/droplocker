<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Product_Location_Price_Model extends My_Model
{
    protected $tableName = 'product_location_price';
    protected $primary_key = 'product_location_priceID';

    /**
     * gets the total number of locations with location price
     *
     * @param int $business_id Optional
     * @return int
     */
    public function locationsWithPricingCount($business_id = '')
    {
        $business_id = ($business_id!='')?$business_id:$this->business_id;
        $getCount = "SELECT count(*) as total FROM product_location_price plp JOIN location ON location.locationID=plp.location_id "
                . "WHERE plp.business_id = ?";
        $q = $this->db->query($getCount, array($business_id));
        $total = $q->result();

        return empty($total[0]->total)?0:$total[0]->total;
    }

    /**
     * gets locations with location price
     *
     * @param int $business_id Optional
     * @return int
     */
    public function locationsWithPricing($business_id = '')
    {
        $business_id = ($business_id!='')?$business_id:$this->business_id;
        $sql = "SELECT location.*, count(*) product_count "
                . "FROM product_location_price plp "
                . "JOIN location ON location.locationID=plp.location_id "
                . "WHERE plp.business_id = ? "
                . "GROUP BY location.locationid";
        $q = $this->db->query($sql, array($business_id));
        return $q->result();
    }
}
