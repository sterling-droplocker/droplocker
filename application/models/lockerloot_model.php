<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class LockerLoot_Model extends My_Model
{
    protected $tableName = 'lockerLoot';
    protected $primary_key = 'lockerLootID';

    /**
        * return the customers locker loot balanace
        *
        * @param int $customer_id
        * @return float|false returns false if $customer_id is not numeric or it's empty
        */
    public function get_balance($customer_id)
    {

        if (!is_numeric($customer_id) || $customer_id == 0 || $customer_id == '') {
            return false;
        }
        $sql = "SELECT sum(points) as total
                FROM lockerLoot
                left join orders on order_id = orderID
                WHERE lockerLoot.customer_id = ?";
        $query = $this->db->query($sql, array($customer_id));
        $rewards = $query->row();

        return $rewards->total;
    }




    /**
    * Creates a customer discount
    *
    * @param int $customer_id
    * @return int returns the customer discount insert id
    *
    */
    public function redeem_points($customer_id, $business_id)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        //get the customers balance
        $points = $this->get_balance($customer_id);
        
        //prevent when the number is with ',' by the setLocale, but need to store always the number with '.', because float type.
        $points = str_replace(',', '.', $points);

        if ($this->create_discount($customer_id, $points, $business_id)) {

                $this->points = $points * -1;
                $this->customer_id = $customer_id;
                $this->description = "redemption";

                return $this->insert();
        }

        return false;
    }




    /**
    * Inserts the customer locker loot balance into the customerDiscounts table
    *
    * @param int $customer_id
    * @return int|false returns the insert_id() or false if unsuccesful
    */
    public function create_discount($customer_id, $points, $business_id)
    {

        $config = \App\Libraries\DroplockerObjects\LockerLootConfig::search_aprax(array('business_id'=>$business_id));

        if($customer_id == '' || $customer_id == 0 || !is_numeric($customer_id))

                return false;

        if (empty($points) || $points <= 0) {
                return false;
        }

        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }

        //prevent when the number is with ',' by the setLocale, but need to store always the number with '.', because float type.
        $points = str_replace(',', '.', $points);

        $programName = str_replace('&trade', '', $config[0]->programName);
        $sql = "INSERT INTO customerDiscount
            SET customer_id = {$customer_id},
                    amount = {$points},
                    amountType = 'dollars',
                    frequency = 'until used up',
                    business_id = {$business_id},
                    description = '$programName',
                    extendedDesc = 'Redemption of {$programName}',
                    active = 1,
                    combine = 1,
                    origCreateDate = now(),
                    updated = now();";
        $this->db->query($sql);

        return $this->db->insert_id();
    }

    /**
     * redeems locker loot as cash
     * updates the lockerLoot table only, no discount is created
     *
     */
    public function redeem_cash($customer_id, $points, $minRedeemAmount)
    {
        if ($points > $this->get_balance($customer_id)) {
            return false;
        }

        if ($points < $minRedeemAmount) {
            return false;
        }

        $this->points = $points * -1;
        $this->customer_id = $customer_id;
        $this->description = "cash redemption";

        return $this->insert();
    }

}
