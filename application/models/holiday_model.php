<?php
class Holiday_Model extends My_Model
{
    protected $tableName = 'holiday';
    protected $primary_key = 'holidayID';


    /**
     * Get current or past holidays
     *
     * @param int $business_id
     * @param bool $showPastHolidays
     */
    public function getHolidays($business_id, $showPastHolidays = false)
    {
        $sql = "SELECT * FROM holiday WHERE business_id = ? ";
        if (!$showPastHolidays) {
            $sql .= "AND date > DATE_SUB(NOW(), INTERVAL 5 DAY) ";
        }
        $sql .= "ORDER BY date ASC";

        return $this->db->query($sql, array($business_id))->result();
    }
}