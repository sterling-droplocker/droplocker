<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\ProductPlan;

class CustomerDiscount_Model extends My_Model
{
    protected $tableName = 'customerDiscount';
    protected $primary_key = 'customerDiscountID';

    /**
    * returns all the customer's discounts
    *
    * @param int $customer_id
    * @param int $business_id
    * @param bool $active_only Only retrieve active discounts.
    * @return array consists of stdClass objects
    */
    public function get_discounts($customer_id, $business_id, $active_only = false)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->customer_id = $customer_id;

        $this->db->where("customer_id", $customer_id);
        $this->db->where("business_id", $business_id);

        $this->active = 1;
        if ($active_only) {
            $this->db->where("active", 1);
        }

        $get_discounts_query =  $this->db->get($this->tableName);
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $get_discounts_query->result();
    }
    /**
     * gets pending customer discounts
     *
     * @param int $customer_id
     * @return array of objects
     */
    public function get_pending($customer_id)
    {
            $customer_business_id_query = "select business_id from customer where customerID = $customer_id";
            $customer_business_id = $this->db->query($customer_business_id_query)->result();
            $customer_business_id = $customer_business_id[0]->business_id;
                    
            $sql = "SELECT * FROM customerDiscount
                    WHERE customer_id = $customer_id
                    AND business_id = $customer_business_id
                    AND active = 1
                    ORDER BY origCreateDate desc";
            $query = $this->db->query($sql);

            return $query->result();
    }

    public function get_applied($customer_id)
    {
        $sql = "
            SELECT orderID, chargeAmount, chargeType, customerDiscount_id, updated as dateCreated FROM orderCharge
                    JOIN orders ON orderID = orderCharge.order_id
                    WHERE customer_id = $customer_id
                    AND chargeAmount < 0
                    ORDER BY updated desc";

        $query = $this->db->query($sql);

        return $query->result();
    }


    /**
     * Gets the discount reasons for a business
     *
     * @param int $business_id default 0
     * @param string $description
     * @return array of objects
     */
    public function getDiscountReasons($business_id=0, $description='')
    {
        if (empty($business_id)) {
            $this->db->where('business_id',0);
        } else {
            $this->db->where_in('business_id', array(0,$business_id));
        }
        if ($description!='') {$this->db->where('description',strtolower($description));}

        $this->db->order_by('description');
        $query = $this->db->get('discountReason');

        return $query->result();
    }


    /**
     *
     * @param string $description
     * @param int $business_id
     * @throws Exception missing business_id
     * @return int insert_id
     */
    public function insertDiscountReason($business_id, $description)
    {
        if (empty($business_id)) {
            throw new Exception("Missing Business Id");
        }

        // if the description already exists, just return the existing ID without trying to insert
        if ($result = $this->getDiscountReasons($business_id, $description)) {
            return $result[0]->discountReasonID;
        }

        $this->db->insert('discountReason', array('description'=>strtolower($description), 'business_id'=>$business_id));

        return $this->db->insert_id();
    }


    /**
     * Reactivated a customer discount. Sets active to 1
     *
     * @param int $customerDiscountID
     * @return int affected rows
     */
    public function reactivateDiscount($customerDiscountID)
    {
        $sql = "UPDATE customerDiscount
                SET active = 1
                WHERE customerDiscountID = ".$customerDiscountID;
        $this->db->query($sql);

        return $this->db->affected_rows();
    }

    /**
    * used to expire customerDiscounts for a specific business. This will not expire ALL discounts.
    *
    * If customer_id is passed, only the customers discounts will exire
    *
    * If this will be part of a cron job, you must pass business_id
    *
    * @param int $business_id
    * @param int $customer_id --NOT USED
    * @return int affected_rows()
    * @throws Exception Missing business_id
    */
    public function expireCustomerDiscount($business_id, $customer_id = '')
    {
        if ($business_id == '') {
            throw new Exception("Missing business ID");
        }

        $business = new Business($business_id);

        $local_date = new DateTime();
        $local_date->setTimezone(new DateTimeZone($business->timezone));

        //expire discounts
        $sql = "UPDATE customerDiscount SET active = 0, description = CONCAT('EXPIRED - ', description) WHERE active = 1 AND business_id = {$business->businessID}
                AND expireDate < '{$local_date->format("Y-m-d")}' AND recurring_type_id IN (0,1)";
        $this->db->query($sql);

        //The following statements update monthly recurring discounts if they have expired.
        $recurring_customerDiscounts = CustomerDiscount::search_aprax(array(
            "recurring_type_id" => 2,
            "business_id" => $business_id,
        ));

        foreach ($recurring_customerDiscounts as $recurring_customerDiscount) {

            // This was added due to tests failing becuase the customer_id in the customerDiscount table
            // belonged to a non-existant customer. During validation while saving a customerDiscount and
            // exception is thrown if customer object is not found
            $customerObjs = Customer::search_aprax(array(
                'customerID' =>$recurring_customerDiscount->customer_id
            ));

            if (sizeof($customerObjs) == 0) {
                continue;
            }

            $today = new DateTime("now", new DateTimeZone($business->timezone));
            $today->setTime(0,0,0);
            if ($recurring_customerDiscount->expireDate && $recurring_customerDiscount->expireDate < $today) {
                $recurring_customerDiscount->refresh();
                $recurring_customerDiscount->active = 0;
                $recurring_customerDiscount->recurring_type_id = 0;
                $recurring_customerDiscount->save();

                $new_recurring_customerDiscount = new CustomerDiscount();
                $new_recurring_customerDiscount->business_id = $recurring_customerDiscount->business_id;
                $new_recurring_customerDiscount->customer_id = $recurring_customerDiscount->customer_id;
                $new_recurring_customerDiscount->amount = $recurring_customerDiscount->amount;
                $new_recurring_customerDiscount->amountType = $recurring_customerDiscount->amountType;
                $new_recurring_customerDiscount->frequency = $recurring_customerDiscount->frequency;
                $new_recurring_customerDiscount->extendedDesc = $recurring_customerDiscount->extendedDesc;
                $new_recurring_customerDiscount->description = $recurring_customerDiscount->description;
                $new_recurring_customerDiscount->couponCode = $recurring_customerDiscount->couponCode;
                $new_recurring_customerDiscount->createdBy = $recurring_customerDiscount->createdBy;
                $new_recurring_customerDiscount->recurring_type_id = 2;
                $new_recurring_customerDiscount->active = 1;

                $end_of_this_month = new DateTime($today->format("Y-m-t"), new DateTimeZone($business->timezone));
                $end_of_this_month->setTime(23,59,59);
                $new_recurring_customerDiscount->expireDate = $end_of_this_month;

                $new_recurring_customerDiscount->origCreateDate = $today;
                $new_recurring_customerDiscount->save();
            }
        }
        $return = $this->db->affected_rows();
        if (empty($return)) $return = 0;


        //Process expired products plans
        $productPlanExpiredDiscounts = CustomerDiscount::search_aprax(array(
            "amountType" => "product_plan",
            "business_id" => $business_id,
            "active" => 0
        ));

        $today = new DateTime("now", new DateTimeZone($business->timezone));
        $today->setTime(0,0,0);
        foreach ($productPlanExpiredDiscounts as $productPlan_customerDiscount) {
            if (strpos($productPlan_customerDiscount->description, "EXPIRED") === false) {
                continue;
            }

            //check if customer already has an expired discount percent
            $percentDiscounts = CustomerDiscount::search_aprax(array(
                "amountType" => "product_plan_percent",
                "business_id" => $business_id,
                "customer_id" => $productPlan_customerDiscount->customer_id,
                "active" => 1
            ));

            if (!empty($percentDiscounts)) continue;

            $plan = ProductPlan::getActiveProductPlan($productPlan_customerDiscount->customer_id);
            if (empty($plan[0])) continue;
            $plan = $plan[0];

            if (empty($plan->expired_discount_percent)) {
                continue;
            }

            if ($plan->days > 0 && $plan->renew == 1) {
                continue;
            }

            $new_customerDiscount = new CustomerDiscount();
            $new_customerDiscount->business_id = $productPlan_customerDiscount->business_id;
            $new_customerDiscount->customer_id = $productPlan_customerDiscount->customer_id;
            $new_customerDiscount->amount = $plan->expired_discount_percent;
            $new_customerDiscount->amountType = "product_plan_percent";
            $new_customerDiscount->frequency = "every order";
            $new_customerDiscount->extendedDesc = $productPlan_customerDiscount->extendedDesc;
            $new_customerDiscount->description = $plan->description;
            $new_customerDiscount->couponCode = $productPlan_customerDiscount->couponCode;
            $new_customerDiscount->createdBy = $productPlan_customerDiscount->createdBy;
            $new_customerDiscount->recurring_type_id = 2;
            $new_customerDiscount->active = 1;
            $end_of_this_month = new DateTime($today->format("Y-m-t"), new DateTimeZone($business->timezone));
            $end_of_this_month->setTime(23,59,59);
            $new_customerDiscount->expireDate = $end_of_this_month;

            $new_customerDiscount->origCreateDate = $today;
            $new_customerDiscount->save();
        }

        if (!empty($this->db->affected_rows())) {
            $return += $this->db->affected_rows();
        }

        return $return;
    }


    /**
     * Creates a discount
     *
     * @param int $custId
     * @param double $amount
     * @param string $amountType
     * @param string $frequency
     * @param string $description
     * @param string $extendedDesc
     * @param datetime $expireDate
     * @param double $combine
     * @param double $maxAmount
     * @return boolean|int if discount was already made = false, else returns the insert_id()
     */
    public function addDiscount($custId, $amount, $amountType, $frequency, $description, $extendedDesc, $expireDate, $combine = 0, $maxAmount = 0)
    {
        if (!$custId) {
            return false;
        }

        $customer = new Customer($custId);

        //make sure not to double add a discount
        $select1 = "SELECT COUNT(*) AS total FROM customerDiscount WHERE customer_id = ? AND extendedDesc = ?";
        $discounts = $this->db->query($select1, array($custId, $extendedDesc))->result();

        if ($discounts[0]->total > 0) {
            return false;
        }

        //prevent when the number is with ',' by the setLocale, but need to store always the number with '.', because float type.
        $amount = str_replace(',', '.', $amount);

        $active = 1;
        if ($amountType == 'lockerloot') {
            //add discount as Locker Loot
            $insert1 = "INSERT INTO lockerLoot SET
                points =  ?,
                customer_id = ?,
                updated = NOW(),
                description = ?";
            $insert = $this->db->query($insert1, array($amount, $custId, $description));

            $active = 0;
        }

        $expireDate = $expireDate ? date("Y-m-d", strtotime($expireDate)) : null;

        $insert2 = "INSERT INTO customerDiscount SET
            customer_id = ?,
            business_id = ?,
            amount = ?,
            amountType = ?,
            maxAmt = ?,
            frequency = ?,
            description = ?,
            extendedDesc = ?,
            active = ?,
            origCreateDate = NOW(),
            updated = NOW(),
            expireDate = ?,
            combine = ?";

        $insert = $this->db->query($insert2, array(
            $custId,
            $customer->business_id,
            $amount,
            $amountType,
            $maxAmount,
            $frequency,
            $description,
            $extendedDesc,
            $active,
            $expireDate,
            $combine
        ));

        return $this->db->insert_id();
    }

}
