<?php

use App\Libraries\DroplockerObjects\Cash_Register_Record;
use App\Libraries\DroplockerObjects\Employee;

class Cash_Register_Record_Model extends My_Model
{
    protected $tableName = 'cash_register_record';
    protected $primary_key = 'cash_register_recordID';
    
    protected $cache = array(
//        'key' => array(
//            'callable' => array(
//                'function' => 'fcn',
//                'params' => array(
//                    'param1',
//                    'param2',
//                )
//            ),
//            'value' =>'',
//        ),
    );
    
    protected function getCached($key, $callable)
    {
        $flat_params = lb_flatten($callable['params']);
        
        $key = $key . '#' . implode('#', $flat_params);
        
        if (!$this->cache[$key]['value']) {
            $this->cache[$key]['value'] = call_user_func_array($callable['function'], $callable['params']);
        };
        
        return $this->cache[$key]['value'];
    }
    
    protected function setQueryForRetrieveRecords($cash_register_id, $report_mode = false)
    {
        $extra = $report_mode ? 'SQL_CALC_FOUND_ROWS' : '';
        
        $this->db->select("$extra {$this->tableName}.*, "
            . "cash_register_record_type.*, "
            . "payment_method.slug 'payment_method', "
            . "payment_method.name 'payment_method_name', "
            . "cash_register_record_type.slug 'record_type', "
            . "cash_register_record_type.name 'record_type_name', "
            . "employee.firstName, employee.lastName, {$this->tableName}.notes notes", !$report_mode);
        $this->db->join('cash_register_record_type', 'cash_register_record_type_id=cash_register_record_typeID', 'left');
        $this->db->join('payment_method', 'payment_method_id=payment_methodID', 'left');
        $this->db->join('employee', 'employee_id=employeeID', 'left');
        $this->db->where("cash_register_id", $cash_register_id);
    }
    
    public function getRecords($cash_register_id, $context = array(), $pagination = array(), $filter = array(), $order = array(), $report_mode = false)
    {
        $date_format = "Y-m-d H:i:s";
        $business_id = get_current_business_id();
        
        if ($context['from_date']) {
            if (strpos($context['from_date'], ':') === FALSE) {
                $context['from_date'] .= " 00:00:00";
            }
            $context['from_date'] = convert_from_local_to_gmt($context['from_date'], $date_format, $business_id);
        };
        
        if ($context['to_date']) {
            if (strpos($context['to_date'], ':') === FALSE) {
                $context['to_date'] .= " 23:59:59";
            }
            $context['to_date'] = convert_from_local_to_gmt($context['to_date'], $date_format, $business_id);
        };
        
        return $this->retrieveRecords($cash_register_id, $context, $pagination, $filter, $order, $report_mode);
    }
    
    protected function retrieveLastRecords($cash_register_id, $count = 1, $record_type_slug = null, $context = array())
    {
        $pagination = array(
            'count' => $count,
        );
        
        $filter = array(
            'record_type_slug' => $record_type_slug,
        );
        
        $this->db->order_by($this->primary_key, "DESC");
        return $this->retrieveRecords($cash_register_id, $context, $pagination, $filter);
    }
    
    protected function retrieveRecords($cash_register_id, $context = array(), $pagination = array(), $filter = array(), $order = array(), $report_mode = false)
    {
        $count = $pagination['count'];
        $offset = $pagination['offset'];
        
        $this->setQueryForRetrieveRecords($cash_register_id, $report_mode);
        
        if ($count) {
            if ($offset) {
                $this->db->limit($count, $offset);
            } else {
                $this->db->limit($count);
            }
        }
        
        if ($filter['record_type_slug']) {
            $this->db->where("cash_register_record_type.slug", $filter['record_type_slug']);
        }
        
        if ($context['from_date']) {
            $this->db->where("cash_register_record.date_created >= ", $context['from_date']);
        }
        
        if ($context['to_date']) {
            $this->db->where("cash_register_record.date_created <= ", $context['to_date']);
        }
        
        if ($context['from_record_id']) {
            $this->db->where("cash_register_recordID >= ", $context['from_record_id']);
        }
        
        if ($context['to_record_id']) {
            $this->db->where("cash_register_recordID <= ", $context['to_record_id']);
        }
        
        $query = $this->db->get($this->tableName);
        $results = $query->result();
        
        if ($report_mode) {
            $sQuery = "SELECT FOUND_ROWS() AS found_rows";

            $aResultFilterTotal = $this->db->query($sQuery)->row();
            $count = $aResultFilterTotal->found_rows;

            $results = array(
                "items" => $results, 
                "count" => (int)$count
            );
        } else if ($count === 1) {
            $results = $results[0];
        }
        
        return $results;
    }
    
    protected function getLastRecords($cash_register_id, $count = 1, $record_type_slug = null, $context = array())
    {
        return $this->getCached('last_record', array(
            'function' => array(
                $this, 
                'retrieveLastRecords',
            ),
            'params' => array(
                $cash_register_id,
                $count,
                $record_type_slug,
                $context,
            ),
        ));
    }
    
    protected function getLastRecord($cash_register_id, $record_type_slug = null, $context = array())
    {
        $count = 1;
        return $this->getLastRecords($cash_register_id, $count, $record_type_slug, $context);
    }
    
    protected function getLastClosed($cash_register_id, $context = array())
    {
        return $this->getLastRecord($cash_register_id, 'drawer-end', $context);
    }
    
    protected function getLastOpen($cash_register_id, $context = array())
    {
        return $this->getLastRecord($cash_register_id, 'drawer-start', $context);
    }
    
    protected function getLastLogin($cash_register_id, $context = array())
    {
        return $this->getLastRecord($cash_register_id, 'login', $context);
    }
    
    protected function getLastLogout($cash_register_id, $context = array())
    {
        return $this->getLastRecord($cash_register_id, 'logout', $context);
    }
    
    //-------------- Info --------------------------------------
    
    protected function isEmptyRegister($cash_register_id)
    {
        $lastRecord = $this->getLastRecord($cash_register_id);
        return !$lastRecord;
    }
    
    protected function isOpenRegister($cash_register_id)
    {
        //note: empty register is closed
        
        $lastOpen = $this->getLastOpen($cash_register_id);
        $lastClosed = $this->getLastClosed($cash_register_id);
        
        return $lastOpen && (!$lastClosed || $lastClosed->{$this->primary_key} < $lastOpen->{$this->primary_key});
    }
    
    protected function isClosedRegister($cash_register_id)
    {
        return !$this->isOpenRegister($cash_register_id);
    }
    
    protected function getCurrentEmployee($cash_register_id)
    {
        $lastLogin = $this->getLastLogin($cash_register_id);
        $lastLogout = $this->getLastLogout($cash_register_id);
        
        $employeeLoggedIn = $lastLogin && (!$lastLogout || $lastLogout->{$this->primary_key} < $lastLogin->{$this->primary_key});
        
        if ($employeeLoggedIn) {
            return $lastLogin->employee_id;
        } else {
            return null;
        }
    }
    
    protected function isFree($cash_register_id)
    {
        return !$this->getCurrentEmployee($cash_register_id);
    }

    protected function isCurrentEmployee($cash_register_id, $employee_id)
    {
        return $this->getCurrentEmployee($cash_register_id) == $employee_id;
    }
    
    public function is_pos_manager($employeeData)
    {
        return strtolower($employeeData->roleName) == 'admin';
    }
    
    public function is_pos_casheer($employeeData)
    {
        return !$this->is_pos_manager($employeeData);
    }
    
    //-------------- Check --------------------------------------
    
    protected function checkIsManager($employeeData)
    {
        if (!$this->is_pos_manager($employeeData)) {
            throw new Exception(get_translation('no_manager', 'cash_register_record', array(), 'Only managers are allowed to perform this operation.'));
        }
    }
    
    public function checkCashRegisterEmployee($cash_register_id, $employee_id)
    {
        $this->load->model('cash_register_model');
        if (! $this->cash_register_model->belongsToEmployee($cash_register_id, $employee_id) ) {
            throw new Exception(get_translation('employee_not_on_business', 'cash_register_record', array('cash_register_id'=>$cash_register_id, 'employee_id'=>$employee_id), 
                'Cash register set for this terminal (#%cash_register_id%) can not be used by this employee. '
                . 'Employee #%employee_id% belongs to a different business.'
                . 'If you want to use this terminal with a different business, please unselect asociated cash register'));
        }
    }
    
    protected function checkCurrentEmployee($cash_register_id, $employee_id)
    {
        if (!$this->isCurrentEmployee($cash_register_id, $employee_id)) {
            throw new Exception(get_translation('last_employee_different', 'cash_register_record', array(), 'Cash register is currently being used by a different employee.'));
        }
    }
    
    protected function checkClosedRegister($cash_register_id)
    {
        if (!$this->isClosedRegister($cash_register_id)) {
            throw new Exception(get_translation('error_not_closed', 'cash_register_record', array(), 'Cash register is not closed.'));
        }
    }
    
    protected function checkOpenRegister($cash_register_id)
    {
        if (!$this->isOpenRegister($cash_register_id)) {
            throw new Exception(get_translation('error_not_open', 'cash_register_record', array(), 'Cash register is not open.'));
        }
    }
    
    protected function checkNotOpenRegister($cash_register_id)
    {
        if ($this->isOpenRegister($cash_register_id)) {
            throw new Exception(get_translation('error_not_open', 'cash_register_record', array(), 'Cash register is open.'));
        }
    }
    
    protected function checkCashRegisterSelected($cash_register_id, $employeeData)
    {
        if (!$cash_register_id && !$this->is_pos_manager($employeeData)) {
            $message = get_translation("no_register_no_manager", "pos/global", array(), "A manager needs to select and start the drawer before being able to use this.");
            throw new Exception($message);
        }
    }
    
    //-------------- Can do --------------------------------------
    
    protected function canDoLogin($cash_register_id, $employeeData)
    {
        if (!$cash_register_id) {
            if ($this->is_pos_manager($employeeData)) {
                return true;
            } else {
                $message = get_translation("no_register_no_manager", "pos/global", array(), 'Please ask a Manager to Open & Setup the cash register drawer for this session.');
                throw new Exception($message);
            }
        }
        
        $employee_id = $employeeData->employeeID;
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        
        if ($this->is_pos_manager($employeeData)) {
            return true;
        } else {
//            $this->checkOpenRegister($cash_register_id);
        }
        
        return $this->isFree($cash_register_id) || $this->checkCurrentEmployee($cash_register_id, $employee_id);
    }
    
    protected function canDoLogout($cash_register_id, $employeeData)
    {
        $employee_id = $employeeData->employeeID;
        if (!$cash_register_id && $this->is_pos_manager($employeeData))
        {
            return true;
        }
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
    }
    
    protected function canDoStartDrawer($cash_register_id, $employeeData)
    {
        $employee_id = $employeeData->employeeID;
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        $this->checkCurrentEmployee($cash_register_id, $employee_id);
//        $this->checkIsManager($employeeData);
        $this->checkNotOpenRegister($cash_register_id);
    }
    
    protected function canDoEndDrawer($cash_register_id, $employeeData)
    {
        $employee_id = $employeeData->employeeID;
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        $this->checkCurrentEmployee($cash_register_id, $employee_id);
//        $this->checkIsManager($employeeData);
        $this->checkOpenRegister($cash_register_id);
    }
    
    protected function canDoOpenDrawer($cash_register_id, $employeeData)
    {
        $employee_id = $employeeData->employeeID;
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        $this->checkCurrentEmployee($cash_register_id, $employee_id);
    }
    
    protected function canDoAddRecord($cash_register_id, $employee_id)
    {
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        $this->checkOpenRegister($cash_register_id);
        $this->checkCurrentEmployee($cash_register_id, $employee_id);
    }
    
    //-------------- Add --------------------------------------
    
    public function switchCashRegister($old_cash_register_id, $new_cash_register_id, $employeeData, $notes = '')
    {
        $data = array('amount'=>0);
        
        $this->logout($data, $old_cash_register_id, $employeeData, $notes);
        $this->login($data, $new_cash_register_id, $employeeData, $notes);
        
        return array(
            'success' => TRUE,
            'message' => get_translation('register_switched', 'cash_register_record', array(
                'old_cash_register_id' => $old_cash_register_id,
                'new_cash_register_id' => $new_cash_register_id,
                ), 'Successfuly siwtched from register #%old_cash_register_id% to register #%new_cash_register_id%.'),
        );
    }

    public function login($data, $cash_register_id, $employeeData, $notes = '')
    {
        $employee_id = $employeeData->employeeID;
        $this->canDoLogin($cash_register_id, $employeeData);
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'non-monetary', 'login');
    }

    public function logout($data, $cash_register_id, $employeeData, $notes = '')
    {
        $employee_id = $employeeData->employeeID;
        
        $this->canDoLogout($cash_register_id, $employeeData);
        if ($this->isCurrentEmployee($cash_register_id, $employee_id)) {
            $type = 'logout';
        } else {
            // log but not affect status
            $type = 'unneeded-logout';
        }
        
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'non-monetary', $type);
    }

    public function startDrawer($data, $cash_register_id, $employeeData, $notes = '')
    {
        $employee_id = $employeeData->employeeID;
        
        $this->canDoStartDrawer($cash_register_id, $employeeData);
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'cash', 'drawer-start');
    }

    public function endDrawer($data, $cash_register_id, $employeeData, $notes = '')
    {
        $employee_id = $employeeData->employeeID;
        
        $this->canDoEndDrawer($cash_register_id, $employeeData);
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'cash', 'drawer-end');
    }
   
    public function openDrawer($data, $cash_register_id, $employeeData, $notes = '')
    {
        $employee_id = $employeeData->employeeID;
        
        $this->canDoOpenDrawer($cash_register_id, $employeeData);
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'cash', 'drawer-open');
    }

    public function addCreditCardSale($data, $cash_register_id, $employee_id, $notes = '')
    {
        $this->canDoAddRecord($cash_register_id, $employee_id);
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'credit-card', 'sale');
    }

    public function addCashSale($data, $cash_register_id, $employee_id, $notes = '')
    {
        $this->canDoAddRecord($cash_register_id, $employee_id);
        return $this->add($data, $cash_register_id, $employee_id, $notes, 'cash', 'sale');
    }

    public function addRefund($data, $cash_register_id, $employeeData, $payment_method_slug, $notes = '')
    {
        $employee_id = $employeeData->employeeID;
        
        $order_id = $data['order_id'];
        $amount = $data['amount'];
        if ($amount > 0) {
            $amount *= -1;
        }
        
        $_data = array(
            'amount' => $amount,
            'paid_orders' => array(
                $order_id,
            ),
        );
        $_data['details'][$order_id]['amount'] = $amount;
        
        $this->canDoAddRecord($cash_register_id, $employee_id);
        return $this->add($_data, $cash_register_id, $employee_id, $notes, $payment_method_slug, 'refund');
    }

    protected function add($data, $cash_register_id, $employee_id, $notes, $payment_method_slug, $record_type_slug)
    {
        $data['change'] = $data['change'] ? $data['change'] : 0;
        
        $this->load->model('payment_method_model');
        $this->load->model('cash_register_record_type_model');

        $record = new Cash_Register_Record();
        
        $record->cash_register_id = $cash_register_id;
        $record->payment_method_id = $this->payment_method_model->getIdBySlug($payment_method_slug);
        $record->cash_register_record_type_id = $this->cash_register_record_type_model->getIdBySlug($record_type_slug);

        $record->amount = $data['amount'];
        $record->change = $data['change'];
        $record->paid = $data['amount'] - $data['change'];
        
        $record->notes = $notes;
        $record->employee_id = $employee_id;
//        $record->device_identifier = $data['device_identifier'];

        $record->save();
        $recordId = $record->getId();
        
        if (!$recordId) {
            throw new Exception(get_translation('record_error_general', 'cash_register_record', array(), 'Error while processing information.'));
        }
        
        $this->load->model('cash_register_record_order_model');
        
        foreach ($data['paid_orders'] as $order_id) {
            $this->cash_register_record_order_model->save(array(
                'order_id' => $order_id,
                'paid' => $data['details'][$order_id]['amount'],
                'cash_register_record_id' => $recordId,
            ));
        }
        
        return array(
            'success' => TRUE,
            'message' => get_translation('record_added', 'cash_register_record', array(), 'Information added succesfully.'),
        );
    }
    
    //-------------- Get --------------------------------------
    
    public function getReportableDates($cash_register_id, $employeeData, $date_range_local)
    {
        $employee_id = $employeeData->employeeID;
        $business_id = $employeeData->business_id;
        $date_format = "Y-m-d H:i:s";
        
        $localDateTimeInfo = getLocalDateTimeInfo(null, $business_id);
        $timezone = $localDateTimeInfo['tz'];
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        
        $date_range_gmt = array (
            'from_date' => convert_from_local_to_gmt($date_range_local['from_date'], $date_format, $business_id),
            'to_date' => convert_from_local_to_gmt($date_range_local['to_date'], $date_format, $business_id),
        );
        
        $query = "SELECT DISTINCT DATE(CONVERT_TZ(date_created, '+00:00', '$timezone')) date_created_local
            FROM `cash_register_record`
            LEFT JOIN cash_register_record_type ON cash_register_record_type_id=cash_register_record_typeID
            WHERE cash_register_record_type.slug IN ('sale', 'refund') 
            AND `date_created` >=  ?
            AND `date_created` <=  ?
            AND `cash_register_id` =  ?";
        
        $result = $this->db->query($query, array($date_range_gmt['from_date'], $date_range_gmt['to_date'], $cash_register_id))->result();
        $data = array_column_7($result, 'date_created_local');
        
        return array(
            'success' => true,
            'data' => $data,
        );
    }
    
    public function getReport($cash_register_id, $employeeData, $date_range_local)
    {
        $employee_id = $employeeData->employeeID;
        $date_format = "Y-m-d H:i:s";
        
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        
        $paymentMethodSlug = null; // all
        $register_close_id = null; //all
        $date_range_gmt = array (
            'from_date' => convert_from_local_to_gmt($date_range_local['from_date'], $date_format, $employeeData->business_id),
            'to_date' => convert_from_local_to_gmt($date_range_local['to_date'], $date_format, $employeeData->business_id),
        );
        
        $data = $this->getSummary($cash_register_id, $employee_id, $register_close_id, $paymentMethodSlug, $date_range_gmt);

        return array(
            'success' => true,
            'data' => $data,
        );
    }
    
    public function getClosedCashStatus($cash_register_id, $employee_id, $register_close_id)
    {
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        
        $data = $this->getRegisterCashSummary($cash_register_id, $employee_id, $register_close_id);

        return array(
            'success' => true,
            'data' => $data,
        );
    }
    
    public function getCurrentCashStatus($cash_register_id, $employee_id)
    {
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        $this->checkCurrentEmployee($cash_register_id, $employee_id);
        
        try {
            $this->checkNotOpenRegister($cash_register_id);
            return array(
                'success' => true,
                'data' => array('drawer-open' => false),
                'message' => get_translation('ready_to_open', 'cash_register_record', array(), 'Cash register is ready to start.')
            );
        } catch (Exception $exc) {
            
        }
        
        try {
            $this->checkOpenRegister($cash_register_id);
            
            $data = $this->getCurrentCashSummary($cash_register_id, $employee_id);
            $data['drawer-open'] = true;
            
            return array(
                'success' => true,
                'message' => get_translation('ready_to_add', 'cash_register_record', array(), 'Cash register is open.'),
                'data' => $data,
            );
        } catch (Exception $exc) {
            
        }
    }
    
    protected function getRegisterCashSummary($cash_register_id, $employee_id, $register_close_id)
    {
        $paymentMethodSlug = 'cash';
        
        return $this->getSummary($cash_register_id, $employee_id, $register_close_id, $paymentMethodSlug);
    }
    
    protected function getCurrentCashSummary($cash_register_id, $employee_id)
    {
        $register_close_id = 'last';
        $paymentMethodSlug = 'cash';
        
        return $this->getSummary($cash_register_id, $employee_id, $register_close_id, $paymentMethodSlug);
    }
    
    protected function getSummary($cash_register_id, $employee_id, $register_close_id = null, $paymentMethodSlug = 'cash', $date_range_gmt = array())
    {
        $context = array();
        
        if ($register_close_id === 'last') {
            $lastOpen = $this->getLastOpen($cash_register_id, array());
            $context['from_record_id'] = $lastOpen->{$this->primary_key};
            $this->db->where($this->primary_key . " >= ", $context['from_record_id']);
        } else if ($register_close_id) {
            $context['to_record_id'] = $register_close_id;
            
            $lastOpen = $this->getLastOpen($cash_register_id, $context);
            $context['from_record_id'] = $lastOpen->{$this->primary_key};
            
            $this->db->where($this->primary_key . " >= ", $context['from_record_id']);
            $this->db->where($this->primary_key . " <= ", $context['to_record_id']);
        } 
        
        if ($date_range_gmt) {
            $context = array_merge($context, $date_range_gmt);
            
            $this->db->where('date_created' . " >= ", $date_range_gmt['from_date']);
            $this->db->where('date_created' . " <= ", $date_range_gmt['to_date']);
        }
        
        if ($paymentMethodSlug) {
            $this->db->where("payment_method.slug", $paymentMethodSlug);
        } else {
            $this->db->group_by('payment_method_id');
        }
        
        $this->db->group_by('cash_register_record_type_id');
        
        $this->db->select("cash_register_record_type.slug record_type, "
                . "payment_method.slug payment_method, "
                . "SUM(amount) amount, "
                . "SUM(paid) paid, "
                . "SUM(`change`) `change`, "
                . "COUNT({$this->primary_key}) `count`,  "
                . "MAX(date_created) last_entry,"
                . "GROUP_CONCAT({$this->primary_key} ORDER BY {$this->primary_key} ASC) `source`, "
                . "employee.firstName, "
                . "employee.lastName ");
        
        $this->db->join('cash_register_record_type', 'cash_register_record_type_id=cash_register_record_typeID', 'left');
        $this->db->join('payment_method', 'payment_method_id=payment_methodID', 'left');
        $this->db->join('employee', 'employee_id=employeeID', 'left');
        
        $this->db->where("cash_register_id", $cash_register_id);
        
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        
        $employee = new Employee($employee_id);
        
        $ignore = array(
            'drawer-end',
        );
        
        $total = array();
        $summary = array();
        foreach ($result as $item) {
            if (!in_array( $item->record_type, $ignore)) {
                $total[$item->record_type] += $item->paid;
                $total[$item->payment_method] += $item->paid;
                $total['all'] += $item->paid;
            }
            
            $item->last_entry_local = getLocalDateTimeInfo($item->last_entry, $employee->business_id);
            
            $item->source = explode(',', $item->source);
            
            $group = $item->record_type;
            if (!$paymentMethodSlug) {
                $group .= '-' . $item->payment_method;
            }
            
            $summary[$group] = $item;
        }
        
        $summary['total'] = $total;
        
        $summary['source'] = $context;
        
        return $summary;
    }
    
    public function getCloseHistory($cash_register_id, $employeeData)
    {
        $since = "-1 week";
        $since_date = gmdate("Y-m-d 00:00:00", strtotime($since));
        
        $employee_id = $employeeData->employeeID;
        $this->checkCashRegisterEmployee($cash_register_id, $employee_id);
        
        $context = array(
            'from_date' => $since_date,
        );
        $results = $this->getLastRecords($cash_register_id, 0, 'drawer-end', $context);
        
        foreach ($results as $key => $item) {
            $item->date_created_local = getLocalDateTimeInfo($item->date_created, $employeeData->business_id);
            $results[$key] = $item;
        }
        
        return array(
            'success' => true,
            'data' => $results,
        );
    }
}
