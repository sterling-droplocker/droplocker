<?php
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\Business;

class Automat_Model extends My_Model
{
    protected $tableName = 'automat';
    protected $primary_key = 'automatID';

    /**
     * returns data from the automat table
     *
     * @param string $barcode
     * @param int $orderID
     * @return array of objects
     */
    public function barcodeDetail($barcode, $orderID)
    {
        $sqlGetHistory = "select *
            from automat
            where garcode in ($barcode)
            and order_id = $orderID
            order by fileDate";
        $query = $this->db->query($sqlGetHistory);
        $history = $query->result();

        return $history;
    }


    /**
     * Inserts the item into the automat table
     *
     * @param string $barcode
     * @param int $order_id
     * @param int $business_id
     * @return array
     *
     * return array keys
     * -----------------
     * status string error - only if the item already exists
     * message string - only if the item already exists
     * insert_id
     *
     */
    public function insertRecieptPrinter($barcode, $order_id, $business_id)
    {
        $result['status'] = 'success';

        $str = "Manually Scanned at Receipt Printer";
        $this->db->where('gardesc', $str);
        $this->db->where('order_id', $order_id);
        $this->db->where('garcode', $barcode);
        $query = $this->db->get('automat');
        $q = $this->db->last_query();
        $item = $query->result();
        if ($item) {
            $result['status'] = 'error';
            $result['message'] = 'Item already added to order';
        }

        $order = new \App\Libraries\DroplockerObjects\Order($order_id);
        $locker = new \App\Libraries\DroplockerObjects\Locker($order->locker_id);
        $location = new \App\Libraries\DroplockerObjects\Location($locker->location_id);

        $business = new Business($business_id);
        $options['gardesc'] = $str;
        $options['garcode'] = $barcode;
        $options['order_id'] = $order_id;
        $options['slot'] = $location->sortOrder; //sortOrder
        $options['arm'] = $location->route_id; //route
        //possible double GMT conversion
        $options['fileDate'] = convert_to_gmt_aprax(date("Y-m-d H:i:s"), $business->timezone);
        $this->db->insert('automat', $options);

        $result['insert_id'] = $this->db->insert_id();

        return $result;

    }

    /**
     * updates the outPos field in the auomat table
     *
     * @param string $barcode
     * @return int
     */
    public function updateAutomatOutPos($automatID, $position)
    {
       if ($automatID) {
            $sql = "update automat set outPos = $position where automatID = $automatID;";
            $result = $this->db->query($sql);
       }

       return $result;
    }

    /**
     * takes the barcode and gets the active order_id from the automat table
     *
     * @param string $barcode
     * @return int
     */
    public function getOrderId($barcode)
    {
        $barcode = new Barcode($barcode);
        $item_id = $barcode->item_id;

        if (empty($item_id)) {
            throw new Exception("Missing item ID");
        }

        $sql = "SELECT order_id FROM orderItem oi
                INNER JOIN orders on orderID = order_id
                    AND orders.orderStatusOption_id NOT IN (9,10)
                WHERE oi.item_id = {$item_id}";
        $query = $this->db->query($sql);
        $order = $query->row();
        $order_id = $order->order_id;

        return $order_id;
    }



    /**
     * get the total number of items in an order and the number count in the automat table.
     *
     * Used for printing receipts
     *
     * Example output: item 2 of 6
     * in printer controller, when automat count = orderItem count, display a special message like "order complete"
     *
     * @param string $barcode
     * @param int $order_id optional
     * @throws exception for missing item_id or missing order_id
     * @return array
     */
    public function getOrderItemCounts($order_id)
    {
        if (empty($order_id)) {
            throw new Exception("Missing order ID");
        }

        // Scanned at Receipt Printer
        // Get the total item count  in the order for the order
        $sql = "SELECT count(orderItemID) as total FROM orderItem WHERE order_id = {$order_id}";
        $query = $this->db->query($sql);
        $result = $query->row();
        $orderTotalItems = $result->total;

        // Get the total item count  in the automat table for the order
        $sql = "SELECT automatID FROM automat WHERE order_id = {$order_id} and gardesc = 'Manually Scanned at Receipt Printer' group by garcode";
        $query = $this->db->query($sql);
        //$result = $query->result();
        $automatItemCount = $query->num_rows();

        return array('order_id'=>$order_id, 'totalItems'=>$orderTotalItems, 'automatCount'=>$automatItemCount);
    }

}
