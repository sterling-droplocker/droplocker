<?php
class Product_Supplier_Model extends My_Model
{
    protected $tableName = 'product_supplier';
    protected $primary_key = 'product_supplierID';

    /**
     * gets the default supplier for a product_process
     * @param int $product_process_id
     * @return stdClass object
     */
    public function getDefault($product_process_id, $order_id = 0)
    {
        if (!is_numeric($product_process_id)) {
            throw new \InvalidArgumentException("'product_process_id' must be numeric.");
        }

        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric.");
        }

        // If we don't have order, get the default supplier
        if ($order_id == 0) {
            return $this->getSupplierByProductID($product_process_id);
        }

        //get product type slug first
        $getType = "select module.slug from product_process
                        join product on productID = product_id
                        join productCategory on productCategory_id = productCategoryID
                        join module on productCategory.module_id = moduleID
                        where product_processID = ?";
        $sql = $this->db->query($getType, array($product_process_id));
        $type = $sql->row();
        
        $orderServiceDay_id = $this->getOrderServiceDayId($order_id);

        //find product based on route and product type slug (or 'All')
        $getProductType = "SELECT productType, supplierID from `supplierWFRoutes`
                        JOIN supplier on supplierID = supplier_id
                        LEFT JOIN supplierServiceDays ssd on ssd.supplier_id = supplierID 
                        WHERE business_id = ? AND productType IN (?,?)
                        AND (ssd.day_id = ? OR ssd.day_id IS NULL)
                        AND route_id = (SELECT route_id FROM orders
                            JOIN locker on lockerID = locker_id
                            JOIN location on locationID = location_id
                            WHERE orderID = ?)";

        //FIXME: bussiness_id is coming from the controller using __get() magic, this needs to be a formal parameter of the method
        $productType = $this->db->query($getProductType, array($this->business_id, 'ALL', $type->slug, $orderServiceDay_id, $order_id))->row();

        if (!empty($productType)) {
            $supplier = $this->getSupplierByProductAndSupplierID($product_process_id, $productType->supplierID);
            if ($supplier) {
                return $supplier;
            }
        } 
       
        return $this->getSupplierByProductID($product_process_id);  
    }
    
    public function getOrderServiceDayId($order_id)
    {
        $statusId = 1;//Picked Up
        
        $deliveryDayIdQuery = "SELECT DAYOFWEEK(date) day_id "
            . "FROM orderStatus "
            . "WHERE order_id = ? AND orderStatusOption_id = ? "
            . "ORDER BY date ASC ";
        $result = $this->db->query( $deliveryDayIdQuery,  array($order_id, $statusId) )->row();
        
        return $result->day_id;
    }

    public function getSupplierByProductID($product_process_id)
    {
        return $this->db->query("SELECT supplier_id,cost FROM product_supplier WHERE product_process_id = ? AND `default` = 1",
            array($product_process_id))->row();
    }

    public function getSupplierByProductAndSupplierID($product_process_id, $supplier_id)
    {
        return $this->db->query("SELECT supplier_id,cost FROM product_supplier WHERE product_process_id = ? AND supplier_id = ?",
            array($product_process_id, $supplier_id))->row();
    }

    /**
     *
     *
     * @param int $product_process_id
     * @param int $supplier_id
     * @param int $default
     * @param int $cost
     * @param int $minQty
     * @return
     */
    public function createSupplierForProductProcess($product_process_id, $supplier_id, $default=1, $cost=0, $minQty=0)
    {
            $insert_values['product_process_id'] = $product_process_id;
            $insert_values['supplier_id'] = $supplier_id;
            $insert_values['default'] = $default;
            $this->db->insert($this->tableName, $insert_values);
    }

}
