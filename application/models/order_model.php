<?php
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\LockerLoot;
use App\Libraries\DroplockerObjects\OrderStatus;
use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\OrderItem;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\ProductCategory;

class Order_Model extends My_Model
{
    protected $tableName = 'orders'; // TABLE IS PLURAL
    protected $primary_key = 'orderID';

    public $order_id = '';
    public $order;
    public $customer;
    public $alertType = 'html';
    public $business;

    /**
     * Returns the combined order status and transaction history for the order sorted by the creation date of the entry..
     * @param int $order_id The primary key of an Order entity
     * @return array Each element contains the following keys: "date","name", 'note',"employee", "locker", "lockerID"
     */
    public function get_history($order_id)
    {
        $history = array();

        $order = $this->get_by_primary_key($order_id);
        $this->load->model("business_model");
        $business = $this->business_model->get_by_primary_key($order->business_id);
        $this->load->model("orderstatus_model");
        //$orderStatuses = $this->orderstatus_model->get_aprax(array("order_id" => $order->orderID));

        $orderStatuses = $this->db->query("SELECT orderStatusID, note, orderStatus.date,
                employee.firstName AS employee_firstName, employee.lastName AS employee_lastName,
                locker.lockerName, locker_id, orderStatusOption.name AS orderStatusOption_name, orderStatusOption_id
            FROM orderStatus
            LEFT JOIN locker ON locker_id=lockerID
            LEFT JOIN employee ON employee_id=employeeID
            LEFT JOIN orderStatusOption ON orderStatusOptionID=orderStatusOption_id
           WHERE order_id = ?
           ORDER BY orderStatusID ASC
        ", array($order->orderID))->result();

        $this->load->model("orderstatusoption_model");
        $this->load->model("locker_model");
        $this->load->model("employee_model");
        foreach ($orderStatuses as $orderStatus) {
            if (is_numeric($orderStatus->orderStatusOption_id)) {
                if ($orderStatus->orderStatusOption_name == "Loaded on Van") {
                    $orderStatusOption_name = get_translation("status_loaded_on_van", "admin/orders/history", array(), "Loaded on Van [<a href='/admin/orders/unload/{$order->orderID}'> Unload </a>]");
                } else {
                    $orderStatusOption_name = $orderStatus->orderStatusOption_name;
                }
            } else {
                $orderStatusOption_name = get_translation("status_unknown_option", "admin/orders/history", array(), "--Unknown Order Status Option--");
            }
            if (empty($orderStatus->lockerName)) {
                $locker_name = get_translation("status_unknown_locker", "admin/orders/history", array(), "Unknown Locker");
            } else {
                $locker_name = $orderStatus->lockerName;
            }
            if (empty($orderStatus->employee_firstName) && empty($orderStatus->employee_lastName)) {
                $employee_name =  get_translation("status_unknown_employee", "admin/orders/history", array(), "--Unknown Employee--");
            } else {
                $employee_name = getPersonName($orderStatus, true, 'employee_firstName', 'employee_lastName');
            }
            $orderStatus_date = new DateTime($orderStatus->date, new DateTimeZone("GMT"));
            $orderStatus_date->setTimeZone(new DateTimeZone($business->timezone));

            $history[] = array(
                "id" => $orderStatus->orderStatusID,
                "type" => "status",
                "date" => $orderStatus_date,
                "name" => $orderStatusOption_name,
                "employee" => $employee_name,
                "lockerID" => $orderStatus->locker_id,
                "locker" => $locker_name,
                "note" => $orderStatus->note
            );
        }
        $this->load->model("transaction_model");
        $transactions = $this->transaction_model->get_aprax(array("order_id" => $order->orderID));
        $this->load->model("customer_model");
        foreach ($transactions as $transaction) {
            if (is_numeric($transaction->customer_id)) {
                $customer = $this->customer_model->get_by_primary_key($transaction->customer_id);
            } else {
                $customer = null;
            }
            if ($transaction->message == "Approved") {
                $resultMessage = get_translation("status_paid", "admin/orders/history", array(), "Paid ");
            } else {
                $resultMessage = $transaction->message;
            }
            switch ($transaction->processor) {
                case 'verisign':
                    $link = 'https://manager.paypal.com/searchTranx.do?subaction=transDetails&timezone=U.S.%20Pacific&reportName=SearchTransaction&id='.$transaction->pnref;
                    break;
                case 'authorizenet':
                    $link = 'https://account.authorize.net/UI/themes/anet/transaction/transactiondetail.aspx?transID='.$transaction->pnref;
                    break;
                case 'paypro':
                    $link = "https://businessview.paygateway.com";
                    break;
                case 'paypalprocessor':
                    $link = "https://www.paypal.com/cgi-bin/webscr?cmd=_view-a-trans&id=" . $transaction->pnref;
                    break;
                case 'coinbase':
                    $link = '#';
                    $tokens = json_decode($transaction->pnref, true);
                    $transaction->pnref = $tokens['access_token'];
                    break;
                case 'cybersource':
                    $link = 'https://ebc.cybersource.com/ebc/transactionsearch/TransactionSearchDetailsLoad.do?requestSearch=true&requestId=' . $transaction->pnref;
                    break;
                default:
                    $link = '';
            }
            if (is_numeric($transaction->employee_id)) {
                $employee = $this->employee_model->get_by_primary_key($transaction->employee_id);
                if (empty($employee)) {
                    $employee_name = get_translation("status_unknown_employee", "admin/orders/history", array(), "--Unknown Employee--");
                } else {
                    $employee_name = "{$employee->firstName} {$employee->lastName}";
                }
            } else {
                $employee_name = get_translation("status_unknown_employee", "admin/orders/history", array(), "--Unknown Employee--");
            }
            $transaction_updated = new DateTime($transaction->updated, new DateTimeZone("GMT"));
            $transaction_updated->setTimezone(new DateTimeZone($business->timezone));
            $history[] = array(
                "id" => $transaction->transactionID,
                "type" => "transaction",
                "date" => $transaction_updated,
                "name" => $resultMessage.' <div style="float:right; padding-left:10px;">('.format_money_symbol($order->business_id, '%.2n', $transaction->amount).')</div><br><b>'.$transaction->processor.':</b><a style="float:right" href="'.$link.'" target="_blank">'.$transaction->pnref.'</a>',
                'note'=> getPersonName($customer)."<br> CustomerID: <a href='https://droplocker.com/admin/customers/detail/".$customer->customerID."'>".$customer->customerID."</a>",
                "employee" => $employee_name,
                "locker" => "",
                "lockerID" => ""
            );
        }
        //Sorts two arrays with a 'date' key that are both DateTime instances in ascending order.
        usort($history, function ($array1, $array2) {
            if ($array1['date'] == $array2['date']) {
                if ($array1['type'] == $array2['type']) {
                    return $array1['id'] > $array2['id'] ? -1 : 1;
                } else {
                    return $array1['type'] == 'transaction' ? 1 : -1;
                }
            }

            return $array1['date'] > $array2['date'] ? -1 : 1;
        });

        return $history;
    }
    public function update($options = array(), $where = array())
    {
        $this->db->update('orders', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * This is a very large sql statement used primarily on the orders/view controller to display all all open orders for a business
     *
     * @param int $business_id
     * @return array of objects
     */
    public function viewOrders($business_id)
    {
        $sql = "
            SELECT `orderID`, `location`.`address`, `dueDate`,
                `locationID`, locker.`lockerID`, locker.`lockerName`,
                `orderStatusOption`.`name` as statusName,
                `orderPaymentStatusOption`.`name` as paymentStatusName,
                `orderPaymentStatusOption_id`, orders.`orderStatusOption_id`,
                `bagNumber`, `orders`.`dateCreated`, `firstName`,
                `lastName`, `orders`.`customer_id`, `orders`.`llnotes`,
                    `customer`.`autoPay`, `claimID`, `route_id`, `orders`.`loaded`, `orders`.`orderType`,
                    customer.invoice,
                MAX(orderStatus.date) AS originalLockerOrderStatusDate,
                orderStatus.locker_id AS ordiginalLockerID,
                orderStatusLocker.lockerName AS originalLockerName

                FROM `orders`
                JOIN `locker` ON `orders`.`locker_id` = `locker`.`lockerID`
                JOIN `location` ON `location`.`locationID` = `locker`.`location_id`
                JOIN `orderStatusOption` ON `orderStatusOption`.`orderStatusOptionID` = `orders`.`orderStatusOption_id`
                JOIN `orderPaymentStatusOption` ON `orderPaymentStatusOptionID` = `orders`.`orderPaymentStatusOption_id`
                LEFT JOIN `customer` ON `customerID` = `orders`.`customer_id`
                LEFT JOIN `claim` ON `customerID` = `claim`.`customer_id` AND claim.locker_id = locker.lockerID AND claim.active = 1
                LEFT JOIN `bag` ON `bagID` = `orders`.`bag_id`
                LEFT JOIN orderStatus ON orderStatus.order_id = orders.orderID AND orderStatus.orderStatusOption_id=1
                LEFT JOIN locker orderStatusLocker ON orderStatus.locker_id=orderStatusLocker.lockerID
                WHERE `orders`.`business_id` = ?
                AND orders.`orderStatusOption_id` NOT IN (9, 10)
                GROUP BY orderID
                ORDER BY `location`.`address`, locker.`lockerName`, `firstName`, `orderPaymentStatusOption_id` DESC";

        $query = $this->db->query($sql, array($business_id));

        return $query->result();
    }

    public function view_by_due_date($business_id, $due_date, $past_orders = false)
    {
        $operator = $past_orders ? '<=' : '=';

        $sql = "
        SELECT `orderID`, `location`.`address`, `dueDate`,
        `locationID`, locker.`lockerID`, locker.`lockerName`,
        `orderStatusOption`.`name` as statusName,
        `orderPaymentStatusOption`.`name` as paymentStatusName,
        `orderPaymentStatusOption_id`, orders.`orderStatusOption_id`,
        `bagNumber`, `orders`.`dateCreated`, `firstName`,
        `lastName`, `orders`.`customer_id`, `orders`.`llnotes`,
        `customer`.`autoPay`, `claimID`, `route_id`, `orders`.`loaded`, `orders`.`orderType`,
        customer.invoice,
        MAX(orderStatus.date) AS originalLockerOrderStatusDate,
        orderStatus.locker_id AS ordiginalLockerID,
        orderStatusLocker.lockerName AS originalLockerName

        FROM `orders`
        JOIN `locker` ON (`orders`.`locker_id` = `locker`.`lockerID`)
        JOIN `location` ON (`location`.`locationID` = `locker`.`location_id`)
        JOIN `orderStatusOption` ON (`orderStatusOption`.`orderStatusOptionID` = `orders`.`orderStatusOption_id`)
        JOIN `orderPaymentStatusOption` ON (`orderPaymentStatusOptionID` = `orders`.`orderPaymentStatusOption_id`)
        LEFT JOIN `customer` ON (`customerID` = `orders`.`customer_id`)
        LEFT JOIN `claim` ON (`customerID` = `claim`.`customer_id` AND claim.locker_id = locker.lockerID AND claim.active = 1)
        LEFT JOIN `bag` ON `bagID` = `orders`.`bag_id`
        LEFT JOIN orderStatus ON (orderStatus.order_id = orders.orderID AND orderStatus.orderStatusOption_id = 1)
        LEFT JOIN locker orderStatusLocker ON (orderStatus.locker_id = orderStatusLocker.lockerID)
        WHERE `orders`.`business_id` = ?
        AND `orders`.dueDate $operator ?
        AND orders.`orderStatusOption_id` NOT IN (9, 10)
        GROUP BY orderID
        ORDER BY dueDate ASC";

        $query = $this->db->query($sql, array($business_id, $due_date));

        return $query->result();
    }

    /**
     * Removes all items from an order.
     *
     * @param type $order_id
     * @throws Exception
     */
    public function remove_all_items($order_id)
    {
        $this->load->model("orderItem_model");
        if (!is_numeric($order_id)) {
            throw new Exception("'order_id' must be numeric.");
        }
        $this->orderItem_model->options = array("order_id" => $order_id);
        $this->orderItem_model->delete(array("order_id" => $order_id));
    }

    /**
     * Sets the order object
     * @param object or int $order
     */
    public function set_order($order)
    {
        //passed only the order_id
        if (is_numeric($order)) {
            $this->order = $this->get(array('orderID' => $order));
        } else {
            $this->order = $order;
        }
    }

    /**
     * Checks to see if there is an open order for a bag
     *
     * @param string $bagNumber
     * @param int $business_id
     * @return int orderID if found
     */
    public function checkOpenOrder($bagNumber, $business_id)
    {
        // trim 0
        $bagNumber = ltrim($bagNumber, '0');

        $this->db->join("bag", "bagID=bag_id");
        $this->db->where("bagNumber", $bagNumber);
        if (is_numeric($business_id)) {
            $this->db->where("orders.business_id", $business_id);
        }
        $this->db->where_in("orderStatusOption_id", array(1,2,3));
        $orders = $this->db->get("orders");
        $order = $orders->row();

        $order_id = isset($order->orderID)?$order->orderID:'';

        return $order_id;
    }



    /**
     * The following function is *
     * @deprecated Use the Order Droplocker object for creating new Order entities
     * --------------------------------
     * new_order method creates a new order
     *
     * NOTE: options['deltime'] must be converted to gmt. If not passed, we use the current
     * system time and convert it. If you do not convert the options['deltime'] to gmt, it will
     * not create the proper dataTime for the pickup of the order
     *
     * @param int location_id
     * @param int locker_id
     * @param int bagNumber
     * @param int employee_id
     * @param string notes
     * @param int business_id
     * @param array $options
     *
     * Options Values
     * --------------------------
     * array claim
     * array items
     * int orderStatusOption_id default = 1
     * int paymentStatusOption_id default = 1
     * array charges
     * datetime deltime
     * int claimID
     * int customer_id
     * string llnotes
     *
     * @return int insert_id
     */
    public function new_order($location_id, $locker_id, $bagNumber, $employee_id, $notes, $business_id, $options)
    { //Note, 'location_id' does not seem to be used in this function
        $this->load->model('claim_model');
        $this->load->model('locker_model');
        $this->load->model('location_model');
        $this->load->model('bag_model');
        $this->load->model('customer_model');
        $this->load->model('orderstatus_model');
        $this->load->model('orderpaymentstatus_model');

        $deactivate_claim = true;
        $search_order = array();

        if (!is_numeric($bagNumber)) {
            $this->load->helper("barcode_helper");
            $bagNumber = parse_barcode($bagNumber);
        }

        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        } elseif (!is_numeric($locker_id)) {
            throw new \InvalidArgumentException("'locker_id' must be numeric.");
        } elseif (!is_numeric($bagNumber)) {
            throw new \InvalidArgumentException("'bagNumber' must be numeric.");
        } else {
            $bag = array();
            $business = new Business($business_id);

            // IT is possible that the order was stored on the driver phone for a long time before syncing. There for if the deliverytime is
            // sent from the driver phone, use that time, else, just use the current GMT dateTime
            $dateCreated = (array_key_exists("deltime", $options))?$options['deltime']:convert_to_gmt_aprax();

            $customer_id = isset($options['customer_id'])?$options['customer_id']:"";

            //get the locker model
            $locker = $this->locker_model->get(array('lockerID' => $locker_id));

            // Check to see if this is a new bag
            // This will create a new bag if not found
            // returns an array - bagID, customer_id, is_new
            if ($bagNumber != 0) {
                $bag = $this->bag_model->get_id($bagNumber, $business_id);
            }

            //set the defaults for statuses
            if (isset($options['orderStatusOption_id'])) {
                $orderStatusOption_id = $options['orderStatusOption_id'];
            } else {
                $orderStatusOption_id = 1;
            }

            if (isset($options['orderPaymentStatusOption_id'])) {
                $orderPaymentStatusOption_id = $options['orderPaymentStatusOption_id'];
            } else {
                $orderPaymentStatusOption_id = 1;
            }
            $orderPaymentStatusOption_id = isset($options['orderPaymentStatusOption_id']) ? $options['orderPaymentStatusOption_id'] : 1;

            // below is handling of the claim scenarios
            $passedClaimID = isset($options['claimID'])?$options['claimID']:'';
            if (empty($passedClaimID)) {
                // The locker_id will be 0 for certain orders, such as creditCard authorizations
                if (!empty($locker_id)) {
                    //get the locker Object
                    $lockerObj = new Locker($locker_id);

                    // check to see if this locker can handle multiple claims
                    if ($lockerObj->is_multi_order()) {
                        $this->claim_model->locker_id = '';
                    } else {
                        $claimOptions = array();
                        $claimOptions['locker_id'] = $locker_id;
                        $claimOptions['business_id'] = $business_id;
                        $claim = $this->claim_model->get($claimOptions); //get the claim
                        if ($claim) {
                            $claim = $claim[0];
                        } else {
                            $claim = null;
                        }
                    }
                }
                if (!empty($bag['customer_id'])) {
                    $customer_id = $bag['customer_id'];
                }
            } else { //claimID has been passed
                $claim = \App\Libraries\DroplockerObjects\Claim::search_aprax(array('business_id'=>$business_id, 'claimID'=>$passedClaimID));
                $claim = $claim[0];
                //make sure we have a correct claimID
                if ($claim->active == 1) {
                    // is the bag owner the same as the claim customer
                    if (($bag['customer_id'] != $claim->customer_id) && !$bag['is_new']) {
                        $body = "While creating a new order, the bag customer does not match the claim customer";
                        $body .= "<br><br>The locker was claimed by CustomerID: " . $claim->customer_id . "<br>but we picked up a bag for CustomerID: " . $bag['customer_id'];
                        send_admin_notification("Bag customer does not match claim customer", $body, $business_id);
                        $deactivate_claim = false;
                        $claim = null;
                        $customer_id = $bag['customer_id'];
                    } else {
                        $customer_id = $claim->customer_id;
                    }
                } else {
                    // if the claim deals with multiple bags, the claim_id will be passed but the claim will be deactivated.
                    // check to see if the bag customer id, is the same as the claim customer id.
                    // if it is not, then contact support
                    if ($bag['customer_id'] != $claim->customer_id) {
                        send_admin_notification(
                            'Claim ID passed for new order, but the claim was not found',
                            "The system tried to create a new order for Claim ID [" . $options['claimID'] . "] "
                                . "but Claim ID [" . $options['claimID'] . "] was not found. "
                                . "Please check to ensure this was not an oversight.",
                            $business_id
                        );
                    }
                    //if there is a customer_id with the bag, use the bag customer_id
                    $customer_id = ($bag['customer_id'] != '') ? $bag['customer_id'] : "";
                }
            } // END claimID has been passed

            // even if there is a claim, but no locker
            if (($locker_id != null) && !empty($claim)) {
                if (!$customer_id) {
                    $customer_id = $claim->customer_id;
                }
                // add customer to the bag if the bag has no customer
                if (empty($bag['customer_id']) && !empty($bag['bagID'])) {
                    // will return 1 if the bag table was updated
                    if ($this->bag_model->addCustomerToBag($customer_id, $bag['bagID'])) {
                        $bag['customer_id'] = $customer_id;
                    } else {
                        throw new Exception("Error adding a customer to an empty bag");
                    }
                }
            } else {
                $claim = false;
            }

            /**
             * if there is an order already in the locker with a customer,
             * assign this bag to that customer
             */
            if (!$claim && !$customer_id) {
                $this->clear();

                $sql = "SELECT customer_id, bag_id, orderID, lockerName location
                        FROM orders
                        JOIN locker ON lockerID = orders.locker_id
                        WHERE locker_id = '{$locker_id}'
                        AND orderStatusOption_id NOT IN (9,10)
                        AND lockerName != 'N/A'";
                $search_order = $this->db->query($sql)->result();

                //if we find an order with the locker_id and it's not completed and we only get 1 result
                if ($search_order && sizeof($search_order) == 1) {
                    // set the customer_id
                    $customer_id = $search_order[0]->customer_id;

                    // will return 1 if the bag table was updated
                    if ($this->bag_model->addCustomerToBag($customer_id, $bag['bagID'])) {
                        $bag['customer_id'] = $customer_id;
                    } else {
                        throw new Exception("Error adding a customer to an empty bag");
                    }
                }
            }
            //if we don't have a locker yet
            if (!$locker) {
                $locker = $this->locker_model->get(array('lockerID' => @$search_order[0]->locker_id));
            }

            //deactivate the claim if true and pass the notes the to the order
            $claimCustomer_id = isset($claim->customer_id)?$claim->customer_id:'';
            if ($customer_id == $claimCustomer_id && !empty($claim->claimID)) {
                //$notes .= "\n".$claim->notes;
                $this->claim_model->deactivate_claim($claim->claimID, $employee_id);
            }

            $recentOrderFound = false;
            if (empty($customer_id)) {
                /*
                 *  If we do not have a customer at this point,
                 *  try to the get the customer from recent previous orders in the locker.
                */
                $recentOrderId = $this->orderstatus_model->getRecentLockerOrder($locker_id, $dateCreated);

                if (!empty($recentOrderId)) {
                    $recentOrder = new Order($recentOrderId);
                    $customer_id = $recentOrder->customer_id;
                    if ($this->bag_model->addCustomerToBag($customer_id, $bag['bagID'])) {
                        $bag['customer_id'] = $recentOrder->customer_id;
                        $notes = $recentOrder->notes;
                    } else {
                        throw new Exception("Error adding a customer to an empty bag");
                    }
                    $recentOrderFound = true;
                } else {
                    //can't find out who customer is, assigning it to blank customer
                    $customer_id = $business->blank_customer_id;
                }
            }

            //validate customer_id here after all possible assignments that variable could have.
            if (!intval($customer_id)) {
                throw new \InvalidArgumentException("'customer_id' must be numeric: $customer_id");
            }

            $this->customer_model->customerID = $customer_id;
            $this->customer_model->business_id = $business_id;

            /*
             * Make sure the bag is not already in an active order
             */
            $checkOrderID = $this->checkOpenOrder($bagNumber, $business_id);
            if ($checkOrderID) {
                $foundOrder = new Order($checkOrderID);
                send_admin_notification("There is already an open order [".$checkOrderID."] for this bag", "There is already an open order for this bag ".$bagNumber."<br>".formatArray($foundOrder->to_array())."</pre>", $business->businessID);

                return false;
            }

            // set the order fields
            $this->clear();
            if (array_key_exists('bagID', $bag)) {
                $this->bag_id = $bag['bagID'];
            } else {
                $this->bag_id = null;
            }
            $this->orderStatusOption_id = $orderStatusOption_id;
            $this->orderPaymentStatusOption_id = $orderPaymentStatusOption_id;
            $this->locker_id = $locker_id;
            $this->customer_id = $customer_id;
            $this->dateCreated = $dateCreated;
            $this->statusDate = $dateCreated;
            $this->orderPaymentStatusDate = $dateCreated;
            $this->business_id = $business_id;
            
            
            if ($options['due_date']) {
                $this->dueDate = $options['due_date'];
            } else {
                //figure out the due date
                //start by getting the location turnaround
                $getLocationInfo = "select * from location where locationID = ".$locker[0]->location_id;
                $locationInfo = $this->db->query($getLocationInfo)->row();
                // VERY IMPORTANT, DUE DATE IS STORED IN *LOCAL BUSINESS TIMEZONE*
                $dueDate = date("Y-m-d", strtotime(convert_from_gmt_aprax('now', "Y-m-d")." +$locationInfo->turnaround days"));

                //then see if we deliver to that location on that day
                //load delivery dates into an array
                $getLocationDeliveryDays = "SELECT * FROM location
                                            join location_serviceDay on location_id = locationID
                                            where locationID = ".$locker[0]->location_id."
                                            and location_serviceType_id = 1";
                $locationDays = $this->db->query($getLocationDeliveryDays)->result();
                $deliveryDays = array();
                foreach ($locationDays as $loc) {
                    $deliveryDays[] = $loc->day;
                }

                //if not, keep going forward until we find what day we service this location
                $foundDueMatch = 0;
                for ($i = 0; $i <= 10; $i ++) {
                    $newDueDate = date("Y-m-d", strtotime("$dueDate + $i days"));
                    if (in_array(date("N", strtotime($newDueDate)), $deliveryDays)) {
                        $foundDueMatch = 1;
                        //make sure this day is not a holiday
                        $findHoliday = "select * from holiday
                                        where date = '".date("Y-m-d", strtotime($newDueDate))."'
                                        and business_id = $business_id
                                        and delivery = 0";
                        $holiday = $this->db->query($findHoliday)->result();
                        if (!empty($holiday)) {
                            $foundDueMatch = 0;
                        }
                    }
                    if ($foundDueMatch ==1) {
                        $this->dueDate = $newDueDate;
                        break;
                    }
                }
            }
            
            if ($options['source']) {
                $this->source = $options['source'];
            }
            
            $customers = $this->customer_model->get();
                
            if (isset($customers)) {
                if (count($customers) > 0) {
                    $customer = $customers[0];
                    $this->postAssembly = ($customer->internalNotes=="ON PAYMENT HOLD")?$customer->internalNotes."\n".$customer->permAssemblyNotes:$customer->permAssemblyNotes;
                    $totalOrder = $this->customer_model->get_customer_order_count($customer->customerID);
                    if ($totalOrder == 0) {
                        $this->postAssembly .= "ATTENTION THIS IS A NEW CUSTOMER\n";
                    }

                    $ada_deliver_to_lower_locker = get_customer_meta($customer->customerID, "ada_deliver_to_lower_locker", 0);
                    if (!empty($ada_deliver_to_lower_locker)) {
                        $this->alertNotes = "Deliver to Lower Locker";
                    }
                }
            } else {
                $this->postAssembly = '';
                $this->notes = $notes;
            }
            
            if ($notes == 'Authorizing Card') {
                $this->notes = $notes;
            } else {
                $this->notes = $this->setOrderNotes($claim, $customer, $notes);
            }

            $this->llnotes = isset($options["llnotes"])?$options["llnotes"]:"";

            //insert and return the new order ID
            $order_id = $this->insert();

            //set the orderStatus and paymentStatus if we have an order_id
            if ($order_id) {
                if ($recentOrderFound) {
                    $message = "
                    while creating an order we did not have a customer, so we did one final search in process lockers that were in
                    pickup status and had been placed recently. This is mainly for a customer that places 2 bags in a locker. The first bag created and order,
                    but then then second bag gets misplaced because the claim is deactivated. We found a recent order and a customer so we are assuming that the customer is the same for this order.
                    <br>
                    CustomerID: [$customer_id]<br>
                    OrderID: [$order_id]";
                    send_admin_notification('Check Customer [' . $customer_id . '] with order [' . $order_id . ']', $message, $business_id);
                }

                //order status
                $this->orderstatus_model->order_id = $order_id;
                $this->orderstatus_model->orderStatusOption_id = $orderStatusOption_id;
                $this->orderstatus_model->visible = 1;
                $this->orderstatus_model->date = $dateCreated;
                $this->orderstatus_model->employee_id = $employee_id;
                $this->orderstatus_model->locker_id = $locker_id;
                $this->orderstatus_model->note = $notes;
                $this->orderstatus_model->insert();

                //payment status
                $this->orderpaymentstatus_model->order_id = $order_id;
                $this->orderpaymentstatus_model->orderPaymentStatusOption_id = $orderPaymentStatusOption_id;
                //$this->orderpaymentstatus_model->dateCreated =  dateString_to_gmt(date("Y-m-d H:i:s"));
                $this->orderpaymentstatus_model->insert();

                //if we never found a due date, throw an exception
                if ($foundDueMatch == 0) {
                    send_admin_notification("Due Date Could not be set [".$order_id."]", "When creating this order [<a href='http://".$_SERVER['HTTP_HOST']."/admin/orders/details/".$order_id."'>".$order_id."</a>], the location does not have any days of service setup so no due date was set.  Please make sure to add <a href='http://".$_SERVER['HTTP_HOST']."/admin/admin/view_all_locations'>service days</a> for this location", $business_id);
                }
            } else {
                throw new Exception("Missing order_id after new order created");
            }

            // if we have a claim
            if (!empty($claim->claimID)) {

                // check for home pickups
                $homePickup = OrderHomePickup::search(array(
                    'claim_id' => $claim->claimID,
                    'pickup_response IS NOT NULL' => null,
                ));

                // if home pickup found, assign to this order
                if ($homePickup) {
                    $homePickup->order_id = $order_id;
                    $homePickup->save();

                    if ($homePickup->fee > 0) {
                        if (!isset($options['charges'])) {
                            $options['charges'] = array();
                        }

                        $options['charges'][] = array(
                            'chargeType' => 'Home delivery fee',
                            'chargeAmount' => $homePickup->fee,
                        );
                    }
                }
            }

            //clear out the options for order so we can perform new queries
            $this->clear();

            //TODO remove this and put in an update function
            $orderStatusOption_id = '';
            if (isset($options['items'])) {
                $this->load->model('orderItem_model');
                foreach ($items as $itemArr) {
                    $this->orderItem_model->order_id = $order_id;
                    $this->orderItem_model->washLocation_id = $itemArr['washLocation_id'];
                    $this->orderItem_model->product_id = $itemArr['product_id'];
                    $this->orderItem_model->qty = OrderItem_Model::getRoundedQty($business_id, $itemArr['qty']);
                    $this->orderItem_model->notes = $itemArr['notes'];
                    $this->orderItem_model->insert();
                }
                $orderStatusOption_id = 3;

                //if any items where added, update the order status
                if ($orderStatusOption_id) {
                    $o = array();
                    $where = array();
                    $o['orderStatusOption_id'] = $orderStatusOption_id;
                    $where = array('OrderID' => $order_id);
                    $this->update($o, $where);

                    //insert another orderStatus for this order
                    $this->orderstatus_model->clear();
                    $this->orderstatus_model->order_id = $order_id;
                    $this->orderstatus_model->orderStatusOption_id = $orderStatusOption_id;
                    //$this->orderstatus_model->date = dateString_to_gmt(date('Y-m-d H:i:s'));
                    //TODO: why is employee_id here?
                    //$this->orderstatus_model->employee_id = $employee_id;
                    $this->orderstatus_model->locker_id = $locker_id;
                    $this->orderstatus_model->insert();
                }
            }
            if (isset($options['charges'])) {
                $this->load->model('ordercharge_model');
                foreach ($options['charges'] as $chargeArr) {
                    $this->ordercharge_model->save(array(
                        'order_id' => $order_id,
                        'chargeType' => $chargeArr['chargeType'],
                        'chargeAmount' => $chargeArr['chargeAmount'],
                    ));
                }
            }

            //If this is an electronic locker, move the order to the inProcess Locker
            if (Locker::isElectronicLock($locker[0]->lockerLockType_id)) { // 2 = Electronic lock type
                // a) find InProcess locker in currect location from dropLocationLocker table
                // if the location ID is passed, use that to find the inProcess locker
                if ($locker[0]->location_id != '') {
                    //lockerStyle_id 7 = 'inProcess'
                    $process_locker = $this->locker_model->get(array('location_id' => $locker[0]->location_id, 'lockerName' => 'InProcess'));
                }

                if (!$process_locker) {
                    throw new \Exception("No inProcess locker could be found for locationID '{$locker[0]->location_id}']");
                } else {
                    //reassign this order to the new locker  $inProcessLocker->id;
                    //if a customer goes and puts in another order after the blank customer (bc the locker phyically is empty) then goes online to claim it, it will auto assign the previous order does that make sense?
                    $this->clear();
                    $o = array();
                    $where = array();
                    $o['locker_id'] = $process_locker[0]->lockerID;
                    $where = array('orderID' => $order_id);
                    $this->update($o, $where);
                }
            }

            /**
             * The following if statement handles new bags for existing customers
             */
            if ($locker) {
                //get the order
                $options = array();
                $options['orderID'] = $order_id;
                $options['business_id'] = $business_id;
                $order = $this->get($options);

                /*
                 * see if there is an unassigned order in this locker. If there is and it was placed within
                 * the last couple hours, we add the current customer_id to it.
                 *
                 * This handles when a customer has a temp bag and an existing bag. The customer
                 * places both bags into the same locker. The driver makes a temp bag and enters it first.
                 * Then the driver enters the second bag found in the locker.
                 */
                if (Locker::isElectronicLock($locker[0]->lockerLockType_id)) { // 2 = Electronic
                    $this->orderstatus_model->clear();
                    //if this is an electronic locker, we need to make sure the last locker it was in matches
                    $sql = "SELECT * FROM orderStatus
                            JOIN orders ON orderID = order_id
                            WHERE orderStatus.locker_id = {$locker[0]->lockerID}
                            AND orders.orderStatusOption_id <> 10
                            AND (orders.customer_id is null OR orders.customer_id = 0)";
                    $search_orders = $this->db->query($sql)->result();
                } else {
                    //see if there is an unassigned order in this locker.
                    $this->clear();
                    $sql = "SELECT * FROM orders
                            WHERE locker_id = {$locker[0]->lockerID}
                            AND orderStatusOption_id <> 10
                            AND (customer_id = 0 OR customer_id is null)";
                    $search_orders = $this->db->query($sql)->result();
                }

                if ($search_orders) {
                    foreach ($search_orders as $search_order) {
                        //make sure it's not this order
                        if ($customer_id) {
                            //get location
                            $sql = "SELECT address FROM locker
                                    INNER JOIN location ON locationID = location_id
                                    WHERE lockerID = '{$locker[0]->lockerID}'";
                            $searchLocation = $this->db->query($sql)->row();

                            if (date("m/d/y", strtotime($search_order->dateCreated)) == date("m/d/y", strtotime($order[0]->dateCreated)) && $search_order->orderStatusOption_id == 1 && strtolower($locker[0]->lockerName) <> 'N/A') {
                                send_admin_notification('Order Automatically Reassigned - Informational', 'Order: <a href="/admin/orders/details/' . $search_order->orderID . '">' . $search_order->orderID . '</a> from: [' . $search_order->dateCreated . '] in locker [' . $locker[0]->lockerName . '] at [' . $searchLocation->address . '] is not assigned to customer.  <br><br>An order in this same locker was just picked up for customer: [<a href="http://'.$_SERVER['HTTP_HOST'].'/admin/customers/detail/' . $order[0]->customer_id . '">' . $order[0]->customer_id . '</a>], and the dates match so it was automatically reassigned.<br><br><br>', $business_id);

                                $sql = "update orders
                                        set customer_id = $customer_id,
                                        notes = '$search_order->notes',
                                        llNotes = '$search_order->llNotes',
                                        postAssembly = '$search_order->postAssembly',
                                        alertNotes = '$search_order->alertNotes' 
                                        where orderID = $search_order->orderID";

                                $this->db->query($sql);
                                $this->bag_model->clear();
                                $this->bag_model->customer_id = $order[0]->customer_id;
                                $this->bag_model->where = array('bagID' => $bag['bagID']);
                                $this->bag_model->update();
                            } else {
                                send_admin_notification('Issue with unassigned order', 'Order: <a href="https://'.$_SERVER['HTTP_HOST'].'/admin/orders/details/' . $search_order->orderID . '">' . $search_order->orderID . '</a> from: [' . $search_order->dateCreated . '] in locker [' . $locker[0]->lockerName . '] at [' . $searchLocation->address . '] is not assigned to customer.  <br><br>Order <a href="https://'.$_SERVER['HTTP_HOST'].'/admin/orders/details/' . $order[0]->orderID . '">' . $order[0]->orderID . '</a> in this same locker was just picked up for customer: [<a href="https://'.$_SERVER['HTTP_HOST'].'/admin/customers/detail/' . $order[0]->customer_id . '">' . $order[0]->customer_id . '</a>].  It is most likely theirs, but the system could not automatically reassign it and the order needs further investigation.', $business_id);
                            }
                        }
                    }
                }

                /*
                 * If there is no claim, but we know the customer,
                 * check to see if there is a claim made by this customer within the last hour.
                 * If there is, copy the notes to this new order
                 */
                $order = new Order($order_id);
                if (!empty($order->customer_id) && !$claim) {
                    $sql = "SELECT * FROM claim WHERE customer_id = {$order->customer_id}
                            AND updated > DATE_ADD(NOW(), INTERVAL -1 HOUR)
                            AND business_id = {$order->business_id}
                            AND active = 0;";
                    $claims = $this->db->query($sql)->result();
                    if (sizeof($claims) > 1) {
                        $message = "A customer has placed an order [{$order->orderID}] and has multiple bags. When trying to find a
                                recent claim so we found more then one claim within the hour. Please make sure that all claim
                                notes are passed to all orders that were created for this customer [{$order->customer_id}]";
                        send_admin_notification("Multiple claims for a customer", $message);
                    } elseif (sizeof($claims) == 1) {
                        $order->notes = $claims[0]->notes;
                        if (!is_numeric($order->locker_id)) {   //this threw an exception - so we'll check to see if its empty, set to zero if it is.
                            $order->locker_id = 0;
                        }

                        $order->save();
                    }
                }
            }

            if (!empty($bag)) {
                //send out a 'picked up' email to customer
                //don't send this out on special orders
                if ($order->llNotes != 'Special Order') {
                    $action_array = array(
                        'do_action' => 'picked_up',
                        'customer_id' => $customer_id,
                        'business_id' => $business_id,
                        'order_id' => $order_id
                    );
                    Email_Actions::send_transaction_emails($action_array);
                }
            }

            //if this customers has a Deliver To Address, then reassign all orders to that location.
            $getDeliverTo = "select lockerID from customer
                            join location on locationID = deliverTo_location_id
                            join locker on locker.location_id = locationID
                            where customerID = $customer_id
                            and lockerStatus_id = 1
                            order by lockerName desc";
            $deliverTo = $this->db->query($getDeliverTo)->row();
            if (!empty($deliverTo)) {
                //reassign the order to it.
                $this->clear();
                $o = array();
                $where = array();
                $o['locker_id'] = $deliverTo->lockerID;
                $where = array('orderID' => $order_id);
                $this->update($o, $where);

                $this->logger->set('orderStatus', array('order_id' => $order_id, 'locker_id' => $deliverTo->lockerID, 'orderStatusOption_id' => 1, 'note' => "Reassigned to Default Delivery Address", 'visible' => 1));
            }
            
            
            // Adding a check to ensure customerNotes are added to orderNote.
            $order = new Order($order_id);
            $note_sql = "select customerNotes from customer where customerid = $order->customer_id";
            
            $note_query = $this->db->query($note_sql);
            $note_row = $note_query->row();
            
            $found = strpos($order->notes, $note_row->customerNotes);
            if (!empty($note_row->customerNotes) && $found === false) {
                $order->notes .= "\n--".$this->getEveryOrderNote($order->customerID, $note_row->customerNotes);
                $order->save();
            }

            return $order_id;
        }
    }


    /**
     * Updates the order notes. This is called after an order is made because
     * that gives us away to get the driver notes
     *
     * Combines the claim notes, driver notes, and customer notes
     * updates the order
     *
     * @param object $claim
     * @param object $customer
     * @param string notes
     * @return string
     */
    public function setOrderNotes($claim, $customer, $notes)
    {
        // get the claim notes
        if (!empty($claim)) {
            $notes .= "\n".$claim->notes;
        }

        // get the customer notes
        if ($customer) {
            if ($customer->customerNotes) {
                $notes .= "\n".$this->getEveryOrderNote($customer->customerID, $customer->customerNotes);
            }
        }

        //@todo - add the code to add customer starch prefs to the order notes here
        return trim($notes);
    }
    
    public function getEveryOrderNote($customerID, $customerNotes)
    {
        return get_translation("every_order", "customer_notes", array(), "EVERY ORDER: ", $customerID) . $customerNotes;
    }

    /**
     * ----------------------------------------------
     * BEGIN
     * ----------------------------------------------
     *
     * Pulling the code over from order.php::update
     *
     * $options values
     * ---------------------
     * $order = object, if not passed will use order_id to get the order
     * order_id
     * orderStatusOption_id
     * locker_id
     * orderPaymentStatusOption_id
     * employee_id - If not provided, we try to use the current user session
     *
     * ----------------------------------------------
     * END
     * ----------------------------------------------
     */
    public function update_order($options = array())
    {
        $options_params = $options;
        
        if (!empty($options['order']->orderID)) {
            $order = new Order($options['order']->orderID);
        } elseif (!empty($options['order_id'])) {
            $order = new Order($options['order_id']);
        }

        $update = false;
        $date = convert_to_gmt_aprax();
        $this->load->model('order_model');
        $this->load->model('customer_model');
        $this->load->model('locker_model');
        $this->load->model('orderitem_model');
        $new_orderPaymentStatusOption_id = isset($options['orderPaymentStatusOption_id']) ? $options['orderPaymentStatusOption_id'] : null;
        $status_changed = false;

        $employee_id = (isset($options['employee_id'])) ? $options['employee_id'] : get_current_employee_id();

        if (!isset($options['order'])) {
            if (!$options['order_id']) {
                throw new Exception('Missing order or orderID');
            }

            //$business_id = (isset($options['business_id'])) ? $options['business_id'] : $order->business_id ;
            $order = $this->get(array('orderID' => $options['order_id']));
            $order = $order[0];
        } else {
            $order = $options['order'];
        }

        if (!$order) {
            throw new Exception("Missing order.");
        }

        $business_id = $order->business_id;

        $this->order = $order;
        $this->order_id = $order->orderID;

        //used for all the updates
        $where['orderID'] = $order->orderID;

        //$oso_id = $options['orderStatusOption_id'];

        $oso_id = isset($options['orderStatusOption_id']) ? $options['orderStatusOption_id'] : $order->orderStatusOption_id;
        $locker_id = isset($options['locker_id']) ? $options['locker_id'] : @$order->locker_id;

        //get the customer
        
        $this->customer_model->customerID = $order->customer_id;
        $this->customer_model->business_id = $order->business_id;
        $customer = $this->customer_model->get();
        $customer = $customer[0];
        $this->customer = $customer;

        // Make sure that the customer ID is set.
        // Added becuase when 2 orders that come into status 7, the second one does not have a customer object. This causes a sql error
        if (!$this->customer) {
            $sql = "SELECT * FROM customer INNER JOIN orders o ON o.customer_id = customerID WHERE orderID = {$this->order_id}";
            $query = $this->db->query($sql);
            $this->customer = $query->row();
        }

        if (($order->orderStatusOption_id != $oso_id) && $oso_id != '') {
            $status_changed = true;
            $this->load->model('orderstatus_model');
            $this->orderstatus_model->order_id = $order->orderID;
            $this->orderstatus_model->orderStatusOption_id = $oso_id;
            $this->orderstatus_model->locker_id = $locker_id;
            $this->orderstatus_model->employee_id = $employee_id;
            $this->orderstatus_model->date = $date;


            switch ($oso_id) {
                case 1: //This is the 'Picked Up' status.
                    $action_array = array(
                        'do_action' => 'picked_up',
                        'customer_id' => $this->customer->customerID,
                        'business_id' => $business_id,
                        'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 2: //This is the 'waiting for service' status.
                    $action_array = array(
                    'do_action' => 'waiting_for_service',
                    'customer_id' => $this->customer->customerID,
                    'business_id' => $business_id,
                    'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 3: //This is the 'Inventoried' Status.

                    if ($customer->inactive == 1) {
                        queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($order->business_id, 'customerSupport'), "Order for inactive customer", 'Order ' . $order->orderID . ' was inventoried for an inactive customer`s account.  Please investigate ASAP.', array(), null, $order->business_id);

                        return array(
                            'status' => 'error',
                            "message" => 'Order ' . $order->orderID . ' is for an inactive customer.  Something is wrong!'
                        );
                    }


                    //get the locker type
                    $locker = new Locker($locker_id, false);
                    if (Locker::isElectronicLock($locker->lockerLockType_id)) {
                        //move the locker to the process locker for this location
                        // if the location ID is passed, use that to find the inProcess locker
                        if ($locker->location_id != '') {
                            //lockerStyle_id 7 = 'inProcess'
                            $process_locker = $this->locker_model->get_one(array(
                                'location_id' => $locker->location_id,
                                'lockerName' => 'InProcess'
                            ));
                        }

                        if (!$process_locker) {
                            queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($order->business_id, 'customerSupport'), "No inProcess locker could be found", "No inProcess locker could be found in location: " . $locker->location_id, array(), null, $order->business_id);

                            return array(
                                'status' => 'error',
                                "message" => "No inProcess locker could be found, Please create one"
                            );
                        }

                        //make sure the order is not already in the process locker
                        if ($process_locker->lockerID != $order->locker_id) {
                            //reassign this order to the new locker  $inProcessLocker->id;
                            //if a customer goes and puts in another order after the blank customer (bc the locker phyically is empty) then goes online to claim it, it will auto assign the previous order does that make sense?
                            $o['locker_id'] = $process_locker->lockerID;
                            //update the order status

                            $locker_id = $process_locker->lockerID;
                        }
                    } //End of locker type = electronic


                    $action = 'order_inventoried';

                    // POS order
                    if(Order::isPosOrder($order)) {
                        $action = 'pos_order_invoiced';
                    }
                        
                    $location = new Location($locker->location_id);

                    // if this is a deliv location, use the home delivery email
                    //TODO: the other delivery service types should get a custom email too
                    if (in_array($location->serviceType, Location::getHomeDeliveryServiceTypes())) {
                        $action = 'order_inventoried_home_delivery';
                    }

                    // If this is a delivery route, make sure there is an orderHomeDelivery object created
                    if ($location->serviceType == Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY) {
                        $homeDelivery = $this->setupOrderHomeDeliveryFromSubscription($order, $locker->location_id);
                        // TODO: maybe send a customised email ?
                    }

                    //Automated Conveyor
                    if ($location->locationType_id == 11) {
                        $action = 'order_inventoried_automated_conveyor';
                    }

                    $action_array = array(
                        'do_action' => $action,
                        'customer_id' => $this->customer->customerID,
                        'business_id' => $business_id,
                        'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);

                    //update the order type
                    $updateOrder = "update orders set orderType = '".$this->getOrderType($order->orderID)."' where orderID = $order->orderID";
                    $q = $this->db->query($updateOrder);

                    break;

                case 4: //This is the 'Schedule Delivery' status.
                    $update['locker_id'] = $options['locker_id'];
                    $this->update($update, $where);

                    $action_array = array(
                        'do_action' => 'schedule_delivery',
                        'customer_id' => $this->customer->customerID,
                        'business_id' => $business_id,
                        'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 5: //This is the 'washing order' status.
                    $action_array = array(
                    'do_action' => 'washing_order',
                    'customer_id' => $this->customer->customerID,
                    'business_id' => $business_id,
                    'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 6: //This is the 'Washed' status.
                    if ($order->orderPaymentStatusOption_id == 1) {
                        if ($customer) {
                            if ($customer->autoPay) {
                                $sql = "SELECT * from creditCardTransaction cct
                                INNER JOIN orders o ON orderID = cct.order_id
                                INNER JOIN customer c ON customerID = o.customer_id
                                WHERE cct.order_id = {$order->orderID}";

                                $q = $this->db->query($sql);
                                $results = $q->result();
                                if ($results) {
                                    //$cc->fetch();
                                    //TODO: use auth to do sale (not auth)
                                } else {
                                    //no auto pay
                                }
                            } else {
                                //change to On Payment Hold
                                $update['orderStatusOption_id'] = $oso_id = 7;
                                $this->update($update, $where);
                            }
                        } else {
                            //change to On Payment Hold
                            $update['orderStatusOption_id'] = $oso_id = 7;
                            $this->update($update, $where);
                        }
                    }

                    $action_array = array(
                        'do_action' => 'washed_order',
                        'customer_id' => $this->customer->customerID,
                        'business_id' => $business_id,
                        'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 7: //This is the 'On Payment Hold' status.
                    // Make sure that the customer ID is set.
                    // Added becuase when 2 orders that come into status 7, the second one does not have a customer object. This causes a sql error
                    if (!$this->customer) {
                        $sql = "SELECT * FROM customer INNER JOIN orders o ON o.customer_id = customerID WHERE orderID = {$this->order_id}";
                        $query = $this->db->query($sql);
                        $this->customer = $query->row();
                    }

                    if (!$this->customer->customer_deliver_on_hold) {
                        // if the customer has an internal note, move it to customer history
                        if ($this->customer->internalNotes != '') {
                            $this->load->model('customerhistory_model');
                            $this->customerhistory_model->insert($this->customer->internalNotes, $this->customer->customerID, "Internal Note", $business_id, $employee_id);
                        }

                        $this->load->model('customer_model');
                        $this->customer_model->hold = 1;
                        $this->customer_model->internalNotes = "ON PAYMENT HOLD";
                        $this->customer_model->where = array('customerID' => $this->customer->customerID);
                        $this->customer_model->update();

                        $action_array = array(
                            'do_action' => 'payment_hold',
                            'customer_id' => $this->customer->customerID,
                            'business_id' => $business_id,
                            'order_id' => $order->orderID
                        );
                        Email_Actions::send_transaction_emails($action_array);
                    }

                    break;

                case 8: //This is the 'out for delivery' status.
                    // Get last used location for order
                    $order_id = $order->orderID;

                    // If previous status was Scheduled Delivery find last location
                    if ($order->orderStatusOption_id == 4) {
                        $lastLocation = $this->getOrderLastLocation($order_id);
                        $locker_id = $lastLocation['lockerID'];

                        // If location type is locker get the inProcess locker for location
                        if (Locker::isElectronicLock($lastLocation['locationType_id'])) {
                            $location = new Location($lastLocation['locationID']);
                            $locker = $location->getInProcessLocker($lastLocation['locationID']);
                            $locker_id = $locker->lockerID;
                        }

                        if (!empty($locker_id)) {
                            $update['locker_id'] = $locker_id;
                            $this->update($update, $where);
                        }
                    }

                    $action_array = array(
                    'do_action' => 'out_for_delivery',
                    'customer_id' => $this->customer->customerID,
                    'business_id' => $business_id,
                    'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 9: //THis is the 'Ready for Customer Pickup' status.

                    if ($oso_id == 10) {
                        try {
                            throw new Exception("tried to set Order[".$order->orderID."] to status 9 when the order was already in status 10");
                        } catch (Exception $e) {
                            send_exception_report($e);
                        }
                    } else {
                        $order_id = $order->orderID;
                        $customer_id = $this->customer->customerID;
                        $isZeroCharge = $this->isZeroCharge($order_id);
                        $customerIsOnPaymentHold = $this->customer_model->isOnPaymentHold($customer_id);
                        //client has "on hold" pending orders => is on payment hold
                        //order was not on payment hold (7)
                        if (($customerIsOnPaymentHold and $order->orderStatusOption_id != 7)) {

                            //mark as unpaid so we don't deliver free orders until all non free are paid
                            if ($isZeroCharge) {
                                //uncapture does not modify orderStatusOption_id, so it won't enter again into this switch
                                $this->uncapture($order_id);
                                $order = $this->get_by_primary_key($order_id);
                            }

                            //if order is unpaid (3)
                            if ($order->orderPaymentStatusOption_id != 3) {
                                return $this->putOnPaymentHold($order, $employee_id);
                            }
                        }

                        //This happen when order is an split order and status change from
                        //payment hold to ready for customer pick up
                        //$order->split_from has been implemented since 2016
                        //$isZeroCharge try to detect < 2016 split orders
                        if ($customerIsOnPaymentHold && ($isZeroCharge || !empty($order->split_from))) {
                            if ($oso_id == 9) {
                                $action_array = array(
                                    'do_action' => 'payment_hold',
                                    'customer_id' => $order->customer_id,
                                    'business_id' => $order->business_id,
                                    'order_id' => $order->orderID
                                );
                                Email_Actions::send_transaction_emails($action_array);
                                return true;
                            }
                        }

                        $action = 'ready_for_pickup';

                        $locker = new Locker($locker_id, false);
                        $location = new Location($locker->location_id);

                        // POS order
                        if(Order::isPosOrder($order)) {
                            $action = 'pos_order_conveyored';
                        }
                        
                        // if this is a deliv location, use the home delivery email
                        // TODO: the other home delivery service types should get a custom email
                        if (in_array($location->serviceType, Location::getHomeDeliveryServiceTypes())) {
                            $action = 'ready_for_home_delivery';
                        }

                        //Automated Conveyor
                        if ($location->locationType_id == 11) {
                            $action = 'ready_for_pickup_automated_conveyor';
                        }
                        
                        $action_array = array(
                            'do_action' => $action,
                            'customer_id' => $this->customer->customerID,
                            'business_id' => $business_id,
                            'order_id' => $order->orderID,
                            'lockerListDescription' => $options['lockerListDescription'],
                        );
                        Email_Actions::send_transaction_emails($action_array);

                        // We need to update the order status and log it in the orderStatus history
                        $this->order_model->update(array('orderStatusOption_id' => $oso_id), array('orderID' => $order->orderID));
                        $this->logger->set('orderStatus', array('order_id' => $order->orderID, 'locker_id' => $locker_id, 'employee_id' => $employee_id, 'orderStatusOption_id' => $oso_id, 'note' => "Changed Order Status", 'visible' => 1));

                        // test if there has been a delivery to this locker within the last hour
                        // if so, email supprt
                        $order = new Order($order->orderID);
                        $order->checkRecentDeliveryToSameLockerDifferentCustomer();

                        if (!$options_params['not_complete']) {
                            
                            // Call this function again, to complete the order
                            $options = array();
                            $options['order_id'] = $order->orderID;
                            $options['orderStatusOption_id'] = 10;
                            $options['business_id'] = $order->business_id;
                            $options['locker_id'] = $order->locker_id;
                            $options['employee_id'] = $employee_id;
                            $response = $this->update_order($options);
                        } else {
                            $response = array('status' => 'success', "message" => "Status has been updated. NOT marked as complete");
                        }
 
                        return $response;
                    }

                    break;

                case 10: //THis is the 'Completed' status.
                    //update the table with the order amount to help with reporting.
                    //$order_id


                    $options = array();
                    $closedGross = $this->orderitem_model->get_item_total($order->orderID);
                    $options['closedGross'] = ($closedGross['subtotal'])?$closedGross['subtotal']:0;
                    //incase it's trying to save euros or other comma formatted money
                    $options['closedGross'] = str_replace(',', '.', $options['closedGross']);
                    $closedDiscounts = $this->orderitem_model->get_discounts($order->orderID);
                    $options['closedDisc'] = ($closedDiscounts['total']) ? str_replace(',', '.', $closedDiscounts['total']) : 0;
                    $options['orderStatusOption_id'] = $oso_id;
                    $options['tax'] = str_replace(',', '.', $this->get_tax_total($order->orderID));
                    $this->order_model->update($options, $where);

                    //if there is a Employer Match discount in this order, create a new completed order based on that match.
                    $select2 = "select firstName, lastName, orderCharge.chargeType,locker.location_id, lockerID as lockerId, orderCharge.order_id, sum(orderCharge.chargeAmount) as chargeAmount
                    from orderCharge
                    join orders on orderID = orderCharge.order_id
                    join locker on lockerID = orders.locker_id
                    join customer on customerID = orders.customer_id
                    where orderCharge.order_id = {$order->orderID}
                    and orderCharge.chargeType like '%match[%'
                    group by orderCharge.order_id";
                    $employeMatchRow = $this->db->query($select2)->row();

                    if ($employeMatchRow) {
                        //create an order for the credit that this customer received
                        preg_match("/\[(.*)\]/", $employeMatchRow->chargeType, $matches);
                        $custId = $matches[1];

                        $matchInfo = get_translation("employer_match", "order_notes", array(
                            'firstName' => $employeMatchRow->firstName,
                            'lastName' => $employeMatchRow->lastName,
                            'order_id' => $employeMatchRow->order_id,
                        ), "Employer match for %firstName% %lastName% order: %order_id%");

                        $chargeArr = array('chargeType' => $matchInfo, 'chargeAmount' => number_format($employeMatchRow->chargeAmount * -1, 2, '.', ''));
                        //new_order($location_id, $locker_id, $bagNumber, $employee_id, $notes, $business_id, $options){
                        /* Options Values
                         * --------------------------
                         * array claim
                         * array items
                         * int orderStatusOption_id default = 1
                         * int paymentStatusOption_id default = 1
                         * array charges
                         * datetime deltime
                         * int claimID
                         */
                        $options = array();
                        $options['charges'][0] = $chargeArr;
                        $options['orderStatusOption_id'] = 10;
                        $options['customer_id'] = $custId;
                        $new_order_id = $this->order_model->new_order($employeMatchRow->location_id, $employeMatchRow->lockerId, 0, $employee_id, $matchInfo, $order->business_id, $options);
                        $this->updateClosedAmounts($new_order_id);
                        
                        queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($business_id, "customerSupport"), "Employer Match Order", 'Employer match order: ' . $order->orderID . ' created!', array(), null, $order->business_id);
                    }


                    // check to see if the customer has violated locker loot terms
                    $customerLockerLootViolation = (get_customer_meta($customer->customerID, 'rewardsViolation'))?true:false;
                    if ($customerLockerLootViolation) {
                        break;
                    }

                    // get the business lockerLoot Config
                    $lockerLootConfigs = \App\Libraries\DroplockerObjects\LockerLootConfig::search_aprax(array('business_id'=>$business_id));
                    $lockerLootConfig = $lockerLootConfigs[0];

                    if ($lockerLootConfig->lockerLootStatus == "active" && !empty($customer->customerID) && $customer->customerID > 0) {
                        try {
                            $lockerLoot = new LockerLoot();
                            $lockerLoot->setLockerLootConfig($lockerLootConfig, $customer, $order);
                            if (!$error = $lockerLoot->_error_message()) {
                                $lockerLootID = $lockerLoot->applyReward();

                                if ($error = $lockerLoot->_error_message()) {
                                    $body = "applyReward lockerLoot error: ". $error."\n";
                                }
                            } else {
                                $body = "setLockerLootConfig error: ". $error."\n";
                            }
                        } catch (Exception $e) {
                            queueEmail(
                                SYSTEMEMAIL,
                                SYSTEMFROMNAME,
                                DEVEMAIL,
                                "Exception caught while applying lockerloot to order: " . $order->orderID . " order.customerID = " . $order->customer_id,
                                'before exception (' . $e->getMessage() . ')',
                                array(),
                                null,
                                $business_id
                            );
                            $body .= $e->getMessage() .'<br> OrderID: '. $order->orderID . ' <br>Backtrace: <br> <pre>' . print_r(debug_backtrace(), true) . '</pre> <br> . URI: ' . $_SERVER['REQUEST_URI'];
                            queueEmail(
                                SYSTEMEMAIL,
                                SYSTEMFROMNAME,
                                DEVEMAIL,
                                    "Exception caught while applying lockerloot to order: " . $order->orderID,
                                $body,
                                array(),
                                null,
                                $business_id
                            );
                        }
                    } // end of lockerLoot

                    //update the order type
                    $updateOrder = "update orders set orderType = '".$this->getOrderType($order->orderID)."' where orderID = $order->orderID";
                    $q = $this->db->query($updateOrder);

                    $action_array = array(
                        'do_action' => 'completed_order',
                        'customer_id' => $this->customer->customerID,
                        'business_id' => $business_id,
                        'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 11: //THis is the  'Partially Delivered' status.
                    break;
                case 12: //This is the 'out for partial delivery' status.
                    break;

                // delivery failure - unpaid
                case 13:
                    //The following statement sets the order payment status option ID the same so the order status does not get updated if the order is on delivery failure unpaid.
                    $new_orderPaymentStatusOption_id = $order->orderPaymentStatusOption_id;
                    $action_array = array(
                        'do_action' => 'delivery_failure_unpaid',
                        'customer_id' => $this->customer->customerID,
                        'business_id' => $business_id,
                        'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                case 14: //This is the 'loaded on van' status.
                    $action_array = array(
                    'do_action' => 'loaded_on_van',
                    'customer_id' => $this->customer->customerID,
                    'business_id' => $business_id,
                    'order_id' => $order->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    break;

                default:
                    break;
            }

            $this->update(array('orderStatusOption_id' => $oso_id), $where);
            //The following conditional determines whether the status change should be visible to the customer order history.
            // Business rules dictate that any order statuses of 2 (Waiting for Service), 6 (Washed),  13 (Delivery Failure - unpaid), and 14 (Loaded on van) should not be visible to the customer facing order history display.
            if (in_array($oso_id, array(2, 6, 13, 14))) {
                $visible = 0;
            } else {
                $visible = 1;
            }
            $this->logger->set('orderStatus', array('order_id' => $order->orderID, 'locker_id' => $locker_id, 'employee_id' => $employee_id, 'orderStatusOption_id' => $oso_id, 'note' => "Changed Order Status", 'visible' => $visible));
            $response = array('status' => 'success', "message" => "Status has been updated");
        } else {
            $response = array('status' => 'success', "message" => "Status not updated");
        }


        if ($new_orderPaymentStatusOption_id != '' && ($order->orderPaymentStatusOption_id != $new_orderPaymentStatusOption_id)) {
            $this->load->model('orderpaymentstatus_model');
            $this->orderpaymentstatus_model->order_id = $order->orderID;
            $this->orderpaymentstatus_model->orderPaymentStatusOption_id = $new_orderPaymentStatusOption_id;
            $this->orderpaymentstatus_model->dateCreated = $date;
            if ($this->orderpaymentstatus_model->insert()) {
                $options = array('orderPaymentStatusOption_id' => $new_orderPaymentStatusOption_id);
                $this->order_model->update($options, $where);
                //$note = "Payment Captured (DEV)";
                //$this->logger->set('orderStatus', array('order_id'=>$order->orderID,'locker_id'=>$locker_id, 'employee_id'=>$employee_id, 'orderStatusOption_id'=>$oso_id, 'note'=>$note));
                $response = array('status' => 'success', 'message' => 'Payment Status has been updated');
            }
        }

        //$order = new Order($order->orderID);
        return $response;
    }

    public function updateClosedAmounts($order_id)
    {
        $where['orderID'] = $order_id;
        $this->load->model('orderitem_model');

        $options = array();
        $closedGross = $this->orderitem_model->get_item_total($order_id);
        $options['closedGross'] = ($closedGross['subtotal'])?$closedGross['subtotal']:0;
        //incase it's trying to save euros or other comma formatted money
        $options['closedGross'] = str_replace(',', '.', $options['closedGross']);

        $closedDiscounts = $this->orderitem_model->get_discounts($order_id);
        $options['closedDisc'] = ($closedDiscounts['total']) ? str_replace(',', '.', $closedDiscounts['total']) : 0;

        $options['tax'] = str_replace(',', '.', $this->get_tax_total($order_id));

        $this->update($options, $where);
    }
    
    /**
     * Do not use this *, use get_aprax().
     * @deprecated
     * ------
     * Note, this get method overrides the get in MY_Model since this query can be very long
     * @param array $options
     *      Required:
     *          orderID
     *          business_id
     *      Optional
     *          ticketNum
     *          locker_id
     *          business_id
     *          bag_id
     *          exclude_orderStatusOption_id
     *          limit
     *          select
     *          order_by
     * @return array The query result
     */
    public function get($options = array())
    {
        //if we don't have an orderID, we MUST have a business_id
        if (!isset($options['orderID'])) {
            if (!required(array('business_id'), $options)) {
                throw new Exception('Business business_id');
            }
        }

        if (isset($options['orderID'])) {
            if (is_array($options['orderID'])) {
                $this->db->where_in('orderID', $options['orderID']);
            } else {
                $this->db->where('orderID', $options['orderID']);
            }
        }
        if (isset($options['ticketNum'])) {
            $this->db->where('ticketNum', $options['ticketNum']);
        }
        if (isset($options['locker_id'])) {
            $this->db->where('locker_id', $options['locker_id']);
        }
        if (isset($options['customer_id'])) {
            $this->db->where('orders.customer_id', $options['customer_id']);
        }
        if (isset($options['business_id'])) {
            $this->db->where('orders.business_id', $options['business_id']);
        }
        if (isset($options['orderStatusOption_id'])) {
            $this->db->where("orders.orderStatusOption_id", $options['orderStatusOption_id']);
        }

        if (isset($options['bag_id'])) {
            $this->db->where('orders.bag_id', $options['bag_id']);
        }
        if (isset($options['exclude_orderStatusOption_id'])) {
            $this->db->where_not_in('orderStatusOption_id', $options['exclude_orderStatusOption_id']);
        }
        if (isset($options['limit'])) {
            $this->db->limit($options['limit']);
        }
        if (isset($options['select'])) {
            $this->db->select($options['select']);
        }
        if (isset($options['order_by'])) {
            $this->db->order_by($options['order_by'], 'DESC');
        }

        if (isset($options['with_location'])) {
            $this->db->join('locker', 'orders.locker_id = locker.lockerID', "LEFT");
            $this->db->join('location', 'location.locationID = locker.location_id', "LEFT");

            if (empty($options['select'])) {
                $this->db->select('*, orders.dateCreated');
            }
        }

        if (isset($options['with_orderStatus'])) {
            $this->db->join('orderStatusOption', 'orderStatusOption.orderStatusOptionID = orders.orderStatusOption_id');
        }

        if (isset($options['with_orderPaymentStatus'])) {
            $this->db->join('orderPaymentStatusOption', 'orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id');
        }

        if (isset($options['with_customer'])) {
            $this->db->join('customer', 'customerID = orders.customer_id', 'left');
        }

        if (isset($options['with_claim_customer'])) {
            $this->db->join('claim', 'customerID = claim.customer_id AND claim.locker_id = lockerID', 'left');
        }

        if (isset($options['with_bag'])) {
            $this->db->join('bag', 'bagID = orders.bag_id', 'left');
        }
        $query = $this->db->get('orders');

        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        $result = $query->result();

        return $result;
    }


    /**
     * gets the order history for a customer
     *
     * @param int $customer_id
     * @param int $business_id
     * @param int $limit
     * @param bool $unpaid
     * @param bool $includeNoBagOrders If false, only returns orders that have a defined Bag, which is defined as a bag ID other than the value "0". An orde3r with no bag is known as a 'authorization' order
     * @throws Exception
     * @return array of stdClass objects
     */
    public function getCustomerOrderHistory($customer_id, $business_id, $limit = null, $unpaid = false, $includeNoBagOrders = false, $includeHomeDelivery = true)
    {
        if (empty($customer_id)) {
            throw new Exception("Missing customer_id");
        }

        if (empty($business_id)) {
            throw new Exception("Missing business_id");
        }

        $skipUnpaid = $unpaid ? "": "AND orderStatusOption_id != 13";
        $skipNoBag = $includeNoBagOrders ? "" : "AND bag_id != 0";
        $skipHomeDelivery = $includeHomeDelivery ? "" : "AND locationType.Name != 'Home Delivery'";

        $sql = "SELECT orderID, lockerName, orders.locker_id, orders.notes, COALESCE(orderHomeDelivery.address1, location.address) AS address,
                        orderStatusOption.name AS status, orders.dateCreated,
                        orderPaymentStatusOption.name as payment, orderStatusOption_id, bag_id, orderHomeDeliveryID
                        ,statusDate referenceDate, location.serviceType location_serviceType, locationType.name location_locationType
                        ,location.description location_description, locationID
                FROM orders
                INNER JOIN locker ON (lockerID = orders.locker_id)
                LEFT JOIN location ON (locationID = locker.location_id)
                LEFT JOIN locationType ON (location.locationType_id = locationType.locationTypeID)
                LEFT JOIN orderHomeDelivery ON (orderID = orderHomeDelivery.order_id)
                JOIN orderStatusOption ON (orderStatusOption.orderStatusOptionID = orders.orderStatusOption_id)
                JOIN orderPaymentStatusOption ON (orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id)
                WHERE orders.customer_id = ?
                AND locker_id != 0
                AND orders.business_id = ?
                $skipNoBag
                $skipUnpaid
                $skipHomeDelivery
                ORDER BY orders.dateCreated DESC";

        if (!is_null($limit)) {
            if (is_numeric($limit)) {
                $sql .= " LIMIT $limit";
            } else {
                throw new \InvalidArgumentException("'limit' must be numeric");
            }
        }
        $orders = array();
        $result = $this->db->query($sql, array($customer_id, $business_id))->result();

        foreach ($result as $order) {
            $order->orderType = $this->getOrderType($order->orderID);
            $order->orderTotal = $this->get_net_total($order->orderID);
            $orders[] = $order;
        }

        return $orders;
    }

    public function getOrderAndLocation($order_id)
    {
        if (empty($order_id)) {
            throw new Exception("Missing order_id");
        }

        $sql = "SELECT locker_id, lockerName, orders.notes, companyName, orderID, orders.dateCreated, statusDate, bag_id, bagNumber,
                    orders.customer_id, orderStatusOption_id, orders.dueDate, orders.business_id, locker.location_id,
                    COALESCE(orderHomeDelivery.address1, orderHomePickup.address1, location.address) AS address,
                    COALESCE(orderHomeDelivery.address2, orderHomePickup.address2, location.address2) AS address2,
                    COALESCE(orderHomeDelivery.city, orderHomePickup.city, location.city) AS city,
                    COALESCE(orderHomeDelivery.state, orderHomePickup.state, location.state) AS state,
                    COALESCE(orderHomeDelivery.zip, orderHomePickup.zip, location.zipcode) AS zip
                FROM (orders)
                LEFT JOIN locker ON (orders.locker_id = locker.lockerID)
                LEFT JOIN location ON (location.locationID = locker.location_id)
                LEFT JOIN orderHomeDelivery ON (orderID = orderHomeDelivery.order_id)
                LEFT JOIN orderHomePickup ON (orderID = orderHomePickup.order_id)
                LEFT JOIN bag ON (bagID = orders.bag_id)
                WHERE orderID =  ?";

        return $this->db->query($sql, array($order_id))->row();
    }

    /**
     * Retrieves an order from the database based on the supplied criteria. Note that only the first supplied criteria will be searched one based on the parameter order.
     * @param int $bag_id the bag ID
     * @param int $order_id The order ID
     * @param string $ticket_number The ticket number
     * @param string $cust_name The first or last name of the customer
     * @return object The results of the search query
     */
    public function getOrder($bag_id, $order_id, $ticket_number, $customer_name, $dueDate = null, $brand = null)
    {
        $this->db->select("orders.orderID, orders.ticketNum, concat_ws(' ', customer.firstName, customer.lastName ) AS customerName,
                    customer.customerID, bag.bagNumber, bag.notes as bagNotes, orderStatusOption.name AS orderStatus,
                    orderPaymentStatusOption.name as orderPaymentStatus, customer.customerID as cust_id, concat_ws(' / ', concat_ws(' ', location.address, location.address2 ), locker.lockerName ) AS location, orders.dueDate");
        //Note, these conditionals are executed mutually exclusively because it was written this way in the old system.
        if (!empty($bag_id)) {
            $this->db->like("bag.bagNumber", "$bag_id");
        } elseif (!empty($order_id)) {
            $this->db->like("orders.orderID", "$order_id");
        } elseif (!empty($ticket_number)) {
            $this->db->like("orders.ticketNum", "$ticket_number");
        } elseif (!empty($customer_name)) {
            $this->db->like("customer.lastName", $customer_name);
            $this->db->or_like("customer.firstName", $customer_name);
        } elseif (!empty($dueDate)) {
            $this->db->where("orders.dueDate >=", $dueDate);
        } elseif (!empty($brand)) {
            $this->db->like("item.manufacturer", $brand);
        }

        $this->db->join("orderStatusOption", "orderStatusOption.orderStatusOptionID=orders.orderStatusOption_id", "left");

        $this->db->join("orderPaymentStatusOption", "orderPaymentStatusOption.orderPaymentStatusOptionID=orders.orderPaymentStatusOption_id", "left");
        $this->db->join("bag", "bag.bagID=orders.bag_id AND bag.business_id={$this->business_id}", 'LEFT');
        $this->db->join("customer", "customer.customerID=orders.customer_id AND customer.business_id={$this->business_id}", 'LEFT');

        $this->db->join("locker", "orders.locker_id=locker.lockerID", 'LEFT'); //Note, we do a left join here because we want to include orders that don't have an associated locker.
        $this->db->join("location", "locker.location_id=location.locationID AND location.business_id={$this->business_id}", 'LEFT');

        if (!empty($brand)) {
            $this->db->join("orderItem", "orderItem.order_id=orders.orderID");
            $this->db->join("item", "item.itemID=orderItem.item_id");
        }

        $this->db->where('orders.business_id', $this->business_id);
        $this->db->group_by('orders.orderID');
        $this->db->order_by('orderID desc');
        $this->db->limit(100);
        $query = $this->db->get('orders');

        $results = $query->result();

        return $results;
    }

    /**
     * Retrieves the items for a partiular order
     *
     * @param int $order_id
     * @param array $search Array(Array("column_name"=>"search_value"))
     * @return array An array of 'item' objects
     */
    public function getItems($order_id, $search = array())
    {
        $sql = 'SELECT itemID, product_process.product_id, barcode, qty, unitPrice, supplier.supplierBusiness_id, orderItem.notes, orderItemID, product.displayName, productCategory.module_id, taxable, taxGroup_id, manufacturer
            FROM (orderItem)
            LEFT JOIN item ON itemID = item_id
            LEFT JOIN barcode ON barcode.item_id = itemID
            LEFT JOIN product_process ON product_process.product_processID = orderItem.product_process_id
            LEFT JOIN product ON productID = product_process.product_id
            LEFT JOIN productCategory ON productCategoryID = productCategory_id
            LEFT JOIN supplier ON supplier.supplierID = orderItem.supplier_id
            WHERE orderItem.order_id =  ?';

        $conditions = array($order_id);

        if (!empty($search)) {
            foreach($search as $k=>$v) {
                $sql .= " AND {$k} LIKE ?";
                $conditions[] = '%'.$this->db->escape_like_str($v).'%';
            }
        }

        $sql .= '
            GROUP BY orderItemID
            ORDER BY orderItemID, barcodeID';

        $result = $this->db->query($sql, $conditions)->result();

        return $result;
    }

    /**
     * This is *, do not use.
     *
     * ###############################################
     * BEGIN *
     * ###############################################
     *
     * wrapper for orderItems::get_item_total();
     *
     * @param int $order_id
     * @return decimal $total;
     *
     * ###############################################
     * END *
     * ###############################################
     */
    public function getTotal($order_id = '')
    {
        $order_id = ($order_id == '') ? $this->order_id : $order_id;

        $this->load->model('orderItem_model');
        $total = $this->orderItem_model->get_item_total($order_id);
        $total = $total['total'];

        return $total;
    }
    /**
     * Returns the order Charge total for a particular order. Note, this includes both order charges and discounts associated with an order.
     * @param int $order_id
     * @param bool $taxable_only Only get charges with "taxable" = 1
     * @return float
     */
    public function get_orderCharge_total($order_id, $taxable_only = false)
    {
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        }
        
        $where = array("order_id" => $order_id);
        
        if ($taxable_only) {
            $where['taxable'] = 1;
        }
        
        $this->load->model("orderCharge_model");
        $orderCharges = $this->orderCharge_model->get_aprax($where, null, array('SUM(chargeAmount) as total'));
        $orderCharge_total = $orderCharges[0]->total;

        return $orderCharge_total;
    }

    /**
     * Retrieves the total of all discounts and order items in the order, excluding any taxes
     * @param int $order_id
     * return float
     */
    public function get_subtotal($order_id)
    {
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        }

        $order = $this->get_by_primary_key($order_id);
        $breakout_vat = get_business_meta($order->business_id, 'breakout_vat');

        $orderCharge_total = $this->get_orderCharge_total($order_id);
        $gross_total = $this->get_gross_total($order_id);
        $subtotal = $orderCharge_total + $gross_total;

        $taxes = $this->get_stored_taxes($order_id);
        foreach ($taxes as $taxItem) {
            if ($taxItem['vat_breakout']) {
                $subtotal -= $taxItem['amount'];
            }
        }

        return $subtotal;
    }
    /**
     * Retrieves the item total in the order, excluding any discounts and taxes.
     * @param int $order_id
     * @return float
     */
    public function get_gross_total($order_id)
    {
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        }

        $this->load->model("orderitem_model");
        $orderItems = $this->orderitem_model->get_aprax(array("order_id" => $order_id), null, array('SUM(unitPrice * qty) as total'));
        $gross_total = $orderItems[0]->total;

        return $gross_total;
    }

    /**
     * Retrieves the total tax that is applied to the order.
     *
     * This tax total is calculated for all order items and discounts in the order.
     * If the customer associated with the order is not taxable', then the tax total will be 0
     *
     * @param int $order_id
     * @return float
     */
    public function get_tax_total($order_id)
    {
        $taxes = $this->get_stored_taxes($order_id);

        $total = 0;
        foreach ($taxes as $tax) {
            $total += $tax['amount'];
        }

        return $total;
    }

    /**
     * Calculate order tax lines
     * @param int $order_id
     * @return array
     */
    public function get_taxes_subtotals($order_id)
    {
        $order = $this->get_by_primary_key($order_id);

        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($order->customer_id);

        // Skip non-taxable customers
        if (!$customer->taxable) {
            return array();
        }

        $taxable_totals = array();

        $gross_total = $this->get_gross_total($order_id);
        $orderCharge_total_taxable = $this->get_orderCharge_total($order_id, true);
        $subtotal = $gross_total + $orderCharge_total_taxable;// not used, remove??

        // Order has no charges if the total is 0 or the charges are 0
        $order_has_charges = $gross_total * $orderCharge_total_taxable != 0;

        // Iterate through each order item and check to see if it is taxable.
        // If the order item is taxable, then it's price is added into the tax group total
        $orderItems = $this->get_taxable_order_items($order_id);
        foreach ($orderItems as $orderItem) {

            // if the item is not taxable or does not have a tax applied skip it
            if (!$orderItem->taxable || $orderItem->taxGroup_id == 0) {
                continue;
            }

            // intialize the adder for the taxgroup
            if (!isset($taxable_totals[$orderItem->taxGroup_id])) {
                $taxable_totals[$orderItem->taxGroup_id] = 0;
            }

            // Get the item total
            $item_total = $orderItem->unitPrice * $orderItem->qty;

            // If there are charges, need to distribute the taxable total on items to apply the tax proportionally
            if ($order_has_charges) {
                $item_charges = ($item_total / $gross_total) * $orderCharge_total_taxable;
                $item_total = $item_total + $item_charges;
            }

            // store the taxable total
            $taxable_totals[$orderItem->taxGroup_id] += $item_total;
        }

        // order with no items but with charges, mostly Laundry plans
        if (empty($taxable_totals) && $orderCharge_total_taxable > 0) {
            $default_taxGroup_id = get_business_meta($order->business_id, 'default_taxGroup');

            $taxable_totals[$default_taxGroup_id] = $orderCharge_total_taxable;
        }

        $taxes = $this->get_taxes_from_items($order_id, $order->business_id, $taxable_totals);

        return $taxes;
    }

    /**
     * Get every order item paired with its taxGroup_id
     * @param int $order_id
     */
    public function get_taxable_order_items($order_id)
    {
        $sql = 'SELECT orderItemID, unitPrice, qty, taxGroup_id, taxable
            FROM (orderItem)
            JOIN product_process ON product_process.product_processID = orderItem.product_process_id
            JOIN taxGroup ON taxGroup.taxGroupID = product_process.taxGroup_id
            WHERE orderItem.order_id =  ?
            AND taxable = 1';

        return  $this->db->query($sql, array($order_id))->result();
    }

    /**
     * Calculate tax lines for each tax group
     *
     * @param int $order_id
     * @param int $business_id
     * @param array $taxable_totals key is taxGroup_id and value is taxable total
     */
    protected function get_taxes_from_items($order_id, $business_id, $taxable_totals)
    {
        $this->load->model('taxgroup_model');

        // if Breakout VAT is enabled, we have to remove the tax from the items
        $breakout_vat = get_business_meta($business_id, 'breakout_vat');

        // calculate the tax amount foreach tax group
        $taxes = array();
        foreach ($taxable_totals as $taxGroup_id => $item_total) {

            // get the tax rate
            $taxRate = $this->taxgroup_model->get_tax_rate($order_id, $taxGroup_id);

            // calculte the tax
            $item_tax = $item_total * $taxRate;

            // If business breaks out VAT, the tax is included in the price.
            // So we have to calculate the amount from the item total and the tax rate.
            if ($breakout_vat) {
                $item_subtotal = $item_total / (1 + $taxRate);
                $item_tax = $item_total - $item_subtotal;
            }

            // create a tax line for this tax group
            $taxes[] = array(
                'amount'       => round($item_tax, 2),
                'taxRate'      => $taxRate,
                'taxGroup_id'  => $taxGroup_id,
                'breakout_vat' => $breakout_vat,
            );

            // Check to see if there are child taxes associated with this tax group
            $childTaxes = $this->taxgroup_model->get_aprax(array(
                'parent_id'   => $taxGroup_id,
                'business_id' => $business_id
            ));

            // child taxes, are taxed over the original price plus the parent tax
            foreach ($childTaxes as $childTaxGroup) {

                // get the tax rate
                $child_taxRate = $this->taxgroup_model->get_tax_rate($order_id, $childTaxGroup->taxGroupID);

                // If the child is a sub tax, calculate the tax from the item plus the parent tax
                if ($childTaxGroup->is_sub_tax) {
                    // calculte the tax, from the item plus the tax
                    $child_tax = ($item_total + $item_tax) * $child_taxRate;

                    // If business breaks out VAT, the tax is included in the price.
                    // So we have to calculate the amount from the item total plus its tax and the child tax rate.
                    if ($breakout_vat) {
                        $child_subtotal = $item_subtotal / (1 + $child_taxRate);
                        $child_tax = $item_subtotal - $child_subtotal;
                    }
                } else {
                    $child_tax = $item_total * $child_taxRate;

                    if ($breakout_vat) {
                        $child_subtotal = $item_total / (1 + $child_taxRate);
                        $child_tax = $item_total - $child_subtotal;
                    }
                }

                // create a tax line from this child tax group
                $taxes[] = array(
                    'amount'       => round($child_tax, 2),
                    'taxRate'      => $child_taxRate,
                    'taxGroup_id'  => $childTaxGroup->taxGroupID,
                    'breakout_vat' => $breakout_vat,
                );
            }
        }

        return $taxes;
    }

    /**
     * Re-calculates and stores the order's taxes
     * @param int $order_id
     */
    public function update_order_taxes($order_id)
    {
        $order = $this->get_by_primary_key($order_id);
        $breakout_vat = get_business_meta($order->business_id, 'breakout_vat');

        $this->load->model('ordertax_model');

        // delete order taxes
        $this->ordertax_model->delete_aprax(array(
            'order_id' => $order_id,
        ));

        // get the location zipcode
        $sql = "SELECT zipcode FROM orders
            JOIN locker ON (locker_id = lockerID)
            JOIN location ON (location_id = locationID)
            WHERE orderID = ?
            ";
        $row = $this->db->query($sql, array($order_id))->row();
        $zipcode = $row->zipcode;

        // calculate the taxes
        $taxes = $this->get_taxes_subtotals($order_id);

        //save new order taxes
        $total = 0;
        foreach ($taxes as $tax) {
            $this->ordertax_model->save(array(
                'order_id'     => $order_id,
                'taxGroup_id'  => $tax['taxGroup_id'],
                'zipcode'      => $zipcode,
                'amount'       => number_format($tax['amount'], 2, '.', ''),
                'taxRate'      => $tax['taxRate'],
                'vat_breakout' => $tax['breakout_vat'],
            ));

            // get the new total
            $total += $tax['amount'];
        }

        // update order tax
        $this->save(array(
            'orderID' => $order_id,
            'tax' => number_format($total, 2, '.', ''),
        ));
    }

    /**
     * Get the sotred order taxes and their associated tax groups
     * @param int $order_id
     * @return array
     */
    public function get_order_taxes($order_id)
    {
        $sql = "SELECT * FROM orderTax LEFT JOIN taxGroup ON taxGroup_id = taxGroupID WHERE order_id = ? AND business_id = ?";
        $results = $this->db->query($sql, array($order_id, $this->business_id))->result();
        return $results;
    }

    /**
     * Gets the order stored taxes
     *
     * @param int $order_id
     * @return array
     */
    public function get_stored_taxes($order_id)
    {
        $this->load->model('ordertax_model');
        $orderTaxes = $this->ordertax_model->get_aprax(array(
            'order_id' => $order_id,
        ));

        $taxes = array();
        foreach ($orderTaxes as $orderTax) {
            $taxes[] = array(
                'amount'       => round($orderTax->amount, 2),
                'taxRate'      => $orderTax->taxRate,
                'taxGroup_id'  => $orderTax->taxGroup_id,
                'vat_breakout' => $orderTax->vat_breakout,
            );
        }

        return $taxes;
    }

    /**
     * Check if the order has breakout VAT taxes
     *
     * @param int $order_id
     * @return boolean
     */
    public function is_breakout_vat($order_id)
    {
        $taxes = $this->get_stored_taxes($order_id);
        if ($taxes) {
            foreach ($taxes as $taxItem) {
                if ($taxItem['vat_breakout']) {
                    return true;
                }
            }

            return false;
        }

        // fallback to business default
        $order = $this->get_by_primary_key($order_id);
        return get_business_meta($order->business_id, 'breakout_vat');
    }

    /**
     * @deprecated Use get_net_total in order helper
     * Retrieves the total of all discounts, order items, and taxes in the order.
     * @param int $order_id
     * @return float
     */
    public function get_net_total($order_id)
    {
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        }
        $this->load->model('orderItem_model');
        $subtotal = $this->get_subtotal($order_id);
        $orderItem_tax_total = $this->get_tax_total($order_id);

        $net_total = round((float) $subtotal + (float) $orderItem_tax_total, 2);

        return $net_total;
    }

    /**
     * @deprecated Use the Order Droplocker Object for updating order entities
     * Upgrade a laundryplan for a customer
     *
     * Option values
     * ----------------
     * customer_id
     * business_id
     * new_plan_id
     *
     * @param array $options
     */
    public function upgrade_laundry_plan($options = array())
    {
        $customer_id = $options['customer_id'];
        $business_id = $options['business_id'];
        $new_plan_id = $options['new_plan_id'];

        $type = false;
        if (!empty($options["type"])) {
            $type = $options["type"];
        }

        $this->load->model('laundryplan_model');
        if (!$type || $type === 'laundry_plan') {
            $active_plan = $this->laundryplan_model->get_active_laundry_plan($customer_id, $business_id, 'laundry_plan');
        } elseif ($type == "everything_plan") {
            $active_plan = $this->laundryplan_model->get_active_laundry_plan($customer_id, $business_id, 'everything_plan');
        } elseif ($type == "product_plan") {
            $active_plan = $this->laundryplan_model->get_active_laundry_plan($customer_id, $business_id, 'product_plan');
        }

        if (!$active_plan) {
            throw new Exception('Either missing customer plan or the customers current plan is not active');
        }

        // get the new plan
        if (!$type || $type === 'laundry_plan') {
            $this->load->model('businesslaundryplan_model');
            $this->businesslaundryplan_model->businessLaundryPlanID = $new_plan_id;
            $new_plan = $this->businesslaundryplan_model->get();
            if (!$new_plan) {
                throw new Exception('the laundry plan requested for upgrade was not found');
            }

            //turn off renewal on current plan
            $sql = "update laundryPlan set renew = 0 where laundryPlanID = " . $active_plan->laundryPlanID;
            $this->db->query($sql);

            //add laundry plan for customer
            $this->load->model('laundryplan_model');
            $options = array();
            $options['customer_id'] = $customer_id;
            $options['business_id'] = $business_id;
            $options['plan'] = $new_plan[0]->pounds;
            $options['price'] = $new_plan[0]->price;
            $options['days'] = $new_plan[0]->days;
            $options['startDate'] = $active_plan->endDate;
            $options['endDate'] = $active_plan->endDate;
            $options['active'] = 1;
            $options['description'] = $new_plan[0]->description;
            $options['renew'] = 1;
            $options['pounds'] = $new_plan[0]->pounds;
            $options['rollover'] = $new_plan[0]->rollover;

            return $this->laundryplan_model->insert($options);
        } elseif ($type == "everything_plan") {
            $new_plan = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(array(
                    'businessLaundryPlanID' => $options['new_plan_id']
            ));

            if (empty($new_plan)) {
                throw new Exception('the everything plan requested for upgrade was not found');
            }

            //turn off renewal on current plan
            $sql = "update laundryPlan set renew = 0 where laundryPlanID = " . $active_plan->laundryPlanID;
            $this->db->query($sql);

            //add laundry plan for customer
            $everythingPlan = new \App\Libraries\DroplockerObjects\EverythingPlan();
            $everythingPlan->customer_id = $customer_id;
            $everythingPlan->business_id = $business_id;
            $everythingPlan->plan = $new_plan[0]->credit_amount;
            $everythingPlan->description = $new_plan[0]->description;
            $everythingPlan->price = $new_plan[0]->price;
            $everythingPlan->credit_amount = $new_plan[0]->credit_amount;
            $everythingPlan->days = $new_plan[0]->days;
            $everythingPlan->startDate = $active_plan->endDate;
            $everythingPlan->endDate = $new_plan[0]->endDate;
            $everythingPlan->rollover = $new_plan[0]->rollover;
            $everythingPlan->renew = 1;
            $everythingPlan->active = 1;
            $everythingPlan->expired_discount_percent = $new_plan[0]->expired_discount_percent;
            $everythingPlan->type = "everything_plan";
            return $everythingPlan->save();
        } elseif ($type == "product_plan") {
            $new_plan = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(array(
                    'businessLaundryPlanID' => $options['new_plan_id']
            ));

            if (empty($new_plan)) {
                throw new Exception('the product plan requested for upgrade was not found');
            }

            //turn off renewal on current plan
            $sql = "update laundryPlan set renew = 0 where laundryPlanID = " . $active_plan->laundryPlanID;
            $this->db->query($sql);

            //add laundry plan for customer
            $productPlan = new \App\Libraries\DroplockerObjects\ProductPlan();
            $productPlan->customer_id = $customer_id;
            $productPlan->business_id = $business_id;
            $productPlan->plan = $new_plan[0]->credit_amount;
            $productPlan->description = $new_plan[0]->description;
            $productPlan->price = $new_plan[0]->price;
            $productPlan->credit_amount = $new_plan[0]->credit_amount;
            $productPlan->days = $new_plan[0]->days;
            $productPlan->startDate = $active_plan->endDate;
            $productPlan->endDate = $new_plan[0]->endDate;
            $productPlan->rollover = $new_plan[0]->rollover;
            $productPlan->renew = 1;
            $productPlan->active = 1;
            $productPlan->expired_discount_percent = $new_plan[0]->expired_discount_percent;
            $productPlan->type = "product_plan";
            return $productPlan->save();
        }

        return false;
    }

    /**
     * @deprecated Use the Order Droplocker Object for creating and updating Order entities.\
     * renews a customers laundry plan
     * @param stdClass $laundryPlan - customers current laundryplan
     * @return array result
     */
    public function renew_laundry_plan($laundryPlan)
    {
        if (empty($laundryPlan)) {
            throw new \InvalidArgumentException("'laundryPlan' must be a codeigniter query instance of a Laundry Plan entity.");
        } else {
            $customer_id = $laundryPlan->customer_id;
            $business_id = $laundryPlan->business_id;
            $date = date('Y-m-d H:i:s');
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($business_id);
            $locker_id = $business->prepayment_locker_id;
            $bag_id = 0;
            $rollover = 0;


            // create the order with a status of 3 so we do not send out an inventoried email to the customer
            $options['customer_id'] = $customer_id;
            $options['orderStatusOption_id'] = 3;
            $options['paymentStatusOption_id'] = 1;
            $order_id = $this->order_model->new_order(0, $locker_id, $bag_id, 0, "Laundry Plan (" . $laundryPlan->description . ")", $business_id, $options);


            $sql2 = "SELECT sum(amount) AS remaining FROM customerDiscount
                WHERE DESCRIPTION = 'Laundry Plan' AND customer_id = $customer_id  AND active = 1";
            $select2 = $this->db->query($sql2);
            $line2 = $select2->row();

            // check to see if the customer gets any rollover pounds added to the new plan
            $rollover = ($line2->remaining > $laundryPlan->rollover) ? $laundryPlan->rollover : $line2->remaining;

            //remove any remaining credit
            $update1 = "UPDATE customerDiscount SET active = 0, description = 'Laundry Plan - EXPIRED'
                    WHERE description = 'Laundry Plan'
                    AND customer_id = $customer_id
                    AND active = 1";
            $query = $this->db->query($update1);

            //deactivate current plans
            $update1 = "UPDATE laundryPlan SET active = 0, renew = 0 WHERE customer_id = $customer_id";
            $query = $this->db->query($update1);

            $newLbs = $rollover + $laundryPlan->pounds;

            //insert into customer discount
            $this->load->model('customerdiscount_model');
            $endDate = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') + $laundryPlan->days, date('Y')));
            $this->customerdiscount_model->clear();
            $this->customerdiscount_model->customer_id = $customer_id;
            $this->customerdiscount_model->amount = $newLbs;
            $this->customerdiscount_model->amountType = 'pounds';
            $this->customerdiscount_model->frequency = 'until used up';
            $this->customerdiscount_model->description = 'Laundry Plan';
            $this->customerdiscount_model->extendedDesc = $laundryPlan->description . " - " . $laundryPlan->pounds . weight_system_format('', false, true) . "@ " . format_money_symbol($business_id, '%.2n', $laundryPlan->price);
            $this->customerdiscount_model->active = 1;
            $this->customerdiscount_model->order_id = $order_id;
            $this->customerdiscount_model->origCreateDate = $date;
            $this->customerdiscount_model->expireDate = $endDate;
            $this->customerdiscount_model->business_id = $business_id;
            $customerDiscountID = $this->customerdiscount_model->insert();

            //add laundry plan for customer
            $this->load->model('laundryplan_model');
            $options = array();
            $options['customer_id'] = $customer_id;
            $options['business_id'] = $business_id;
            $options['plan'] = $laundryPlan->pounds;
            $options['price'] = $laundryPlan->price;
            $options['days'] = $laundryPlan->days;
            $options['startDate'] = date('Y-m-d H:i:s');
            $options['endDate'] = $endDate;
            $options['active'] = 1;
            $options['description'] = $laundryPlan->description;
            $options['renew'] = 1;
            $options['pounds'] = $laundryPlan->pounds;
            $options['rollover'] = $laundryPlan->rollover;
            $options['businessLaundryPlan_id'] = $laundryPlan->businessLaundryPlan_id;
            $laundryPlanID = $this->laundryplan_model->insert($options);

            // add order charge

            $this->load->model('ordercharge_model');
            $orderChargeID = $this->ordercharge_model->save(array(
                'chargeType' => 'Laundry Plan (' . $laundryPlan->description . ')',
                'chargeAmount' => number_format($laundryPlan->price, 2, '.', ''),
                'order_id' => $order_id,
            ));

            $response = $this->capture($order_id, $business_id);
            $order = $this->get_by_primary_key($order_id);
            if ($response['status'] == "success") {
                $orderStatus = new OrderStatus();
                $orderStatus->order_id = $order_id;
                $orderStatus->locker_id = $order->locker_id;
                $orderStatus->employee_id = null;
                $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
                $orderStatus->note = "Captured Payment";
                $orderStatus->save();
            }

            return array('status' => 'success', 'ordercharge_id' => $orderChargeID, 'laundryplan_id' => $laundryPlanID, 'customerdiscount_id' => $customerDiscountID, 'order_id' => $order_id);
        }
    }

    /**
     * @param stdClass $productPlan - customers current productPlan
     * @return array result
     */
    public function renew_product_plan($productPlan)
    {
        if (empty($productPlan)) {
            throw new \InvalidArgumentException("'productPlan' must be a codeigniter query instance of a Product Plan entity.");
        } else {
            $customer_id = $productPlan->customer_id;
            $business_id = $productPlan->business_id;
            $date = date('Y-m-d H:i:s');
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($business_id);
            $locker_id = $business->prepayment_locker_id;
            $bag_id = 0;
            $rollover = 0;

            // create the order with a status of 3 so we do not send out an inventoried email to the customer
            $options['customer_id'] = $customer_id;
            $options['orderStatusOption_id'] = 3;
            $options['paymentStatusOption_id'] = 1;
            $order_id = $this->order_model->new_order(0, $locker_id, $bag_id, 0, "Product Plan (" . $productPlan->description . ")", $business_id, $options);


            $sql2 = "SELECT sum(amount) AS remaining FROM customerDiscount
                WHERE amountType = 'product_plan' AND customer_id = $customer_id  AND active = 1";
            $select2 = $this->db->query($sql2);
            $line2 = $select2->row();

            // check to see if the customer gets any rollover items added to the new plan
            $rollover = ($line2->remaining > $productPlan->rollover) ? $productPlan->rollover : $line2->remaining;

            //remove any remaining credit
            $update1 = "UPDATE customerDiscount SET active = 0, description = 'Product Plan - EXPIRED'
                    WHERE amountType like 'product_plan%'
                    AND customer_id = $customer_id
                    AND active = 1";
            $query = $this->db->query($update1);

            //deactivate current plans
            $update1 = "UPDATE laundryPlan SET active = 0, renew = 0 WHERE customer_id = $customer_id";
            $query = $this->db->query($update1);

            $newItems = (int)$rollover + (int)$productPlan->credit_amount;

            //insert into customer discount
            $this->load->model('customerdiscount_model');
            $endDate = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') + $productPlan->days, date('Y')));
            $this->customerdiscount_model->clear();
            $this->customerdiscount_model->customer_id = $customer_id;
            $this->customerdiscount_model->amount = $newItems;
            $this->customerdiscount_model->amountType = 'product_plan';
            $this->customerdiscount_model->frequency = 'until used up';
            $this->customerdiscount_model->description = 'Product Plan';
            $this->customerdiscount_model->extendedDesc = $productPlan->description . " - " . (int)$productPlan->credit_amount . " items " . "@ " . format_money_symbol($business_id, '%.2n', $productPlan->price);
            $this->customerdiscount_model->active = 1;
            $this->customerdiscount_model->order_id = $order_id;
            $this->customerdiscount_model->origCreateDate = $date;
            $this->customerdiscount_model->expireDate = $endDate;
            $this->customerdiscount_model->business_id = $business_id;
            $customerDiscountID = $this->customerdiscount_model->insert();

            //add product plan for customer
            $productPlanObj = new \App\Libraries\DroplockerObjects\ProductPlan();
            $productPlanObj->customer_id = $customer_id;
            $productPlanObj->business_id = $business_id;
            $productPlanObj->plan = $productPlan->credit_amount;
            $productPlanObj->description = $productPlan->description;
            $productPlanObj->price = $productPlan->price;
            $productPlanObj->credit_amount = $productPlan->credit_amount;
            $productPlanObj->days = $productPlan->days;
            $productPlanObj->startDate = date('Y-m-d H:i:s');
            $productPlanObj->endDate = $endDate;
            $productPlanObj->rollover = $productPlan->rollover;
            $productPlanObj->active = 1;
            $productPlanObj->expired_discount_percent = $productPlan->expired_discount_percent;
            $productPlanObj->products = $productPlan->products;
            $productPlanObj->renew = 1;
            $productPlanObj->businessLaundryPlan_id = $productPlan->businessLaundryPlan_id;
            $productPlanObj->type = "product_plan";
            $productPlanID = $productPlanObj->save();

            // add order charge

            $this->load->model('ordercharge_model');
            $orderChargeID = $this->ordercharge_model->save(array(
                'chargeType' => 'Product Plan (' . $productPlan->description . ')',
                'chargeAmount' => number_format($productPlan->price, 2, '.', ''),
                'order_id' => $order_id,
            ));

            $response = $this->capture($order_id, $business_id);
            $order = $this->get_by_primary_key($order_id);
            if ($response['status'] == "success") {
                $orderStatus = new OrderStatus();
                $orderStatus->order_id = $order_id;
                $orderStatus->locker_id = $order->locker_id;
                $orderStatus->employee_id = null;
                $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
                $orderStatus->note = "Captured Payment";
                $orderStatus->save();
            }

            return array('status' => 'success', 'ordercharge_id' => $orderChargeID, 'productplan_id' => $productPlanID, 'customerdiscount_id' => $customerDiscountID, 'order_id' => $order_id);
        }
    }

    /**
     * @param stdClass $everythingPlan - customers current everythingPlan
     * @return array result
     */
    public function renew_everything_plan($everythingPlan)
    {
        if (empty($everythingPlan)) {
            throw new \InvalidArgumentException("'everythingPlan' must be a codeigniter query instance of a Product Plan entity.");
        } else {
            $customer_id = $everythingPlan->customer_id;
            $business_id = $everythingPlan->business_id;
            $date = date('Y-m-d H:i:s');
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($business_id);
            $locker_id = $business->prepayment_locker_id;
            $bag_id = 0;
            $rollover = 0;

            // create the order with a status of 3 so we do not send out an inventoried email to the customer
            $options['customer_id'] = $customer_id;
            $options['orderStatusOption_id'] = 3;
            $options['paymentStatusOption_id'] = 1;
            $order_id = $this->order_model->new_order(0, $locker_id, $bag_id, 0, "Everything Plan (" . $everythingPlan->description . ")", $business_id, $options);


            $sql2 = "SELECT sum(amount) AS remaining FROM customerDiscount
                WHERE amountType = 'product_plan' AND customer_id = $customer_id  AND active = 1";
            $select2 = $this->db->query($sql2);
            $line2 = $select2->row();

            // check to see if the customer gets any rollover items added to the new plan
            $rollover = ($line2->remaining > $everythingPlan->rollover) ? $everythingPlan->rollover : $line2->remaining;

            //remove any remaining credit
            $update1 = "UPDATE customerDiscount SET active = 0, description = 'Everything Plan - EXPIRED'
                    WHERE amountType like 'everything_plan%'
                    AND customer_id = $customer_id
                    AND active = 1";
            $query = $this->db->query($update1);

            //deactivate current plans
            $update1 = "UPDATE laundryPlan SET active = 0, renew = 0 WHERE customer_id = $customer_id";
            $query = $this->db->query($update1);

            $newItems = (int)$rollover + (int)$everythingPlan->credit_amount;

            //insert into customer discount
            $this->load->model('customerdiscount_model');
            $endDate = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') + $everythingPlan->days, date('Y')));
            $this->customerdiscount_model->clear();
            $this->customerdiscount_model->customer_id = $customer_id;
            $this->customerdiscount_model->amount = $newItems;
            $this->customerdiscount_model->amountType = 'dollars';
            $this->customerdiscount_model->frequency = 'until used up';
            $this->customerdiscount_model->description = 'Everything Plan';
            $this->customerdiscount_model->extendedDesc = $everythingPlan->description . " - " . (int)$everythingPlan->credit_amount . "@ " . format_money_symbol($business_id, '%.2n', $everythingPlan->price );
            $this->customerdiscount_model->active = 1;
            $this->customerdiscount_model->order_id = $order_id;
            $this->customerdiscount_model->origCreateDate = $date;
            $this->customerdiscount_model->expireDate = $endDate;
            $this->customerdiscount_model->business_id = $business_id;
            $customerDiscountID = $this->customerdiscount_model->insert();


            //add everything plan for customer
            $everythingPlanObj = new \App\Libraries\DroplockerObjects\EverythingPlan();
            $everythingPlanObj->customer_id = $customer_id;
            $everythingPlanObj->business_id = $business_id;
            $everythingPlanObj->plan = $everythingPlan->credit_amount;
            $everythingPlanObj->description = $everythingPlan->description;
            $everythingPlanObj->price = $everythingPlan->price;
            $everythingPlanObj->credit_amount = $everythingPlan->credit_amount;
            $everythingPlanObj->days = $everythingPlan->days;
            $everythingPlanObj->startDate = date('Y-m-d H:i:s');
            $everythingPlanObj->endDate = $endDate;
            $everythingPlanObj->rollover = $everythingPlan->rollover;
            $everythingPlanObj->active = 1;
            $everythingPlanObj->expired_discount_percent = $everythingPlan->expired_discount_percent;
            $everythingPlanObj->products = $everythingPlan->products;
            $everythingPlanObj->renew = 1;
            $everythingPlanObj->type = "everything_plan";
            $everythingPlanObj->businessLaundryPlan_id = $everythingPlan->businessLaundryPlan_id;
            $everythingPlanID = $everythingPlanObj->save();

            // add order charge

            $this->load->model('ordercharge_model');
            $orderChargeID = $this->ordercharge_model->save(array(
                'chargeType' => 'Everything Plan (' . $everythingPlan->description . ')',
                'chargeAmount' => number_format($everythingPlan->price, 2, '.', ''),
                'order_id' => $order_id,
            ));

            $response = $this->capture($order_id, $business_id);
            $order = $this->get_by_primary_key($order_id);
            if ($response['status'] == "success") {
                $orderStatus = new OrderStatus();
                $orderStatus->order_id = $order_id;
                $orderStatus->locker_id = $order->locker_id;
                $orderStatus->employee_id = null;
                $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
                $orderStatus->note = "Captured Payment";
                $orderStatus->save();
            }

            return array('status' => 'success', 'ordercharge_id' => $orderChargeID, 'productplan_id' => $everythingPlanID, 'customerdiscount_id' => $customerDiscountID, 'order_id' => $order_id);
        }
    }

    /**
     * customer orders a laundry plan
     *
     * $options values
     * --------------------
     * customer_id
     * business_id
     * plan_id
     * plan_description
     */
    public function new_laundry_plan_order($options = array())
    {
        if (!required(array('business_id', 'customer_id', 'plan_id'), $options)) {
            return array('status' => 'error', 'message' => "Missing required fields");
        }

        $type = false;
        if (!empty($options["type"])) {
            $type = $options["type"];
        }

        $business = new Business($options['business_id'], false);

        $date = date('Y-m-d H:i:s');
        $locker_id = $business->prepayment_locker_id;
        $bag_id = 0;
        $business_id = $options['business_id'];
        $customer_id = $options['customer_id'];

        $sql = "SELECT count(*) as total from laundryPlan WHERE customer_id = {$customer_id}
                AND active = 1";
        $query = $this->db->query($sql);
        $result = $query->row();
        if ($result->total > 1) {
            set_flash_message('error', "You already have an active plan. You can only upgrade at this time");
            if (!$type) {
                redirect('/main/plans');
            } elseif ($type == "everything_plan") {
                redirect('/main/everything_plans');
            } elseif ($type == "product_plan") {
                redirect('/main/product_plans');
            }
        }

        // get the laundry plan
        $businessLaundryPlanID = $options['plan_id'];
        if (!$type) {
            $this->load->model('businesslaundryplan_model');
            $this->businesslaundryplan_model->businessLaundryPlanID = $options['plan_id'];
            $plan = $this->businesslaundryplan_model->get();
        } elseif ($type == "everything_plan") {
            $plan = $this->everything_plans = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(array(
                    'businessLaundryPlanID' => $options['plan_id']
                ));
        } elseif ($type == "product_plan") {
            $plan = $this->product_plans = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(array(
                    'businessLaundryPlanID' => $options['plan_id']
                ));
        }


        // if days END endDate is set, go with endDate first
        // if endDate is not valid, ie... expired, go with days
        if ($plan[0]->endDate > $date) {
            $endDate = $plan[0]->endDate;
            $autoRenew = 0;
            $days = 0;
        } else {
            $days = $plan[0]->days;
            $endDate = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d') + $days, date('Y')));
            $autoRenew = 1;
        }

        if ($endDate <= $date) {
            throw new Exception("plan end date is in the past");
        }


        $locker = new Locker($locker_id);

        // create the order with a status of 3 so we do not send out an inventoried email to the customer
        $options['customer_id'] = $customer_id;
        $options['orderStatusOption_id'] = 3;
        $options['paymentStatusOption_id'] = 1;
        
        $plan_description = "Laundry Plan (" . $plan[0]->description . ")";
        if ($type == "everything_plan") {
            $plan_description = "Everything Plan (" . $plan[0]->description . ")";
        }
        if ($type == "product_plan") {
            $plan_description = "Product Plan (" . $plan[0]->description . ")";
        }

        $order_id = $this->order_model->new_order($locker->location_id, $locker_id, $bag_id, 0, $plan_description, $business_id, $options);

        // add order charge
        $this->load->model('ordercharge_model');
        $this->ordercharge_model->chargeType = $plan_description;
        $this->ordercharge_model->chargeAmount = number_format($plan[0]->price, 2, '.', '');
        $this->ordercharge_model->order_id = $order_id;
        $this->ordercharge_model->insert();

        //insert into customer discount
        $this->load->model('customerdiscount_model');
        $this->customerdiscount_model->customer_id = $customer_id;
        
        if (!$type) {
            $this->customerdiscount_model->amount = $plan[0]->pounds;
            $this->customerdiscount_model->amountType = 'pounds';
            $this->customerdiscount_model->frequency = 'until used up';
            $this->customerdiscount_model->description = 'Laundry Plan';
            $this->customerdiscount_model->extendedDesc = $plan[0]->description . " - " . $plan[0]->pounds . weight_system_format('', false, true) . " @ " . format_money_symbol($options['business_id'], '%.2n', $plan[0]->price);
        } elseif ($type == "everything_plan") {
            $this->customerdiscount_model->amount = $plan[0]->credit_amount;
            $this->customerdiscount_model->amountType = 'dollars';
            $this->customerdiscount_model->frequency = 'until used up';
            $this->customerdiscount_model->description = 'Everything Plan';
            $this->customerdiscount_model->extendedDesc = $plan[0]->description . " - " . format_money_symbol($options['business_id'], '%.2n', $plan[0]->credit_amount) . " @ " . format_money_symbol($options['business_id'], '%.2n', $plan[0]->price);
        } elseif ($type == "product_plan") {
            $this->customerdiscount_model->amount = (int)$plan[0]->credit_amount;
            $this->customerdiscount_model->amountType = 'product_plan';
            $this->customerdiscount_model->frequency = 'until used up';
            $this->customerdiscount_model->description = 'Product Plan';
            $this->customerdiscount_model->extendedDesc = $plan[0]->description . " - " . (int)$plan[0]->credit_amount . " items @ " . format_money_symbol($options['business_id'], '%.2n', $plan[0]->price);
        }
        
        $this->customerdiscount_model->active = 1;
        $this->customerdiscount_model->order_id = $order_id;
        $this->customerdiscount_model->origCreateDate = $date;
        $this->customerdiscount_model->expireDate = $endDate;
        $this->customerdiscount_model->business_id = $business_id;
        $customerdiscountID = $this->customerdiscount_model->insert();

        //add laundry plan for customer
        if (!$type) {
            $this->load->model('laundryplan_model');
            $options = array();
            $options['customer_id'] = $customer_id;
            $options['business_id'] = $business_id;
            $options['plan'] = $plan[0]->pounds;
            $options['price'] = $plan[0]->price;
            $options['days'] = $days;
            $options['startDate'] = date('Y-m-d H:i:s');
            $options['endDate'] = $endDate;
            $options['active'] = 1;
            $options['description'] = $plan[0]->description;
            $options['renew'] = $autoRenew;
            $options['pounds'] = $plan[0]->pounds;
            $options['rollover'] = $plan[0]->rollover;
            $options['expired_price'] = $plan[0]->expired_price;
            $options['businessLaundryPlan_id'] = $businessLaundryPlanID;
            $laundryPLanID = $this->laundryplan_model->insert($options);
        } elseif ($type == "everything_plan") {
            $everythingPlan = new \App\Libraries\DroplockerObjects\EverythingPlan();
            $everythingPlan->customer_id = $customer_id;
            $everythingPlan->business_id = $business_id;
            $everythingPlan->plan = $plan[0]->credit_amount;
            $everythingPlan->description = $plan[0]->description;
            $everythingPlan->price = $plan[0]->price;
            $everythingPlan->credit_amount = $plan[0]->credit_amount;
            $everythingPlan->days = $days;
            $everythingPlan->endDate = $endDate;
            $everythingPlan->startDate = date('Y-m-d H:i:s');
            $everythingPlan->rollover = $plan[0]->rollover;
            $everythingPlan->active = 1;
            $everythingPlan->renew = $autoRenew;
            $everythingPlan->expired_discount_percent = $plan[0]->expired_discount_percent;
            $everythingPlan->businessLaundryPlan_id = $businessLaundryPlanID;
            $everythingPlan->type = "everything_plan";
            $laundryPLanID = $everythingPlan->save();
        } elseif ($type == "product_plan") {
            $productPlan = new \App\Libraries\DroplockerObjects\ProductPlan();
            $productPlan->customer_id = $customer_id;
            $productPlan->business_id = $business_id;
            $productPlan->plan = $plan[0]->credit_amount;
            $productPlan->description = $plan[0]->description;
            $productPlan->price = $plan[0]->price;
            $productPlan->credit_amount = $plan[0]->credit_amount;
            $productPlan->days = $days;
            $productPlan->startDate = date('Y-m-d H:i:s');
            $productPlan->endDate = $endDate;
            $productPlan->rollover = $plan[0]->rollover;
            $productPlan->active = 1;
            $productPlan->expired_discount_percent = $plan[0]->expired_discount_percent;
            $productPlan->products = $plan[0]->products;
            $productPlan->renew = $autoRenew;
            $productPlan->businessLaundryPlan_id = $businessLaundryPlanID;
            $productPlan->type = "product_plan";
            $laundryPLanID = $productPlan->save();
        }

        return array('status' => 'success', 'customerdiscount_id' => $customerdiscountID, 'laundryplan_id' => $laundryPLanID, 'order_id' => $order_id, 'message' => "You have successfully purchased the {$plan[0]->description}");
    }

    /** Deprecated, do not use.
     * Gets the orderType for a specific order
     *
     * * At time of writing orderTypes include SMS, Dry Cleaning, Wash and Fold, Other, Shoe Shine
     * * Additional orderTypes will be added in the future.
     * * orderTypes are stored in the orderType table
     *
     * @param int $orderID
     * @return string orderType
     */
    public function getOrderType($orderID)
    {
        $order = $this->get_by_primary_key($orderID);
        $orderType = 'Other';
        $sqlGetItems = "select module.name, module.slug, productCategory.slug as module, product_processID
                        from orderItem
                        join product_process on product_processID = product_process_id
                        join product on product_id = productID
                        JOIN productCategory ON product.productCategory_id = productCategoryID
                        join module on moduleID = productCategory.module_id
                        where order_id = $orderID";
        $query = $this->db->query($sqlGetItems);
        $items = $query->result();
        if ($items) {
            $default_package_delivery_id = get_business_meta($order->business_id, 'default_package_delivery_id');
            foreach ($items as $i) {
                if (stristr($i->name, 'whsl')) {
                    $orderType = 'Wholesale';
                } elseif ($i->slug == 'wf') {
                    $orderType = 'WF';
                    break;
                } elseif ($i->slug == 'shoe') {
                    $orderType = 'SS';
                    break;
                } elseif ($i->product_processID == $default_package_delivery_id) {
                    $orderType = 'PD';
                    break;
                } elseif ($i->slug == 'dc') {
                    $orderType = 'DC';
                } elseif ($i->slug == 'laundry') {
                    $orderType = 'DC';
                } elseif ($i->slug == 'upcharge') {
                    $orderType = 'DC';
                } else {
                    $orderType = 'Other';
                    break;
                }
            }
        }

        return $orderType;
    }
    
    /**
     * Uncaptures an order by setting its payment status to "Unpayed".
     *
     * @param int $order_id
     * @param int $employee_id
     * @return bool "True" if the uncapturing the order succeeded, or "False" if uncapturing the order failed
     */
    public function uncapture($order_id, $employee_id, $notes = null)
    {
        $order = new Order($order_id);
        // update the orderstatus so that it shows in the order history on the order detail page
        $this->logger->set('orderStatus', array(
            'order_id' => $order_id,
            'locker_id' => $order->locker_id,
            'employee_id' => $employee_id,
            'orderStatusOption_id' => $order->orderStatusOption_id,
            'note' => "Uncaptured Payment. $notes"
        ));

        $options = array();
        $options['order_id'] = $order_id;
        $options['locker_id'] = $order->locker_id;
        $options['orderStatusOption_id'] = $order->orderStatusOption_id;
        $options['orderPaymentStatusOption_id'] = 1; //Note, "1" is expected to be the 'Unpayed" status

        return $this->update_order($options);
    }

    public function mark_as_captured($order_id, $employee_id)
    {
        $order = new Order($order_id);
        // update the orderstatus so that it shows in the order history on the order detail page
        $this->logger->set('orderStatus', array(
            'order_id' => $order_id,
            'locker_id' => $order->locker_id,
            'employee_id' => $employee_id,
            'orderStatusOption_id' => $order->orderStatusOption_id,
            'note' => "Manually changed to captured without charging card"
        ));

        $this->load->model('transaction_model');
        $this->transaction_model->save(array(
            'processor'   => 'manual',
            'type'        => 'sale',
            'pnref'       => '',
            'customer_id' => $order->customer_id,
            'business_id' => $order->business_id,
            'order_id'    => $order->orderID,
            'employee_id' => $employee_id,
            'amount'      => 0,
            'resultCode'  => 0,
            'message'     => "Manually changed to captured without charging card",
        ));

        $options = array();
        $options['order_id'] = $order_id;
        $options['locker_id'] = $order->locker_id;
        $options['orderStatusOption_id'] = $order->orderStatusOption_id;
        $options['orderPaymentStatusOption_id'] = 3;

        return $this->update_order($options);
    }

    /**
     * Checks to see if the customer has additional orders that need to be captured
     * Sends an email to Customer Support for the business if any orders are captured
     * @param int $customer_id
     * @param int $business_id
     * @param int $order_id
     * @param string $subject The subject of the email that goes to customer support
     * @param bool $capture
     */
    public function checkAdditionalOrders($customer_id, $business_id, $order_id = '', $subject='', $capture = false)
    {
        $sqlOrder = '';

        if (!empty($order_id)) {
            $sqlOrder = " AND orderID != ".$order_id;
        }

        // check to see if the customer has any other open orders.
        // if they do, email support
        $sql = 'SELECT * FROM orders
            WHERE customer_id = '.$customer_id;
        $sql .= $sqlOrder;
        $sql .= '
            AND orderPaymentStatusOption_id <> 3
            AND orderStatusOption_id NOT IN (1, 2, 3, 5, 6, 14)
            AND business_id = '.$business_id;
        $query = $this->db->query($sql);

        $customer = new Customer($customer_id);
        $body = "Customer: ".  getPersonName($customer)." [<a href='https://".$_SERVER['HTTP_HOST']."/admin/customers/detail/".$customer_id."'>".$customer_id."</a>]<br><br>";
        $body .= (!empty($order_id))?"The original order that was captured: <a href='https://".$_SERVER['HTTP_HOST']."/admin/orders/details/{$order_id}'>{$order_id}</a><br>":"";
        $additionalOrders = $query->result();
        $captureResults = array();
        if ($additionalOrders) {
            if ($capture) {
                $body .= "Orders captured after customer authorizes new card:<br>";
            } else {
                $body .= "This customer has additional unpaid orders:<br>";
            }

            foreach ($additionalOrders as $additionalOrder) {
                if ($capture) {
                    $captureResults[] = $lastResponse = $this->capture($additionalOrder->orderID, $business_id);
                }
                $statusText = $lastResponse['status'] == 'success' ? 'SUCCESS' : 'ERROR: ' . $lastResponse['message'];
                $body .= "<a href='https://".$_SERVER['HTTP_HOST']."/admin/orders/details/".$additionalOrder->orderID."'>".$additionalOrder->orderID." ".$statusText.".</a><br>";
            }

            queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($business_id, 'customerSupport'), $subject, $body, array(), null, $business_id);
        }

        return $results = array('additionalOrders'=>$additionalOrders, 'captureResults'=>$captureResults);
    }

    /**
     * Charges a customer associated with the specified order
     * @param int $order_id
     * @param int $business_id
     * @param int $employee_id
     * @return array (status = success/fail, order_id, message, amount)
     * {
     *      status => string success|fail
     *      order_id => int
     *      message => string
     *      amount => float
     * }
     */
    public function capture($order_id, $business_id, $employee_id='', $isPrePayment = false, $is_one_time = 0)
    {
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric");
        } elseif (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        } else {
            // Gets the order by order_id, and thet customer from order->customer_id and customer creditcard
            $sql = "SELECT orders.*, customerID, customer.inactive, customer.autoPay, creditCard.payer_id FROM orders
                LEFT JOIN customer ON customerID = orders.customer_id
                LEFT JOIN creditCard ON creditCard.customer_id = customerID AND creditCard.business_id = ?
                WHERE orderID = ?";
            $query = $this->db->query($sql, array($business_id, $order_id, $business_id));
            $order = $query->row();
            // does order have a customer
            if ($order->customer_id == '') {
                return array("status" => "error",'order_id' => $order->orderID, 'message' => "This order cannot be captured - Missing Customer.");
            } elseif ($order->inactive == 1) { // is customer inactive

                return array("status" => "error",'order_id' => $order->orderID, 'message' => "This order cannot be captured - Customer is inactive.");
            } elseif ($order->orderPaymentStatusOption_id == 3) {  // has order been captured already

                return array("status" => "error",'order_id' => $order->orderID, 'message' => "Order has already been captured.");
            } else {
                $this->update_order_taxes($order->orderID);
                $orderTotal = $this->get_net_total($order->orderID);
                $orderNetTotal = $orderTotal;

                $orderTotal = number_format($orderTotal, 2, '.', '');

                // if this customer is not on autopayments (thus does not have a credit card on file), and the order is NOT on payment hold, put order on payment hold
                // does not have a credit card on file and not on payment hold already
                //for one time payments, autopay is 0 but there is a card on file
                if (!$is_one_time && $order->autoPay != 1 && $orderTotal > 0) {
                    if ($order->orderStatusOption_id != 7) {
                        $options = array();
                        $options['order'] = $order;
                        $options['locker_id'] = $order->locker_id;
                        $options['orderStatusOption_id'] = 7;
                        $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
                        $options['employee_id'] = $employee_id;
                        $this->update_order($options);
                    }

                    return array("status" => "error",'order_id' => $order->orderID, 'message' => "This order cannot be captured - Customer is not auto pay.");
                } else {
                    $CI = get_instance();
                    $CI->customer_id = $order->customer_id;

                    // If this is a free order
                    if ($orderTotal <= .05) {
                        //ASL - 10-13-07 - change $0 orders to captured
                        $options = array();
                        $options['order'] = $order;
                        $options['locker_id'] = $order->locker_id;
                        $options['orderStatusOption_id'] = $order->orderStatusOption_id;
                        $options['orderPaymentStatusOption_id'] = 3;
                        $options['employee_id'] = $employee_id;
                        $this->update_order($options);
                        //$message = 'Order ' . $order->orderID . ' total is 0 - changed to captured';
                        $message = "Funds captured for order {$order->orderID}.";

                        if ($isPrePayment) {
                            $message .= " Prepaid amount set.";
                            
                            $prepaidOrder = new Order($order_id);
                            $prepaidOrder->pre_paid_amount = $orderNetTotal;
                            $prepaidOrder->save();
                        }

                        return array("status" => "success", 'order_id' => $order->orderID, 'message' => $message, 'amount' => $orderTotal);
                    } elseif ($order->payer_id == '') { // If the customer does not have a reference code (payer_id) in the creditcard table

                        if ($order->orderStatusOption_id != 7) {
                            $options = array();
                            $options['order'] = $order;
                            $options['locker_id'] = $order->locker_id;
                            $options['orderStatusOption_id'] = 7; //payment hold
                            $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
                            $options['employee_id'] = $employee_id;
                            $this->update_order($options);
                        }

                        return array("status" => "error",'order_id' => $order->orderID, 'message' => 'Customer does not have a credit card on file', 'amount' => $orderTotal);
                    } else {
                        $this->load->library('creditcardprocessor');
                        $creditCard = CreditCard::search_aprax(array('customer_id'=>$order->customer_id));

                        if (CreditCard::isExpired($creditCard[0]->expMo, $creditCard[0]->expYr)) {
                            $this->putOnPaymentHold($order, $employee_id);
                            $expDate = sprintf('%02d/%d', $creditCard[0]->expMo, $creditCard[0]->expYr);

                            return array("status" => "error",'order_id' => $order->orderID, 'message' => 'Payment Not Captured - Credit card is expired. Exp Date: ' . $expDate, 'amount' => $orderTotal);
                        }

                        //get the processor for this customer
                        $payment = $this->creditcardprocessor->factory($creditCard[0]->processor, $business_id);

                        if (DEV) {
                            $payment->setTestMode();
                        }

                        //make the sale
                        $response = $payment->makeSale($orderTotal, $order->orderID, $order->customer_id, $order->business_id);

                        // process the order based on the status from the processor
                        if ($response->get_status() == 'SUCCESS') {
                            //update the order to show that it has been paid for
                            $options = array();
                            $options['order'] = $order;
                            $options['locker_id'] = $order->locker_id;
                            $options['orderStatusOption_id'] = $order->orderStatusOption_id;
                            $options['orderPaymentStatusOption_id'] = 3; //payment captured
                            $options['employee_id'] = $employee_id;
                            $this->update_order($options);
                            $message = 'Funds captured for order ' . $order->orderID.' '.$response->get_message();

                            if ($isPrePayment) {
                                $message .= ". Prepaid amount set.";

                                $prepaidOrder = new Order($order_id);
                                $prepaidOrder->pre_paid_amount = $orderNetTotal;
                                $prepaidOrder->save();
                            }

                            //if this order was on payment hold, change the status
                            if ($order->orderStatusOption_id == 7) {
                                //make sure there was a delivery failure though
                                $this->load->model('orderstatus_model');
                                $this->orderstatus_model->order_id = $order->orderID;

                                //if there was a delivery failure
                                $this->orderstatus_model->orderStatusOption_id = 13;
                                if ($stat = $this->orderstatus_model->get()) {
                                    //change it to ready for pickup
                                    $options = array();
                                    $options['order'] = $order;
                                    $options['locker_id'] = $order->locker_id;
                                    $options['orderStatusOption_id'] = 9; // ready for customer pickup - this in turn sets to 10 - complete
                                    $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
                                    $options['employee_id'] = $employee_id;
                                    $this->update_order($options);
                                    $message .= '<br>Status changed to 9 for ' . $order->orderID;
                                } else {
                                    //if not change it to out for delivery
                                    $this->orderStatusOption_id = 8;
                                    $options = array();
                                    $options['order'] = $order;
                                    $options['locker_id'] = $order->locker_id;
                                    $options['orderStatusOption_id'] = 8;
                                    $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
                                    $options['employee_id'] = $employee_id;
                                    $this->update_order($options);
                                    $message .= '<br>Status changed to 8 for ' . $order->orderID;
                                }
                            } else {
                                // if not already on payment hold
                                //see if there was ever a Delivery Failure - unpaid
                                $sqlGetDeliv = "select * from orderStatus where order_id = {$order->orderID} and orderStatusOption_id = 13;";
                                $select = query($sqlGetDeliv);
                                if (sizeof($select) >= 1) {
                                    //change it to ready for pickup
                                    $options = array();
                                    $options['order'] = $order;
                                    $options['locker_id'] = $order->locker_id;
                                    $options['orderStatusOption_id'] = 9;
                                    $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
                                    $options['employee_id'] = $employee_id;
                                    $this->update_order($options);

                                    $message .= '<br>Status changed to 9 for ' . $order->orderID;
                                }
                            }

                            return array('status' => "success", "order_id" => $order->orderID, "message" => $message, "amount" => $response->get_amount());
                        } else { // Credit card process FAIL
                            $errorMessage = $response->get_message();
                            $putOnPaymentHold = true;

                            // Paypro error:
                            // 101 = ACQUIRER GATEWAY ERROR: Response code indicating that the acquirer processor encountered an error. This is software that handles financial transactions and communicates with the private banking network.
                            // 102 = PAYMENT ENGINE ERROR: Response indicating that the payment service encountered an error.
                            if ($response->get_resultCode() == 101 || $response->get_resultCode() == 102) {
                                $putOnPaymentHold = false;
                            }

                            // verisign result code for amex and discovery when amount is less then 1.00
                            if ($response->get_resultCode() == 4 && $creditCard[0]->processor == 'verisign') {
                                $putOnPaymentHold = false;
                            }

                            send_admin_notification("Problem with payment for order " . $order->orderID, "Problem with payment for order " . $order->orderID . "<br>{$errorMessage}", $business_id);

                            //put the order on payment hold if it's not on payment hold already
                            if ($putOnPaymentHold) {
                                $this->putOnPaymentHold($order, $employee_id);
                            }

                            return array('order_id' => $order->orderID, 'message' => "Payment Not Captured - Credit card on file but processor rejected it. ".$response->get_message()." ResultCode: " . $response->get_resultCode());
                        }
                    }
                }
            }
        }
    }
    
    public function captureMany($order_id_list, $business_id, $employee_id, $isPrePayment = false, $is_one_time = 0)
    {
        $success = true;
        $message_list = array();
        $data = array(
            'details' => array(),
            'paid_orders' => array(),
            'unpaid_orders' => array(),
            'amount' => 0,
            'change' => 0,
        );
        
        $order_id_list = (array) $order_id_list;
        
        foreach ($order_id_list as $order_id) {
            $res = $this->capture($order_id, $business_id, $employee_id, $isPrePayment, $is_one_time);
            $order_success = ($res['status'] === 'success');
            
            $success = $success && $order_success;
            $data['details'][$order_id] = $res;
            
            if ($order_success) {
                $data['paid_orders'][] = $order_id;
                $data['amount'] += $res['amount'];
                
                $note = "Captured Payment.";
                if ($isPrePayment) {
                    $note .= " Prepayment set.";
                }
                
                $order = new Order($order_id);
                $orderStatus = new OrderStatus();
                $orderStatus->order_id             = $order_id;
                $orderStatus->locker_id            = $order->locker_id;
                $orderStatus->employee_id          = $employee_id;
                $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
                $orderStatus->note                 = $note;
                $orderStatus->save();
            } else {
                $data['unpaid_orders'][] = $order_id;
            }
        }
        
        if ($data['paid_orders']) {
            $message_list[] = "Paid orders: " . implode(', ', $data['paid_orders']) . ".";
        } else {
            $message_list[] = "No orders have been paid.";
        };
        
        if ($data['unpaid_orders']) {
            $message_list[] = "Non paid orders: " . implode(', ', $data['unpaid_orders']) . ".";
            
            foreach ($data['unpaid_orders'] as $unpaid_order) {
                $message_list[] = "Order $unpaid_order: {$data['details'][$unpaid_order]['message']}";
            }
        }
        
        return array(
            'success' => $success,
            'message' => implode("\n", $message_list),
            'data' => $data,
        );
    }

    /**
     * Puts an order in payment hold
     *
     * @param object $order
     * @param int $employee_id
     * @return boolean
     */
    public function putOnPaymentHold($order, $employee_id)
    {
        if (!in_array($order->orderStatusOption_id, array(7,13))) {
            $options = array();
            $options['order'] = $order;
            $options['locker_id'] = $order->locker_id;
            $options['orderStatusOption_id'] = 7;
            $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
            $options['employee_id'] = $employee_id;
            $this->update_order($options);

            return true;
        }

        return false;
    }

    /**
     *
     * checks to see if an order is capturable (payment)
     *
     * the return array
     * --------------
     * status (SUCCESS|FAIL)
     * message - only if a FAIL
     *
     *
     * @param object or int $order
     * @return array
     */
    public function isCapturable($order)
    {
        if (is_object($order)) {
            $order = $order->orderID;
        }

        return $this->isAutoPayable($order);
    }

    /**
     *
     * verifies that an order is payable and the customer has the ability to pay
     *
     * the return array
     * --------------
     * status (SUCCESS|FAIL)
     * message - only if a FAIL
     *
     * @param object or int $order
     * @return array
     */
    public function isAutoPayable($order_id)
    {
        $autoPay = false;
        if (is_numeric($order_id)) {
            $order = $this->get(array('orderID' => $order_id));
            $order = $order[0];
        } else {
            throw new \Exception('order_id is missing');
        }

        if ($order->customer_id != '') {
            $sql = "SELECT autoPay, inactive, cc.* FROM customer
                LEFT JOIN creditCard cc ON customer_id = customerID
                WHERE customerID = {$order->customer_id}";
            $customer = $this->db->query($sql)->row();
            $order->autoPay = $customer->autoPay;
            $order->inactive = $customer->inactive;
            $order->payer_id = $customer->payer_id;
        }


        $error = false;
        $message = array();

        if ($order->customer_id == '') {
            $error = true;
            $message[] = "No Customer for this order";
        }

        if ($order->autoPay != 1) {
            $error = true;
            $message[] = "Customer not on autopay";
        }

        if ($order->inactive == 1) {
            $error = true;
            $message[] = "Customer is inactive";
        }

        if ($order->payer_id == '') {
            $error = true;
            $message[] = "Customer missing payer_id";
        }

        if ($order->orderPaymentStatusOption_id != 1) {
            $error = true;
            $message[] = "Order Payment Status Options is not 1";
        }

        if ($error) {
            $response = array("status" => "FAIL", 'message' => $message);
        } else {
            $response = array('status' => 'SUCCESS');
        }

        return $response;
    }


    //this function returns some stats about orders
    public function orderStats()
    {
        $getStats = "select sum(closedGross) as grossSales, sum(closedDisc) as grossDisc, count(orderID) as orderCount from report_orders "
                . "    where business_id = $this->business_id"
                . "      and dateCreated >= date_sub(now(), INTERVAL 90 DAY)";
        $q = $this->db->query($getStats);
        $total = $q->row();

        return $total;
    }

    public function getOpenOrders($bagNumber, $business_id)
    {
        $sql = "SELECT * FROM orders
                JOIN bag ON (bagID = bag_id)
                WHERE bagNumber = ?
                AND orders.business_id = ?
                AND orderStatusOption_id <> 10";

        return $this->db->query($sql, array($bagNumber, $business_id))->result();
    }

    public function getOrdersByBag($bagNumber, $business_id)
    {
        //FIXME: hardcoded locationType name
        $sql = "SELECT firstName, lastName, orderID, lockerLockType.name as lockType,lockerID,
                locker_id, locker.location_id, lockerName, locationID, location.address,
                locationType.name as serviceMethod, locationType.lockerType, bagNumber, orders.dateCreated
            FROM customer
            join bag on bag.customer_id = customerID
            join orders on orders.bag_id = bagID
            join locker on orders.locker_id = lockerID
            join location on locationID = locker.location_id
            join lockerLockType on lockerLockTypeID = lockerLockType_id
            join locationType on locationTypeID = locationType_id
            where bagNumber = ?
            and customer.business_id = ?
            order by dateCreated DESC";

        return $this->db->query($sql, array((string)$bagNumber, $business_id))->result();
    }

    /**
     * Makes a manual capture
     *
     * @param int $order_id
     * @param float $amount
     * @param string $processor
     * @param int $employee_id
     * @param string $data
     *
     * @return array
     *  - status: 'error' or 'success'
     *  - message
     *  - order_id
     *  - amount
     */
    public function manual_capture($order_id, $amount, $processor, $employee_id, $data = null, $isPrePayment = false)
    {
        if (strstr($amount, ',')) {
            //this is for currency format like: 1.200,50
            $amount = str_replace('.', '', $amount);
            $amount = floatval(str_replace(',', '.', $amount));
        }

        $order = new Order($order_id);
        $total = $this->get_net_total($order_id);

        $change = $amount - $total;
        $f_total = format_money_symbol($order->business_id, '%.2n', $total);
        $f_amount = format_money_symbol($order->business_id, '%.2n', $amount);
        $f_change = format_money_symbol($order->business_id, '%.2n', $change);

        if ($amount < $total) {
            return array(
                'status'   => 'error',
                'message'  => "Insufficient funds for order {$order->orderID}: $f_amount is less than order total $f_total.",
                'order_id' => $order->orderID,
                'amount'   => $total
            );
        }

        $message = "Funds captured for order {$order->orderID}.";
        switch ($processor) {
            case 'cash':
                $notes = "Paid with cash, amount: $f_amount, change: $f_change.";
                break;

            case 'check':
                $notes = "Paid with check number: $data, amount: $f_amount.";
                break;

            case 'order':
                $notes = "Paid with order: $data, amount: $f_amount.";
                break;

            default:
                $notes = "Paid with $processor, amount: $f_amount, change: $f_change.";
                break;
        }

        $this->load->model('transaction_model');
        $this->transaction_model->save(array(
            'processor'   => $processor,
            'type'        => 'sale',
            'pnref'       => '',
            'customer_id' => $order->customer_id,
            'business_id' => $order->business_id,
            'order_id'    => $order->orderID,
            'employee_id' => $employee_id,
            'amount'      => (string)$amount,
            'resultCode'  => 0,
            'message'     => $notes,
        ));

        $options = array();
        $options['order_id'] = $order->orderID;
        $options['locker_id'] = $order->locker_id;
        $options['orderStatusOption_id'] = $order->orderStatusOption_id;
        $options['orderPaymentStatusOption_id'] = 3; //payment captured
        $options['employee_id'] = $employee_id;
        
        // set as paid
        $this->update_order($options);

        if ($isPrePayment) {
            $prepaidOrder = new Order($order_id);
            $prepaidOrder->pre_paid_amount = $total;
            $prepaidOrder->save();
        }
        
        if ($notes && $isPrePayment) {
            $notes .= " Prepaid amount set.";
        }

        $orderStatus = new OrderStatus();
        $orderStatus->order_id             = $order_id;
        $orderStatus->locker_id            = $order->locker_id;
        $orderStatus->employee_id          = $employee_id;
        $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
        $orderStatus->note                 = $notes;
        $orderStatus->save();

        // check for a delivery failure
        $deliveryFailures = OrderStatus::search_aprax(array(
            'orderStatusOption_id' => 13,
            'order_id' => $order->orderID,
        ));

        // if the order was on hold or had a delivery failure, change its status
        if ($order->orderStatusOption_id != 10 && ($order->orderStatusOption_id == 7 || $deliveryFailures)) {
            //if there was a delivery failure
            if ($deliveryFailures) {
                $options['orderStatusOption_id'] = 9; //change it to ready for pickup
            } else {
                $options['orderStatusOption_id'] = 8; //change it to out for delivery
            }

            $message .= "<br>Status changed to {$options['orderStatusOption_id']} for {$order->orderID}";
            $this->update_order($options);
        }

        return array(
            'status'   => 'success',
            'message'  => $message,
            'order_id' => $order->orderID,
            'amount'   => $total,
            'change' => $change,
            'amount_provided' => $amount,
        );
    }
    
    public function pay_all_with_cash($order_id_list, $amount, $employee_id, $isPrePayment = false)
    {
        $success = true;
        $message_list = array();
        $data = array(
            'details' => array(),
            'paid_orders' => array(),
            'unpaid_orders' => array(),
            'amount' => $amount,
            'change' => 0,
        );
        
        $order_id_list = (array) $order_id_list;
        
        foreach ($order_id_list as $order_id) {
            $res = $this->manual_capture($order_id, $amount, 'cash', $employee_id, null, $isPrePayment);
            $order_success = ($res['status'] === 'success');
            
            $success = $success && $order_success;
            $data['details'][$order_id] = $res;
            
            if ($order_success) {
                $data['paid_orders'][] = $res['order_id'];
                $data['change'] = $res['change'];
                
                $amount = $res['change'];
            } else {
                $data['unpaid_orders'][] = $res['order_id'];
            }
        }
        
        if ($data['paid_orders']) {
            $message_list[] = "Paid orders: " . implode(', ', $data['paid_orders']) . ".";
        } else {
            $message_list[] = "No orders have been paid.";
        };
        
        if ($data['unpaid_orders']) {
            $message_list[] = "Non paid orders: " . implode(', ', $data['unpaid_orders']) . ".";
        }
        
        return array(
            'success' => $success,
            'message' => implode("\n", $message_list),
            'data' => $data,
        );
    }

    public function update_order_status($order_id, $orderStatusOption_id, $employee_id, $business_id, $options = array())
    {
        $order = $this->get_by_primary_key($order_id);

        if (empty($order)) {
            throw new Exception("Order '{$order_id}' does not exist");
        }

        if ($order->business_id != $business_id) {
            throw new Exception("This order does not belong to your business.");
        }

        $options['order'] = $order;
        $options['orderStatusOption_id'] = $orderStatusOption_id;
        $options['locker_id'] = $order->locker_id;
        $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
        $options['employee_id'] = $employee_id;

        return $this->update_order($options);
    }
    
    public function update_all_statuses($order_id_list, $orderStatusOption_id, $employee_id, $business_id)
    {
        $success = true;
        $message_list = array();
        $data = array();
        
        $order_id_list = (array) $order_id_list;
        
        foreach ($order_id_list as $order_id) {
            try {
                $res = $this->update_order_status($order_id, $orderStatusOption_id, $employee_id, $business_id);
            } catch (Exception $exc) {
                $res = array(
                    'status' => 'error',
                    'message' => "Exception: " . $exc->getMessage(),
                );
            }
            
            $success = $success && ($res['status'] === 'success');
            $message_list[] = $res['message'];
            $data[] = $res;
        }
        
        return array(
            'success' => $success,
            'message' => implode('\n', $message_list),
            'data' => $data,
        );
    }

    public function isZeroCharge($order_id)
    {
        //criteria taken from capture()
        return $this->get_net_total($order_id)  <= .05;
    }

    /**
     * @param $order_id
     * @return array
     *   - lockerID
     *   - locationID
     *   - locationType_id
     */
    protected function getOrderLastLocation($order_id)
    {
        // Search for the last completed status for the order_id
        $q = "SELECT l.lockerID, lo.locationID, lo.locationType_id
                FROM orderStatus os
                INNER JOIN locker l ON os.locker_id = l.lockerID
                INNER JOIN location lo ON l.location_id = lo.locationID
                WHERE order_id = ?
                AND os.orderStatusOption_id = ?
                ORDER BY os.date DESC";

        $lastLocation = $this->db->query($q, array($order_id, 10))->row();

        return array(
            'lockerID' => $lastLocation->lockerID,
            'locationID' => $lastLocation->locationID,
            'locationType_id' => $lastLocation->locationType_id
        );
    }

    /**
     * Creates an OrderHomeDelivery for orders that are for a delivery route
     *
     * @param stdClass $order
     * @param int $location_id
     * @return \App\Libraries\DroplockerObjects\DroplockerObject
     */
    public function setupOrderHomeDeliveryFromSubscription($order, $location_id)
    {
        $this->load->model('delivery_customersubscription_model');

        $customerSubscriptions = $this->delivery_customersubscription_model->get_subscriptions($order->customer_id);
        if (!$customerSubscriptions) {
            // Notify customer service, this order is in a delivery route but customer lacks subscription
            $customer = new Customer($order->customer_id);
            send_admin_notification(
                "Can't schedule delivery for order [{$order->orderID}] in delivery route location",
                    "Order {$order->orderID} is in a delivery route location but can't be scheduled because the customer is missing the subscription to a delivery route.\nCustomer: ".  getPersonName($customer)." $customer->customerID\n",
                $order->business_id,
                $order->customer_id
            );

            return false;
        }

        // every customer will have only one subscription
        $customerSubscription = current($customerSubscriptions);

        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($order->business_id);

        $homeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $order->orderID,
        ));

        // Delivery already scheduled, delete and re-schedyle
        if ($homeDelivery && $homeDelivery->delivery_id) {
            $homeDeliveryService->cancelDelivery($homeDelivery);

            $homeDelivery->delete();
            $homeDelivery = null;
        }

        // If no home delivery, create a new one
        if (!$homeDelivery) {
            $homeDelivery = new OrderHomeDelivery();
        }

        $homeDelivery->business_id = $order->business_id;
        $homeDelivery->customer_id = $order->customer_id;
        $homeDelivery->order_id    = $order->orderID;
        $homeDelivery->location_id = $location_id;
        $homeDelivery->state       = $customerSubscription->state;
        $homeDelivery->city        = $customerSubscription->city;
        $homeDelivery->address1    = $customerSubscription->address1;
        $homeDelivery->address2    = $customerSubscription->address2;
        $homeDelivery->zip         = $customerSubscription->zip;
        $homeDelivery->lat         = $customerSubscription->lat;
        $homeDelivery->lon         = $customerSubscription->lon;
        $homeDelivery->service     = get_business_meta($order->business_id, 'home_delivery_service');
        $homeDelivery->sortOrder   = $customerSubscription->sortOrder;
        $homeDelivery->notes       = $customerSubscription->notes;

        $homeDelivery->save();

        $firstDeliveryWindow = current($customerSubscription->windows);
        $deliveryWindow = $homeDeliveryService->getDeliveryWindowByID($homeDelivery, $firstDeliveryWindow->delivery_windowID);

        //$windows_by_date = $homeDeliveryService->getDeliveryWindows($homeDelivery);
        if (empty($deliveryWindow)) {
            //Notify customer service, no suitable window found
            $customer = new Customer($order->customer_id);
            send_admin_notification(
                "Can't schedule delivery for order [{$order->orderID}] in delivery route location",
                    "Order {$order->orderID} is in a delivery route location but can't be scheduled because we failed to get a delivery window.\nCustomer: ".getPersonName($customer)." $customer->customerID\n",
                $order->business_id,
                $order->customer_id
            );

            return false;
        }

        $homeDelivery->delivery_window_id = $firstDeliveryWindow->delivery_windowID;
        $homeDelivery->deliveryDate       = gmdate('Y-m-d', strtotime($deliveryWindow->starts_at));
        $homeDelivery->windowStart        = gmdate('Y-m-d H:i:s', strtotime($deliveryWindow->starts_at));
        $homeDelivery->windowEnd          = gmdate('Y-m-d H:i:s', strtotime($deliveryWindow->ends_at));
        $homeDelivery->save();

        $homeDeliveryService->scheduleHomeDelivery($homeDelivery);

        return $homeDelivery;
    }

    public function count_order_items($order_id, $with_bar_code = true)
    {
        $q = "select count(*) as qty from orderItem oi ";
        if ($with_bar_code) {
            $q .= "join barcode b on b.item_id = oi.item_id ";
        }
        $q .= "where oi.order_id = ?";
        $items = $this->db->query($q, array($order_id))->result();

        return empty($items)?0:$items[0]->qty;
    }

    public function getNotInAutomatItems($order_id, $business_id)
    {
        $sql = "select a.item_id from
                    (select oi.item_id, b.barcode from orderItem oi join barcode b on b.item_id = oi.item_id where oi.order_id = ?
                    and b.business_id = ?) as a
                    LEFT JOIN
                    (select b.item_id, b.barcode from automat a join barcode b on b.barcode = a.garcode where a.order_id = ?
                    and b.business_id = ?) as b
                    ON b.barcode = a.barcode
                    WHERE b.item_id IS NULL AND b.barcode IS NULL";
        $items = $this->db->query($sql, array($order_id, $business_id, $order_id, $business_id))->result();
        if (empty($items)) {
            return false;
        }

        $item_id = $items[0]->item_id;

        if (empty($item_id)) {
            return false;
        }

        $sql = "SELECT oi.*, b.barcode from orderItem oi JOIN barcode b on b.item_id = oi.item_id WHERE oi.item_id = ? AND oi.order_id = ?";
        $items = $this->db->query($sql, array($item_id, $order_id))->result();

        if (empty($items)) {
            return false;
        }

        return $items[0];
    }
    
    public function addNote($order_id, $note)
    {
        $order = new Order($order_id, false);
        $order->notes .= "\n$note";
        $order->save();
    }


    public function create_quick_drop($customer_id, $business_id, $location_id, $employee_id, $quick_drop_notes = '', $due_date = null)
    {
        $orderRes = $this->create_order_for_customer($customer_id, $business_id, $location_id, $employee_id, $due_date);
        if ($orderRes['success']) {
            $order_id = $orderRes['order_id'];
        } else {
            return $orderRes;
        }
        
        $res = $this->do_quick_drop($order_id, $quick_drop_notes);
        if ($res['success']) {
            $res['message'] = $orderRes['message'] . "\n " . $res['message'];
        }
        
        return $res;
    }
    
    /*
     * Deprecated bad requirements
     */
    public function do_quick_drop($order_id, $quick_drop_notes = '')
    {
        $success = false;
        $message = '';
        
        try {
            $items = $this->getItems($order_id);
            
            if (count($items)) {
                throw new Exception("Quick drop can not be set for order $order_id, since it already contains ".count($items)." items");
            }
            
            $order = new Order($order_id);
            $order->notes = $order->notes . "\nQuick drop notes:\n" . $quick_drop_notes;
            $order->save();
            
            $success = true;
            $message = "Quick drop data set for order $order_id.";
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
        
        return array(
            'success' => $success,
            'message' => $message,
            'order_id' => $order_id,
        );
    }
    
    /*
     * Creates a new order for the customer, and assigns a new bag to it
     */
    public function create_order_for_customer($customer_id, $business_id, $location_id, $employee_id, $due_date = null, $notes = "", $source = null, $check_for_bag_before_create = null)
    {
        $this->load->model('customer_model');
        $customerRes = $this->customer_model->getCustomerForBusiness($customer_id, $business_id);

        if ($customerRes['success']) {
            $customer = $customerRes['data'];
        } else {
            return $customerRes;
        }
        
        $this->load->model('bag_model');
        $bagNumber = null;
        if (!empty($check_for_bag_before_create)) {
            $bag = Bag::search_aprax(array(
                'customer_id'=>$customer_id,
                'business_id'=>$business_id
            ));
            $bagNumber = empty($bag[0]->bagNumber)?null:$bag[0]->bagNumber;
        }

        if (empty($bagNumber)) {    
            $bagNumber = $this->bag_model->newBagForCustomer($customer_id, $business_id);
            $message = "New bag $bagNumber created for customer {$customer->firstName} {$customer->lastName}. ";
        }

        $result = $this->newOrderFromBag(ltrim($bagNumber, "0"), $location_id, $business_id, $employee_id, $due_date, $notes, $source);
        $result['customer'] = $customer;
        if ($result['success']) {
            $result['message'] = $message . "\n" . $result['message'];
        }
        
        return $result;
    }
    
    /**
      * Non "web page only" version, let the controller decide what to do
      *
      */
    public function newOrderFromBag($bagNumber, $location_id, $business_id, $employee_id, $due_date = null, $notes = "", $source = null)
    {
        // get inProcess locker from location
        $locker_id = 0;
        $locker = Locker::search(array(
            'lockerName' => 'inProcess',
            'location_id' => $location_id,
        ));

        if (!$locker) {
            $locker = new Locker();
            $locker->lockerName = 'inProcess';
            $locker->location_id = $location_id;
            $locker->lockerStatus_id = 1; //In Service
            $locker->lockerStyle_id = 7; //In Process
            $locker->lockerLockType_id = 2; //Electronic
            $locker->save();
        }
        $locker_id = $locker->lockerID;
        
        // check max bag number
        $maxBagNumber = Bag::get_max_bagNumber();
        if ($bagNumber > $maxBagNumber) {
            $message = "You have entered a bag that is greater then the highest printed bag. "
                . "This will cause duplicate bag issues in the future. "
                . "The highest bag number printed is [".$maxBagNumber."]. "
                . "To solve this problem, print out a new bag number.";
            
            return array(
                'success' => false,
                'error_code' => Order::CREATE_ERROR_BAG_NUMBER_MAX,
                'message' => $message,
                'redirect' => '/admin/orders/bag_tags',
            );
        }

        // check bag number for business
        $bag = Bag::search(array(
            'business_id' => $business_id,
            'bagNumber' => $bagNumber
        ));

        if (empty($bag)) {
            return array(
                'success' => false,
                'error_code' => Order::CREATE_ERROR_BAG_NUMBER_NOT_FOUND,
                'message' => "Bag '$bagNumber' not found for business"
            );
        }

        // check customer on hold
        $customer_id = $bag->customer_id;
        $customer = new Customer($bag->customer_id);

        if ($customer->hold) {
            $message = "Can not create order because there is a hold on {$customer->firstName} {$customer->lastName}'s account";
            
            return array(
                'success' => false,
                'error_code' => Order::CREATE_ERROR_CUSTOMER_ON_HOLD,
                'message' => $message,
                'redirect' => "/admin/customers/detail/{$customer->customerID}",
            );
        }

        // check open orders for bag
        
        $openOrders = $this->getOpenOrders($bagNumber, $business_id);
        if ($openOrders) {
            $message = 'There is already an open order for this bag. Order ' . $openOrders[0]->orderID;
            
            return array(
                'success' => false,
                'error_code' => Order::CREATE_ERROR_BAG_NUMBER_OPEN_ORDER,
                'message' => $message,
                'redirect' => '/admin/orders/details/'.$openOrders[0]->orderID,
            );
        }

        // search existent claim on locker
        $options = array();
        $options['customer_id'] = $customer_id;

        $claims = Claim::search_aprax(array(
            "business_id" => $business_id,
            "customer_id" => $customer_id,
            "locker_id" => $locker_id,
            "active" => 1
        ));

        if ($claims) {
            $claim = $claims[0];
            $options['claimID'] = $claim->claimID;
        }

        // check for multiple claims
        if (count($claims) > 1) {
            $location = new Location($location_id);

            $error_message = "Multiple claims were found for customer '{$customer->firstName} {$customer->lastName} ({$customer->customerID})' "
            . "in locker '{$locker->lockerName}' "
            . "at location '{$location->address}'. "
            . "Only one claim should exist.";
            
            send_admin_notification("Customer has multiple claims error", $error_message);
            
            return array(
                'success' => false,
                'error_code' => Order::CREATE_ERROR_MULTIPLE_CLAIMS_ON_LOCKER,
                'message' => $error_message
            );
        }
        
        $options['due_date'] = $due_date;
        $options['source'] = $source;

        // finally, create the order
        //note: on original code, employee_id got from get_employee_id($this->session->userdata('buser_id'));
        $order_id = $this->new_order(
            $location_id,
            $locker_id,
            $bagNumber,
            $employee_id,
            $notes,
            $business_id,
            $options
        );
        
        $message = "New order $order_id created for bag $bagNumber. ";
        return array(
            'success' => true,
            'order_id' => $order_id,
            'message' => $message,
        );
    }
    
    public function getOrderDetails($business, $employee, $order_id = null)
    {
        $business_id = $business->businessID;
        
        $message = '';
        $success = false;
        
        $dateFormat = get_business_meta($business_id, "shortWDDateLocaleFormat", '%a %m/%d');
        if (!is_numeric($order_id)) {
            $message = "'order_id' must be numeric ($order_id)";
            return array(
                'success' => $success,
                'message' => $message,
            );
        }

        $this->load->model("order_model");
        if ($this->does_exist($order_id) === false) {
            $message = "Order '{$order_id}' does not exist";
            return array(
                'success' => $success,
                'message' => $message,
            );
        }

        $order = $this->get_by_primary_key($order_id);
        $order->orderType = $this->getOrderType($order->orderID);
        
        $this->load->model("orderstatusoption_model");
        $orderStatus = $this->orderstatusoption_model->get_by_primary_key($order->orderStatusOption_id);
        $order->orderStatusOption_label = get_translation($orderStatus->name, "OrderStatusOptions", array(), $orderStatus->name, $order->customer_id);
        
        $content['order'] = $order;

        if ($order->business_id != $business_id) {
            if ($employee->developer_mode) {
                $this->load->model("business_model");
                $otherBusiness = $this->business_model->get_by_primary_key($order->business_id);
                $message = "Order '{$order->orderID}' belongs to  '$otherBusiness->companyName' ($otherBusiness->businessID)'";
            } else {
                $message = "Order $order_id does not belong to your business (#$business_id).";
            }

            return array(
                'success' => $success,
                'message' => $message,
            );
        }

        // get bag info
        $this->load->model("bag_model");
        $content['bag'] = $this->bag_model->get_by_primary_key($order->bag_id);
        
        //get order items/products
        $this->load->model("orderitem_model");

        $orderItems = $this->db->query("SELECT orderItem.product_process_id, orderItem.notes, orderItemID, orderItem.order_id,
                unitPrice, product.name AS displayName, orderItem.item_id, orderItem.supplier_id, product.unitOfMeasurement,
                productCategory.slug as module, module.slug module_slug, module.name module_name, 
                orderItem.qty, (
                  SELECT barcode
                  FROM barcode
                  WHERE item_id = orderItem.item_id
                  AND business_id = ?
                  ORDER BY barcodeID DESC
                  LIMIT 1
                ) AS barcode
            FROM orderItem
            LEFT JOIN product_process ON product_processID = product_process_id
            LEFT JOIN product ON (productID = product_id)
            LEFT JOIN productCategory ON (productCategoryID = productCategory_id)
            LEFT JOIN module on moduleID = productCategory.module_id

            WHERE order_id = ?", array($business_id, $content['order']->orderID))->result();
        
        $this->load->model('product_model');
        foreach ($orderItems as $key => $orderItem) {
            $orderItems[$key]->productType = $this->product_model->getProductType($orderItem, $business_id);
        }

        $content['items'] = $orderItems;
        $content['item_count'] = count($orderItems);
        
        $get_qty_amount_result = $this->db->query("SELECT SUM(qty) AS qty FROM orderItem WHERE order_id=?", array($content['order']->orderID))->result();
        $content['qty'] = $get_qty_amount_result[0]->qty;
        

        //get order status history
        $this->load->model('orderstatus_model');

        $content['orderStatus_history'] = $this->get_history($order->orderID);

        // get the totals for the order
        $total = $this->orderitem_model->get_full_total($order_id);
        $content['money']['order_total'] = isset($total['total'])?$total['total']:0;
        $content['money']['order_taxes'] = $this->get_order_taxes($order_id);

        $this->load->model('orderCharge_model');
        $this->orderCharge_model->order_id = $order_id;
        $content['money']['charges'] = $charges = $this->orderCharge_model->get();
        $ctotal = 0;
        foreach ($charges as $c) {
            $ctotal = $ctotal+$c->chargeAmount;
        }
        $content['money']['charges_total'] = $ctotal;

        $content['money']['gross_total'] = $this->get_gross_total($order_id);
        $content['money']['net_total'] = $this->get_net_total($order_id);
        $content['money']['subtotal'] = $this->get_subtotal($order_id);
        
        $content['breakout_vat'] = !!$this->is_breakout_vat($order_id);

        $content['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
        $content['orderStatusOption_id'] = $order->orderStatusOption_id;
        $content['order_id'] = $order_id;

        // check if customer defaultWF is active
        $this->load->model('product_process_model');
        $this->load->model('product_model');
        if ($this->product_process_model->is_active($customer->defaultWF)) {
            $content['default_wash_fold_product_process_id'] = $customer->defaultWF;
        } else {
            $content['default_wash_fold_product_process_id'] = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $business_id);
        }

        //get wf Log details for this order
        $getWfLog = "select * from wfLog where order_id = $order_id";
        $query = $this->db->query($getWfLog);
        $content['wfLog'] = $query->result();
        $content['notify_status_emails'] = $this->getNotifyOrderStatuses($business_id);

        // We check if this is the blank_customer for the current business
        $content['is_blank_customer'] = $business->blank_customer_id == $order->customer_id;
        $content['related_claims'] = array(); // claims from the same location
        if ($content['is_blank_customer']) {
            $locker = $this->locker_model->get_by_primary_key($order->locker_id);
            $content['related_claims'] = $this->db->query(
                'SELECT claimID, customerID, firstName, lastName
                FROM claim
                JOIN locker ON locker_id = lockerID
                JOIN customer ON customer_id = customerID
                WHERE claim.active = 1 AND locker.location_id = ?',
                    $locker->location_id
            )->result();
        }

        if (!empty($customer)) {
            $this->load->model('customerdiscount_model');
            $content['activeCoupons'] = $this->customerdiscount_model->get_aprax(array(
                'customer_id' => $customer->customerID,
                'active' => 1,
            ));
        }

        $content['print_day_tags']        = get_business_meta($business_id, 'print_day_tags');
        $content['pay_with_cash']         = get_business_meta($business_id, 'pay_with_cash');
        $content['show_manual_discounts'] = get_business_meta($business_id, 'show_manual_discounts', in_bizzie_mode());
        
        $content['paid'] = $order->orderPaymentStatusOption_id == 3;
        
        // logic to decide if there is a note
        $content['show_notes_warning'] = false;
        if (!empty($_GET['new_order'])) {

            //$content['customer'] not set
            // if customer has internalNotes, show the warning
            if (!empty($content['customer']) && $content['customer']->internalNotes) {
                $content['show_notes_warning'] = true;
            } else {
                // search for notes to show
                $lines = explode("\n", $order->notes);
                foreach ($lines as $line) {
                    $line = trim($line);
                    $fields = explode(":", $line, 2);

                    // ignore empty lines
                    if (empty($line)) {
                        continue;
                    }

                    // this is not a field, so show notes and stop searching
                    if (count($fields) < 2) {
                        $content['show_notes_warning'] = true;
                        break;
                    }

                    // ignore empty notes, move to next line
                    if (empty($fields[1])) {
                        continue;
                    }

                    //if "ORDER TYPE: SMS", ignore this line
                    if (trim($fields[1]) == "SMS") {
                        continue;
                    }

                    // we have a note, stop here
                    $content['show_notes_warning'] = true;
                    break;
                }
            }
        }

        return array(
            'success' => true,
            'data' => $content,
        );
    }
    
    //returns the order statuses for email notifications
    public function getNotifyOrderStatuses($business_id)
    {
        $email_order_statuses = $this->db->query("select orderStatusOption_id, emailAction_id, emailTemplate.smsText, emailText
                                                    from emailTemplate
                                                    join emailActions on emailAction_id = emailActionID
                                                    where business_id = $business_id
                                                    and orderStatusOption_id != 0 and (emailTemplate.smsText !='' or emailText !='') ");

        $email_statuses = $email_order_statuses->result();

        if (!empty($email_statuses)) {
            $status_array = array();

            foreach ($email_statuses as $email_status) {
                $status_array[] =  $email_status->orderStatusOption_id;
            }

            return $status_array;
        } else {
            return array();
        }
    }

    /**
     * adds an item to an order (originaly from the "Add New Item" form on order_detail)
     *
     *
     * $_POST values
     * ------------------------
     * order_id int required
     * qty int required
     * item_id int (Optional)
     * notes string optional
     * supplier_id int optional
     * product_process_id int required
     *
     */
    public function add_new_item($order_id, $item_id, $business_id, $employee_id)
    {
        if (!is_numeric($item_id)) {
            throw new \InvalidArgumentException("'item_id' must be numeric.");
        }
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric.");
        }
        $item = new Item($item_id, false);

        $this->load->model("barcode_model");
        $barcode = $this->barcode_model->get_one(array(
            "item_id" => $item->itemID,
        ));

        $existing_items = OrderItem::search_aprax(array(
            "item_id" => $item_id,
            "order_id" => $order_id
        ));
        if (!empty($existing_items)) {
            return array(
                'success' => false,
                'error_code' => 'item_already_in_order',
                'message' => "Item Barcode '{$barcode->barcode}' already exists in order '$order_id'",
            );
        }

        $order = new Order($order_id, false);
        $customer = new Customer($order->customer_id);

        try {
            $product_process = new Product_Process($item->product_process_id);
        } catch (NotFound_Exception $e) {
            $product_process = null;
        }

        if (empty($item->product_process_id) || is_null($product_process) || $product_process->active == 0) {
            return array(
                'success' => false,
                'error_code' => 'item_product_inactive',
                'message' => "Inactive Product Error",
            );
        } else {
            $product_process_id = $item->product_process_id;

            $this->load->model('orderitem_model');

            //remove any one time specifications
            $this->load->model('itemspecification_model');
            $item_specifications = $this->itemspecification_model->remove_oneTimeSpecification($item_id);

            //remove any one time upcharges
            $this->load->model('itemupcharge_model');
            $upchargesRemoved = $this->itemupcharge_model->remove_oneTimeUpcharges($item_id);

            try {
                $product = new Product($product_process->product_id, false);
            } catch (NotFound_Exception $e) {
                $product = null;
            }

            // For business with breakout vat enabled, the tax is included in the item price.
            // So, if the customer is non-taxable, we have to remove the tax from the item price.
            $unitPrice = $product_process->price;
            if (!$customer->taxable) {
                $breakout_vat = get_business_meta($order->business_id, 'breakout_vat');
                if ($breakout_vat) {
                    $this->load->model('taxgroup_model');
                    $taxRate = $this->taxgroup_model->get_tax_rate($order_id, $product_process->taxGroup_id);
                    $unitPrice = $unitPrice / (1 + $taxRate);
                }
            }
            //get the issues for this item
            $this->load->model('itemissue_model');
            $issues = $this->itemissue_model->get_aprax(array(
                'item_id' => $item->itemID,
            ), 'updated');

            //begin add issues to assembly notes
            if ($order->postAssembly != '') {
                $assemblyNotes = $order->postAssembly.' ';
            } else {
                $assemblyNotes = '';
            }

            $regex = "/".$barcode->barcode."\[(.*?)\]/i";
            if (!preg_match($regex, $assemblyNotes)) {
                $newNotes = '';
                foreach ($issues as $issue) {
                    if (trim($issue->note)!='') {
                        $newNotes .= $issue->note.' ';
                    }
                }
                if ($newNotes!='') {
                    $assemblyNotes .= $barcode->barcode.'['.trim($newNotes).'] ';
                }
            }

            $where['orderID'] = $order_id;
            $options['postAssembly'] = trim($assemblyNotes);
            
            $this->update($options, $where);

            $res = $this->orderitem_model->add_item(
                $order_id,           // $order_id
                $item_id,            // $item_id
                $product_process_id, // $product_process_id
                1,                   // $qty
                $employee_id,  // $employee_id
                '',                  // $supplier_id
                $unitPrice           // $price
            );

            if ($res['status']=='success') {
                // Need to retake picture?
                $show_retake_picture_msg = $this->orderitem_model->retake_picture($item_id, $business_id);
                $retake_picture_msg = "";
                if ($show_retake_picture_msg) {
                    $retake_picture_msg = get_translation("retake_picture_of_item", "(Please, retake picture of this item.)", array(), "(Please, retake picture of this item.)");
                }
                
                //is this a wash fold item
                $productCategory = new ProductCategory($product->productCategory_id);
                if ($productCategory->slug == WASH_AND_FOLD_CATEGORY_SLUG) {
                    $this->load->library('wash_fold_product');
                    $result = $this->wash_fold_product->add_wash_fold_preferences($res['orderItemID']);
                }

                if (empty($barcode)) {
                    return array(
                        'success' => true,
                        'message' => "Added Item $item_id to Order $order_id",
                    );
                } else {
                    return array(
                        'success' => true,
                        'message' => "Added Barcode '$barcode->barcode' to Order $order_id. ".$retake_picture_msg,
                    );
                }
                redirect("/admin/orders/item_details/order/".$order_id."/item_id/".$item->itemID);
            } else {
                return array(
                    'success' => false,
                    'error_code' => $res['error_id'],
                    'message' => $res['error'],
                );
            }
        }
    }

    public function add_barcode_to_order_or_create($options)
    {
        $success = false;
        $message = "";
        $error_code = '';
            
        $order_id = $options['order_id'];
        $barcode = $options['barcode'];
        $business_id = $options['business_id'];
        $employee_id = $options['employee_id'];
        $notes = $options['notes'];
        $source = $options['source'];
        
        if (!$order_id) {
            $customer_id = $options['customer_id'];
            $location_id = $options['location_id'];
            $due_date = $options['due_date'];
            
            $orderRes = $this->create_order_for_customer($customer_id, $business_id, $location_id, $employee_id, $due_date, $notes, $source);
            if ($orderRes['success']) {
                $order_id = $orderRes['order_id'];
                $message .= $orderRes['message'];
            } else {
                return $orderRes;
            }
        }
            
        $this->load->model('barcode_model');
        $items = $this->barcode_model->getBarcode($barcode, $business_id);

        $order = new Order($order_id);

        // redo orders have a blank customer id so when adding items the barcode will belong
        // to a different customer. We need to have a different process for adding items to redo orders
        if ($order->llNotes == REDO_ORDER) {
            $order->addRedoItem($items[0], $business_id);
            $success = true;
            $message .= "New item added to redo order $order_id. ";
        } else {
            if ($items) {
                $found = false;
                foreach ($items as $item) {
                    if (($item->customerID == $order->customer_id) || ($item->customer_id==0 && $order->customer_id == 0)) {
                        $found = true;

                        //The following conditional displayed the edit item interface if the barcode already exists in the system.
                        $res = $this->add_new_item($order->orderID, $item->itemID, $business_id, $employee_id);
                        $success = $res['success'];
                        $message .= $res['message'];
                        $error_code = $res['error_code'];

                        break;
                    }
                }

                if (!$found) {
                    $message .= "Barcode $barcode not associated to current customer. Please check customer or reassign the item to the current customer. ";
                    $error_code = "barcode_not_for_customer";

                    //If we can't find an item that is already associated with the customer, we render the interface for reassigning the item.
                    //redirect("/admin/orders/item_error/order/".$order->orderID."/barcode/".$barcode."/error/customer_mismatch/item/".$items[0]->customerID."/order_customer/". $order->customer_id);
                }
            } else {
                $message .= "Barcode $barcode not associated to any item for this business. Please check business or create an item for the barcode. ";
                $error_code = "barcode_do_not_exist";
                //redirect("/admin/orders/create_item/order/$order_id/barcode/$barcode");
            }
        }
        
        return array(
            'success' => $success,
            'message' => $message,
            'error_code' => $error_code,
            'order_id' => $order_id,
        );
    }
    
    public function add_product_to_order_or_create($options)
    {
        $success = false;
        $message = '';

        $order_id = $options['order_id'];
        $customer_id = $options['customer_id'];
        $location_id = $options['location_id'];
        
        $qty = $options['qty'];
        $notes = $options['notes'];
        $supplier_id = $options['supplier_id'];
        $item_id = $options['item_id'];
        $product_id = $options['product_id'];
        $process_id = $options['process_id'];
        $upcharge_ids = $options['upcharge'];
        $color = $options['color'];
        $brand = $options['manufacturer'];
        $specials = $options['specificationsLabels'];
        $due_date = $options['due_date'];
        $notes = $options['notes'];
        $source = $options['source'];
        $price = empty($options['price'])?null:$options['price'];
        $check_for_bag_before_create = empty($options['check_for_bag_before_create'])?null:$options['check_for_bag_before_create'];

        $business_id = $options['business_id'];
        $employee_id = $options['employee_id'];
        
        if (!$order_id) {
            $orderRes = $this->create_order_for_customer($customer_id, $business_id, $location_id, $employee_id, $due_date, $notes, $source, $check_for_bag_before_create);
            if ($orderRes['success']) {
                $order_id = $orderRes['order_id'];
                $message = $orderRes['message'];
            } else {
                return $orderRes;
            }
        }
            
        $this->load->model('itemissue_model');
        $issuesRes = $this->itemissue_model->checkItemIssues($item_id, $order_id, $product_id, $process_id);
        if (!$issuesRes['success']) {
            return $issuesRes;
        }

        //get the product_processID
        $this->load->model("product_process_model");
        $product_process = $this->product_process_model->get_one(array(
            'product_id' => $product_id,
            'process_id' => $process_id,
        ));

        $unitPrice = empty($price)?$product_process->price:$price;

        // get supplier_id
        if (empty($supplier_id)) {
            $this->load->model('product_supplier_model');
            $supplier = $this->product_supplier_model->getDefault($product_process->product_processID, $order_id);
            if (isset($supplier->supplier_id)) {
                $supplier_id = $supplier->supplier_id;
            } else {
                $supplier_id = $business_id;
            }
        }

        //get upcharges cost
        if (empty($upcharge_ids)) {
            $upchargeCost = 0;
        } else {
            $order = new Order($order_id);
            $customer = new Customer($order->customer_id);
            $upchargeCostData = $this->processUpcharges($unitPrice, $customer, $supplier_id, $business_id, $upcharge_ids);

            $upchargeCost = $upchargeCostData['upchargeCost'];
            $notes .= $upchargeCostData['notes'];
        }

        $notes .= $this->getDescriptionForNote(array(
            'color' => $color,
            'brand' => $brand,
            'specials' => $specials,
        ));
        
        // add order item
        $this->load->model('orderitem_model');
        $res = $this->orderitem_model->add_item(
            $order_id,
            $item_id,
            $product_process->product_processID,
            $qty,
            $employee_id,
            $supplier_id,
            $unitPrice,
            $notes,
            $autoOverwrite = 0,
            $upchargeCost
        );

        if ($res['status']=='success') {
            $success = true;

            $this->load->library('wash_fold_product');
            $this->wash_fold_product->storeWFPReferencesAndLog($product_process, $res['orderItemID'], $order_id, $qty);
            $sub_message = $this->getTaxableWarning($product_process, $order_id);

            $item = new Item($item_id, true);
            if (empty($item->relationships['Barcode'][0]->barcode)) {
                $message .=  " Added Item {$res['orderItemID']} to Order $order_id. $sub_message";
            } else {
                $message .=  " Added Barcode '{$item->relationships['Barcode'][0]->barcode}' to Order $order_id. $sub_message";
            }
        } else {
            $message = $res['error'];
        }
        
        return array(
            'success' => $success,
            'message' => $message,
            'order_id' => $order_id,
        );
    }
    
    public function processUpcharges($unitPrice, $customer, $supplier_id, $business_id, $upcharge_ids)
    {
        $upchargeCost = 0;
        $notes = '<br />';
        
        $this->db->where_in('upchargeID', $upcharge_ids);
        $upcharges = $this->db->get("upcharge")->result();

        $basePrice = $unitPrice;

        foreach ($upcharges as $upcharge) {
            //if this customer doesn't pay for upcharges make the price 0;
            if ($customer->noUpcharge == 0) {
                if ($upcharge->priceType == "dollars") {
                    $upchargeAmt = $upcharge->upchargePrice;
                    $unitPrice = $upcharge->upchargePrice + $unitPrice;
                } elseif ($upcharge->priceType == "percent") {
                    $upchargeAmt = number_format((($upcharge->upchargePrice / 100) * $basePrice), 2, '.', '');
                    $unitPrice = $unitPrice + $upchargeAmt;
                }
            } else {
                $upchargeAmt = 0;
            }

            $notes .= $upcharge->name . ' @ ' . format_money_symbol($business_id, '%.2n', $upchargeAmt) . '<br>';
            // get the upcharge costs
            $sql = "SELECT cost FROM upchargeSupplier WHERE upcharge_id = {$upcharge->upchargeID} and supplier_id = {$supplier_id};";
            $row = $this->db->query($sql)->row();
            if (isset($row->cost)) {
                $upchargeCost = $upchargeCost + $row->cost;
            }
        }
        
        return array(
            'notes' => $notes,
            'upchargeCost' => $upchargeCost,
        );
    }
    
    public function getDescriptionForNote($items, $listGlue = ', ', $lineGlue = '<br />')
    {
        $description = '';
        
        foreach ($items as $name => $values) {
            $values = (array) $values;
            if (count($values)) {
                $description .= "$name: " . implode($listGlue, $values) . $lineGlue;
            }
        }
        
        if ($description) {
            $description = $lineGlue . $description;
        }
        
        return $description;
    }
    
    public function getTaxableWarning($product_process, $order_id)
    {
        $warning_message = '';
        if ($product_process->taxable) {
            $order = new Order($order_id);
            $locker = new Locker($order->locker_id);
            $location = new Location($locker->location_id);
            if (empty($location->state)) {
                switch ($location->country) {
                    case 'usa': $field = 'state'; break;
                    case 'australia': $field = 'state'; break;
                    case 'canada': $field = 'province'; break;
                    default: $field = 'locality'; break;
                }
                $warning_message = "Warning: You added a taxable item but this location does not have a $field so we could not calculate the tax.";
            }
        }

        return $warning_message;
    }

    public function getCustomerOrdersInProgress($business, $employee, $customer_id)
    {
        $statuses = array(
            'exclude' => true,
            9, //Ready for Customer Pickup
            10, //Completed
//            3, //inventoried
        );
        
        return $this->getCustomerOrdersByStatus($business, $employee, $customer_id, $statuses);
    }
    
    public function getCustomerOrdersCompleted($business, $employee, $customer_id, $limit = 20)
    {
        $statuses = array(
            10, //Completed
        );
        
        return $this->getCustomerOrdersByStatus($business, $employee, $customer_id, $statuses, $limit);
    }
    
    public function getCustomerOrdersReady($business, $employee, $customer_id)
    {
        $statuses = array(
            9, //Ready for Customer Pickup
        );
        
        return $this->getCustomerOrdersByStatus($business, $employee, $customer_id, $statuses);
    }
    
    public function getCustomerOrdersByStatus($business, $employee, $customer_id, $statuses = array(), $limit = null, $includeNoBagOrders = false)
    {
        if (empty($customer_id)) {
            throw new Exception("Missing customer_id");
        }

        if (empty($business->businessID)) {
            throw new Exception("Missing business_id");
        }
        $business_id = $business->businessID;
        
        
        $statusFilter = '';
        if ($statuses) {
            $exclude = '';
            if ($statuses['exclude']) {
                $exclude = 'NOT';
            }
            unset($statuses['exclude']);
            
            $statusList = implode(', ', $statuses);
            $statusFilter = " AND orderStatusOption_id $exclude in ($statusList) ";
        }

        $skipNoBag = $includeNoBagOrders ? "" : "AND bag_id != 0";

        $sql = "SELECT orderID
                FROM orders 
                WHERE orders.locker_id != 0 
                AND orders.customer_id = ? 
                AND orders.business_id = ? 
                $statusFilter
                $skipNoBag
                ORDER BY orders.dateCreated DESC";

        if (!is_null($limit)) {
            if (is_numeric($limit)) {
                $sql .= " LIMIT $limit";
            } else {
                throw new \InvalidArgumentException("'limit' must be numeric");
            }
        }
        $orders = array();
        $result = $this->db->query($sql, array($customer_id, $business_id))->result();

        foreach ($result as $orderData) {
            $res = $this->getOrderDetails($business, $employee, $orderData->orderID);
            if ($res['success']) {
                $orders[$res['data']['order_id']] = $res['data'];
            }
        }
        
        return $orders;
    }

    /**
     * Unapplies all discounts/charges from an order, removes all items from an order, and changes the order pickup status to 'pickedup'.
     *
     * refactored from order admin controller
     */
    public function gut_order($order_id, $employee_id)
    {
        $success = false;
        $message = '';
        
        try {
            $this->load->model('order_model');
            $orders = $this->order_model->get(array("orderID" => $order_id));
            $order = $orders[0];
            $this->load->model('orderStatus_model');
            $this->load->library("discounts");

            if (!is_numeric($order_id)) {
                throw new Exception("The order ID must be numeric.");
            }
            $this->discounts->unapply_discounts($order);
            $this->order_model->remove_all_items($order_id);
            $this->orderStatus_model->update_orderStatus(array(
                'order_id' => $order_id,
                "orderStatusOption_id" => PICKED_UP_STATUS,
                "locker_id" => $order->locker_id,
                'employee_id' => $employee_id,
            ));

            $this->order_model->update_order_taxes($order_id);
            
            $success = true;
            $message = "Successfully gutted order $order_id";
        } catch (Exception $e) {
            $message = "Could not gut order $order_id: {$e->getMessage()})";
            send_exception_report($e);
        }

        //the order also needs to be emptied from the wf log
        $queryDeleteLog = "DELETE FROM wfLog where order_id = ?";
        $query = $this->db->query($queryDeleteLog, array($order_id));

        return array(
            'success' => $success,
            'message' => $message,
            'order_id' => $order_id,
        );
    }
    
    /*
     * Creates a new item, assigns a barcode, assigns to customer, asigns to order (optional)
     * refactored from orders controller
     */
    public function createItem($request, $employee_id, $business_id, $add_to_order = true)
    {
        // nasty, but validations work only with $_POST
        $post_backup = $_POST;
        $_POST = $request;
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules("barcode", "Barcode", "integer");//not required
        $this->form_validation->set_rules("process_id", "Process", "integer|does_process_exist");
        $this->form_validation->set_rules("product_id", "Product", "required|integer|does_product_exist");
        $this->form_validation->set_rules("customer_id", "Customer", "integer");
        
        $valid = $this->form_validation->run();
        $errors = validation_errors();
        
        $_POST = $post_backup;
        
        if (!$valid) {
            return array("status" => "error", "message" => $errors);
        }

        $process_id = $request["process_id"];
        $product_id = $request["product_id"];

        $customer_id = $request["customer_id"];
        $order_id = $request["order_id"];
        $barcode = $request["barcode"]; // OPTIONAL

        $options['color'] = $request['color'];
        $options['manufacturer'] = $request['manufacturer'];
        //$options['starchOnShirts_id'] = $request['starch'];
        $options['note'] = $request['notes'];
        $options['business_id'] = $business_id;

        $upcharges = $request["upcharge"];
        $specifications = $request["specifications"];

        //get the product_processID
        $this->load->model("product_process_model");
        $product_process = $this->product_process_model->get_one(array(
            'product_id' => $product_id,
            'process_id' => $process_id
        ));

        if (empty($product_process)) {
            return array("status" => "error", "message" => "Could not find product process for product ID '$product_id' and process ID '$process_id'");
        }

        $options['product_process_id'] = $product_process->product_processID;

        $this->load->model('item_model');
        $item_id = $this->item_model->save($options);


        if ($barcode) {
            //check to see that the barcode does not exist
            $this->load->model('barcode_model');

            //insert into barcode
            $existing_barcodes_count = $this->barcode_model->count(array(
                "barcode" => $barcode,
                "business_id" => $business_id
            ));

            if ($existing_barcodes_count > 0) {
                return array("status" => "error", "message" => "Barcode '$barcode' already exists");
            }

            $this->barcode_model->save(array(
                'item_id' => $item_id,
                'barcode' => $barcode,
                'business_id' => $business_id,
            ));
            
            $barcode_label = "Barcode '$barcode'";
        } else {
            $barcode_label = 'no barcode asigned';
        }

        //insert into customer_item
        $this->load->model('customer_item_model');
        $this->customer_item_model->save(array(
            'customer_id' => $customer_id,
            'item_id' => $item_id,
        ));

        //insert into upcharges, if any
        if ($upcharges) {
            $this->load->model('itemupcharge_model');
            $this->itemupcharge_model->update($item_id, $upcharges, $employee_id);
        }

        //insert into specifications, if any
        if ($specifications) {
            $this->load->model('itemspecification_model');
            $this->itemspecification_model->update($item_id, $specifications);
        }

        //insert into itemhistory
        $this->load->model('itemhistory_model');

        $this->itemhistory_model->save(array(
            'item_id' => $item_id,
            'description' => 'Created Item',
            'creator' => $employee_id,
        ));




        if ($add_to_order && $order_id) {
            $notes = $request['notes'] . "\n";
            $this->load->model('orderitem_model');

            $qty = 1;
            if ($request["qty"]) {
                $qty = $request["qty"];
            }
            
            $result = $this->orderitem_model->add_item(
                $order_id,                           // $order_id
                $item_id,                            // $item_id
                $product_process->product_processID, // $product_process_id
                $qty,                                   // $qty
                $employee_id,                  // $employee_id
                '',                                  // $supplier_id
                $product_process->price,             // $price
                $notes,                               // $notes
                0,                                  //$autoOverwrite
                0,                                  //$upchargeCost
                true                               //$allow_no_barcode
            );

            $message = "Created item #$item_id with $barcode_label and Added it to Order '$order_id'";
        } else {
            $message = "Created item #$item_id with $barcode_label";
        }

        return array(
            'status' => 'success',
            'message' => $message,
        );
    }

    /**
     * Used for the coeigniter form validator rule callback to assert that the specified product process exists.
     * refactored from orders controller
     * @param int $product_process_id
     * @return boolean
     */
    public function does_process_exist($process_id)
    {
        $this->load->model("process_model");
        //Note, a processs is optional. No process is sometimes passed incorrectly as '0' by * code.
        if ($process_id == 0 || $this->process_model->does_exist($process_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_process_exist", "Product process ID '$process_id' does not exist");

            return false;
        }
    }
    /**
     * Used for the coeigniter form validator rule callback to assert that the specified product process exists.
     * refactored from orders controller
     * @param int $product_process_id
     * @return boolean
     */
    public function does_product_exist($product_id)
    {
        $this->load->model("product_model");
        if ($this->product_model->does_exist($product_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_product_exist", "Product ID '$product_id' does not exist");

            return false;
        }
    }


    public function getBookedHomeDeliveries($business_id, $firstName = false, $lastName = false, $address = false)
    {
        $params = array();

        $sql = "select 
                    ohd.*, l.*, c.firstName, c.lastName, c.email, c.address1, c.address2, c.customerNotes 
                from orderHomeDelivery ohd
                join customer c on c.customerID = ohd.customer_id
                join location l on ohd.location_id = l.locationID
                where ohd.business_id = ? AND trackingCode IS NOT NULL";
        
        $params[] = $business_id;

        if ($firstName) {
            $sql .= " AND c.firstName LIKE ?";
            $params[] = $firstName;
        }

        if ($lastName) {
            $sql .= " AND c.lastName LIKE ?";
            $params[] = $lastName;
        }

        if ($address) {
            $sql .= " AND c.address1 LIKE ?";
            $params[] = $address;
        }

        $sql .= " ORDER BY orderHomeDeliveryID DESC LIMIT 0, 500";

        $items = $this->db->query($sql, $params)->result();

        if (empty($items)) {
            return false;
        }

        return $items;
    }
}
