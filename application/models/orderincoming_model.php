<?php

class OrderIncoming_Model extends My_Model
{
    protected $tableName = 'orderIncoming';
    protected $primary_key = 'orderIncomingID';


        function get_incoming_order($itemID)
        {
            if (empty($itemID)) {
                throw new Exception("You must pass a value for 'orderIncomingID'");
            }
            $this->db->select("*");
            $this->db->where(array("itemID" => $itemID));
            $get_incoming_order_query = $this->db->get("orderIncoming");
            $get_incoming_order_result = $get_incoming_order_query->result();

            return $get_incoming_order_result[0];
        }
        function insert($customer_id, $content, $itemId, $fromAddr)
        {
            if (!is_numeric($customer_id)) {
                throw new Exception("'customer_id' must be numeric.");
            }
            if (empty($content)) {
                throw new Exception("'content' can not be empty");
            }
            if (!is_numeric($itemId)) {
                throw new Exception("'item_id' must be numeric.");
            }
            if (!is_numeric($fromAddr)) {
                throw new Exception("'fromAddr' must be numeric.");
            }

        }
}
