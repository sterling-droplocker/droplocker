<?php

class InvoiceCustomFieldValue_Model extends My_Model
{
    protected $tableName = 'invoiceCustomFieldValue';
    protected $primary_key = 'invoiceCustomFieldValueID';

    public function getByLanguage($invoiceCustomField_id, $business_language_id)
    {
        $this->db->where(compact("invoiceCustomField_id", "business_language_id"));
        $this->db->order_by("sortOrder", "ASC");
        $options = $this->db->get("invoiceCustomFieldValue")->result();
        return $options;
    }

    public function updateDefault($business_id, $field_id, $value_id, $default)
    {
        if (!$default) {
            return true;
        }

        $this->db->set('default', 0);
        $this->db->where(array(
            'business_id' => $business_id,
            'customOption_id' => $option_id,
            'default' => 1,
            'customOptionValueID != ' => $value_id
        ));
        $this->db->update($this->tableName);
    }
}