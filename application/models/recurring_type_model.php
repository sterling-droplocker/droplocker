<?php

class Recurring_Type_Model extends My_Model
{
    protected $tableName = 'recurring_type';
    protected $primary_key = 'recurring_typeID';

    public function get_recurring_types_as_codeigniter_dropdown()
    {
        $result = array();
        foreach ($this->get_aprax() as $recurring_type) {
            $result[$recurring_type->recurring_typeID] = $recurring_type->name;
        }

        return $result;
    }
}
