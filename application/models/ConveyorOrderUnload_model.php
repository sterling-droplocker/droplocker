<?php

class ConveyorOrderUnload_Model extends My_Model
{
    protected $tableName = 'conveyorOrderUnload';
    protected $primary_key = 'conveyorOrderUnloadID';

    public function set_unload($params)
    {
        if (count($params) == 0) {
            throw new Exception("'Parameters must not be empty.");
        }

        $result = $this->save($params);

        return $result;
    }

}
