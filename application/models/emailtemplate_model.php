<?php

/**
 * Note, an email_template_id of 0 indicates that the business has not associated that template with any business language.
 */
class Emailtemplate_Model extends My_Model
{
    protected $tableName = 'emailTemplate';
    protected $primary_key = 'emailTemplateID';

    /**
     * Retrieves all the templates of a an email action for specified business cateogiries by business's available language.
     * @param int $emailAction_id
     * @param int $business_id
     * @return array Returns an a nested associative array in the following format
     *  {
    *       businessLanguageID1 : The email template for this language for this email action
     *          {
*                   emailSubject
*                   emailTExt,
*                   bodyText,
*                   smsSubject,
*                   smsText,
*                   adminSubject
*                   adminText
     *          },
     *      businessLanguageID2
     *          {
     *              ...
     *          },
         *      ...
     *   }
     * @throws \InvalidArgumentException
     *
     * Note, if an email template has no defined business language, it is set to index zero of the result data structure.
     */
    public function get_templates_by_business_language_for_action_for_business($emailAction_id, $business_id)
    {
        if (!is_numeric($emailAction_id)) {
            throw new \InvalidArgumentException("'emailAction_id' must be numeric");
        } elseif (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        } else {
            $this->load->model("business_language_model");
            $business_languages = $this->business_language_model->get_aprax(array("business_id" => $business_id));
            $emailTemplates_by_business_language_for_action_for_business = array();
            foreach ($business_languages as $business_language) {
                $emailTemplate = $this->get_one(array("emailAction_id" => $emailAction_id, "business_language_id" => $business_language->business_languageID));
                if (empty($emailTemplate)) {
                    continue;
                } else {
                    $emailTemplates_by_business_language_for_action_for_business[$business_language->business_languageID] = array("emailSubject" => $emailTemplate->emailSubject,
                    "emailText" => $emailTemplate->emailText, "bodyText" => $emailTemplate->bodyText, "smsSubject" => $emailTemplate->smsSubject, "smsText" => $emailTemplate->smsText,
                    "adminSubject" => $emailTemplate->adminSubject, "adminText" => $emailTemplate->adminText, "emailTemplateID" => $emailTemplate->emailTemplateID);
                }
            }
            //The following statements retrieve an email tempalte that has no defined business language.
            $emailTemplate = $this->get_one(array("emailAction_id" => $emailAction_id, "business_id" => $business_id, "business_language_id" => 0));
            if (!empty($emailTemplate)) {
                $emailTemplates_by_business_language_for_action_for_business[0] = array("emailSubject" => $emailTemplate->emailSubject,
                    "emailText" => $emailTemplate->emailText, "bodyText" => $emailTemplate->bodyText, "smsSubject" => $emailTemplate->smsSubject, "smsText" => $emailTemplate->smsText,
                    "adminSubject" => $emailTemplate->adminSubject, "adminText" => $emailTemplate->adminText, "emailTemplateID" => $emailTemplate->emailTemplateID);
            }

            return $emailTemplates_by_business_language_for_action_for_business;

        }
    }
	
	public function get_template_by_business($action, $business_id)
	{
		$emailAction = \App\Libraries\DroplockerObjects\EmailAction::search_aprax(array("action" => $action));
		
		if (!$emailAction) {		
			return false;
		}

		$emailAction_id = $emailAction[0]->emailActionID;
		
		return $this->get_one(array('emailAction_id' => $emailAction_id, 'business_id' => $business_id));
	}
}
