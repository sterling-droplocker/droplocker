<?php

class DiscountReason_Model extends My_Model
{
    protected $tableName = 'discountReason';
    protected $primary_key = 'discountReasonID';

    /**
     * Retrieves the number of discount reasons for a particular business
     * @param int $business_id
     * @return int
     */
    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $discountReasons_count = $this->db->query("SELECT COUNT(discountReasonID) AS total FROM {$this->tableName} WHERE business_id = ?", array($business_id))->row();

        return $discountReasons_count->total;
    }
}
