<?php

class LocationType_Model extends My_Model
{
    protected $tableName = 'locationType';
    protected $primary_key = 'locationTypeID';

    public function simple_array()
    {
        $array = array();
        foreach ($this->get_aprax() as $row) {
            $array[$row->locationTypeID] = $row->name;
        }

        return $array;
    }


}
