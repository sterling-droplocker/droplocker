<?php
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Bag;
class DroidPostPickups_Model extends My_Model
{
    protected $tableName = 'droid_postPickups';
    protected $primary_key = 'ID';

    /**
     * inserts a postPickup from the droid to the database
     *
     * Required Options Values
     * ----------------------
     * transaction_id
     * bagNumber
     * location_id
     * lockerName
     * employee_id
     * business_id
     *
     * @param array $options
     * @throws Exception for missing required option values
     * @return int insert_id
     */
    public function insert($options = array())
    {
        $required = array('transaction_id',
                'bagNumber',
                'location_id',
                'lockerName',
                'employee_id',
                'business_id');

        if (!required($required, $options)) {
            throw new Exception("Missing required option values. <br>Required: <br>". print_r($required, 1)."<br>Sent: <br>".print_r($options, 1));
        }

        // rename FRONT to Front_Counter
        $options['lockerName'] = ($options['lockerName'] == 'FRONT')?'Front_Counter':$options['lockerName'];

        // check to see if the transaction exists... if it do, then just return that id
        if (!$id = $this->get($options)) {
            $this->db->insert('droid_postPickups', $options);
            $id = $this->db->insert_id();
        }

        return $id;
    }


    /**
     *
     * Updates a row in the  table
     *
     * @param array $options
     * @param array $where
     * @throws Exception for missing where statement
     * @return int affected_rows
     */
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where statement");
        }

        $this->db->update('droid_postPickups', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * gets data from the table
     *
     * $options values
     * ----------------
     * transaction_id
     * status
     *
     * @param array $options
     * @return array of objects
     */
    public function get($options = array())
    {
        if (isset($options['ID'])) {
            $this->db->where('ID',$options['ID']);
        }
        if (isset($options['transaction_id'])) {
            $this->db->where('transaction_id',$options['transaction_id']);
        }
        if (isset($options['status'])) {
            $this->db->where('status',$options['status']);
        }
        if (isset($options['version'])) {
            $this->db->where('version',$options['version']);
        }
        if (isset($options['claim_id'])) {
            $this->db->where('claim_id',$options['claim_id']);
        }

        $query = $this->db->get('droid_postPickups');

        return $query->result();
    }

    /**
     * Deletes from the database
     *
     * @param array $options
     * @return int affected_rows()
     */
    public function delete($options = array())
    {
        $query = $this->db->delete('droid_postPickups', $options);

        return $this->db->affected_rows();
    }


    /**
     * Creates an order from the droid_postPickup table
     *
     * @param object $pickup
     * @throws Exception
     * @return boolean
     */
    public function processPickup($pickup)
    {
        $base_url = get_admin_url();

        $this->load->model('order_model');
        $this->load->model("droidpostpickups_model");
        try {

            if (empty($pickup)) {
                return true;
            }

            $originalLockerName = urldecode($pickup->lockerName);
            $originalBagNumber = $pickup->bagNumber;
            $where = array('ID'=>$pickup->ID); // used for updating the droid_postPickups table
            $defaultOptions = array('status'=>'complete');


            $maxBagNumber = Bag::get_max_bagNumber();
            if ((int) ltrim($pickup->bagNumber, '0') > $maxBagNumber) {
                $statusNotes[] = "Pickup Bagnumber [{$pickup->bagNumber}] is higher then the last bag number [{$maxBagNumber}]";
                $options = $defaultOptions;
                $options['methodCalled'] = __METHOD__;

                $message = "The bag for this pickup is higher then than last bag number that was printed. This is most likely due to a scanner error from the phone. Please check this claim<br><br>".formatArray($pickup);
                $email_id = send_admin_notification("Pickup Bagnumber [{$pickup->bagNumber}] is higher then the last bag number [{$maxBagNumber}]", $message, $pickup->business_id);
                $statusNotes[] = "email_id:".$email_id;
                $options['statusNotes'] = serialize($statusNotes);
                $this->update($options,$where);

                return true;
            }
            $originalLockerName = utf8_decode( $originalLockerName );
            $locker = Locker::search(array(
                'lockerName' => $originalLockerName,
                'location_id' => $pickup->location_id,
            ));

            if (empty($locker->lockerID) && $pickup->lockerName != '0') {
                $locker = Locker::search(array(
                    'lockerName' => 'Home Delivery',
                    'location_id' => $pickup->location_id,
                ));
                
                if (empty($locker->lockerID)) {                
                    $statusNotes[] = "Could Not Find Locker in Location";
                    $options = $defaultOptions;
                    $options['statusNotes'] = serialize($statusNotes);
                    $options['methodCalled'] = __METHOD__;
                    $this->droidpostpickups_model->update($options,$where);

                    $body = "Could not find locker for pickup bag number: ".$pickup->bagNumber."<br>";
                    $body .= "---------------------------<br>";
                    $body .= "<br>LockerName: ".urldecode($pickup->lockerName);
                    $body .= "<br>Location: <a href=\"$base_url/admin/orders/view_location_orders/{$pickup->location_id}\">".$pickup->location_id . "</a>";
                    $body .= "<br>bagNumber: ".$pickup->bagNumber;
                    $body .= "<br>Driver: ".$pickup->employee_id;
                    $body .= "<br>Time: ".$pickup->deliveryTime;
                    send_admin_notification("Could not find locker for pickup bag number: ".$pickup->bagNumber, $body, $pickup->business_id);

                    return true;
                }
            }

            /**
             * Check if there are any inProcess lockers in that location.
             *
             * If not notify by email and stop processing.
             */
            try {
                $hasInProcessLocker = Locker::checkForInProcessLocker($pickup->location_id);
            } catch (Exception $e) {
                $hasInProcessLocker = true; //more than 1 inProcess locker or does not have electronic lockers
            }

            if (!$hasInProcessLocker) {
                $this->load->model('location_model');
                $location_text = '';
                try {
                    $location = new Location($pickup->location_id);
                    $location_text = $location->address;
                } catch (Exception $e) {
                    $location_text = "Location not found [{$pickup->location_id}]";
                }

                $body = "Could not find inProcess locker for location: $location_text locationID:[{$pickup->location_id}]<br>";
                $body .= "<BR><BR>DUMP: ".print_r($pickup,1);
                send_admin_notification("Could not find inProcess locker for location : ".$location_text, $body, $pickup->business_id);

                return true;
            }

            /**
             * Temp bags will have a 0 on the barcode.
             * We need to strip it and check to see if the number
             * has been used before
             *
             * if it has, we notify support to check.
             *
             * This is just a notification, continue processing.
             * The pickup->bagNumber will not have a leading 0 after this
             */
            if (substr($pickup->bagNumber, 0, 1) == '0') {
                $tempTag = 1;
                $pickup->bagNumber = ltrim($pickup->bagNumber, "0");

                //see if this bag has been used before

                $pickupBag = Bag::search(array("bagNumber"=>$pickup->bagNumber));
                if ($pickupBag->bagID) {
                    $bodyText = 'Bag '.$pickup->bagNumber.' has a temp tag on it.  This temp tag has been used before.  Please make sure this is not a mistake';
                    $bodyText .= "<BR><BR>DUMP: ".print_r($pickup,1);
                    //removed because too many messages were being sent.
                    //$email_id = send_admin_notification('Temp Tag Reused ['.$pickup->bagNumber.']', $bodyText, $bag->business_id);

                }

                // we will log this
                $statusNotes[] = 'Bag '.$pickup->bagNumber.' has a temp tag on it.';

            } else {
                $pickupBag = Bag::search(array("bagNumber"=>$pickup->bagNumber));
            }


            // check to see if this bagnumber is in an open order
            if ($openOrder = $this->order_model->checkOpenOrder($pickup->bagNumber, $pickup->business_id)) {

                $options = $defaultOptions;

                $options['methodCalled'] = __METHOD__;
                $options['order_id'] = $openOrder;

                $email_id = send_admin_notification("There is already an open order [".$openOrder."] for this bag", "There is already an open order for this bag ".$pickup->bagNumber."<br><pre>".print_r($pickup,1)."</pre>", $pickup->business_id);
                $options['statusNotes'] = serialize(array("There is already an open order [".$openOrder."] for this bag", "email_id:".$email_id));
                $this->update($options,$where);

                return true; //move onto the next pickup
            }


            if ($pickup->lockerName == "0") {
                //try to find the last locker this bag was in.
                $sql = "select
                locker.*,
                orders.*
                FROM orders
                JOIN bag on bagID = orders.bag_id
                JOIN locker on lockerID = orders.locker_id
                WHERE bagNumber = $pickup->bagNumber
                AND bag.business_id = {$pickup->business_id}
                ORDER BY orders.dateCreated desc
                limit 1;";
                $query = $this->db->query($sql);
                $lastOrder = $query->row();
                if ($lastOrder->location_id != $pickup->location_id) {
                    try {
                        $location = new Location($pickup->location_id);
                    } catch (Exception $e) {
                        $statusNotes[] = "LocationID[".$pickup->location_id."] Not found";
                    }

                    $testlocationID = $lastOrder->locationID;
                    if (empty($testlocationID)) {
                        $bodyText = 'Locker number 0 was entered and this bag has no previous orders.<br> Location: '.$location->address.' ['.$pickup->location_id.']. <br> Please create this order manually.';
                    } else {
                        $bodyText = 'Locker number 0 was entered but the prior order for this bag was at a different location than it was just scanned into ('.$location->address.' ['.$pickup->location_id.']).  Please create this order manually.';
                    }
                   $email_id = send_admin_notification('Bag ['.$pickup->bagNumber.'] needs to be entered manually', $bodyText, $pickup->business_id);

                    // we will log this
                    $statusNotes[] = 'Locker number 0 was entered but the prior order for this bag was at a different location than it was just scanned into, OR the bag is new';
                    $options = $defaultOptions;
                    $options['statusNotes'] = serialize($statusNotes);
                    $options['methodCalled'] = __METHOD__;
                    $options['order_id'] = $lastOrder->orderID;
                    $this->update($options,$where);

                    return true;
                } else {
                    $pickup->lockerName = $lastOrder->lockerName;
                    $statusNotes[] = "Set pickup lockerName to last Order LockerName";
                    // update the lockername
                    $locker = Locker::search(array('lockerName'=>$pickup->lockerName, "location_id"=>$pickup->location_id));
                }

            } //end locker == 0

            // look to see if there is an order in this locker
            // only look in locations of type "lockers" from the locationType table
            //TODO write a test for this scenario
            $sql = "SELECT * FROM orders
            INNER JOIN locker ON lockerID = orders.locker_id
            INNER JOIN location ON locationID = locker.location_id
            INNER JOIN locationType ON locationTypeID = location.locationType_id
            WHERE locker_id = {$locker->lockerID}
            AND lockerType = 'lockers'
            AND orderStatusOption_id != 10
            AND orders.business_id = {$pickup->business_id}";
            $query = $this->db->query($sql);
            if ($orders = $query->result()) {

                $updatedOrders = array();
                foreach ($orders as $order) {

                    //order ready for pickup, set to completed and allow new order to be created
                    if ($order->orderStatusOption_id == 9) {
                        $testOrderID = $order->orderID;
                        if (empty($testOrderID)) {
                            throw new Exception("Order ID is empty. Processed orders: ".print_r($order, 1).". SQL:  ".$sql);
                        }

                        //$order->orderStatusOption_id = 10;
                        $options = array();
                        $options['order_id'] = $order->orderID;
                        $options['orderStatusOption_id'] = 10;
                        $options['locker_id'] = $locker->lockerID;
                        $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
                        $options['business_id'] = $order->business_id;
                        $options['employee_id'] = $pickup->employee_id;
                        $this->order_model->update_order($options);

                    } else {

                        //order (bag) already in DB, don't create new order
                        if (isset($order) &&  ($order->bag_id == $pickupBag->bagID)) {
                            $statusNotes[] = "order ready for pickup, set to finished and allow new order to be created [$order->orderID]";
                        } else {
                            $statusNotes[] = "Non-completed order already in locker. Order[".$order->orderID."]";

                            $message = "Non-completed order already in locker[".$order->locker_id."]";
                            $message .= "Location:".$order->address."";
                            $message .= "Order:".$order->orderID."";

                            $email_id = send_admin_notification("Non-completed order already in locker. Order[".$order->orderID."]", $message, $order->business_id);
                            $statusNotes[] = "email_id:".$email_id;
                        }


                        /*
                         $options = $defaultOptions;
                        $options['statusNotes'] = serialize($statusNotes);
                        $options['methodCalled'] = __METHOD__;
                        $options['order_id'] = $order->orderID;
                        $this->update($options,$where);

                        return true;
                        */
                    }

                    if (isset($order)) {
                        $updatedOrders[] = $order->orderID;
                    }
                }
            }

            // format the driver notes properly
            $notes = trim($pickup->notes);
            if ($notes !='') {

                // if the beginning of the note is 1, add a break
                if (substr($notes, 0, 1) == "1") {

                    $noteArray = explode(" ", $notes);
                    $notes = $noteArray[0]."\n";
                    if ($noteArray[1]!='') {
                        $notes .= "DRIVER NOTE: ";
                        for ($i=1; $i<=sizeof($noteArray)-1; $i++) {
                            $notes .= $noteArray[$i]." ";
                        }
                    }
                } else {
                    if (!empty($notes)) {
                        $notes = "DRIVER NOTE: ".$notes;
                    }
                }
            }

            // Create the new order
            $options['deltime'] = $pickup->deliveryTime;
            $testClaimID = $pickup->claim_id;
            if (!empty($testClaimID)) {
                $options['claimID'] = $pickup->claim_id;
            }
            $newOrderID = $this->order_model->new_order($pickup->location_id,
                    $locker->lockerID,
                    $pickup->bagNumber,
                    $pickup->employee_id,
                    $notes,
                    $pickup->business_id,
                    $options);

            //if the notes are set, email operations.
            if (trim($pickup->notes) <> "") {
                $body = "NOTE: ". urldecode($pickup->notes)."<br>---------------------------------<br>";

                //grab all the bits of the object and make them pretty
                $pickup_members = get_object_vars($pickup);
                $pickup_details = '';
                foreach ($pickup_members as $member_key => $pickup_member_value) {
                    $pickup_details .= $member_key . ': ' . $pickup_member_value . "<br />";
                }
                $body .= "Additional Info:<br>" . $pickup_details;
                send_admin_notification('Driver entered notes for order ['.$newOrderID.']', $body, $pickup->business_id);
            }

            if (!$newOrderID) {
                $message = "Order::new_order did not return an orderID. An Error has occured in the new_order method<BR><BR>DUMP: ".print_r($pickup,1);
                $email_id = send_admin_notification("Order::new_order did not return an orderID. An Error has occured in the new_order method", $message, $pickup->business_id);

                $statusNotes[] = "Order::new_order did not return an orderID. An Error has occured in the new_order method";
                $statusNotes[] = "email_id:".$email_id;

                $options = $defaultOptions;
                $options['statusNotes'] = serialize($statusNotes);
                $options['methodCalled'] = __METHOD__;
                //$options['order_id'] = $newOrderID;

                $this->update($options,$where);

                return true;
            }

            $statusNotes[] = "Order Created [$newOrderID]";
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;
            $options['order_id'] = $newOrderID;
            $this->update($options,$where);

            //Update the order notes with the driver notes (if any), claim notes, and customer notes
            //$this->order_model->updateOrderNotes($order_id, $pickup->claim_id);

            $this->load->model('driver_log_model');
            $options = array();
            $options['order_id'] = $newOrderID;
            $options['employee_id'] = $pickup->employee_id;
            $options['delTime'] = $pickup->deliveryTime;
            $options['delType'] = 'pickup';
            $options['locker_id'] = $locker->lockerID;
            $options['bagNumber'] = $pickup->bagNumber;
            $options['location_id'] = $pickup->location_id;
            $options['business_id'] = $pickup->business_id;
            $this->driver_log_model->insertTransaction($options);

            return $newOrderID;

        } catch (Exception $e) {
            send_exception_report($e);
        }
    }

}
