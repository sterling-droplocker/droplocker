<?php

class TransbankOrder_Model extends My_Model
{
    protected $tableName = 'transbankOrders'; // TABLE IS PLURAL
    protected $primary_key = 'transbankOrderID';



    public function getByOrderId($order_id)
    {
        return $this->db->get_where($this->tableName, array('order_id' => $order_id));
    }

    public function add($order_id, $code)
    {
        $tbOrder = $this->getByOrderId($order_id);

        if ($tbOrder->num_rows) {
            $this->db->where('order_id', $order_id);
            $result = $this->db->update($this->tableName, array('code' => $code));
        } else {
            $result = $this->db->insert($this->tableName, array('order_id' => $order_id, 'code' => $code));
        }

        return $result;
    }
}
