<?php

class UpchargeGroup_model extends MY_Model
{
    protected $tableName = 'upchargeGroup';
    protected $primary_key = 'upchargeGroupID';

    /**
     *
     * @param int $business_id
     * @return array A list of upcharge groups for the specified business in a codeigniter dropdown menu format.
     * @throws \InvalidArgumentException
     * @throws \App\Libraries\DroplockerObjects\NotFound_Exception
     */
    public function get_as_codeigniter_dropdown($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }

        $get_upchargeGroups_query = $this->db->get_where($this->tableName, array("business_id" => $business_id));
        $result = $get_upchargeGroups_query->result();

        $upchargeGroups = array();
        foreach ($result as $upchargeGroup) {
            $upchargeGroups[$upchargeGroup->upchargeGroupID] = $upchargeGroup->name;
        }

        return $upchargeGroups;
    }
}
