<?php

class Smssettings_model extends MY_Model
{
    protected $tableName = 'smsSettings';
    protected $primary_key = 'business_smsID'; // USES WEIRD PK

    public function get_settings($business_id)
    {
        return $this->get_one(array(
            'business_id' => $business_id
        ));
    }

    public function update_settings($sms_data)
    {
        $sms_update = array();

        $this->db->replace('smsSettings', $sms_data);
        $success = $this->db->affected_rows();

        return $success;
    }


}
