<?php

class EmailAction_Model extends MY_Model
{
    protected $tableName = 'emailActions';
    protected $primary_key = 'emailActionID';

    public function does_emailAction_name_exist($name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException("'name' can not be empty");
        } else {
            $this->db->get_where($this->tableName, array("name" => $name));
            if ($this->db->count_all_results() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Get email actions and translations for a business in a given language
     *
     * @param int $business_id
     * @param int $business_language_id
     * @param bool $homeDelivery
     * @throws \InvalidArgumentException
     * @return array
     */
    public function getBusinessActions($business_id, $business_language_id, $homeDelivery = false)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'$business_id' must be numeric.");
        } elseif (!is_numeric($business_language_id)) {
            throw new \InvalidArgumentException("'business_language_id' must be numeric");
        }
        $this->load->model("business_model");
        $business = $this->business_model->get_by_primary_key($business_id);

        if (empty($business)) {
             throw new \InvalidArgumentException("Business ID '$business_id' does not exist");
        }

        $sql = "SELECT emailTemplateID, emailActionID, emailActions.body as defaultEmail,
                    emailActions.description, emailActions.adminBody as defaultAdmin,
                    emailActions.smsText as defaultText, emailActions.name, emailActions.orderStatusOption_id,
                    emailTemplate.emailText as customEmail, emailTemplate.smsText as customText,
                    emailTemplate.adminText as customAdmin, business_language_id,
                    orderStatusOption.name as statusName, action, description
                FROM emailActions
                LEFT JOIN orderStatusOption ON (orderStatusOptionID = orderStatusOption_id)
                LEFT JOIN emailTemplate ON (emailTemplate.emailAction_id = emailActionID
                            AND emailTemplate.business_id = ? AND business_language_id = ?)
                WHERE isHomeDelivery = ?
           ";

        $get_emailActions_result =  $this->db->query($sql, array($business_id, $business_language_id, $homeDelivery ? 1 : 0))->result();
        $emailActions = array();

        foreach ($get_emailActions_result as $emailAction) {
            $emailActions[$emailAction->emailActionID] = array(
                'emailTemplateID' => $emailAction->emailTemplateID,
                'name' => $emailAction->name,
                'status' => $emailAction->statusName,
                'sms_found' => $this->_found($emailAction->customText, $emailAction->defaultText),
                'email_found' => $this->_found($emailAction->customEmail, $emailAction->defaultEmail),
                'action' => $emailAction->action,
                'description' => $emailAction->description,
            );

            //The following hackery searches for any admin alert text among any language that is defined, because admin alerts are not intended to be intyernationalziated but are part of the email template model.

            $get_emailTemplate_for_defined_adminAlert = "SELECT emailTemplate.adminText FROM emailTemplate INNER JOIN emailActions ON emailActionID = emailAction_id WHERE emailTemplate.emailAction_id = ? AND emailTemplate.business_id = ? AND adminText IS NOT NULL AND adminText != ''";
            $adminAlert_emailTemplates = $this->db->query($get_emailTemplate_for_defined_adminAlert, array($emailAction->emailActionID, $business_id))->result();
            if (empty($adminAlert_emailTemplates)) {
                $emailActions[$emailAction->emailActionID]['admin_found'] = "";
            } else {
                $emailActions[$emailAction->emailActionID]['admin_found'] = "F";
            }
        }

        return $emailActions;
    }

    /**
     * Unknown * function
     */
    public function _found($custom = '', $default = '')
    {
        $flag = '';
        if (!empty($custom)) {
            $flag = "C";
        } elseif (!empty($default)) {
            $flag = "D";
        }

        return $flag;
    }
}
