<?php

use App\Libraries\DroplockerObjects\Location;

/**
 *
 * Location Model
 *
 *
 */
class Location_Model extends MY_Model
{
    protected $tableName = 'location';
    protected $primary_key = 'locationID';
    private $reporting_database = null;
    
    const LOCATION_CLASS__HOME_DELIVERY = 1;
    const LOCATION_CLASS__LOCKERS = 2;
    const LOCATION_CLASS__CONCIERGE = 3;

    /**
     * Retrieves locations with at least one locker that is not have the status 'removed'
     * @param array $where_parameters
     * @return array
     */
    public function get_with_lockers_and_locationType($where_parameters)
    {
        //FIXME: hardcoded locationType name, used only in API
        $this->db->select("location.locationID AS location_id, location.business_id, location.companyName, location.address2, location.city, location.state, location.zipcode, location.lat, location.lon AS lng, location.public, location.serviceType, locationType.name AS locationType, location.address, location.hours, location.location ");
        $this->db->join("locker", "location_id=locationID");
        $this->db->join("locationType", "locationType_id=locationTypeID", "LEFT");
        $this->db->where("locker.lockerStatus_id", 1);
        $this->db->group_by("locationID");
        $locations_with_lockers = $this->db->get_where($this->tableName, $where_parameters)->result();

        return $locations_with_lockers;
    }

    /**
     * Generates an array of all business locations suitable for a dropdown
     * @param int $business_id
     * @return array
     */
    public function get_all_as_codeigniter_dropdown_for_business($business_id)
    {
        $this->db->order_by("address");
        $locations = $this->db->get_where($this->tableName, array("business_id" => $business_id))->result();
        $locations_dropdown = array();
        foreach ($locations as $location) {
            $locations_dropdown["{$location->locationID}"] = $location->address;
        }

        natsort($locations_dropdown);

        return $locations_dropdown;
    }

    /**
     * The following function is  , do not use.
     * @deprecated use get_aprax() in MY_Model instead
     * ----------------------------------
     * BEGIN
     * ----------------------------------

     * gets data from the location table
     *
     * ----------------------------------
     * END
     * ----------------------------------
     */
    public function get($options = array())
    {
        if (isset($options['locationID'])) {
            if(!is_array($options['locationID'])) {
                $this->db->where('locationID', $options['locationID']);
            } else {
                $this->db->where_in('locationID', $options['locationID']);
            }
            unset($options['locationID']);
        }

        $this->db->where('status !=', 'deleted');


        if (isset($options['not_locationType_id'])) {
            $this->db->where_not_in('locationType_id',$options['not_locationType_id']);
            unset($options['not_locationType_id']);
        }

        if (isset($options['where_address_like'])) {
            $this->db->like('address', $options['where_address_like']);
            unset($options['where_address_like']);
        }

        if (isset($options['limit'])) {
            $this->db->limit($options['limit']);
            unset($options['limit']);
        }

        if (isset($options['inService'])) {
            $this->db->where('serviceType !=', 'not in service');
            unset($options['inService']);
        }

        if (isset($options['exclude_serviceTypes'])) {
            $this->db->where_not_in('serviceType', $options['exclude_serviceTypes']);
            unset($options['exclude_serviceTypes']);
        }

        if (isset($options['with_active_locker_count'])) {
            $this->db->join('locker','locker.location_id = locationID');
            $this->db->group_by('locationID');
            $this->db->where('locker.lockerStatus_id', 1);

            if (!isset($options['select'])) {
                $options['select'] = 'location.*, count(lockerID) as active_locker_count';
                if (isset($options['customer_id'])) {
                    $options['select'] .= ', customer.*';
                }

                if (isset($options['with_location_type'])) {
                    $options['select'] .= ', locationType.*';
                }
            }

            unset($options['with_active_locker_count']);
        }

        if (isset($options['customer_id'])) {
            $this->db->join('customer_location', 'customer_location.location_id=location.locationID');
            $this->db->where('customer_id', $options['customer_id']);
            unset($options['customer_id']);
        }

        if (isset($options['with_location_type'])) {
            $this->db->join('locationType','locationType.locationTypeID=location.locationType_id');
            unset($options['with_location_type']);
        }

        if (isset($options['having'])) {
            $this->db->having($options['having']);
            unset($options['having']);
        }

        if (isset($options['select'])) {
            $this->db->select($options['select']);
            unset($options['select']);
        }

        if (isset($options['sort'])) {
            if (is_array($options['sort'])) {
                $this->db->order_by($options['sort'][0], $options['sort'][1]);
            } else {
                $this->db->order_by($options['sort']);
            }

            unset($options['sort']);
        }

        $this->db->where($options);

        $query = $this->db->get('location');

        return $query->result();

    }

    /**
    * gets a total count of active lockers in a location.
    *
    * This must match the criteria at Locker_Model::get_all_useable_for_location()
    *
    * @param int $location_id
    * @return int count of active lockers
    */
    public function getActiveLockerCount($location_id)
    {
        $sql = "SELECT count(lockerID) as total FROM locker WHERE locker.location_id = {$location_id} AND locker.lockerStatus_id = 1
                AND locker.lockerName != 'inProcess' AND locker.lockerStyle_id NOT IN (7,9)";
        $query = $this->db->query($sql);
        $lockers = $query->row();

        return $lockers->total;
    }

    public function get_all_with_business()
    {
        $sql = "SELECT locationID, l.lat, l.lon, business_id, l.companyName, l.address, l.city, l.state, l.country,
                l.hours, l.public, b.companyName AS business, t.name AS type
                FROM location AS l
                JOIN business AS b ON (business_id = businessID)
                JOIN locationType AS t ON (locationType_id = locationTypeID)
                WHERE status = 'active'
                ORDER BY business_id ASC, locationID DESC";
        return $this->db->query($sql)->result();
    }

    /**
    * update function
    *
    * @param array $options
    * @param array $where
    * @return int affected_rows
    */
    public function update($options = array(), $where = array())
    {
        $this->db->update('location', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * delete function
     *
     * @param array $where
     * @return int affected_rows
     */
    public function delete($where = array())
    {
        $this->db->delete('location', $where);

        return $this->db->affected_rows();
    }

    /**
     * The following fucntion is  , do not use.
     * @deprecated use the save() functionality in the Location Droplocker Object instead
     * ----------------------------------
     * BEGIN
     * ----------------------------------
     * Locations are never deleted so we have to check to see if the location has
     * a status of active or delete and handle it
     * ----------------------------------
     * END
     * ----------------------------------
    */
    public function insert($options = array())
    {
        $get['address'] = $options['address'];
        $get['address2'] = $options['address2'];
        $get['city'] = $options['city'];
        $get['state'] = $options['state'];
        $get['business_id'] = $options['business_id'];
        $get['select'] = "locationID, status,address";

        //this location is already in the database
        if ($row = $this->get($get)) {
            if($row[0]->status=='active')

                    return array('status'=>"fail",'message'=>'Location: '.$row[0]->address.' is already in the system and is active');
            else {
                if ($this->update(array('status'=>'active'), array('locationID'=>$row[0]->locationID))) {
                    return array('status'=>"success",'message'=>'Location: '.$row[0]->address.' has been re-activated', 'insert_id'=>$row[0]->locationID);
                }
            }
        }

        //location not is system to do an insert
        $this->db->insert('location', $options);
        if($id = $this->db->insert_id())

            return array('status'=>"success",'message'=>"{$options['address']} added", 'insert_id'=>$id);
        else
            return array('status'=>"fail",'message'=>'Location has been not been added');

    }

    /**
     * Get all routes for a business
     *
     * @param int $business_id
     */
    function get_routes($business_id)
    {
        $sql = "SELECT DISTINCT COALESCE(route, route_id) AS route_id
            FROM location
            LEFT JOIN delivery_zone ON (locationID = delivery_zone.location_id)
            LEFT JOIN delivery_window ON (delivery_zoneID = delivery_zone_id)
            WHERE location.business_id = ?
            ORDER BY route_id
            ";

        $query = $this->db->query($sql, array($business_id));

        return $query->result();
    }

    /**
     * Gets all business's routes in array format for a dropdown
     *
     * @param int $business_id
     * @return array
     */
    public function get_routes_dropdown($business_id)
    {
        $routes = array();
        foreach($this->get_routes($business_id) as $r) {
            $routes[$r->route_id] = $r->route_id;
        }

        return $routes;
    }

    /**
     * Get all master routes for a business
     *
     * @param int $business_id
     */
    public function get_master_routes($business_id)
    {
        $this->db->distinct();
        $this->db->select('masterRoute_id');
        $this->db->where('business_id', $business_id);
        $this->db->order_by('masterRoute_id');
        $query = $this->db->get('location');

        return $query->result();
    }

    /**
     * Gets all business's master routes in array format for a dropdown
     *
     * @param int $business_id
     * @return array
     */
    public function get_master_routes_dropdown($business_id)
    {
        $routes = array();
        foreach($this->get_master_routes($business_id) as $r) {
            $routes[$r->masterRoute_id] = $r->masterRoute_id;
        }

        return $routes;
    }

    /**
     * add_customer_location makes a connection between a customer and a location
     *
     * Option Values
     * ---------------------
     * customer_id
     * location_id
     *
     * @param array $options
     * @return int insert_id
     */
     function add_customer_location($options = array())
     {

         $keys = array();
         $quotes = array();
         $values = array();
         foreach ($options as $key => $value) {
                $keys[] = $key;
                $quotes[] = '?';
                $values[] = $value;
         }

         $keys = implode(', ', $keys);
         $quotes = implode(', ', $quotes);

         $sql = "INSERT IGNORE INTO customer_location ($keys) VALUES ($quotes);";
         $this->db->query($sql, $values);

         return $this->db->insert_id();
     }

    /**
     * delete_customer_location makes a connection between a customer and a location
     *
     * Option Values
     * ---------------------
     * customer_id
     * location_id
     *
     * @param array $options
     * @return int insert_id
     */
    function delete_customer_location($options = array())
    {
        $this->db->delete('customer_location', $options);

        return $this->db->affected_rows();
    }

    /**
     * Gets all locations added to the customer
     *
     * @param int $customer_id
     * @param array $options
     * @return array
     */
    public function get_customer_locations($customer_id, $options = array())
    {

        $this->db->join('customer_location', 'customer_location.location_id = location.locationID');
        $query = $this->db->get_where($this->tableName, $options);

        return $query->result();
    }

    /**
     *
     * @todo move this to locationType_model
     * @param unknown_type $options
     */
    function get_location_type($options = array())
    {
      if (isset($options['locationTypeID'])) {$this->db->where('locationTypeID', $options['locationTypeID']);}
      if (isset($options['name'])) {$this->db->where('name', $options['name']);}
      if (isset($options['abbreviation'])) {$this->db->where('abbreviation', $options['abbreviation']);}
      if (isset($option['select'])) {$this->db->select($options['select']);}

      $this->db->order_by('name');

      $query = $this->db->get('locationType');

      return $query->result();
    }

    /**
     * @deprecated - use Locker object
     * checks to see if a location can handle mutiple orders
     *
     * @param int $locationID
     * @return boolean
     */
    function is_multi_order($locationID)
    {
        $sql = "SELECT lockerType FROM locationType
                INNER JOIN location on locationTypeID = locationType_id
                WHERE locationID = {$locationID}";
        $res = $this->db->query($sql)->row();
        if ($res->lockerType !='lockers') {
            return true;
        }

        return false;
    }

    /**
     * checks to see if all the locations for a business are kiosks. If they are, the checkbox
     * on the customer profile billing page to set kiosk access is hidden and always set
     *
     * loops through all the locations of a business and checks to see if the location type is not kiosk. When found, returns false
     *
     * @param unknown_type $business_id
     * @return boolean
     * @example $content['showKioskCheckbox'] = $this->location_model->isOnlyKiosks($this->business_id);
     */
    function isOnlyKiosks($business_id)
    {
        $this->db->where('business_id', $business_id);
        $query = $this->db->get('location');
        foreach ($query->result() as $location) {
            if ($location->locationType_id != 1) {
                return false;
            }
        }

        return true;
    }

    /**
     * get the public kiosks for mapping
     */
    public function get_public_kiosks($business_id)
    {
        $this->db->where('business_id',$business_id);
        $this->db->join('locationType','locationTypeID=locationType_id');
        $this->db->join('business','businessID=business_id');
        $this->db->where('locationType_id',1);
        $this->db->where('status', 'active');
        $this->db->where('serviceType !=', 'not in service');
        $this->db->where('location.lat IS NOT NULL', NULL);
        $this->db->where('location.lon IS NOT NULL', NULL);
        $this->db->select('business.companyName as businessName, location.companyName,
            location.hours,location.location, locationType.name, locationType_id,location.address,
            location.address2, serviceType,location.city, location.state, location.zipcode,
            location.lat, location.lon, locationID, business_id');
        $q = $this->db->get('location');

        return $q->result();
    }

    /**
     * The following function is unknown *. do not use.
     * @deprecated This * function does not follow coding standards.
     * ************************************************
     * BEGIN
     * ************************************************
        * simply uses a get, but I have to use like so I added a method
     * ************************************************
     * END
     * ************************************************
    */
    public function autocomplete($word, $business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $word = '%'.$word.'%';
        $sql = "SELECT address, address2, locationID, city, state, zipcode, companyName FROM location WHERE CONCAT(address, ' ', address2) like ? AND business_id = ? limit 20";
        $query = $this->db->query($sql, array($word, $business_id));

        return $query->result();
    }

    /**
     * @deprecated This is an unknown * function with unknown behavior. Do not use.
     * gets locations based on a radius set by the business owner
     */
    public function get_locations_distance($latlon, $business_id, $distance)
    {
        $lat1 = $latlon[0];
        $lng1 = $latlon[1];
        $options['business_id'] = $business_id;
        //$options['public'] = 'yes';
        //$options['not_locationType_id'] = array(8);
        $options['status'] = 'active';
        $options['select'] = 'companyName, hours,location,(SELECT name FROM locationType WHERE locationTypeID = locationType_id) as name,locationType_id,address,lat,lon,locationID';
        $locations = $this->get($options);
        //echo $this->db->last_query();
        $a=array();
        $count = 0;
        for ($i=0;$i<sizeof($locations);$i++) {

                if ( distance($lat1,$lng1,$locations[$i]->lat,$locations[$i]->lon,'m')<$distance ) {
                    echo $locations[$i]->address."<br>";
                        $a[$locations[$i]->locationID] = $locations[$i];
                    }
                        if ($count==5) {
                                return $a;
                        }
                        $count++;
                }

        return $a;
    }

    /**
     * Retrieves all the available locations with active lockers and within specified distance of a specified coordinate
     * @param float $latitude_degrees In units of degrees
     * @param float $longitutde_degrees In units of degrees
     * @param float $radius The maximum distance away from the specified coordinate to search for. This isn units of km. By default, the radius is 0.8 km
     * @param int $business_id
     * @param bool $search_public if True, searches only for locations that are 'public', otherwise searches only for locations that are not 'public'.
     * @param int $limit The maximum number of locations of to return. By default, the limit is 20 locations.
     * @return array
     */
    public function get_all_by_distance_for_business($latitude_degrees, $longitude_degrees, $radius, $business_id, $search_public, $limit = 20)
    {
      $R = 6371;  // earth's radius, km

      #########################################
      # BEGIN *
      #########################################

      // first-cut bounding box (in degrees)
      $maximum_latitude = $latitude_degrees + rad2deg($radius/$R);
      $minimum_latitude = $latitude_degrees - rad2deg($radius/$R);
      // compensate for degrees longitude getting smaller with increasing latitude
      $maximum_longitude = $longitude_degrees + rad2deg($radius/$R/cos(deg2rad($latitude_degrees)));
      $minimum_longitude = $longitude_degrees - rad2deg($radius/$R/cos(deg2rad($latitude_degrees)));

      ########################################
      # END *
      ########################################

      //The following statements convert the incoming latitude and longitude into radians.
      $latitude = deg2rad($latitude_degrees);
      $longitude = deg2rad($longitude_degrees);


      $latitude = (float) str_replace(",", ".", $latitude);

      $longitude = (float) str_replace(",", ".", $longitude);

      $maximum_latitude = (float) str_replace(",", ".", $maximum_latitude);

      $minimum_latitude = (float) str_replace(",", ".", $minimum_latitude);

      $maximum_longitude = (float) str_replace(",", ".", $maximum_longitude);

      $minimum_longitude = (float) str_replace(",", ".", $minimum_longitude);

      if ($search_public === 'both') {
          $public = "1=1";
      } elseif ($search_public === true) {
          $public = "public = 'yes'";
      } else {
          $public = "public != 'yes'";
      }

      $exclude = array(8); // undefined
      if (get_business_meta($business_id, "hide_home_delivery_locations")) {
          $exclude[] = 4; // home delivery
      }
      $exclude = implode(", ", $exclude);

      //Unknown * query
      $sql = "
        SELECT locationID, address, lat, lon, address2, zipcode, hours, city, state, name, companyName, public,locationType_id,serviceType,business_id,
               acos(sin(?)*sin(radians(lat)) + cos(?)*cos(radians(lat))*cos(radians(lon)-?))*? As D
        FROM (
          SELECT locationID, address, lat, lon, address2, zipcode, hours, city, public, companyName, state,locationType_id,serviceType,business_id,(SELECT name FROM locationType WHERE locationTypeID = locationType_id) AS name
          FROM location
          INNER JOIN locker ON location.locationID=locker.location_id
          WHERE lat > ? AND lat < ?
            AND lon > ? AND lon < ?
            AND $public
            AND locationType_id NOT IN ($exclude)
            AND business_id = ?
            AND locker.lockerStatus_id = 1
            AND location.status = 'active'
            AND location.serviceType != 'not in service'
          GROUP BY locationID
          ) AS firstCut
        WHERE acos(sin(?)*sin(radians(lat)) + cos(?)*cos(radians(lat))*cos(radians(lon)-?))*? < ?
        ORDER BY D LIMIT ?";

        $get_locations_within_distance_query = $this->db->query($sql, array($latitude, $latitude, $longitude, $R, $minimum_latitude, $maximum_latitude, $minimum_longitude, $maximum_longitude, $business_id, $latitude, $latitude, $longitude, $R, $radius, $limit));
        $points = $get_locations_within_distance_query->result();

        $locations = array();

        for ( $i=0; $i < sizeof($points); $i++) {
            $locations[$points[$i]->locationID] = $points[$i];
        }

        return $locations;
    }

    /**
     * Generates an array with the ID as the key and the street as the value
     *
     * @param array $options
     * @param boolean $blank
     * $blank sets whether to include a "-- SELECT --" as the first in the array. Good for dropdowns where
     * location can by any
     * @return array $array
     */
    public function simple_array($options = array(), $blank='')
    {
        $locations = $this->get($options);
        $array = "";
        if($blank!='')
        $array[] = $blank;
        foreach ($locations as $l) {
                $array[$l->locationID] = $l->address;
        }

        return $array;
    }

    /**
     * returns the number of locations
     *
     * @param array $options
     * @param return int $total;
     */
    public function location_count($options = array())
    {
        if (isset($options['business_id'])) {$this->db->where('business_id', $options['business_id']);}
        if (isset($options['public'])) {$this->db->where('public', $options['public']);}
        $this->db->select("count(locationID) as total");

        $q = $this->db->get('location');
        $res = $q->result();

        return $res[0]->total;
    }
    
    public function getDeliveryWindowsForLocation($location_id)
    {
        $list = array();
        
        $sql = "SELECT DISTINCT delivery_windowID 

        FROM location
        LEFT JOIN delivery_zone ON (locationID = delivery_zone.location_id)
        LEFT JOIN delivery_window ON (delivery_zoneID = delivery_window.delivery_zone_id)

        WHERE locationID = ?";
        
        $CI = get_instance();
        $res = $CI->db->query($sql, array($location_id))->result_array();
        
        $proyect = 'delivery_windowID';
        foreach ($res as $row) {
            if ($row[$proyect]) {
                $list[] = $row[$proyect];
            }
        }
        
        return $list;
    }
    
    /*
     * Based on code from order_model->new_order
     * 
     * Returns the next valid due date starting from provided date
     */
    public function getDueDateForDay($location_id, $business_id, $gmt_date_string = 'now')
    {
        $daysAheadLimit = 10;
        
        $local_date = convert_from_gmt_aprax($gmt_date_string, "Y-m-d", $business_id);
        $due_date = null;
        
        //figure out the due date
        //start by getting the location turnaround
        $getLocationInfo = "select * from location where locationID = ".$location_id;
        $locationInfo = $this->db->query($getLocationInfo)->row();
        $locationTurnaround = $locationInfo->turnaround;
        
        $dueDate = date("Y-m-d", strtotime("$local_date +$locationTurnaround days"));

        //then see if we deliver to that location on that day
        //load delivery dates into an array
        $getLocationDeliveryDays = "SELECT * FROM location
                                    join location_serviceDay on location_id = locationID
                                    where locationID = ".$location_id."
                                    and location_serviceType_id = 1";
        $locationDays = $this->db->query($getLocationDeliveryDays)->result();
        $deliveryDays = array();
        foreach ($locationDays as $loc) {
            $deliveryDays[] = $loc->day;
        }

        //if not, keep going forward until we find what day we service this location
        $foundDueMatch = 0;
        for ($i = 0; $i <= $daysAheadLimit; $i ++) {
            $newDueDate = date("Y-m-d", strtotime("$dueDate + $i days"));
            if (in_array(date("N", strtotime($newDueDate)), $deliveryDays)) {
                $foundDueMatch = 1;
                //make sure this day is not a holiday
                $findHoliday = "select * from holiday
                                where date = '".date("Y-m-d", strtotime($newDueDate))."'
                                and business_id = $business_id
                                and delivery = 0";
                $holiday = $this->db->query($findHoliday)->result();
                if (!empty($holiday)) {
                    $foundDueMatch = 0;
                }
            }
            if ($foundDueMatch ==1) {
                $due_date = $newDueDate;
                break;
            }
        }
        
        return array(
            $local_date => array(
                'due_date' => $due_date,
                'location_turnaround_days' => $locationTurnaround,
                'location_delivery_days' => $deliveryDays,
            ),
        );
    }
    
    protected function getLocationClass($location_id)
    {
        $location = $this->get(array(
            'locationID' => $location_id,
            'with_location_type' => true,
            'select' => 'locationID, location.serviceType, locationType.name locationType'
        ));
        
        $location = $location[0];
       
        if (!$location) {
            return '';
        }

        $locationClass = '';

        $serviceType = strtolower($location->serviceType);
        $locationType = strtolower($location->locationType);                    

        //'recurring home delviery' is not a typo, that is what backend returns
        if ($serviceType === 'recurring home delviery' || $serviceType === 'time window home delivery') {
            $locationClass = self::LOCATION_CLASS__HOME_DELIVERY;
        } else if ($locationType === 'lockers' || $locationType === 'kiosk') {
            $locationClass = self::LOCATION_CLASS__LOCKERS;
        } else {
            $locationClass = self::LOCATION_CLASS__CONCIERGE;
        }

        return $locationClass;
    }

    
    public function isConcierge($location_id) {
        return $this->getLocationClass($location_id) === self::LOCATION_CLASS__CONCIERGE;
    }
}