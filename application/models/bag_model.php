<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Customer;

class Bag_Model extends My_Model
{
    protected $tableName = 'bag';
    protected $primary_key = 'bagID';

    /**
     * @deprecated use addBagToCustomer
     * adds a customer to a bag
     *
     * @param int $customer_id
     * @param int $bagID
     * @throws Exception missing customer ID or bagID
     * @return int affected_rows
     */
    public function addCustomerToBag($customer_id, $bagID)
    {
        if (empty($customer_id)) {
            throw new Exception("Missing customer_id");
        }

        if (empty($bagID)) {
            throw new Exception("Missing bagID");
        }

        // does the bag exist?
        $bagObj = new Bag($bagID);
        if (!array_key_exists('bagNumber',$bagObj->properties)) {
            throw new Exception("Bag already exists");
        }

        $maxBagNumber = Bag::get_max_bagNumber();


        $this->db->update('bag', array('customer_id'=>$customer_id), array('bagID'=>$bagID));

        return $this->db->affected_rows();
    }

    /**
     * adds a bag to a customer based on the bagNumber being passed
     *
     * @param int $customer_id
     * @param int $bagNumber
     * @param int $business_id
     * @throws Exception
     * @return int bagID from the bag table
     */
    public function addBagToCustomer($customer_id, $bagNumber, $business_id, $notes='', $maxBagNumber = true)
    {

        if (empty($customer_id)) {
            throw new \InvalidArgumentException("Missing customer_id");
        }

        if (empty($bagNumber)) {
            throw new Exception("Missing bagNumber");
        }

        $this->load->helper("barcode_helper");
        $bagNumber = parse_barcode($bagNumber);

        // does the bag exist?
        $bag = Bag::search_aprax(array('bagNumber'=>$bagNumber));
        if (empty($bag)) {
            //is it less then the max bag number in the settings table
            $maxBagNumber = Bag::get_max_bagNumber();
            if ( ($maxBagNumber < $bagNumber) && $maxBagNumber) {
                throw new Exception('This bag does not exist. You must print the bag tag first then add to the customer');
            } else {
                //create the new bag for the customer
                $bag = new Bag();
                $bag->customer_id = $customer_id;
                $bag->business_id = $business_id;
                $bag->bagNumber = $bagNumber;
                $bag->notes = $notes;

                return $bag->save();
            }
        }

        if (sizeof($bag)>1) {
            throw new Exception("Found duplicate bags");
        }

        $bagObj = $bag[0];
        $business = new Business($business_id);
        $blankCustomer = $business->blank_customer_id;

        if ($bagObj->business_id != $business_id) {
            throw new Exception('Bag does not belong to the current business_id ['.$business_id.']');
        }

        if ($bagObj->customer_id != $blankCustomer) {
            throw new Exception("Bag is not available, already belongs to another customer");
        }

        // update the bag by addeding the customer_id to it
        $bagObj->customer_id = $customer_id;
        $bagObj->notes = $notes;
        $bagObj->save();

        return $bagObj->bagID;

    }
    
    /**
     * Creates and assign new bag number to customer
     */
    public function newBagForCustomer($customer_id, $business_id, $notes = null)
    {
        $bagNumber = Bag::get_max_bagNumber() + 1;
        Bag::set_max_bagNumber($bagNumber);

        $this->addBagToCustomer($customer_id, $bagNumber, $business_id, $notes);

        return $bagNumber;
    }
    
    /**
     * removes a bag from customer account.
     * if the bag_id is not found in the orders table, we completely delete the bag.
     * if the bag does have an order, we assign to the business blank customer id
     *
     * @param int $bag_id
     * @return array
     */
    public function deleteBag($bag_id, $business_id)
    {
        if (empty($business_id)) {
            throw new \Exception('Cannot delete a bag without passing business_id');
        }

        $bagObj = new Bag($bag_id);

        if ($bagObj->business_id != $business_id) {
            throw new \Exception("Cannot delete a bag that do not belong to business.");
        }

        $ordersWithThisBag = \App\Libraries\DroplockerObjects\Order::search(array('bag_id'=>$bag_id));

        if (empty($ordersWithThisBag) || sizeof($ordersWithThisBag)==0) {

            if ($bagObj->delete()) {
                $result = array('status'=>'deleted', 'bagID'=>$bag_id);
            }

        } else {

            //reassing to the blank customer
            $business = new Business($business_id);
            $blankCustomerID = $business->blank_customer_id;
            $bagObj->customer_id = $blankCustomerID;
            if ($bagObj->save()) {
                $result = array('status'=>'reassigned', 'bagID'=>$bag_id);
            }

        }

        return $result;

    }


    /**
     * get_id method returns the bagID
     * Temp bag number will start with 0
     * when adding them to the bag table, remove the leading 0
     *
     * @param string $bagNumber
     * @param int $business_id
     * @param int $customer_id Optional
     *
     * @return array bagID, bagNUmber, is_new, customer_id, log
     */
    public function get_id($bagNumber, $business_id, $customer_id='')
    {
        if (empty($bagNumber)) {
            throw new Exception('Missing bagNumber');
        }

        if (empty($business_id)) {
            throw new Exception('Missing business_id');
        }


        $is_new = false;

        // first check to see if the bag number exists
        $this->clear();
        $this->bagNumber = ltrim(trim($bagNumber),"0");
        $this->business_id = $business_id;
        $bag = $this->get();

        if (!$bag) {

            // trim the leading 0
            $this->clear();
            $bag_number = ltrim($bagNumber,"0");
            $this->bagNumber = $bag_number;
            $business = new Business($business_id);
            $this->customer_id = $business->blank_customer_id;
            $this->business_id = $business_id;
            $bagID = $this->insert();
            $is_new = TRUE;
            $log = "new bag created";

        } else {
            //we found a bag, now need to check the owner
            if ($bag[0]->customer_id != 0) {

                if ($customer_id != '' && $customer_id != $bag[0]->customer_id) {
                    // this bag has a different owner then then customer id passed
                    $customer_id = $bag[0]->customer_id;
                    $bag_number = $bag[0]->bagNumber;
                    $is_new = FALSE;
                    $bagID = $bag[0]->bagID;
                    $log = "bag found with different customer id - Need to do something about this";

                } else {
                    $customer_id = $bag[0]->customer_id;
                    $bag_number = $bag[0]->bagNumber;
                    $is_new = FALSE;
                    $bagID = $bag[0]->bagID;
                    $log = "bag found with customer id";

                }

            } else {
                $bagID = $bag[0]->bagID;
                $bag_number = $bag[0]->bagNumber;
                $customer_id = $bag[0]->customer_id;
                $is_new = TRUE;
                $log = "bag found without customer, assuming temp bag";
            }
        }

        return array('bagID'=>$bagID,
                     'bagNumber'=>$bag_number,
                     'is_new'=>$is_new,
                     'customer_id'=>$customer_id,
                     'log'=>$log);
    }
    
    /*
     * Marks bag as printed nd return info to generate print page
     */
    public function print_bag_tag($customerID, $bag_number, $business_id, $with_default_location)
    {
        $success = true;
        $message = '';
        $data = array();
        
        $customer = new Customer($customerID);
        $data['customer'] = $customer->firstName . " " . $customer->lastName;
        $data['bag'] = $bag_number;
        $data['bag_message'] = get_translation("message", "print/bag_tag", array(), "FREE VIP BAG", $customerID);
        $data['bag_instructions'] = get_translation("instructions", "print/bag_tag", array(), "use with every order", $customerID);
        $data['bag_code'] = ($business_id == 3) ? substr($customer->phone, -4) : '';

        //for translation
        $data['customerId'] = $customer->customerID;

        update_customer_meta($customer->customerID, "assign_new_bag_on_new_order", "");

        if($with_default_location){
            //add default location
            $get_default_location_query = $this->db->get_where("location", array("locationID" => $customer->location_id));
            $default_location_row = $get_default_location_query->row();
            $default_location= $default_location_row->address;

            if ($default_location) {
                $data['default_location'] = get_translation("default_location", "print/bag_tag", array(), "Loc", $customerID) . ' : ' . $default_location;
            }
        }

        //make sure this bag is assigned to this company
        $getCustomer = "select * from  bag
        join customer on customerID = customer_id
        where bagNumber = ?
        and customer.business_id = $business_id";
        $query = $this->db->query($getCustomer, array($bag_number));
        $bag_customer = $query->result();

        if ($bag_customer) {
            //see if there are notes on this bag, if so, print them
            $sqlNotes = "select * from bag where bagNumber = $bag_number";
            $query = $this->db->query($sqlNotes);
            $data['notes'] = $query->result();

            //update the db to note that this bag has been printed
            $update1 = "update bag set printed = 1 where bagNumber = $bag_number";
            $update = $this->db->query($update1);
            $message = "Bag $bag_number marked as printed";

            $data['barcode_url'] = get_full_base_url() . "/barcode.php?barcode=$bag_number&width=220&height=50";
        } else {
            $data['__warning'] = 'This bag is not associated to a customer that is registered with your business';
        }
        
        $data['success'] = $success;
        $data['message'] = $message;
        
        return $data;
    }

}
