<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class ItemUpcharge_Model extends My_Model
{
    protected $tableName = 'itemUpcharge';
    protected $primary_key = 'itemUpchargeID';

    public function insert_batch($array)
    {
        $this->db->insert_batch('itemUpcharge', $array);
    }

    /**
     * Returns the item upcharges
     * @param int $item_id The item ID
     * @return array The product IDs of all the upcharges.
     */
    public function get_upcharges($item_id)
    {
        $this->db->select("name, itemUpcharge.*");
        $this->db->join("upcharge", "upchargeID = upcharge_id");
        $result = $this->db->get_where($this->tableName, array("item_id" => $item_id));

        return $result->result();
    }

    public function get_employee_upcharges($employee_id, $startDate = null, $endDate = null)
    {

        if (empty($startDate)) {
            $startDate = date("Y-m-d");
        }

        list($startTime, $endTime) = get_date_range($startDate, $endDate);

        $sql = 'SELECT u.upchargeID, u.name, u.upchargePrice, u.upchargeGroup_id, i_u.*';
        $sql.= ' FROM upcharge u';
        $sql.= ' INNER JOIN itemUpcharge i_u ON (u.upchargeID = i_u.upcharge_id)';
        $sql.= ' WHERE i_u.employee_id = ? AND i_u.dateCreated >= ? and i_u.dateCreated <= ?';

        $query = $this->db->query($sql, array($employee_id, $startTime, $endTime));
        return $query->result();

    }

    /**
     * function deletes rows then does an insert
     *
     * @param int $item_id
     * @param array $upcharges If not set, then all upcharges for the items are removed
     * @param int $employee_id
     */
    public function update($item_id, $upcharges, $employee_id = null)
    {
        $this->db->where('item_id', $item_id);
        $this->db->delete('itemUpcharge');

        if (!empty($upcharges)) {
            foreach ($upcharges as $upcharge_id) {
                $a[] = array('item_id' => $item_id, 'upcharge_id' => $upcharge_id, 'employee_id' => $employee_id );
            }
            $this->db->insert_batch('itemUpcharge', $a);
        }
    }

     /**
     * removes any one time upcharges from an item
     *
     * @param int $item_id
     * @return int insert_id
     */
    public function remove_oneTimeUpcharges($item_id)
    {
        $delOneTime = "DELETE FROM itemUpcharge
                            where item_id = $item_id
                            and upcharge_id in (select upchargeID from upcharge where everyOrder != 1)";
        $this->db->query($delOneTime);
    }

}
