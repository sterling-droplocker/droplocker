<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Testimonial_Model extends My_Model
{
    protected $tableName = 'testimonial';
    protected $primary_key = 'testimonialID';

}
