<?php
class Reminder_Model extends My_Model
{
    protected $tableName = 'reminder';
    protected $primary_key = 'reminderID';

    /**
     * Retrieves the number of acl roles for a particular business
     * @param int $business_id
     * @return int
     */
    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $reminders_count = $this->db->query("SELECT COUNT(reminderID) AS total FROM {$this->tableName} WHERE business_id = ?", array($business_id))->row();

        return $reminders_count->total;
    }
}
