<?php


use App\Libraries\DroplockerObjects\Location;

class DeliveryZone_Model extends My_Model
{
    protected $tableName = 'delivery_zone';
    protected $primary_key = 'delivery_zoneID';

    /**
     * Gets the delivery zones for the business
     *
     * @param int $business_id
     * @return array an array of delivery zone objects
     */
    public function get_delivery_zones($business_id)
    {
        $sql = 'SELECT * FROM delivery_zone
                LEFT JOIN delivery_window ON delivery_window.delivery_zone_id = delivery_zone.delivery_zoneID
                WHERE delivery_zone.business_id =  ?';

        return $this->db->query($sql, array($business_id))->result();
    }

    public function get_all_routes($business_id)
    {
        $sql = 'SELECT delivery_zone.*, delivery_window.* FROM delivery_zone
                LEFT JOIN delivery_window ON delivery_window.delivery_zone_id = delivery_zone.delivery_zoneID
                LEFT JOIN location ON (location_id = locationID)
                WHERE delivery_zone.business_id =  ?
                AND serviceType = ?';

        $rows = $this->db->query($sql, array($business_id, Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY))->result();

        $out = array();

        foreach ($rows as $row) {
            $id = $row->delivery_zoneID;
            if (!isset($out[$id])) {
                $out[$id] = (object) array(
                    'delivery_zoneID' => $row->delivery_zoneID,
                    'route' => $row->route,
                    'business_id' => $row->business_id,
                    'location_id' => $row->location_id,
                    'windows' => array(),
                );
            }

            $day_indexes = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
            $days = array();
            foreach ($day_indexes as $key) {
                if ($row->$key) {
                    $days[] = ucfirst($key);
                }
            }

            $out[$id]->windows[$row->delivery_windowID] = (object) array(
                'delivery_windowID' => $row->delivery_windowID,
                'route' => $row->route,
                'start' => $row->start,
                'end' => $row->end,
                'days' => $days,
            );

        }

        return $out;
    }

    public function get_all_routes_array($business_id)
    {
        $routes = $this->get_all_routes($business_id);

        $out = array();
        foreach ($routes as $id => $route) {
            $windows = array();
            foreach ($route->windows as $window) {
                $start = preg_replace('/:00$/', '', $window->start);
                $end = preg_replace('/:00$/', '', $window->end);
                $days = implode('-', $window->days);
                $windows[] = sprintf("%s-%s %s", $start, $end, $days);
            }

            $windows = implode(', ', $windows);
            $out[$id] = sprintf("Route %s, %s", $route->route, $windows);
        }

        return $out;
    }

}
