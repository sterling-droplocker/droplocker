<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class OrderPaymentStatus_Model extends My_Model
{
    protected $tableName = 'orderPaymentStatus';
    protected $primary_key = 'orderPaymentStatusID';

    /**
     * returns an array for processing
     */
    public function simple_array()
    {
        $this->db->select('orderPaymentStatusOptionID, name');
        $q = $this->db->get('orderPaymentStatusOption');
        $res = $q->result();
        foreach ($res as $r) {
            $a[$r->orderPaymentStatusOptionID] = $r->name;
        }

        return $a;
    }

}
