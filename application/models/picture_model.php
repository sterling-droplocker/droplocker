<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Picture_Model extends My_Model
{
    protected $tableName = 'picture';
    protected $primary_key = 'pictureID';

    /**
     * gets the picture file and returns a string
     *
     * @param int $item_id
     * @param int $business_id
     * @return string
     */
    public function getPictureLocation($item_id, $business_id)
    {
        if (empty($business_id)) {
            throw new Exception("Missing business ID");
        }

        $this->db->where('item_id', $item_id);
        $this->db->select('file');
        $picture = $this->db->get('picture');

        return "https://s3.amazonaws.com/LaundryLocker/".$picture[0]->file;
    }

}
