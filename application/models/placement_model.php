<?php

class Placement_Model extends My_Model
{
    protected $tableName = 'placement';
    protected $primary_key = 'placementID';

    /**
     * Returns all placements from the database
     */
    public function get_all()
    {
       $placements = $this->db->query("SELECT * FROM placement");
       foreach ($placements->result() as $index => $placement) {
           $result[$placement->placementID] = $placement->place;
       }

       return $result;
    }


}
