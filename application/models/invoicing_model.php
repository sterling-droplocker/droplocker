<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Invoicing_Model extends My_Model
{
    public function __construct()
    {
        parent::__construct('invoicing');
    }

    public function update_settings($options = array(),$where=array())
    {
        $this->db->update('invoicing', $options, $where);

        return $this->db->affected_rows();
    }
}
