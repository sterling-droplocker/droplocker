<?php

class App_Model extends MY_Model
{
    protected $tableName = 'app';
    protected $primary_key = 'appID';

    public function get_by_business_id($business_id)
    {
        if (is_numeric($business_id)) {
            $app = $this->get_one(array(
                "business_id" => $business_id
            ));

            return $app;
        } else {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        }
    }
}
