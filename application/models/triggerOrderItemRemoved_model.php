<?php

/**
 * Note, this table populated by the trigger, logRemovedOrderItem which logs any deletions on the order item table.
 */
class triggerOrderItemRemoved_model extends MY_Model
{
    protected $tableName = 'triggerOrderItemRemoved';
    protected $primary_key = 'triggerOrderItemRemovedID';

}
