<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Supplier_Model extends My_Model
{
    protected $tableName = 'supplier';
    protected $primary_key = 'supplierID';

    public function getBusinessSuppliers($business_id)
    {
        $this->db->order_by('companyName');
        $this->db->where('business_id', $business_id);
        $this->db->where('enabled', 1);
        $this->db->join("business", "supplierBusiness_id = businessID");
        $this->db->select("companyName, supplierID");
        $q = $this->db->get('supplier');

        return $q->result();
    }


    public function simple_array($business_id)
    {
        $suppliers = $this->getBusinessSuppliers($business_id);
        $s = array();
        foreach ($suppliers as $v) {
            $s[$v->supplierID] = $v->companyName;
        }

        return $s;
    }

}
