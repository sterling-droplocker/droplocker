<?php

class InvoiceCustomField_Model extends My_Model
{
    protected $tableName = 'invoiceCustomField';
    protected $primary_key = 'invoiceCustomFieldID';

    public function getMaxSortOrder($business_id)
    {
        $this->db->select_max('sortOrder');
        $this->db->from($this->tableName);
        $this->db->where(array(
            'business_id' => $business_id
        ));

        $row = $this->db->get()->row();
        return $row->sortOrder;
    }

    public function updateSortOrder($business_id, $field_id, $newOrder, $oldOrder)
    {
        // If this has an oldOrder, then a hole has been left in the oldOrder position
        if ($oldOrder) {
            $this->db->set('sortOrder', 'sortOrder - 1', false);
            $this->db->where(array(
                'business_id' => $business_id,
                'sortOrder >' => $oldOrder,
                'invoiceCustomFieldID != ' => $field_id
            ));
            $this->db->update($this->tableName);
        }

        // then we make room for the new sortOrder
        if ($newOrder) {
            $this->db->set('sortOrder', 'sortOrder + 1', false);
            $this->db->where(array(
                'business_id' => $business_id,
                'sortOrder >=' => $newOrder,
                'invoiceCustomFieldID != ' => $field_id
            ));
            $this->db->update($this->tableName);
        }
    }

    public function getInvoiceCustomFields($business_id, $business_languageID)
    {
        $this->db->join("invoiceCustomFieldValue", "invoiceCustomField_id = invoiceCustomFieldID", "LEFT");
        $this->db->where(array(
            'business_id' => $business_id,
            'business_language_id' => $business_languageID,
        ));

        $this->db->order_by("sortOrder", "ASC");
        return $this->db->get("invoiceCustomField")->result();
    }

}