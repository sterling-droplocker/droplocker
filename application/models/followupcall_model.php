<?php

class FollowupCall_Model extends My_Model
{
    protected $tableName = 'followupCall';
    protected $primary_key = 'followupCallID';

    /**
     * Retrieves the number of followup calls for a particular business
     * @param int $business_id
     * @return int
     */
    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $followupCalls_count = $this->db->query("SELECT COUNT(followupCallID) AS total FROM {$this->tableName} WHERE business_id = ?", array($business_id))->row();

        return $followupCalls_count->total;
    }
    /**
     * Retrieves the followup call history for a customer.
     * @param int $customer_id
     * @param int $business_id
     * @return array An array of stdClass objects
     * @throws Exception
     */
    public function get_history($customer_id, $business_id, $limit = false)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->db->select("note, employee_id, employee.firstName, employee.lastName, dateCalled, followupCallType.name AS call_type");
        $this->db->join("employee", "employeeID = employee_id", "left");
        $this->db->join("followupCallType", "followupCallType_id = followupCallTypeID");
        $this->db->where("followupCall.customer_id", $customer_id);
        $this->db->where("followupCall.business_id", $business_id);
        $this->db->order_by("dateCalled", "desc");
        if ($limit) {
            $this->db->limit($limit); 
        }
        $get_followup_calls_query = $this->db->get($this->tableName);

        $calls = $get_followup_calls_query->result();

        return $calls;
    }
    /**
     *
     * @param type $customer_id
     * @param type $employee_id
     * @param type $followupCallType_id
     * @param type $dateCalled
     * @param type $business_id
     * @param type $note
     * @return int
     * @throws Exception
     */
    public function insert($customer_id, $employee_id, $followupCallType_id, $dateCalled, $business_id, $note = "")
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        if (empty($dateCalled)) {
            throw new Exception("'dateCalled' must be a valid MYSQL date.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        /*
        if (!is_numeric($order_id)) {
            throw new Exception("'order_id' must be numeric.");
        }
        */
        if (!is_numeric($employee_id)) {
            throw new Exception("'employee_id' must be numeric.");
        }
        if (!is_numeric($followupCallType_id)) {
            throw new Exception("'followupCallType_id' must be numeric.");
        }
        $this->load->model("followupCallType_model");

        return $this->db->insert($this->tableName,
            array("customer_id" => $customer_id, "followupCallType_id" => $followupCallType_id, "dateCalled" => $dateCalled, "business_id" => $business_id, "employee_id" => $employee_id, "note" => $note)
        );
    }
}
