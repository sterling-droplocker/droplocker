<?php
use App\Libraries\DroplockerObjects\Locker;
class DroidPostReturnToOffice_Model extends My_Model
{
    protected $tableName = 'droid_postReturnToOffice';
    protected $primary_key = 'ID';

    /**
     * inserts a postPickup from the droid to the database
     *
     * Required Options Values
     * ----------------------
     * transaction_id
     * bagNumber
     * location_id
     * lockerName
     * employee_id
     * business_id
     *
     * @param array $options
     * @throws Exception for missing required option values
     * @return int insert_id
     */
    public function insert($options = array())
    {
        $required = array('transaction_id',
                          'bagNumber',
                          'location_id',
                          'lockerName',
                          'employee_id',
                          'business_id');

        if (!required($required, $options)) {
            throw new Exception("Missing required option values. <br>Required: <br>". print_r($required, 1)."<br>Sent: <br>".print_r($options, 1));
        }

        // rename FRONT to Front_Counter
        $options['lockerName'] = ($options['lockerName'] == 'FRONT')?'Front_Counter':$options['lockerName'];

        // check to see if the transaction exists... if it do, then just return that id
        if (!$id = $this->get($options)) {
            $this->db->insert('droid_postReturnToOffice', $options);
            $id = $this->db->insert_id();
        }

        return $id;
    }


    /**
     *
     * Updates a row in the  table
     *
     * @param array $options
     * @param array $where
     * @throws Exception for missing where statement
     * @return int affected_rows
     */
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where statement");
        }

        $this->db->update('droid_postReturnToOffice', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * gets data from the table
     *
     * $options values
     * ----------------
     * transaction_id
     * status
     *
     * @param array $options
     * @return array of objects
     */
    public function get($options = array())
    {
        if (isset($options['transaction_id'])) {$this->db->where('transaction_id',$options['transaction_id']);}
        if (isset($options['status'])) {$this->db->where('status',$options['status']);}
        if (isset($options['version'])) {$this->db->where('version',$options['version']);}

        $query = $this->db->get('droid_postReturnToOffice');

        return $query->result();
    }



    /**
    * Processes a single return to office reminder
    *
    * @param stdClass $reminder
    * @throws Exception
    */
    public function processReturnToOffice($reminder)
    {
        $this->load->model('droidpostreturntooffice_model');
        $where = array('ID'=>$reminder->ID); // used for updating the droid_postPickups table
        $defaultOptions = array('status'=>'complete');

        try {

            if (empty($reminder->bagNumber)) {
                throw new Exception("Missing bag number");
            }

            //trim leading zero off bag number
            $bagNumber = ltrim($reminder->bagNumber, '0');
            //find the order.
            $select2 = "SELECT max(orderID) as lastOrder, orderPaymentStatusOption_id, orders.business_id, returnToOfficeLockerId, location.address, locker.lockerID
                FROM orders
                JOIN bag on bagID = orders.bag_id
                JOIN business ON businessID = orders.business_id
                JOIN locker on lockerID = locker_id
                JOIN location on locationID = location_id
                WHERE bagNumber = '$bagNumber'
                AND orders.business_id = {$reminder->business_id}";

            $select2 = $this->db->query($select2);
            $line2 = $select2->result();
            $line2 = $line2[0];

            $locker = Locker::search(array('lockerName'=>$reminder->lockerName, "location_id"=>$reminder->location_id));

            //email customer by changing to status 4 and put it in the Return to Office Locker
            if ($line2->lastOrder!='') {
                $this->load->model('order_model');

                //set a note where the order was pulled from
                $this->logger->set('orderStatus', array('order_id' => $line2->lastOrder, 'locker_id' => $line2->lockerID, 'employee_id' => $reminder->employee_id, 'orderStatusOption_id' => '4', 'note' => "Pulled From Locker ".$reminder->lockerName." at ".$line2->address, 'visible' => 0));

                $options['order_id'] = $line2->lastOrder;
                $options['orderStatusOption_id'] = 4;
                $options['locker_id'] = $line2->returnToOfficeLockerId; //return to office lockerID
                $options['orderPaymentStatusOption_id'] = $line2->orderPaymentStatusOption_id;
                $options['business_id'] = $line2->business_id;
                $options['employee_id'] = $reminder->employee_id;
                $response = $this->order_model->update_order($options);

                $updateLoaded = "update orders set loaded = 0 where orderID = $line2->lastOrder";
                $update = $this->db->query($updateLoaded);

                $statusNotes[]  = "Order status was changed to 4";

            } else {
                $statusNotes[]  = "Order not found";
            }

            $statusNotes[] = "Succesfully sent return to office reminder";
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;
            $this->droidpostreturntooffice_model->update($options,$where);


        } catch (Exception $e) {
            send_exception_report($e);
        }
    }

}
