<?php

class Business_Order_Model extends My_Model
{
    protected $tableName = 'business_order';
    protected $primary_key = 'ID';

    /**
     
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where clause");
        }

       # $this->db->update('business_employee', $options, $where);

        return $this->db->affected_rows();
    }

    **/
    
    /**
    
    public function update($options = array(), $where = array())
    {
        
        
        if (sizeof($where)<1) {
            throw new Exception("Missing where clause");
        }
        
        
    }
    **/
    
    public function get($options = array())
    {
                   
            
        $sqlGetBisOrders = "select businessid, business.companyname, year(dueDate) as yearDue, month(dueDate) as mthDue, sum(closedGross) closedGross, sum(closedDisc) as closedDisc, sum(tax) as tax, count(*) orders
                        from orders
                        join business
                          on businessid = business_id
                        where orders.dueDate >= '2016-01-01'
                          and orders.dueDate <  '2019-01-01'
                          and businessid in (
                            select distinct business_id from orders where dateCreated >= '2019-01-01'
                          )
                        group by businessid, year(dueDate), month(dueDate)
                        ;";
        
        
        $query = $this->db->query($sqlGetBisOrders);
        
        return $query->result();
        
        
    }
    
    public function get_all()
    {
        $options['exclude_business_employee'] = true;
       // $options['order_by'] = 'lastName, firstName';
        
        return $this->get($options);
    }

}
