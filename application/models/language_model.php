<?php
class Language_Model extends MY_Model
{
    protected $tableName = 'language';
    protected $primary_key = 'languageID';

    public function findByTag($tag) {
        return $this->db->get_where($this->tableName, array("tag" => $tag))->row();
    }
    /**
     * Returns all the business languages for a business.
     * @param int $business_id
     * @return array
     */
    public function findAllForBusiness($business_id) {
        $this->db->join("business_language","business_language.language_id = language.languageID");
        return $this->db->get_where($this->tableName, array("business_language.business_id" => $business_id))->result();
    }
    /**
     * Retrieves language entities associated toa paritcular business in a format compatible with the coedeigntier form helper dropdown
     * @param int $business_id
     * @return array
     */
    public function get_all_as_codeigniter_dropdown_for_business($business_id)
    {
        $this->db->join("business_language","business_language.language_id = language.languageID");
        $languages_by_business = $this->db->get_where($this->tableName, array("business_language.business_id" => $business_id))->result();
        $languages_by_business_dropdown = array();
        foreach ($languages_by_business as $language_by_business) {
            $languages_by_business_dropdown[$language_by_business->languageID] = sprintf("%s -%s", $language_by_business->tag, $language_by_business->description);
        }

        return $languages_by_business_dropdown;
    }
    /**
     * Retrieves language entities and returns them in a codeignite form helper dropdown format
     * @param int $business_id If set, then this fucntion only returns languages that do not have a an associated business_language
     * @return array
     */
    public function get_all_as_codeigniter_dropdown($business_id = null)
    {
        if (is_null($business_id)) {
            $languages = $this->get_all();
            $languages_dropdown = array();
            foreach ($languages as $language) {
                $languages_dropdown[$language->languageID] = sprintf("%s - %s", $language->tag, $language->description);
            }

            return $languages_dropdown;
        } else {
            $languages = $this->db->query("SELECT * FROM language LEFT JOIN business_language ON language.languageID=business_language.language_id
                WHERE business_language.language_id IS NULL")->result();
            $languages_dropdown = array();
        }
    }

}
