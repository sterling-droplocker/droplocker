<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Transaction_Model extends My_Model
{
    protected $tableName = 'transaction';
    protected $primary_key = 'transactionID';

    /**
     * (non-PHPdoc)
     * @see My_Model::get()
     */
    public function get($options = array())
    {
        if (isset($options['order_id'])) {$this->db->where('order_id', $options['order_id']);}
        if (isset($options['customer_id'])) {$this->db->where('customer_id', $options['customer_id']);}
        if (isset($options['resultCode'])) {$this->db->where('resultCode', $options['resultCode']);}
        if (isset($options['business_id'])) {$this->db->where('business_id', $options['business_id']);}
        if (isset($options['updated'])) {$this->db->where('updated', $options['updated']);}

        if (isset($options['type'])) {
            if(is_array($options['type']))
                $this->db->where_in('type', $options['type']);
            else
                $this->db->where('type', $options['type']);
        }
        if (isset($options['order_by'])) {$this->db->order_by($options['order_by'], "desc");}

        $query = $this->db->get('transaction');

        return $query->result();
    }


    /**
     * @deprecated This function is deprecated due to the only calling function has been deprecated also.
     *
     * For order history we now use the order object get_history function
     * @param int $order_id
     */
    public function getTransactionHistory($order_id)
    {
        $sql = "SELECT * FROM transaction WHERE resultCode in (0,1) AND `type`='sale' AND order_id = {$order_id}";

        return $this->db->query($sql)->result();
    }


    /**
     * gets the latest pnref for a customer from the transaction table.
     *
     * @param int $customer_id
     * @param int $business_id
     * @param string $processor
     * @return string pnref
     */
    public function getMaxPnref($customer_id, $business_id, $processor='verisign')
    {
        $sql = "SELECT pnref FROM transaction
                        WHERE resultCode = 0
                        AND customer_id = {$customer_id}
                        AND business_id = {$business_id}
                        AND processor = '{$processor}'
                        AND `type` in ('sale', 'auth')
                        ORDER BY transactionID desc
                        LIMIT 1";
        $query = $this->db->query($sql);
        $row = $query->row();

        return $row->pnref;
    }

    /**
     * gets the latest sale record for a given order.
     *
     * @param int $customer_id
     * @param int $business_id
     * @param string $processor
     * @return string pnref
     */
    public function getLastOrderSale($order_id)
    {
        $sql = "SELECT transactionID, order_id, pnref, message, processor, amount FROM transaction
                        WHERE type = 'sale'
                        AND order_id = {$order_id}
                        ORDER BY transactionID desc
                        LIMIT 1";
        $query = $this->db->query($sql);
        $row = $query->row();

        return $row;
    }
}
