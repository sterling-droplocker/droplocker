<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Barcode_Model extends My_Model
{
    protected $tableName = 'barcode';
    protected $primary_key = 'barcodeID';

    /**
     * pass the barcode to get the related item and then get the orders with that item
     *
     * @param string barcode
     * @return array of objects
     * @todo escape the barcode
     */
    public function get_order_item_from_barcode($barcode)
    {
        $this->db->select("barcode.barcode,customer.firstName,customerID, customer.lastName,orderItem.unitPrice, product.displayName, orderItem.order_id, orders.dateCreated AS order_date,orderStatusOption.name AS order_status, itemID, bag.bagNumber");
        $this->db->where("barcode", $barcode);
        $this->db->where("orders.business_id", $this->business_id);
        $this->db->join('item', 'barcode.item_id=item.itemID', 'left');
        $this->db->join('product_process', 'item.product_process_id=product_processID', 'left');
        $this->db->join('product', 'product_process.product_id=product.productID', 'left');
        $this->db->join('orderItem', 'barcode.item_id=orderItem.item_id', "left");
        $this->db->join('orders', 'orderItem.order_id = orders.orderID', "left");
        $this->db->join('customer', 'orders.customer_id=customer.CustomerID', 'left');
        $this->db->join('orderStatusOption', 'orders.orderStatusOption_id=orderStatusOption.orderStatusOptionID', "left");
        $this->db->join("bag", "bag.bagID=orders.bag_id", "left");
        $query = $this->db->get('barcode');

        return $query->result();
    }

    /**
     * --------------------------------
     * BEGIN
     * --------------------------------
        * gets a barcode with customer and item information
        *
        * @param string $barcode
        * @param int $business_id
        * @return array of objects
     * --------------------------------
     * BEGIN
     * --------------------------------*
    */
    public function getBarcode($barcode, $business_id)
    {
        $sql = "SELECT `customerID`, `firstName`, `lastName`, `itemID`, `customer_item`.`customer_id`, `product_process_id`
                FROM (`barcode`)
                JOIN `item` ON `itemID` = `barcode`.`item_id`
                JOIN `customer_item` ON `item`.`itemID` = `customer_item`.`item_id`
                LEFT JOIN `customer` ON `customerID` = `customer_item`.`customer_id`
                WHERE `barcode`.`barcode` =  ?
                    AND barcode.business_id = ?
                    AND customer.business_id = ?";

        $query = $this->db->query($sql, array($barcode, $business_id, $business_id));

        return $query->result();
    }
    /**
     * A barcode is considered assembled if there is at least one entry in the automat table.
     * @param int $barcode
     * @return boolean
     */
    public function is_assembled($barcode)
    {
        $this->db->select("automatID");
        $this->db->where("garcode", $barcode);
        $this->db->limit(1);
        $check_if_serviced_query = $this->db->get("automat");
        $check_if_serviced_query_result = $check_if_serviced_query->result();
        if (empty($check_if_serviced_query_result)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @param int $barcode
     * @param int $business_id
     * @return boolean
     * @throws Exception
     */
    public function barcode_not_exists($barcode, $business_id)
    {

        if (empty($barcode)) {
            throw new Exception("'barcode' can not be empty.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }

        $item = $this->get_one(array(
            'barcode' => $barcode,
            'business_id' => $business_id
        ));

        if (!$item) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getBarcodesForCustomer($customer_id, $business_id)
    {
        $sql = "SELECT barcode.* 
                FROM barcode
                JOIN item ON itemID = barcode.item_id
                JOIN customer_item ON item.itemID = customer_item.item_id
                LEFT JOIN customer ON customerID = customer_item.customer_id
                WHERE customer.customerID =  ?
                    AND barcode.business_id = ?
                    AND customer.business_id = ?";

        $query = $this->db->query($sql, array($customer_id, $business_id, $business_id));

        return $query->result();
    }
}
