<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Bag;
class DroidPostDeliveries_Model extends My_Model
{
    protected $tableName = 'droid_postDeliveries';
    protected $primary_key = 'ID';

    /**
     * inserts a postPickup from the droid to the database
     *
     * Required Options Values
     * ----------------------
     * transaction_id
     * bagNumber
     * order_id
     * lockerName
     * employee_id
     * business_id
     *
     * @param array $options
     * @throws Exception for missing required option values
     * @return int insert_id
     */
    public function insert($options = array())
    {
        $required = array('transaction_id', 'bagNumber', 'order_id', 'lockerName', 'employee_id', 'business_id');

        if (!required($required, $options)) {
            throw new Exception("Missing required option values. <br>Required: <br>". print_r($required, 1)."<br>Sent: <br>".print_r($options, 1));
        }

        // check to see if the transaction exists... if it do, then just return that id
        if (!$id = $this->get($options)) {
            $this->db->insert('droid_postDeliveries', $options);
            $id = $this->db->insert_id();
        }

        return $id;
    }


    /**
     *
     * Updates a row in the  table
     *
     * @param array $options
     * @param array $where
     * @throws Exception for missing where statement
     * @return int affected_rows
     */
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where statement");
        }

        $this->db->update('droid_postDeliveries', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * gets data from the table
     *
     * $options values
     * ----------------
     * transaction_id
     * status
     * order_id
     *
     * @param array $options
     * @return array of objects
     */
    public function get($options = array())
    {
        if (isset($options['transaction_id'])) {
            $this->db->where('transaction_id',$options['transaction_id']);

        }
        if (isset($options['status'])) {
            $this->db->where('status',$options['status']);

        }
        if (isset($options['order_id'])) {
            $this->db->where('order_id',$options['order_id']);

        }
        if (isset($options['version'])) {
            $this->db->where('version',$options['version']);

        }

        //Note, this is an temporary hack to quickly patch * design flaws.
        if (isset($options['process_status'])) {
            $this->db->where('process_status', $options['process_status']);
        }
        $query = $this->db->get('droid_postDeliveries');

        return $query->result();
    }


    /**
    *
    * @param stdClass object $delivery
    * @throws Exception
    * @return boolean
    */
    public function processDelivery($delivery)
    {
        $this->load->model('order_model');
        //make sure we have a delivery to process
        if (empty($delivery->ID)) {
            throw new Exception("Missing delivery object");
        }

        //$where = array('ID'=>$delivery->ID); // used for updating the droid_postPickups table
        $defaultOptions = array('status'=>'complete');
        $where = array('ID'=>$delivery->ID);

        $delivery->lockerName = trim(urldecode($delivery->lockerName));
        
        $lockerList = array_map('trim', explode(',', $delivery->lockerName));
        $firstLocker = $lockerList[0];
        $multiLocker = count($lockerList) > 1;
        
        if (substr($delivery->bagNumber, 0, 1) == '0') {
            $tempTag = 1;
            $delivery->bagNumber = ltrim($delivery->bagNumber, "0");
        }


        $maxBagNumber = Bag::get_max_bagNumber();
        if ((int) ltrim($delivery->bagNumber, '0') > $maxBagNumber) {
            $statusNotes[] = "Delivery Bagnumber [{$delivery->bagNumber}] is higher then the last bag number [{$maxBagNumber}]";
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;
            $this->update($options,$where);
            $message = "The bag for this delivery is higher then then last bag number that was printed. This is most likely due to a scanner error from the phone. Please check this claim<br><br>".formatArray($delivery);
            queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($delivery->business_id,'customerSupport'), "Delivery Bagnumber [{$delivery->bagNumber}] is higher then the last bag number [{$maxBagNumber}]", $message, array(), null, $delivery->business_id);

            return true;
        }


        $order = Order::search(array('orderID'=>$delivery->order_id, 'business_id'=>$delivery->business_id));

        if (!$order->orderID) {

            $statusNotes[] = 'order not found: ['.$delivery->order_id.'], or this order does not belong to this business';
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;

            $this->update($options,$where);

            return true;
        }

        if ($delivery->business_id != $order->business_id) {

            $statusNotes[] = "OrderID and business_id mismatch";
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;
            $this->update($options,$where);

            return true;
        }

        // The order locker and location
        $orderLocker = $order->find("locker_id");
        $orderLocation = $orderLocker->find('location_id');

        $lockerListDescription = $multiLocker ? (" " . get_translation('multi_locker_list', 'orders', array('locker_list' => implode(', ', $lockerList)), "(Delivered to lockers: %locker_list%)", $order->customer_id) . " ") : "";

        $deliveryLocker = Locker::search(array('lockerName'=>$firstLocker, 'location_id'=>$orderLocation->locationID));
        $deliveryLockerID = @$deliveryLocker->lockerID; //Note, the misuse of the error control ioperator is a workaround * design flaw
        if (empty($deliveryLockerID)) {

            /**
             * The locker that was sent from the phone does not exist in this location.
             * An email will be sent to support
             * When the driver refreshes the phone the delivery will come back
             *
             */

            // get the bag for this order
            $bag = $order->find('bag_id');
            $loc = $orderLocation;


            $bodyText = 'Bag: '.$bag->bagNumber.'<br>Order: <a href="https://droplocker.com/admin/orders/details/'.$order->orderID.'">'.$order->orderID.'</a><br><br>';
            $bodyText .= 'This order was delivered to electronic locker #'. $firstLocker . $lockerListDescription . '.  This locker number is not listed at '.$loc->address.', which is where this order was supposed to be delivered.<br><br>';
            $bodyText .= '<a href="https://droplocker.com/admin/admin/search?search_type=locker&lockerName='.$firstLocker.'">Click Here</a> to see a list of where locker '.$firstLocker.' may be located. <br><br>';
            $bodyText .= 'Please investigate this immediately.';
            send_admin_notification('Order '.$order->orderID.' Mis-Delivered', $bodyText, $delivery->business_id);

            $statusNotes[] = 'Order '.$order->orderID.' Mis-Delivered';
            $options = $defaultOptions;
            $options['statusNotes'] = serialize($statusNotes);
            $options['methodCalled'] = __METHOD__;
            $this->update($options,$where);

            // update the order status
            $sql = "INSERT INTO orderStatus (order_id, date, employee_id, note, statusType)
                    VALUES (".$delivery->order_id.", '".date('Y-m-d H:i:s')."', ".$delivery->employee_id.", 'Order was missdelivered to locker ". $firstLocker . $lockerListDescription . "', 'order')";
            $this->db->query($sql);

            return true;
        }

        if (Locker::isElectronicLock($orderLocker->lockerLockType_id)) {          
            // update the order with new drop location locker id
            //$where['orderID'] = $order->orderID;
            $options['locker_id'] = $deliveryLocker->lockerID;
            $response = $this->order_model->update($options, array('orderID'=>$order->orderID));

            $statusNotes[] = "Updated order locker_id to ".$deliveryLocker->lockerID . $lockerListDescription;

            // get the new order object
            $order = new Order($delivery->order_id);

            if ($delivery->order_id != $order->orderID) {
                throw new Exception("Order not found - Order Mismaatch between found order and delivery order");
            }

        }

        // an option was set earlier so just to be safe, let's clear options
        $options = array();

        $locker_id = $order->locker_id;
        $bag = $order->find('bag_id');

        //find out if this customer pays by invoice
        $customer = $order->find('customer_id');
        $invoice = $customer->invoice;

        $order_total = $this->order_model->getTotal($order->orderID);

        if ($order->orderPaymentStatusOption_id == 3 || $invoice == 1 || $order_total <= .02) {

            if ($order_total <= .02) {
                $this->load->model("customer_model");

                //we dont want to capture order if customer has unpaid orders
                $customerIsOnPaymentHold = $this->customer_model->isOnPaymentHold($order->customer_id);                
                if ($customerIsOnPaymentHold) {
                    $$result = array("status" => "error");
                } else {
                    $result = $this->order_model->capture($order->orderID, $order->business_id);
                }

                if ($result['status']=='success' || $customer->customer_deliver_on_hold) {
                    // If the order was 'Out for Partial Delivery', then change the status to 'Partially Delivered.
                    if ($order->orderStatusOption_id == 12) {
                    //change to partially delivered
                        $options['orderStatusOption_id'] = 11;
                    } else { //Otherwise, change to ready for pickup
                        $options['orderStatusOption_id'] = 9;
                    }
                } else {
                    // Fail - put on payment hold
                    // The capture function will update the order and place on 7
                    // Just to be safe, I make sure the order will be set to 13, even though it should already be 13
                        $options['orderStatusOption_id'] = 13;
                        $statusNotes[] = "Updated order status to 13 (delivery failure - unpaid). If the order is currently not on payment hold, we will try to capture. Therefore, the status might not be 13 after the capture attempt.";
                }
            }

            if ($order->orderStatusOption_id == 10) {
                $statusNotes[] = 'trying to deliver an order that is complete ['.$order->orderID.']';
                $options = $defaultOptions;
                $options['statusNotes'] = serialize($statusNotes);
                $options['methodCalled'] = __METHOD__;
                $this->update($options,$where);

                return true;
            }

            if ($order->orderStatusOption_id == 12) {
                //change to partially delivered
                $options['orderStatusOption_id'] = 11;
            } else {
                $options['orderStatusOption_id'] = 9;
            }

            $statusNotes[] = "Updated order status to ". $options['orderStatusOption_id'];

        } else {

            // If the order is NOT on 7, try to capture
            if ($order->orderStatusOption_id != 7) {
                // Try to capture payment
                $result = $this->order_model->capture($order->orderID, $order->business_id);
                if ($result['status']=='success' || $customer->customer_deliver_on_hold) {
                    // If the order was 'Out for Partial Delivery', then change the status to 'Partially Delivered.
                    if ($order->orderStatusOption_id == 12) {
                        //change to partially delivered
                        $options['orderStatusOption_id'] = 11;
                    } else { //Otherwise, change to ready for pickup
                        $options['orderStatusOption_id'] = 9;
                    }
                } else {
                    // Fail - put on payment hold
                    // The capture function will update the order and place on 7
                    // Just to be safe, I make sure the order will be set to 13, even though it should already be 13
                    $options['orderStatusOption_id'] = 13;
                    $statusNotes[] = "Updated order status to 13 (delivery failure - unpaid). If the order is currently not on payment hold, we will try to capture. Therefore, the status might not be 13 after the capture attempt.";
                }
            } else {
                $options['orderStatusOption_id'] = 13;
                $statusNotes[] = "Updated order status to 13 (delivery failure - unpaid). If the order is currently not on payment hold, we will try to capture. Therefore, the status might not be 13 after the capture attempt.";
            }
        }

        //check to see if the order has a blank customer...
        $business = new Business($order->business_id);
        if ($order->customer_id == $business->blank_customer_id) {
            // send email to notify supprt that a delivery has been made with a blank customer
            $message = "Order delivered with a blank customer";
            $message .= "Order: {$order->orderID}<br>";
            $message .= "LockerName: $firstLocker $lockerListDescription <br>";
            $message .= "Location: {$orderLocation->address}<br>";
            send_admin_notification('Order ['.$order->orderID.'] delivered with a blank customer', $message,$order->business_id);
        }

        //update order
        $options['order_id'] = $order->orderID;
        $options['business_id'] = $order->business_id;
        $options['locker_id'] = $locker_id;
        $options['employee_id'] = $delivery->employee_id;
        $options['lockerListDescription'] = $lockerListDescription;
        
        $response = $this->order_model->update_order($options);
        
        if ($lockerListDescription) {
            $this->order_model->addNote($order->orderID, $lockerListDescription);
        }

        // insert transaction into driver log
        $this->load->model('driver_log_model');
        $options = array();
        $options['order_id'] = $order->orderID;
        $options['employee_id'] = $delivery->employee_id;
        $options['delTime'] = $delivery->deliveryTime;
        $options['delType'] = 'delivery';
        $options['locker_id'] = $locker_id;
        $options['bagNumber'] = $delivery->bagNumber;
        $options['location_id'] = $orderLocation->locationID;
        $options['business_id'] = $delivery->business_id;
        $this->driver_log_model->insertTransaction($options);

        //flag it as delivered for faster querying
        $update1 = "update orders set delivered = 1 where orderID = '$delivery->order_id';";
        $update = $this->db->query($update1);
        $affected = $this->db->affected_rows();

        $statusNotes[] = 'Order ['.$order->orderID.'] Delivered to '.mysql_real_escape_string($orderLocation->address).', Locker Number: '.$deliveryLocker->lockerID . $lockerListDescription;
        $options = $defaultOptions;
        $options['statusNotes'] = serialize($statusNotes);
        $options['methodCalled'] = __METHOD__;
        $this->update($options,$where);



    }

}
