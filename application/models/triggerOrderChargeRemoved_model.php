<?php

/**
 * Note,this table is populated by the trigger 'logRemovedOrderCharge' on the orderCharge table.
 */
class triggerOrderChargeRemoved_model extends MY_Model
{
    protected $tableName = 'triggerOrderChargeRemoved';
    protected $primary_key = 'triggerOrderChargedRemovedID';

}
