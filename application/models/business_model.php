<?php

use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\Reminder_Email;
use App\Libraries\DroplockerObjects\Business_Language;
use App\Libraries\DroplockerObjects\CreditCard;

class Business_Model extends MY_Model
{
    protected $tableName = 'business';
    protected $primary_key = 'businessID';

    /**
    * Retrieves all the businesses in a Codeigniter menu format
    */
    public function get_all_as_codeigniter_dropdown($only_active = false)
    {
        $business_menu = array();
        $sql = "SELECT businessID, companyName, city, state FROM business WHERE serviceTypes is not NULL ";
        if ($only_active) {
            $sql .= " AND active = 1 ";
        }
        $sql .= " ORDER BY companyName ASC";
        $businesses = $this->db->query($sql)->result();
        foreach ($businesses as $business) {
            $business_menu[$business->businessID] = $business->companyName .' - '.$business->city.', '.$business->state . ' (#'.$business->businessID.')';
        }

        return $business_menu;
    }
    /**
     * Returns all the businesses except for the master business when the server is in Bizzie mode
     * @return array
     * @throws \LogicException
     */
    public function get_all_slave_businesses()
    {
        if (!in_bizzie_mode()) {
            throw new \LogicException("This function can only be accessed when the server is in Bizzie mode");
        }
        $master_business = get_master_business();
        $slave_businesses = $this->db->query("SELECT * FROM business WHERE businessID != ?", $master_business->businessID)->result();

        return $slave_businesses;
    }
    /**
     * Note, the following function is  .
     * @deprecated Use get_aprax defined in MY_Model instead.
     * ----------------------------------
     *
     *
     * ----------------------------------
     * BEGIN
     * ----------------------------------
     * gets data from the business table
     *
     * Option values
     * ----------------------
     * businessID
     * website_url
     * slug
     * select
     * order_by
     *
     * @param array $options
     * @return array of Objects
     *
     * ----------------------------------
     * END
     * ----------------------------------
     */
    public function get($options = array())
    {
        if (isset($options['businessID'])) {
            $this->db->where('businessID', $options['businessID']);
        }
        if (isset($options['website_url'])) {
            $this->db->where('website_url', $options['website_url']);
        }
        if (isset($options['slug'])) {
            $this->db->where('slug', $options['slug']);
        }
        //if (isset($options['subdomain'])) {$this->db->where('subdomain', $options['subdomain']);}
        if (isset($options['select'])) {
            $this->db->select($options['select']);
        }
        if (isset($options['order_by'])) {
            $this->db->order_by($options['order_by']);
        }
        if (isset($options['real'])) {
            $this->db->where('serviceTypes is not NULL');
        }

        $query = $this->db->get('business');

        return $query->result();
    }


    /**
     * performs a serach for businesses based on a radius
     *
     * @param float $lat
     * @param float $lon
     * @param int $rad
     * @param int $limit
     * @throws Exception
     * @return array of businesses
     */
    public function getBydistance($lat, $lng, $rad = 10, $limit = 20)
    {
        $l1 = $lat;
        $l2 = $lng;
        $R = 6371;  // earth's radius, km

        // first-cut bounding box (in degrees)
        $maxLat = $lat + rad2deg(.1/$R);
        $minLat = $lat - rad2deg(.1/$R);
        // compensate for degrees longitude getting smaller with increasing latitude
        $maxlng = $lng + rad2deg(.1/$R/cos(deg2rad($lat)));
        $minlng = $lng - rad2deg(.1/$R/cos(deg2rad($lat)));

        // convert origin of filter circle to radians
        $lat = deg2rad($lat);
        $lng = deg2rad($lng);

        $sql = "
        Select businessID, address, lat, lng, address2, zipcode, city, state, companyName,
        acos(sin($lat)*sin(radians(lat)) + cos($lat)*cos(radians(lat))*cos(radians(lng)-$lng))*$R As D
        From (Select BusinessID, address, lat, lng, address2, zipcode, city, companyName, state
              From business
              Where lat>$minLat And lat<$maxLat
              And lng>$minlng And lng<$maxlng
              ) As FirstCut
        Where acos(sin($lat)*sin(radians(lat)) + cos($lat)*cos(radians(lat))*cos(radians(lng)-$lng))*$R < .1
        Order by D limit $limit";

        $q = $this->db->query($sql);

        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        $points = $q->result();

        for ($i=0;$i<sizeof($points);$i++) {
            $a[$points[$i]->businessID] = $points[$i];
        }

        $points = array();
        $lat = $l1;
        $lng = $l2;
        // first-cut bounding box (in degrees)
        $maxLat = $lat + rad2deg($rad/$R);
        $minLat = $lat - rad2deg($rad/$R);
        // compensate for degrees longitude getting smaller with increasing latitude
        $maxLon = $lng + rad2deg($rad/$R/cos(deg2rad($lat)));
        $minLon = $lng - rad2deg($rad/$R/cos(deg2rad($lat)));

        // convert origin of filter circle to radians
        $lat = deg2rad($lat);
        $lng = deg2rad($lng);

        $sql = "
        Select businessID, address, lat, lng, address2, zipcode, city, state, companyName,
        acos(sin($lat)*sin(radians(lat)) + cos($lat)*cos(radians(lat))*cos(radians(lng)-$lng))*$R As D
        From (
        Select businessID, address, lat, lng, address2, zipcode, hours, city, companyName, state
        From business
        Where lat>$minLat And lat<$maxLat
        And lng>$minLon And lng<$maxLon
        ) As FirstCut
        Where acos(sin($lat)*sin(radians(lat)) + cos($lat)*cos(radians(lat))*cos(radians(lng)-$lng))*$R < $rad
        Order by D limit $limit";

        $q = $this->db->query($sql);

        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        $points = $q->result();
        for ($i=0;$i<sizeof($points);$i++) {
            $a[$points[$i]->locationID] = $points[$i];
        }

        return $a;
    }

    public function get_all_by_distance_for_app($lat, $lon)
    {
        $sql = "
            SELECT businessID, business.companyName, business.city, business.state, business.website_url, location.lat, location.lon, locationID,
                3959 * acos(cos(radians(?)) * cos(radians(location.lat)) * cos(radians(location.lon) - radians(?)) + sin(radians(?)) * sin(radians(location.lat))) As distance
            FROM business
            JOIN location ON (hq_location_id = locationID)
            WHERE
                business.serviceTypes IS NOT NULL
                AND show_in_droplocker_app = 1
            ORDER BY distance ASC, business.companyName ASC";

        return $this->db->query($sql, array($lat, $lon, $lat))->result();
    }

    /**
     * Retrieves a business by a hashed id.
     * @param string $hash
     * @return stdClass
     */
    public function get_by_hash($hash)
    {
        $salt = $this->config->config['hash_salt'];
        $get_by_hash_query = $this->db->query("SELECT * FROM $this->tableName WHERE MD5(CONCAT($this->primary_key, '$salt')) = ?", $hash);
        return $get_by_hash_query->row();
    }

    /**
     * Retrieves the timezone for a business.
     * @param int $business_id
     * @return \DateTimeZone
     * @throws \InvalidArgumentException
     * @throws \Database_Exception
     */
    public function get_timezone($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric.");
        }
        $this->db->select("timezone");
        $get_timezone_query = $this->db->get_where($this->table_name, array("businessID" => $business_id));
        if ($this->db->_error_message()) {
            throw new \Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $get_timezone_query_result = $get_timezone_query->row();

        return new \DateTimeZone($get_timezone_query_result->timezone);
    }

    /**
         * @deprecated use \App\Libraries\DroplockerObjects\Business
     * Updates a business
     *
     * @param array $options
     * @param array $where
     * @return int affected_rows()
     */
    public function update($options = array(), $where = array())
    {
        $this->db->update('business', $options, $where);

        return $this->db->affected_rows();
    }

    /**
         * @deprecated use \App\Libraries\DroplockerObjects\Business
     * Deletes a business
     *
     * @param array $where
     * @return int affected_rows
     */
    public function delete($where = array())
    {
        $this->db->delete('business', $where);

        return $this->db->affected_rows();
    }


    /**
         * @deprecated use \App\Libraries\DroplockerObjects\Business
     * Inserts a new business into the database
     *
     * @param array $options
     * @return int insert_id()
     */
    public function insert($options = array())
    {
        $this->db->insert('business', $options);

        return $this->db->insert_id();
    }

    /**
     * checks to see if a business has ACL roles
     *
     * @param int $business_id
     * @return boolean
     */
    public function checkAcl($business_id)
    {
        $query = $this->db->get_where('aclroles', array('business_id'=>$business_id));
        if ($query->num_rows()>0) {
            return true;
        }

        return false;
    }

    /**
     * copy_acl will add the default acl roles to a newly created business
     *
     * @param int $business_id The business to copy to
     * @param int $sourceBusiness_id The business to copy from
     * @return int
     */
    public function copyAcl($business_id, $sourceBusiness_id='')
    {
        if (empty($sourceBusiness_id)) {
            $sourceBusiness_id = 3;
        }

        //get all the old roles and put them in an array
        $oldRoles = $this->db->query("SELECT * FROM aclroles WHERE business_id = $business_id")->result();
        foreach ($oldRoles as $role) {
            $oldBusinessRoles[$role->name] = $role->aclID;
        }

        //delete all the current acl roles for the business
        $sql = "DELETE FROM aclroles WHERE business_id = $business_id";
        $this->db->query($sql);


        $sql = "INSERT INTO aclroles (name, roleorder, business_id)  SELECT name, roleorder, {$business_id} FROM aclroles WHERE business_id = $sourceBusiness_id";
        $this->db->query($sql);


        //get all the roles and put them in an array
        $roles = $this->db->query("SELECT * FROM aclroles WHERE business_id = $business_id")->result();
        foreach ($roles as $role) {
            $businessRoles[$role->name] = $role->aclID;
        }

        //get all the roles and put them in an array
        $roles = $this->db->query("SELECT * FROM aclroles WHERE business_id = $sourceBusiness_id")->result();
        foreach ($roles as $role) {
            $sourceBusinessRoles[$role->name] = $role->aclID;
        }

        //update all the users to the new roles
        $sql = 'SELECT * FROM employee
            INNER JOIN business_employee ON employeeID = employee_id
            WHERE business_id = '.$business_id;
        $employees = $this->db->query($sql)->result();
        foreach ($employees as $employee) {
            $oldAclName = array_search($employee->aclRole_id, $oldBusinessRoles);
            $newAclKey = $businessRoles[$oldAclName];
            if ($newAclKey) {
                $sql = "UPDATE business_employee SET aclRole_id = {$newAclKey} WHERE ID = $employee->ID";
                $this->db->query($sql);
            } else {
                $error = "Some employees where not updated due to missing or outdated roles";
            }
        }

        //delete all the current acl permissions
        $sql = "DELETE FROM acl WHERE business_id = $business_id";
        $this->db->query($sql);


        // copy all the resources
        $sql = "SELECT * FROM acl WHERE business_id = $sourceBusiness_id";
        $sourceACL = $this->db->query($sql)->result();
        foreach ($sourceACL as $acl) {
            if ($acl->type=='role') {
                $aclName = array_search($acl->type_id, $sourceBusinessRoles);
                $newKey = @$businessRoles[$aclName];
                if (!empty($newKey)) {
                    $sql = "INSERT INTO acl (type, type_id, resource_id, action, business_id) VALUES ('role', {$newKey}, {$acl->resource_id}, '{$acl->action}', {$business_id})";
                    $this->db->query($sql);
                }
            }
        }

        if (isset($error)) {
            return $error;
        }

        return true;
    }

    /**
     * Copies reminders and reminder emails from one business to another
     * @param int $destination_business_id
     * @param int $source_business_id
     */
    public function copy_reminders_and_reminderEmails($source_business_id, $destination_business_id)
    {
        if ($destination_business_id == $source_business_id) {
            throw new Exception("Can not copy from a destination businesss that is the same as the source business");
        } else {
            $existing_destination_reminders = Reminder::search_aprax(array("business_id" => $destination_business_id));
            array_map(function ($destination_reminder) {
                $destination_reminder->delete();
            }, $existing_destination_reminders);
            $source_reminders = Reminder::search_aprax(array("business_id" => $source_business_id));

            $this->load->model("reminder_email_model");
            $this->load->model("business_language_model");
            foreach ($source_reminders as $source_reminder) {
                $source_reminder_emails = $this->reminder_email_model->get_aprax(array("reminder_id" => $source_reminder->reminderID));
                $destination_reminder = $source_reminder->copy();
                $destination_reminder->business_id = $destination_business_id;
                $destination_reminder->save();
                foreach ($source_reminder_emails as $source_reminder_email) {
                    $source_business_language = $this->business_language_model->get_by_primary_key($source_reminder_email->business_language_id);
                    $existing_destination_business_language_for_source_language = $this->business_language_model->get_one(array("language_id" => $source_business_language->language_id, "business_id" => $destination_business_id));
                    //The following conditional checks to see if there is a
                    if (empty($existing_destination_business_language_for_source_language)) {
                        $destination_business_language = Business_Language::create($destination_business_id, $source_business_language->language_id);
                        $destination_business_language_id = $destination_business_language->{Business_Language::$primary_key};
                    } else {
                        $destination_business_language_id = $existing_destination_business_language_for_source_language->business_languageID;
                    }

                    $destination_reminder_email = Reminder_Email::create($destination_business_language_id, $destination_reminder->reminderID, $source_reminder_email->subject, $source_reminder_email->body);
                }
            }
        }
    }


    public function copyCoupon($destination, $source = 3)
    {
        //delete all the current emails for the business
        $sql = "DELETE FROM coupon WHERE business_id = $destination";
        $this->db->query($sql);

        $sql = "INSERT INTO coupon (prefix, startDate, endDate, amount, amountType, frequency, description, extendedDesc, code, maxAmt, combine, firstTime, redeemed, business_id, email, fullName, order_id, recurring_type_id)
            SELECT `prefix`, `startDate`, `endDate`, `amount`, `amountType`, `frequency`, `description`, `extendedDesc`, `code`, `maxAmt`, `combine`, `firstTime`, `redeemed`, {$destination}, `email`, `fullName`, `order_id`, `recurring_type_id`
            FROM coupon
            WHERE business_id = $source AND prefix != 'GF'";
        $this->db->query($sql);

        return true;
    }

    public function copyReason($destination, $source = 3)
    {
        //delete all the current emails for the business
        $sql = "DELETE FROM discountReason WHERE business_id = $destination";
        $this->db->query($sql);

        $sql = "INSERT INTO discountReason (business_id, description)
                    SELECT {$destination}, `description`
                    FROM discountReason
                    WHERE business_id = $source";
        $this->db->query($sql);

        if ($error) {
            return $error;
        }

        return true;
    }


    public function copyFollowup($destination, $source = 3)
    {
        //delete all the current emails for the business
        $sql = "DELETE FROM followupCallScript WHERE business_id = $destination";
        $this->db->query($sql);

        $sql = "INSERT INTO followupCallScript
                    SELECT '',`text`, `followupCallType_id`, {$destination}
                    FROM followupCallScript
                    WHERE business_id = $source";
        $this->db->query($sql);

        if ($error) {
            return $error;
        }

        return true;
    }

    public function copyPrintNotice($destination, $source = 3)
    {
        //delete all the current emails for the business
        $sql = "DELETE FROM deliveryNotice WHERE business_id = $destination";
        $this->db->query($sql);

        $sql = "INSERT INTO deliveryNotice (noticeText, barcodeText, business_id)
                    SELECT `noticeText`, `barcodeText`, {$destination}
                    FROM deliveryNotice
                    WHERE business_id = $source";
        $this->db->query($sql);

        if ($error) {
            return $error;
        }

        return true;
    }


    /**
     * checks to see if a business has default products setup
     *
     * @param int $business_id
     * @return boolean
     */
    public function checkDefaultProducts($business_id)
    {
        $query = $this->db->get_where('product', array('business_id'=>$business_id));
        if ($query->num_rows()>0) {
            return true;
        }

        return false;
    }

    /**
     * Convert business country to ISO-3166 format.
     *
     * @param string $country
     * @return string
     */
    public static function countryToISO3166($country)
    {
        // Set a default country as USA
        if (empty($country)) {
            $country = 'usa';
        }

        $countries = array(
            'usa' => 'US',
            'australia' => 'AU',
            'canada' => 'CA',
            'chile' => 'CL',
            'france' => 'FR',
            'germany' => 'DE',
            'mexico' => 'MX',
            'new zealand' => 'NZ',
            'netherlands' => 'NL',
            'russia' => 'RU',
            'spain' => 'ES',
            'uae' => 'AE',
            'uk' => 'GB',
            'south africa' => 'ZA'
        );

        return $countries[$country];
    }
    
    public function get_all_active_ids_for_driver_app()
    {
        $business_list = array();
        
        $businesses = $this->db->query("SELECT businessID FROM business WHERE serviceTypes is not NULL AND active=1")->result();
        foreach ($businesses as $business) {
            $business_list[] = $business->businessID;
        }

        return $business_list;
    }

    /**
     * Charges a customer associated with the specified business
     * @param int $business_id
     * @param int $amount
     * @return array (status = success/fail, message, amount)
     * {
     *      status => string success|fail
     *      message => string
     *      amount => float
     * }
     */
    public function capture($business_id, $amount=0)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        } elseif (!is_numeric($amount)) {
            throw new \InvalidArgumentException("'amount' must be numeric.");
        } else {
            $amount = number_format($amount, 2, '.', '');
            $this->load->library('creditcardprocessor');
            
            $customer_id = $this->get_invoicing_credit_card($business_id);

            $creditCard = CreditCard::search_aprax(array(
                'business_id'=>$this->business_id,
                'customer_id'=>$customer_id
            ));

            if (empty($creditCard)) {
                return array("status" => "error", 'message' => 'Payment Not Captured - Customer does not have Credit Card on file', 'amount' => $amount);
            }

            //create a new order for this payment
            $this->load->model('order_model');

            $location_id = get_business_meta($this->business_id, "droplocker_invoices_default_location");
            $product_id = get_business_meta($this->business_id, "droplocker_invoices_default_product");
            
            if (empty($location_id)) {
                return array("status" => "error", 'message' => 'Payment Not Captured - Business default location for monthly invoices is not defined <a target="_blank" href="/admin/admin/defaults">Setup</a>', 'amount' => $amount);
            }
            
            if (empty($product_id)) {
                return array("status" => "error", 'message' => 'Payment Not Captured - Business default product for monthly invoices is not defined <a target="_blank" href="/admin/admin/defaults">Setup</a>', 'amount' => $amount);
            }

            $options = array(
                "customer_id"           => $customer_id,
                "location_id"           => $location_id,
                "qty"                   => 1,
                "notes"                 => "Monthly License invoicing",
                "supplier_id"           => null,
                "item_id"               => 0,
                "product_id"            => $product_id,
                "process_id"            => 0,
                "upcharge"              => null,
                "color"                 => null,
                "brand"                 => null,
                "due_date"              => null,
                "notes"                 => "Monthly License invoicing",
                "source"                => null,
                "business_id"           => $this->business_id,
                "employee_id"           => get_employee_id($this->session->userdata('buser_id')),
                "price"                 => $amount,
                "check_for_bag_before_create"=> true
            );

            $new_order = $this->order_model->add_product_to_order_or_create($options);

            $order_id = null;
            if (!empty($new_order['order_id'])) {
                $order_id = $new_order['order_id'];
            }

            if (empty($order_id)) {
                send_admin_notification("Problem with payment for invoice ", "Problem with payment for invoice <br>" . $new_order['message']."", $business_id);
                
                return array('message' => "Payment Not Captured - " . $new_order['message']);
            }

            $response = $this->order_model->capture($order_id, $this->business_id);

            $order = new App\Libraries\DroplockerObjects\Order($order_id);
            if ($response['status'] == 'success') {
                $order_status = array(
                    "invetoried" => 3,
                    "completed"  => 10
                );

                foreach ($order_status  as $name=>$value) {
                    $orderStatus = new App\Libraries\DroplockerObjects\OrderStatus();
                    $orderStatus->order_id             = $order_id;
                    $orderStatus->locker_id            = $order->locker_id;
                    $orderStatus->employee_id          = get_employee_id($this->session->userdata('buser_id'));
                    $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
                    $orderStatus->note                 = "Captured Payment";
                    $orderStatus->save();
                }

                $order->orderStatusOption_id = $order_status['completed'];
                $order->save();

                return array('status' => "success", "message" => $response['message'], "amount" => $response['amount']);
            } else {
                // Credit card process FAIL
                $errorMessage = $response->get_message();
                
                send_admin_notification("Problem with payment for invoice ", "Problem with payment for invoice <br>{$errorMessage}", $business_id);
                

                return array('message' => "Payment Not Captured - Credit card on file but processor rejected it. ".$response->get_message()." ResultCode: " . $response->get_resultCode());
            }
        }
    }

    public function get_invoicing_credit_card($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new \InvalidArgumentException("'business_id' must be numeric");
        } else {
            $sql = "select * from customer c join customerMeta cm on cm.customer_id = c.customerID 
where c.business_id = ? and cm.`key`  = 'pay_droplocker_invoices' and cm.value = ? limit 0,1;";
            $result = $this->db->query($sql, array($this->business_id, $business_id))->result();
            if (!empty($result[0])) {
                return $result[0]->customerID;
            }
        }
        return false;
    }
}
