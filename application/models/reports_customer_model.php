<?php
use App\Libraries\DroplockerObjects\Business;
class Reports_Customer_Model extends My_Model
{
    protected $tableName = 'reports_customer';
    protected $primary_key = 'customerID';

    public function update_customers($full, $verbose, $missing){
        if ($missing) {
            $this->update_all_missing_customers($verbose);
        } else {
            if ($full) {
                $this->update_all_customers($verbose);
            } else {
                $this->update_changed_customers_and_orders();
            }
        }
    }

    /**
     * Updates all customers to the reports_customer table
     * @param $verbose
     */
    private function update_all_customers($verbose) {
        //need to break it in chucks so it doesn't use up all the memory
        $this->db->query("TRUNCATE reports_customer");
        $customerCount = "select max(customerID) as custCount from customer";
        $q = $this->db->query($customerCount);
        $count = $q->result();
        $increment = 50;
        for ($currentCust = 0; $currentCust < $count[0]->custCount; $currentCust += $increment) {
            $getOrders = $this->return_customer_and_order_selects() .
                        "where customerID >= $currentCust
                        and customerID < ($currentCust + $increment)
                        and (o.bag_id != 0 OR o.bag_id IS NULL)
                        group by customerID
                        order by o.dateCreated";

            try {
                $q = $this->db->query($getOrders);
                $orders = $q->result();
                $this->update_report_customer_table($orders);
            } catch (Exception $e) {
                throw($e);
            }
            if ($verbose) {
                $percent = round($currentCust * 100 / $count[0]->custCount);
                echo "\rAdvance: $currentCust/{$count[0]->custCount} ($percent%)";
            }
        }
    }

    /**
     * Updates all customers missing from the reports_customer table
     * @param $verbose
     */
    private function update_all_missing_customers($verbose) {
        $missing_ids_query = "SELECT customerID FROM customer WHERE customer.customerID NOT IN (SELECT customerID FROM reports_customer)";
        echo $missing_ids_query . "\n";
        $missing_ids_fetch = $this->db->query($missing_ids_query);
        $missing_ids = $missing_ids_fetch->result_array();
        $increment = 50;
        $missing_ids_total = count($missing_ids);
        for ($currentCust = 0; $currentCust < $missing_ids_total; $currentCust += $increment) {
            $customers_where_in = $this->concat_missing_ids($currentCust, $increment, $missing_ids);
            $getCustomer = $this->return_customer_and_order_selects() .
                            "where customerID in ($customers_where_in)
                            and (o.bag_id > 0 or o.bag_id is null)
                            group by customerID";
            try {
                $q = $this->db->query($getCustomer);
                $orders = $q->result();
                $this->update_report_customer_table($orders);
            } catch (Exception $e) {
                throw($e);
            }
            if ($verbose) {
                $percent = round($currentCust * 100 / $missing_ids_total);
                echo "\rAdvance: $currentCust/{$missing_ids_total} ($percent%)";
            }
        }
    }

    /**
     * Updates any Customer that have been updated more recently then the most recent reports_customer row.  Also updates customer
     * if any orders have recently been updated.
     */
    private function update_changed_customers_and_orders() {
        //find out the last time this was updated
        $getLastUpdate = "select max(updated) as lastUpdate from reports_customer";
        $q = $this->db->query($getLastUpdate);
        $lastUpdate = $q->result();
        //get a list of all customers that have had upated orders and new customers since the last time this ran
        $getCustomers = "select distinct customer_id from orders where orders.statusDate > '".$lastUpdate[0]->lastUpdate."' and customer_id is not null
                            UNION
                            select distinct customerID as customer_id from customer where customer.signupDate > '".$lastUpdate[0]->lastUpdate."';";
        $q = $this->db->query($getCustomers);
        $customers = $q->result();
        //just update those customers
        $counter = 0;
        foreach ($customers as $customer) {
            $getOrders = $this->return_customer_and_order_selects() .
                            "where customerID = $customer->customer_id
                            and (o.bag_id != 0 OR o.bag_id IS NULL)
                            group by customerID
                            order by o.dateCreated";
            try {
                $q = $this->db->query($getOrders);
                $orders = $q->result();
                $this->update_report_customer_table($orders);
                $counter ++;
            } catch (Exception $e) {
                throw($e);
            }
        }
    }

    private function return_customer_and_order_selects() {
        return "select customer.customerID,
                customer.firstName,
                customer.lastName,
                customer.email,
                customer.phone,
                customer.username,
                customer.signupDate,
                customer.autopay,
                CASE WHEN subLocation.locationid IS NOT NULL
                  THEN subLocation.locationid
                  ELSE customer.location_id
                END location_id,
                customer.sms,
                customer.type,
                customer.address1,
                customer.address2,
                customer.city,
                customer.state,
                customer.zip,
                customer.starchOnShirts_id,
                customer.invoice,
                customer.noEmail,
                customer.referredBy,
                customer.kioskAccess,
                customer.wfNotes,
                customer.how,
                customer.dcNotes,
                customer.inactive,
                customer.lockerLootTerms,
                customer.W9,
                customer.twitterid,
                customer.mgrAgreement,
                customer.ip,
                customer.lat,
                customer.lon,
                customer.hold,
                customer.permAssemblyNotes,
                customer.owner_id,
                customer.birthday,
                customer.sex,
                customer.pendingEmailUpdate,
                customer.defaultWF,
                customer.business_id,
                customer.internalNotes,
                customer.preferences,
                customer.customerNotes,
                customer.default_business_language_id,
                customer.autoPay,
                customer.shirts,
                customer.twitterId,
                CASE WHEN subLocation.route_id is not null 
                   THEN subLocation.route_Id
                   ELSE location.route_id 
                 END route_id,
                orderID, 
                o.bag_id, 
                sum(o.closedGross) as totGross, 
                sum(o.closedDisc) as totDisc, 
                count(o.orderID) as orderCount, 
                datediff(now(), o.dateCreated) as dateDiff,
                max(o.dateCreated) as lastOrder,
                min(o.dateCreated) as firstOrder,
                count(DISTINCT DATE_FORMAT(o.dateCreated, '%Y-%m-%d')) as visits
                from customer as customer
                left join orders as o on o.customer_id = customerID and o.bag_id > 0
                left join location
                  on location.locationid = customer.location_id
                left join delivery_customerSubscription
                  on delivery_customerSubscription.customer_id = customerid
                left join delivery_zone
                  on delivery_customerSubscription.delivery_zone_id = delivery_zoneid
                left join location subLocation
                  on delivery_zone.location_id = subLocation.locationid
                ";
    }

    private function concat_missing_ids($currentCust, $increment, $missing_ids) {
        $stop = $currentCust + $increment;
        $concat_missing_ids = "";
        for($i = $currentCust; $i < $stop; $i++) {
            if (isset($missing_ids[$i])) {
                if ($i == $currentCust) {
                    $concat_missing_ids .= $missing_ids[$i]['customerID'];
                } else {
                    $concat_missing_ids .= "," . $missing_ids[$i]['customerID'];
                }
            }
        }
        return $concat_missing_ids;
    }

    /**
     * Called from update_customer_report_table to do the calculations and insert
     */
    private function update_report_customer_table($customers) {
        foreach($customers as $key => $customer) {
            $lastOrder = strtotime($customer->lastOrder);
            $firstOrder = strtotime($customer->firstOrder);
            $seconds_diff = $lastOrder - $firstOrder;
            $daysElapsed = floor($seconds_diff/86400);
            if ($daysElapsed > 0) {
                $avgOrderLapse = $daysElapsed / ($customer->visits - 1);
                $avgOrderLapse = number_format($avgOrderLapse,2, '.', '');
            } else {
                $avgOrderLapse = 0;
            }
            $planStatus = $this->get_plan_status($customer->customerID);

            $customer->email = strlen(trim($customer->email)) == 0?$customer->customerID . "@droplocker.com":$customer->email;
            
            $this->db->replace('reports_customer', array(
                'customerID'          => $customer->customerID,
                'firstName'           => $customer->firstName,
                'lastName'            => $customer->lastName,
                'email'               => $customer->email,
                'phone'               => $customer->phone,
                'username'            => $customer->username,
                'signupDate'          => $customer->signupDate,
                'autoPay'             => $customer->autoPay,
                'location_id'         => $customer->location_id,
                'sms'                 => $customer->sms,
                'type'                => $customer->type,
                'shirts'              => $customer->shirts,
                'address1'            => $customer->address1,
                'address2'            => $customer->address2,
                'city'                => $customer->city,
                'state'               => $customer->state,
                'zip'                 => $customer->zip,
                'starchOnShirts_id'   => $customer->starchOnShirts_id,
                'invoice'             => $customer->invoice,
                'noEmail'             => $customer->noEmail,
                'referredBy'          => $customer->referredBy,
                'kioskAccess'         => $customer->kioskAccess,
                'wfNotes'             => $customer->wfNotes,
                'how'                 => $customer->how,
                'dcNotes'             => $customer->dcNotes,
                'inactive'            => $customer->inactive,
                'lockerLootTerms'     => $customer->lockerLootTerms,
                'W9'                  => $customer->W9,
                'twitterId'           => $customer->twitterId,
                'mgrAgreement'        => $customer->mgrAgreement,
                'ip'                  => $customer->ip,
                'lat'                 => $customer->lat,
                'lon'                 => $customer->lon,
                'hold'                => $customer->hold,
                'permAssemblyNotes'   => $customer->permAssemblyNotes,
                'owner_id'            => $customer->owner_id,
                'birthday'            => $customer->birthday,
                'sex'                 => $customer->sex,
                'pendingEmailUpdate'  => $customer->pendingEmailUpdate,
                'defaultWF'           => $customer->defaultWF,
                'business_id'         => $customer->business_id,
                'internalNotes'       => $customer->internalNotes,
                'customerNotes'       => $customer->customerNotes,
                'totGross'            => $customer->totGross,
                'totDisc'             => $customer->totDisc,
                'firstOrder'          => $customer->firstOrder,
                'lastOrder'           => $customer->lastOrder,
                'orderCount'          => $customer->orderCount,
                'visits'              => $customer->visits,
                'avgOrderLapse'       => $avgOrderLapse,
                'default_business_language_id' => $customer->default_business_language_id,
                'route_id'            => $customer->route_id,
                'planStatus'          => $planStatus
            ));
        }
    }

    private function get_plan_status($id) {
        $planStatus = "";
        $activePlansSQL = "SELECT * 
                                FROM laundryPlan
                                WHERE customer_id = ?
                                AND active = 1
                                ORDER BY startDate ASC";
        $activePlansQuery = $this->db->query($activePlansSQL, [$id]);
        $activePlans = $activePlansQuery->result();
        $planCount = count($activePlans);
        if ($planCount == 1) {
            $planStatus = "Active - " . $activePlans[0]->description;
        } else if ($planCount > 1) {
            $planStatus = "Active - Multiple Plans";
        } else if ($planCount == 0) {
            // Check to see if NoLongerActive or None
            $inactivePlansSQL = "SELECT count(*) AS planCount
                                    FROM laundryPlan
                                    WHERE customer_id = ?
                                    AND active = 0";
            $inactivePlansQuery = $this->db->query($inactivePlansSQL, [$id]);
            $inactivePlans = $inactivePlansQuery->row();
            if ($inactivePlans->planCount > 0) {
                $planStatus = "NoLongerActive";
            } else {
                $planStatus = "None";
            }
        }
        return $planStatus;

    }
}
