<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class ItemSpecification_Model extends My_Model
{
    protected $tableName = 'item_specification';
    protected $primary_key = 'item_specificationID';

    public function insert_batch($array)
    {
        $this->db->insert_batch('item_specification', $array);
    }

    /**
     * Returns the item specifications
     * @param int $item_id The item ID
     */
    public function get_specifications($item_id)
    {
        $this->db->select("specification_id, description");
        $this->db->join("specification", "specificationID = specification_id");
        $result = $this->db->get_where($this->tableName, array("item_id" => $item_id));

        return $result->result();
    }

    /**
     * function deletes rows then does an insert
     *
     * @param int $item_id
     * @param array $specifications If not set, then all specifications for the items are removed
     */
    public function update($item_id, $specifications)
    {
        $this->db->where('item_id', $item_id);
            $this->db->delete('item_specification');

            if (isset($specifications)) {
                foreach ($specifications as $s) {
                        $a[] = array('item_id'=>$item_id, 'specification_id'=>str_replace("specification-","",$s));
                }
            $this->db->insert_batch('item_specification', $a);
            }
    }

     /**
     * Removes and one time
     * @param int $item_id The item ID
     */
    public function remove_oneTimeSpecification($item_id)
        {
            $delOneTime = "DELETE FROM item_specification
                            where item_id = $item_id
                            and specification_id in (select specificationID from specification where business_id = $this->business_id and everyOrder != 1)";
            $this->db->query($delOneTime);

        }

}
