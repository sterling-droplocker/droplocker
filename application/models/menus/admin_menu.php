<?php
/**
 * The following class renders the sidebar navigation menu in the admin interface.
 */
class Admin_Menu extends CI_Model
{
    public $menu = array();
    public $submenu = array();
    public $bizzie_mode;
    public $is_superadmin;

    /**
     * If the intended entry is to be a header, then the menu item expects the following parameter:
     *  header: text that represents the title for the header
     * If the intended entry is to be a clickable menu item, then the menu item expects the following parameters:
     *  link: the url that the menu item should direct to when licked on
     *  icon: The image associated with the menu
     *  text: describes the menu item
     *  attributes: text that contains HTML attributes that should also be applied to the menu item
     */
    public function __construct()
    {
        $this->bizzie_mode = in_bizzie_mode();
        $this->is_superadmin = is_superadmin();

        enter_translation_domain("Admin Tabs");

        $menu['index']         = array('text' => __(null, 'Home'),      'link' =>'/admin/index');
        $menu['orders']        = array('text' => __(null, 'Orders'),    'link' => '/admin/orders');
        $menu['customers']     = array('text' => __(null, 'Customers'), 'link' => '/admin/customers');
        $menu['employees']     = array('text' => __(null, 'Employees'), 'link' => '/admin/employees/view');
        $menu['mobile']        = array('text' => __(null, 'Mobile'),    'link' => '/admin/mobile', 'hidden' => true);
        $menu['reports']       = array('text' => __(null, 'Reports'),   'link' => '/admin/reports');
        $menu['website']       = array('text' => __(null, 'Website'),   'link' => '/admin/website');
        $menu['admin']         = array('text' => __(null, 'Admin'),     'link' => '/admin/admin');
        $menu['followupCalls'] = array("text" => __(null, 'Follow Up Calls'), 'link' => '/admin/followupCalls', 'hidden' => true);

        if ($this->is_superadmin) {
            $menu['superadmin'] = array('text' => __(null, 'Super Admin'), 'link' => '/admin/superadmin');
        }

        // reset translation domain, so it does not get used in other place
        leave_translation_domain();

        $this->menu = $menu;
    }

    public function menu_followupCalls()
    {
        enter_translation_domain("Admin Menu/Followup Calls");

        /* Followup Calls */
        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'View Customers'),
            'link' => '/admin/customers/view',
            'icon' => '/images/icons/eye.png'
        );

        $this->submenu['followupCalls'][] = array(
            'header' => __(null, 'Laundry Plans')
        );

        if ($this->bizzie_mode) {
            $this->submenu['followupCalls'][] = array(
                'text' => __(null, 'Manage Franchisee Laundry Plans'),
                'link' => '/admin/customers/laundry_plans',
                'icon' => '/images/icons/add.png'
            );
        } else {
            $this->submenu['followupCalls'][] = array(
                'text' => __(null, 'Add/Edit Laundry Plans'),
                'link' => '/admin/customers/laundry_plans',
                'icon' => '/images/icons/add.png'
            );
        }

        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'View Customer Plans'),
            'link' => '/admin/customers/customer_plans',
            'icon' => '/images/icons/user.png'
        );

        $this->submenu['followupCalls'][] = array(
            'header' => __(null, 'Coupons and Discounts')
        );

        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'Apply Coupon to All Customers'),
            'link' => '/admin/customers/apply_coupon_to_all_form',
            'icon' => '/images/icons/tag_blue_edit.png'
        );

        $this->submenu['followupCalls'][] = array(
            'header' => __(null, 'Follow Up Calls')
        );

        // Note, the followup call settings interface shall only be accessible by a superadmin when the server is in bizzie mode.
        if ($this->bizzie_mode && $this->is_superadmin) {
            $this->submenu['followupCalls'][] = array(
                'text' => __(null, 'Settings'),
                'link' => '/admin/followupCalls/',
                'icon' => '/images/icons/application_edit.png'
            );
        }
        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'Inactive Customers'),
            'link' => '/admin/followupCalls/view_inactive_customers',
            'icon' => '/images/icons/eye.png'
        );
        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'New Customers'),
            'link' => '/admin/followupCalls/view_new_signups',
            'icon' => '/images/icons/eye.png'
        );
        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'First Order Customers'),
            'link' => '/admin/followupCalls/view_firstOrder_customers',
            'icon' => '/images/icons/eye.png'
        );
        $this->submenu['followupCalls'][] = array(
            'text' => __(null, 'Onhold Customers'),
            'link' => '/admin/followupCalls/view_onHold_customers',
            'icon' => '/images/icons/eye.png'
        );

        leave_translation_domain();
    }

    public function menu_customers()
    {
        enter_translation_domain("Admin Menu/Customers");

        /* Customers */
        $this->submenu['customers'][] = array(
            'header' => __(null, 'Customers')
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Add Customer'),
            'link' => '/admin/customers/add',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Import Customers'),
            'link' => '/admin/customers/import',
            'icon' => '/images/icons/group.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'View Customers'),
            'link' => '/admin/customers/view',
            'icon' => '/images/icons/eye.png'
        );

        $this->submenu['customers'][] = array(
            'header' => __(null, 'Laundry Plans')
        );

        $this->submenu['customers'][] = array(
            'text' => __(null, 'Add/Edit Laundry Plans'),
            'link' => '/admin/customers/laundry_plans',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'View Customer Plans'),
            'link' => '/admin/customers/customer_plans',
            'icon' => '/images/icons/user.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Add/Edit Everything Plans (Beta)'),
            'link' => '/admin/customers/everything_plans',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'View Customer Everything Plans'),
            'link' => '/admin/customers/customer_everything_plans',
            'icon' => '/images/icons/user.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Add/Edit Product Plans (Beta)'),
            'link' => '/admin/customers/product_plans',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'View Customer Product Plans'),
            'link' => '/admin/customers/customer_product_plans',
            'icon' => '/images/icons/user.png'
        );
        $this->submenu['customers'][] = array(
            'header' => __(null, 'Follow Up Calls')
        );

        // Note, the followup call settings interface shall only be accessible by a superadmin when the server is in bizzie mode.
        if ($this->bizzie_mode && $this->is_superadmin) {
            $this->submenu['customers'][] = array(
                'text' => __(null, 'Settings'),
                'link' => '/admin/followupCalls/',
                'icon' => '/images/icons/application_edit.png'
            );
        }
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Inactive Customers'),
            'link' => '/admin/followupCalls/view_inactive_customers/',
            'icon' => '/images/icons/eye.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'New Customers'),
            'link' => '/admin/followupCalls/view_new_signups',
            'icon' => '/images/icons/eye.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'First Order Customers'),
            'link' => '/admin/followupCalls/view_firstOrder_customers',
            'icon' => '/images/icons/eye.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Onhold Customers'),
            'link' => '/admin/followupCalls/view_onHold_customers',
            'icon' => '/images/icons/eye.png'
        );

        $this->submenu['customers'][] = array(
            'header' => __(null, 'Other')
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Apply Coupon to All Customers'),
            'link' => '/admin/customers/apply_coupon_to_all_form',
            'icon' => '/images/icons/tag_blue_edit.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Out Of Pattern Customers'),
            'link' => '/admin/customers/outOfPattern',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['customers'][] = array(
            'text' => __(null, 'Setup Filters'),
            'link' => '/admin/customers/filters',
            'icon' => '/images/icons/folder_user.png'
        );
        /* End of customers menu */

        leave_translation_domain();
    }

    public function menu_orders()
    {
        $CI = get_instance();

        enter_translation_domain("Admin Menu/Orders");

        $this->submenu['orders'][] = array(
            'header' => __(null, 'Orders')
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'View All Open Orders'),
            'link' => '/admin/orders/view',
            'icon' => '/images/icons/basket_put.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Orders by Due Date'),
            'link' => '/admin/orders/due_date',
            'icon' => '/images/icons/calendar_view_day.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Quick Scan Bag'),
            'link' => '/admin/orders/quick_scan_bag',
            'icon' => '/images/icons/lightning.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Print Receipts'),
            'link' => '/admin/orders/print_receipts',
            'icon' => '/images/icons/printer.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Print Bag Tags'),
            'link' => '/admin/orders/bag_tags',
            'icon' => '/images/icons/printer.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Manual Assembly'),
            'link' => '/admin/orders/print_assembly',
            'icon' => '/images/icons/shape_move_back.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Claimed Orders'),
            'link' => '/admin/orders/claimed',
            'icon' => '/images/icons/lock_edit.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Scheduled Orders'),
            'link' => '/admin/orders/scheduled',
            'icon' => '/images/icons/calendar.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Quick Order'),
            'link' => '/admin/orders/quick_order',
            'icon' => '/images/icons/lightning.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Barcode Lookup'),
            'link' => '/admin/orders/barcode_lookup',
            'icon' => '/images/icons/magnifier.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Point of Sale'),
            'link' => '/admin/pos',
            'icon' => '/images/icons/application_lightning.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Point of Sale Beta'),
            'link' => 'https://pos.droplocker.com',
            'icon' => '/images/icons/application_lightning.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Booked order list'),
            'link' => '/admin/orders/booked',
            'icon' => '/images/icons/calendar.png'
        );
        // Note, the wholesale menu items shall not be accessible in Bizzie mode.
        if (! $this->bizzie_mode) {
            $this->submenu['orders'][] = array(
                'header' => __(null, 'Wholesale')
                );
            $this->submenu['orders'][] = array(
                'text' => __(null, 'Export Orders'),
                'link' => '/admin/orders/export_orders/file',
                'icon' => '/images/icons/basket_remove.png'
            );
            $this->submenu['orders'][] = array(
                'text' => __(null, 'Upload Orders'),
                'link' => '/admin/orders/wholesale_upload',
                'icon' => '/images/icons/disk.png'
                );
            $this->submenu['orders'][] = array(
                'text' => __(null, 'View Orders'),
                'link' => '/admin/orders/wholesale_view',
                'icon' => '/images/icons/magnifier.png'
                );
            $this->submenu['orders'][] = array(
                'text' => __(null, 'Import Setup'),
                'link' => '/admin/orders/wholesale_mapping',
                'icon' => '/images/icons/arrow_switch.png'
                );
            $this->submenu['orders'][] = array(
                'text' => __(null, 'Close Orders'),
                'link' => '/admin/orders/close_wholesale',
                'icon' => '/images/icons/database_go.png'
                );
        }

        $this->submenu['orders'][] = array(
            'header' => __(null, 'Wash & Fold')
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Approve WF Orders'),
            'link' => '/admin/orders/wf_approve',
            'icon' => '/images/icons/tick.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Clean WF Orders'),
            'link' => '/admin/orders/wf_processing',
            'icon' => '/images/icons/tag_blue_edit.png'
        );

        $this->submenu['orders'][] = array(
            'header' => __(null, 'Export')
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Export Orders'),
            'link' => '/admin/orders/export_orders',
            'icon' => '/images/icons/basket_remove.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, 'Export Settings'),
            'link' => '/admin/orders/export_settings',
            'icon' => '/images/icons/application_edit.png'
        );
        $this->submenu['orders'][] = array(
            'text' => __(null, '3rd Party Deliveries / Pickups'),
            'link' => '/admin/third_party_deliveries/',
            'icon' => '/images/icons/car.png'
        );

        leave_translation_domain();
    }

    public function menu_item()
    {
        enter_translation_domain("Admin Menu/Item");

        $this->submenu['item'][] = array(
            'text' => __(null, 'Quick Scan Bag'),
            'link' => '/admin/orders/quick_scan_bag',
            'icon' => '/images/icons/lightning.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Print Receipts'),
            'link' => '/admin/orders/print_receipts',
            'icon' => '/images/icons/printer.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Print Bag Tags'),
            'link' => '/admin/orders/bag_tags',
            'icon' => '/images/icons/printer.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Manual Assembly'),
            'link' => '/admin/orders/print_assembly',
            'icon' => '/images/icons/shape_move_back.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'View All Claimed'),
            'link' => '/admin/orders/claimed',
            'icon' => '/images/icons/basket.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Scheduled Orders'),
            'link' => '/admin/orders/scheduled',
            'icon' => '/images/icons/calendar.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Quick Order'),
            'link' => '/admin/orders/quick_order',
            'icon' => '/images/icons/lightning.png'
        );

        // Note, wholesale menu items should not be shown in Bizzie mode
        if (! $this->bizzie_mode) {
            $this->submenu['item'][] = array(
                'header' => __(null, 'Wholesale')
            );
            $this->submenu['item'][] = array(
                'text' => __(null, 'Export Orders'),
                'link' => '/admin/orders/export_orders?selected=file',
                'icon' => '/images/icons/basket_remove.png'
            );
            $this->submenu['item'][] = array(
                'text' => __(null, 'Upload Orders'),
                'link' => '/admin/orders/wholesale_upload',
                'icon' => '/images/icons/disk.png'
            );
            $this->submenu['item'][] = array(
                'text' => __(null, 'View Orders'),
                'link' => '/admin/orders/wholesale_view',
                'icon' => '/images/icons/magnifier.png'
            );
            $this->submenu['item'][] = array(
                'text' => __(null, 'Import Mapping'),
                'link' => '/admin/orders/wholesale_mapping',
                'icon' => '/images/icons/arrow_switch.png'
            );
            $this->submenu['item'][] = array(
                'text' => __(null, 'Close Orders'),
                'link' => '/admin/orders/close_wholesale',
                'icon' => '/images/icons/database_go.png'
            );
        }
        $this->submenu['item'][] = array(
            'header' => __(null, 'Wash & Fold')
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Approve WF Orders'),
            'link' => '/admin/orders/wf_approve',
            'icon' => '/images/icons/tick.png'
        );
        $this->submenu['item'][] = array(
            'text' => __(null, 'Clean WF Orders'),
            'link' => '/admin/orders/wf_processing',
            'icon' => '/images/icons/tag_blue_edit.png'
        );

        leave_translation_domain();
    }

    public function menu_employees()
    {
        enter_translation_domain("Admin Menu/Employees");

        $this->submenu['employees'][] = array(
            'header' => __(null, 'Employees')
        );

        $this->submenu['employees'][] = array(
            'text' => __(null, 'Add Employee'),
            'link' => '/admin/employees/manage',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['employees'][] = array(
            'text' => __(null, 'View Employees'),
            'link' => '/admin/employees/view',
            'icon' => '/images/icons/magnifier.png'
        );

        // Note, the 'timecard' menu shall not be accessible when in bizzie mode.
        if (! $this->bizzie_mode) {
            $this->submenu['employees'][] = array(
                'header' => __(null, 'Timecards')
            );
            $this->submenu['employees'][] = array(
                'text' => __(null, 'Timecards'),
                'link' => '/admin/employees/timecard',
                'icon' => '/images/icons/clock.png'
            );
            $this->submenu['employees'][] = array(
                'text' => __(null, 'Time Off Request'),
                'link' => '/admin/employees/timeoff',
                'icon' => '/images/icons/calendar.png'
            );
            $this->submenu['employees'][] = array(
                'text' => __(null, 'Presser Scanning'),
                'link' => '/admin/employees/presser_scans',
                'icon' => '/images/icons/award_star_gold_2.png'
            );
            $this->submenu['employees'][] = array(
                'header' => __(null, 'Roles/Permissions')
            );
            $this->submenu['employees'][] = array(
                'text' => __(null, 'Manage Employee Roles'),
                'link' => '/admin/employees/roles',
                'icon' => '/images/icons/user.png'
            );
        }

        leave_translation_domain();
    }

    public function menu_reports()
    {
        enter_translation_domain("Admin Menu/Reports");

        /* Reports Menu */
        $this->submenu['reports'][] = array(
            'header' => __(null, 'Production Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Customers'),
            'link' => '/admin/reports/customers',
            'icon' => '/images/icons/user.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Top Customers'),
            'link' => '/admin/reports/show_top_customers',
            'icon' => '/images/icons/user.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Production Report'),
            'link' => '/admin/reports/production_report',
            'icon' => '/images/icons/chart_curve.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Invoicing'),
            'link' => '/admin/reports/invoicing',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Mark-In Log'),
            'link' => '/admin/reports/markin_log',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Daily Items'),
            'link' => '/admin/reports/daily_items',
            'icon' => '/images/icons/camera.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'WF Supplier Production'),
            'link' => '/admin/reports/wf_supplier_production',
            'icon' => '/images/icons/group_link.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Supplier Manifest'),
            'link' => '/admin/reports/supplier_manifest',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Employee Productivity'),
            'link' => '/admin/reports/employee_productivity',
            'icon' => '/images/icons/user.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'PCS per Hour'),
            'link' => '/admin/reports/pcs_per_hour',
            'icon' => '/images/icons/clock_go.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Assembly Log'),
            'link' => '/admin/reports/assembly_log',
            'icon' => '/images/icons/shape_group.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Un-Assembled Items'),
            'link' => '/admin/reports/unassembled_items',
            'icon' => '/images/icons/shape_square_delete.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Order Notes'),
            'link' => '/admin/reports/order_notes',
            'icon' => '/images/icons/note.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Complete Activity Log'),
            'link' => '/admin/reports/activity_log',
            'icon' => '/images/icons/book_open.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Operations Dashboard'),
            'link' => '/admin/reports/operations_dashboard',
            'icon' => '/images/icons/layout_content.png'
        );

        $this->submenu['reports'][] = array(
            'text' => __(null, 'Reassigned Barcodes'),
            'link' => '/admin/reports/reassigned_barcodes',
            'icon' => '/images/icons/barcode.png'
        );
        
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Employee Log In/Out'),
            'link' => '/admin/reports/employee_login_out',
            'icon' => '/images/icons/user.png'
        );

        $this->submenu['reports'][] = array(
            'text' => __(null, 'Slow Mark-Ins'),
            'link' => '/admin/reports/item_speed',
            'icon' => '/images/icons/speed.png'
        );

        $this->submenu['reports'][] = array(
            'header' => __(null, 'Sales Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Sales by Location'),
            'link' => '/admin/reports/by_location',
            'icon' => '/images/icons/chart_bar.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Sales by Order Type'),
            'link' => '/admin/reports/by_order_type',
            'icon' => '/images/icons/chart_pie.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Sales Tax'),
            'link' => '/admin/reports/sales_tax',
            'icon' => '/images/icons/money_dollar.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Orders by Location'),
            'link' => '/admin/reports/orders_by_location',
            'icon' => '/images/icons/chart_bar.png'
        );

        $this->submenu['reports'][] = array(
            'text' => __(null, 'Daily Orders by Location'),
            'link' => '/admin/reports/order_count',
            'icon' => '/images/icons/house.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Locker History'),
            'link' => '/admin/reports/locker_history',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Follow Up Calls'),
            'link' => '/admin/reports/followupCall',
            'icon' => '/images/icons/telephone.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Daily Revenue'),
            'link' => '/admin/reports/daily_revenue',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Credit Card Deposits'),
            'link' => '/admin/reports/cc_deposits',
            'icon' => '/images/icons/creditcards.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Salesmen Commission'),
            'link' => '/admin/reports/sales_commission',
            'icon' => '/images/icons/car.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Double Charges'),
            'link' => '/admin/reports/double_charges',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'How Did You Hear About Us'),
            'link' => '/admin/reports/how_hear',
            'icon' => '/images/icons/help.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Laundry Plans'),
            'link' => '/admin/reports/laundry_plans',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Everything Plans'),
            'link' => '/admin/reports/everything_plans',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Product Plans'),
            'link' => '/admin/reports/product_plans',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['reports'][] = array(
            'header' => __(null, 'Driver Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Map v3'),
            'link' => '/admin/drivers/manifest',
            'icon' => '/images/icons/map.png',
            'attributes' => 'target="_blank"'
        );        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Map'),
            'link' => '/admin/reports/driver_map_v2',
            'icon' => '/images/icons/map.png',
            'attributes' => 'target="_blank"'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Log'),
            'link' => '/admin/reports/driver_log',
            'icon' => '/images/icons/lorry.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Dashboard'),
            'link' => '/admin/reports/driver_dashboard',
            'icon' => '/images/icons/hourglass.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Notes'),
            'link' => '/admin/reports/driver_notes',
            'icon' => '/images/icons/note_edit.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'End Of Route Report'),
            'link' => '/admin/reports/possible_problems',
            'icon' => '/images/icons/error.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Revenue'),
            'link' => '/admin/reports/driver_revenue',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Driver Phones'),
            'link' => '/admin/reports/driver_phone',
            'icon' => '/images/icons/phone.png'
        );

        $this->submenu['reports'][] = array(
            'header' => __(null, 'Finance Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Unpaid Orders'),
            'link' => '/admin/reports/unpaid_orders',
            'icon' => '/images/icons/money_delete.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Property Commission'),
            'link' => '/admin/reports/property_commission',
            'icon' => '/images/icons/house.png'
        );
        //Note, Invoices shall not be accessible in Bizzie mode.
        if (! $this->bizzie_mode) {
            $this->submenu['reports'][] = array(
                'text' => __(null, 'Drop Locker Invoices'),
                'link' => '/admin/reports/dl_invoices',
                'icon' => '/images/icons/droplocker.png'
            );
        }

        $this->submenu['reports'][] = array(
            'header' => __(null, 'Location Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Location History'),
            'link' => '/admin/reports/display_location_history_form',
            'icon' => '/images/icons/map.png'
        );
        $this->submenu['reports'][] = array(
            'header' => __(null, 'Discount Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Coupons Redeemed'),
            'link' => '/admin/reports/display_discounts_given_over_time_period_form',
            'icon' => '/images/icons/tag_blue_edit.png'
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Coupon Usage'),
            'link' => '/admin/reports/coupon_usage',
            'icon' => '/images/icons/tag_blue.png'
        );
        $this->submenu['reports'][] = array(
            'header' => __(null, 'POS Reports')
        );
        $this->submenu['reports'][] = array(
            'text' => __(null, 'Activity'),
            'link' => '/admin/reports/pos_activity',
            'icon' => '/images/icons/application_edit.png'
        );

        leave_translation_domain();
    }

    public function menu_website()
    {
        enter_translation_domain("Admin Menu/Website");

        $this->submenu['website'][] = array(
            'header' => __(null, 'Website')
        );

        /* Note, Website Menu shall not be accessible in Bizzie mode*/
        if (! $this->bizzie_mode) {
            $this->submenu['website'][] = array(
                'text' => __(null, 'Details and Status'),
                'link' => '/admin/website/details',
                'icon' => '/images/icons/server_go.png'
            );
        }

        $this->submenu['website'][] = array(
            'text' => __(null, 'Add/Edit Images'),
            'link' => '/admin/website/edit_logo',
            'icon' => '/images/icons/page_edit.png'
        );

        $this->submenu['website'][] = array(
            'text' => __(null, 'Manage Customer Website Template'),
            'link' => '/admin/website/edit_website_code',
            'icon' => '/images/icons/html.png'
        );
        $this->submenu['website'][] = array(
            'text' => __(null, 'Website Analytics'),
            'link' => '/admin/website/analytics',
            'icon' => '/images/icons/report.png'
        );
        $this->submenu['website'][] = array(
            'text' => __(null, 'Features'),
            'link' => '/admin/website/features',
            'icon' => '/images/icons/cog.png'
        );
        $this->submenu['website'][] = array(
            'text' => __(null, 'Iphone App'),
            'link' => '/admin/website/manage_iphone_app',
            'icon' => '/images/icons/phone.png'
        );

        leave_translation_domain();
    }

    public function menu_admin()
    {
        enter_translation_domain("Admin Menu/Admin");

        /* Admin Menu */
        $this->submenu['admin'][] = array(
            'header' => __(null, 'Orders')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Add/Edit Service Types'),
            'link' => '/admin/admin/services',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['admin'][] = array(
            'header' => __(null, 'Modules')
        );

        $this->submenu['admin'][] = array(
            'text' => __(null, 'Wash and Fold'),
            'link' => '/admin/modules/wash_and_fold',
            'icon' => '/images/icons/basket.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Dry Clean'),
            'link' => '/admin/modules/dry_clean',
            'icon' => '/images/icons/basket.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'UpCharge'),
            'link' => '/admin/upcharges',
            'icon' => '/images/icons/basket.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Shoe'),
            'link' => '/admin/modules/shoe',
            'icon' => '/images/icons/basket.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Bulk Pricing Update'),
            'link' => '/admin/admin/bulk_pricing_update',
            'icon' => '/images/icons/money_dollar.png'
        );

        $this->submenu['admin'][] = array(
            'header' => __(null, 'Business Settings')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Business Diagnostic'),
            'link' => '/admin/admin/diagnose',
            'icon' => '/images/icons/spellcheck.png'
        );

        $this->submenu['admin'][] = array(
            'text' => __(null, 'Business Details'),
            'link' => '/admin/admin/edit',
            'icon' => '/images/icons/application_edit.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Business Defaults'),
            'link' => '/admin/admin/defaults',
            'icon' => '/images/icons/application_edit.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Custom Options'),
            'link' => '/admin/custom_options',
            'icon' => '/images/icons/application_form_edit.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Mobile App Download'),
            'link' => '/admin/admin/mobile',
            'icon' => '/images/icons/phone_add.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Manage Did You Know Notices'),
            'link' => '/admin/didYouKnows',
            'icon' => '/images/icons/email.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Follow Up Calls'),
            'link' => '/admin/followupCalls',
            'icon' => '/images/icons/telephone.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Timecard Setup'),
            'link' => '/admin/admin/timecard_setup',
            'icon' => '/images/icons/time_go.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Payment Processing'),
            'link' => '/admin/admin/processor',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Pressing Stations'),
            'link' => '/admin/admin/pressing_stations',
            'icon' => '/images/icons/medal_gold_1.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'SMS Provider Settings'),
            'link' => '/admin/sms',
            'icon' => '/images/icons/phone.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Holiday Schedule'),
            'link' => '/admin/holidays',
            'icon' => '/images/icons/calendar.png'
        );

        $this->submenu['admin'][] = array(
            'header' => __(null, 'Customer Communications')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Email Templates'),
            'link' => '/admin/email_templates',
            'icon' => '/images/icons/email.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Newsletters'),
            'link' => '/admin/newsletters',
            'icon' => '/images/icons/newspaper.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Reminders'),
            'link' => '/admin/reminders',
            'icon' => '/images/icons/email.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Mailchimp Beta'),
            'link' => '/admin/mailchimp',
            'icon' => '/images/icons/mailchimp.png'
        );

        $this->submenu['admin'][] = array(
            'header' => __(null, 'Product Settings')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Quick Items'),
            'link' => '/admin/quick_items',
            'icon' => '/images/icons/lightning_go.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Allowed Barcodes'),
            'link' => '/admin/admin/manage_barcodes',
            'icon' => '/images/icons/application_edit.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Per Location Pricing'),
            'link' => '/admin/admin/location_pricing',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Special Item Settings'),
            'link' => '/admin/admin/item_specifications',
            'icon' => '/images/icons/error.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Manage Tax Rates'),
            'link' => '/admin/taxTables',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['admin'][] = array(
            'header' => __(null, 'Suppliers')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Create New Supplier'),
            'link' => '/admin/suppliers/create_new_supplier',
            'icon' => '/images/icons/lorry_add.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Suppliers'),
            'link' => '/admin/suppliers',
            'icon' => '/images/icons/lorry.png'
        );

        $this->submenu['admin'][] = array(
            'header' => __(null, 'Locations')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Add Location'),
            'link' => '/admin/locations/display_update_form',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'View Locations'),
            'link' => '/admin/admin/view_all_locations',
            'icon' => '/images/icons/magnifier.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Master Routes'),
            'link' => '/admin/locations/master_routes',
            'icon' => '/images/icons/chart_line.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Home Delivery Settings'),
            'link' => '/admin/home_delivery/settings',
            'icon' => '/images/icons/plugin_edit.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Deliv Locations'),
            'link' => '/admin/home_delivery/locations',
            'icon' => '/images/icons/house_go.png'
        );

        $this->submenu['admin'][] = array(
            'header' => __(null, 'Tools')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Email Admin'),
            'link' => '/admin/admin/email_admin',
            'icon' => '/images/icons/email.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Support Tickets'),
            'link' => '/admin/tickets',
            'icon' => '/images/icons/monitor_lightning.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Shoe Tags'),
            'link' => '/admin/admin/shoe_tags',
            'icon' => '/images/icons/tag_blue.png'
        );
        // Note, the functionality for managing or creating gift cards should not be accessible when the server is in Bizzie mode.
        if (! $this->bizzie_mode) {
            $this->submenu['admin'][] = array(
                'text' => __(null, 'Create Gift Card'),
                'link' => '/admin/gift_cards/create',
                'icon' => '/images/icons/money.png'
            );
            $this->submenu['admin'][] = array(
                'text' => __(null, 'Manage Gift Cards'),
                'link' => '/admin/gift_cards/index',
                'icon' => '/images/icons/newspaper_go.png'
            );
        }
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Maintenance Log'),
            'link' => '/admin/maintenance_log',
            'icon' => '/images/icons/wrench.png'
        );
        // Note, the OSHA Injury Log menu item shall not be shown when in Bizzie mode.
        if (! $this->bizzie_mode) {
            $this->submenu['admin'][] = array(
                'text' => __(null, 'OSHA Injury Log'),
                'link' => '/admin/osha_injury',
                'icon' => '/images/icons/user.png'
            );
        }
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Manage Coupons'),
            'link' => '/admin/admin/manage_coupons',
            'icon' => '/images/icons/tag_blue_edit.png'
        );
        // Note, the testimonials should not be accessible when the server is in Bizzie mode.
        if (! $this->bizzie_mode) {
            $this->submenu['admin'][] = array(
                'text' => __(null, 'Testimonials'),
                'link' => '/admin/testimonials',
                'icon' => '/images/icons/comment.png'
            );
        }
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Print Notices'),
            'link' => '/admin/notices',
            'icon' => '/images/icons/printer.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Rewards Programs'),
            'link' => '/admin/admin/locker_loot',
            'icon' => '/images/icons/money.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Card Readers'),
            'link' => '/admin/card_readers',
            'icon' => '/images/icons/drive_user.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Data Dump'),
            'link' => '/admin/data_dump',
            'icon' => '/images/icons/database_go.png'
        );

         $this->submenu['admin'][] = array(
            'text' => __(null, 'Data Dump Log'),
            'link' => '/admin/data_dump/logs',
            'icon' => '/images/icons/database_go.png'
        );
         
        $this->submenu['admin'][] = array(
            'text' => __(null, 'App Downloads'),
            'link' => '/admin/admin/mobile',
            'icon' => '/images/icons/application_edit.png'
        );

        $this->submenu['admin'][] = array(
            'header' => __(null, 'Internationalization')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Manage Translations'),
            'link' => '/admin/admin/manage_languageKeys',
            'icon' => '/images/icons/world_edit.png'
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Manage Languages'),
            'link' => '/admin/admin/manage_business_languages',
            'icon' => '/images/icons/world_edit.png'
        );
        $this->submenu['admin'][] = array(
            'header' => __(null, 'POS')
        );
        $this->submenu['admin'][] = array(
            'text' => __(null, 'Manage cash register'),
            'link' => '/admin/pos_manager/cash_register',
            'icon' => '/images/icons/application_edit.png'
        );

        leave_translation_domain();
    }

    public function menu_locations()
    {

        enter_translation_domain("Admin Menu/Locations");

        /* Locations Menu */
        $this->submenu['locations'][] = array(
            'header' => __(null, 'Locations')
        );
        $this->submenu['locations'][] = array(
            'text' => __(null, 'Add Location'),
            'link' => '/admin/locations/display_update_form',
            'icon' => '/images/icons/add.png'
        );
        $this->submenu['locations'][] = array(
            'text' => __(null, 'View Locations'),
            'link' => '/admin/admin/view_all_locations',
            'icon' => '/images/icons/magnifier.png'
        );
        $this->submenu['locations'][] = array(
            'text' => __(null, 'Master Routes'),
            'link' => '/admin/locations/master_routes',
            'icon' => '/images/icons/chart_line.png'
        );
        $this->submenu['locations'][] = array(
            'text' => __(null, 'Home Delivery Settings'),
            'link' => '/admin/home_delivery/settings',
            'icon' => '/images/icons/plugin_edit.png'
        );
        $this->submenu['locations'][] = array(
            'text' => __(null, 'Deliv Locations'),
            'link' => '/admin/home_delivery/locations',
            'icon' => '/images/icons/house_go.png'
        );

        leave_translation_domain();
    }

    public function menu_superadmin()
    {
        enter_translation_domain("Admin Menu/Superadmin");

        /* Superadmin Menu */
        if ($this->is_superadmin) {
            $this->submenu['superadmin'][] = array(
                'header' => __(null, 'Business')
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Add Business'),
                'link' => '/admin/superadmin/add_business',
                'icon' => '/images/icons/add.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'View Businesses'),
                'link' => '/admin/superadmin/view_businesses',
                'icon' => '/images/icons/eye.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'License Invoicing'),
                'link' => '/admin/superadmin/license_invoicing',
                'icon' => '/images/icons/money.png'
            );

            if ($this->bizzie_mode) {
                $this->submenu['superadmin'][] = array(
                    'header' => __(null, 'Master Business Settings')
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Master Coupons'),
                    'link' => '/admin/superadmin/manage_coupons',
                    'icon' => '/images/icons/tag_blue_edit.png'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Default Logos'),
                    'link' => '/admin/website/edit_logo',
                    'icon' => '/images/icons/image_edit.png'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Set Master Business'),
                    'link' => '/admin/superadmin/manage_master_business',
                    'icon' => '/images/icons/building_edit.png'
                );

                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Customer Website Features'),
                    'link' => '/admin/website/features',
                    'icon' => '/images/icons/page_white_edit.png'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Master Did You Know Notices'),
                    'link' => '/admin/superadmin/manage_master_did_you_knows',
                    'icon' => '/images/icons/overlays.png'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Master Laundry Plans'),
                    'link' => '/admin/superadmin/manage_master_laundry_plans',
                    'icon' => '/images/icons/magnifier'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Products'),
                    'link' => '/admin/superadmin/manage_dryclean_module',
                    'icon' => '/images/icons/basket.png'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Email Templates'),
                    'link' => '/admin/superadmin/manage_email_templates',
                    'icon' => '/images/icons/email.png'
                );
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Master Print Notices'),
                    'link' => '/admin/superadmin/manage_print_notices',
                    'icon' => '/images/icons/printer.png'
                );
            }

            $this->submenu['superadmin'][] = array(
                'header' => __(null, 'Employees')
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'View Employees'),
                'link' => '/admin/superadmin/view_employees',
                'icon' => '/images/icons/eye.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Add Employee to Business'),
                'link' => '/admin/employees/manage',
                'icon' => '/images/icons/user_add.png'
            );

            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Manage All Business Employee Roles'),
                'link' => '/admin/employees/roles',
                'icon' => '/images/icons/user.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Manage ACL Resources'),
                'link' => '/admin/superadmin/manage_aclresources',
                'icon' => '/images/icons/lock_edit.png'
            );

            $this->submenu['superadmin'][] = array(
                'header' => __(null, 'Tools')
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Manage Server Mode'),
                'link' => '/admin/superadmin/manage_server_mode',
                'icon' => '/images/icons/server_edit.png'
            );
            //Note, the Superadmin Rewards Programs shall only be accessible in Bizzie mode.
            if ($this->bizzie_mode) {
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Manage Rewards Programs'),
                    'link' => '/admin/superadmin/manage_rewards_programs',
                    'icon' => '/images/icons/money.png'
                );
            }
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Email Server Settings'),
                'link' => '/admin/superadmin/view_email_server_settings',
                'icon' => '/images/icons/server_edit.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Setup Business'),
                'link' => '/admin/superadmin/setup_business',
                'icon' => '/images/icons/arrow_switch.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'System Email Admin'),
                'link' => '/admin/superadmin/email_admin',
                'icon' => '/images/icons/email.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'System Email History'),
                'link' => '/admin/superadmin/view_email_log',
                'icon' => '/images/icons/email.png'
            );
            if (! $this->bizzie_mode) {
                $this->submenu['superadmin'][] = array(
                    'text' => __(null, 'Shipment Calendar'),
                    'link' => '/admin/superadmin/shipment_overview',
                    'icon' => '/images/icons/calendar.png'
                );
            }

            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'SMS Flow Test'),
                'link' => '/admin/tools/test_sms_claim',
                'icon' => '/images/icons/phone.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Product Import'),
                'link' => '/admin/superadmin/product_import',
                'icon' => '/images/icons/page_white_get.png'
            );

            $this->submenu['superadmin'][] = array(
                'header' => __(null, 'Reports')
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Cron Jobs'),
                'link' => '/admin/superadmin/recent_cron_jobs',
                'icon' => '/images/icons/report.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Locations'),
                'link' => '/admin/superadmin/locations',
                'icon' => '/images/icons/house.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Api Calls'),
                'link' => '/admin/superadmin/view_api_request_logs',
                'icon' => '/images/icons/report.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Active Accounts'),
                'link' => '/admin/superadmin/view_active_accounts',
                'icon' => '/images/icons/report.png'
            );

            $this->submenu['superadmin'][] = array(
                'header' => __(null, 'Internationalization')
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Manage Language Views and Keys'),
                'link' => '/admin/superadmin/manage_languageKeys_and_languageViews',
                'icon' => '/images/icons/world_edit.png'
            );
            $this->submenu['superadmin'][] = array(
                'text' => __(null, 'Manage Languages'),
                'link' => '/admin/superadmin/manage_languages',
                'icon' => '/images/icons/world.png'
            );
            $this->submenu['serviceTypes'] = $this->submenu['superadmin'];
        }

        leave_translation_domain();
    }

    public function menu_index()
    {

        enter_translation_domain("Admin Menu/Home");

        $this->submenu['index'][] = array(
            'text' => __(null, 'Dashboard Settings'),
            'link' => '/admin/index/dashboard_settings',
            'icon' => '/images/icons/application_view_tile.png'
        );

        leave_translation_domain();
    }

    public function get_submenu($active_page = '')
    {
        if (! $active_page) {
            $CI = get_instance();
            $active_page = ! empty($CI->submenu) ? $CI->submenu : $CI->uri->segment(2);
        }

        if (empty($active_page)) {
            return $this->get_submenu('index');
        }

        if (!isset($this->submenu[$active_page])) {
            $function = "menu_" . $active_page;
            if (method_exists($this, $function )) {
                $this->$function();
            } else {
                return $this->get_submenu('index');
            }
        }

        return $this->submenu[$active_page];
    }
}
