<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 *
 */
class CustomerHistory_Model extends My_Model
{
    protected $tableName = 'customerHistory';
    protected $primary_key = 'customerHistoryID';

    /**
     * This function is depcreated
     * Inserts data into the customerHistory table
     *
     * @param string $note
     *            A message explaining the change made to the customer
     * @param int $customer_id
     * @param string $historyType
     * @param int $business_id
     * @param int $employee_id
     * @throws Exception if missing business_id and session business_id is not set
     * @return int The unique identifer of the new history Fitem.
     */
    public function insert($note, $customer_id, $historyType, $business_id, $employee_id = null)
    {
        if (empty($business_id)) {
            throw new Exception('Missing business ID');
        }

        if (is_null($employee_id)) {
            $employee_id = get_current_employee_id();
        }

        $options['note'] = $note;
        $options['customer_id'] = $customer_id;
        $options['historyType'] = $historyType;
        $options['business_id'] = $business_id;
        $options['employee_id'] = $employee_id;
        $this->db->insert('customerHistory', $options);

        return $this->db->insert_id();
    }

    /**
     * Retreives the notes history for the specified customer in the business.
     *
     * @param int $customer_id
     * @param int $business_id
     * @return array An array of stdClass objects
     * @throws Exception
     */
    public function get_history($customer_id, $business_id)
    {
        if (! is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        if (! is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->db->select("customerHistory.historyType, customerHistory.timestamp, customerHistory.note, employee.firstName, employee.lastName");
        $this->db->join("employee", sprintf("employee.employeeID = %s.employee_id", $this->tableName), 'left');
        $this->db->where("customerHistory.customer_id", $customer_id);
        $this->db->where("customerHistory.business_id", $business_id);
        $this->db->order_by("customerHistory.timestamp", "DESC");
        $get_customer_history_query = $this->db->get($this->tableName);
        $history = $get_customer_history_query->result();

        // merge in any support tickets
        $getTickets = "select concat('<a href=/admin/tickets/edit_ticket/', ticketID, '>Support Ticket</a>') as historyType, dateCreated as timestamp, concat('<a href=/admin/tickets/edit_ticket/', ticketID, '>', title, '</a>') as note, employee.firstName, employee.lastName
                        from ticket
                        join employee on employee_id = employeeID
                        where ticket.customer_id = $customer_id
                        order by ticket.dateCreated desc";
        $query = $this->db->query($getTickets);
        $tickets = $query->result();

        $fullHistory = array_merge($tickets, $history);
        usort($fullHistory, 'cmp');

        return $fullHistory;
    }
}

function cmp($a, $b)
{
    if ($a->timestamp > $b->timestamp) {
        return - 1;
    } else
        if ($a->timestamp == $b->timestamp) {
            return 0;
        } else {
            return 1;
        }
}
