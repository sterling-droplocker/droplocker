<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class LaundryPlan_Model extends My_Model
{
    protected $tableName = 'laundryPlan';
    protected $primary_key = 'laundryPlanID';

    public function insert($options = array())
    {
        $this->db->insert('laundryPlan', $options);

        return $this->db->insert_id();
    }


    public function get_active_laundry_plan($customer_id, $business_id, $type)
    {
        $sql = "SELECT * FROM laundryPlan 
                  WHERE customer_id = ?
                  AND active = 1 
                  AND type = ?
                  AND endDATE > CURDATE()
                  AND business_id = ?
                  ORDER BY startDate DESC";
        $activePlan = $this->db->query($sql,[$customer_id, $type, $business_id])->row();

        return $activePlan;
    }

    public function get_active_laundry_plans($customer_id, $business_id, $type)
    {
        $sql = "SELECT * FROM laundryPlan 
                  WHERE customer_id = ?
                  AND active = 1 
                  AND type = ?
                  AND endDATE > CURDATE()
                  AND business_id = ?
                  ORDER BY startDate DESC";
        $activePlans = $this->db->query($sql,[$customer_id, $type, $business_id])->result();

        return $activePlans;
    }

    public function get_active_laundry_plans_by_business_id($business_id)
    {
        $sql = "SELECT customerID, firstName, lastName, description, startDate, businessLaundryPlan_id, laundryPlanID
                FROM (laundryPlan)
                JOIN customer ON customerID = customer_id
                WHERE laundryPlan.active =  1
                AND laundryPlan.business_id =  ?
                AND laundryPlan.endDate > CURDATE()
                AND laundryPlan.type = 'laundry_plan' ORDER BY concat(firstName, lastName) ASC";

        return $this->db->query($sql, array($business_id))->result();
    }

    /**
     * gets the wash and fold price.
     *
     * @param int $customer_id
     * @param int $business_id
     * @return float
     */
    public function getDefaultPrice($customer_id, $business_id)
    {
        // viewing this page from a public page, ie... no cookies set yet
        $this->load->model('product_model');
        $defaultWF = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $business_id);

        $sql = "SELECT price FROM product_process pp WHERE pp.product_processID = ?";
        $defaultWfPrice = $this->db->query($sql, array($defaultWF))->row();

        if ( !empty( $customer_id ) ) {

            // get the price fror wash and fold for the customer
            $sql = "SELECT price FROM customer c
                    INNER JOIN product_process pp ON pp.product_processID = defaultWF
                    WHERE customerID = " . $customer_id;
            $customerWfPrice = $this->db->query($sql)->row();
        }

        if ( empty( $customerWfPrice->price ) || $customerWfPrice->price < 0.01  ) {
            return (float) $defaultWfPrice->price;
        } else {
            return (float) $customerWfPrice->price;
        }
    }


    /**
     * checks to see if the customer is going to upgrade after the enddate of the current plan
     */
    public function is_upgrading($customer_id)
    {
            if (!is_numeric($customer_id)) {
                throw new Exception("'customer_id' must be numeric.");
            }
            $sql = "SELECT renew, laundryPlanID FROM laundryPlan WHERE customer_id = {$customer_id} and active = 1 and type = 'laundry_plan'";
            $query = $this->db->query($sql);
            if ($query->num_rows()==1) {
                    return false;
            }

            foreach ($query->result() as $plan) {
                    if ($plan->renew == 1) {
                            return $plan->laundryPlanID;
                    }
            }

            return false;
    }

    /**
     * Retrieves a count of the nubmer of results for a query
     * @param array $options An associate array of WHERE parameters
     * @return int The count
     */
    public function count($options = array())
    {
        $options["type"] = "laundry_plan";
        foreach ($options as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->from($this->tableName);

        return $this->db->count_all_results();
    }

    /**
     * Retreives data based on the supplied criteria
     * @param $options array An array of WHERE parameters
     * @param $order_by string A column to order the results
     * @param $select string The fields to SELECT
     * @param limit int
     * @param offset int
     * @return array Consist of stdclasses with properties associated with the query result columns
     */
    public function get_aprax($options = array(), $order_by = null, $select = null, $limit = null, $offset = null)
    {
        if (!is_null($order_by)) {
            $this->db->order_by($order_by);
        }
        if ($select) {
            $this->db->select($select);
        }

        $options["type"] = "laundry_plan";
        
        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $this->db->where_in($key, $value);
            } else {
                $this->db->where($key, $value);
            }
        }

        $query = $this->db->get($this->tableName, $limit, $offset);

        return $query->result();
    }

    /**
     * This is *, do not use or I murder your face repeatedly
     * @deprecated use get_aprax() instead
     */
    public function get()
    {
        if ($this->required) {
                if(!required($this->required, $this->options))
                die("MISSING REQUIRED FIELDS:<br>DATABASE: ".$this->tableName."<hr>REQUIRED: <pre>".print_r($this->required, true)."</pre><hr>YOU PASSED:<pre>".print_r($this->options, true)."</pre><hr>");
        }

        if (empty($this->options)) {
            $this->options = array();
        }

        $this->options["type"] = "laundry_plan";
        if ($this->options) {
            foreach ($this->options as $key => $value) {
                if (($key != 'select') && ($key != 'order_by')) {
                    if (is_array($value)) {
                        $this->db->where_in($this->tableName.".".$key,$value);
                    } else {
                        $this->db->where($this->tableName.".".$key, $value);
                    }
                }
            }
        }

        if (isset($this->options['select'])) {
                $this->db->select($this->options['select']);
        }

        if (isset($this->options['order_by'])) {
                $this->db->order_by($this->options['order_by']);
        }

        if (sizeof($this->joins)) {
                foreach ($this->joins as $j) {
                        $a = explode(" ", $j);
                        //get tablename
                        $table = $a[0];
                        //remove tablename and "on"
                        unset($a[0]);
                        unset($a[1]);
                        //rebuild the statement
                        $statement = implode(" ",$a);

                        $this->db->join($table, $statement);
                }
        }

        if (sizeof($this->leftjoins)) {
                foreach ($this->leftjoins as $j) {
                        $a = explode(" ", $j);
                        //get tablename
                        $table = $a[0];
                        //remove tablename and "on"
                        unset($a[0]);
                        unset($a[1]);
                        //rebuild the statement
                        $statement = implode(" ",$a);

                        $this->db->join($table, $statement, "LEFT");
                }
        }

        $query = $this->db->get($this->tableName);

        return $query->result();
    }
}
