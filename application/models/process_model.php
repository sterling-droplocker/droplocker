<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Process_Model extends My_Model
{
    protected $tableName = 'process';
    protected $primary_key = 'processID';
    
    protected $translationDomain = "Processes";

    /**
     * @return array
    */
    public function get_as_codeigniter_dropdown()
    {
        $idKey = $this->primary_key;
        
        $items = $this->db->query("SELECT * FROM " . $this->tableName)->result();
        foreach ($items as $item) {
            $a[$item->$idKey] = $this->getTranslatedName($item);
        }

        return $a;
    }
    
    public function getName($itemID)
    {
        $items = $this->get_aprax(array($this->primary_key => $itemID));
        $name = $this->getTranslatedName($items[0]);
        
        return $name;
    }
    
    protected function getTranslatedName($item)
    {
        return get_translation($item->name, $this->translationDomain, array(), $item->name);
    }

}
