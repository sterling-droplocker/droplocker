<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Invoice_Model extends My_Model
{
    protected $tableName = 'invoice';
    protected $primary_key = 'order_id'; // THIS IS NOT A TYPO

    public function update($options = array(),$where=array())
    {
        $this->db->update('invoice', $options, $where);

        return $this->db->affected_rows();
    }
}
