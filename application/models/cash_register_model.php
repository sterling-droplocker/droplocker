<?php
use App\Libraries\DroplockerObjects\Cash_Register;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Employee;

class Cash_Register_Model extends My_Model
{
    protected $tableName = 'cash_register';
    protected $primary_key = 'cash_registerID';
    
    public function belongsToEmployee($cash_register_id, $employee_id)
    {
        //lb: if performance not good enough, just do it with  query
        $employee = new Employee($employee_id);
        
        $cashRegister = new Cash_Register($cash_register_id);
        $cashRegisterLocation = new Location($cashRegister->location_id);
        
        return $employee->getBusinessId() === $cashRegisterLocation->business_id;
    }
    
    public function getForLocation($location_id) 
    {
        return $this->get_aprax(array('location_id' => $location_id), 'name');
    }
    
    public function getEnabledLocationsForSelect($business_id)
    {
        $locations = array(
            0 => array(
                'id' => 0,
                'label' => get_translation("select_location", "pos/cash_register", array(), "Select Location"),
            ),
        );
        
        $result = $this->getForBusiness($business_id);
        
        foreach ($result as $item) {
            $location_id = $item->locationID;
            if (!$locations[$location_id]) {
                $locations[$location_id] = array (
                    'id' => $location_id,
                    'label' => $item->address,
                );
            };
            
            if (!$locations[$location_id]['cash_registers']) {
                $locations[$location_id]['cash_registers'][0] = array(
                    'cash_registerID' => 0, 
                    'name' => get_translation("select_cash_register", "pos/cash_register", array(), "Select Cash Register")
                );
            };
            
            $locations[$location_id]['cash_registers'][$item->cash_registerID] = array(
                'cash_registerID' => $item->cash_registerID,
                'location_id' => $item->location_id,
                'name' => $item->name,
                'notes' => $item->notes,       
            );
        }
        
        return $locations;
    }
    
    public function getForBusiness($business_id)
    {
        $this->db->join('location', 'location_id=locationID', 'left');
        $this->db->where("business_id", $business_id);
        $this->db->select("locationID, location_id, location.address, cash_registerID, cash_register.name, cash_register.notes");
        
        $query = $this->db->get($this->tableName);
        $result = $query->result();
        
        return $result;
    }
    
}
