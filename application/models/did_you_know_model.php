<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Did_You_Know_Model extends My_Model
{
    protected $tableName = 'didYouKnow';
    protected $primary_key = 'didYouKnowID';

    /**
     * Retrieves a random "Did You Know" note
     * @param int $business_id
     * @return string a "did you know" note
     * @throws Exception
     */
    public function get_random_note($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $this->db->select("note");
        $this->db->where("business_id", $business_id);
        $this->db->order_by("RAND()");
        $this->db->limit("1");
        $get_random_note_query = $this->db->get($this->tableName);
        $notes = $get_random_note_query->result();

        return isset($notes[0]->note)?$notes[0]->note:'';
    }

}
