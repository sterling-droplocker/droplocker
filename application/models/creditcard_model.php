<?php

class CreditCard_Model extends My_Model
{
    protected $tableName = 'creditCard';
    protected $primary_key = 'creditCardID';

    /**
     * @deprecated Use get_one() in My_Model instead
     * Gets the credit card for the customer
     * @param int $customer_id
     * @param int $business_id
     */
    public function get_customer_card($customer_id, $business_id)
    {
        $query = $this->db->query('SELECT * FROM creditCard WHERE customer_id = '.$customer_id. ' AND business_id = '.$business_id);

        return $query->result();
    }

    /**
     * deletes a credit card for a customer
     * @deprecated Use the CreditCard class in the Droplocker Objects
     * @param int $customer_id
     * @param int $business_id
     * @return int affected_rows();
     */
    public function delete_card($customer_id, $business_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->where('business_id', $business_id);
        $this->db->delete('creditCard');

        return $this->db->affected_rows();
    }


    /**
     * Saves a credit card for a customer
     * If the card exists, we delete it and then do the insert
     *
     * @param array $options
     * @return int creditCardID
     * @deprecated use the save functionality in the CreditCard class in the Droplocker Objects
     * Requires the following options
     *      customer_id
     *      business_id
     *      cardNumber
     *      csc
     *      expMo
     *      expYr
     *      cardType
     *      address1
     *      city
     *      state
     *      zip
     *      pnRef
    */
    public function save_card($options = array())
    {
        // save email for paypal cards
        if (!in_array($options['cardType'], array('PayPal', 'Coinbase'))) {
            $options['cardNumber'] = substr($options['cardNumber'], -4);
        }

        //don't store the csc
        unset($options['csc']);

        //delete all cards that are saved for the customer and business
        $this->db->delete('creditCard', array(
            'customer_id' => $options['customer_id'],
            'business_id' => $options['business_id']
        ));

        $this->db->insert('creditCard', $options);

        return $this->db->insert_id();
    }
    
    public function getPublicInfo($customer_id, $business_id)
    {
        $creditCard = $this->get_one(array("customer_id" => $customer_id, "business_id" => $business_id));
        
        $output = array(
            'customer_id' => $customer_id,
            'business_id' => $business_id,
        );
        
        if (!empty($creditCard)) {
            $output['creditCardID'] = $creditCard->creditCardID;
            $output['lastFourCardNumber'] = $creditCard->cardNumber;
            $output['expireMonth'] = $creditCard->expMo;
            $output['expireYear'] = $creditCard->expYr;
            $output['type'] = $creditCard->cardType;
            $output['is_one_time'] = $creditCard->is_one_time;
            
            $output['address1'] = $creditCard->address1;
            $output['address2'] = $creditCard->address2;
            $output['city'] = $creditCard->city;
            $output['state'] = $creditCard->state;
            $output['zip'] = $creditCard->zip;
            
            switch (strtolower($output['type'])) {
                case 'paypal':
                    $output['is_paypal'] = true;
                    break;
                case 'coinbase':
                    $output['is_bitcoin'] = true;
                    break;
                default:
                    $output['is_card'] = true;
            }
        } else {
            $output['lastFourCardNumber'] = null;
            $output['expireMonth'] = null;
            $output['expireYear'] = null;
            
            $output['no_card'] = true;
        }
        
        return $output;
    }

    public function updateBillingInfo($customer, $business_id)
    {
        if (empty($customer) || empty($business_id)) {
            return false;
        }
             
        $data = array(
                "address1" => $customer->address1,
                "city"     => $customer->city,
                "state"    => $customer->state,
                "zip"      => $customer->zip
            );

        $this->db->where(
                array(
                    "customer_id" => $customer->customerID,
                    "business_id" => $business_id
                )
            );

        if ($this->db->update($this->tableName, $data)) {
            return true;
        }
        
        return false;
    }
}
