<?php


class BusinessSetting_Model extends My_Model
{
    protected $tableName = 'businessSetting';
    protected $primary_key = 'businessSettingID';

    public function update($options = array(), $where=array())
    {
        $affected_rows = $this->db->update('businessSetting',$options, $where);

        return $affected_rows;
    }

}
