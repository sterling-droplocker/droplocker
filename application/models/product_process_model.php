<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Product_Process_Model extends My_Model
{
    protected $tableName = 'product_process';
    protected $primary_key = 'product_processID';

    public function get_as_codeigniter_dropdown($business_id)
    {
        $this->db->select("product.name AS product_name, process.name AS process_name, product_processID");
        $this->db->join("product", "productID=product_id");
        $this->db->join("process", "processID=process_id");
        $this->db->where("business_id", $business_id);
        $this->db->where("active", 1);
        $get_product_processes_query = $this->db->get("product_process");
        $product_processes = $get_product_processes_query->result();
        foreach ($product_processes as $product_process) {
            $result[$product_process->product_processID] = sprintf("%s - %s",  $product_process->product_name, $product_process->process_name);
        }

        return $result;
    }

    public function is_active($product_processID)
    {
        return $this->get_aprax(array(
            'active' => 1,
            'product_processID' => $product_processID
        ));
    }

    /**
     * Returns all active product processes and the processes associated
     *
     *  format:
     *  {
     *     productID1 =>  {
     *          [0] => {"processID" =>  processID, "name" => name},
     *          [1] => {"processID" => processID, "name" => name},
     *          ...
     *     },
     *     productID2 => {
     *          [0] => {"processID" => processID, "name" => name},
     *          [1] => {"processID" => processID, "name" => name},
     *          ...
     *     },
     *     ...
     *  }
     *
     * @param int $business_id
     * @return array
     */
    public function get_products_and_product_processes($business_id)
    {
        $products_product_processes = array();
        $sql = "SELECT productID, processID, process.name
                FROM product_process
                JOIN process ON (process_id = processID)
                JOIN product ON (product_id = productID)
                WHERE business_id = ?
                AND product_process.active = 1";

        $processes = $this->db->query($sql, array($business_id))->result();

        foreach ($processes as $index => $process) {
            $products_product_processes[$process->productID][] = array(
                'processID' => $process->processID,
                'name' => $process->name,
            );
        }

        return $products_product_processes;
    }

    /**
     *
     * @param int $product_id
     * @param int $process_id
     * @param float $price
     * @throws Exception
     * @return int The insert ID
     */
    public function insert($product_id, $process_id, $price = "0.00")
    {
        if (!is_numeric($product_id)) {
            throw new \InvalidArugmentException("'product_id' must be an integer.");
        }
        if (!is_numeric($process_id)) {
            throw new \InvalidArugmentException("'process_id' must be an integer.");
        }

        return $this->db->insert($this->tableName, array("product_id" => $product_id, "process_id" => $process_id, "price" => $price));

    }
    /**
     * @deprecated Use the Droplocker Object \App\Libraries\DroplockerObject\Product_Process instead, because it has data validation.
     * Updates the process for an existing product process
     * @param int $product_id
     * @param int $process_id The new product process
     * @return int The number of rows that were affected
     * @throws Exception
     */
    public function update_process($product_processID, $process_id)
    {
        if (!is_numeric($product_processID)) {
            throw new Exception("'product_processID' must be an integer.");
        }
        if (!is_numeric($process_id)) {
            throw new Exception("'process_id' must be an integer.");
        }
        $this->db->where(array("product_processID" => $product_processID));
        $rows_affected =  $this->db->update($this->name, array( "process_id" => $process_id));

        return $rows_affected;
    }
}
