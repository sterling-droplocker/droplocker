<?php
/**
 *
 * @author matt
 *
 */
class ServiceType_Model extends My_Model
{
    protected $tableName = 'serviceType';
    protected $primary_key = 'serviceTypeID';

    /**
     * get_formatted methods gets a simple array of service types.
     *
     * pass either an int or an array to get the servicetypes. If nothing is passed all service types are returned
     *
     * This method returns an array of servicetypes with the key being the serviceTypeID and the value being another array
     * including the name and slug of the service type
     *
     * @example setting array: $options['serviceTypeID'] = array(1,2)
     * @example setting int: $options['serviceTypeID'] = 1
     * @param array $options
     * @return array
     */
    public function get_formatted($options = array())
    {
        if (isset($options['serviceTypeID'])) {
            if (is_array($options['serviceTypeID'])) {
                $this->db->where_in('serviceTypeID', $options['serviceTypeID']);
            } else {
                $this->db->where('serviceTypeID', $options['serviceTypeID']);
            }
            unset($options['serviceTypeID']);
        }
        $this->db->where($options);

        $types = $this->get();
        foreach ($types as $t) {
            $type[$t->serviceTypeID] = array('name'=>$t->name, 'slug'=>$t->slug);
        }

        return $type;
    }

    /**
     * serializeServiceTypes method serializes an array of serviceTypes
     *
     * The serialized string is stored in the businesses table
     *
     * @param array $serviceTypes Optional
     * @return string
     */
    public function serializeServiceTypes($serviceTypes = array())
    {
        foreach ($serviceTypes as $key=>$value) {
            $newArray[$key] = $value['slug'];
        }
        $str = serialize($newArray);

        return $str;
    }

    /**
     * Overiding the MY_Model get method.
     *
     * Option Values
     * ---------------------
     * serviceTypeID
     *
     * serviceTypeID can be either an array or an int
     *
     * @example setting orderTypeID as array: $options['orderTypeID']=array(1,2,3);
     * @example setting orderTypeID as int: $options['orderTypeID']=1;
     * @param array $options
     * @return array of objects
     *
     */
    public function get($options = array())
    {
        if (isset($options['serviceTypeID'])) {
            if(is_array($options['serviceTypeID']))
                $this->db->where_in('serviceTypeID', $options['serviceTypeID']);
            else
                $this->db->where('serviceTypeID', $options['serviceTypeID']);
        }

        $this->db->order_by('name');

        $q = $this->db->get('serviceType');

        return $q->result();
    }

    public function get_as_codeigniter_dropdown($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric");
        }

        $result = $this->db->query("SELECT * FROM serviceType WHERE business_id = 0 OR business_id = ?", array($business_id))->result();

        $serviceTypes = array();
        foreach ($result as $serviceType) {
            $serviceTypes[$serviceType->serviceTypeID] = $serviceType->name;
        }

        return $serviceTypes;
    }
}
