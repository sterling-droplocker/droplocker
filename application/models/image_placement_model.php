<?php
class Image_Placement_Model extends My_Model
{
    protected $tableName = 'image_placement';
    protected $primary_key = 'image_placementID';

    /**
     * Retrieves the image for a placement for a particular business. If the server is in bizzie mode, then the iamge is pulled from the master business
     * @param string $name The name of the placement
     * @param string $business_id
     * @return Image_Placement_Model
     * @throws Exception
     */
    public function get_by_name($name, $business_id)
    {
        if (empty($name)) {
            throw new Exception("'name' can not be empty.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' can not be empty.");
        }
        //Note, If the server is in Bizzie mode, then we only retrieve the placement defined in the master business.
        if (in_bizzie_mode()) {
            $master_business = get_master_business();
            $business_id = $master_business->businessID;
        }

        $get_images_query = $this->db->query("SELECT path, filename FROM image_placement INNER JOIN placement ON placement_id=placementID INNER JOIN image ON image_id=imageID WHERE place = ? AND business_id = ?", array($name, $business_id));

        $images = $get_images_query->result();

        return isset($images[0])?$images[0]:'';
    }
}
