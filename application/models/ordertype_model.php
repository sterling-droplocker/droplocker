<?php

/**
 * @deprecated - consolidated into serviceType_model
 *
 *  My_model extends CI_Model and provides access to CRUD
 *
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class OrderType_Model extends My_Model
{
    protected $tableName = 'orderType';
    protected $primary_key = 'orderTypeID';

    /**
     * get_formatted returns an simple array of order types
     *
     * The keys for the array value is the the orderTypeID and the value is the name
     *
     * @todo allow user to pass arguments if they need to get only a subset of orderTypes
     * @return array of orderType names
     */
    public function get_formatted()
    {
        $query = $this->db->get('orderType');
        $types = $query->result();
        foreach ($types as $t) {
            $a[$t->orderTypeID] = $t->name;
        }

        return $a;
    }

    /**
     * Same as above but returns the slug instead of name.
     * Used in motes
     */
    public function get_formatted_slug()
    {
        $query = $this->db->get('orderType');
        $types = $query->result();
        foreach ($types as $t) {
            $a[$t->orderTypeID] = $t->slug;
        }

        return $a;
    }

    /**
     * Returns an array of slug => name
     */
    public function get_formatted_slug_name()
    {
        $query = $this->db->get('orderType');
        $types = $query->result();
        foreach ($types as $t) {
            $a[$t->slug] = $t->name;
        }

        return $a;
    }

    /**
     * Overiding the MY_Model get method.
     *
     * Option Values
     * ---------------------
     * orderTypeID
     *
     * orderTypeID can be either an array or an int
     *
     * @example setting orderTypeID as array: $options['orderTypeID']=array(1,2,3);
     * @example setting orderTypeID as int: $options['orderTypeID']=1;
     * @param array $options
     * @return array of objects
     *
     */
    public function get($options = array())
    {
        if (isset($options['orderTypeID'])) {
            if(is_array($options['orderTypeID']))
                $this->db->where_in('orderTypeID', $options['orderTypeID']);
            else
                $this->db->where('orderTypeID', $options['orderTypeID']);
        }

        $this->db->order_by('name');

        $q = $this->db->get('orderType');

        return $q->result();
    }

    /**
     * Retrieves order types as key=>value array compatible with form_dropdown() function in CodeIgniter.
     *
     * @return array
     */
    public function get_order_types_as_codeigniter_dropdown()
    {
        $sql = "select name, slug from orderType where 1=1";
        $q = $this->db->query($sql);
        $order_types = array();
        foreach ($q->result() as $a) {
            $order_types[$a->slug] = $a->name;
        }
        return $order_types;
    }

}
