<?php
class DeliveryWindow_Model extends My_Model
{
    protected $tableName = 'delivery_window';
    protected $primary_key = 'delivery_windowID';

    /**
     * Deletes all delivery windows for a business_id
     * @param int $business_id
     */
    public function deleteDeliveryWindows($business_id)
    {
        $sql = 'DELETE delivery_window FROM delivery_window
                JOIN delivery_zone ON delivery_window.delivery_zone_id = delivery_zone.delivery_zoneID
                WHERE delivery_zone.business_id =  ?';

        return $this->db->query($sql, array($business_id));
    }

    public function getDeliveryWindowsAndZones($ids)
    {
        if (empty($ids)) {
            return array();
        }

        $sql = sprintf('SELECT delivery_window.*, delivery_zone.*, location.address, location.serviceType
            FROM delivery_zone
                JOIN delivery_window ON (delivery_zoneID = delivery_window.delivery_zone_id)
                JOIN location ON (locationID = location_id)
                WHERE delivery_zoneID IN (%s)
            ', trim(str_repeat('?,', count($ids)), ','));

        return $this->db->query($sql, $ids)->result();
    }

}
