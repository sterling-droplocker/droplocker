<?php
class Employee_Log_Model extends MY_Model
{
    protected $tableName = 'employee_log';
    protected $primary_key = 'employeeLogID';
    private $reporting_database = null;


    /**
     * Logs successful login for an employee and business.
     *
     * @param int $business_id
     * @param int $employee_id
     */
    public function login($business_id, $employee_id)
    {
        $this->save(array(
            'business_id' => $business_id,
            'employee_id' => $employee_id,
            'ip_address' => $this->input->ip_address(),
            'dateStatus' => convert_to_gmt_aprax()
        ));
    }

    /**
     * Logs successful user switch
     *
     * @param int $business_id
     * @param int $employee_from
     * @param int $employee_id
     */
    public function switch_user($business_id, $employee_from, $employee_id)
    {
        $this->save(array(
            'business_id' => $business_id,
            'from_employee_id' => $employee_from,
            'employee_id' => $employee_id,
            'ip_address' => $this->input->ip_address(),
            'dateStatus' => convert_to_gmt_aprax()
        ));
    }

    /**
     * Logs business switch attemp
     *
     * @param int $business_id
     * @param int $employee_id
     */
    public function switch_business($business_id, $employee_id, $referer)
    {
        if (!$employee_id) {
            $employee_id = 0;
        }
        
        if (!$business_id) {
            $business_id = 0;
        }
        
        $this->save(array(
            'business_id' => $business_id,
            'employee_id' => $employee_id,
            'ip_address' => $this->input->ip_address(),
            'dateStatus' => convert_to_gmt_aprax(),
            'job' => 'business_employee_switch_attemp',
            'referer' => $referer,
        ));
    }

    /**
     * Display a list of employees for the given business and filters.
     *
     * @param int $business_id
     * @param array $filters
     */
    public function get_employees_login_out_history($business_id, $options = array())
    {
        $this->reporting_database = $this->load->database("reporting", TRUE);
        if (DEV) {
            $this->reporting_database = $this->db;
        }

        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must to be numeric.");
        }
        $this->reporting_database->select("SQL_CALC_FOUND_ROWS e.*, e.*, el.*, concat(ee.firstName, concat(' ', ee.lastName)) as loginFrom", false);
        $this->reporting_database->join("employee_log el", "el.employee_id=e.employeeID");
        $this->reporting_database->join("employee ee", "ee.employeeID = el.from_employee_id", "left");
        $this->reporting_database->where("el.business_id", $business_id);

        if (!empty($options["filters"]["start_date"])) {
            $this->reporting_database->where("el.dateStatus >=", $options["filters"]["start_date"]." 00:00:01");
        }
        if (!empty($options["filters"]["end_date"])) {
            $this->reporting_database->where("el.dateStatus <=", $options["filters"]["end_date"]." 23:59:59");
        }        
        if (!empty($options["filters"]["firstName"])) {
            $this->reporting_database->like("e.firstName", $options["filters"]["firstName"]);
        }
        if (!empty($options["filters"]["username"])) {
            $this->reporting_database->like("e.username", $options["filters"]["username"]);
        }
        if (!empty($options["filters"]["email"])) {
            $this->reporting_database->like("e.email", $options["filters"]["email"]);
        }

        if (!empty($options["order"])) {
            $columns_map = array(
                "el.dateStatus ",
                "e.firstName ",
                "e.lastName ",
                "e.username ",
                "e.email ",
                "loginFrom "
            );
            $this->reporting_database->order_by($columns_map[$options["order"]["index"]].$options["order"]["direction"]);
        } else {
            $this->reporting_database->order_by("el.dateStatus DESC");
        }

        if (!empty($options["pagination"]["limit"]) && !empty($options["pagination"]["page_from"])) {
            $this->reporting_database->limit(
                (int)$options["pagination"]["limit"], 
                (int)$options["pagination"]["page_from"]
                );
        }
        
        $query = $this->reporting_database->get("employee e");

        $result = $query->result();

        $sQuery = "SELECT FOUND_ROWS() AS found_rows";

        $aResultFilterTotal = $this->reporting_database->query($sQuery)->row();
        $count = $aResultFilterTotal->found_rows;

        return array("items"=>$result, "count"=>(int)$count);
    }

}