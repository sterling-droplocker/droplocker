<?php

class Driver_App_Journal_Model extends MY_Model
{
    protected $tableName = 'driver_app_journal';
    protected $primary_key = 'driver_app_journalID';
    
    public function getPendingChanges($business_id, $lastProcessed) {
        $q = "SELECT "
            . "tableName, "
            . "id, "
            . "business_id, "
            . "GROUP_CONCAT(event ORDER BY {$this->primary_key} ASC) event, "
            . "GROUP_CONCAT(newData ORDER BY {$this->primary_key} ASC) newData, "
            . "GROUP_CONCAT(oldData ORDER BY {$this->primary_key} ASC) oldData "
            . "FROM {$this->tableName} "
            . "WHERE {$this->primary_key} > ? AND business_id = ? "
            . "GROUP BY tableName, id ";
        $res = $this->db->query($q, array($lastProcessed, $business_id))->result_array();
        
        foreach ($res as $key => $r) {
            $res[$key]['event'] = explode(',', $res[$key]['event']);
            $res[$key]['newData'] = explode(',', $res[$key]['newData']);
            $res[$key]['oldData'] = explode(',', $res[$key]['oldData']);
        }
        
        return $res;
    }
    
    public function getLastRecordId() {
        $q = "SELECT MAX({$this->primary_key}) {$this->primary_key} FROM {$this->tableName}";
        $data = $this->db->query($q, array())->result_array();
        
        return $data[0][$this->primary_key];
    }
    
}
