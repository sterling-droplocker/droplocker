<?php

use App\Libraries\DroplockerObjects\Customer;

class Item_Model extends My_Model
{
    protected $tableName = 'item';
    protected $primary_key = 'itemID';

    /**
     * ########################################
     * BEGIN *
     * ########################################
     *
     * calls insert()
     *
     * options values
     * -----------------------
     * color,
     * size,
     * manufacturer,
     * note,
     * starchOnShirts_id,
     * crease
     *
     * @param array $options
     * @return int itemID
     *
     * ########################################
     * END *
     * ########################################
     */
    public function new_item($options = array())
    {
            if (sizeof($options)==0) {
                    throw new Exception('Missing required options');
            }

    unset($options['order_id']);
            unset($options['customer_id']);
    unset($options['orderStatusOption_id']);

            $this->db->insert('item', $options);

            return $this->db->insert_id();
    }


    /**
     * ########################################
     * BEGIN *
     * ########################################
     *
     * gets item count
     *
     * @param int $business_id
     * @return array of objects
     *
     * ########################################
     * END *
     * ########################################
     */
    public function item_count($business_id = '')
    {
            $business_id = ($business_id!='')?$business_id:$this->business_id;
            $q = $this->db->query("SELECT count(itemID) as total FROM item where business_id = $this->business_id");
            $total = $q->result();

            return $total[0]->total;
    }


    /**
     * Updates an Item
     *
     * @param array $options
     * @param array $where
     * @throws Exception missing where array
     * @return array of objects
     */
    public function update($options = array(), $where=array())
    {
            if (!$where) {
                    throw new Exception("Missing where array in update");
            }

            $upcharges = $options['upcharges'];
            unset($options['upcharges']);
            $affected_rows = $this->db->update('item',$options, $where);

            return $affected_rows;
    }


    /**
     * ########################################
     * BEGIN *
     * ########################################
    * gets items of a customer using the customer_item table with a lot of joins
    * If you wnt to avoid all the joins, just write another function with a sql statement
    *
    * Options Values
    * -----------------
    * customer_id
    * itemID
    * select
    *
    * Joins
    * --------------
    * item
    * product_process
    * product
    * process
    * barcode
    * picture
    *
    * @param array $options
    * @return array of objects
     *
     * ########################################
     * END *
     * ########################################
    */
    public function get_customer_items($options = array())
    {
        if (isset($options['customer_id'])) {$this->db->where('customer_id', $options['customer_id']);}
        if (isset($options['itemID'])) {$this->db->where('itemID', $options['itemID']);}
        if (isset($options['select'])) {$this->db->select($options['select']);}

        $this->db->join('item', 'customer_item.item_id = itemID');
        $this->db->join('product_process', 'product_process_id = product_processID', "left");
        $this->db->join('product', 'product_process.product_id = productID', "left");
        $this->db->join('process', 'product_process.process_id = processID', "left");
        $this->db->join('barcode', 'itemID = barcode.item_id', 'left');

        $this->db->join('picture', 'picture.item_id = itemID', 'left');
        $this->db->order_by('pictureID', 'DESC');
        $query = $this->db->get('customer_item');
        $items = $query->result();

        return $items;

    }

    /**
     * Retrieves the items for a customer in a "Closet 2.0" compatible format.
     *
     * @param int $customer_id
     * @param int order_id Optional. If specified, then only items associated with the specified order are returned.
     * @return array
     */
    public function get_closet($customer_id, $order_id='', $barcode = null)
    {
        if (empty($customer_id)) {
            throw new Exception("No customer_id passed");
        }

        $customer = new Customer($customer_id);
        $params = array();
        if ($order_id != '') {
            $sql = "SELECT itemID, displayName, product.name, process.name as process, file, (select barcode from barcode where item_id = itemID limit 1) as barcode
                FROM item
                INNER JOIN customer_item ON customer_item.item_id = itemID
                INNER JOIN product_process ON product_processID = item.product_process_id
                INNER JOIN product ON productID = product_process.product_id
                LEFT JOIN process ON processID = product_process.process_id
                INNER JOIN orderItem ON orderItem.item_id = customer_item.item_id
                LEFT JOIN picture ON picture.item_id = itemID
                WHERE orderItem.order_id = {$order_id}
                and customer_item.customer_id = {$customer_id}
                and item.business_id = $customer->business_id
                order by pictureID desc";
        } else {
            $sql = "SELECT itemID, displayName, p.name, process.name as process, file, (select barcode from barcode where item_id = itemID limit 1) as barcode
                FROM item i
                INNER JOIN product_process pp ON product_processID = i.product_process_id
                INNER JOIN product p ON productID = pp.product_id
                INNER JOIN customer_item ci ON ci.item_id = itemID
                INNER JOIN barcode b ON b.item_id = itemID
                LEFT JOIN process ON processID = pp.process_id
                LEFT JOIN picture ON picture.item_id = itemID
                WHERE ci.customer_id = {$customer_id}
                and i.business_id = $customer->business_id";
            if ($barcode != null) {
                $sql.= " AND b.barcode = ?";
                $params[] = $barcode;
            }
            $sql.= " order by pictureID desc";
        }
        $items = $this->db->query($sql, $params)->result();

        foreach ($items as $item) {
            //find out if an item is not on any orders for this customer
            $getOrders = "Select count(orderItemID) as count from orderItem join orders on orderID = order_id where customer_id = $customer_id and item_id = $item->itemID";
            $orders = query($getOrders);

            $process = $item->process ? $item->process : "Other";
            $category = $item->displayName." - ". get_translation($process, "Processes", array(), $process);
            if (empty($closet[$category][$item->itemID])) {
                $closet[$category][$item->itemID] = 
                        array('itemID'=>$item->itemID, 'displayName'=>$item->displayName, 
                            'productName'=>$item->name, 'file'=>$item->file, 'barcode'=>$item->barcode, 
                            'category'=> $category, 'orders'=>$orders[0]->count);
            }
        }

        return isset($closet)?$closet:'';
    }

    /**
     * records that the item was cleaned and work is done on the item
     *
     * @order_id int $order_id
     * @garcode string $garcode
     **/
    function mark_as_assembled($order_id, $garcode)
    {
        $addAutomat = "insert into automat (gardesc, order_id, garcode)
                        values ('Marked as Assembled', ?, ?)";
        $q = $this->db->query($addAutomat, array($order_id, $garcode));

        return $this->db->insert_id();
    }

    /**
     * Gets the history of an item
     * @param int $itemID
     * @return array
     */
    function get_item_history($itemID)
    {
        $sql = "SELECT date, description, '' AS order_id, CONCAT(employee.firstName,' ', employee.lastName) AS creator
                    FROM itemHistory
                    LEFT JOIN employee ON creator=employeeID
                    WHERE item_id = ?
                UNION
                SELECT updated AS date, 'Added to order' AS description, order_id, '0' AS creator
                    FROM orderItem
                    WHERE item_id = ?
                ORDER BY date DESC";
        
        return $this->db->query($sql, array($itemID, $itemID))->result();
    }
        
    /**
     * Gets the upcharges of an item
     * @param int $itemID
     * @return array
     */
    public function get_item_upcharges($itemID)
    {
        $sql = "SELECT upcharge_id, item_id, upcharge.upchargePrice as price, upcharge.name FROM itemUpcharge
                    INNER JOIN upcharge ON upchargeID = upcharge_id
                    WHERE item_id = ?";

        return $this->db->query($sql, array($itemID))->result();
    }

    /**
     * Gets the specifiactions of an item
     * @param int $itemID
     * @return array
     */
    public function get_item_specifications($itemID)
    {
        $sql = "SELECT * FROM item_specification
                    JOIN specification on specification_id = specificationID
                    WHERE item_id = ?";

        return $this->db->query($sql, array($itemID))->result();
    }
}
