<?php
class FollowupCallType_Model extends My_Model
{
    protected $tableName = 'followupCallType';
    protected $primary_key = 'followupCallTypeID';

    /**
     * Retrieves all followup call types.
     * @return an array in the following format:
     *      [followCallTypeID] => [name]
     */
    public function get_all()
    {
        $followup_call_types_query = $this->db->get($this->tableName);
        $followup_call_types_result = $followup_call_types_query->result();

        $followup_call_types = array();

        foreach ($followup_call_types_result as $call_type) {
            $followup_call_types[$call_type->followupCallTypeID] = $call_type->name;
        }

        return $followup_call_types;
    }
    public function get_by_id($followupCallTypeID)
    {
        if (!is_numeric($followupCallTypeID)) {
            throw new Exception("'followupCallTypeID' must be numeric.");
        }
        $this->db->get_where($this->tableName, array("followupCallTypeID" => $followupCallTypeID));
    }
}
