<?php

class CustomOptionValue_Model extends My_Model
{
    protected $tableName = 'customOptionValue';
    protected $primary_key = 'customOptionValueID';

    public function getByLanguage($customOption_id, $business_language_id)
    {
        $this->db->join("customOptionValueName", "customOptionValue_id = customOptionValueID", "LEFT");
        $this->db->where(compact("customOption_id", "business_language_id"));
        $this->db->order_by("sortOrder", "ASC");
        $options = $this->db->get("customOptionValue")->result();

        return $options;
    }

    public function getMaxSortOrder($business_id, $option_id)
    {
        $this->db->select_max('sortOrder');
        $this->db->from($this->tableName);
        $this->db->where(array(
            'business_id' => $business_id,
            'customOption_id' => $option_id
        ));

        $row = $this->db->get()->row();
        return $row->sortOrder;
    }

    public function updateSortOrder($business_id, $option_id, $value_id, $newOrder, $oldOrder)
    {
        // If this has an oldOrder, then a hole has been left in the oldOrder position
        if ($oldOrder) {
            $this->db->set('sortOrder', 'sortOrder - 1', false);
            $this->db->where(array(
                'business_id' => $business_id,
                'customOption_id' => $option_id,
                'sortOrder >' => $oldOrder,
                'customOptionValueID != ' => $value_id
            ));
            $this->db->update($this->tableName);
        }

        // then we make room for the new sortOrder
        if ($newOrder) {
            $this->db->set('sortOrder', 'sortOrder + 1', false);
            $this->db->where(array(
                'business_id' => $business_id,
                'customOption_id' => $option_id,
                'sortOrder >=' => $newOrder,
                'customOptionValueID != ' => $value_id
            ));
        }
        $this->db->update($this->tableName);
    }

    public function updateDefault($business_id, $option_id, $value_id, $default)
    {
        if (!$default) {
            return true;
        }

        $this->db->set('default', 0);
        $this->db->where(array(
            'business_id' => $business_id,
            'customOption_id' => $option_id,
            'default' => 1,
            'customOptionValueID != ' => $value_id
        ));
        $this->db->update($this->tableName);
    }
}