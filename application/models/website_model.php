<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 *
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Website_Model extends My_Model
{
    protected $tableName = 'website';
    protected $primary_key = 'websiteID';

    /**
     * Gets the website settings for a business
     *
     * @param int $business_id
     * @return array of objects
     */
    public function get_website($business_id)
    {
        $query = $this->db->query("SELECT * FROM website WHERE business_id = ".$business_id);
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $query->result();
    }
}
