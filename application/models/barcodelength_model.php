<?php

class BarcodeLength_Model extends My_Model
{
    protected $tableName = 'barcodeLength';
    protected $primary_key = 'barcodeLengthID';

    /**
     * Returns all the barcode length settings for a business. If there are no barcode lengths defined for the business, then "8" and "10" are returned as defaults.
     * @return array An array consisting of the valid lengths
     * @throws Exception
     */
    public function get_all($business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $get_barcode_lengths_query = $this->db->get_where($this->tableName, array("business_id" => $business_id));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
        $results = $get_barcode_lengths_query->result();
        if (empty($results)) {
            return array(8,10);
        } else {
            $lengths = array();
            foreach ($results as $result) {
                $lengths[] = $result->value;
            }
            $lengths = array_map("intval", $lengths);

            return $lengths;
        }
    }
    /**
     *
     * @param type $length
     * @param type $business_id
     * @return bool
     * @throws Exception
     */
    public function insert($length, $business_id)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        if (!is_numeric($length)) {
            throw new Exception("'length' must be numeric.");
        }
        $result = $this->db->insert($this->tableName, array("value" => $length, "business_id" => $business_id));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message(), $this->db->_error_number());
        }

        return $result;
    }
    /**
     * Deletes a barcode lenght setting for the sepcified business.
     * @param int $length
     * @param int $business_id
     * @return bool
     * @throws Exception
     */
    public function delete($length, $business_id)
    {
        if (!is_numeric($length)) {
            throw new Exception("'length' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $result =  $this->db->delete($this->tableName, array("value" => $length, "business_id" => $business_id));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }

        return $result;
    }
    /**
     *
     * @param int $barcode
     * @param int $business_id
     * @return boolean
     * @throws Exception
     */
    public function is_valid_barcode_length($barcode, $business_id)
    {

        if (empty($barcode)) {
            throw new Exception("'barcode' can not be empty.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must be numeric.");
        }
        $length = strlen((string) $barcode);
        $lengths = $this->get_all($business_id);

        if (in_array($length, $lengths)) {
            return true;
        } else {
            return false;
        }

    }
}
