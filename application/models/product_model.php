<?php

/**
 * product model
 */

class Product_model extends MY_Model
{
    protected $tableName = 'product';
    protected $primary_key = 'productID';

    /**
     * Retrieves the number of products for a particular business
     * @param int $business_id
     * @return int
     */
    public function get_count($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");
        $products_count = $this->db->query("SELECT COUNT(productID) AS total FROM {$this->tableName} WHERE business_id = ?", array($business_id))->row();

        return $products_count->total;
    }

    /**
     * Returns all active products with valid associated product categories for a particular business whose product process is active
     * @param int $business_id
     * @param bool exclude_wash_and_fold_products
     * @return array
     */
    public function get_all($business_id, $exclude_wash_and_fold_products = false)
    {
        $get_products_query = $this->db->query("
            SELECT product.name, product.displayName, process.name AS process_name, productID, productCategory.name AS category_name, upchargeGroup_id, productCategory_id
                FROM product JOIN productCategory ON productCategory_id = productCategoryID
                JOIN product_process ON product_id=productID
                LEFT JOIN process ON process_id=processID
                WHERE product_process.active = 1 AND product.business_id=?
                ORDER BY product.name,productCategoryID
            ", array($business_id));

        $products =  $get_products_query->result();

        return $products;
    }
    /**
     * Returns all active products with valid product categories for a particular business
     * @param int $business_id
     * @return array
     */
    public function get_all_as_codeigniter_dropdown($business_id)
    {
        $products = $this->db->query("SELECT product.name, productID
                FROM product JOIN productCategory ON productCategory_id = productCategoryID
                JOIN product_process ON product_id=productID
                WHERE product_process.active=1 AND product.business_id=?
                ORDER BY product.name", array($business_id))->result();
        $product_dropdown = array();
        foreach ($products as $product) {
            $product_dropdown[$product->productID] = $product->name;
        }

        return $product_dropdown;
    }

    /**
     * Retrieves the business wash products for the defined service types
     * @param int $business_id
     * @return array
     * @throws Exception
     * @throws \App\Libraries\DroplockerObjects\NotFound_Exception
     */
    public function get_business_wash_preferences($business_id)
    {
        $this->load->model("business_model");
        $business = new \App\Libraries\DroplockerObjects\Business($business_id);
        //Note, a * design flaw serializes the service types instead of storing them in a table.
        $serviceTypes = unserialize($business->serviceTypes);
        if (empty($serviceTypes)) {
            throw new Exception("'services' can not be empty");
        }
        $modules_and_associated_products =array();
        foreach ($serviceTypes as $index => $service) {
            $module = new \App\Libraries\DroplockerObjects\Module($service['serviceType_id']);

            $this->db->select("product.name, product.displayName,product.productID, productCategory.slug, productCategory.name as productCategory_name, product.sortOrder");
            $this->db->join("productCategory", "productCategoryID=productCategory_id", "LEFT");
            $this->db->where("product.business_id", $business_id);
            $this->db->where("productCategory.module_id", $module->moduleID);
            $this->db->where("product.productType", "preference");
            $this->db->order_by("product.sortOrder");
            $products = $this->db->get($this->tableName)->result();
            foreach ($products as $product) {
                if (empty($product->sortOrder)) {
                    $sortOrder = $index;
                } else {
                    $sortOrder = $product->sortOrder;
                }
                $modules_and_associated_products[$module->name][$product->slug][$sortOrder] = array('displayName' => $product->displayName, 'name' => $product->name,'productID' => $product->productID, 'productCategory_name' => $product->productCategory_name, 'productCategory_slug' => $product->slug);
            }
        }

        return $modules_and_associated_products;
    }
    /**
     * @deprecated Do not use the following * filth, which is undocumented and does not follow established coding standards. use get_aprax instead.
     * @param type $options
     * @return type
     */
    public function get($options = array())
    {
        // returns a simple array with the processes
        $this->load->model('process_model');
        $processes = $this->process_model->get_as_codeigniter_dropdown();

        $this->db->join('product_process', 'product_process.product_id = productID', 'left');
        if (isset($options['productID'])) {
            $this->db->where('productID', $options['productID']);
        }
        if (isset($options['module_id'])) {
            $this->db->where_in('productCategory.module_id', $options['module_id']);
        }
        if (isset($options['productCategory.module_id'])) {
            $this->db->where("productCategory.module_id", $options['productCategory.module_id']);
        }
        if (isset($options['module.slug'])) {
            $this->db->where('module.slug', $options['module.slug']);
        }
        if (isset($options['productCategory_id'])) {
            $this->db->where('productCategory_id', $options['productCategory_id']);
        }
        if (isset($options['business_id'])) {
            $this->db->where('product.business_id', $options['business_id']);
        }
        if (isset($options['active'])) {
            $this->db->where("product_process.active", $options['active']);
        }
        if (isset($options['productType'])) {
            $this->db->where('productType', $options['productType']);
        }
        if (isset($options['select'])) {
            $this->db->select($options['select']);
        }
        if (isset($options['group_by'])) {
            $this->db->group_by($options['group_by']);
        }

        if (!isset($options['product_only'])) {
            $this->db->join('productCategory', 'productCategoryID = product.productCategory_id');
            $this->db->join('module', 'module.moduleID = productCategory.module_id');
            $this->db->join('process', 'process_id = processID', 'left');
            $this->db->join('upchargeGroup', 'upchargeGroup_id = upchargeGroupID', "left");
            $this->db->join('taxGroup', 'taxGroup_id = taxGroupID', "left");

            if (isset($options['process_id'])) {
                if (is_array($options['process_id'])) {
                    $this->db->where_in('product_process.process_id', $options['process_id']);
                } else {
                    $this->db->where('product_process.process_id', $options['serviceType_id']);
                }
            }
            if (isset($options['not_process_id'])) {
                $this->db->where('product_process.process_id !=', $options['not_process_id']);
            }
        }

        if (isset($options['order_by'])) {
            $this->db->order_by($options['order_by']);
        }
        if (isset($options['limit'])) {
            $this->db->limit($options['limit']);
        }

        $business_id = (isset($options['business_id']))?$options['business_id']:$this->session->userdata('business_id');

        $query =  $this->db->get('product');
        $products =  $query->result();
        for ($i=0; $i < sizeof($products) ; $i++) {
            //get the process
            $array = array();

            if (array_key_exists($i, $products) && property_exists($products[$i], "processID")) {
                $products[$i]->process = @$processes[$products[$i]->processID];
            }
            //get the suppliers

            if (property_exists($products[$i], "product_processID") && isset($products[$i]->product_processID)) {
                //The following query all suppliers associated with the company, and information, if any of that supplier that is associated with the current product.
                $sql = "SELECT companyName, supplierID,
                                (select cost FROM product_supplier WHERE product_process_id = {$products[$i]->product_processID} AND supplier_id = supplierID) AS cost,
                                (select `default` FROM product_supplier WHERE product_process_id = {$products[$i]->product_processID} AND supplier_id = supplierID) AS `default`
                            FROM supplier
                            JOIN business on businessID = supplierBusiness_id
                            WHERE business_id = '{$business_id}'
                            ORDER BY companyName";

                $result = $this->db->query($sql);
                $suppliers = $result->result();
                foreach ($suppliers as $supplier) {
                    $array[] = array('supplierID'=>$supplier->supplierID, 'companyName'=>$supplier->companyName,  'cost'=>$supplier->cost, 'default' => $supplier->default);
                }
            } else {
                //$array[0] = array("businessID"=>$this->business->businessID, 'companyName'=>$this->business->companyName);
            }
            $products[$i]->suppliers = $array;
        }

        return $products;
    }

    /**
     *
     * Sql query that returns all the products for a business
     * This is easier to read than this->get() with a bunch of options
     *
     * @todo use memcache for this
     * @param int $business_id
     * @return array of objects
     */
    public function getBusinessProducts($business_id)
    {
        $sql = "SELECT productID,
               pp.product_processID,
               pp.process_id,
               displayName,
               product.name as name,
               product.productCategory_id,
               pp.price,
               p.name as process_name,
               product.unitOfMeasurement,
               supplier_id,
               upchargeGroup_id,
               i.filename image
               FROM `product`
               left join product_process pp on pp.product_id = productID
               left join product_supplier ps ON product_process_id = product_processID
               left join process p ON processID = pp.process_id
               left join image i ON imageID = product.image_id
               WHERE product.business_id = {$business_id}
               AND pp.active = 1
               group by name,processID
               order by name";
        $query = $this->db->query($sql);

        return $query->result();
    }


    /**
     * Gets the default productID for each business preference in a module
     *
     * @param int $business_id
     * @param int $module_id
     * @return array
     */
    public function getBusinessDefaultPreferences($business_id, $module_id)
    {
        $query = $this->db->query("SELECT productCategory.slug, productID, sortOrder
            FROM product
            JOIN product_process ON (product_id = productID)
            JOIN productCategory ON (productCategoryID = productCategory_id)
            WHERE productCategory.module_id = ?
                AND product.business_id = ?
                AND product_process.active = 1
                AND productType = 'preference'
        ", array($module_id, $business_id));

        $products = $query->result();

        $defaults = array();
        foreach ($products as $product) {
            if ($product->sortOrder == 1) {
                $defaults[$product->slug] = $product->productID;
            }
        }

        return $defaults;
    }


    /**
    * insert method inserts a product into the database
    *
    * @param array $options
    * @return int insert_id()
    */
    public function insert($options = array())
    {
        $options = check_fields($options,'product');
        $this->db->insert('product', $options);

        return $this->db->insert_id();
    }

    //Note, this function is broken.
    /**
     *
     * @param array $options A list of properties to set
     * @param array $where A list of conditionals
     * @return int The number of rows that were updated in the query
     */
    public function update($options = array(), $where=array())
    {
        //The following conditional handles a database schema change where the price is no longer in the product model but instead in the product_supplier table.
        if (array_key_exists('price', $options)) {
            $where = array("product_id" => $where['productID']);
            $options['price'] = floatval(ltrim($options['price'], '$'));
            $this->db->update('product_process', $options, $where);
        } else {
            $options = check_fields($options,'product');
            $this->db->update('product', $options, $where);
        }

        return $this->db->affected_rows();
    }


    /**
     * deletes a product
     *
     * Options can be any field header in the product table
     *
     * @param array $options
     * @return int affected_rows()
     */
    public function delete($options = array())
    {
        $this->db->delete('product', $options);

        return $this->db->affected_rows();
    }

    /**
     * temp method that migrates the old product table to the new product
     */
    public function move_products()
    {
        $db = $this->load->database('laundrylocker', true);

        $q = $db->get('product');
        $old = $q->result();

        foreach ($old as $o) {
            $q = $this->db->get_where('productClass',array('slug'=>$o->class));
            if (!$c = $q->result()) {
                $this->db->insert('productClass',array('name'=>$o->class,'slug'=>$o->class));
                $class_id = $this->db->insert_id();
            } else
            $class_id = $c[0]->productClassID;

            $cat = ($o->category!='')?$o->category:"None";
            $q = $this->db->get_where('productCategory',array('slug'=>$cat));
            if (!$c2 = $q->result()) {

                $this->db->insert('productCategory',array('name'=>$cat,'slug'=>$cat));
                $cat_id = $this->db->insert_id();
            } else
                $cat_id = $c2[0]->productCategoryID;

            $this->db->insert('product',array('name'=>$o->name,'price'=>$o->price,'unitOfMeasurement'=>$o->unitOfMeasurement,'cost'=>$o->cost,'productClass_id'=>$class_id,
            'productCategory_id'=>$cat_id,'notes'=>$o->notes,'url'=>$o->url,'taxable'=>$o->taxable,'business_id'=>1));
            //die($this->db->last_query());
        }

    }

    /**
     * dropdown method creates an object to be used for a dropdown
     *
     * @param int $customer_type
     * @return array of products
     */
    public function dropdown($customer_type = '')
    {
        $this->load->model('product_process_model');
        $sql = "SELECT * FROM process";
        $q = $this->db->query($sql);
        $process = $q->result();
        foreach ($process as $p) {
            $processes[$p->processID] = $p->name;
        }

        $options['select'] = "name, productID";
        $options['order_by'] = 'name';
        $options['product_only'] = true;
        $options['module_id'] = array(2,3,5);
        $options['group_by'] = 'displayName';
        $products =  $this->get($options);

        for ($i=0; $i < sizeof($products); $i++) {
            $str = "(";
            $this->product_process_model->product_id = $products[$i]->productID;
            $pp = $this->product_process_model->get();
            foreach ($pp as $p) {
                $str .= $processes[$p->process_id].", ";
            }
            $str = rtrim($str, ", ").")";
            if($str == "()")$str = "";
            $products[$i]->processes = $str;
        }

        return $products;
    }


    /**
     * The following function is *, do not use.
     * checks to see if there is location pricing. If not, return the price of the product
     * @deprecated This function does not work because there is no product_id property in the product location price table.
     * @param object $product - productID and unitPrice is required
     * @param int location_id
     * @param int $business_id
     * @return double $unitPrice
     */
    public function getPrice($product, $order_id, $business_id)
    {
        $this->load->model('product_location_price_model');

        $sql = "SELECT locationID FROM orders INNER JOIN locker ON lockerID = orders.locker_id
                        INNER JOIN location on locationID = location_id
                        WHERE orderID = ".$order_id;
        $query = $this->db->query($sql);
        $location = $query->result();

        $this->product_location_price_model->location_id = $location[0]->locationID;
        $this->product_location_price_model->product_id = $product->productID;
        $this->product_location_price_model->business_id = $business_id;
        $location_pricing = $this->product_location_price_model->get();
        if ($location_pricing) {
            return $location_pricing[0]->price;
        } else {
            return $product->price;
        }
    }

    /**
     * gets the default product for a product category
     *
     * @param int $productCategory_id
     * @return int productID
     */
    public function get_default_product_in_category($productCategory_id)
    {
        $sql = "SELECT productID FROM product WHERE productCategory_id = {$productCategory_id} ORDER BY sort_order, productID limit 1";
        $query = $this->db->query($sql);
        $product = $query->row();

        return $product->productID;
    }

    /**
     * gets the default product for a product category
     *
     * @param string $slug
     * @param int business_id
     * @return int productID Note, if the default product process is not found, then null is returned
     */
    public function get_default_product_process_in_category($slug, $business_id)
    {
        if (empty($slug)) {
            throw new Exception("'slug' can not be empty.");
        }
        if (!is_numeric($business_id)) {
            throw new  Exception("'business_id' must be numeric.");
        }
        $sql = "SELECT product_processID FROM product
            INNER JOIN product_process ON product_process.product_id = product.productID
            INNER JOIN productCategory ON productCategory.productCategoryID = product.productCategory_id
            WHERE slug = '{$slug}' AND product.business_id = $business_id
            and product_process.active = 1
            ORDER BY sortOrder  LIMIT 1";

        $query = $this->db->query($sql);
        $product = $query->row();
        if (empty($product)) {
            return null;
        } else {
            return $product->product_processID;
        }
    }

    /**
     * used to get all orderItems that are wash and fold
     *
     * @param int $order_id
     * @return array of objects
     */
    public function getOrderWF($order_id)
    {
        $this->load->model("OrderItem_Model");
        $sqlWFPrefs = "SELECT orderItem.qty, orderItem.notes, product.unitOfMeasurement
                      FROM orderItem
                      JOIN product_process pp ON product_processID = orderItem.product_process_id
                      JOIN product on productID = pp.product_id
                      JOIN productCategory ON productCategory.productCategoryID = product.productCategory_id
                      WHERE order_id = ?
                      AND slug = ?";

        $query = $this->db->query($sqlWFPrefs, array($order_id, WASH_AND_FOLD_CATEGORY_SLUG));
        $order = $query->result();
        $order[0]->qty = $this->OrderItem_Model->getRoundedQty($this->business_id, $order[0]->qty, $order[0]->unitOfMeasurement);
        return $order;
    }

    function get_product_with_processes($business_id)
    {
        $this->db->select("product_processID, product_process.price, product_process.taxable");
        $this->db->join("product_process","product_id=productID");
        $this->db->where("business_id", $business_id);

        $product_processes = array();
        foreach ($this->db->get("product")->result() as $product_process) {
            $product_processes[$product_process->product_processID] = $product_process;
        }

        return $product_processes;
    }
    
    public function getProductType($product, $business_id = null){
        $productType = 'Other';
        
        if (!$business_id) {
            $business_id = $product->business_id;
        }
        
        $default_package_delivery_id = get_business_meta($business_id, 'default_package_delivery_id');

        if (stristr($product->module_name, 'whsl')) {
            $productType = 'Wholesale';
        } elseif ($product->module_slug == 'wf') {
            $productType = 'WF';
        } elseif ($product->module_slug == 'shoe') {
            $productType = 'SS';
        } elseif ($product->product_process_id == $default_package_delivery_id) {
            $productType = 'PD';
        } elseif ($product->module_slug == 'dc') {
            $productType = 'DC';
        } elseif ($product->module_slug == 'laundry') {
            $productType = 'DC';
        } elseif ($product->module_slug == 'upcharge') {
            $productType = 'DC';
        }
            
        return $productType;
    }

}
