<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class BusinessLaundryPlan_Model extends My_Model
{
    protected $tableName = 'businessLaundryPlan';
    protected $primary_key = 'businessLaundryPlanID';

    /**
     * Retrieves a count of the nubmer of results for a query
     * @param array $options An associate array of WHERE parameters
     * @return int The count
     */
    public function count($options = array())
    {
    	$options["type"] = "laundry_plan";
        foreach ($options as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->from($this->tableName);

        return $this->db->count_all_results();
    }

    /**
     * Retreives data based on the supplied criteria
     * @param $options array An array of WHERE parameters
     * @param $order_by string A column to order the results
     * @param $select string The fields to SELECT
     * @param limit int
     * @param offset int
     * @return array Consist of stdclasses with properties associated with the query result columns
     */
    public function get_aprax($options = array(), $order_by = null, $select = null, $limit = null, $offset = null)
    {
        if (!is_null($order_by)) {
            $this->db->order_by($order_by);
        }
        if ($select) {
            $this->db->select($select);
        }

        $options["type"] = "laundry_plan";
        
        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $this->db->where_in($key, $value);
            } else {
                $this->db->where($key, $value);
            }
        }

        $query = $this->db->get($this->tableName, $limit, $offset);

        return $query->result();
    }

    /**
     * This is *, do not use or I murder your face repeatedly
     * @deprecated use get_aprax() instead
     */
    public function get()
    {
        if ($this->required) {
                if(!required($this->required, $this->options))
                die("MISSING REQUIRED FIELDS:<br>DATABASE: ".$this->tableName."<hr>REQUIRED: <pre>".print_r($this->required, true)."</pre><hr>YOU PASSED:<pre>".print_r($this->options, true)."</pre><hr>");
        }

        if (empty($this->options)) {
        	$this->options = array();
        }

        $this->options["type"] = "laundry_plan";
        if ($this->options) {
            foreach ($this->options as $key => $value) {
                if (($key != 'select') && ($key != 'order_by')) {
                    if (is_array($value)) {
                        $this->db->where_in($this->tableName.".".$key,$value);
                    } else {
                        $this->db->where($this->tableName.".".$key, $value);
                    }
                }
            }
        }

        if (isset($this->options['select'])) {
                $this->db->select($this->options['select']);
        }

        if (isset($this->options['order_by'])) {
                $this->db->order_by($this->options['order_by']);
        }

        if (sizeof($this->joins)) {
                foreach ($this->joins as $j) {
                        $a = explode(" ", $j);
                        //get tablename
                        $table = $a[0];
                        //remove tablename and "on"
                        unset($a[0]);
                        unset($a[1]);
                        //rebuild the statement
                        $statement = implode(" ",$a);

                        $this->db->join($table, $statement);
                }
        }

        if (sizeof($this->leftjoins)) {
                foreach ($this->leftjoins as $j) {
                        $a = explode(" ", $j);
                        //get tablename
                        $table = $a[0];
                        //remove tablename and "on"
                        unset($a[0]);
                        unset($a[1]);
                        //rebuild the statement
                        $statement = implode(" ",$a);

                        $this->db->join($table, $statement, "LEFT");
                }
        }

        $query = $this->db->get($this->tableName);

        return $query->result();
    }
}
