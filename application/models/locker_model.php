<?php

class Locker_Model extends MY_Model
{
    protected $tableName = 'locker';
    protected $primary_key = 'lockerID';

    /**
     * Retreives all the active lockers associated business and formats them as a codeigniter dropdown data structure.
     * @param int $business_id
     * @return array
     */
    public function get_all_as_codeigniter_dropdown($business_id)
    {
        \App\Libraries\DroplockerObjects\NotFound_Exception::does_exist($business_id, "Business");

        $this->db->join("location", "location_id=locationID");
        $this->db->where("business_id", $business_id);
        $this->db->where("lockerName !=", "inProcess");
        $this->db->where("lockerStatus_id", 1); //Only active lockers should be retrieved.

        $this->db->order_by("lockerName");
        $lockers = $this->db->get("locker")->result();
        $lockers_dropdown = array();
        foreach ($lockers as $locker) {
            $lockers_dropdown[$locker->lockerID] = $locker->lockerName;
        }

        return $lockers_dropdown;
    }
    /**
     * Returns all lockers lockers that are 'active' and do not have the name 'inprocess' for a particular locaiton in a codeigniter dropdown compatible format.
     * @param int $location_id
     * @return array
     */
    public function get_all_as_codeigniter_dropdown_for_location($location_id)
    {
        if (is_numeric($location_id)) {
            $this->db->join("location", "location_id=locationID");
            $this->db->where("lockerName !=", "inProcess");
            $this->db->where("lockerStatus_id", 1);
            $this->db->where("location_id", $location_id);
            $this->db->order_by("lockerName");
            $lockers = $this->db->get("locker")->result();
            $lockers_dropdown = array();
            foreach ($lockers as $locker) {
                $lockers_dropdown[$locker->lockerID] = $locker->lockerName;
            }

            return $lockers_dropdown;
        } else {
            throw new \InvalidArgumentException("'location_id' must be numeric");
        }
    }

     /**
     * Returns all lockers lockers that are 'active' and do not have the name 'inprocess' for a particular locaiton in a codeigniter dropdown compatible format.
     * @param int $location_id
     * @return array
     */
    public function get_all_as_usable_codeigniter_dropdown_for_location($location_id)
    {
        $this->db->join("location", "location_id=locationID");
        $this->db->where("lockerName !=", "inProcess");
        $this->db->where("lockerStatus_id", 1);
        $this->db->where_not_in("lockerStyle_id",array(7, 9)); //Note, 7 and 9 refer to locker styles inprocess and hidden
        $this->db->where("location_id", $location_id);
        $this->db->order_by("lockerName");
        $lockers = $this->db->get("locker")->result();
        $lockers_dropdown = array();
        foreach ($lockers as $locker) {
            $lockers_dropdown[$locker->lockerID] = $locker->lockerName;
        }

        return $lockers_dropdown;
    }

     /**
     * Returns all lockers for a particular location in a codeigniter dropdown compatible format.
     * @param int $location_id
     * @return array
     */
    public function get_all_all_as_codeigniter_dropdown_for_location($location_id)
    {
        $this->db->where("location_id", $location_id);
        $this->db->order_by("lockerName");
        $lockers = $this->db->get("locker")->result();
        $lockers_dropdown = array();
        foreach ($lockers as $locker) {
            $lockers_dropdown[$locker->lockerID] = $locker->lockerName;
        }

        return $lockers_dropdown;
    }

    /**
     * retrieves all lockers that are status active and are not locker style 'hidden' or inprocess.
     * @param int $location_id
     * @return array An array of lockers ordered by the lockerName.
     */
    public function get_all_useable_for_location($location_id)
    {
        $this->db->join("location", "location_id=locationID");
        $this->db->where("lockerName !=", "inProcess");
        $this->db->where("lockerStatus_id", 1);
        $this->db->where_not_in("lockerStyle_id",array(7, 9)); //Note, 7 and 9 refer to the primary keys of the locker styles InProcess and hidden
        $this->db->where("location_id", $location_id);
        $this->db->order_by("lockerName");
        $lockers = $this->db->get("locker")->result();

        return $lockers;
    }

    /**
     * Retrieves all lockers that are status active and not locker style inProcess.
     * @param int $location_id
     * @return array An array of lockers ordered by locker name.
     */
    public function get_all_returnable_for_location($location_id)
    {
        $this->db->join("location", "location_id=locationID");
        $this->db->where("lockerName !=", "inProcess");
        $this->db->where("lockerStatus_id", 1);
        $this->db->where_not_in("lockerStyle_id",array(7)); //Note, 7 refers to the primary key of the locker styles InProcess
        $this->db->where("location_id", $location_id);
        $this->db->order_by("lockerName");
        $lockers = $this->db->get("locker")->result();

        return $lockers;
    }

    /**
     * Get all the history of the orders that were placed in a locker
     *
     * @param int $locker_id
     * @param int $businesss_id
     * @return array of objects
     */
    public function getHistory($locker_id, $business_id)
    {
        if (!is_numeric($locker_id)) {
            throw new Validation_Exception("'locker_id' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Validation_Exception("'business_id' must be numeric.");
        }
        $sql = "SELECT orderStatus.date as statusDate, orderID, name, customer_id, firstName, lastName, orderStatus.orderStatusOption_id
            FROM orderStatus
            join `orders` on orderID = orderStatus.order_id
            join orderStatusOption on orderStatusOptionID = orderStatus.orderStatusOption_id
            LEFT join customer on customerID = orders.customer_id
            where orderStatus.locker_id = $locker_id
            AND orders.business_id = $business_id
            and orderStatus.orderStatusOption_id not in (10,3,2)
            group by orderID
            order by orderStatus.date desc;";

        return $this->db->query($sql)->result();
    }

    /**
     * Gets an active order in a locker
     *
     *
     * @param int $locker_id
     * @param int $bagID
     * @return array
     */
    public function getOrderInLocker($locker_id, $bagID)
    {
        $sql = "SELECT customer_id, bag_id, orderID, lockerName location
                FROM orders
                JOIN locker ON lockerID = orders.locker_id
                WHERE locker_id = '{$locker_id}'
                AND orderStatusOption_id NOT IN (9,10)
                AND lockerName != 'N/A'";
        $search_order = $this->query($sql);

        //if we find an order with the locker_id and it's not completed and we only get 1 result
        if ($search_order && sizeof($search_order)==1) {
            // set the customer_id
            $customer_id = $search_order[0]->customer_id;

            //assign the bag to this customer
            $this->load->model('bag_model');
            $this->bag_model->addCustomerToBag($customer_id, $bagID);

        }

        return array('customer_id'=>$customer_id, );
    }

    /**
     * Do not use the following undocumented  .
     * @deprecated
     *
     * ####################################
     * BEGIN
     * ####################################
     *
     * gets locker data from the business table
     *
     * ####################################
     * END
     * ####################################
     */
    public function get($options = array())
    {
        if (isset($options['lockerID'])) {$this->db->where('lockerID', $options['lockerID']);}
        if (isset($options['location_id'])) {$this->db->where('locker.location_id', $options['location_id']);}
        if (isset($options['like_lockerName'])) {$this->db->like('lockerName', $options['like_lockerName']);}
        if (isset($options['lockerName'])) {$this->db->where('lockerName', $options['lockerName']);}
        if (isset($options['lockerStatus_id'])) {
            if (is_array($options['lockerStatus_id'])) {
                    $this->db->where_in('lockerStatus_id', $options['lockerStatus_id']);
            } else {
                    $this->db->where('lockerStatus_id', $options['lockerStatus_id']);
            }
        }
        if (isset($options['lockerStyle_id'])) {$this->db->where('lockerStyle_id', $options['lockerStyle_id']);}
        if (isset($options['business_id'])) {$this->db->where('business_id', $options['business_id']);}
        if (isset($options['not_lockerName'])) {$this->db->where('lockerName != ', $options['not_lockerName']);}


        $this->db->join('lockerStyle','lockerStyle.lockerStyleID=locker.lockerStyle_id');
        $this->db->join('lockerLockType','lockerLockType.lockerLockTypeID=locker.lockerLockType_id');

        if (isset($options['with_location'])) {
            $this->db->join('location', 'location.locationID = locker.location_id');
            $this->db->join('locationType', 'locationTypeID = location.locationType_id');
        }

        if (isset($options['with_order'])) {
            $this->db->join('orders', 'lockerID = orders.locker_id', 'left');
            if (isset($options['not_orderStatusOption_id'])) {$this->db->where_not_in('orderStatusOption_id', $options['not_orderStatusOption_id']);}
        }

        if (isset($options['with_status'])) {
            $this->db->join('lockerStatus', 'lockerStatusID = locker.lockerStatus_id', 'left');
        }

        if (isset($options['with_claim'])) {
            $this->db->join('claim', 'claim.locker_id = lockerID', 'left');
            $this->db->join('customer', 'claim.customer_id = customerID', 'left');
        }

        if (isset($options['select'])) {$this->db->select($options['select']);}
        if (isset($options['group_by'])) {$this->db->group_by($options['group_by']);}
        if (isset($options['order_by'])) {$this->db->order_by($options['order_by']);}

        $query = $this->db->get('locker');
        //echo $this->db->last_query();
        return $query->result();

    }




    public function update($options = array(), $where = array())
    {
            $this->db->update('locker', $options, $where);

            return $this->db->affected_rows();
    }


    /**
     * delete method deletes rows from the database
     *
     * NOTES:
     * if we are doing a mass delete, the options value will be batch_lockerIDs
     *
     */
    public function delete($options= array())
    {
        if (isset($options['batch_lockerIDs'])) {
            $array = explode(",",$options['batch_lockerIDs']);
            $this->db->where_in('lockerID', $array);
            $this->db->delete('locker');
        } else {
            $this->db->delete('locker', $options);
        }

        return $this->db->affected_rows();
    }
    
    /**
      * add_location_locker adds a locker to a location
      * I first check to see if the lockerName is already added to the location
      *
      * Options Values
      * ------------
      * location_id
      * lockerName
      * lockerType_id
      * lockerStyle_id
      *
      * @param array $options
      * @return int $insert_id
     */
     function insert($options = array(), $batch=false)
     {
         //Add prefix to lockername
         if ($batch) {
             foreach ($options as $k => $o) {
                $options[$k]['lockerName'] = $options[$k]['prefix'].$options[$k]['lockerName'];
                unset($options[$k]['prefix']);
             }
         } else {
             $options['lockerName'] = $options['prefix'].$options['lockerName'];
             unset($options['prefix']);
         }
         
        //Check to see if the lockerName is already added
         if ($batch) {
             $location_id = $options[0]['location_id'];
             foreach ($options as $o) {
                $a[] = $o['lockerName'];
             }
         } else {
             $location_id = $options['location_id'];
             $a[] = $options['lockerName'];
         }


         $this->db->where('location_id',$location_id);
         $this->db->where_in('lockerName', $a);
         $q = $this->db->get('locker');
         if ($q->result()) {
             return array('status'=>'fail','message'=>'A locker already exists');
         }




         //proceed
         if ($batch) {
             $this->db->insert_batch('locker', $options);

             return array('status'=>'success','message'=>'Lockers have been added');
         } else {
            $this->db->insert('locker', $options);

            return array('status'=>'success','message'=>'Locker has been added');
         }
     }

     /**
      *
      * Checks to see if a locker exists in a location
      *
      * @param string $lockerName
      * @param int $location_id
      * @return boolean true if found
      */
     function doesLockerExistInLocation($lockerName, $location_id)
     {
         $sql = "SELECT * FROM locker
                 WHERE location_id = '".$location_id."'
                 AND lockerName = '".$lockerName."'";
         $query = $this->db->query($sql);
         if ($query->result()) {
             return true;
         }

         return false;
     }

     /**
      * get_locker_style method returns a list of locker styles
      */
     function get_locker_Style($options = array())
     {
          if (isset($options['lockerStyleID'])) {$this->db->where('lockerStyleID', $options['lockerStyleID']);}
          if (isset($options['name'])) {$this->db->where('name', $options['name']);}
          if (isset($options['abbreviation'])) {$this->db->where('abbreviation', $options['abbreviation']);}
          if (isset($option['select'])) {$this->db->select($options['select']);}

          $this->db->order_by('name');

          $query = $this->db->get('lockerStyle');

          return $query->result();
     }


     /**
      * returns a list of locker statuses
      */
     function get_locker_status($options = array())
     {
         if (isset($options['lockerStatusID'])) {$this->db->where('lockerStatusID', $options['lockerStatusID']);}
         if (isset($options['name'])) {$this->db->where('name', $options['name']);}
         if (isset($option['select'])) {$this->db->select($options['select']);}

         $this->db->order_by('lockerStatusID');

         $query = $this->db->get('lockerStatus');

         return $query->result();
     }

     /**
      * method to get the location when only a locker ID is known
      * @param int locker_id
      * @return array of objects
      */
     function get_locker_location($locker_id)
     {
        $this->db->select('location.*');
        $this->db->join('location', "locationID = locker.location_id");
        $this->db->where('lockerID', $locker_id);
        $query = $this->db->get('locker');

        return $query->result();
     }

     /**
      * get_locker_type method returns a list of locker styles
      */
     function get_locker_lock_type($options = array())
     {
          if (isset($options['lockerlockTypeID'])) {$this->db->where('lockerlockTypeID', $options['lockerlockTypeID']);}
          if (isset($options['name'])) {$this->db->where('name', $options['name']);}
          if (isset($option['select'])) {$this->db->select($options['select']);}

          $this->db->order_by('name');

          $query = $this->db->get('lockerLockType');

          return $query->result();
     }

    /**
     * moves an order in an electronic locker to the inProcess locker for the location
     *
     * @param int $order_id
     * @param stdClass $locker
     *
     * $locker values
     * -------------------
     * lockerID
     * location_id
     *
     * @return array (status, old_locker_id, new_locker_id, message)
     */
    public function move_to_process_locker($order_id, $locker)
    {
        //get the location
        $this->db->where(array('location_id'=>$locker->location_id));
        $this->db->where(array('lockerStyle_id'=>7)); //inProcess locker Style
        $query = $this->db->get('locker');
        if (!$process_locker = $query->result()) {
                $message = 'Process locker not found for this location '.$locker->location_id;

                return array('status'=>'fail', "old_locker_id"=>$locker->lockerID, 'new_locker_id'=>'', 'message'=>$message);
        }
        $process_locker = $process_locker[0];

        //update the locker id for the order;
        if ($locker->lockerID == $process_locker->lockerID) {
                $message = "This order was already in the process locker";
        } else {
                $this->load->model('order_model');
                $this->order_model->orderID = $order_id;
                $this->order_model->locker_id = $process_locker->lockerID;
                $this->order_model->update();
                $message = "Updated the locker id";
        }

        return array('status'=>'success', "old_locker_id"=>$locker->lockerID, 'new_locker_id'=>$process_locker->lockerID, 'message'=>$message);

    }



}
