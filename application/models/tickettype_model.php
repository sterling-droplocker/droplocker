<?php

class TicketType_Model extends My_Model
{
    protected $tableName   = 'ticketType';
    protected $primary_key = 'ticketTypeID';

    public function get_by_business_id($business_id)
    {
        $query = $this->db->query("SELECT * from ticketType WHERE business_id = ?", array($business_id));
        if ($this->db->_error_message()) {
            throw new Exception($this->db->_error_message());
        }
        return $query->result();
    }

}