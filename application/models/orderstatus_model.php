<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Locker;

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 */
class OrderStatus_Model extends My_Model
{
    protected $tableName = 'orderStatus';
    protected $primary_key = 'orderStatusID';

    /**
     * looks for recent orders in a locker that were recently placed
     *
     * @param int $locker_id
     * @param dateTime $dateCreated
     * @throws Exception multiple orders found
     * @return int order_id
     */
    public function getRecentLockerOrder($locker_id, $dateCreated)
    {
        $sql = "SELECT o.customer_id, o.orderID, o.business_id FROM orderStatus
                INNER JOIN orders o ON o.orderID = order_id
                WHERE orderStatus.locker_id = {$locker_id}
                AND orderStatus.orderStatusOption_id = 1
                AND orderStatus.date > DATE_ADD('" . $dateCreated . "', INTERVAL -2 HOUR)";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 1) {
            $order = $query->row();
            $locker = new Locker($locker_id);
            $body = "This locker has more than one order in it with pickup status as pickup [orderStatusOptionID 1]
            and the orders were placed within 2 hours of eachother. Therefore, there is a good chance these orders
            have the same customer. Please check the order for this locker. <br><br>";
            $body .= "LockerName: " . $locker->lockerName . "<br>";
            $body .= "LockerID: " . $locker_id . "<br>";
            $body .= "<a href='https://droplocker.com/admin/orders/details/" . $order->orderID . "'>orderID: " . $order->orderID . "</a><br>";
            send_admin_notification('Found a locker with multiple orders in pick up status.', $body, $order->business_id);

            return '';
        } elseif ($query->num_rows() == 1) {
            $order = $query->row();

            return $order->orderID;
        } else {
            return 0;
        }
    }

    /**
     * Sets the order status.
     * I made a function because I want want to check if the employee_id is passed.
     *
     * @param array $options
     *            Required Values
     *            -------------------
     *            order_id
     *            orderStatusOption_id
     *            locker_id
     *            * Optional values
     *            * employee_id
     *
     * @return int insert_id
     */
    public function update_orderStatus($options = array())
    {
        if (! is_numeric($options['order_id'])) {
            throw new Exception("'order_id' must be numeric in the options array.");
        }
        if (! is_numeric($options['orderStatusOption_id'])) {
            throw new Exception("'orderStatusOption_id' must be numeric in the options array.");
        }
        if (! is_numeric($options['locker_id'])) {
            throw new Exception("'locker_id' mus be numeric in the options array.");
        }

        $this->load->model('locker_model');
        $this->load->model('customer_model');
        $this->load->model('order_model');

        if (! isset($options['order'])) {
            $order = $this->order_model->get(array(
                'orderID' => $options['order_id']
            ));
            $order = $order[0];
        } else {
            $order = $options['order'];
        }

        // get the customer
        $this->customer_model->customerID = $order->customer_id;
        $customer = $this->customer_model->get();
        $customer = $customer[0];

        // easier to type
        $oso_id = $options['orderStatusOption_id'];
        $locker_id = $options['locker_id'];
        $order_id = $options['order_id'];
        $username = $customer->username;
        $options['customer_username'] = $username;

        if ($order->orderStatusOption_id == $oso_id) {
            return "order Status is already set to " . $oso_id;
        }

        // check to see if the statusOption has already been set
        $q = $this->db->get_where($this->tableName, array(
            "order_id" => $order_id,
            "orderStatusOption_id" => $oso_id
        ));
        if ($row = $q->result()) {
            // die("order Status for {$order_id} is already set to ".$oso_id);
        }

        // if status has been changed to inventoried
        if ($oso_id == 3) {

            if (! $customer) {
                die("Cannot find customer for this order");
            }

            if ($customer->inactive == 1) {
                queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($order->business_id, 'customerSupport'), 'Order for inactive customer', 'Order ' . $order_id . ' was inventoried for an inactive customer`s account.  Please investigate ASAP.', array(), null, $order->business_id);

                return false;
            }

            // TODO there is customer code for LL for certain location - need to know how to handle it

            // get locker type
            $query = $this->db->get_where('locker', array(
                'lockerID' => $locker_id
            ));
            $locker = $query->result();
            $locker = $locker[0];
            
            if (Locker::isElectronicLock($locker->lockerLockType_id)) { // type = 2
                // move order to the process locker
                $result = $this->locker_model->move_to_process_locker($order_id, $locker);
                if ($result['status'] == 'fail') {
                    echo $result['message']; // display what went wrong
                }
            }
        }

        // if changing to Washed
        if ($oso_id == 4) {
            if (! $customer->autoPay || $customer_id == 0) {
                // update to payment hold
                $options['orderPaymentStatusOption_id'] = 7;
            } else {
                // charge card
                // TODO find out how to charge card
                // $options['orderPaymentStatusOption_id'] = 3;
            }
        }

        // order has been completed
        if ($oso_id == 10) {
            // $this->closedGross = $this->getItemTotal();
            $this->load->model('orderitem_model');
            $item_array = $this->orderitem_model->get_item_total($order_id);
            // $this->closedDisc = $this->getDiscounts();
            $discount_array = $this->orderitem_model->get_discounts($order_id);

            // get employee match
            // TODO: We are going to handle employees differently... This will most likely change
            $emp_discounts = $this->orderitem_model->get_employee_discounts();
            if ($emp_discounts) {
                // create an order for the credit that this customer received
                preg_match("/\[(.*)\]/", $emp_discounts[0]->chargeType, $matches);
                $custId = $matches[1];
                $matchInfo = 'Employer match for ' . getPersonName($emp_disscounts[0]). ' order: ' . $emp_discounts[0]->order_id;
                $chargeArr = array(
                    'chargeType' => $matchInfo,
                    'chargeAmount' => number_format($emp_discounts[0]->chargeAmount * - 1, 2, '.', '')
                );

                $options = array();
                $options['charges'] = array($chargeArr);
                $options['orderStatusOption_id'] = 10;
                $options['customer_id'] = $custId;
                $new_order_id = $this->order_model->new_order($emp_discounts[0]->location_id, $emp_discounts[0]->locker_id, 0, null, null, $matchInfo, $order->business_id, $options);
                queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($order->business_id, 'customerSupport'), 'Employer match order', 'Employer match order: ' . $new_order_id . ' created!', array(), null, $order->business_id);
            }

            // locker loot
            // give the customer 1 locker loot for every dollar spent
            if ($customer->type != 'wholesale') {
                $points = $item_array['total'];
                $points = $points / 100;
                $rows = '';

                if ($points > 0) {
                    // don't apply loot twice
                    $query2 = "select * from lockerLoot where order_id = {$order_id} and customer_id = {$customer->customerID}";
                    $query = $this->db->query($query2);
                    $rows = $query->result();
                    if (! $rows) {
                        $insert1 = "INSERT into lockerLoot (order_id, points, customer_id, description)
                                                                    VALUES ('$order_id', '$points', '$customer->customerID', '1% Rebate');";
                        $insert = $db->query($insert1);

                        // find out if anyone referred this customer if the order was over $25 and this is one of their first 5 orders over $25
                        if ($points >= 25) {
                            $query1 = "SELECT customer.referredBy, refer.type
                                                                            FROM customer
                                                                            join customer as refer on refer.id = customer.referredBy
                                                                            WHERE customer.id = '$customer->customerID';";
                            $query = $this->db->query($query1);
                            $lines = $query->result();
                            foreach ($lines as $line) {
                                // don't give locker loot to employees
                                if ($line->type == 'delivery' or $line->type == 'employee') {} else {
                                    // if so, give the person that referred them 10 dollars
                                    if ($line->referredBy > 0) {
                                        // only give it on the first 5 orders so see how many times it has been given already
                                        $query4 = "SELECT orders.customer_id, count(orders.customer_id) as numOrd
                                                                                    FROM lockerLoot
                                                                                    join orders on orderID = lockerLoot.order_id
                                                                                    where lockerLoot.customer_id = '{$line->referredBy}'
                                                                                    and orders.customer_id = '{$customer->customerID}'
                                                                                    group by orders.customer_id";
                                        $query4 = $this->db->query($query4);
                                        $line4 = $query4->result();
                                        if ($line4[0]->numOrd <= 4) {
                                            $insert1 = "INSERT into lockerLoot (order_id, points, customer_id, description)
                                                                                                            VALUES ('$order_id', '10', '$line->referredBy', '$10 per first 5 orders referral');";
                                            $insert = $this->db->query($insert1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (! $this->order_model->update(array(
            'orderStatusOption_id' => $oso_id
        ), array(
            'orderID' => $order->orderID
        ))) {
            die($this->db->last_query());
        }

        // insert the data
        unset($options['order']);
        $this->db->insert($this->tableName, $options);
        $orderStatusID = $this->db->insert_id();

        if ($orderStatusID) {
            set_flash_message('success', "Order Status has been updated");

            return $orderStatusID;
        }
    }

    /**
     * This returns a simple array key=>value
     */
    public function simple_array()
    {
        $this->db->select('orderStatusOptionID, name');
        $q = $this->db->get('orderStatusOption');
        $res = $q->result();
        foreach ($res as $r) {
            $a[$r->orderStatusOptionID] = $r->name;
        }

        return $a;
    }

    /**
     *
     *
     *
     * Gets the history for an order
     *
     * TODO: This is very similar to getHistory. I recommend removing getHistory and replace with this
     *
     * @param int $order_id
     * @param int $visible
     *            0,1
     * @return array of objects
     */
    public function get_order_history($order_id, $visible = 0)
    {
        $sql = "SELECT name, date FROM orderStatus
                INNER JOIN orderStatusOption o ON orderStatusOptionID = orderStatusOption_id
                WHERE order_id = {$order_id} AND visible >= {$visible}
                ORDER BY date DESC";

        return query($sql);
    }

    /**
     *
     * @deprecated Use get_history in the order model isntead
     *             getHistory method gets the status history of the order. This method combines status and payment information.
     *
     * @param int $orderID
     * @param int $visible
     * @return multitype:NULL
     */
    public function getHistory($orderID, $visible = 0)
    {
        $sqlGetStatus = "SELECT name, date, e.username, note, lockerName, lockerID
                                                    FROM (`orderStatus`)
                                                    JOIN `orderStatusOption` ON `orderStatusOptionID` = `orderStatusOption_id`
                                                    LEFT JOIN employee e on employeeID = employee_id
                                                    LEFT JOIN `locker` ON `lockerID` = `locker_id`
                                                    WHERE `orderStatus`.`order_id` = '$orderID' AND visible >= {$visible}
                                                    ORDER BY date desc";
        $query = $this->db->query($sqlGetStatus);
        $statuses = $query->result();

        $this->load->model('transaction_model');
        $transactions = $this->transaction_model->getTransactionHistory($orderID);
        foreach ($transactions as $transaction) {
            switch ($transaction->processor) {
                case 'verisign':
                    $link = 'https://manager.paypal.com/searchTranx.do?subaction=transDetails&timezone=U.S.%20Pacific&reportName=SearchTransaction&id=' . $transaction->pnref;
                    break;
                case 'authorizenet':
                    $link = 'https://authorize.net/transaction/transactiondetail.aspx?transID=' . $transaction->pnref;
                    break;
                case 'paypro':
                    $link = "https://businessview.paygateway.com";
                    break;
            }
            $customer = new Customer($transaction->customer_id);
            $history[] = array(
                'customer' => $transaction->employee_id,
                'date' => $transaction->updated,
                'name' => 'Paid <div style="float:right; padding-left:10px;">($' . $transaction->amount . ')</div><br><a style="float:right" href="' . $link . '" target="_blank">' . $transaction->pnref . '</a>',
                'note' => getPersonName($customer) . "<br> CustomerID: <a href='https://droplocker.com/admin/customers/detail/" . $customer->customerID . "'>" . $customer->customerID . "</a>",
                'customer' => '',
                'locker' => ''
            );
        }

        foreach ($statuses as $status) {
            $history[] = array(
                'date' => $status->date,
                'name' => $status->name,
                'customer' => $status->username,
                'locker' => $status->lockerName,
                'note' => $status->note,
                'lockerID' => $status->lockerID
            );
        }

        // multisort by time
        if ($history) {
            foreach ($history as $key => $row) {
                $date[$key] = $row['date'];
            }
            array_multisort($date, SORT_DESC, $history);
        }

        return $history;
    }
    
    public function getItem($orderStatus_id)
    {
        $sql = "SELECT orderStatusOption_id, name, date, CONCAT(e.firstName, ' ', e.lastName) employeeName
            FROM orderStatus
            LEFT JOIN orderStatusOption o ON orderStatusOptionID = orderStatusOption_id
            LEFT JOIN employee e on employeeID = employee_id
            WHERE orderStatusID = $orderStatus_id
            ORDER BY date DESC";

        return query($sql);
    }

 
    /**
     *
     * Get an specific status data for order ID
     *
     * @param int $orderID
     * @param int $orderStatusOptionID
     * @return multitype:NULL
     */
    public function getOrderStatusHistoryByStatusID($orderID, $orderStatusOptionID)
    {   
        $sql = "select * from orderStatus os JOIN orderStatusOption oso
            ON oso.orderStatusOptionID = os.orderStatusOption_id
            WHERE os.order_id = ?
            AND oso.orderStatusOptionID = ?
            ORDER BY oso.orderStatusOptionID DESC";
        $result = $this->db->query($sql, array($orderID, $orderStatusOptionID))->result();
        return empty($result)?false:$result[0];
    }
}
