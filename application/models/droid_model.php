<?php

/**
 * My_model extends CI_Model and provides access to CRUD
 * __set() : set options
 * ---------------------
 *
 * get()
 * insert()
 * update() : set options[where]
 * delete() : set options[where]
 *
 */
class Droid_Model extends My_Model
{
    protected $tableName = 'droid';
    protected $primary_key = 'droidID';

    /**
     * Inserts data into the droid table
     *
     * Options values
     * --------------------
     * transaction_id
     * postData
     * methodCalled
     * dateCreated
     * status
     *
     * @param array $options
     *
     */
    public function insert($options = array())
    {
        $this->db->insert('droid', $options);

        return $this->db->insert_id();
    }

}
