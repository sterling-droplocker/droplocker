<?php

class Customer_Location_Model extends My_Model
{
    protected $tableName = 'customer_location';
    protected $primary_key = 'ID';

    public function get_customer_locations($customer_id, $business_id)
    {
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }
        if (!is_numeric($business_id)) {
            throw new Exception("'busines_id' must be numeric.");
        }
        $this->db->where("customer_id", $customer_id);
        $this->db->join('location', "customer_location.location_id = locationID and location.business_id = $business_id");
        $this->db->order_by("address");
        $get_customer_locations_query = $this->db->get($this->tableName);

        return $get_customer_locations_query->result();
    }
    /**
     * The following function is unknown  , do not use
     * @deprecated This   does not follow established coding standards and its purpose is not known
     *
     * --------------------
     *
     * @param type $customer_id
     * @param type $business_id
     * @return type
     */
    public function simple_array($customer_id, $business_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->join('location', 'customer_location.location_id = locationID and location.business_id = '.$business_id);
        $this->db->select('location_id');
        $q = $this->db->get('customer_location');
        $location_id = $q->result();
        $a = array();
        foreach ($location_id as $l) {
                $a[] = $l->location_id;
        }

        return $a;
    }

    /**
     * The following function is  , do not use
     * @deprecated Use the deletion functionality in the Customer Location Droplocker object instead.
     *
     * ------------------------------------
     * @param array $options
     * @return int
     */
    public function delete($options = array())
    {
        $this->db->where('customer_id', $options['customer_id']);
        $this->db->delete('customer_location', $options);

        return $this->db->affected_rows();
    }

    /**
     * Get customers by location name
     *
     * ------------------------------------
     * @param int $business_id
     * @param string $location_name location name or all
     * @param string $type [customers_last_month|customers_this_month|new_customers_this_month]
     * @return int
     */
    public function get_customers_by_location($business_id, $location_name, $type = 'customers_last_month')
    {
        $interval = "- INTERVAL 1 MONTH"; 
        
        if ($type == "customers_this_month") {
            $interval = "";
        }

        $new_customer_join = "";
        $new_customer_date_range = "";
        if ($type == "new_customers_this_month") {
            $interval = "";
            $new_customer_join = " JOIN customer on customer.customerID = orders.customer_id";
            $new_customer_date_range = "AND customer.signupDate BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW()), '%Y-%m-%d 23:59:59')";
        }

        $sql = "SELECT 
                    COUNT(DISTINCT orders.customer_id) AS customers
                FROM
                    orders
                        JOIN
                    locker ON lockerID = orders.locker_id
                        JOIN
                    location ON locker.location_id = locationID
                        JOIN
                    locationType ON locationType_id = locationTypeID
                        JOIN
                    customer_location ON customer_location.customer_id = orders.customer_id and customer_location.location_id=location.locationID
                    {$new_customer_join}
                WHERE
                    bag_id <> 0 AND orders.business_id = ?
                        AND orders.dateCreated BETWEEN DATE_FORMAT(NOW() {$interval},
                            '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW() {$interval}),
                            '%Y-%m-%d 23:59:59')
                        {$new_customer_date_range}";
        
        $conditions = array($business_id);
        if ($location_name!="Overall") {
            $sql .= " AND locationType.name = ?";
            $conditions[] = $location_name;
        }

        $customers = $this->db->query($sql, $conditions)->result();
        if (!empty($customers)){
            return $customers[0]->customers;
        } 
        return 0;
    }
}
