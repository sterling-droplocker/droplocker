<?php

class Cash_Register_Record_Order_Model extends My_Model
{
    protected $tableName = 'cash_register_record_order';
    protected $primary_key = 'cash_register_record_orderID';
    
    public function getCashSaleOrders($cash_register_id, $employeeData, $date_range_local)
    {
        $paymentMethodSlug = 'cash';
        $recordTypeSlug = 'sale';
        
        return $this->getOrders($cash_register_id, $employeeData, $date_range_local, $paymentMethodSlug, $recordTypeSlug);
    }

    public function getOrders($cash_register_id, $employeeData, $date_range_local, $paymentMethodSlug, $recordTypeSlug)
    {
        $employee_id = $employeeData->employeeID;
        
        $this->load->model('cash_register_record_model');
        $this->cash_register_record_model->checkCashRegisterEmployee($cash_register_id, $employee_id);

        $this->db->join('cash_register_record', 'cash_register_record_id=cash_register_recordID', 'left');
        $this->db->join('orders', 'order_id=orderID', 'left');
        $this->db->join('customer', 'customer_id=customerID', 'left');

        $this->db->where("cash_register_id", $cash_register_id);
       
        if ($paymentMethodSlug) {
            $this->db->join('payment_method', 'payment_method_id=payment_methodID', 'left');
            $this->db->where("payment_method.slug", $paymentMethodSlug);
        }
       
        if ($recordTypeSlug) {
            $this->db->join('cash_register_record_type', 'cash_register_record_type_id=cash_register_record_typeID', 'left');
            $this->db->where("cash_register_record_type.slug", $recordTypeSlug);
        }
        
        if ($date_range_local) {
            $date_format = "Y-m-d H:i:s";
            $date_range_gmt = array (
                'from_date' => convert_from_local_to_gmt($date_range_local['from_date'], $date_format, $employeeData->business_id),
                'to_date' => convert_from_local_to_gmt($date_range_local['to_date'], $date_format, $employeeData->business_id),
            );
        
            $this->db->where('cash_register_record.date_created' . " >= ", $date_range_gmt['from_date']);
            $this->db->where('cash_register_record.date_created' . " <= ", $date_range_gmt['to_date']);
        }
        
        $this->db->select("{$this->tableName}.*, "
            . "cash_register_record.date_created , "
            . "(orders.orderPaymentStatusOption_id = 3) `is_paid`, "
            . "customer.firstName, "
            . "customer.lastName");

        $query = $this->db->get($this->tableName);
        $results = $query->result();

        foreach ($results as $key => $item) {
            $item->date_created_local = getLocalDateTimeInfo($item->date_created, $employeeData->business_id);
            $results[$key] = $item;
        }
        
        $data = array(
            'orders' => $results,
            'source' => array_merge((array)$date_range_gmt, array(
                'paymentMethodSlug' => $paymentMethodSlug, 
                'recordTypeSlug' => $recordTypeSlug,
            )),
        );

        return array(
            'success' => TRUE,
            'data' => $data,
        );
    }
    
}
