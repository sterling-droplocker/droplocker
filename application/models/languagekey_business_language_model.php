<?php

class LanguageKey_Business_Language_Model extends MY_Model
{
    protected $tableName = 'languageKey_business_language';
    protected $primary_key = 'languageKey_business_languageID';

    /**
     * Returns all language keys in a language for in a language defined by the specified business
     * @param int $business_id
     * @return array
     */
    public function get_languageKeys_for_languages_for_languageViews_for_business($business_id)
    {
        $this->load->model("languageView_model");
        $languageViews = $this->languageView_model->get_all();

        $this->load->model("language_model");
        $business_languages = $this->business_language_model->get_all_for_business($business_id);

        $languageViews_languages_languageKeys = array();
        foreach ($languageViews as $languageView) {
            $languages = array();
            foreach ($business_languages as $business_language) {

                $languageKey_business_languages = $this->getTranslationsForLanguageView($business_language->languageID, $business_id, $languageView->languageViewID);
                $languageKeys = array();
                foreach ($languageKey_business_languages as $languageKey_business_language) {
                    $languageKeys[] = array(
                        "name" => $languageKey_business_language->name,
                        "translation" => $languageKey_business_language->translation,
                        "defaultText" => $languageKey_business_language->defaultText,
                        "languageKeyID" => $languageKey_business_language->languageKeyID,
                        "languageView_id" => $languageView->languageViewID,
                    );
                }

                $languages[$business_language->languageID] = array('tag' => $business_language->tag, 'description' => $business_language->description, "languageKeys" => $languageKeys);
            }
            $languageViews_languages_languageKeys[$languageView->languageViewID] = array('name' => $languageView->name, "languages" => $languages);
        }

        return $languageViews_languages_languageKeys;
    }


    /**
     * Returns all the business defined translations for a particular language view
     * @param int $language_id
     * @param int $business_id
     * @param int $languageView_id
     * @return array
     */
    public function getTranslationsForLanguageView($language_id, $business_id, $languageView_id)
    {
        $get_translations_query = $this->db->query("
            SELECT languageKey.name,languageKey.languageKeyID, languageKey_business_language.languageKey_id,languageKey_business_language.language_id,languageKey_business_language.translation, languageKey.defaultText FROM languageKey
                LEFT JOIN languageView ON languageKey.languageView_id = languageView.languageViewID
                LEFT JOIN languageKey_business_language
                    ON languageKey.languageKeyID = languageKey_business_language.languageKey_id
                    AND languageKey_business_language.language_id = ?
                    AND languageKey_business_language.business_id = ?
            WHERE languageKey.languageView_id = ?", array($language_id, $business_id, $languageView_id));
        $translations = $get_translations_query->result();

        return $translations;
    }
    /**
     * Retrieves all language view key translation for the specified language views for the specified language for the specified business.
     * @param int $language_id
     * @param int $business_id
     * @param mixed $languageViewNames To specify a single language view, pass a string. To specify multiable language views, pass an array.
     * @return array
     */
    public function getTranslationsForLanguageViewNameForLanguageNameForBusiness($language_id, $business_id, $languageViewNames)
    {
        $this->load->model("languageview_model");
        $translations = array();
        if (is_array($languageViewNames)) {
            foreach ($languageViewNames as $languageViewName) {
                $languageView = $this->languageview_model->findByName($languageViewName);
                $translationKeys = $this->getTranslationsForLanguageView($language_id, $business_id, $languageView->languageViewID);
                if (empty($translationKeys)) {
                    $translations[$languageViewName] = array();
                } else {
                    foreach ($translationKeys as $translationKey) {
                        if (empty($translationKey->translation)) {
                            $translations[$languageViewName][$translationKey->name] = $translationKey->defaultText;
                        } else {
                            $translations[$languageViewName][$translationKey->name] = $translationKey->translation;
                        }
                    }
                }
            }
        } else {
            $languageViewName = $languageViewNames;
            $translationKeys = $this->languageview_model->getTranslationForLanguageView($languageViewName);
            foreach ($translationKeys as $translationKey) {
                if (empty($translationKey->translation)) {
                    $translations[$languageViewName][$translationKey->name] = $translationKey->defaultText;
                } else {
                    $translations[$languageViewName][$translationKey->name] = $translationKey->translation;
                }
            }
        }
        return $translations;
    }

    /**
     * Retrieves a translation for the specified language key in the specified language view
     * @param int $language_id
     * @param int $business_id
     * @param int $languageView_id
     * @param int $languageKey_id
     * @return array
     */
    public function get_translation_for_languageKey_in_languageView($language_id, $business_id, $languageView_id, $languageKey_id)
    {
        $get_translation_query = $this->db->query("
            SELECT languageKey.name,languageKey.languageKeyID, languageKey_business_language.languageKey_id,languageKey_business_language.language_id,languageKey_business_language.translation, languageKey.defaultText FROM languageKey
                JOIN languageView ON languageKey.languageView_id = languageView.languageViewID
                LEFT JOIN languageKey_business_language
                    ON languageKey.languageKeyID = languageKey_business_language.languageKey_id
                    AND languageKey_business_language.language_id = ?
                    AND languageKey_business_language.business_id = ?
            WHERE languageKey.languageView_id = ?
                AND languageKey.languageKeyID = ?", array($language_id, $business_id, $languageView_id, $languageKey_id ));

        $translation = $get_translation_query->row();

        return $translation;
    }
}
