<?php

class CustomOption_Model extends My_Model
{
    protected $tableName = 'customOption';
    protected $primary_key = 'customOptionID';

    public function getMaxSortOrder($business_id)
    {
        $this->db->select_max('sortOrder');
        $this->db->from($this->tableName);
        $this->db->where(array(
            'business_id' => $business_id
        ));

        $row = $this->db->get()->row();
        return $row->sortOrder;
    }

    public function updateSortOrder($business_id, $option_id, $newOrder, $oldOrder)
    {
        // If this has an oldOrder, then a hole has been left in the oldOrder position
        if ($oldOrder) {
            $this->db->set('sortOrder', 'sortOrder - 1', false);
            $this->db->where(array(
                'business_id' => $business_id,
                'sortOrder >' => $oldOrder,
                'customOptionID != ' => $option_id
            ));
            $this->db->update($this->tableName);
        }

        // then we make room for the new sortOrder
        if ($newOrder) {
            $this->db->set('sortOrder', 'sortOrder + 1', false);
            $this->db->where(array(
                'business_id' => $business_id,
                'sortOrder >=' => $newOrder,
                'customOptionID != ' => $option_id
            ));
            $this->db->update($this->tableName);
        }
    }

    public function getCustomOptions($business_id, $business_languageID)
    {
        $this->db->join("customOptionName", "customOption_id = customOptionID", "LEFT");
        $this->db->where(array(
            'business_id' => $business_id,
            'business_language_id' => $business_languageID
        ));

        $this->db->order_by("sortOrder", "ASC");
        $options = $this->db->get("customOption")->result();

        $this->load->model('customoptionvalue_model');

        $out = array();
        foreach ($options as $option) {
            $out[$option->name] = $option;
            $out[$option->name]->values = $this->customoptionvalue_model->getByLanguage($option->customOptionID, $business_languageID);
        }

        return $out;
    }

}