<?php
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Supplier;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\OrderItem;

class OrderItem_Model extends My_Model
{
    protected $tableName = 'orderItem';
    protected $primary_key = 'orderItemID';

    /**
     *The following function is *, do not use.
     * @deprecated This is *, do not use. The results for the total types are wrong. Use get_subtotal, get_gross_total, get_net_total in the order model instead instead.
     * Retrieves the total, subtotal, and charges for a particular order.
     * @param int order_id
     * @return array An array containing the calculated totals
     *  'subtotal' : The sub of all items in the order
     *  'discounts' : The sum of all charges in the order
     *  'total' : The sum of the subotal and discounts
    */
    public function get_item_total($order_id)
    {
        $subtotal = 0;
        $this->load->model('order_model');
        $items = $this->order_model->getItems($order_id);

        if ($items) {
            foreach ($items as $item) {
                $subtotal = $subtotal + $item->unitPrice * $item->qty;
                $order['items'][] = $item;
            }
        }

        $this->load->model('ordercharge_model');
        $this->ordercharge_model->order_id = $order_id;
        $this->ordercharge_model->select = "SUM(chargeAmount) as total";
        $charges = $this->ordercharge_model->get();
        $charge_total = $charges[0]->total;

        $order['subtotal'] = $subtotal;
        $order['discounts'] = $charge_total;

        $total = number_format($subtotal + $charge_total,2,'.', '');
        if ($total == -0) {
            $total = 0;
        }
        $order['total'] = $total;

        return $order;
    }

    /**
     * This is *, do not use.
     * @deprecated The terminology is wrong, do not use. Use get_net_total in the order model instead.
     * get the full total including taxable and non-taxable items
     * currently nothing in product_process is taxable
     */
    public function get_full_total($order_id, $business_id)
    {
        $this->db->where('order_id', $order_id);
        $query = $this->db->get($this->tableName);
        $items = $query->result();
        $total = 0;
        if (!$items) {
            return array('status' => 'fail', 'error' => 'No items in order');
        }

        $total = 0;
        foreach ($items as $item) {
            $item->qty = OrderItem_Model::getRoundedQty($business_id, $item->qty);
            $total = $total + $item->unitPrice * $item->qty;
            $order['items'][] = $item;
        }

        $this->load->model('ordercharge_model');
        $this->ordercharge_model->order_id = $order_id;
        $this->ordercharge_model->select = "SUM(chargeAmount) as total";
        $charges = $this->ordercharge_model->get();
        //echo $this->db->last_query();
        $charge_total = $charges[0]->total;

        $total = $total + $charge_total;

        $order['total'] = $total;

        $this->load->model('order_model');
        $total_tax = $this->order_model->get_tax_total($order_id);

        $order['taxable_total'] = $total_tax;
        $order['non_taxable_total'] = $total;
        $order['status'] = 'success';

        return $order;
    }

    /**
     * gets the price of an item. Checks to see if the product has a price
     *
     * return if the orderItem has a unitprice
     * return if the location has special product price
     * return product price
     */
    public function getPrice($order_id, $product_id)
    {
            //($i->unitPrice > 0)?$i->unitPrice:$this->product_model->getPrice($product[0], $order_id, $this->business_id);
            $order_item = $this->db->get_where($this->tableName, array('order_id' => $order_id, 'product_id' => $product_id));
            if ($order_item[0]->unitPrice > 0) {
                    return $order_item[0]->unitPrice;
            } else {
                    $this->load->model('product_model');
                    $product = $this->product_model->get(array('productID' => $product_id));
                    $unitPrice = $this->product_model->getPrice($product[0], $order_id, $this->business_id);

                    return $unitPrice;
            }

    }

    /**
    * get_discounts method gets all the items and the total charge for the order
    *
    * @param int order_id
    * @return array discounts
    */
    public function get_discounts($order_id)
    {
        $total = 0;

        $this->load->model('ordercharge_model');

        $charges = $this->ordercharge_model->getCharges($order_id);
        if ($charges) {
            foreach ($charges as $charge) {
                $total = $total + $charge->chargeAmount;
                $discounts['items'][] = $charge->chargeType;
            }
            $discounts['total'] = $total;

            return $discounts;
        }

        return false;
    }

    /**
     * get_employee_discounts
     *
     * TODO This will probably change in the future
     *
     * @param int $customer_id
     * @return array of objects
     */
    public function get_employee_discounts($customer_id)
    {
        $this->db->select("firstName, lastName, orderCharge.chargeType,dropLocationLocker.dropLocation_id, dropLocationLocker.id as lockerId, orderCharge.order_id, sum(orderCharge.chargeAmount) as chargeAmount");
        $this->db->join('order', 'orderID = orderCharge.order_id');
        $this->db->join('locker', 'lockerID = order.locker_id');
        $this->db->join('customer', 'customerID = order.customer_id');
        $this->db->where('orderCharge.order_id', $order_id);
        $this->db->like('chargeType', 'match[');
        $this->db->group_by('orderCharge.order_id');
        $query = $this->db->get("orderCharge");

        return $query->result();

        /*
         $select2 = "select firstName, lastName, orderCharge.chargeType,dropLocationLocker.dropLocation_id, dropLocationLocker.id as lockerId, orderCharge.order_id, sum(orderCharge.chargeAmount) as chargeAmount
         from orderCharge
         join `order` on `order`.id = orderCharge.order_id
         join dropLocationLocker on dropLocationLocker.id = `order`.dropLocationLocker_id
         join customer on customer.id = `order`.customer_id
         where orderCharge.order_id = $this->id
         and orderCharge.chargeType like '%match[%'
         group by orderCharge.order_id";
         //echo $select2;
         $query = $db->query($select2);

         */
    }

    /**
     * gets the orderitem when passed an item_id
     *
     * @param int $item_id
     * @param int $order_id
     * @return object $orderitem
     */
    public function get_order_item($item_id, $order_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->where('order_id', $order_id);
        $query = $this->db->get('orderItem');
        $orderItem = $query->result();

        return $orderItem;
    }

    /**
     * gets all the orders with the item associated customer ID of the client request
     *
     * @param int $item_id
     * @return array of objects
     */
    public function get_orders_with_item($item_id)
    {
            $this->db->select("order_id, updated, bagNumber, firstName, lastName, bag.notes");
            $this->db->join('orderItem', "orderItem.order_id = orders.orderID");
            $this->db->join("customer", "customerID = orders.customer_id");
            $this->db->join("bag", "bag.bagID = orders.bag_id");
            $this->db->where(array("orderItem.item_id" => $item_id, "orders.customer_id" => $this->customer_id));
            $this->db->order_by("updated desc");
            $get_orders_with_item_query = $this->db->get("orders");

            return $get_orders_with_item_query->result();
    }

    /**
     * deletes an item from an order
     *
     * @param int $orderItemID
     * @param int $employee_id
     * @return int
     */
    public function delete_item($orderItemID, $employee_id)
    {
        $orderItem = $this->get_by_primary_key($orderItemID);
        $item_id = $orderItem->item_id;
        $order_id = $orderItem->order_id;

        $barcode_note = '';
        if ($item_id) {
            $this->load->model('barcode_model');
            $this->barcode_model->item_id = $item_id;
            $barcode = $this->barcode_model->get();
            $barcode_note = ", Barcode: {$barcode[0]->barcode}";
        }

        if ($orderItem->product_process_id) {
            $product = Product::getName($orderItem->product_process_id);
            $productName = $product['displayName'];
        } else {
            $productName = 'No Product';
        }

        $this->db->delete('orderItem', array("orderItemID" => $orderItemID));
        $deleted = $this->db->affected_rows();
        if ($deleted) {
            // log the item in the orderStatus table
            $note = "Deleted Item: '$productName' $barcode_note";
            $this->logger->set('orderStatus', array(
                'order_id' => $order_id,
                'locker_id' => get_order_locker($order_id),
                'employee_id' => $employee_id,
                'orderStatusOption_id' => current_order_status($order_id),
                'note' => $note
            ));
        }

        return $deleted;
    }

    public function delete_item_and_clean($orderItemID, $order_id, $employee_id)
    {
        $orderitem = new OrderItem($orderItemID, false);
        $affected_rows = $this->delete_item($orderItemID, $employee_id);

        if ($affected_rows) {
            //remove assambly notes in the order of the item
            $order = new Order($order_id, false);
            if($order->postAssembly != ''){
                $assemblyNotes = $order->postAssembly.' ';
            }else{
                $assemblyNotes = '';
            }
            $this->load->model("barcode_model");
            $barcode = $this->barcode_model->get_one(array(
                "item_id" => $orderitem->item_id,
            ));
            $regex = "/".$barcode->barcode."\[(.*?)\]/i";

            $newAssemblyNotes = preg_replace($regex, '', $order->postAssembly);
            $where['orderID'] = $order_id;
            $options['postAssembly'] = trim($newAssemblyNotes);
            
            $this->load->model("order_model");
            $this->order_model->update($options, $where);
            
            $success = true;
            $message = "Item $orderItemID was deleted from order $order_id";
        } else {
            $message = "Item was NOT deleted from order";
        }

        return array(
            'success' => $success,
            'message' => $message,
        );
    }

    /**
        * checks to see if an item is already in an order
        *
        * @param int $item_id
        * @param int $order_id
        * @return boolean
        */
    public function inOrder($item_id, $order_id)
    {
            $query = $this->db->get_where($this->tableName, array('order_id'=>$order_id, 'item_id'=>$item_id));
            if($query->result())

                    return true;

            return false;
    }

    /**
     * Adds an item to an order
     *
     * @param int $order_id
     * @param int $item_id
     * @param int $product_process_id
     * @param int $qty
     * @param int $employee_id
     * @param float $price = 0
     * @param string $notes = null
     * @param int $autoOverwrite = 0 Delets and re-adds the item to the order
     * @param float $upchargeCost
     * @throws Exception
     * @return array $results
     */
    public function add_item($order_id, $item_id, $product_process_id, $qty, $employee_id, $supplier_id = '', $price = 0, $notes = null, $autoOverwrite = 0, $upchargeCost = 0, $allow_no_barcode = false)
    {

        $item = new Item($item_id);
        $order = new Order($order_id, false);

        $this->load->model('product_model');

        $default_wf_product_process = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $order->business_id);

        //don't let WF be added twice
        if (in_array($product_process_id, array($default_wf_product_process , WFTOWELSPRODUCTPROCESSID))) {
            $sql = "SELECT orderItemID, qty FROM orderItem WHERE order_id = ?
                            AND product_process_id IN (?,?)";
            $query = $this->db->query($sql, array($order->orderID, $default_wf_product_process, WFTOWELSPRODUCTPROCESSID));
            $wfitem = $query->row();

            if ($wfitem && $wfitem->orderItemID) {
                return array('status' => 'fail', 'error' => 'Wash Fold already exists in this order', 'error_id' => 'item_exists', 'orderItemID' => $wfitem->orderItemID);
            }
        }

        try {
            $locker = new Locker($order->locker_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new \User_Exception("Locker ID {$order->locker_id} not found in database");
        }
        try {
            $location = new Location($locker->location_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new \User_Exception("Location ID {$locker->location_id} not found in database");
        }

        try {
            $locker = new Locker($order->locker_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new User_Exception("Locker ID {$order->locker_id} not found in database");
        }
        try {
            $location = new Location($locker->location_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new User_Exception("Location ID {$locker->location_id} not found in database");
        }

        if (!is_numeric($product_process_id)) {
            throw new Exception("'product_process_id' must be integer");
        }

        // Check if the locker is PrePayment.
        if ($locker->lockerName == 'PrePayment') {
            return array('status' => 'fail', 'error' => 'This is the pre-payment location.  You can not add items to this order', 'error_id' => 'prepay_locker');
        }

        //see if this item is already in the order
        if ($this->inOrder($item->itemID, $order->orderID)) {
            if ($autoOverwrite == 0) {
                $existing_barcode = $item->relationships['Barcode'][0]->barcode;
                if ($existing_barcode) {
                    $existing_barcode_message = "Item barcode '{$item->relationships['Barcode'][0]->barcode}' already exists in this order";
                } else {
                    $existing_barcode_message = "Item #{$item->itemID} with no barcode assigned already exists in this order";
                }
                
               return array('status' => 'fail', 'error' => $existing_barcode_message, 'error_id' => 'item_exists');
            } else {
                //delete the item
                $sql = "delete from orderItem where item_id = " . $item->itemID . " and order_id = " . $order->orderID . ";";
                $this->db->query($sql);
            }
        }

        //get the product
        $product_process = new Product_Process($product_process_id);

        try {
            $product = new Product($product_process->product_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $product = null;
        }

        $getCustomer = "SELECT * FROM customer WHERE customerID = " . $order->customer_id;
        $q = $this->db->query($getCustomer);
        $customer = $q->row();

        // if wash fold, get the proper category based on customers default wf
        if ($product && $product->productCategory_id == WFCATEGORY) { //31 = WFCATEGORY
            if ($customer->defaultWF != 0 && $customer->defaultWF != $product->productID) {
                //change the product ID to the customers default WF productID
                $product_process = new Product_Process($customer->defaultWF);
                try {
                    $product = new Product($product_process->product_id);
                } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                    $product = null;
                }
            } else {
                throw new Exception("Customer does not have a default wash and fold product process ID set.");
            }
        }

        if (!$product) {
            if ($autoOverwrite == 1) {
                show_error('No product found, '.__METHOD__);
                send_admin_notification('Mapping Error', 'Could not find a product mapping for item: '.$item->itemID);
            } else {
                return array('status' => 'fail', 'error' => 'There is no mapping for the product / process combination', 'error_id' => 'no_product_mapping');
            }
        } else {

            if (empty($product_process->active)) {
                return array('status' => 'fail', 'error' => 'The product is not active', 'error_id' => 'inactive_product');
            }

            //if a price was passed, use that
            if ($price != 0) {
                $unitPrice = $price;
            } else {
                if ($order->llNotes != REDO_ORDER) {
                    $unitPrice = $product_process->price;
                }
            }
            // The following conditional checks to see if the product is under the detergent category and the current day is Thursday.
            // If the product is a detergent and the current day is Thursday, then we set the unit price to 0 before adding the item to the order..
            // Otherwise, we apply the regular product price.
            // Note, this conditional is currently hardedcoded so that only the business with the unique identifier of 3 should be applying the discount. This business is expected to be Laundry Locker.
            if ($product->productCategory_id == DETERGENT_CATEGORY_ID && date("w") == 4 && $this->business_id == 3 ) {
                $notes .= " Thursday Discount Promotion";
                $unitPrice = 0.00;
            }

            $this->load->model('product_location_price_model');
            $this->product_location_price_model->location_id =  $location->locationID;
            $this->product_location_price_model->product_process_id = $product_process_id;

            if ($location_discount = $this->product_location_price_model->get()) {
                $unitPrice = $location_discount[0]->price;
            }

            $upcharges = array();
            $preferences = array();
            if (!empty($item->itemID)) {
                //add any upcharges
                $sql = "SELECT upcharge_id, item_id, upcharge.upchargePrice as price, priceType, upcharge.name FROM itemUpcharge
                        INNER JOIN upcharge ON upchargeID = upcharge_id
                        WHERE item_id = '". $item->itemID."'";

                $upcharges = $this->db->query($sql)->result();

                // get customer preferences for this item
                $this->load->model('customerpreference_model');
                $customer_preferences = $this->customerpreference_model->get_preferences_data_for_category($customer->customerID, $product->productCategory_id);

                // mark preferences inherited from customer with an *
                $customer_preferences = array_map(function ($a) { $a->displayName .= '*'; return $a; }, $customer_preferences);

                // get the item preferences
                $this->load->model('itempreference_model');
                $item_preferences = $this->itempreference_model->get_preferences_data($item_id);

                // merge customer and item preferences
                $preferences = array_merge($customer_preferences, $item_preferences);
            }

            //get the supplier
            $cost = 0;
            if (empty($supplier_id)) {
                $this->load->model('product_supplier_model');
                $supplier = $this->product_supplier_model->getDefault($product_process_id, $order->orderID);
                if (isset($supplier->supplier_id)) {
                    $cost = $supplier->cost;
                    $supplier_id = $supplier->supplier_id;
                } else {
                    $supplier_id = $order->business_id;
                }
            } else {
                $sql = "SELECT * FROM product_supplier WHERE product_process_id = {$product_process_id} and supplier_id = {$supplier_id}";
                $q = $this->db->query($sql);

                if ($supplier = $q->row()) {
                    $supplier_id = $supplier->supplier_id;
                    $cost = $supplier->cost;
                } else {
                    $supplier_id = $supplier_id;
                    $cost = 0;
                }
            }

            // This adds the upcharges to the notes of the the item in the order
            $basePrice = $unitPrice;
            if ($upcharges) {
                foreach ($upcharges as $upcharge) {
                    //if this customer doesn't pay for upcharges make the price 0;
                    if ($customer->noUpcharge == 0) {
                        if ($upcharge->priceType == "dollars") {
                            $upchargeAmt = $upcharge->price;
                            $unitPrice = $upcharge->price + $unitPrice;
                        } elseif ($upcharge->priceType == "percent") {
                            $upchargeAmt = number_format((($upcharge->price / 100) * $basePrice),2, '.', '');
                            $unitPrice = $unitPrice + $upchargeAmt;
                        }
                    } else {
                        $upchargeAmt = 0;
                    }

                    $notes = $notes . $upcharge->name . ' @ ' . format_money_symbol($order->business_id, '%.2n', $upchargeAmt) . "\n";
                    // get the upcharge costs
                    $sql = "SELECT cost FROM upchargeSupplier WHERE upcharge_id = {$upcharge->upcharge_id} and supplier_id = {$supplier_id};";
                    $row = $this->db->query($sql)->row();
                    if (isset($row->cost)) {
                        $upchargeCost = $upchargeCost + $row->cost;
                    }
                }
            }

            if ($preferences) {
                foreach ($preferences as $name => $preference) {

                    //if this customer doesn't pay for upcharges make the price 0;
                    if ($customer->noUpcharge == 0) {
                        $preferenceAmt = $preference->price;
                    } else {
                        $preferenceAmt = 0;
                    }

                    $unitPrice = $unitPrice + $preferenceAmt;

                    $value = $preference->sortOrder == 1 ? $preference->displayName : "<b>{$preference->displayName}</b>";

                    $line = "{$name}: {$value}";
                    if ($preference->price) {
                        $line .= ' @ ' . format_money_symbol($order->business_id, '%.2n', $preferenceAmt);
                    }

                    $notes = $notes . $line . "\n";
                }
            }

            $cost = (!isset($cost)) ? 0 : $cost;
            // add the product_process cost and the upcharge cost
            $cost = $cost + $upchargeCost;
            $item_id = ($item->itemID) ? $item->itemID : 0;


            $this->clear();



            //The following query adds the item to the order
            if ($item_id > 0) {
                $this->load->model("barcode_model");
                $this->barcode_model->options = array("item_id" => $item_id);
                $barcodes = $this->barcode_model->get();

                if (empty($barcodes) && !$allow_no_barcode) {
                    throw new Exception("Item must have a barcode");
                }

            }
            if (is_null($unitPrice)) {
                $unitPrice = 0;
            }

            $this->order_id           = $order->orderID;
            $this->qty                = OrderItem_Model::getRoundedQty($order->business_id, $qty);
            $this->product_process_id = $product_process_id;
            $this->supplier_id        = $supplier_id;
            $this->unitPrice          = str_replace( ',' , '.', $unitPrice ); //swap commas for periods (international)
            $this->item_id            = $item_id;
            $this->notes              = trim($notes);
            $this->cost               = str_replace( ',' , '.', $cost );  //swap commas for periods (international)
            $insert_id                = $this->insert();

            //add to scanned table
            //$sql = "INSERT INTO scannedOrderItem (orderItem_id, dateScanned) VALUES ({$insert_id}, NOW());";
            //$this->db->query($sql);

            if ($item_id > 0) {
                //get barcode for logging
                $this->load->model('barcode_model');
                $this->barcode_model->item_id = $item->itemID;
                $barcode = $this->barcode_model->get();
                
                if ($barcode) {
                    $barcode_label = "Barcode: {$barcode[0]->barcode}";
                } else {
                    $barcode_label = "Barcode: not assigned, Item #$item_id";
                }
                
                $note = "Added Item: '{$product->name}', $barcode_label";
            } else {
                $note = "Added Product: '{$product->name}'";
            }

            // log the item in the orderStatus table
            $this->logger->set('orderStatus', array(
                'order_id' => $order->orderID,
                'locker_id' => $locker->lockerID,
                'employee_id' => $employee_id,
                'orderStatusOption_id' => $order->orderStatusOption_id,
                'note' => $note
            ));

            return array('status' => 'success', 'orderItemID' => $insert_id);
        }
    }

    /**
     * Gets all items for a business in a date range
     *
     * @param int $business_id
     * @param strign $startDate
     * @param strign $endDate
     * @return array
     */
    public function todays_items($business_id, $startDate = null, $endDate = null, $convert = true)
    {
        if (empty($startDate)) {
            $startDate = date("Y-m-d");
        }

        if ($convert) {
            list($startTime, $endTime) = get_date_range($startDate, $endDate);
        } else {
            $startTime = $startDate;
            $endTime = $endDate;
        }

        $allItems = "select order_id, orderItem.item_id, updated,  (select count(upcharge_id) as upcharges from itemUpcharge where orderItem.item_id = itemUpcharge.item_id) as upcharges, b.barcode
                from orderItem
                join orders on orderID = order_id
                LEFT JOIN barcode b ON b.item_id = orderItem.item_id
                where updated >= ? and updated <= ?
                and orderItem.item_id <> 0
                and orders.business_id = ?
                order by orderItem.updated;";

        $query = $this->db->query($allItems, array($startTime, $endTime, $business_id));
        $items = $query->result();

        return $items;
    }

    /**
     * Gets orders and items for a business in a date range
     *
     * @param int $business_id
     * @param strign $startDate
     * @param strign $endDate
     * @return array
     */
    public function get_orders_and_items_between_dates($business_id, $start = null, $end = null)
    {
        $sql = "SELECT * FROM (
        SELECT e.firstName, e.lastName, date, orderStatus.order_id, item_id, item.updated, product.name, c.customerID, c.firstName as custFirst, c.lastName as custLast
        FROM `orderStatus` FORCE INDEX (date)
        JOIN employee e ON e.`employeeID` = `orderStatus`.`employee_id`
        JOIN `business_employee` ON `business_employee`.`employee_id` = e.`employeeID`
        LEFT JOIN orderItem on orderItem.order_id = orderStatus.order_id
        join orders on orderID = orderStatus.order_id
        left JOIN customer c on c.customerID = orders.customer_id
        JOIN item on itemID = item_id
        join product_process on product_processID = orderItem.product_process_id
        join product on productID = product_id
        WHERE `orderStatus`.`orderStatusOption_id` = '3'
        AND `orderStatus`.`date` >= ?
        AND `orderStatus`.`date` <= ?
        AND `orders`.`business_id` = ?
        ORDER BY order_id, orderStatusID ASC
        )  AS t
        GROUP BY order_id, item_id
        ORDER BY firstName, `date`, order_id;
        ";

        $this->loadReportingDatabase();
        $query = $this->reports_db->query($sql, array($start, $end, $business_id));
        return $query->result();
    }

    /**
     * Gets new items from items array until date
     *
     * @param strign $startDate
     * @param strign $endDate
     * @return array
     */
    public function get_new_items_from_items_array($startDate = null, $order_items = array())
    {
        if (empty($order_items)) {
            return $order_items;
        }

        $this->loadReportingDatabase();

        foreach ($order_items as $k => $v) {
            $order_items[$k] = $this->reports_db->escape($v);
        }
        $items = implode(",", $order_items);

        $sql = "SELECT item_id, order_id, count(orderItemID) AS count FROM orderItem WHERE updated < ? AND item_id IN ($items)
        GROUP BY item_id
        HAVING count = 1";
        $query = $this->reports_db->query($sql, array($startDate));
        $rows = $query->result();
        $newItems = array();
        foreach ($rows as $r) {
            $newItems[] = $r->item_id;
        }
        return $newItems;
    }

    
    /**
     * --------------------------------
     * BEGIN
     * --------------------------------
        * check if retake picture is needed
        *
        * @param int $item_id
        * @param int $business_id
        * @return boolean
     * --------------------------------
     * BEGIN
     * --------------------------------*
    */
    public function retake_picture($item_id, $business_id)
    {
        $show_message = false;
        $retake_picture_setting = get_business_meta($this->business_id, 'retake_picture_after_x_times_submitted_for_cleaning', false);

        if ($retake_picture_setting) {
            $sql = "
                select count(*) seen_since_last_picture from orderItem
                where item_id = ? 
                  and updated > (select max(taken) from picture where business_id = ? and item_id = ?)
            ";

            $query = $this->db->query($sql, array($item_id, $business_id, $item_id));
            $query_results = $query->result();
            $seen_since_last_picture = empty($query_results[0]->seen_since_last_picture) ? 0 : $query_results[0]->seen_since_last_picture;

            if ($seen_since_last_picture >= $retake_picture_setting) {
                $show_message = true;
            } else {
                $show_message = true;
            }
        }
        return $show_message;
    }

    /**
     * Returns rounded orderItem qty according to business settings
     *
     * @param int $business_id
     * @param float $qty
     * @param string $unit
     * @return float
     */
    public static function getRoundedQty($business_id, $qty, $unit = '')
    {
        $wf_rounding_precision = 0;

        if ($unit == "Lbs" || $unit == "Kgs") {
            $wf_rounding_precision = get_business_meta($business_id, 'wf_rounding_precision', 0);
        }

        $rounded_qty = number_format(round($qty, $wf_rounding_precision), $wf_rounding_precision);

        return $rounded_qty;
    }

    /*
     * Gets orders wich has >= maxDiff between two items
     * @param integer $startDate
     * @param string $startDate
     * @param string $endDate
     * @param string $maxDiff diff in minutes
     * @return resultset
     */
    public function get_orders_with_maxdiff($business_id, $startDate = null, $endDate = null, $maxDiff = 5)
    {
        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must to be numeric.");
        }

        $this->loadReportingDatabase();

        $this->reports_db = $this->load->database("reporting", TRUE);
        if (empty($startDate)) {
            $startDate = date("Y-m-d");
        }

        if (!empty($convert)) {
            list($startTime, $endTime) = get_date_range($startDate, $endDate);
        } else {
            $startTime = $startDate;
            $endTime = $endDate;
        }

        $allItems = "SELECT 
                        order_id, MAX(if( @lastOrderId = orderItem.order_id, 
                        TIMESTAMPDIFF(MINUTE,@lastUpdated, orderItem.updated), 0)) as diff,
                        DATE_FORMAT(orders.dateCreated,'%Y-%m-%d') as dateCreated,
                        @lastOrderId := orderItem.order_id,
                        @lastUpdated := orderItem.updated as lastUpdated
                    FROM orderItem 
                        JOIN orders 
                          ON orderId = order_id
                        WHERE updated > ? 
                          AND updated < ?
                          AND business_id = ?
                        GROUP BY order_id
                        HAVING diff > ?
                        ORDER BY orderItem.order_id, orderItemID";

        $query = $this->reports_db->query($allItems, array($startTime, $endTime, $business_id, $maxDiff));

        $items = $query->result();
        return array("items"=>$items, "count"=>count($items));

    }
}
