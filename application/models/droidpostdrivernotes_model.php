<?php
use App\Libraries\DroplockerObjects\Location;
class DroidPostDriverNotes_Model extends My_Model
{
    protected $tableName = 'droid_postDriverNotes';
    protected $primary_key = 'ID';

    /**
     * inserts a postPickup from the droid to the database
     *
     * Required Options Values
     * ----------------------
     * transaction_id
     * bagNumber
     * location_id
     * lockerName
     * employee_id
     * business_id
     *
     * @param array $options
     * @throws Exception for missing required option values
     * @return int insert_id
     */
    public function insert($options = array())
    {
        $required = array('transaction_id',
                          'employee_id',
                          'business_id',
                          'notes');

        if (!required($required, $options)) {
            try {
                throw new Exception ("Missing required option values. <br>Required: <br>". print_r($required, 1)."<br>Sent: <br>".print_r($options, 1));
            } catch (Exception $e) {
                send_exception_report($e);
            }

            return false;
        }

        // check to see if the transaction exists... if it do, then just return that id
        if (!$id = $this->get($options)) {
            $this->db->insert('droid_postDriverNotes', $options);
            $id = $this->db->insert_id();
        }

        return $id;
    }


    /**
     *
     * Updates a row in the  table
     *
     * @param array $options
     * @param array $where
     * @throws Exception for missing where statement
     * @return int affected_rows
     */
    public function update($options = array(), $where = array())
    {
        if (sizeof($where)<1) {
            throw new Exception("Missing where statement");
        }

        $this->db->update('droid_postDriverNotes', $options, $where);

        return $this->db->affected_rows();
    }

    /**
     * gets data from the table
     *
     * $options values
     * ----------------
     * transaction_id
     * status
     *
     * @param array $options
     * @return array of objects
     */
    public function get($options = array())
    {
        if (isset($options['transaction_id'])) {$this->db->where('transaction_id',$options['transaction_id']);}
        if (isset($options['noteType'])) {$this->db->where('noteType',$options['noteType']);}
        if (isset($options['status'])) {$this->db->where('status',$options['status']);}
        if (isset($options['version'])) {$this->db->where('version',$options['version']);}

        $query = $this->db->get('droid_postDriverNotes');

        return $query->result();
    }

    /**
     *
     * @return boolean
     */
    public function processAllClaimNotes()
    {
        $notes = $this->get(array('status'=>'pending', 'noteType'=>'claimNote'));

        if (!$notes) {
            return false;
        }

        $base_url = get_admin_url();

        foreach ($notes as $note) {

            $body = (DEV) ? '(DEV MESSAGE, PLEASE IGNORE) ' : '';
            $body .= "Driver: {$note->employeeUsername} had a problem with claim: <a href='$base_url/admin/orders/claimed'>{$note->claim_id}</a> on ".convert_from_gmt_aprax("now", STANDARD_DATE_FORMAT, $note->business_id)."<br><br>";
            $body .= "". $note->notes."<br>";
            $body .= "-----------------------------<br><br>";

            $body .= formatArray($note);

            $body .= "Locker: ".$note->lockerName;
            $email_id = queueEmail(SYSTEMEMAIL,"DO NOT REPLY", get_business_meta($note->business_id, 'customerSupport'),'Driver had a problem with claim', $body, array(), null, $note->business_id);
            if (!$email_id) {
                $this->update(array('status'=>'error', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Failure Sending Note'), array('transaction_id'=>$note->transaction_id));
            } else {
                $this->update(array('status'=>'complete', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Message Sent'), array('transaction_id'=>$note->transaction_id));
            }

            //update the claim notes.
            $newNote = '[DRIVER NOTE: '.$note->notes.'] ';
            $updateNote = "update claim set notes = concat(".$this->db->escape($newNote)." , notes) where claimID = $note->claim_id";
            $this->db->query($updateNote);
        }
    }


    public function processAllDriverNotes()
    {
        $options['status'] = 'pending';
        $options['noteType'] = 'driverNote';

        $notes = $this->get($options);

        if (!$notes) {
            return true;
        }

        foreach ($notes as $note) {
            $sql = "SELECT address, note, driverNotesID
                    FROM driverNotes
                    LEFT JOIN location ON locationID = location_id
                    WHERE driverNotesID = $note->note_id";

            $line = $this->db->query($sql)->row();

            $subject = "Driver has sent a message regarding a driver note";

            $body = (DEV) ? '(DEV MESSAGE, PLEASE IGNORE) ' : '';
            $body .= "DriverNote: {$line->note} on ".convert_from_gmt_aprax("now", STANDARD_DATE_FORMAT, $note->business_id)."<br><br>";
            $body .= "DriverUsername: {$note->employeeUsername}<br>";
            $body .= "Driver Message: {$note->notes}<br>";
            $body .= "Location: ". $line->address."<br>";
            $body .= "DriverNote ID: $note->note_id";

            //echo $body;
            $emaiLogID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($note->business_id, 'customerSupport'), $subject, $body, array(), null, $note->business_id);
            if (!$emaiLogID) {
                $this->update(array('status'=>'error', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Failure Sending Note'), array('transaction_id'=>$note->transaction_id));
            } else {
                $this->update(array('status'=>'complete', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Message Sent'), array('transaction_id'=>$note->transaction_id));
            }
        }
    }


    public function processAllDeliveryNotes()
    {
        $notes = $this->get(array('status'=>'pending', 'noteType'=>'deliveryNote'));

        if (!$notes) {
            return;
        }

        foreach ($notes as $note) {

            $body = (DEV) ? '(DEV MESSAGE, PLEASE IGNORE) ' : '';
            $body .= "Driver: {$note->username} sent a message about order: {$note->order_id} on ".convert_from_gmt_aprax("now", STANDARD_DATE_FORMAT, $note->business_id)."<br><br>";
            $body .= "". $note->notes."<br>";
            $body .= "-----------------------------<br><br>";

            foreach ($note as $k => $v) {
                $body .= $k.": ".$v."<br>";
            }

            $email = get_business_meta($note->business_id, 'customerSupport');
            $emaiLogID = queueEmail(SYSTEMEMAIL,SYSTEMFROMNAME, $email, 'Driver message for order['.$note->order_id.']', $body, array(), null, $note->business_id);
            if (!$emaiLogID) {
                $this->update(array('status'=>'error', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Failure Sending Note'), array('transaction_id'=>$note->transaction_id));
            } else {
                $this->update(array('status'=>'complete', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Message Sent'), array('transaction_id'=>$note->transaction_id));
            }
        }
    }


    public function processAllIssues()
    {
        $options['status'] = 'pending';
        $options['noteType'] = 'issue';
        $notes = $this->get($options);

        if (!$notes) {
            return true;
        }

        foreach ($notes as $note) {

            if ($note->location_id) {
                try {
                    $location = new Location($note->location_id);
                } catch (Exception $e) {
                  //Failed to find location... not doing anything.
                }
            }

            // send email
            $body = "Driver Issue<br>";
            $body .= "<br>Message: ".$note->notes."<br>";
            $body .= "<br>BagNumber: ".$note->bagNumber;
            $body .= "<br>Location: ".$location->address;
            $body .= "<br>locker: ".$note->lockerName;
            $body .= "<br>Employee: ".$note->employeeUsername;

            //echo $body;
            $emailLogID = queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($note->business_id, 'customerSupport'), "Driver Issue", $body, array(), null, $note->business_id);
            if (!$emailLogID) {
                $this->update(array('status'=>'error', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Failure Sending Note'), array('transaction_id'=>$note->transaction_id));
            } else {
                $this->update(array('status'=>'complete', 'methodCalled'=>__METHOD__, 'statusNotes'=>'Message Sent'), array('transaction_id'=>$note->transaction_id));
            }

        }
    }

}
