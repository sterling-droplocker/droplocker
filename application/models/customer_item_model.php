<?php

class Customer_Item_Model extends My_Model
{
    protected $tableName = 'customer_item';
    protected $primary_key = 'customer_itemID';
    private $reporting_database = null;

    public function get_with_customer_information($where_conditions = array())
    {
        $this->db->join("customer", "customer_item.customer_id=customer.customerID");

        return $this->db->get_where($this->tableName, $where_conditions)->result();
    }
    public function insert_ignore($customer_id, $item_id)
    {
        $sql = "INSERT IGNORE INTO customer_item (customer_id, item_id) VALUES ('".$customer_id."', '".$item_id."')";
        $this->db->query($sql);
    }

    public function get_customer_item_history($business_id, $barcode = false, $limit=false, $page_from=false, $start_date, $end_date)
    {
        $this->reporting_database = $this->load->database("reporting", TRUE);
        if (DEV) {
            $this->reporting_database = $this->db;
        }

        if (!is_numeric($business_id)) {
            throw new Exception("'business_id' must to be numeric.");
        }
        $this->reporting_database->select("SQL_CALC_FOUND_ROWS cih.*, c.firstName as from_customer_firstName, c.lastName as from_customer_lastName, c1.firstName as to_customer_firstName, c1.lastName as to_customer_lastName, e.firstName as employee_firstName, e.lastName as employee_lastName", false);
        $this->reporting_database->join("orders o", "cih.orderID=o.orderID");
        $this->reporting_database->join("customer c", "c.customerID=cih.from_customer_id");
        $this->reporting_database->join("customer c1", "c1.customerID=cih.to_customer_id");
        $this->reporting_database->join("employee e", "e.employeeID=cih.employee_id");
        $this->reporting_database->where("o.business_id", $business_id);
        $this->reporting_database->where("cih.date >", $start_date." 00:00:00");
        $this->reporting_database->where("cih.date <=", $end_date." 23:59:59");
        $this->reporting_database->order_by("cih.barcode, cih.date ASC");
        if ($limit !== false && $page_from !== false) {
            $this->reporting_database->limit($limit, $page_from);
        }
        $query = $this->reporting_database->get("customerItemHistory cih");

        $result = $query->result();

        $sQuery = "SELECT FOUND_ROWS() AS found_rows";

        $aResultFilterTotal = $this->reporting_database->query($sQuery)->row();
        $count = $aResultFilterTotal->found_rows;

        return array("items"=>$result, "count"=>(int)$count);
    }

    public function log_customer_item_history($params = array())
    {
        $sql = "INSERT INTO customerItemHistory(from_customer_id, to_customer_id, employee_id, barcode, orderID, `date`) VALUES(?, ?, ?, ?, ?, ?);";
        $this->db->query($sql, $params);
    }
}
