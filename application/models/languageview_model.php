<?php

class LanguageView_Model extends MY_Model
{
    protected $tableName = 'languageView';
    protected $primary_key = 'languageviewID';

    /**
     * Returns a single language view with the specified name
     * @param string $name
     * @return stdClass
     */
    public function findByName($name) {
        return $this->db->get_where($this->tableName, array("name" => $name))->row();
    }
    /**
     * Retrieves all the language views in the system and returns them in a data form compatiable with the Codeigniter dropdown menu helper.
     * @return array
     */
    public function get_all_as_codeigniter_dropdown()
    {
        $languageViews = $this->get_all();
        $languageView_dropdown = array();
        foreach ($languageViews as $languageView) {
            $languageView_dropdown[$languageView->languageViewID] = $languageView->name;
        }

        return $languageView_dropdown;
    }
}
