<?php

class Employee_Model extends MY_Model
{
    protected $tableName = 'employee';
    protected $primary_key = 'employeeID';

    /**
    * Logs in an employee
    *
    * An employee can login with either email or username
    *
    * @param string loginString - either email or username
    * @param string password - plain password
    * @return object
    */
    public function login($loginString, $password)
    {
        $login = $loginString;
        $password = md5($password);

        $sql = "SELECT employeeID FROM employee WHERE password = ?
                AND (username = ? OR email = ?) AND empStatus = 1;";

        $query = $this->db->query($sql, array($password, $login, $login));
        $employee = $query->row();

        return $employee;
    }

    /**
     * gets data from the business table
     */
    public function get($options = array())
    {
        if (isset($options['employeeID'])) {
            $this->db->where('employeeID', $options['employeeID']);
        }
        if (isset($options['active'])) {$this->db->where('empStatus', $options['active']);}
        if (isset($options['phoneCell'])) {$this->db->where('phoneCell', $options['phoneCell']);}
        if (isset($options['email'])) {$this->db->where('email', $options['email']);}
        if (isset($options['password'])) {$this->db->where('password', $options['password']);}
        if (isset($options['select'])) {$this->db->select($options['select']);}
        if (isset($options['business_id'])) {$this->db->where('business_employee.business_id', $options['business_id']);}
        if (isset($options['employee_id'])) {$this->db->where('business_employee.employee_id', $options['employee_id']);}

        if (!isset($options['exclude_business_employee'])) {
            //all employees will need to join a business
            $this->db->join('business_employee', 'business_employee.employee_id=employee.employeeID');
            
            if (isset($options['include_role_data'])) {
                //all employees will need to join a business
                
                if(!isset($options['select'])) {
                    $this->db->select('employee.*');
                    $this->db->select('business_employee.*');
                    $this->db->select('aclroles.aclID, aclroles.name roleName, aclroles.roleorder, aclroles.business_id aclroles_business_id, aclroles.custom aclroles_custom');
                }
                $this->db->join('aclroles', 'business_employee.aclRole_id=aclroles.aclID');
            }
        }


        if (isset($options['order_by'])) {$this->db->order_by($options['order_by']);}

        $query = $this->db->get('employee');
        $query_result = $query->result();

        return $query_result;

    }
    
    public function _get_one($options = array()) {
        $list = $this->get($options);
        
        return $list[0];
    }
    /**
     *Retrieves all the employees sepcified by the business ID in a codeigniter dropdown compatiable format.
     * @param int $business_id
     */
    public function get_all_as_codeigniter_dropdown($business_id)
    {
        $result= array();
        $this->db->select("employeeID, firstName, lastName");
        $this->db->join("business_employee", "employee_id=employeeID");
        $this->db->where("business_id", $business_id);
        $this->db->order_by("firstName");

        $get_all_employees_query = $this->db->get("employee");
        $employees = $get_all_employees_query->result();
        // add entry for system created tickets
        $result[0] = "System created";

        foreach ($employees as $employee) {
            $result[$employee->employeeID] = getPersonName($employee);
        }

        return $result;
    }

    public function get_all()
    {
        $options['exclude_business_employee'] = true;
        $options['order_by'] = 'lastName, firstName';

        return $this->get($options);
    }

    public function update($options = array(), $where = array())
    {
            if (sizeof($where)<1) {
                die("Missing Where ". __METHOD__);
            }

            $this->db->update('employee', $options, $where);

            return $this->db->affected_rows();
    }

    public function delete($where = array())
    {
            $this->db->delete('employee', $where);
    }

    /**
     * Activates an employee.
     * @param int $employee_id
     * @throws Exception
     */
    public function activate($employee_id)
    {
        if (is_numeric($employee_id)) {
            $this->db->update("employee", array("empStatus" => 1), array("employeeID" => $employee_id));
        } else {
            throw new Exception("'employee_id' must be an integer.");
        }
    }
    /**
     * Deactivates an employee.
     * @param int $employee_id
     */
    public function deactivate($employee_id)
    {
        if (is_numeric($employee_id)) {
            $this->db->update("employee", array("empStatus" => 0, "endDate" => date("Y-m-d")), array("employeeID" => $employee_id));
        } else {
            throw new Exception("'employee_id' must be an integer.");
        }
    }
    /**
     * Updates the e-mail for a particular employee.
     * @param int $employee_id
     * @param int $new_email
     */
    public function update_email($employee_id, $new_email)
    {
        if (empty($employee_id)) {
            throw new Exception("'employee_id' can not be empty.");
        }
        if (empty($new_email)) {
            throw new Exception("'new_email' can not be empty.");
        }
        $this->db->where("employeeID", $employee_id);
        $this->db->update('employee', array("email" => $new_email));
    }

    /**
     * Retrieves an employee object by the specified employee ID.
     * @param int $employeeID
     * @return stdClass Employee record set
     * @throws Exception
     */
    public function get_by_id($employeeID)
    {
        if (!is_numeric($employeeID)) {
            throw new Exception("'employeeID' must be numeric.");
        }

        return  $this->db->get_where($this->tableName, array("employeeID" => $employeeID))->row();

    }
    /**
     * get_employee_businesses method returns all the businesses and roles for that business
     *
     * Options Values
     * --------------------------
     * employee_id
     *
     *
     * @param type $options
     * @return array of objects
     */
    public function get_employee_businesses($options = array())
    {
        if (isset($options['employee_id'])) {$this->db->where('employee_id', $options['employee_id']);}
        if (isset($options['default_only'])) {$this->db->where('default',1);}
        //if select statement
        if (isset($options['select'])) {
            $this->db->select($options['select']);
        } else {
            $this->db->select("companyName, businessID, aclRole_id, default, name, business_employee.id");
        }

        $this->db->join('aclroles','aclroles.aclID=business_employee.aclRole_id');
        $this->db->join('business','business.businessID=business_employee.business_id');

        $query = $this->db->get('business_employee');

        return $query->result();
    }

    /**
     * get_employee_role returns the row for the role of the user within the
     * current company
     *
     * Employees can have multiple roles with different businesses
     *
     * @param int $employee_id
     * @param int $business_id
     * @param string $select
     * @return array of objects
     */
    public function get_employee_role($employee_id, $business_id='', $select='')
    {
        $this->db->where('employee_id', $employee_id);

        if($business_id)
        $this->db->where('business_employee.business_id', $business_id);

        if ($select) {$this->db->select($select);}

        $this->db->join('business_employee', 'business_employee.aclRole_id=aclroles.aclID');
        $this->db->join('business', 'businessID=business_employee.business_id');

        $this->db->order_by('companyName');

        $query = $this->db->get('aclroles');

        return $query->result();
    }

    /**
     * The buser_id is set in the business_employee table becuase an employee can work for multiple business.
     * This method returns the actual employee id from the employee table
     */
    public function get_employee_id($buser_id)
    {
        if (!is_numeric($buser_id)) {
            throw new Exception("'buser_id' must be numeric.");
        }
        $sql = "SELECT employeeID FROM employee INNER JOIN business_employee b ON b.employee_id = employeeID WHERE ID = ?";
        $query = $this->db->query($sql, array($buser_id));
        $employee = $query->result();

        return $employee[0]->employeeID;
    }

    /**
     * Gets an employee record using the buser_id
     * @param int $buser_id
     * @return object
     */
    public function get_employee_by_buser_id($buser_id)
    {
        $sql = "SELECT employee.* FROM employee
                JOIN business_employee ON employee_id = employeeID
                WHERE ID = ?";
        $query = $this->db->query($sql, array($buser_id));
        return $query->row();
    }

    /**
     * This method returns all the roles for this business
     */
    public function get_roles()
    {
        $sqlGetRoles = "select *
                        from aclroles
                        where business_id = $this->business_id
                        order by name";
        $query = $this->db->query($sqlGetRoles);
                if ($this->db->_error_message()) {
                    throw new Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
                }

        return $query->result();
    }

    /**
     * This method returns all the employees with the specified roleID or array of roleIDs
     *
     */
    public function get_emp_by_role($aclID)
    {
        if (is_array($aclID)) {
            $aclID = implode(',', $aclID);
        }

        $sqlGetEmps = "SELECT * FROM business_employee
                        join employee on employeeID = employee_id
                        where aclRole_id IN (?)
                        and business_id = ?
                        order by firstName";
        $query = $this->db->query($sqlGetEmps, array($aclID, $this->business_id));

        return $query->result();
    }

     /**
     * This method returns all the employees with the specified roleName
     */
    public function get_emp_by_roleName($aclName)
    {
        $sqlGetEmps = "SELECT * FROM business_employee
                        join employee on employeeID = employee_id
                        join aclroles on aclroles.aclID = business_employee.aclRole_id
                        where aclroles.name = '$aclName'
                        and business_employee.business_id = $this->business_id
                        order by firstName";
        $query = $this->db->query($sqlGetEmps);

        return $query->result();
    }
}
