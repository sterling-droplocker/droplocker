<?php

class Cash_Register_Record_Type_Model extends My_Model
{
    protected $tableName = 'cash_register_record_type';
    protected $primary_key = 'cash_register_record_typeID';
}
