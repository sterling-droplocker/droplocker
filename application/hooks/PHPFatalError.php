<?php
class PHPFatalError
{
    public function setHandler()
    {
        register_shutdown_function('handleShutdown');
    }
}

function handleShutdown()
{
    if (php_sapi_name() === 'cli') {
        return true;
    }

    $error = error_get_last();
    $shutdown_errors = array(E_PARSE, E_ERROR, E_USER_ERROR, E_COMPILE_ERROR);

    if ($error && in_array($error['type'], $shutdown_errors)) {
        if (function_exists('send_error_report')) {
            send_error_report($error, "Fatal Error Report");
        }

        $CI = get_instance();
        switch ($CI->uri->segment(1)) {
            case 'admin':   $url = '/admin';   break;
            case 'account': $url = '/account'; break;
            default:        $url = '/'; break;
        }
        $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $url;
        $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
        $hide_errors = ENVIRONMENT == "production"  && $extension != "qa";

        $error['referer'] = $_SERVER['HTTP_REFERER'];
        $error['url']  = $_SERVER['REQUEST_URI'];
        $error['ip']  = $_SERVER['REMOTE_ADDR'];

        if ($hide_errors) {
            set_flash_message("error", "Sorry, an internal error occurred attempting to process your request. Please contact Droplocker Support at development@droplocker.com");
        } else {
            die("<pre><code>" . print_r($error, true) . "</code></pre>");
        }
        header("Location: ".$url);
        die("<script>window.location.href = '$url';</script>");
    }
}
