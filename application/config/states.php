<?php

/**
 * This file stores the list of countries and provinces/states by country
 */


/**
 * Countries
 */
$countries['usa']          = 'USA';
$countries['australia']    = 'Australia';
$countries['canada']       = 'Canada';
$countries['chile']        = 'Chile';
$countries['france']       = 'France';
$countries['germany']      = 'Germany';
$countries['japan']        = 'Japan';
$countries['mexico']       = 'Mexico';
$countries['new zealand']  = 'New Zealand';
$countries['netherlands']  = 'Netherlands';
$countries['russia']       = 'Russia';
$countries['spain']        = 'Spain';
$countries['uae']          = 'United Arab Emirates';
$countries['uk']           = 'UK';
$countries['south africa'] = 'South Africa';
$countries['sweden']       = 'Sweden';



/**
 * USA states
 */
$states['usa']["AL"] = "Alabama";
$states['usa']["AK"] = "Alaska";
$states['usa']["AZ"] = "Arizona";
$states['usa']["AR"] = "Arkansas";
$states['usa']["CA"] = "California";
$states['usa']["CO"] = "Colorado";
$states['usa']["CT"] = "Connecticut";
$states['usa']["DE"] = "Delaware";
$states['usa']["DC"] = "District of Columbia";
$states['usa']["FL"] = "Florida";
$states['usa']["GA"] = "Georgia";
$states['usa']["HI"] = "Hawaii";
$states['usa']["ID"] = "Idaho";
$states['usa']["IL"] = "Illinois";
$states['usa']["IN"] = "Indiana";
$states['usa']["IA"] = "Iowa";
$states['usa']["KS"] = "Kansas";
$states['usa']["KY"] = "Kentucky";
$states['usa']["LA"] = "Lousiana";
$states['usa']["ME"] = "Maine";
$states['usa']["MD"] = "Maryland";
$states['usa']["MA"] = "Massachusetts";
$states['usa']["MI"] = "Michigan";
$states['usa']["MN"] = "Minnesota";
$states['usa']["MS"] = "Mississippi";
$states['usa']["MO"] = "Missouri";
$states['usa']["MT"] = "Montana";
$states['usa']["NE"] = "Nebraska";
$states['usa']["NV"] = "Nevada";
$states['usa']["NV"] = "Nevada";
$states['usa']["NH"] = "New Hampshire";
$states['usa']["NJ"] = "New Jersey";
$states['usa']["NM"] = "New Mexico";
$states['usa']["NY"] = "New York";
$states['usa']["NC"] = "North Carolina";
$states['usa']["ND"] = "North Dakota";
$states['usa']["OH"] = "Ohio";
$states['usa']["OK"] = "Oklahoma";
$states['usa']["OR"] = "Oregon";
$states['usa']["PA"] = "Pennsylvania";
$states['usa']["RI"] = "Rhode Island";
$states['usa']["SC"] = "South Carolina";
$states['usa']["SD"] = "South Dakota";
$states['usa']["TN"] = "Tennessee";
$states['usa']["TX"] = "Texas";
$states['usa']["UT"] = "Utah";
$states['usa']["VT"] = "Vermont";
$states['usa']["VA"] = "Virginia";
$states['usa']["WA"] = "Washington";
$states['usa']["WV"] = "West Virginia";
$states['usa']["WI"] = "Wisconsin";
$states['usa']["WY"] = "Wyoming";


/**
 * Australian states
 */
$states['australia']['AU-VIC'] = 'Victoria';


/**
 * Canadian provinces
 */
$states['canada']['AB'] = 'Alberta';
$states['canada']['BC'] = 'British Columbia';
$states['canada']['MB'] = 'Manitoba';
$states['canada']['NB'] = 'New Brunswick';
$states['canada']['NL'] = 'Newfoundland and Labrador';
$states['canada']['NS'] = 'Nova Scotia';
$states['canada']['ON'] = 'Ontario';
$states['canada']['PE'] = 'Prince Edward Island';
$states['canada']['QC'] = 'Quebec';
$states['canada']['SK'] = 'Saskatchewan';
$states['canada']['NT'] = 'Northwest Territories';
$states['canada']['NU'] = 'Nunavut';
$states['canada']['YT'] = 'Yukon';


/**
 * German states
 */
$states['germany']['DE-BW']='Baden-Württemberg';
$states['germany']['DE-BY']='Bayern';
$states['germany']['DE-BE']='Berlin';
$states['germany']['DE-BB']='Brandenburg';
$states['germany']['DE-HB']='Bremen';
$states['germany']['DE-HH']='Hamburg';
$states['germany']['DE-HE']='Hessen';
$states['germany']['DE-MV']='Mecklenburg-Vorpommern';
$states['germany']['DE-NI']='Niedersachsen';
$states['germany']['DE-NW']='Nordrhein-Westfalen';
$states['germany']['DE-RP']='Rheinland-Pfalz';
$states['germany']['DE-SL']='Saarland';
$states['germany']['DE-SN']='Sachsen';
$states['germany']['DE-ST']='Sachsen-Anhalt';
$states['germany']['DE-SH']='Schleswig-Holstein';
$states['germany']['DE-TH']='Thüringen';

/**
 * Mexican states
 */
$states['mexico']['MX-DIF'] = 'Distrito Federal';
$states['mexico']['MX-AGU'] = 'Aguascalientes';
$states['mexico']['MX-BCN'] = 'Baja California';
$states['mexico']['MX-BCS'] = 'Baja California Sur';
$states['mexico']['MX-CAM'] = 'Campeche';
$states['mexico']['MX-COA'] = 'Coahuila';
$states['mexico']['MX-COL'] = 'Colima';
$states['mexico']['MX-CHP'] = 'Chiapas';
$states['mexico']['MX-CHH'] = 'Chihuahua';
$states['mexico']['MX-DUR'] = 'Durango';
$states['mexico']['MX-GUA'] = 'Guanajuato';
$states['mexico']['MX-GRO'] = 'Guerrero';
$states['mexico']['MX-HID'] = 'Hidalgo';
$states['mexico']['MX-JAL'] = 'Jalisco';
$states['mexico']['MX-MEX'] = 'México';
$states['mexico']['MX-MIC'] = 'Michoacán';
$states['mexico']['MX-MOR'] = 'Morelos';
$states['mexico']['MX-NAY'] = 'Nayarit';
$states['mexico']['MX-NLE'] = 'Nuevo León';
$states['mexico']['MX-OAX'] = 'Oaxaca';
$states['mexico']['MX-PUE'] = 'Puebla';
$states['mexico']['MX-QUE'] = 'Querétaro';
$states['mexico']['MX-ROO'] = 'Quintana Roo';
$states['mexico']['MX-SLP'] = 'San Luis Potosí';
$states['mexico']['MX-SIN'] = 'Sinaloa';
$states['mexico']['MX-SON'] = 'Sonora';
$states['mexico']['MX-TAB'] = 'Tabasco';
$states['mexico']['MX-TAM'] = 'Tamaulipas';
$states['mexico']['MX-TLA'] = 'Tlaxcala';
$states['mexico']['MX-VER'] = 'Veracruz';
$states['mexico']['MX-YUC'] = 'Yucatán';
$states['mexico']['MX-ZAC'] = 'Zacatecas';


/**
 * Netherlands's provinces
 */
$states['netherlands']['NL-DR'] = 'Drenthe';
$states['netherlands']['NL-FL'] = 'Flevoland';
$states['netherlands']['NL-FR'] = 'Fryslân';
$states['netherlands']['NL-GE'] = 'Gelderland';
$states['netherlands']['NL-GR'] = 'Groningen';
$states['netherlands']['NL-LI'] = 'Limburg';
$states['netherlands']['NL-NB'] = 'Noord Brabant';
$states['netherlands']['NL-NH'] = 'Noord Holland';
$states['netherlands']['NL-OV'] = 'Overijssel';
$states['netherlands']['NL-UT'] = 'Utrecht';
$states['netherlands']['NL-ZE'] = 'Zeeland';
$states['netherlands']['NL-ZH'] = 'Zuid Holland';

/**+
 * South Africa provinces
 */
$states['south africa']['ZA-EC'] = 'The Eastern Cape';
$states['south africa']['ZA-FS'] = 'The Free State';
$states['south africa']['ZA-GT'] = 'Gauteng';
$states['south africa']['ZA-NL'] = 'KwaZulu-Natal';
$states['south africa']['ZA-LP'] = 'Limpopo';
$states['south africa']['ZA-MP'] = 'Mpumalanga';
$states['south africa']['ZA-NC'] = 'The Northern Cape';
$states['south africa']['ZA-NW'] = 'North West';
$states['south africa']['ZA-WC'] = 'The Western Cape';

/**
 * Sweden provinces
 */
$states['sweden']['SE-K']['Blekinge län'];
$states['sweden']['SE-W']['Dalarnas län'];
$states['sweden']['SE-I']['Gotlands län'];
$states['sweden']['SE-X']['Gävleborgs län'];
$states['sweden']['SE-N']['Hallands län'];
$states['sweden']['SE-Z']['Jämtlands län'];
$states['sweden']['SE-F']['Jönköpings län'];
$states['sweden']['SE-H']['Kalmar län'];
$states['sweden']['SE-G']['Kronobergs län'];
$states['sweden']['SE-BD']['Norrbottens län'];
$states['sweden']['SE-M']['Skåne län'];
$states['sweden']['SE-AB']['Stockholms län'];
$states['sweden']['SE-D']['Södermanlands län'];
$states['sweden']['SE-C']['Uppsala län'];
$states['sweden']['SE-S']['Värmlands län'];
$states['sweden']['SE-AC']['Västerbottens län'];
$states['sweden']['SE-Y']['Västernorrlands län'];
$states['sweden']['SE-U']['Västmanlands län'];
$states['sweden']['SE-O']['Västra Götalands län'];
$states['sweden']['SE-T']['Örebro län'];
$states['sweden']['SE-E']['Östergötlands län'];