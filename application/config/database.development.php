<?php
$active_record = TRUE;
$active_group = 'default';


$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'droplocker_production_dump_01';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = FALSE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$db['reporting']['hostname'] = 'localhost';
$db['reporting']['username'] = 'root';
$db['reporting']['password'] = '';
$db['reporting']['database'] = 'droplocker_production_dump_01';
$db['reporting']['dbdriver'] = 'mysql';
$db['reporting']['dbprefix'] = '';
$db['reporting']['pconnect'] = TRUE;
$db['reporting']['db_debug'] = FALSE;
$db['reporting']['cache_on'] = FALSE;
$db['reporting']['cachedir'] = '';
$db['reporting']['char_set'] = 'utf8';
$db['reporting']['dbcollat'] = 'utf8_general_ci';
$db['reporting']['swap_pre'] = '';
$db['reporting']['autoinit'] = TRUE;
$db['reporting']['stricton'] = FALSE;

// Local test database

$db['test']['hostname'] = 'localhost';
$db['test']['username'] = 'root';
$db['test']['password'] = "";
$db['test']['database'] = "droplocker_production_dump_01";
$db['test']['dbdriver'] = "mysql";
$db['test']['dbprefix'] = "";
$db['test']['pconnect'] = TRUE;
$db['test']['db_debug'] = TRUE;
$db['test']['cache_on'] = FALSE;
$db['test']['cachedir'] = "";
$db['test']['char_set'] = "utf8";
$db['test']['dbcollat'] = "utf8_general_ci";
$db['test']['autoinit'] = TRUE;
$db['test']['stricton'] = FALSE;

/**
echo '<pre>';
print_r($db['default']);
echo '</pre>';

echo 'Trying to connect to database: ' .$db['default']['database'];
$dbh = mysql_connect $db['default']['hostname'], $db['default']['username'],$db['default']['password']) or die('Cannot connect to the database because: ' . mysql_error());
mysql_select_db ($db['default']['database']);
echo '<br />   Connected OK:'  ;
die( 'file: ' .__FILE__ . '--> Line: ' .__LINE__);
 */
