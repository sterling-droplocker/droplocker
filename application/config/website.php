<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

$config['base_path'] = '/tmp/mobile_assets/';
$config['zip_file_name'] = 'mobile_assets.zip';

$config['app_assets'] = array(
    array(
        'path' => 'iOS/xcassets/AppIcon.appiconset',
        'files' => array(
            '20x20.png'        => array('width' =>   20, 'height' =>   20, 'type' => 'icon'),
            '20x20@2x.png'     => array('width' =>   40, 'height' =>   40, 'type' => 'icon'),
            '20x20@2x-1.png'   => array('width' =>   40, 'height' =>   40, 'type' => 'icon'),
            '20x20@3x.png'     => array('width' =>   60, 'height' =>   60, 'type' => 'icon'),
            '29x29.png'        => array('width' =>   29, 'height' =>   29, 'type' => 'icon'),
            '29x29@2x.png'     => array('width' =>   58, 'height' =>   58, 'type' => 'icon'),
            '29x29@2x-1.png'   => array('width' =>   58, 'height' =>   58, 'type' => 'icon'),
            '29x29@3x.png'     => array('width' =>   87, 'height' =>   87, 'type' => 'icon'),
            '40x40.png'        => array('width' =>   40, 'height' =>   40, 'type' => 'icon'),
            '40x40@2x.png'     => array('width' =>   80, 'height' =>   80, 'type' => 'icon'),
            '40x40@2x-1.png'   => array('width' =>   80, 'height' =>   80, 'type' => 'icon'),
            '40x40@3x.png'     => array('width' =>  120, 'height' =>  120, 'type' => 'icon'),
            '50x50.png'        => array('width' =>   50, 'height' =>   50, 'type' => 'icon'),
            '50x50@2x.png'     => array('width' =>  100, 'height' =>  100, 'type' => 'icon'),
            '57x57.png'        => array('width' =>   57, 'height' =>   57, 'type' => 'icon'),
            '57x57@2x.png'     => array('width' =>  114, 'height' =>  114, 'type' => 'icon'),
            '60x60@2x.png'     => array('width' =>  120, 'height' =>  120, 'type' => 'icon'),
            '60x60@3x.png'     => array('width' =>  180, 'height' =>  180, 'type' => 'icon'),
            '72x72.png'        => array('width' =>   72, 'height' =>   72, 'type' => 'icon'),
            '72x72@2x.png'     => array('width' =>  144, 'height' =>  144, 'type' => 'icon'),
            '76x76.png'        => array('width' =>   76, 'height' =>   76, 'type' => 'icon'),
            '76x76@2x.png'     => array('width' =>  152, 'height' =>  152, 'type' => 'icon'),
            '83.5x83.5@2x.png' => array('width' =>  167, 'height' =>  167, 'type' => 'icon'),
            '1024x1024.png'    => array('width' => 1024, 'height' => 1024, 'type' => 'icon'),
        ),
    ),
    array(
        'path' => 'iOS/xcassets/Brand Assets.launchimage',
        'files' => array(
            'iphoneX.png'      => array('width' => 1125, 'height' => 2436, 'type' => 'splash'),
            'retina5.5@3x.png' => array('width' => 1242, 'height' => 2208, 'type' => 'splash'),
            'retina4.7@2x.png' => array('width' => 750,  'height' => 1334, 'type' => 'splash'),
            'retina4@2x.png'   => array('width' => 640,  'height' => 1136, 'type' => 'splash'),
            'retina4@2x-1.png' => array('width' => 640,  'height' => 1136, 'type' => 'splash'),
            'splash@2x.png'    => array('width' => 640,  'height' => 960,  'type' => 'splash'),
            'splash@2x-1.png'  => array('width' => 640,  'height' => 960,  'type' => 'splash'),
            'splash.png'       => array('width' => 320,  'height' => 480,  'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'iOS/Store',
        'files' => array(
            '1024x1024.png'    => array('width' => 1024, 'height' => 1024, 'type' => 'icon'),
        ),
    ),
    array(
        'path' => 'Android/drawable-ldpi',
        'files' => array(
            'app_icon.png'      => array('width' =>  36, 'height' =>  36, 'type' => 'icon'),
            'splash_screen.png' => array('width' => 200, 'height' => 320, 'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'Android/drawable-mdpi',
        'files' => array(
            'app_icon.png'      => array('width' =>  48, 'height' =>  48,  'type' => 'icon'),
            'splash_screen.png' => array('width' => 320, 'height' => 480, 'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'Android/drawable-hdpi',
        'files' => array(
            'app_icon.png'      => array('width' =>  72, 'height' =>  72, 'type' => 'icon'),
            'splash_screen.png' => array('width' => 480, 'height' => 800, 'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'Android/drawable-xhdpi',
        'files' => array(
            'app_icon.png'      => array('width' =>  96, 'height' =>   96, 'type' => 'icon'),
            'splash_screen.png' => array('width' => 720, 'height' => 1280, 'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'Android/drawable-xxhdpi',
        'files' => array(
            'app_icon.png'      => array('width' => 144, 'height' =>  144, 'type' => 'icon'),
            'splash_screen.png' => array('width' => 960, 'height' => 1600, 'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'Android/drawable-xxxhdpi',
        'files' => array(
            'app_icon.png'      => array('width' =>  192, 'height' =>  192, 'type' => 'icon'),
            'splash_screen.png' => array('width' => 1280, 'height' => 1920, 'type' => 'splash'),
        ),
    ),
    array(
        'path' => 'Android/store',
        'files' => array(
            'Feature_Graphic.png' => array('width' => 1024, 'height' => 500, 'type' => 'feature'),
            'icon.png'            => array('width' =>  512, 'height' => 512, 'type' => 'icon'),
        ),
    ),
);

