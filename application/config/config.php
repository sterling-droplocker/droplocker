<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//keys for recapcha when there is not business
$config['recaptcha_keys'] = array(
  'public'  => '6Ld3_DgUAAAAAAvk6x3E3S1Nn31PcvaxMdMKs_wc',
  'private' => '6Ld3_DgUAAAAAIQROZoqiYIBQ3Ys8FhBR93cdFXx'
);

//units are used for additional products during an order
$config['units'] = array(
    'N/A' => "None",
    'Lbs' => "/pound",
    'Kgs' => "/kilogram",
    'each' => "each",
    'order' => "/order",
    'pcs' => "item",
    'qty' => "/qty",
);

$config['productTypes'] = array(
    "preference" => "preference",
    "product" => "product"
);

$config['providers'] = array(
    'Alltel' => 'message.alltel.com',
    'ATT' => 'txt.att.net',
    'Cingular' => 'txt.att.net',
    'MetroPCS' => 'mymetroPCS.com',
    'Nextel' => 'messaging.nextel.com',
    'Sprint' => 'messaging.sprintpcs.com',
    'T-Mobile' => 'tmomail.net',
    'US Cellular' => 'email.uscc.net',
    'Verizon' => 'vtext.com',
    'BoostMobile' => 'sms.myboostmobile.com'
);

$config['preferences'] = array(
    'colorTemp_id' => 'colorTemp',
    'whiteTemp_id' => 'whiteTemp',
    'detergent_id' => 'detergent',
    'bleach_id' => 'bleach',
    'fabSoft_id' => 'fabSoft',
    'dryerSheet_id' => 'dryerSheet',
    'dryLow_id' => 'dryLow',
    'superWash_id' => 'superWash',
    'loads_id' => 'loads'
);

$config['captcha_words'] = array("socks", "pants", "locker", "drier", "washer", "spots", "soaps", "shirt");
$config['controllers'] = array('admin', 'claim', 'employees', 'kiosk', 'locations', 'printer', 'product', 'reports', 'orders', 'customers');
$config['hash_salt'] = 'hCxqMbZSxp5FasM7EF5M';

// droid version and link for download
// used on the phones for checking if there is updated code to download
$config['version'] = '2.7';
$config['link'] = 'android_2_7.apk';

/**
 * List of date formats that will be set by the business in the edit business section of admin
 *
 */
$config['dateFormats'] = array(
    "m/d/y",
    "d/m/y",
    "m/d/Y",
    "d/m/Y",
    "m/d/y g:i a",
    "m/d/y H:i",
    "d/m/y g:i a",
    "d/m/y H:i",
    "Y-m-d H:i:s",
    "m-d-Y H:i:s",
    "F j, Y g:i:sa",
    "Y F j,  g:i:sa",
    "Y/m/d (D) H:i:s",
    "Y年m月d日 (D) H時i分s秒",
    "Y/m/d (D)",
    "Y/m/d",
    "d.m.y",
    "d.m.Y",
    "d.m.y g:i a",
    "d.m.y H:i"
);

$config['japaneseTimeZones'] = array(
  "Asia/Tokyo"  
);

$config['japaneseDays'] = array(
    "月", //monday
    "火", //tuesday
    "水", //wednesday
    "木", //thursday
    "金", //friday
    "土", //saturday
    "日", //sunday
);

$config['locales']['en_US.utf-8'] = "US";
$config['locales']['en_AU.utf-8'] = "Australia";
$config['locales']['fr_FR.utf-8'] = "France";
$config['locales']['de_DE.utf-8'] = "Germany";
$config['locales']['ja_JP.utf-8'] = "Japan";
$config['locales']['en_NZ.utf-8'] = "New Zealand";
$config['locales']['es_MX.utf-8'] = "Mexico";
$config['locales']['nl_NL.utf-8'] = "Netherlands";
$config['locales']['ru_RU.utf-8'] = "Russia";
$config['locales']['es_ES.utf-8'] = "Spain";
$config['locales']['ar_AE.utf-8'] = "United Arab Emirates";
$config['locales']['en_GB.utf-8'] = "UK";
$config['locales']['sv_SE.utf-8'] = "Sweden";

// For pdf generation.. Eg: japanese business
$config['pdf_font_set']['ja_JP.utf-8'] = "non-ascii";

// Popups to show in my account page and apps
// $config['popups'][BUSINESS_ID][ROUTE_ID]
$config['popups']['3']['11'] = array(
  'enabled' => true,
  'view'   => 'custom_templates/laundrylocker/popup'
);

/*
  |--------------------------------------------------------------------------
  | Base Site URL
  |--------------------------------------------------------------------------
  |
  | URL to your CodeIgniter root. Typically this will be your base URL,
  | WITH a trailing slash:
  |
  |	http://example.com/
  |
  | If this is not set then CodeIgniter will guess the protocol, domain and
  | path to your installation.
  |
 */
$config['base_url'] = '';

/*
  |--------------------------------------------------------------------------
  | Index File
  |--------------------------------------------------------------------------
  |
  | Typically this will be your index.php file, unless you've renamed it to
  | something else. If you are using mod_rewrite to remove the page set this
  | variable so that it is blank.
  |
 */
$config['index_page'] = '';

/*
  |--------------------------------------------------------------------------
  | URI PROTOCOL
  |--------------------------------------------------------------------------
  |
  | This item determines which server global should be used to retrieve the
  | URI string.  The default setting of 'AUTO' works for most servers.
  | If your links do not seem to work, try one of the other delicious flavors:
  |
  | 'AUTO'			Default - auto detects
  | 'PATH_INFO'		Uses the PATH_INFO
  | 'QUERY_STRING'	Uses the QUERY_STRING
  | 'REQUEST_URI'		Uses the REQUEST_URI
  | 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
  |
 */
$config['uri_protocol'] = 'AUTO';

/*
  |--------------------------------------------------------------------------
  | URL suffix
  |--------------------------------------------------------------------------
  |
  | This option allows you to add a suffix to all URLs generated by CodeIgniter.
  | For more information please see the user guide:
  |
  | http://codeigniter.com/user_guide/general/urls.html
 */

$config['url_suffix'] = '';

/*
  |--------------------------------------------------------------------------
  | Default Language
  |--------------------------------------------------------------------------
  |
  | This determines which set of language files should be used. Make sure
  | there is an available translation if you intend to use something other
  | than english.
  |
 */
$config['language'] = 'english';

/*
  |--------------------------------------------------------------------------
  | Default Character Set
  |--------------------------------------------------------------------------
  |
  | This determines which character set is used by default in various methods
  | that require a character set to be provided.
  |
 */
$config['charset'] = 'UTF-8';

/*
  |--------------------------------------------------------------------------
  | Enable/Disable System Hooks
  |--------------------------------------------------------------------------
  |
  | If you would like to use the 'hooks' feature you must enable it by
  | setting this variable to TRUE (boolean) .  See the user guide for details.
  |
 */
$config['enable_hooks'] = TRUE;


/*
  |--------------------------------------------------------------------------
  | Class Extension Prefix
  |--------------------------------------------------------------------------
  |
  | This item allows you to set the filename/classname prefix when extending
  | native libraries.  For more information please see the user guide:
  |
  | http://codeigniter.com/user_guide/general/core_classes.html
  | http://codeigniter.com/user_guide/general/creating_libraries.html
  |
 */
$config['subclass_prefix'] = 'MY_';


/*
  |--------------------------------------------------------------------------
  | Allowed URL Characters
  |--------------------------------------------------------------------------
  |
  | This lets you specify with a regular expression which characters are permitted
  | within your URLs.  When someone tries to submit a URL with disallowed
  | characters they will get a warning message.
  |
  | As a security measure you are STRONGLY encouraged to restrict URLs to
  | as few characters as possible.  By default only these are allowed: a-z 0-9~%.:_-
  |
  | Leave blank to allow all characters -- but only if you are insane.
  |
  | DO NOT CHANGE THIS UNLESS YOU FULLY UNDERSTAND THE REPERCUSSIONS!!
  |
 */
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-+';


/*
  |--------------------------------------------------------------------------
  | Enable Query Strings
  |--------------------------------------------------------------------------
  |
  | By default CodeIgniter uses search-engine friendly segment based URLs:
  | example.com/who/what/where/
  |
  | By default CodeIgniter enables access to the $_GET array.  If for some
  | reason you would like to disable it, set 'allow_get_array' to FALSE.
  |
  | You can optionally enable standard query string based URLs:
  | example.com?who=me&what=something&where=here
  |
  | Options are: TRUE or FALSE (boolean)
  |
  | The other items let you set the query string 'words' that will
  | invoke your controllers and its functions:
  | example.com/index.php?c=controller&m=function
  |
  | Please note that some of the helpers won't work as expected when
  | this feature is enabled, since CodeIgniter is designed primarily to
  | use segment based URLs.
  |
 */
$config['allow_get_array'] = TRUE;
$config['enable_query_strings'] = FALSE;
$config['controller_trigger'] = 'c';
$config['function_trigger'] = 'm';
$config['directory_trigger'] = 'd'; // experimental not currently in use

/*
  |--------------------------------------------------------------------------
  | Error Logging Threshold
  |--------------------------------------------------------------------------
  |
  | If you have enabled error logging, you can set an error threshold to
  | determine what gets logged. Threshold options are:
  | You can enable error logging by setting a threshold over zero. The
  | threshold determines what gets logged. Threshold options are:
  |
  |	0 = Disables logging, Error logging TURNED OFF
  |	1 = Error Messages (including PHP errors)
  |	2 = Debug Messages
  |	3 = Informational Messages
  |	4 = All Messages
  |
  | For a live site you'll usually only enable Errors (1) to be logged otherwise
  | your log files will fill up very fast.
  |
 */
$config['log_threshold'] = 0;

/*
  |--------------------------------------------------------------------------
  | Error Logging Directory Path
  |--------------------------------------------------------------------------
  |
  | Leave this BLANK unless you would like to set something other than the default
  | application/logs/ folder. Use a full server path with trailing slash.
  |
 */
$config['log_path'] = '';

/*
  |--------------------------------------------------------------------------
  | Date Format for Logs
  |--------------------------------------------------------------------------
  |
  | Each item that is logged has an associated date. You can use PHP date
  | codes to set your own date formatting
  |
 */
$config['log_date_format'] = 'Y-m-d H:i:s';

/*
  |--------------------------------------------------------------------------
  | Cache Directory Path
  |--------------------------------------------------------------------------
  |
  | Leave this BLANK unless you would like to set something other than the default
  | system/cache/ folder.  Use a full server path with trailing slash.
  |
 */
$config['cache_path'] = '';

/*
  |--------------------------------------------------------------------------
  | Encryption Key
  |--------------------------------------------------------------------------
  |
  | If you use the Encryption class or the Session class you
  | MUST set an encryption key.  See the user guide for info.
  |
 */
$config['encryption_key'] = 'XN#&UN#&*XNUIWXNINUQNUIN@N987n8734nhd';

/*
  |--------------------------------------------------------------------------
  | Session Variables
  |--------------------------------------------------------------------------
  |
  | 'sess_cookie_name'		= the name you want for the cookie
  | 'sess_expiration'			= the number of SECONDS you want the session to last.
  |   by default sessions last 7200 seconds (two hours).  Set to zero for no expiration.
  | 'sess_expire_on_close'	= Whether to cause the session to expire automatically
  |   when the browser window is closed
  | 'sess_encrypt_cookie'		= Whether to encrypt the cookie
  | 'sess_use_database'		= Whether to save the session data to a database
  | 'sess_table_name'			= The name of the session database table
  | 'sess_match_ip'			= Whether to match the user's IP address when reading the session data
  | 'sess_match_useragent'	= Whether to match the User Agent when reading the session data
  | 'sess_time_to_update'		= how many seconds between CI refreshing Session Information
  |
 */
$config['sess_cookie_name'] = 'ci_session';
$config['sess_expiration'] = time() + (3600 * 24 * 365 * 5);
$config['sess_expire_on_close'] = FALSE;
$config['sess_encrypt_cookie'] = TRUE;
$config['sess_use_database'] = FALSE;
//$config['sess_table_name']		= 'ciSessions';
$config['sess_match_ip'] = FALSE;
$config['sess_match_useragent'] = FALSE;
$config['sess_time_to_update'] = 1;

/*
  |--------------------------------------------------------------------------
  | Cookie Related Variables
  |--------------------------------------------------------------------------
  |
  | 'cookie_prefix' = Set a prefix if you need to avoid collisions
  | 'cookie_domain' = Set to .your-domain.com for site-wide cookies
  | 'cookie_path'   =  Typically will be a forward slash
  | 'cookie_secure' =  Cookies will only be set if a secure HTTPS connection exists.
  |
 */


$config['cookie_prefix'] = "";
$config['cookie_domain'] = ""; //default
//$config['cookie_domain']	= $domain; //custom
$config['cookie_path'] = "/";
$config['cookie_secure'] = TRUE;//for PCI compliance
$config['cookie_httponly'] = TRUE;//for PCI compliance

/*
  |--------------------------------------------------------------------------
  | Global XSS Filtering
  |--------------------------------------------------------------------------
  |
  | Determines whether the XSS filter is always active when GET, POST or
  | COOKIE data is encountered
  |
 */
$config['global_xss_filtering'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Cross Site Request Forgery
  |--------------------------------------------------------------------------
  | Enables a CSRF cookie token to be set. When set to TRUE, token will be
  | checked on a submitted form. If you are accepting user data, it is strongly
  | recommended CSRF protection be enabled.
  |
  | 'csrf_token_name' = The token name
  | 'csrf_cookie_name' = The cookie name
  | 'csrf_expire' = The number in seconds the token should expire.
 */
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'csrf_test_name';
$config['csrf_cookie_name'] = 'csrf_cookie_name';
$config['csrf_expire'] = 7200;

/*
  |--------------------------------------------------------------------------
  | Output Compression
  |--------------------------------------------------------------------------
  |
  | Enables Gzip output compression for faster page loads.  When enabled,
  | the output class will test whether your server supports Gzip.
  | Even if it does, however, not all browsers support compression
  | so enable only if you are reasonably sure your visitors can handle it.
  |
  | VERY IMPORTANT:  If you are getting a blank page when compression is enabled it
  | means you are prematurely outputting something to your browser. It could
  | even be a line of whitespace at the end of one of your scripts.  For
  | compression to work, nothing can be sent before the output buffer is called
  | by the output class.  Do not 'echo' any values with compression enabled.
  |
 */
$config['compress_output'] = FALSE;

/*
  |--------------------------------------------------------------------------
  | Master Time Reference
  |--------------------------------------------------------------------------
  |
  | Options are 'local' or 'gmt'.  This pref tells the system whether to use
  | your server's local time as the master 'now' reference, or convert it to
  | GMT.  See the 'date helper' page of the user guide for information
  | regarding date handling.
  |
 */
$config['time_reference'] = 'local';


/*
  |--------------------------------------------------------------------------
  | Rewrite PHP Short Tags
  |--------------------------------------------------------------------------
  |
  | If your PHP installation does not have short tag support enabled CI
  | can rewrite the tags on-the-fly, enabling you to utilize that syntax
  | in your view files.  Options are TRUE or FALSE (boolean)
  |
 */
$config['rewrite_short_tags'] = TRUE;


/*
  |--------------------------------------------------------------------------
  | Reverse Proxy IPs
  |--------------------------------------------------------------------------
  |
  | If your server is behind a reverse proxy, you must whitelist the proxy IP
  | addresses from which CodeIgniter should trust the HTTP_X_FORWARDED_FOR
  | header in order to properly identify the visitor's IP address.
  | Comma-delimited, e.g. '10.0.1.200,10.0.1.201'
  |
 */
$config['proxy_ips'] = '';


/* End of file config.php */
/* Location: ./application/config/config.php */
