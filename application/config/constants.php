<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('ROOT_DIR', str_ireplace("application/config", "", __DIR__));

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('STANDARD_DATE_FORMAT', "m/d/y g:i a"); // 11/15/12 10:54 AM
define('FULL_DATE_FORMAT', "F j, Y g:i:sa");
define('SHORT_DATE_FORMAT', "m/d/y");

//Allows us to move images to a CDN when in production
define("CDNIMAGES", '/images/');

define("SUPPORTEMAIL", 'support@laundrylocker.com');
define("SYSTEMEMAIL", 'admin@droplocker.com');
define("DEVEMAIL", 'development@droplocker.com');
define("SYSTEMFROMNAME", "Droplocker Admin");

define("ELECTRONIC_LOCKER", 2); //This is the expected locker type value for an electronic locker.
define("WFCATEGORY", 28); //default category for wash and fold
define("DETERGENT_CATEGORY_ID", 13); //This is the category

//This is the expected moduleID in the database for the wash and fold module.
define("UPCHARGEMODULE", 8);
define("WFMODULE", 1);

//This is the expected moduleID value in the database for the dry clean module.
define("DCMODULE", 3);

define("HOMEDELIVERYTYPE", 4); //locationTypeID
define("DEVPAYERID", 'V24P1EC9F781'); //paypal dev id
define("WFPRODUCTPROCESSID", 512); // This is only used in the UnitTest... DO NOT USE IN PRODUCTION
define("WFTOWELSPRODUCTPROCESSID", 604);
define("PICKED_UP_STATUS", 1); //This is the order status of 'Picked Up".
define("WASH_AND_FOLD_CATEGORY_SLUG", "wf"); //This is the expected slug value for the Wash and Fold category.
define("BUSINESS_IMAGES_DIRECTORY", "/images/logos/"); //This is the directory where business can upload and store their images.
define("REDO_ORDER", "Redo Order");
define("AUTHORIZENET_API_LOGIN_ID", "6H3t67jZXQ"); //This is the Droplocker dev Authoriz.net API login key.
define("AUTHORIZENET_TRANSACTION_KEY", "6Z25W4uVsgY9G8Mc"); //This is the droplocker dev Authorize.NetTransaction key.


/* End of file constants.php */
/* Location: ./application/config/constants.php */
