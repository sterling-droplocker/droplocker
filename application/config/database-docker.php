<?php
$active_group = 'default';
$query_builder = TRUE;

$db['default']['hostname'] = getenv("MYSQL_HOST");
$db['default']['username'] =getenv("MYSQL_USER");
$db['default']['password'] = getenv("MYSQL_PASSWORD");
$db['default']['database'] = getenv("MYSQL_DB_NAME");
$db['default']['dbdriver'] = getenv("MYSQL_DRIVER");
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = FALSE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;
//$db['default']['port'] = 4040;

$db['reporting']['hostname'] = getenv("MYSQL_HOST");
$db['reporting']['username'] = getenv("MYSQL_USER");
$db['reporting']['password'] = getenv("MYSQL_PASSWORD");
$db['reporting']['database'] = getenv("MYSQL_DB_NAME");
$db['reporting']['dbdriver'] = getenv("MYSQL_DRIVER");
$db['reporting']['dbprefix'] = '';
$db['reporting']['pconnect'] = TRUE;
$db['reporting']['db_debug'] = FALSE;
$db['reporting']['cache_on'] = FALSE;
$db['reporting']['cachedir'] = '';
$db['reporting']['char_set'] = 'utf8';
$db['reporting']['dbcollat'] = 'utf8_general_ci';
$db['reporting']['swap_pre'] = '';
$db['reporting']['autoinit'] = TRUE;
$db['reporting']['stricton'] = FALSE;

// Local test database

$db['test']['hostname'] = getenv("MYSQL_HOST");
$db['test']['username'] = getenv("MYSQL_USER");
$db['test']['password'] = getenv("MYSQL_PASSWORD");
$db['test']['database'] = "__droplocker_local";
$db['test']['dbdriver'] = getenv("MYSQL_DRIVER");
$db['test']['dbprefix'] = "";
$db['test']['pconnect'] = TRUE;
$db['test']['db_debug'] = TRUE;
$db['test']['cache_on'] = FALSE;
$db['test']['cachedir'] = "";
$db['test']['char_set'] = "utf8";
$db['test']['dbcollat'] = "utf8_general_ci";
$db['test']['autoinit'] = TRUE;
$db['test']['stricton'] = FALSE;

/**
echo '<pre>';
print_r($db['default']);
echo '</pre>';

echo 'Trying to connect to database: ' .$db['default']['database'];
$dbh = mysql_connect $db['default']['hostname'], $db['default']['username'],$db['default']['password']) or die('Cannot connect to the database because: ' . mysql_error());
mysql_select_db ($db['default']['database']);
echo '<br />   Connected OK:'  ;
die( 'file: ' .__FILE__ . '--> Line: ' .__LINE__);
 */
