<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";
//$route['404_override'] = 'main/index';

$route['admin'] = "admin/index";
$route['admin/login'] = "admin/login";
$route['admin/index/login'] = "admin/login";
$route['admin/logout'] = "admin/index/logout";

$route['services'] = 'main/services/';
$route['register'] = 'account/main/view_registration';
$route['login'] = 'account/main/login';
$route['logout'] = 'account/main/logout';
$route['forgot_password'] = 'account/main/forgot_password';
$route['account'] = 'account/main';

$route['[a-zA-Z0-9]+/login'] = 'account/main/login';
$route['[a-zA-Z0-9]+/signup'] = 'account/main/view_registration';

$route['get/the/publicLocations.js?(:any)'] = 'main/get_business_locations';

//main site routes
//$route['locations.php'] = 'main/locations';
//$route['business.php'] = 'main/business';
//$route['pricing.php'] = 'main/pricing';
if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST']!='droplocker.com') {
    $route["^.*\.(html?|php)$"] = "main/get_page/$1";
}

$route['set_cookie'] = "/main/set_cookie/";

$route['admin/orders/quick_order/(:any)/(:any)'] = 'admin/orders/quick_order/$1/$2';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
