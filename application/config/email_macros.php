<?php

/**
 * This is the production e-mail server. This one should be always used unless the server goes down.
 * THe limit is 50000 per day
 */
$config['smtp_host'] = 'ssl://email-smtp.us-east-1.amazonaws.com';
$config['smtp_user'] = 'AKIAJCYZXZXNH2OKTOEA';
$config['smtp_pass'] = 'Aps13ySgJCzOR0dpzOVbWFnfirobqqfkyvHtk9CZAwyA';
//$config['smtp_crypto'] = 'ssl';

/**
 * This is the backup e-mail server, do not use unless production goes down
 * The e-mail limit for this server is 10000 per day
 */
//$config['smtp_host'] = 'ssl://email-smtp.us-east-1.amazonaws.com';
//$config['smtp_user'] = 'AKIAJNHD4GATMXPLJJMA';
//$config['smtp_pass'] = 'Ate9z+AK2apLoFPhLXle0YSWDbz8xWgkAYpNMU9+OBE7';

$config['protocol'] = 'smtp';
$config['mailpath'] = '/usr/sbin/sendmail';
$config['charset'] = 'UTF-8';
$config['wordwrap'] = FALSE;
$config['smtp_port'] = '465';
$config['mailtype'] = 'html';


$macro['customer']['customerID']='';
$macro['customer']['customer_firstName']='';
$macro['customer']['customer_lastName']='';
$macro['customer']['customer_fullName']='';
$macro['customer']['customer_username']='';
$macro['customer']['customer_phone']='';
$macro['customer']['customer_email']='';
$macro['customer']['customer_address']='';
$macro['customer']['customer_address2']='';
$macro['customer']['customer_city']='';
$macro['customer']['customer_state']='';
$macro['customer']['customer_zip']='';
$macro['customer']['customer_Dump']='';
$macro['customer']['customer_cardNumber'] = ''; //This is the customer's credit card
$macro['customer']['customer_expMo'] = ''; //This is the customer's credit card expiration month.
$macro['customer']['customer_expYr'] = ''; //This is the customer's credit card expiration year.
$macro['customer']['customer_customerDiscounts'] = '';
$macro['customer']['customer_defaultLocation'] = '';
$macro['customer']['customer_lockersLocation'] = '';
$macro['customer']['customer_firstOrderMessage'] = '';

$macro['customerDiscount']['customerDiscount_amount']='';
$macro['customerDiscount']['customerDiscount_amountType']='';
$macro['customerDiscount']['customerDiscount_frequency']='';
$macro['customerDiscount']['customerDiscount_description']='';
$macro['customerDiscount']['customerDiscount_active']='';
$macro['customerDiscount']['customerDiscount_updated']='';
$macro['customerDiscount']['customerDiscount_amountApplied']='';
$macro['customerDiscount']['customerDiscount_expireDate']='';
$macro['customerDiscount']['customerDiscount_extendedDesc']='';

$macro['business']['business_companyName']='';
$macro['business']['business_address']='';
$macro['business']['business_address2']='';
$macro['business']['business_email']='';
$macro['business']['business_phone']='';
$macro['business']['business_zipcode']='';
$macro['business']['business_city']='';
$macro['business']['business_state']='';
$macro['business']['business_website_url'] = '';
$macro['business']['business_didYouKnow'] = "";
$macro['business']['business_year'] = "";
$macro['business']['business_email_logo_url'] = ""; //This is the email logo URL for the specified e-mail placement image.
$macro['business']['business_barcode_url'] = ""; //This is used to generate a barcode
$macro['business']['business_qrcode_url'] = ""; //This is used to generate a QR code

$macro['claim']['claimID']='';
$macro['claim']['claim_lockerName']='';
$macro['claim']['claim_companyName']='';
$macro['claim']['claim_locationTitle']='';
$macro['claim']['claim_address']='';
$macro['claim']['claim_address2']='';
$macro['claim']['claim_state']='';
$macro['claim']['claim_city']='';
$macro['claim']['claim_zipcode']='';
$macro['claim']['claim_notes']='';
$macro['claim']['claim_updated']='';
$macro['claim']['claim_pickup_time'] = "";
$macro['claim']['claim_locationType'] = "";
$macro['claim']['claim_tracking_code'] = '';
$macro['claim']['claim_delivery_time'] = '';

$macro['giftCard']['giftCard_code']='';
$macro['giftCard']['giftCard_email']='';
$macro['giftCard']['giftCard_fullName']='';
$macro['giftCard']['giftCard_amount']='';
$macro['giftCard']['giftCard_description']='';
$macro['giftCard']['giftCard_sentToRecipent']='';

// these fields are from the businessLaundryPLan table
$macro['laundryPlan']['laundryPlan_name']='';
$macro['laundryPlan']['laundryPlan_description']='';
$macro['laundryPlan']['laundryPlan_price']='';
$macro['laundryPlan']['laundryPlan_pounds']='';
$macro['laundryPlan']['laundryPlan_poundsDescription']='';
$macro['laundryPlan']['laundryPlan_days']='';
$macro['laundryPlan']['laundryPlan_rollover']='';
$macro['laundryPlan']['laundryPlan_term']='';
$macro['laundryPlan']['laundryPlan_endDate']='';
$macro['laundryPlan']['laundryPlan_startDate']='';
$macro['laundryPlan']['laundryPlan_credit_amount']='';
$macro['laundryPlan']['laundryPlan_expired_discount_percent']='';

$macro['order']['orderID']='';
$macro['order']['order_itemTable']='';
$macro['order']['order_notes'] = '';
$macro['order']['order_companyName'] = '';
$macro['order']['order_locationTitle'] = '';
$macro['order']['order_address'] = '';
$macro['order']['order_address2'] = '';
$macro['order']['order_city'] = '';
$macro['order']['order_state'] = '';
$macro['order']['order_zip'] = '';
$macro['order']['order_lockerName'] = '';
$macro['order']['order_lockerCode'] = '';
$macro['order']['order_Dump'] = '';
$macro['order']['order_lockerLimited'] = '';
$macro['order']['order_dateCreated'] = '';
$macro['order']['order_statusDate'] = '';
$macro['order']['order_lockerAccessCode'] = '';
$macro['order']['order_pickupLockerName'] = '';
$macro['order']['order_partiallyDeliveredMessage'] = '';
$macro['order']['order_dueDate'] = '';
$macro['order']['order_bagNumber'] = '';
$macro['order']['order_lockerList'] = '';

$macro['lockerLoot']['lockerLoot_points'] = '';
$macro['lockerLoot']['lockerLoot_referralCustomerName'] = '';
$macro['lockerLoot']['lockerLoot_referralCustomerFirstName'] = '';
$macro['lockerLoot']['lockerLoot_referralCustomerLastName'] = '';
$macro['lockerLoot']['lockerLoot_referrerCustomerName'] = '';
$macro['lockerLoot']['lockerLoot_referrerCustomerFirstName'] = '';
$macro['lockerLoot']['lockerLoot_referrerCustomerLastName'] = '';
$macro['lockerLoot']['lockerLoot_dateCreated'] = '';

$config['macros'] = $macro;
