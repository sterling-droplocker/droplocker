<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Helper files
| 4. Custom config files
| 5. Language files
| 6. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packges
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/

$autoload['packages'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in the system/libraries folder
| or in your application/libraries folder.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'session', 'xmlrpc');
*/
$zacl = (PHP_SAPI === 'cli')?'':'Zacl';
$autoload['libraries'] = array('database', 'subdomain','session','email_actions','Email', 'user_agent', 'logger', $zacl);


/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/

$autoload['helper'] = array('utility','actions', 'form', 'date', 'email', 'languageView', 'ajax', 'acl', 'html', 'module', 'serviceType');


/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.
*/

$autoload['config'] = array('email_macros');


/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/

$autoload['language'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('model1', 'model2');
|
*/

$autoload['model'] = array();

if (!function_exists("third_party_autoload")) {
    function third_party_autoload($class) {
        $third_party = array(
            'Facebook' => APPPATH . "third_party/facebook-php-sdk/facebook.php",
            'Bitly' => APPPATH . "third_party/bitly-api-php-master/Bitly.php"
        );

        if (!class_exists($class, false) && !interface_exists($class, false)) {
            if (isset($third_party[$class])) {
                require_once($third_party[$class]);
            }
        }
    }
    spl_autoload_register('third_party_autoload');
}

if (!function_exists("droplockerobject_autoload")) {
    function droplockerobject_autoload($class)
    {
        if (!class_exists($class, false) && !interface_exists($class, false)) {
            $parts = explode("\\", $class);

            $file = end($parts);
            $path = APPPATH . 'libraries/objects/' . $file . EXT;
            if (file_exists($path)) {
                require_once($path);
            }
        }
    }
    spl_autoload_register('droplockerobject_autoload');
}

if (!function_exists("lib_psr0_autoload")) {
	function lib_psr0_autoload($class) {
        if (!class_exists($class, false) && !interface_exists($class, false)) {
            $path = APPPATH . 'libraries/' . str_replace(array('\\', '_'), '/', $class) . EXT;
            if (file_exists($path)) {
				require_once($path);
			}
        }
    }
    spl_autoload_register('lib_psr0_autoload');
}

if (!function_exists("unittest_autoload")) {
    function unittest_autoload($class)
    {
        $path = APPPATH . "libraries/UnitTest/" . $class . EXT;
        if (file_exists($path)) {
            require_once($path);
        }
    }
}
spl_autoload_register("unittest_autoload");
/*
function __autoload($class)
{

    $path = APPPATH . 'libraries/objects/' . strtolower($class) . EXT;
    if (file_exists($path)) {
            require_once($path);
    }
}
*/

/* End of file autoload.php */
/* Location: ./application/config/autoload.php */
