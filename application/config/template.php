<?php  if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template
| group to make active.  By default there is only one group (the
| "default" group).
|
*/
$template['active_template'] = 'default';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land.
|   You may also include default markup, wrappers and attributes here
|   (though not recommended). Region keys must be translatable into variables
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/


/************************************************
* AdminTemplate
*
* Used for droplocker.com/admin
*/
$template['admin']['template'] = 'admin/template';
$template['admin']['regions'] = array(
   'header'=>array(
      'content' => array('<h1>Business Admin</h1>'),
      'name' => 'Page Header',
      'wrapper' => '<div>',
      'attributes' => ''),
   'page_title',
   'menu',
   'sidebar',
   'content',
   'footer',
);
$template['admin']['parser'] = 'parser';
$template['admin']['parser_method'] = 'parse';
$template['admin']['parse_template'] = false;


/************************************************
 * DropLocker Template
 *
 * The template for droplocker.com
 */
$template['droplocker']['template'] = 'droplocker/template';
$template['droplocker']['regions'] = array(
   'page',
);
$template['droplocker']['parser'] = 'parser';
$template['droplocker']['parser_method'] = 'parse';
$template['droplocker']['parse_template'] = false;



/************************************************
 * Default Template
 *
 * This template will be displayed when accessing droplocker.com acount section
 * unless the business has a custom template
 *
 */
$template['default']['template'] = 'template';
$template['default']['regions'] = array(
    'title',
    'javascript',
    'style',
    'header',
    'page_title',
    'menu',
    'sidebar',
    'content',
    'footer',
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = false;


$config['templates'] = array();
$config['templates']['default'] = 'template';
$config['templates']['empty'] = 'empty';
$config['templates']['newBusiness'] = 'custom_templates/newBusiness';
$config['templates']['bizzie'] = 'custom_templates/bizzie/account';

$config['templates']['alfredservice']         = 'custom_templates/alfredservice/account';
$config['templates']['amysgreencleaners']     = 'custom_templates/amysgreencleaners/account';
$config['templates']['boomeranggr']           = 'custom_templates/boomeranggr/account';
$config['templates']['breezylaundrylockers']  = 'custom_templates/breezylaundrylockers/account';
$config['templates']['bubble-stitch']         = 'custom_templates/bubble-stitch/account';
$config['templates']['bubblesandsudslockers'] = 'custom_templates/bubblesandsudslockers/account';
$config['templates']['butlerbox']             = 'custom_templates/butlerbox/account';
$config['templates']['cleanbox']              = 'custom_templates/cleanbox/account';
$config['templates']['cleanboxdfw']           = 'custom_templates/cleanboxdfw/account';
$config['templates']['cleanpost']             = 'custom_templates/cleanpost/account';
$config['templates']['comfortcube']           = 'custom_templates/comfortcube/account';
$config['templates']['conciergecleanersatl']  = 'custom_templates/conciergecleanersatl/account';
$config['templates']['dashlocker']            = 'custom_templates/dashlocker/account';
$config['templates']['daousteco']             = 'custom_templates/daousteco/account';
$config['templates']['dormaid']               = 'custom_templates/dormaid/account';
$config['templates']['dropgo']                = 'custom_templates/dropgo/account';
$config['templates']['dropnlock']             = 'custom_templates/dropnlock/account';
$config['templates']['dropnlockerz']          = 'custom_templates/dropnlockerz/account';
$config['templates']['dropspotcleaner']       = 'custom_templates/dropspotcleaner/account';
$config['templates']['drycleanlockers']       = 'custom_templates/drycleanlockers/account';
$config['templates']['dryydc']                = 'custom_templates/dryydc/account';
$config['templates']['easylaundry']           = 'custom_templates/easylaundry/account';
$config['templates']['freshbox247']           = 'custom_templates/freshbox247/account';
$config['templates']['freshgarmentvalet']     = 'custom_templates/freshgarmentvalet/account';
$config['templates']['fressshhh']             = 'custom_templates/fressshhh/account';
$config['templates']['groombox']              = 'custom_templates/groombox/account';
$config['templates']['hakuyosha']             = 'custom_templates/hakuyosha/account';
$config['templates']['imhuckleberry']         = 'custom_templates/imhuckleberry/account';
$config['templates']['jandjlockers']          = 'custom_templates/jandjlockers/account';
$config['templates']['jarvisserve']           = 'custom_templates/jarvisserve/account';
$config['templates']['laundrop']              = 'custom_templates/laundrop/account';
$config['templates']['laundryboxde']          = 'custom_templates/laundryboxde/account';
$config['templates']['laundryboxcleaners']    = 'custom_templates/laundryboxcleaners/account';
$config['templates']['laundryboxusa']         = 'custom_templates/laundryboxusa/account';
$config['templates']['laundryconcierge']      = 'custom_templates/laundryconcierge/account';
$config['templates']['laundrygenie']          = 'custom_templates/laundrygenie/account';
$config['templates']['laundrylocker']         = 'custom_templates/laundrylocker/account';
$config['templates']['laundrypress']          = 'custom_templates/laundrypress/account';
$config['templates']['laundrystork']          = 'custom_templates/laundrystork/account';
$config['templates']['laundryup']             = 'custom_templates/laundryup/account';
$config['templates']['lavalocker']            = 'custom_templates/lavalocker/account';
$config['templates']['leveelocker']           = 'custom_templates/leveelocker/account';
$config['templates']['lockergenie']           = 'custom_templates/lockergenie/account';
$config['templates']['lockerdropcleaners']    = 'custom_templates/lockerdropcleaners/account';
$config['templates']['lockerlobby']           = 'custom_templates/lockerlobby/account';
$config['templates']['lockermd']              = 'custom_templates/lockermd/account';
$config['templates']['lockerrevolution']      = 'custom_templates/lockerrevolution/account';
$config['templates']['lovelockerz']           = 'custom_templates/lovelockerz/account';
$config['templates']['lucyslaundry']          = 'custom_templates/lucyslaundry/account';
$config['templates']['mintlocker']            = 'custom_templates/mintlocker/account';
$config['templates']['mintlocker_ru']         = 'custom_templates/mintlocker_ru/account';
$config['templates']['mrlocker']              = 'custom_templates/mrlocker/account';
$config['templates']['mrwash']                = 'custom_templates/mrwash/account';
$config['templates']['mybutlerbox']           = 'custom_templates/mybutlerbox/account';
$config['templates']['mycleaningcloset']      = 'custom_templates/mycleaningcloset/account';
$config['templates']['mydropoff']             = 'custom_templates/mydropoff/account';
$config['templates']['myfivestarcleaners']    = 'custom_templates/fivestarcleaners/account';
$config['templates']['myvaletlaundry']        = 'custom_templates/myvaletlaundry/account';
$config['templates']['newash']                = 'custom_templates/newash/account';
$config['templates']['oneredlocker']          = 'custom_templates/oneredlocker/account';
$config['templates']['pinglocker']            = 'custom_templates/pinglocker/account';
$config['templates']['pressAtlanta']          = 'custom_templates/pressAtlanta/account';
$config['templates']['pressbox']              = 'custom_templates/pressbox/account';
$config['templates']['presscleaners']         = 'custom_templates/presscleaners/account';
$config['templates']['rockymountainlaundries']= 'custom_templates/rockymountainlaundries/account';
$config['templates']['smartlocker']           = 'custom_templates/smartlocker/account';
$config['templates']['soapbox']               = 'custom_templates/soapboxdrycleaning/account';
$config['templates']['spkl']                  = 'custom_templates/spkl/account';
$config['templates']['tidecleaners']           = 'custom_templates/tidecleaners/account';
$config['templates']['tintolocker']           = 'custom_templates/tintolocker/account';
$config['templates']['tintoreriabosques']     = 'custom_templates/tintoreriabosques/account';
$config['templates']['totallockers']          = 'custom_templates/totallockers/account';
$config['templates']['urbanlaundrychicago']   = 'custom_templates/urbanlaundrychicago/account';
$config['templates']['usecleandrop']          = 'custom_templates/usecleandrop/account';
$config['templates']['usequickdrop']          = 'custom_templates/usequickdrop/account';
$config['templates']['usepressbox']           = 'custom_templates/usepressbox/account';
$config['templates']['usewashbox']            = 'custom_templates/usewashbox/account';
$config['templates']['voilalaundry']          = 'custom_templates/voilalaundry/account';
$config['templates']['washlocker']            = 'custom_templates/washlocker/account';
$config['templates']['youngsdrycleaning']     = 'custom_templates/youngsdrycleaning/account';
$config['templates']['zenboxed']              = 'custom_templates/zenboxed/account';
$config['templates']['zerolaundry']           = 'custom_templates/zerolaundry/account';
$config['templates']['zoomlocker']            = 'custom_templates/zoomlocker/account';

foreach ($config['templates'] as $key => $path) {
    $template[$key]['template'] = $path;
    $template[$key]['regions'] = array(
        'title',
        'javascript',
        'style',
        'header',
        'page_title',
        'menu',
        'sidebar',
        'content',
        'footer',
    );
    $template[$key]['parser'] = 'parser';
    $template[$key]['parser_method'] = 'parse';
    $template[$key]['parse_template'] = false;
}

/* End of file template.php */
/* Location: ./system/application/config/template.php */
