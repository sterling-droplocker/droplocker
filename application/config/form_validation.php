<?php
$config = array(
    "update_reminder_email" => array(
        array("field" => "reminder_emailID", "label" => "Reminder Email ID", "rules" => "required|is_natural_no_zero|does_reminder_email_exist"),
        array("field" => "business_language_id", "Business Language", "required|is_natural_no_zero|does_business_language_exist"),
        array("field" => "reminder_id", "label"  => "Reminder ID", "rules" => "required|is_natural_no_zero|does_reminder_exist"),
        array("field" => "subject", "label" => "Subject", "rules" => "required"),
        array("field" => "body", "label" => "Body", "rules" => "required")
    ),
    "create_reminder_email" => array(
        array("field" => "business_language_id", "Business Language ID", "required|is_natural_no_zero|does_business_language_exist"),
        array("field" => "reminder_id", "label"  => "Reminder ID", "rules" => "required|is_natural_no_zero|does_reminder_exist"),
        array("field" => "subject", "label" => "Subject", "rules" => "required"),
        array("field" => "body", "label" => "Body", "rules" => "required")
    ),
    "update_coupon_ajax" => array(
        array("field" => "couponID", "label" => "Coupon ID", "rules" => "required|is_natural_no_zero"),
        array("field" => "property", "label" => "Property", "rules" => "required")
    ),
    "get_product_suppliers" => array(
        array("field" => "product_process_id", "label" => "Product Process ID", "rules" => "required|is_natural_no_zero|does_product_process_exist"),
        array("field" => "order_id", "label" => "Order ID", "rules" => "required|is_natural_no_zero|does_order_exist")
    )
);
