<?php
/**
 * Location account/locations.php
 */

class Locations extends My_Account_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->requireLogin();

        $this->load->model('customer_model');
        $this->load->model('location_model');
    }

    /**
     * Shows the map for selecting customer locations
     */
    public function index()
    {
        $latlon = array();
        $show_all = false;

        /**
         * get the total number of location for each business
         */
        $location_count = $this->location_model->location_count(array(
            'business_id' => $this->business_id
       ));

        if ($location_count <= 60) {
            $show_all = true;
        }

        //get the customer locations
        $this->load->model('customer_location_model');
        $current_locations = $this->customer_location_model->simple_array($this->customer_id, $this->business_id);

        //if the customer has no locations
        if (empty($current_locations)) {

            //if the customer doesn't have an address redirect them to set up their profile
            if (!$this->customer->address1) {
                $message = get_translation('set_your_address_error',"business_account/locations",
                    array(), "Please set your address so we can find a close location", $this->customer_id);

                set_alert('error', $message);

                return redirect('/account/profile/edit');
            }


            //use the customer address to find an exact match

            //if this is an exact match add it to the users locations
            $location_search = array(
                'address'     => $this->customer->address1,
                'state'       => $this->customer->state,
                'city'        => $this->customer->city,
                'business_id' => $this->business_id,
                'status'      => 'active',
            );

            if ($exactlocation = $this->location_model->get($location_search)) {
                // check to see if this location has any active lockers
                if ($this->location_model->getActiveLockerCount($exactlocation[0]->locationID)) {
                    $this->customer_location_model->save(array(
                        'customer_id' => $this->customer_id,
                        'location_id' => $exactlocation[0]->locationID,
                    ));

                    return redirect($this->uri->uri_string());
                }
            }

            $latlon = array($this->customer->lat, $this->customer->lon);
            if (!$latlon[0]) {
                $geo = geocode($this->business->address, $this->business->city, $this->business->state);
                $latlon = array($geo['lat'], $geo['lng']);
            }

            $content['customer_locations'] = $current_locations;
            $content['json_customer_locations'] = json_encode(array());

            if(!isset($_POST['search_submit'])) {
                $content['message'] = get_translation('address_not_set_error',"business_account/locations",
                    array(), "You do not have any locations setup yet. In order to place an order, we need to know where you will be using our service.", $this->customer_id);
            }

        } else { //the customer has locations
            //get the customer locations
            $options = array(
                'locationID' => $current_locations,
                'business_id' => $this->business_id,
                'with_location_type' => true,
                'select' => 'companyName, hours,location, locationType.name, locationType_id, address, lat, lon, locationID, serviceType, city, state',
            );
            $customer_locations = $this->location_model->get($options);
            $customer_locations = $this->_setInfoBoxImages($customer_locations);
            $customer_locations_formatted_hash = array();

            foreach ($customer_locations as &$customer_location) {   //setup to use the locationID as key
                //The following statement retreives the business translation for the location type name from the Location Types language view.
                $customer_location->name = get_translation($customer_location->name,"Location Types", array(), $customer_location->name);
                $customer_locations_formatted_hash[ $customer_location->locationID ] = $customer_location;
            }
            $content['json_customer_locations'] = json_encode($customer_locations_formatted_hash);
            $content['customer_locations'] = $customer_locations;
        }

        $content['businessAddress'] = $this->business->address;
        $content['businessState']   = $this->business->state;
        $content['businessCity']    = $this->business->city;
        $content['businessZip']     = $this->business->zipcode;

        //lat/lon
        $content['businessLat']     = $this->business->lat;
        $content['businessLon']     = $this->business->lng;

        $content['customerAddress'] = getFormattedAddress($this->customer);//implode(', ', $content['customerAddress'] );
        $content['customerLatLon']  = array('lat' => $this->customer->lat, 'lon' => $this->customer->lon);
        $content['customer']        = $this->customer;

        //end of my code, lets see how small and elegant I can make this

        $content['show_all']        = $show_all;
        $content['business_id']     = $this->business_id;
        $content['mypoint']         = json_encode($latlon);

        $maps_api_key = get_business_meta($this->business_id, 'maps_api_key');
        $maps_api_key = empty($maps_api_key)?"":"&key=".$maps_api_key;
        $content['maps_api_key'] = $maps_api_key;

        switch ($this->business->show_locations_map) {
            case 3:
                $view = 'locations_index_v3';

                $content['show_home_pickup'] = false;

                $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
                if ($homeDeliveryService) {
                    $content['show_home_pickup'] = $homeDeliveryService->canCustomerRequestHomeDelivery($this->customer);
                }

                if (empty($content['customerAddress'])) {
                    $firstLocation = $customer_locations[0];
                    $content['customerAddress'] = "{$firstLocation->address}, {$firstLocation->city}, {$firstLocation->state}";
                }

                $this->load->model('business_language_model');
                $content['customerLanguage'] =  $this->business_language_model->get_by_primary_key($this->customer->default_business_language_id);
                break;
            case 2:
                $view = 'locations_index_v2';
                $this->template->add_js('https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false'.$maps_api_key.'','source');
                $this->template->add_js("/js/keydragzoom_packed.js");
                break;
            default:
                $view = 'locations_index';
                $this->template->add_js('https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false'.$maps_api_key.'','source');
                $this->template->add_js("/js/keydragzoom_packed.js");
                break;
        }

        $this->renderWide($view, $content);
    }

    /**
     * Show a message when there is only one location for the business
     *
     * If a business only has one location, the customer is redirected to
     * this page when selecting a location.
     */
    public function no_edit()
    {
        $maps_api_key = get_business_meta($this->business_id, 'maps_api_key');
        $maps_api_key = empty($maps_api_key)?"":"&key=".$maps_api_key;
        $content['maps_api_key'] = $maps_api_key;
        
        $this->load->model('customer_location_model');
        $current_locations = $this->customer_location_model->simple_array($this->customer_id, $this->business_id);

        if (sizeof($current_locations)>0) {
            foreach ($current_locations as $k) {
                $options['customer_id'] = $this->customer_id;
                $options['location_id'] = $k;
                $this->customer_location_model->delete($options);
            }

        } else {
            //get the business locations
            $options['business_id'] = $this->business_id;
            $options['select'] = '(SELECT name FROM locationType WHERE locationTypeID = locationType_id) as name,locationType_id,address,lat,lon,locationID';
            $locations = $this->location_model->get($options);

            //set the location to the business location and then redirect
            $this->customer_location_model->customer_id = $this->customer_id;
            $this->customer_location_model->location_id = $locations[0]->locationID;

            //TODO: get rid of this die()
            if($this->customer_location_model->insert())
            redirect($this->uri->uri_string());
            else
            die("Something went wrong");
        }

        //sets the active locations for this business
        //$options['locationID'] = $current_locations;
        $options=array();
        $options['business_id'] = $this->business_id;
        $options['select'] = '(SELECT name FROM locationType WHERE locationTypeID = locationType_id) as name,locationType_id,address,lat,lon,locationID';
        $locations = $this->location_model->get($options);
        $content['json_locations'] = json_encode($locations);
                $content['locations'] = $locations;

        //print_r($current_locations);
        $this->template->add_js('https://maps.googleapis.com/maps/api/js?sensor=false' . $maps_api_key,'source');
                $this->template->add_js("/js/keydragzoom_packed.js");

        $this->data['content'] = $this->load->view($this->viewFileLocation.'locations_no_edit', $content, true);
                $this->template->write_view('content', $this->viewWideTemplate, $this->data);
                $this->template->render();
    }

    /**
     * Adss a location to the user account via AJAX
     */
    public function add_customer_location_ajax()
    {
       $this->load->model('location_model');
       $options['location_id'] = $this->input->post( 'locationID' );
       $options['customer_id'] = $this->customer_id;
       if ($id = $this->location_model->add_customer_location($options)) {
           $this->load->model('customer_model');

           $this->customer_model->options['location_id'] = $this->input->post("locationID");
           $this->customer_model->options['where'] = array("customerID" => $this->customer_id, "business_id" => $this->business_id);
           $this->customer_model->update();

           $message = $this->_create_location_message( $options['location_id'] );
           $this->session->set_flashdata("location_add_message", $message);
           $res = array('status'=>'success');
       } else {
           $res = array('status'=>'fail', 'message'=>'Database Error');
       }
       echo json_encode($res);
       exit;
    }

    /**
     * Removes a location from the customer account via AJAX
     */
    public function delete_customer_location_ajax()
    {
       $this->load->model('location_model');
       $options['location_id'] =  $this->input->post( 'locationID' );
       $options['customer_id'] = $this->customer_id;
       if ($id = $this->location_model->delete_customer_location($options)) {
              $msg = get_translation('location_deleted','business_account/locations',array(),"Location Deleted");
              set_alert('success', $msg);
           $res = array('status'=>'success');
       } else {
              $msg = get_translation('location_not_deleted','business_account/locations',array(),"Location was not deleted");
               set_alert('error', $msg);
           $res = array('status'=>'fail', 'message'=>'Database Error');
       }
       echo json_encode($res);
    }

    /**
     * Adds a home pickup/delivery location to the customer locations
     *
     * This enables the user to schedule a home pickup/delivery
     */
    public function setup_home_delivery()
    {
        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
        $pickup_locations = $homeDeliveryService->getLocationAvailablesForCustomer($this->customer);

        if ($pickup_locations) {
            $options = array();
            $options['customer_id'] = $this->customer_id;
            $options['location_id'] = $pickup_locations[0]->locationID;

            $added_id = $this->location_model->add_customer_location($options);

            $message = get_translation('home_pickup_added', 'locations/setup_home_delivery',
                array(), "Home delivery location added", $this->customer_id);
            set_alert('success', $message);
            redirect("/account/orders/new_order");
        }

        $message = get_translation('home_pickup_not_added', 'locations/setup_home_delivery',
            array(), "Sorry, could not find any home pickup location near your address", $this->customer_id);
        set_alert('error', $message);
        redirect("/account/locations/");
    }

    //
    // THESE ARE THE ASYNCHRONOUS METHODS FOR PULLING LOCATION JSON!
    // Most of these are setup to return a tree for cities/neighborhoods/locations
    // for client side filtering/searching

    public function searchLocationsJSON()
    {
        $search_term = $this->input->get_post('term');
        $search_public = $this->input->get_post('public');
        $search_private = $this->input->get_post('private');

        if ( !empty( $search_public ) ) {
            $search_public = 'yes';
        } else {
            $search_public = '';
        }

        $radius = $this->input->get_post('radius');
        if (empty($radius)) {
           $radius = 1.2;
        }
        
        $this->load->library('units');
        
        $private_radius = get_business_meta($this->business_id, 'private_location_radius', 2000);
        $private_radius_unit = get_business_meta($this->business_id, 'private_location_radius_unit', Units::DISTANCE_FEET);
        $private_radius_in_miles = $this->units->convert_distance($private_radius, $private_radius_unit, Units::DISTANCE_MILES);

        $this->load->library('locationslogic');
        $search_attempt = $this->locationslogic->location_search($search_term,$this->business_id);

        //if we found something for the location
        $search_output = array();
        if ($search_attempt['status'] == 1) {
            $lat = $search_attempt['result']['lat'];
            $lon = $search_attempt['result']['lon'];

            if ($search_public && $search_private) {
                $search_output = array();
                foreach ($this->locationslogic->public_locations($this->business_id) as $location) {
                    $search_output[$location->locationID] = (object) array(
                        'locationID' => $location->locationID,
                        'distance' => distance($lat, $lon, $location->lat, $location->lon, 'm'),
                    );
                }

                $location_in_radius = $this->locationslogic->radius_search($lat, $lon, $private_radius_in_miles, $this->business_id);

                foreach ($location_in_radius as $location) {
                    $search_output[$location->locationID] = $location;
                }

                $distance_msg = " including private locations up to $private_radius $private_radius_unit ($private_radius_in_miles miles)";
        
            } else {
                $search_output = $this->locationslogic->radius_search( $search_attempt['result']['lat'] , $search_attempt['result']['lon'] , $radius, $this->business_id, $search_public);
            }

            usort($search_output, function($a, $b) {
                return floatval($a->distance) > floatval($b->distance) ? 1 : -1;
            });
        }
        
        if ( empty( $search_output ) ) {
            $output_response = array(
                'status'  => 0,
                'message' => 'No results found for '. $search_term . $distance_msg,
                'results' => array(),
                'geo' => array('lat' => $search_attempt['result']['lat'], 'lon' => $search_attempt['result']['lon'])
            );
        } else {
            $output_response = array(
                'status' => 1,
                'message' => 'Found ' . count( $search_output ) . ' results for ' . $search_term . $distance_msg,
                'results' => $search_output,
                'geo' => array('lat' => $search_attempt['result']['lat'], 'lon' => $search_attempt['result']['lon'])
            );
        }

        //header("Content-Type: application/json");
        echo json_encode( $output_response );
        exit;
    }

    public function businessNeighborhoodsJSON()
    {
        $this->load->library('locationslogic');

        $client_formatted_neighborhoods = $this->locationslogic->business_neighborhoods( $this->business_id );
        $businessLocationNeighborhoods  = json_encode( $client_formatted_neighborhoods );
        echo $businessLocationNeighborhoods;
        exit;
    }

    public function cityLocationsJSON()
    {
        $this->load->library('locationslogic');

        $client_formatted_locations = $this->locationslogic->city_locations( $this->business_id );
        $cityLocations = json_encode( $client_formatted_locations );
        echo $cityLocations;
        exit;
    }

    public function locationsJSON()
    {
        $this->load->library('locationslogic');

        $locations = $this->locationslogic->locations( $this->business_id );
        $output = $this->_setInfoBoxImages($locations);
        $json_locations = json_encode($output);

        echo $json_locations;
        exit;
    }

    public function neighborhoodsJSON()
    {
        $this->load->library('locationslogic');

        $client_formatted_neighborhood_keys = $this->locationslogic->neighborhoods( $this->business_id );
        echo json_encode( $client_formatted_neighborhood_keys );
        exit;
    }

    public function radius_searchJSON()
    {
        $this->load->library('locationslogic');

        $lat      = $this->input->get_post('lat');
        $lon      = $this->input->get_post('lon');
        $distance_in_miles = $this->input->get_post('distance');
        $public   = $this->input->get_post('public');
        $private  = $this->input->get_post('public');

        $this->load->library('units');
        
        $private_radius = get_business_meta($this->business_id, 'private_location_radius', 2000);
        $private_radius_unit = get_business_meta($this->business_id, 'private_location_radius_unit', Units::DISTANCE_FEET);
        $private_radius_in_miles = $this->units->convert_distance($private_radius, $private_radius_unit, Units::DISTANCE_MILES);

        if ( !empty( $lat ) && !empty( $lon ) ) {

            //hack for geo issue for youngs
            if ( in_bizzie_mode() && 11 == $this->business_id ) {
                $distance_in_miles = 1;  //set this to a mile
            }

            if ($public && $private) {
                $locations_in_radius = array();
                
                $this->load->library('units');
                $location_distance_display_unit = get_current_business_meta('location_distance_display_unit', Units::DISTANCE_MILES);
                $location_distance_display_unit_label = $this->units->get_distance_unit_label($location_distance_display_unit);

                foreach ($this->locationslogic->public_locations($this->business_id) as $location) {
                    $distance_in_miles = distance($lat, $lon, $location->lat, $location->lon, 'm');
                    
                    $locations_in_radius[$location->locationID] = (object) array(
                        'locationID' => $location->locationID,
                        'distance_in_miles' => $distance_in_miles,
                        'distance' => $this->units->convert_distance($distance_in_miles, Units::DISTANCE_MILES, $location_distance_display_unit),
                        'distance_unit' => $location_distance_display_unit,
                        'distance_unit_label' => $location_distance_display_unit_label,
                    );
                }

                foreach ($this->locationslogic->radius_search($lat, $lon, $private_radius_in_miles, $this->business_id) as $location) {
                    $locations_in_radius[$location->locationID] = $location;
                }

                $distance_msg = " including private locations up to $private_radius $private_radius_unit ($private_radius_in_miles miles)";
        
            } else {
                $locations_in_radius = $this->locationslogic->radius_search( $lat , $lon , $distance_in_miles, $this->business_id, $public );
            }

            usort($locations_in_radius, function($a, $b) {
                return floatval($a->distance) > floatval($b->distance) ? 1 : -1;
            });

        } else {
            $locations_in_radius = array();
        }

        echo json_encode( array( 'results' => $locations_in_radius , 'msg' => $distance_msg) );
        exit;

    }

    /**
     * Set the image for the location
     *
     * This image is displayed when the user clicks on the map icon
     * The function takes an array of functions and adds the image to the array
     * then returns the array
     *
     * @param array $locations
     * @return array
     */
    protected function _setInfoBoxImages($locations)
    {
        $is_bizzie = in_bizzie_mode();

        foreach ($locations as $key => $location) {
            $filename = $_SERVER['DOCUMENT_ROOT'].'/images/location_'.$location->locationID.'.jpg';
            if (file_exists($filename)) {
                $img = getimagesize($filename);
                $locations[$key]->image = array(
                    'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/images/location_'.$location->locationID.'.jpg',
                    'width' => $img[0],
                    'height' => $img[1],
                );
            }
            $locations[$key]->bizzie = $is_bizzie;
        }

        return $locations;
    }

    /**
     * Creates the message when the customer adds a location to their account
     *
     * This message is dependent on the locker and location types.
     *
     * @param int $location_id
     * @throws Exception if missing location_id argument
     * @return string
     */
    public function _create_location_message($location_id)
    {
        if (!isset($location_id)) {
            throw new Exception('Missing location_id when trying to make the location message');
        }

        // get the stdClass location object
        $location = $this->location_model->get(array('select'=>'serviceType, quarter, address, name','with_location_type'=>true,'locationID'=>$location_id));

        //FIXME: hardcoded locationType name
        //FIXME: needs translation
        $locationName = ($location[0]->name=='Kiosk')?'retail':$location[0]->name;

        // some locations have cutouff times to dropoff
        $cutOff = date("ga", strtotime(get_business_meta($this->business_id, 'cutOff')));
        $message = '<p class="alert alert-info" style="margin-top:10px">'.$location[0]->address.' is a '.$locationName.' location';

        //FIXME: hardcoded locationType name
        //FIXME: needs translation

        // some location require a quarter to the get the key
        $quarter = ($location[0]->quarter == 1)?" and <u>requires ONE QUARTER.</u>":"";
        switch ($location[0]->name) {
            case "Lockers":
                $message .= "Please make sure you <u>place your clothes in the locker BEFORE</u> you place your order online";
                break;

            case "Kiosk":
                $message .= "Please make sure you <u>place your clothes in the locker BEFORE</u> you place your order online";
                break;

            case "Corner Store":
                $message .= "Please leave your order with the front counter and <u>do not take the key</u> to the locker";
                break;
        }
        $message .= ". ";
        $message .= "Service at this location is <u>{$location[0]->serviceType}</u>, which means ";
        $message .= ($location[0]->serviceType == 'daily')?'we come there every day, Monday thru Friday. Orders dropped off before '.$cutOff.' will be picked up that day':'you need to place an order to schedule a pickup. Orders placed before 9am will be picked up that day';
        $message .= ".</p>";

        return $message;
    }

}