<?php
use App\Libraries\DroplockerObjects\Business;

class sms Extends CI_Controller
{
    private $sms_keyword;

    public $business_support_email;
    public $business_support_from_name;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
        $this->sms_keyword = \App\Libraries\DroplockerObjects\Server_Meta::get("sms_keyword");
        if (empty($this->sms_keyword)) {
            $this->sms_keyword = "LAUNDRY";
        }
    }

    /**
     * THIS IS WHERE NEXMO HITS
     * There is a different flow for NEXMO than twilio, as the responses have to be sent through
     * the API instead of a XML response, etc.
     *
     * Nexmo GET vars:
     *
     * - type
     * - to
     * - msisdn
     * - network-code
     * - messageId
     * - message-timestamp
     * - text
     */
    public function nexmo_claim()
    {
        // Nexmo is hitting us with a "Delivery Receipt"
        // the account is misconfigured
        if (!isset($_GET['text'])) {
            return;
        }

        $business_number = $this->input->get('to');
        if (strpos($business_number, '%') !== false) {
            $business_number = urldecode($business_number);
        }

        $business_settings = $this->get_provider_settings($business_number);

        //this should be a try/catch
        if (empty($business_settings->business_id)) {
           $this->report_provider_not_found($business_number);
           return;
        }

        $sms_body        = $this->input->get('text');
        $customer_number = $this->input->get('msisdn');
        $sms_number      = clean_phone_number($customer_number);

        //send a copy of this SMS to the businesses support email
        $this->send_copy_to_customer_support($customer_number, $sms_body, $business_settings->business_id);

        //load the sms_claim library
        $this->load->library('nexmo_claim', array(
            'business_id' => $business_settings->business_id,
            'sms_keyword' => $this->sms_keyword, //TODO: why not use $business_settings->keyword
            'business_sms_number' => $business_number,
        ));

        // process the incoming message
        $this->nexmo_claim->process_message($sms_number, $sms_body);
    }

    /**
     * THIS IS WHERE TWILIO HITS
     */
    public function twilio_claim()
    {
        //if getting twilio response, this will be business number
        $business_number = $this->input->get('To');

        if (strpos($business_number, '%') !== false) {
            $business_number = urldecode($business_number);
        }

        //check to make sure no extra space
        $business_number = trim($business_number);

        if (strpos($business_number, '+') === false) {
            $business_number = '+' . $business_number;
        }

        $business_settings = $this->get_provider_settings($business_number);

        //this should be a try/catch
        if (empty($business_settings->business_id)) {
           $this->report_provider_not_found($business_number);
           return;
        }

        //send a copy of this SMS to the businesses support email
        $sms_body        = $this->input->get("Body");
        $customer_number = $this->input->get("From");
        $sms_number      = clean_phone_number($customer_number);
        $sms_number      = ltrim($sms_number, '1');

        //send a copy of this SMS to the businesses support email
        $this->send_copy_to_customer_support($customer_number, $sms_body, $business_settings->business_id);

        //load the sms_claim library
        $this->load->library('twilio_claim', array(
            'business_id' => $business_settings->business_id,
            'sms_keyword' => $business_settings->keyword,
            'business_sms_number' => $business_number
        ));

        // process the incoming message
        $this->twilio_claim->process_message($sms_number, $sms_body);

        // log the SMS message to email history
        $this->twilio_claim->logHistory($sms_number, $sms_body);

    }

    /**
     * Sends a copy of the sms message to customer support
     *
     * @param string $customer_number
     * @param string $sms_body
     * @param int $business_id
     */
    protected function send_copy_to_customer_support($customer_number, $sms_body, $business_id)
    {
        $this_is_sms_copy_reminder = "<p style='color:red;font-size:32px;'>This is a copy of a SMS do not respond to this email.</p>";

        send_admin_notification(
            "Incoming Customer SMS",
            "SMS FROM: " . $customer_number . "<br>\n\n" . $sms_body . "<br>\n\n" . $this_is_sms_copy_reminder,
            $business_id
        );
    }

    /**
     * Retrieves the business sms settings using the phone number
     *
     * @param string $number
     * @return object
     */
    protected function get_provider_settings($number)
    {
        $number_query = $this->db->get_where('smsSettings', array('number' => $number));
        $business_sms_settings = $number_query->row();

        return $business_sms_settings;
    }


    /**
     * Notifies the developers about a sms for a number not setup
     *
     * @param string $business_number
     */
    protected function report_provider_not_found($business_number)
    {
        $attempted_sms_data     = $_GET;
        $error_string_from_sms  = '';
        $business_search_query  = $this->db->last_query();

        //create the string
        foreach ($attempted_sms_data as $key => $val) {
            $error_string_from_sms .= $key . ': ' . $val . "\n\n";
        }

        send_development_email(
            "SMS Claim Error",
            "<pre>
                We could not find the business associated with this SMS claim attempt:
                " . $error_string_from_sms . " " . $business_number . "
                " . $business_search_query . "
            </pre>"
        );
    }


    /**
     * Takes the body of the message and return an array of it's details
     *
     * @param array sms_body
     * @return array
     */
    protected function parse_sms_body($sms_body)
    {
        //decode incase for somereason the body is escaped
        $sms_body = urldecode($sms_body);
        $sms_body = trim($sms_body);

        $parsed_output = array(
            'email' => null,
            'locker' => null,
            'action' => null,
            'customer_sms_number' => null,
            'original_message' => $sms_body
        );

        if (empty($sms_body)) {
            $parsed_output['action'] = 'error';

            return $parsed_output;
        }

        // is there is a valid email address ßwe should mark this as an attempt to link an account
        $emails = array();
        if (preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+[_a-zA-Z0-9-]/i", $sms_body, $emails)) {

            $email_address = $emails[0][0];
            if ($email_address) {
                $parsed_output['email']  = $email_address;
                $parsed_output['action'] = 'link_email';
            }

            return $parsed_output;
        }

        // if we don't have an email in the message, customer is trying to place an order
        $parsed_output['action'] = 'place_order';

        // try to find the locker number in the message
        // any digits appearing correspond to the order, lockerName
        $parsed_output['locker'] = preg_replace("/[^0-9]/", '', $sms_body);
        $match = array();
        if (preg_match_all('/([0-9]+)/', $sms_body, $match)) {
            $parsed_output['locker'] = $match[1][0];
        }

        return $parsed_output;
    }


    /** This action recies a text message from MOSIO . **/
    public function create_claim()
    {

        // from table serviceType, serviceTypeID 11 = SMS
        $smsOrderType = serialize(array(11));
        try {
            /** Example POST Request string from MOSIO:
            * JSON={"id": 18432, "message" : "Locker 07", "date" : "2012-05-01", "carrier" : "ATT", "carrierId" : 31007, "phone" : "19079784496", "operator_name" : "natawski", "operator_id" : 13225}
            */

            $json_data = json_decode($this->input->post("JSON"));

            $this->load->model("locker_model");

            if (!$json_data) {
                throw new Exception("The POST parameter 'JSON' does not contain a valid JSON structure: " . var_export($_POST, true));
            }
            //The following conditionals check to make sure the expec___ted parameters were set in the POST request.
            if (!isset($json_data->phone)) {
                throw new Exception("Missing 'phone' property in JSON object.");
            } elseif (!isset($json_data->id)) {
                throw new Exception("Missing 'id' property in JSON object.");
            } elseif (!isset($json_data->message)) {
                throw new Exception("Missing 'message' property in JSON object.");
            } elseif (!isset($json_data->operator_name)) {
                throw new Exception("Missing 'operator_name' property in JSON object.");
            } elseif (!isset($json_data->operator_id)) {
                throw new Exception("Missing 'operator_id' property in JSON object.");
            } else {
                //MOSIO sens the phone number with a 1 in the front but to email out a text number, you need to have no 1, so we store it that way
                $smsNumber = substr($json_data->phone, 1);

                $smsId = $json_data->id; //This is the unique ID for the text message.
                $message = $json_data->message; //This is the user specified content.
                $date = $json_data->date; //This is the timestamp of the text message.
                $carrier = $json_data->operator_name; //This is the name of the cell phone provider.
                $carrierId = $json_data->operator_id; //This is the ID of the cell phone provider.

                //if any numbers are sent in the message, that is the locker number
                $locker = preg_replace("/[^0-9]/", '', $message);

                $email = false;

                //if the text contains an email, link the account
                $emails = array();
                if (preg_match( "/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+[_a-zA-Z0-9-]/i", $message, $emails)) {
                    $email = $emails[0];
                }

                if ($email) {

                    $this->load->model('customer_model');

                    //try to find this email
                    $result = $this->customer_model->get_aprax(array("email" => $email));

                    if ($result) {
                        //lookup carrier email
                        switch ($carrierId) {
                            case '31002': $smsValue = $smsNumber . '@txt.att.net'; break; //AT&T Wireless
                            case '31009': $smsValue = $smsNumber . '@message.alltel.com'; break; //Alltel
                            //case '': $smsValue = $smsNumber . '@mymetroPCS.com'; break; //Metro PCS NOT SUPPORTED
                            case '31007': $smsValue = $smsNumber . '@messaging.nextel.com'; break; //Nextel
                            case '31005': $smsValue = $smsNumber . '@messaging.sprintpcs.com'; break; //Sprint
                            case '31004': $smsValue = $smsNumber . '@tmomail.net'; break; //T-Mobile
                            case '31003': $smsValue = $smsNumber . '@vtext.com'; break; //Verizon
                            case '31010': $smsValue = $smsNumber . '@vmobl.com'; break; //Virgin
                            case '31008': $smsValue = $smsNumber . '@myboostmobile.com'; break; //Boost
                            case '31011': $smsValue = $smsNumber . '@myboostmobile.com'; break; //Boost Unlimited
                            case '31012': $smsValue = $smsNumber . '@email.uscc.net'; break; //US Cellular
                            default:
                                $smsValue = $smsNumber . '@txt.att.net';
                                break;
                        }

                        //add this SMS number to this customer's account
                        $update = $this->db->query("update customer set sms = ? where email= ?", array($smsValue, $email));

                        //respond that they can now place an order
                        $response = "SMS number: $smsNumber has been linked to acccount: $email. You may now place your order by texting the word {$this->sms_keyword}";
                    } else {
                        $response = 'Sorry, we could not find an account with the email: ['.$email.'].  Please try again or contact customer support.';
                        send_email($this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                Phone Number: $smsNumber
                                Message: $message
                        $response </pre>");
                    }
                } else {
                    //find out if we have this sms number on file

                    $customers_query = $this->db->query("SELECT * FROM customer WHERE (sms LIKE ? OR sms2 LIKE ?)", array("%$smsNumber%", "%$smsNumber%"));
                    $customers = $customers_query->result();
                    if (!$customers) {
                        $response = "Sorry, this cell phone is not linked to your account.  Please text: {$this->sms_keyword} EMAIL [your email].  e.g. {$this->sms_keyword} EMAIL TSMITH@GMAIL.COM '$smsNumber'";
                        $this->db->insert("orderIncoming", array("content" => $message, "fromAddr" => $json_data->phone));
                        send_email($this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                Phone Number: $smsNumber
                                Message: $message
                        $response </pre>");
                    } else {
                        $this->load->model('claim_model');

                        $customer = $customers[0];

                        //The following statements determine which business the customer is associated with.
                        $businesses = new Business($customer->business_id);
                        $this->business = $businesses;

                        $this->business_support_email = get_business_meta( $this->business->businessID, 'customerSupport');
                        $this->business_support_from_email    = get_business_meta(  $this->business->businessID, 'from_email_name');

                        $get_location_method_query = $this->db->query("SELECT locationType.lockerType, location.address, location.locationID FROM customer
                            INNER JOIN location ON location.locationID = customer.location_id
                            INNER JOIN locationType ON locationType.locationTypeID = location.locationType_id
                            WHERE customerID = {$customer->customerID}");
                        if ($this->db->_error_message()) {
                            throw new Exception($this->db->_error_message());
                        }

                        //
                        //

                        $location_methods = $get_location_method_query->result();

                        //The following conditional checks to see if the customer has a default location set up. I
                        if ($location_methods) {
                            $location_method = $location_methods[0];
                            //The following conditionla checks if the lockerType of the location is "lockers", if so, then we process the locker number.
                            //otherwise, we process the claim without a locker number since any other location type has no lockers.
                            if ($location_method->lockerType == "lockers") { //This can be a location type of "Lockers" or "Kiosk".
                                //if the customer did not specify a locker number and the location has lockers, then we need to inform the customer to send the locker number.
                                if (empty($locker)) {
                                    $short_address = character_limiter( $location_method->address, 20 );
                                    $response = "Your default location ({$short_address}) has lockers so we need you to text us the locker number. (e.g. 'Laundry 452')";
                                    send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                                    Phone Number: $smsNumber
                                                    Message: $message
                                            $response </pre>");
                                } else {
                                    $get_locker_query = $this->db->query("SELECT * FROM locker WHERE location_id = {$location_method->locationID} AND lockerName=$locker");
                                    $lockers = $get_locker_query->result();

                                    //If the locker exists in this location, then we try to create a claim.
                                    if ($lockers) {
                                        $locker = $lockers[0];
                                        if ($this->claim_model->does_claim_exist($locker->lockerID, $customer->business_id)) {
                                            $short_address = character_limiter( $location_method->address, 20 );
                                            $response = "The locker {$locker->lockerName} at location {$short_address} has already been claimed, please choose another locker.";
                                            send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                                    Phone Number: $smsNumber
                                                    Message: $message
                                            $response </pre>");
                                        } else {
                                            // This will set the response paramter in case of error
                                            if ($this->checkOrderLimit($customer->customerID, $customer->business_id, $response)) {
                                                $short_address = character_limiter( $location_method->address, 20 );
                                                $claim_id = $this->claim_model->new_claim(array("customer_id" => $customer->customerID, "locker_id" => $locker->lockerID, "business_id" => $customer->business_id, "orderType" => $smsOrderType));
                                                $response = "Hi " .  getPersonName($customer) . ", your claim was successfully placed in locker {$locker->lockerName} at location {$short_address}.";
                                                $action_array = array('do_action' => 'new_claim', 'customer_id'=>$customer->customerID,'claim_id'=>$claim_id, 'business_id'=>$customer->business_id);
                                                try {
                                                    Email_Actions::send_transaction_emails($action_array);
                                                } catch (Exception $e) { //If sending the new claim e-mail fails, we still want to continue processing the order normally.
                                                    send_exception_report($e, "Customer SMS Number: {$smsNumber}. Customer ID: {$customer->customerID}");
                                                }
                                            } else {
                                                send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                                    Phone Number: $smsNumber
                                                    Message: $message
                                                $response </pre>");
                                            }
                                        }
                                    } else {
                                        $short_address = character_limiter( $location_method->address, 20 );
                                        $response = "Sorry, we could not find Locker {$locker} at {$short_address}.  Please place your order at {$this->business->website_url} or call {$this->business->phone}.  Thanks!";
                                        send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email,"SMS Claim Error", "<pre>
                                                Phone Number: $smsNumber
                                                Message: $message
                                        $response </pre>");
                                    }

                                }

                            } else { //Home delivery, office, concierge, or units
                                //Note, we deterine the right locker and location by the last order placed by the customer.
                                $this->db->select("locker_id");
                                $this->db->where(array("customer_id" => $customer->customerID, "closedGross !=" => "0.00"));
                                $this->db->limit(1);
                                $this->db->order_by("dateCreated", "DESC");
                                $get_last_order_query = $this->db->get("orders");

                                $last_order = $get_last_order_query->row();

                                //The following conditional retrieves the locker for which the last order that hte customer placed in. If the customer never placed an order, then an error message is sent back to the customer stating that the first need to place an order first.
                                if ($last_order) {
                                    $this->db->select("lockerID, location_id, lockerName");
                                    $get_last_locker_query = $this->db->get_where("locker", array("lockerID" => $last_order->locker_id));

                                    $last_locker = $get_last_locker_query->row();

                                    $this->db->select("locationID, address");
                                    $get_last_location_query = $this->db->get_where("location", array("locationID" => $last_locker->location_id));

                                    $last_location = $get_last_location_query->row();

                                    $this->db->select("locationID, address");

                                    $get_default_location_query = $this->db->get_where("location", array("locationID" => $customer->location_id));

                                    $default_location = $get_default_location_query->row();

                                    // check to see if the location is still active
                                    $location = new \App\Libraries\DroplockerObjects\Location($default_location->locationID);
                                    if (!$location->isActive()) {
                                        $short_address = character_limiter($location->address, 20);
                                        $response = "Sorry, but the location [{$short_address}] is not active. Please call {$this->business->phone} or go to {$this->business->website_url} to place your claim. ";

                                    } else {

                                        //The following conditional checks to see if the location of the last order matches the customer's default location.
                                        if ($last_location->locationID == $customer->location_id) {
                                            try {
                                                // This will set the response paramter in case of error
                                                if ($this->checkOrderLimit($customer->customerID, $customer->business_id, $response)) {
                                                    $claim_id = $this->claim_model->new_claim(array("customer_id" => $customer->customerID, "locker_id" => $last_locker->lockerID, "business_id" => $customer->business_id, "orderType" => $smsOrderType));
                                                    if (!$claim_id) {
                                                        throw new Exception("No claim ID returned when creating claim.");
                                                    }
                                                    $short_address = character_limiter($last_location->address, 20);
                                                    $response = "Hi " .  getPersonName($customer) . ", your claim was successfully placed in {$last_locker->lockerName} at location {$short_address}.";
                                                } else {
                                                    send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                                    Phone Number: $smsNumber
                                                    Message: $message
                                                    $response </pre>");
                                                }
                                            } catch (Exception $e) {
                                                $response = "Sorry, an error ocurred creating your claim. Please call {$this->business->phone} or go to {$this->business->website_url} to place your claim. ";
                                                send_exception_report($e);
                                            }
                                        } else {
                                            $short_address      = character_limiter( $default_location->address, 20 );
                                            $last_short_address = character_limiter( $last_location->address, 20 );

                                            $response = "Your default location ('{$short_address}') does not match the location of your last order ({$last_short_address}).  Please place your order at {$this->business->website_url}";
                                            //support email
                                            send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                                Phone Number: $smsNumber
                                                Message: $message
                                                $response </pre>");
                                        }
                                    }
                                } else {
                                    $response = "Unfortunately we can't take this order via SMS.  Please place your order at {$this->business->website_url} or call {$this->business->phone}.  Thanks!'";
                                    send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                            Phone Number: $smsNumber
                                            Message: $message
                                    $response </pre>");
                                }
                            }
                        } else {
                            $response = "You don't have a default location set up yet. Please place an order at {$this->business->website_url} or call {$this->business->phone}. Thanks!";
                            send_email( $this->business_support_email, $this->business_support_from_email, $this->business_support_email, "SMS Claim Error", "<pre>
                                    Phone Number: $smsNumber
                                    Message: $message
                            $response </pre>");
                        }


                    }
                }

            }
        } //End of try statement
        catch (Exception $e) {
            $response = "An error occurred when creating the claim.";
            send_exception_report($e);
        }

        $this->db->insert("orderIncoming", array( "content"  => $message,
                                                  "fromAddr" => $smsNumber,
                                                  "response" => $response ) );

        $output_data = array('username' => 'laundrylocker', 'password' => 'wcFI8hLOM9fOA06MMeJ0', 'message' => $response);
        echo json_encode($output_data);


    } //End of function

    /**
     * Checks that the customer has not exceeded the claims limit
     *
     * In case of error, this function will set an localized error message
     * in the $response paramteter.
     *
     * @param int $customer_id
     * @param int $business_id
     * @param string $response OUT: the localized error message
     * @return boolean True if client has not exceeded the claims limit
     */
    protected function checkOrderLimit($customer_id, $business_id, &$response = null)
    {
        return \App\Libraries\DroplockerObjects\Claim::checkOrderLimit($customer_id, $business_id, $response);
    }

    /** This action recieves a text message from bizzie . **/
    public function bizzie_claim()
    {
        //relevant $_GET vars
        $sms_data = $this->input->get();

        if (strpos($sms_data['From'], '%') !== false) {
            $sms_data['From'] = urldecode($sms_data['From']);
        }

        if (strpos($sms_data['To'], '%') !== false) {
            $sms_data['To'] = urldecode($sms_data['To']);
        }

        $sms_output = '';
        $customer = $this->customer_sms_lookup( $sms_data['From'] );    //try to find the customer from that sms number

        // this might happen multiple times, but we should try to load up the library with the
        // specific business idea as early on as possible, so error handling and responses are automatic
        $parsed_body = $this->parse_sms_body( $sms_data['Body'] );

        switch ($parsed_body['action']) {
            case 'place_order':     //got a locker number attempt to make a claim if we have a customer

                if ( !empty( $customer->business_id ) ) { //we have a customer and business_id
                    $this->load->library( 'Twilio_claim', array('business_id' => $customer->business_id ) );  //load the new library
                    $this->twilio_claim->attempt_sms_order( $sms_data['From'], $parsed_body );
                } else {
                   //SMS NUMBER NOT ON FILE, SEND MESSAGE BACK TELLING THEM WHAT TO DO
                    $response = get_translation("phone_not_linked_to_account_error", "SMS Claim", array(),  "Sorry, this phone is not linked to your account. To activate it, please text us your email address.");
                    $this->send_process_response( $sms_data['From'], $parsed_body, $response, $sms_data['To']  );
                }
                break;

            case 'link_email':
                $customer = $this->customer_email_lookup( $parsed_body['email'] );  //quick lookup for the email, because we need a business_id

                if ( !empty( $customer->business_id ) ) { //we found a customer from that email and a business id
                    //load the sms_claim library
                    $this->load->library( 'Twilio_claim', array('business_id' => $customer->business_id ) );  //load the library and try to connect
                    $this->twilio_claim->attempt_link_email( $sms_data['From'], $parsed_body );

                } else {
                    //error response here, NO CREATE ALLOWED - new lang here, or need to make sure
                    $response = get_translation("link_email_failure", "SMS Claim", array("email" => $parsed_body['email'] ), 'Sorry, we could not find an account for %email%.');
                    $this->send_process_response( $sms_data['From'], $parsed_body, $response, $sms_data['To'] );
                    exit;
                }
                break;

            default:
                $response = "There was a problem placing your SMS order";   //failsafe, we need to let SOMEONE know that there is no relevant business ID or data
                $this->send_process_response(  $sms_data['From'], $parsed_body, $response, $sms_data['To']);
                break;
        }



    } //End of function

    /**
     * This is for the bizzie claim, since we need to cross ref the email first
     * @param string $email
     * @return mixed
     */
    protected function customer_email_lookup($email)
    {
        $this->db->select('customerID,business_id');
        $this->db->like( "email", $email );
        $this->db->order_by('lastUpdated','DESC');
        $customer_email_query = $this->db->get('customer');
        $customer_email_attempt = $customer_email_query->row();
        if (!empty($customer_email_attempt)) {
            return $customer_email_attempt;
        } else {
            return false;
        }
    }

    protected function customer_sms_lookup($sms_number)
    {
        $this->db->select('customerID,business_id');
        $this->db->like("sms", $sms_number);
        $this->db->or_like("sms2", $sms_number);
        $customer_sms_query = $this->db->get('customer');
        $customer_sms_attempt = $customer_sms_query->row();
        if (!empty($customer_sms_attempt)) {
            return $customer_sms_attempt;
        } else {
            return false;
        }
    }

    protected function send_process_response($sms_number, $message_data, $response = '', $business_sms_number)
    {
        $this->db->insert("orderIncoming", array( "content"  => $message_data['original_message'],
                                                  "fromAddr" => $sms_number,
                                                  "response" => $response ) );

        //this is TwiML - how twilio handles automated SMS responses when our endpoint is set
        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<Response>';
        $output .= '<Message>' . $response . '</Message>';
        $output .= '</Response>';

        echo $output;
        exit;

    }
}
