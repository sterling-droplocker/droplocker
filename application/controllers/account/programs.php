<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\Coupon;

class Programs extends MY_Account_Controller
{
    /**
     * @var LockerLootConfig
     */
    public $rewards_config;
    public $lockerLootEndPoint;
    public $promo_code;

    public function __construct()
    {
        parent::__construct();

        $this->requireLogin();

        // promo codes
        $this->promo_code = "CR".$this->customer_id;
        $this->lockerLootEndPoint = get_full_url('/register?promo='.$this->promo_code);

        //config for rewards
        $this->rewards_config = $this->getLockerLootConfig();
        $this->content['lockerLootEndPoint'] = $this->lockerLootEndPoint;

        // update the bitly url
        if ($this->rewards_config) {
            $bitly = file_get_contents("https://api-ssl.bitly.com/v3/shorten?access_token=93b1c94462f53b28a66e4f48411b476a2dd84bdd&longUrl=".urlencode($this->lockerLootEndPoint));
            $result = json_decode($bitly);
            $this->rewards_config->bitly = $result->data->url;
            $this->rewards_config->save();
        }

        $this->template->add_js('js/account/profile/fix_tables.js');
    }

    /**
     * Gets the business LockerLootConfig
     *
     * @return LockerLootConfig
     */
    protected function getLockerLootConfig()
    {
        $lockerLootConfig = LockerLootConfig::search(array(
            'business_id' => $this->business_id
        ));

        return $lockerLootConfig;
    }

    /**
     * Rewards Home
     */
    public function index()
    {
        $this->rewards();
    }

    /**
     * Show UI to buy gift cards
     */
    public function gift_cards()
    {
        $this->load->model('creditcard_model');
        $card = $this->creditcard_model->get_one(array(
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
        ));

        if (!$card) {
            $message = get_translation('missing_card', 'business_account/programs_gift_cards',
                array(), "You must have a credit card on file before purchasing a gift card");

            set_alert('error', $message);
            redirect('/account/profile/billing');
        }

        $this->content['card'] = $card;

        if (!empty($_POST)) {

            if (!$this->validateGiftCard()) {
                $this->session->set_flashdata("post", serialize($_POST));
                redirect('/account/programs/gift_cards/');
            }

            // show customers card on file
            $this->content['post'] = $_POST;
            $this->content['post']['amount'] = preg_replace('/[\$,]/', '', $_POST['amount']);
            $this->content['json_customer'] = json_encode($this->customer);
            $this->renderAccount('programs_gift_cards_invoice', $this->content);
        }

        $this->content['customer_fullName'] = getPersonName($this->customer);

        if ($this->session->flashdata('post')) {
            $_POST = unserialize($this->session->flashdata('post'));
        }

        $this->renderAccount('programs_gift_cards', $this->content);
    }

    /**
     * Validates git card data
     */
    protected function validateGiftCard()
    {
        //minimum purchase price is $5.00
        $amount = preg_replace('/[\$,]/', '', $_POST['amount']);
        $minimum = 5; //TODO: this should be a business setting

        $errors = array();

        if ($amount < $minimum) {
            $errors[] = get_translation('invalid_amount', 'business_account/programs_gift_cards',
                array('minimum' => format_money_symbol($this->business_id, '%.2n', $minimum)),
                "Minimum gift card purchase is %minimum%");
        }

        if (empty($_POST['to'])) {
            $errors[] = get_translation('missing_recipient', 'business_account/programs_gift_cards',
                array(), "You must specify the recipient");
        }

        if (empty($_POST['email']) || !valid_email($_POST['email'])) {
            $errors[] = get_translation('missing_email', 'business_account/programs_gift_cards',
                array(), "You must specify a valid recipient's email address");
        }

        if ($errors) {
            $message = implode("<br>", $errors);
            set_alert("error", $message);
        }

        return empty($errors);
    }

    /**
     * Expects the following POST parameters
     *   total float the total amount of the order
     *   email string The recipient email
     *   to string The recipient's name
     */
    public function gift_card_complete_ajax()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("total", "Total", "required");
        $this->form_validation->set_rules("email", "Email", "valid_email");
        $this->form_validation->set_rules("to", "To", "required");
        $this->form_validation->set_rules("cardNumber", "Card Number", "is_natural_no_zero");

        if (!$this->form_validation->run()) {
            output_ajax_error_response(validation_errors());
        }

        $message = "";

        //load the credit card processor
        $this->load->library('creditcardprocessor');
        $processor = get_business_meta($this->business_id, 'processor');
        $payment = $this->creditcardprocessor->factory($processor, $this->business_id);

        if (DEV) {
            $payment->setTestMode();
        }

        $recipientEmail = $this->input->post('email');

        $this->load->model('creditcard_model');

        $total = $this->input->post("total");

        $amount = number_format($total, 2, '.', '');
        $notes = format_money_symbol($this->business_id, '%.2n', $amount). ' Gift Card ordered by customer on website.';
        $locker_id = $this->business->returnToOfficeLockerId;
        $bag_id = 0;

        $options['customer_id'] = $this->customer_id;
        $options['orderStatusOption_id'] = 10;
        $options['orderPaymentStatusOption_id'] = 3;

        //make an order for this giftCard
        $this->load->model('order_model');

        $order_id = $this->order_model->new_order(0, $locker_id, $bag_id, 0, $notes, $this->business_id, $options);

        try {
            $response = $payment->makeSale($total, $order_id, $this->customer_id, $this->business_id);
        } catch (Exception $e) {

            $this->order_model->delete_aprax(array(
                'orderID' => $order_id
            ));

            send_exception_report($e);

            send_admin_notification('No response from processor during gift card purchases', 'OrderID ['.$order_id.'].<br>Make sure we do not double charge this customer');

            $message = get_translation('cc_error', 'business_account/programs_gift_cards',
                array(), "No response from credit card processor. Please try again");

            output_ajax_error_response($message);
        }

        $status = $response->get_status();

        if ($status != 'SUCCESS') {
            $this->order_model->clear();
            $this->order_model->orderID = $order_id;
            $this->order_model->delete();

            $message = get_translation("payment_not_authorized", "business_account/programs_gift_cards",
                array(), "Payment was not authorized, Please check your card on file. <br><a href='/account/profile/billing'>Click here to add a new credit card</a>");
            output_ajax_error_response($message);
        }


        $cardNumber = time();
        // add coupon
        $this->load->model('coupon_model');

        if ($couponID = $this->coupon_model->new_gift_card($amount, $order_id, $cardNumber, $this->business_id, $recipientEmail, $_POST['to'], $recipientEmail)) {

            //create charge
            $sql = "INSERT INTO orderCharge (order_id, chargeType, chargeAmount)
                    VALUES ({$order_id}, 'Gift Card <a href=/admin/admin/manage_gift_card/".$couponID.">GF".$cardNumber."</a>', ".$amount.")";
            $charge_id = $this->db->query($sql);

            //insert into orderHistory
            $sql = "INSERT INTO orderStatus (order_id, orderStatusOption_id, employee_id, locker_id, visible) VALUES (?, 10, 1, ?, 1)";
            $this->db->query($sql, array($order_id, $this->business->prepayment_locker_id));

            Email_Actions::send_transaction_emails(array(
                "do_action" => 'buy_giftcard',
                "customer_id" => $this->customer_id,
                "business_id" => $this->business_id,
                "giftCard_id" => $couponID
            ));

            if ($this->input->post("email")) {
                Email_Actions::send_giftCard_recipient_email($this->customer_id, $recipientEmail, $couponID);
            }


            $message = get_translation("successful_gift_card_purchase_message", "business_account/programs_gift_cards",
                array(), "Successfully purchased gift card");

            echo json_encode(array('status' => $status, 'message'=>$message,'insert_id'=>$couponID));

        } else {
            send_admin_notification('Gift card purchased but the order was not created', "The payment was a success, but the card was not stored. Please make a coupon for the card and email the customer with the card information.<br>OrderID: {$order_id}");

            $message = get_translation("error_purchase_message", "business_account/programs_gift_cards",
                array(), "Gift card purchased but the order was not created");

            echo json_encode(array('status' => 'FAIL', 'message' => $message, 'insert_id'=> $couponID));
        }
    }

    /**
     * Shows the gift card code
     *
     * @param int $couponID
     */
    public function gift_card_complete($couponID = null)
    {
        $this->load->model('coupon_model');
        $coupon = $this->coupon_model->get_by_primary_key($couponID);
        $this->content['coupon'] = $coupon;

        $this->load->model('order_model');
        $order = $this->order_model->get_by_primary_key($coupon->order_id);

        if ($this->customer_id != $order->customer_id) {
            redirect('/account/programs/gift_cards');
        }

        $this->renderAccount('programs_gift_card_complete', $this->content);
    }

    /**
     * Deletes a pending laundry plan upgrade
     */
    public function cancel_laundryplan_upgrade()
    {
        if (!isset($_POST['submit'])) {
            redirect('/main/plans');
        }

        $this->load->model('laundryplan_model');
        $affected_rows = $this->laundryplan_model->delete_aprax(array(
            'customer_id' => $this->customer_id,
            'laundryPlanID' => $_POST['laundryplan_id'],
        ));

        if ($affected_rows) {
            $message = get_translation("plan_cancelled", "business_account/cancel_laundryplan_upgrade",
                array(), "Laundry plan upgrade has been cancelled");
            set_alert("success", $message);
        } else {
            $message = get_translation("plan_not_cancelled_error", "business_account/cancel_laundryplan_upgrade",
                array(), "Laundry plan upgrade has not been cancelled");
            set_alert("error", $message);
        }

        redirect("/main/plans");
    }

    /**
     * Upgrades a customers laundry plan
     */
    public function upgrade_plan($plan_id = null)
    {
        // get the customers current laundry plan
        $this->load->model('laundryplan_model');
        $plans = $this->laundryplan_model->get_active_laundry_plans($this->customer_id, $this->business_id, 'laundry_plan');

        $this->content['current_plan'] = $plans[0];

        if (count($plans) > 1) {
            $message = get_translation("already_have_plan_error", "business_account/programs_upgrade_plan",
                array(), "You are already set for an upgrade. Please cancel the current upgrade before proceeding");
            set_alert('error', $message);

            return redirect("/main/plans");
        }

        // get the new plan the customer wants to upgrade to
        $this->load->model('businesslaundryplan_model');
        $new_plan = $this->businesslaundryplan_model->get_one(array(
            'businessLaundryPlanID' => $plan_id,
            'business_id' => $this->business_id,
        ));

        $this->content['new_plan'] = $new_plan;

        if (isset($_POST['submit'])) {

            if ($plan_id != $_POST['new_plan_id']) {
                throw new Exception("Plan Mismatch on Posting for plan upgrade");
            }

            $this->load->model('order_model');
            $options['customer_id'] = $this->customer_id;
            $options['business_id'] = $this->business_id;
            $options['type'] = 'laundry_plan';
            $options['new_plan_id'] = $_POST['new_plan_id'];
            $laundryPlanID = $this->order_model->upgrade_laundry_plan($options);

            redirect("/main/plans");
        }

        $this->renderAccount('programs_upgrade_plan', $this->content);
        return;
    }

    /**
     * Shows the UI to buy a new laundry plan
     *
     * @param int $plan_id
     */
    public function display_buy_plan_form($plan_id = null)
    {
        /*check if customer already has a everything plan */

        $this->load->model('laundryplan_model');
        $active_everything_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'everything_plan');

        $active_product_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'product_plan');

        if ($active_everything_plan || $active_product_plan) {
            redirect("/account/programs/display_buy_plan_error");           
        }

        if (!$plan_id) {
            set_alert('error', "Unknown Plan");
            redirect("/main/plans/");
        }

        $customer = new Customer($this->customer_id);

        //check to see if customer is on invoice
        if (!$customer->invoice) {
            //check to see if there is a credit card on file
            $this->load->model('creditcard_model');
            $card = $this->creditcard_model->get_one(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
            ));

            if (!$card) {
                $message = get_translation('missing_card', 'business_account/programs_buy_plan',
                    array(), "You must have a credit card on file to purchase a laundry plan.");

                set_alert('error', $message);
                redirect('/account/profile/billing');
            }
        }

        $this->load->model('businesslaundryplan_model');
        $plan = $this->businesslaundryplan_model->get_one(array(
            'businessLaundryPlanID' => $plan_id,
            'business_id' => $this->business_id,
        ));

        $this->content['wfPrice'] = $this->laundryplan_model->getDefaultPrice($this->customer_id, $this->business_id);
        $this->content['plan'] = $plan;

        $this->renderAccount('programs_buy_plan', $this->content);
    }

    /**
     * Buys a laundry plan
     */
    public function buy_plan()
    {
        $plan_id = $this->input->post("businessLaundryPlanID");
        $this->load->model('order_model');
        $options['customer_id'] = $this->customer_id;
        $options['business_id'] = $this->business_id;
        $options['plan_id'] = $plan_id;

        //check to see if customer is on invoice
        $customer = new Customer($this->customer_id);
        if ($customer->invoice) {
            $options['orderPaymentStatusOption_id'] = 4;
        }
        

        $response = $this->order_model->new_laundry_plan_order($options);

        if ($response['status']!='success') {
            set_alert('error', $response['message']);
            redirect("/account/programs/display_buy_plan_form/");
        }

        // send the email to the customer with laundry plan details
        $action_array = array(
            'do_action' => 'new_laundryplan',
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'order_id' => $response['order_id'],
            'laundryPlan_id' => $plan_id
        );

        Email_Actions::send_transaction_emails($action_array);

        //capture order
        $inmediate_order_capture = get_business_meta($this->business_id, 'inmediate_order_capture','0');

        if($inmediate_order_capture && !$customer->invoice) {
            $this->load->model('order_model');
            $result = $this->order_model->capture($response['order_id'], $this->business_id);
        }

        set_alert('success', $response['message']);
        redirect("/account/programs/buy_plan_complete/");

    }

    /**
     * Shows the success screen after buying a laundry plan
     */
    public function buy_plan_complete()
    {
        $this->load->model('laundryplan_model');
        $plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'laundry_plan');
        $this->content['plan'] = $plan;
        $this->content['wfPrice'] = $this->laundryplan_model->getDefaultPrice($this->customer_id, $this->business_id);

        $this->renderAccount('programs_buy_plan_complete', $this->content);
    }

    /**
     * Toggle on/off the laundry plans renewal
     */
    public function plan_update()
    {
        if (!isset($_POST['submit'])) {
            set_alert('error', 'POST is required');
            redirect('/account/programs/plans');
        }
        unset($_POST['submit']);
        $this->load->model('laundryplan_model');
        $plan = $this->laundryplan_model->get_one(array(
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'active' => 1,
            'days >' => 0,
        ));

        if (!$plan) {
            set_alert('error', get_translation('laundry_plan_not_updated', 'business_account/plan_update', array(), "Laundry Plan Not Updated"));
            redirect('/main/plans');
        }

        $this->laundryplan_model->save(array(
            'laundryPlanID' => $plan->laundryPlanID,
            'renew' => $_POST['renew'],
        ));

        //if the plan has totally expired
        if ($plan->renew && strtotime($plan->endDate) < now()) {
            //shouldn't be here... the plan is not active!
            set_alert('error', get_translation('laundry_plan_expired', 'business_account/plan_update', array(), 'Your plan has expired. You need to buy a new laundry plan'));
            redirect('/main/plans');
        }

        set_alert('success', get_translation('laundry_plan_updated', 'business_account/plan_update', array(), 'Laundry Plan has been updated'));
        redirect('/main/plans');
    }

    /**
     * Shows the UI to buy a new everything plan
     *
     * @param int $plan_id
     */
    public function display_buy_everything_plan_form($plan_id = null)
    {
        /*check if customer already has a WF plan */
        $this->load->model("laundryplan_model");
        $active_wf_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'laundry_plan');

        $active_product_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'product_plan');

        if ($active_wf_plan || $active_product_plan) {
            redirect("/account/programs/display_buy_plan_error");           
        }

        if (!$plan_id) {
            set_alert('error', "Unknown Plan");
            redirect("/main/everything_plans/");
        }

        $customer = new Customer($this->customer_id);
        
        //check to see if customer is on invoice
        if (!$customer->invoice) {
            //check to see if there is a credit card on file
            $this->load->model('creditcard_model');
            $card = $this->creditcard_model->get_one(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
            ));  

            if (!$card) {
                $message = get_translation('missing_card', 'business_account/programs_buy_everything_plan',
                    array(), "You must have a credit card on file to purchase a everything plan.");

                set_alert('error', $message);
                redirect('/account/profile/billing');
            }
        }

        
        $plan = $this->everything_plans = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(array(
                'businessLaundryPlanID' => $plan_id,
                "business_id" => $this->business_id
            ));

        $this->content['wfPrice'] = $this->everything_plans = \App\Libraries\DroplockerObjects\EverythingPlan::getDefaultPrice($this->customer_id, $this->business_id);

        $this->content['plan'] = $plan[0];

        $this->renderAccount('programs_buy_everything_plan', $this->content);
    }

    /**
     * Shows the UI to buy a new product plan
     *
     * @param int $plan_id
     */
    public function display_buy_product_plan_form($plan_id = null)
    {
        /*check if customer already has a WF plan */
        $this->load->model("laundryplan_model");
        $active_wf_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'laundry_plan');

        $active_everything_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'everything_plan');

        if ($active_wf_plan || $active_everything_plan) {
            redirect("/account/programs/display_buy_plan_error");           
        }

        if (!$plan_id) {
            set_alert('error', "Unknown Plan");
            redirect("/main/product_plans/");
        }

        $customer = new Customer($this->customer_id);
        
        //check to see if customer is on invoice
        if (!$customer->invoice) {
            //check to see if there is a credit card on file
            $this->load->model('creditcard_model');
            $card = $this->creditcard_model->get_one(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
            ));  

            if (!$card) {
                $message = get_translation('missing_card', 'business_account/programs_buy_product_plan',
                    array(), "You must have a credit card on file to purchase a product plan.");

                set_alert('error', $message);
                redirect('/account/profile/billing');
            }
        }

        
        $plan = $this->product_plans = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(array(
                'businessLaundryPlanID' => $plan_id,
                "business_id" => $this->business_id
            ));

        $this->content['wfPrice'] = $this->product_plans = \App\Libraries\DroplockerObjects\ProductPlan::getDefaultPrice($this->customer_id, $this->business_id);

        $this->content['plan'] = $plan[0];

        $this->renderAccount('programs_buy_product_plan', $this->content);
    }

    /**
     * Shows error message
     *
     * @param int $couponID
     */
    public function display_buy_plan_error()
    {
        /*check if customer already has an active plan of any type */
        $sql = "SELECT *
                FROM laundryPlan
                WHERE 
                    customer_id = ? AND
                    business_id = ? AND
                    active = ?;";
        $result = $this->db->query($sql, array($this->customer_id, $this->business_id, 1));
        $result = $result->result();
        $this->content["plan"] = $result[0];
        $this->renderAccount('programs_buy_plan_error', $this->content);
    }

    /**
     * Buys a everything plan
     */
    public function buy_everything_plan()
    {
        $plan_id = $this->input->post("businessLaundryPlanID");
        $this->load->model('order_model');
        $options['customer_id'] = $this->customer_id;
        $options['business_id'] = $this->business_id;
        $options['plan_id'] = $plan_id;
        $options['type'] = "everything_plan";

        //check to see if customer is on invoice
        $customer = new Customer($this->customer_id);
        if ($customer->invoice) {
            $options['orderPaymentStatusOption_id'] = 4;
        }

        $response = $this->order_model->new_laundry_plan_order($options);

        if ($response['status']!='success') {
            set_alert('error', $response['message']);
            redirect("/account/programs/display_buy_everything_plan_form/");
        }

        // send the email to the customer with everything plan details
        $action_array = array(
            'do_action' => 'new_everythingplan',
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'order_id' => $response['order_id'],
            'everythingPlan_id' => $plan_id
        );

        Email_Actions::send_transaction_emails($action_array);

        //capture order
        $inmediate_order_capture = get_business_meta($this->business_id, 'inmediate_order_capture','0');

        if($inmediate_order_capture && !$customer->invoice) {
            $this->load->model('order_model');
            $result = $this->order_model->capture($response['order_id'], $this->business_id);
        }

        set_alert('success', $response['message']);
        redirect("/account/programs/buy_everything_plan_complete/");

    }

    /**
     * Shows the success screen after buying a laundry plan
     */
    public function buy_everything_plan_complete()
    {
        $plan = \App\Libraries\DroplockerObjects\EverythingPlan::search_aprax(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
                'active' => 1,
            ));

        $this->content['plan'] = $plan[0];

        $this->renderAccount('programs_buy_everything_plan_complete', $this->content);
    }

    /**
     * Toggle on/off the laundry plans renewal
     */
    public function everything_plan_update()
    {
        if (empty($this->input->post('submit'))) {
            set_alert('error', 'POST is required');
            redirect('/main/everything_plans');
        }
        unset($_POST['submit']);

        $plan = \App\Libraries\DroplockerObjects\EverythingPlan::search_aprax(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
                'active' => 1,
                'days >' => 0
            ));


        if (empty($plan)) {
            set_alert('error', get_translation(' everything_plan_not_updated', 'business_account/everything_plan_update', array(), "Everything Plan Not Updated"));
            redirect('/main/everything_plans');
        }

        $everythingPlan = new \App\Libraries\DroplockerObjects\EverythingPlan($plan[0]->laundryPlanID);
        $everythingPlan->renew = (int)$this->input->post("renew");
        $everythingPlan->save();

        //if the plan has totally expired
        if ($plan[0]->renew && strtotime($plan[0]->endDate) < now()) {
            set_alert('error', get_translation('everything_plan_expired', 'business_account/everything_plan_update', array(), 'Your plan has expired. You need to buy a new everything plan'));
            redirect('/main/everything_plans');
        }

        set_alert('success', get_translation('everything_plan_updated', 'business_account/everything_plan_update', array(), 'Everything Plan has been updated'));
        redirect('/main/everything_plans');
    }

    /**
     * Upgrades a customers laundry plan
     */
    public function upgrade_everything_plan($plan_id = null)
    {
        $plans = \App\Libraries\DroplockerObjects\EverythingPlan::search_aprax(array(
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'type'        => "everything_plan",
            'active'      => 1
        ));

        $this->content['current_plan'] = $plans[0];

        if (count($plans) > 1) {
            $message = get_translation("already_have_plan_error", "business_account/programs_upgrade_everything_plan",
                array(), "You are already set for an upgrade. Please cancel the current upgrade before proceeding");
            set_alert('error', $message);

            return redirect("/main/everything_plans");
        }

        // get the new plan the customer wants to upgrade to
        $new_plan = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(array(
            'businessLaundryPlanID' => $plan_id,
            'business_id' => $this->business_id
        ));

        $this->content['new_plan'] = $new_plan[0];

        if (!empty($this->input->post("submit"))) {

            if ($plan_id != $this->input->post('new_plan_id')) {
                throw new Exception("Plan Mismatch on Posting for plan upgrade");
            }

            $this->load->model('order_model');
            $options['customer_id'] = $this->customer_id;
            $options['business_id'] = $this->business_id;
            $options['type'] = "everything_plan";
            $options['new_plan_id'] = $this->input->post('new_plan_id');
            $laundryPlanID = $this->order_model->upgrade_laundry_plan($options);

            redirect("/main/everything_plans");
        }

        $this->renderAccount('programs_upgrade_everything_plan', $this->content);
        return;
    }

    /**
     * Deletes a pending laundry plan upgrade
     */
    public function cancel_everything_plan_upgrade()
    {
        if (!isset($_POST['submit'])) {
            redirect('/main/plans');
        }

        $everythingPlan = new \App\Libraries\DroplockerObjects\EverythingPlan($this->input->post('laundryplan_id'));
        $affected_rows = $everythingPlan->delete();

        if ($affected_rows) {
            $message = get_translation("everything_plan_cancelled", "business_account/cancel_everything_upgrade",
                array(), "Everything plan upgrade has been cancelled");
            set_alert("success", $message);
        } else {
            $message = get_translation("everything_plan_not_cancelled_error", "business_account/cancel_everything_upgrade",
                array(), "Everything plan upgrade has not been cancelled");
            set_alert("error", $message);
        }

        redirect("/main/everything_plans");
    }

    /**
     * Buys a product plan
     */
    public function buy_product_plan()
    {
        $plan_id = $this->input->post("businessLaundryPlanID");
        $this->load->model('order_model');
        $options['customer_id'] = $this->customer_id;
        $options['business_id'] = $this->business_id;
        $options['plan_id'] = $plan_id;
        $options['type'] = "product_plan";

        //check to see if customer is on invoice
        $customer = new Customer($this->customer_id);
        if ($customer->invoice) {
            $options['orderPaymentStatusOption_id'] = 4;
        }

        $response = $this->order_model->new_laundry_plan_order($options);

        if ($response['status']!='success') {
            set_alert('error', $response['message']);
            redirect("/account/programs/display_buy_product_plan_form/");
        }

        // send the email to the customer with everything plan details
        $action_array = array(
            'do_action' => 'new_productplan',
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'order_id' => $response['order_id'],
            'productPlan_id' => $plan_id
        );

        Email_Actions::send_transaction_emails($action_array);

        //capture order
        $inmediate_order_capture = get_business_meta($this->business_id, 'inmediate_order_capture','0');

        if($inmediate_order_capture && !$customer->invoice) {
            $this->load->model('order_model');
            $result = $this->order_model->capture($response['order_id'], $this->business_id);
        }

        set_alert('success', $response['message']);
        redirect("/account/programs/buy_product_plan_complete/");

    }

    /**
     * Shows the success screen after buying a product plan
     */
    public function buy_product_plan_complete()
    {
        $plan = \App\Libraries\DroplockerObjects\ProductPlan::search_aprax(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
                'active' => 1,
            ));

        $this->content['plan'] = $plan[0];

        $this->renderAccount('programs_buy_product_plan_complete', $this->content);
    }

    /**
     * Toggle on/off the product plans renewal
     */
    public function product_plan_update()
    {
        if (empty($this->input->post('submit'))) {
            set_alert('error', 'POST is required');
            redirect('/main/product_plans');
        }
        unset($_POST['submit']);

        $plan = \App\Libraries\DroplockerObjects\ProductPlan::search_aprax(array(
                'customer_id' => $this->customer_id,
                'business_id' => $this->business_id,
                'active' => 1
            ));


        if (empty($plan)) {
            set_alert('error', get_translation(' product_plan_not_updated', 'business_account/product_plan_update', array(), "Product Plan Not Updated"));
            redirect('/main/product_plans');
        }

        $productPlan = new \App\Libraries\DroplockerObjects\ProductPlan($plan[0]->laundryPlanID);
        $productPlan->renew = (int)$this->input->post("renew");
        $productPlan->save();

        //if the plan has totally expired
        if ($plan[0]->renew && strtotime($plan[0]->endDate->format("Y-m-d")) < now()) {
            set_alert('error', get_translation('product_plan_expired', 'business_account/product_plan_update', array(), 'Your plan has expired. You need to buy a new product plan'));
            redirect('/main/product_plans');
        }

        set_alert('success', get_translation('product_plan_updated', 'business_account/product_plan_update', array(), 'Product Plan has been updated'));
        redirect('/main/product_plans');
    }

    /**
     * Upgrades a customers laundry plan
     */
    public function upgrade_product_plan($plan_id = null)
    {
        $plans = \App\Libraries\DroplockerObjects\ProductPlan::search_aprax(array(
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'type'        => "product_plan",
            'active'      => 1
        ));

        $this->content['current_plan'] = $plans[0];

        if (count($plans) > 1) {
            $message = get_translation("already_have_plan_error", "business_account/programs_upgrade_product_plan",
                array(), "You are already set for an upgrade. Please cancel the current upgrade before proceeding");
            set_alert('error', $message);

            return redirect("/main/product_plans");
        }

        // get the new plan the customer wants to upgrade to
        $new_plan = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(array(
            'businessLaundryPlanID' => $plan_id,
            'business_id' => $this->business_id
        ));

        $this->content['new_plan'] = $new_plan[0];

        if (!empty($this->input->post("submit"))) {

            if ($plan_id != $this->input->post('new_plan_id')) {
                throw new Exception("Plan Mismatch on Posting for plan upgrade");
            }

            $this->load->model('order_model');
            $options['customer_id'] = $this->customer_id;
            $options['business_id'] = $this->business_id;
            $options['type'] = "product_plan";
            $options['new_plan_id'] = $this->input->post('new_plan_id');
            $laundryPlanID = $this->order_model->upgrade_laundry_plan($options);

            redirect("/main/product_plans");
        }

        $this->renderAccount('programs_upgrade_product_plan', $this->content);
        return;
    }

    /**
     * Deletes a pending laundry plan upgrade
     */
    public function cancel_product_plan_upgrade()
    {
        if (!isset($_POST['submit'])) {
            redirect('/main/plans');
        }

        $productPlan = new \App\Libraries\DroplockerObjects\ProductPlan($this->input->post('laundryplan_id'));
        $affected_rows = $productPlan->delete();

        if ($affected_rows) {
            $message = get_translation("product_plan_cancelled", "business_account/cancel_product_upgrade",
                array(), "Product plan upgrade has been cancelled");
            set_alert("success", $message);
        } else {
            $message = get_translation("product_plan_not_cancelled_error", "business_account/cancel_product_upgrade",
                array(), "Product plan upgrade has not been cancelled");
            set_alert("error", $message);
        }

        redirect("/main/product_plans");
    }


    /**
     * Prints the referral flyer
     */
    public function print_coupon()
    {


        $this->load->model('coupon_model');
        $this->content['promo_code'] = $this->promo_code;
        $this->content['business'] = $this->business;
        $active_coupons = $this->coupon_model->get_active_coupons($this->business_id);
        foreach ($active_coupons as $aCoupon) {
            if($aCoupon->prefix == 'CR') {
                $this->content['coupon_amount'] = $aCoupon->amount;
                $this->content['coupon_amount_type'] = $aCoupon->amountType;
                $this->content['coupon_maxAmt'] = $aCoupon->maxAmt;
            }
        }

        $this->load->view('/business_account/programs_print_coupon', $this->content);
    }

    /**
     * Will redirect to account home if rewards are not enabled
     */
    public function checkRewardsEnabled()
    {
        if ($this->rewards_config->lockerLootStatus != 'active') {
            set_alert('error', $this->rewards_config->programName . ' not enabled');
            redirect("/account/");
        }
    }

    /**
     * Renders the rewards sidebar
     */
    protected function loadRewardsSidebar()
    {
        $lockerLootConfig = $this->rewards_config;

        $sidebar['totalReward'] = $lockerLootConfig->referralAmountPerReward * $lockerLootConfig->referralNumberOrders;
        $sidebar['minRedeemAmount'] = $lockerLootConfig->minRedeemAmount;
        $sidebar['programName'] = $lockerLootConfig->programName;

        $this->load->model('customer_model');
        $balance = $this->customer_model->get_locker_loot_balance($this->customer_id);

        $sidebar['balance'] = $balance;
        $sidebar['redeemAmount'] = $balance * ($lockerLootConfig->redeemPercent / 100);

        $sidebar['promo_code'] = $this->promo_code;
        $sidebar['business'] = $this->business;
        $sidebar['lockerLootEndPoint'] = $this->lockerLootEndPoint;

        $this->data['sidebar'][] = $this->load->view($this->viewFileLocation.'programs_locker_loot_sidebar', $sidebar, true);
    }

    /**
     * Shows the referral program info
     */
    public function rewards()
    {
        $this->checkRewardsEnabled();

        $lockerLootConfig = $this->rewards_config;
        $this->content['totalReward'] = $lockerLootConfig->referralAmountPerReward * $lockerLootConfig->referralNumberOrders;
        $this->content['percentPerOrder'] = $lockerLootConfig->percentPerOrder;
        $this->content['redeemPercent'] = $lockerLootConfig->redeemPercent;
        $this->content['minRedeemAmount'] = $lockerLootConfig->minRedeemAmount;
        $this->content['referralNumberOrders'] = $lockerLootConfig->referralNumberOrders;
        $this->content['referralAmountPerReward'] = $lockerLootConfig->referralAmountPerReward;
        $this->content['referralOrderAmount'] = $lockerLootConfig->referralOrderAmount;
        $this->content['allowToRedeem'] = $lockerLootConfig->allowToRedeem;
        $this->content['programName'] = $lockerLootConfig->programName;
        $this->content['promo_code'] = $this->promo_code;

        //find out who your earners are
        $getEarners = "SELECT firstName, lastName, customerID
                FROM customer
                WHERE referredBy = '$this->customer_id';";
        $result = $this->db->query($getEarners);
        $this->content['earners'] = $earners = $result->result();

        foreach ($earners as $e) {
            //get last orders
            $getLastOrder = "select max(`orders`.dateCreated) as lastOrder
                    from `orders`
                    WHERE orderStatusOption_id = '10'
                    AND customer_id = $e->customerID;";
            $result = $this->db->query($getLastOrder);
            $lastOrder = $result->result();

            if (!$lastOrder) {
                $this->content['points'][$e->customerID] = 0;
                $this->content['days'][$e->customerID] = get_translation('no_orders', 'business_account/programs_rewards', array(), 'No Orders');
                continue;
            }

            //if they have an order
            $date = strtotime("now");
            $date2 = strtotime($lastOrder[0]->lastOrder);

            $difference = $date-$date2; //Calcuates Difference
            $days2 = floor($difference /60/60/24); //Calculates Days Old
            $this->content['days'][$e->customerID] = get_translation('days_ago', 'business_account/programs_rewards', array('days' => round($days2)), '%days% Days Ago');

            //get amount of loot from this customer
            $getLoot = "SELECT `orders`.customer_id, sum(points) as totPoints
                FROM lockerLoot
                join `orders` on orderID = lockerLoot.order_id
                where lockerLoot.customer_id = '$this->customer_id'
                and `orders`.customer_id = $e->customerID
                group by `orders`.customer_id";
            $result = $this->db->query($getLoot);
            $loot = $result->result();

            if ($loot) {
                $this->content['points'][$e->customerID] = $loot[0]->totPoints;
            } else {
                $this->content['points'][$e->customerID] = 0;
            }
        }

        $this->load->model("website_model");
        $website = $this->website_model->get_one(array(
            "business_id" => $this->business_id
        ));
        $this->content['facebook_share_rewards_program_thumbnail_url'] = $website->facebook_share_rewards_program_thumbnail_url;

        $this->content['business_id'] = $this->business_id;
        $this->content['facebookLink'] = 'http://www.facebook.com/sharer.php?u='.$this->lockerLootEndPoint;
        $this->content['twitterLink'] = 'http://twitter.com/home?status='.urlencode(
            get_translation('twitter_link', 'business_account/programs_rewards', array(
                'companyName' => $this->business->companyName,
                'promo_code' => $this->promo_code,
                'url' => $lockerLootConfig->bitly,
            ), '%companyName% is my new Dry Cleaner. Use my Promo Code %promo_code% to receive 25% off your first order. %url%')
        );


        $this->loadRewardsSidebar();
        $this->renderAccount('programs_rewards', $this->content);
    }

    /**
     * Redeem referral credits as cash
     */
    public function redeem_cash()
    {
        $lockerLootConfig = $this->rewards_config;

        // must agree to the terms first
        if (!$this->customer->lockerLootTerms) {
            $message = get_translation("error_terms", "business_account/programs_redeem_cash",
                array(
                    'program_name' => $lockerLootConfig->programName,
                ),
                "Before you can redeem %program_name%, you must agree to the terms");
            set_alert('error', $message);
            return redirect("/account/programs/reward_terms");
        }

        $balance = $this->customer_model->get_locker_loot_balance($this->customer_id);

        // baclance is too low to show this page, so redirect to reward page
        if ($balance <= $lockerLootConfig->minRedeemAmount) {
            $message = get_translation("error_amount", "business_account/programs_redeem_cash",
                array(
                    'program_name' => $lockerLootConfig->programName,
                    'amount' => $lockerLootConfig->minRedeemAmount,
                    'balance' => $balance,
                ),
                "You need to earn at least %amount% in order to redeem %program_name%");
            set_alert('error', $message);

            return redirect("/account/programs/rewards");
        }

        $redeemAmount = $balance * ($lockerLootConfig->redeemPercent / 100);
        //process cash redeem
        if ($this->customer->W9) {
            $this->content['total'] = $total = $redeemAmount;
            $this->lockerloot_model->redeem_cash($this->customer_id, $balance, $lockerLootConfig->minRedeemAmount);
            send_admin_notification('Spread the word redeemed', 'A check for '.format_money_symbol($this->business_id, '%.2n', $total).' needs to be sent to customer id '.$this->customer_id.'.');
            $balance = 0.00;
        }

        $this->content['W9'] = $this->customer->W9;
        $this->content['agree'] = $this->customer->lockerLootTerms;
        $this->content['program_name'] = $lockerLootConfig->programName;

        $this->loadRewardsSidebar();
        $this->renderAccount('programs_redeem_cash', $this->content);
    }

    /**
    * interface for redeeming lockerloot
    */
    public function redeem_points()
    {
        $lockerLootConfig = $this->rewards_config;

        $balance = $this->customer_model->get_locker_loot_balance($this->customer_id);
        $this->content['points'] = $balance;

        if ($balance>0) {
            $this->load->model('lockerloot_model');
            $this->lockerloot_model->redeem_points($this->customer_id, $this->business_id);
            $balance = $this->customer_model->get_locker_loot_balance($this->customer_id);
        } else {
            $message = get_translation("error_amount", "business_account/programs_redeem_cash",
                array(
                    'program_name' => $lockerLootConfig->programName,
                    'amount' => $lockerLootConfig->minRedeemAmount,
                    'balance' => $balance,
                ),
                "You need to earn %program_name% before you can redeem them");
            set_alert('error', $message);

            return redirect('/account/programs/rewards');
        }

        $this->content['program_name'] = $lockerLootConfig->programName;

        $this->loadRewardsSidebar();
        $this->renderAccount('programs_redeem_points', $this->content);
    }

    /**
     * interface for reading the rewards terms
     */
    public function reward_terms()
    {
        $lockerLootConfig = $this->rewards_config;

        $balance = $this->customer_model->get_locker_loot_balance($this->customer_id);
        $customer = new Customer($this->customer_id);
        if (!empty($_POST)) {
            $customer->lockerLootTerms = 1;
            $customer->save();

            $response = get_translation("successful_participation", "business_account/programs_reward_terms",
                array("programName" => $lockerLootConfig->programName),
                "Thank you for your participation in the %programName% terms. <a href='/account/programs/rewards'>Back to %programName%</a>");
            set_alert("success", $response);

            redirect($this->uri->uri_string());
            return;
        }
        $this->content['agree'] = $customer->lockerLootTerms;
        $this->content['business'] = $this->business;
        $this->content['terms'] = $lockerLootConfig->terms;
        $this->content['business_id'] = $this->business_id;
        $this->content['programName'] = $lockerLootConfig->programName;

        $this->renderWide('programs_reward_terms', $this->content);
    }

    /**
     * UI for viewing and editing coupons and discounts
     */
    public function discounts()
    {
        //if this customer has any expired discounts, make them inactive
        $this->load->model('customerdiscount_model');
        $this->customerdiscount_model->expireCustomerDiscount($this->business_id,$this->customer_id);

        if (isset($_POST['submit']) || isset($_GET['promo'])) {

            if (!empty($_POST)) {
                $code = $this->input->post('code');
            } else {
                $code = $this->input->get('promo');
            }

            if (empty($code)) {
                set_alert("error", get_translation("coupon_code_validation_error_message", 'business_account/programs_discounts', array(), "Please enter a coupon code", $this->customer_id));
                return redirect("/account/programs/discounts");
            }

            $this->load->library('coupon');
            $response = $this->coupon->applyCoupon($code, $this->customer_id, $this->business_id);

            if ($response['status']=='fail') {
                    set_alert('error', $response['error']);
            } else {
                    set_alert('success', $response['message']);
            }

            return redirect($this->uri->uri_string());
        }

        $this->content['date_format'] = get_business_meta($this->business_id, 'shortDateLocaleFormat', '%b %e, %Y');

        $this->content['discounts'] = $this->customerdiscount_model->get_pending($this->customer_id);
        $this->content['applied_discounts'] = $this->customerdiscount_model->get_applied($this->customer_id);
        $this->content['zone'] = isset($this->customer->timezone)?$this->customer->timezone:"UTC";

        // load extra sidebar module at the end
        $this->content['sidebars']['/business_account/programs_discount_sidebar'] = array('business' => $this->business);

        $this->renderAccount('programs_discounts', $this->content);
    }

}
