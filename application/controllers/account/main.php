<?php
use App\Libraries\DroplockerObjects\Customer;

/**
 *
 * Used for the account controls for the businesses.
 *
 * Note that there is no access control in the contructor. There are many functions in this controller that
 * do not need access control. If access control is required, put it in the function
 *
 * @author matt
 *
 */
class Main extends MY_Account_Controller
{
    /**
     * this displays the customer main account page /account
     */
    public function index()
    {
        $this->requireLogin();

        $content = $this->content;

        //On the main account page, show the current laundryplan if the customer has one
        $this->load->model('laundryplan_model');
        $active_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'laundry_plan');
        $content['active_plan'] = $active_plan;

        $content['business'] = new \App\Libraries\DroplockerObjects\Business($this->business_id, false);

        /*On the main account page, show the everything plan if the customer has one*/
        $content['active_everything_plan'] = \App\Libraries\DroplockerObjects\EverythingPlan::search_aprax(array(
            "business_id" => $this->business_id,
            "customer_id" => $this->customer->customerID,
            "type"        => "everything_plan",
            "active"     => 1
        ));

        /*On the main account page, show the product plan if the customer has one*/
        $content['active_product_plan'] = \App\Libraries\DroplockerObjects\ProductPlan::search_aprax(array(
            "business_id" => $this->business_id,
            "customer_id" => $this->customer->customerID,
            "type"        => "product_plan",
            "active"     => 1
        ));


        $content['isProfileComplete'] = $this->customer_model->isProfileComplete($this->customer->customerID);

        $content['lockerLootActive'] = $this->rewards;
        if (!empty($this->rewards_name)) {
            $content['programName'] = $this->rewards_name;
        }

        // show custom popup to customer in route
        $this->load->library("routelibrary");
        $customerRoutes = $this->routelibrary->getRoutesForCustomer($this->customer->customerID, $this->business_id);

        $popups = $this->config->item('popups');
        foreach ($customerRoutes as $route_id) {
            $session_key = md5($this->customer->customerID.$this->business_id.$route_id);
            $already_showed = $this->session->userdata($session_key);
            if (empty($already_showed)) {
                if (!empty($popups[$this->business_id][$route_id]['enabled'])) {
                    $content['popup'] = $this->load->view($popups[$this->business_id][$route_id]['view'], '', true);
                    $this->session->set_userdata($session_key, true);
                }
            }
        }
        //

        $this->renderAccount('main_index', $content);
    }

    /**
     * Login for customers to a business account
     */
    public function login()
    {
        $content = $this->content;

        if (!empty($this->business->facebook_api_key) && !empty($this->business->facebook_secret_key)) {
            $redirect_uri = get_business_account_url($this->business) . '/login';
            $this->attemptFacebookLogin($redirect_uri);
            
            $content['facebook_url'] = $this->facebookloginlibrary->getLoginUrl();
            $redirect = $this->input->get("redirect");
            if ($redirect == "facebook") {
                redirect($this->facebookloginlibrary->getLoginUrl());
            }
        } else {
            $content['facebook_url'] = '';
        }

        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));

        //Note, we need custom validators in order to internationalize the validaiton error messages
        $this->form_validation->set_rules('username', "Email", 'callback__username_check');

        $this->form_validation->set_rules('password', 'Password', 'callback__password_check');

        if ($this->form_validation->run()) {
            $this->load->model('customer_model');
            $customer = $this->customer_model->login($_POST['username'], md5($_POST['password']), $this->business_id);

            if ($customer) {

                //if we have a location id passed, and the user doesn't have a default, set it
                $new_default_location_id = $this->input->post('lid');
                $custObj = new Customer($customer->customerID);
                if (empty($customer->location_id) && !empty($new_default_location_id)) {
                    $existing_check_query = $this->db->get_where('customer_location', array('customer_id' => $custObj->customerID, 'location_id' => $new_default_location_id ));
                    $existing_customer_location = $existing_check_query->row();
                    if (empty($existing_customer_location)) { //if there isn't already a customer_location record for this
                        $this->db->insert('customer_location', array( 'customer_id' => $custObj->customerID, 'location_id' => $new_default_location_id ));
                    }
                    $custObj->location_id = $new_default_location_id;
                    $custObj->save();
                }

                $this->session->set_userdata('user_id', $customer->customerID);

                set_alert('success', get_translation("welcome_back_notice", "business_account/main_index", array(), "Welcome Back"));

                $return = $this->session->userdata('ref');
                if (empty($return)) {
                    $return = '/account';
                }

                redirect($return);
            } else {
                set_alert('error', get_translation("wrong_email_notice", "business_account/main_index", array(), "Wrong Email, Username, or Password"));
                redirect('/login');
            }
        }

        $view_name = 'main_login';

        $data['content'] = $this->load->view($this->viewFileLocation.$view_name, $content, true);
        $this->template->write_view('content', $this->viewWideTemplate, $data);
        $this->template->render();
    }

    protected function attemptFacebookLogin($redirect_uri = "")
    {
        if (empty($_REQUEST["code"])) {
            return false;
        }

        $result = $this->facebookloginlibrary->loginOrCreateCustomer($redirect_uri);

        if ($result['status'] != 'success') {
            set_alert("error", $result['message']);
            redirect('/login');
        }

        set_alert("success", $result['message']);
        $this->session->set_userdata('user_id', $result['data']['customer']['customerID']);

        $url = $result['data']['firstTime'] ? get_business_meta($this->business_id, 'registerLandingPage', '/account/profile/edit') : '/account';
        redirect($url);
    }

    public function start_mobile_login()
    {
        $url = $this->facebookloginlibrary->getLoginUrl(get_business_account_url($this->business) . '/account/main/mobile_login');

        die("<html><body><script>setTimeout(function() {window.location.href = '$url'}, 300)</script></body></html>");

        //redirect($url);
    }

    public function mobile_login()
    {
        $mobile_app = "https://mobile.droplocker.com/";

        if (empty($_REQUEST["code"])) {
            return redirect($mobile_app);
        }

        $result = $this->facebookloginlibrary->loginOrCreateCustomer();

        if ($result['status'] != 'success') {
            return redirect("$mobile_app#/signin?error=" . urlencode($result['message']));
        }

        redirect("$mobile_app#/signin?user=" . urlencode($result['data']['customer']['email']) . '&password=' . urldecode($result['data']['customer']['password']));
    }

    /**
     * removes ths customers account access
     */
    public function logout()
    {
        if ($this->input->get("facebook_logout")) {
            $this->facebook->destroySession();
            //$this->facebook->clearAllPersistentData();
        }


        // store customer language in session to use for logout page
        $language_id = $this->customer->default_business_language_id;
        $this->session->set_userdata('language', $language_id);

        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_fname');
        $this->session->unset_userdata('user_lname');

        set_alert('success', get_translation(
            'logout',
            "business_account/main_index",
            array(),
            'You have been logged out',
            $this->customer_id
        ));
        redirect('/login');
    }

    /**
     * The following validator is used for internationalizing validation error messages for the email field in the login view.
     * @param string $email
     * @return boolean
     */
    public function _username_check($email)
    {
        if (empty($email)) {
            $this->form_validation->set_message(__FUNCTION__, get_translation("invalid_email_error", "business_account/main_login", array(), "Email is Required"));
            return false;
        } else {
            return true;
        }
    }
    /**
     * The following validator is used for internationalizaing validaiton error messages for tbhe password field in the login view.
     * @param type $password
     */
    public function _password_check($password)
    {
        if (empty($password)) {
            $this->form_validation->set_message(__FUNCTION__, get_translation("invalid_password_error", "business_account/main_login", array(), "Password is Required"));

            return false;
        } else {
            return true;
        }
    }


    /**
     * Sends a verification link to a customer when they have forgotten their password
     */
    public function forgot_password()
    {
        $content = $this->content;
        if (get_business_meta($this->business_id, 'customerSupport')=='') {
            $content['error'] = "<div class='alert alert-error'>This company is not configured to send customer service emails. Please contact support for help.</div>";
        }
        if (isset($_POST['submit'])) {
            if (empty($_POST['email'])) {
                set_alert("error", get_translation("invalid_email_error", "business_account/main_forgot_password", array(), "Please provide a valid email address"));
                redirect($this->uri->uri_string());
            }
            $email = $this->input->post("email");
            $this->load->model("customer_model");

            $customer = $this->customer_model->get_one(array("email" => $email, "business_id" => $this->business_id));
            if (empty($customer)) {
                set_alert("error", get_translation("customer_not_found_error_message", "business_account/main_forgot_password", array("customer_email" => $email), "There is no customer with e-mail '%customer_email%'"));
            } else {
                $this->load->helper("customer_helper");

                if (send_forgot_password_email($customer)) {
                    set_alert("success", get_translation("forgot_email_success_message", "business_account/main_forgot_password", array("customer_email" => $email), "An email has been sent to %customer_email% with instructions on how to change your password"));
                } else {
                    set_alert("error", get_translation("forgot_email_failure_message", "business_account/main_forgot_password", array(), "Could not send forgot password e-mail"));
                }
            }
            redirect($this->uri->uri_string());
        }

        $view_name = 'main_forgot_password';

        $this->data['content'] = $this->load->view($this->viewFileLocation.$view_name, $content, true);
        $this->template->write_view('content', $this->viewWideTemplate, $this->data);
        $this->template->render();
    }


    public function register()
    {
        redirect("/account/main/view_registration");
    }

    /**
     * Displays the interface for registering with the applicaiton and also handles new registrations.
     *
     * f18bc755b905270b64481dbcfe7430a7-us1
     */
    public function view_registration()
    {
        $xss_clean = true;
        
        $this->load->model("business_language_model");

        $content['business_language_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business->businessID);
        $content['selected_business_language'] = $this->input->get("business_language_id", $xss_clean);
        if (empty($content['selected_business_language'])) {
            $content['selected_business_language'] = $this->business->default_business_language_id;
        }

        if (!empty($this->business->facebook_api_key) && !empty($this->business->facebook_secret_key)) {
            $content['facebook_url'] = $this->facebookloginlibrary->getLoginUrl(get_full_url("/login"));
        } else {
            $content['facebook_url'] = '';
        }
        $content['business'] = $this->business;
        $content['message'] = null;
        $content['promo_code'] = $this->input->get("promo", $xss_clean);

        if ($content['promo_code'] == false && array_key_exists('laundrylocker_promo', $_COOKIE)) {
            $content['promo_code'] = $_COOKIE['laundrylocker_promo'];
        }

        $content['how'] = $this->input->get("how", $xss_clean);

        $content['recaptcha_enabled'] = get_business_meta($this->business_id, 'recaptcha_enabled');
        if ($content['recaptcha_enabled']) {
            $content['recaptcha_public_key'] = get_business_meta($this->business_id, 'recaptcha_public_key');
        }

        $view_name = 'main_register';
        $this->data['content'] = $this->load->view($this->viewFileLocation.$view_name, $content, true);
        $this->template->write_view('content', $this->viewWideTemplate, $this->data);
        $this->template->render();
    }

    /**
     * Expects the following POST parameters
     *  email
     *  password
     *  business_language_id
     */
    public function register_new_user()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("email", "Email", "required|valid_email");
        $this->form_validation->set_rules("password", "Password", "required");
        $this->form_validation->set_rules("business_language_id", "is_natural_not_zero");

        $params = array();

        //set the promo code, the POST value with promo_code will be unset later
        if ($promo_code = trim($_POST['promo_code'])) {
            $params['promo'] = $promo_code;
        }

        // save the method
        if ($how = trim($_POST['how'])) {
            $params['how'] = $how;
        }

        // preserve parameters in the query string
        $quey_append = $params ? '?' . http_build_query($params)  : '';

        if (!$this->form_validation->run()) {
            set_alert("error", validation_errors());

            return redirect_with_fallback("/account/main/view_registration" . $quey_append);
        }


        $this->load->model('customer_model');

        $email    = trim($this->input->post("email"));
        $password = $this->input->post("password");
        $how      = $this->input->post("how");
        $signup   = "website";

        // check for already registered customer
        $found = $this->customer_model->get_aprax(array(
            'email'       => $email,
            'business_id' => $this->business_id,
        ));

        if ($found) {
            $message = get_translation(
                "email_already_registered_error",
                "business_account/main_register",
                array('email' => $email),
                "There is already a customer with the e-mail '%email%'."
            );

            set_alert("error", $message);
            return redirect_with_fallback("/account/main/view_registration" . $quey_append);
        }

        $recaptcha_enabled = get_business_meta($this->business->businessID, 'recaptcha_enabled');
        if ($recaptcha_enabled) {
            $recaptcha_private_key = get_business_meta($this->business->businessID, 'recaptcha_private_key');
            require_once APPPATH . 'third_party/recaptcha/src/autoload.php';
            $recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_private_key);
            $gRecaptchaResponse = $_POST["g-recaptcha-response"];
            $resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER["REMOTE_ADDR"]);
            if (!$resp->isSuccess()) {
                $message = get_translation(
                    "recaptcha_error",
                    "business_account/main_register",
                    array(),
                    "Please, check reCAPTCHA field."
                );
                set_alert("error", $message);
                return redirect_with_fallback("/account/main/view_registration" . $quey_append);
            }
        }

        $newCustomer_id = $this->customer_model->new_customer($email, $password, $this->business_id, $how, $signup);
        if (!$newCustomer_id) {
            throw new Exception("Failed to create new Customer");
        }

        $this->session->set_userdata('user_id', $newCustomer_id);

        if ($promo_code) {
            $this->load->library('coupon');
            $response = $this->coupon->applyCoupon($promo_code, $newCustomer_id, $this->business_id);
            if ($response['status'] == 'fail') {
                $message = get_translation(
                    "invalid_promotional_code",
                    "business_account/main_register",
                    array(),
                    "Promotional code was invalid. To re-enter the promotional code, go to the <a href='/account/programs/discounts'>discount page</a>."
                );
                set_alert('error', $message);
            } else {
                $this->load->model('coupon_model');
                $discount = $this->coupon_model->get_discount_value_as_string($promo_code, $this->business_id);
                $message = get_translation(
                    "successful_discount_application",
                    "business_account/main_register",
                    array(
                        "promo_code" => $promo_code,
                        "discount" => $discount
                    ),
                    "Promotional code '%promo_code% - %discount%' was applied to your new account."
                );
                set_alert("success", $message);
            }
        } else {
            //If no promotional code was entered, we display the default welcome message.
            $message = get_translation(
                "default_welcome_message",
                "business_account/main_register",
                array(),
                "<h2>Now That You're Registered... Let's Get You Into Our Stores!</h2><p>And it all starts by registering your credit card:</p>"
            );
            set_alert('success', $message);
        }

        $new_customer = new Customer($newCustomer_id);
        if ($this->input->post("default_business_language_id")) {
            $new_customer->default_business_language_id = $this->input->post("default_business_language_id");
        } else {
            $new_customer->default_business_language_id = $this->business->default_business_language_id;
        }
        $new_customer->save();

        //check to see if lid passed, if so, set the location_id for the customer to that
        if ($new_location_id = $this->input->post('lid')) {
            if (is_numeric($new_location_id)) {
                $new_customer->location_id = $new_location_id;
                $new_customer->save();

                //then add it to the list of customer_locations
                $this->db->insert('customer_location', array(
                    'customer_id' => $newCustomer_id,
                    'location_id' => $new_location_id
                ));
            }
        }

        $url = get_business_meta($this->business_id, 'registerLandingPage', '/account');
        redirect($url.'?new=1');
    }

    /**
     * verify password
     *
     * @param string $code
     * @param int $customer_id
     */
    public function verify_password($code = null, $customer_id = null)
    {
        $content = array();

        if (isset($_POST['submit'])) {
            if ($_POST['password'] == $_POST['password2']) {
                $expected = get_customer_meta($customer_id, 'password_verify_code');
                if ($expected == $code) {
                    $customer = new Customer($customer_id);

                    $_POST['username'] = $customer->email;

                    //does the customer old password == the new password
                    if ($customer->password == md5($_POST['password'])) {
                        update_customer_meta($customer_id, 'password_verify_code');
                        set_alert('success', 'Your password has been updated');
                        $this->login();

                        return;
                    }

                    $this->load->model('customer_model');
                    $this->customer_model->password = md5($_POST['password']);
                    $this->customer_model->where = array('customerID' => $customer_id);
                    if ($this->customer_model->update()) {
                        update_customer_meta($customer_id, 'password_verify_code');
                        set_alert('success', 'Your password has been updated');
                        $this->login();

                        return;
                    }
                } else {
                    set_alert('error', "Un validated error");
                    redirect($this->uri->uri_string());
                }
            } else {
                set_alert('error', "Sorry, your passwords do not match");
                redirect($this->uri->uri_string());
            }
        }


        $expected = get_customer_meta($customer_id, 'password_verify_code');
        if ($code == $expected) {
            $this->data['content'] = $this->load->view($this->viewFileLocation.'main_change_password', $content, true);
        } else {
            $this->data['content'] = $this->load->view($this->viewFileLocation.'main_change_password_fail', $content, true);
        }

        $this->template->write_view('content', $this->viewWideTemplate, $this->data);
        $this->template->render();
    }
    /**
     * Displays the terms and conditions for businesses
     */
    public function terms()
    {
        $businessObj = new \App\Libraries\DroplockerObjects\Business($this->business_id);
        $terms = $businessObj->terms;


        $content['terms'] = $terms;
        $this->data['content'] = $this->load->view($this->viewFileLocation.'custom_terms', $content, true);

        $this->template->write_view('content', $this->viewWideTemplate, $this->data);
        $this->template->render();
    }

    /**
     * verify_new_email method is called when a customer wants to update their email.
     *
     * @param int $user_id
     * @param string $verify
     * @param string $type
     */
    public function verify_new_email($user_id = null, $verify = null, $type = null)
    {
        $class = 'error';

        // update customer account
        $this->load->model('customer_model');
        $this->customer_model->customerID = $user_id;
        if (!$user = $this->customer_model->get()) {
            $message = get_translation(
                "profile_not_found",
                "verify_new_email",
                array(),
                "Email not updated. User Profile not found."
            );
        } else {
            if ($user[0]->pendingEmailUpdate != '') {
                $check = unserialize($user[0]->pendingEmailUpdate);

                if ($check['verify'] == $verify) {
                    $found = $this->customer_model->get_aprax(array(
                        'business_id' => $this->customer->business_id,
                        'email' => $check['email'],
                    ));

                    if ($found) {
                        $message = get_translation(
                            "email_already_used_error",
                            "verify_new_email",
                            array('new_email' =>  $check['email']),
                            "Email address '%new_email%' is already in use by another customer. Please enter a new email address."
                        );
                    } else {
                        $this->customer_model->clear();
                        $this->customer_model->email = $check['email'];
                        $this->customer_model->pendingEmailUpdate = '';
                        $this->customer_model->bounced = 0;
                        $this->customer_model->where = array('customerID' => $user_id);
                        $this->customer_model->update();
                        $message = get_translation(
                            "email_updated",
                            "verify_new_email",
                            array(),
                            "Email has been updated."
                        );
                        $class = 'success';
                    }
                } else {
                    $message = get_translation(
                        "wrong_verify_code",
                        "verify_new_email",
                        array(),
                        "Email not updated. Verify code is wrong."
                    );
                }
            } else {
                $message = get_translation(
                    "no_pending_email_found",
                    "verify_new_email",
                    array(),
                    "Email not updated. No pending email found."
                );
            }
        }

        $this->data['sidebar'][] = $this->load->view('/business_account/account_menu', array( 'business' => $this->business ), true);

        $content['class'] = $class;
        $content['message'] = $message;
        $this->data['content'] = $this->load->view($this->viewFileLocation.'main_verify_new_email', $content, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    /**
     * Needed for coupon from old laundrylocker site...
     *
     * The link to this method is in /views/main_partners.
     * Not sure how to view that page from the browser
     */
    public function lombardiCoupon()
    {
        start_session();
        $this->load->view('/business_account/main_lombardiCoupon', $data);
    }


    //shows the businesses specific contact us page
    public function contact()
    {
        $content['message'] = '';
        $content[''] ='';
        if ($this->session->userdata('user_id')) {
            $this->data['sidebar'][] = $this->load->view('/business_account/account_menu', array( 'business' => $this->business ), true);
        }
        $this->data['content'] = $this->load->view($this->viewFileLocation.'main_contact', $content, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }
}
