<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\OrderCharge;
use App\Libraries\DroplockerObjects\Customer_Location;

/**
 *
 * Controller for orders in the account section
 */
class Orders extends My_Account_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->requireLogin();

        $this->load->model('order_model');
        $this->template->add_js('js/account/profile/fix_tables.js');
    }

    /**
     * Display the details for a single order
     *
     * Get the order_id from the url segment 4
     */
    public function order_details($order_id = null)
    {
        $order = new Order($order_id);

        // redirect to account page id the customer should not see this order
        if ($order->customer_id != $this->customer_id || $order->business_id != $this->business_id) {
            redirect('/account/');
        }

        $this_order_only = get_translation("this_order_only", "customer_notes", array(), "THIS ORDER ONLY: ", $this->customer_id);
        $parts = explode($this_order_only, $order->notes, 2);

        $order->systemNotes = !empty($parts[0]) ? $parts[0] : '';
        $order->userNotes = !empty($parts[1]) ? $parts[1] : '';
        $content['order'] = $order;
        $content['this_order_only'] = $this_order_only;

        $this->load->model("locker_model");
        $content['locker'] = $locker = $this->locker_model->get_by_primary_key($order->locker_id);
        $this->load->model("location_model");
        $content['location'] = $this->location_model->get_by_primary_key($locker->location_id);
        $this->load->model("bag_model");

        if (is_numeric($order->bag_id)) {
           $content['bag'] = $this->bag_model->get_by_primary_key($order->bag_id);
        } else {
            $content['bag'] = null;
        }

        //get order items/products
        $items = $order->getData('orderItem');
        for ($i = 0; $i < sizeof($items); $i++) {
            $productName = Product::getName($items[$i]->product_process_id);
            $items[$i]->displayName = $productName['displayName'];
        }
        $content['items'] = $items;

        $this->load->model('ordercharge_model');
        $this->ordercharge_model->order_id = $order_id;
        $content['charges'] = $this->ordercharge_model->get();

        $this->load->model('orderitem_model');

        $order_totals = $this->orderitem_model->get_item_total($order_id);

        $code = $this->customer->phone;
        // last 4 of the customers phone
        $content['accessCode'] = substr(trim($code), -4);

        $this->load->helper("order_helper");
        $content['total'] = $this->order_model->get_net_total($order_id);
        $content['subtotal'] = $order_totals['subtotal'];

        $this->load->model('orderstatus_model');
        $content['history'] = $this->orderstatus_model->get_order_history($order_id, 1);

        $this->load->model('taxgroup_model');

        $content['taxes'] = $this->order_model->get_stored_taxes($order->orderID);
        $content['tax_total'] = 0;
        $content['taxGroups'] = array();

        $content['breakout_vat'] = false;
        foreach ($content['taxes'] as $taxItem) {
            $content['tax_total'] += $taxItem['amount'];

            if ($taxItem['vat_breakout']) {
                $content['breakout_vat'] = true;
            }

            if (empty($content['taxGroups'][$taxItem['taxGroup_id']]) &&
                $taxGroup = $this->taxgroup_model->get_by_primary_key($taxItem['taxGroup_id'])) {
                $content['taxGroups'][$taxGroup->taxGroupID] = $taxGroup;
            }
        }

        if ($this->business_id == 3) {
            $this->data['sidebar'][] = $this->load->view('/business_account/callout_sms',false,true);
            $this->data['sidebar'][] = $this->load->view('/business_account/kiosk_hours',false,true);
        }

        $content['business'] = $this->business;

        if ($this->input->get("print_friendly") == "true") {
            $this->renderEmpty('orders_order_details', $content);
        } else {
            $this->renderAccount('orders_order_details', $content);
        }
    }

    /**
     * Accepts $_POST from new_order'
     *
     * Expects the following POST parameters
     *  locker_id int
     *  location_id int
     * Can take the following optional POST parameters
     *  promotional_code
     */
    public function confirm_order()
    {
        // will redirect on order limit exceeded
        $this->checkOrderLimit();
 
        $this->load->library("form_validation");
        $this->form_validation->set_rules("location_id", "Location ID", "is_natural_no_zero|required");
        $this->form_validation->set_rules("locker_id", "Locker ID", "is_natural_no_zero|required");

        if (!$this->form_validation->run()) {
            set_alert("error", validation_errors());
            redirect_with_fallback("/account/orders/new_order");
        }

        if ($promo_code = $this->input->post('promotional_code')) {
            $this->load->library('coupon');
            $response = $this->coupon->applyCoupon($promo_code, $this->customer_id, $this->business_id);

            if ($response['status'] == 'fail') {
                set_alert('error', $response['error']);
                $this->session->set_flashdata("post", serialize($_POST));
                redirect_with_fallback("/account/orders/new_order");
            }
        }

        $location_id = $this->input->post("location_id");
        $locker_id = $this->input->post("locker_id");

        $content['business_id'] = $this->business_id;
        $content['customer'] = $this->customer;
        $content['location'] = $location = new Location($location_id);
        $content['locker'] = $locker = new Locker($locker_id);

        $customer_location = Customer_Location::search(array(
            'customer_id' => $this->customer_id,
            'location_id' => $location->locationID
        ));

        // If this location has been placed out of service, notify the customer, delete
        // from the customer locations and then redirect so they can choose another location
        if ($location->serviceType == 'not in service') {

            // delete the location from the customers account
            if ($customer_location) {
                $customer_location->delete();
            }

            $message = get_translation("location_out_of_service", "business_account/orders_confirm_order", array(),
                "Location has been placed out of service. Please choose another location.", $this->customer_id);

            set_alert('error', $message);
            redirect("/account/orders/new_order");
        }

        // save customer last location
        $this->customer->location_id = $location_id;
        $this->customer->save();

        //create a new customer_location record for this, if it doesn't already exist
        if (!$customer_location) {
            $customer_location = Customer_Location::create(array(
                'location_id' => $location_id,
                'customer_id' => $this->customer_id,
            ));
        }

        if (isset($_POST['pickup_window_id']) && empty($_POST['pickup_window_id'])) {
            $message = get_translation("missing_pickup_window", "business_account/orders_confirm_order", array(),
                "Missing pickup window, please select a pickup window.", $this->customer_id);

            set_alert('error', $message);
            redirect("/account/orders/new_order");
        }

        // if this is a home pickup, display the pickup info and pass data to the next action
        if ($this->input->post('pickup_window_id')) {
            $content['date_format'] = get_business_meta($this->business_id, 'standardDateLocaleFormat', '%A, %B %e');
            $content['pickup_window_id'] = $this->input->post('pickup_window_id');
            $content['pickupDate'] = $this->input->post('pickupDate');
            $content['windowStart'] = $this->input->post('windowStart');
            $content['windowEnd'] = $this->input->post('windowEnd');

            $homePickup = $this->getHomePickup($location_id, false);

            $homePickup->pickupDate = $this->input->post('pickupDate');

            //format dates to mysql
            $homePickup->windowStart = $this->input->post('windowStart');

            $homePickup->windowStart = date("Y-m-d H:i:s", strtotime($homePickup->windowStart));
            $homePickup->windowEnd = $this->input->post('windowEnd');
            $homePickup->windowEnd = date("Y-m-d H:i:s", strtotime($homePickup->windowEnd));

            $homePickup->pickup_window_id = $this->input->post('pickup_window_id');

            if ($this->input->post('pickup_notes')) {
                $homePickup->notes = $this->input->post('pickup_notes');
            }

            if (!empty($this->input->post('quantity'))) {
                $homePickup->quantity = $this->input->post('quantity');
            }

            $homePickup->save();
        }

        $this->load->helper('claim_helper');

        // perform checks on the claim
        $results = check_claim_exists($locker->lockerID, $locker->is_multi_order());
        if ($results['status'] == 'fail') {
            set_alert('error', $results['message']);
            $this->session->set_flashdata("post", serialize($_POST));

            redirect("/account/orders/new_order");
        }

        //service types
        $service_type = $this->input->post('service_type');
        $content['service_type_serialized'] = serialize($service_type);
        $content['service_type'] = array();

        if ($service_type) {
            $businessServiceType = unserialize($this->business->serviceTypes);

            $this->load->model('serviceType_model');
            $types = $this->serviceType_model->get_aprax(array(
                'serviceTypeID' => $service_type,
            ));

            foreach ($types as $t) {
                $name = $t->name;
                if (!empty($businessServiceType[$t->serviceTypeID])) {
                    $name = $businessServiceType[$t->serviceTypeID]['displayName'];
                }
                $content['service_type'][$t->serviceTypeID] = $name;
            }
        }

        $content['skip_menu'] = true;
        $this->renderPreferencesSidebar(true);
        $this->renderAccount('orders_confirm_order', $content);
    }

    /**
     * Displays the form that allows a customer to place an order
     */
    public function new_order()
    {
        $this->load->model('location_model');
        $this->load->model('locker_model');
        $this->load->model('serviceType_model');

        $content['business_id'] = $sidebar['business_id'] = $this->business_id;

        // check to see if the customer has a complete profile. If not redirect to profile page
        if (!$this->customer_model->isProfileComplete($this->customer_id)) {
			$message = get_translation("profile_not_complete_error", "business_account/profile_index", array(),
                                       "Profile must be complete before placing an order.", $this->customer_id);
            set_alert('error', $message);
            redirect("/account/profile/edit");

            return;
        }

        //get the customer locations
        $options = array();
        $options['customer_id'] = $this->customer_id;
        $options['business_id'] = $this->business_id;
        $options['select'] = "locationType.name as locationType, count(lockerID) as total,companyName as title,lockerType,locationID,description,locationType_id,address,city,state,zipcode,serviceType";
        $options['with_location_type'] = true;
        $options['with_active_locker_count'] = true;
        $customer_locations = $this->location_model->get($options);

        //get business location count
        $options = array();
        $options['business_id'] = $this->business_id;
        $options['select'] = "locationType.name as locationType, count(lockerID) as total,lockerType,companyName as title,locationID,description,locationType_id,address,city,state,zipcode,serviceType";
        $options['with_location_type'] = true;
        $options['with_active_locker_count'] = true;
        $locs = $this->location_model->get($options);



        $content['locationsCount'] = sizeof($locs);

        $content['show_locations_map'] = $this->business->show_locations_map;
        //this pushes all the locations into the dropdown, if the flag isn't set (by default shouldnt be unless theyve changed their settings)
        if (!$this->business->show_locations_map) {
            $customer_locations = $locs;
        }

        if (!$customer_locations && sizeof( $locs ) > 10) {
            $this->session->set_flashdata('need_location');
            redirect('/account/locations/');
        }

        //check the business setting to see if a card is required to place an order
        $card_required = !empty( $this->business->require_card_on_order );

        if ($card_required) {
            //check to see if there is a credit card on file
            if ($this->customer->invoice != 1) {
                if ($this->customer->autoPay != 1) {
                    if ($this->customer->orderWithNoCC != 1) {
                        $this->load->model('creditcard_model');
                        //see if the customer has a card on file.
                        $card = $this->creditcard_model->get_customer_card($this->customer_id,$this->business_id);
                        if (!$card) {
							$response = get_translation("no_cc_error", "business_account/profile_index",  array(),
                                                        "You must have a credit card on file before placing an order.", $this->customer_id);
                            set_alert('error', $response);
                            redirect('/account/profile/billing');

                            return;
                        }
                    }
                }
            }
        }

        if (!$this->checkOrderLimit()) {
            return;
        }

        $customerObj = new Customer($this->customer_id);
        $content['default_location'] = $customerObj->location_id;

        if ( ($content['default_location'] == 0) && (count($customer_locations) == 1) ) {
            $content['default_location'] = $customer_locations[0]->locationID;
        }

        if (!empty($_GET['loc'])) {
            $content['default_location'] = (int) $_GET['loc'];
        }

        $location = array();
        $location_dropdown = array();

        $has_home_pickup = false;

        foreach ($customer_locations as $customer_location) {
            $location[$customer_location->locationID] = array(
                'locationType'=> $customer_location->locationType,
                'lockerType'=>   ucfirst($customer_location->lockerType),
                'description'=>  ucfirst($customer_location->description),
                'address'=>      ucfirst($customer_location->address),
                'city'=>         ucfirst($customer_location->city),
                'state'=>        ucfirst($customer_location->state),
                'zipcode'=>      $customer_location->zipcode,
                'title' =>       ucfirst($customer_location->title)
            );

            // add lockers
            $lockers = $this->locker_model->get_all_useable_for_location($customer_location->locationID);

            $is_home_delivery = in_array($customer_location->serviceType, array(Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY, Location::SERVICE_TYPE_RECURRING_HOME_DELVIERY));
            if ($is_home_delivery) {
                $has_home_pickup = true;
            }

            // if there is no lockers, then remove the location from the UI
            if ( empty( $lockers ) ) {
                unset( $location[ $customer_location->locationID ] );
                continue;
            }

            foreach ($lockers as $locker) {
                $location[$customer_location->locationID]['lockers'][$locker->lockerName] = array('name'=>$locker->lockerName,'id'=>$locker->lockerID);
            }

            $value = $location[$customer_location->locationID];

            // if the location is marked as home pickup, enable home pickup and do not add to dropdown menu
            if (!$is_home_delivery) {
                $location_dropdown[$customer_location->locationID] = empty($value['title']) ? $value['address'] : $value['address'] . ' (' . $value['title'] . ')';
            }
        }
        natsort($location_dropdown);
        $content['location_dropdown'] = $location_dropdown;
        $content['customer'] = $this->customer;

        // if home pickup enabled, select the nearest home pickup location and add it to the list
        if ($has_home_pickup) {
            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            if ($homeDeliveryService) {
                $pickup_locations = $homeDeliveryService->getLocationAvailablesForCustomer($this->customer);
            }

            // if a pickup location was found in radius for this user
            if (!empty($pickup_locations)) {
                $pickup_id = $pickup_locations[0]->locationID;

                // get the pickup locker for this location
                $pickupLocker = $this->locker_model->get_one(array(
                    'lockerStatus_id' => 1,
                    'lockerStyle_id' => 5, //apartment
                    'location_id' => $pickup_id,
                ));

                // if pickup locker found
                if ($pickupLocker) {

                    // if not already in customer's locations
                    if (!isset($location[$pickup_id])) {

                        // add it to customer's location
                        $location[$pickup_id] = array(
                            'locationType'=> 'pickup',
                            'lockerType'=>   '',
                            'description'=>  'Home Pickup',
                            'address'=>      ucfirst($this->customer->address1),
                            'city'=>         ucfirst($this->customer->city),
                            'state'=>        ucfirst($this->customer->state),
                            'zipcode'=>      $this->customer->zipcode,
                            'title' =>       get_translation("home", "business_account/orders_new_order", array(), "Home"),
                        );
                        $location[$pickup_id]['lockers'] = array();
                    }

                    // set the pickup locker for the location
                    $location[$pickup_id]['pickup_locker'] = array(
                        'name' => $pickupLocker->lockerName,
                        'id' => $pickupLocker->lockerID
                    );

                    $content['pickup_location'] = $pickup_locations[0];
                    $content['default_location'] = $pickup_id;
                }
            }
        }

        $content['customer_locations'] = $location;

        $serviceTypes = unserialize($this->business->serviceTypes);
        $content['service_types'] = array();
        $content['service_types_render_order'] = array();
        foreach ($serviceTypes as $st_id => $serviceType) {
            if ($serviceType['active'] == 1) {
                $content['service_types'][ $st_id ] = $serviceType;
                $content['service_types_render_order'][] = $st_id;
            }
        }

        $content['service_types_by_key'] = array();
        foreach ($content['service_types'] as $st) {
            $st["displayName"] = get_translation(trim($st["displayName"]), "business_account/orders_new_order_service_types", array(), trim($st["displayName"]), $this->customer_id);
            $content['service_types_by_key'][ $st['serviceType_id'] ] = $st;
        }

        $content['location_services'] = $this->get_location_services();

        if ($this->session->flashdata('post')) {
            $_POST = unserialize($this->session->flashdata('post'));
        }
        $content['post'] = $_POST;

        $content['show_quantity_selector'] = get_business_meta($this->business_id, 'boxture_show_quantity_selector', 'no')=='no'?false:true;
        $content['quantity_options'] = explode(",", get_business_meta($this->business_id, 'boxture_quantity_selector_options', '1'));
        if (empty($content['quantity_options'])) $content['show_quantity_selector'] = false;

        $content['skip_menu'] = true;
        $this->renderPreferencesSidebar();
        $this->renderAccount('orders_new_order', $content);
    }

    protected function renderPreferencesSidebar($read_only = false)
    {
        //if true, this will hide the 'edit preferences' button
        $sidebar['read_only'] = $read_only;

        $this->load->library('productslibrary');
        $sidebar['businessPreferences'] = $this->productslibrary->getBusinessPreferences($this->business_id, $this->customer_id);

        $this->load->model('customerpreference_model');
        $sidebar['customerPreferences'] = $this->customerpreference_model->get_preferences($this->customer_id);
        $sidebar['customer_notes'] = $this->customer->customerNotes;

        $this->template->add_js("js/thickbox.js");
        $this->template->add_css("css/thickbox.css");
        $this->data['sidebar'][] = $this->load->view('/business_account/orders_new_order_sidebar', $sidebar, true);
    }

    protected function checkOrderLimit()
    {
        // is employee logged in as customer ?
        $old_buser_id = $this->session->userdata('old_buser_id');
        if ($old_buser_id) {
            return true;
        }

        $response = null;
        if (Claim::checkOrderLimit($this->customer_id, $this->business_id, $response)) {
            return true;
        }

        set_alert('error', $response);
        redirect('/account/');
        return false;
    }

    private function get_location_services()
    {
        $location_services_query = $this->db->get_where( 'location_services', array( 'business_id' => $this->business_id ) );
        $location_services = $location_services_query->result();
        $location_services_data = array();

        foreach ($location_services as $loc_service) {
            $location_services_data[ $loc_service->location_id ][ $loc_service->serviceType_id ] = $loc_service->serviceType_id;
        }

        return $location_services_data;
    }

    /**
     * view_orders method shows all the orders for a customer
     * claim_model has a get override in the model so we have to pass option array
     */
    public function view_orders()
    {
        //pending orders
        $this->load->model('claim_model');
        $claims = $this->claim_model->getCustomerActiveClaims($this->customer_id, $this->business_id);

        // show notes
        $this_order_only = get_translation("this_order_only", "customer_notes", array(), "THIS ORDER ONLY:", $this->customer_id);
        foreach ($claims as $i => $claim) {
            $parts = explode($this_order_only, $claim->notes, 2);
            $claims[$i]->userNotes = !empty($parts[1]) ? trim($parts[1]) : '';
        }

        $content['claims'] = $claims;

        // show laundry plans and auths
        $noBag = get_business_meta($this->business_id, 'list_laundy_plans');

        //past orders
        $this->load->model("order_model");

        $content['past_orders'] = array();
        $content['current_orders'] = array();

        $orders = $this->order_model->getCustomerOrderHistory($this->customer_id, $this->business_id, null, true, $noBag);
        foreach ($orders as $order) {
            if ($order->orderStatusOption_id == 10) {
                $content['past_orders'][] = $order;
            } else {
                $content['current_orders'][] = $order;
            }
        }


        $this->load->model("ordertype_model");
        $content['orderTypes'] = $this->ordertype_model->get_formatted_slug_name();

        $content['business'] = $this->business;
        $content['zone'] = $this->business->timezone;

        $this->renderAccount('orders_view_orders', $content);
    }

    public function update_claim_notes($claimID)
    {
        $this->load->model('claim_model');
        $claim = current($this->claim_model->get_aprax(array(
            'claimID' => $claimID,
            'customer_id' => $this->customer_id)
        ));

        if (!$claim) {
            $message = get_translation("claim_not_found", "update_notes",
                array(), "Order not found", $this->customer_id);

            set_alert('error', $message);
            redirect('/account/orders/view_orders');
        }

        $this_order_only = get_translation("this_order_only", "customer_notes", array(), "THIS ORDER ONLY:", $this->customer_id);
        $parts = explode($this_order_only, $claim->notes, 2);
        $parts[1] = convert_from_gmt_aprax('', "Y-m-d H:i:d", $this->business_id) . " - " . $_POST['notes'];
        $claim->notes = implode($this_order_only . " ", $parts);

        $this->claim_model->save(array(
            'claimID' => $claimID,
            'notes' => $claim->notes,
        ));

        $message = get_translation("claim_notes_updated", "update_notes",
            array(), "The Notes have been updated", $this->customer_id);

        set_alert('success', $message);

        redirect('/account/orders/view_orders');
    }

    public function update_order_notes($orderID)
    {
        $this->load->model('order_model');
        $order = current($this->order_model->get_aprax(array(
            'orderID' => $orderID,
            'customer_id' => $this->customer_id)
        ));

        if (!$order) {
            $message = get_translation("order_not_found", "update_notes",
                array(), "Order not found", $this->customer_id);

            set_alert('error', $message);
            redirect('/account/orders/view_orders');
        }

        if ($order->orderStatusOption_id != 1) {
            $message = get_translation("notes_not_editable", "update_notes",
                array(), "This order's notes can't be updated", $this->customer_id);

            set_alert('error', $message);
            redirect("/account/orders/order_details/$orderID");
        }

        $this_order_only = get_translation("this_order_only", "customer_notes", array(), "THIS ORDER ONLY:", $this->customer_id);
        $parts = explode($this_order_only, $order->notes, 2);
        $parts[1] = convert_from_gmt_aprax('', "Y-m-d H:i:d", $this->business_id) . " - " . $_POST['notes'];
        $order->notes = implode($this_order_only . " ", $parts);

        $this->order_model->save(array(
            'orderID' => $orderID,
            'notes' => $order->notes,
        ));

        $message = get_translation("order_notes_updated", "update_notes",
            array(), "The Notes have been updated", $this->customer_id);

        set_alert('success', $message);

        redirect("/account/orders/order_details/$orderID");
    }

    /**
     * Complete the claim - passed to this page from confirm_order
     */
    public function complete()
    {
        if (empty($_POST)) {
            redirect('/account/orders/new_order');
        }

        $this->load->helper('claim_helper');

        $location = new Location($_POST['location_id']);
        $locker = new Locker($_POST['locker_id']);

        $checkorder = check_order_exist($locker->lockerID);
        $multiOrder = $locker->is_multi_order();

        //if there IS an order AND the locker IS NOT multiorder
        if ($checkorder && !$multiOrder) {
            foreach ($checkorder as $o) {
                $order = new Order($o->orderID);

                // order belongs to this customer
                if ($this->customer_id == $order->customer_id) {
                    $message = get_translation("already_opened_order_view_order_error", "claim_errors",
                                                array("customer_support" => get_business_meta($this->business_id, "customerSupport")),
                                                "You already have an open order in this locker. To view your open orders, go to <a href='/account/orders/view_orders/'>My Orders</a>.  If you think this is in error, please call us at %customer_support%", $this->customer_id);
                    set_alert('error', $message);
                    redirect("/account/orders/new_order");
                }

                //some other customers order is in the locker
                if ($order->customer_id) {
                    $message = get_translation("another_user_order_error", "claim_errors",
                                                array("customer_support" => get_business_meta($this->business_id, "customerSupport")),
                                                "There is another customer's order currently in this locker. If you think this is in error, please call %customer_support%", $this->customer_id);

                    set_alert('error', $message);
                    redirect("/account/orders/new_order");
                }

                //use this order
                $bag = new Bag($order->bag_id);

                //bag found but the bag has customer_id, but is not the same
                if ($bag->customer_id && $bag->customer_id != $this->customer_id) {
                    $message = get_translation("bag_mismatch_error", "claim_errors",
                                                array("customer_support" => get_business_meta($this->business_id, "customerSupport")),
                                                "The bag in this locker does not belong to you. If you think this is in error, please call %customer_support%", $this->customer_id);

                    set_alert('error', $message);
                    redirect("/account/orders/new_order");
                }

                // bag has no customer, update it
                $bag->customer_id = $this->customer_id;
                $bag->save();

                $service_type = array();
                if ($_POST['service_type_serialized']) {
                    $type_array = unserialize($_POST['service_type_serialized']);
                    $this->load->model('serviceType_model');

                    $types = $this->serviceType_model->get_aprax(array(
                        'serviceTypeID' => $type_array,
                    ));

                    foreach ($types as $t) {
                        $name = $t->name;
                        if (!empty($businessServiceType[$t->serviceTypeID])) {
                            $name = $businessServiceType[$t->serviceTypeID]['displayName'];
                        }
                        $service_type[$t->serviceTypeID] = "- $name<br>";
                    }
                }
                $service_type = implode(', ', $service_type);
                $order->customer_id = $this->customer_id;
                $order->notes = convert_from_gmt_aprax('', "Y-m-d H:i:d", $this->business_id) . " - " . "Order Notes: ".$_POST['order_notes']."<br>".$service_type."<br>".$order->notes;
                $order->save();

                send_admin_notification("Order ({$order->orderID}) in Locker {$locker->lockerName} in location {$location->address} has been claimed",
                                        "Order ({$order->orderID}) in Locker {$locker->lockerName} in location {$location->address} has been claimed by " . getPersonName($this->customer));

                $message = get_translation("another_user_order_error", "claim_errors",
                    array("customer_support" => get_business_meta($this->business_id, "customerSupport")),
                    "There is another customer's order currently in this locker. If you think this is in error, please call %customer_support%", $this->customer_id);

                set_alert('error', $message);
                redirect("/account/orders/new_order");
            }

            throw new \Exception("Should never have reached here, please rever the commit that introduced this exception");

        } //end - after this no order exists


        // perform checks on the claim
        $results = check_claim_exists($locker->lockerID, $locker->is_multi_order());

        if ($results['status'] == 'fail') {
            set_alert('error', $results['message']);
            redirect("/account/orders/new_order");
        }

        $fullOrderNotes = convert_from_gmt_aprax('', "Y-m-d H:i:d", $this->business_id) . " - " . $_POST['order_notes'];

        //need to claim the locker
        $this->load->model('claim_model');
        $claim['customer_id'] = $this->customer_id;
        $claim['locker_id'] = $_POST['locker_id'];
        $claim['business_id'] = $this->business_id;
        $claim['order_notes'] = $fullOrderNotes;
        $claim['orderType'] =  $_POST['service_type_serialized'];

        if (!$claim_id = $this->claim_model->new_claim($claim)) {
            $message = get_translation("undefined_error", "claim_errors", array(), "Error claiming locker");

            set_alert('error', $message);
            redirect('/account/orders/new_order');
        }

        $action = 'new_claim';

        if ($this->input->post('pickup_window_id')) {
            if ($this->scheduleHomePickup($location->locationID, $claim_id)) {

                $message = get_translation("schedule_pickup_success", "business_account/orders_home_pickup",
                    array(), "Home pickup scheduled", $this->customer_id);
                set_alert("success", $message);

                $action = 'new_home_pickup';
            } else {

                // failed to setup claim, error message set in ::scheduleHomePickup()
                $this->load->model('claim_model');
                $this->claim_model->delete_aprax(array('claimID' => $claim_id));

                $message = get_translation("schedule_pickup_error", "business_account/orders_home_pickup",
                    array(), "An error occurred trying to schedule the order home pickup. Please, try again.", $this->customer_id);
                set_alert("error", $message);
                redirect('/account/orders/new_order');
            }
        }

        Email_Actions::send_transaction_emails(array(
            'do_action' => $action,
            'customer_id' => $this->customer_id,
            'claim_id' => $claim_id,
            'business_id' => $this->business_id
        ));

        //if this is a customer service order, put the account on hold
        $selectedTypes = unserialize($_POST['service_type_serialized']);
        if (in_array('12', $selectedTypes)) {
            $this->customer->hold = 1;
            $this->customer->save();
        }

        redirect('/account/orders/review/'.$claim_id);
    }

    public function review($claim_id)
    {
        $this->load->model('claim_model');
        $content['business'] = $this->business;

        $claim = $this->claim_model->get_claim($claim_id);

        // pickup date is calculated from this
        $time = time();

        // check for home pickup
        $homePickup = OrderHomePickup::search(array(
            'claim_id' => $claim_id,
            'pickup_id IS NOT NULL' => null,
        ));

        // if home pickup found, set time to expected delivery time
        if ($homePickup) {
            $pickupDay = $homePickup->deliver_by ? $homePickup->deliver_by : convert_from_gmt_aprax($homePickup->windowStart, "Y-m-d");
        } else {
            //get the expected pickup day
            $this->load->helper('claim_helper');
            $pickupDay = pickup_date($claim[0]->locationID, $time);
        }
        
        $this->claim_model->save(array(
            'claimID' => $claim_id,
            'pickupDate' => $pickupDay->date
        ));

        $this->renderAccount('orders_review', $content);
    }

    public function home_delivery_reschedule($order_id = null)
    {
        $order = new Order($order_id);

        // redirect to account page if the customer should not see this order
        if ($order->customer_id != $this->customer_id || $order->business_id != $this->business_id) {
            $message = get_translation("customer_order_mismatch", "business_account/orders_home_delivery",
                array(), "Sorry, that order belogs to a different customer account.", $this->customer_id);
            set_alert("error", $message);

            return redirect('/account/');
        }

        // customers on invoice cant order home delivery
        if ($this->customer->invoice) {
            $message = get_translation("customer_on_invoices_error", "business_account/orders_home_delivery",
                array(), "Sorry, home delivery is not available for customer on invoices.", $this->customer_id);
            set_alert("error", $message);

            return redirect("/account/orders/view_orders");
        }

        //valid statuses for home delivery: Inventoried, Ready for Customer pickup, Completed, Partially Delivered
        if (!in_array($order->orderStatusOption_id, array(3, 9, 10, 11))) {
            $message = get_translation("order_not_ready_error", "business_account/orders_home_delivery",
                array(), "Sorry, the order is not ready for home delivery.", $this->customer_id);
            set_alert("error", $message);

            return redirect("/account/orders/view_orders");
        }


        $service = get_business_meta($this->business_id, 'home_delivery_service');

        $homeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $order_id,
            'service'  => $service,
        ));

        if (empty($homeDelivery)) {
            $message = get_translation("order_reschedule_home_delivery_not_found_error", "business_account/orders_home_delivery",
                array(), "Sorry, we can't found this order.", $this->customer_id);
            set_alert("error", $message);

            return redirect("/account/orders/view_orders");
        }

        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);

        $status = $homeDeliveryService->cancelDelivery($homeDelivery);

        if ($status) {
            $homeDelivery->delete(array('orderHomeDeliveryID' => $homeDelivery->orderHomeDeliveryID));
            return redirect("/account/orders/home_delivery/".$order_id);
        }

        $message = get_translation("order_reschedule_delivery_error", "business_account/orders_home_delivery",
                array(), "Sorry, there was an error trying to reschedule home delivery.", $this->customer_id);
        set_alert("error", $message);

        return redirect("/account/orders/view_orders"); 
    }

    public function home_delivery($order_id = null, $step = 1)
    {
        $content['order'] = $order = new Order($order_id);
        $content['step'] = $step;

        // redirect to account page if the customer should not see this order
        if ($order->customer_id != $this->customer_id || $order->business_id != $this->business_id) {
            $message = get_translation("customer_order_mismatch", "business_account/orders_home_delivery",
                array(), "Sorry, that order belogs to a different customer account.", $this->customer_id);
            set_alert("error", $message);

            return redirect('/account/');
        }

        // customers on invoice cant order home delivery
        if ($this->customer->invoice) {
            $message = get_translation("customer_on_invoices_error", "business_account/orders_home_delivery",
                array(), "Sorry, home delivery is not available for customer on invoices.", $this->customer_id);
            set_alert("error", $message);

            return redirect("/account/orders/view_orders");
        }

        //valid statuses for home delivery: Inventoried, Ready for Customer pickup, Completed, Partially Delivered
        if (!in_array($order->orderStatusOption_id, array(3, 9, 10, 11))) {
            $message = get_translation("order_not_ready_error", "business_account/orders_home_delivery",
                array(), "Sorry, the order is not ready for home delivery.", $this->customer_id);
            set_alert("error", $message);

            return redirect("/account/orders/view_orders");
        }

        $content['locker'] = $locker = new Locker($order->locker_id);
        $content['location'] = $location = new Location($locker->location_id);
        $content['accessCode'] = $accessCode = substr(trim($this->customer->phone), -4); // last 4 of the customers phone

        // check that the customer can use the service
        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
        try {
            $response = $homeDeliveryService->addOrderToExisingHomeDelivery($order);
            if ($response['status'] == 'success') {
                $message = get_translation("order_added_to_home_delivery", "business_account/orders_home_delivery",
                    array(), "There was already a home delivery scheduled and this order has been included.", $this->customer_id);
                set_alert("success", $message);

                return redirect("/account/orders/view_orders");
            }

        } catch (Exception $e) {
            send_exception_report($e);
        }

        // create or get the home delivery object
        $homeDelivery = $this->getHomeDelivery($order_id, $locker->location_id);

        // dont allow orders scheduled to be re-scheduled
        if ($homeDelivery->delivery_id) {
            $message = get_translation("already_scheduled_delivery_error", "business_account/orders_home_delivery",
                array(), "Sorry, home delivery for this order has already been scheduled and can't be changed.", $this->customer_id);
            set_alert("error", $message);

            return redirect("/account/orders/view_orders");
        }

        $content['can_continue'] = true;

        if (!$homeDeliveryService->canCustomerRequestHomeDeliveryToLocation($this->customer, $location)) {
            $message = get_translation("location_out_of_raius", "business_account/orders_home_delivery",
                array(
                    'address' => $this->customer->address1,
                    'address2' => $this->customer->address2,
                    'city' => $this->customer->city,
                    'state' => $this->customer->state,
                    'zip' => $this->customer->zip,
                ),
                "<b>Sorry!</b> We don't offer delivery to %address%", $this->customer_id);
                set_alert_now("error", $message);
            $content['can_continue'] = false;
        }

        // zipcode is required for home delivery
        if (empty($homeDelivery->zip)) {
            $message = get_translation("missing_zip_code", "business_account/orders_home_delivery",
                array(), "<b>Sorry!</b> You need to provide a valid zipcode for home delivery to work", $this->customer_id);
            set_alert_now("error", $message);
            $content['can_continue'] = false;
        }

        $content['homeDelivery'] = $homeDelivery;

        // update delivery address flow
        if (!empty($_POST['update_address'])) {
            $this->updateHomeDeliveryAddress($homeDelivery, $location);
            return redirect("/account/orders/home_delivery/$order_id/" . max(2, $step));
        }

        // if a POST get this far, we are setting up a home delivery request
        if (!empty($_POST)) {
            $homeDelivery->deliveryDate = $_POST['deliveryDate'];
            $homeDelivery->windowStart = $_POST['windowStart'];
            $homeDelivery->windowEnd = $_POST['windowEnd'];
            
            //format dates to mysql
            $homeDelivery->windowStart = date("Y-m-d H:i:s", strtotime($homeDelivery->windowStart));
            $homeDelivery->windowEnd = date("Y-m-d H:i:s", strtotime($homeDelivery->windowEnd));

            $homeDelivery->delivery_window_id = $_POST['delivery_window_id'];
            $homeDelivery->notes = $_POST['notes'];
            $homeDelivery->save();
        }

        // change step
        if (!empty($_POST['goto'])) {
            $goto = $_POST['goto'];

            if ($goto > 2) {
                // validate that we have delivery_window_id set, if not, back to step 2
                if (empty($homeDelivery->delivery_window_id)) {
                    $goto = 2;
                    $message = get_translation("missing_delivery_window_id", "business_account/orders_home_delivery",
                        array(), "<b>Sorry!</b> You need to select a delivery window", $this->customer_id);
                    set_alert("error", $message);
                }
            }

            return redirect("/account/orders/home_delivery/$order_id/" . $goto);
        }

        // if not at the start, then we need the delivery windows
        if ($step >= 2) {
            if (!$deliveryWindows = $this->setDeliveryDatesAndTimes($content, $homeDelivery, $location)) {

                $message = get_translation("no_delivery_windows_available", "business_account/orders_home_delivery",
                    array(), "Sorry, there are no delivery windows available.", $this->customer_id);
                set_alert("error", $message);

                return redirect("/account/orders/home_delivery/$order_id/1");
            }
        }

        // complete the home delivery, this is the final step
        if (!empty($_POST['schedule'])) {
            if ($this->scheduleHomeDelivery($homeDelivery)) {
                return redirect("/account/orders/view_orders");
            } else {
                return redirect("/account/orders/home_delivery/$order_id/" . $step);
            }
        }

        // constants
        $content['customer'] = $this->customer;
        $content['date_format'] = get_business_meta($this->business_id, 'standardDateLocaleFormat', '%A, %B %e');
        $content['business_country'] = $this->business->country;
        $this->load->model('ordertype_model');
        $content['orderTypes'] = $this->ordertype_model->get_formatted_slug_name();

        $content['time_format'] = "g:ia";
        $service = get_business_meta($this->business_id, 'home_delivery_service');
        if ($service == 'boxture') {
            $content['time_format'] = "ga"; //24 hours format
        }

        $this->renderAccount('orders_home_delivery', $content);
    }

    /**
     * Finds or creates a HomeDelivery object for the order
     *
     * @param int $order_id
     * @param int $location_id
     * @return App\Libraries\DroplockerObjects\OrderHomeDelivery
     */
    protected function getHomeDelivery($order_id, $location_id)
    {
        $service = get_business_meta($this->business_id, 'home_delivery_service');

        $homeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $order_id,
            'service'  => $service,
        ));

        if (!$homeDelivery) {
            $homeDelivery = new OrderHomeDelivery();
        }

        $homeDelivery->business_id        = $this->business_id;
        $homeDelivery->customer_id        = $this->customer_id;
        $homeDelivery->order_id           = $order_id;
        $homeDelivery->location_id        = $location_id;
        $homeDelivery->state              = $this->customer->state;
        $homeDelivery->city               = $this->customer->city;
        $homeDelivery->address1           = $this->customer->address1;
        $homeDelivery->address2           = $this->customer->address2;
        $homeDelivery->zip                = $this->customer->zip;
        $homeDelivery->lat                = $this->customer->lat;
        $homeDelivery->lon                = $this->customer->lon;
        $homeDelivery->service            = $service;
        $homeDelivery->notes              = ""; 
        $homeDelivery->payment_order_id   = 0; 
        $homeDelivery->delivery_response  = 0; 

        $boxture_show_quantity_selector = get_business_meta($this->business_id, 'boxture_show_quantity_selector', 'no')=='no'?false:true;

        if ($boxture_show_quantity_selector) {
            $homePickup = OrderHomePickup::search(array(
                'order_id' => $homeDelivery->order_id,
                'location_id' => $homeDelivery->location_id,
                'customer_id' => $homeDelivery->customer_id,
                'service' => $service,
            ));

            if (!empty($homePickup->quantity)) {
                $homeDelivery->quantity = $homePickup->quantity;
            } else {
               $homeDelivery->quantity = 1; 
            }
        }

        $homeDelivery->save();

        return $homeDelivery;
    }

    /**
     * Validates and updates the home delivery address
     *
     * This is the logic for step 1 of the wizard.
     *
     * @param OrderHomeDelivery $homeDelivery
     * @param Location $location
     * @return boolean
     */
    protected function updateHomeDeliveryAddress($homeDelivery, $location)
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('zip', 'Zipcode', 'required|min_length[5]');

        if (!$this->form_validation->run()) {
            set_alert("error", validation_errors());
            return false;
        }

        // Create a copy of the customer and set the new address
        $customer = new Customer($this->customer_id);
        $customer->address1 = $_POST['address'];
        $customer->address2 = $_POST['address2'];
        $customer->city = $_POST['city'];
        $customer->state = $_POST['state'];
        $customer->zip = $_POST['zip'];

        // also update coords
        $geo = geocode($customer->address1, $customer->city, $customer->state);
        $customer->lat = str_replace(',', '.', $geo['lat']);
        $customer->lon = str_replace(',', '.', $geo['lng']);

        // then use the Home Delivery Service to check the new address
        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
        if (!$homeDeliveryService->canCustomerRequestHomeDelivery($customer)) {
            $message = get_translation("location_out_of_raius", "business_account/orders_home_delivery",
                array(
                    'address' => $customer->address1,
                    'address2' => $customer->address2,
                    'city' => $customer->city,
                    'state' => $customer->state,
                    'zip' => $customer->zip,
                ),
                "<b>Sorry!</b> We don't offer delivery to %address%", $this->customer_id);

            set_alert("error", $message);

            return false;
        }

        $homeDelivery->address1 = $_POST['address'];
        $homeDelivery->address2 = $_POST['address2'];
        $homeDelivery->city     = $_POST['city'];
        $homeDelivery->state    = $_POST['state'];
        $homeDelivery->zip      = $_POST['zip'];
        $homeDelivery->lat      = $lat;
        $homeDelivery->lon      = $lon;
        $homeDelivery->save();

        $message = get_translation("update_delivery_address", "business_account/orders_home_delivery",
            array(), "Updated home delivery address", $this->customer_id);

        set_alert("success", $message);

        return true;
    }

    /**
     * Sets the delivery windows and the delivery fee
     *
     * This is the logic for step 2 of the wizard.
     *
     * This will call Deliv's API and fetch available delivery windows, then it will format
     * those windows in groups by date and fee. It will also convert the hours into the business's
     * local time.
     *
     * This function will updates the fee if a delivery window is selected
     * in the OrderHomeDelivery object.
     *
     * This takes $content by reference in order to pass the delivery windows to the view.
     *
     * @param array $content
     * @param OrderHomeDelivery $homeDelivery
     * @param Location $location
     */
    protected function setDeliveryDatesAndTimes(&$content, $homeDelivery, $location)
    {
        try {
            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            $windows_by_date = $homeDeliveryService->getDeliveryWindows($homeDelivery);
            $content['delivery_dates'] = array_keys($windows_by_date);
            $content['delivery_windows'] = $homeDeliveryService->splitWindowsByFees($windows_by_date);

            return $windows_by_date;
        } catch (Exception $e) {

            send_exception_report($e);

            $error = get_translation("empty_delivery_windows", "business_account/orders_home_delivery",
                array(), "An error ocurred while fetching the delivery windows", $this->customer_id);

            set_alert('error', $error);

            return false;
        }
    }

    /**
     * Finish the schedule of the home delivery
     *
     * This will create and capture an order for the fee of the home delivery.
     *
     * If the funds are captured, then a it will send a request to Deliv's API
     * and schedule the request.
     *
     * This is the logic for step 3 of the wizard.
     *
     * @param OrderHomeDelivery $homeDelivery
     * @param Location  $location
     * @param Locker $locker
     * @param string $accessCode
     * @param Array $deliveryWindows
     * @return boolean
     */
    protected function scheduleHomeDelivery($homeDelivery)
    {
        if (!$homeDelivery->delivery_id) {
            try {
                $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
                $homeDeliveryService->scheduleHomeDelivery($homeDelivery);

                $message = get_translation("schedule_delivery_success", "business_account/orders_home_delivery",
                    array(), "Home delivery scheduled", $this->customer_id);
                set_alert("success", $message);

                return true;

            } catch (Exception $e) {
                send_exception_report($e);
                $message = get_translation("schedule_delivery_error", "business_account/orders_home_delivery",
                    array(), "An error occurred trying to schedule the order home delivery. Please, try again.", $this->customer_id);
                set_alert("error", $message);

                return false;
            }
        }

        return true;
    }

    /**
     * Renders via AJAX the form elements to select a pickup window
     *
     * @param int $location_id
     */
    public function home_pickup($location_id)
    {
        try {
            $location = new Location($location_id);

            $content = array();
            $content['location'] = $location;
            $content['date_format'] = get_business_meta($this->business_id, 'standardDateLocaleFormat', '%A, %B %e');
            $content['homePickup'] = $homePickup = $this->getHomePickup($location_id);

            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            $windows_by_date = $homeDeliveryService->getPickupWindows($homePickup);

            if (!$windows_by_date) {
                throw new Exception("Error getting time windows. Please refresh this page and try again.");
            }

            $content['pickup_dates'] = array_keys($windows_by_date);
            $content['pickup_windows'] = $homeDeliveryService->splitWindowsByFees($windows_by_date);

            $content['time_format'] = "g:ia";
            $service = get_business_meta($this->business_id, 'home_delivery_service');
            if ($service == 'boxture') {
                $content['time_format'] = "ga"; //24 hours format
            }
        } catch (Exception $e) {
            set_alert('error', $e->getMessage());
            $this->session->_flashdata_mark(); // pass alert messages to the view
        }

        $this->load->view('business_account/orders_home_pickup', $content);
    }

    /**
     * Finds or creates a HomePickup object for the customer
     *
     * @param object $customer
     * @param int $location_id
     * @return App\Libraries\DroplockerObjects\OrderHomePickup
     */
    protected function getHomePickup($location_id, $save = true)
    {
        $service = get_business_meta($this->business_id, 'home_delivery_service');

        $homePickup = OrderHomePickup::search(array(
            'claim_id' => 0,
            'location_id' => $location_id,
            'customer_id' => $this->customer_id,
            'service' => $service,
        ));

        if (!$homePickup) {
            $homePickup = new OrderHomePickup();
        }

        $homePickup->business_id        = $this->business_id;
        $homePickup->customer_id        = $this->customer_id;
        $homePickup->order_id           = 0;
        $homePickup->location_id        = $location_id;
        $homePickup->state              = $this->customer->state;
        $homePickup->city               = $this->customer->city;
        $homePickup->address1           = $this->customer->address1;
        $homePickup->address2           = $this->customer->address2;
        $homePickup->zip                = $this->customer->zip;
        $homePickup->lat                = $this->customer->lat;
        $homePickup->lon                = $this->customer->lon;
        $homePickup->service            = $service;
        $homePickup->notes              = ""; 
        $homePickup->payment_order_id   = 0; 
        $homePickup->pickup_response    = 0; 
        $homePickup->claim_id           = 0;
        if ($save) {
            $homePickup->save();            
        }

        return $homePickup;

    }
    
    /**
     * Finish the schedule of the home pickup
     *
     * Will send a request to Deliv's API  and schedule the request.
     *
     * @param int $location_id
     * @param int $claim_id
     * @return boolean
     */
    protected function scheduleHomePickup($location_id, $claim_id)
    {
        $homePickup = $this->getHomePickup($location_id, false);
        $homePickup->claim_id = $claim_id;
        $homePickup->notes = $_POST['pickup_notes'];
        $homePickup->save();

        if (!$homePickup->pickup_id) {
            try {
                $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
                $homeDeliveryService->scheduleHomePickup($homePickup);
            } catch (Exception $e) {
                //continue with exception
                send_exception_report($e);

                $customer = $this->customer;
                $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
                $ip_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

                $subject = "Home Delivery Error: {$e->getMessage()}";
                $body = "<pre>
                Client IP Address: {$ip_addr}
                HTTP Referer: {$referer}
                Request URI: {$_SERVER['REQUEST_URI']}
                Message: {$e->getMessage()}

                Customer Information
                Name: " .  getPersonName($customer) . "
                Phone: {$customer->phone}
                SMS: {$customer->sms}
                E-mail: {$customer->email}
                Customer Address: " . getFormattedAddress($customer, 'address', '##skip address 2') . "

                </pre>";

                send_admin_notification($subject, $body, $this->business_id, $this->customer_id);

                return false;
            }
        }

        return true;
    }

}
