<?php

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderPaymentStatus;
use App\Libraries\DroplockerObjects\Location;

/**
 * class Account
 * location: account/account.php
 */
class Profile extends MY_Account_Controller
{
    protected $jQueryUpgradedBusiness = array(
        3,
    );
    
    public function __construct()
    {
        parent::__construct();

        $this->requireLogin();
        
        if (in_array($this->business_id, $this->jQueryUpgradedBusiness)) {
            $this->template->add_js('js/jquery-ui-1.8.24.min.js');
        } else {
            $this->template->add_js('js/jquery-ui-1.8.16.custom.min.js');
        }
        
        $this->template->add_css('css/blue_custom/jquery-ui-1.8.17.custom.css');

        $this->template->add_js('js/account/profile/fix_tables.js');
    }

    public function index()
    {
        redirect("/account/profile/edit");
    }

    /**
     * Updates a customer's profile.
     * Required the following POST parameters
     * zipcode
     * firstName
     * lastName
     * phone
     *
     */
    public function update()
    {
        $this->load->library('form_validation');

        $zipcode_rules = array(
            'usa' => array('name' => 'Zipcode', 'rule' => 'min_length[5]'),
            'canada' => array('name' => 'Zipcode', 'rule' => 'min_length[5]'),
            'australia' => array('name' => 'Zipcode', 'rule' => 'min_length[4]'),
            'netherlands' => array('name' => 'Zipcode', 'rule' => 'callback_validateCustomZipCodeByCountry')
        );

        if (isset($zipcode_rules[$this->business->country])) {
            $rule = $zipcode_rules[$this->business->country];
            $this->form_validation->set_rules('zipcode', $rule['name'], $rule['rule']);
        }

        $this->form_validation->set_rules("phone", "Phone", "required");
        $this->form_validation->set_rules("firstName", "First Name", "required");
        $this->form_validation->set_rules("lastName", "Last Name", "required");

        if (!$this->form_validation->run()) {
            set_alert("error", validation_errors());

            //store the POST data into a flash variable to repopulate the form
            $this->session->set_flashdata("post", serialize($_POST));
            redirect("/account/profile/edit");
        }

        if ((strtolower(trim($this->input->post('firstName'))) == 'sms') || (strtolower(trim($this->input->post('lastName'))) == 'user')) {
            $message = get_translation("change_sms_user_name_profile", "business_account/profile_edit", array(), "Please, you must input your full real name. This is necessary so you can update your billing information.");
            set_alert('error', $message);
            redirect('account/profile/edit');
        }

        $customer = new Customer($this->customer_id);

        // used to tell if phone needs to be copied to sms
        $phone_changed = $this->customer->phone != $this->input->post('phone');
        $sms_changed = $this->customer->sms != $this->input->post('sms');
        $copy_sms = get_business_meta($this->business_id, 'copy_phone_to_sms');

        //sms settings check to see how to save the SMS number
        $business_sms_settings = $this->db->get_where('smsSettings', array(
            'business_id' => $this->business_id
        ))->row();

        //if we have sms settings, then just save the number, don't worry about the carrier stuff
        if (!empty($business_sms_settings)) {
            $sms_number = $this->input->post('sms');
            if ($copy_sms && $phone_changed && !$sms_changed) {
                $sms_number = $this->input->post('phone');
            }

            //some formatting
            $sms_number = clean_phone_number($sms_number);
            $sms_number = ltrim($sms_number, '1');
            $customer->sms = $sms_number;

            if (isset($_POST['sms2'])) {
                $sms_number2 = $this->input->post('sms2');
                $sms_number2 = clean_phone_number($sms_number2);
                $sms_number2 = ltrim($sms_number2, '1');
                $customer->sms2 = $sms_number2;
            }
        } else {

            // list of sms providers
            $providers = $this->config->item('providers');

            $number = $this->input->post('sms');
            $provider = $this->input->post('cellProvider');
            $number = clean_phone_number($number);

            //otherwise save the carrier and setup the email-to-sms string
            if ($number && $provider) {
                //combine the number and the provider
                $customer->sms = $number . "@" . $providers[$provider];
            } else {
                $customer->sms = '';
            }

            if (isset($_POST['sms2'])) {
                $number2 = $this->input->post('sms2');
                $number2 = clean_phone_number($number2);
                $provider2 = $this->input->post('cellProvider2');

                if ($number2 && $provider2) {
                    //combine the number and the provider
                    $customer->sms2 = $number2 . "@" . $providers[$provider2];
                } else {
                    $customer->sms2 = '';
                }
            }
        }

        // geocode new address
        $geo = geocode($this->input->post('address'), $this->input->post('city'), $this->input->post('state'));
        $customer->lat = $geo['lat'];
        $customer->lon = $geo['lng'];

        $address_changed = $this->customer->address1 != $this->input->post('address');

        $customer->address1 = $this->input->post('address');
        //check if the customer's address1 property has changed
        if (!empty($this->customer->address1) && $this->customer->address1 != $this->input->post("address")) {
            $subject = "Addresss Updated [{$this->customer_id}]";
            $message = sprintf(
                "Address2 updated from '%s' to '%s'.<br>
                    If this customer is not at a locker location, you will want to edit
                    their last order location so their next order is sent to their new location.<br><br>
                    <a href='http://droplocker.com/admin/customers/detail/%s'>View customer account</a>",
                $this->customer->address1,
                $this->input->post('address'),
                $this->customer_id
            );
            send_admin_notification($subject, $message, $this->business_id);
        }

        $customer->address2 = $this->input->post('address2');
        //check if the customer's address2 property has changed
        if (!empty($this->customer->address2) && $this->customer->address2 != $this->input->post("address2")) {
            $subject = "Addresss Updated [{$this->customer_id}]";
            $message = sprintf(
                    "Address2 updated from '%s' to '%s'.<br>
                    If this customer is not at a locker location, you will want to edit
                    their last order location so their next order is sent to their new location.<br><br>
                    <a href='http://droplocker.com/admin/customers/detail/%s'>View customer account</a>",
                    $this->customer->address2,
                    $this->input->post('address2'),
                    $this->customer_id
                );
            send_admin_notification($subject, $message, $this->business_id);
        }

        $customer->city = $this->input->post('city');
        $customer->state = $this->input->post('state');
        $customer->zip = $this->input->post('zipcode');
        $customer->firstName = $this->input->post('firstName');
        $customer->lastName = $this->input->post('lastName');
        $customer->location_id = $this->input->post('location_id');

        if ($address_changed) {
            $this->clearHomePickup($customer);
        }

        $phone_number = $this->input->post('phone');
        $phone_number = clean_phone_number($phone_number);
        $customer->phone = $phone_number;

        //check if the phone number has changed
        if (!empty($this->customer->phone) && $this->customer->phone != $phone_number) {
            if ($this->business_id == 3) {
                $subject = "Phone Updated [{$this->customer_id}]";
                $message = sprintf(
                    "Phone updated from '%s' to '%s'.<br>
                    You may need to print a new bag tag for this customer.<br><br>
                    <a href='http://droplocker.com/admin/customers/detail/%s'>View customer account</a>",
                    $this->customer->phone,
                    $phone_number,
                    $this->customer_id
                );
                send_admin_notification($subject, $message, $this->business_id);
            }
        }

        // birthday
        $birth_month = $this->input->post('birth_month');
        $birth_day   = $this->input->post('birth_day');
        $birth_year  = $this->input->post('birth_year');

        //only care if they've put in the whole thing
        if ($birth_month && $birth_day && $birth_year) {
            $formatted_birthday = date('Y-m-d', mktime(0, 0, 0, $birth_month, $birth_day, $birth_year));
            $customer->birthday = $formatted_birthday;
        }

        if ($this->input->post('hourSpan')) {
            $hourSpan = explode("-", $this->input->post('hourSpan'));
            if ($hourSpan[0] == 0 && $hourSpan[1] == 24) {
                $customer->emailWindowStart = "00:00:00";
                $customer->emailWindowStop = "00:00:00";
            } else {
                $customer->emailWindowStart = convert_to_gmt_aprax("{$hourSpan[0]}:00:00", $this->business->timezone, "H:i:s");
                $customer->emailWindowStop = convert_to_gmt_aprax("{$hourSpan[1]}:00:00", $this->business->timezone, "H:i:s");
            }
        }

        if ($this->input->post("default_business_language_id")) {
            $customer->default_business_language_id = $this->input->post("default_business_language_id");
        }
        
        if ($customer->save()) {
            $this->load->model("creditcard_model");
            $this->creditcard_model->updateBillingInfo($customer, $this->business_id);
        }

        update_customer_meta($customer->customerID, "ada_deliver_to_lower_locker", (int)$this->input->post("ada_deliver_to_lower_locker"));

        set_alert("success", get_translation(
            "update_profile_success_message",
            "business_account/profile_index",
            array(),
            "Updated Profile",
            $this->customer_id
        ));

        redirect("/account/profile/edit");
    }

    public function validateCustomZipCodeByCountry()
    {
        $zip_code = $this->input->post('zipcode');
        $country_code = strtolower($this->business->country);

        $zip_preg=array(
            "netherlands"=>"^[1-9][0-9]{3}?[A-Z]{2}?$"
        );

        if ($zip_preg[$country_code]) {
            if (!preg_match("/".$zip_preg[$country_code]."/i", $zip_code)) {
                $this->form_validation->set_message('validateCustomZipCodeByCountry', get_translation(
                    "update_profile_wrong_zipcode",
                    "business_account/profile_update",
                array(),
                    "Zipcode need to have 4 numbers and 2 uppercase letters without spaces",
                    $this->customer_id
                ));
                return false;
            }
        }
        return true;
    }

    /**
     * Removes out of range home pickup locaions when the customer address changes
     * @param Customer $customer
     */
    protected function clearHomePickup($customer)
    {
        $this->load->model('location_model');
        $pickup_locations = $this->location_model->get_customer_locations($customer->customerID, array(
            'serviceType' => Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY,
        ));

        // if the customer does not have home pickup setup
        if (empty($pickup_locations)) {
            return;
        }

        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);

        // check every pickup location added if in range
        foreach ($pickup_locations as $location) {
            if (!$homeDeliveryService || !$homeDeliveryService->canCustomerRequestHomeDeliveryToLocation($this->customer, $location)) {
                $this->location_model->delete_customer_location(array(
                    'customer_id' => $customer->customerID,
                    'location_id' => $location->locationID,
                ));

                if ($customer->location_id == $location->locationID) {
                    $customer->location_id = null;
                }
            }
        }
    }


    /**
     * Renders the interface for a customer to edit his or her profile.
     */
    public function edit()
    {
        //pull the business sms settings to determine whether or not to show the carrier form
        $business_sms_settings = $this->db->get_where('smsSettings', array(
            'business_id' => $this->business_id
        ))->row();

        $this->content['show_carriers'] = empty($business_sms_settings);

        // sms providers
        $providers = $this->config->item('providers');

        //get the proper sms format
        list($sms, $provider) = explode("@", $this->customer->sms . '@');
        $this->content['sms'] = $sms;
        $this->content['cellProvider'] = array_search($provider, $providers);

        list($sms2, $provider2) = explode("@", $this->customer->sms2 . '@');
        $this->content['sms2'] = $sms2;
        $this->content['cellProvider2'] = array_search($provider2, $providers);

        if ($this->session->flashdata('post')) {
            $_POST = unserialize($this->session->flashdata('post'));
        }

        $this->content['emailWindowStart'] = ($this->customer->emailWindowStart == '00:00:00') ? 0 : convert_from_gmt_aprax($this->customer->emailWindowStart, "G", $this->business_id);
        $this->content['emailWindowStop'] = ($this->customer->emailWindowStop == '00:00:00') ? 24 : convert_from_gmt_aprax($this->customer->emailWindowStop, "G", $this->business_id);
        if ($this->content['emailWindowStop'] == 0) {
            $this->content['emailWindowStop'] = 24;
        }

        $this->content['business_country'] = $this->business->country;
        $this->load->model("business_language_model");
        $this->content['business_languages'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id, $this->customer_id);

        $this->load->model('location_model');
        $this->load->model('customer_location_model');
        /* location logic here */
        $this->content['current_location'] = $this->customer->location_id;

        if (empty($this->business->show_locations_map)) {
            $customer_locations = $this->location_model->get(array(
                'business_id' => $this->business_id,
                'with_location_type' => true,
                'inService' => true
            ));
        } else {
            $customer_locations = $this->customer_location_model->get_customer_locations($this->customer_id, $this->business_id);
        }

        $location_dropdown = array();

        // if there is no lockers, then remove the location from the UI
        $this->load->model('locker_model');
        foreach ($customer_locations as $key => $cust_loc) {
            $lockers = $this->locker_model->get_all_useable_for_location($cust_loc->locationID);
            if (empty($lockers)) {
                continue;
            }

            $location_dropdown[$cust_loc->locationID] = empty($cust_loc->companyName) ? $cust_loc->address : substr($cust_loc->address, 0, 40) . ' (' . substr($cust_loc->companyName, 0, 40) . ')';
        }

        natsort($location_dropdown);
        $this->content['location_dropdown'] = $location_dropdown;
        $this->content['require_address2'] = get_business_meta($this->business_id, "require_address2", 0);
        $this->renderAccount('profile_index', $this->content);
    }


    /**
     * UI for the customer to change their password
     */
    public function password()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('new_password', 'New Password', 'required');

        if ($this->input->post()) {
            if (!$this->form_validation->run()) {
                set_alert("error", get_translation(
                    "password_empty",
                    "business_account/profile_password",
                   array(),
                    "The password can't be empty",
                    $this->customer_id
                ));

                redirect('/account/profile/password');
            }

            if ($_POST['new_password'] != $_POST['new_password_again']) {
                set_alert("error", get_translation(
                    "passwords_do_not_match",
                    "business_account/profile_password",
                    array(),
                    "New Passwords do not match",
                    $this->customer_id
                ));

                redirect('/account/profile/password');
            }

            $Customer = new Customer($this->customer_id);
            $Customer->password = $_POST['new_password'];
            $Customer->save();

            set_alert("success", get_translation(
                "password_updated",
                "business_account/profile_password",
                array(),
                "Password has been updated",
                $this->customer_id
            ));

            redirect('/account/profile/edit');
        }

        $this->renderAccount('profile_password', $this->content);
    }


    /**
     * removes a bounce block for an account
     */
    public function removeBounceBlock()
    {
        $this->customer->bounced = 0;
        $this->customer->save();

        set_alert('success', get_translation(
            "email_unblocked",
            "business_account/profile_removeBounceBlock",
            array(),
            "Email address has been un-blocked.",
            $this->customer_id
        ));

        redirect('/account/');
    }


    /**
     * UI for customer to request a email change.
     *
     * A confirmation email is sent to the new email address. The link guuides the customer back to
     * account/main/verify_new_email
     */
    public function email()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('new_email', 'New Email', 'required|valid_email');
        $this->form_validation->set_message(array(
            'required' => get_translation("new_email_required", "update_email_account", array(), 'The New Email field is required', $this->customer_id),
            'valid_email' => get_translation("new_email_invalid", "update_email_account", array(), 'The New Email is not a valid email', $this->customer_id),
        ));

        if ($this->form_validation->run()) {
            $new_email = trim($this->input->post('new_email'));

            if ($new_email == $this->customer->email) {
                set_alert('error', get_translation(
                    "same_email_error",
                    "update_email_account",
                    array(),
                    "Please enter a new email address.",
                    $this->customer_id
                ));

                return redirect('/account/profile/email');
            }

            $found = $this->customer->search_aprax(array(
                'business_id' => $this->customer->business_id,
                'email' => $new_email,
            ));

            if ($found) {
                set_alert('error', get_translation(
                    "email_already_used_error",
                    "update_email_account",
                    array('new_email' => $new_email),
                    "Email address '%new_email%' is already in use by another customer. Please enter a new email address.",
                    $this->customer_id
                ));
                return redirect('/account/profile/email');
            }

            $this->load->helper('customer');

            if (send_verify_email_email($this->customer, $new_email)) {
                set_alert('success', get_translation(
                    "verify_email_sent",
                    "update_email_account",
                    array('new_email' => $new_email),
                    "Email has been sent to %new_email% with a verification link.",
                    $this->customer_id
                ));
                redirect('/account/profile/email');
            } else {
                set_alert('error', get_translation(
                    "verify_email_error",
                    "update_email_account",
                    array(),
                    "There was a problem sending email",
                    $this->customer_id
                ));
                redirect('/account/profile/email');
            }
        }

        $this->renderAccount('profile_email', $this->content);
    }


    /**
     * Update customer's laundry preferences
     */
    public function preferences()
    {
        // update the prefs if form is submitted
        if ($this->input->post()) {
            $this->saveCustomerPreferences();
            set_alert('success', get_translation(
                "preference_update_success_message",
                "business_account/profile_preferences",
                array(),
                'Updated Preferences',
                $this->customer_id
            ));
            redirect('/account/profile/preferences');
        }

        // set all the options for all the modules
        $this->load->library('productslibrary');
        $this->content['businessPreferences'] = $this->productslibrary->getBusinessPreferences($this->business_id, $this->customer_id);

        $this->load->model('customerpreference_model');
        $this->content['customerPreferences'] = $this->customerpreference_model->get_preferences($this->customer_id);
        $this->content['customer_notes'] = $this->customer->customerNotes;


        $this->renderAccount('profile_preferences', $this->content);
    }


    /**
     * Save customer preferences via AJAX
     */
    public function update_preferences_ajax()
    {
        if ($this->input->post()) {
            $this->saveCustomerPreferences();
        }

        $preferences = $this->input->post('preferences');

        //need to set an error case
        echo json_encode(array('status' => 1, 'preferences' => $preferences));
        exit;
    }


    /**
     * Does the actual saving of customer preferences
     */
    protected function saveCustomerPreferences()
    {
        // TODO: this will be moved into a preference in the DC module
        $customer = new Customer($this->customer_id);
        $customer->starchOnShirts_id = $this->input->post('starchOnShirts');
        $customer->save();

        // save the customer notes
        $customerNotes = $this->input->post('notes');
        $this->customer_model->update_customerNotes($this->customer_id, $customerNotes);

        // save the preferences in the customerPreference table
        $this->load->model('customerpreference_model');
        $preferences = $this->input->post('preferences');
        $this->customerpreference_model->save_preferences($this->customer_id, $preferences);
    }


    /**
     * Deletes a customer credit card
     *  redirect string The addres to redirect the client to
     */
    public function delete_card()
    {
        $creditCard = CreditCard::search(array(
            "customer_id" => $this->customer_id
        ));

        if ($creditCard) {
            $processor = get_business_meta($this->business_id, 'processor');
            //this is required for the Transbank software audit, but we can remove it once approved.
            if ($processor == 'transbank') {
                $transbank = new \DropLocker\Processor\Transbank(array('business_id' => $this->business_id));
                if ($transbank->removeUser($creditCard)) {
                    $creditCard->delete();
                    set_alert('success', 'Se ha eliminado la tarjeta');
                } else {
                    set_alert('error', 'Huno un error al eliminar la tarjeta.');
                }
            } else {
                $creditCard->delete();
                set_alert('error', get_translation("success", "business_account/profile_billing", array('cardNumber' => $creditCard->cardNumber), "Deleted card ending with %cardNumber%", $this->customer_id));
            }
        } else {
            set_alert('error', get_translation("no_cc_on_file", "business_account/profile_billing", array(), "You have no credit cards on file", $this->customer_id));
        }

        $this->customer->autoPay = 0;
        $this->customer->save();

        if ($this->input->post("redirect")) {
            redirect($this->input->post("redirect"));
        } else {
            redirect('/account/profile/billing');
        }
    }


    /**
     * Displays a page that notifies the customer that the credit card processor in not avaiable.
     *
     * If the processor is restored, properly configured, and the customer refreshes the page,
     * the page will redirect to the billing page
     *
     */
    public function billing_configuration_error()
    {
        $this->load->library('creditcardprocessor');
        try {
            $processor = get_business_meta($this->business_id, 'processor');
            $processor = $this->creditcardprocessor->factory($processor, $this->business_id);
            redirect("/account/profile/billing");
        } catch (Exception $e) {
            //Do Nothing
        }

        $this->renderAccount('profile_billing_configuration_error', $content);
    }


    /**
     * Renders the interface for a customer to update his or her credit card.
     */
    public function billing_transbank()
    {
        //When a new user is created trough an sms claim the default firstname is 'SMS' and default lastname is 'USER'. If the user wants to update Billing info, and it has that default name
        //then redirect it to profile/edit
        if ((strtolower($this->customer->firstName) == 'sms') || (strtolower($this->customer->lastName) == 'user')) {
            $message = get_translation("change_sms_user_name", "business_account/profile_billing", array(), "Before updating your Billing Information you must input your full real name.");
            set_alert('error', $message);
            return redirect('/account/profile/edit');
        }

        if (!empty($_POST)) {
            // we need to enter this fake data to bypass authorize card. The real auth is done by transbank on their side.
            $_POST['cardNumber'] = '4111111111111111';
            $_POST['expMo'] = '12';
            $_POST['expYr'] = date('Y', strtotime('+5 years'));
            $_POST['csc'] = '123';
            return $this->authorize_card();
        }

        $this->load->model("creditcard_model");
        $this->content['creditCard'] = $this->creditcard_model->get_one(array(
            "customer_id" => $this->customer_id,
            "business_id" => $this->business_id
        ));

        $this->load->model('location_model');
        $this->content['hideKioskCheckbox'] = $this->location_model->isOnlyKiosks($this->business_id);

        if ($this->business->store_access) {
            $this->load->model('kioskaccess_model');
            //get the customer kiosks
            $this->content['kiosk_access'] = $this->kioskaccess_model->get_kiosk_access(
                $this->customer_id,
                $this->business_id
            );
        }

        // Populate the form vields with the data stored in the POST variable
        // for use with the 'set_value' function in Codeigniter.
        if ($this->session->flashdata('post')) {
            $_POST = unserialize($this->session->flashdata('post'));
        }

        $this->renderAccount('profile_billing_transbank', $this->content);
    }

    public function billing()
    {
        $processor = get_business_meta($this->business_id, 'processor');
        if ($processor == 'transbank') {
            return $this->billing_transbank();
        }

        //When a new user is created trough an sms claim the default firstname is 'SMS' and default lastname is 'USER'. If the user wants to update Billing info, and it has that default name
        //then redirect it to profile/edit
        if ((strtolower($this->customer->firstName) == 'sms') || (strtolower($this->customer->lastName) == 'user')) {
            $message = get_translation("change_sms_user_name", "business_account/profile_billing", array(), "Before updating your Billing Information you must input your full real name.");
            set_alert('error', $message);
            return redirect('/account/profile/edit');
        }

        if (!empty($_POST)) {
            return $this->authorize_card();
        }

        $this->content["processor"] = $processor;
        
        $this->load->model("creditcard_model");
        $this->content['creditCard'] = $this->creditcard_model->get_one(array(
            "customer_id" => $this->customer_id,
            "business_id" => $this->business_id
        ));

        $this->load->model('location_model');
        $this->content['hideKioskCheckbox'] = $this->location_model->isOnlyKiosks($this->business_id);

        if ($this->business->store_access) {
            $this->load->model('kioskaccess_model');
            //get the customer kiosks
            $this->content['kiosk_access'] = $this->kioskaccess_model->get_kiosk_access(
                $this->customer_id,
                $this->business_id
            );
        }

        // Populate the form vields with the data stored in the POST variable
        // for use with the 'set_value' function in Codeigniter.
        if ($this->session->flashdata('post')) {
            $_POST = unserialize($this->session->flashdata('post'));
        }

        $this->renderAccount('profile_billing', $this->content);
    }


    /**
     * Authorizes the customer's credit card
     *
     * Can take the following POST parameters
     *  fullName
     *  firstName
     *  lastName
     *  address1
     *  cardNumber
     *  expMo
     *  expYr
     *  city
     *  state
     *  zipcode
     *  csc
     *  kioskAccess
     */
    public function authorize_card()
    {
        $this->load->library("creditcard");
        if (empty($_POST)) {
            redirect_with_fallback("/account/profile/billing");
        }

        try {
            $results = $this->creditcard->authorizePOST($this->customer_id);
        } catch (CreditCardProcessorException $creditCardProcessorException) {
            set_alert("error", $creditCardProcessorException->getMessage());
            redirect("/account/profile/billing_configuration_error");
        }

        if ($results['status'] != "success") {
            set_alert('error', get_translation(
                "failed_credit_card_authorization",
                "business_account/profile/billing",
                array("authorization_message" => $results['message']),
                "We are sorry, but we received the following response from your credit card company: %authorization_message%",
                $this->customer_id
            ));

            $this->session->set_flashdata("post", serialize($_POST));
            redirect_with_fallback("/account/profile/billing");
        }

        if ($results['data']['captureResults']) {
            $captured = array();
            $failed = array();

            foreach ($results['data']['captureResults'] as $captureResult) {
                if ($captureResult['status'] == 'success') {
                    $captured[] = $captureResult['order_id'];
                } else {
                    $failed[] = $captureResult['order_id'];
                }
            }

            $this->session->set_flashdata('captureResult', compact('captured', 'failed'));
        }

        set_alert('success', get_translation(
            "success_credit_card_authorization",
            "business_account/profile/billing",
                array(),
                "Credit card authorized",
            $this->customer_id
        ));

        redirect_with_fallback("/account/profile/billing");
    }


    /**
     * Initializes paypal billing agreement
     *
     * This is used to bind a paypal account as a credit card.
     * This will redirect the user to paypal.
     *
     * If the user cancels on paypal it will be redirected back to the
     * paypal_cancel action.
     *
     * If the user accepts on paypal it will be redirected to the
     * paypal_success action.
     */
    public function paypal()
    {
        $this->load->library('creditcardprocessor');
        $paypal = $this->creditcardprocessor->factory('paypalprocessor', $this->business_id);

        $baseURL = get_business_account_url($this->business);
        $returnURL = $baseURL . '/account/profile/paypal_success';
        $canelURL = $baseURL . '/account/profile/paypal_cancel';

        $response = $paypal->setExpressCheckout($returnURL, $canelURL);
        if ($response['status'] != 'SUCCESS') {
            set_alert('error', $response['message']);
            return redirect('/account/profile/billing');
        }

        return redirect($response['url']);
    }


    /**
     * Setup a paypal billing agreement
     *
     * This receives the token via get and proceeds to setup the customer
     * paypal account as a credit card.
     */
    public function paypal_success()
    {
        if (empty($_GET['token'])) {
            set_alert('error', get_translation(
                "missing_paypal_token",
                "business_account/profile_billing",
                array(),
                "Missing Paypal token",
                $this->customer_id
            ));

            redirect('/account/profile/billing');
        }

        $token = $_GET['token'];

        $this->load->library('creditcardprocessor');
        $paypal = $this->creditcardprocessor->factory('paypalprocessor', $this->business_id);

        $authorizationOrder = Order::create(array(
            "customer_id" => $this->customer_id,
            "locker_id" => 0, // set to $business->returnToOfficeLockerId in the order
            "orderPaymentStatusOption_id" => 1,
            "orderStatusOption_id" => 3,
            "notes" => "Authorizing Card",
            "business_id" => $this->business_id,
            "bag_id" => null,
        ));
        $order_id = $authorizationOrder->orderID;

        $response = $paypal->authorizeToken($token, $order_id, $this->customer_id, $this->business_id);
        if ($response->get_status() != "SUCCESS") {
            $authorizationOrder->delete();

            set_alert('error', get_translation(
                "failed_paypal_authorization",
                "business_account/profile_billing",
                array("authorization_message" => $response->get_message()),
                "We are sorry, but we received the following response from paypal: %authorization_message%",
                $this->customer_id
            ));

            redirect('/account/profile/billing');
        }

        // store the paypal email as the card number
        $details = $paypal->getCustomerDetails($token);
        $cardNumber = empty($details['EMAIL']) ? $details['PAYERID'] : $details['EMAIL'];

        $this->load->model('creditcard_model');
        $creditCardID = $this->creditcard_model->save_card(array(
            'cardNumber' => $cardNumber,
            'expMo' => 12,
            'expYr' => date('Y') + 10,
            'payer_id' => $response->get_refNumber(),
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'processor' => $paypal->getProcessor(),
            'cardType' => 'PayPal',
        ));

        if (!is_numeric($creditCardID)) {
            throw new Exception('Error inserting creditCard data into db following successful card authorization');
        }

        $authorizationOrder->setProperties(array(
            "statusDate" => date("Y-m-d H:i:s"),
            "orderStatusOption_id" => 10,
            "orderPaymentStatusOption_id" => 3
        ));

        $orderPaymentStatus = OrderPaymentStatus::create(array(
            "order_id" => $authorizationOrder->orderID,
            "orderPaymentStatusOption_id" => 3
        ));


        $this->afterCreditCard();

        $message = "Customer '" .  getPersonName($this->customer) . " ({$this->customer->customerID})' had unpaid orders and has just linked a PayPal accout";
        $this->checkAdditionalOrders($message);

        set_alert('success', get_translation(
            "successful_paypal_authorization",
            "business_account/profile_billing",
            array(),
            "Your PayPal account has been linked",
            $this->customer_id
        ));

        redirect('/account/profile/billing');
    }


    /**
     * User canceled on paypal
     *
     * Set error message  and redirect back to billing setup page.
     */
    public function paypal_cancel()
    {
        set_alert('error', get_translation(
            "cancel_paypal_authorization",
            "business_account/profile_billing",
            array(),
            "Authorization cancelled by the user in PayPal.",
            $this->customer_id
        ));

        redirect('/account/profile/billing');
    }


    /**
     * Initializes coinbase oauth
     */
    public function coinbase()
    {
        $this->load->library('creditcardprocessor');
        $coinbase = $this->creditcardprocessor->factory('coinbase', $this->business_id);

        $baseURL = get_business_account_url($this->business);
        $returnURL = $baseURL . '/account/profile/coinbase_return';

        $response = $coinbase->oauth($returnURL);
        if ($response['status'] != 'success') {
            set_alert('error', $response['message']);
            return redirect('/account/profile/billing');
        }

        return redirect($response['url']);
    }


    /**
     * Setup coibase oauth access
     *
     * This receives the token via get and proceeds to setup the customer
     * coinbase account as a credit card.
     */
    public function coinbase_return()
    {
        if (empty($_GET['code'])) {
            set_alert('error', get_translation(
                "missing_coinbase_token",
                "business_account/profile_billing",
                array(),
                "Missing Coinbase token",
                $this->customer_id
            ));

            redirect('/account/profile/billing');
        }

        $this->load->library('creditcardprocessor');
        $coinbase = $this->creditcardprocessor->factory('coinbase', $this->business_id);

        $authorizationOrder = Order::create(array(
            "customer_id" => $this->customer_id,
            "locker_id" => 0, // set to $business->returnToOfficeLockerId in the order
            "orderPaymentStatusOption_id" => 1,
            "orderStatusOption_id" => 3,
            "notes" => "Authorizing Card",
            "business_id" => $this->business_id,
            "bag_id" => null,
        ));
        $order_id = $authorizationOrder->orderID;

        $baseURL = get_business_account_url($this->business);
        $returnURL = $baseURL . '/account/profile/coinbase_return';

        $token = $_GET['code'];

        $response = $coinbase->authorizeToken($token, $returnURL, $order_id, $this->customer_id, $this->business_id);
        if ($response->get_status() != "SUCCESS") {
            $authorizationOrder->delete();

            set_alert('error', get_translation(
                "failed_coinbase_authorization",
                "business_account/profile_billing",
                array("authorization_message" => $response->get_message()),
                "We are sorry, but we received the following response from coinbase: %authorization_message%",
                $this->customer_id
            ));

            redirect('/account/profile/billing');
        }


        $tokens = json_decode($response->get_refNumber(), true);

        $details = $coinbase->getCustomerDetails($tokens, $returnURL);
        $cardNumber = $details['email'];

        $this->load->model('creditcard_model');
        $creditCardID = $this->creditcard_model->save_card(array(
            'cardNumber' => $cardNumber,
            'expMo' => 12,
            'expYr' => date('Y') + 10,
            'payer_id' => $response->get_refNumber(),
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'processor' => $coinbase->getProcessor(),
            'cardType' => 'Coinbase',
        ));

        if (!is_numeric($creditCardID)) {
            throw new Exception('Error inserting creditCard data into db following successful card authorization');
        }

        $authorizationOrder->setProperties(array(
            "statusDate" => date("Y-m-d H:i:s"),
            "orderStatusOption_id" => 10,
            "orderPaymentStatusOption_id" => 3
        ));

        $orderPaymentStatus = OrderPaymentStatus::create(array(
            "order_id" => $authorizationOrder->orderID,
            "orderPaymentStatusOption_id" => 3
        ));

        $this->afterCreditCard();

        $message = "Customer '" .  getPersonName($this->customer) . " ({$this->customer->customerID})' had unpaid orders and has just linked a Coinbase accout";
        $this->checkAdditionalOrders($message);

        set_alert('success', get_translation(
            "successful_coinbase_authorization",
            "business_account/profile_billing",
            array(),
            "Your Coinbase account has been linked",
            $this->customer_id
        ));
        redirect('/account/profile/billing');
    }

    /**
     * Setup Cybersource Secure Aceptance
     *
     * Expects the Cybersource response via POST
     *
     */
    public function cybersource_return()
    {
        if (empty($_POST)) {
            redirect('account/profile/billing');
        }

        $this->load->library("creditcard");

        try {
            $cybersourceResponse = $this->input->post();
            list($expMonth, $expYear) = explode('-', $cybersourceResponse['req_card_expiry_date']);

            $data = array(
                'cybersourceResponse' => $cybersourceResponse,
                'cardNumber' => $cybersourceResponse['req_card_number'],
                'expMonth' => $expMonth,
                'expYear' => $expYear,
                'city' => $cybersourceResponse['req_bill_to_address_city'],
                'state' => $cybersourceResponse['req_bill_to_address_state'],
                'zipcode' => $cybersourceResponse['req_bill_to_address_postal_code'],
                'address' => $cybersourceResponse['req_bill_to_address_line1'],
                'cardType' => CreditCard::identify($cybersourceResponse['req_card_number']),
            );

            $order = new Order($cybersourceResponse['req_reference_number']);

            $results = $this->creditcard->authorizeCardData($data, $this->customer, true, $order);
        } catch (CreditCardProcessorException $creditCardProcessorException) {
            set_alert("error", $creditCardProcessorException->getMessage());
            redirect("/account/profile/billing_configuration_error");
        }

        if ($results['status'] != "success") {
            set_alert('error', get_translation(
                "failed_credit_card_authorization",
                "business_account/profile/billing",
                array("authorization_message" => $results['message']),
                "We are sorry, but we received the following response from your credit card company: %authorization_message%",
                $this->customer_id
            ));

            redirect("/account/profile/billing");
        }

        set_alert('success', get_translation(
            "success_credit_card_authorization",
            "business_account/profile/billing",
            array(),
            "Credit card authorized",
            $this->customer_id
        ));

        redirect("/account/profile/billing");
    }

    /**
     * Setup Cybersource Secure Aceptance
     *
     * Expects the Cybersource response via POST
     *
     */
    public function transbank_return()
    {
        if (empty($_POST)) {
            redirect('account/profile/billing');
        }

        if (isset($_POST['TBK_TOKEN'])) {
            $transbank = new \DropLocker\Processor\Transbank(array('business_id' => $this->business_id));
            $result = $transbank->finishInscription($_POST['TBK_TOKEN'], $_GET['params'], $this->business_id);

            if (is_array($result) && $result['status'] == 'success') {
                set_alert('success', get_translation("success_credit_card_authorization", "business_account/profile/billing", array(), "Inscripción exitosa.", $this->customer_id));
            } else {
                set_alert('error', get_translation(
                    "failed_credit_card_authorization",
                    "business_account/profile/billing",
                    array("authorization_message" => 'Inscripción fallida, por favor inténtelo más tarde o comuníquese con el soporte técnico.'),
                                                    "Inscripción fallida, por favor inténtelo más tarde o comuníquese con el soporte técnico.",
                    $this->customer_id
                ));
            }
        }
        redirect('/account/profile/billing/');
    }

    /**
     * Remove customer from payment hold and log CC authorization
     *
     * Called after a customer enters a credit card
     */
    protected function afterCreditCard()
    {
        // setup autopay
        $this->customer->autoPay = 1;

        //get rid of hold on account if we have successful authorization
        //log it in the notes
        $note_text = 'Successful authorization';
        if ($this->customer->hold == 1) {
            $note_text .= ', removed payment hold from customer account';
        }
        $this->customer->internalNotes = str_replace('ON PAYMENT HOLD', '', $this->customer->internalNotes);
        $this->customer->hold = 0;
        $this->customer->save();

        $this->db->insert('customerHistory', array(
            'customer_id' => $this->customer_id,
            'business_id' => $this->business_id,
            'historyType' => 'Internal Note',
            'note' => $note_text
        ));
    }


    /**
     * Try to capture non captured orders
     *
     * It takes a message to use as subject of the notification email sent
     *
     * @param sting $message
     */
    protected function checkAdditionalOrders($message)
    {
        $this->load->model("order_model");

        $results = $this->order_model->checkAdditionalOrders(
            $this->customer_id,
            $this->business_id,
            '',
            $message,
            true
        );

        $captured = array();
        $failed = array();

        foreach ($results['captureResults'] as $captureResult) {
            if ($captureResult['status'] == 'success') {
                $captured[] = $captureResult['order_id'];
            } else {
                $failed[] = $captureResult['order_id'];
            }
        }

        $this->session->set_flashdata('captureResult', compact('captured', 'failed'));
    }


    /**
     * Shows many items for a customer
     *
     * if orderID is passed, this will only display the items in an order
     */
    public function closet($order_id = null)
    {
        $this->load->model('item_model');

        $this->content['barcode'] = $this->input->post('barcode');
        $this->content['order_id'] = $order_id;
        $this->content['items'] = $items = $this->item_model->get_closet($this->customer_id, $order_id, $this->input->post('barcode'));
        $this->content['default_image'] = get_translation(
            "default_image",
            "business_account/profile/closet",
            array(),
            "/images/missing_image.jpg",
            $this->customer_id
        );

        $this->renderAccount('profile/closet', $this->content);
    }


    /**
     * displays item detail
     *
     * @param int $item_id
     */
    public function item_detail($item_id)
    {
        $this->load->model('item_model');
        $this->load->model('orderitem_model');

        $item = $this->item_model->get_customer_items(array(
            'select' => 'itemID,barcode,displayName,process.name as processName, file',
            'itemID' => $item_id,
            'customer_id' => $this->customer_id
        ));
        $this->content['item'] = $item[0];

        //get order history
        $this->content['history'] = $this->orderitem_model->get_orders_with_item($item_id);
        $this->content['default_image'] = get_translation(
            "default_image",
            "business_account/profile/closet",
            array(),
            "/images/missing_image.jpg",
            $this->customer_id
        );

        $this->renderAccount('profile_item_detail', $this->content);
    }


    /**
     * Updates an itemIssue
     *
     * @param int $item_id
     */
    public function edit_note($item_id)
    {
        if ($this->input->post()) {
            if ($this->input->post('itemIssueID')) {
                $this->db->update('itemIssue', array(
                    'item_id' => $item_id,
                    'frontBack' => $_POST['frontBack'],
                    'x' => $_POST['x'],
                    'y' => $_POST['y'],
                    'issueType' => $_POST['issueType'],
                    'note' => $_POST['note'],
                    'updated' => gmdate('Y-m-d H:i:s'),
                ), array(
                    'itemIssueID' => $_POST['itemIssueID'],
                ));
                $itemIssueID = $_POST['itemIssueID'];

                set_alert('success', "Updated item issue");
            } else {
                $this->db->insert('itemIssue', array(
                    'item_id' => $item_id,
                    'frontBack' => $_POST['frontBack'],
                    'x' => $_POST['x'],
                    'y' => $_POST['y'],
                    'issueType' => $_POST['issueType'],
                    'note' => $_POST['note'],
                    'created' => gmdate('Y-m-d H:i:s'),
                    'status' => 'Opened',
                ));
                $itemIssueID = $this->db->insert_id();

                set_alert('success', get_translation(
                    "add_note_success_message",
                    "business_account/edit_note",
                    array(),
                    "Added note to item",
                    $this->customer_id
                ));
            }
        }

        redirect("/account/profile/add_note/" . $item_id);
    }


    /**
     * Deletes an itemIssue
     *
     * @param int $item_id
     * @param int $itemIssueID
     */
    public function delete_note($item_id, $itemIssueID)
    {
        $this->load->model('itemissue_model');
        $this->itemissue_model->delete_aprax(array(
            'itemIssueID' => $itemIssueID,
        ));

        set_alert('success', get_translation(
            "delete_note_success_message",
            "business_account/delete_note",
            array(),
            "Note removed!",
            $this->customer_id
        ));


        redirect("/account/profile/add_note/" . $item_id);
    }


    /**
     * UI to show or edit itemIssues
     */
    public function add_note($itemID, $itemIssueID = null)
    {
        $content['itemIssueID'] = $itemIssueID;
        $content['itemID'] = $itemID;

        $this->load->model('item_model');
        $this->load->helper('item_picture');

        $item = current($this->item_model->get_customer_items(array(
            'select' => 'itemID,barcode,displayName,process.name as processName, file',
            'itemID' => $itemID,
            'customer_id' => $this->customer_id
        )));

        $content['item'] = $item;

        //get the issues for this item
        $this->load->model('itemissue_model');
        $issues = $this->itemissue_model->get_aprax(array(
            'item_id' => $item->itemID,
        ), 'updated');

        $content['closed'] = array();
        $content['open'] = array();
        foreach ($issues as $issue) {
            if ($issue->status == 'Closed') {
                $content['closed'][] = $issue;
            } else {
                $content['open'][] = $issue;
            }
        }

        if ($itemIssueID) {
            $current_issue = $this->itemissue_model->get_by_primary_key($itemIssueID);
            $content['x'] = $current_issue->x;
            $content['y'] = $current_issue->y;
            $content['issueType'] = $current_issue->issueType;
            $content['note'] = $current_issue->note;
            $content['frontBack'] = $current_issue->frontBack;
        } else {
            $content['x'] = "";
            $content['y'] = "";
            $content['issueType'] = "";
            $content['note'] = "";
            $content['frontBack'] = "";
        }

        $this->renderAccount('profile/add_note', $content);
    }


    /**
     * UI for the customer to manage their kiosk access
     */
    public function kiosk_access()
    {
        // this form has moved here
        redirect('/account/profile/billing#access');

        $this->load->model('kioskaccess_model');
        $this->load->model('location_model');

        // get the kiosks for the dropdown
        $options['business_id'] = $this->business_id;
        $options['locationType_id'] = 1;
        $options['with_location_type'] = true;
        $this->content['kiosks'] = $this->location_model->get($options);

        //get the customer kiosks
        $this->content['access'] = $this->kioskaccess_model->get_kiosk_access(
            $this->customer_id,
            $this->business_id
        );

        $this->template->add_js("/js/account/profile/kiosk_access.js", "import");
        $this->renderAccount('profile_kiosk_access', $this->content);
    }


    /**
     * Updates a customers access to a kiosk. There is no UI associated with this method
     */
    public function update_kiosk_card()
    {
        if (!empty($_POST['delete'])) {
            $this->delete_kiosk_access();
        }

        $this->load->library("form_validation");
        $this->form_validation->set_rules("cardNumber", "Card Number", "required|exact_length[4]");
        $this->form_validation->set_rules("expMo", "Expiration Month", "required");
        $this->form_validation->set_rules("expYr", "Expiration Year", "required");

        if (!$this->form_validation->run()) {
            set_alert("error", validation_errors());
            redirect("/account/profile/billing");
        }

        $this->load->model('kioskaccess_model');
        $id = $this->kioskaccess_model->update_access(
            $this->customer_id,
            $this->business_id,
            $_POST['cardNumber'],
            $_POST['expMo'],
            $_POST['expYr']
        );

        if ($id) {
            set_alert('success', get_translation(
                "store_access_updated",
                "update_kiosk_card",
                array(),
                "Store access has been updated. It may take a few minutes to activate at the stores"
            ));
        } else {
            set_alert('error', get_translation(
                "store_access_error",
                "update_kiosk_card",
                array(),
                "Store access not updated"
            ));
        }

        redirect("/account/profile/billing");
    }


    /**
     * Removes kiosk access from a customer
     */
    public function delete_kiosk_access()
    {
        if (!empty($_POST['delete'])) {
            $this->load->model('kioskaccess_model');
            $this->kioskaccess_model->remove_access($this->customer_id, $this->business_id);

            set_alert('success', get_translation(
                "store_access_deleted",
                "update_kiosk_card",
                array(),
                "Store access has been disabled"
            ));
        }

        redirect("/account/profile/billing");
    }


    /**
     * Deletes a pending email from a customers account
     */
    public function delete_pending_email()
    {
        $this->customer->pendingEmailUpdate = '';
        $this->customer->save();

        set_alert("success", get_translation(
            "pending_email_deleted",
            "business_account/profile/delete_pending_email",
                array(),
            "Pending email deleted"
        ));
        redirect('/account/profile/email/');
    }


    /**
     * Changes the customer default language
     */
    public function change_language()
    {
        $lang_key = $this->input->get('ll_swap');
        $previous_page = $this->input->get('r');
        $bounce_back = base64_decode($previous_page);

        $this->db->join('language', 'language.languageID=business_language.language_id');
        $lang_query = $this->db->get_where('business_language', array('business_id' => $this->business_id));
        $langs = $lang_query->result();

        $business_langs = array();

        foreach ($langs as $lang_data) {
            $business_langs[$lang_data->business_languageID] = $lang_data;
        }

        //if its numeric its probably ok
        if (is_numeric($lang_key)) {
            //check to see if its in array of languages the business has
            if (!empty($business_langs[$lang_key])) {
                //swap language
                $this->customer->default_business_language_id = $business_langs[$lang_key]->business_languageID;
                $this->customer->save();
            }
        } elseif (!empty($lang_key)) {  //check to pull by the tag
            foreach ($business_langs as $bus_lang_data) {
                if ($bus_lang_data->tag == $lang_key) {
                    //swap language
                    $this->customer->default_business_language_id = $bus_lang_data->business_languageID;
                    $this->customer->save();
                }
            }
        }

        redirect($bounce_back);
    }
}
