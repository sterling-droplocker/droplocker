<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\Location;

class Main extends CI_Controller
{

    public $business;
    public $customer_id;
    public $business_id;
    private $data;
    public $rewards;

    public function __construct()
    {
        parent::__construct();

        /**
         * check to see if we are in a subdomain.
         * if we are not, load the droplocker website
         */
        if ($this->business) {

            // functions used by other business, TODO: move into new controller
            $shared_actions = array('unsubscribe', 'set_user', 'plans', 'everything_plans', 'product_plans');
            $action = $this->uri->segment(2);
            if ($this->business->businessID != 3 && !in_array($action, $shared_actions)) {
                $url = get_business_account_url($this->business);

                redirect($url, 301);
            }

            //load the sub business template
            $this->subdomain->title();
            $this->subdomain->javascript();
            $this->subdomain->style();
            $this->subdomain->header();
            $this->subdomain->footer();
            $this->business_id = $this->business->businessID;

            $this->facebook = new Facebook(array(
                "appId" => $this->business->facebook_api_key,
                "secret" => $this->business->facebook_secret_key,
                'allowSignedRequest' => false,
            ));
            $this->content['facebook'] = $this->facebook;
        } else {
            $this->template->set_template('droplocker');
        }

        $this->viewFileLocation = "/business_account/";
        //bizzie sites are a bit different, so the content body is slighty diff
        if ( in_bizzie_mode() ) {
            $this->viewTemplate = "/business_account/bizzie_content_template";
        } else {
            $this->viewTemplate = "/business_account/content_template_wide";
        }

        // if there is a customer session, load the object
        if ($this->session->userdata('user_id')) {
            try {
                $customer = new Customer($this->session->userdata("user_id"));
                $this->customer = $customer;
                $this->customer_id = $customer->customerID;
                $this->content['bouncedEmail'] = ($customer->bounced == 1 ) ? TRUE : FALSE;
                $this->data['customer'] = $this->customer;
                $this->content['customer'] = $this->customer;
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                $this->session->unset_userdata("user_id");
            }
        }

        $this->load->helper('ll');
        $this->data = array();
        $this->data['business_id'] = $this->business_id;
        $this->customer_id = $this->session->userdata("user_id");

    }

    public function show_404()
    {
        session_start();
        $CI_Exceptions = new CI_Exceptions;
        //The following conditional only sends e-mail error reports if there was a referred URL
        if (!empty($_SESSION['referred_url'])) {
            send_404_report($_SESSION['page'], $_SESSION['referred_url']);
        }
        $CI_Exceptions->show_404();
    }
    public function get_page()
    {
        $file_location = $this->uri->segment(1);

        $file = "hosted/" . $this->business->businessID . "/" . $file_location;

        if (!file_exists($file)) {
            redirect("/main", "location", 301);
        }
        $fh = fopen($file, 'r');
        $theData = fread($fh, filesize($file));
        fclose($fh);
        echo $theData;
    }

    public function index()
    {
        if ($this->business) {
            $content['business'] = $this->business;

            $this->template->write('title', "Laundry Locker:. 24 hour Dry Cleaning and Laundry Delivery Service", true);
            $this->data['content'] = $this->load->view('index', array(), true);
            $this->template->write_view('content', $this->viewTemplate, $this->data);
        } else {
            // The following conditional will redirect all root requests to Bizzie's home page if the server is in bizzie mode and the hostname is admin.bizzie.com
            if (in_bizzie_mode() && $_SERVER['HTTP_HOST'] == "admin.bizzie.com") {
                redirect("http://bizzie.com");
            } else {
                $this->template->write_view("page", 'droplocker/index');
            }
        }

        $this->template->render();
    }

    /**
     * Allows the customer to unsubscribe from emails
     */
    public function unsubscribe()
    {
        if (isset($_POST['submit'])) {
            $customerObjs = \App\Libraries\DroplockerObjects\Customer::search_aprax(array(
                'email'=>$this->input->post('email'), 
                'business_id' => $this->business->businessID
                )
            );
            
            $customer = $customerObjs[0];

            if ($customer) {
                 $customer->unsubscribe();
                 $message = get_translation("unsuscribe_success", "unsuscribe", array('company_name' => $this->business->companyName), 'Thank you for using %company_name% You have been removed from our mailing list and we will no longer send you any email communications.');
                 set_flash_message('success', $message);
                 set_alert('success', $message);
            } else {
                $message = get_translation("unsuscribe_error", "unsuscribe", array('phone' => $this->business->phone), 'Sorry, but the email was not found. Please try your email again, or call customer support at %phone%');
                set_flash_message('error', $message);
                set_alert('error', $message);
            }

            redirect($this->uri->uri_string());
        }

        /*
        * Laundrylocker is the only business with a custom sidebar.
        * I wrapped it in an if statement to display that menu when LL business is the session business_id (3)
        */

        $this->content['class'] = "btn btn-primary";
        $this->content['business'] = $this->business;

        $this->content['business_id'] = $this->business->businessID;
        $this->data['content'] = $this->load->view('business_account/unsubscribe', $this->content, true);
        //$this->template->write_view('content', 'inc/dual_column_right', $this->data);

        //$this->data['content'] = $this->load->view($this->viewFileLocation."profile_index", $this->content, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function infrastructure()
    {
        $this->load->view('droplocker/infrastructure');
    }

    public function locations()
    {
        $this->load->model('location_model');
        // get the business locations and pass to the view
        $region = $this->input->cookie('region');
        $business_id = null;

        if ($region == 'sf') {
            $business_id = 3;
        } elseif ($region == "ny") {
            $business_id = 20;
        } else {
            // set the region to sf and then reload the page
            $cookie = array(
                    'name' => 'region',
                    'value' => 'sf',
                    'expire' => '86500',
                    'path' => '/'
            );
            $this->input->set_cookie($cookie);
            redirect($this->uri->uri_string());
        }

        $options['public'] = 'Yes';
        $options['status'] = 'active';
        //$options['business_id'] = $business_id;
        $options['with_location_type'] = true;
        $locations = $this->location_model->get($options);
        
        for ($i = 0; $i < sizeof($locations); $i++) {
            $info = '<table><tr><td valign=top><span class=bodyBold><u>' . $locations[$i]->companyName . '</u>
          <br><br>' . $locations[$i]->address . '</span><span class=bodyReg><br>' . $locations[$i]->location . '</span>';
            $info .= ($locations[$i]->hours) ? '<br><br><span class=bodyBold>Hours: </span><span class=bodyReg>' . $locations[$i]->hours . '</apan></td>' : "";
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $locations[$i]->locationID . '.jpg')) {
                $img = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $locations[$i]->locationID . '.jpg');
                $info .= '<td style="padding:5px;"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src=/images/location_' . $locations[$i]->locationID . '.jpg width=' . $img[0] . ' height=' . $img[1] . '></td>';
            }
            $info .= '</tr></table>';
            $locations[$i]->info = $info;
        }
        $data['locations'] = $locations;
        foreach ($locations AS $index => $location) {
            $json_locations[$index] = array("locationID" => $location->locationID, "address" => $location->address,"state"=>$location->state, "city"=>$location->city, "lat" => $location->lat, "lon" => $location->lon, "companyName"=>$location->companyName, "info" => $location->info, "abbreviation" => $location->abbreviation);
        }
        $data['json_locations'] = json_encode($json_locations);

        $this->load->view('droplocker/locations', $data);
    }


    public function business()
    {
        $this->load->view('droplocker/business');
    }

    public function contact()
    {
        if (!empty($_POST)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("email", "Email", "required|valid_email");
            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("phone", "Phone", "required");
            $this->form_validation->set_rules('g-recaptcha-response', 'ReCaptcha', 'callback_recaptcha_check');

            if ($this->form_validation->run()) {
                $message = array();
                $message[] = 'From: ' . $_POST['name'];
                $message[] = 'E-Mail: ' . $_POST['email'];
                $message[] = 'Phone: ' . $_POST['phone'];
                $message[] = 'City: ' . $_POST['city'];
                $message[] = 'State / Country: ' . $_POST['state'];
                $message[] = 'Business Type '.$_POST['business_type'];
                //$message[] = 'Country: ' . $_POST['country'];
                $message[] = 'Company Name: ' . $_POST['company'];
                $message[] = 'When To Start: ' . $_POST['when_to_start'];
                $message[] = 'Details: ' . $_POST['notes'];
                $message[] = 'IP Address: ' . $_SERVER['REMOTE_ADDR'];
                $message[] = 'Browser: ' . (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
                $message = implode("<br>\n", $message);


                // hard code business_id to 3 because its validated by this function
                queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, 'sales@droplocker.com', "Contact Form", $message, array(), null, 3);

                set_flash_message("success", "Your message has been sent. Thank you for contacting us.");
                redirect("/main/contact");
            } else {
                set_flash_message("error", validation_errors());
                redirect("/main/contact");
            }

        }


        $recaptcha_key = $this->config->item('recaptcha_keys');
        $content['recaptcha_public_key'] = $recaptcha_key['public'];

        $this->load->view('droplocker/contact', $content);
    }

    public function recaptcha_check($gRecaptchaResponse)
    {
        $recaptcha_key = $this->config->item('recaptcha_keys');
        $recaptcha_private_key = $recaptcha_key['private'];

        require_once APPPATH . 'third_party/recaptcha/src/autoload.php';
        $recaptcha = new \ReCaptcha\ReCaptcha($recaptcha_private_key);
        $resp = $recaptcha->verify($gRecaptchaResponse, $_SERVER["REMOTE_ADDR"]);

        if (!$resp->isSuccess()) {
            $message = get_translation("recaptcha_error", "main/contact",
                array(), "Please, check reCAPTCHA field.");
            $this->form_validation->set_message('recaptcha_check', $message);
            return false;
        }

        return true;
    }

    public function faq()
    {
        $this->load->view('droplocker/faq');
    }

    public function how()
    {
        $q = $this->db->query("SELECT b.companyName  FROM business b inner join locker l on b.returnToOfficeLockerId = l.lockerID WHERE b.businessID > 1");
        $business = $q->result();
        $this->load->view('droplocker/how', array('business' => $business));
    }

    public function lockers()
    {
        $this->load->view('droplocker/lockers');
    }

    public function droplocker_press()
    {
        $this->load->view('droplocker/press');
    }

    public function pricing()
    {
        $this->load->view('droplocker/pricing');
    }
    
    public function driver_app_isup()
    {
        $q = $this->db->query("
               select 1 completed from
               (
                  select * from cronLog        
                  where  job = 'Driver_App::sync_all'
                    and results like '%Execution finished%'
                  order by cronLogID desc
                  limit 1
               ) cronLog
               where dateCreated >= now() - INTERVAL 1 hour;
        ");
        $results = $q->result();
        $completed = $results[0]->completed;
        
        if ($completed) {
            echo "Driver_app:sync_all daemon completed in the last hour $completed";
        } else {
            $this->db->insert('cronLog', array('job' => 'Driver_App::status_check', 'results' => 'Uptime robot (external service) check. Result: Error, execution toke more than 1 hour'));
            redirect('/main/error');
        }
    }

    public function products()
    {
        $this->load->view('droplocker/products');
    }

    public function technology()
    {
        $this->load->view('droplocker/technology');
    }

    public function newsletter()
    {
        if (isset($_POST['submit'])) {
            //I added to hidden fields. If there are values in those fields, we have a bot
            if ($_POST['email'] != '' || $_POST['name'] != '') {
                die('Too many fields');
            } else {
                //valid so add to newsletter list
                $this->load->library('form_validation');
                $this->form_validation->set_rules('e', 'Email', 'required|valid_email');
                if ($this->form_validation->run()) {
                    $this->load->model('maillist_model');
                    $this->maillist_model->email = $_POST['e'];
                    if ($this->maillist_model->insert()) {
                        $this->session->set_flashdata('message', "Thank you for joining our mailing list.");
                        send_email('admin@droplocker.com', 'Admin', 'sales@droplocker.com,cmoreno@laundrylocker.com', 'New mailing list subscription', $_POST['e'] .' joined the Drop Locker mailing list');
                    } else {
                        $this->session->set_flashdata('message', "Email has not been added due to internal error");
                    }
                } else {
                    $this->session->set_flashdata('message', "Invalid Email Address");
                }

                redirect('/main/newsletter');
            }
        }
        $this->load->view('droplocker/newsletter');
    }

    /**
     * Logs in as the specified user
     *
     * @param int $customer_id
     * @param int $buser_id
     */
    public function set_user($customer_id = null, $buser_id = null)
    {
        $this->load->model('customer_model');
        $this->customer_model->customerID = $customer_id;
        $customer = $this->customer_model->get();
        $this->session->set_userdata('user_id', $customer[0]->customerID);
        $this->session->set_userdata('user_fname', $customer[0]->firstName);
        $this->session->set_userdata('user_lname', $customer[0]->lastName);
        $this->session->set_userdata('old_buser_id', $buser_id);

        if (!empty($this->business->locale)) {
            setlocale(LC_ALL, $this->business->locale);
        }
        
        redirect("/account");
    }


    public function error()
    {
        $content['content'] = $this->load->view('/account/main_error', $this->data, true);
        $this->template->write_view('content', 'inc/dual_column_right', $content);
        $this->template->render();
    }

    public function howto()
    {
        $this->template->add_js("js/thickbox.js");
        $this->template->add_css("css/thickbox.css");
        $this->template->write('title', "How to order laundry service - Laundry Locker", true);
        //$this->template->write_view('content', 'main_howto', $this->data);
        $this->data['content'] = $this->load->view('main_howto',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);

        $this->template->render();
    }

    public function howother()
    {
        $this->template->write('title', "How to order laundry service - Laundry Locker", true);
        $this->template->write_view('content', 'main_howother', $this->data);
        $this->template->render();
    }

    public function concierge()
    {
        $this->template->write('title', "laundry service at concierge buildings - Laundry Locker", true);
      //  $this->template->write_view('content', 'main_concierge', $this->data);

        $this->data['content'] = $this->load->view('main_concierge',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function atwork()
    {
        $variables = array();
        if (!empty($_POST)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("email", "Email", "required|valid_email");
            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("phone", "Phone", "required");

            if ($this->form_validation->run()) {
                
                $message = array();
                $message[] = 'From: ' . $_POST['name'];
                $message[] = 'Company: ' . $_POST['company'];
                $message[] = 'Email: ' . $_POST['email'];
                $message[] = 'Phone: ' . $_POST['phone'];

                $message[] = 'IP Address: ' . $_SERVER['REMOTE_ADDR'];
                $message[] = 'Browser: ' . (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
                $message = implode("<br>\n", $message);

                queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, 'support@laundrylocker.com', "At Work Request Info Form", $message, array(), null, 3);

                $variables['email_sent'] = true;
                
            } else {
                $variables['validation_errors'] = validation_errors();
            }
        }

        $this->template->write('title', "laundry service at work - Laundry Locker", true);
        $this->template->add_js("js/jquery.validate-1.9.js");
        $this->data['content'] = $this->load->view('main_atwork',$variables, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    

    public function athome()
    {
        $this->template->write('title', "laundry service at home - Laundry Locker", true);
        $this->data['content'] = $this->load->view('main_athome',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function wholesale()
    {
        $this->template->write('title', "wholesale eco-friendly dry cleaning - Laundry Locker", true);
        //$this->template->write_view('content', 'main_wholesale', $this->data);
        $this->data['content'] = $this->load->view('main_wholesale',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function homedelivery()
    {
        $this->template->write('title', "home delivery laundry service - Laundry Locker", true);
        //$this->template->write_view('content', 'main_homedelivery', $this->data);
        $this->data['content'] = $this->load->view('main_homedelivery',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
        //redirect('/main/athome');
    }

    public function lockerloot()
    {
        $this->template->write('title', "How to earn free dry cleaning and laundry service - Laundry Locker", true);
        //$this->template->write_view('content', 'main_lockerloot', $this->data);
        $this->data['content'] = $this->load->view('main_lockerloot',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function lockerlocations() {
        $this->template->write('title', "Locker Locations - Laundry Locker", true);
        $this->data['content'] = $this->load->view('main_lockerlocations', array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();

    }

    public function locations24hr() {

        $this->load->model('location_model');
        $locationCount         = $this->location_model->location_count(array('business_id' => $this->business_id));
        $data = array();
        $data['locationCount'] = $locationCount;

        $options['public']             = 'Yes';
        $options['business_id']        = $this->business_id;
        $options['with_location_type'] = true;
        $locations                     = $this->location_model->get($options);
       
        $data['locations'] = $locations;
        foreach ($locations AS $index => $location) {
            $json_locations[$index] = array("locationID" => $location->locationID,
                "address" => $location->address, "state" => $location->state, "city" => $location->city,
                "lat" => $location->lat, "lon" => $location->lon, "companyName" => $location->companyName,
                "abbreviation" => $location->abbreviation);
        }
        $data['json_locations'] = json_encode($json_locations);
        


        $this->template->write('title', "24 Hour Public Locations - Laundry Locker", true);
        $this->data['content'] = $this->load->view('main_24hrlocations', $data, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function kiosk()
    {
        $this->template->write('title', "24 hour laundry service via public kiosks - Laundry Locker", true);
        $this->template->write_view('content', 'main_kiosk', $this->data);
        $this->template->render();
    }

    public function services()
    {
        $this->template->write('title', "Dry cleaning and laundry services - Laundry Locker", true);
       // $this->template->write_view('content', 'main_services', $this->data);
        $this->data['content'] = $this->load->view('main_services',array(), true );
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function dryclean()
    {
        $this->template->write('title', "Eco-friendly dry cleaning - Laundry Locker", true);
        //$this->template->write_view('content', 'main_dryclean', $this->data);
        $this->data['content'] = $this->load->view('main_dryclean',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function washfold()
    {
        $this->template->write('title', "Wash and fold laundry service - Laundry Locker", true);
       // $this->template->write_view('content', 'main_washfold', $this->data);
        $this->data['content'] = $this->load->view('main_washfold',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function shoeshine()
    {
        $this->template->write('title', "Shoe Repairs and Shines - Laundry Locker", true);
        //$this->template->write_view('content', 'main_shoeshine', $this->data);
         $this->data['content'] = $this->load->view('main_shoeshine',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function packagedelivery()
    {
        $this->template->write('title', "Package delivery service - Laundry Locker", true);
       // $this->template->write_view('content', 'main_packagedelivery', $this->data);
        $data['fancybox'] = true;
        $this->data['content'] = $this->load->view('main_packagedelivery', $data, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function businessaccounts()
    {
        $this->template->write('title', "wholesale linen and towel service - Laundry Locker", true);
       // $this->template->write_view('content', 'main_businessaccounts', $this->data);
        $this->data['content'] = $this->load->view('main_businessaccounts',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function specials()
    {
        $this->template->write('title', "Dry cleaning and laundry specials - Laundry Locker", true);
        //$this->template->write_view('content', 'main_specials', $this->data);
        $this->data['content'] = $this->load->view('main_specials',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function publiclocations()
    {
        $this->load->model('location_model');
        $locationCount = $this->location_model->location_count(array('business_id' => $this->business_id));
        $data['locationCount'] = $locationCount;

        $options['public'] = 'Yes';
        $options['business_id'] = $this->business_id;
        $options['with_location_type'] = true;
        $locations = $this->location_model->get($options);
        for ($i = 0; $i < sizeof($locations); $i++) {
            $info = '<table><tr><td valign=top><span class=bodyBold><u>' . $locations[$i]->companyName . '</u>
          <br><br>' . $locations[$i]->address . '</span><span class=bodyReg><br>' . $locations[$i]->location . '</span>';
            $info .= ($locations[$i]->hours) ? '<br><br><span class=bodyBold>Hours: </span><span class=bodyReg>' . $locations[$i]->hours . '</apan></td>' : "";
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $locations[$i]->locationID . '.jpg')) {
                $img = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $locations[$i]->locationID . '.jpg');
                $info .= '<td style="padding:5px;"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src=/images/location_' . $locations[$i]->locationID . '.jpg width=' . $img[0] . ' height=' . $img[1] . '></td>';
            }
            $info .= '</tr></table>';
            $locations[$i]->info = $info;
        }
        $data['locations'] = $locations;
        foreach ($locations AS $index => $location) {
            $json_locations[$index] = array("locationID" => $location->locationID, "address" => $location->address,"state"=>$location->state, "city"=>$location->city, "lat" => $location->lat, "lon" => $location->lon, "companyName"=>$location->companyName, "info" => $location->info, "abbreviation" => $location->abbreviation);
        }
        $data['json_locations'] = json_encode($json_locations);
        $this->template->write('title', "Public locations - Laundry Locker", true);

        $this->data['content'] = $this->load->view('main_publiclocations',$data, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        //$this->template->write_view('content', 'main_publiclocations', $data);
        $this->template->render();
    }

    public function locationdetail()
    {
        $this->data['content'] = $this->load->view('main_locationdetail',$data, true);
        $this->template->write('title', "Location Details - Laundry Locker", true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function lockerpics()
    {
        $this->template->write('title', "Pictures of lockers - Laundry Locker", true);
        $this->template->write_view('content', 'main_lockerpics', $this->data);
        $this->template->render();
    }


    public function berkeley()
    {
        redirect('/');
        //$this->template->write('title', "Berkeley Kiosk - Laundry Locker", true);
        //$this->template->write_view('content', 'main_berkeley', $data);
        //$this->template->render();
    }


    public function clay()
    {
        $this->template->write('title', "250 Clay - Laundry Locker", true);
        $this->template->write_view('content', 'main_clay', $this->data);
        $this->template->render();
    }

    public function onecalifornia()
    {
        $this->template->write('title', "One California - Laundry Locker", true);
        $this->template->write_view('content', 'main_onecalifornia', $this->data);
        $this->template->render();
    }

    public function about()
    {
        $this->template->write('title', "About Us - Laundry Locker", true);
        //$this->template->write_view('content', 'main_about', $this->data);
        $this->data['content'] = $this->load->view('main_about', $this->data, true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function ourtechnology()
    {
        $this->template->write('title', "Technology - Laundry Locker", true);
        //$this->template->write_view('content', 'main_ourtechnology', $this->data);
        $this->data['content'] = $this->load->view('main_ourtechnology',array(),true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function press()
    {
        $this->template->write('title', "In the Press - Laundry Locker", true);
        //$this->template->write_view('content', 'main_press', $this->data);
        $this->data['content'] = $this->load->view('main_press',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);

        $this->template->render();
    }

    public function contactus()
    {
        if (isset($_POST['submit'])) {
            //join email list
        }
        $this->template->write('title', "Contact Us - Laundry Locker", true);
        //$this->template->write_view('content', 'main_contactus', $data);
        $this->data['content'] = $this->load->view('main_contactus',$data,true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function partners()
    {
        $this->template->write('title', "Our Partners - Laundry Locker", true);
        //$this->template->write_view('content', 'main_partners', $this->data);
        $this->data['content'] = $this->load->view('main_partners',array(),true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);

        $this->template->render();
    }

    function dlpartners() {

        $q = $this->db->query("SELECT b.companyName, b.website_url, loc.locationID, loc.address, loc.state, b.city, loc.lat, loc.lon, loc.location, loc.hours  FROM business b
            inner join locker l on b.returnToOfficeLockerId = l.lockerID
            inner join location loc on l.location_id = loc.locationID WHERE b.businessID > 1
            AND b.active = 1");

        $locations = $q->result();

        // get the business locations and pass to the view

        for ($i = 0; $i < sizeof($locations); $i++) {
            $info = '<table><tr><td valign=top><span class=bodyBold><u>' . $locations[$i]->companyName . '</u>
          <br><a href="http://' . $locations[$i]->website_url . '" target="_blank">'.$locations[$i]->website_url.'</a></span><span class=bodyReg><br><br>' . $locations[$i]->city . '</span>';
            
            
            $info .= '</tr></table>';
            $locations[$i]->info = $info;
        }
        $data['locations'] = $locations;
        /*foreach ($locations AS $index => $location) {
            $json_locations[$index] = array("locationID" => $location->locationID, "address" => $location->address,"state"=>$location->state, "city"=>$location->city, "lat" => $location->lat, "lon" => $location->lon, "companyName"=>$location->companyName, "info" => $location->location);
        }*/
        $data['json_locations'] = json_encode($locations);

        $this->load->view('droplocker/partners', $data);

    }

    /**
     * Shows laundry plans
     *
     * @param string $group
     */
    public function plans($group = " ")
    {
        $this->rewards = \App\Libraries\DroplockerObjects\LockerLootConfig::isActive($this->business_id);
        if ( !empty( $this->rewards ) ) {
               $this->rewards_name = \App\Libraries\DroplockerObjects\LockerLootConfig::getName($this->business_id);
        }

        $this->load->model('laundryplan_model');
        //The following conditional retrieves the customer's current laundry plan if he or she is logged in.
        if ($this->customer_id) {
            $active_plan = $this->laundryplan_model->get_active_laundry_plan($this->customer_id, $this->business_id, 'laundry_plan');
            $this->content['active_plan'] = $active_plan;

            if ($upgrade_plan_id = $this->laundryplan_model->is_upgrading($this->customer_id)) {
                $this->laundryplan_model->clear();
                $this->laundryplan_model->laundryPlanID = $upgrade_plan_id;
                $this->content['upgrade_plan'] = $this->laundryplan_model->get();
            }
        }

        $old_buser_id = $this->session->userdata('old_buser_id');

        $options = array(
            'business_id' => $this->business_id,
            'visible' => 1,
        );

        if (is_numeric($group)) {
            $options['businessLaundryPlanID'] = $group;

            // if its not an admin logged in as the user
            if ($old_buser_id) {
                unset($options['visible']);
            }
        } else {
            $options['grouping'] = $group;
        }

        $options['(endDate is NULL or endDate > ' . gmdate('Y-m-d') . ')'] = null;

        $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');

        $this->load->model('businesslaundryplan_model');
        $plans = $this->businesslaundryplan_model->get_aprax($options, 'price');

        $num_plans = sizeof( $plans );
        for ($i = 0; $i < $num_plans; $i++) {

            // if the plan date is in the past, do not show it
            if (($plans[$i]->endDate < date('Y-m-d')) && $plans[$i]->days == 0) {
                unset($plans[$i]);
                continue;
            }

            if ( substr( $plans[$i]->endDate,0,10 ) != '0000-00-00') {
                //this plan has an endDate
                $plans[$i]->term = get_translation("laundry_plans_term_enddate", "business_account/main_plans",
                    array('endDate' => strftime($dateFormat, strtotime($plans[$i]->endDate))), "Now thru %endDate%");
            } else {
                $plans[$i]->term = get_translation("laundry_plans_term_recurring", "business_account/main_plans",
                        array('days' => $plans[$i]->days), "%days% Days (Recurring)");
            }
        }

        $q = $this->db->query("SELECT distinct(grouping) as grouping
                              FROM businessLaundryPlan
                              WHERE grouping!=''
                              AND business_id = {$this->business_id} 
                              AND type = 'laundry_plan'");

        $groups = $q->result();
        $this->content['current_group'] = $group;
        $this->content['groups'] = $groups;
        $this->content['plans'] = $plans;

        $business = new Business($this->business_id);
        $this->content['business'] = $business;

        //this is for the sidebar menu
        $this->plans = $plans;
        $this->product_plans = 1;
        $this->set_plans_menu('everything_plan');
        //

        $this->content['wfPrice'] = $this->laundryplan_model->getDefaultPrice($this->customer_id, $this->business_id);
        $this->load->helper('ll');

        //if there is customer data, pass it to the content variable
        if ( !empty( $this->customer_id ) ) {
            $this->content['customer'] = new Customer( $this->customer_id );
        }

        $this->content['customer_id'] = $this->customer_id;

        if (is_numeric($group) && !empty($plans)) {
            $this->content['plan_title'] = get_translation("laundry_plan_title", "business_account/main_plans",
            array('name' => $plans[0]->name), "%name% Laundry Plan");
        } else if ($group && !is_numeric($group)) {
            $this->content['plan_title'] = get_translation("group_laundry_plans_title", "business_account/main_plans",
                                                array('group' => $group), "%group% Laundry Plans");
        } else {
            $this->content['plan_title'] = get_translation("laundry_plans_title", "business_account/main_plans",
                                                array(), "Laundry Plans");
        }

        $this->data['sidebar'][] = $this->load->view('/business_account/account_menu', array( 'business' => $this->business ), true );
        $this->data['content'] = $this->load->view( $this->viewFileLocation . 'main_plans', $this->content, true);

        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    /**
     * Shows everything_plans
     *
     * @param string $group
     */
    public function everything_plans($group = " ")
    {
        $this->rewards = \App\Libraries\DroplockerObjects\LockerLootConfig::isActive($this->business_id);
        if ( !empty( $this->rewards ) ) {
               $this->rewards_name = \App\Libraries\DroplockerObjects\LockerLootConfig::getName($this->business_id);
        }

        $active_plan = null;

        if ($this->customer_id) {
            $this->everything_plan = \App\Libraries\DroplockerObjects\EverythingPlan::search_aprax(array(
                "business_id" => $this->business_id,
                "customer_id" => $this->customer_id,
                "type"        => "everything_plan",
                "active"     => 1
            ));
            $this->content['active_plan'] = !empty($this->everything_plan) ? $this->everything_plan[0] : null;

            $upgrade_plan_id = \App\Libraries\DroplockerObjects\EverythingPlan::is_upgrading(
                $this->customer_id
            );

            if (!empty($upgrade_plan_id)) {
                $this->content['upgrade_plan'] = \App\Libraries\DroplockerObjects\EverythingPlan::search_aprax(array(
                    "laundryPlanID" => $upgrade_plan_id
                ));
            }
        }

        $old_buser_id = $this->session->userdata('old_buser_id');

        $options = array(
            'business_id' => $this->business_id,
            'visible' => 1,
        );

        if (is_numeric($group)) {
            $options['businessLaundryPlanID'] = $group;

            // if its not an admin logged in as the user
            if ($old_buser_id) {
                unset($options['visible']);
            }
        } else {
            $options['grouping'] = $group;
        }

        $options['(endDate is NULL or endDate > ' . gmdate('Y-m-d') . ')'] = null;

        $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');

        $options["type"] = 'everything_plan';
        $plans = \App\Libraries\DroplockerObjects\BusinessEverythingPlan::search_aprax(
            $options, 
            false,
            "price" //order by
        );

        $num_plans = sizeof( $plans );

        for ($i = 0; $i < $num_plans; $i++) {

            // if the plan date is in the past, do not show it
            if (($plans[$i]->endDate < date('Y-m-d')) && $plans[$i]->days == 0) {
                unset($plans[$i]);
                continue;
            }

            if ( substr( $plans[$i]->endDate,0,10 ) != '0000-00-00') {
                //this plan has an endDate
                $plans[$i]->term = get_translation("everything_plans_term_enddate", "business_account/main_everything_plans",
                    array('endDate' => strftime($dateFormat, strtotime($plans[$i]->endDate))), "Now thru %endDate%");
            } else {
                $plans[$i]->term = get_translation("everything_plans_term_recurring", "business_account/main_everything_plans",
                        array('days' => $plans[$i]->days), "%days% Days (Recurring)");
            }
        }

        $q = $this->db->query("SELECT distinct(grouping) as grouping
                              FROM businessLaundryPlan
                              WHERE grouping!=''
                              AND business_id = {$this->business_id}
                              AND type = 'everything_plan'");

        $groups = $q->result();
        $this->content['current_group'] = $group;
        $this->content['groups'] = $groups;
        $this->content['plans'] = $plans;
        
        $business = new Business($this->business_id);
        $this->content['business'] = $business;

        //this is for the sidebar menu
        $this->everything_plans = $plans;
        $this->product_plans = 1;
        $this->set_plans_menu('laundry_plan');
        //
        

        $this->content['wfPrice'] = \App\Libraries\DroplockerObjects\EverythingPlan::getDefaultPrice($this->customer_id, $this->business_id);
        $this->load->helper('ll');

        //if there is customer data, pass it to the content variable
        if ( !empty( $this->customer_id ) ) {
            $this->content['customer'] = new Customer( $this->customer_id );
        }

        $this->content['customer_id'] = $this->customer_id;

        if (is_numeric($group) && !empty($plans)) {
            $this->content['plan_title'] = get_translation("everything_plan_title", "business_account/main_everything_plans",
            array('name' => $plans[0]->name), "%name% Everything Plan");
        } else if ($group && !is_numeric($group)) {
            $this->content['plan_title'] = get_translation("group_everything_plans_title", "business_account/main_everything_plans",
                                                array('group' => $group), "%group% Everything Plans");
        } else {
            $this->content['plan_title'] = get_translation("everything_plans_title", "business_account/main_everything_plans",
                                                array(), "Everything Plans");
        }

        $this->data['sidebar'][] = $this->load->view('/business_account/account_menu', array( 'business' => $this->business ), true );
        $this->data['content'] = $this->load->view( $this->viewFileLocation . 'main_everything_plans', $this->content, true);

        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    /**
     * Shows product_plans
     *
     * @param string $group
     */
    public function product_plans($group = " ")
    {
        $this->rewards = \App\Libraries\DroplockerObjects\LockerLootConfig::isActive($this->business_id);
        if ( !empty( $this->rewards ) ) {
               $this->rewards_name = \App\Libraries\DroplockerObjects\LockerLootConfig::getName($this->business_id);
        }

        $active_plan = null;

        if ($this->customer_id) {
            $this->product_plan = \App\Libraries\DroplockerObjects\ProductPlan::search_aprax(array(
                "business_id" => $this->business_id,
                "customer_id" => $this->customer_id,
                "type"        => "product_plan",
                "active"     => 1
            ));

            $this->content['active_plan'] = !empty($this->product_plan) ? $this->product_plan[0] : null;

            $upgrade_plan_id = \App\Libraries\DroplockerObjects\ProductPlan::is_upgrading(
                $this->customer_id
            );

            if (!empty($upgrade_plan_id)) {
                $this->content['upgrade_plan'] = \App\Libraries\DroplockerObjects\ProductPlan::search_aprax(array(
                    "laundryPlanID" => $upgrade_plan_id
                ));
            }
        }

        $old_buser_id = $this->session->userdata('old_buser_id');

        $options = array(
            'business_id' => $this->business_id,
            'visible' => 1,
        );

        $options['(endDate is NULL or endDate > ' . gmdate('Y-m-d') . ')'] = null;

        $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');

        $options["type"] = 'product_plan';
        $plans = \App\Libraries\DroplockerObjects\BusinessProductPlan::search_aprax(
            $options, 
            false,
            "price" //order by
        );

        $num_plans = sizeof( $plans );

        for ($i = 0; $i < $num_plans; $i++) {

            // if the plan date is in the past, do not show it
            if (($plans[$i]->endDate < date('Y-m-d')) && $plans[$i]->days == 0) {
                unset($plans[$i]);
                continue;
            }

            if ( substr( $plans[$i]->endDate,0,10 ) != '0000-00-00') {
                //this plan has an endDate
                $plans[$i]->term = get_translation("product_plans_term_enddate", "business_account/main_product_plans",
                    array('endDate' => strftime($dateFormat, strtotime($plans[$i]->endDate))), "Now thru %endDate%");
            } else {
                $plans[$i]->term = get_translation("product_plans_term_recurring", "business_account/main_product_plans",
                        array('days' => $plans[$i]->days), "%days% Days (Recurring)");
            }
        }

        $q = $this->db->query("SELECT distinct(grouping) as grouping
                              FROM businessLaundryPlan
                              WHERE grouping!=''
                              AND business_id = {$this->business_id}
                              AND type = 'product_plan'");

        $groups = $q->result();
        $this->content['current_group'] = $group;
        $this->content['groups'] = $groups;
        $this->content['plans'] = $plans;
        
        $business = new Business($this->business_id);
        $this->content['business'] = $business;

        //this is for the sidebar menu
        $this->everything_plans = 1;
        $this->product_plans = 1;
        $this->set_plans_menu('laundry_plan');
        //
        

        $this->content['wfPrice'] = \App\Libraries\DroplockerObjects\ProductPlan::getDefaultPrice($this->customer_id, $this->business_id);
        $this->load->helper('ll');

        //if there is customer data, pass it to the content variable
        if ( !empty( $this->customer_id ) ) {
            $this->content['customer'] = new Customer( $this->customer_id );
        }

        $this->content['customer_id'] = $this->customer_id;

        if (is_numeric($group) && !empty($plans)) {
            $this->content['plan_title'] = get_translation("product_plan_title", "business_account/main_product_plans",
            array('name' => $plans[0]->name), "%name% Product Plan");
        } else if ($group && !is_numeric($group)) {
            $this->content['plan_title'] = get_translation("group_product_plans_title", "business_account/main_product_plans",
                                                array('group' => $group), "%group% Product Plans");
        } else {
            $this->content['plan_title'] = get_translation("product_plans_title", "business_account/main_product_plans",
                                                array(), "Product Plans");
        }

        $this->data['sidebar'][] = $this->load->view('/business_account/account_menu', array( 'business' => $this->business ), true );
        $this->data['content'] = $this->load->view( $this->viewFileLocation . 'main_product_plans', $this->content, true);

        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function green()
    {
        $this->template->write('title', "Eco-friendly cleaning - Laundry Locker", true);
        //$this->template->write_view('content', 'main_green', $this->data);
        $this->data['content'] = $this->load->view('main_green',array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function jobs()
    {
        $this->template->write('title', "Jobs - Laundry Locker", true);
        //$this->template->write_view('content', 'main_jobs', $this->data);
        $this->data['content'] = $this->load->view('main_jobs',array(),true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function testimonials()
    {
        $this->load->model('testimonial_model');
        $this->testimonial_model->business_id = $this->business->businessID;
        $this->data['testimonials'] = $this->testimonial_model->get();
        $this->template->write('title', "Testimonials - Laundry Locker", true);
        $this->template->write_view('content', 'main_testimonials', $this->data);
        $this->template->render();
    }

    public function storage()
    {
        $this->template->write('title', "Clothing Storage - Laundry Locker", true);
        $this->template->write_view('content', 'main_storage', $this->data);
        $this->template->render();
    }

    public function faqs()
    {
        $this->template->write('title', "Faqs - Laundry Locker", true);
        //$this->template->write_view('content', 'main_faqs', $this->data);
        $this->data['content'] = $this->load->view('main_faqs', array(), true);
        $this->template->write_view('content', $this->viewTemplate, $this->data);
        $this->template->render();
    }

    public function terms()
    {
        $this->template->write('title', "Terms of service - Laundry Locker", true);
        $this->template->write_view('content', 'main_terms', $this->data);
        $this->template->render();
    }

    function anytimeorders() 
    {
        $this->template->write('title', "Anytime orders - Laundry Locker", true);
        $this->template->write_view('content', 'main_anytimeorders', $this->data);
        $this->template->render();
    }

    protected function set_plans_menu($type = "laundry_plan")
    {
        $q = $this->db->query("SELECT count(*) as count
                              FROM businessLaundryPlan
                              WHERE business_id = {$this->business_id}
                              AND type = '".$type."'");

        $result = $q->result(); 

        if ($type == "laundry_plan") {      
            $this->plans = $result[0]->count;
        } else {
            $this->everything_plans = $result[0]->count;
        }
    }

    /*
    * Show locations on external sites using google maps
    *
    * Paste the code below on any page :
    *    <input id="map-search" class="controls" type="text" placeholder="Search Box" style="width:400px;" />(optional)
    *    <div id="map" style="width:400px;height:400px;"></div>
    *    <script type="text/javascript" src="https://myaccount.laundrylocker.loc/get/the/publicLocations.js?business_id=[business_id]&type=[public|private]"></script>
    */
    public function get_business_locations()
    {
        $business_id = (int)$this->input->get("business_id");

        if (empty($business_id)) {
            die('console.error("get/the/publicLocations.js => business_id not found")');
        }

        $type = $this->input->get("type");
        $type = ($type=="private")?"private":"public";

        $this->load->model('location_model');

        $options['public'] = ($type=="public")?"Yes":"No";
        $options['business_id'] = $business_id;
        $options['with_location_type'] = true;
        $locations = $this->location_model->get($options);

        foreach ($locations as $key => $location) {
            $locations[$key]->is_public = ((strtolower($location->public) === "yes") ? 1 : 0);
            $typeLabel = '(Private)';
            if ($locations[$key]->is_public) {
                $typeLabel = '(Public)';
            }
            $info = '<table><tr><td valign=top><span class=bodyBold><u>' . $location->companyName . " $typeLabel " . '</u>
          <br><br>' . $location->address . '</span><span class=bodyReg><br>' . $location->location . '</span>';
            $info .= ($location->hours) ? '<br><br><span class=bodyBold>Hours: </span><span class=bodyReg>' . $location->hours . '</apan></td>' : "";
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $location->locationID . '.jpg')) {
                $img = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $location->locationID . '.jpg');
                $info .= '<td style="padding:5px;"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src=/images/location_' . $location->locationID . '.jpg width=' . $img[0] . ' height=' . $img[1] . '></td>';
            }
            $info .= '</tr></table>';
            
            $locations[$key]->info = $info;
        }
        
        $json_locations = array();
        foreach ($locations AS $index => $location) {
            $json_locations[$index] = array(
                "locationID" => $location->locationID, 
                "address" => $location->address,
                "state"=>$location->state, 
                "city"=>$location->city, 
                "lat" => $location->lat, 
                "lon" => $location->lon, 
                "companyName"=>$location->companyName, 
                "info" => $location->info, 
                "abbreviation" => $location->abbreviation,
                "locationType_id" => $location->locationType_id,
                "is_public" => ((strtolower($location->public) === "yes") ? 1 : 0),
            );
        }

        $script = "";
        if (!empty($json_locations)) { 
            $script = file_get_contents("https://maps.googleapis.com/maps/api/js?v=3.9&sensor=false&libraries=places&key=" . get_default_gmaps_api_key());
            $script .= file_get_contents("https://cdn.rawgit.com/googlemaps/v3-utility-library/master/styledmarker/src/StyledMarker.js");
            $script .= "\n";
            $script .= "var dl__business_id = $business_id;\n";
            $script .= "var dl__business_location_data = ".json_encode($json_locations).";\n";
            $script .= file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/js/locations_map.js");
        }
        
        echo $script;
    }
    
    public function quick_register()
    {
        if ($this->input->post('quick_register_submit')) {
            $register_result = $this->do_quick_register();
            if ($register_result) {
                $this->quick_register_success($register_result);
                return;
            }
        }
        
        $content = array();
        
        $business_id = (int)$this->input->get("business_id");
        if (!$business_id) {
            set_flash_message('error', 'Missing business id');
            redirect('/main/quick_register');
            return;
        }
        
        $business = new Business($business_id);
        $content['business_id'] = $business_id;
        $content['business'] = $business;
        $content['business_subpath'] = strtolower(str_replace(array(' ','&'), '', $content['business']->companyName));
        $content['business']->image = $this->getBusinessImage($business_id, $content['business_subpath']);
        
        $location_id = (int)$this->input->get("location_id");
        $content['location_id'] = $location_id;
        $content['locations'] = $this->getBusinessLocations($business_id, $location_id);
        
        $content['promocode'] = $this->input->get("promocode");;
        $content['owner_id'] = (int)$this->input->get("owner_id");
        $content['owners'] = $this->getBusinessEmployees($business_id, $content['owner_id']);

        if ($business->facebook_api_key && $business->facebook_secret_key) {
            // create a FacebookLogin object
            $this->load->library('facebookloginlibrary', array(
                'appId' => $business->facebook_api_key,
                'secret' => $business->facebook_secret_key,
                'business_id' => $business_id,
            ));
            
            $redirect = get_business_account_url($business) . '/login';
            $content['facebook_url'] = $this->facebookloginlibrary->getLoginUrl($redirect);
        } else {
            $content['facebook_url'] = '';
        }
        
        $this->load->view('main_quick_register', $content);
    }
    
    protected function do_quick_register($send_email = true)
    {
        $register_result = array();
        
        $this->load->library("form_validation");
        $this->form_validation->set_rules("firstName", "First Name", "trim|required|trim");
        $this->form_validation->set_rules("lastName", "Last Name", "trim|required");
        $this->form_validation->set_rules("phone", "Phone Number", "trim|required");
        $this->form_validation->set_rules("location_id", "Location", "trim|required");
        $this->form_validation->set_rules("address2", "Unit / Apt #", "trim|required");
        $this->form_validation->set_rules("email", "Email", "trim|required|valid_email|matches[email_confirm]");
        $this->form_validation->set_rules("email_confirm", "Email Confirm", "trim|required|valid_email");
        $this->form_validation->set_rules("password", "Password", "trim|required|matches[password_confirm]");
        $this->form_validation->set_rules("password_confirm", "Password Confirm", "trim|required");
        $this->form_validation->set_rules("terms", "Accept terms", "trim|required");
        $this->form_validation->set_rules("owner_id", "Representative", "trim|required");
        $this->form_validation->set_rules("business_id", "Business", "trim|required");
 
        if ($this->form_validation->run()) {
            $business_id = $this->input->post("business_id");
            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $signup = "quick_register";

            $sql = "SELECT email FROM customer WHERE business_id = ? AND email = ?";
            $q = $this->db->query($sql, array($business_id, $email));
            if ($res = $q->result()) {
                if (strtolower($res[0]->email) == strtolower($email)) {
                    set_flash_message("error", 'Email is already registered');
                    return false;
                }
            } else {
                $this->load->model('customer_model');
                $newCustomer_id = $this->customer_model->new_customer($email, $password, $business_id, $how, $signup);
                if ($newCustomer_id) {
                    $promocode = $this->input->post("promocode");
                    if ($promocode) {
                        $register_result['has_promocode'] = true;
                        
                        $this->load->library('coupon');
                        $response = $this->coupon->applyCoupon($promocode, $newCustomer_id, $business_id);
                        if ($response['status'] == 'fail') {
                            $register_result['promocode_message'] = "Promotional code $promocode was invalid.";
                        } else {
                            $this->load->model('coupon_model');
                            $discount = $this->coupon_model->get_discount_value_as_string($promocode, $business_id);
                            $promo_message_success = "Promotional code '$promocode - $discount' was applied to your new account.";
                        }
                    } 
                    
                    $location_id = $this->input->post("location_id");
                    $this->load->model('location_model');
                    $location = $this->location_model->get_one(array(
                        'business_id' => $business_id,
                        'locationID' => $location_id
                    ));
                    if (!$location) {
                        $register_result['custom_message'] = "Provided location not found for the business";
                    };
                    
                    $new_customer = new Customer($newCustomer_id);

                    $new_customer->firstName   = $this->input->post("firstName");
                    $new_customer->lastName    = $this->input->post("lastName");
                    $new_customer->phone       = $this->input->post("phone");
                    
                    $new_customer->address1    = $location->address;
                    $new_customer->address2    = $this->input->post("address2");
                    $new_customer->city        = $location->city;
                    $new_customer->state       = $location->state;
                    $new_customer->zip         = $location->zipcode;
                    
                    $new_customer->location_id = $location_id;//default location
                    $new_customer->deliverTo_location_id = $location_id;
                    $new_customer->owner_id    = $this->input->post("owner_id");
                    
                    $new_customer->alertSent   = 1;

                    $copy_sms = get_business_meta($business_id, 'copy_phone_to_sms');
                    if (($copy_sms || $this->input->post("sms_copy")) && empty($new_customer->sms)) {
                        $new_customer->sms = $new_customer->phone;
                    }

                    if ($new_customer->address1 && $new_customer->city && $new_customer->state) {
                        $geo = geocode($new_customer->address1, $new_customer->city, $new_customer->state);
                        $new_customer->lat = str_replace(',', '.', $geo['lat']);
                        $new_customer->lon = str_replace(',', '.', $geo['lng']);
                    }

                    $business = new Business($business_id);
                    $new_customer->default_business_language_id = $business->default_business_language_id;

                    $new_customer->save();

                    $register_result['send_email'] = $send_email;
                    if ($send_email) {
                        $emailTemplate = null;
                        
                        // Use submited email template if not empty
                        if ($this->input->post("emailText") || $this->input->post("smsText")) {
                            $emailTemplate = (object) array(
                                'emailSubject' => $this->input->post("emailSubject"),
                                'emailText' => $this->input->post("emailText"),
                                'smsText' => $this->input->post("smsText"),
                            );
                        }

                        $action_array = array('do_action' => 'new_customer', 'customer_id' => $newCustomer_id, 'business_id' => $business_id, 'emailTemplate' => $emailTemplate);
                        Email_Actions::send_transaction_emails($action_array);
                    }
                    
                    $register_result['business_id'] = $business_id;
                    $register_result['customer_id'] = $newCustomer_id;
                    
                    return $register_result;
                } else {
                    set_flash_message("error", "Failed to create new Customer");
                    return false;
                }
            }
        } else {
            return FALSE;
        }
    }

    protected function quick_register_success($register_result)
    {
        $business = new Business($register_result['business_id']);
        
        $content['business'] = $business;
        $content['business_subpath'] = strtolower(str_replace(array(' ','&'), '', $content['business']->companyName));
        $content['business']->image = $this->getBusinessImage($register_result['business_id'], $content['business_subpath']);
        
        $content['thanks'] = get_translation('success_thanks', 'main_quick_register', array(
                'company_name' => $business->companyName,
            )
            , 'Thanks for registering with %company_name%!'
            , $register_result['customer_id']
        );
        
        if($register_result['has_promocode']) {
            if ($register_result['promocode_message']) {
                $content['promocode'] = $register_result['promocode_message'];
            } else {
                $content['promocode'] = get_translation('success_promocode', 'main_quick_register', array(
                    )
                    , 'Your	discount code has been added to your account and will be applied to your first order'
                    , $register_result['customer_id']
                );
            }
        }

        if ($register_result['send_email']) {
            $content['email'] = get_translation('success_email', 'main_quick_register', array(
                )
                , 'You will receive a confirmation email shortly with more information!'
                , $register_result['customer_id']
            );
        }

        if ($register_result['custom_message']) {
            $content['custom'] = $register_result['custom_message'];
        }

        $content['go_back'] = current_url() . '?' . $_SERVER['QUERY_STRING'];
        
        $this->load->view('main_quick_register_success', $content);
    }
    
    protected function getBusinessImage($business_id, $path)
    {
        $image = '';
        
        $this->load->model('image_model');
        $image = $this->image_model->get_placement_path('e-mail', $business_id);
        if (!$image) {
            $image = "/images/$path/logo.png";
        }
        
        return $image;
    }

    protected function getBusinessLocations($business_id, $location_id = '')
    {
        $locationFilter = array(
            "business_id" => $business_id, 
            "status" => "active"
        );
        
        if ($location_id) {
            $locationFilter['locationID'] = $location_id;
        };
        
        $business_locations = Location::search_aprax($locationFilter);
        $locations = array();
        foreach ($business_locations as $business_location) {
            $locations[$business_location->locationID] = $business_location->address;
        }

        natsort($locations);
        
        return $locations;
    }
    
    protected function getBusinessEmployees($business_id, $employee_id = '')
    {
        $employee_filter = array(
            "business_id" => $business_id, 
            "empStatus" => 1
        );
        
        if ($employee_id) {
            $employee_filter['employeeID'] = $employee_id;
        }
        
        $this->db->select("firstName, lastName, employeeID");
        $this->db->join("employee", "business_employee.employee_id=employee.employeeID");
        $get_employees_query = $this->db->get_where("business_employee", $employee_filter);
        $employees_result = $get_employees_query->result();
        
        $employees = array();
        foreach ($employees_result as $employee) {
            $employees[$employee->employeeID] = $employee->firstName . " " . $employee->lastName;
        }
        
        natsort($employees);
        
        return $employees;
    }
    
    public function terms_and_conditions($business_id = 3)
    {
        $business = new Business($business_id);
        $content['business'] = $business;
        $content['business_subpath'] = strtolower(str_replace(array(' ','&'), '', $content['business']->companyName));
        $content['business']->image = $this->getBusinessImage($business_id, $content['business_subpath']);
        
        $this->load->view('main_terms_and_conditions', $content);
    }
}
