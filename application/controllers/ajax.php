<?Php

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
class Ajax extends CI_Controller
{
    private $emailLogColumns;

    public function __construct()
    {
        parent::__construct();

        $this->emailLogColumns = array(
            array("display_name" => "Email ID", "query_name" => "emaillogID"),
            array("display_name" => "Subject", "query_name" => "subject"),
            array("display_name" => "Body", "query_name" => "body"),
            array("display_name" => "to", "query_name" => "to"),
            array("display_name" => "status", "query_name" => "status"),
            array("display_name" => "businessID", "query_name" => "business_id"),
            array('display_name' => 'Attachment', 'query_name' => 'attachment')
        );
        
        $this->loadContext();
    }
    
    protected function loadContext()
    {
        $CI = get_instance();
        $me = $CI;
        
        $me->business_id = $me->session->userdata('business_id');
        $me->buser_id = $me->session->userdata('buser_id');
        $me->user_id = $me->session->userdata('user_id');

        // get current business
        $me->load->model('business_model');
        $me->business = $me->business_model->get_by_primary_key($me->business_id);

        // if there is a employee session, load the object
        if ($me->buser_id) {
            $me->load->model("business_employee_model");
            $business_employee = $me->business_employee_model->get_by_primary_key($me->buser_id);

            $me->load->model("employee_model");
            $me->employee = $me->employee_model->get_by_primary_key($business_employee->employee_id);
            $me->employee_id = $business_employee->employee_id;
        }
        
        // if there is a customer session, load the object
        if ($me->user_id) {
            try {
                $customer = new Customer($me->user_id);
                $me->customer = $customer;
                $me->customer_id = $customer->customerID;
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            }
        }

    }

    /**
     * The following function is *. Do not use.
     */
    public function getEmailLogColumns()
    {
        $result = array();
        $counter = 0;
        foreach ($this->emailLogColumns as $index => $aColumn) {
            $counter = $index;
            $result[$index]['sTitle'] = $aColumn['display_name'];
        }
        echo json_encode($result);
    }

    /**
     * Used for server-side datatables data processing.
     * @throws Exception
     */
    public function getEmailLogData()
    {
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
        */

        $aColumns = array();
        foreach ($this->emailLogColumns as $emailLogColumn) {
            $aColumns[] = $emailLogColumn['query_name'];
        }

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "emailLogID";

        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' && is_numeric($_GET['iDisplayLength'])) {
            $sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
                    mysql_real_escape_string( $_GET['iDisplayLength'] );
        }

        /*
         * Ordering
        */
        $sOrder = "ORDER BY emailLogID DESC";
        if ( isset( $_GET['iSortCol_0'] ) ) {
            $column_key = $this->input->get("iSortCol_0");
            $sOrder = "ORDER BY  `". $this->emailLogColumns[$column_key]['query_name'] . "` " .  $this->input->get("sSortDir_0");
        }

        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
            $sWhere = "WHERE (";
            $emailLogColumnLength = count($this->emailLogColumns);
            for ($i = 0; $i < $emailLogColumnLength; $i++) {
                if (isset($this->emailLogColumns[$i]['where_name'])) {
                    $column_name = $this->emailLogColumns[$i]['where_name'];
                } else {
                    $column_name = $this->emailLogColumns[$i]['query_name'];
                }
                $sWhere .= "`$column_name` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
            }


            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ")";
        }

        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
                $sWhere .= "AND ";
                $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
            }
        }

        /*
         * SQL queries
        * Get data to display
        */
        $select = "SELECT SQL_CALC_FOUND_ROWS `". implode("`, `", $aColumns)."`";
        $sQuery = trim("
                $select
                FROM   emailLog
                $sWhere
                $sOrder
                $sLimit
                ");
                $rQuery = $this->db->query($sQuery);
                    $rResult = $rQuery->result();

                    /* Data set length after filtering */
                    $sQuery = "
                    SELECT FOUND_ROWS() AS found_rows
                    ";

                    $aResultFilterTotal = $this->db->query($sQuery)->row();
                    $iFilteredTotal = $aResultFilterTotal->found_rows;

                    /* Total data set length */
                    $sQuery = "
                    SELECT COUNT(`".$sIndexColumn."`) AS total
                    FROM   emailLog
                    ";
                    $aResultTotal = $this->db->query($sQuery)->row();
                    $iTotal = $aResultTotal->total;


                    /*
                    * Output
                    */
                        $output = array(
                                "sEcho" => intval($_GET['sEcho']),
                                "iTotalRecords" => $iTotal,
                                "iTotalDisplayRecords" => $iFilteredTotal,
                                "aaData" => array()
                                );

                                foreach ($rResult as $aRow) {
                                    $row = array();

                                    for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                                        $row[] = $aRow->{$aColumns[$i]};
                                    }

                                    $output['aaData'][] = $row;
                                }
        $output['columns'] = $aColumns;
        echo json_encode($output );
    }

    /**
     * google_geocode method accepts post data and then
     * queries google geocode api to return the JSON data
     *
     * $_POST valiables
     * --------------------
     * address
     * city
     * state
     * zipcode
     *
     */
    public function google_geocode()
    {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".str_replace(" ", "+",$_POST['address']).",+".str_replace(" ", "+",$_POST['city']).",+".str_replace(" ", "+",$_POST['state'])."&sensor=false&key=" . get_default_gmaps_api_key();
//echo $url;
        $response = file_get_contents($url);

        //response should be in JSON format
        echo $response;
    }

    /**
	 * TODO: this needs to be moved into DriverNotes controller
	 */
    public function inactiveDriverNotesAjax($business_id = null)
    {
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $aColumns = array('driverNotesID','route_id', 'locationAddress', 'companyName', 'note', 'employeeName', 'created', 'driverResponseCount');

        // DB table to use
        $sTable = 'driverNotes';

       $sqlNotes = "SELECT dn.driverNotesID, l.route_id, l.address as locationAddress, l.companyName, dn.note, 
                          dn.created, CONCAT( e.firstName, ' ', e.lastName ) AS employeeName,
                          COUNT(dpDN.note_id) AS driverResponseCount
                      FROM driverNotes dn
                      LEFT JOIN droid_postDriverNotes dpDN ON dpDN.note_id = dn.driverNotesID
                      JOIN location l ON l.locationID = dn.location_id
                      JOIN business_employee be
                        ON dn.createdBy = be.ID
                      JOIN employee e 
                        ON e.employeeID = be.employee_Id
                      WHERE dn.active = 0
                        AND dn.business_id = ?
                      GROUP BY dn.driverNotesID
                      ORDER BY dn.created DESC";


        $rResult = $this->db->query($sqlNotes, array($business_id));

        // Data set length after filtering
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;

        // Total data set length
        $iTotal = $this->db->count_all($sTable);

        // Output
        $output = array(
            'sEcho' => intval($_GET['sEcho']),
            'iTotalRecords' => $iTotal,
            //'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );

        foreach ($rResult->result_array() as $aRow) {
            $row = array();

            foreach ($aColumns as $col) {
                $row[] = $aRow[$col];
            }

            $output['aaData'][] = $row;
        }

        echo json_encode($output);

    }

    /**
     * @deprecated This action does not follow JQuery coding standards.
     *
     * creates a dropdown for searching a customer.
     *
     * The search term is past in the querystring (term)
     *
     * The result a json object from the array.
     * array key is the customerID and the value is the full name of the customer
     */
    public function customer_autocomplete()
    {
        $this->load->model('customer_model');
        $customers = $this->customer_model->autocomplete($this->input->get('term'), $this->session->userdata('business_id'));
        foreach ($customers as $c) {
            $res[$c->customerID] = getPersonName($c);
        }
        echo json_encode($res);
    }

    /**
     * The following function is a modification of the original customer_autocomplete function in that it returns the data in the standard format expected by the JQuery autocomplete widget
     * Expects the following GET parameters
     * term: a string to search for possible completions
     */
    public function customer_autocomplete_aprax()
    {
        parse_str($_SERVER['QUERY_STRING'], $_GET);

        $this->load->model('customer_model');
        $customers = $this->customer_model->autocomplete($_GET['term'], $this->session->userdata('business_id'));

        $result = array();
        foreach ($customers as $customer) {
            $result[] = array(
                "id" => $customer->customerID, 
                "label" => getPersonName($customer) . " ({$customer->customerID} - {$customer->address1})",
                "email"=>"{$customer->email}",
                "phone"=>"{$customer->phone}",
                "customerID"=>"{$customer->customerID}"
            );
        }
        echo json_encode($result);
    }

    /**
     * *******************************************
     * BEGIN
     * *******************************************
     * creates a dropdown of location addresses when searching for locations
     *
     * Expets the term in the querystring (term)
     * Displays the json object from the array
     *
     * array key is the locationID and the value is the address + address_2
     * ******************************************
     * END
     * *****************************************
     */
    public function location_autocomplete()
    {
        $business_id = $this->session->userdata('business_id');
        if (empty($business_id) || !is_numeric($business_id)) {
            $this->load->helper("ajax_helper");
            output_ajax_error_response("'location_id' must be present in the session");
        }

        $this->load->model('location_model');
        $locations = $this->location_model->autocomplete(urlencode($this->input->get('term')), $business_id);
        $res = array();
        foreach ($locations as $c) {
            $res[$c->locationID] = $c->address." ".$c->address2 . ($c->companyName ? " (".$c->companyName.")" : '');
        }

        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function search_orders()
    {
        $flag = 0;
        if ($_POST['submit']) {
            // pass the bag number, get the bagID
            if ($_POST['bag_id']!='') {
                $this->load->model('bag_model');
                $this->bag_model->bagNumber = $_POST['bag_id'];
                $res = $this->bag_model->get();
                if ($res) {
                $options['bag_id'] = $res[0]->bagID;
                    $flag = 1;
                }
            }
            $this->load->model('order_model');
            if ($_POST['ticketNum']!='') {$options['ticketNum'] = $_POST['ticketNum'];$flag=1;}
            if ($_POST['customer_id']!='') {$options['customer_id'] = $_POST['customer_id'];$flag=1;}
            if ($_POST['order_id']!='') {$options['orderID'] = $_POST['order_id'];$flag=1;}
            $options['business_id'] = $this->session->userdata('business_id');
            $options['with_customer'] = true;
            $options['with_bag'] = true;
            $options['with_location'] = true;
            $options['with_orderStatus'] = true;
            $options['with_orderPaymentStatus'] = true;
            $options['select'] = "bagNumber, firstName, lastName, customerID, orderID, location.address, ticketNum,
                                    orderStatusOption.name as statusName,
                                    orderPaymentStatusOption.name as paymentStatusName,statusDate";
            if ($flag == 1) {
            $array = $this->order_model->get($options);
            //echo $this->db->last_query();


            echo json_encode($array);
            } else {
                echo json_encode(array('status'=>'fail', 'error'=>"no arguments"));
            }
        }


    }

    /**
     * need to get the itemID from the barcode and then get the orders that have this item
     * tables: order, orderItem, barcode
     */
    public function search_barcodes()
    {
        if ($_POST['barcode']) {
            $this->load->model('barcode_model');
            $items = $this->barcode_model->get_order_item_from_barcode($_POST['barcode']);
            echo json_encode($items);
        }
    }

    public function search_lockers()
    {
        if ($_POST['lockerName']) {
            $this->load->model('locker_model');
            $options['like_lockerName'] = $_POST['lockerName'];
            $options['order_by'] = 'status';
            $options['with_location'] = true;
            $options['with_status'] = true;
            $options['select'] = "address, locationType.name as locationTypeName, lockerName, lockerLockType.name as type, lockerStatus.name as status, lockerStyle.name as style";
            $lockers = $this->locker_model->get($options);
            echo json_encode($lockers);
        }
    }

    /**
     *
     * Note, most of the function still contains code written by *.
     * @deprecated This function has unknown behavior.
     *
     * ---------------------------------
     * BEGIN
     * ---------------------------------
     *
     * Used to get the lockers in a location
     *
     * @uses locker_model
     *
     * This creates an array with the key as the lockerType and the value is the array of lockers.
     * The locker array: key = lockerID value = lockerName
     *
     * displayed as a json objects
     *
     * @example array('Lockers'=>array(12345=>304_Unit, 12347=>305_Unit));
     *
     * ---------------------------------
     * END
     * ---------------------------------
     */
    public function get_lockers_at_location()
    {
        if (!$this->input->post("location_id")) {
            $this->load->helper("ajax_helper");
            output_ajax_error_response("'location_id' must be passed as a POST parameter and must be numeric");
        }

        $this->load->model('locker_model');
        $options['location_id'] = $_POST['location_id'];
        $options['lockerStatus_id'] =1;
        $options['select'] = "lockerName, lockerID, lockerType";
        $options['order_by'] = "lockerName";
        $options['with_location'] = true;
        $lockers = $this->locker_model->get($options);

        foreach ($lockers as $locker) {
            $l[$locker->lockerID."-k"] = $locker->lockerName;
        }

        $fullArray = array(ucfirst($lockers[0]->lockerType)=>$l);
        echo json_encode($fullArray);
    }

    /**
     * Searches for a customer
     *
     * refer to customer_model->search_customer for $_POST Keys
     */
    public function search_customers()
    {
        $options = $_POST;
        $options['business_id'] = $this->session->userdata('business_id');

        $this->load->model('customer_model');
        $customers = $this->customer_model->search_customer($options);
        echo json_encode($customers);
    }

    /**
     * gets the roles for a business
     */
    public function get_roles()
    {
        $business_id = $_POST['business_id'];
        $this->load->model('aclroles_model');
        $this->aclroles_model->business_id = $business_id;
        $this->aclroles_model->order_by = "name";
        $roles = $this->aclroles_model->get();
        echo json_encode($roles);
    }

    /**
     * gets the bags that belong to a customer
     */
    public function get_bags_of_customer()
    {
        if (isset($_POST['customer_id'])) {
            $this->load->model('bag_model');
            $this->bag_model->customer_id = $_POST['customer_id'];
            $bags = $this->bag_model->get();
            echo json_encode($bags);
        }
    }

    /**
     * gets locations for a business
     */
    public function get_locations()
    {
        $this->load->model('location_model');
        $options['business_id'] = $this->session->userdata['business_id'];
        $options['sort'] = "address";
        $locations = $this->location_model->get($options);
        echo json_encode($locations);
    }

}
