<?php

use App\Libraries\DroplockerObjects\Supplier;
use App\Libraries\DroplockerObjects\Business;

class Suppliers extends MY_Admin_Controller
{
    public $submenu = 'admin';

    public function index()
    {
        $this->load->model('supplier_model');
        $this->supplier_model->business_id = $this->business_id;
        $this->supplier_model->select = "supplierBusiness_id, companyName, address, city, state, phone, supplierID";
        $this->supplier_model->join("business ON business.businessID = supplier.supplierBusiness_id"); 
        $content['suppliers'] = $this->supplier_model->get();
        $options['order_by'] = 'companyName';
        $content['business'] = $this->business_model->get($options);

        $this->renderAdmin('index', $content);
    }

    /**
     * The following action renders the interface for creating a new supplier
     */
    public function create_new_supplier()
    {
        $this->template->write('page_title','Create New Supplier', true);
        $this->load->model("business_model");
        $content = array();
        $content['businesses'] = $this->business_model->get_all_as_codeigniter_dropdown();

        $this->renderAdmin('create_new_supplier', $content);
    }

    public function add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("companyName", "Company Name",  "required");
        if ($this->form_validation->run()) {
            $business = new Business();
            $business->companyName = $this->input->post("companyName");
            $business->timezone = "America/Los_Angeles";
            $business->blank_customer_id = 0;
            $business->save();

            $supplier = new Supplier();
            $supplier->supplierBusiness_id = $business->{Business::$primary_key};
            $supplier->business_id = $this->business_id;
            if ($this->input->post("notes")) {
                $supplier->notes = $this->input->post("notes");
            }
            $supplier->save();
        }
        set_flash_message("success", sprintf("Created new supplier %s", $business->companyName));

        redirect("/admin/suppliers");
    }


    public function edit($id = null)
    {
        $this->load->model('supplier_model');

        if (isset($_POST['submit'])) {

            if (trim($_POST['companyName']) == '') {
                set_flash_message('error', 'Company name can not be empty');
                redirect("/admin/suppliers/edit/".$id);
            }

            $query_supplier = $this->db->get_where('supplier', array('supplierID' => $id));
            $supplier = $query_supplier->result();

            if (count($supplier)) {
                $supplier = $supplier[0];
               
                $this->db->query('UPDATE business SET companyName = ? WHERE businessID = ?', array($_POST['companyName'], $supplier->supplierBusiness_id));
                         
                
                $this->supplier_model->notes = $_POST['notes'];
                $this->supplier_model->enabled = $_POST['enabled'];
                $this->supplier_model->where = array('supplierID'=>$id);
                $this->supplier_model->update();

                //first delete all routes
                $deleteRoutes = "delete from supplierWFRoutes where supplier_id = ?";
                $q = $this->db->query($deleteRoutes, array($id));
                if (isset($_POST['supplierRoutes'])) {
                    //now update routes this supplier provides WF for
                    //now add them back
                    foreach ($_POST['supplierRoutes'] as $r) {
                        $addRoute = "insert into supplierWFRoutes (supplier_id, route_id, productType)
                        values (?, ?, ?)";
                        $this->db->query($addRoute, array($id, $r, $_POST['productType']));
                    }
                }

                
                //now update days this supplier services
                //first delete all
                $deleteDays = "delete from supplierServiceDays where supplier_id = ?";
                $q = $this->db->query($deleteDays, array($id));
                //now add them back
                if(!isset($_POST['supplierServiceDays'])){
                    $_POST['supplierServiceDays'] = array();
                };
                foreach ($_POST['supplierServiceDays'] as $day_id) {
                    $addDay = "insert into supplierServiceDays (supplier_id, day_id)
                    values (?, ?)";
                    $this->db->query($addDay, array($id, $day_id));
                }

                set_flash_message('success', 'Supplier updated');
                redirect("/admin/suppliers/edit/".$id);

            } else {
                 set_flash_message('error', 'Supplier with ID '.$id.' does not exists');
                 redirect("/admin/suppliers");
            }
                 
        }

        //products that have pricing for this supplier
        $getSupplierProducts = "SELECT p.name, productID, product_processID, price, cost, minQty,
            pr.name as process_name, displayName, product_supplierID
        FROM product_supplier ps
        JOIN product_process pp ON pp.product_processID = ps.product_process_id
        LEFT JOIN process pr ON processID = pp.process_id
        JOIN product p ON productID = pp.product_id
        WHERE supplier_id = ?
        ORDER BY p.name";

        $q = $this->db->query($getSupplierProducts, array($id));
        $content['supplierPricing'] = $q->result();

        $getSupplierInfo = "SELECT * FROM business
        JOIN supplier ON (supplierBusiness_id = businessID)
        WHERE supplierID = ?";
        $q = $this->db->query($getSupplierInfo, array($id));
        $content['supplierInfo'] = $q->result();

        //get all products
        $sqlGetProducts = "SELECT * FROM product WHERE business_id = ? ORDER BY name";
        $query = $this->db->query($sqlGetProducts, array($this->business_id));
        $content['products'] = $query->result();

        //get the processes
        $this->load->model('process_model');
        $content['processes'] = $this->process_model->get();

        $sqlGetAllRoute = "SELECT DISTINCT(route_id) FROM location WHERE business_id = ?";
        $query = $this->db->query($sqlGetAllRoute, array($this->business_id));
        $content['allRoutes'] = $query->result();

        $sqlGetSupplierRoute = "SELECT * FROM supplierWFRoutes WHERE supplier_id = ?";
        $query = $this->db->query($sqlGetSupplierRoute, array($id));
        $content['supplierRoutes'] = $query->result();

        $sqlGetAllDays = "SELECT * FROM day";
        $query = $this->db->query($sqlGetAllDays);
        $content['allDays'] = $query->result();

        $sqlGetSupplierServiceDays = "SELECT * FROM supplierServiceDays WHERE supplier_id = ?";
        $query = $this->db->query($sqlGetSupplierServiceDays, array($id));
        $content['supplierDays'] = $query->result();

        $this->renderAdmin('edit', $content);
    }

    public function delete($id = null)
    {

        if (!is_superadmin()) {
            set_flash_message('error', "You're not allowed to delete a supplier.");
            redirect('/admin/suppliers');
        }

        $this->load->model('supplier_model');
        $this->supplier_model->supplierID = $id;

        $sql = 'SELECT count(*) as total_products FROM product_supplier WHERE supplier_id = ?';
        $query = $this->db->query($sql, array($id));

        $result = $query->result();

        if (isset($result[0]->total_products) && ($result[0]->total_products == 0) ) {
            if ($this->supplier_model->delete()) {
                set_flash_message('success', "Deleted Supplier");
            } else {
                set_flash_message('error', "Unexpected Error: Supplier not deleted.");
            }
            redirect("/admin/suppliers");
        } else {
             set_flash_message('error', "Can't delete a Supplier with products.");
             redirect("/admin/suppliers");
        }
    }

    public function add_product()
    {
        $this->load->model('product_supplier_model');
        $this->product_supplier_model->supplier_id = $_POST['supplier'];
        $this->product_supplier_model->product_process_id = $_POST['product_process_id'];
        $this->product_supplier_model->cost = $_POST['cost'];
        $this->product_supplier_model->minQty = $_POST['minQty'];
        $this->product_supplier_model->dateCreated = date('c');
        $this->product_supplier_model->dateUpdated = date('c');
        $this->product_supplier_model->insert();

        set_flash_message('success', "New Product Added To Supplier");

        redirect('/admin/suppliers/edit/'.$_POST['supplier']);
    }

    public function delete_product($product_supplierID = null, $supplierID = null)
    {
        $this->load->model('product_supplier_model');
        $this->product_supplier_model->product_supplierID = $product_supplierID;
        $this->product_supplier_model->delete();

        set_flash_message('success', "Product Deleted From Supplier");

        redirect('/admin/suppliers/edit/'.$supplierID);
    }


}
