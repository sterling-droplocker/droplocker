<?php

class Data_Dump extends MY_Admin_Controller
{
    /**
     * @var CI_DB_active_record
     */
    public $reporting_database;

    public $submenu = 'admin';

    public function __construct()
    {
        parent::__construct();

        // load reporting databse
        $this->reporting_database = $this->load->database("reporting", true);
    }

    public function index()
    {
        $time = mktime(0, 0, 0, date('m') - 5, 1, date('Y'));
        $content['end_month'] = date('m');
        $content['end_year'] = date('Y');
        $content['start_month'] = date('m', $time);
        $content['start_year'] = date('Y', $time);
        $content['months'] = array_combine(range(1, 12), range(1, 12));
        $content['years'] = array_combine(range(date('Y'), 2000, -1), range(date('Y'), 2000, -1));

        $this->renderAdmin("index", $content);
    }

    public function dump()
    {
        $format = $this->input->get('output_format');
        $action = $this->input->get("table");
        $dateFrom = $this->input->get('from');
        $dateTo = $this->input->get('to');
        $this->load->model("datadumpslog_model");

        $options = array(
            'business_id' => $this->business_id,
            'employee_id' => $this->employee_id ,
            'format' => $format,
            'dumpedTable' => $action,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'dateCreated' => gmdate('Y-m-d H:i:s'),
        );

        if ($action == "locations") {
           $start_month = $this->input->get("start_month");
           $start_year = $this->input->get("start_year");
           $end_month = $this->input->get("end_month");
           $end_year = $this->input->get("end_year");

           $options["dateFrom"] = $start_year.'-'.$start_month.'-01';
           $last_day_of_month = date('t',strtotime($options["dateFrom"]));
           $options["dateTo"] = $end_year.'-'.$end_month.'-'.$last_day_of_month;
        }

        $this->datadumpslog_model->save($options);

        //a zoho dump generates a public url that returns a json file so we don't need to execute the query
        $site_url = $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"];
        if ($format == 'zoho') {
            $salt = $this->config->config['hash_salt'];
            $hashed_bid = md5($this->business_id.$salt);
            $url = $site_url.'/data_dump/zoho?bid=';
            if ($action != "locations") {
                $records = $this->input->get('records');
                $from = $this->input->get('from');
                $to = $this->input->get('to');
                $url .= $hashed_bid.'&t='.$action.'&r='.$records.'&from='.$from.'&to='.$to;
            } else if ($action == "locations") {
                $grouping = $this->input->get("grouping");
                $url .= $hashed_bid.'&t='.$action.'&start_month='.$start_month.'&start_year='.$start_year.'&end_month='.$end_month.'&end_year='.$end_year.'&grouping='.$grouping;
            }
            $content = array('url' => $url, 'table' => $action);
            $this->renderAdmin("dump", $content);
        } else {
            $url = "";
            if ($action == 'locations') {
                $salt = $this->config->config['hash_salt'];
                $hashed_bid = md5($this->business_id.$salt);
                $url = $site_url.'/data_dump/zoho?bid=';
                $grouping = $this->input->get("grouping");
                $url .= $hashed_bid.'&t='.$action.'&start_month='.$start_month.'&start_year='.$start_year.'&end_month='.$end_month.'&end_year='.$end_year.'&grouping='.$grouping;  
            }
            $this->$action($url, $format);
        }
    }

    /**
     * Exports locations table
     */
    public function locations($url, $format)
    {
        $data = file_get_contents($url); 
        if (!empty($data)) {
            $data = json_decode($data, true);
            if (!empty($data["totals"])) {
                unset($data["totals"]);
            }

            $locations = $data["locations"];
            foreach($locations as $k=>$l) {
                $sql = "SELECT * FROM location WHERE address=? AND business_id=? LIMIT 0,1";
                $query = $this->db->query($sql, array($l["address"], $this->business_id));
                $current_location = $query->result();
                $locations[$k] = (object)array_merge((array)$current_location[0], $l);
            }

            $params["isLocations"] = true;
            $params["data"] = $data["locations"];
            $this->dumpQuery(NULL, $params, 'locations');
        }
    }

    /**
     * Exports claim table
     */
    public function claims()
    {
        // convert datetime columns into local time
        $fields = $this->getFields('claim');
        $fields = implode(", ", $fields);

        $conditions = array("business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "created >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "created <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM claim WHERE $conditions";
        $this->dumpQuery($sql, $params, 'claim', 'Claims');
    }

    /**
     * Exports reports_customer table
     */
    public function customers()
    {
        // convert datetime columns into local time
        $fields = $this->getFields('reports_customer');
        $fields = implode(", ", $fields);

        $conditions = array("business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "signupDate >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "signupDate <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM reports_customer WHERE $conditions";
        $this->dumpQuery($sql, $params, 'customers', 'Customer');
    }

    /**
     * Exports report_orders table
     */
    public function orders()
    {
        // convert datetime columns into local time
        $fields = $this->getFields('report_orders');
        $fields[] = 'lat, lon';

        $fields = implode(", ", $fields);

        $conditions = array("report_orders.business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "report_orders.dateCreated >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "report_orders.dateCreated <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM report_orders
            LEFT JOIN location ON (location_id = locationID)
            WHERE $conditions";

        $this->dumpQuery($sql, $params, 'orders', 'Order');
    }

    /**
     * Exports orderItem table
     */
    public function order_items()
    {
        $timezone = $this->business->timezone;

        $conditions = array("orders.business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "orders.dateCreated >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "orders.dateCreated <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);

        $sql = "SELECT orderItemID, order_id, qty, unitPrice, orderItem.notes AS itemNotes,
            convert_tz(orderItem.updated, 'GMT', '{$timezone}') AS updated,
            cost, product.productID as productID, product.name as productName, productType,
            productCategory.productCategoryID AS productCategoryID, productCategory.name AS productCategoryName,
            supplierID, business.companyName AS supplierCompanyName, location.companyName As locationName, location.locationID AS locationID, location.route_id AS routeID,
            item.color AS itemColor, item.size AS itemSize, item.manufacturer AS itemManufacturer
        FROM orderItem
        LEFT JOIN item on (orderItem.item_id = item.itemID)
        JOIN orders ON (orderID = order_id)
        JOIN locker ON (lockerID = locker_id)
        JOIN location ON (locationID = location_ID)
        JOIN product_process ON (product_processID = orderItem.product_process_id)
        JOIN product ON (productID = product_id)
        LEFT JOIN supplier ON (supplier_id = supplierID)
        JOIN productCategory ON (productCategoryID = productCategory_id)
        LEFT JOIN business ON (supplier.supplierBusiness_id = business.businessID)
        WHERE $conditions";

        $this->dumpQuery($sql, $params, 'order_items', 'OrderItem');
    }

    /**
     * Exports orderCharge table
     */
    public function order_charges()
    {
        $timezone = $this->business->timezone;

        $conditions = array("orders.business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "orders.dateCreated >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "orders.dateCreated <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);

        $sql = "SELECT orderChargeID, orderCharge.order_id, chargeType, chargeAmount,
        convert_tz(orderCharge.updated, 'GMT', '{$timezone}') AS updated,
        customerDiscount_id,
        customerDiscount.amount AS discountAmount,
        customerDiscount.amountType AS discountAmountType,
        customerDiscount.amountApplied AS discountAmountApplied,
        customerDiscount.description AS discountDescription,
        customerDiscount.extendedDesc AS discountExtendedDescription,
        customerDiscount.couponCode AS discountcouponCode,
        convert_tz(customerDiscount.expireDate , 'GMT', '{$timezone}') AS discountExpireDate,
        customerID, customer.firstName,  customer.lastName,
        orders.closedGross,
        locationID,
        location.companyName AS locationName,
        location.address AS locationAddress
        FROM orderCharge
        JOIN orders ON (orderID = orderCharge.order_id)
        LEFT JOIN customerDiscount ON (customerDiscountID = customerDiscount_id)
        LEFT JOIN customer ON (customerID = orders.customer_id)
        LEFT JOIN locker ON (lockerID = orders.locker_id)
        LEFT JOIN location ON (locationID = locker.location_id)
        WHERE $conditions";

        $this->dumpQuery($sql, $params, 'order_items', 'OrderItem');
    }

    /**
     * Exports driverNotes table
     */
    public function driver_notes()
    {
        $timezone = $this->business->timezone;

        $conditions = array("driverNotes.business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "driverNotes.created >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "driverNotes.created <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);

        $sql = "SELECT driverNotes.*, location.address, firstName, lastName,
        convert_tz(driverNotes.created, 'GMT', '{$timezone}') AS created,
        convert_tz(driverNotes.completed, 'GMT', '{$timezone}') AS completed
        FROM driverNotes
        JOIN location ON (location_id = locationID)
        JOIN employee ON (employeeID = completedBy)
        WHERE $conditions";

        $this->dumpQuery($sql, $params, 'order_items', 'OrderItem');
    }

    /**
     * Exports time cards table
     */
    public function timeCards()
    {
        // convert datetime columns into local time
        $fields = array("timeCard.weekOf", "employee.firstName", "employee.lastName", "timeCardDetail.day", "timeCardDetail.timeInOut", "timeCardDetail.in_out", "timeCardDetail.note");
        $fields = implode(", ", $fields);

        $conditions = array(
                "timeCardDetail.type = ?",
                "business_employee.business_id = ?"
            );

        $params = array('Regular', $this->business_id);

        if (!empty($this->input->get("from"))) {
            $conditions[] = "date(timeCardDetail.day) >= ?";
            $date = $this->input->get("from") . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($this->input->get("to"))) {
            $conditions[] = "date(timeCardDetail.day) <= ?";
            $date = $this->input->get("to") . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM timeCard 
            JOIN employee ON employee.employeeID = timeCard.employee_id
            JOIN timeCardDetail ON timeCardDetail.timeCard_id = timeCard.timeCardID
            JOIN business_employee ON business_employee.employee_id = employee.employeeID
            WHERE $conditions";
        $this->dumpQuery($sql, $params, 'timeCard', 'Time Cards');
    }

    /**
     * Exports customerDiscounts report
     */
    public function customerDiscounts()
    {
        // convert datetime columns into local time
        $conditions = array("c.business_id = ?");
        $params = array($this->business_id);

        if (!empty($_GET['from'])) {
            $conditions[] = "cd.origCreateDate >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "cd.origCreateDate <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT 
                    cd.customer_id,
                    c.firstName as 'first_name',
                    c.lastName as 'last_name',
                    c.email as 'email',
                    cd.couponCode,
                    cd.description,
                    cd.amountType,
                    cd.amount,
                    cd.amountApplied,
                    cd.order_id,
                    o.closedGross as 'gross_rev',
                    o.dateCreated as 'order_created_date',
                    l.address2 as 'building',
                    l.locationID,
                    l.companyName as 'location_name',
                    l.address as 'location_address',
                    cd.origCreateDate as 'created'
                FROM 
                    customerDiscount cd JOIN orders o on o.orderID = cd.order_id
                    JOIN customer c on c.customerID = cd.customer_id
                    JOIN location l on l.locationID = cd.location_id 
                WHERE 
                    $conditions";
        $this->dumpQuery($sql, $params, 'customerDiscount', 'Customer Discounts');
    }

    /**
     * Returns an array of the columns for the table with dates converted into local timezone
     *
     * @param string $table
     * @return array
     */
    protected function getFields($table)
    {
        $timezone = $this->business->timezone;

        $fields = array();
        foreach ($this->reporting_database->field_data($table) as $column) {
            if (in_array($column->type, array("datetime", "timestamp"))) {
                $fields[] = "convert_tz({$table}.{$column->name}, 'GMT', '{$timezone}') AS {$column->name}";
                continue;
            }

            $fields[] = "{$table}.{$column->name}";
        }

        return $fields;
    }

    /**
     * Exports a SQL query into CSV, JSON, XML
     *
     * It uses GET parameter format to decide the format
     *
     * @param mixed $sql    SQL query | array
     * @param array  $params replacements for the query placeholders
     * @param string $prefix prefix of the file generated
     * @param string $row    name of the element for each row in XML export
     */
    protected function dumpQuery($sql, $params = array(), $prefix = '', $row = 'row')
    {
        set_time_limit(0); //dump can take long

        $isLocations = (!empty($params["isLocations"]))?true:false;

        if (!$isLocations) {
          $query = $this->reporting_database->query($sql, $params);  
        } else {
            $this->load->model("data_dump_model");
        }

        $format = $_GET['format'];
        $busines_slug = preg_replace('/[^A-Za-z0-9]+/i', '_', trim($this->business->companyName));
        $prefix = preg_replace('/[^A-Za-z0-9]+/i', '_', trim($prefix));
        $date = date('Y-m-d-H.i.s');
        $filename = sprintf('%sdump_%s_%s.%s', $prefix ? $prefix . '_' : '', $busines_slug, $date, $format);

        // set get parameter show=1 to prevent download
        if (empty($_GET['show'])) {
            // disposition / encoding on response body
            header("Content-Disposition: attachment;filename={$filename}");
            header("Content-Transfer-Encoding: binary");
        }

        // export the data
        switch ($format) {
            case 'csv':
                header("Content-Type: text/plain; charset=utf-8");
                if (!$isLocations) {
                    $this->exportCSV($query);
                } else {
                    $this->data_dump_model->exportCSV($params["data"]);
                }
            break;

            case 'json':
                header("Content-Type: application/json; charset=utf-8");
                if (!$isLocations) {
                    $this->exportJSON($query);
                } else {
                    $this->data_dump_model->exportJSON($params["data"]);
                }
            break;

            case 'xml':
                header("Content-Type: text/xml; charset=utf-8");
                if (!$isLocations) {
                    $this->exportXML($query, $row);
                } else {
                    $this->data_dump_model->exportXML($params["data"]);
                }
            break;

            default: die("Invalid format '$format'");
        }

        die();
    }

    /**
     * Exports a query in CSV format
     *
     * @param CI_DB_mysql_result $query
     */
    protected function exportCSV($query, $headers = true)
    {
        if (empty($query)) {
            return false;
        }

        $fout = fopen('php://output', 'w');

        for($i = 0 ; $i < $query->num_rows; $i++) {
            $row = $query->row_array($i);

            if (!$i && $headers) {
                fputcsv($fout, array_keys($row));
            }

            fputcsv($fout, $row);
        }
        fclose($fout);

        return $query->num_rows;
    }

    /**
     * Exports a query in JSON format
     *
     * @param CI_DB_mysql_result $query
     */
    protected function exportJSON($query)
    {
        if (empty($query)) {
            return false;
        }

        echo "[\n";

        for($i = 0 ; $i < $query->num_rows; $i++) {
            $row = $query->row_array($i);
            echo $i ? ",\n " : " ";
            echo json_encode($row);
        }

        echo "\n]";

        return $query->num_rows;
    }

    /**
     * Exports a query in XML format
     *
     * @param CI_DB_mysql_result $query
     * @param string $entity
     */
    protected function exportXML($query, $entity)
    {
        if (empty($query)) {
            return false;
        }

        $writer = new XmlWriter();
        $writer->openURI('php://output');
        $writer->startDocument('1.0', 'utf-8');
        $writer->startElement('Data');
        $writer->setIndent(true);

        for($i = 0 ; $i < $query->num_rows; $i++) {
            $row = $query->row_array($i);

            $writer->startElement($entity);
            foreach ($row as $key => $value) {
                $writer->writeElement($key, iconv("UTF-8", "UTF-8//IGNORE", $value));
            }
            $writer->endElement();
        }

        $writer->endElement();
        $writer->flush();

        return $query->num_rows;
    }

    public function logs($offset = 0)
    {
        $this->load->model("datadumpslog_model");
        $limit = 20;
        $total = $this->datadumpslog_model->countTotal();
        $this->load->library('pagination');
        $config['base_url'] = '/admin/data_dump/logs/';
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;
        $config['num_links'] = 10;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);

        $content['total'] = $total;
        $content['paging'] = $this->pagination->create_links();

        $options['business_id'] = $this->business_id;
        $options['limit'] = $limit;
        $options['offset'] = $offset;
        $options['sort'] = 'lastName';
        $options['select'] = 'customerID, customer.firstName, customer.lastName, customer.phone, customer.email, customer.username, customer.signupDate, customer.sms, customer.default_business_language_id ';
        $content['logs'] = $this->datadumpslog_model->get_by_business($options);
        $content['timezone'] = $this->business->timezone;

        $this->renderAdmin('logs', $content);
    }


}