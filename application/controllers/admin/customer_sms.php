<?php

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Validation_Exception;

class Customer_Sms extends MY_Admin_Controller
{

    public $submenu = 'customers';

    public function __construct()
    {
        parent::__construct();
    }

    public function new_sms($customer_id = null)
    {
        if (!is_numeric($customer_id)) {
            set_flash_message("error", "Customer ID must be numeric and passed as segment 4 of the URL.");
            return $this->goBack("/admin/customers/view");
        }

        $customer = new Customer($customer_id);
        if ($customer->business_id != $this->business_id) {
            set_flash_message("error", "This customer does not belong to your business.");
            return $this->goBack("/admin/customers/view");
        }
        $content['customer'] = $customer;

        $this->setPageTitle('Customer Sms');
        $this->renderAdmin('customer_sms', $content);

    }

    public function send_customer_sms($customer_id = null)
    {

        try {

            if (!is_numeric($customer_id)) {
                set_flash_message("error", "Customer ID must be numeric and passed as segment 4 of the URL.");
                return $this->goBack("/admin/customers/view");
            }

            $this->load->library("form_validation");
            $this->form_validation->set_rules("smsText", "SMS Text", "required");

            if ($this->form_validation->run()) {
                $customer = new Customer($customer_id);
                if ($customer->business_id != $this->business_id) {
                    set_flash_message("error", "This customer does not belong to your business.");
                    return $this->goBack("/admin/customers/view");
                }

                if($customer){
                    Email_Actions::send_sms($customer, $this->input->post("smsText"), $this->business_id, array());
                }

            } else {
                set_flash_message('error', validation_errors());
                redirect("/admin/customer_sms/new_sms/".$customer_id."");
            }

        }catch (Validation_Exception $e) {
            set_flash_message('error', $e->getMessage());
            redirect("/admin/customer_sms/new_sms/".$customer_id."");
        }

        set_flash_message('success', "Sms sent");
        redirect("/admin/customer_sms/new_sms/".$customer_id."");
    }

}