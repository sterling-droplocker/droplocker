<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Business_Language;
use App\Libraries\DroplockerObjects\InvoiceCustomField;
use App\Libraries\DroplockerObjects\InvoiceCustomFieldValue;

class Invoice_Custom_Fields extends MY_Admin_Controller
{

    public $submenu = 'admin';

    function __construct()
    {
        parent::__construct();
        $this->load->model('invoicecustomfield_model');
    }

    function index()
    {

        $fields = $this->invoicecustomfield_model->get_aprax(array(
            'business_id' => $this->business_id
        ), 'sortOrder ASC');

        $content = compact('fields');
        $this->pageTitle = "Invoice Custom Fields";
        $this->renderAdmin('index', $content);
    }

    function add($field_name = null)
    {
        $this->load->model('business_language_model');
        $languages = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $field_name = $this->input->post('name');

        if ($field_name) {

            $field = $this->getFieldByName($field_name);
            if (!empty($field)) {
                set_flash_message("error", "A field with the same name already exists, edit that one instead");
                return redirect('/admin/invoice_custom_fields/edit/' . $option->invoiceCustomFieldID);
            }

            $this->save();
        }

        $field = (object) array(
            'name' => $field_name,
            'sortOrder' => $this->invoicecustomfield_model->getMaxSortOrder($this->business_id) + 1
        );

        $title = $this->input->post('title');
        $value = $this->input->post('value');

        $content = compact('field', 'languages', 'title', 'value');
        $this->pageTitle = "Add Invoice Custom Fields";
        $this->renderAdmin('add', $content);
    }

    function edit($field_id = null)
    {
        if (!ctype_digit($field_id)) {
            $field = $this->getFieldByName(urldecode($field_id));
            if ($field) {
                return redirect('/admin/invoice_custom_fields/edit/' . $field->invoiceCustomFieldID);
            } else {
                return redirect('/admin/invoice_custom_fields/add/' . $field_id);
            }
        }

        $field = $this->getFieldByID($field_id);
        if (empty($field)) {
            set_flash_message("error", "The field does not exists");
            return redirect('/admin/invoice_custom_fields');
        }

	    // Versions prior to PHP 5.5 apparently don't like calling empty() on non-variables
	    $name = $this->input->post('name');
        if (!empty($name)) {
            $this->save($field);
        }

        $this->load->model('business_language_model');
        $languages = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $values = $field->getValues();
        $content = compact('field', 'languages', 'values', 'new_sort_order');
        $this->pageTitle = "Edit Custom Fields";
        $this->renderAdmin('edit', $content);
    }

    function delete($field_id = null)
    {
        $field = $this->getFieldByID($field_id);
        if (empty($field)) {
            set_flash_message("error", "The field does not exists");
            return redirect('/admin/invoice_custom_fields');
        }

        if (!empty($_POST['delete'])) {
            $field->delete();
            set_flash_message("success", "The field has been deleted");
            return redirect('/admin/invoice_custom_fields');
        }

        $content = compact('field');
        $this->pageTitle = "Delete Custom Field";
        $this->renderAdmin('delete', $content);
    }


    protected function getFieldByName($field_name)
    {
        $fields = InvoiceCustomField::search_aprax(array(
            'name' => $field_name,
            'business_id' => $this->business_id
        ));
        return $fields ? current($fields) : false;
    }

    /**
     * Gets a CField by id checking the business_id
     *
     * @param Integer $field_id
     * @return InvoiceCustomField
     */
    protected function getFieldByID($field_id)
    {
        $fields = InvoiceCustomField::search_aprax(array(
            'invoiceCustomFieldID' => $field_id,
            'business_id' => $this->business_id
        ));
        return $fields ? current($fields) : false;
    }

    /**
     * Gets a Value by id checking the business_id
     *
     * @param Integer $value_id
     * @return InvoiceCustomFieldValue
     */
    protected function getValueByID($value_id)
    {
        $values = InvoiceCustomFieldValue::search_aprax(array(
            'invoiceCustomFieldValueID' => $value_id,
            'business_id' => $this->business_id
        ));
        return $values ? current($values) : false;
    }

    protected function save($field = null)
    {
        $title = $this->input->post('title');
        $value = $this->input->post('value');
        $validate = true;

        foreach ($title as $k => $v) {
            if (empty($v) || empty($value[$k])) {
                $validate = false;
                break;
            }
        }

        if (!$validate) {
            set_flash_message_now('error', 'Please complete all possible translations.');
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required|trim|is_numeric');

            if ($this->form_validation->run()) {
                $name = $this->input->post('name');
                $sortOrder = $this->input->post('sortOrder');

                if ($field == null) {
                    $field = new InvoiceCustomField();
                }

                $field->name = convert_to_slug($name);
                $field->sortOrder = $sortOrder;
                $field->business_id = $this->business_id;

                try {
                    $field->save();
                    $field->saveValues($title, $value);
                    set_flash_message("success", "The field has been saved");
                    return redirect('/admin/invoice_custom_fields/');
                } catch (Exception $e) {
                     set_flash_message_now("error", $e->getMessage());
                }
            } else {
                set_flash_message_now("error", validation_errors());
            }
        }
    }
}
