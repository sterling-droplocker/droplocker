<?php

use App\Libraries\DroplockerObjects\UpchargeGroup;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\ProductCategory;

class Modules extends MY_Admin_Controller
{

    public $submenu = 'admin';

    public function wash_and_fold($type = 'products', $master = false)
    {
        $moduleID = 1;
        $this->show($moduleID, $type, $master);
    }

    public function dry_clean($type = 'products', $master = false)
    {
        $moduleID = 3;
        $this->show($moduleID, $type, $master);
    }

    public function shoe($type = 'products', $master = false)
    {
        $moduleID = 9;
        $this->show($moduleID, $type, $master);
    }

    /**
     * Displays the product management view for the specified module.
     *
     * $moduleID selects the module to display, posibles values are:
     *  - 1 Wash and Fold
     *  - 3 Dry Cleaning
     *  - 9 Shoes
     *
     * $type can be 'product' or 'preferences', and will determine what type
     * of products it will show.
     *
     * If $master is true and in the server is in bizzie mode, it will display
     * the module preferences of the master business.
     *
     * @param int $moduleID
     * @param string $type
     * @param bool $master
     */
    public function show($moduleID, $type = 'products', $master = false)
    {
        $content = array();

        if (!is_numeric($moduleID)) {
            set_flash_message("error", "The parameter 'moduleID' must be passed as a GET parameter.");

            return redirect($this->agent->referrer());
        }

        $moduleID = (int) $moduleID;

        $this->load->model("module_model");
        $content['module'] = $this->module_model->get_by_primary_key($moduleID);

        $content['master'] = $master ? 1 : '';
        if ($master && in_bizzie_mode()) {
            $master_business = get_master_business();
            $business_id = $master_business->businessID;
            $content['add_product_action'] = "/admin/products/add_master_product";
        } else {
            $business_id = $this->business_id;
            $content['add_product_action'] = "/admin/products/add_product";
        }
        
        $content['update_image_action'] = '/admin/products/update_product_image';

        $content['base_url'] = '/admin/modules/' . $this->uri->segment(3);
        $content['type'] = $type;
        switch ($type) {
            case 'products': $productType = 'product'; break;
            case 'preferences': $productType = 'preference'; break;
            case 'all': $productType = null; break;
            default: $productType = null; break;
        }

        $this->load->model("productCategory_model");
        $this->load->model('upchargeGroup_model');
        $this->load->model("serviceType_model");
        $this->load->model("process_model");
        $this->load->model('taxgroup_model');

        // generate the product sections
        $content['productCategories'] = $this->productCategory_model->get_productCategories_with_products($moduleID, $business_id, $productType);
        $content['productCategories_with_no_products'] = $this->productCategory_model->get_productCategories_with_no_products($this->business_id, $moduleID);

        $content['business_id'] = $business_id;

        //This variable stores the ID of the module that we are currently in.
        $content['module_id'] = $moduleID;


        // used in dropdowns
        $content['units'] = $this->config->item('units');
        $content['productTypes'] = $this->config->item("productTypes");
        $content['upchargeGroups'] = $this->upchargeGroup_model->get_as_codeigniter_dropdown($this->business_id);
        $content['productCategory_options'] = $this->productCategory_model->get_as_codeigniter_dropdown($business_id, $moduleID);
        $content['serviceTypes'] = $this->serviceType_model->get_as_codeigniter_dropdown($this->business_id);
        $content['processes'] = $this->process_model->get_as_codeigniter_dropdown();
        $content['taxGroups'] = $this->taxgroup_model->get_as_codeigniter_dropdown($this->business_id);
        
        $content['product_image_path'] = Product::$product_image_path;
        $this->load->model('image_model');
        $content['productImages'] = $this->image_model->get_aprax(array(
            'business_id' => $business_id,
        ), 'imageID desc');
        
        $this->renderAdmin('preferences', $content);
    }

    /**
     * Show the UI to edit the suppliers of a product_process
     *
     * @param int $product_processID
     */
    public function edit_suppliers($product_processID = null)
    {
        $content['productProcess'] = new Product_Process($product_processID);

        $sql = "SELECT companyName, supplierID, cost, `default`
                    FROM supplier
                    JOIN business on businessID = supplierBusiness_id
                    LEFT JOIN product_supplier ON (supplier_id = supplierID AND product_process_id = ?)
                    WHERE business_id = ?
                    AND enabled = 1
                    ORDER BY companyName";

        $content['suppliers'] = $this->db->query($sql, array($product_processID, $this->business_id))->result();

        $this->renderAdmin('edit_suplpier', $content);
    }

    /**
     * Show the UI to edit the translation of a product
     *
     * @param int $productID
     */
    public function edit_translation($productID = null)
    {
        $product = new Product($productID);

        $content['product'] = $product;
        $content['productNames'] = $product->getProductNames();

        $this->load->model('business_language_model');
        $content['languages'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $content['is_ajax'] = $this->input->is_ajax_request();

        $this->renderAdmin('edit_translation', $content);
    }

    /**
     * Updates the translations of a product
     *
     * @param int $productID
     */
    function update_translations($productID = null)
    {
        $product = new Product($productID);

        if (!empty($_POST['title'])) {
            $product->saveProductNames($_POST['title']);
        }

        if ($this->input->is_ajax_request()) {
            $this->outputJSONsuccess('Translation updated for product');
        }

        set_flash_message('success', 'Translation updated for product');

        redirect_with_fallback('/admin/modules/');
    }

    /**
     * Show the UI to edit the translation of a product category
     *
     * @param int $productCategoryID
     */
    public function edit_category_translation($productCategoryID = null)
    {
        $productCategory = new ProductCategory($productCategoryID);

        $content['productCategory'] = $productCategory;
        $content['productCategoryNames'] = $productCategory->getProductCategoryNames();

        $this->load->model('business_language_model');
        $content['languages'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $content['is_ajax'] = $this->input->is_ajax_request();

        $this->renderAdmin('edit_category_translation', $content);
    }

    /**
     * Updates the translations of a product category
     *
     * @param int $productCategoryID
     */
    function update_category_translations($productCategoryID = null)
    {
        $productCategory = new ProductCategory($productCategoryID);

        if (!empty($_POST['title'])) {
            $productCategory->saveProductCategoryNames($_POST['title']);
        }

        if ($this->input->is_ajax_request()) {
            $this->outputJSONsuccess('Translation updated for product category');
        }

        set_flash_message('success', 'Translation updated for product category');

        redirect_with_fallback('/admin/modules/');
    }

}