<?php

class Gift_Cards extends MY_Admin_Controller
{
    public $submenu = 'admin';

    public function index()
    {
        $content = array();

        $couponID = $this->input->get('couponID');
        $code = $this->input->get('code');

        if ($couponID) {
            //get the code
            $getCode = "select * from coupon where couponID = ?";
            $q = $this->db->query($getCode, array($couponID));
            $code = $q->result();
            $code = $code[0]->code;
        }

        $content['code'] = $code;

        if ($code) {

            if (strtolower(substr($code, 0, 2)) == 'gf') {
                $code = substr($code, 2);
            }

            $getCards = "select *
            from coupon
            where prefix = 'GF'
            and code like '%$code%'
            and business_id = $this->business_id";
            $q = $this->db->query($getCards);
            $content['cards'] = $cards = $q->result();

            //if it was redeemed, find out who redeemed it
            foreach ($cards as $card) {
                $getCust = "select * from customerDiscount
                join customer on customer_id = customerID
                where couponCode = 'GF$card->code'";
                $q = $this->db->query($getCust);
                $cust = $q->result();
                $content['customer'][$card->code] = $cust ? $cust[0] : null;
            }
        }

        $this->setPageTitle('Manage Gift Cards');
        $this->renderAdmin('index', $content);
    }

    public function create()
    {
        $content = array();
        if (!empty($_POST)) {

            if (!is_numeric($_POST['amount'])) {
                set_flash_message('error', "Amount must be numeric");
                redirect($this->uri->uri_string());
            }

            if (isset($_POST['lbs'])) {
                $options['amountType'] = 'pounds';
                $options['description'] = $_POST['amount'].'LB Gift Certificate';
            } else {
                $options['amountType'] = 'dollars';
                $options['description'] = '$'.$_POST['amount'].' Gift Certificate';
            }

            if (!empty($_POST['expiration'])) {
                $options['endDate'] = date('Y-m-d', strtotime($_POST['expiration']));
            }

            $options['frequency'] = 'until used up';
            if (!empty($_POST['frequency'])) {
                $options['frequency'] = $_POST['frequency'];
            }

            $options['firstTime'] = '0';
            if (!empty($_POST['firstTime'])) {
                $options['firstTime'] = $_POST['firstTime'];
            }

            $counter = 0;
            $content['cardNumber'] = '';
            for ($i = 1; $i <= $_POST['qty']; $i++) {
                $options['prefix'] = 'GF';
                $options['amount'] = $_POST['amount'];
                $options['business_id'] = $this->business_id;
                $options['extendedDesc'] = $_POST['description'];
                $options['code'] = rand(1000000000, 9999999999);
                $options['combine'] = $_POST['combine'];

                try {
                    $ret = $this->db->insert('coupon', $options);
                    $content['cardNumber'] .= 'Card Number GF'.$options['code'].' was created for $'.$_POST['amount'].'<br>';
                    $counter ++;
                } catch (Database_Exception $e) {
                    //We don't care if the coupon insertion fails, we ignore the insertion failure and continue on as normally.
                }
            }
            $content['cardNumber'] = $counter . " Gift Cards Created: <br><br>". $content['cardNumber'];
        }

        $this->template->add_js("/js/jquery.validate-1.9.js", "source");
        $this->renderAdmin('create', $content);
    }

    public function deactivate($couponID = null)
    {
        if ($couponID) {
            //get the code
            $sql = "update coupon
                set redeemed = 1,
                    extendedDesc = concat(extendedDesc, ' **MANUALLY DELETED')
                where couponID = ?
                and business_id = ?";

            $q = $this->db->query($sql, array($couponID, $this->business_id));
            set_flash_message('success', "Gift Card Deactivated");
        }
        redirect("/admin/gift_cards/?couponID=" . $couponID);
    }

    public function anyuse($couponID = null)
    {
        if ($couponID) {
            $updateCard = "update coupon set firstTime = 0 where couponID = ?";
            $q = $this->db->query($updateCard, array($couponID));
            set_flash_message('success', "Gift Card Updated");
        }
        redirect("/admin/gift_cards/?couponID=" . $couponID);
    }

}