<?php

class LockerLootConfigs extends MY_Admin_Controller
{
    /**
     * If the server is in Droplocker mode, updates the rewards program for a particular business.
     * Expects the following POST parameters
     *  percentPerOrder
     *  redeemPercent
     *  minRedeemAmount
     *  referralAmountPerReward
     *  referralNumberOfOrders
     *  referralOrderAmount
     *  programName
     *  terms
     */
    public function update_rewards_program()
    {
        $this->load->library("form_validation");

        $this->form_validation->set_rules('percentPerOrder', 'Percent Per Order', 'required|numeric');
        $this->form_validation->set_rules('redeemPercent',   'Redeem Percent', 'required|numeric');
        $this->form_validation->set_rules('minRedeemAmount', 'Min Redeem Amount', 'required|numeric');
        $this->form_validation->set_rules('referralAmountPerReward', 'Referral Amount Per Reward', 'required|numeric');
        $this->form_validation->set_rules('referralNumberOrders', 'Referral Number Orders', 'required|numeric');
        $this->form_validation->set_rules('referralOrderAmount', 'Referral Order Amount', 'required|numeric');
        $this->form_validation->set_rules('programName', 'Program Name', 'required');
        $this->form_validation->set_rules("terms", "Terms", "required");

        if ($this->form_validation->run()) {
            //The following conditional updates the locker loot for all businesses if the server is in Bizzie mode. Otherwise, it updates the locker loot for the business stored in the user's session.
            if (in_bizzie_mode()) {
                $businesses = $this->business_model->get(array("select" => "businessID"));
            } else {
                $businesses = $this->business_model->get(array("select" => "businessID", "businessID" => $this->business_id));
            }
            foreach ($businesses as $business) {
                $lockerLootConfigs = \App\Libraries\DroplockerObjects\LockerLootConfig::search_aprax(array("business_id" => $business->businessID));
                if (empty($lockerLootConfigs)) {
                    $lockerLootConfig = new \App\Libraries\DroplockerObjects\LockerLootConfig();
                } else {
                    $lockerLootConfig = $lockerLootConfigs[0];
                }

                if ($this->input->post("lockerLootStatus")) {
                    $coupons = \App\Libraries\DroplockerObjects\Coupon::search_aprax(array('business_id' => $business->businessID, 'prefix' => 'CR'));
                    if (empty($coupons)) {
                        // create a CR coupon
                        $coupon = new \App\Libraries\DroplockerObjects\Coupon();
                        $coupon->business_id = $business->businessID;
                        $coupon->prefix = "CR";
                        $coupon->amount = 25.00;
                        $coupon->amountType = 'percent';
                        $coupon->frequency = 'until used up';
                        $coupon->description = "Customer Referral";
                        $coupon->extendedDesc = "New Customer Referral - 25% off for new customers";
                        $coupon->code = '*';
                        $coupon->maxAmt = '25.00';
                        $coupon->combine = 0;
                        $coupon->firstTime = 1;
                        $coupon->save();
                    }
                    $lockerLootConfig->lockerLootStatus = 1;
                } else {
                    $lockerLootConfig->lockerLootStatus = 0;
                }
                if ($this->input->post("allowToRedeem")) {
                    $lockerLootConfig->allowToRedeem = 1;
                } else {
                    $lockerLootConfig->allowToRedeem = 0;
                }
                $lockerLootConfig->terms = $this->input->post("terms");
                $lockerLootConfig->percentPerOrder = $this->input->post("percentPerOrder");
                $lockerLootConfig->redeemPercent = $this->input->post("redeemPercent");
                $lockerLootConfig->minRedeemAmount = $this->input->post("minRedeemAmount");
                $lockerLootConfig->referralAmountPerReward = $this->input->post("referralAmountPerReward");
                $lockerLootConfig->referralNumberOrders = $this->input->post("referralNumberOrders");
                $lockerLootConfig->referralOrderAmount = $this->input->post("referralOrderAmount");
                $lockerLootConfig->programName = $this->input->post("programName");
                $lockerLootConfig->business_id = $business->businessID;
                $lockerLootConfig->save();

            }
            set_flash_message("success", "Updated Locker Loot Configuration");
        } else {
            set_flash_message("error", validation_errors());
        }
        //The following statement redirects to the Superadmin page if the server is in Bizzie mode, or goes to the regular admin page if the server is not in Bizzie mode.
        $this->goBack(in_bizzie_mode() ? "/admin/superadmin" : "/admin/admin");
    }

    /**
     * The following action is used in conjunction Jquery.Editlabe plugin for taking and outputting data in the correct format.
     * Required POST parameters:
     *  property: The name of the column that will be updated
     *  value: the value to set the product's property to
     * @throws \InvalidArgumentException
     */
    public function update()
    {
        try {
            if (!$this->input->get_post("property")) {
                throw new \InvalidArgumentException("'property' must be passed as a POST or GET parameter.");
            }
            if (!array_key_exists("value", $_POST) && !array_key_exists("value", $_GET)) {
                throw new \InvalidArgumentException("'value' must be passed as a POST or GET parameter.");
            }
            $value = trim($this->input->get_post("value"));
            $property = trim($this->input->get_post("property"));
            try {
                $lockerLootConfigs = \App\Libraries\DroplockerObjects\LockerLootConfig::search_aprax(array("business_id" => $this->business_id));
                if (empty($lockerLootConfigs)) {
                    if ($this->input->is_ajax_request()) {
                        echo "You must first set up locker loot";
                    } else {
                        set_flash_message("error", "You must first set up locker loot");

                        $this->goBack("/admin/admin");
                    }
                } else {
                    //The following conditional updates all the business' locker loot configuration if the server is in Bizzie mode. Otherwise, this conditional updates the locker loot configuration for the business stored in the user's session.
                    if (in_bizzie_mode()) {
                        $this->load->model("business_model");
                        $this->lockerLootConfig_model->options = array($property => $value);
                        $businesses = $this->lockerLootConfig_model->update();
                    } else {
                        $lockerLootConfig = $lockerLootConfig[0];
                        $lockerLootConfig->$property = $value;
                        $lockerLootConfig->save();
                        echo $value;
                    }
                }
            } catch (\App\Libraries\DroplockerObjects\Validation_Exception $e) {
                echo "Invalid Value";
            }

        } catch (\Exception $e) {
            send_exception_report($e);
            if ($this->input->is_ajax_request()) {
                echo "Sorry, an error occurred";
            } else {
                $this->goBack("/admin/admin");
            }
        }
    }

    public function fix_overloot()
    {
        //get locker loot config for each business
        $getLootConfig = "select * from lockerLootConfig";
        $result = $this->db->query($getLootConfig);
        $configs = $result->result();
        foreach ($configs as $config) {
            $maxAmt = $config->referralAmountPerReward * $config->referralNumberOrders;

            $getLoot = "SELECT lockerLoot.customer_id as lootCust, orders.customer_id as ordCust, sum(points) as total, count(lockerLootID), description from lockerLoot
                        join orders on orderID = order_id
                        where description like '$".$config->referralAmountPerReward."%'
                        and orders.business_id = ".$config->business_id."
                        group by lockerLoot.customer_id, orders.customer_id";
            $result = $this->db->query($getLoot);
            $loots = $result->result();
            foreach ($loots as $loot) {
                if ($loot->total > $maxAmt) {
                    echo 'Loot customer: '.$loot->lootCust.' Business: '.$config->business_id.'<br>';
                    $lootToDelete = "SELECT lockerLoot.*
                                    FROM lockerLoot
                                    left join orders on order_id = orderID
                                    WHERE lockerLoot.customer_id = $loot->lootCust
                                    and orders.customer_id = $loot->ordCust
                                    and points = ".$config->referralAmountPerReward."
                                    order by updated asc
                                    limit ".$config->referralNumberOrders.",100";
                    $result = $this->db->query($lootToDelete);
                    $deletes = $result->result();
                    foreach ($deletes as $delete) {
                        $sql = "update lockerLoot
                                set points = 0, description = 'Referral Bonus - deleted'
                                 where lockerLootID = $delete->lockerLootID";
                        echo $sql.': '.$delete->customer_id.'<br>';
                        $result = $this->db->query($sql);
                    }
                }
            }
        }



    }


}
