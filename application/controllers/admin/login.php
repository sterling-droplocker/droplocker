<?php
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\Business;
class Login extends CI_Controller
{
    public function index()
    {
        $content = array();
        $this->session->keep_flashdata("login_redirect_url");
        if (isset($_POST['submit'])) {
            $this->load->model('employee_model');
            $this->load->model('employee_log_model');

            $email_or_username = $this->input->post("email_or_username");
            $password = md5($this->input->post("password"));

            if (!empty($email_or_username) && $this->input->post("password")) {
                $query = "SELECT * FROM employee
                    WHERE (email = ? OR username = ?) AND password = ? AND empStatus IS TRUE";
                $get_user_query = $this->db->query($query, array($email_or_username, $email_or_username, $password));

                $users = $get_user_query->result();
            }

            if ($users) {
                $user = $users[0];
                //if a user is found, we need to get all the business information and roles for each business
                $business_employees = Business_Employee::search_aprax(array("employee_id" => $user->employeeID, "default" => 1));
                if (empty($business_employees)) {
                    set_flash_message("error", "No default business set for employee");
                    redirect("/admin/login");
                } else {
                    $business_employee = $business_employees[0];
                    $this->session->set_userdata('business_id', $business_employee->business_id);
                    $this->session->set_userdata('buser_id', $business_employee->{Business_Employee::$primary_key});
                    $business = new Business($business_employee->business_id);
                    $this->session->set_userdata("companyName", sprintf("%s - %s, %s", $business->companyName, $business->city, $business->state));
                }
                // Log successful login
                $this->employee_log_model->login($business_employee->business_id, $user->employeeID);

                $this->session->set_userdata('employee_id', $user->employeeID);
                $this->session->set_userdata('buser_fname', $user->firstName);
                $this->session->set_userdata('buser_lname', $user->lastName);
                $this->session->set_userdata('developer_mode', $user->developer_mode);
                //$this->session->set_userdata('timezone', $user->timezone);
                $this->session->set_userdata('super_admin', $user->superAdmin);

                if (!empty($user->allowed_businesses)) {
                    $this->session->set_userdata('allowed_businesses', json_decode($user->allowed_businesses));
                } else {
                    $this->session->set_userdata('allowed_businesses', array());
                }

                $this->session->set_userdata("timezone", $business->timezone);

                if ($this->session->flashdata("login_redirect_url")) {
                    $redirect = $this->session->flashdata("login_redirect_url");
                } else {
                    $redirect = "/admin/orders";
                }
                redirect($redirect);

            } else {
                set_flash_message('error','Wrong Username or Password');
                redirect('/admin/login');
            }
        }
        $this->load->view('/admin/login/index', $content);
    }

    /**
    * Verification for when employees need to update a password
    */
    public function verify_forgot_password($verify = null)
    {
        $this->load->model('employee_model');
        $employees = \App\Libraries\DroplockerObjects\Employee::search_aprax(array("verifyResetPassword" => $verify));

        if (empty($employees)) {
            set_flash_message("error", "Could not find employee account with verification code $verify");
            redirect("/admin/login");
        } else {
            if (isset($_POST['submit'])) {
                $this->load->library("form_validation");
                $this->form_validation->set_rules("password", "Password", "required");
                $this->form_validation->set_rules("password", "Password", "required");
                if ($this->form_validation->run()) {
                    $password = $this->input->post("password");
                    $password2 = $this->input->post("password2");
                    if ($password == $password2) {
                        $employee = $employees[0];
                        $employee->password = $password;
                        $employee->verifyResetPassword = "";
                        $employee->save();
                        set_flash_message("success", "Updated password");
                        redirect("/admin/login");
                    } else {
                        set_flash_message("error", "Passwords do not match");
                        redirect($this->agent->referrer());
                    }
                } else {
                    set_flash_message("error", validation_errors());
                    redirect($this->agent->referrer());
                }
            } else {
                $this->load->view("/admin/login/verify_forgot_password", null);
            }
        }
    }

    /**
     * THis function should be no longer used
     */
    public function send_forgot_password_email()
    {
        $email = $this->input->post("email");
        $this->load->model('employee_model');
        $employees = \App\Libraries\DroplockerObjects\Employee::search_aprax(array("email" => $email));
        if (empty($employees)) {
            set_flash_message('error', "Email not found");
            redirect_with_fallback("/admin/login/display_forgot_password_form");
        } else {
            $employee = $employees[0];
            $this->load->model("business_employee_model");
            $business_employee = $this->business_employee_model->get_one(array("employee_id" => $employee->employeeID));
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($business_employee->business_id);
            $verify = md5(rand(6,1000).date('Y-m-d H:i:s'));

            $body = "You requested a password update from {$business->companyName}. Please follow this link to create a new one:<br>";
            $body .= "<a href='http://{$_SERVER['HTTP_HOST']}/admin/login/verify_forgot_password/$verify'>http://{$_SERVER['HTTP_HOST']}/admin/login/verify_forgot_password/$verify</a>";
            $body .= "<p>If you did not request this update, please contact us at {$business->email}.</p>";
            $body .= "<br><br>Sincerely,<br> The {$business->companyname} team.";
            $subject = 'Reset password for Droplocker';

            $email_sent = send_email(SYSTEMEMAIL, SYSTEMFROMNAME, $email, $subject, $body );

            if ($email_sent) {
                $options = array();
                $options['verifyResetPassword'] = $verify;
                $where['employeeID'] = $employee->employeeID;
                $employee->verifyResetPassword = $verify;
                $employee->save();
                $this->employee_model->update($options, $where);
                set_flash_message('success', "Sent verification email to $email");
            } else {
                set_flash_message('error', 'Error sending email.');
            }
            redirect('/admin/login/display_forgot_password_form');
        }
    }
    /**
    * UI for employees to retrieve their passwords
    */
    public function display_forgot_password_form()
    {
        $this->load->view('admin/login/display_forgot_password_form', null);
    }
}
