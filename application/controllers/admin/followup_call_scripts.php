<?php

class Followup_Call_Scripts extends MY_Admin_Controller
{
    public function update()
    {
        $this->load->model("followupcallscript_model");

        try {
            if (!$this->input->post("followupCallType_id")) {
                throw new Exception("'followupCallType_id' must be passed as a POST parameter.");
            }
            $followupCallType_id = $this->input->post("followupCallType_id");
            if (!$this->input->post("text")) {
                throw new Exception("'text' must be passed as a POST parameter.");
            }
            $text = $this->input->post("text");
            $this->followupcallscript_model->update($text, $followupCallType_id, $this->business_id);
            set_flash_message("success", "Updated script");
        } catch (Exception $e) {
            set_flash_message("error", "Could not update script.");
            send_exception_report($e);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
}
