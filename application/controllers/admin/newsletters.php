<?php

use App\Libraries\DroplockerObjects\Newsletter;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Customer;

class Newsletters extends MY_Admin_Controller
{

    public $submenu = 'admin';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $content['newsletters'] = Newsletter::search_aprax(array(
            'business_id' => $this->business_id
        ));

        $this->renderAdmin('index', $content);
    }

    public function add()
    {
        if (isset($_POST['submit'])) {
            $newsletter = new Newsletter();
            $newsletter->name = $_POST['name'];
            $newsletter->subject = $_POST['subject'];
            $newsletter->body = $_POST['body'];
            $newsletter->business_id = $this->business_id;
            $newsletter->save();

            if ($newsletter->newsletterID) {
                set_flash_message('success', "Newsletter has been added");
            } else {
                set_flash_message('error', "Newsletter has not been added");
            }

            return redirect("/admin/newsletters");
        }

        $content['newsletters'] = Newsletter::search_aprax(array('business_id'=>$this->business_id));

        $this->renderAdmin('add', $content);
    }


    public function edit($id = null)
    {
        $newsletter = $this->getNesletter($id);

        if (isset($_POST['submit'])) {
            $newsletter->name = $_POST['name'];
            $newsletter->subject = $_POST['subject'];
            $newsletter->body = $_POST['body'];
            $newsletter->save();

            set_flash_message('success', 'Newsletter saved');
            redirect($this->uri->uri_string());
        }

        $employeeID = get_employee_id($this->buser_id);
        $employee = new Employee($employeeID);

        $content['employee'] = $employee;
        $content['newsletter'] = $newsletter;

        $this->renderAdmin('edit', $content);
    }

    public function test($id = null)
    {
        $customer_id = '';
        $newsletter = $this->getNesletter($id);

        $employeeID = get_employee_id($this->buser_id);
        $employee = new Employee($employeeID);

        if ($employee->customer_id!=0) {
            $customer = new Customer($employee->customer_id);
            $customer_id = $customer->customerID;
        } else {
            $sql = 'SELECT customerID FROM customer WHERE inactive=0 AND business_id = ? order by RAND() limit 1';
            $customer_id = $this->db->query($sql, array($this->business_id))->row()->customerID;
        }

        $options['customer_id'] = $customer_id;
        $options['business_id'] = $this->business_id;
        $data = get_macros($options);
        $subject = email_replace($newsletter->subject, $data);
        $body = email_replace($newsletter->body, $data);
        queueEmail(get_business_meta($this->business_id, 'customerSupport'), get_business_meta($this->business_id, 'from_email_name'), $employee->email, $subject, $body);
        set_flash_message('success', 'Email Sent');

        redirect('/admin/newsletters/edit/'.$id);
    }


    public function delete($id = null)
    {
        $newsletter = $this->getNesletter($id);
        $newsletter->delete();
        set_flash_message('success', "Newsletter has been deleted");

        redirect("/admin/newsletters");
    }


    public function view($id = null)
    {
        $newsletter = $this->getNesletter($id);

        $employeeID = get_employee_id($this->buser_id);
        $employee = new Employee($employeeID);

        $content['employee'] = $employee;
        $content['newsletter'] = $newsletter;

        $this->renderAdmin('view', $content);
    }


    /**
     * sets a newsletter to active also fills the queue with customers to send the email to
     */
    public function activate($id = null)
    {

        $newsletter = $this->getNesletter($id);

        $getFilters = "select * from filter where business_id = ?";
        $query = $this->db->query($getFilters, array($this->business_id));
        $content['filters'] = $query->result();

        if (isset($_POST['activate'])) {
            // fill the queue with users
            $result = $newsletter->fillQueue($_POST['customerType'], $this->business_id, $id);
            if ($result['insert_id']) {
                $newsletter->startDate = convert_to_gmt_aprax($_POST['startDate'], $this->business->timezone);
                $newsletter->noSendDate = convert_to_gmt_aprax($_POST['endDate'], $this->business->timezone);
                $newsletter->status = 'active';
                $newsletter->save();
                set_flash_message('success', "{$result['totalRows']} customers have been queued up to receive this newsletter");
            } else {
                set_flash_message('error', "Problem with adding customer to the newsletter queue");
            }

            redirect("/admin/newsletters");
        }

        $content['newsletter'] = $newsletter;
        $this->renderAdmin('activate', $content);
    }

    /**
     * deletes all the customers in a newsletter queue. This does not effect any customer that has already received the newsletter
     * There is no view, this redirects back to newsletters
     */
    function delete_queue($id = null)
    {
        $newletter = $this->getNesletter($id);

        $sql = "DELETE FROM newsletterQueue where newsletter_id = ?";
        $this->db->query($sql, array($id));
        if ($this->db->affected_rows() > 0) {
            set_flash_message('success', "The newsletter queue has been deleted");
        } else {
            set_flash_message('error', "The newsletter queue has NOT been deleted");
        }

        redirect("/admin/newsletters");
    }


    /**
     * Gets a newsletter and validates the business
     * @param int $id
     * @return \App\Libraries\DroplockerObjects\Newsletter
     */
    protected function getNesletter($id)
    {
        // silently ignore the exception
        if (empty($id) || !is_numeric($id)) {
            set_flash_message('error', 'Missing newsletter ID');
            redirect("/admin/newsletters");
        }

        $newsletter = new Newsletter($id);
        if ($newsletter->business_id != $this->business_id) {
            set_flash_message('error', 'Newsletter does not belong to this business');
            redirect("/admin/newsletters");
        }

        return $newsletter;
    }

}
