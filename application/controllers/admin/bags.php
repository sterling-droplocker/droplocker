<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Bag;

class Bags extends MY_Admin_Controller
{

    public $submenu = 'customers';

    /**
     * @throws \RunTimeException
     */
    public function delete_bag()
    {
        $bag_id = $this->input->get('bagID');
        $this->load->model('bag_model');
        $result = $this->bag_model->deleteBag($bag_id, $this->business_id);

        if ($result['status']=='deleted') {
            set_flash_message("success", "Bag was deleted from system.");
        } else {
            set_flash_message("success", "Bag was reassigned to the blank customer.");
        }
        redirect($this->agent->referrer());
    }
    /**
     * The following action updates a bag's notes
     * Expects the following POST parameters:
     *  bagID : The bag ID
     * Can take the following optional POST parameters:
     *  notes : The bag notes
     */
    public function update()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("bagID", "Bag ID", "required");


        if ($this->form_validation->run()) {
            $bagID = $this->input->post("bagID");
            $bag = new Bag($bagID);

            if ($this->input->post("notes")) {
                $bag->notes = $this->input->post("notes");
            }
            $bag->save();
            set_flash_message("success" ,"Updated bag number {$bag->bagNumber}");

        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/bags/edit/{$bag->bagID}");
    }

    /**
     * The following action renders the interface for editing a paritcular bag
     */
    public function edit($bagID = null)
    {
        try {
            $bag = new Bag($bagID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Bag ID $bagID Not Found");

            $this->goBack("/admin/customers");
        }
        if ($bag->business_id != $this->business->{Business::$primary_key}) {
            set_flash_message("error", "This bag does not belong to your business");

            $this->goBack("/admin/customers");
        }
        $content = array();
        $content['bag'] = $bag;

        $this->template->write('page_title', 'Edit Bag');

        $this->data['content'] = $this->load->view('admin/bags/edit', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    //validation callback for adding a bag to customer
    //checks that the bag belons to the current business and customer
    public function customer_bag_check()
    {
        $bag_number = $this->input->post("bagNumber");
        $customer_id = $this->input->post("customer_id");

       // does the bag exist?
        $bag = Bag::search_aprax( array('bagNumber' => $bag_number, 'business_id' => $this->business_id));

        if ( !empty( $bag ) ) {
            if ( sizeof( $bag ) > 1 ) {
                $this->form_validation->set_message('customer_bag_check', 'Found duplicate bags');

                return FALSE;
            }

            $bagObj = $bag[0];
            $business = new Business( $this->business_id );
            $blankCustomer = $business->blank_customer_id;

            if ($bagObj->business_id != $this->business_id) {
                $this->form_validation->set_message('customer_bag_check', 'Bag does not belong to the current business_id [' . $this->business_id . ']');

                return FALSE;
            }

            if ($bagObj->customer_id != $blankCustomer) {
                $this->form_validation->set_message('customer_bag_check', "Bag is not available, already belongs to another customer");

                return FALSE;
            }
        }

        return TRUE;

    }

    public function add_bag_to_customer()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('bagNumber', 'Bag Number', 'required|callback_customer_bag_check');
        $this->form_validation->set_rules('customer_id', 'Customer ID', 'required');

        if ($this->form_validation->run()) {
            $bagNumber = $this->input->post("bagNumber");
            $customer_id = $this->input->post("customer_id");

            //strip leading 0's
            $bagNumber = trim($bagNumber);
            $bagNumber = ltrim($bagNumber, "0");

            if ($this->input->post("notes")) {
                $notes = $this->input->post("notes");
            } else {
                $notes = null;
            }
            $this->load->model('bag_model');
            try {
                $this->bag_model->addBagToCustomer($customer_id, $bagNumber, $this->business_id, $notes);
            } catch (Exception $e) {
                set_flash_message('error', $e->getMessage());
                $this->goBack("/admin/customers/detail/".$customer_id);
            }

            if (empty($notes)) {
                set_flash_message('success', "Reassigned bag $bagNumber to customer");
            } else {
                set_flash_message('success', "Reassigned bag $bagNumber to customer with notes '$notes'");
            }

        } else {
            set_flash_message('error', validation_errors());
        }

        $this->goBack("/admin/customers/detail/".$customer_id);
    }
    
    public function getByNumber()
    {
        $bag = array();
        $bag_number = $this->input->post('bag_number');
        
        if($bag_number){
            $this->load->model('bag_model');
            $bag = $this->bag_model->get_aprax(array(
                'bagNumber' => $bag_number,
                'business_id' => $this->business_id,
                ));
        }
        
        header('Content-Type: application/json');
        echo json_encode($bag);
    }

}
