<?php

use App\Libraries\DroplockerObjects\Testimonial;

class Testimonials extends MY_Admin_Controller
{

    public $submenu = 'admin';

    /**
     * UI for listing all the testimonials
     */
    public function index()
    {
        $testimonial = new Testimonial();
        $content = array();
        $content['testimonials'] = $testimonial->listAll();
        $this->renderAdmin('index', $content);
    }


    /**
     * UI for adding a testimonial
     */
    public function add()
    {
        $content = $_POST;

        if (isset($_POST['submit'])) {
            $testimonial = new Testimonial();
            $testimonial->customerName = $_POST['customerName'];
            $testimonial->testimonial = $_POST['testimonial'];
            $testimonial->siteLink = $_POST['siteLink'];
            $testimonial->business_id = $this->business_id;
            $testimonial->save();
            if (!empty($testimonial->testimonialID)) {
                set_flash_message('success', 'Testimonial has been added');
                redirect('/admin/testimonials/');
            }
        }

        $this->renderAdmin('add');
    }

    /**
     * UI for updating testimonials
     */
    public function edit($id)
    {
        $testimonial = new Testimonial($id);
        if ($testimonial->business_id != $this->business_id) {
            set_flash_message('error', "Cannot edit testimonials of other businesses");
            redirect('admin/admin/testimonials');
        }

        $content['testimonial'] = $testimonial;

        if (isset($_POST['submit'])) {
            $testimonial->customerName = $_POST['customerName'];
            $testimonial->testimonial = $_POST['testimonial'];
            $testimonial->siteLink = $_POST['siteLink'];
            $testimonial->business_id = $this->business_id;
            $testimonial->save();

            if (!empty($testimonial->testimonialID)) {
                set_flash_message('success', 'Testimonial has been updated');
                redirect('/admin/testimonials/');
            }
        }

        $this->renderAdmin('edit', $content);
    }


    /**
     * Deletes a testimonial.
     *
     * No UI, this redirects no matter what
     */
    public function delete($id)
    {
        $testimonial = new Testimonial($id);
        if ($testimonial->business_id == $this->business_id) {
            $testimonial->delete();
        } else {
            set_flash_message('error', "Cannot edit testimonials of other businesses");
            redirect('admin/admin/testimonials');
        }

        set_flash_message('success', "Testimonial has been deleted");
        redirect('admin/testimonials');
    }

}
