<?php

/**
 * #########################################
 * BEGIN
 * #########################################
 *
 * segments
 * 4 = barcode or text to print
 * 5 = printer key
 * 6 = business_id
 *
 * #########################################
 * END
 * #########################################
 */

use App\Libraries\DroplockerObjects\DeliveryNotice;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Order;
class Printer extends CI_Controller
{
    public $business_id;
    private $auto;

    /**
     * The following constructor is unknown
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->auto = "";

        $business_id = $this->uri->segment(6);
        if ($business_id) {
            $this->business_id = $business_id;
        } else {
            $this->business_id = $this->session->userdata('business_id');
        }


        if (!$this->business_id) {
            set_flash_message('error', 'Missing business ID');
            redirect('/admin/login');
        }

        //get business settings
        $this->load->model('business_model');
        $this->business = $this->business_model->get_by_primary_key($this->business_id);
        $this->settings = $this->business;

        $printer_key = $this->uri->segment(5);
        if ($printer_key && $this->business_id) {
            $key = get_business_meta($this->business_id , 'printer_key');
            if ($printer_key != $key) {
                $message = "The printer key '$printer_key' in the Settings of this application does not match the printer key '$key' in your DropLocker admin panel";
                die($message);
            }
        }

    }

    /**
     * THe following function is unkown  .
     * @throws Exception
     */
    public function armPrinter($arm = null)
    {
        $armID = str_replace("ARM",'', $arm);

        //look at automat table and get the order_id and then print the order receipt
        $this->load->model('automat_model');
        $this->load->model('assembly_model');

        // print orders at least 10 hours in the past
        $dateLimit = gmdate('Y-m-d H:i:s', strtotime('-600 minutes'));

        //gets orders that has not been printed yet
        $getOrders = "SELECT order_id, garcode, outPos, ngarm, garment
                    FROM automat
                    INNER JOIN orders ON orderID = order_id AND orders.business_id = ?
                    WHERE arm = ?
                        AND printed != 1
                        AND stat != 'U'
                        AND fileDate > ?
                    GROUP BY order_id, outPos, arm
                    ORDER BY fileDate";
        $query = $this->db->query($getOrders, array($this->business_id, $armID, $dateLimit));

        $orders = $query->result();

        $grouping = get_business_meta($this->business_id, 'metalproGrouping');
        $loadBags = get_business_meta($this->business_id, 'loadBags');

        if (!$orders) {
            echo "<h2>Nothing on Arm{$armID}</h2>";
            return;
        }

        $counter = 0;
        foreach ($orders as $row) {
                //now see if this is the last item

                //need to first figure out how many items should be on this offload
                $rem = $row->ngarm;

                if ($grouping) {
                    $rem = $rem % $grouping;
                    $batches = floor($row->ngarm / $grouping);
                    $groups = ceil($row->ngarm / $grouping);
                }

                //get a matrix of what the offloads should look like
                $matrix = array();
                for ($i = 1; $i <= $batches; $i++) {
                    $matrix['#'.str_pad($i,3,'0',STR_PAD_LEFT).'#'] = $grouping;
                }
                $matrix['#'.str_pad($i,3,'0',STR_PAD_LEFT).'#'] = $rem;

                //get all the barcodes in this batch
                $getBatchItems = "SELECT * FROM automat
                            WHERE order_id = {$row->order_id}
                            AND arm = {$armID}
                            AND outPos = '{$row->outPos}'
                            AND stat != 'U'
                            AND printed = 0
                            ";
                $query = $this->db->query($getBatchItems);
                $batchItems = $query->result();

                //find out how many items are not printed of this batch
                $itemCount = count($batchItems);

                //if this should trigger a print
                if ($matrix[$row->outPos] == $itemCount) {
                    $printedItems = '';
                    foreach ($batchItems as $batchItem) {
                        //update each items to printed
                        $updateItem = "UPDATE automat SET printed = 1 WHERE automatID = ?";
                        $this->db->query($updateItem, array($batchItem->automatID));
                        $printedItems .= $batchItem->garcode.',';
                    }
                    $printedItems = substr($printedItems, 0, -1);
                    $autoNotes = $printedItems.'<br><strong><font size="3">Bundle '.ltrim(substr($row->outPos, 1,3), '0').' of '. $groups.': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$itemCount.' items</font></strong>';

                    //print the receipt
                    $this->auto = "auto";
                    $this->_print_barcode_receipt($row->garcode, 0, $autoNotes);

                    //we don't want to keep going, so stop and this will run again in a couple seconds
                    $counter++;
                    break;
                }
                //update any rows that don't have a barcode
                if (empty($row->garcode)) {
                        $sql = "UPDATE automat SET printed = 1 WHERE order_id = ? AND (arm = ? OR arm = '' OR arm IS NULL)";
                        $this->db->query($sql, array($row->order_id, $armID));
                        //throw new Exception("Missing garcode<br>SQL:<BR>".$sql);
                }
        }

        if (!$counter) {
            echo "<h2>Nothing on Arm{$armID}</h2>";
        }

    }


    /**
     * #####################################################
     * BEGIN *
     * #####################################################
     *
     * print_receipts calls private function print_barcode_receipt()
     * @return true
     *
     * #####################################################
     * END *
     * #####################################################
     */
    public function print_receipts($code = null)
    {
        $number_to_print = $this->input->get("number_to_print");
        if ( (int) $number_to_print >= 99 ) {
            $number_to_print = 99;
        }

        $include_date = $this->input->get('include_date');

        //if the size is 8, we have a temp tag, strip the leading 0
        if (strlen($code)==8 && substr($code, 0, 2)=='01') {
            $code = substr($code, 1, strlen($code));
        }
        $short = $this->uri->segment(7); //Short receipt or long receipt

        if (strpos($code, 'ARM')!==FALSE) {
            $this->armPrinter($code);

            return;
        }
        /**
            * print a notice
            */
        if (strpos($code,"notice")!==FALSE) {
            $this->print_notice($code);

            return;
        }

        $this->_print_barcode_receipt($code, $short, "", $number_to_print, $include_date);

        return true;
    }


    /**
    * Prints the receipt for a particular order.
    *
    * When generating a partial receipt, you must pass the following parameters as a POST rueqest
    *  @param partial int  The nubmer of items witheld
    *  @param item string Any user-specified details about why the order was partialed
    *  @param reason string A user-specified reason why the order was partialed.
    * Note that if the 'partial' parameter is passed, this function will also change the order status to 'Partially Delivered'
    */
    public function print_order_receipt($order_id = null)
    {
        $business_id = $this->business_id;

        $orderObj = new \App\Libraries\DroplockerObjects\Order($order_id);
        if ($orderObj->locker_id == 0) {
            echo "<h2>No Receipt Reason:</h2>";
            echo $orderObj->notes;

            return false;
        }

        $request['order'] = $order_id;
        $request['routes'] = '';
        $request['barcode'] = '';
        $request['showYesterday'] = '';
        $request['short'] = 0;
        $request['dc'] = '';
        $request['orderDate'] = '';

        //These parametrs are for generating a partial receipt.
        $request['partial'] = ($this->input->post("partial"))?$this->input->post("partial"):'';
        $request['item'] = $this->input->post("item");
        $request['reason'] = $this->input->post("reason");

        $data['request'] = $request;

        //If the partial parameter is passed, update the order status.
        $this->load->model('orderstatus_model');
        $this->load->model('order_model');
        if ($this->input->post("partial")) {
            $order = $this->order_model->get(array("orderID" => $order_id, "business_id" => $business_id));
            //Note, order status option 12 is expected to be "Out for partial delivery".
            $this->orderstatus_model->update_orderStatus(array(
                "order_id" => $order_id,
                "orderStatusOption_id" => 12,
                "locker_id" => $order[0]->locker_id,
                "employee_id" => get_current_employee_id(),
                "note" => $this->input->post("reason")
            ));

        }

        //this is only used by boxture service
        $service = get_business_meta($this->business_id, 'home_delivery_service');
        if (!empty($service)) {
            $homeDelivery = \App\Libraries\DroplockerObjects\OrderHomeDelivery::search(array(
                'order_id' => $order_id,
                'service'  => $service,
            ));
            if (!empty($homeDelivery->delivery_response)) {
                $object = json_decode($homeDelivery->delivery_response);
                if (!empty($object->shipment->lines)) {
                    $data['shipment'] = $object->shipment->lines;
                }
            }
        }


        $this->load->library('printerlibrary');
        $data['order'] = new Order($order_id);
        echo $this->printerlibrary->print_receipts($data);
    }

    /**
     * #####################################################
     * BEGIN
     * #####################################################
     *
        * The business login is in the view of this controller because it was copied over from the old site
        *
        * @todo update this function so the business logic is not in the view
        *
        * @param string $code
        * @param string $short
     *
     * #####################################################
     * END
     * #####################################################
        */
    public function _print_barcode_receipt($code, $short, $autoNotes = '', $number_to_print = 24, $include_date = false)
    {
        $request['order'] = '';
        $request['routes'] = '';
        $request['barcode'] = $code;
        $request['showYesterday'] = '';
        $request['short'] = $short;
        $request['dc'] = '';
        $request['orderDate'] = '';
        $request['auto'] = $this->auto;
        $request['autoNotes'] = $autoNotes;

        $data['request'] = $request;
        $data['number_to_print'] = $number_to_print;
        $data['include_date'] = $include_date;
        $this->load->view('admin/print_receipts', $data);

    }





    /**
     * Note, the following function is undocumented, unknown *.
     *
     * The following comments below are unknown * babble.
     * ------------
     * The business login is in the view of this controller because it was copied over from the old site
     *
     * @todo update this function so the business logic is not in the view
     *
     */
    public function print_route_receipts()
    {
        $request['order'] = '';
        $request['routes'] = $_POST['routes'];
        $request['barcode'] = '';
        $request['showYesterday'] = '';
        $request['short'] = 2;
        $request['dc'] = '1';
        $request['orderDate'] = $_POST['created'];
        $request['show_price'] = !empty($_POST['show_price']);
        $request['dateBegin'] = $_POST['date_begin'];
        $request['dateEnd'] = $_POST['date_end'];
        
        if (!empty($_POST['ignore_loaded'])) {
            $request['ignore_loaded'] = $_POST['ignore_loaded'];
        } else {
            $request['ignore_loaded'] = 0;
        }


        $data['request'] = $request;
        //$this->load->view('admin/print_receipts', $data);

        $this->load->library('printerlibrary');
        echo $this->printerlibrary->print_receipts($data);
    }

    /**
     * THe following function is unknown Scumbag Mat Dunwhopper Wharrgarble.
     */
    public function print_all_notices()
    {
        $deliveryNotices = DeliveryNotice::search_aprax(array('business_id'=>$this->business_id));

        $content['deliveryNotices'] = $deliveryNotices;
        $this->load->view('print/print_notices', $content);
    }

    /**
     * ################################################
     * BEGIN
     * ################################################
     *
    * The business login is in the view of this controller because it was copied over from the old site
    *
    * @todo update this function so the business logic is not in the view
    *
     * ################################################
     * END
     * ################################################
    */
    public function print_collar_stays($barcode = null)
    {
        $request['barcode'] = $barcode;
        $data['request'] = $request;
        $this->load->view('admin/print_collar_stays', $data);
    }
    /**
     * THe following function is unknown
     *
     * @param type $notice
     */
    public function print_notice($notice)
    {
        $deliveryNotice = DeliveryNotice::search_aprax(array('barcodeText'=>$notice), FALSE);
        $CI = get_instance();
        $this->load->model('image_placement_model');
        for ($i=0; $i<10; $i++) {
            echo '<div style="page-break-after:always; font-size: 10pt; font-family: Verdana;  WIDTH: 250px; margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; border-width: 0;">';
            $receipt_image = $CI->image_placement_model->get_by_name("receipt", $this->business_id);
            echo '<img src="/images/logos/'.$receipt_image->filename.'" />';
            echo $deliveryNotice[0]->noticeText;
            echo '</div>';
        }
    }

    public function print_day_tags($oderID)
    {
        $content = array();

        $this->load->model('order_model');
        $order = $this->order_model->get_by_primary_key($oderID);

        $this->load->model('customer_model');
        $customer = $this->customer_model->get_by_primary_key($order->customer_id);

        $content['orderID'] = $order->orderID;
        $content['customerFullName'] = $customer ? getPersonName($customer) : '--No Customer--';

        $orderItems = $this->db->query("SELECT SUM(qty) AS total
                        FROM orderItem
                        WHERE order_id = ?", array($order->orderID))->row();

        $content['start'] = 1;
        $content['end'] = $orderItems->total;

        /*
        $number = ($number_to_print)?$number_to_print:24;

        $content['business'] = $business;
        $content['lastBag'] = $lastBag = Bag::get_max_bagNumber() +1;
        $content['endBag'] = $lastBag + $number -1;
        Bag::set_max_bagNumber($content['endBag']);
        */

        $this->load->view('print/day_tags', $content);
        return;
    }

}
