<?php

use App\Libraries\DroplockerObjects\AcceptedPossibleProblemReport;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\OrderItem;
use App\Libraries\DroplockerObjects\OrderCharge;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Commission_Orders_Setting;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Transaction;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\CreditCard;

/**
 * location: admin/reports.php
 */

class Reports extends MY_Admin_Controller
{
    public $buser_id;
    public $business_id;
    public $business;
    private $top_customer_report_columns; //THis variable is used to store the columns that are rendered by dataTables for the top customers report.

    /**
     *
     * @var CI_DB_active_record
     */
    private $reporting_database; //This variable stores a reference to the reporting database CD Database instance
    public function __construct()
    {
        parent::__construct();

        setlocale(LC_ALL, $this->business->locale);

        $this->top_customer_report_columns = array(
            array("displayName" => "Customer ID", "queryName" => "customer_id"),
            array("displayName" => "First Name", "queryName" => "firstName"),
            array("displayName" => "Last Name", "queryName" => "lastName"),
            array("displayName" => "Signup Date", "queryName" => "CONVERT_TZ(signupDate, 'GMT', '{$this->business->timezone}')"),
            array("displayName" => "Address 1", "queryName" => "address1"),
            array("displayName" => "Number Of Orders", "queryName" => "COUNT(DISTINCT orders.orderID) AS number_of_orders", "dataTable_options" => array("bSearchable" => false)),
            array("displayName" => "Order Amount", "queryName" => "sum(orderItem.qty * orderItem.unitPrice) AS order_amount", "dataTable_options" => array("bSearchable" => false)),
            array("displayName" => "Last Order", "queryName" => "MAX(CONVERT_TZ(orders.statusDate, 'GMT', '{$this->business->timezone}')) AS lastOrder", "dataTable_options" => array("bSearchable" => false)),
            array("displayName" => "Internal Notes", "queryName" => "customer.internalNotes"),
            array("displayName" => "Phone", "queryName" => "phone"),
            array("displayName" => "Email", "queryName" => "email")
        );

        $this->reporting_database = $this->load->database("reporting", true);
        if (DEV) {
            $this->reporting_database = $this->db;
        }
    }

    public function double_charges()
    {
        $days = (isset($_POST['days']))?$_POST['days']:5;

        //get double charges
        $transaction = new Transaction;
        $content['transactions'] = $transaction->getDoubleCharges($this->business_id, $days);
        $content['days'] = $days;

        $this->renderAdmin('double_charges', $content);
    }


    public function display_sales_commission_report_form()
    {
        $this->db->select("employeeID, firstName, lastName");
        $this->db->join("business_employee", "employee_id=employeeID");
        $this->db->where("commission !=", "CAST(0 AS CHAR)", false);
        $this->db->where("business_id", $this->business_id);
        $get_salespersons_query = $this->db->get("employee");
        if ($this->db->_error_message()) {
            throw new \Database_Exception($this->db->_error_message, $this->db->last_query(), $this->db->_error_number());
        }
        $salespersons_result = $get_salespersons_query->result();
        foreach ($salespersons_result as $salesperson) {
            $content['salespersons'][$salesperson->employeeID] = getPersonName($salesperson);
        }
        $this->data['content'] = $this->load->view('admin/reports/display_sales_commission_report_form', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    public function display_salesperson_commission_configuration()
    {
        if (!$this->input->get("employeeID")) {
            throw new \InvalidArgumentException("'employeeID' must be passed as a GET parameter.");
        }
        $employeeID = $this->input->get("employeeID");
        if (!is_numeric($employeeID)) {
            throw new \InvalidArgumentException("'employeeID' must be numeric.");
        }
        try {
            $content['salesperson'] = new Employee($employeeID, true);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Employee ID '{$salesperson->employeeID}' not found in database.");
            redirect($this->agent->referrer());
        }
        $this->data['content'] = $this->load->view('admin/reports/display_salesperson_commission_configuration', $content, true);
        $this->template->write('page_title', 'Salesperson Commission Configuration', true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    public function display_location_history_form()
    {
        $this->load->model('location_model');
        $locations = $this->location_model->get_aprax(array(
            'business_id' => $this->business_id,
        ));

        foreach ($locations as $l) {
            $content['locations'][$l->locationID] = $l->address . ($l->companyName ? " ({$l->companyName})" : '');
        }
        natsort($content['locations']);

        $this->renderAdmin('location_history_form', $content);
    }

    public function display_discounts_given_over_time_period_form()
    {
        $this->renderAdmin('display_discounts_given_over_time_period_form', array());
    }

    public function get_discounts_given_over_time_period()
    {
        if (!$this->input->post("start_date")) {
            set_flash_message('error', "'start_date' must be passed as a POST parameter.");

            return redirect("/admin/reports/display_discounts_given_over_time_period_form");
        }

        if (!$this->input->post("end_date")) {
            set_flash_message('error', "'end_date' must be passed as a POST parameter.");

            return redirect("/admin/reports/display_discounts_given_over_time_period_form");
        }

        $start_date = new DateTime($this->input->post("start_date"));
        $end_date = new DateTime($this->input->post("end_date"));


        $content['date_range'] = array();
        $current_date = clone $start_date;
        while ($current_date <= $end_date) {
            $content['date_range'][] = clone $current_date;
            $current_date->add(new DateInterval("P1D"));
        }

        $end_date->add(new DateInterval("P1D"));

        $this->db->distinct();
        $this->db->select("couponCode");
        $this->db->where("origCreateDate >= ", $start_date->format("Y-m-d"));
        $this->db->where("origCreateDate <= ", $end_date->format("Y-m-d"));
        $this->db->where("business_id", $this->business_id);

        $get_couponCodes_in_date_range_query = $this->db->get("customerDiscount");

        if ($this->db->_error_message()) {
            throw new \Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $couponCodes = $get_couponCodes_in_date_range_query->result();

        $content['coupons'] = array();
        foreach ($couponCodes as $couponCode) {
            $prefix = substr($couponCode->couponCode, 0, 2);
            $code = substr($couponCode->couponCode, 2);

            if (!empty($prefix) && !empty($code)) {
                $coupon = \App\Libraries\DroplockerObjects\Coupon::search(array(
                    "prefix" => $prefix,
                    "code" => $code,
                    "business_id" => $this->business_id
                ));

                if ($coupon) {
                    $content['coupons'][] = $coupon;
                }
            }
        }

        $this->db->select("date_format(CONVERT_TZ(origCreateDate, 'GMT', '{$this->business->timezone}'),'%m-%d-%y') AS origCreateDate_format, couponCode, count(couponCode) AS total", false);
        $this->db->group_by("origCreateDate_format");
        $this->db->group_by("couponCode");
        $this->db->where("origCreateDate >= ", $start_date->format("Y-m-d"));
        $this->db->where("origCreateDate <= ", $end_date->format("Y-m-d"));
        $this->db->where("business_id", $this->business_id);
        $this->db->order_by("origCreateDate");
        $get_discounts_by_dates_query = $this->db->get("customerDiscount");
        if ($this->db->_error_message()) {
            throw new \Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }

        $customer_discounts_by_dates = $get_discounts_by_dates_query->result();
        $content['dates'] = array();
        foreach ($customer_discounts_by_dates as $customer_discount_by_date) {
            $content['dates'][$customer_discount_by_date->origCreateDate_format][$customer_discount_by_date->couponCode] = $customer_discount_by_date->total;
        }

        $this->data['content'] = $this->load->view('admin/reports/discounts_given_over_time_period', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function location_history($locationID = null)
    {
        if ($this->input->post("locationID")) {
            redirect('/admin/reports/location_history/' . $this->input->post("locationID"));
            return ;
        }

        if (!$locationID) {
            throw new \InvalidArgumentException("'locationID' must be passed as a POST parameter or as the 4th segment.");
        }

        if (!is_numeric($locationID)) {
            throw new \InvalidArgumentException("'locationID' must be numeric.");
        }
        $location = new Location($locationID, true);
        $customers = array();
        foreach ($location->relationships['Locker'] as $locker) {
            $locker = new Locker($locker->lockerID, true);
            foreach ($locker->relationships['Order'] as $order) {
                if (!empty($order->customer_id)) {
                    $order = new Order($order->orderID, false);
                    $customers[$order->customer_id]['total_spent'] += $order->get_gross_total();
                    $customers[$order->customer_id]['total_discounts'] += $order->get_discount_total();
                }
            }
        }
        foreach ($customers as $customerID => $data) {
            try {
                $customer = new Customer($customerID, false);
            } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                unset($customers[$customerID]);
                continue;
            }

            $customers[$customerID]['default_location'] = null;
            try {
                $customers[$customerID]['default_location'] = new Location($customer->location_id, true);
            } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                // there's nothing to do with this exception.
            }

            $max_order = $customer->get_last_order();
            $customers[$customerID]['last_order_date'] = $max_order->dateCreated;
            $customers[$customerID]['address'] = $customer->address1;
            $customers[$customerID]['email'] = $customer->email;
            $customers[$customerID]['name'] = getPersonName($customer);
        }
        $content['headers'] = array("Name", "Address", "Total Spent", "Total Discounts", "Last Order Date");
        $content['location'] = $location;
        $content['customers'] = $customers;

        $this->data['content'] = $this->load->view('admin/reports/location_history', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function index()
    {
        //find out which widgets to show
        $getWidgets = "select * from dashboard
                        left join widget on widgetID = widget_id
                        where employee_id = $this->buser_id
                        and module = 'Reports'
                        order by location";
        $result = $this->db->query($getWidgets);
        $content['widgets'] = $result->result();

        $this->load->view('admin/blank', '', true);
        $this->data['content'] = $this->load->view('admin/index', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    public function get_lat_long()
    {
        $get_lat_long_query = $this->reporting_database->query("SELECT * FROM  location WHERE lat='' or lat = 0");
        $lat_longs = $get_lat_long_query->result();
        $counter = 0;
        foreach ($lat_longs as $lat_long) {
            $coordinates = geocode($lat_long->street, $lat_long->city, $lat_long->state);
            $this->reporting_database->query("UPDATE location SET lat={$coordinates["lat"]}, lon = {$coordinates["lng"]}
            WHERE locationID = {$lat_long->locationID}");
            ++$counter;
        }
        set_flash_message("success", "$counter Locations Updated");

        redirect("/admin/reports/driver_map_old");
    }

    /**
     *Renders the view for assigning drivers
     */
    public function assign_drivers()
    {
        $sql_get_routes_with_drivers = $this->reporting_database->query("select distinct(location.route_id) as route_id, employee_id, llphone, phoneHome
                                            from location
                                            left join empRoutes on empRoutes.route_id = location.route_id
                                            LEFT JOIN employee ON employee.employeeID = empRoutes.employee_id
                                            where location.business_id = {$this->business_id}
                                            order by location.route_id");
        $routes_and_drivers = $sql_get_routes_with_drivers->result();


        $get_all_drivers_query = $this->reporting_database->query("SELECT employeeID, employee.firstName, employee.lastName
            FROM employee
            INNER JOIN customer ON customer.customerID = employee.customer_id
            WHERE customer.business_id={$this->business_id}
            AND empStatus = 1
            ORDER BY firstName
        ");
        $drivers_result = $get_all_drivers_query->result();

        $drivers = array();
        $drivers[0] = "";
        foreach ($drivers_result as $driver) {
            $drivers[$driver->employeeID] = getPersonName($driver);
        }
        $content['all_routes'] = $routes_and_drivers;
        $content['drivers'] = $drivers;

        $this->template->write('page_title', 'Assign Drivers', true);

        $this->renderAdmin('assign_drivers', $content);
    }

    /**
     * customers reports
     */
    public function customers()
    {
        $this->template->write('page_title', 'Customers', true);

        $this->data['content'] = $this->load->view('admin/reports/customers', null, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function update_driver_sort()
    {
        $sort_locations = $this->input->post('sort');
        $updated_locations = 0;
        if (!empty($sort_locations)) {
            foreach ($sort_locations as $sort_val => $location_id) {
                $this->db->where('locationID', $location_id);
                $this->db->where('business_id', $this->business_id);
                $this->db->update('location', array( 'sortOrder' => $sort_val ));
                $updated_locations += $this->db->affected_rows();
            }
        }
        echo json_encode(array('updated' => $updated_locations, 'locations' => $sort_locations ));
        exit;
    }

    public function update_location_route_sort()
    {
        $this->load->model('location_model');
        $location_id = $this->input->post('location_id');
        $sort_val = $this->input->post('sort');
        $route_val = $this->input->post('route');

        //update the sort/route
        $this->location_model->update(array( 'route_id' => $route_val, 'sortOrder' => $sort_val ), array( 'locationID' => $location_id, 'business_id' => $this->business_id ));

        echo json_encode(array( 'location_id' => $location_id, 'sort' => $sort_val, 'route' => $route_val ));
        exit;
    }

    public function routes_revert()
    {
        $update1 = "update location set route_id = masterRoute_id where location.business_id = $this->business_id";
        $update = $this->db->query($update1);
        $update1 = "update location set sortOrder = masterSortOrder where location.business_id = $this->business_id";
        $update = $this->db->query($update1);
        send_email(SYSTEMEMAIL, SYSTEMFROMNAME, "alevy@droplocker.com", 'Routes reverted to master routes', 'routes reverted to master routes by '.$this->employee->properties['firstName']. ' '.$this->employee->properties['lastName']);
        set_flash_message("Success", "Route stops reverted");
        redirect("admin/reports/driver_map_v2");
    }

    public function driver_map_v2()
    {
        $this->load->model('location_model');
        $content['allRoutes'] = $this->location_model->get_routes($this->business_id);
        $content['business'] = $this->business->companyName;

        $loaded_only = $this->input->post('loaded');

        //if routes are selected, use that.  Otherwise use the routes assigned to this driver for the day
        if ($this->input->get_post('route')) {
            $selectedRoutes = implode(",", $this->input->get_post('route'));
        } else {
            $getEmpRoutes = "select *
                            from empRoutes
                            join business_employee on business_employee.employee_id = empRoutes.employee_id
                            where business_employee.ID = $this->buser_id";
            $query = $this->reporting_database->query($getEmpRoutes);
            $empRoutes = $query->result();

            $selectedRoutes = '';
            foreach ($empRoutes as $route) {
                $selectedRoutes .= $route->route_id.',';
            }
            $selectedRoutes = substr($selectedRoutes, 0, -1);
        }
        $content['selectedRoutes'] = explode(",", $selectedRoutes);

        $content['allData'] = $allData = array('stops' => array());

        if ($selectedRoutes != '') {
            $this->load->library('legacy_route');

            //need to filter this based on pickups or deliveries
            $type = $this->input->get_post('tranType');
            $content['allData'] = $allData = $this->legacy_route->route_stops_2($selectedRoutes, $this->business_id, $this->input->get_post('dayOfWeek'), $type, $loaded_only);

            //get driver notes
            $getDriverNotes = "SELECT *, driverNotesID as notesId from driverNotes
                join location on locationID = driverNotes.location_id
                where active = 1
                and route_id in ($selectedRoutes)
                and location.business_id = $this->business_id
                order by created desc";
            $query = $this->reporting_database->query($getDriverNotes);
            $content['driverNotes'] = $driverNotes = $query->result();
            //find out what locations have driver notes
            foreach ($driverNotes as $driverNote) {
                $content['hasNote'][$driverNote->locationID] = 1;
            }
        }

        $orderTypeList = array();
        if ($this->input->get_post('route') != 'Pickups' && !empty($allData['deliveries'])) {
            foreach ($allData['deliveries'] as $delivery) {
                //find out what order types we have
                foreach ($delivery['orderTypes'] as $orderType => $value) {
                    $orderTypeList[$orderType] = substr($orderType, 0, 1);
                }
                //get order qty
                foreach ($delivery['orders'] as $order) {
                    $getQty = "select sum(qty) as qty from orderItem
                                join product_process on product_processID = product_process_id
                                join product on productID = product_id
                                where order_id = ".$order[0]->orderID."
                                and productType = 'product'";
                    $query = $this->reporting_database->query($getQty);
                    $qty = $query->row();
                    $content['qty'][$order[0]->orderID] = $qty->qty;
                }
            }
        }
        arsort($orderTypeList);
        $content['orderTypeList'] = $orderTypeList;

        //adjust sort value of the stops (incrementally)
        $total_stops = count($content['allData']['stops']);
        $current_sort = 1;
        foreach ($content['allData']['stops'] as $loc_id => $stop) {
            if ($content['allData']['stops'][$loc_id]->lat == '' || $content['allData']['stops'][$loc_id]->lon == '') {
                echo 'Location <a href="/admin/locations/display_update_form/'.$loc_id.'">'.$loc_id.'</a> does not have a lat / long so the map can not be generated.  Please update this location';
                exit;
            }

            $content['allData']['stops'][$loc_id]->hidden = $content['allData']['stops'][$loc_id]->sortOrder == 99;
            $content['allData']['stops'][$loc_id]->sortOrder = $current_sort;
            $current_sort++;

            //if no orders are loaded, don't print a delivery manifest
            $content['allData']['stops'][$loc_id]->printManifestNow = 0;
            if ($content['allData']['stops'][$loc_id]->printManifest == 1) {
                //go through each order at that stop
                if (!empty($allData['deliveries'][$loc_id]['orders'])) {
                    foreach ($allData['deliveries'][$loc_id]['orders'] as $order) {
                        if ($order[0]->loaded == 1) {
                            $content['allData']['stops'][$loc_id]->printManifestNow = 1;
                        }
                    }
                }
            }
        }

        $this->data['content'] = $this->load->view('admin/reports/driverMap_v2', $content);
    }

    /**
     * driver_map method is the inital load page for driver maps.
     * There is a lot of ajax involved with this page
     *
     * ajax methods are defined with dm_ as a prefix
     */
    public function driver_map($selected_routes = null)
    {
        $route_string = ($selected_routes)?str_replace("-", ",", $selected_routes):"1";
        $this->load->model('order_model');

        $content['selected_routes'] = explode("-", $selected_routes);

        //get all the locations in these routes
        $sql = "SELECT
                l.locationID,
                l.address,
                l.accessCode,
                l.sortOrder,
                l.route_id,
                l.lat,
                l.lon
                FROM location l
                WHERE serviceType='daily'
                AND route_id IN ({$route_string})
                ORDER BY route_id,sortOrder";
        $locations = query($sql);
        foreach ($locations as $l) {
            $json_locations[$l->route_id][$l->sortOrder] = $l;
        }

        $content['locations'] = $locations;
        $content['locations_json'] = json_encode($json_locations);

        //get claimed orders
        $sql = "SELECT * FROM location
        inner join locker on locker.location_id = locationID
        inner join claim on claim.locker_id = lockerID
        WHERE route_id in ({$route_string})";
        $claims = query($sql);

        //get deliveries
        $sql = "SELECT orderID as ordId,
                       location.*,
                       locker.*,
                       orders.*
                FROM location
                INNER JOIN locker on locker.location_id = locationID
                INNER JOIN orders on orders.locker_id = lockerID
                WHERE orders.orderStatusOption_id <> 10
                AND customer_id <> 2884
                AND route_id in ({$route_string})";
        $deliveries = query($sql);


        //Unkown
        $sql = "SELECT *,
                orders.llNotes as llNotesO,
                location.sortOrder
                FROM orders
                inner join orderItem on orderItem.order_id = orderID
                inner join bag on bagID = orders.bag_id
                inner join customer on customerID = orders.customer_id
                inner join locker on lockerID = orders.locker_id
                inner join location on locationID = locker.location_id
                where orderStatusOption_id <> 10
                and customerID <> 2884
                and route_id in ({$route_string})
                group by bagID
                order by sortOrder, firstName";
        $orders = query($sql);

        $this->load->model('location_model');
        $content['routes_box'] = $r =$this->location_model->get_routes($this->business_id);
        foreach ($r as $route) {
            $r_array[] = $route->route_id;
        }
        $content['routes'] = json_encode($r_array);


        //get all orders for today
        $sql = "SELECT
                orders.llNotes as internalNotes,
                sortOrder,
                orderID,
                oi.qty,
                route_id,
                locationID,
                lockerID,
                loc.address,
                loc.city,
                loc.state,
                loc.zipcode,
                loc.lat,
                loc.lon,
                loc.accessCode,
                firstName,
                lastName,
                concat(firstName,' ',lastName) as fullName,
                bagNumber
                FROM orders
                INNER JOIN orderItem oi ON oi.order_id = orderID
                INNER JOIN locker l ON lockerID = locker_id
                INNER JOIN location loc ON locationID = l.location_id
                LEFT JOIN customer c ON customer_id = customerID
                INNER JOIN bag ON bagID = orders.bag_id
                WHERE loc.route_id IN ($route_string)
                AND orderStatusOption_id != 10
                AND customerID != 2884
                GROUP BY bagID
                ORDER BY route_id";
        $orders = query($sql);

        $wforders = array();
        $dcorders = array();
        foreach ($orders as $key => $order) {
            $orders[$key]->fullName = getPersonName($order);
            
            $order_id = $order->orderID;
            if ($order->sortOrder < 99) {
                $stop = 0;
                $loaded = '<b>Not Loaded</b>';

                if ($order->orderStatusOption_id == 7) {
                    $sql = 'SELECT * FROM orderStatus where order_id = '.$order_id.' and orderStatusOption_id = 13;';
                    $select2 = query($sql);
                    if (sizeof($select2) > 0) {
                        $stop = 1;
                    }
                }

                if ($stop==0) {

                    //$ordertype = $this->order_model->get(array('orderID'=>$order->orderID));
                    $ordType = $this->order_model->getOrderType($order_id);

                    $select3 = 'SELECT * FROM orderStatus where order_id = '.$order_id.' and orderStatusOption_id = 14;';
                    $select3 = query($select3);
                    if (sizeof($select3) > 0) {
                        $loaded = 'L';
                    }

                    if ($ordType == 'WF') {
                        $status = $order->orderStatusOption_id;
                        if ($order->orderStatusOption_id <> 3) {
                            $status = ($order->orderStatusOption_id == 7)?"$":$order->orderStatusOption_id;
                        }

                        $wforders[] = array('address'=>$order->address,
                                            'customer'=>$order->fullName,
                                            'bagNumber'=>$order->bagNumber,
                                            'qty'=>$order->qty,
                                            'route_id'=>$order->route_id,
                                            'status'=>$status,
                                            'loaded'=>$loaded." ".$order->internalNotes);


                        $wforders[$order->locationID]['total'] += 1;
                        if ($order->orderStatusOption_id <> 3) {
                            if ($order->orderStatusOption_id == 7) {
                                $WFordDetails .= '<td><strong>$</strong></td>';
                            } else {
                                $WFordDetails .= '<td><strong>'.$order->orderStatusOption_id.'</strong></td>';
                            }
                        }
                    } else {
                        $dcorders[] = array('address'=>$order->address,
                                            'customer'=>$order->fullName,
                                            'bagNumber'=>$order->bagNumber,
                                            'route_id'=>$order->route_id,
                                            'status'=>$status,
                                            'loaded'=>$loaded." ".$order->internalNotes);

                        $dcorders[$order->locationID]['total'] += 1;
                    }

                    $content['wforders'] = $wforders;
                    $content['dcorders'] = $dcorders;
                }
            }
        }



        //$order_options['select'] = "orderStatusOption_id,dropLocation.lat,dropLocation.lon,firstName, lastName";
        //$order_options['route_id'] = $r_array;
        //$orders = $this->ll_droplocation_model->get_orders($order_options);
        foreach ($orders as $o) {
            $routes['routes'][$o->route_id]['locations'][$o->locationID]['order'][$o->orderID] = array('firstName'=>$o->firstName,'lastName'=>$o->lastName);
            $routes['routes'][$o->route_id]['locations'][$o->locationID]['locationAddress'] = array('lat'=>$o->lat,'lon'=>$o->lon,'street'=>$o->address,'city'=>$o->address,'state'=>$o->state,'zipcode'=>$o->zip,'accessCode'=>$o->accessCode);
        }


        //display
        $this->load->view('admin/reports/driver_map', $content);
    }

    public function driver_log()
    {
        $this->load->model('driver_log_model');
        $sql = "SELECT distinct employeeID, firstName, lastName
                FROM employee
                JOIN business_employee on employee.employeeID = business_employee.employee_id
                JOIN driverLog  ON employee.employeeID = driverLog.employee_id
                WHERE business_employee.business_id = $this->business_id
                AND employee.empStatus = 1
                ORDER BY firstName";

        $query = $this->reporting_database->query($sql);
        $rows = $query->result();
        $content['drivers'] = $rows;

        if ($_POST['Submit']) {
            $this->load->model('driver_log_model');

            //get all stops for this driver
            $content['transactions'] = $transactions = $this->driver_log_model->getTransactions($this->input->get_post('driver'), $this->input->get_post('logDate'), $this->business, true);

            $content['driverMetrics'] = $this->driver_log_model->getMetrics(array('driverLog'=>$transactions, 'withClockInOut'=>true));

            foreach ($transactions as $t) {
                foreach ($t as $key => $value) {
                    if ($key=='order_id') {
                        if ($value) {
                            //get the customer info for this order
                            $sql3 = "select * from customer
                            join orders on customer_id = customerID
                            where orderID = $value;";
                            $query = $this->reporting_database->query($sql3);
                            $customers = $query->result();
                            $o[$value] = getPersonName($customers[0]);
                        }
                    }
                }
            }
            $content['customers'] = $o;
        }

        $content["logDate"] = $this->input->post('logDate');

        $this->template->write('page_title', 'Driver Log', true);
        $this->renderAdmin('driver_log', $content);
    }

    public function dm_get_routes_ajax()
    {
        if (!isset($_POST)) {
            die("no data");
        }

        $this->load->model('ll_droplocation_model');
        $options['route_id'] = explode(",", $_POST['routes']);
        $options['serviceType'] = 'daily';
        $options['select'] = "route_id, lon, lat, street, city,accessCode,id";
        $options['sort_by'] = 'route_id,sortOrder';

        $routes = $this->ll_droplocation_model->get($options);

        $array = array('routes'=>$routes);
        echo json_encode($array);
    }

    public function dm_get_route_information()
    {
        if (!isset($_POST)) {
            die("no data");
        }

        $this->load->model('ll_droplocation_model');
        $options['route_id'] = explode(",", $_POST['routes']);

        $notes = $this->ll_droplocation_model->get_driver_notes($options);
        $claims = $this->ll_droplocation_model->get_claims($options);
        $orders = $this->ll_droplocation_model->get_orders($options);
        $str = '';
        foreach ($orders as $os) {
            $o[] = $os->order_id;
        }
        $types = array(7,14);
        //TODO original code had 13
        $order_status = $this->ll_droplocation_model->get_order_status(array('select'=>'order_id,orderStatusOption_id','orderStatusOption_id'=>$types,'order_id'=>$o));
        $order_type = $this->ll_droplocation_model->get_order_type(array('select'=>'order_id,name,class','order_id'=>$o));
        foreach ($order_status as $s) {
            $status[$s->order_id] = $s->orderStatusOption_id;
        }

        //get all the orders
        foreach ($orders as $order) {
            if ($order->sortOrder < 99) {
                $stop = 0;
                $loaded = 'Not Loaded';

                if ($status[$order->order_id]==7) {
                    $stop = 1;
                }
            }

            if ($stop == 0) {

                //see if it was loaded on the van today
                if ($status[$order->order_id]==14) {
                    $loaded = 'L';
                }

                if ($order_type[$order->order_id] == 'WF') {
                    if ($order->orderStatusOption_id <> 3) {
                        if ($order->orderStatusOption_id == 7) {
                            $details = '$';
                        } else {
                            $details = $order->orderStatusOption_id;
                        }
                    } else {
                        $details = $order->orderStatusOption_id;
                    }

                    $wf[$order->dropLocation_id]['count'] +=1;
                    $wf[$order->dropLocation_id][] = array(
                        'order_id' => $order->order_id,
                        'street' => $order->street,
                        'firstName' => $order->firstName,
                        'lastName' => $order->lastName,
                        'bagNumber' => $order->bagNumber,
                        'qty' => $order->qty,
                        'route_id' => $order->route_id,
                        'order_details' => $details,
                        'loaded' => $loaded,
                        'notes' => $order->llNotes0
                    );
                } else {
                    if ($order->orderStatusOption_id <> 3) {
                        if ($order->orderStatusOption_id == 7) {
                            $details = '$';
                        } else {
                            $details = $order->orderStatusOption_id;
                        }
                    } else {
                        $details = $order->orderStatusOption_id;
                    }
                    $dc[$order->dropLocation_id]['count'] +=1;
                    $dc[$order->dropLocation_id][] = array(
                        'order_id' => $order->order_id,
                        'street' => $order->street,
                        'firstName' => $order->firstName,
                        'lastName' => $order->lastName,
                        'bagNumber' => $order->bagNumber,
                        'qty' => $order->qty,
                        'route_id' => $order->route_id,
                        'order_details' => $details,
                        'loaded' => $loaded,
                        'notes' => $order->llNotes0
                    );
                }
            }
        }

        $array = array('notes'=>$notes,'dc_orders'=>$dc,'wf_orders'=>$wf,'claims'=>$claims);
        echo json_encode($array);
    }

    public function print_map($id = null)
    {
        $query = $this->db->get_where('mapData', array('ID'=>$id));
        $map_data = $query->result();
        $content['markers'] = $map_data[0]->markers;
        $content['lines'] = $map_data[0]->lines;
        $content['left_column'] = $map_data[0]->left_column;
        $content['right_column'] = $map_data[0]->right_column;
        $this->load->view('admin/reports/print_map', $content);
    }

    public function dm_store_map()
    {
        $this->load->model('ll_droplocation_model');
        $options['markers'] = $_POST['markers'];
        $options['lines'] = $_POST['lines'];
        $options['id'] = $_POST['ID'];
        $options['left_column'] = $_POST['left_column'];
        $options['right_column'] = $_POST['right_column'];

        //update the sort order

        $points = json_decode($_POST['markers'], true);

        foreach ($points as $p) {
            $sort[($p['sort_id']+1)] = $p['drop_id'];
        }

        //resort the locations
        $this->ll_droplocation_model->store_route_update($sort);

        $q=$this->db->get_where('mapData', array('ID'=>$options['id']));
        if ($q->result()) {
            $this->db->update('mapData', $options, array('ID'=>$options['id']));
        } else {
            $this->db->insert('mapData', $options);
        }

        $res = array('status'=>"success");
        echo json_encode($res);
    }

    public function pcs_per_hour()
    {
        $content['from'] = date('Y-m-d', strtotime('-1 day'));
        $content['to'] = date('Y-m-d');
        $content['from_hours'] = '00';
        $content['from_minutes'] = '00';
        $content['to_hours'] = '23';
        $content['to_minutes'] = '59';
        $content['role'] = null;

        $startDay = $this->input->get_post('from');
        $endDay = $this->input->get_post('to');
        $role = strtolower($this->input->get_post('role'));

        if (!empty($_POST)) {
            redirect('/admin/reports/pcs_per_hour?' . http_build_query($_POST));
        }

        if ($startDay) {
            $content['from'] = $startDay;
            $content['to'] = $endDay;
            $content['from_hours'] = $this->input->get_post('from_hours');
            $content['from_minutes'] = $this->input->get_post('from_minutes');
            $content['to_hours'] = $this->input->get_post('to_hours');
            $content['to_minutes'] = $this->input->get_post('to_minutes');
            $content['role'] = $role;
        }

        $this->load->model('employee_model');
        $this->load->model('timecard_model');
        $this->load->model('itemupcharge_model');
        $this->load->library('production_metrics');
        $this->load->library('timecards');

        //get a list of all roles
        $this->load->model('aclroles_model');

        $content['roles'] = $this->aclroles_model->get_roles_name_array($this->business_id);

        //die($startDay. ' '.$endDay);
        if ($role and $startDay) {
            $startDay.= ' '.$content['from_hours'].':'.$content['from_minutes'];
            $endDay.= ' '.$content['to_hours'].':'.$content['to_minutes'];

            //get all employees that have the selected role
            $content['emps'] = $emps = $this->employee_model->get_emp_by_roleName($role);

            //get the weekOf
            $defaultWeekStart = get_business_meta($this->business_id, 'timecards_start_of_week', 'Mon');
            $startWeekDay = date('D', strtotime($startDay));
            if ($defaultWeekStart == $startWeekDay) {
                $weekOf = date("Y-m-d", strtotime($startDay));
            } else {
                $weekOf = date("Y-m-d", strtotime('last ' . $defaultWeekStart, strtotime($startDay)));
            }
            
            //$i = 1 - date('w', strtotime($startDay));
            //$weekOf = date("Y-m-d", strtotime($startDay ." $i days"));

            $dates = array();
            $diff = round((strtotime($endDay) - strtotime($startDay)) / 86400);
            for ($i = 0; $i <= $diff; $i++) {
                $dates[] = date("Y-m-d", strtotime($startDay . " +$i days"));
            }

            $content['totHrs'] = 0;
            $content['totItems'] = 0;
            $content['regTot'] = 0;
            $content['otTot'] = 0;
            $content['upchargeTot'] = 0;

            $employee_ids = array();

            foreach ($emps as $e) {
                //get this week's timecard id
                $card = $this->timecard_model->get_one(array(
                    'employee_id' => $e->employeeID,
                    'weekOf' => $weekOf,
                ));

                $employee_ids[] = $e->employeeID;

                if (!empty($card)) {
                    $content['showEmp'][$e->employeeID] = 1;
                    $content['card'][$e->employeeID] = $card;
                    if ($role == 'operations support') {
                        $content['pph'][$e->employeeID] = $this->production_metrics->pphInventory($e->employeeID, $startDay, $endDay);
                    }
                    if ($role == 'pressers') {
                        $content['pph'][$e->employeeID] = $this->production_metrics->pphPressing($e->employeeID, $startDay, $endDay);
                    }

                    //get pictures production
                    $pics = $this->production_metrics->pphPictures($e->employeeID, $startDay, $endDay);
                    if ($pics['itemCount'] > 0) {
                        $content['picturesPPH'][$e->employeeID] = $pics;
                    }

                    $hours = array();
                    foreach ($dates as $date) {
                        $current = $this->timecards->todaysHours($e->employeeID, $date);
                        foreach ($current as $k => $v) {
                            if (!empty($hours[$k])) {
                                $hours[$k] += $v;
                            } else {
                                $hours[$k] = $v;
                            }
                        }
                    }

                    $content['hours'][$e->employeeID] = $hours;
                    $content['totHrs'] += $content['pph'][$e->employeeID]['elapsedTime'];
                    $content['totItems'] += $content['pph'][$e->employeeID]['itemCount'];
                    $content['regTot'] += $content['hours'][$e->employeeID]['regHours'];
                    $content['otTot'] += $content['hours'][$e->employeeID]['overTime'];
                    $content['upchargeTot'] += $content['pph'][$e->employeeID]['userUpcharges'];
                } else {
                    if ($role == 'operations support') {
                        $content['pph'][$e->employeeID] = $metrics = $this->production_metrics->pphInventory($e->employeeID, $startDay, $endDay);
                        if ($metrics['itemCount'] > 0) {
                            $content['showEmp'][$e->employeeID] = 1;
                        }

                        if (!empty($content['pph'][$e->employeeID])) {
                            $content['totHrs'] += $content['pph'][$e->employeeID]['elapsedTime'];
                            $content['totItems'] += $content['pph'][$e->employeeID]['itemCount'];
                            $content['upchargeTot'] += $content['pph'][$e->employeeID]['userUpcharges'];
                        }

                        if (!empty($content['hours'][$e->employeeID])) {
                            $content['regTot'] += $content['hours'][$e->employeeID]['regHours'];
                            $content['otTot'] += $content['hours'][$e->employeeID]['overTime'];
                        }
                    }
                }

                $content['upchargesToday'][$e->employeeID] = $this->itemupcharge_model->get_employee_upcharges($e->employeeID, $startDay, $endDay);
            }

            $content['upchargeTot'] = $content['upchargeTot'] . ' = '.number_format(($content['upchargeTot'] / max($content['totItems'], 1))*100, 0, '.', '').'%';
        }

        $this->template->write('page_title', 'Pieces Per Hour', true);
        $this->renderAdmin('pcs_per_hour', $content);
    }

    public function pcs_detail()
    {
        $from = $this->input->get('from');
        $to = $this->input->get('to');

        $from_hours = $this->input->get('from_hours');

        if ($from_hours) {
            $from.= ' '.$from_hours.':'.$this->input->get('from_minutes').':00';
            $to.= ' '.$this->input->get('to_hours').':'.$this->input->get('to_minutes').':59';
        }

        $employeeID = $this->input->get('emp');
        $role = $this->input->get('role');

        $this->load->library('production_metrics');
        if ($role == 'pressers') {
            $content['presses'] = $this->production_metrics->pphPressing($employeeID, $from, $to, true);
        } elseif ($role == 'operations support') {
            $content['inventories'] = $this->production_metrics->pphInventory($employeeID, $from, $to, true);
        }

        $content['from'] = $from;
        $content['to'] = $to;

        $this->load->model('employee_model');
        $content['emp'] = $this->employee_model->get_by_primary_key($employeeID);

        $this->setPageTitle('Pieces Details');
        $this->renderAdmin('pcs_detail', $content);
    }

    public function invoicing()
    {
        if (!$_POST) {
            $sqlAllInvoices = "SELECT count(orderID) as ordCount, customer_id, firstName, lastName, invoiceNumber, dateInvoiced, sum(invoice.amount) as amount
                FROM `orders`
                left join invoice on invoice.order_id = orderID
                   join customer on customerID = `orders`.customer_id
                where orderPaymentStatusOption_id <> 3
                and closedGross + closedDisc > .02
                and customer.invoice = 1
                and orders.business_id = $this->business_id
                group by customer_id, invoiceNumber";
            $query = $this->db->query($sqlAllInvoices);
            $rows = $query->result();
            $content['allInvoices'] = $rows;
        }

        $this->template->write('page_title', 'Invoicing', true);
        $this->renderAdmin('invoicing', $content);
    }

    public function invoices_open($custID = null)
    {
        $this->load->model('customer_model');
        $this->customer_model->customerID = $custID;
        $content['customer'] = $this->customer_model->get();

        $sqlUnpaidOrders = "SELECT invoiceNumber, dateInvoiced, firstName, lastName, bagNumber, `orders`.* , orderPaymentStatusOption.name as payStat, orderStatusOption.name as ordStat, autoPay, closedGross, closedDisc, terms
                FROM `orders`
                left join customer on customerID = `orders`.customer_id
                left join bag on bagID = `orders`.bag_id
                join orderPaymentStatusOption on orderPaymentStatusOptionID = `orders`.orderPaymentStatusOption_id
                join orderStatusOption on orderStatusOptionID = `orders`.orderStatusOption_id
                join invoicing on invoicing.customer_id = customerID
                left join invoice on invoice.order_id = orderID
                where orderPaymentStatusOption_id <> 3
                and `orders`.customer_id = $custID
                order by `orders`.dateCreated asc;";
        $query = $this->db->query($sqlUnpaidOrders);
        $rows = $query->result();

        $this->load->model('order_model');
        foreach ($rows as $i => $row) {
            $rows[$i]->net_total = $this->order_model->get_net_total($row->orderID);
        }
        $content['unpaidOrders'] = $rows;

        $this->template->write('page_title', 'Open Invoices', true);
        $this->renderAdmin('invoicing', $content);
    }

    public function invoices_history($custID = null)
    {
        $this->load->model('customer_model');
        $this->customer_model->customerID = $custID;
        $content['customer'] = $this->customer_model->get();

        $this->load->model('order_model');

        $sqlInvHistory = "SELECT invoiceNumber, dateInvoiced, datePaid, `check`, sum(amount) as amount
                        from invoice
                        join orders on orderID = order_id
                        where customer_id = $custID
                        and invoice.business_id = $this->business_id
                        group by invoiceNumber order by orderID desc";
        $query = $this->db->query($sqlInvHistory);
        $rows = $query->result();
        $content['history'] = $rows;

        $this->template->write('page_title', 'Invoice History', true);
        $this->renderAdmin('invoicing', $content);
    }

    public function invoices_allCustomers()
    {
        $sqlUnpaidInvoices = "SELECT firstName, lastName, amount, dateInvoiced, invoiceNumber, datePaid, `check`, checkAmt,
                CASE WHEN datePaid IS NOT NULL THEN CASE WHEN `check` REGEXP '^[0-9]+$' THEN 'Check'
                                                         WHEN SUBSTRING(`check`, 1, 2) = 'CC' THEN 'Credit Card'
                                                         ELSE 'Other'
                                                    END
                END AS paymentMethod
                FROM `invoice`
                join orders on orderID = order_id
                join customer on customerID = customer_id
                where invoice.business_id = $this->business_id";
        if ($_GET['status'] == "unpaid") {
            $sqlUnpaidInvoices .= " and orderPaymentStatusOption_id in (1,4) ";
        }
        $sqlUnpaidInvoices .= " order by firstName asc;";
        $query = $this->db->query($sqlUnpaidInvoices);
        $rows = $query->result();
        if ($rows) {
            foreach ($rows as $r) {
                $content['unpaidInvoices'][$r->invoiceNumber]['cust'] = getPersonName($r);
                @$content['unpaidInvoices'][$r->invoiceNumber]['total'] += $r->amount;
                $content['unpaidInvoices'][$r->invoiceNumber]['dateInvoiced'] = $r->dateInvoiced;
                $content['unpaidInvoices'][$r->invoiceNumber]['datePaid'] = $r->datePaid;
                $content['unpaidInvoices'][$r->invoiceNumber]['check'] = $r->check;
                $content['unpaidInvoices'][$r->invoiceNumber]['checkAmt'] = $r->checkAmt;
                $content['unpaidInvoices'][$r->invoiceNumber]['paymentMethod'] = $r->paymentMethod;
            }
        } else {
            $content['unpaidInvoices'] = 'x';
        }

        $_GET['status'] == "unpaid" ? $content['filter'] = "All UNPAID invoices" : $content['filter'] = "All invoices";

        $this->template->write('page_title', 'Invoices', true);
        $this->renderAdmin('invoicing', $content);
    }

    public function invoices_chargeToCard($customerID = null, $invoiceNumber = null)
    {
        $this->load->model('order_model');
        $this->load->model('invoice_model');
        $this->load->model('ordercharge_model');

        //find all orders on that invoice
        $getOrders = "select *
                        from invoice
                        join orders on orderID = order_id
                        where invoiceNumber = ? and invoice.business_id = ? and orders.business_id = ?";
        $query = $this->db->query($getOrders, array($invoiceNumber, $this->business_id, $this->business_id));
        $orders = $query->result();

        $totAmt = 0;
        foreach ($orders as $o) {
            //get the amount of the order
            $ordTot = $this->order_model->get_net_total($o->orderID);
            $chargeArr[$o->orderID] = number_format($ordTot, 2, '.', '');
            $totAmt += $chargeArr[$o->orderID];
        }

        //create an order for the customer
        $newOrdId = $this->order_model->save(array(
            'customer_id'                 => $orders[0]->customer_id,
            'locker_id'                   => $orders[0]->locker_id,
            'business_id'                 => $orders[0]->business_id,
            'notes'                       => "Automatically created order for invoice $invoiceNumber.",
            'statusDate'                  => gmdate('Y-m-d H:i:s'),
            'dateCreated'                 => gmdate('Y-m-d H:i:s'),
            'orderStatusOption_id'        => 1,
            'orderPaymentStatusOption_id' => 1,
        ));

        //add each order as a charge
        foreach ($chargeArr as $order_id => $value) {
            $this->ordercharge_model->save(array(
                'order_id'     => $newOrdId,
                'chargeType'   => "Order $order_id",
                'chargeAmount' => $value,
                'taxable' => '0',
            ));

            // generate a negative order charge for the original order
            $this->ordercharge_model->save(array(
                'order_id'      => $order_id,
                'chargeType'    => "Paid by Credit Card on Order $newOrdId",
                'chargeAmount'  => -($value)
            ));

            //mark each order as paid
            $order = $this->order_model->get_by_primary_key($order_id);
            $date = date("m/d/y");
            $this->order_model->update_all(array(
                'llNotes' => $order->llNotes . "\nPaid on $date By Credit Card.  Order number: $newOrdId",
                'notes' => "Paid on $date By Credit Card.  Order number: $newOrdId\n" . $order->notes
            ), array(
               'orderID' => $order_id,
            ));

            //mark the invoice line as paid
            $ret = $this->invoice_model->update_all(array(
                'datePaid' => gmdate('c'),
                'checkAmt' => $totAmt,
                'check'    => "CC Order #: $newOrdId",
            ), array(
                'order_id' => $order_id,
            ));

            $amount = $this->order_model->get_net_total($order_id);
            $result = $this->order_model->manual_capture($order_id, $value, 'order', $this->employee_id, $newOrdId);
        }

        //change order to inventoried
        $ret = $this->order_model->update_all(array(
            'orderStatusOption_id' => 3,
        ), array(
            'orderID' => $newOrdId,
        ));

        redirect("/admin/orders/details/$newOrdId");
    }

    public function invoices_generate($mode = null, $invNumber = null, $type = null)
    {
        if ($this->input->post('generateInv') || $mode == 'view') {
            $breakout_vat = null;

            if ($mode == 'view') {
                $content['newInvoice']['number'] = $invNumber;
                $getInvNumber = "select * from invoice join orders on orderID = invoice.order_id where invoiceNumber = ? and invoice.business_id = ?";
                $query = $this->db->query($getInvNumber, array($invNumber, $this->business_id));
                $rows = $query->result();

                $invDate = $rows[0]->dateInvoiced;
                $custID = $rows[0]->customer_id;

                foreach ($rows as $r) {
                    $_POST['generate'][] = $r->order_id;
                }
            } else {
                $content['newInvoice']['number'] = $invNumber = strtotime("now");
                $invDate = date("m/d/Y");
                $custID = $_POST['custID'];
            }

            $this->load->model('invoicing_model');
            $this->invoicing_model->customer_id = $custID;
            $content['customer'] = $customer = $this->invoicing_model->get();

            $this->load->model('orderitem_model');
            $this->load->model('ordercharge_model');
            $this->load->model('order_model');
            $this->load->model('invoice_model');
            $this->load->model('invoicecustomfield_model');

            $content['custom_fields'] = $this->invoicecustomfield_model->getInvoiceCustomFields($this->business->businessID, $this->business->default_business_language_id);

            $content['newInvoice']['subTotal'] = 0;
            $content['newInvoice']['taxTotal'] = 0;
            $content['newInvoice']['amtPaid'] = 0;
            $content['newInvoice']['taxDetail'] = array();

            $errors = array();
            foreach ($_POST['generate'] as $order_id) {
                if ($mode != 'view' && $invoice = $this->invoice_model->get_by_primary_key($order_id)) {
                    $errors[] = "Order $order_id already invoiced in invoice #{$invoice->invoiceNumber}";
                    continue;
                }

                if ($mode != 'view' || $this->input->get('tax')) {
                    $this->order_model->update_order_taxes($order_id);
                }

                $order = $this->order_model->get_by_primary_key($order_id);
                $ordTotal = $this->order_model->get_net_total($order_id);
                $subTotal = $this->order_model->get_gross_total($order_id) + $this->order_model->get_orderCharge_total($order_id);
                
                if ($mode == 'view' && !empty($rows)) {
                    foreach($rows as $row) {
                        if ($row->order_id==$order_id){
                            $ordTotal = $row->amount;
                            $subTotal = $ordTotal;
                        }
                    }
                } 

                $pay_with_credit_card = !empty($content['customer'][0]->terms)?($content['customer'][0]->terms=="Credit Card"?true:false):false;
                if ($ordTotal <> 0 || $pay_with_credit_card) {
                    $content['newInvoice']['subTotal'] += $subTotal;
                    $content['newInvoice']['detail'][$order_id]['dateCreated'] = $order->dateCreated;
                    $content['newInvoice']['detail'][$order_id]['ordPayment'] = $order->orderPaymentStatusOption_id;
                    $content['newInvoice']['detail'][$order_id]['ordNotes'] = $order->notes;
                    $content['newInvoice']['detail'][$order_id]['ordTotal'] = $subTotal;
                    $content['newInvoice']['detail'][$order_id]['tax'] = 0;

                    $taxItems = $this->order_model->get_stored_taxes($order_id);
                    foreach ($taxItems as $taxItem) {
                        if (is_null($breakout_vat)) {
                            $breakout_vat = $taxItem['vat_breakout'];
                        } elseif ($breakout_vat != $taxItem['vat_breakout']) {
                            $content['newInvoice']['detail'][$order_id]['error'] = 'VAT mismatch for this order, check this invoice';
                        }

                        $content['newInvoice']['detail'][$order_id]['tax'] += $taxItem['amount'];

                        if (!isset($content['newInvoice']['taxDetail'][$taxItem['taxGroup_id']])) {
                            $content['newInvoice']['taxDetail'][$taxItem['taxGroup_id']] = 0;
                        }
                        $content['newInvoice']['taxDetail'][$taxItem['taxGroup_id']] += $taxItem['amount'];
                    }

                    $content['newInvoice']['taxTotal'] += $content['newInvoice']['detail'][$order_id]['tax'];

                    if ($order->orderPaymentStatusOption_id == 3) {
                        $content['newInvoice']['amtPaid'] += $ordTotal;
                    }

                    //if we need to show details for the order
                    if ($content['customer'][0]->bagDetail != 0) {
                        $this->orderitem_model->clear();
                        $this->db->select('orderItem.notes, qty, unitPrice, product.name, barcode, itemID');
                        $this->orderitem_model->order_id = $order_id;
                        $this->orderitem_model->join("product_process ON product_processID = product_process_id");
                        $this->orderitem_model->join("product ON product_id = productID");
                        $this->orderitem_model->leftjoin("item ON item_id = itemID");
                        $this->orderitem_model->leftjoin("barcode ON item.itemID = barcode.item_id");
                        $content['items'][$order_id] = $this->orderitem_model->get();

                        $this->ordercharge_model->clear();
                        $this->ordercharge_model->order_id = $order_id;
                        $content['charges'][$order_id] = $this->ordercharge_model->get();

                        //for customers that need a breakdown by employee
                        if ($content['customer'][0]->bagDetail >= 1) {
                            $bagNumber = ' ';
                            $notes = '';
                            if ($order->bag_id) {
                                $getBagInfo = "select * from bag where bagID = ".$order->bag_id;
                                $query = $this->db->query($getBagInfo);
                                $bagInfo = $query->result();
                                if ($bagInfo) {
                                    $bagNumber = $bagInfo[0]->bagNumber;
                                    $notes = $bagInfo[0]->notes;
                                }
                            }

                            if (!isset($content['byBag'][$bagNumber]['amt'])) {
                                $content['byBag'][$bagNumber]['amt'] = 0;
                            }

                            $content['byBag'][$bagNumber]['amt'] += $ordTotal;
                            $content['byBag'][$bagNumber]['name'] = $notes;

                            $content['newInvoice']['detail'][$order_id]['bag'] = $bagNumber;
                            $content['newInvoice']['detail'][$order_id]['notes'] = $notes;
                        }
                    }

                    if ($mode != 'view') {
                        $options='';
                        $options['order_id'] = $order_id;
                        $options['invoiceNumber'] = $invNumber;
                        $options['dateInvoiced'] = date('c');
                        $options['amount'] = number_format($ordTotal, 2, '.', '');
                        $options['business_id'] = $this->business_id;
                        $ret = $this->db->insert('invoice', $options);
                        if ($ret) {
                            set_flash_message('success', "Invoice Created");
                        } else {
                            set_flash_message("error", "Error Creating Invoice");
                        }
                    }
                }
            }

            $this->load->model('taxgroup_model');
            $content['newInvoice']['taxGroups'] = array();
            foreach ($content['newInvoice']['taxDetail'] as $taxGroup_id => $amount) {
                if ($taxGroup = $this->taxgroup_model->get_by_primary_key($taxGroup_id)) {
                    $content['newInvoice']['taxGroups'][$taxGroup_id] = $taxGroup;
                }
            }

            $content['newInvoice']['total'] = $content['newInvoice']['subTotal'] + $content['newInvoice']['taxTotal'];

            if (!empty($errors)) {
                set_flash_message("error", $errors);
            }
        }

        $content['date_format'] = get_business_meta($this->business_id, "shortDateDisplayFormat");
        $content['breakout_vat'] = $breakout_vat;

        if ($content['breakout_vat']) {
            $content['newInvoice']['total'] = $content['newInvoice']['subTotal'];
            $content['newInvoice']['subTotal'] -= $content['newInvoice']['taxTotal'];
        }


        //apply payments to an invoice
        if ($this->input->post('applyInv')) {
            $this->load->model('order_model');
            $this->load->model('invoice_model');

            if (empty($_POST['checkNumber'])) {
                set_flash_message('error', "Check number is empty");
                $this->goBack('/admin/reports/invoicing');
            }

            foreach ($_POST['apply'] as $order_id) {
                if (!$this->invoice_model->get_aprax(array('order_id' => $order_id))) {
                    set_flash_message('error', "Order $order_id does not have an invoice generated");
                    $this->goBack('/admin/reports/invoicing');
                }
            }

            foreach ($_POST['apply'] as $order_id) {
                $option = array();
                $where = array();
                $where['orderID'] = $order_id;
                $order = $this->order_model->get($where);
                $option['llNotes'] = $order[0]->llNotes . '<br/>Paid on '. date("m/d/y") . ' Check number ' . $_POST['checkNumber'];
                $option['notes'] = 'Paid on '. date("m/d/y") . ' Check number ' . $_POST['checkNumber'] .'<br/>'. $order[0]->notes;
                $ret = $this->order_model->update($option, $where);

                $option = array();
                $where = array();
                $where['order_id'] = $order_id;
                $option['datePaid'] = date('Y-m-d H:i:s');
                $option['checkAmt'] = $_POST['total'];
                $option['check'] = $_POST['checkNumber'];
                $ret = $this->invoice_model->update($option, $where);

                $amount = $this->order_model->get_net_total($order_id);
                $result = $this->order_model->manual_capture($order_id, $amount, 'check', $this->employee_id, $_POST['checkNumber']);
            }

            if ($ret) {
                set_flash_message('success', "Payment Applied to Invoice");
            } else {
                set_flash_message("error", "Payment could not be applied");
            }
            redirect("admin/reports/invoices_open/".$_POST['custID']);
        }

        if ($type == 'email') {
            $this->load->library('pdf');

            // override employee language
            $this->employee->business_language_id = get_customer_business_language_id($customer[0]->customer_id);

            $filename = $this->pdf->invoice($this->load->view('admin/reports/invoice_pdf', $content, true), $invNumber, $this->business, 'email', $customer[0]->customer_id);

            //mail
            $default_body = 'Dear %customer_billingName%,
                <br><br>
                Attached are our open invoices for your account.  Payment can be sent to:
                <br><br>
                Laundry Locker, Inc.<br>
                1530 Custer Ave.<br>
                San Francisco, CA 94124
                <br><br>
                If you have any question, please contact us at 415-255-7500.
                <br><br>
                %invoiceNotes%
                <br><br>
				Thank you for your prompt assistance with this matter.
				<br><br>
				Sincerely,<br>
				The Laundry Locker Team';

            $body = get_translation(
                "invoice_email_body",
                "admin/reports/invoices_generate",
                array(
                    "customer_billingName" => $customer[0]->billingName,
                    "invoiceNotes" => get_business_meta($this->business_id, "invoiceNotes")
                ),
                $default_body,
                null,
                $this->business->default_business_language_id
            );

            $to = (DEV) ? DEVEMAIL : $customer[0]->email;

            $emailFrom = get_business_meta($this->business_id, 'accounting');
            if (empty($emailFrom)) {
                set_flash_message("error", 'Accounting email account not set, please update <a href="/admin/admin/defaults">business defaults</a>.');
                redirect("/admin/reports/invoices_generate/view/$invNumber");
            }

            $emailFromName = get_business_meta($this->business_id, 'from_email_name');
            $emailTo = $to;
            $emailSubject = 'Invoice '.$invNumber;
            $emailAttach = $filename;
            $emailMessage = $body;
            $emailCc[0] = $cc;
            $emailBcc[0] = get_business_meta($this->business_id, 'accounting');
            //$this->email->set_newline("\r\n");

            if (!send_email($emailFrom, $emailFromName, $emailTo, $emailSubject, $emailMessage, $emailCc, $emailBcc, $emailAttach)) {
                echo $this->email->print_debugger();
                
                @unlink($filename);
                return array('status'=>'fail', "message"=>"Please send with debug to see error");
            }

            //delete file
            @unlink($filename);

            echo 'Invoice has been emailed to '.$customer[0]->email.'<br><br><a href="/admin/reports/invoicing">Return to Invoicing</a>';

            //if this customer pays with CC
            if ($customer[0]->terms == 'Credit Card') {
                echo '<h1>This customer pays with credit card.  Make sure to charge their order <a href="/admin/reports/invoices_open/'.$customer[0]->customer_id.'">here</a></h1>';
            }

            return;
        }

        $this->template->add_js("/js/invoicing.js");
        $this->renderAdmin('invoicing', $content);
    }

    public function markin_log()
    {
        if ($_POST['submit']) {
            $this->load->model("orderitem_model");

            $date = $_POST['logDate'];
            $start = convert_to_gmt_aprax("$date 00:00:00", $this->business->timezone);
            $end = convert_to_gmt_aprax("$date 23:59:59", $this->business->timezone);

            $content['orders'] = $this->orderitem_model->get_orders_and_items_between_dates($this->business_id, $start, $end);

            $allItemsIds = array();
            foreach ($content['orders'] as $r) {
                $allItemsIds[] = $r->item_id;
                $content['newCount'][$r->order_id] = 0;
            }

            $content["newItems"] = $this->orderitem_model->get_new_items_from_items_array($end, $allItemsIds);

            //find out how many new items each order has
            $this->load->model('picture_model');
            foreach ($content['orders'] as $r) {
                if (in_array($r->item_id, $content["newItems"])) {
                    $content['newCount'][$r->order_id]++;
                }
            
                $show_pics = (int)$this->input->post("pics");
                if ($show_pics == 1) {
                    if ($r->item_id <> 0) {
                        $this->picture_model->item_id = $r->item_id;
                        $pictures = $this->picture_model->get();
                        if (!empty($pictures[0])) {
                            $content['pictures'][$r->item_id] = $pictures[0];
                        }
                    }
                }
            }
        }

        $this->template->write('page_title', 'Mark In Log', true);
        $this->renderAdmin('markin_log', $content);
    }

    public function activity_log()
    {
        if ($_POST['submit']) {
            $sql = "select orderStatusOption.name, employee.firstName as eFirst, employee.lastName as eLast, orderStatus.note, orderStatus.date, customer.firstName as cFirst, customer.lastName as cLast, orderID, customer.customerID
                    from orderStatus
                    join orderStatusOption on orderStatusOption_id = orderStatusOptionID
                    left join employee on employee_id = employeeID
                    join orders on order_id = orderID
                    join customer on orders.customer_id = customerID
                    where date_format(convert_tz(date, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') = '$_POST[logDate]'
                    and orders.business_id = $this->business_id
                    ORDER BY employee.firstName, `date`, order_id";
            $query = $this->reporting_database->query($sql);
            $rows = $query->result();
            $content['activity'] = $rows;
        }

        $this->template->write('page_title', 'Complete Activity Log', true);
        $this->data['content'] = $this->load->view('admin/reports/activity_log', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }


    /**
     * UI for displaying the locker history
     *
     * expects either the locker_id to be passed in a $_POST or as segment 4 in the url (Does not use $_GET)
     *
     * @uses helper lockerDropdown($locker_id) to create the dropdown to select locker
     */
    public function locker_history($locker_id = null)
    {
        $this->load->model('locker_model');

        if (isset($_POST['locker'])) {
            $locker_id = $_POST['locker'];
        }

        // check to see if there are really trying to get a locker
        if (!is_numeric($locker_id)) {
            $locker_id = null;
        }

        if ($locker_id) {
            $content['locker'] = $locker_id;
            $history = $this->locker_model->getHistory($locker_id, $this->business_id);
            $content['history'] = $history;
        }

        $this->template->write('page_title', 'Locker History Report', true);
        $this->renderAdmin('locker_history', $content);
    }

    public function possible_problems_redirect()
    {
        if ($_POST['route']) {
            $implode = implode('-', $_POST['route']);
            redirect("/admin/reports/possible_problems/".$implode."/".$_POST['dayOfWeek']);
        }
    }

    public function possible_problems($routeIds = null, $dayOfWeek = null)
    {
        //get routes
        $this->load->model('location_model');
        $options['business_id'] = $this->business_id;
        $options['select'] = "distinct(route_id)";
        $options['sort'] = 'route_id';
        $content['allRoutes'] = $this->location_model->get($options);
        $content['cutoff'] = $this->business->default_report_cut_off_time;

        if (!$dayOfWeek) {
            $dayOfWeek = date("N");
        }
        $content['dayOfWeek'] = $dayOfWeek;

        $blankCustomer = $this->business->blank_customer_id;

        if ($routeIds) {
            $selectedRoutes = explode('-', $routeIds);

            $content['selectedRoutes'] = $selectedRoutes;

            $showRoutes = '';

            if ($content['selectedRoutes']) {
                foreach ($content['selectedRoutes'] as $r => $value) {
                    $showRoutes .= $value.',';
                }
            } else {
                if ($routeIds == 'all') {
                    foreach ($content['allRoutes'] as $r => $value) {
                        $showRoutes .= $value->route_id.',';
                    }
                } else {
                    $showRoutes = '-1,';
                }
            }
            $showRoutes = substr($showRoutes, 0, -1);

            if (!empty($_POST['showRoutes'])) {
                $showRoutes = $_POST['showRoutes'];
            }

            //get missed deliveries
            $sql = "SELECT `orders`.orderID, firstName, lastName, address, companyName, lockerName, route_id, `orders`.llNotes, date_format(orders.dateCreated, '%m/%d/%Y') as created, orderStatusOption.name, invoice, `orders`.*
                        FROM `orders`
                        join customer on customer.customerID = `orders`.customer_id
                        join locker on locker.lockerID = `orders`.locker_id
                        join location on location.locationID = locker.location_id
                        join orderStatusOption on orderStatusOption.orderStatusOptionID = `orders`.orderStatusOption_id
                        join location_serviceDay on location_serviceDay.location_id = locationID
                        where orders.dateCreated < (now() - interval 12 hour)
                        and orderStatusOption_id in(3,8,11,12)
                        and route_id in ($showRoutes)
                        and location.business_id = $this->business_id
                        and orders.customer_id != ".$blankCustomer."
                        and location_serviceDay.location_serviceType_id = 1
                        and location_serviceDay.day = $dayOfWeek
                        order by orders.dateCreated desc limit 100";
            $query = $this->reporting_database->query($sql);
            $rows = $query->result();
            $content['deliveries'] = $rows;
            $content['showRoutes'] = $showRoutes;

            //get missed pickups
            $sql2 = "SELECT claimID, firstName, lastName, route_id, lockerName, address, companyName, serviceType, claim.notes, orderType, customer_id , DATE_FORMAT(claim.updated, '%m/%d/%y %h:%i %p') as dateUpdated, locker_id, locationID
                        FROM claim
                        JOIN customer on customerID = claim.customer_id
                        JOIN locker on lockerID = claim.locker_id
                        JOIN location on locationID = locker.location_id
                        join location_serviceDay on location_serviceDay.location_id = locationID
                        where route_id in ($showRoutes)
                        and claim.active = 1
                        and location.business_id = $this->business_id
                        and location_serviceDay.location_serviceType_id = 2
                        and location_serviceDay.day = $dayOfWeek
                        order by claim.updated desc;";
            $query = $this->reporting_database->query($sql2);
            $pickups = $query->result();
            $content['pickups'] = $pickups;

            //find the last orders in this locker and the last orders for ths customers
            foreach ($pickups as $pickup) {
                $custOrders = "select orderID, orders.dateCreated, location.address as locAddress, companyName, locker.lockerName, customer.firstName from orders
                            join locker on lockerID = locker_id
                            join location on locationID = location_id
                            join customer on customerID = customer_id
                            where customer_id = $pickup->customer_id
                            order by orders.orderID desc
                            limit 2";
                $query = $this->reporting_database->query($custOrders);
                $custOrders = $query->result();
                $content['custOrder'][$pickup->customer_id] = $custOrders;

                $lockOrders = "select orderID, orders.dateCreated, location.address as locAddress, companyName, locker.lockerName, customer.firstName from orders
                            join locker on lockerID = locker_id
                            join location on locationID = location_id
                            join customer on customerID = customer_id
                            where locker_id = $pickup->locker_id
                            order by orders.orderID desc
                            limit 2";
                $query = $this->reporting_database->query($lockOrders);
                $lockOrders = $query->result();
                $content['lockOrders'][$pickup->customer_id] = $lockOrders;
            }


            $sql3 = "SELECT *, driverNotesID from driverNotes
                    join location on locationID = driverNotes.location_id
                    where active = 1
                    and location.route_id in ($showRoutes)
                    and driverNotes.business_id = $this->business_id
                    order by created desc;";
            $query = $this->reporting_database->query($sql3);
            $rows = $query->result();
            $content['notes'] = $rows;

            $this->load->model('serviceType_model');
            $serviceTypes = $this->serviceType_model->get();
            foreach ($serviceTypes as $t) {
                $a[$t->serviceTypeID] = $t->name;
            }
            $content['serviceTypes'] = $a;

            //find any stops that did not have a transaction in the driver log
            $this->load->library('legacy_route');
            //get stops scheduled for today
            $scheduledStops = $this->legacy_route->route_stops_2($showRoutes, $this->business_id, null);
            $stopsScheduled = array();
            foreach ($scheduledStops['stops'] as $scheduledStop => $value) {
                $stopsScheduled[$scheduledStop] = $value;
            }

            //get all the stops that had a transaction
            $getDriverLog = "SELECT distinct(locationID), location.address, companyName
                            FROM driverLog
                            join location on location_id = locationID
                            where locker_id is not null
                            and location.route_id in ($showRoutes)
                            and location.business_id = $this->business_id
                            and delTime > (now() - interval 12 hour)
                            order by locationID";
            $query = $this->reporting_database->query($getDriverLog);
            $driverLogs = $query->result();
            //figure out what stops were not visited
            foreach ($driverLogs as $driverLog) {
                unset($stopsScheduled[(int) $driverLog->locationID]);
                //echo (int) $driverLog->locationID.',';
            }
            $content['stopsMissed'] = $stopsScheduled;
        }
        $content['routes'] = $routeIds;

        $today = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d'), date('Y')));
        $acceptedRoutes = \App\Libraries\DroplockerObjects\AcceptedPossibleProblemReport::search_aprax(array('dateCreated'=>$today, 'business_id'=>$this->business_id));
        $content['acceptedRoutes'] = $acceptedRoutes;

        $this->template->write('page_title', 'Possible Problems Report', true);
        $this->renderAdmin('possible_problems', $content);
    }

    /**
     * accepts a the possible problems report for the day
     */
    public function accept_possible_problem_report($routes = null)
    {
        $routesArray = explode("-", $routes);

        foreach ($routesArray as $route) {
            $acceptedPossibleProblemReport = new \App\Libraries\DroplockerObjects\AcceptedPossibleProblemReport();
            $acceptedPossibleProblemReport->dateCreated = convert_to_gmt_aprax();
            $acceptedPossibleProblemReport->employee_id = $this->employee->employeeID;
            $acceptedPossibleProblemReport->business_id = $this->business_id;
            $acceptedPossibleProblemReport->routes = $route;
            $insert_id = $acceptedPossibleProblemReport->save();
        }

        if ($insert_id) {
            set_flash_message("success", "Possible problem report has been saved");
        } else {
            set_flash_message("success", "Possible problem report has not been saved");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function possible_problems_update()
    {
        if ($_POST['llNotes']) {
            foreach ($_POST['llNotes'] as $order_id => $val) {
                if ($val) {
                    $sql = "update orders set llNotes = ".$this->db->escape($val)." where orderID = '$order_id'";
                    $res = $this->db->query($sql);
                    if ($res) {
                        set_flash_message('success', "Notes updated");
                    } else {
                        set_flash_message('error', 'Failure updating notes');
                    }
                }
            }
        }
        redirect("/admin/reports/possible_problems/".$_POST['showRoutes']);
    }
    /**
     *  Changes an order to 'ready for pickup'.
     * Expects the 'order_id', 'locker_id' as GET parameters.
     *
     */
    public function possible_problems_ready()
    {
        try {
            if (!$this->input->get("order_id")) {
                throw new Exception("'order_id' must be passed as a GET parameter.");
            }
            if (!$this->input->get('locker_id')) {
                throw new Exception("'locker_id' must be passed as a GET parameter.");
            }

            $options['order_id'] = $this->input->get("order_id");
            $options['orderStatusOption_id'] = 9;
            $options['locker_id'] = $this->input->get("locker_id");
            $this->load->model('order_model');
            $res = $this->order_model->update_order($options);

            if ($res) {
                set_flash_message('success', "Order ". $options['order_id'] ." changed to Ready");
            } else {
                set_flash_message('error', 'Failure delivering order');
            }

            redirect($_SERVER['HTTP_REFERER']);
        } catch (Exception $e) {
            send_exception_report($e);
            set_flash_message("error", "Could not perform operation.");
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function possible_problems_check($routes = null, $order_id = null)
    {
        $sql = "update orders
                set llNotes = 'Driver needs to check locker'
                where orderID = '$order_id'";
        $res = $this->db->query($sql);
        if ($res) {
            set_flash_message('success', "Order ".$order_id." to be checked tomorrow");
        } else {
            set_flash_message('error', 'Failure updating notes');
        }

        redirect("/admin/reports/possible_problems/".$routes);
    }


    public function problems_delete_claim($routes = null, $claim_id = null)
    {
        $this->load->model('claim_model');
        $ret = $this->claim_model->deactivate_claim($claim_id);
        if ($ret) {
            set_flash_message('success', "Claim Deleted");
        } else {
            set_flash_message("error", "Claim not deleted");
        }
        redirect("/admin/reports/possible_problems/".$routes."#pickups");
    }

    public function driver_dashboard()
    {
        $this->input->get_post('logDate') ? $logDate = $this->input->get_post('logDate') : $logDate = date("Y-m-d");
        $this->input->get_post('driver') ? $driver = $this->input->get_post('driver') : $driver = '%';
        $this->input->get_post('route') ? $route_id = $this->input->get_post('route') : $route_id = '%';
        
        $content['logDate'] = $logDate;

        //get all drivers
        $getDrivers = "select distinct employeeID, business_id, firstName, lastName from driverLog
                        join employee on employee_id = employeeID
                        where business_id = $this->business_id
                        order by firstName";
        $query = $this->reporting_database->query($getDrivers);
        $content['drivers'] = $query->result();

        $this->load->library('legacy_route');
        $stops = $this->legacy_route->todays_stops($logDate, $this->business_id, $driver, $route_id);

        //find out when each stop was visited
        foreach ($stops as $stop) {
            //load up all the stops that have been visited so far on this route
            //$content['routes'][$stop->route_id][$stop->locationID] = $stop->address;
            $content['delTimes'][$stop->locationID]['when'] = convert_from_gmt_aprax($stop->delTime, "h:i a");
            //find out how many transations happened at each stop
            $content['delTimes'][$stop->locationID]['qty'] ++;

            //find out how many pickups and deliveries each employee did this day
            if ($stop->delType == 'pickup') {
                $content['routes'][$stop->route_id]['pickups'] ++;
            } elseif ($stop->delType == 'delivery') {
                $content['routes'][$stop->route_id]['delivery'] ++;
            }
            $routes[$stop->route_id] = 1;
            $content['routes'][$stop->route_id]['driver'] = getPersonName($stop);
            $content['routes'][$stop->route_id]['employee_id'] = $stop->employee_id;
        }

        //get info for each route
        foreach ($routes as $route => $value) {
            $routeStops = '';

            $content['route_stops'][$route] = $routeStops = $this->legacy_route->getRoute($route, $this->business_id);

            //get orders in picked status
            $content['picked'][$route] = $this->legacy_route->pickups($route, $this->business_id);

            //The following query retrieves all the claims for the particular route.
            $claims = $this->reporting_database->query("SELECT customer.customerID, customer.firstName,customer.lastName, customer.address1, customer.address2, locker.lockerName, location.locationID, location.address
                FROM claim
                INNER JOIN locker ON locker.lockerID = claim.locker_id
                INNER JOIN location ON location.locationID = locker.location_id
                INNER JOIN customer ON customer.customerID = claim.customer_id
                WHERE active = 1
                and location.business_id = $this->business_id
                and location.route_id = {$route}
            ");
            $content['claims'][$route] = $claims->result();

            //get all order that still need to be delivered
            $to_be_deliveries = $this->reporting_database->query(
                "SELECT customer.customerID, customer.firstName, customer.lastName, customer.address1, customer.address2, locker.lockerName, bag.bagNumber, location.locationID, location.address, orders.orderID
                FROM location
                INNER JOIN locker ON locker.location_id = location.locationID
                INNER JOIN orders ON orders.locker_id = locker.lockerID
                JOIN bag ON bag.bagID = orders.bag_id
                INNER JOIN customer ON customer.customerID = bag.customer_id
                WHERE location.business_id = $this->business_id
                and orders.orderStatusOption_id IN (3, 8, 12, 14) AND location.route_id = {$route}"
            );

            $content['to_be_deliveries'][$route] = $to_be_deliveries->result();
        }

        $this->template->write('page_title', 'Driver Dashboard', true);
        $this->renderAdmin('driver_dashboard', $content);
    }

    public function operations_dashboard()
    {
        $content['selected_date'] = $selected_date = date("Y-m-d");
        //$content['selected_date'] = $selected_date = "2013-02-04";

        $this->load->library('legacy_route');
        $phones = $this->legacy_route->getPhones($this->business_id);

        //get driver_status
        $routes = $this->legacy_route->driver_status($selected_date, $this->business_id);
        foreach ($routes as $route => $driver) {
            $content['route'][$route]['driver'] = $driver['name'] .'<br>'.$phones[$driver['employee_id']]->phoneNumber;
            $content['route'][$route]['lastStop'] = $driver['lastStop']  .' @ '.convert_from_gmt_aprax($driver['delTime'], "h:i a") .' ('.$driver['stopsMade'].' stops made)';
            $content['route'][$route]['employee_id'] = $driver['employee_id'];
        }
        //go through all the pickups for this route figure out how many are WF
        foreach ($routes as $key => $value) {
            $pickups = $this->legacy_route->pickups($key, $this->business_id, $selected_date);
            foreach ($pickups as $pickup) {
                substr($pickup->notes, 0, 1) == '1' ? $content['route'][$key]['WF'] += 1 : $content['route'][$key]['DC'] += 1;
            }
            //see if this route is complete yet
            $finished = get_business_meta($this->business_id, 'routeFinished_'.$key);
            if ($finished == date("Y-m-d")) {
                $content['route'][$key]['finished'] = 1;
            }

            //see if this route is complete yet
            $eor = get_business_meta($this->business_id, 'eorFinished_'.$key);
            if ($eor == date("Y-m-d")) {
                $content['route'][$key]['eor'] = 1;
            }
        }

        $this->load->model('employee_model');
        $this->load->library('production_metrics');
        $this->load->library('timecards');

        //get all operations support employees
        $emps = $this->employee_model->get_emp_by_roleName('operations support');
        foreach ($emps as $emp) {
            $content['pph'][$emp->employeeID] = $metrics = $this->production_metrics->pphInventory($emp->employeeID);
            $content['pph'][$emp->employeeID]['firstName'] = $emp->firstName;
            $content['totPieces'] = $totPieces = $metrics['allItems'];
        }

        //get lot info
        $lotInfo = "select * from lot
                    where business_id = $this->business_id
                    and date_format(convert_tz(startTime, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') = '".$selected_date."'
                    order by lotNumber desc";
        $query = $this->reporting_database->query($lotInfo);
        $lot = $query->result();

        //get machines
        $getMachines = "select * from machine where business_id = $this->business_id";
        $q = $this->reporting_database->query($getMachines);
        $machines = $q->result();
        foreach ($machines as $machine) {
            //find out how much time is left
            $now = strtotime("now");
            $start = strtotime(convert_from_gmt_aprax($machine->startTime, STANDARD_DATE_FORMAT));

            $diff = ($now - $start)/60;
            $content['machines'][$machine->machineID] = $machine;
            $content['timeLeft'][$machine->machineID] = number_format($machine->cycleTime - $diff, 0, '.', '');
        }

        $content['currentLot'] = $totPieces - $lot[0]->lotCount;
        $content['lotNumber'] = $lot[0]->lotNumber;
        $content['lots'] = $lot;
        $content['message'] = get_business_meta($this->business_id, 'ops_dashboard_message');
        $content['calendarURL'] = get_business_meta($this->business_id, 'ops_calendar_url');

        $this->template->write('page_title', 'Operations Dashboard', true);
        $this->data['content'] = $this->load->view('admin/reports/operations_dashboard', $content, true);
        $this->template->write_view('content', 'inc/full_column', $this->data);
        $this->template->render();
    }

    public function ops_dashboard_pcs_detail()
    {
        $this->load->model('orderItem_model');
        $content['allItems'] = $this->orderItem_model->todays_items($this->busines_id);
        $this->template->write('page_title', 'Operations Dashboard Pcs Detail', true);
        $this->data['content'] = $this->load->view('admin/reports/ops_dashboard_pcs_detail', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function ops_dashboard_add_lot_to_route()
    {
        $route = implode(', ', $this->input->post('Route'));
        $addToLot = "update lot set routes = '$route' where lotID = ".$this->input->post('lotID');
        $query = $this->db->query($addToLot);

        set_flash_message('success', "Routes Added to Lot");
        redirect('/admin/reports/operations_dashboard');
    }

    public function ops_dashboard_finish_route()
    {
        if (isset($_GET['no'])) {
            update_business_meta($this->business_id, 'routeFinished_'.$_GET['no'], 0);
            set_flash_message('success', "Route Finished");
        } elseif (isset($_GET['yes'])) {
            update_business_meta($this->business_id, 'routeFinished_'.$_GET['yes'], date("Y-m-d"));
            set_flash_message('success', "Route Finished");
        } else {
            set_flash_message('error', "Error Finishing Route");
        }
        redirect('/admin/reports/operations_dashboard');
    }

    public function ops_dashboard_finish_eor()
    {
        if (isset($_GET['no'])) {
            update_business_meta($this->business_id, 'eorFinished_'.$_GET['no'], 0);
            set_flash_message('success', "Route Finished");
        } elseif (isset($_GET['yes'])) {
            update_business_meta($this->business_id, 'eorFinished_'.$_GET['yes'], date("Y-m-d"));
            set_flash_message('success', "End of Route Finished");
        } else {
            set_flash_message('error', "Error Finishing EOR");
        }
        redirect('/admin/reports/operations_dashboard');
    }

    public function new_lot()
    {
        //get the total number or items checked in
        $this->load->model('employee_model');
        $this->load->library('production_metrics');

        $emps = $this->employee_model->get_emp_by_roleName('operations support');
        $metrics = $this->production_metrics->pphInventory($emp[0]->employeeID);
        $totPieces = $metrics['allItems'];

        $lastLot = "select max(lotNumber) as lastLot
                    from lot
                    where business_id = $this->business_id
                    and date_format(convert_tz(startTime, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') = date_format(convert_tz(now(), 'GMT', 'America/Los_Angeles'), '%Y-%m-%d')";
        $query = $this->reporting_database->query($lastLot);
        $lot = $query->result();
        $newLot = $lot[0]->lastLot + 1;

        $addLot = "insert into lot (business_id, lotNumber, lotCount, startTime)
                    values('$this->business_id', '$newLot', '$totPieces', now())";
        $query = $this->db->query($addLot);

        set_flash_message('success', "New Lot Created");
        redirect('/admin/reports/operations_dashboard');
    }

    public function reassigned_barcodes()
    {
        if ($this->input->is_ajax_request()) {
            $this->ajax_get_reassigned_barcodes();
            die();
        }

        $start_date = $this->input->get("start_date");
        $end_date = $this->input->get("end_date");
        $content["start_date"] = $start_date?$start_date:date("Y-m-d", time() - 60 * 60 * 24);
        $content["end_date"] = $end_date?$end_date:date("Y-m-d");

        $this->template->write('page_title', 'Reassigned Barcodes', true);
        $this->renderAdmin('reassigned_barcodes', $content);
    }

    protected function ajax_get_reassigned_barcodes()
    {
        $this->load->model("customer_item_model");

        $records = array(
            "sEcho" => (int)$this->input->get("sEcho"),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "iDisplayStart" => $this->input->get("iDisplayStart"),
            "aaData" => array(),
            "aoSorting" => array("0", "asc")
        );

        $start_date = $this->input->get("start_date");
        $end_date = $this->input->get("end_date");

        $page_from = $this->input->get("iDisplayStart");
        $limit = $this->input->get("iDisplayLength");


        $result = $this->customer_item_model->get_customer_item_history($this->business_id, false, $limit, $page_from, $start_date, $end_date);
        $records["aaData"] = $result["items"];

        //add custom data/format to rows
        $this->load->model("picture_model");
        $this->load->model("barcode_model");
        foreach ($records["aaData"] as $index => $item) {
            $barcode = $this->barcode_model->getBarcode($item->barcode, $this->business_id);
            if (empty($barcode)) {
                continue;
            }
            $records["aaData"][$index]->barcode_number = $item->barcode;
            $records["aaData"][$index]->order_id = $item->orderID;
            $records["aaData"][$index]->barcode = '<a href="/admin/orders/edit_item?orderID='.$item->orderID.'&itemID='.$barcode[0]->itemID.'">'.$item->barcode.'</a>';
            $records["aaData"][$index]->from_customer_name = '<a href="/admin/customers/detail/'.$item->from_customer_id.'">'.  getPersonName($item, true, 'from_customer_firstName', 'from_customer_lastName').'</a>';
            $records["aaData"][$index]->to_customer_name = '<a href="/admin/customers/detail/'.$item->to_customer_id.'">'.  getPersonName($item, true, 'to_customer_firstName', 'to_customer_lastName').'</a>';
            $records["aaData"][$index]->employee = '<a href="/admin/employees/manage/'.$item->employee_id.'">'.  getPersonName($item, true, 'employee_firstName', 'employee_lastName').'</a>';
            $records["aaData"][$index]->order = '<a href="/admin/orders/details/'.$item->orderID.'" target="_blank">'.$item->orderID.'</a><br/>
                    '.local_date($this->business->timezone, $item->date, 'n/d/y g:i:s a').'';

            //get item picture
            $pictures = $this->picture_model->get_aprax(array("item_id" => $barcode[0]->itemID));
            $picture = end($pictures);
            if (!empty($picture->file)) {
                $records["aaData"][$index]->picture = "<a target='_blank' href='https://s3.amazonaws.com/LaundryLocker/{$picture->file}'><img style='width:100px' src='https://s3.amazonaws.com/LaundryLocker/{$picture->file}' alt='Current Picture' /></a>";
            } else {
                $records["aaData"][$index]->picture = "NO PICTURE";
            }
        }

        //return datables info
        $records["iTotalRecords"] = $result["count"];
        $records["iTotalDisplayRecords"] = $records["iTotalRecords"];

        usort($records["aaData"], function ($a, $b) {
            $columns_map = array(
                "barcode_number",
                "from_customer_lastName",
                "to_customer_lastName",
                "employee_lastName",
                "order_id",
            );

            $sort_column = $this->input->get("iSortCol_0");

            if ($this->input->get("sSortDir_0") == "asc") {
                return strtolower($a->{$columns_map[$sort_column]}) > strtolower($b->{$columns_map[$sort_column]});
            }

            return strtolower($a->{$columns_map[$sort_column]}) < strtolower($b->{$columns_map[$sort_column]});
        });

        echo json_encode($records);
    }

    public function employee_login_out()
    {
        if ($this->input->is_ajax_request()) {
            $this->ajax_get_employee_login_out();
            die();
        }

        $start_date = $this->input->get("start_date");
        $end_date = $this->input->get("end_date");
        $firstName = $this->input->get("firstName");
        $email = $this->input->get("email");
        $username = $this->input->get("username");

        $content["start_date"] = $start_date?$start_date:date("Y-m-d", time() - 60 * 60 * 24);
        $content["end_date"] = $end_date?$end_date:date("Y-m-d");
        $content["firstName"] = $firstName?$firstName:'';
        $content["username"] = $username?$username:'';
        $content["email"] = $email?$email:'';

        $this->template->write('page_title', 'ajax_get_employee_login_out', true);
        $this->renderAdmin('employee_log_in_out', $content);
    }

    protected function ajax_get_employee_login_out()
    {
        $this->load->model("employee_log_model");

        $records = array(
            "sEcho" => (int)$this->input->get("sEcho"),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );

        $options["filters"] = array();
        $options["filters"]["start_date"] = $this->input->get("start_date");
        $options["filters"]["end_date"] = $this->input->get("end_date");
        $options["filters"]["firstName"] = $this->input->get("firstName");
        $options["filters"]["email"] = $this->input->get("email");
        $options["filters"]["username"] = $this->input->get("username");

        $options["pagination"]["page_from"] = $this->input->get("iDisplayStart");
        $options["pagination"]["limit"] = $this->input->get("iDisplayLength");

        $options["order"]["index"] = $this->input->get("iSortCol_0");
        $options["order"]["direction"] = $this->input->get("sSortDir_0");

        $result = $this->employee_log_model->get_employees_login_out_history($this->business_id, $options);

        $records["aaData"] = $result["items"];

        //add custom data/format to rows
        $standardDateFormat = get_business_meta($this->business_id, "standardDateDisplayFormat");
        foreach ($records["aaData"] as $index => $item) {
            $records["aaData"][$index]->date = convert_from_gmt_aprax($item->dateStatus, $standardDateFormat);
            $records["aaData"][$index]->firstName = '<a href="/admin/employees/manage/'.$item->employeeID.'">'.$item->firstName.'</a>';
            $records["aaData"][$index]->lastName = '<a href="/admin/employees/manage/'.$item->employeeID.'">'.$item->lastName.'</a>';
            $records["aaData"][$index]->email = '<a href="/admin/employees/manage/'.$item->employeeID.'">'.$item->email.'</a>';
            $records["aaData"][$index]->username = '<a href="/admin/employees/manage/'.$item->employeeID.'">'.$item->username.'</a>';
        }

        //return datables info
        $records["iTotalRecords"] = $result["count"];
        $records["iTotalDisplayRecords"] = $records["iTotalRecords"];

        echo json_encode($records);
    }

    public function item_speed()
    {
        if ($this->input->is_ajax_request()) {
            $this->ajax_get_item_speed();
            die();
        }

        $start_date = $this->input->get("start_date");
        $end_date = $this->input->get("end_date");
        $content["start_date"] = $start_date?$start_date:date("Y-m-d", time() - 60 * 60 * 24);
        $content["end_date"] = $end_date?$end_date:date("Y-m-d");

        $this->template->write('page_title','Slow Mark-Ins', true);
        $this->renderAdmin('item_speed', $content);
    }

    protected function ajax_get_item_speed()
    {
        $this->load->model("orderitem_model");

        $records = array(
            "sEcho" => (int)$this->input->get("sEcho"),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );

        $start_date = $this->input->get("start_date");
        $end_date = $this->input->get("end_date");

        $page_from = $this->input->get("iDisplayStart");
        $limit = $this->input->get("iDisplayLength");

        //sort columns
        /*
        $sorted = false;
        if ($this->input->get("iSortCol_0") !== false) {
            $this->db->order_by("orderID", $this->input->get("sSortDir_0"));
            $sorted = true;
        }*/

        $result = $this->orderitem_model->get_orders_with_maxdiff($this->business_id, $start_date, $end_date);
        $records["aaData"] = $result["items"];

        //add custom data/format to rows
        foreach ( $records["aaData"] as $index => $item ) {
            $records["aaData"][$index]->order_id = '<a href="/admin/orders/details/'.$item->order_id.'">'.$item->order_id.'</a>';
            $records["aaData"][$index]->dateCreated = $item->dateCreated;
            $records["aaData"][$index]->maxDiff = $item->diff." minutes";
        }

        //return datables info
        $records["iTotalRecords"] = $result["count"];
        $records["iTotalDisplayRecords"] = $records["iTotalRecords"];

        echo json_encode($records);
    }

    /**
     * The follwoign action is *, do not use.
     * UI for viewing and creating driver notes
     */
    public function driver_notes()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('location', 'Location', 'required');

        if ($this->form_validation->run()) {
            $note = new \App\Libraries\DroplockerObjects\DriverNotes();
            $note->business_id = $this->business_id;
            $note->location_id = $_POST['location'];
            $note->active = 1;
            $note->createdBy = $this->buser_id;
            $note->created = date("c");
            $note->note = $_POST['note'];
            $res = $note->save();

            if ($res) {
                set_flash_message('success', "Note has been created");
            } else {
                set_flash_message('error', 'Failed saving driver note');
            }
            redirect('/admin/reports/driver_notes');
        }

        // load the notes
        $content['activeNotes'] = \App\Libraries\DroplockerObjects\DriverNotes::view($this->business_id);


        // load the locations
        $this->load->model('location_model');

        //TODO: probably filter status = 'active' and serviceType != 'not in service'
        $locations = $this->location_model->get_aprax(array(
            'business_id' => $this->business_id,
            'status' => 'active'
        ));

        $this->load->model('locationtype_model');
        $locationTypes = $this->locationtype_model->simple_array();

        foreach ($locations as $l) {
            $content['locations'][$l->locationID] = sprintf(
                '%s %s (%s) (%s-%s) [%s-%s](%s)',
                $l->address,
                $l->address2,
                substr($l->companyName, 0, 35),
                $l->locationID,
                substr($l->accessCode, 0, 10),
                $locationTypes[$l->locationType_id],
                $l->serviceType,
                $l->route_id
            );
        }
        natsort($content['locations']);


        $content['business_id'] = $this->business_id;
        $this->template->write('page_title', 'Driver Notes', true);
        $this->data['content'] = $this->load->view('admin/reports/driver_notes', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function driver_note_detail($note_id = null)
    {
        $driverNote = new \App\Libraries\DroplockerObjects\DriverNotes($note_id);

        $content['dateCreated'] = convert_from_gmt_aprax($driverNote->created->format("Y-m-d H:i:s"), STANDARD_DATE_FORMAT);
        $content['status'] = ($driverNote->active==1)?"Active":"Closed";
        $content['note'] = $driverNote->note;
        $content['noteID'] = $driverNote->driverNotesID;
        $employee = new Employee(get_employee_id($driverNote->createdBy));
        $content['employee'] = getPersonName($employee);

        if (!empty($driverNote->completedBy)) {
            $employee = $employee = new Employee(get_employee_id($driverNote->completedBy));
            $content['closedBy'] = getPersonName($employee);
            $content['closedDate'] = convert_from_gmt_aprax($driverNote->completed->format('Y-m-d g:ia'), STANDARD_DATE_FORMAT);
        }

        // from the droid table
        $responses = $driverNote->driverResponses();
        foreach ($responses as $response) {
            $employee = new Employee($response->employee_id);
            $note = null;
            $note->date = convert_from_gmt_aprax($response->dateCreated, STANDARD_DATE_FORMAT);
            $note->name = getPersonName($employee);
            $note->notes = $response->notes;
            $responseArray[] = $note;
        }

        $content['responses'] = $responseArray;

        $this->data['content'] = $this->load->view('admin/reports/driver_notes_detail', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }


    /**
         * Removes a particular note form the lsit of active notes.
         * Expects the noteID as $_POST['note_id']
         */
    public function driver_notes_complete()
    {
        if (isset($_POST['submit'])) {
            $noteId = $_POST['note_id'];
            $note = new \App\Libraries\DroplockerObjects\DriverNotes($noteId);
            $note->active = 0;
            $note->closedNotes = mysql_escape_string($_POST['closedNote']);
            $note->completedBy = $this->buser_id;
            $note->completed = date("Y-m-d H:i:s");
            $res = $note->save();

            if ($res) {
                set_flash_message('success', "Note has been completed");
            } else {
                set_flash_message("error", 'Failure completing note');
            }
        } else {
            set_flash_message('Note ID not passed to action');
        }
        redirect('/admin/reports/driver_notes');
    }

    /*
     * TODO: update to query by productID, product name is not indexed so it's slowing down the query
     */
    public function daily_items()
    {
        $sqlProducts = "select distinct name, productID from product where business_id = $this->business_id order by name";
        $query = $this->reporting_database->query($sqlProducts);
        $content['products'] = $query->result();

        if ($_POST['logDate']) {
            $sqlItems = "select orders.dateCreated, product.name, firstName, lastName, orderID, item_id,
                            (select barcode from barcode where barcode.item_id = orderItem.item_id limit 1) as barcode,
                            (select file from picture where picture.item_id = orderItem.item_id limit 1) as picture, orderItem.updated
                        from orderItem
                        join orders on orderID = order_id
                        left join customer on customerID = customer_id
                        join product_process on product_processID = product_process_id
                        join product on productID = product_id
                        where orders.business_id = $this->business_id
                        and productID = '$_POST[product]'
                        and date_format(convert_tz(orderItem.updated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') = ?";
            $query = $this->reporting_database->query($sqlItems, array($_POST["logDate"]));
            $content['items'] = $rows = $query->result();

            //get the maximum itemId
            foreach ($rows as $r) {
                if ($r->item_id > $maxItem) {
                    $maxItem = $r->item_id;
                }
            }
            //set the minimum item id for new items
            $content['newItem'] = $maxItem - count($rows);
        }

        $this->template->write('page_title', 'Daily Items', true);
        $this->renderAdmin('daily_items', $content);
    }


    /**
     * UI for displaying unpaid orders.
     *
     * NOTE, that this function will try to capture payments
     */
    public function unpaid_orders()
    {
        $this->load->model('orderitem_model');
        $this->load->model('order_model');


        if (isset($_POST['captureOrders'])) {
            $count = 0;
            $total = 0;
            //goes through each order that is selected
            foreach ($_POST['captureOrders'] as $order_id) {
                $total++;
                //get the order info
                $options = array();
                $options['orderID'] = $order_id;
                $options['business_id'] = $this->business_id;
                $o = $this->order_model->get($options);

                $isCapturableStatus = $this->order_model->isCapturable($o[0]);
                if ($isCapturableStatus['status']=='SUCCESS') {
                    $result = $this->order_model->capture($order_id, $this->business_id);
                    echo 'order '.$order_id.' is capturable<br>';
                    if ($result['status'] == 'success') {
                        echo 'order '.$order_id.' was captured<br>';
                        //if order is captured. Change it to completed
                        $options['orderStatusOption_id'] = 10;
                        $options['order_id'] = $order_id;
                        $options['locker_id'] = $o[0]->locker_id;
                        $options['business_id'] = $o[0]->business_id;
                        $options['orderPaymentStatusOption_id'] = 3;
                        $ret = $this->order_model->update_order($options);
                        $count++;
                    } else {
                        echo 'order '.$order_id.' was not captured because '.$result['message'].'<br>';
                    }
                } else {
                    echo 'order '.$order_id.' is not capturable<br>';

                    //order is not capturable (probalby because the customer does not have a card on file
                    //so put it on payment hold
                    $options['orderStatusOption_id'] = 7;
                    $options['order_id'] = $order_id;
                    $options['locker_id'] = $o[0]->locker_id;
                    $options['orderPaymentStatusOption_id'] = $o[0]->orderPaymentStatusOption_id;
                    $ret = $this->order_model->update_order($options);
                }
            }
            set_flash_message('success', $count. " orders out of {$total} were captured");
            redirect($this->uri->uri_string());

            return;
        }

        $blankCustomer = $this->business->blank_customer_id;


        $sqlUnpaidOrders = "SELECT creditCardID, firstName, lastName, bagNumber, orders.* , orderPaymentStatusOption.name as payStat, orderStatusOption.name as ordStat, autoPay, invoice, COALESCE(location.route_id, delivery_window.route) AS route
            FROM orders
            left join customer on customerID = orders.customer_id
            left join creditCard on creditCard.customer_id = customerID
            left join bag on bagID = orders.bag_id
            left join orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id
            left join orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
            LEFT JOIN locker ON locker.lockerID = orders.locker_id
            LEFT JOIN location ON location.locationID = locker.location_id
            LEFT JOIN orderHomeDelivery ON orderHomeDelivery.order_id = orders.orderID
            LEFT JOIN delivery_window ON delivery_window.delivery_windowID = orderHomeDelivery.orderHomeDeliveryID
            where orderPaymentStatusOption_id <> 3
            and orders.dateCreated < date_sub(NOW(), INTERVAL 3 day)
            and orders.business_id = $this->business_id
            and invoice = 0
            and customerID != {$blankCustomer}
            and orders.orderStatusOption_id = 10
            UNION
            select creditCardID, firstName, lastName, bagNumber, orders.* , orderPaymentStatusOption.name as payStat, orderStatusOption.name as ordStat, autoPay, invoice, COALESCE(location.route_id, delivery_window.route) AS route
            from orders
            join customer on customerID = customer_id
            left join creditCard on creditCard.customer_id = customerID
            left join bag on bagID = orders.bag_id
            left join orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id
            left join orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
            left join transaction on transaction.order_id = orders.orderID
            LEFT JOIN locker ON locker.lockerID = orders.locker_id
            LEFT JOIN location ON location.locationID = locker.location_id
            LEFT JOIN orderHomeDelivery ON orderHomeDelivery.order_id = orders.orderID
            LEFT JOIN delivery_window ON delivery_window.delivery_windowID = orderHomeDelivery.orderHomeDeliveryID            where orderPaymentStatusOption_id = 3
            and transactionID is null
            and bag_id <> 0
            and invoice = 0
            and orders.dateCreated > '2011-01-01'
            and (closedGross + closedDisc) > 0.02
            and orders.business_id = $this->business_id
            order by orderID desc";
        $query = $this->reporting_database->query($sqlUnpaidOrders);
        $content['unpaid'] = $query->result();

        //customers on invoice are already excluded
        $count = 0;
        $content['grandTotal'] = 0;
        foreach ($content['unpaid'] as $i => $c) {
            //$order = new Order($c->orderID, FALSE);
            //$itemTotal = $order->get_gross_total();
            //$itemDisc = $order->get_discount_total();
            $content['grandTotal'] += $content['total'][$c->orderID] = $total = $this->order_model->get_subtotal($c->orderID);

            if ($total <= 0.02) {
                if ($c->orderStatusOption_id == '10') { //if the order is completed and it's a free order
                    //change it to captured
                    $options['orderPaymentStatusOption_id'] = 3;
                    $where['orderID'] = $c->orderID;
                    $ret = $this->order_model->update($options, $where);
                }
                //do not show the order
                unset($content['unpaid'][$i]);
            }
            //if an order is somehow captured, make it unpaid.
            elseif ($c->orderPaymentStatusOption_id == 3) {
                $where['orderID'] = $c->orderID;
                $options['orderPaymentStatusOption_id'] = 1;
                $ret = $this->order_model->update($options, $where);
            }
            $count++;
        }

        $this->template->write('page_title', 'Unpaid Orders', true);
        $this->renderAdmin('unpaid_orders', $content);
    }

    public function wf_supplier_production()
    {
        $business = new Business($this->business_id, false);

        $this->load->model('employee_model');
        $sql = "SELECT supplierID, supplierBusiness_id, companyName, address, city, state, phone
            FROM supplier
            JOIN business ON business.businessID = supplier.supplierBusiness_id
            WHERE business_id = ?
            ";
        $params = array($this->business_id);

        $supplier = $this->input->post("supplier");

        // if limited to a specific supplier_id
        if ($this->employee->supplier_id) {
            $sql .= "AND supplierID = ?";
            $params[] = $this->employee->supplier_id;
            $supplier = $this->employee->supplier_id;
        }
        $content['suppliers'] = $this->db->query($sql, $params)->result();

        if ($this->input->post('byBag') == 'byBag') {

            //use business cutoff time
            $cutOff = $business->default_report_cut_off_time;
            $selectDayz = $_POST['selectedDate'] . ' ' . $cutOff;

            $content['rangeStart'] = $rangeStart = date("Y-m-d H:i:s", strtotime($selectDayz));
            $content['rangeEnd'] = $rangeEnd = date("Y-m-d H:i:s", strtotime($rangeStart . "+ 1 day"));

            $rangeStart =  convert_to_gmt_aprax($rangeStart);
            $rangeEnd =  convert_to_gmt_aprax($rangeEnd);


            $sqlGetWF = "SELECT bag.bagNumber, orderItem.order_id, orderItem.supplier_id, route_id, wfLog.weightOutTime as ordDate, orderItem.qty, product.name, productCategory.module_id, customer.firstName, orderItemID, product.productCategory_id, customer.lastName, orders.customer_id, location.serviceType, locker.lockerName, orders.notes as busNotes,
                                    (select cost from product_supplier where product_supplier.supplier_id = orderItem.supplier_id
                                        and product_supplier.product_process_id = orderItem.product_process_id) as cost,
                                    (select minQty as minCost from product_supplier where product_supplier.supplier_id = orderItem.supplier_id
                                        and product_supplier.product_process_id = orderItem.product_process_id) as minCost,
                                        wfLog.*
                                    FROM wfLog
                                    join orderItem on orderItem.order_id = wfLog.order_id
                                    inner join product_process on orderItem.product_process_id = product_processID
                                    join product on productID = product_process.product_id
                                    JOIN productCategory ON product.productCategory_id=productCategory.productCategoryID
                                    inner join orders on orderItem.order_id = orderID
                                    inner join locker on lockerID = orders.locker_id
                                    inner join location on locationID = locker.location_id
                                    inner join bag on bagID = orders.bag_id
                                    left join customer on customerID = orders.customer_id
                                    where DATE_FORMAT(wfLog.weightOutTime, '%Y-%m-%d %H:%i:%s') > '$rangeStart'
                                    and DATE_FORMAT(wfLog.weightOutTime, '%Y-%m-%d %H:%i:%s') < '$rangeEnd'
                                    and supplier_id = $supplier
                                    order by ordDate, bagNumber";

            $query = $this->reporting_database->query($sqlGetWF);
            $content['allItems'] = $allItems = $query->result();

            $lbTotal = 0;
            $costTotal = 0;
            $bagTotal = array();

            //get wf product category
            $getCategory = "SELECT productCategoryID FROM productCategory where slug = '".WASH_AND_FOLD_CATEGORY_SLUG."' and business_id = ".$this->business_id;
            $query = $this->reporting_database->query($getCategory);
            $wfCategory = $query->result();

            $content['wfCat'] = $wfCat = $wfCategory[0]->productCategoryID;

            $justBags = array();
            foreach ($allItems as $a) {
                //if it is WF
                if ($a->productCategory_id == $wfCat) {
                    $lbTotal += $a->qty;
                    $supplierIn += $a->weightIn;
                    $supplierOut += $a->weightOut;
                }
                $bagTotal[$a->bagNumber] = 1;

                //if there is a minimum cost set, make sure it hits that
                if ($a->minCost > ($a->qty * $a->cost)) {
                    $costTotal += $a->minCost;
                    $content['cost'][$a->orderItemID] = $a->minCost;
                } else {
                    $costTotal += ($a->qty * $a->cost);
                    $content['cost'][$a->orderItemID] = ($a->qty * $a->cost);
                }

                $justBags[] = $a->bagNumber ;
            }

            $justBags = array_unique($justBags);
            sort($justBags);

            $content['justBags'] = $justBags;
            $content['vars']['lbTotal'] = $lbTotal;
            $content['vars']['costTotal'] = $costTotal;
            $content['vars']['supplierIn'] = $supplierIn;
            $content['vars']['supplierOut'] = $supplierOut;
            $content['vars']['bagTotal'] = $bagTotal;
        }

        if ($this->input->post('byBag') == 'byEmp') {
            $this->load->library('production_metrics');

            isset($_POST['daysToShow']) ? $daysToShow = $_POST['daysToShow'] : $daysToShow = 15;
            for ($i = 0; $i <= $daysToShow; $i++) {
                $newDate = date("Y-m-d", strtotime($_POST['selectedDate'] .'-'.$i.' days'));
                $content['byEmp'][$newDate] = $returnData = $this->production_metrics->getWFDataByEmp($newDate, $_POST['supplier']);
                foreach ($returnData as $r) {
                    $content['emp'][$r->foldCust][$newDate]['lb'] = $r->lbs;
                    $content['emp'][$r->foldCust][$newDate]['bags'] = $r->bags;
                    $content['total'][$newDate]['lbs'] += $r->lbs;
                    $content['total'][$newDate]['bags'] += $r->bags;
                }
            }
        }

        //get weekly totals
        if ($_POST) {
            $supplier = $this->input->post("supplier");
            //0 (for Sunday) through 6 (for Saturday)
            $numDaysShow = date("w", strtotime($selectDayz));

            //gets the Sunday of the week
            $content['weekStart'] = $rangeStart = date("Y-m-d H:i:s", strtotime($selectDayz . " -$numDaysShow days"));
            //gets 7 days later
            $content['weekEnd'] = $rangeEnd = date("Y-m-d H:i:s", strtotime($rangeStart . " +7 day"));

            //convert to GMT
            $rangeStart =  convert_to_gmt_aprax($rangeStart);
            $rangeEnd =  convert_to_gmt_aprax($rangeEnd);

            $sqlWeekNums = "SELECT bag.bagNumber, orderItem.order_id, orderItem.supplier_id, route_id, wfLog.weightOutTime as ordDate, orderItem.qty, product.name, productCategory.module_id, customer.firstName, orderItemID, product.productCategory_id, customer.lastName, orders.customer_id, location.serviceType, locker.lockerName, orders.notes as busNotes,
                                    (select cost from product_supplier where product_supplier.supplier_id = orderItem.supplier_id
                                        and product_supplier.product_process_id = orderItem.product_process_id) as cost,
                                    (select minQty as minCost from product_supplier where product_supplier.supplier_id = orderItem.supplier_id
                                        and product_supplier.product_process_id = orderItem.product_process_id) as minCost,
                                        wfLog.*
                                    FROM wfLog
                                    join orderItem on orderItem.order_id = wfLog.order_id
                                    inner join product_process on orderItem.product_process_id = product_processID
                                    join product on productID = product_process.product_id
                                    JOIN productCategory ON product.productCategory_id = productCategory.productCategoryID
                                    inner join orders on orderItem.order_id = orderID
                                    inner join locker on lockerID = orders.locker_id
                                    inner join location on locationID = locker.location_id
                                    inner join bag on bagID = orders.bag_id
                                    left join customer on customerID = orders.customer_id
                                    where wfLog.weightOutTime > ?
                                    and wfLog.weightOutTime < ?
                                    and supplier_id = ?
                                    order by ordDate, bagNumber";
            $query = $this->reporting_database->query($sqlWeekNums, array($rangeStart, $rangeEnd, $supplier));
            $weeklyData = $query->result();

            foreach ($weeklyData as $w) {
                //the cutoff time is 10AM so any orders before 10AM go to the prior day
                if (local_date($this->business->timezone, $w->ordDate, 'H') <= 10) {
                    $today = local_date($this->business->timezone, $w->ordDate ."-1 day", 'Y-m-d');
                } else {
                    $today = local_date($this->business->timezone, $w->ordDate, 'Y-m-d');
                }

                $content['week'][$today]['bags'][$w->bagNumber] = 1;
                //if(stristr($w->name, 'Wash and Fold'))
                if ($w->productCategory_id == $wfCat) {
                    $content['week'][$today]['qtyTotal'] += $w->qty;
                    $content['sum']['qtyTotal'] += $w->qty;
                }

                if ($w->minCost > ($w->qty * $w->cost)) {
                    $content['week'][$today]['extCost'] += $w->minCost;
                    $content['sum']['extCost'] += $w->minCost;
                } else {
                    $content['week'][$today]['extCost'] += ($w->qty * $w->cost);
                    $content['sum']['extCost'] += ($w->qty * $w->cost);
                }
                $content['sum']['bags'][$w->order_id] = 1;
            }
        }

        $this->template->write('page_title', 'WF Supplier Production', true);
        $this->renderAdmin('wf_supplier_production', $content);
    }

    public function production_report()
    {
        $this->load->library('timecards');

        //get suppliers
        $sqlSuppliers = "SELECT companyName, supplierID FROM supplier
                        join business on businessID = supplierBusiness_id
                        where supplier.business_id = $this->business_id";
        $query = $this->reporting_database->query($sqlSuppliers);
        $content['suppliers'] = $query->result();

        //get modules
        $this->load->model('module_model');
        $content['modules'] = $this->module_model->get();

        ($_POST['selectedDate'] ? $selectDayz = $_POST['selectedDate'] : $selectDayz = date("Y-m-d"));
        ($_POST['interval'] ? $interval = $_POST['interval'] : $interval = 15);
        $content['interval'] = $interval;
        $content['selectedServices'] = array();

        if ($_POST['update']) {
            foreach ($_POST['note'] as $key => $val) {
                $insert1 = "replace into dayNotes (day, note, business_id)
                                                    values('$key', '".addslashes($val)."', '$this->business_id')";
                $query = $this->db->query($insert1);
            }
        }

        //set defaults if nothing selected
        if (!$_POST['modules']) {
            $_POST['modules'][0] = 2;
            $_POST['modules'][1] = 3;
            $_POST['supplier'] = '%';
            $_POST['byRoute'] = 1;
            $_POST['status'] = 'inv';
        }

        if ($_POST['modules']) {
            foreach ($_POST['modules'] as $s) {
                $modules .= $s.',';
            }
            $modules = rtrim($modules, ',');
            $content['selectedServices'] = explode(",", $modules);

            //determine what the cutoff is going to be
            if ($_POST['status'] == 'inv') {
                $status = 'orderItem.updated';
            } else {
                $status = 'orders.dateCreated';
            }

            if (isset($_POST['selectedDate'])) {
                $dateTo = date("Y-m-d 00:00:00", strtotime($_POST['selectedDate'] ." +1 day"));
                $endTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateTo), $this->business->timezone));
            } else {
                $dateTo = date("Y-m-d 00:00:00", strtotime("+1 day"));
                $endTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateTo), $this->business->timezone));
            }
            $dateFrom = date("Y-m-d 00:00:00", strtotime("$endTime -$interval day"));
            $startTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateFrom), $this->business->timezone));

            //show a detailed report just for that route or day
            $sqlAllItems = "SELECT orderItem.order_id, route_id, process.name as process, product.name as product, orderItem.supplier_id, DATE_FORMAT(convert_tz(".$status.", 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') as dateCreated, orderItem.qty, module.name as service
                    FROM orderItem
                    inner join `orders` on orderID = orderItem.order_id
                    join locker on lockerID = orders.locker_id
                    join location on locationID = locker.location_id
                    join product_process on product_processID = orderItem.product_process_id
                    left join process on processID = product_process.process_id
                    join product on productID = product_process.product_id
                    join productCategory ON product.productCategory_id = productCategory.productCategoryID
                       join module on moduleID = productCategory.module_id
                    where $status <= '$endTime'
                    and $status >= '$startTime'
                    and location.business_id in ('$this->business_id')
                    and moduleID in ($modules)
                    and orderItem.supplier_id like '$_POST[supplier]'
                    and productType <> 'preference'
                    order by ".$status." desc, route_id, orderID";

            $query = $this->reporting_database->query($sqlAllItems);
            $allItems = $query->result();

            foreach ($allItems as $a) {
                $content['allItems'][$a->dateCreated][$a->service][$a->process]['qty'] += $a->qty;
                $content['allItems'][$a->dateCreated]['total'] += $a->qty;
                $content['services'][$a->service][$a->process] = 1;
                $content['routes'][$a->dateCreated][$a->route_id] += $a->qty;
                $content['routeList'][$a->route_id] = 1;
            }

            $sqlGetNotes = "select * from dayNotes
                    where DATE_SUB('$selectDayz',INTERVAL $interval DAY) <= day
                    and day <= DATE_ADD('$selectDayz',INTERVAL 1 DAY)
                    and business_id = $this->business_id";
            $query = $this->reporting_database->query($sqlGetNotes);
            $getNotes = $query->result();
            foreach ($getNotes as $g) {
                $content['notes'][$g->day] = $g->note;
            }
            if ($content['routeList']) {
                ksort($content['routeList']);
            }
        }

        //if timecards are on get productivity by role
        if (get_business_meta($this->business_id, 'timecard_onOff') == 1) {
            for ($i = 0; $i <= $interval; $i++) {
                $thisDay = date("Y-m-d", strtotime($selectDayz ." -$i days"));
                $content['roleHours'][$thisDay] = $roles =  $this->timecards->dailyHoursByRole($thisDay);
            }
            //get a list of all the roles
            if ($roles) {
                foreach ($roles as $key => $value) {
                    $content['roles'][] = $key;
                }
            }
        }

        $content['selectedDate'] = $selectDayz;
        $this->template->write('page_title', 'Production Report', true);

        $this->renderAdmin('production_report', $content);
    }

    public function daily_revenue()
    {
        $this->load->library('timecards');
        $content['business'] = new Business($this->business_id);
        if ($this->input->post("report_cut_off_time")) {
            $content['cutOff'] = $cutOff = $this->input->post("report_cut_off_time");
        } else {
            $content['cutOff'] = $cutOff =  $content['business']->default_report_cut_off_time;
        }

        if (strtotime($this->input->post("dateFrom")) > strtotime($this->input->post("dateTo"))) {
            $dateFrom = $this->input->post("dateTo");
            $dateTo = $this->input->post("dateFrom");
        } else {
            $dateFrom = $this->input->post("dateFrom");
            $dateTo = $this->input->post("dateTo");
        }

        if (!$_POST) {
            $dateFrom = date("Y-m-d", strtotime("-8 days"));
            $dateTo = date("Y-m-d");
            $selectedLocation = '%';
        } else {
            $selectedLocation = $this->input->post("selectedLocation");
        }

        $content['dateFrom'] = $dateFrom;
        $content['dateTo'] = $dateTo;
        $dateFrom .= ' '. $cutOff;
        $dateTo .= ' '. $cutOff;
        $startDate = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateTo), $this->business->timezone));
        $endDate = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateFrom), $this->business->timezone));

        $getOrderItems = "SELECT *, orders.dateCreated FROM orderItem
            join orders on orderID = order_id
            join locker on lockerID = locker_id
            where orders.business_id = $this->business_id
            and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') <= '".$startDate."'
            and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') >= '".$endDate."'
            and location_id like '".$selectedLocation."'
            order by orders.dateCreated desc";
//            echo $getOrderItems;
        $query = $this->reporting_database->query($getOrderItems);
        $orderItems = $query->result();
        foreach ($orderItems as $i) {
            $thisDate = local_date($this->business->timezone, $i->dateCreated, 'Y-m-d');
            @$content['order'][$thisDate][$i->orderID] += $i->unitPrice * $i->qty;
            @$content['gross'][$thisDate] += $i->unitPrice * $i->qty;
            @$content['cost'][$thisDate] += $i->qty * $i->cost;
        }
        //echo $getOrderItems;
        $getDiscounts = "SELECT *, orders.dateCreated FROM orderCharge
            join orders on orderID = order_id
            join locker on lockerID = locker_id
            where orders.business_id = $this->business_id
            and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') <= '".$startDate."'
            and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') >= '".$endDate."'
            and location_id like '".$selectedLocation."'
            order by orderCharge.updated";
        $query = $this->reporting_database->query($getDiscounts);
        $discounts = $query->result();
        foreach ($discounts as $d) {
            $thisDate = local_date($this->business->timezone, $d->dateCreated, 'Y-m-d');
            @$content['order'][$thisDate][$d->orderID] += $d->chargeAmount;
            @$content['discounts'][$thisDate] += $d->chargeAmount;
            @$days[$thisDate] = 1;
        }
        //if timecards are on get productivity by role
        if (get_business_meta($this->business_id, 'timecard_onOff') == 1) {
            foreach ($days as $day => $value) {
                $hours = $this->timecards->dailyHours($day);
                foreach ($hours as $hour) {
                    if ($hour['reg'] > 480) {
                        $content['reg'][$day] += number_format(480 / 60, 1, '.', '');
                        $content['ot'][$day] += number_format(($hour['reg'] - 480) / 60, 1, '.', '');
                    } else {
                        $content['reg'][$day] += number_format($hour['reg'] / 60, 1, '.', '');
                    }
                }
            }
        }

        //$content['locations'] = \App\Libraries\DroplockerObjects\Location::search_aprax(array("business_id" => $this->business_id, "status" => "active"), true, "address");
        $this->load->model('location_model');
        $options['business_id'] = $this->business_id;
        $options['status'] = "active";
        $options['sort'] = "address";
        $content['locations'] = $this->location_model->get($options);

        @$content['interval'] = $interval;
        $this->template->write('page_title', 'Daily Revenue', true);
        $this->renderAdmin('daily_revenue', $content);
    }

    public function cc_deposits()
    {
        if (isset($_POST['selectedDateStart'])) {
            $startDate = $this->input->post("selectedDateStart") . ' '.$this->input->post("cutOffStart");
            update_business_meta($this->business->businessID, 'supplier_manifest_cutOff', $this->input->post("cutOffStart"));
        } else {
            $startDate = date("Y-m-d ".get_business_meta($this->business->businessID, 'supplier_manifest_cutOff'), strtotime("-5 days"));
        }

        if (isset($_POST['selectedDateEnd'])) {
            $endDate = $this->input->post("selectedDateEnd") . ' '.$this->input->post("cutOffEnd");
        } else {
            $endDate = date("Y-m-d ".get_business_meta($this->business->businessID, 'supplier_manifest_cutOff'));
        }

        $content['selectedDateStart'] = date("Y-m-d", strtotime($startDate));
        $content['selectedDateEnd'] = date("Y-m-d", strtotime($endDate));
        $content['cutOffStart'] = date("H:i:s", strtotime($startDate));
        $content['cutOffEnd'] = date("H:i:s", strtotime($endDate));
        isset($_POST['byDate']) ? $content['byDate'] = $byDate = $this->input->post("byDate") : $content['byDate'] = $byDate = 'day';

        $getCharges = "SELECT transaction.*, customer.firstName, customer.lastName, customerID, orderID, location.address, locker.lockerName, location.custom1, locationID
            FROM transaction
            JOIN orders on orderID = order_id
            JOIN customer on customerID = orders.customer_id
            JOIN locker on lockerID = locker_id
            JOIN location on locker.location_id = locationID
            WHERE transaction.business_id = $this->business_id
            AND date_format(convert_tz(transaction.updated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') >= '".$startDate."'
            AND date_format(convert_tz(transaction.updated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') <= '".$endDate."'
            AND resultCode in (0,1)
            ORDER BY transaction.updated desc";
        $query = $this->reporting_database->query($getCharges);
        $charges= $query->result();
        if ($byDate == 'day') {
            foreach ($charges as $charge) {
                $thisDate = convert_from_gmt_aprax($charge->updated, "Y-m-d");
                if ($this->input->post("details") == 1) {
                    $order = new Order($charge->orderID);
                    $customer = new Customer($order->customer_id);
                    $type = $order->getOrderType();

                    switch ($charge->processor) {
                        case 'verisign':
                            $link = '<a href="https://manager.paypal.com/searchTranx.do?subaction=transDetails&timezone=U.S.%20Pacific&reportName=SearchTransaction&id='.$charge->pnref.'">'. $charge->pnref.'</a>';
                            break;
                        case 'authorizenet':
                            $link = '<a href="https://account.authorize.net/UI/themes/anet/transaction/transactiondetail.aspx?transID='.$charge->pnref.'">'. $charge->pnref.'</a>';
                            break;
                        case 'paypro':
                            $link = '<a href="https://businessview.paygateway.com">'. $charge->pnref.'</a>';
                            break;
                        case 'paypalprocessor':
                            $link = '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_view-a-trans&id=' . $charge->pnref.'">'. $charge->pnref.'</a>';
                            break;
                        default:
                            $line = "Unknown Processor";
                    }

                    $content['charge'][$thisDate][$charge->orderID]['pnref'] = $link;
                    $content['charge'][$thisDate][$charge->orderID]['gross'] += $order->get_gross_total();
                    $content['charge'][$thisDate][$charge->orderID]['discounts'] += $order->get_discount_total();
                    $content['charge'][$thisDate][$charge->orderID]['net'] += $order->get_gross_total() + $order->get_discount_total();
                    $content['charge'][$thisDate][$charge->orderID]['typeCost'][$type] += $order->get_cost_total();
                    $content['charge'][$thisDate][$charge->orderID]['cost'] += $order->get_cost_total();
                    $content['charge'][$thisDate][$charge->orderID]['profit'] += $order->get_gross_total() + $order->get_discount_total() - $order->get_cost_total();
                    $content['charge'][$thisDate][$charge->orderID]['custID'] = $charge->customerID;
                    $content['charge'][$thisDate][$charge->orderID]['customer'] = $charge->firstName .' '.$charge->lastName;
                    $content['charge'][$thisDate][$charge->orderID]['location'] = $charge->address;
                    $content['charge'][$thisDate][$charge->orderID]['locker'] = $charge->lockerName;
                    $content['charge'][$thisDate][$charge->orderID]['processor'] = $charge->processor;
                    $content['charge'][$thisDate][$charge->orderID]['custom1'] = $charge->custom1;

                    $content['total'][$thisDate]['gross'] += $order->get_gross_total();
                    $content['total'][$thisDate]['discounts'] += $order->get_discount_total();
                    $content['total'][$thisDate]['net'] += $order->get_gross_total() + $order->get_discount_total();
                    $content['total'][$thisDate]['cost'][$type] += $order->get_cost_total();
                    $content['total'][$thisDate]['profit'] += $order->get_gross_total() + $order->get_discount_total() - $order->get_cost_total();
                }
                $content['charge'][$thisDate][$charge->orderID]['charged'] += $charge->amount;
                $content['total'][$thisDate]['charged'] += $charge->amount;
                $content['types'][$type] = 1;
            }
        } else {
            foreach ($charges as $charge) {
                $thisDate = convert_from_gmt_aprax($charge->updated, "Y-m-d");
                if ($this->input->post("details") == 1) {
                    $order = new Order($charge->orderID);
                    $customer = new Customer($order->customer_id);
                    $type = $order->getOrderType();

                    @$content['charge'][$charge->address][$charge->orderID]['chargeDate'] = $thisDate;
                    @$content['charge'][$charge->address][$charge->orderID]['gross'] += $order->get_gross_total();
                    @$content['charge'][$charge->address][$charge->orderID]['discounts'] += $order->get_discount_total();
                    @$content['charge'][$charge->address][$charge->orderID]['net'] += $order->get_gross_total() + $order->get_discount_total();
                    @$content['charge'][$charge->address][$charge->orderID]['typeCost'][$type] += $order->get_cost_total();
                    @$content['charge'][$charge->address][$charge->orderID]['cost'] += $order->get_cost_total();
                    @$content['charge'][$charge->address][$charge->orderID]['profit'] += $order->get_gross_total() + $order->get_discount_total() - $order->get_cost_total();
                    @$content['charge'][$charge->address][$charge->orderID]['custID'] = $charge->customerID;
                    @$content['charge'][$charge->address][$charge->orderID]['customer'] = $charge->firstName .' '.$charge->lastName;
                    @$content['charge'][$charge->address][$charge->orderID]['location'] = $charge->address;
                    @$content['charge'][$charge->address][$charge->orderID]['locker'] = $charge->lockerName;
                    @$content['charge'][$charge->address][$charge->orderID]['processor'] = $charge->processor;
                    @$content['charge'][$charge->address][$charge->orderID]['custom1'] = $charge->custom1;
                    switch ($charge->processor) {
                    case 'verisign':
                        $link = '<a href="https://manager.paypal.com/searchTranx.do?subaction=transDetails&timezone=U.S.%20Pacific&reportName=SearchTransaction&id='.$charge->pnref.'">'. $charge->pnref.'</a>';
                        break;
                    case 'authorizenet':
                        $link = '<a href="https://account.authorize.net/UI/themes/anet/transaction/transactiondetail.aspx?transID='.$charge->pnref.'">'. $charge->pnref.'</a>';
                        break;
                    case 'paypro':
                        $link = '<a href="https://businessview.paygateway.com">'. $charge->pnref.'</a>';
                        break;
                    case 'paypalprocessor':
                        $link = '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_view-a-trans&id=' . $charge->pnref.'">'. $charge->pnref.'</a>';
                        break;
                    }

                    @$content['charge'][$charge->address][$charge->orderID]['pnref'] = $link;

                    @$content['total'][$charge->address]['gross'] += $order->get_gross_total();
                    @$content['total'][$charge->address]['discounts'] += $order->get_discount_total();
                    @$content['total'][$charge->address]['net'] += $order->get_gross_total() + $order->get_discount_total();
                    @$content['total'][$charge->address]['cost'][$type] += $order->get_cost_total();
                    @$content['total'][$charge->address]['profit'] += $order->get_gross_total() + $order->get_discount_total() - $order->get_cost_total();
                }
                @$content['total'][$charge->address]['custom1'] = $charge->custom1;
                @$content['charge'][$charge->address][$charge->orderID]['charged'] += $charge->amount;
                @$content['total'][$charge->address]['charged'] += $charge->amount;
                @$content['types'][$type] = 1;
            }
        }

        @$content['selectedDate'] = $_POST['selectedDate'];
        $this->template->write('page_title', 'Credit Card Deposits', true);
        $this->renderAdmin('cc_deposits', $content);
    }

    public function metalprogetti_history($orderID = null)
    {
        $this->load->model('automat_model');
        $content['orderID'] = $orderID;

        $order = new Order($orderID);
        if ($order->business_id != $this->business_id) {
            set_flash_message('error', "Order ID [{$orderID}] does not belong to your business");
            redirect("/admin/orders");
        }

        //add the bag
        $bag = $order->find('bag_id');

        if (!empty($bag->bagID)) {
            $content['bag'] = $bag;
            $content['item'][$bag->bagNumber]['product'] = 'bag';
            if ($bag->business_id != $this->business_id) {
                throw new Exception('Order bag ID ['.$bag->bagID.'] and logged in business ID ['.$this->business_id.'] mismatch');
            }
        }
        $itemList = "'".$bag->bagNumber."'";

        $sqlGetItems = "select order_id, orderItem.item_id, qty, product.name
            from orderItem
            left join item on itemID = orderItem.item_id
            LEFT JOIN product_process ON product_process.product_processID=orderItem.product_process_id
            left join product on productID = product_process.product_id
            where orderItem.order_id = $orderID
            order by orderItem.updated asc";
        $query = $this->reporting_database->query($sqlGetItems);
        $items = $query->result();

        foreach ($items as $item) {
            //get barcodes for this item
            $sqlGetBarcodes = "select * from barcode where item_id = $item->item_id";
            $query = $this->reporting_database->query($sqlGetBarcodes);
            $barcodes = $query->result();

            foreach ($barcodes as $barcode) {
                $itemList .= ",'".$barcode->barcode."'";
                $content['item'][$barcode->barcode]['product'] = $item->name;
                $content['item'][$barcode->barcode]['item_id'] = $item->item_id;
            }
        }
        $metalData = $this->automat_model->barcodeDetail($itemList, $orderID);

        $content['info'] = array();
        foreach ($metalData as $m) {
            //don't show logs of the data being loaded on the MP
            if ($m->slot == '' and $m->fileName != null) {
                $content['info'][$m->garcode][$m->fileDate]['slot'] = $m->slot;
                $content['info'][$m->garcode][$m->fileDate]['outPos'] = $m->outPos;
                $content['info'][$m->garcode][$m->fileDate]['name'] = 'Data Sent to Conveyor';
            } else {
                $content['info'][$m->garcode][$m->fileDate]['slot'] = $m->slot;
                $content['info'][$m->garcode][$m->fileDate]['outPos'] = $m->outPos;
                $content['info'][$m->garcode][$m->fileDate]['name'] = $m->gardesc;
            }
        }

        $this->template->write('page_title', 'Assembly - '.$orderID, true);
        $this->renderAdmin('metalprogetti_history', $content);
    }

    public function employee_productivity()
    {
        $this->load->library('timecards');
        $this->load->model('employee_model');

        if ($_GET) {
            foreach ($_GET as $key => $value) {
                $_POST[$key] = $_GET[$key];
            }
        }

        if (!$_POST['daysToShow']) {
            $_POST['daysToShow'] = 12;
        }
        $content['selectedRole'] = $content['roles'] = $this->employee_model->get_roles();
        if ($_POST['role'] and $_POST['role'] != '%') {
            $content['selectedRole'] = array();
            $getRole = "select * from aclroles where name = '{$_POST['role']}' and business_id = $this->business_id";
            $query = $this->reporting_database->query($getRole);
            $role = $query->result();
            $content['selectedRole'][0]->name = $role[0]->name;
        }

        //if a specific day is passed, only show that day
        if ($_POST['day']) {
            $_POST['daysToShow'] = 0;
            $tempArray = $this->timecards->dailyHours($_POST['day']);
            $startDay = date("Y-m-d", strtotime($_POST['day']. "+1 day"));
            $endDay = date("Y-m-d", strtotime($_POST['day']));
            $content['firstDay'] = $endDay;
        } else {
            $startDay = date("Y-m-d");
            $endDay = date("Y-m-d", strtotime("-". $_POST['daysToShow']." days"));
            $content['firstDay'] = $startDay;
        }

        //get the employee hours
        for ($i=0; $i <= $_POST['daysToShow']; $i++) {
            $dayToShow = date("Y-m-d", strtotime($content['firstDay']."-$i days"));
            $tempArray = $this->timecards->dailyHoursByRole($dayToShow);
            // var_e($tempArray);
            if ($tempArray) {
                foreach ($tempArray as $t) {
                    $content['empHours'][$dayToShow] = $tempArray;
                }
            }
        }

        //here is a query for total pcs of dry cleaning.    This breaks out both laundry and dry cleaning.  Show this detail on the report.  Make the wash Location and date dynamic.
        $sqlQtyByService = "SELECT sum(orderItem.qty) as pcs, orders.business_id, DATE_FORMAT(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') as ordDate, process.name
                    FROM orderItem
                    inner join `orders` on orderID = orderItem.order_id
                    join locker on lockerID = orders.locker_id
                    join location on locationID = locker.location_id
                    join product_process on product_processID = orderItem.product_process_id
                    join process on processID = product_process.process_id
                    join product on productID = product_process.product_id
                    join productCategory ON product.productCategory_id = productCategory.productCategoryID
                    join module on moduleID = productCategory.module_id
                    where convert_tz(orderItem.updated, 'GMT', '".$this->business->timezone."') <= '$startDay'
                    and convert_tz(orderItem.updated, 'GMT', '".$this->business->timezone."') >= '$endDay'
                    and location.business_id in ('".$this->business_id."') and moduleID in (2,3) and orderItem.supplier_id like '%'
                    and productType <> 'preference'
                    group by ordDate, orders.business_id, process.name";
        $query = $this->reporting_database->query($sqlQtyByService);
        $qtyByService = $query->result();
        foreach ($qtyByService as $q) {
            $content['qtyByService'][$q->ordDate][$q->name] = $q->pcs;
        }

        $this->template->write('page_title', 'Employee Productivity Report', true);
        $this->renderAdmin('employee_productivity', $content);
    }
    /**
     *
     */
    public function sales_commission()
    {
        //get last month by default
        if ($_POST['fromDate']) {
            $content['fromDate'] = $_POST['fromDate'];
            $content['toDate'] = $_POST['toDate'];
        } else {
            $content['fromDate'] = date("m/d/y", mktime(0, 0, 0, date("m") -1, 1, date("y")));
            $content['toDate'] = date("m/d/y", mktime(0, 0, 0, date("m"), 0, date("y")));
        }
        $getEmployees = "select employeeID, firstName, lastName, commission
            from employee
            join business_employee on employee_id = employeeID
            where commission <> '0'
            and business_employee.business_id = $this->business_id";
        $q = $this->reporting_database->query($getEmployees);
        $content['employees'] = $employees = $q->result();

        if ($_POST['fromDate']) {
            //get this employees info
            foreach ($employees as $e) {
                if ($e->employeeID == $_POST['employee']) {
                    $content['thisEmployee'] = $thisEmployee = $e;
                }
            }

            //get Named Accounts
            $getNamedAccounts = "select * from customer where owner_id = $_POST[employee]";
            $q = $this->reporting_database->query($getNamedAccounts);
            $content['namedAccount'] = $namedAccount = $q->result();

            $fromDate = date("y-m-d", strtotime($_POST['fromDate']));
            $toDate = date("y-m-d", strtotime($_POST['toDate']));
            foreach ($namedAccount as $n) {
                //get all the orders for this customer in this timeframe.
                $getAccountOrders = "SELECT *
                    FROM orders
                    where convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') >= '$fromDate'
                    and convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') <= '$toDate'
                    and customer_id = $n->customerID
                    AND business_id = {$this->business_id}
                    order by dateCreated";
                $q = $this->reporting_database->query($getAccountOrders);
                $accountOrders = $q->result();

                $custTot = 0;
                foreach ($accountOrders as $a) {
                    $ordTot = 0;
                    $ordTot = $a->closedGross + $a->closedDisc;
                    if ($ordTot <> 0) {
                        $content['orders'][$n->customerID][$a->orderID] = $a;
                        $content['custTot'][$n->customerID] += $ordTot;
                    }
                }

                $content['commission_total'][$n->customerID] += $content['custTot'][$n->customerID] * $n->commission_rate * 0.01;
                $content['commTot'] += $content['commission_total'][$n->customerID];
            }
            $commission_orders_settings = Commission_Orders_Setting::search_aprax(array("employee_id" => $thisEmployee->employeeID));
            if (!empty($commission_orders_settings)) {

                //get all orders placed in this time frame
                $getOrders = "SELECT location.address as lAddress, orderID, customer_id, customer.firstName as cFirstName, customer.lastName as cLastName, orders.dateCreated, customer.referredBy, rCust.firstName as rCustFirstName, rCust.lastName as rCustLastName
                    FROM orders
                    join customer on customerID = orders.customer_id
                    join locker on orders.locker_id = lockerID
                    join location on locker.location_id = locationID
                    left join customer as rCust on rCust.customerID = customer.referredBy
                    where convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') >= '$fromDate'
                    and convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') <= '$toDate'
                    AND orders.business_id = {$this->business_id}
                    and bag_id > 1
                    order by dateCreated";
                $q = $this->reporting_database->query($getOrders);
                $allOrders = $q->result();
                $content['commission'] = 0;
                foreach ($allOrders as $index => $o) {
                    $getCount = "select count(orderID) as ordCount from orders
                        where customer_id = $o->customer_id
                        and orderID <= '$o->orderID'
                        AND business_id = {$this->business_id}
                        and bag_id > 1";
                    $q = $this->reporting_database->query($getCount);
                    $ordCount = $q->result();

                    if ($ordCount[0]->ordCount <= 10) {
                        $commission_orders_settings = Commission_Orders_Setting::search_aprax(array("employee_id" => $thisEmployee->employeeID, "index" => $ordCount[0]->ordCount-1));
                        if (empty($commission_orders_settings)) {
                            $commission = 0;
                        } else {
                            $commission_orders_setting = $commission_orders_settings[0];
                            if ($commission_orders_setting->amount > 0) {
                                $commission = $commission_orders_settings[0]->amount;
                                $content['allOrders'][$o->orderID] = $o;
                                $content['reffered'][$o->orderID] = getPersonName($line, true, 'cFirstName', 'cLastName');
                                $content['ordCount'][$o->orderID] = $ordCount[0]->ordCount;
                                $content['commissionAmt'][$o->orderID] = $commission;
                            } else {
                                continue;
                            }
                        }
                    } else {
                        $commission = 0;
                    }
                    $content['commission'] += $commission;
                }
            }
        }

        $this->template->write('page_title', 'Sales Commission Report', true);
        $this->renderAdmin('sales_commission', $content);
    }

    /**
     *
     */
    public function update_salesperson_commission_settings()
    {
        if (!$this->input->post("employeeID")) {
            throw new \InvalidArgumentException("'employeeID' must be passed as a POST parameter.");
        }
        $employeeID = $this->input->post("employeeID");
        if (!is_numeric($employeeID)) {
            throw new \InvalidArgumentException("'employeeID' must be numeric.");
        }
        try {
            $employee = new Employee($employeeID, true);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Employee ID '$employeeID' not found");
            redirect($this->agent->referrer());
        }

        $order_settings = $this->input->post("orders");
        foreach ($order_settings as $index => $amount) {
            $settings = Commission_Orders_Setting::search_aprax(array("index" => $index, "employee_id" => $employee->employeeID));
            if (empty($settings)) {
                $setting = new Commission_Orders_Setting();
                $setting->employee_id = $employee->{Employee::$primary_key};
                $setting->index = $index;
                if (empty($amount)) {
                    $setting->amount = 0.00;
                } else {
                    $setting->amount = $amount;
                }
                $setting->save();
            } else {
                $setting = $settings[0];
                $setting->amount = $amount;
                $setting->save();
            }
        }


        set_flash_message("success", "Updated Salesperson Order Commission Settings");
        redirect($this->agent->referrer());
    }
    /**
     *
     */
    public function display_sales_commission_report()
    {
        $start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");

        try {
            $start_date = new DateTime($start_date);
        } catch (Exception $e) {
            set_flash_message("error", "The start date is not a valid format.");
            redirect("/admin/reports/display_sales_commission_report_form");
        }
        try {
            $end_date = new DateTime($end_date);
        } catch (Exception $e) {
            set_flash_message("error", "THe end date is not a valid format.");
            redirect("/admin/reports/display_sales_commission_report_form");
        }

        if (!$this->input->post("employeeID")) {
            throw new \InvalidArgumentException("'employeeID' must be numeric");
            redirect("/admin/reports/display_sales_commission_report_form");
        }
        $employeeID = $this->input->post("employeeID");
        try {
            $employee = new Employee($employeeID, true);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Employee ID '$employeeID' not found");
            redirect("/admin/reports/display_sales_commission_report_form");
        }
        $start_date->setTimezone(new DateTimeZone("GMT"));
        $end_date->setTimezone(new DateTimeZone("GMT"));
        $this->db->select("customerID");
        $this->db->where("signupDate >", $start_date->format("Y-m-d"));
        $this->db->where("signupDate <", $end_date->format("Y-m-d"));
        $this->db->where("owner_id", $employee->employeeID);
        $get_customer_signups_query = $this->db->get("customer");

        $customer_signups = $get_customer_signups_query->result();
        foreach ($customer_signups as $signup) {
            $content['customer_signups'][] = new Customer($signup->customerID);
        }

        $content['employee'] = $employee;

        $this->db->select("customerID");
        $this->db->join("orders", "customer_id=customerID");
        $this->db->where("dateCreated >=", $start_date->format("Y-m-d"));
        $this->db->where("dateCreated <=", $end_date->format("Y-m-d"));
        $this->db->where("owner_id", $employeeID);

        $this->db->group_by("customerID");
        $get_customers_query = $this->db->get("customer");
        $customers = $get_customers_query->result();

        $content['orders_commission_total'] = 0;
        foreach ($customers as $customer) {
            $this->db->select("orderID");
            $this->db->where("dateCreated >=", $start_date->format("Y-m-d"));
            $this->db->where("dateCreated <=", $end_date->format("Y-m-d"));
            $this->db->order_by("dateCreated");
            $this->db->limit(10);
            $this->db->where("customer_id", $customer->customerID);
            $get_orders_query = $this->db->get("orders");
            $orders = $get_orders_query->result();
            if (empty($orders)) {
                continue;
            } else {
                foreach ($orders as $index => $order) {
                    $commission_orders_settings = Commission_Orders_Setting::search_aprax(array("employee_id" => $employee->employeeID, "index" => $index));
                    if (empty($commission_orders_settings)) {
                        $content['orders_commission_total'] += 0;
                    } else {
                        $commission_orders_setting = $commission_orders_settings[0];
                        $content['orders_commission_total'] += $commission_orders_setting->amount;
                    }

                    $content['customers'][$customer->customerID][] = new Order($order->orderID);
                }
            }
        }

        $this->data['content'] = $this->load->view('admin/reports/display_sales_commission_report', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function property_commission()
    {
        //get locations with commissions
        $getLocations = "select * from location
                                where commissionAddr != ''
                                and commissionFrequency <> 'none'
                                and business_id = $this->business_id
                                order by address;";
        $q = $this->reporting_database->query($getLocations);
        $content['locations'] = $q->result();

        $this->template->write('page_title', 'Property Commissions Report', true);
        $this->renderAdmin('property_commission', $content);
    }


    public function property_commission_print($locationID = null, $numMonths = null)
    {
        if ($numMonths) {
            $content['numMonths'] = $numMonths;
        } else {
            $content['numMonths'] = $numMonths = 6;
        }

        $this->load->model("image_placement_model");
        $invoice_logo_image = $this->image_placement_model->get_by_name("invoice", $this->business_id);
        $signature_image = $this->image_placement_model->get_by_name("signature", $this->business_id);
        $content['invoice_logo_image'] = BUSINESS_IMAGES_DIRECTORY . $invoice_logo_image->filename;
        $content['signature_image'] = BUSINESS_IMAGES_DIRECTORY . $signature_image->filename;

        $getLocation = "select * FROM location WHERE locationID = $locationID AND business_id = $this->business_id";
        $q = $this->reporting_database->query($getLocation);
        $content['location'] = $location = $q->result();
        if ($location) {
            $currQtr = date("m");
            switch ($currQtr) {
            case 1:
                $content['qtr'] = 'the Fourth quarter of '.(date("Y")-1);
                break;
            case 4:
                $content['qtr'] = 'the First quarter of '.date("Y");
                break;
            case 7:
                $content['qtr'] = 'the Second quarter of '.date("Y");
                break;
            case 10:
                $content['qtr'] = 'the Third quarter of '.date("Y");
                break;
            }

            if (strpos($location[0]->commission, '%') >= 1) {
                $content['commissionType'] = $commissionType = "percentage";
                $content['commissionAmt'] = $commissionAmt = subStr($location[0]->commission, 0, strpos($location[0]->commission, '%'));
            } else {
                $content['commissionType'] = $commissionType = "dollar";
                $content['commissionAmt'] = $commissionAmt = $location[0]->commission;
            }

            if ($location[0]->units > 0) {
                $content['numUnits'] = $numUnits = $location[0]->units;
            }

            //get num of cust
            $getNumCust = "select count(distinct `orders`.customer_id) as numCust
            FROM locker
            JOIN `orders` ON `orders`.locker_id = lockeriD
            JOIN orderItem ON orderItem.order_id = orderID
            join customer on customerID = customer_id
            WHERE `orders`.orderStatusOption_id = 10
            AND locker.location_id = $locationID
            AND customer.type <> 'wholesale'";
            $q = $this->reporting_database->query($getNumCust);
            $content['numCust'] = $q->result();

            //num of ord
            $getNumOrd = "select count(distinct orderID) as numOrd
            FROM locker
            JOIN `orders` ON `orders`.locker_id = lockerID
            JOIN orderItem ON orderItem.order_id = orderID
            join customer on customerID = customer_id
            WHERE orders.orderStatusOption_id = 10
            AND locker.location_id = $locationID
            AND customer.type <> 'wholesale'";
            $q = $this->reporting_database->query($getNumOrd);
            $content['numOrd'] = $q->result();


            //utilization - measured as customers who used the service in the last 30 days / number or units
            $getUtilization = "select count(distinct `orders`.customer_id) as numCust
            FROM locker
            JOIN `orders` ON `orders`.locker_id = lockerID
            JOIN orderItem ON orderItem.order_id = orderID
            join customer on customerID = customer_id
            WHERE `orders`.orderStatusOption_id = 10
            AND locker.location_id = $locationID
            AND DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= orders.dateCreated
            AND customer.type <> 'wholesale'";
            $q = $this->reporting_database->query($getUtilization);
            $utilization = $q->result();
            if ($numUnits) {
                $content['utilization'] = round((($utilization[0]->numCust / $numUnits) * 100), 2);
            }

            for ($i = 1; $i <= $numMonths; $i++) {
                $selectMonth = date("Y-n", mktime(0, 0, 0, date("m")-$i, 1, date("Y")));
                $getTotCust = "select count(distinct `orders`.customer_id) as numCust
                FROM locker
                JOIN `orders` ON `orders`.locker_id = lockerID
                JOIN orderItem ON orderItem.order_id = orderID
                join customer on customerID = customer_id
                WHERE `orders`.orderStatusOption_id = 10
                AND locker.location_id = $locationID
                AND concat(year(orders.dateCreated),'-', month(orders.dateCreated))='$selectMonth'
                AND customer.type <> 'wholesale'";
                $q = $this->reporting_database->query($getTotCust);
                $totCust = $q->result();
                foreach ($totCust as $t) {
                    $content['totCust'][$i] = $t->numCust;
                }
            }


            for ($i = 1; $i <= $numMonths; $i++) {
                $selectMonth = date("Y-n", mktime(0, 0, 0, date("m")-$i, 1, date("Y")));
                $getTotOrd = "select count(distinct orderID) as numOrd
                FROM locker
                JOIN `orders` ON `orders`.locker_id = lockerID
                JOIN orderItem ON orderItem.order_id = orderID
                join customer on customerID = customer_id
                WHERE `orders`.orderStatusOption_id = 10
                AND locker.location_id = $locationID
                AND concat(year(orders.dateCreated),'-', month(orders.dateCreated))='$selectMonth'
                AND customer.type <> 'wholesale'";
                $q = $this->reporting_database->query($getTotOrd);
                $totOrd = $q->result();
                foreach ($totOrd as $t) {
                    $content['totOrd'][$i] = $t->numOrd;
                }
            }

            for ($i = 1; $i <= $numMonths; $i++) {
                $selectMonth = date("Y-n", mktime(0, 0, 0, date("m")-$i, 1, date("Y")));
                $getTotAmt = "SELECT sum(orderItem.qty * orderItem.unitPrice) as totAmt
                FROM locker
                JOIN `orders` ON `orders`.locker_id = lockerID
                JOIN orderItem ON orderItem.order_id = orderID
                join customer on customerID = customer_id
                WHERE `orders`.orderStatusOption_id = 10
                AND locker.location_id = $locationID
                AND concat(year(orders.dateCreated),'-', month(orders.dateCreated))='$selectMonth'
                AND customer.type <> 'wholesale'";
                $q = $this->reporting_database->query($getTotAmt);
                $totOrd = $q->result();
                foreach ($totOrd as $t) {
                    $content['totAmt'][$i] = $t->totAmt;
                }
            }

            for ($i = 1; $i <= $numMonths; $i++) {
                $selectMonth = date("Y-n", mktime(0, 0, 0, date("m")-$i, 1, date("Y")));
                $getTotDisc = "SELECT sum(chargeAmount) as totDisc
                FROM locker
                JOIN `orders` ON `orders`.locker_id = lockerID
                JOIN orderCharge ON orderCharge.order_id = orderID
                join customer on customerID = customer_id
                WHERE `orders`.orderStatusOption_id = 10
                AND locker.location_id = $locationID
                AND concat(year(orders.dateCreated),'-', month(orders.dateCreated))='$selectMonth'
                AND customer.type <> 'wholesale'";
                $q = $this->reporting_database->query($getTotDisc);
                $totDisc = $q->result();
                foreach ($totDisc as $t) {
                    $content['totDisc'][$i] = $t->totDisc;
                }
            }
            //figure out commission due
            $commMonths = 3;
            if ($commissionType == "percentage") {
                for ($i = 0; $i <= $commMonths; $i++) {
                    $dollarOrders += $content['totAmt'][$i] + $content['totDisc'][$i];
                }
                $content['commissionDue'] = round($commissionAmt * $dollarOrders / 100, 2);
            } else {
                for ($i = 0; $i <= $commMonths; $i++) {
                    $numOrders += $content['totOrd'][$i];
                }
                $content['commissionDue'] = round($commissionAmt * $numOrders, 2);
            }

            //if this is a corner store
            if ($location[0]->locationType_id == 5) {
                for ($i = 1; $i <= $commMonths; $i++) {
                    $selectMonth = date("Y-n", mktime(0, 0, 0, date("m")-$i, 1, date("Y")));

                    $getAllOrders = "select orderID, `orders`.customer_id, firstName, lastName,email, customer.phone, customer.address1, `orders`.statusDate, `orders`.dateCreated
                            from orders
                            INNER JOIN `customer` ON customerID = `orders`.customer_id
                            INNER JOIN locker ON lockerID = `orders`.locker_id
                            WHERE orderStatusOption_id = '10'
                            AND locker.location_id = $locationID
                            AND customer.type <> 'wholesale'
                            AND concat(year(orders.dateCreated),'-', month(orders.dateCreated))='$selectMonth'
                            ORDER BY dateCreated DESC;";
                    $q = $this->reporting_database->query($getAllOrders);
                    $allOrders = $q->result();

                    $this->load->model('orderitem_model');
                    foreach ($allOrders as $a) {
                        $content['allOrders'][$a->orderID] = $a;
                        $content['totals'][$a->orderID] = $this->orderitem_model->get_item_total($a->orderID);
                        $order = new Order($a->orderID);
                        $content['totals'][$order->orderID]['gross'] = $order->get_gross_total();
                        $content['totals'][$order->orderID]['discounts'] = $order->get_discount_total();
                    }

                    $getAllCust = "select orderID, `orders`.customer_id,firstName, lastName,email, customer.phone, customer.address1, count(distinct orderID) as numOrd,sum(orderItem.qty * orderItem.unitPrice) as ordAmt, max(`orders`.dateCreated) as lastOrder
                        from orderItem
                        INNER JOIN `orders` ON orderItem.order_id = orderID
                        INNER JOIN `customer` ON customerID = `orders`.customer_id
                        INNER JOIN locker ON lockerID = orders.locker_id
                        WHERE orderStatusOption_id = '10'
                        AND locker.location_id = $locationID
                        AND customer.type <> 'wholesale'
                        GROUP BY customer_id
                        ORDER BY lastOrder DESC;";
                    $q = $this->reporting_database->query($getAllCust);
                    $content['allCust'] = $q->result();
                }
            }
        } else {
            echo 'There is no information for this location. You likely typed an incorrect location ID into the url';
            exit;
        }
        $content['business'] = $this->business;

        $this->template->write('page_title', 'Commission for '.$location->address, true);
        $this->load->view('print/commission_report', $content);
    }

    public function assembly_log($automatID = null)
    {
        $content['automatID'] = $automatID;
        if ($automatID) {
            /* TODO: bug checking:
             * if $lookupArm is null,
             * arm = '$lookupArm' is false (NULL = '' is false) and record is not shown
             * it seems to have been designed to always show the selected automat
             *
             * Same for outPos, if it is NULL, the record won't be shown
             *
             * If this is behaviour by design, please remove this comment
             */
            
            
            $getRecord = "select * from automat where automatID = $automatID";
            $q = $this->reporting_database->query($getRecord);
            $record = $q->result();

            $startDate = date("Y-m-d", strtotime($record[0]->fileDate));
            $endDate = date("Y-m-d", strtotime($record[0]->fileDate. "+ 1 day"));
            $lookupArm = $record[0]->arm;

            $getAllRecords = "select automat.*, customer.firstName, customer.lastName from automat
                join `orders` on orderID = automat.order_id
                join customer on customerID = `orders`.customer_id
                where arm = '$lookupArm'
                and fileDate > '$startDate'
                and fileDate < '$endDate'
                and outPos != ''
                and orders.business_id = $this->business_id
                order by fileDate desc;";
            $q = $this->reporting_database->query($getAllRecords);
            $content['allRecords'] = $q->result();
        }
        if ($_POST['getDate']) {
            $startDate = date("Y-m-d", strtotime($_POST['getDate']));
            $endDate = date("Y-m-d", strtotime($_POST['getDate']. "+ 1 day"));

            $getAllRecords = "select automat.*, customer.firstName, customer.lastName from automat
                join `orders` on orderID = automat.order_id
                join customer on customerID = `orders`.customer_id
                and fileDate > '$startDate'
                and fileDate < '$endDate'
                and outPos != ''
                and orders.business_id = $this->business_id
                order by fileDate desc;";
            $q = $this->reporting_database->query($getAllRecords);
            $content['allRecords'] = $q->result();
            //echo $getAllRecords;
        }

        $this->template->write('page_title', 'Assembly Log', true);

        $this->renderAdmin('assembly_log', $content);
    }

    private function get_by_location_content()
    {
        $this->load->model("location_model");
                
        $selectedGroup = "month";
        if ($this->input->post("grouping")) {
            $selectedGroup = $this->input->post("grouping");
        }
        $content['grouping'] = $selectedGroup;

        $content['months'] = array_combine(range(1, 12), range(1, 12));
        $content['years'] = array_combine(range(date('Y'), 2000, -1), range(date('Y'), 2000, -1));

        $time = mktime(0, 0, 0, date('m') - 5, 1, date('Y'));
        $content['end_month'] = date('m');
        $content['end_year'] = date('Y');
        $content['start_month'] = date('m', $time);
        $content['start_year'] = date('Y', $time);

        if ($this->input->post()) {
            $content['start_month'] = $this->input->post('start_month');
            $content['start_year'] = $this->input->post('start_year');
            $content['end_month'] = $this->input->post('end_month');
            $content['end_year'] = $this->input->post('end_year');
        }

        // calculate the number of months
        $numMonths = $content['end_year'] * 12 + $content['end_month']
                    - 12 * $content['start_year'] - $content['start_month'] + 1;

        // days to use in utilization rate per building formula
        $elapsedDays = empty($numMonths)?30:$numMonths*30;

        $increment = 1;
        if ($selectedGroup == 'quarter') {
            $increment = 3;
        } elseif ($selectedGroup == 'year') {
            $increment = 12;
        } elseif ($selectedGroup == 'week') {
            $numMonths = $numMonths * 4;
        }

        $content['increment'] = $increment;
        $content['numMonths'] = $numMonths;
        $content['show_extra'] = $this->input->post("extra");

        $date_y = $content['end_year'];
        $date_m = $content['end_month'];
        $current_time = mktime(0, 0, 0, $content['end_month'], date('d'), $content['end_year']);

        for ($i = 0; $i <= $numMonths -1; $i = $i + $increment) {
            $totComm = 0;

            //show the right heading
            switch ($selectedGroup) {
                case 'year':
                    
                    $time1 = mktime(0, 0, 0, 1, 1, $date_y - ($i/12));
                    $selectYear = date("Y", $time1);
                    $content['headers'][$i] = date("Y", $time1);
                    break;
               case 'quarter':

                    $month = $date_m - $i;
                    $time1 = mktime(0, 0, 0, $month -0, 1, $date_y);
                    $time2 = mktime(0, 0, 0, $month -1, 1, $date_y);
                    $time3 = mktime(0, 0, 0, $month -2, 1, $date_y);

                    $date1 = date("Y-n", $time1);
                    $date2 = date("Y-n", $time2);
                    $date3 = date("Y-n", $time3);

                    $selectQuarter = "'$date1', '$date2', '$date3'";
                    $content['headers'][$i] = date("M 'y", $time1)." to ".date("M 'y", $time3);
                    break;

                case 'week':
                    $end = date("m/d/y", strtotime("this sunday", $current_time));
                    $endUnix = date("Y-m-d 23:59:59", strtotime("this sunday", $current_time));

                    $start = date("m/d/y", strtotime("$end - 6 days"));
                    $startUnix = date("Y-m-d 00:00:00", strtotime("$endUnix - 6 days"));

                    $dayJump = $i * 7;
                    $start = date("m/d/y", strtotime("$start - $dayJump days"));
                    $startUnix = date("Y-m-d 00:00:00", strtotime("$startUnix - $dayJump days"));

                    $end = date("m/d/y", strtotime("$end - $dayJump days"));
                    $endUnix = date("Y-m-d 23:59:59", strtotime("$endUnix - $dayJump days"));

                    $selectWeek = date("YW", strtotime($end));
                    $content['headers'][$i] = $end." - ".$start;
                    break;

                case '4week':
                    $end = date("m/d/y", strtotime("this sunday", $current_time));
                    $start = date("m/d/y", strtotime("$end - 28 days"));

                    $dayJump = $i * 28;
                    $start = date("m/d/y", strtotime("$start - $dayJump days"));
                    $end = date("m/d/y", strtotime("$end - $dayJump days"));

                    $w1 = date("YW", strtotime($end));
                    $w2 = date("YW", strtotime("$end - 7 days"));
                    $w3 = date("YW", strtotime("$end - 14 days"));
                    $w4 = date("YW", strtotime("$end - 21 days"));

                    $selectRange = $w1.','.$w2.','.$w3.','.$w4;
                    $content['headers'][$i] = $end." - ".$start;
                    break;

                case 'month':
                    $time1 = mktime(0, 0, 0, $date_m - $i, 1, $date_y);
                    $selectMonth = date("Y-n", $time1);
                    $content['headers'][$i] = date("M 'y", $time1);
                    break;
            }

            $getFirstCustomers = "select count(customer_id) as firstCust, reportingClass from(
                    SELECT distinct `orders`.customer_id, min(orders.dateCreated) as firstOrder, locationType.name as reportingClass
                    FROM `orders`
                    join locker on lockerID = orders.locker_id
                    join location on locker.location_id = locationID
                    join locationType on locationType_id = locationTypeID
                    where bag_id <> 0
                    and orders.business_id = $this->business_id
                    group by customer_id) t2 ";


            switch ($selectedGroup) {
                case 'year':
                    $getFirstCustomers .= "where year(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."')) = '$selectYear'";
                    break;
                case 'quarter':
                    $getFirstCustomers .= "where concat(year(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."'))) in ($selectQuarter)";
                    break;
                case 'month':
                    $getFirstCustomers .= "where concat(year(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'";
                    break;
                case 'week':
                    $getFirstCustomers .= "where convert_tz(firstOrder, 'GMT', '".$this->business->timezone."') >= '$startUnix' and convert_tz(firstOrder, 'GMT', '".$this->business->timezone."') <= '$endUnix'";
                    break;
                case '4week':
                    $getFirstCustomers .= "where yearweek(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."')) in ($selectRange)";
                    break;
            }
            $getFirstCustomers .= " group by reportingClass";

            $q = $this->reporting_database->query($getFirstCustomers);
            $firstCustomers = $q->result();
            $content['firstCustomers'][$i] = empty($content['firstCustomers'][$i])?0:$content['firstCustomers'][$i];
            foreach ($firstCustomers as $firstCustomer) {
                $content['firstCustomers'][$i] += $firstCustomer->firstCust;
                $content['custFirstCategory'][$firstCustomer->reportingClass][$i] = $firstCustomer->firstCust;
            }

            //find out how many signups there were
            $getSignups = "select count(customerId) as firstCust
                    from customer
                    where business_id = $this->business_id ";

            switch ($selectedGroup) {
                case 'year':
                    $getSignups .= "and year(convert_tz(signupDate, 'GMT', '".$this->business->timezone."')) = '$selectYear'";
                    break;
                case 'quarter':
                    $getSignups .= "and concat(year(convert_tz(signupDate, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(signupDate, 'GMT', '".$this->business->timezone."'))) in ($selectQuarter)";
                    break;
                case 'month':
                    $getSignups .= "and concat(year(convert_tz(signupDate, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(signupDate, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'";
                    break;
                case 'week':
                    $getSignups .= "and (convert_tz(signupDate, 'GMT', '".$this->business->timezone."') >= '$startUnix' AND convert_tz(signupDate, 'GMT', '".$this->business->timezone."') <= '$endUnix')";
                    break;
                case '4week':
                    $getSignups .= "and yearweek(convert_tz(signupDate, 'GMT', '".$this->business->timezone."')) in ($selectRange)";
                    break;
            }

            $q = $this->reporting_database->query($getSignups);
            $signups = $q->result();
            $content['signups'][$i] = $signups[0]->firstCust;

            //find out how many unique customers
            $getUnique = "select count(distinct customer_id) as customers
                        from orders
                        where orders.business_id = $this->business_id
                        and bag_id <> 0 ";

            switch ($selectedGroup) {
                case 'year':
                    $getUnique .= "and year(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')) = '$selectYear'";
                    break;
                case 'quarter':
                    $getUnique .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."'))) in ($selectQuarter)";
                    break;
                case 'month':
                    $getUnique .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'";
                    break;
                case 'week':
                    $getUnique .= "and (convert_tz(dateCreated, 'GMT', '".$this->business->timezone."') >= '$startUnix' and convert_tz(dateCreated, 'GMT', '".$this->business->timezone."') <= '$endUnix')";
                    break;
                case '4week':
                    $getUnique .= "and yearweek(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')) in  ($selectRange)";
                    break;
            }

            $q = $this->reporting_database->query($getUnique);
            $unique = $q->result();
            $content['uniqueCusts'][$i] = $unique[0]->customers;


            //FIXME: hardcoded locationType name, see below

            //main query
            $getAllData = "SELECT location.address, route_id, locationID, companyName, count(distinct `orders`.customer_id) as numCust, sum(orderItem.qty * orderItem.unitPrice) as totOrds, count(distinct orderID) as numOrds, location.units, serviceType, locationType.name as serviceMethod, locationType.name as reportingClass, lowTarget, highTarget, custom1
                    FROM orders
                    JOIN locker on lockerID = orders.locker_id
                    JOIN location on locationID = locker.location_id
                    JOIN orderItem on orderItem.order_id = orderID
                    join locationType on locationTypeID = locationType_id ";

            switch ($selectedGroup) {
                case 'year':
                    $getAllData .= "where year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')) = '$selectYear'";
                    break;
                case 'quarter':
                    $getAllData .= "where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'))) in ($selectQuarter)";
                    break;
                case 'month':
                    $getAllData .= "where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'";
                    break;
                case 'week':
                    $getAllData .= "where (convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') >= '$startUnix'  and convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') <= '$endUnix') ";
                    break;
                case '4week':
                    $getAllData .= "where yearweek(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')) in ($selectRange)";
                    break;
            }

            $getAllData .= "
            and orders.business_id = $this->business_id
            and bag_id <> 0
            group by location.locationid
            order by totOrds desc";

            $q = $this->reporting_database->query($getAllData);
            $allData = $q->result();

            foreach ($allData as $a) {
                if ($this->input->post('locList')) {
                    if ($this->input->post('locList') != $a->reportingClass) {
                        continue;
                    }
                }

                $content['title'][$a->locationID] = '<a href="/admin/locations/display_update_form/'.$a->locationID.'" target="_blank">'.substr($a->companyName, 0, 35).'</a>';
                $content['address'][$a->locationID] = '<a href="/admin/locations/display_update_form/'.$a->locationID.'" target="_blank">'.substr($a->address, 0, 20).'</a>';
                $content['units'][$a->locationID] = $a->units;
                $content['route'][$a->locationID] = $a->route_id;
                
                

                //get locker count
                $totLockers = 0;
                $getLockerCount = "SELECT location_id, lockerLockType.name as type, count(lockerLockType_id) as numLock
                    FROM locker
                    join lockerLockType on lockerLockTypeID = lockerLockType_id
                    join location on locationID = location_id
                    WHERE location_id = $a->locationID
                    and location.business_id = $this->business_id
                    and lockerStatus_id = 1
                    group by location_id, lockerLockType_id";
                
                if (empty($content['lockers'][$a->locationID])) {
                    $q = $this->reporting_database->query($getLockerCount);
                    $lockerCount = $q->result();
                    foreach ($lockerCount as $lc) {
                        $content['lockers'][$a->locationID][$lc->type] = $lc->numLock;
                        $types = array('WF', 'DC', 'Combo', 'Unknown', 'Electronic');
                        if (in_array($lc->type, $types)) {
                            $content['totLockers'] = empty($content['totLockers'])?0:$content['totLockers'];
                            $content['totLockers'] += $lc->numLock;
                            $totLockers = $content['totLockers'];
                        }
                    }
                }

                $content['repClass'][$a->locationID] = $a->reportingClass;
                $content['serviceType'][$a->locationID] = $a->serviceType;
                if ($a->lowTarget > 0 or $a->highTarget > 0) {
                    $content['target'][$a->locationID] = number_format($a->lowTarget / 1000, 1, '.', '').'K/'.number_format($a->highTarget / 1000, 1, '.', '').'K';
                } else {
                    $content['target'][$a->locationID] = $a->custom1;
                }
                
                //Store data for utilization rate per location
                $content['utilization'][$a->locationID]['orders'][$i] = $a->numOrds;
                $lockersCount = empty($content['lockers'][$a->locationID])?0:array_sum($content['lockers'][$a->locationID]);
                if (in_array($a->serviceMethod, array('Lockers', 'Kiosk'))) {
                    $content['utilization'][$a->locationID]['lockers'] = $lockersCount;
                }
                $content['utilization'][$a->locationID]['customers'][$i] = $a->numCust;
                $content['utilization'][$a->locationID]['units'] = $a->units;

                //store the monthly revenue per location
                $content['location'][$a->locationID][$i] = $a->totOrds;
                $content['numOrds'][$a->locationID][$i] = $a->numOrds;
                $content['gross'][$i] = empty($content['gross'][$i])?0:$content['gross'][$i];
                $content['gross'][$i] = $content['gross'][$i] + $a->totOrds;
                $content['tax'][$i] = empty($content['tax'][$i])?0:$content['tax'][$i];
                $content['tax'][$i] += !empty($a->tax) ? $a->tax:0;
                $content['numOrders'][$i] = empty($content['numOrders'][$i])?0:$content['numOrders'][$i];
                $content['numOrders'][$i] += $a->numOrds;
                $content['custCountCategory'][$a->reportingClass][$i] = empty($content['custCountCategory'][$a->reportingClass][$i])?0:$content['custCountCategory'][$a->reportingClass][$i];
                $content['custCountCategory'][$a->reportingClass][$i] += $a->numCust;
                $content['countNumOrders'][$a->reportingClass][$i] = empty($content['countNumOrders'][$a->reportingClass][$i])?0:$content['countNumOrders'][$a->reportingClass][$i];
                $content['countNumOrders'][$a->reportingClass][$i] += $a->numOrds;
                $content['reportingClass'][$a->reportingClass][$i] = empty($content['reportingClass'][$a->reportingClass][$i])?0:$content['reportingClass'][$a->reportingClass][$i];
                $content['reportingClass'][$a->reportingClass][$i] += $a->totOrds;
            }

            if (isset($selectMonth)) {
                $content['selectMonth'] = $selectMonth;
            }

            $getDiscounts = "SELECT sum(chargeAmount) as charges
                    FROM orderCharge
                    join orders on orderID = order_id
                    where chargeType <> 'basic authorization'
                    and orders.business_id = $this->business_id ";

            switch ($selectedGroup) {
                case 'year':
                    $getDiscounts .= "and year(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')) = '$selectYear'";
                    break;
                case 'quarter':
                    $getDiscounts .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."'))) in ($selectQuarter)";
                    break;
                case 'month':
                    $getDiscounts .= "and concat(year(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'";
                    break;
                case 'week':
                    $getDiscounts .= "and (convert_tz(dateCreated, 'GMT', '".$this->business->timezone."') >= '$startUnix' and convert_tz(dateCreated, 'GMT', '".$this->business->timezone."') <= '$endUnix') ";
                    break;
                case '4week':
                    $getDiscounts .= "and yearweek(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."')) in ($selectRange)";
                    break;
            }

            $q = $this->reporting_database->query($getDiscounts);
            $discounts = $q->result();
            $content['discounts'][$i] = empty($content['discounts'][$i])?0:$content['discounts'][$i];
            foreach ($discounts as $d) {
                $content['discounts'][$i] = $content['discounts'][$i] + $d->charges;
            }

            $content['gross'][$i] = empty($content['gross'][$i])?0:$content['gross'][$i];
            $content['net'][$i] = $content['gross'][$i] + $content['discounts'][$i];
        }

        //calculate utilization rate
        foreach ($content['utilization'] as $locationID=>$ur) {
            $util  = 0;
            $content['utilization'][$locationID]  = "N/A";
            $content['lockerUtilization'][$locationID] = "N/A";
            
            $customerCount = array_sum($ur["customers"]);
            if (!empty($ur["units"])) {
                $util = ($customerCount/($ur["units"] * $numMonths)) * 100;
                $content['utilization'][$locationID] = number_format($util, 2, '.', '') . '% (' . $customerCount . ' customers / ' . $ur["units"] . ' units * ' . $numMonths . ' ' . $selectedGroup . 's)' ;
            } else {
                $util = ($customerCount/$numMonths);
                $content['utilization'][$locationID] = number_format($util, 2, '.', '') . ' avg customers (' . $customerCount . ' customers / ' . $numMonths . ' ' . $selectedGroup . 's)';
            }
            
            if (!empty($ur["lockers"])) {
                $orderCount = array_sum($ur["orders"]);
                if ($ur["lockers"] > 0) {
                    $util = ($orderCount/($ur["lockers"] * $elapsedDays)) * 100;
                    $content['lockerUtilization'][$locationID] = number_format($util, 2, '.', '') . '% (' . $orderCount.' orders / ' . $ur["lockers"] . ' lockers * ' . $elapsedDays . ' days)' ;
                }
            }
        }

        //data that is compared to prior month
        for ($i = 0; $i <= $numMonths -1; $i++) {
            $priorMonth = $i + 1;
            $content['firstCustomers'][$i] = empty($content['firstCustomers'][$i])?0:$content['firstCustomers'][$i];
            $content['uniqueCusts'][$i] = empty($content['uniqueCusts'][$i])?0:$content['uniqueCusts'][$i];
            if ($priorMonth<$numMonths) {
                $content['lostCusts'][$i] = $content['firstCustomers'][$i] - ($content['uniqueCusts'][$i] - $content['uniqueCusts'][$priorMonth]);
                if ($content['uniqueCusts'][$priorMonth] > 0) {
                    $content['retentionRate'][$i] = (1-($content['lostCusts'][$i] / $content['uniqueCusts'][$priorMonth]));
                } else {
                    $content['retentionRate'][$i] = 1;
                }
            } else {
                $content['lostCusts'][$i] = 'N/A';
                $content['retentionRate'][$i] = 'N/A';
            }
            $content['lifetime'][$i] = ($content['net'][$i] / $content['uniqueCusts'][$i]);
             
            // Category totals
            foreach ($content['custCountCategory'] as $class => $value) {
                $content['custFirstCategory'][$class][$i] = empty($content['custFirstCategory'][$class][$i])?0:$content['custFirstCategory'][$class][$i];
                $content['custCountCategory'][$class][$i] = empty($content['custCountCategory'][$class][$i])?0:$content['custCountCategory'][$class][$i];
                if ($priorMonth<$numMonths) {
                    $content['lostCustCategory'][$class][$i] = $content['custFirstCategory'][$class][$i] - ($content['custCountCategory'][$class][$i] - $content['custCountCategory'][$class][$priorMonth]);
                    $content['retentionRateCategory'][$class][$i] = (1-($content['lostCustCategory'][$class][$i] / $content['custCountCategory'][$class][$priorMonth]));
                } else {
                    $content['lostCustCategory'][$class][$i] = 'N/A';
                    $content['retentionRateCategory'][$class][$i] = 'N/A';
                }
            }
        }
        
        return $content;
    }
    
    public function by_location()
    {
        $content = $this->get_by_location_content();
        $this->template->write('page_title', 'Sales by Location', true);
        $content['select'] = $this->load->view('admin/reports/by_location_select', $content, true);
        $content['by_location'] = $this->load->view('admin/reports/by_location', $content, true);
        $this->renderAdmin('sales_by_location', $content);
    }
    
    public function orders_by_location()
    {
        $content = $this->get_by_location_content();
        $content['display_order_counts'] = 1;
        $this->template->write('page_title', 'Orders by Location', true);
        $content['select'] = $this->load->view('admin/reports/by_location_select', $content, true);
        $content['body'] = $this->load->view('admin/reports/by_location', $content, true);
        $this->renderAdmin('orders_by_location', $content);
    }

    public function firstTimeDetails()
    {
        $selectMonth = $this->input->get_post("month");
        $getFirstCustomers = "select * from(
            SELECT distinct `orders`.customer_id, min(dateCreated) as firstOrder, customer.*
                FROM `orders`
                join customer on customer_id = customerID
                where bag_id <> 0
                and orders.business_id = $this->business_id
                group by customer_id) t2
                where concat(year(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(firstOrder, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'
                order by firstOrder";
        $q = $this->reporting_database->query($getFirstCustomers);
        $firstCustomers = $q->result();
        $content['firstCustomers'] = $firstCustomers;

        $this->template->write('page_title', 'Details of First Time Customers', true);
        $this->renderAdmin('firstTimeDetails', $content);
    }

    public function signupDetails()
    {
        $selectMonth = $this->input->get_post("month");
        $getCustomers = "select * from customer
                where business_id = $this->business_id
                and concat(year(convert_tz(signupDate, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(signupDate, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'
                order by signupDate";

        $q = $this->reporting_database->query($getCustomers);
        $customers = $q->result();
        $content['customers'] = $customers;

        $this->template->write('page_title', 'Details of Signups', true);
        $this->data['content'] = $this->load->view('admin/reports/signupDetails', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function uniqueDetails()
    {
        $selectMonth = $this->input->get_post("month");
        $getFirstCustomers = "select customer.*, max(orders.dateCreated) as lastOrder
                from orders
                join customer on customerID = customer_id
                where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'))) = '$selectMonth'
                and orders.business_id = $this->business_id
                and bag_id <> 0
                group by customerID
                order by lastOrder";

        $q = $this->reporting_database->query($getFirstCustomers);
        $firstCustomers = $q->result();
        $content['firstCustomers'] = $firstCustomers;

        $this->template->write('page_title', 'Details of Unique Customers', true);
        $this->data['content'] = $this->load->view('admin/reports/uniqueDetails', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function by_order_type()
    {
        $this->load->model("ordertype_model");
        $content["order_types"] = $this->ordertype_model->get_order_types_as_codeigniter_dropdown();
        $content["selected_order_types"] = $this->input->post("order_types");

        $selectedGroup = "month";
        if ($this->input->post("grouping")) {
            $selectedGroup = $this->input->post("grouping");
        }
        $content['grouping'] = $selectedGroup;

        $content['months'] = array_combine(range(1, 12), range(1, 12));
        $content['years'] = array_combine(range(date('Y'), 2000, -1), range(date('Y'), 2000, -1));

        $time = mktime(0, 0, 0, date('m') - 5, 1, date('Y'));
        $content['end_month'] = date('m');
        $content['end_year'] = date('Y');
        $content['start_month'] = date('m', $time);
        $content['start_year'] = date('Y', $time);

        if ($this->input->post()) {
            $content['start_month'] = $this->input->post('start_month');
            $content['start_year'] = $this->input->post('start_year');
            $content['end_month'] = $this->input->post('end_month');
            $content['end_year'] = $this->input->post('end_year');
        }

        // calculate the number of months
        $numMonths = $content['end_year'] * 12 + $content['end_month']
                    - 12 * $content['start_year'] - $content['start_month'] + 1;


        $increment = 1;
        if ($selectedGroup == 'quarter') {
            $increment = 3;
        } elseif ($selectedGroup == 'week') {
            $numMonths = $numMonths * 4;
        }

        $content['increment'] = $increment;
        $content['numMonths'] = $numMonths;
        $content['show_extra'] = $this->input->post("extra");

        $date_y = $content['end_year'];
        $date_m = $content['end_month'];
        $current_time = mktime(0, 0, 0, $content['end_month'], date('d'), $content['end_year']);

        for ($i = 0; $i <= $numMonths -1; $i = $i + $increment) {
            $totComm = 0;

            //show the right heading
            switch ($selectedGroup) {
                case 'quarter':

                    $month = $date_m - $i;
                    $time1 = mktime(0, 0, 0, $month -0, 1, $date_y);
                    $time2 = mktime(0, 0, 0, $month -1, 1, $date_y);
                    $time3 = mktime(0, 0, 0, $month -2, 1, $date_y);

                    $date1 = date("Y-n", $time1);
                    $date2 = date("Y-n", $time2);
                    $date3 = date("Y-n", $time3);

                    $selectQuarter = "'$date1', '$date2', '$date3'";
                    $content['headers'][$i] = date("M 'y", $time1)." to ".date("M 'y", $time3);
                    break;

                case 'week':
                    $end = date("m/d/y", strtotime("this sunday", $current_time));
                    $endUnix = date("Y-m-d 23:59:59", strtotime("this sunday", $current_time));

                    $start = date("m/d/y", strtotime("$end - 7 days"));
                    $startUnix = date("Y-m-d 00:00:00", strtotime("$endUnix - 7 days"));

                    $dayJump = $i * 7;
                    $start = date("m/d/y", strtotime("$start - $dayJump days"));
                    $startUnix = date("Y-m-d 00:00:00", strtotime("$startUnix - $dayJump days"));

                    $end = date("m/d/y", strtotime("$end - $dayJump days"));
                    $endUnix = date("Y-m-d 23:59:59", strtotime("$endUnix - $dayJump days"));

                    $selectWeek = date("YW", strtotime($end));
                    $content['headers'][$i] = $end." - ".$start;
                    break;

                case '4week':
                    $end = date("m/d/y", strtotime("this sunday", $current_time));
                    $start = date("m/d/y", strtotime("$end - 28 days"));

                    $dayJump = $i * 28;
                    $start = date("m/d/y", strtotime("$start - $dayJump days"));
                    $end = date("m/d/y", strtotime("$end - $dayJump days"));

                    $w1 = date("YW", strtotime($end));
                    $w2 = date("YW", strtotime("$end - 7 days"));
                    $w3 = date("YW", strtotime("$end - 14 days"));
                    $w4 = date("YW", strtotime("$end - 21 days"));

                    $selectRange = $w1.','.$w2.','.$w3.','.$w4;
                    $content['headers'][$i] = $end." - ".$start;
                    break;

                case 'month':
                    $time1 = mktime(0, 0, 0, $date_m - $i, 1, $date_y);
                    $selectMonth = date("Y-n", $time1);
                    $content['headers'][$i] = date("M 'y", $time1);
                    break;
            }

            //main query
            $getAllData = "
                SELECT
                    count(orderID) as totOrds,
                    sum(closedGross) as gross,
                    sum(closedDisc) as disc,
                    orderType as reportingClass,
                    count(distinct orderID) as numOrds,
                    count(distinct `orders`.customer_id) as numCust
                FROM
                report_orders as orders ";

            switch ($selectedGroup) {
                case 'quarter':
                    $getAllData .= "where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'))) in ({$selectQuarter})";
                    break;
                case 'month':
                    $getAllData .= "where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'))) = '{$selectMonth}'";
                    break;
                case 'week':
                    $getAllData .= "where (convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') >= '{$startUnix}'  and convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') <= '{$endUnix}') ";
                    break;
                case '4week':
                    $getAllData .= "where yearweek(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')) in ({$selectRange})";
                    break;
            }

            if (!empty($content["selected_order_types"])) {
                $getAllData .= " AND orderType IN ('".implode("','", $content["selected_order_types"])."') ";
            }

            $getAllData .= "AND business_id = {$this->business_id}
                                group by orderType";

            $q = $this->reporting_database->query($getAllData);
            $allData = $q->result();

            foreach ($allData as $a) {
                $content['repClass'][$a->locationID] = $a->reportingClass;
                $util = 0;
                $utilMeas = '';
                $content['gross'][$i] += $a->gross;
                $content['discounts'][$i] += $a->disc;
                $content['net'][$i] += $a->gross+$a->disc;

                $content['reportingClass'][$a->reportingClass][$i] += $a->gross;
                $content['totalDisc'][$a->reportingClass][$i] += $a->disc;
            }

            if (isset($selectMonth)) {
                $content['selectMonth'] = $selectMonth;
            }
        }

        $this->template->write('page_title', 'Sales By Order Type', true);
        $this->renderAdmin('by_order_type', $content);
    }

    public function discount_details($month = null, $location_id =  null)
    {
        $getDiscounts = "SELECT orderCharge.*, firstName, lastName
                                FROM orderCharge
                                left join orders on orderID = orderCharge.order_id
                                left join locker on lockerID = locker_id
                                left join customer on customerID = `orders`.customer_id
                                where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')))='".$month."'
                                and orders.business_id = $this->business_id ";
        if ($location_id) {
            $getDiscounts .= ' and locker.location_id = '. $location_id;
        }
        $getDiscounts .= " order by orderCharge.updated, firstName";
        $q = $this->reporting_database->query($getDiscounts);
        $content['discounts'] = $q->result();

        $this->template->write('page_title', 'Discount Details', true);
        $this->renderAdmin('discount_details', $content);
    }

    public function by_location_details($month = null, $location = null)
    {
        //note, month should be YYYY-m (month with no leading zeros)
        
        if (empty($month)) {
            throw new Exception("'month' must be passed as segment 4.");
        }
        $content['locationID'] = $location;
        if (!is_numeric($location)) {
            throw new Exception("'location' must be passed as segment 5 and must be numeric.");
        }


        $get_location_orders_raw_query = "select orders.*, location.locationID, location.address, customer.firstName, customer.lastName
                        from orders
                        join locker on lockerID = locker_id
                        join location on locationID = location_id
                        left join customer on customer_id = customerID
                        where concat(year(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')),'-', month(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."')))='$month'
                        and locationID = $location
                        and orders.business_id = $this->business_id
                        order by dateCreated asc;";
        $get_location_orders_query = $this->reporting_database->query($get_location_orders_raw_query);
        $content['orders'] = $orders = $get_location_orders_query->result();


        $content['location']['month'] = $month;
        $content['location']['address'] = $orders[0]->address;

        $totalWF  = 0; //orders that are WF
        $totalDC = 0;  // orders that are DC
        $sumWFamt = 0; //sum of all WF amt
        $sumDCamt = 0; // sum of all DC amt
        $sumWFdiscounts = 0;
        $sumDCdiscounts = 0;

        $sumWFtotal = 0;
        $sumDCtotal = 0;

        $this->load->model('orderitem_model');
        $this->load->model('order_model');
        $this->load->model('product_model');
        $getWFProduct = "select productID from productCategory
                    join product on productCategoryID = productCategory_id
                    where productCategory.business_id = $this->business_id
                    and productCategory.slug = 'wf'";
        $q = $this->reporting_database->query($getWFProduct);
        $wfProducts = $q->result();
        foreach ($wfProducts as $wf) {
            $wfProduct[] = $wf->productID;
        }


        foreach ($orders as $o) {
            $custCount[$o->customer_id] = 1;
            $content['ordAmts'][$o->orderID] = $ordTotal = $this->orderitem_model->get_item_total($o->orderID);
            $content['ordType'][$o->orderID] = $ordType = $this->order_model->getOrderType($o->orderID);
            $content['ordCount'][$ordType] ++;
            $content['gross'][$ordType] += $ordTotal['subtotal'];
            $content['discounts'][$ordType] += $ordTotal['discounts'];
            $content['net'][$ordType] += $ordTotal['total'];
            $thisDay = local_date($this->business->timezone, $o->dateCreated, 'Y-m-d');
            $content['dailyCount'][$thisDay][$ordType] ++;
            $content['dailyGross'][$thisDay] += $ordTotal['subtotal'];
            $content['dailyDisc'][$thisDay] += $ordTotal['discounts'];
            $content['dailyNet'][$thisDay] += $ordTotal['total'];
            $content['ordCount']['WF LBS'] = 0;
            foreach ($ordTotal['items'] as $item) {
                if (in_array($item->product_id, $wfProduct)) {
                    $content['dailyCount'][$thisDay]['WF LBS'] += $item->qty;
                }
            }
        }


        $content['location']['custCount'] = count($custCount);

        ksort($content['ordCount']);



        //get any prepayment and such
        $this->load->model('orderItem_model');
        $this->load->model('order_model');
        $this->load->model('customer_model');
        $getPrePayments = "	select orders.* from orders
                                join locker on lockerID = orders.locker_id
                                join location on locationID = locker.location_id
                                join customer on customerID = orders.customer_id
                                where concat(year(orders.dateCreated),'-', month(orders.dateCreated))='$month'
                                and customer.location_id = $location
                        and (bag_id is null or bag_id = 0);";
        $q = $this->reporting_database->query($getPrePayments);
        $payments = $q->result();
        foreach ($payments as $p) {
            $ordInfo = $this->orderItem_model->get_item_total($p->orderID);
            $custInfo = $this->customer_model->get_customer($p->customer_id);

            $content['prePayments'][$p->orderID] = '<tr valign="bottom"><td>'.convert_from_gmt_aprax($p->dateCreated, SHORT_DATE_FORMAT).'</td>
                            <td><a href="/admin/orders/details/'.$p->orderID.'" target="_blank">'.$p->orderID.'</a></td>
                            <td>'.  getPersonName($custInfo[0]).'</td>
                <td>'.format_money_symbol($this->business_id, '%.2n', $ordInfo['total']).'</td>
                <td>'.format_money_symbol($this->business_id, '%.2n', $ordInfo['discounts']).'</td>
                <td>'.format_money_symbol($this->business_id, '%.2n', $ordInfo['total']).'</td>
                <td>'.$this->order_model->getOrderType($p->orderID).'</td><td></td></tr>';
        }

        $this->template->write('page_title', 'Location Detail', true);
        $this->renderAdmin('by_location_details', $content);
    }

    public function customer_order_breakdown($customer = null)
    {
        $this->load->model('customer_model');
        $content['customer'] = $this->customer_model->get_customer($customer);

        if ($this->input->post('start_date')) {
            $start_date = new DateTime($this->input->post("start_date"));
            $finishedPlans .= "AND startDate > '" . convert_to_gmt_aprax($start_date->format("Y-m-d")) . "' ";
            $content['start_date'] = $this->input->post("start_date");
        }

        if ($this->input->post('end_date')) {
            $end_date = new DateTime($this->input->post("end_date"));
            $finishedPlans .= "AND endDate < '" . convert_to_gmt_aprax($end_date->format("Y-m-d")) . "' ";
            $content['end_date'] = $this->input->post('end_date');
        }

        $getOrders = "SELECT orderID, orders.notes, qty,
            orders.dateCreated as dateCreated,
            orders.orderPaymentStatusDate as orderPaymentStatusDate,
            orderPaymentStatusOption_id, 
            product_process_id, 
            product.name, 
            closedGross, 
            closedDisc,
            statusDate,
            dueDate
            FROM orders
                left join orderItem on orderItem.order_id = orderID
                left join product_process on product_processID = product_process_id
                left join product on productID = product_id
                where customer_id = $customer
                and orders.business_id = $this->business_id
                AND orders.notes NOT LIKE '%Authorizing Card%' ";

        if (!empty($start_date)) {
            $getOrders .= "AND orders.dateCreated >= '" . convert_to_gmt_aprax($start_date->format("Y-m-d")) ."' ";
        }

        if (!empty($end_date)) {
            $getOrders .= "AND orders.dateCreated <= '" . convert_to_gmt_aprax($end_date->format("Y-m-d")) ."' ";
        }

        $this->load->model('order_model');

        $getOrders .= "order by orderID desc, notes";
        $q = $this->reporting_database->query($getOrders);
        $orders = $q->result();

        $this->load->model("orderstatus_model");
        foreach ($orders as $order) {
            $content['products'][$order->name] = $order->name;
            $content['orders'][$order->orderID][$order->name] += $order->qty;
            $content['ordID'][$order->orderID] = $order;
            $content['amount'][$order->orderID] = $order->closedGross + $order->closedDisc;
            $content['netAmount'][$order->orderID] = $this->order_model->get_net_total($order->orderID);
            $content['total_products'][$order->name] += $order->qty;

            $inventoried_date = $this->orderstatus_model->getOrderStatusHistoryByStatusID($order->orderID, 3);
            $content['inventoriedDate'][$order->orderID]  = empty($inventoried_date)?false:$inventoried_date->date;
        }

        $this->template->write('page_title', 'Customer Order Breakdown', true);
        $this->renderAdmin('customer_order_breakdown', $content);
    }

    public function supplier_manifest()
    {
        $this->load->model('supplier_model');
        $this->load->model('process_model');
        $this->supplier_model->business_id = $this->business_id;
        $this->supplier_model->select = "supplierID, supplierBusiness_id, companyName, address, city, state, phone";
        $this->supplier_model->join("business ON business.businessID = supplier.supplierBusiness_id");
        $business = new Business($this->business_id, false);

        $suppliers = $this->supplier_model->get();
        foreach ($suppliers as $supplier) {
            $content['suppliers'][$supplier->supplierID] = $supplier->companyName;
        }

        $processes = $this->process_model->get();
        foreach ($processes as $process) {
            $content['processes'][$process->processID] = $process->name;
        }
        $content['processes']['NULL'] = 'Other';

        if ($_POST) {
            update_business_meta($this->business->businessID, 'supplier_manifest_cutOff', $this->input->post("cutOffEnd"));
            $content['selectedDateStart'] = $this->input->post("selectedDateStart");
            $content['selectedDateEnd'] = $this->input->post("selectedDateEnd");
            $content['cutOffStart'] = $this->input->post("cutOffStart");
            $content['cutOffEnd'] = $this->input->post("cutOffEnd");

            $cutOffStart = $this->input->post("cutOffStart");
            $startDate = $this->input->post("selectedDateStart") . ' '. $cutOffStart;
            $cutOffEnd = $this->input->post("cutOffEnd");
            $endDate = $this->input->post("selectedDateEnd") . ' '. $cutOffEnd;

            $content['startDate'] = $startDate = date("Y-m-d H:i:s", strtotime($startDate));
            $content['endDate'] = $endDate = date("Y-m-d H:i:s", strtotime($endDate));

            $processes = implode("', '", $this->input->post("process"));
            $processes = "'".$processes."'";

            if ($this->input->post("supplier") == "ALL") {
                $supplierIDs = implode(",", array_keys($content['suppliers']));
                $getItems = "SELECT orderID, companyName, dateScanned as scanned, orderItem.unitPrice, orderItemID, product.name as product, product.productType as type, process.name as process, 
                    CASE orderItem.unitPrice
                        WHEN 0 THEN 0
                        ELSE cost
                    END AS cost, 
                    barcode, orderItem.item_id, orderItem.updated, qty,
                            (select minQty from product_supplier where product_supplier.supplier_id = orderItem.supplier_id
                                            and product_supplier.product_process_id = orderItem.product_process_id) as minQty,
                                            IF ((select automatID from automat where automat.order_id = orderItem.order_id and barcode.barcode = automat.garcode limit 1), 'Y', 'NOT ASSEMBLED') as assembled,
                                            productID
                        FROM orderItem
                        LEFT JOIN scannedOrderItem ON orderItem_id = orderItem.orderItemID
                        LEFT JOIN supplier ON supplier.supplierID = orderItem.supplier_id
                        LEFT JOIN business ON business.businessID = supplier.supplierBusiness_id
                        join orders on orderID = order_id
                        left join barcode on barcode.item_id = orderItem.item_id
                        join product_process on product_processID = product_process_id
                        join product on product_id = productID

                        left join process on process_id = processID

                        where orders.business_id = ".$this->business_id."
                        and process_id IN ($processes)
                        and supplier_id IN ($supplierIDs)
                        and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') < '".$endDate."'
                        and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') > '".$startDate."'";
            } else {
                $getItems = "SELECT orderID, companyName, barcode, dateScanned as scanned, orderItem.unitPrice, orderItemID, product.name as product, product.productType as type, process.name as process,
                        CASE orderItem.unitPrice
                            WHEN 0 THEN 0
                            ELSE cost
                        END AS cost, 
                        barcode, orderItem.item_id, orderItem.updated, qty,
                            (select minQty from product_supplier where product_supplier.supplier_id = orderItem.supplier_id
                                            and product_supplier.product_process_id = orderItem.product_process_id) as minQty,
                                            IF ((select automatID from automat where automat.order_id = orderItem.order_id and barcode.barcode = automat.garcode limit 1), 'Y', 'NOT ASSEMBLED') as assembled,
                                            productID
                        FROM orderItem
                        LEFT JOIN scannedOrderItem ON orderItem_id = orderItem.orderItemID
                        LEFT JOIN supplier ON supplier.supplierID = orderItem.supplier_id
                        LEFT JOIN business ON business.businessID = supplier.supplierBusiness_id
                        join orders on orderID = order_id
                        left join barcode on barcode.item_id = orderItem.item_id
                        join product_process on product_processID = product_process_id
                        join product on product_id = productID

                        left join process on process_id = processID
                        where orders.business_id = ".$this->business_id."
                        and process_id IN ($processes)
                        and supplier_id = ".$this->input->post("supplier")."
                        and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') < '".$endDate."'
                        and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d %H:%i:%s') > '".$startDate."'";
                    }
            //echo $getItems; die();
            //if a barcode is scanned record that
            if ($_POST['barcode']) {
                //first find the orderitem
                $findItem = $getItems." and barcode = '".$_POST['barcode']."'";
                $q = $this->reporting_database->query($findItem);
                $item = $q->result();
                if ($item) {
                    //updatd to handle multiple scans
                    //$updateOrderItem = "update orderItem set scanned = now() where orderItemID = ".$item[0]->orderItemID;
                    $updateOrderItem = "INSERT INTO scannedOrderItem (orderItem_id, dateScanned) VALUES ({$item[0]->orderItemID}, now())";
                    $this->db->query($updateOrderItem);
                } else {
                    echo ' <h1>Sorry we could not find barcode '.$_POST['barcode'].'</h1><br><br>';
                }
            }

            //we are using this same query, just appending a sort to it
            $getItems = $getItems." GROUP BY orderItemID order by scanned desc, orderItem.updated, product, process";

            $q = $this->db->query($getItems);
            $items = $q->result();

            foreach ($items as $item) {
                if ($item->scanned > "0000-00-0000") {
                    $sql = "SELECT dateScanned from scannedOrderItem WHERE orderItem_id = {$item->orderItemID}";
                    $scans = $this->db->query($sql)->result();
                    $array =null;
                    foreach ($scans as $scan) {
                        $array[] = convert_from_gmt_aprax($scan->dateScanned, "g:ia");
                    }
                    $item->scannedTimes = implode(", ", $array);
                }
            }


            $content['details'] = $items;
            $this->load->model("barcode_model");

            foreach ($items as $item) {
                if ($item->minQty > $item->qty) {
                    $item->qty = $item->minQty;
                }

                if ($item->type == 'product') {
                    $content['items'][$item->product.'</td><td>'.$item->process]['qty'] += $item->qty;
                    $content['items'][$item->product.'</td><td>'.$item->process]['cost'] += $item->cost * $item->qty;
                    $content['items'][$item->product.'</td><td>'.$item->process]['unitPrice'] += $item->unitPrice * $item->qty;
                    $content['items'][$item->product.'</td><td>'.$item->process]['companyName'] = $item->companyName;
                    $content['items'][$item->product.'</td><td>'.$item->process]['productID'] = $item->productID;
                    $content['total']['qty'] += $item->qty;
                } elseif ($item->type == 'preference') {
                    $content['items'][$item->product.' [ preference ]</td><td>'.$item->process]['qty'] = '-';
                    $content['items'][$item->product.' [ preference ]</td><td>'.$item->process]['cost'] += $item->cost * $item->qty;
                    $content['items'][$item->product.' [ preference ]</td><td>'.$item->process]['unitPrice'] += $item->unitPrice * $item->qty;
                    $content['items'][$item->product.' [ preference ]</td><td>'.$item->process]['companyName'] = $item->companyName;
                    $content['items'][$item->product.'</td><td>'.$item->process]['productID'] = $item->productID;
                }


                $content['total']['cost'] += $item->cost * $item->qty;
                $content['total']['unitPrice'] += $item->unitPrice * $item->qty;
            }
        } else { //if no post was sent
            $content['selectedDateStart'] = date("Y-m-d", strtotime("-1 day"));
            $content['selectedDateEnd'] = date("Y-m-d", strtotime("today"));
            $content['cutOffStart'] =  get_business_meta($this->business->businessID, 'supplier_manifest_cutOff');
            $content['cutOffEnd'] =  get_business_meta($this->business->businessID, 'supplier_manifest_cutOff');
        }
        if ($this->input->post("receipt_printer_friendly") == 1) {
            $this->load->view('admin/reports/supplier_manifest_receipt_printer_friendly', $content);
        } else {
            $content['suppliers']['ALL'] = "ALL";

            $this->template->write('page_title', 'Supplier Manifest', true);
            $this->renderAdmin('supplier_manifest', $content);
        }
    }

    public function show_top_customers()
    {
        $this->data['content'] = $this->load->view('admin/reports/top_customers', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function get_top_customers_report_dataTables_columns()
    {
        $columns = array();
        foreach ($this->top_customer_report_columns as $index => $top_customer_report_column) {
            $columns[$index]['sTitle'] = $top_customer_report_column['displayName'];
            if (array_key_exists("dataTable_options", $top_customer_report_column)) {
                foreach ($top_customer_report_column['dataTable_options'] as $name => $value) {
                    $columns[$index][$name] = $value;
                }
            }
        }
        echo json_encode($columns);
    }

    public function get_top_customers_report_dataTables_data()
    {
        try {
            /* Array of database columns which should be read and sent back to DataTables. Use a space where
            * you want to insert a non-database field (for example a counter or static image)
            */
            foreach ($this->top_customer_report_columns as $top_customer_report_column) {
                $aColumns[] = $top_customer_report_column['queryName'];
            }
            array_filter(array_map("trim", $aColumns));

            /* Indexed column (used for fast and accurate table cardinality) */
            $sIndexColumn = "orderID";

            /*
            * Paging
            */
            $sLimit = "";
            if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
                $sLimit = "LIMIT ".mysql_real_escape_string($_GET['iDisplayStart']).", ".
                mysql_real_escape_string($_GET['iDisplayLength']);
            }


            /*
            * Ordering
            */
            $sOrder = "";
            if (isset($_GET['iSortCol_0'])) {
                $sOrder = "ORDER BY  ";
                for ($i=0 ; $i<intval($_GET['iSortingCols']) ; $i++) {
                    if ($_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true") {
                        $queryName = $this->top_customer_report_columns[$_GET['iSortCol_'.$i]]['queryName'];
                        preg_match("/ AS (.*)$/i", $queryName, $matches);
                        if (empty($matches)) {
                            $sOrder .= $queryName . " ". mysql_real_escape_string($_GET['sSortDir_'.$i]) .", ";
                        } else {
                            $alias = $matches[1];
                            $sOrder .= $alias . " ". mysql_real_escape_string($_GET['sSortDir_'.$i]) .", ";
                        }
                    }
                }

                $sOrder = substr_replace($sOrder, "", -2);
                if ($sOrder == "ORDER BY") {
                    $sOrder = "";
                }
            }


            /*
            * Filtering
            * NOTE this does not match the built-in DataTables filtering which does it
            * word by word on any field. It's possible to do here, but concerned about efficiency
            * on very large tables, and MySQL's regex functionality is very limited
            */
            $sWhere = "";
            if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
                $sWhere = "AND (";
                for ($i=0 ; $i<count($aColumns) ; $i++) {
                    if (isset($this->top_customer_report_columns[$i]['dataTable_options']['bSearchable']) && $this->top_customer_report_columns[$i]['dataTable_options']['bSearchable'] == false) {
                        continue;
                    } else {
                        $sWhere .= $this->top_customer_report_columns[$i]['queryName'] ." LIKE '%".mysql_real_escape_string($_GET['sSearch'])."%' OR ";
                    }
                }

                $sWhere = substr_replace($sWhere, "", -3);
                $sWhere .= ")";
            }

            /* Individual column filtering */
            for ($i=0 ; $i<count($aColumns) ; $i++) {
                if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '') {
                    $sWhere .= "AND ";
                    $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
                }
            }

            /*
            * SQL queries
            * Get data to display
            */
            $select = "SELECT SQL_CALC_FOUND_ROWS ". implode(", ", $aColumns);
            $sQuery = "
                $select
                FROM orderItem
                INNER JOIN orders ON orders.orderID = orderItem.order_id
                INNER JOIN customer ON customer.customerID= orders.customer_id
                WHERE orders.business_id={$this->business_id}
                $sWhere

                GROUP BY customer_id
                $sOrder
                $sLimit
            ";
            $rQuery = $this->reporting_database->query($sQuery);
            $rResult = $rQuery->result_array();

            /* Data set length after filtering */
            $sQuery = "
                    SELECT FOUND_ROWS() AS found_rows
            ";

            $aResultFilterTotal = $this->reporting_database->query($sQuery)->row();
            $iFilteredTotal = $aResultFilterTotal->found_rows;

            /* Total data set length */
            $sQuery = $this->reporting_database->query("
                    SELECT COUNT(DISTINCT customer_id) AS total
                    FROM orderItem
                    INNER JOIN orders ON orders.orderID = orderItem.order_id
                    INNER JOIN customer ON orders.customer_id=customer.customerID
                    WHERE customer.business_id = {$this->business_id}
            ");

            $aResultTotal = $sQuery->row();
            $iTotal = $aResultTotal->total;

            /*
            * Output
            */
            $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
            );

            foreach ($rResult as $aRow) {
                $row = array();
                for ($i=0 ; $i<count($aColumns) ; $i++) {
                    $keys = array_keys($aRow);
                    $row[] = $aRow[$keys[$i]];
                }
                //format price
                $row[6] = format_money_symbol($this->business_id, '%.2n', $row[6]);
                
                $output['aaData'][] = $row;
            }

            echo json_encode($output);
        } catch (Exception $e) {
            send_exception_report($e);
            output_ajax_error_response($e->getMessage());
        }
    }

    public function production_details()
    {
        $content['day'] = $day = $this->input->get('day');
        $content['service'] = $service = $this->input->get('service');
        $content['status'] = $status = $this->input->get('status');
        $business = new \App\Libraries\DroplockerObjects\Business($this->business_id);

        $dateTo = date("Y-m-d 00:00:00", strtotime($day ." +1 day"));
        $endTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateTo), $this->business->timezone));

        $dateFrom = date("Y-m-d 00:00:00", strtotime("$endTime -1 day"));
        $startTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateFrom), $this->business->timezone));

        $dateEnd = $this->input->get('dateEnd');
        $content['dateEnd'] = date("Y-m-d", strtotime($endTime));
        if (!empty($dateEnd)) {
            $startTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($day), $this->business->timezone));
            $endTime = date("Y-m-d H:i:s", convert_to_gmt(strtotime($dateEnd), $this->business->timezone));
            $content['dateEnd'] = $dateEnd;
        }

        //get modules
        $this->load->model('module_model');
        $content['modules'] = $this->module_model->get();
        
        //set defaults if nothing selected
        if (!$_GET['modules']) {
            $_GET['modules'][0] = 2;
            $_GET['modules'][1] = 3;
        }
        $modules = mysql_real_escape_string(implode(',', $_GET['modules']));
        $content['selectedServices'] = $_GET['modules'];

        if ($this->input->get('status') == 'inv') {
            $status = 'orderItem.updated';
        } else {
            $status = 'orders.dateCreated';
        }

        $show_customer_details = empty($this->input->get('show_customer_details'))?false:true;

        if ($show_customer_details) {
            $getDetails = "SELECT order_id, qty, orderItem.item_id, product.name as productName, module.name as processName, convert_tz($status, 'GMT', '".$business->timezone."') as created, concat(customer.firstname, ' ', customer.lastname) as custName, productCategory.name as category, customerID
                FROM orderItem
                inner join `orders` on orderID = orderItem.order_id
                join locker on lockerID = orders.locker_id
                join location on locationID = locker.location_id
                join product_process on product_processID = orderItem.product_process_id
                join product on productID = product_process.product_id
                JOIN productCategory ON product.productCategory_id = productCategory.productCategoryID
                join module on moduleID = productCategory.module_id
                join customer on customerID = orders.customer_id
                where $status >= ?
                and $status <= ?
                and location.business_id = ?
                and module.moduleID IN ($modules)
                and productType <> 'preference'
                order by orderItem.updated desc";


            $q = $this->reporting_database->query($getDetails, array($startTime, $endTime, $this->business_id, $service));
            $content['details'] = $q->result();
        }
        $content['show_customer_details'] = $show_customer_details;

        $sqlGetTotals = "SELECT sum(qty) as pcs, sum(qty*orderItem.unitPrice) as price, product.name as productName, productCategory.name as category
            FROM orderItem inner join `orders` on orderID = orderItem.order_id
            join locker on lockerID = orders.locker_id join location on locationID = locker.location_id
            join product_process on product_processID = orderItem.product_process_id
            join product on productID = product_process.product_id
            JOIN productCategory ON product.productCategory_id = productCategory.productCategoryID
            join module on moduleID = productCategory.module_id
            where $status >= ?
            and $status <= ?
            and location.business_id = ?
            and productType <> 'preference'
            and module.moduleID in ($modules)
            group by product.name
            order by pcs desc";

        $q = $this->reporting_database->query($sqlGetTotals, array($startTime, $endTime, $this->business_id, $modules));

        $content['totals'] = $q->result();

        $this->template->write('page_title', 'Production Details', true);

        $this->renderAdmin('production_details', $content);
    }

    public function order_notes()
    {
        $status = $this->input->get('status');
        if (!$status) {
            $status = 3;
        }

        $content['status'] = $status;

        $getNotes = "
                SELECT customerID, firstName, lastName, orderID, location.route_id, position, orders.notes, postAssembly, orderStatus.date, orders.dueDate,
                location.address, location.companyName, location.address, location.address2, location.locationID  
                FROM orders
                left join assembly on orderID = order_id
                left join orderStatus on orderStatus.order_id = orders.orderID
                join customer on customerID = customer_id
                join locker on orders.locker_id = locker.lockerID
                join location on locker.location_id = location.locationID
                where orders.business_id = ?
                and orders.orderStatusOption_id = ?
                and orderStatus.orderStatusOption_id = ?
                AND ((orders.notes != '' AND orders.notes != 'ORDER TYPE: SMS') OR postAssembly != '')
                group by orderID
                order by dueDate, route_id, position";

        $q = $this->reporting_database->query($getNotes, array($this->business_id, $status, $status));

        $content['notes'] = $notes = $q->result();

        $this->load->model('order_model');
        foreach ($notes as $n) {
            $content['type'][$n->orderID] = $this->order_model->getOrderType($n->orderID);
        }

        $this->setPageTitle('Order Notes');
        $this->renderAdmin('admin/reports/order_notes', $content);
    }

    public function coupon_usage()
    {
        if (isset($_POST['code'])) {
            $getCoupons = "SELECT couponCode, count(customerDiscountID) as totCount, sum(amountApplied) as totAmt, extendedDesc, origCreateDate, expireDate 
                    FROM customerDiscount
                    where couponCode IS NOT NULL
                        AND couponCode <> ''
                        AND business_id = '{$this->business_id}'
                        and couponCode like '".$_POST['code']."'
                        group by couponCode
                    order by totCount desc";
            $c = $this->reporting_database->query($getCoupons);
            $content['coupons'] = $c->result();
        }
        $this->template->write('page_title', 'Coupon Usage', true);
        $this->data['content'] = $this->load->view('admin/reports/coupon_usage', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function coupon_usage_details($couponCode = null, $show = null)
    {
        $content['couponCode'] = $couponCode;

        $getCouponInfo = "select * from coupon where concat(prefix, code) = '$couponCode' and business_id = $this->business_id";
        $q = $this->reporting_database->query($getCouponInfo);
        $content['couponInfo'] = $couponInfo = $q->result();


        //if they want to see all coupons like this (e.g for a gift card)
        if ($show == 'all') {
            $where = "extendedDesc = '".$couponInfo[0]->extendedDesc."'";
        } else {
            $where = "couponCode = '".$couponCode."'";
        }

        $getCount = "SELECT count(customerDiscountID) as totCount
                    FROM customerDiscount
                    where ".$where."
                    and business_id = $this->business_id";
        $q = $this->reporting_database->query($getCount);
        $content['issued'] = $q->result();

        $getRedeemed = "SELECT count(customerDiscountID) as totCount
                    FROM customerDiscount
                    where ".$where."
                    and active='0'
                    and business_id = $this->business_id";
        $q = $this->reporting_database->query($getRedeemed);
        $content['redeemed'] = $q->result();


        if ($couponInfo->frequency == 'every order') {
            $getCust = "select customer_id from customerDiscount where ".$where." and business_id = $this->business_id";
        } else {
            $getCust = "select customer_id from customerDiscount where ".$where." and business_id = $this->business_id";
        }
        $q = $this->reporting_database->query($getCust);
        $customers = $q->result();

        $this->load->model('customer_model');

        foreach ($customers as $c) {
            $content['custOrders'][$c->customer_id] = $this->customer_model->orderInfo($c->customer_id);
            $custDump = "select firstName, lastName, location.address, email
                        from customer
                        left join location on locationID = location_id
                        where customerID = $c->customer_id";
            $q = $this->reporting_database->query($custDump);
            $content['custInfo'][$c->customer_id] = $q->row();
        }


        $this->template->write('page_title', 'Coupon Usage Details', true);
        $this->data['content'] = $this->load->view('admin/reports/coupon_usage_details', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function followUpCall()
    {
        if ($_POST['getCount']) {
            $limit = $_POST['getCount'];
        } else {
            $limit = 100;
        }
        $getCalls = "select customer.firstName as cfirst, customer.lastName as clast, followupCallType.name, dateCalled, followupCall.note, employee.firstName as efirst, employee.lastName as eLast, followupCall.customer_id
            from followupCall
            join customer on customer_id = customerID
            join employee on employeeID = employee_id
            join followupCallType on followupCallType_id = followupCallTypeID
            where followupCall.business_id = $this->business_id
            order by dateCalled desc
            limit $limit";
        $q = $this->reporting_database->query($getCalls);
        $content['calls'] = $q->result();



        $this->template->write('page_title', 'Follow Up Calls', true);
        $this->data['content'] = $this->load->view('admin/reports/followUpCalls', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function order_count()
    {
        $dayCount = 30;
        $timeStart = strtotime("-$dayCount days");
        $timeEnd = time();
        $sortOrder = 'asc';

        if (!empty($_GET['sortOrder'])) {
            $sortOrder = $_GET['sortOrder'];
        }

        if (!empty($_GET['dateStart'])) {
            $timeStart = strtotime($_GET['dateStart']);
        }

        if (!empty($_GET['dateEnd'])) {
            $timeEnd = strtotime($_GET['dateEnd']);
        }

        $dayCount = ceil(($timeEnd - $timeStart) / 86400);
        $dateStart = date("Y-m-d", $timeStart);
        $dateEnd = date("Y-m-d", $timeEnd);

        $content['dayCount'] = $dayCount;
        $content['dateStart'] = $dateStart;
        $content['dateEnd'] = $dateEnd;
        $content['timeStart'] = $timeStart;
        $content['timeEnd'] = $timeEnd;
        $content['sortOrder'] = $sortOrder;

        $get_order_count = "select location.address, locationID, count(orderID) as ordCount, serviceType,
                    sum(closedGross) as closedGross, sum(closedDisc) as closedDisc,
                    date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') as ordDate
                from orders
                join locker on lockerID = locker_id
                join location on locationID = location_id
                left join customer on customer_id = customerID
                where convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') >= '$dateStart 00:00:00'
                and convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."') <= '$dateEnd 23:59:59'
                and orders.business_id = $this->business_id
                and bag_id != 0
                group by locationID, date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%M-%d')
                order by orders.dateCreated desc;";
        $get_order_count = $this->reporting_database->query($get_order_count);
        $orders = $get_order_count->result();

        foreach ($orders as $order) {
            $content['orders'][$order->locationID]['address'] = $order->address;
            $content['orders'][$order->locationID][$order->ordDate] = $order;
        }

        //get location service days
        $getFrequency = "Select * from location
                        where business_id = $this->business_id";
        $getFrequency = $this->reporting_database->query($getFrequency);
        $frequencies = $getFrequency->result();

        foreach ($frequencies as $frequency) {
            $content['frequency'][$frequency->locationID] = $frequency->serviceType;
        }


        $this->template->write('page_title', 'Count Of Orders By Location', true);
        $this->data['content'] = $this->load->view('admin/reports/order_count', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function unassembled_items()
    {
        if (!empty($_POST['garcode'])) {
            $this->load->model('item_model');
            $this->item_model->mark_as_assembled($_POST['order_id'], $_POST['garcode']);
            redirect_with_fallback("/admin/reports/unassembled_items");
        }

        $fromDate = date("Y-m-d", strtotime("2 days ago"));
        if (isset($_GET['fromDate'])) {
            $fromDate = $_GET['fromDate'];
        }

        $endDate = date("Y-m-d");
        if (isset($_GET['endDate'])) {
            $endDate = $_GET['endDate'];
        }

        if ($endDate < $fromDate) {
            $temp = $fromDate;
            $fromDate = $endDate;
            $endDate = $temp;
        }
        $content['fromDate'] = $fromDate;
        $content['endDate'] = $endDate;

        $endDate = convert_to_gmt_aprax($endDate, $this->business->timezone);
        $fromDate = convert_to_gmt_aprax($fromDate, $this->business->timezone);

        $getAllItems = "SELECT barcode, business.companyName, firstName, lastName, customerID, orders.dateCreated,
                orderItem.order_id, orderItem.supplier_id, orderItem.updated, orderItem.item_id, product.name as productName,
                CONCAT(COALESCE(gardesc,''), COALESCE(slot,'')) AS automat,
                picture.file AS picture, location.route_id, assembly.position
            FROM orderItem
            JOIN barcode on barcode.item_id = orderItem.item_id
            JOIN orders on orderID = orderItem.order_id
            JOIN customer on customerID = orders.customer_id
            JOIN supplier on supplierID = supplier_id
            JOIN business on businessID = supplier.supplierBusiness_id
            JOIN product_process on product_processID = orderItem.product_process_id
            JOIN product on productID = product_process.product_id
            LEFT JOIN picture ON (picture.item_id = orderItem.item_id)
            LEFT JOIN automat FORCE INDEX (order_id) ON (automat.order_id = orderID AND barcode = garcode)
            LEFT JOIN locker ON (locker_id = lockerID)
            LEFT JOIN location ON (locker.location_id = locationID)
            LEFT JOIN assembly ON (assembly.order_id = orderID)

            WHERE orderItem.item_id != 0
                AND orders.business_id = ?
                AND orders.dateCreated > ?
                AND orders.dateCreated < ?
            GROUP BY orderItemID
            ORDER BY orderItemID DESC";
        $q = $this->reporting_database->query($getAllItems, array($this->business_id, $fromDate, $endDate));
        $content['allItems'] = $q->result();

        $this->setPageTitle('Un-Assembled Items');
        $this->renderAdmin('unassembled_items', $content);
    }

    public function customers_map($locationID = null)
    {
        $getLocation = "select * from location where locationID = $locationID and business_id = $this->business_id";
        $q = $this->reporting_database->query($getLocation);
        $content['location'] = $location = $q->result();
        if (empty($location)) {
            echo "This location does not belong to your business";
            exit;
        }

        $getCustomers = "select count(orderID) as orders, sum(closedGross) as gross, sum(closedDisc) as disc, max(report_orders.dateCreated) as lastOrder, customer.firstName,  customer.lastName, customer.address1, lat, lon, customer.email, customerID
                        from report_orders
                        join customer on customer_id = customerID
                        where report_orders.location_id = $locationID
                        and report_orders.business_id = $this->business_id
                        group by customerID";

        $q = $this->reporting_database->query($getCustomers);
        $content['customers'] = $q->result();
        
        foreach ($content['customers'] as $key => $customer) {
            $content['customers'][$key]->printableName = getPersonName($customer);
        }
        
        $this->template->add_js('https://maps.googleapis.com/maps/api/js?sensor=false&key=' . get_default_gmaps_api_key(), 'source');
        $this->template->add_js("/js/keydragzoom_packed.js");
        $this->template->write('page_title', 'Map of Customers for '.$location[0]->address, true);
        $this->data['content'] = $this->load->view('admin/reports/customers_map', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function driver_revenue()
    {
        $getDrivers = "select distinct employee_id, employee.*
                        from driverLog
                        join employee on employeeID = employee_id
                        where business_id = $this->business_id
                        order by firstName";

        $q = $this->reporting_database->query($getDrivers);
        $content['drivers'] = $q->result();

        if ($_POST) {
            $startDate = convert_to_gmt_aprax($_POST['startDate'], $this->business->timezone);
            $endDate = convert_to_gmt_aprax($_POST['endDate'], $this->business->timezone);
            if ($endDate < $startDate) {
                $temp = $startDate;
                $startDate = $endDate;
                $endDate = $temp;
            }

            $getOrders = "select distinct order_id, delTime, report_orders.*
                            from driverLog
                            join report_orders on orderID = order_id
                            where employee_id = $_POST[driver]
                            and delTime < '$endDate'
                            and delTime > '$startDate'
                            and delType = 'delivery'
                            order by delTime desc";
            $q = $this->reporting_database->query($getOrders);
            $content['orders'] = $q->result();
        }

        $this->template->write('page_title', 'Driver Revenue', true);
        $this->data['content'] = $this->load->view('admin/reports/driver_revenue', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function driver_phone()
    {
        $this->load->library('legacy_route');

        $this->template->write('page_title', 'Driver Phones', true);
        $this->data['content'] = $this->load->view('admin/reports/driver_phone', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function manage_machine()
    {
        $getMachines = "select * from machine where business_id = $this->business_id";
        $q = $this->reporting_database->query($getMachines);
        $content['machines'] = $q->result();

        $this->template->write('page_title', 'Manage Machines', true);
        $this->data['content'] = $this->load->view('admin/reports/manage_machines', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function add_machine()
    {
        $addMachines = "insert into machine (name, cycleTime, business_id)
                        values ('$_POST[name]', '$_POST[cycleTime]', '$this->business_id')";
        $q = $this->db->query($addMachines);
        set_flash_message("success", "New Machine Added");
        redirect('/admin/reports/manage_machine');
    }

    public function delete_machine()
    {
        $deleteMachines = "delete from machine where machineID = $_GET[machine]";
        $q = $this->db->query($deleteMachines);
        set_flash_message("success", "Machine Deleted");
        redirect('/admin/reports/manage_machine');
    }

    public function reset_machine()
    {
        $updateMachine = "update machine set startTime = now() where machineID = $_POST[machine]";
        $q = $this->db->query($updateMachine);
        set_flash_message("success", "Machine Restarted");
        redirect('/admin/reports/operations_dashboard');
    }

    public function ops_dashboard_message()
    {
        $content['message'] = get_business_meta($this->business_id, 'ops_dashboard_message');

        $this->template->write('page_title', 'Operations Dasboard Message', true);
        $this->data['content'] = $this->load->view('admin/reports/ops_dashboard_message', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function ops_dashboard_addCal()
    {
        $return = update_business_meta($this->business_id, 'ops_calendar_url', $this->input->get_post("calendar"));
        set_flash_message("success", "Calendar Updated");
        redirect('/admin/reports/operations_dashboard');
    }

    public function ops_dashboard_message_update()
    {
        update_business_meta($this->business_id, 'ops_dashboard_message', $this->input->get_post("message"));
        set_flash_message("success", "Message Updated");
        redirect('/admin/reports/operations_dashboard');
    }

    public function sales_tax_details($month = null)
    {
        $month = DateTime::createFromFormat("Y-m", $month)->format("Y-m");

        $getTax = "SELECT *
                    FROM orders
                    left join customer on customerID = `orders`.customer_id
                    where date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m') = '".$month."'
                    and orders.business_id = $this->business_id
                    and tax > 0
                    order by orders.dateCreated";

        $q = $this->reporting_database->query($getTax);
        $content['taxes'] = $q->result();
        $content['month'] = $month;
        $this->template->write('page_title', 'Sales Tax Details', true);
        $this->data['content'] = $this->load->view('admin/reports/sales_tax_details', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function sales_tax()
    {
        $from = $this->input->get('from');
        $to = $this->input->get('to');

        if (!$from) {
            $from = date('Y-01-01');
        }

        if (!$to) {
            $to = date('Y-m-d');
        }

        $content['from'] = $from;
        $content['to'] = $to;

        $to = convert_to_gmt_aprax("$to 23:59:59", $this->business->timezone);
        $from = convert_to_gmt_aprax("$from 00:00:00", $this->business->timezone);

        $getTax = "SELECT sum(closedGross) as gross, sum(closedDisc) as disc, sum(tax) as tax,
                    date_format(convert_tz(orders.dateCreated, 'GMT', '{$this->business->timezone}'), '%Y-%m') as month
                    FROM orders
                    WHERE orders.business_id = ?
                    AND orders.dateCreated > ?
                    AND orders.dateCreated <= ?
                    GROUP BY month
                    ORDER BY orders.dateCreated asc";
        $q = $this->reporting_database->query($getTax, array($this->business_id, $from, $to));
        $taxes = $q->result();

        $content['taxes'] = array();
        foreach ($taxes as $tax) {
            $content['taxes'][$tax->month] = $tax;
        }

        $getSubtax = "SELECT sum(amount) as taxAmount, orderTax.taxGroup_id, taxRate,
                    date_format(convert_tz(orders.dateCreated, 'GMT', '{$this->business->timezone}'), '%Y-%m') as month
                    FROM orders
                    JOIN orderTax ON (orderID = orderTax.order_id)
                    WHERE orders.business_id = ?
                    AND orders.dateCreated > ?
                    AND orders.dateCreated <= ?
                    GROUP BY taxGroup_id, taxRate, month";

        $q = $this->reporting_database->query($getSubtax, array($this->business_id, $from, $to));

        $content['subTaxes'] = array();
        $content['rates'] = array();
        $content['taxGroups'] = array();

        $this->load->model('taxgroup_model');

        $subTaxes = $q->result();
        foreach ($subTaxes as $subTax) {
            $content['subTaxes'][$subTax->month][$subTax->taxGroup_id] = $subTax;
            $content['rates'][$subTax->taxGroup_id] = $subTax->taxRate;

            if (!isset($content['taxGroups'][$subTax->taxGroup_id])) {
                $content['taxGroups'][$subTax->taxGroup_id] = $this->taxgroup_model->get_by_primary_key($subTax->taxGroup_id);
            }
        }

        $this->setPageTitle('Sales Tax');
        $this->renderAdmin('sales_tax', $content);
    }

    public function dl_invoices($customers_page = 0)
    {
        $getInvoices = "select *
                            from licenseInvoice
                            where business_id = $this->business_id
                            order by month desc;";
        $q = $this->reporting_database->query($getInvoices);
        $content['invoices'] = $q->result();

        $content["business_id"] = $business_id;
        $this->template->write('page_title', 'Drop Locker Invoices', true);
        $this->data['content'] = $this->load->view('admin/reports/dl_invoice', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function dl_invoices_view()
    {
        $this->load->library('license_invoicing');
        $content = $this->license_invoicing->showInvoice($this->input->get_post("invId"));
        $content['details'] = $this->input->get_post("details");

        $this->template->write('page_title', 'Drop Locker Invoices', true);
        $this->renderAdmin('license_invoice_view', $content);
    }

    public function how_hear()
    {
        if ($this->input->get_post("noOrders") == 1) {
            $havingQuery = '';
        } else {
            $havingQuery = ' HAVING orderCount > 0 ';
        }

        $addToQuery = '';
        $params = array($this->business_id);
        if (!empty($_POST['searchParam'])) {
            $addToQuery = 'and customer.how like ?';
            $params[] = '%'.$this->input->get_post("searchParam").'%';
        }

        $getCustomers = "select SQL_CALC_FOUND_ROWS customer.*, orderCount, lastOrder
                        from customer
                        left join reports_customer on reports_customer.customerID = customer.customerID
                        where customer.business_id = ?
                        ".$addToQuery."
                        ".$havingQuery."
                        order by customer.customerID desc
                        limit 500
                        ";

        $q = $this->reporting_database->query($getCustomers, $params);
        $content['customers'] = $q->result();


        $rowCount = $this->reporting_database->query("SELECT FOUND_ROWS() AS found_rows")->row();
        $content['rowCount'] = $rowCount->found_rows;

        $this->template->write('page_title', 'How did you hear about us', true);
        $this->data['content'] = $this->load->view('admin/reports/howHear', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function laundry_plans()
    {
        //get this business's wash & fold product ID
        $this->load->model('product_model');
        $WFProduct = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);
        $finishedPlans = "SELECT firstName, lastName, location_id, lp1.*, location.address, renew,
                             (select active from laundryPlan lp2 where lp2.customer_id = lp1.customer_id and lp2.active = 1 limit 1) as renewed,
                             (select concat(sum(qty),'|',sum(unitPrice * qty)) from orderItem join orders on orderID = order_id where customer_id = lp1.customer_id and product_process_id = " . $WFProduct . " and orders.dateCreated > lp1.startDate and orders.dateCreated < lp1.endDate) as lbsUsed
                             FROM laundryPlan lp1
                             join customer on customerID = lp1.customer_id
                             left join location on location_id = locationID
                             where startDate <> endDate
                             and lp1.business_id = $this->business_id ";

        if ($this->input->post('start_date')) {
            $start_date = new DateTime($this->input->post("start_date"));
            $finishedPlans .= "AND startDate >= '" . convert_to_gmt_aprax($start_date->format("Y-m-d")) . "' ";
            $content['start_date'] = $this->input->post("start_date");
        } else {
            $content['start_date'] = date("m/d/Y", strtotime("-1 months"));
        }

        if ($this->input->post('end_date')) {
            $end_date = new DateTime($this->input->post("end_date"));
            $finishedPlans .= "AND startDate <= '" . convert_to_gmt_aprax($end_date->format("Y-m-d")) . "' ";
            $content['end_date'] = $this->input->post('end_date');
        }
        
        $finishedPlans .=   "order by endDate desc, customer_id";

        if (!empty($this->input->post("start_date")) || !empty($this->input->post("end_date"))) {
            $q = $this->reporting_database->query($finishedPlans);
            $content['plans'] = $plans = $q->result();

            foreach ($plans as $plan) {
                $volume = explode('|', $plan->lbsUsed);
                $content['lbsUsed'][$plan->laundryPlanID] = $volume[0];
                $content['spend'][$plan->laundryPlanID] = empty($volume[1]) ? '' : $volume[1];
                if ($volume[0] < $plan->pounds) {
                    $content['rate'][$plan->laundryPlanID] = $volume[0] ? $plan->price / $volume[0] : 0;
                } else {
                    $effectiveRate = $volume[1] / $volume[0];
                    $totSpend = (($volume[0] - $plan->pounds) * $effectiveRate) + $plan->price;
                    $content['rate'][$plan->laundryPlanID] = $totSpend / $volume[0];
                }
            }
        }

        $this->template->write('page_title', 'Laundry Plans', true);
        $this->data['content'] = $this->load->view('admin/reports/laundry_plans', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function terminate_plan()
    {
        $type = empty($this->input->post("type"))?false:"everything_plan";
        if (!$type) {
            //make all plans for this customer inactive.  This will make it so that no plans will renew
            $updatePlans = "update laundryPlan set active = 0, renew = 0 where customer_id = ".$this->input->post("customer_id")." and type='laundry_plan'";
            $q = $this->db->query($updatePlans);

            set_flash_message("success", "All laundry plans for this customer have been terminated.<br>You may now go into their account and create a new plan.<br>Any open discounts still remain.");
            redirect('admin/reports/laundry_plans');
        } elseif ($type=="everything_plan") {
            //make all plans for this customer inactive.  This will make it so that no plans will renew
            $updatePlans = "update laundryPlan set active = 0, renew = 0 where customer_id = ".$this->input->post("customer_id")." and type='everything_plan'";
            $q = $this->db->query($updatePlans);

            set_flash_message("success", "All everything plans for this customer have been terminated.<br>You may now go into their account and create a new plan.<br>Any open discounts still remain.");
            redirect('admin/reports/everything_plans');
        } elseif ($type=="product_plan") {
            //make all plans for this customer inactive.  This will make it so that no plans will renew
            $updatePlans = "update laundryPlan set active = 0, renew = 0 where customer_id = ".$this->input->post("customer_id")." and type='product_plan'";
            $q = $this->db->query($updatePlans);

            set_flash_message("success", "All product plans for this customer have been terminated.<br>You may now go into their account and create a new plan.<br>Any open discounts still remain.");
            redirect('admin/reports/product_plans');
        }
    }

    public function everything_plans()
    {
        //get this business's wash & fold product ID
        $this->load->model('product_model');

        $finishedPlans = "SELECT firstName, lastName, location_id, lp1.*, location.address, renew,
                            (select active from laundryPlan lp2 where lp2.customer_id = lp1.customer_id and lp2.active = 1 and lp2.type = 'everything_plan' limit 1) as renewed,
                            (select concat(sum(qty),'|',sum(unitPrice * qty)) from orderItem join orders on orderID = order_id where customer_id = lp1.customer_id and orders.dateCreated > lp1.startDate and orders.dateCreated < lp1.endDate) as amountUsed
                            FROM laundryPlan lp1
                            join customer on customerID = lp1.customer_id
                            left join location on location_id = locationID
                            where startDate <> endDate
                            and lp1.business_id = $this->business_id
                            and lp1.type = 'everything_plan'
                            order by endDate desc, customer_id
                            limit 1000";
        $q = $this->reporting_database->query($finishedPlans);
        $content['plans'] = $plans = $q->result();

        foreach ($plans as $plan) {
            $volume = explode('|', $plan->lbsUsed);
            $content['amountUsed'][$plan->laundryPlanID] = $volume[0];
            $content['spend'][$plan->laundryPlanID] = empty($volume[1]) ? '' : $volume[1];
            if ($volume[0] < $plan->credit_amount) {
                $content['rate'][$plan->laundryPlanID] = $volume[0] ? $plan->price / $volume[0] : 0;
            } else {
                $effectiveRate = $volume[1] / $volume[0];
                $totSpend = (($volume[0] - $plan->credit_amount) * $effectiveRate) + $plan->price;
                $content['rate'][$plan->laundryPlanID] = $totSpend / $volume[0];
            }
        }

        $this->template->write('page_title', 'Everything Plans', true);
        $this->data['content'] = $this->load->view('admin/reports/everything_plans', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function product_plans()
    {
        //get this business's wash & fold product ID
        $this->load->model('product_model');

        $finishedPlans = "SELECT firstName, lastName, location_id, lp1.*, location.address, renew,
                            (select active from laundryPlan lp2 where lp2.customer_id = lp1.customer_id and lp2.active = 1 and lp2.type = 'everything_plan' limit 1) as renewed,
                            (select concat(sum(qty),'|',sum(unitPrice * qty)) from orderItem join orders on orderID = order_id where customer_id = lp1.customer_id and orders.dateCreated > lp1.startDate and orders.dateCreated < lp1.endDate) as amountUsed
                            FROM laundryPlan lp1
                            join customer on customerID = lp1.customer_id
                            left join location on location_id = locationID
                            where startDate <> endDate
                            and lp1.business_id = $this->business_id
                            and lp1.type = 'product_plan'
                            order by endDate desc, customer_id
                            limit 1000";
        $q = $this->reporting_database->query($finishedPlans);
        $content['plans'] = $plans = $q->result();

        foreach ($plans as $plan) {
            $volume = explode('|', $plan->lbsUsed);
            $content['amountUsed'][$plan->laundryPlanID] = $volume[0];
            $content['spend'][$plan->laundryPlanID] = empty($volume[1]) ? '' : $volume[1];
            if ($volume[0] < $plan->credit_amount) {
                $content['rate'][$plan->laundryPlanID] = $volume[0] ? $plan->price / $volume[0] : 0;
            } else {
                $effectiveRate = $volume[1] / $volume[0];
                $totSpend = (($volume[0] - $plan->credit_amount) * $effectiveRate) + $plan->price;
                $content['rate'][$plan->laundryPlanID] = $totSpend / $volume[0];
            }
        }

        $this->template->write('page_title', 'Product Plans', true);
        $this->data['content'] = $this->load->view('admin/reports/product_plans', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function ticket_report()
    {
        $supportTickets = "SELECT date_format(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m') as month, ticketProblem.name, count(ticketID) as tickets, ticketProblem_id
                            FROM ticket
                            join ticketProblem on ticketProblem_id = ticketProblemID
                            where ticket.business_id = $this->business_id
                            group by date_format(convert_tz(dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m'), name
                            order by dateCreated desc";
        $q = $this->reporting_database->query($supportTickets);
        $content['tickets'] = $tickets = $q->result();

        foreach ($tickets as $ticket) {
            $content['problems'][$ticket->name] = $ticket->ticketProblem_id;
            $content['months'][$ticket->month] = 1;
            $content['ticket'][$ticket->month][$ticket->name] = $ticket->tickets;
        }

        $this->template->write('page_title', 'Support Tickets', true);
        $this->data['content'] = $this->load->view('admin/reports/ticket_report', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function pos_activity()
    {
        if ($this->input->is_ajax_request()) {
            $ajax = "ajax_" . __FUNCTION__;
            $this->$ajax();
            die();
        }
        
        $template = __FUNCTION__;
        $title = ucwords(str_replace('_', ' ', __FUNCTION__));
        
        $setups = array(
            "feature" => __FUNCTION__,
            "title" => $title,
        );
        
        //--------------------
        
        $this->load->model('cash_register_model');
        $cash_registers_data = $this->cash_register_model->getForBusiness($this->business_id);
        
        $cash_registers = array();
        $cash_register_id = null;
        foreach ($cash_registers_data as $data) {
            $id = $data->cash_registerID;
            $label = $data->name . " ({$data->address})";
            
            $cash_registers[$id] = $label;
            
            if (!$cash_register_id) {
                $cash_register_id = $id;
            }
        }
        
        $defaults = array(
            "start_date" => date("Y-m-d", time() - 60 * 60 * 24),
            "end_date" => date("Y-m-d"),
            "cash_registers" => $cash_registers,
            "cash_register_id" => $cash_register_id,
        );
        
        $content = array_merge($setups, $defaults, (array)$this->input->get());

        $this->template->write('page_title', $title, true);
        $this->renderAdmin($template, $content);
    }

    protected function ajax_pos_activity()
    {
        $this->load->model("cash_register_record_model");

        $records = array(
            "sEcho" => (int)$this->input->get("sEcho"),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );
        
        $context_local = array(
            'from_date' => $this->input->get("start_date"),
            'to_date' => $this->input->get("end_date"),
        );
        
        $pagination = array(
            'count' => $this->input->get("iDisplayLength"),
            'offset' => $this->input->get("iDisplayStart"),
        );
        
        $filter = array(
            
        );
        
        $order = array(
            'index' => $this->input->get("iSortCol_0"),
            'direction' => $this->input->get("sSortDir_0"),
        );

        $result = $this->cash_register_record_model->getRecords(
            $this->input->get("cash_register_id"),
            $context_local,
            $pagination,
            $filter,
            $order,
            true
        );

        $records["aaData"] = $result['items'];

        //add custom data/format to rows
        $dateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat");
        foreach ($records["aaData"] as $index => $item) {
            $records["aaData"][$index]->date = convert_from_gmt_aprax($item->date_created, $dateFormat);
        }

        //return datables info
        $records["iTotalRecords"] = $result['count'];
        $records["iTotalDisplayRecords"] = $records["iTotalRecords"];

        echo json_encode($records);
    }


    /**
     * List customers
     */
    protected function list_business_customers($offset = 0, $search=false, $business_id = null)
    {
        $this->load->model("customer_model");
        $limit = 20;
        
        $options = array();
        $options['business_id'] = empty($business_id)?$this->business_id:$business_id;

        if (!empty($search)) {
            $options["with_search"]  = $search;
        }

        $options['select'] = 'count(*) as total';
        $total = $this->customer_model->get_business_customers($options);
        $total = empty($total)?0:$total[0]->total;

        $this->load->library('pagination');
        $config['base_url'] = '/admin/reports/dl_invoices/';
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;
        $config['num_links'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);

        $content['total'] = $total;
        $content['paging'] = $this->pagination->create_links();

        $options['limit'] = $limit;
        $options['offset'] = $offset;
        $options['sort'] = 'lastName';
        $options['select'] = 'customerID, customer.firstName, customer.lastName, customer.phone, customer.email, customer.username, customer.signupDate, customer.sms, customer.default_business_language_id ';
        
        $customers = $this->customer_model->get_business_customers($options);
        $content['customers'] = $customers;

        $content['timezone'] = $this->business->timezone;

        return $content;
    }
}
