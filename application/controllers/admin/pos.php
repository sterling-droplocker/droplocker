<?php

use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;

class Pos extends MY_Admin_Controller
{
    public $submenu = 'orders';

	public function index()
	{
	    $this->load->model('location_model');
	    $locations = $this->location_model->get_all_as_codeigniter_dropdown_for_business($this->business_id);

	    $customer_id = null;
	    if (!empty($_GET['customer_id'])) {
	        $customer_id = intval($_GET['customer_id']);
            $customer = $this->getCustomerById($customer_id);
	    }

	    // get location from cookie
	    $location_id = $this->input->cookie("pos_location");
	    if ($location_id) {
    	    $location = Location::search(array(
    	        'locationID' => $location_id,
    	        'business_id' => $this->business_id,
    	    ));

    	    // delete cookie if location not found
    	    if (!$location) {
    	        $location_id = null;
    	        $this->input->set_cookie('pos_location', 0);
    	    }
	    }

	    $content = compact('locations', 'location_id', 'location', 'customer_id', 'customer');
	    $this->renderAdmin('index', $content);
	}

	public function set_location()
	{
        if (empty($_POST['location_id'])) {
            set_flash_message_now("error", "Missing location_id");
            redirect("/admin/pos");
        }

        $location_id = $_POST['location_id'];
        $location = Location::search(array(
            'locationID' => $location_id,
            'business_id' => $this->business_id,
        ));

        if (!$location) {
            set_flash_message_now("error", "Location not found '$location_id'");
            redirect("/admin/pos");
        }

        //save location_id for a year
        $this->input->set_cookie('pos_location', $location_id, 86400 * 365);

        redirect("/admin/pos");
	}

    public function create_order_by_bag()
    {
        checkAccess('add_order');

        if (empty($_POST['bag_number'])) {
            set_flash_message("error", "Missing bag_number");
            redirect("/admin/pos");
        }

        if (empty($_POST['location_id'])) {
            set_flash_message("error", "Missing location_id");
            redirect("/admin/pos");
        }

        $bagNumber = trim($_POST['bag_number']);
        $bagNumber = ltrim($bagNumber, "0");
        $location_id = $_POST['location_id'];

        $this->newOrder($bagNumber, $location_id);
    }


    public function create_order_by_customer()
    {
        checkAccess('add_order');

        if (empty($_POST['customer_id'])) {
            set_flash_message("error", "Missing customer ID");
            redirect("/admin/pos");
        }

        if (empty($_POST['location_id'])) {
            set_flash_message("error", "Missing location_id");
            redirect("/admin/pos");
        }

        $location_id = $_POST['location_id'];
        $customer_id = $_POST['customer_id'];
        $customer = $this->getCustomerById($customer_id);

        $bagNumber = $this->addBagToCustomer($customer_id);

        $content = compact('customer', 'location_id', 'bagNumber');
        $this->renderAdmin("new_bag", $content);
    }

    public function create_customer()
    {
        $location_id = $_POST['location_id'];
        $firstName = !empty($_POST['firstName']) ? $_POST['firstName'] : 'Anonymous';
        $lastName = !empty($_POST['lastName']) ? $_POST['lastName'] : 'Customer';
        $phone = !empty($_POST['phone']) ? $_POST['phone'] : '';
        $sms = !empty($_POST['sms']) ? $_POST['sms'] : '';

        // copy phone to sms
        if (!empty($_POST['copy'])) {
            $sms = $phone;
        }
        $promo = !empty($_POST['promo_code']) ? $_POST['promo_code'] : null;

        if (!empty($_POST['email'])) {
            $email = trim($_POST['email']);

            $customer = Customer::search(array(
                'business_id' => $this->business_id,
                'email' => $email,
            ));

            if ($customer) {
                set_flash_message("error", "A customer with email '<a href='/admin/customers/detail/{$customer->customerID}' target='_blank'>$email</a>' already exists in this business");
                redirect("/admin/pos?customer_id=" . $customer->customerID);
            }
        } else {
            $email = 'temp' . time() . '@' . $this->business->website_url;
        }

        $password = md5(uniqid());
        $how = "POS signup";
        $signup = "admin POS";

        $this->load->model('customer_model');

        if (!$newCustomer_id = $this->customer_model->new_customer($email, $password, $this->business_id, $how, $signup)) {
            set_flash_message("error", "An error occured creating the customer account.");
            redirect("/admin/pos");
        }

        // save customer first and last names
        $this->customer_model->save(array(
            'customerID' => $newCustomer_id,
            'firstName'  => $firstName,
            'lastName'   => $lastName,
            'sms'        => $sms,
            'phone'      => $phone,

        ));

        if ($promo) {
            $this->load->library('coupon');
            $response = $this->coupon->applyCoupon($promo, $newCustomer_id, $this->business_id);
        }

        $bagNumber = $this->addBagToCustomer($newCustomer_id);
        $customer = new Customer($newCustomer_id);
        $customer->default_business_language_id = $this->business->default_business_language_id;
        $customer->save();
        $new_customer = true;

        $content = compact('customer', 'location_id', 'bagNumber', 'new_customer');
        $this->renderAdmin("new_bag", $content);
    }

    /**
     * Creates a new bag and assing it to the customer
     *
     * @param int $customer_id
     * @return int Bag number
     */
    protected function addBagToCustomer($customer_id)
    {
        $notes = null;
        $bagNumber = Bag::get_max_bagNumber() + 1;
        Bag::set_max_bagNumber($bagNumber);

        $this->load->model('bag_model');
        $this->bag_model->addBagToCustomer($customer_id, $bagNumber, $this->business_id, $notes);

        return $bagNumber;
    }

    /**
     * Creates an order in the location using a bagNumber
     *
     * @param int $bagNumber
     * @param int $location_id
     */
    protected function newOrder($bagNumber, $location_id)
    {
        $locker_id = 0;
        $locker = Locker::search(array(
            'lockerName' => 'inProcess',
            'location_id' => $location_id,
        ));

        if (!$locker) {
            $locker = new Locker();
            $locker->lockerName = 'inProcess';
            $locker->location_id = $location_id;
            $locker->lockerStatus_id = 1; //In Service
            $locker->lockerStyle_id = 7; //In Process
            $locker->lockerLockType_id = 2; //Electronic
            $locker->save();
        }
        $locker_id = $locker->lockerID;

        $maxBagNumber = Bag::get_max_bagNumber();
        if ($bagNumber > $maxBagNumber) {
            set_flash_message("error", "You have entered a bag that is greater then the highest printed bag. This will cause duplicate bag issues in the future. The highest bag number printed is [".$maxBagNumber."]. To solve this problem, <a target='_parent' href='/admin/orders/bag_tags'>print out a new bag number</a>.");
            return redirect("/admin/pos");
        }

        $bag = Bag::search(array(
            'business_id' => $this->business_id,
            'bagNumber' => $bagNumber
        ));

        if (empty($bag)) {
            set_flash_message("error", "Bag '$bagNumber' not found");
            return redirect("/admin/pos");
        }

        $customer_id = $bag->customer_id;
        $customer = new Customer($bag->customer_id);

        if ($customer->hold) {
            set_flash_message("error", "Can not create order because there is a hold on <a href='/admin/customers/detail/{$customer->customerID}'> ".  getPersonName($customer) ."</a> account");
            return redirect("/admin/pos");
        }

        $this->load->model('order_model');
        $openOrders = $this->order_model->getOpenOrders($bagNumber, $this->business_id);
        if ($openOrders) {
            set_flash_message("error", 'There is already an open order for this bag. <a target="_blank" href="/admin/orders/details/'.$openOrders[0]->orderID.'">Order '.$openOrders[0]->orderID.'</a>');
            return redirect("/admin/pos");
        }

        $employee_id = get_employee_id($this->session->userdata('buser_id'));
        $notes = "";
        $options = array();
        $options['customer_id'] = $customer_id;

        $claims = Claim::search_aprax(array(
            "business_id" => $this->business_id,
            "customer_id" => $customer_id,
            "locker_id" => $locker_id,
            "active" => 1
        ));

        if ($claims) {
            $claim = $claims[0];
            $options['claimID'] = $claim->claimID;
        }

        // check for multiple claims
        if (count($claims) > 1) {
            $location = new Location($location_id);

            $error_message = "Multiple claims were found for customer '".  getPersonName($customer)." ({$customer->customerID})' in locker '{$locker->lockerName}' at location '{$location->address}'. Only one claim should exist.";
            send_admin_notification("Customer has multiple claims error", $error_message);
            set_flash_message("error", $error_message);

            return redirect("/admin/pos");
        }

        // create the order
        $order_id = $this->order_model->new_order(
            $location_id,
            $locker_id,
            $bagNumber,
            $employee_id,
            $notes,
            $this->business_id,
            $options
        );

        redirect("/admin/orders/details/$order_id?new_order=true");
    }


    /**
     * Gets a customer by it's ID
     *
     * @param int $customer_id
     * @return Customer
     */
    protected function getCustomerById($customer_id)
    {
        try {
            $customer = new Customer($customer_id, false);
        } catch (Exception $e) {
            set_flash_message('error', "Customer [$customer_id] does not exist");
            return redirect("/admin/pos");
        }

        if ($customer->business_id != $this->business_id) {
            set_flash_message("error", "This customer does not belong to your business.");
            return redirect("/admin/pos");
        }

        return $customer;
    }
}
