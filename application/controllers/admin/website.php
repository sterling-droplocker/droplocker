<?php
/**
 * Location: admin/website
 */
class Website extends MY_Admin_Controller
{
    public $buser_id;
    public $business;
    public $business_id;

    public function index()
    {
        //find out which widgets to show
        $getWidgets = "select * from dashboard
                        left join widget on widgetID = widget_id
                        where employee_id = $this->buser_id
                        and module = 'Website'
                        order by location";
        $result = $this->db->query($getWidgets);
        $content['widgets'] = $result->result();

        $this->load->view('admin/blank', '', true);
        $this->data['content'] = $this->load->view('admin/index', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function details()
    {
        $this->load->model('website_model');
        $this->website_model->select = 'status';
        $this->website_model->business_id = $this->business_id;
        $website = $this->website_model->get();

        $content['status'] = $website[0]->status;

        $this->renderAdmin('details', $content);
    }
    /**
     * Renders the interface for managing the iphone app settings.
     */
    public function manage_iphone_app()
    {
        $this->load->model("app_model");
        $app = $this->app_model->get_by_business_id($this->business_id);
        if (empty($app)) {
            $content = array(
                "name" => "",
                "token" => "",
                "secret" => "",
                "active" => "",
                "description" => "",
                "keywords" => "",
                "support_url" => "",
                "marketing_url" => "",
                "development_mode" => "",
                "app_large_icon_path" => "",
                "app_splash_path" => "",
                "android_feature_graphic_path" => "",
            );
        } else {
            $content = array(
                "name" => $app->name,
                "token" => $app->token,
                "secret" => $app->secret,
                "active" => $app->active,
                "description" => $app->description,
                "keywords" => $app->keywords,
                "support_url" => $app->support_url,
                "marketing_url" => $app->marketing_url,
                "development_mode" => $app->development_mode,
                "app_large_icon_path" => $app->app_large_icon_path,
                "app_splash_path" => $app->app_splash_path,
                "android_feature_graphic_path" => $app->android_feature_graphic_path,
            );
        }

        $this->renderAdmin('manage_iphone_app', $content);
    }

    /**
     * Generate ZIP file with app assets.
     */
    public function download_app_assets()
    {
        $this->load->model("app_model");
        $app = $this->app_model->get_by_business_id($this->business_id);

        if (empty($app)) {
            set_flash_message('error', 'Please, config your app first');
            return $this->goBack('/admin/website/manage_iphone_app');
        }

        if (empty($app->app_large_icon_path) || empty($app->android_feature_graphic_path) || empty($app->app_splash_path)) {
            set_flash_message('error', 'Please, upload all images first');
            return $this->goBack('/admin/website/manage_iphone_app');
        }

        $types = array(
            "icon" => $app->app_large_icon_path,
            "feature" => $app->android_feature_graphic_path,
            "splash" => $app->app_splash_path
        );

        foreach ($types as $k=>$v) {
            $file_name = "/tmp/".$k;
            if (file_exists($file_name)) {
                unlink($file_name);
            }
            file_put_contents($file_name, file_get_contents($v));
            $types[$k] = $file_name;
        }

        $this->load->config("website");
        $images = $this->config->item("app_assets");
        //$images = $this->config->item("test_app_assets");

        try {
            $this->load->library("simple_image");

            /* Parse images */
            $base_path = $this->config->item("base_path");
            recursive_rmdir($base_path);
            foreach ($images as $i) {
                $path = $base_path.$i["path"].'/';
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }

                foreach ($i["files"] as $file=>$attrs) {
                    if (!empty($types[$attrs['type']])) {
                        $filename = $types[$attrs['type']];
                        $img = $this->simple_image->set_image(array("filename" => $filename));
                        $dimensions = $this->simple_image->get_dimensions();

                        //if image already has required dimensions just copy
                        if ($dimensions["width"] == $attrs['width'] && $dimensions["height"] == $attrs['height']) {
                            copy($filename, $path.$file);
                            continue;
                        }

                        $img->thumbnail($attrs['width'], $attrs['height'])->save($path.$file);
                    }
                }
            }


            $zip_file = $base_path.$this->config->item("zip_file_name");

            /* Remove ZIP file */
            if (file_exists($zip_file)) {
                unlink($zip_file);
            }

            /* Create ZIP file */
            $zip = new ZipArchive();
            $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($base_path),
                RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($base_path));

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

            // Zip archive will be created only after closing object
            $zip->close();

            /* Download ZIP file */
            if (!file_exists($zip_file)) {
                set_flash_message('error', "Something went wrong while creating zip file.");
                return $this->goBack('/admin/website/manage_iphone_app');
            }

            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename=".$this->config->item("zip_file_name"));
            header("Content-length: " . filesize($zip_file));
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile($zip_file);
            die();

        } catch(Exception $e) {
            set_flash_message('error', $e->getMessage());
            return $this->goBack('/admin/website/manage_iphone_app');
        }

    }

    /**
     * Renders the interface and functionality for uploading new logos and assigning logos to placements.
     */
    public function edit_logo()
    {
        $this->load->helper('form');

        if ($this->input->post("submit")) {
            $config['upload_path']   = './images/logos/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	     = '10240';
            $config['max_width']     = '5000';
            $config['max_height']    = '2000';
            $config['file_name']     = strtotime("now");

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $error = array('error' => $this->upload->display_errors());
                set_flash_message('error', $error);
                return $this->goBack('/admin/website/edit_logo');
            }

            $data = array('upload_data' => $this->upload->data());
            $filename = $data['upload_data']['file_name'];
            $path = $data['upload_data']['file_path'];

            // If in bizzie mode, update logos for all businesses
            if (in_bizzie_mode() && is_superadmin()) {
                $this->load->model("business_model");
                $businesses = $this->business_model->get_aprax();
                set_flash_message('success', $filename.' has been uploaded for all businesses');
            } else {
                $businesses = array($this->business);
                set_flash_message('success', $filename.' has been uploaded');
            }

            foreach ($businesses as $business) {
                $image = new \App\Libraries\DroplockerObjects\Image();
                $image->filename = $filename;
                $image->path = $path;
                $image->business_id = $business->businessID;
                $image->save();
            }

            $this->goBack('/admin/website/edit_logo');
        }

        //If the server is in Bizzie mode, then we retrieve the master business.
        // Otherwise, we retrieve the business associated with the current user.
        if (in_bizzie_mode()) {
            try {
                $master_business = get_master_business();
            } catch (\User_Exception $e) {
                set_flash_message("error", $e->getMessage());
                $this->goBack("/admin/superadmin");
            }
            $business_id = $master_business->businessID;
        } else {
            $business_id = $this->business_id;
        }

        $this->load->model('image_model');
        $content['images'] = $this->image_model->get_aprax(array(
            'business_id' => $business_id,
        ));


        //The following variable retrieves all placements set with this business.
        $content['business_placements'] = $this->image_model->get_business_placements($business_id);

        $this->load->model('placement_model');
        $content['placements'] = $this->placement_model->get_all();

        if (in_bizzie_mode()) {
            $this->setPageTitle('Manage Master Business Logos');
        } else {
            $this->setPageTitle('Manage Images');
        }

        $this->renderAdmin('edit_logo', $content);
    }

    /**
     * Sets the image placement settings for the business.
     * Expects the following POST parameters in the following format
     *  placement_id => image_id
     */
    public function update_placements()
    {
        $this->load->model('image_placement_model');

        foreach ($_POST as $placement_id => $image_id) {
            //The following statemnt updates the image placements for all businesses if the server is in Bizzie mode
            if (in_bizzie_mode()) {
                $businesses = $this->business_model->get();
                foreach ($businesses as $business) {
                    $query = $this->db->query("SELECT image_placementID FROM image_placement INNER JOIN image ON image_placement.image_id=image.imageID WHERE placement_id=$placement_id AND image.business_id={$business->businessID}");
                    $result = $query->result();
                    if ($result) {
                        $this->image_placement_model->placement_id = $placement_id;
                        $this->image_placement_model->image_id = $image_id;
                        $this->image_placement_model->where = array("image_placementID" => $result[0]->image_placementID);
                        $status = $this->image_placement_model->update();
                    } else {
                        $this->image_placement_model->image_id = $image_id;
                        $this->image_placement_model->placement_id = $placement_id;
                        $status = $this->image_placement_model->insert();
                    }
                }

            } else {
                $query = $this->db->query("SELECT image_placementID FROM image_placement INNER JOIN image ON image_placement.image_id=image.imageID WHERE placement_id=$placement_id AND image.business_id={$this->business_id}");
                $result = $query->result();
                if ($result) {
                    $this->image_placement_model->placement_id = $placement_id;
                    $this->image_placement_model->image_id = $image_id;
                    $this->image_placement_model->where = array("image_placementID" => $result[0]->image_placementID);
                    $status = $this->image_placement_model->update();
                } else {
                    $this->image_placement_model->image_id = $image_id;
                    $this->image_placement_model->placement_id = $placement_id;
                    $status = $this->image_placement_model->insert();
                }
            }
        }
        set_flash_message('success', 'Image placement settings updated');

        $this->goBack("/admin/website/edit_logo");
    }

    /**
     * Updates the website settings for a business, or all businessesif the server is in Bizzie mode
     * If making a POST request, this action expects the following POST parameters
     *  gift_card
     *  registerPage
     *  limit_geo_country
     *  copy_phone_to_sms
     *  list_laundry_plans
     */
    public function features()
    {
        $websites = \App\Libraries\DroplockerObjects\Website::search_aprax(array("business_id" => $this->business->businessID));
        $website = $websites[0];
        $content['facebook_share_rewards_program_thumbnail_url'] = $website->facebook_share_rewards_program_thumbnail_url;

        $businesses = array($this->business);
        if (isset($_POST['submit'])) {
            //If the server is in Bizze mode, we update all bussinesess in the system
            if ($in_bizzie_mode = in_bizzie_mode()) {
                $this->load->model("business_model");
                $businesses = $this->business_model->get();
            }

            foreach ($businesses as $business) {
                $business_websites = \App\Libraries\DroplockerObjects\Website::search_aprax(array("business_id" => $business->businessID));
                $business_website = $business_websites[0];
                update_business_meta($business->businessID, 'sell_gift_cards', $_POST['gift_card']);
                update_business_meta($business->businessID, 'registerLandingPage', $_POST['registerPage']);
                update_business_meta($business->businessID, 'limit_geo_country', $_POST['limit_geo_country']);
                update_business_meta($business->businessID, 'copy_phone_to_sms', $_POST['copy_phone_to_sms']);
                update_business_meta($business->businessID, 'show_second_sms', $_POST['show_second_sms']);
                update_business_meta($business->businessID, 'list_laundy_plans', $_POST['list_laundy_plans']);

                $business_website->facebook_share_rewards_program_thumbnail_url = $this->input->post("facebook_share_rewards_program_thumbnail_url");
                $business_website->save();
            }

            if ($in_bizzie_mode) {
                set_flash_message("success", "Updated all businesses' website features");
            } else {
                set_flash_message('success', 'Updated website features');
            }

            $this->goBack("/admin/website/features");
        }
        $this->template->write("page_title", "Manage Website Features");
        $this->data['content'] = $this->load->view('admin/website/features', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
    * Deletes a image from the specified business.
    * Expects the image ID
    */
    public function delete_image($imageID = null)
    {
        $this->load->model("image_model");
        $this->load->model("image_placement_model");

        if (!is_numeric($imageID)) {
            throw new Exception("The image ID must be passed as segment 4 and be numeric.");
        }
        $image = new \App\Libraries\DroplockerObjects\Image($imageID);
        if (file_exists($image->path . $image->filename)) {
            unlink($image->path . $image->filename);
        }

        $this->image_model->options = array("filename" => $image->filename);
        $this->image_model->delete();

        $this->image_placement_model->options = array("image_id" => $image->imageID);
        $this->image_placement_model->delete();

        set_flash_message("success", "Deleted image {$image->imageID} and associated placements");
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * The following action updates a particular website for a buinsess, or all business websites if the server is in Bizzie mode
     * Can take the following POST Parameters:
     *  header
     *  footer
     *  title
     *  javascript
     *  stylescript
     *
     */
    public function edit_website_code()
    {
        $this->load->model('website_model');
        //If the server is in Bizzie mode, only the superadmin is allowed to edit the website code.
        if (in_bizzie_mode() && !is_superadmin()) {
            set_flash_message("error", "This functionality is only accessible by Superadmin users when the system is in Bizzie mode");

            $this->goBack("/admin/website");
        }

        $website = \App\Libraries\DroplockerObjects\Website::search(array(
            "business_id" => $this->business_id
        ));

        // if no business website is defined yet, create one
        if (empty($website)) {
            $website = new \App\Libraries\DroplockerObjects\Website();
            $website->business_id = $this->business_id;
            $website->save();
        }

        if ($this->input->post()) {
            $website->setProperties($this->input->post());
            $website->save();
            set_flash_message("success", "Updated {$this->business->companyName} Website Configuration");
            redirect($this->uri->uri_string());
        }

        $content['website'] = $website;

        $this->config->load('template');
        $templates = $this->config->item('templates');
        $templates = array_combine(array_keys($templates), array_keys($templates));
        $templates[""] = "--EMPTY--";
        ksort($templates);
        $content['templates'] = $templates;

        $this->setPageTitle("{$this->business->companyName} Website Configuration");
        $this->renderAdmin('edit_website_code', $content);
    }

    /**
     * Allows a business to add google analytics to their droplocker account section.
     * The business is still responsible for adding GA to their normally hosted site
     *
     * This sets a business_meta key: google_analytics
     */
    public function analytics()
    {
        if ($this->input->post("submit")) {
            update_business_meta($this->business_id, "google_analytics", $_POST['ga']);
            set_flash_message("success", "Updated website analytics");

            $this->goBack("/admin/website/analytics");
        }

        $content['ga'] = get_business_meta($this->business_id, 'google_analytics');
        $this->renderAdmin('analytics', $content);
    }
}
