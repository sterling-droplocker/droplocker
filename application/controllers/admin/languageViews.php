<?php

use App\Libraries\DroplockerObjects\LanguageView;
use App\Libraries\DroplockerObjects\LanguageKey;

class LanguageViews extends MY_Admin_Controller
{

    /**
     * Expects the following POST parameters
     *  name: the name of the new language view
     */
    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules("name", "Language View Name", "required|is_languageView_name_unique");
        if ($this->form_validation->run()) {
            $languageView = new LanguageView();
            $languageView->name = $this->input->post("name");
            $languageView->save();
            $languageKeyNames = array_filter($this->input->post("languageKeyName"));
            $defaultTexts = array_filter($this->input->post("defaultText"));
            
            if (count($languageKeyNames) == count($defaultTexts)) {
                foreach ($languageKeyNames as $index => $languageKeyName) {
                    LanguageKey::create(array("defaultText" => $defaultTexts[$index], "name" => $languageKeyName, "languageView_id" => $languageView->{LanguageView::$primary_key}));
                }
            } else {
                set_flash_mesage("error", "The number of language key names must match the number of default texts");
                redirect_with_fallback("/admin/superadmin/manage_languageKeys_and_languageViews");
            }
            set_flash_message("success", "Created new language view '{$languageView->name}'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_language_views");
    }

    /**
     * The following action is intended to be used with jquery.jeditable.
     * Expects the following POST parameters
     *  property : Must be a property of the language view entity.
     *  languageViewID
     * Returns a response formatted for jquery.jeditable
     */
    public function update()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("languageViewID", "Language View ID", "required|does_languageView_exist");
        $this->form_validation->set_rules("property", "Property", "required");
        if ($this->input->post("property") == "name") {
            $this->form_validation->set_rules("value", "Name", "required|is_languageView_name_unique");
        } else {
            $this->form_validaiton->set_rules("value", "Value", "required");
        }
        if ($this->form_validation->run()) {
            $property = $this->input->post("property");
            $value = $this->input->post("value");
            $languageView = new LanguageView($this->input->post("languageViewID"));
            $languageView->$property = $value;
            $languageView->save();
            echo $languageView->name;
        } else {
            echo validation_errors();
        }
    }

    /**
     * The following action deletes a specified language view
     * Expects the following GET parameters
     *  languageViewID: the primary key fo the language view to be deleted
     */
    public function delete()
    {
        if (isset($_GET)) {
            $_POST = $_GET;
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules("languageViewID", "Language View ID", "required|is_natural_no_zero|does_languageView_exist|is_languageView_deletable");
        if ($this->form_validation->run()) {
            $languageView = new LanguageView($this->input->post("languageViewID"));
            $languageView->delete();
            set_flash_message("success", "Deleted language view '$languageView->name'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_languageKeys_and_languageViews");
    }

}
