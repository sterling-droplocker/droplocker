<?php
use App\Libraries\DroplockerObjects\LocationType;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Location_ServiceDay;
use App\Libraries\DroplockerObjects\Location_Neighborhood;

/**
 * A location belongs to a location type and has many service days
 */
class Locations extends MY_Admin_Controller
{
    public $pageTitle = "Locations";

    // This is not used. The default landing page for location is view method
    public function index()
    {
        $this->load->view('admin/blank', '', true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Renders the view to sort master routes
     *
     * @param int $route
     */
    public function master_routes($route = null)
    {
        if (!empty($_POST)) {
            redirect('/admin/locations/master_routes/' . $_POST['route']);
        }

        $content = array();
        $this->load->model('location_model');
        $content['route'] = $route;
        $content['routes'] = $this->location_model->get_master_routes_dropdown($this->business_id);

        if (!is_null($route)) {
            $content['locations'] = $this->location_model->get_aprax(array(
                'masterRoute_id' => $route,
                'business_id' => $this->business_id
                ), 'masterSortOrder ASC');

            $current = 0;

            $content['locations_map'] = array();
            foreach ($content['locations'] as $i => $location) {
                if ($location->masterSortOrder != $i) {
                    $location->masterSortOrder = $i;

                    $this->location_model->save(array(
                        'locationID' => $location->locationID,
                        'masterSortOrder' => $i,
                    ));
                }

                $content['locations_map'][$location->locationID] = $location;
            }
        }

        $this->renderAdmin('master_routes', $content);
    }

    /**
     * Updates the master sort order of posted routes
     */
    public function update_master_sort()
    {
        $locations = $this->input->post('sort');
        $updated = 0;
        if (!empty($locations)) {
            foreach ($locations as $order => $location_id) {
                $this->db->update('location', array(
                    'masterSortOrder' => $order,
                ), array(
                    'locationID' => $location_id,
                    'business_id' => $this->business_id,
                ));

                $updated += $this->db->affected_rows();
            }
        }

        header('Content-Type: application/json');
        echo json_encode(array(
            'updated' => $updated,
            'locations' => $locations
        ));
        exit;
    }

    /**
     * Sort locations in a master route using distance of points
     */
    public function auto_sort_master_route()
    {
        if (!isset($_POST['route'])) {
            redirect("/admin/locations/master_routes");
        }

        $route = $_POST['route'];

        $this->load->model('location_model');
        $locations = $this->location_model->get_aprax(array(
            'masterRoute_id' => $route,
            'business_id' => $this->business_id
        ), 'masterSortOrder ASC', 'locationID, lat, lon');

        $sorted = array();
        $sorted[] = array_shift($locations);

        while ($locations) {
            $first = reset($sorted);
            $last = end($sorted);

            usort($locations, function ($a, $b) use ($first, $last) {
                $d1 = distance($a->lat, $a->lon, $last->lat, $last->lon, 'M');
                $d2 = distance($b->lat, $b->lon, $last->lat, $last->lon, 'M');

                return $d1 > $d2;
            });

            $sorted[] = array_shift($locations);
        }

        foreach ($sorted as $i => $location) {
            $this->location_model->save(array(
                'locationID' => $location->locationID,
                'masterSortOrder' => $i,
            ));
        }

        set_flash_message('success', 'Locations has been sorted.');

        redirect("/admin/locations/master_routes/$route");
    }

    /**
     * Retrieves the properties a single location and returns the data to the request as a JSON structure
     * Can take the following GET paramters
     *  locationType bool include the locationType fields
     * Returns data in the following format
     *  {
     *      status : success|error string,
     *      data array
     *  }
     */
    public function get()
    {
        if ($this->input->get("locationID") === false) {
            output_ajax_error_response(("'locationID' must be passed as a GET parameter"));
        }
        $locationID = $this->input->get("locationID");
        if (!is_numeric($locationID)) {
            output_ajax_error_response("'locationID' must be numberic");
        }
        $data = array();
        if ($this->input->get("locationType")) {
            $this->db->select("location.*,locationType.*");
            $this->db->join("locationType", "location.locationType_id = locationType.locationTypeID");
        }
        $location = $this->db->get_where("location", array("locationID" => $locationID))->row();
        if (empty($location)) {
            output_ajax_error_response("No location found with id '$locationID'");
        }
        output_ajax_success_response($location, "Properties for Location $locationID");
    }
    /**
     * add method starts the process of mapping a business location
     * The user inputs the address of the location and then the next step is to
     * display a google map with a draggable icon so the business owner can
     * finetune the location on a map
     */
    public function add()
    {
        $this->template->write('page_title', 'Add Location', true);
        $this->load->model('location_model');

        // add the js for mapping a location
        $this->template->add_js('https://maps.googleapis.com/maps/api/js?sensor=false&key=' . get_default_gmaps_api_key(), 'source');
        $this->template->add_js('js/mapping.js');

        // used in the dropdown for location type
        $content['types'] = $this->location_model->get_location_type(array(
            'select' => 'locationTypeID, name'
        ));
        // display
        $this->data['content'] = $this->load->view('admin/locations_add', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function update_serviceDay()
    {
        try {
            if ($this->input->post("locationID")) {
                $location_id = $this->input->post("locationID");
            } else {
                throw new \InvalidArgumentException("'locationID' must be passed as a POST parameter.");
            }

            if ($this->input->post("day")) {
                $day = $this->input->post("day");
            }
            if (array_key_exists("enable", $_POST)) {
                $enable = $_POST['enable'];
            } else {
                throw new \InvalidArgumentException("'enable' must be passed as a POST parameter.");
            }

            if ($this->input->post("location_serviceTypeID")) {
                $location_serviceTypeID = $this->input->post("location_serviceTypeID");
            } else {
                throw new \InvalidArgumentException("'location_serviceTypeID' must be passed as a POST parameter");
            }
            if ($enable == 1) {
                $location_serviceDay = new Location_ServiceDay();
                $location_serviceDay->day = $day;
                $location_serviceDay->location_id = $location_id;
                $location_serviceDay->location_serviceType_id = $location_serviceTypeID;
                $location_serviceDay->save();
                echo "enabled";
            } else {
                $location_serviceDays = Location_ServiceDay::search_aprax(array(
                    "location_id" => $location_id,
                    "location_serviceType_id" => $location_serviceTypeID,
                    "day" => $day
                ));
                if ($location_serviceDays) {
                    foreach ($location_serviceDays as $location_serviceDay) {
                        $location_serviceDay->delete();
                    }
                }
                echo "disabled";
            }
        } catch (\Exception $e) {
            if ($this->input->is_ajax_request()) {
                echo "An error occurred";
                send_exception_report($e);
            } else {
                throw $e;
            }
        }
    }

    /**
     * add_ajax method is for adding location using ajax
     *
     * Normally this is called from mapping.js ajax
     * If the location type = description. Then we add the locker type of "N/A" to the locker model
     */
    public function add_ajax()
    {
        $this->load->model('location_model');
        $options['address'] = $_POST['address'];
        $options['address2'] = $_POST['address2'];
        $options['city'] = $_POST['city'];
        $options['state'] = $_POST['state'];
        $options['zipcode'] = $_POST['zipcode'];
        $options['lat'] = $this->input->post('latitude');
        $options['lon'] = $this->input->post('longitude');
        $options['locationType_id'] = $_POST['locationTypeID'];
        $options['serviceType'] = $_POST['serviceType'];
        $options['route_id'] = $this->input->post("route_id");
        $options['sortOrder'] = $this->input->post("sortOrder");
        update_location_meta($this->input->post("location_id"), 'DC_min', $this->input->post("dc_min"));
        update_location_meta($this->input->post("location_id"), 'WF_min', $this->input->post("wf_min"));

        $options['business_id'] = $this->session->userdata('business_id');
        $options['serviceType'] = $_POST['serviceType'];

        $options['residentManager'] = $this->input->post("residentManager");
        $options['highTarget'] = $this->input->post("highTarget");
        $options['lowTarget'] = $this->input->post("lowTarget");
        $options['notes'] = $this->input->post("notes");
        $options['commission'] = $this->input->post("commission");
        $options['commissionFrequency'] = $this->input->post("commissionFrequency");
        $options['description'] = $this->input->post("description");
        $options['hours'] = $this->input->post("hours");
        $options['accessCode'] = $this->input->post("accessCode");
        $options['units'] = $this->input->post("units");
        $options['quarter'] = $this->input->post("quarter");
        $options['turnaround'] = $this->input->post("turnaround");
        $options['custom1'] = $this->input->post("custom1");
        $options['custom2'] = $this->input->post("custom2");

        $where['locationID'] = $_POST['location_id'];

        $res = $this->location_model->insert($options);

        if ($res['status'] == 'success') {

            // If this is location that does not have lockers (front counter) set the locker type to N/A now
            $type = $this->location_model->get_location_type(array(
                'locationTypeID' => $options['locationType_id']
            ));
            if ($type[0]->lockerType == 'description') {
                // if corner store add a locker called "Front_Counter"
                if ($options['locationType_id'] == 5) {
                    $name = "Front_Counter";
                } else {
                    $name = $type->name;
                }
                $this->locker_model->insert(array(
                    'location_id' => $res['insert_id'],
                    'lockerName' => $name,
                    "lockerStyle_id" => 8,
                    "lockerLockType_id" => 3
                ));
            }
        }

        set_flash_message($res['status'], $res['message']);
        echo json_encode($res);
    }

    /**
     * update_description method updates a location if the locker type is description
     */
    public function update_description()
    {
        if (isset($_POST['submit'])) {
            $this->load->model('locker_model');
            $options['location_id'] = $_POST['locationID'];
            $options['lockerName'] = $_POST['description'];
            $options['lockerStatus_id'] = 1;
            $options['lockerStyle_id'] = 8;
            $options['lockerLockType_id'] = 3;

            $res = $this->locker_model->insert($options);
            if ($res['status'] == 'success') {
                set_flash_message('success', 'Location has been updated');
            } else {
                set_flash_message('error', 'Location has not been updated');
            }

            redirect("/admin/locations/update_lockers/" . $_POST['locationID']);
        } else {
            set_flash_message('error', 'No Post Data');
            redirect('/admin/locations');
        }
        // $options['location'] = $_POST['locationID']
    }

    /*
     * Renders the interface for updating or adding a new location.
     */
    public function display_update_form($locationID = null)
    {
        // The following variable stores any data from a POST request and opulates the form with those fields.
        $post = unserialize($this->session->flashdata('post'));
        $this->load->model("location_model");
        $this->load->model("image_model");

        if ($locationID) {
            try {
                $location = $this->location_model->get_by_primary_key($locationID);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Location ID '$locationID' not found");
                redirect($_SERVER['HTTP_REFERER']);
            }
        } elseif (! empty($post)) {
            $location = (object) $post;
        } else {
            $location = (object) array(
                'country' => $this->business->country
            );
        }
        $this->template->add_js('https://maps.googleapis.com/maps/api/js?v=3.9&sensor=false&key=' . get_default_gmaps_api_key(), 'source');
        $this->template->add_js('js/image-picker.min.js');
        $this->template->add_css('css/image-picker.css');

        $content['locationTypes'] = \App\Libraries\DroplockerObjects\LocationType::get_all();

        // bizzie only uses 'Kiosk', 'Lockers' and 'Concierge'
        if (in_bizzie_mode()) {
            $content['locationTypes'] = array(
                1 => 'Kiosk',
                2 => 'Lockers',
             );

            if ($this->business_id <= 28) {
                $content['locationTypes'][3] = 'Concierge';
            }
        }

        $content['businessState'] = $this->business->state;
        $content['businessCountry'] = $this->business->country;

        $content['location'] = $location;
        isset($location->turnaround) ? $content['turnaround'] = $location->turnaround : $content['turnaround'] = $this->business->turnaround;
        ;

        $content_neighborhood_query = $this->db->query('SELECT DISTINCT neighborhood FROM location_neighborhood WHERE business_id=' . $this->business_id);
        $content['businessNeighborhoods'] = $content_neighborhood_query->result();
        $content['locationNeighborhoods'] = Location_Neighborhood::search_aprax(array(
            'location_id' => isset($location->locationID) ? $location->locationID : 0,
            'business_id' => $this->business->businessID
        ));

        $serviceTypes = unserialize($this->business->serviceTypes);
        $content['service_types'] = array();

        $content['existing_service_types'] = $this->location_service_types($locationID);

        foreach ($serviceTypes as $serviceType) {
            if ($serviceType['active'] == 1) {
                $content['service_types'][] = $serviceType;
            }
        }
        $content['business'] = $this->business;

        $content['wholesale_logos'] = $this->image_model->get_images_without_placement(
            $this->business_id
        );

        $lockerImages = empty($location->lockerImages)?array():json_decode($location->lockerImages, true);

        $this->data['content'] = $this->load->view('admin/locations/update', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    private function location_service_types($location_id = 0)
    {
        $location_services_query = $this->db->get_where('location_services', array(
            'location_id' => $location_id,
            'business_id' => $this->business_id
        ));
        $location_services = $location_services_query->result();
        $services_array = array();
        foreach ($location_services as $ls) {
            array_push($services_array, $ls->serviceType_id);
        }

        return $services_array;
    }

    /**
     * updates a location
     */
    public function update()
    {
        if ($this->input->post("locationID")) {
            $location = new \App\Libraries\DroplockerObjects\Location($this->input->post("locationID"));
            $operation = "Updated";
            unset($_POST['locationID']);
        } else {
            $operation = "Created";
            $location = new Location();
        }

        $location->business_id = $this->business_id;
        $location->status = "active";

        // need to change this to use both CI->input->post and neighborhood/locations jazz // do that first, then unset? its a possibility
        $location_neighborhoodsJSON = $this->input->post('location_neighborhoods');
        unset($_POST['location_neighborhoods']);

        // service types by location
        $available_services = $this->input->post('available_services');
        if (! empty($_POST['available_services'])) {
            unset($_POST['available_services']);
        }

        // location WF minimums (optional - only set if the 'type' has been set)
        $wf_min_type = $this->input->post('wf_min_type');
        $wf_min_amount = $this->input->post('wf_min_amount');
        unset($_POST['wf_min_type']);
        unset($_POST['wf_min_amount']);

        if (! empty($wf_min_amount) && ! empty($wf_min_type)) {
            $location->wf_min_amount = $wf_min_amount;
            $location->wf_min_type = $wf_min_type;
        } else {
            $location->wf_min_amount = '';
            $location->wf_min_type = '';
        }

        // Note, the follow loop that sets the properties based on the client's POST request is written by *.
        unset($_POST['qqfile']);
        foreach ($_POST as $key => $value) {
            if (in_array($key, array(
                'serviceDays',
                'pickupDays',
                'deliveryDays'
            ))) {
                // Get the serviceDays and unset from location becuase they are stored in a different table
                $serviceDays = $value;
            } else {
                $location->$key = $value;
            }
        }
        
        if (property_exists($location, "qqfile")) {
            unset($location->qqfile);
        }

        // Note, the following hack statement is necessary to not rewrite all of *'s faulty save logic above.
        $location->launchDate = new DateTime($location->launchDate, new DateTimeZone($this->business->timezone));

        //zip_codes: clean comma separated list
        if (!empty($location->zip_codes)) {
            $location->zip_codes= trim($location->zip_codes);
            $patterns[0] = '/\s*,\s*/';
            $patterns[1] = '/,{2,}/';
            $patterns[2] = '/,+$/';
            $patterns[3] = '/^,+/';
            $replacements[0] = ',';
            $replacements[1] = ',';
            $replacements[2] = '';
            $replacements[3] = '';
            $location->zip_codes = preg_replace($patterns, $replacements, $location->zip_codes);
        }

        if ($this->input->post("show_locker_name_on_receipt") === false) {
            $location->show_locker_name_on_receipt = 0;
        }

        $location->wholesale_logo_id = $this->input->post("wholesale_logo_id");

        try {
            $location->save();
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
            set_flash_message("error", $validation_exception->getMessage());

            $this->goBack("/admin/locations");
        }
        $locationID = $location->locationID;

        // update the service types
        $this->update_service_types($locationID, $available_services);

        // Unknown * conditional.
        if (! empty($locationID) && $operation == "Created") {
            // if its of type 'lockers', set an InProcess locker.
            $this->load->model('location_model');
            $type = $this->location_model->get_location_type(array(
                'locationTypeID' => $location->locationType_id
            ));

            if ($type[0]->lockerType == 'lockers') {
                $this->load->model('locker_model');
                $this->locker_model->insert(array(
                    'location_id' => $locationID,
                    'lockerName' => 'InProcess',
                    "lockerStyle_id" => 7,
                    "lockerLockType_id" => 2
                ));
            }
        }

        // now add neighborhoods -
        if (! empty($location_neighborhoodsJSON)) {
            $bulk_neighborhood_insert = array();

            // pull out existing neighborhoods for this business & location
            $currentLocationNeighborhoods = Location_Neighborhood::search_aprax(array(
                'location_id' => $locationID,
                'business_id' => $this->business_id
            ));
            $current_location_neighborhood_hash = array();

            // our currently existing neighborhoods
            foreach ($currentLocationNeighborhoods as $loc_neighborhood) {
                $current_location_neighborhood_hash[$loc_neighborhood->neighborhood] = $loc_neighborhood;
            }

            // from the form/post, remember?
            $location_neighborhoods = json_decode($location_neighborhoodsJSON);

            foreach ($location_neighborhoods as $index => $potential_loc_neighborhood) {
                // if we dont have this location already, add it.
                if (! isset($current_location_neighborhood_hash[$potential_loc_neighborhood])) {
                    $bulk_neighborhood_insert[] = array(
                        'business_id' => $this->business_id,
                        'location_id' => $locationID,
                        'neighborhood' => $potential_loc_neighborhood
                    );
                }
            }

            $added = Location_Neighborhood::bulk_insert($bulk_neighborhood_insert);
        }
        /**
         * If we are creating a new location, then we check to see which pickup and delivery days were sent from the form and add those service days accordingly.
         */
        if ($operation == "Created") {
            $pickupDays = $this->input->post("pickupDays");
            foreach ($pickupDays as $pickupDay) {
                Location_ServiceDay::create($pickupDay, $location->locationID, 2);
            }
            $deliveryDays = $this->input->post("deliveryDays");
            foreach ($deliveryDays as $deliveryDay) {
                Location_ServiceDay::create($deliveryDay, $location->locationID, 1);
            }
        }

        $sub_message = '';
        if ($location->zipcode) {

            // get the business taxgroups without a tax entry for the location
            $sql = "SELECT * FROM taxGroup
                    LEFT JOIN taxTable ON taxGroup_id = taxGroupID AND zipcode = ?
                    WHERE zipcode IS NULL AND taxGroup.business_id = ?";

            $taxGroups = $this->db->query($sql, array($location->zipcode, $this->business_id))->result();

            if (!empty($taxGroups)) {
                $groups = array();
                foreach ($taxGroups as $taxGroup) {
                    $groups[] = $taxGroup->name;
                }
                $groups = implode(", '", $groups);

                $link = '/admin/taxTables?zipcode=' . $location->zipcode;
                $sub_message = "<p class='warning'>The location zipcode does not have a tax setup in the tax groups: '$groups'.<br>Orders created here will not have taxes. Please <a href='$link'>setup a tax rate</a> for the zipcode</p>";
            }
        }

        set_flash_message('success', "$operation location '{$location->address}, {$location->city}' $sub_message");

        redirect("/admin/locations/display_update_form/{$location->locationID}");
    }

    private function update_service_types($location_id = 0, $services = array())
    {
        $prev_services = array();
        $current_location_services_query = $this->db->get_where('location_services', array(
            'location_id' => $location_id,
            'business_id' => $this->business_id
        ));
        $current_location_services = $current_location_services_query->result();

        // if we have any existing service types for this location_id
        if (! empty($current_location_services)) {
            foreach ($current_location_services as $cls) {
                if (! in_array($cls->serviceType_id, $services)) {
                    // delete it, because its not in the new array
                    $this->db->where('business_id', $this->business_id);
                    $this->db->where('location_id', $location_id);
                    $this->db->where('serviceType_id', $cls->serviceType_id);
                    $this->db->delete('location_services');
                } else {
                    array_push($prev_services, $cls->serviceType_id);
                }
            }
        }
        foreach ($services as $serviceType_id) {
            if (! in_array($serviceType_id, $prev_services)) {
                $service_data = array(
                    'location_id' => $location_id,
                    'serviceType_id' => $serviceType_id,
                    'business_id' => $this->business_id
                );
                $this->db->insert('location_services', $service_data);
            }
        }
    }

    /**
     * delete method sets the status of the location to DELETED
     */
    public function delete()
    {
        if (! $this->input->post("locationID")) {
            throw new \InvalidArgumentException("'locationID' must be passed as a POST parameter.");
        }
        $locationID = $this->input->post("locationID");
        $this->load->model('location_model');

        if ($this->location_model->getActiveLockerCount($locationID) == 0) {
            $this->location_model->update(array(
                'status' => 'deleted'
            ), array(
                'locationID' => $locationID
            ));
            set_flash_message('success', 'Location deleted');
        } else {
            $location = $this->location_model->get_by_primary_key($locationID);

            set_flash_message("error", "Can not delete location '{$location->address}' because there are lockers still associated with this location");
        }
        redirect('/admin/admin/view_all_locations');
    }

    public function undelete()
    {
        if (! $this->input->post("locationID")) {
            throw new \InvalidArgumentException("'locationID' must be passed as a POST parameter.");
        }
        $locationID = $this->input->post("locationID");
        $location = new Location($locationID);
        $location->status = 'active';
        $location->save();

        set_flash_message('success', 'Location restored');

        redirect('/admin/admin/view_all_locations/1');
    }

    /**
     * TODO: this function seems deprecated and is not referenced anywhere
     *
     * uses ajax to return the results so the search form can stay visible
     */
    public function search_lockers()
    {
        $this->template->add_js("/js/search.js");
        $this->renderAdmin('search_lockers', $content);
    }

    /**
     * update_lockers method allows the admin to add and remove
     * lockers in a location
     *
     * Locker location can have different types
     */
    public function update_lockers($id = null)
    {
        $this->template->write('page_title', 'Manage Lockers', true);
        // get the location id
        $content['id'] = $form_data['id'] = $id;

        $this->load->model('location_model');
        $this->load->model('locker_model');

        // $this->template->add_js('js/add_locker_form.js');

        // get all locker types
        $form_data['locker_types'] = $this->locker_model->get_locker_style(array(
            'select' => 'name, lockerStyleID'
        ));

        // get all lock types
        $form_data['lock_types'] = $this->locker_model->get_locker_lock_type(array(
            'select' => 'name, lockerLockTypeID'
        ));

        //get all status
        $form_data['locker_status'] = $this->locker_model->get_locker_status(array('select'=>'name, lockerLockTypeID'));

        // get the location details
        $location = $this->location_model->get(array(
            'locationID' => $id,
            'select' => '(SELECT lockerType FROM locationType WHERE locationTypeID = locationType_id) as lockerType,description,locationID,business_id,city,address,address2,state,zipcode'
        ));

        if ($location[0]->business_id != $this->business_id) {
            set_flash_message("error", "This location does not belong to your business.");
            redirect("/admin/locations/view_all_locations");
        }

        $form_data['location'] = $location[0];
        $content['location'] = $location[0];
        $content['location_address'] = $location[0]->address . ", " . ucfirst($location[0]->city);

        // get the lockers in this location
        $form_data['lockers'] = $this->locker_model->get(array(
            'lockerStatus_id' => 1,
            'with_status' => true,
            'location_id' => $id,
            'order_by' => $this->orderAlphanumAsNumQuery('lockerName'),
            'select' => 'lockerID,lockerStyle.name,lockerLockType.name as lockName,lockerName, lockerStatus.name as lockerStatus'
        ));
        $form_data['inactive_lockers'] = $this->locker_model->get(array(
            'lockerStatus_id' => array(0,2,3,4,5,6,7,8,9),//all other statuses, have to enumerate since $this->locker_model->get does not accept != operator, just = and IN
            'with_status' => true,
            'location_id' => $id,
            'order_by' => $this->orderAlphanumAsNumQuery('lockerName'),
            'select' => 'lockerID,lockerStyle.name,lockerLockType.name as lockName,lockerName, lockerStatus.name as lockerStatus'
        ));

        // displays different forms depending on the lockertype
        switch ($location[0]->lockerType) {
            case ('lockers'):
                // add_locker_form
                $this->template->add_js('js/add_locker_form.js');
                $content['include_form'] = $this->load->view('inc/add_locker_form', $form_data, true);
                break;

            case ('units'):
                // add_unit_form
                $this->template->add_js('js/add_unit_form.js');
                $content['include_form'] = $this->load->view('inc/add_unit_form', $form_data, true);
                break;

            case ('description'):
                // add_description_form
                $content['include_form'] = $this->load->view('inc/add_description_form', $form_data, true);
                break;
        }

        // display
        $this->data['content'] = $this->load->view('admin/admin/location_update_lockers', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Generate condition to use on order by clause to order alphanumeric columns
     * like numeric colums (2 before 12, numbers before words)
     */
    protected function orderAlphanumAsNumQuery($col)
    {
        return "($col='0' OR $col * 1) DESC, $col * 1 ASC, $col ASC";
    }

    /**
     * add_locker_ajax method adds a locker to the database
     *
     * Normally called from add_locker_form.js or add_unit_form.js
     */
    public function add_locker_ajax()
    {
        $this->load->model('locker_model');

        $this->load->library('form_validation');
        $this->form_validation->set_rules("start", "Start", 'required');
        if ($this->input->post('end')) {
            $this->form_validation->set_rules('start', 'Start', 'numeric');
        }
        $this->form_validation->set_rules("end", "End", 'numeric');
        $this->form_validation->set_rules('lockerStyle_id', 'required|numeric');
        $this->form_validation->set_rules('location_id', 'required|numeric');
        $this->form_validation->set_rules('lock_type', 'required|numeric');
        // check to see if the user is adding a range of lockers or units
        // if it's a range we do a batch insert
        if ($this->form_validation->run()) {
            $start = $this->input->post('start');
            if (trim($_POST['end']) != '') {
                // we need to add a batch of lockers
                $end = trim($_POST['end']);
                for ($i = $start; $i <= $end; $i ++) {
                    $batch[] = array(
                        'location_id' => $_POST['location_id'],
                        'lockerName' => (int) $i,
                        'lockerStyle_id' => $_POST['locker_type'],
                        'lockerLockType_id' => $_POST['lock_type'],
                        'dateCreated' => gmdate('Y-m-d H:i:s'),
                        'prefix' => $_POST['prefix'],
                    );
                }

                $res = $this->locker_model->insert($batch, true);
                if ($res['status'] == 'success') {
                    set_flash_message('success', $res['message']);
                } else {
                    set_flash_message('error', $res['message']);
                }
            } else {
                // not a range so do a normal insert
                $options['location_id'] = $_POST['location_id'];
                $options['lockerStyle_id'] = $_POST['locker_type'];
                $options['lockerLockType_id'] = $_POST['lock_type'];
                if (is_numeric($start)) {
                    $options['lockerName'] = (int) trim($start);
                } else {
                    $options['lockerName'] = trim($start);
                }
                $options['dateCreated'] = gmdate('Y-m-d H:i:s');
                $options['prefix'] = $_POST['prefix'];

                $res = $this->locker_model->insert($options);
                if ($res['status'] == 'success') {
                    set_flash_message('success', $res['message']);
                } else {
                    set_flash_message('error', $res['message']);
                }
            }
        } else {
            $res['status'] = 'fail';
            set_flash_message("error", validation_errors());
        }
        echo json_encode($res);
    }

    /**
     * update_locker_ajax
     */
    public function update_locker_ajax()
    {
        if (! $_POST) {
            echo json_encode(array(
                'status' => 'fail',
                'message' => 'No Post Data'
            ));
            die();
        }
        $this->load->model('locker_model');
        $locker_id = $_POST['lockerID'];

        if (!empty($_POST['lockerName'])) {
            $locker = $this->locker_model->get_by_primary_key($locker_id);
            $lockers = $this->locker_model->get_aprax(array(
                'lockerID !=' => $locker_id,
                'location_id' => $locker->location_id,
                'lockerName' => $_POST['lockerName'],
            ));

            if ($lockers) {
                echo json_encode(array(
                    'status'=>'fail',
                    'message'=>'There is already a locker with that name in this location'
                ));
                die();
            }
        }

        unset($_POST['lockerID']);
        $res = $this->locker_model->update($_POST, array(
            'lockerID' => $locker_id
        ));

        if ($res) {
            echo json_encode(array(
                'status' => 'success',
                'affected_rows' => $res
            ));
        } else {
            echo json_encode(array(
                'status' => 'fail',
                'message' => 'Old Value = New Value'
            ));
        }
    }

    /**
     * Sets a locker as inactive if there are orders associated with that locker, or deletes the locker from the database if there are no orders associated with that locker.
     * Expects segment 4 to be the locker ID, and segment 5 to be the location ID that the locker resides in.
     */
    public function delete_locker($locker_id = null, $location_id = null)
    {
        if (!is_numeric($locker_id)) {
            set_flash_message("error", "'locker_id' must be integer and passed as URL segment 4.");
            redirect("/admin/locations/update_lockers/{$location_id}");
        }
        if (!is_numeric($location_id)) {
            set_flash_message("error", "'location_id' must be integer and passed as URL segment 5.");
            redirect("/admin/locations/update_lockers/{$location_id}");
        }

        $this->load->model('locker_model');
        $sql = "SELECT orderID from orders WHERE locker_id = ?";
        $query = $this->db->query($sql, array($locker_id));

        $sql2 = "SELECT claimID from claim WHERE locker_id = ?";
        $query2 = $this->db->query($sql2, array($locker_id));

        if ($query->num_rows() > 1 || $query2->num_rows() > 1) {
            $this->locker_model->update(array('lockerStatus_id'=>4), array('lockerID'=>$locker_id));
            $res = $this->db->affected_rows();
            set_flash_message("success", 'Locker '. $locker_id." has been set to inactive");
            redirect("/admin/locations/update_lockers/{$location_id}");
        } else {
            $res = $this->locker_model->delete(array('lockerID'=>$locker_id));
        }

        if ($res) {
            set_flash_message("success", "Locker has been deleted");
        } else {
            set_flash_message("error", 'Locker not deleted');
        }

        redirect("/admin/locations/update_lockers/{$location_id}");
    }

    /**
     * delete_locker_ajax method deletes one or more lockers or units from a location
     */
    public function delete_locker_ajax()
    {
        if (! $_POST) {
            echo json_encode(array(
                'status' => 'fail',
                'message' => 'No Post Data'
            ));
            die();
        }
        $this->load->model('locker_model');
        $lockers = rtrim($_POST['batch_lockerIDs'], ",");
        $lockers = explode(",", $lockers);
        foreach ($lockers as $locker_id) {
            $this->locker_model->update(array(
                'lockerStatus_id' => 4
            ), array(
                'lockerID' => $locker_id
            ));
            $res = $this->db->affected_rows();
        }
        set_flash_message('success', "Batch deletion complete");
        echo json_encode(array(
            'status' => 'success'
        ));
    }

    public function enable_locker($locker_id = null, $location_id = null)
    {
        $this->load->model('locker_model');
        $id = $this->locker_model->update(array(
            'lockerStatus_id' => 1
        ), array(
            'lockerID' => $locker_id
        ));
        if ($id) {
            set_flash_message("success", 'Locker ID: ' . $locker_id . ' has been enabled');
        } else {
            set_flash_message("error", "Locker not enabled");
        }

        redirect("/admin/locations/update_lockers/{$location_id}");
    }

    public function routes()
    {
        // display
        $this->data['content'] = $this->load->view('admin/locations_routes', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    
    public function getLockersListForDropDown()
    {
        $location_id = $this->input->post('location_id');
        
        $this->load->model('locker_model');
        $lockers = $this->locker_model->get_all_all_as_codeigniter_dropdown_for_location($location_id);
        
        header('Content-Type: application/json');
        echo json_encode($lockers);
    }

    /**
     * upload_locker_images
     */
    public function upload_locker_images($location_id = null)
    {
        $this->db->select("lockerImages");
        $location = $this->db->get_where("location", array("locationID" => $location_id, 'business_id' => $this->business_id))->row();

        if (empty($location) || empty($_POST)) {
            echo json_encode(array(
                'success' => 'false',
                'error' => get_translation('Wrong Post Data', 'admin/locations/locker_images', array(), 'Wrong Post Data')
            ));
            die();
        }

        $path = "images/lockerImages/";
        $config['upload_path'] = ROOT_DIR . $path;

        if (!is_dir($config['upload_path'])) {
            if (!mkdir($config['upload_path'], DIR_WRITE_MODE, true)) {
                echo json_encode(array(
                    'success' => false,
                    'error' => get_translation('Invalid upload path', 'admin/locations/locker_images', array(), 'Wrong Post Data')
                ));
                die();
            }
        }

        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_width']  = '0';
        $config['max_height']  = '0';

        $nice_name = $_FILES["qqfile"]['name'];
        if (!empty($this->input->post('qqfilename'))) {
            $nice_name = $this->input->post('qqfilename');
        }
        
        $ext = pathinfo($_FILES["qqfile"]['name'], PATHINFO_EXTENSION);
        $name = md5(time().$_FILES["qqfile"]['name']) . "." . $ext;
        $_FILES["qqfile"]['name'] = $name;

        $this->load->library('upload', $config);

        $upload = $this->upload->do_upload("qqfile");
        if (!$upload) {
            echo json_encode(array(
                'success' => false,
                'error' => $this->upload->display_errors()
            ));
        } else {
            $lockerImages = array();
            
            if (!empty($location->lockerImages)) {
                $lockerImages = json_decode($location->lockerImages, true);
            }

            $lockerImages[] = array(
                "name"              => $nice_name,
                "uuid"              => $name,
                "thumbnailUrl"      => "/" . $path . $name,
            );

            $this->db->update('location', array(
                    'lockerImages' => json_encode($lockerImages),
                ), array(
                    'locationID' => $location_id,
                    'business_id' => $this->business_id,
                ));

            if ($this->db->affected_rows()) {
                echo json_encode(array(
                    'success' => true,
                    'newUuid' => $name
                ));
            } else {
                echo json_encode(array(
                    'success' => false,
                    'error' => get_translation('Something went wrong', 'admin/locations/locker_images', array(), 'Something went wrong')
                ));
                @unlink($config['upload_path'] . $name);
            }
        }

        die();
    }

    /**
     * upload_locker_images
     */
    public function delete_locker_images($location_id = null, $uuid = null)
    {
        $this->db->select("lockerImages");
        $location = $this->db->get_where("location", array("locationID" => $location_id, 'business_id' => $this->business_id))->row();

        if (empty($location) || empty($_REQUEST) || empty($uuid)) {
            echo json_encode(array(
                'success' => 'false',
                'error' => get_translation('Wrong Request Data', 'admin/locations/locker_images', array(), 'Wrong Request Data')
            ));
            die();
        }

        $path = "images/lockerImages/";
        $config['upload_path'] = ROOT_DIR . $path;

        $lockerImages = array();
            
        if (empty($location->lockerImages)) {
            echo json_encode(array(
                    'success' => false,
                    'error' => get_translation('Something went wrong', 'admin/locations/locker_images', array(), 'Something went wrong')
                ));
            die();
        }

        $lockerImages = json_decode($location->lockerImages, true);

        foreach ($lockerImages as $k=>$v) {
            if ($v["uuid"] == $uuid) {
                @unlink(ROOT_DIR . $v["thumbnailUrl"]);
                unset($lockerImages[$k]);
            }
        }

        $this->db->update('location', array(
                    'lockerImages' => json_encode(array_values($lockerImages)),
                ), array(
                    'locationID' => $location_id,
                    'business_id' => $this->business_id,
                ));

        if ($this->db->affected_rows()) {
            echo json_encode(array(
                    'success' => true
                ));
        } else {
            echo json_encode(array(
                    'success' => false,
                    'error' => get_translation('Something went wrong', 'admin/locations/locker_images', array(), 'Something went wrong')
                ));
        }
        
        die();
    }
}
