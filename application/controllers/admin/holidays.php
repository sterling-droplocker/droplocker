<?php

class Holidays extends MY_Admin_Controller
{
    public $business_id;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('holiday_model');
    }

    public function index()
    {
        $content['holidays'] = $this->holiday_model->getHolidays($this->business_id, (bool)$this->input->get('old'));

        $this->template->write('page_title','Holiday Schedule', true);

        $this->renderAdmin('holidays', $content);
    }

    public function add()
    {
        $this->load->model('holiday_model');

        $dateFormatted = date('Y-m-d', strtotime($this->input->post("date")));

        $data = array(
            'name' => $this->input->post('name'),
            'date' => $dateFormatted,
            'pickup' => $this->input->post("pickup"),
            'delivery' => $this->input->post("deliver"),
            'business_id' => $this->business_id
        );

        $this->holiday_model->save($data);

        set_flash_message('success', "Holiday Added");
        redirect("/admin/holidays");
    }

    public function delete()
    {
        $this->holiday_model->delete_aprax(
            array(
                'holidayID' => $this->input->get('holidayID'),
                'business_id' => $this->business_id
            )
        );

        set_flash_message('success', "Holiday Deleted");
        redirect("/admin/holidays");
    }
}