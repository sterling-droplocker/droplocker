<?php
use App\Libraries\DroplockerObjects\LanguageKey_Business_Language;
use App\Libraries\DroplockerObjects\Business_Language;
class LanguageKey_Business_Languages extends MY_Admin_Controller
{
    /**
     * Updates an exising business language key based on the business ID, languageKeyID, and language ID. This actikon is intended to be used in conjunction an AJAX request
     * Note, If there is no row that matches the criteria, then this action creates anew row in the languageKey_business_language table.
     * Note, if no translation is passed, then this action deletes any rows in the languageKey_business_language table that match the criteria.
     * Expects the following POST parameters
     *  language_id
     *  languageKey_id
     *  translation : The business specified translation for the language key
     *
     */
    public function update_business_translation()
    {
        $this->load->helper('ajax_helper');
        $this->load->library("form_validation");

        $this->form_validation->set_rules("language_id", "is_natural_no_zero|required|does_language_exist");
        $this->form_validation->set_rules("languageKey_id", "is_natural_no_zero|required|does_languageKey_exist");
        if ($this->form_validation->run()) {
            $business_id = $this->business_id;
            $language_id = $this->input->post("language_id");
            $languageKey_id = $this->input->post("languageKey_id");

            $this->load->model("languagekey_model");
            $languageKey = $this->languagekey_model->get_by_primary_key($languageKey_id);
            $this->load->model("language_model");
            $language = $this->language_model->get_by_primary_key($language_id);

            $this->load->model("languageKey_business_language_model");
            $languageKey_business_languages = LanguageKey_Business_Language::search_aprax(array(
                "business_id" => $business_id,
                "language_id" => $language_id,
                "languageKey_id" => $languageKey_id
            ));

            $business_language = Business_Language::search(array(
                "business_id" => $business_id,
                "language_id" => $language_id,
            ));

            if (empty($languageKey_business_languages)) {
                $languageKey_business_language = new LanguageKey_Business_Language();
            } else {
                $languageKey_business_language = $languageKey_business_languages[0];
            }
            $translation = trim($this->input->post("translation"));
            if ($translation === false || empty($translation)) {
                if (!empty($languageKey_business_language->{LanguageKey_Business_Language::$primary_key})) {
                    $languageKey_business_language->delete();

                    // clear translation cache
                    $business_language->lastUpdated = gmdate('Y-m-d H:i:s');
                    $business_language->save();
                }
                output_ajax_success_response(array(), "Deleted business translation for language key '{$languageKey->name}' and language '{$language->tag} {$language->description}");
            } else {
                $translation = $this->input->post("translation");

                $languageKey_business_language->business_id = $business_id;
                $languageKey_business_language->language_id = $language_id;
                $languageKey_business_language->languageKey_id = $languageKey_id;
                $languageKey_business_language->translation = $translation;
                $languageKey_business_language->save();

                // clear translation cache
                $business_language->lastUpdated = gmdate('Y-m-d H:i:s');
                $business_language->save();

                $output_msg = "Updated translation to '$translation' for language key '{$languageKey->name}' and {$language->tag}-{$language->description}.";

                if (!$languageKey_business_language->validate_substitutions($languageKey->defaultText, $translation)) {
                    $output_msg .= " \n\nWARNING:  You do not have matching Macros!  You should update!";
                }

                output_ajax_success_response(array(), $output_msg);

            }
        } else {
            output_ajax_error_response(validation_errors());
        }
    }
}
