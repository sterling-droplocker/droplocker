<?php

class Lockers extends MY_Admin_Controller
{
    /**
     * Creates a new locker.
     * Expects the following POST parameters
     *  start int
     *  end int
     *  lockerStyle_id int
     *  location_id int
     *  lock_type int
     */
    public function add()
    {
        $this->load->model('locker_model');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('start', 'Start', 'numeric');
        $this->form_validation->set_rules("end", "End", 'numeric');
        $this->form_validation->set_rules('lockerStyle_id', 'required|numeric');
        $this->form_validation->set_rules('location_id', 'required|numeric');
        $this->form_validation->set_rules('lock_type', 'required|numeric');
        //check to see if the user is adding a range of lockers or units
        // if it's a range we do a batch insert

        $lockerStyle_id = $this->input->post('locker_type');
        $lockerLockType_id = $this->input->post('lock_type');
        $location_id = $this->input->post("location_id");
        if ($this->form_validation->run()) {
            $start = $this->input->post('start');
            if (trim($_POST['end']) != '') {
                //we need to add a batch of lockers
                $end = trim($_POST['end']);
                for ($i = $start; $i <= $end; $i++) {
                    $batch[] = array('location_id' => $location_id, 'lockerName' => (int) $i, 'lockerStyle_id' => $lockerStyle_id, 'lockerLockType_id' => $lockerLockType_id);
                }

                $res = $this->locker_model->insert($batch, true);
                if ($res['status'] == 'success') {
                    set_flash_message('success', $res['message']);
                } else {
                    set_flash_message('error', $res['message']);
                }
            } else {
                // not a range so do a normal insert
                $options['location_id'] = $_POST['location_id'];
                $options['lockerStyle_id'] = $_POST['locker_type'];
                $options['lockerLockType_id'] = $_POST['lock_type'];
                $options['lockerName'] = trim($start);
                $res = $this->locker_model->insert($options);
                if ($res['status'] == 'success') {
                    set_flash_message('success', $res['message']);
                } else {
                    set_flash_message('error', $res['message']);
                }
            }
        }
    }
    /**
     * Retreives all lockers that have a status of service and/or hidden
     * Expects the following GET parameters
     *  locationID
     */
    public function get_all_for_location()
    {
        if ($this->input->get("locationID")) {
            $locationID = $this->input->get("locationID");
            $this->load->model("location_model");
            $location = $this->location_model->get_by_primary_key($locationID);
            if (empty($location)) {
                output_ajax_error_repsonse("Location ID '{$locationID}' not found.");
            } else {
                $result = array();
                $this->load->model("locker_model");
                $lockers = $this->locker_model->get_all_returnable_for_location($locationID);
                foreach ($lockers as $locker) {
                    $result[$locker->lockerID] = $locker->lockerName;
                }
                output_ajax_success_response($result, "Lockers for location {$location->address}");
            }
        } else {
            output_ajax_error_response("'locationID' must be numeric.");
        }
    }
}
