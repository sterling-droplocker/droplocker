<?php
use App\Libraries\DroplockerObjects\Location_Neighborhood;

class Location_Neighborhoods extends MY_Admin_Controller
{
    public function delete()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get("location_neighborhoodID")) {
                $location_neighborhoodID = $this->input->get("location_neighborhoodID");
                $location_neighborhood = new Location_Neighborhood($location_neighborhoodID);
                $location_neighborhood->delete();
                output_ajax_success_response(array(), "Deleted location neighborhood '{$location_neighborhood->neighborhood} ({$location_neighborhood->location_neighborhoodID})'");
            } else {
                output_ajax_error_response("'location_neighborhoodID' must be passed as a GET parameter");
            }
        } else {
            echo("This action only supports an ajax request");
        }
    }
}
