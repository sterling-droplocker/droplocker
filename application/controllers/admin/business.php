<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
Use App\Libraries\DroplockerObjects\AclRoles;
use App\Libraries\DroplockerObjects\Business_Language;
use App\Libraries\DroplockerObjects\EmailTemplate;
use App\Libraries\DroplockerObjects\ServiceType;
class Business extends MY_Admin_Controller
{
    /**
     * Updates the specified property of the current business to the specified value.
     * Expects the following post parameter:
     *  property : the name of the business property that shall be updated
     * Can take the following post parameter:
     *  value : The value that the property shall be set to
     * Note, if 'value' is not passed, then the property is set to an empty string.
     * If the input is an ajax request, this action returns a response that is compatible with the jquery.jeditable plugin,
     */
    public function update()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("property", "required");

        if ($this->form_validation->run()) {
            $property = $this->input->post("property");
            $value = $this->input->post("value");
            $business = new \App\Libraries\DroplockerObjects\Business($this->business_id);
            $business->$property = $value;
            $business->save();
            if ($this->input->is_ajax_request()) {
                echo $value;
            } else {
                set_flash_message("success", "Updated business '{$business->companyName}' property '$property' to '$value'");
                $this->goBack("/admin/admin");
            }

        } else {
            if ($this->input->is_ajax_request()) {
                echo validation_errors();
            } else {
                set_flash_message("error", validation_errors());

                $this->goBack("/admin/admin");
            }
        }
    }
    /**
     *  Can take the following POST parameters
     *  acl
     *  laundryPlan
     *  products
     *  reminder
     *  email
     *  coupon
     */
    public function copy_data()
    {
        $source_business_id = $this->input->post("source_business_id");
        $destination_business_id = $this->input->post("destination_business_id");

        if ($source_business_id == $destination_business_id) {
            set_flash_message("error", "The destination and the source businesses can not be the same");
        } else {
            $source_business = new \App\Libraries\DroplockerObjects\Business($source_business_id);
            $destination_business = new \App\Libraries\DroplockerObjects\Business($destination_business_id);

            //ACL's
            $updates = array();
            if ($this->input->post("acl")) {
                $updates[] = "ACL";
                $this->load->model('business_model');
                $this->business_model->copyAcl($destination_business_id, $source_business_id);
            }
            //laundry plans
            if ($this->input->post("businessLaundryPlans")) {
                $updates[] = "Business Laundry Plans";
                \App\Libraries\DroplockerObjects\BusinessLaundryPlan::copy_from_source_business_to_destination_business($source_business_id, $destination_business_id);
            }

            //everything plans
            if ($this->input->post("businessEverythingPlan")) {
                $updates[] = "Business Everything Plans";
                \App\Libraries\DroplockerObjects\businessEverythingPlan::copy_from_source_business_to_destination_business($source_business_id, $destination_business_id);
            }

            //products
            if ($this->input->post("products")) {
                $updates[] = "Products, UpchargeGroups, Product Processes, ProductCategories";
                $this->load->model('business_model');
                \App\Libraries\DroplockerObjects\Product::copy_from_source_business_to_destination_business($source_business_id, $destination_business_id);
            }
            //email reminders
            if ($this->input->post("reminder")) {
                $updates[] = "Reminders";
                $this->load->model('business_model');
                $this->business_model->copy_reminders_and_reminderEmails($source_business_id, $destination_business_id);
            }
            //email templates
            if ($this->input->post("email")) {
                $updates[] = "Email Templates";
                EmailTemplate::copy_from_source_business_to_destination_business($source_business_id, $destination_business_id);
            }
            //coupon templates
            if ($this->input->post("coupon")) {
                $updates[] = "Coupon";
                $this->load->model('business_model');
                $this->business_model->copyCoupon($destination_business_id, $source_business_id);
            }
            //Discount reasons templates
            if ($this->input->post("reasons")) {
                $updates[] = "Reasons";
                $this->load->model('business_model');
                $this->business_model->copyReason($destination_business_id, $source_business_id);
            }
            //follow up call templates
            if ($this->input->post("followup")) {
                $updates[] = "Followup";
                $this->load->model('business_model');
                $this->business_model->copyFollowup($destination_business_id, $source_business_id);
            }
            if ($this->input->post("upcharges")) {
                $updates[] = "Upcharges";
                \App\Libraries\DroplockerObjects\UpchargeGroup::copy_from_source_business_to_destination_business($source_business_id, $destination_business_id);
            }

            $copied_fields = implode(", ", $updates);
            set_flash_message("success", sprintf("Copied %s from %s to %s", $copied_fields, $source_business->companyName, $destination_business->companyName));
        }

        $this->goBack("/admin/superadmin/setup_business");
    }

    /**
     * Adds a new business and also creates a blank customer, website entry, default business language, and admin employee for the business.
     *
     * Expects the folloinwg POST parameters:
     *  companyName
     *  email
     *  admin_email
     *  admin_password
     *  admin_firstName
     *  admin_lastName
     *  language_id
     * Can take the following optional POST parameters
     *  address
     *  address2
     *  state
     *  phone
     *
     */
    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules("companyName", "Company Name", "required");
        $this->form_validation->set_rules('email', "Company Email", 'required');
        $this->form_validation->set_rules("admin_email", "Admin Email" ,'required');
        $this->form_validation->set_rules('admin_password', "Admin Password",'required');
        $this->form_validation->set_rules("language_id", "Language ID", "required|is_natural_no_zero|does_language_id");

        if ($this->form_validation->run()) {
            if ($this->input->post("serviceTypes")) {

                $serviceTypes = array();
                foreach ($this->input->post("serviceTypes") as $serviceTypeID) {
                    $serviceType = new ServiceType($serviceTypeID);
                    $serviceTypes[$serviceTypeID] = array(
                        'serviceType_id' => $serviceTypeID,
                        'active' => 1,
                        'service' => $serviceType->name,
                        'displayName' => $serviceType->name,
                    );
                }

                unset($_POST['serviceTypes']);
            } else {
                $serviceTypes = null;
            }

            $new_business = new \App\Libraries\DroplockerObjects\Business();
            $new_business->companyName = $this->input->post('companyName');
            $new_business->email = $this->input->post("email");

            if (sizeof($serviceTypes) > 0) {
                $new_business->serviceTypes = serialize($serviceTypes);
            }
            if ($this->input->post("address")) {
                $new_business->address = $this->input->post("address");
            }
            if ($this->input->post('address2')) {
                $new_business->address2 = $this->input->post('address2');
            }
            if ($this->input->post('state')) {
                $new_business->state = $this->input->post('state');
            }
            if ($this->input->post("city")) {
                $new_business->city = $this->input->post('city');
            }
            if ($this->input->post('phone')) {
                $new_business->phone = $this->input->post("phone");
            }
            if ($this->input->post('zipcode')) {
                $new_business->zipcode = $this->input->post('zipcode');
            }
            if ($this->input->post('subdomain')) {
                $new_business->subdomain = $this->input->post('subdomain');
            }
            $new_business->timezone = $this->input->post('timezone');
            //The following stateemnt sets the blank customer id to temporary be zero, because we first need to create the business before we can create a blank customer before the business.
            $new_business->blank_customer_id = 0;
            $new_business->save();

            //THe following statements create the blank customer for the business.
            $customer = new Customer();
            $customer->email = "blank_customer@example.com";
            $customer->password = md5(time());
            $customer->business_id = $new_business->{\App\Libraries\DroplockerObjects\Business::$primary_key};
            $customer->save();

            $new_business->blank_customer_id = $customer->{Customer::$primary_key};
            $new_business->save();

            $this->load->model('business_model');
            $this->business_model->copyAcl($new_business->{\App\Libraries\DroplockerObjects\Business::$primary_key});


            //THe following statements create a website entity for the business.
            $website = new App\Libraries\DroplockerObjects\Website();
            $website->business_id = $new_business->{\App\Libraries\DroplockerObjects\Business::$primary_key};
            $website->template = "newBusiness";
            $website->website_url = $this->input->post('website_url');
            $website->title = $this->input->post('companyName');

            //The following conditional copies the master business image logos to the new business  and also copies the master buisness header, footer, javascript, and stylescript.
            if (in_bizzie_mode()) {
                $master_business = get_master_business();

                $master_websites = \App\Libraries\DroplockerObjects\Website::search_aprax(array("business_id" => $master_business->businessID));

                $master_website = $master_websites[0];
                $website->header = $master_website->header;
                $website->footer = $master_website->footer;
                $website->template = "bizzie";
                $website->javascript = $master_website->javascript;
                $website->stylescript = $master_website->stylescript;

                $images = \App\Libraries\DroplockerObjects\Image::search_aprax(array("business_id" => $master_business->{\App\Libraries\DroplockerObjects\Business::$primary_key}));
                foreach ($images as $image) {
                    $new_image = $image->copy();
                    $new_image->business_id = $new_business->{\App\Libraries\DroplockerObjects\Business::$primary_key};
                    $new_image->save();
                }

            }

            // add a website entry

            $website->save();

            set_flash_message("success", "Created new business '{$new_business->companyName}'");

            //The following statements create the admin employee.
            $employee = new Employee();
            $employee->email = $this->input->post('admin_email');
            if ($this->input->post("admin_firstName")) {
                $employee->firstName = $this->input->post('admin_firstName');
            }
            if ($this->input->post("admin_lastName")) {
                $employee->lastName = $this->input->post("admin_lastName");
            }
            $employee->password = $this->input->post("admin_password");

            $employee->save();

            $aclRoles = AclRoles::search_aprax(array('name' => 'admin'));
            $aclRole = $aclRoles[0];
            $business_employee = new Business_Employee();
            $business_employee->employee_id = $employee->{Employee::$primary_key};
            $business_employee->business_id = $new_business->{\App\Libraries\DroplockerObjects\Business::$primary_key};
            $business_employee->aclRole_id = $aclRole->aclID;
            $business_employee->save();

            //The following statements create the default Wash and Fold Product Category.


            \App\Libraries\DroplockerObjects\ProductCategory::createWashandFold($new_business->{\App\Libraries\DroplockerObjects\Business::$primary_key});

            $language_id = $this->input->post("language_id");
            $business_language = Business_Language::create($new_business->businessID, $language_id);
            $new_business->default_business_language_id = $business_language->business_languageID;

            $new_business->save();

            set_flash_message("success", "Logged in as {$employee->firstName} {$employee->lastName}");

        } else {
            set_flash_message("error", validation_errors());
        }


        redirect("/admin/employees/switch_to_employee/{$employee->{Employee::$primary_key}}");

    }
}
