<?php

class Mailchimp extends MY_Admin_Controller
{

    public $submenu = 'admin';

    public function __construct()
    {
        parent::__construct();

        // load reporting databse
        $this->reporting_database = $this->load->database("reporting", true);

        $params = array(
            'business' => $this->business,
            'api_key' => get_business_meta($this->business_id, 'mailchimp_api'),
            'enabled' => get_business_meta($this->business_id, 'mailchimp_enabled'),
            'list_name' => trim(get_business_meta($this->business_id, 'mailchimp_list')),
            'last_update' => get_business_meta($this->business_id, 'mailchimp_last_update')
        );

        $this->load->library('Mailchimplibrary', $params);
    }

    public function index()
    {
        $content['business'] = $this->business;
        $content['mailchimp_enabled'] = $this->mailchimplibrary->enabled;
        $content['mailchimp_valid_key'] = $this->mailchimplibrary->valid;
        $content['mailchimp_stats'] = $this->mailchimplibrary->list_info;
        $content['mailchimp_last_update'] = $this->mailchimplibrary->last_update;
        $content['mailchimp_last_update_qty'] = get_business_meta($this->business_id, 'mailchimp_last_update_qty');
        if ($content['mailchimp_enabled'] && $this->mailchimplibrary->valid) {
            $content['mailchimp_pending_batches'] = $this->mailchimplibrary->getPendingBatches();
        }

        $this->renderAdmin('index', $content);
    }

    /**
     * Update mailchimp settings
     */
    public function settings_update()
    {
        /* Mailchimp Settings */
        update_business_meta($this->business_id, 'mailchimp_enabled', $this->input->post("mailchimp_enabled"));
        update_business_meta($this->business_id, 'mailchimp_api', $this->input->post("mailchimp_api"));
        update_business_meta($this->business_id, 'mailchimp_list', trim($this->input->post("mailchimp_list")));
        update_business_meta($this->business_id, 'mailchimp_automatic_update', $this->input->post("mailchimp_automatic_update"));

        set_flash_message('success', 'Mailchimp settings have been updated');

        $this->goBack('/admin/mailchimp/index');
    }

    /**
     * Update members on mailchimp
     *
     * @param int $partial
     */
    public function updateMembers($partial = 0)
    {
        $i = 0;
        $processed = 0;
        do {
            $batchStart = $i * 500;

            if (!$partial) {
                $query = $this->reporting_database->query(
                    "SELECT * FROM reports_customer 
                  WHERE business_id = ? LIMIT ?, 500",
                    array($this->business->businessID, $batchStart)
                );
            } else {
                $query = $this->reporting_database->query(
                    "SELECT c.customerID, c.firstName, c.lastName, c.email, c.address1, rc.lastOrder, c.location_id, 
                        rc.route_id, rc.avgOrderLapse, c.zip, c.kioskAccess, c.inactive, c.sex, rc.totGross, rc.visits, 
                        c.signupDate, rc.orderCount, c.business_id, rc.firstOrder, c.city, c.state, c.ip, c.birthday, rc.planstatus
                      FROM reports_customer rc
                      INNER JOIN customer c ON c.customerID = rc.customerID 
                      WHERE rc.business_id = ? AND c.lastUpdated > COALESCE(c.mailchimp_sync_date, NOW() - INTERVAL 2 WEEK) 
                      ORDER BY c.lastUpdated DESC 
                      LIMIT ?, 500",
                    array(
                        $this->business->businessID,
                        $batchStart
                    )
                );
            }
            $customers = $query->result();
            $processed += $this->mailchimplibrary->createBatchUpdateMembers($customers);

            $i++;
        } while (!empty($customers));

        update_business_meta($this->business_id, 'mailchimp_last_update', convert_to_gmt_aprax());
        update_business_meta($this->business_id, 'mailchimp_last_update_qty', $processed);

        set_flash_message('success', 'Sent ' . $processed . ' members to Mailchimp');

        $this->goBack('/admin/mailchimp/index');
    }

    /**
     * Create droplocker list on mailchimp with business information
     *
     * @return string list_id
     */
    public function createDroplockerList()
    {
        $this->load->model('business_language_model');
        $language = $this->business_language_model->get_by_primary_key($this->business->default_business_language_id);

        $list = $this->mailchimplibrary->createDroplockerList($language);

        if (isset($list['errors'])) {
            throw new Exception('An error ocurred creating Mailchimp list');
            die();
        }

        $this->mailchimplibrary->createMergeFields();

        set_flash_message('success', 'Mailchimp list has been created');

        $this->goBack('/admin/mailchimp/index');
    }

}
