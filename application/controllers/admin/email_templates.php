<?php

use App\Libraries\DroplockerObjects\EmailTemplate;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\EmailAction;

class Email_Templates extends MY_Admin_Controller
{

    public $submenu = 'admin';

    /**
     *  Renders the interface for viewing email templates
     * Can take the following GeT parameters
     *  business_language_id
     */
    public function index()
    {
        $this->load->model("business_language_model");

        $content['business_languages_menu'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business->businessID);
        $business_languages = $this->business_language_model->get_aprax(array("business_id" => $this->business->businessID));
        if ($this->input->get("business_language_id")) {
            $content['business_language_id'] = $this->input->get("business_language_id");
        } elseif (is_numeric($this->business->default_business_language_id)) {
            $content['business_language_id'] = $this->business->default_business_language_id;
        } else {
            $content['business_language_id'] = $business_languages[0]->business_languageID;
        }

        $this->load->model("emailaction_model");
        $content['emailActions'] = $this->emailaction_model->getBusinessActions($this->business_id, $content['business_language_id'], false);
        $content['homeDeliveryActions'] = $this->emailaction_model->getBusinessActions($this->business_id, $content['business_language_id'], true);

        // is this business using deliv ?
        $content['home_delivery_setup'] = get_business_meta($this->business_id, 'home_delivery_service');

        $this->setPageTitle('Email Templates');
        $this->renderAdmin('manage_email_templates', $content);
    }

    /**
     * Renders the interface for editing a sms template
     */
    public function edit_sms($emailAction_id = null)
    {
        $this->load->model("business_language_model");
        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $this->load->model("emailactions_model");
        $content['emailAction'] = $this->emailactions_model->get_by_primary_key($emailAction_id);
        $content['emailTemplates_by_business_language_for_action'] = array();
        $content['selected_business_language_id'] = $this->input->get("business_language_id");

        if (empty($content['emailAction'])) {
            set_flash_message("error", "Email Action ID $emailAction_id does not exist");
            $this->goBack("/admin/email_templates");
        }

        if (empty($content['business_languages_dropdown'])) {
            // Begin
            $sms = $this->emailactions_model->getEmailTemplate($this->business_id, $emailAction_id);
            $content['sms'] = $sms;
            $content['name'] = $sms->name;
            $content['message'] = ($sms->customText == '') ? $sms->defaultText : $sms->customText;
            $content['subject'] = ($sms->customSmsSubject == '') ? $sms->defaultSmsSubject : $sms->customSmsSubject;
            $this->setPageTitle('Edit SMS Template');
            // End
        } else {
            $this->load->model("emailTemplate_model");
            $content['emailTemplates_by_business_language_for_action'] = $this->emailTemplate_model->get_templates_by_business_language_for_action_for_business($emailAction_id, $this->business_id);

            //The following statement adds an additional selectable option to view an email tempalte that has no defined business language.
            if (array_key_exists(0, $content['emailTemplates_by_business_language_for_action'])) {
                $content['business_languages'][0] = "&lt;UNKNOWN&gt;";
            }
            $this->setPageTitle('Manage SMS Business Language Templates');
        }

        $content['update_action'] = "/admin/email_templates/update_sms_template/$emailAction_id";

        $this->renderAdmin('edit_sms', $content);
    }

    /**
     * Renders the interface for editing an email template.
     */
    public function edit_email($emailAction_id = null)
    {
        if (!$emailAction_id || !is_numeric($emailAction_id)) {
            show_404();
        }

        $this->load->model("business_language_model");

        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $this->load->model("emailactions_model");
        $content['emailAction'] = $this->emailactions_model->get_by_primary_key($emailAction_id);

        $content['employee'] = $this->employee;
        $content['emailTemplates_by_business_language_for_action'] = array();
        $content['selected_business_language_id'] = $this->input->get("business_language_id");
        if (empty($content['business_languages_dropdown'])) {
            $this->load->model('emailactions_model');
            $email = $this->emailactions_model->getEmailTemplate($this->business_id, $emailAction_id);

            $content['email'] = $email;
            $content['name'] = $email->name;
            $content['message'] = ($email->customEmail == '') ? $email->defaultEmail : $email->customEmail;
            $content['subject'] = ($email->customEmailSubject == '') ? $email->defaultEmailSubject : $email->customEmailSubject;
            $content['textBody'] = ($email->customEmailText == '') ? $email->defaultEmailText : $email->customEmailText;

            $content['emailActionID'] = $emailAction_id;

            $this->setPageTitle('Edit Email Template');
        } else {
            $this->load->model("emailTemplate_model");
            $content['emailTemplates_by_business_language_for_action'] = $this->emailTemplate_model->get_templates_by_business_language_for_action_for_business($emailAction_id, $this->business_id);
            if (array_key_exists(0, $content['emailTemplates_by_business_language_for_action'])) {
                $content['business_languages_dropdown'][0] = "&lt;UNKNOWN&gt";
            }
            $this->setPageTitle("Manage Email Business Langauge Templates");
        }

        //The following variable is used to specify which controller action that the form should submit a request.
        $content['update_action'] = "/admin/email_templates/update_email_template/$emailAction_id";

        $this->renderAdmin('edit_email', $content);
    }

    /**
     * UI for editing the emails that are sent to business admin
     *
     * $_POST['email'] will be an array. This is checkboxes for the system emails set in the business admin section
     * $_POST['customEmails'] will be a string of more emails seperated by a comma.
     *
     * All emails will get put into a comma delimited string
     */
    public function edit_adminalert($emailActionID = null)
    {
        if (!is_numeric($emailActionID)) {
            set_flash_message("error", "'emailActionID' must be passed as segment 4 of the URL and must be numeric.");
            redirect_with_fallback("/admin/email_templates");
        }
        $this->load->model('emailactions_model');

        $emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array(
            "business_id" => $this->business_id,
            "emailAction_id" => $emailActionID
        ), true);

        if (empty($emailTemplates)) {
            $emailTemplate = new \App\Libraries\DroplockerObjects\EmailTemplate();
            $emailTemplate->business_id = $this->business_id;
            $emailTemplate->emailAction_id = $emailActionID;
        } else {
            $emailTemplate = $emailTemplates[0];
        }

        $content['business_id'] = $this->business_id;
        $content['emailTemplate'] = $this->emailactions_model->getEmailTemplate($this->business_id, $emailActionID);
        $content['name'] = $content['emailTemplate']->name;
        $content['recipients'] = unserialize($content['emailTemplate']->adminRecipients);
        $content['message'] = ($content['emailTemplate']->customAdmin == '') ? $content['emailTemplate']->defaultAdmin : $content['emailTemplate']->customAdmin;
        $content['subject'] = ($content['emailTemplate']->customAdminSubject == '') ? $content['emailTemplate']->defaultAdminSubject : $content['emailTemplate']->customAdminSubject;
        $content['update_action'] = "/admin/email_templates/update_adminAlert_template/$emailActionID";

        $content['employee'] = $this->employee;
        $this->setPageTitle('Edit Admin Alert Template');
        $this->renderAdmin('edit_adminAlert', $content);
    }

    /**
     * gets the count of email templates for a business (used in superadmin)
     */
    public function get_total()
    {
        if ($this->input->get("business_id")) {
            try {
                $this->load->model("emailtemplate_model");
                $business_id = $this->input->get('business_id');
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($business_id);
                $emailTemplates_total = $this->emailtemplate_model->count(array("business_id" => $business_id));
                output_ajax_success_response(array("total" => $emailTemplates_total), "Total email templates for '{$business->companyName} ({$business->businessID})'");
            } catch (Exception $e) {
                send_exception_report($e);
                output_ajax_error_response("An internal error ocurred retrieving the email tempalte total.");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }
    }

    /**
     * The following action updates a superadmin email template, which is defined as an email template with no base ID.
     * All email templates whose base ID references the emmil template ID will also be updated.
     * @throws LogicException Thrown if the server is not in Bizzie mode
     * @throws InvalidArgumentException Thrown in if the email action ID is not passed in segment 4 of the URL
    */
    public function update_master_email_template($emailAction_id = null)
    {
        $master_business = get_master_business();
        if (!in_bizzie_mode()) {
            throw new \LogicException("This action can only be accessed when the server is in Bizzie mode.");
        }

        $this->load->model('business_model');
        if (!is_numeric($emailAction_id)) {
            throw new \InvalidArgumentException("'emailAction_id' must be passed as segment 4 of the URL");
        }
        if ($this->input->post("insert")) {
            $emailTemplateID = $this->input->post("insert");
            $master_emailTemplate = new EmailTemplate($emailTemplateID);
        } else {
            $master_emailTemplate = new EmailTemplate();
        }
        $master_emailTemplate->emailSubject = $this->input->post("subject");
        $master_emailTemplate->emailText = $this->input->post("body");
        $master_emailTemplate->bodyText = $this->input->post("bodyText");
        $master_emailTemplate->business_id = $master_business->{Business::$primary_key};
        $master_emailTemplate->emailAction_id = $emailAction_id;
        $master_emailTemplate->save();

        $slave_businesses = $this->business_model->get_all_slave_businesses();

        foreach ($slave_businesses as $slave_business) {
            $emailTemplates = EmailTemplate::search_aprax(array("business_id" => $slave_business->businessID, "emailAction_id" => $emailAction_id));
            if (empty($emailTemplates)) {
                $emailTemplate = new EmailTemplate();
                $emailTemplate = $master_emailTemplate->copy();
            } else {
                $emailTemplate = $emailTemplates[0];
                $emailTemplate->emailSubject = $master_emailTemplate->emailSubject;
                $emailTemplate->emailText = $master_emailTemplate->emailText;
                $emailTemplate->bodyText = $this->input->post("bodyText");
            }
            $emailTemplate->base_id = $master_emailTemplate->{EmailTemplate::$primary_key};
            $emailTemplate->business_id = $slave_business->businessID;
            $emailTemplate->save();
        }
        set_flash_message("success", "Updated master email template {$master->emailTemplate->{EmailTemplate::$primary_key}} and all associated business templates");

        $this->goBack("/admin/superadmin/edit_master_email_template/$emailAction_id");
    }

    /**
     * Updates an email template for the user's current business
     */
    public function update_email_template($emailAction_id = null)
    {
        $this->load->model("business_language_model");
        $business_languages_count = $this->business_language_model->count(array("business_id" => $this->business_id));
        if ($business_languages_count > 0) { //The following conditional block handles the case where a busienss has defined business languages.
            $this->load->library("form_validation");
            $this->form_validation->set_rules("emailActionID", "Email Action ID", "required|is_natural_no_zero");
            $this->form_validation->set_rules("business_language_id", "Business Language ID", "required|is_natural");
            if ($this->form_validation->run()) {
                $emailActionID = $this->input->post("emailActionID");
                $business_language_id = $this->input->post("business_language_id");
                $emailTemplates = EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id, "business_language_id" => $business_language_id));
                if (empty($emailTemplates)) {
                    $emailTemplate = new EmailTemplate();
                    $emailTemplate->business_id = $this->business_id;
                    $emailTemplate->emailAction_id = $emailActionID;
                    $emailTemplate->business_language_id = $business_language_id;
                } else {
                    $emailTemplate = $emailTemplates[0];
                }
                if ($this->input->post("bodyText") !== false) {
                    $emailTemplate->bodyText = $this->input->post("bodyText");
                } else {
                    $emailTemplate->bodyText = "";
                }
                if ($this->input->post("emailSubject") !== false) {
                    $emailTemplate->emailSubject = $this->input->post("emailSubject");
                } else {
                    $emailTemplate->emailSubject = "";
                }
                if ($this->input->post("emailText") !== false) {
                    $emailTemplate->emailText = $this->input->post("emailText");
                } else {
                    $emailTemplate->emailText = "";
                }

                $emailTemplate->save();
                $this->load->model("emailaction_model");
                $emailAction = $this->emailaction_model->get_by_primary_key($emailActionID);

                $this->load->model("business_language_model");
                $business_language = $this->business_language_model->get_by_primary_key($business_language_id);

                $this->load->model("language_model");
                $language = $this->language_model->get_by_primary_key($business_language->language_id);

                set_flash_message("success", "Updated email template for action '{$emailAction->name}' for language '{$language->tag} - {$language->description}'");
            } else {
                set_flash_message("error", validation_errors());
            }
        } else { //The following code is unknown   that handles the legacy case where a business does not have any defined buisness languages.

            $this->load->model("emailTemplate_model");

            $this->load->model('business_model');
            if (!is_numeric($emailAction_id)) {
                throw new \InvalidArgumentException("'emailAction_id' must be passed as segment 4 of the URL");
            }
            //check to see if we need to insert or update
            $this->emailTemplate_model->emailSubject = $_POST['subject'];
            $this->emailTemplate_model->emailText = $_POST['body'];
            $this->emailTemplate_model->bodyText = $_POST['bodytext'];
            $this->emailTemplate_model->business_id = $this->business_id;
            $this->emailTemplate_model->emailAction_id = $emailAction_id;

            if ($_POST['insert'] == '') {
                $res = $this->emailTemplate_model->insert();
            } else {
                $this->emailTemplate_model->where = array('business_id'=>$this->business_id, 'emailAction_id'=>$emailAction_id);
                $res = $this->emailTemplate_model->update();
            }

            if ($res) {
                set_flash_message('success', "Email template has been updated");
            } else {
                set_flash_message('error', "Email template has not been updated");
            }


        }

        $this->goBack('/admin/email_templates/edit_email/'.$emailAction_id);
    }

    /**
     * The following action updates a master business SMS templat  and then updates or creates the same template for all slave businesses. Note, the server must be in Bizzie mode.
     * @throws \LogicException Thrown if the server is not in Bizzie mode
     * @throws \InvalidArgumentException Thrown in the email action ID was not set in the POST body. Thrown if the email action ID is not numeric.
     */
    public function update_master_sms_template()
    {
        if (!in_bizzie_mode()) {
            throw new \LogicException("This action can only be accessed when the server is in Bizzie mode");
        }
        if (!$this->input->post("emailActionID")) {
            throw new \InvalidArgumentException("'emailActionID' must be passed as a POST parameter.");
        }

        $emailActionID = $this->input->post("emailActionID");
        if (!is_numeric($emailActionID)) {
            throw new \InvalidArgumentException("'emailActionID' must be numeric.");
        }
        $master_business = get_master_business();

        $master_emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $master_business->businessID), false);
        if (empty($master_emailTemplates)) {
            $master_emailTemplate = new EmailTemplate();
            $master_emailTemplate->business_id = $master_business->{Business::$primary_key};
            $master_emailTemplate->emailAction_id = $emailActionID;
        } else {
            $master_emailTemplate = $master_emailTemplates[0];
        }

        $master_emailTemplate->smsText = $this->input->post("message");
        $master_emailTemplate->save();
        $this->load->model("business_model");
        $slave_businesses = $this->business_model->get_all_slave_businesses();
        foreach ($slave_businesses as $slave_business) {
            $emailTemplates = EmailTemplate::search_aprax(array("business_id" => $slave_business->businessID, "emailAction_id" => $emailActionID));
            if (empty($emailTemplates)) {
                $emailTemplate = new EmailTemplate();
                $emailTemplate = $master_emailTemplate->copy();
            } else {
                $emailTemplate = $emailTemplates[0];
                $emailTemplate->smsText = $master_emailTemplate->smsText;
            }
            $emailTemplate->base_id = $master_emailTemplate->{EmailTemplate::$primary_key};
            $emailTemplate->business_id = $slave_business->businessID;
            $emailTemplate->save();
        }
        set_flash_message("success", "Updated SMS template {$master_emailTemplate->{EmailTemplate::$primary_key}} for all businesses");

        $this->goBack("/admin/superadmin/edit_email_template/$emailActionID");
    }

    /**
     * The following action updates the properties of an sms template for the user's business
     * @throws \InvalidArgumentException
     */
    public function update_sms_template()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("emailActionID", 'Email Action ID', "required|is_natural_no_zero");

        //The following statements determine whether we need a business language ID.
        $this->load->model("business_language_model");
        $business_languages_count = $this->business_language_model->count(array("business_id" => $this->business_id));
        if ($business_languages_count > 0) { //Note, a business language key is required if there are business languages defined for the business.
            $this->form_validation->set_rules("business_language_id", "Business Language", "required|is_natural_no_zero|does_businesslanguage_exist");
        }

        if ($this->form_validation->run()) {
            $emailActionID = $this->input->post("emailActionID");

            if ($business_languages_count > 0) {
                $emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id, "business_language_id" => $this->input->post("business_language_id")), false);
            } else {
                $emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id), false);
            }
            //The following conditional checks to see if the business alreayd has an e-mail template for this action. If the business does not yet have a template, then we create a new template for the business.
            if (empty($emailTemplates)) {
                $emailTemplate = new \App\Libraries\DroplockerObjects\EmailTemplate();
                $emailTemplate->business_id = $this->business_id;
                $emailTemplate->emailAction_id = $emailActionID;
                if ($business_languages_count > 0) {
                    $emailTemplate->business_language_id = $this->input->post("business_language_id");
                }
            } else { //Otherwise, we update the existing template.
                $emailTemplate = $emailTemplates[0];
            }
            $emailTemplate->smsText = $_POST['message'];
            $emailTemplate->save();
            $this->load->model("emailactions_model");
            $emailAction = $this->emailactions_model->get_by_primary_key($emailActionID);
            if ($business_languages_count > 0) {
                $business_language = $this->business_language_model->get_by_primary_key($emailTemplate->business_language_id);
                set_flash_message("success", "Updated SMS Template for business language '{$business_language->tag}' in action '{$emailAction->name}'");
            } else {
                set_flash_message("success", "Updated SMS Template for action '{$emailAction->name}'");
            }
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/admin");
    }

    /**
     * Updates a master admin alert template for all businesses when the server is in Bizzie mode
     * @throws \InvalidArgumentException Thrown if the email action ID was not passed as segment 4. Thrown if the email action ID is not numeric
     */
    public function update_master_adminalert_template($emailActionID = null)
    {
        if (!is_numeric($emailActionID)) {
            throw new \InvalidArgumentException("'emailActionID' must be passed as segment 4 of the URL and must be numeric.");
        }
        $master_business = get_master_business();
        $master_emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $master_business->businessID), false);
        if (empty($master_emailTemplates)) {
            $master_emailTemplate = new EmailTemplate();
            $master_emailTemplate->emailAction_id = $emailActionID;
            $master_emailTemplate->business_id = $master_business->{Business::$primary_key};

        } else {
            $master_emailTemplate = $master_emailTemplates[0];
        }
        $master_emailTemplate->adminSubject = $this->input->post("subject");
        $master_emailTemplate->adminText = $this->input->post("message");
        $master_emailTemplate->adminRecipients = serialize(array("system" => $this->input->post("email"), "custom" => $this->input->post("customEmails")));
        $master_emailTemplate->save();

        $this->load->model("business_model");
        $slave_businesses = $this->business_model->get_all_slave_businesses();
        foreach ($slave_businesses as $slave_business) {
            $emailTemplates = EmailTemplate::search_aprax(array("business_id" => $slave_business->businessID, "emailAction_id" => $emailActionID));
            if (empty($emailTemplates)) {
                $emailTemplate = new EmailTemplate();
                $emailTemplate = $master_emailTemplate->copy();
            } else {
                $emailTemplate = $emailTemplates[0];
                $emailTemplate->adminSubject = $master_emailTemplate->adminSubject;
                $emailTemplate->adminText = $master_emailTemplate->adminText;
                $emailTemplate->adminRecipients = $master_emailTemplate->adminRecipients;
            }
            $emailTemplate->base_id = $master_emailTemplate->{EmailTemplate::$primary_key};
            $emailTemplate->business_id = $slave_business->businessID;
            $emailTemplate->save();
        }

        set_flash_message("success", "Updated master Admin Alert Template {$master_emailTemplate->emailTemplateID} and all associated business templates");

        redirect($this->agent->referrer());
    }

    /**
     * Updates the admin alert template for the user's buisness
     * Expects the email action ID as segment 4 of the URL
     * @throws \InvalidArgumentException
     */
    public function update_adminalert_template($emailActionID = null)
    {
        $CI = get_instance();
        $CI->load->helper('email');

        if (!is_numeric($emailActionID)) {
            throw new \InvalidArgumentException("'emailActionID' must be passed as segment 4 of the URL and must be numeric.");
        }

        $custom_recipients_string = trim(str_replace(' ','',$_POST['customEmails']));

        $invalid_emails = array();

        if ($custom_recipients_string != '') {

            $custom_recipients = explode(',', $custom_recipients_string);
            foreach ($custom_recipients as $c) {
                if (!valid_email($c)) {
                    $invalid_emails[] = $c;
                }
            }

        }

        if (count($invalid_emails)) {
            set_flash_message('error','Please correct the Custom Emails Field: '.implode(',', $invalid_emails).'. They must be separated with a comma without blank spaces.');
            redirect($this->agent->referrer());
        }

        $this->load->model('emailactions_model');

        $EmailAction = new EmailAction($emailActionID);
        $emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array(
            "business_id" => $this->business_id,
            "emailAction_id" => $emailActionID
        ));
        if (empty($emailTemplates)) {
            $emailTemplate = new \App\Libraries\DroplockerObjects\EmailTemplate();
            $emailTemplate->business_id = $this->business_id;
            $emailTemplate->emailAction_id = $emailActionID;
            $emailTemplate->save();

            $emailTemplates[] = $emailTemplate;
        }

        $recipients['system'] = (array)$_POST['email'];
        $recipients['custom'] = $custom_recipients_string;
        $recipientArray = serialize($recipients);

        foreach ($emailTemplates as $emailTemplate) {
            $emailTemplate->adminSubject = $_POST['subject'];
            $emailTemplate->adminText = $_POST['message'];
            $emailTemplate->adminRecipients = $recipientArray;
            $emailTemplate->save();
        }

        set_flash_message("success", "Updated {$EmailAction->name} Template");

        redirect($this->agent->referrer());
    }
}
