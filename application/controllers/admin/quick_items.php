<?php

use App\Libraries\DroplockerObjects\QuickItem;
use App\Libraries\DroplockerObjects\Product;

class Quick_Items extends MY_Admin_Controller
{
    public $submenu = 'admin';

    /**
     * View quick items
     */
    public function index()
    {
        $sql = "SELECT quickItemID, quickItem.name, product.name AS product_name, process.name AS process_name, quickItem.business_id, quickItem.dateCreated FROM quickItem
            LEFT JOIN product ON product_id=productID
            LEFT JOIN process ON process_id=processID
            WHERE quickItem.business_id = ?";

        $content['quickItems'] = $this->db->query($sql, array($this->business_id))->result();

        $this->renderAdmin('manage_quick_items', $content);
    }


    /**
     * Add a new quick item
     */
    public function add()
    {

        $content['product_id'] = null;
        $content['process_id'] = null;

        $content = $this->getBusinessItemSettings($content);
        $this->renderAdmin('form', $content);
    }

    /**
     * Edit a quick item
     */
    public function edit($quickItemID = null)
    {
        if (!is_numeric($quickItemID)) {
            set_flash_message("error", "'item_id' must be passed as segment 4 of the URL");
            redirect("/admin/quick_items/");
        }

        $this->load->model('quickitem_model');
        $item = $this->quickitem_model->get_by_primary_key($quickItemID);

        if (empty($item)) {
            set_flash_message("error", "Quick item ID '$quickItemID' not found");
            redirect("/admin/quick_items/");
        }

        $content['item'] = $item;
        $content['product_id'] = $item->product_id;
        $content['process_id'] = $item->process_id;

        // get the item product
        if ($item->product_id) {
            $currentProduct = Product::search(array(
                'productID' => $item->product_id
            ));

            $content['currentProductCategory_id'] = $currentProduct->productCategory_id;
        }

        $content = $this->getBusinessItemSettings($content);
        $this->renderAdmin('form', $content);
    }

    /**
     * Creates a new quick item or updates an existing quick item.
     */
    public function save()
    {
        if (!$this->input->post('process_id')) {
            set_flash_message('error', "Missing required field 'process_id'");
            redirect_with_fallback('/admin/quick_items/');
        }

        if (!$this->input->post('product_id')) {
            set_flash_message('error', "Missing required field 'product_id'");
            redirect_with_fallback('/admin/quick_items/');
        }

        $name = $this->input->post('name');

        if ($this->input->post("quickItemID")) {
            $quickItem = new QuickItem($this->input->post("quickItemID"));
            $message = "Updated quick item {$name}";
        } else {
            $quickItem = new QuickItem();
            $message = "Updated quick item {$name}";
        }

        $quickItem->name = $name;
        $quickItem->business_id = $this->business_id;
        $quickItem->product_id = $this->input->post("product_id");
        $quickItem->process_id = $this->input->post("process_id");
        $quickItem->color = $this->input->post("color");

        /* //disabled for quick items
         $quickItem->starch_id = $this->input->post("starch");
        $quickItem->crease_id = $this->input->post("crease");
        $quickItem->notes = $this->input->post("notes");
        if ($this->input->post("upcharges")) {
        $quickItem->upcharges = serialize($this->input->post("upcharges"));
        }
        */

        $quickItem->save();
        set_flash_message("success", $message);

        redirect("/admin/quick_items/");
    }

    /**
     * Deletes a quick item
     */
    public function delete($item_id = null)
    {
        $this->load->model('quickitem_model');

        $deleted = $this->quickitem_model->delete_aprax(array(
            "business_id" => $this->business_id,
            "quickItemID" => $item_id
        ));

        if ($deleted) {
            set_flash_message("success", 'Item was deleted');
        } else {
            set_flash_message("error", "Item was not deleted");
        }

        redirect("/admin/quick_items/");
    }

    protected function getBusinessItemSettings($content = array())
    {

        // needed for the productCategory dropdown in the item_edit subview
        $this->load->model('productcategory_model');
        $content['productCategories'] = $this->productcategory_model->get_all_as_codeigniter_dropdown($this->business_id);
        $content['categories_products'] = $this->productcategory_model->get_productCategories_and_products($this->business_id);

        // get the processes for each product
        $this->load->model("product_process_model");
        $content['products_product_processes'] = $this->product_process_model->get_products_and_product_processes($this->business_id);

        //Note, 'item_specifications', upcharge groups, and color are not used in the quick item edit interface.
        $content['item_specifications'] = array();
        $content['groups'] = array();
        $content['upcharges'] = '';
        $content['specifications'] = array();

        return $content;
    }
}