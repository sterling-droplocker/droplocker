<?php
use App\Libraries\DroplockerObjects\Language;
class Languages extends MY_Admin_Controller
{
    public function add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("tag", "Tag", "required|alpha|is_language_tag_unique");
        $this->form_validation->set_rules("description", "Description", "required");
        if ($this->form_validation->run()) {
            $language = new Language();
            $language->tag = $this->input->post("tag");
            $language->description = $this->input->post("description");
            $language->save();
            set_flash_message("success", "Created new language '{$language->tag}'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_languages");
    }

    /**
     * Updates an existing language's property. This action is intended to be used with the jquery.editable library.
     * Expects the following POST parmaeters:
     *  languageID
     *  property
     *  value
     * Outputs a result response formatted for jquery.editable
     */
    public function update()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("languageID", "Language ID", "required|is_natural_no_zero|does_language_exist");
            $this->form_validation->set_rules("property", "Property", "required|alpha");
            $this->form_validation->set_rules("value", "Value", "required");
            if ($this->form_validation->run()) {
                $property = $this->input->post("property");
                $value = $this->input->post("value");
                $languageID = $this->input->post("languageID");
                $language = new Language($languageID);
                $language->$property = $value;
                $language->save();
                echo $value;
            } else {
                echo validation_errors();
            }
        } else {
            echo "This action can only be accessed through an AJAX request.";
        }
    }
    /**
     * Deletes the specified language.
     * Expects the following GET parameters:
     *  languageID
     */
    public function delete()
    {
        if (isset($_GET)) {
            $_POST = $_GET;
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules("languageID", "Language ID", "is_natural_no_zero|required|does_language_exist|is_language_deletable");
        if ($this->form_validation->run()) {
            $languageID = $this->input->post("languageID");
            $language = new Language($languageID);
            $language->delete();
            set_flash_message("success","Deleted language '{$language->name}'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_languages");
    }

}
