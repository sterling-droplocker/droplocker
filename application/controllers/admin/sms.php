<?php
use App\Libraries\DroplockerObjects\Business;

class Sms extends MY_Admin_Controller
{
    public $submenu = 'admin';

    public function __construct()
    {
        parent::__construct();

        //load SMS model
        $this->load->model('smssettings_model');
    }

    public function index()
    {
        //if we happen to be in bizzie mode, and we're not set as the superadmin, no can do, bounce them out
        if (in_bizzie_mode() && !is_superadmin()) {
            redirect('admin');
        }

        $content = null;
        if (! empty($_POST)) {
            $this->save_sms_settings();
        }

        $content['sms_settings'] = $this->smssettings_model->get_settings($this->business_id);
        if (empty($content['sms_settings'])) {
            $content['sms_settings'] = (object) array(
                'provider' => null,
                'number' => null,
                'mode' => 'production',
                'account_sid' => null,
                'auth_token' => null
            );
        }

        $content['provider_options'] = DropLocker\Service\SMS\Factory::listServices();

        $content['forbidden_words'] = get_business_meta($this->business_id,'sms_forbidden_words');

        $content['environment_mode'] = array(
            'production' => 'Live'
        );

        $this->setPageTitle('SMS Settings');
        $this->renderAdmin('sms', $content);
    }

    /**
     * Handles updating settings
     *
     * @return boolean
     */
    protected function save_sms_settings()
    {
        $sms_data = array();

        //the fields/columns we're accepting
        $sms_data_keys = array(
            'number'      => 'SMS Number',
            'mode'        => 'Mode',
            'account_sid' => 'Account Id/Sid',
            'auth_token'  => 'Auth Token',
            'api_version' => 'API Version',
            'provider'    => 'Service Provider',
            'keyword'     => 'SMS Keyword',
            'disable_unicode' => 'Disable Unicode',
        );

        $this->load->library('form_validation');

        $provider_check = $this->input->post('provider');

        foreach ($sms_data_keys as $field => $label) {
            $required = 'required|trim';                 //the keyword isn't required, everthing else is
            if ($field == 'keyword') {                //if we are on keyword, don't make it required
                $required = 'trim';
            }
            $this->form_validation->set_rules($field, $label, $required); // our validation
            $sms_data_item = $this->input->post($field);
            $sms_data_item = trim($sms_data_item);

            if (! empty($sms_data_item)) {

                if ($field == 'number') {
                    $sms_data_item = str_replace('-', '', trim($sms_data_item)); // remove the hyphens if need be

                    // if its a nubmer field, and we're missing the + and proper formatting
                    if ($provider_check == 'twilio' && strpos($sms_data_item, '+') === FALSE) {
                        $sms_data_item = '+' . $sms_data_item;
                    }
                }

                $sms_data[$field] = $sms_data_item;
            }
        }

        if (empty($sms_data)) {
            return false;
        }

        if (!$this->form_validation->run()) {
            set_flash_message("error", validation_errors());
            redirect('admin/sms');
        }

        $sms_data['business_id'] = $this->business_id;
        $this->smssettings_model->update_settings($sms_data);
        update_business_meta($this->business_id,'sms_forbidden_words',$this->input->post('forbidden_words'));

        set_flash_message("success", "Updated SMS Settings!");
        redirect('admin/sms');
    }
}
