<?php
use App\Libraries\DroplockerObjects\AclResource;
class Aclresources extends MY_Admin_Controller
{
    /**
     * Adds a new acl resource to all businesses
     * Expects the following POST parameters
     */
    public function add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("resource", "Resource", "required|alpha_dash");
        $this->form_validation->set_rules("description", "Description", "required");
        if ($this->form_validation->run()) {
            $this->load->model("business_model");
            /**
            $businesses = $this->business_model->get_all();
            foreach ($businesses as $business) {
                $aclResource = new AclResource();
                $aclResource->resource = $this->input->post("resource");
                $aclResource->description = $this->input->post("description");
                $aclResource->business_id = $business->businessID;
                $aclResource->default_value = "false";
                $aclResource->save();
                set_flash_message("success", "Added new ACL Resource '{$aclResource->resource}'");
            }
             *
             */
            $aclResource = new AclResource();
            $aclResource->resource = $this->input->post("resource");
            $aclResource->description = $this->input->post("description");
            $aclResource->default_value = "false";
            $aclResource->save();
            set_flash_message("success", "Added new ACL Resource '{$aclResource->resource}'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_aclresources");
    }

    public function delete()
    {
        if ($this->input->get("resource")) {
            $resource = $this->input->get("resource");
            $aclResources = AclResource::search_aprax(array("resource" => $resource));
            foreach ($aclResources as $aclResource) {
                $aclResource->delete();
            }
            set_flash_message("success", "Deleted resource '$resource'");
        } else {
            set_flash_message("error", "'resource' must be passed as a GET parameter");
        }

        $this->goBack("/admin/superadmin/manage_aclresources");
    }
}
