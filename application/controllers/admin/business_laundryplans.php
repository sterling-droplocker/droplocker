<?php

class Business_LaundryPlans extends MY_Admin_Controller
{
    public function get_total()
    {
        if ($this->input->get("business_id")) {
            try {
                $this->load->model("businesslaundryplan_model");
                $business_id = $this->input->get("business_id");
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($business_id);
                $businessLaundryPlans_total = $this->businesslaundryplan_model->count(array("business_id" => $business_id, "type" => "laundry_plan"));
                output_ajax_success_response(array("total" => $businessLaundryPlans_total), "Total business laundry plans for '{$business->companyName} ({$business->businessID})'");
            } catch (Exception $e) {
                send_exception_report($e);
                output_ajax_error_response("An internal error occurred retrieving the email template total");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }

    }
}
