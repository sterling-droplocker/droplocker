<?php
use App\Libraries\DroplockerObjects\Business;
class Export extends CI_Controller
{
    public $business_id;

    /**
     * @var FabricareLibrary
     */
    public $fabricarelibrary;

    public function __construct()
    {
        parent::__construct();

        //authorization
        $printer_key = $this->uri->segment(5);
        $this->business_id = $this->uri->segment(4);
        $key = get_business_meta( $this->business_id , 'printer_key');
        if ($printer_key != $key) {
            $message = "The printer key in the Settings of this application does not match the printer key in your DropLocker admin panel";
            die($message);
        }
    }

    public function test($business_id)
    {
        $business = new Business($business_id);
        date_default_timezone_set($business->timezone);

        $this->load->library('FabricareLibrary', array( 'business_id' => $business_id ));
        $this->fabricarelibrary->export();
        exit;
    }

    public function fabricare($business_id = 0, $printer_key = '', $bag_number = 0, $save_on_server = false)
    {
        $business = new Business($business_id);
        date_default_timezone_set($business->timezone);

        $this->load->library('FabricareLibrary', array( 'business_id' => $business_id, 'bag_number' => $bag_number ));
        $this->fabricarelibrary->export( false, true, $save_on_server ); //$as_file = false, $automat_ref = true, $save_on_server = false
        exit;
    }

    public function process_fabricare_response()
    {
        //echo 'here';
        $response_file_name = 'RESPONSE.XML';
        if ( empty( $this->business_id ) ) {

            throw new Exception('Missing Business ID');
        }
        $business = new Business( $this->business_id );
        $this->load->library('FabricareLibrary', array( 'business_id' => $this->business_id ), 'fabricare');
        $path =  "/home/droplocker/integration/" . $this->business_id;

        $handler = opendir($path);

        $file_path = DEV ? "/tmp/" : "/home/droplocker/";
        $file_path .= 'fabricare/' . $this->business_id . '/';

        if (!is_dir($file_path)) {
            mkdir($file_path, 0777, true);
        }

        $file_path .= gmdate('Y-m-d_H.i.s_');

        // keep going until all files in directory have been read
        while (false !== ($file = readdir($handler))) {
            echo 'reading files';
            if (($file != ".") and ($file != "..")) {
                if ( strtoupper($file) == $response_file_name ) { //interested in second chunk only
                    echo "Processing file: " . $path . "/" . $file . "<br>";

                    $xml_file_string = file_get_contents( $path . "/" . $file );

                    file_put_contents($file_path . 'response.xml', $xml_file_string);

                    echo $xml_file_string;

                    $result = $this->fabricare->parseResponse( $xml_file_string ); //processing the response into the automat table

                    //delete the response file here
                    //
                }
            }
        }
        // tidy up: close the handler
        closedir($handler);
    }

}
