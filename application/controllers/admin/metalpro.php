<?php
use App\Libraries\DroplockerObjects\Business;
class MetalPro extends CI_Controller
{
    public $business_id;

    public function __construct()
    {
        parent::__construct();

        //authorization
        $printer_key = $this->uri->segment(5);
        $this->business_id = $this->uri->segment(6);
        $key = get_business_meta($this->business_id , 'printer_key');
        if ($printer_key != $key) {
            $message = "The printer key in the Settings of this application does not match the printer key in your DropLocker admin panel";
            die($message);
        }

    }


    public function emptyConveyor($barcode = null, $key = null, $business_id = null)
    {
        $request['barcode'] = $barcode;
        $request['business_id'] = $business_id;

        $data['request'] = $request;
        $this->load->view('admin/metalpro/emptyconveyor', $data);
    }

    public function getOrders($bagNumber = null, $key = null, $business_id = null)
    {
        $request['bagNumber'] = $bagNumber;
        $request['business_id'] = $business_id;

        $data['request'] =$request;
        $this->load->view('admin/metalpro/getorders', $data);
    }

    public function bam($bagNumber = null, $key = null, $business_id = null)
    {

        //get all orders in status inventoried or on payment hold
        $getAllOrders = 'SELECT DISTINCT order_id, orders.customer_id, bagNumber
			FROM `orderItem`
			JOIN orders ON (orderItem.order_id = orderID)
			LEFT JOIN bag ON (bagID = orders.bag_id)
			WHERE orderStatusOption_id IN (3,7)
			AND orders.business_id = ?
			AND item_id > 0
			ORDER BY orderItem.order_id';

        $orders = $this->db->query($getAllOrders, array($business_id))->result();


        $loadBags = get_business_meta($business_id, 'loadBags');

        $data = array();

        $format = array(
            'lot' => '%-20d',
            'ngarm' => '%02d',
            'garment' => '%02d',
            'garcode' => '%-20d',
            'gardesc' => '%-30s',
            'perc' => '%3d',
            'hung' => '%s',
            'status' => '%s',
            'mag' => '%2s',
            'slot' => '%5s',
            'out' => '%5s',
            'customer_id' => '%-20d',
            'ordertype' => '%s',
            'orderkey' => str_repeat(' ', 20),
            'arm' => str_repeat(' ', 3),
        );

        $line_format = implode('', $format);

        foreach ($orders as $order) {

            $checkAutomat = "SELECT automatID FROM automat WHERE order_id = ? AND slot != ''";
            $found = $this->db->query($checkAutomat, array($order->order_id))->row();
            if ($found) {
                continue;
            }


            $getItems = "select name, displayName, itemID, orderItem.notes, barcode, width
            FROM orderItem
            JOIN item ON (itemID = orderItem.item_id)
            JOIN product_process ON (product_processID = item.product_process_id)
            JOIN product ON (productID = product_process.product_id)
            LEFT JOIN barcode ON (barcode.item_id = itemID)
            WHERE order_id = ?
            AND product.name not like '%box%'
            AND orderItem.notes not like '%box%'";

            $items = $this->db->query($getItems, array($order->order_id))->result();

            $ngarm = count($items);

            $ordItemCount = 1;
            if ($loadBags) {
                $ngarm += 1;

                $data[] = vsprintf($line_format, array(
                    'lot' => $order->order_id,
                    'ngarm' => $ngarm,
                    'garment' => $ordItemCount,
                    'garcode' => $order->bagNumber,
                    'gardesc' => 'bag',
                    'perc' => 100,
                    'hung' => 'S',
                    'status' => 'R',
                    'mag' => '',
                    'slot' => '',
                    'out' => '',
                    'customer_id' => $order->customer_id,
                    'ordertype' => '',
                    ));

                $ordItemCount++;
            }

            foreach ($items as $item) {
                $data[] = vsprintf($line_format, array(
                    'lot' => $order->order_id,
                    'ngarm' => $ngarm,
                    'garment' => $ordItemCount,
                    'garcode' => $item->barcode,
                    'gardesc' => $item->displayName,
                    'perc' => $item->width,
                    'hung' => 'S',
                    'status' => 'R',
                    'mag' => '',
                    'slot' => '',
                    'out' => '',
                    'customer_id' => $order->customer_id,
                    'ordertype' => '',
                ));

                $ordItemCount++;
            }
        }


        $business = new Business($business_id);
        $prismFile = "/home/".$business->slug."/integration/AUTOMAT.IN";
        $prismFile = "/tmp/AUTOMAT.IN";

        $data = implode("\r\n", $data);
        file_put_contents($prismFile, $data);

        header('Content-Type: text/plain');
        echo $data;
    }

    public function postAutomat()
    {
        //$request['fileDate'] =
    }



    public function uploadFile()
    {
        if (empty($this->business_id)) {

            throw new Exception('Missing Business ID');

        }

        $business = new Business($this->business_id);
        $path = "/home/".$business->slug."/integration/";
        $handler = opendir($path);

        // keep going until all files in directory have been read
        while (false !== ($file = readdir($handler))) {
            if (($file != ".") and ($file != "..")) {
                $fileChunks = explode(".", $file);
                if (strtoupper($fileChunks[1]) == 'LOG') { //interested in second chunk only
                    echo "Processing file: ".$path.$file."<br>";

                    $lines = file($path.$file);
                    $lineNumber = 0;
                    foreach ($lines as $line_num => $line) {
                        //if ($line_num == 20) {break;}

                        $fileName = substr($line, 0, 21);
                        If ($fileName <> "C:\MAPEXC\AUTOMAT.TMP") {
                            continue;
                        }

                        $fileMo = substr($line, 25, 2);
                        $fileDay = substr($line, 22, 2);
                        $fileYr = substr($line, 28, 4);
                        $fileHr = substr($line, 33, 2);
                        $fileMin = substr($line, 36, 2);
                        $fileSec = substr($line, 39, 2);
                        $fileDate = $fileYr . "-" . $fileMo . "-" . $fileDay . " " . $fileHr . ":" . $fileMin . ":" . $fileSec;
                        $fileDate = convert_to_gmt_aprax(date("Y-m-d H:i:s", strtotime($fileDate)), $business->timezone);

                        $order = trim(substr($line, 49, 20));
                        If ($order == "") {
                            continue;
                        }
                        $ngarm = trim(substr($line, 70, 2));
                        $garment = trim(substr($line, 72, 2));
                        If ($garment == "0") {
                            continue;
                        }
                        $garcode = trim(substr($line, 74, 20));
                        $gardesc = trim(substr($line, 94, 30));
                        $perc = trim(substr($line, 124, 3));
                        $hung = trim(substr($line, 127, 1));
                        $status = trim(substr($line, 128, 1));
                        $mag = trim(substr($line, 129, 2));
                        $slot = trim(substr($line, 131, 5));
                        $out = trim(substr($line, 136, 5));
                        $customer = trim(substr($line, 141, 20));
                        $rfid = trim(substr($line, 161, 8));
                        $arm = trim(substr($line, 182, 5));

                        $insert1 = "INSERT IGNORE INTO automat
                            (order_id, ngarm, garment, garcode, gardesc, perc, hung, stat, mag, slot, outPos, customer, rfid, arm, fileName, fileDate, lineNumber)
                            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        //echo $insert1;
                        $insertDetail = $this->db->query($insert1, array($order, $ngarm, $garment, $garcode, $gardesc, $perc, $hung, $status, $mag, $slot, $out, $customer, $rfid, $arm, $fileName, $fileDate, $line_num));
                        echo "Order: ".$order . ' - SUCCESS<br>';

                    }
                    if (unlink($path.$file)) {
                        echo "Deleted File: ".$path.$file." At: ".date("c")."<br>";
                    }
                }
            }
        }
        // tidy up: close the handler
        closedir($handler);
    }

}
