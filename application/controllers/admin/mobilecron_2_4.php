<?php

use DropLocker\Util\Lock;

class MobileCron_2_4 extends CI_Controller
{
    protected $appVersion = '2.4'; //change this to 3.0 when moving to the new app
    
    public function processTimeCards()
    {
        if (!Lock::acquire('processTimeCards') ) {
            throw new \Exception("TimeCard lockfile present. Cannot processes new timecard entries");
        }

        $timeCard = new \App\Libraries\DroplockerObjects\TimeCard();
        $droidTimeCardEntry = new \App\Libraries\DroplockerObjects\DroidPostTimeCard();
        $entries = $droidTimeCardEntry->getPending();

        foreach ($entries as $entry) {
            try {
                $timeCard->processEntry($entry);
            } catch (\Exception $e) {
                send_exception_report($e);
            }
        }
    }

    /**
    * Gets all the pickups that are in "pending" status and processes them
    *
    * @uses _postPickups
    * @return boolean
    */
    public function processAllPickups()
    {
        $this->load->model('droidpostpickups_model');
        $pickups = $this->droidpostpickups_model->get(array('status'=>'pending', 'version'=>$this->appVersion));

        if (!$pickups) {
            return true;
        }

        foreach ($pickups as $pickup) {
           $this->droidpostpickups_model->processPickup($pickup);
        }
    }

    /**
    * Gets all the pickups that are in "pending" status and processes them
    *
    *
    * @return boolean
    */
    public function processAllDeliveries()
    {
       if (!Lock::acquire('processAllDeliveries')) {
            return;
        }

        $this->load->model('droidpostdeliveries_model');
        $deliveries = $this->droidpostdeliveries_model->get(array('status'=>'pending', 'version'=>'2.4'));

        if (!$deliveries) {
            //echo "No Deliveries To Process";
            return true;
        }

        foreach ($deliveries as $delivery) {
            //echo  $delivery->ID."\n";
            $this->droidpostdeliveries_model->processDelivery($delivery);

        }
    }


    /**
    * gets all the pickups reminders from the database and processes them
    *
    * @return boolean
    */
    public function processAllPickupReminders()
    {
        $this->load->model('droidpostpickupreminders_model');
        $reminders = $this->droidpostpickupreminders_model->get(array('status'=>'pending', 'version'=>'2.4'));

        if (!$reminders) {
            return true;
        }

        foreach ($reminders as $reminder) {
            $this->droidpostpickupreminders_model->processPickupReminder($reminder);
        }
    }


    /**
    * Processes all the return to office reminders
    *
    * @return boolean
    */
    public function processAllReturnToOffices()
    {
        $this->load->model('droidpostreturntooffice_model');
        $reminders = $this->droidpostreturntooffice_model->get(array('status'=>'pending', 'version'=>'2.4'));

        if (!$reminders) {
            return true;
        }

        foreach ($reminders as $reminder) {
            $this->droidpostreturntooffice_model->processReturnToOffice($reminder);
        }
    }


    /**
     * processes all claim notes
     */
    public function processClaimNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllClaimNotes();
    }

    /**
     * Process all the delivery notes
     */
    public function processDeliveryNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllDeliveryNotes();
    }


    /**
     * processes all the driver notes
     *
     * @return boolean
     */
    public function processDriverNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllDriverNotes();
    }

    /**
     * processes all issues sent from drivers
     *
     * @return boolean
     */
    public function processIssues()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllIssues();

    }



}
