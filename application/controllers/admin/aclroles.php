<?php

/**
 * Helper actions for acl roles
 *
 * @author ariklevy
 */
class Aclroles extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("aclroles_model");
    }
    /**
     * Retrieves the total number of aclroles for a particular business
     * Expects the following GET parameter:
     *  business_id
     * @throws \InvalidArgumentException if the request does not include a 'business_id' GET parameter
     * @throws \LogicException if the request does is not AJAX
     */
    public function get_total()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get('business_id')) {
                try {
                    $business_id = $this->input->get('business_id');
                    $aclroles_total = $this->aclroles_model->get_count($business_id);
                    $result = array('status' => 'success', 'total' => $aclroles_total);
                } catch (Exception $e) {
                    $result = array('status' => 'error', 'message' => 'Could not retreive aclroles count');
                    send_exception_report($e);
                }
                echo json_encode($result);
            } else {
                throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter");
            }
        } else {
            throw new \LogicException("This action is only accessible with an AJAX request.");
        }
    }

    /**
     * Retrieves all the ACL roles for a partiucular business in a codeigniter dropdown compatiable JSON vormat
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function get_as_json()
    {
        if (!$this->input->get("business_id")) {
            throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter");
        }
        if ($this->input->is_ajax_request()) {
            $business_id = $this->input->get("business_id");

            $aclRoles = $this->aclroles_model->get_all_as_codeigniter_dropdown($business_id);
            echo json_encode($aclRoles);
        } else {
            throw new \LogicException("This action is only accessible with an AJAX request");
        }
    }

}
