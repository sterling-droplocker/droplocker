<?php
use App\Libraries\DroplockerObjects\ProductCategory;

class Product_Categories extends MY_Admin_Controller
{
    private $name = "productCategory";
    public function __construct()
    {
        parent::__construct();
        $this->load->model("productCategory_model");
    }
    public function update_sort_order()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("productCategories", "Product Categories", "required");
        if ($this->form_validation->run()) {
            $productCategories = $this->input->post("productCategories");
            foreach ($productCategories as $sort_order => $productCategoryID) {
                $productCategory = new ProductCategory($productCategoryID);
                $productCategory->sort_order = $sort_order;
                $productCategory->save();
            }
            output_ajax_success_response(array(), "Updated product category sort order");
        } else {
            output_ajax_error_response(validation_errors());
        }
    }
    /**
     * Retrieves the total number of product categories in a particular business.
     */
    public function get_total()
    {
        if ($this->input->get("business_id")) {
            try {
                $business_id = $this->input->get("business_id");
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($business_id);
                $query = "SELECT COUNT(productCategoryID) AS total FROM productCategory WHERE productCategory.business_id=?";
                $result = $this->db->query($query, array($business_id))->row();
                output_ajax_success_response(array("total" => $result->total), "Total product categories for '{$business->companyName} ({$business->businessID})'");
            } catch (Exception $e) {
                send_exception_report($e);
                output_ajax_error_response("An internal error occurred attempting to retrieve the product categories total");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter.");
        }
    }
    /**
     * Creates a new Product Category
     * Expects teh following POST parameters
     *  name
     *  note
     *  module_id
     */
    public function add()
    {
        if (!$this->input->post("name")) {
            set_flash_messsage("error", "Missing 'name' parameter");
        } elseif (!$this->input->post("note")) {
            set_flash_message("error", "Missing 'note' parameter");
        } elseif (!$this->input->post("module_id")) {
            set_flash_message("error", "Missing 'module_id' parameter.");
        } else {
            $name = $this->input->post("name");
            $note = $this->input->post("note");

            $slug = convert_to_slug($name, false);
            $module_id = $this->input->post("module_id");

            $productCategory = new \App\Libraries\DroplockerObjects\ProductCategory();
            $productCategory->name = $name;
            $productCategory->slug = $slug;
            $productCategory->module_id = $module_id;
            $productCategory->business_id = $this->business_id;
            $productCategory->note = $note;
            $productCategory->save();

            $module = new \App\Libraries\DroplockerObjects\Module($module_id);

            set_flash_message("success", "Successfully added product category '$name' to '{$module->name}' module");
        }
        redirect($this->agent->referrer());
    }

    /**
     * Updates a property of  existing product category.
     * This function is intended to interact with a request from the  jQuery jeditable plugin
     * Expects the following POST parameters
     *  productCategoryID : The Product Category's primary key
     *  property : The Product Category's property name
     *  value : the value for the Product Category's property. If not passed, then the property is set to empty.
     */
    public function update()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("productCategoryID", "Product Category ID",  "required|is_natural_no_zero");
        $this->form_validation->set_rules("property", "Property", "required");
        if ($this->form_validation->run()) {
            $productCategoryID = $this->input->post("productCategoryID");
            $property = $this->input->post("property");
            if ($this->input->post("value") === false) {
                $value = "";
            } else {
                $value = $this->input->post("value");
            }

            if ($property == 'slug') {
                $value = convert_to_slug($value, false);
            }

            $productCategory = new ProductCategory($productCategoryID);
            $productCategory->$property = $value;
            try {
                $productCategory->save();
                if ($property == 'parent_id') {
                    if ($value) {
                        $parent = new ProductCategory($value);
                        $value = $parent->name;
                    } else {
                        $value = '--None--';
                    }
                }
                echo $value;
            } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                send_exception_report($validation_exception);
                echo $validation_exception->getMessage();
            }
        } else {
            echo validation_errors();
        }
    }
}
