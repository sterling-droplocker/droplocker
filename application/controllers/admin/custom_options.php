<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Business_Language;
use App\Libraries\DroplockerObjects\CustomOption;
use App\Libraries\DroplockerObjects\CustomOptionValue;
use App\Libraries\DroplockerObjects\CustomOptionName;

class Custom_Options extends MY_Admin_Controller
{

    public $submenu = 'admin';

    public



    function __construct()
    {
        parent::__construct();

        $this->load->model('customoption_model');
    }

    function index()
    {
        $options = $this->customoption_model->get_aprax(array(
            'business_id' => $this->business_id
        ), 'sortOrder ASC');

        $content = compact('options');
        $this->pageTitle = "Custom Options";
        $this->renderAdmin('index', $content);
    }

    function add($option_name = null)
    {
        if (! empty($_POST['name'])) {
            $option_name = $_POST['name'];
        }

        if ($option_name) {
            $option = $this->getOptionByName($option_name);
            if (! empty($option)) {
                set_flash_message("error", "An option with the same name already exists, edit that option instead");
                return redirect('/admin/custom_options/edit/' . $option->customOptionID);
            }
        }

        $this->load->model('business_language_model');
        $languages = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        if (! empty($_POST['name'])) {
            $option = new CustomOption();
            $option->business_id = $this->business_id;
            $option->name = convert_to_slug($_POST['name']);
            $option->sortOrder = $_POST['sortOrder'];

            try {
                $option->save();

                if (!empty($_POST['title'])) {
                    $option->saveOptionNames($_POST['title']);
                }

                set_flash_message("success", "The option has been saved");
                return redirect('/admin/custom_options/edit/' . $option->customOptionID);
            } catch (Exception $e) {
                set_flash_message("error", $e->getMessage());
                return redirect('/admin/custom_options/add/');
            }
        }

        $option = (object) array(
            'name' => $option_name,
            'sortOrder' => $this->customoption_model->getMaxSortOrder($this->business_id) + 1
        );
        $content = compact('option', 'languages');
        $this->pageTitle = "Add Custom Options";
        $this->renderAdmin('add', $content);
    }

    function edit($option_id = null)
    {
        if (! ctype_digit($option_id)) {
            $option = $this->getOptionByName(urldecode($option_id));
            if ($option) {
                return redirect('/admin/custom_options/edit/' . $option->customOptionID);
            } else {
                return redirect('/admin/custom_options/add/' . $option_id);
            }
        }

        $option = $this->getOptionByID($option_id);
        if (empty($option)) {
            set_flash_message("error", "The option does not exists");
            return redirect('/admin/custom_options');
        }

        if (! empty($_POST['name'])) {
            $option->business_id = $this->business_id;
            $option->name = convert_to_slug($_POST['name']);
            $option->sortOrder = $_POST['sortOrder'];

            try {
                $option->save();

                if (!empty($_POST['title'])) {
                    $option->saveOptionNames($_POST['title']);
                }

                set_flash_message("success", "The option has been saved");
                return redirect('/admin/custom_options/edit/' . $option->customOptionID);
            } catch (Exception $e) {
                set_flash_message("error", $e->getMessage());
                return redirect('/admin/custom_options/add/');
            }
        }

        $this->load->model('business_language_model');
        $languages = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $optionNames = $option->getOptionNames();

        $this->load->model('customoptionvalue_model');
        $values = $this->customoptionvalue_model->get_aprax(array(
            'customOption_id' => $option->customOptionID
        ), 'sortOrder ASC');
        $new_sort_order = $this->customoptionvalue_model->getMaxSortOrder($this->business_id, $option->customOptionID) + 1;

        $content = compact('option', 'languages', 'values', 'new_sort_order', 'optionNames');
        $this->pageTitle = "Edit Custom Option";
        $this->renderAdmin('edit', $content);
    }

    function delete($option_id = null)
    {
        $option = $this->getOptionByID($option_id);
        if (empty($option)) {
            set_flash_message("error", "The option does not exists");
            return redirect('/admin/custom_options');
        }

        if (! empty($_POST['delete'])) {
            $option->delete();
            set_flash_message("success", "The option has been deleted");
            return redirect('/admin/custom_options');
        }

        $content = compact('option');
        $this->pageTitle = "Delete Custom Option";
        $this->renderAdmin('delete', $content);
    }

    function add_value($option_id = null)
    {
        $option = $this->getOptionByID($option_id);
        if (empty($option)) {
            set_flash_message("error", "The option does not exists");
            return redirect('/admin/custom_options');
        }

        if (! empty($_POST['value'])) {
            $value = new CustomOptionValue();
            $value->customOption_id = $option->customOptionID;
            $value->business_id = $this->business_id;
            $value->value = $_POST['value'];
            $value->default = $_POST['default'];
            $value->sortOrder = $_POST['sortOrder'];

            try {
                $value->save();

                if (!empty($_POST['title'])) {
                    $value->saveValueNames($_POST['title']);
                }

                set_flash_message("success", "The value has been saved");
                return redirect('/admin/custom_options/edit/' . $option->customOptionID);
            } catch (Exception $e) {
                set_flash_message("error", $e->getMessage());
                return redirect('/admin/custom_options/edit/' . $option->customOptionID);
            }
        }

        redirect('/admin/custom_options/edit/' . $option->customOptionID);
    }

    function edit_value($value_id = null)
    {
        $value = $this->getValueByID($value_id);
        if (empty($value)) {
            set_flash_message("error", "The value does not exists");
            return redirect('/admin/custom_options');
        }

        if (! empty($_POST['value'])) {
            $value->value = $_POST['value'];
            $value->default = $_POST['default'];
            $value->sortOrder = $_POST['sortOrder'];

            try {
                $value->save();
                if (!empty($_POST['title'])) {
                    $value->saveValueNames($_POST['title']);
                }

                set_flash_message("success", "The value has been saved");
                return redirect('/admin/custom_options/edit/' . $value->customOption_id);
            } catch (Exception $e) {
                set_flash_message("error", $e->getMessage());
                return redirect('/admin/custom_options/edit_value/' . $value->customOption_id);
            }
        }

        $this->load->model('business_language_model');
        $languages = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $valueNames = $value->getValueNames();

        $content = compact('value', 'languages', 'valueNames');
        $this->pageTitle = "Edit Value";
        $this->renderAdmin('edit_value', $content);
    }

    function delete_value($value_id = null)
    {
        $value = $this->getValueByID($value_id);
        if (empty($value)) {
            set_flash_message("error", "The value does not exists");
            return redirect('/admin/custom_options');
        }

        if (! empty($_POST['delete'])) {
            $value->delete();
            set_flash_message("success", "The option has been deleted");
            return redirect('/admin/custom_options/edit/' . $value->customOption_id);
        }

        $content = compact('value');
        $this->pageTitle = "Delete Value";
        $this->renderAdmin('delete_value', $content);
    }

    /**
     * Gets a CustomOption by name checking the business_id
     *
     * @param string $option_name
     * @return CustomOption
     */
    protected function getOptionByName($option_name)
    {
        $options = CustomOption::search_aprax(array(
            'name' => $option_name,
            'business_id' => $this->business_id
        ));
        return $options ? current($options) : false;
    }

    /**
     * Gets a CustomOption by id checking the business_id
     *
     * @param Integer $option_id
     * @return CustomOption
     */
    protected function getOptionByID($option_id)
    {
        $options = CustomOption::search_aprax(array(
            'customOptionID' => $option_id,
            'business_id' => $this->business_id
        ));
        return $options ? current($options) : false;
    }

    /**
     * Gets a CustomOptionValue by id checking the business_id
     *
     * @param Integer $value_id
     * @return CustomOptionValue
     */
    protected function getValueByID($value_id)
    {
        $values = CustomOptionValue::search_aprax(array(
            'customOptionValueID' => $value_id,
            'business_id' => $this->business_id
        ));
        return $values ? current($values) : false;
    }
}
