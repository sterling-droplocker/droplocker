<?php

class Emp_Routes extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Updates an array of route
     * Expects the following POST parametrs
     * $routes array An array whose indecies are the order IDs and the values are the employe IDs
     */
    function update_routes()
    {
        $routes = $this->input->post("routes");
        $this->load->model('empRoutes_model');
        $business_id = $this->session->userdata['business_id'];
        foreach ($routes as $route_id => $driver_id) {
            // Note, a driver_id of 0 means that no driver was selected for the route, so we remove that row from the table.
            if ($driver_id != 0) {
                $this->empRoutes_model->options = array(
                    "route_id" => $route_id,
                    "business_id" => $business_id
                );
                $routes = $this->empRoutes_model->get();
                $this->empRoutes_model->clear();
                // The following code checks to see if we already have a defined route to update.
                if (count($routes) > 0) {
                    $this->empRoutes_model->where = array(
                        "route_id" => $route_id,
                        "business_id" => $business_id
                    );
                    $this->empRoutes_model->employee_id = $driver_id;
                    $update_result = $this->empRoutes_model->update();
                }                 // The following conditional inserts a new route if the route does not already exist in the table.
                else {
                    $this->empRoutes_model->clear();
                    $this->empRoutes_model->options = array(
                        "route_id" => $route_id,
                        "employee_id" => $driver_id,
                        "business_id" => $business_id
                    );
                    $insert_result = $this->empRoutes_model->insert();
                }
            } else { // Removes the route from the table
                $this->empRoutes_model->options = array(
                    "route_id" => $route_id,
                    "business_id" => $business_id
                );
                $routes = $this->empRoutes_model->delete();
            }
            $this->empRoutes_model->clear();
        }
        set_flash_message('success', "Update Successful");
        redirect("/admin/reports/assign_drivers");
    }
}
