<?php

use App\Libraries\DroplockerObjects\App;

class Apps extends MY_Admin_Controller
{

    /**
     *
     * @var \App\Libraries\DroplockerObjects\App
     */
    protected $app;
    protected $bucket        = 'droplockeriphone';
    protected $upload_config = array(
        'upload_path'   => './images/iPhoneApp',
        'allowed_types' => 'png',
        'overwrite'     => 'true',
        'remove_spaces' => FALSE
    );

    public function __construct()
    {
        parent::__construct();

        $this->app = App::search(array(
            "business_id" => $this->business_id,
        ));

        if (!$this->app) {
            $this->app = new App();
            $this->app->business_id = $this->business_id;
        }
    }

    /**
     * Updates the non file related fields of the app.
     */
    public function update_text_fields()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("name", "Name", "required");

        if (!$this->form_validation->run()) {
            set_flash_message("error", validation_errors());
            return $this->goBack("/admin/website/manage_iphone_app");
        }

        $this->app->token            = $this->input->post("token");
        $this->app->secret           = $this->input->post("secret");
        $this->app->active           = $this->input->post("active");
        $this->app->description      = $this->input->post("description");
        $this->app->keywords         = $this->input->post("keywords");
        $this->app->name             = $this->input->post("name");
        $this->app->support_url      = $this->input->post("support_url");
        $this->app->marketing_url    = $this->input->post("marketing_url");
        $this->app->development_mode = $this->input->post("development_mode");

        try {
            $this->app->save();
            set_flash_message("success", "Updated iPhone App Data Fields");
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
            set_flash_message("error", $validation_exception->getMessage());
        }

        $this->goBack("/admin/website/manage_iphone_app");
    }

    /**
     * Expects 'androidFeatureGraphicFile' set in the $_FILES super array
     */
    public function updateAndroidFeatureGraphic()
    {
        $slug = convert_to_slug($this->app->name);
        $this->load->library("upload", $this->upload_config + array(
            'max_width'  => 1024,
            'min_width'  => 1024,
            'max_height' => 500,
            'min_height' => 500,
            'file_name'  => "{$slug}-AndroidFeatureGraphic.png",
        ));

        if (!$this->upload->do_upload("androidFeatureGraphicFile")) {
            set_flash_message("error", $this->upload->display_errors());
            return $this->goBack("/admin/webiste/manage_iphone_app");
        }

        $src = $this->upload->upload_path.$this->upload->file_name;
        $dst = $this->upload->file_name;

        $url = $this->s3Upload($src, $dst);
        $this->app->android_feature_graphic_path = $url;
        $this->app->save();

        set_flash_message("success", "Updated Android feature graphic to '$url");
        $this->goBack("/admin/webiste/manage_iphone_app");
    }

    /**
     * Expects 'app_large_icon' set in the $_FILES super array
     */
    public function update_app_icon_path()
    {
        $slug = convert_to_slug($this->app->name);
        $this->load->library("upload", $this->upload_config + array(
            'max_width'  => 1024,
            'min_width'  => 1024,
            'max_height' => 1024,
            'min_height' => 1024,
            'file_name'  => "{$slug}-app_large_icon.png",
        ));

        if (!$this->upload->do_upload("update_app_icon_path")) {
            set_flash_message("error", $this->upload->display_errors());
            return $this->goBack("/admin/webiste/manage_iphone_app");
        }

        $src = $this->upload->upload_path.$this->upload->file_name;
        $dst = $this->upload->file_name;

        $url = $this->s3Upload($src, $dst);
        $this->app->app_large_icon_path = $url;
        $this->app->save();

        set_flash_message("success", "Updated Icon to '$url");
        $this->goBack("/admin/webiste/manage_iphone_app");
    }

    /**
     * Expects 'app_large_icon' set in the $_FILES super array
     */
    public function update_app_splash_path()
    {
        $slug = convert_to_slug($this->app->name);
        $this->upload_config['file_name']  = "{$slug}-app_splash_path.png";
        $this->load->library("upload", $this->upload_config + array(
            'max_width'  => 1242,
            'min_width'  => 1242,
            'max_height' => 2208,
            'min_height' => 2208,
            'file_name'  => "{$slug}-app_splash_path.png",
        ));

        if (!$this->upload->do_upload("update_app_splash_path")) {
            set_flash_message("error", $this->upload->display_errors());
            return $this->goBack("/admin/webiste/manage_iphone_app");
        }

        $src = $this->upload->upload_path.$this->upload->file_name;
        $dst = $this->upload->file_name;

        $url = $this->s3Upload($src, $dst);
        $this->app->app_splash_path = $url;
        $this->app->save();

        set_flash_message("success", "Updated Splash Image to '$url");
        $this->goBack("/admin/webiste/manage_iphone_app");
    }

    /**
     * Uploads an image into S3 bucket
     *
     * @param string $src the path to the image
     * @param string $dst the destination path inside the S3 bucket
     */
    protected function s3Upload($src, $dst)
    {
        $this->load->library("s3");
        $input = $this->s3->inputFile($src);
        $this->s3->putObject($input, $this->bucket, $dst, "public-read", array(), array("Content-Type" => "image/png"));
        return "http://{$this->bucket}.s3.amazonaws.com/{$dst}";
    }

}
