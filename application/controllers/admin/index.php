<?php
use App\Libraries\DroplockerObjects\Server_Meta;

class Index extends MY_Admin_Controller
{
    public $pageTitle = "Home";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Loading a blank page
     */
    public function index()
    {
        $this->renderAdmin('/admin/blank', array());
    }

    /**
     * Logs an employee out of the system and then redirects to the login page
     */
    public function logout()
    {
        $this->session->unset_userdata('buser_id');
        $this->session->unset_userdata('buser_fname');
        $this->session->unset_userdata('buser_lname');
        $this->session->unset_userdata('business_id');
        set_flash_message('success', 'You have been logged out');
        redirect('/admin/login');
    }

    public function dashboard_settings()
    {
        $content['modules'] = array('Orders', 'Customers', 'Reports', 'Website', 'Admin ');

        //list of all widgets
        $getWidgets = "Select * from widget order by description";
        $q = $this->db->query($getWidgets);
        $content['widgets'] = $q->result();

        $getUserSettings = "select * from dashboard where employee_id = ?";
        $q = $this->db->query($getUserSettings, array($this->buser_id));
        $userSettings = $q->result();
        foreach ($userSettings as $setting) {
            $content['userSettings'][$setting->module][$setting->location] = $setting->widget_id;
        }


        $this->template->write('page_title','Dashboard Settings', true);
        $this->data['content'] = $this->load->view('admin/index/dashboard_settings', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function dashboard_update()
    {
        for ($i = 1; $i <= 6; $i++) {
            $widget_id = "loc_".$i;

            $updateSetting = "replace into dashboard (module, widget_id, location, employee_id)
                                values(?, ?, ?, ?)";
            $q = $this->db->query($updateSetting, array($this->input->post("module"), $this->input->post($widget_id), $i, $this->buser_id));
        }
        set_flash_message('success', $this->input->post("module")." Settings Updated");
        redirect("/admin/index/dashboard_settings");
    }

}
