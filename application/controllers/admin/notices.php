<?php

use App\Libraries\DroplockerObjects\DeliveryNotice;

class Notices extends MY_Admin_Controller
{
    public $submenu = 'admin';

    /**
     * Renders the interface for managing print notices
     * @param bool $bizzie_superadmin If true, then the interface will show the delivery notices for the master business when the server is in bizzie mode
     */
    public function index($bizzie_superadmin = false)
    {
        $content = array();
        if ($bizzie_superadmin) {
            if (!in_bizzie_mode()) {
                throw new \LogicException("The master business can only be managed when the server is in Bizzie mode");
            }
            $content['master_business'] = get_master_business();
            $business_id = $content['master_business']->businessID;
            $this->setPageTitle("Manage Master Print Notices");
        } else {
            $business_id = $this->business_id;
            $this->setPageTitle('Manage Print Notices');
        }
        $content['printer_key'] = get_business_meta($this->business_id, 'printer_key');
        $content['deliveryNotices'] = DeliveryNotice::search_aprax(array('business_id' => $business_id));
        $content['business_id'] = $business_id;

        $this->renderAdmin('list_print_notice', $content);
    }

    /**
     * Creates a new Delivery notice
     * If a POST request, then this action expects the following POST parameters
     *  barcodeText
     *  noticeText
     */
    public function add()
    {
        $content = array();
        $deliveryNotice = new DeliveryNotice();

        if (!empty($_POST)) {
            $deliveryNotice->barcodeText = $_POST['barcodeText'];
            $deliveryNotice->noticeText = $_POST['noticeText'];
            $deliveryNotice->business_id = $this->business_id;
            $deliveryNotice->save();

            set_flash_message('success', "Created new delivery notice");
            redirect("/admin/notices/");
        }

        $this->renderAdmin('add_notice', $content);
    }

    /**
     * Updates a delivery notice
     *
     * if the application is in Bizzie mode and the deliver notice belongs to the master business
     * will update all delivery notices of all salve businesses
     *
     * This action can take the following POST parameters
     *  barcodeText
     *  noticeText
     */
    public function edit($deliveryNotice_id)
    {
        if (!is_numeric($deliveryNotice_id)) {
            set_flash_message('error', "'deliveryNotice_id' must be passed as segment 4 of the URL and must be numeric");
            $this->goBack("/admin/notices/");
        }

        $deliveryNotice = new DeliveryNotice($deliveryNotice_id);
        if (isset($_POST['submit'])) {
            //If the server is in Bizzie mode, then we update the deliveyr notices for all businesses
            if (in_bizzie_mode()) {
                $master_business = get_master_business();

                if ($deliveryNotice->business_id == $master_business->businessID) {
                    $master_deliveryNotice = $deliveryNotice;
                    if ($this->input->post("barcodeText")) {
                        $master_deliveryNotice->barcodeText = $this->input->post("barcodeText");
                    }
                    if ($this->input->post("noticeText")) {
                        $master_deliveryNotice->noticeText = $this->input->post("noticeText");
                    }

                    $master_deliveryNotice->save();
                    $this->load->model("business_model");
                    $slave_businesses = $this->business_model->get_all_slave_businesses();

                    foreach ($slave_businesses as $slave_business) {
                        $slave_deliveryNotices = DeliveryNotice::search_aprax(array("base_id" => $master_deliveryNotice->deliveryNoticeID, "business_id" => $slave_business->businessID));
                        if (empty($slave_deliveryNotices)) {
                            $slave_deliveryNotice = $master_deliveryNotice->copy();
                        } else {
                            $slave_deliveryNotice = $slave_deliveryNotices[0];
                            $slave_deliveryNotice->barcodeText = $master_deliveryNotice->barcodeText;
                            $slave_deliveryNotice->noticeText = $master_deliveryNotice->noticeText;
                        }
                        $slave_deliveryNotice->base_id = $master_deliveryNotice->deliveryNoticeID;
                        $slave_deliveryNotice->business_id = $slave_business->businessID;
                        $slave_deliveryNotice->save();
                    }
                    set_flash_message("success", "Updated master delivery notice and all related franchisee delivery notices");
                } else {
                    if ($this->input->post("barcodeText")) {
                        $deliveryNotice->barcodeText = $this->input->post("barcodeText");
                    }
                    if ($this->input->post("noticeText")) {
                        $deliveryNotice->noticeText = $this->input->post("noticeText");
                    }
                    $deliveryNotice->save();
                    set_flash_message("success", "UPdated delivery notice (id={$deliveryNotice->deliveryNoticeID})");
                }
            } else {
                $deliveryNotice->barcodeText = $_POST['barcodeText'];
                $deliveryNotice->noticeText = $_POST['noticeText'];
                $deliveryNotice->save();
                set_flash_message('success', 'Updated Notice');
            }


            redirect($this->uri->uri_string());
        }

        $content['notice'] = $deliveryNotice;
        $this->renderAdmin('edit_notice', $content);
    }

    /**
     * Deletes a a delivery notice
     *
     * if the server is in bizzie mode and the delivery notice belongs to the master business,
     * will delete all notices with the base ID that matches the primary key of the delivery notice.
     */
    public function delete($deliveryNotice_id = null)
    {
        $deliveryNotice = new DeliveryNotice($deliveryNotice_id);

        if (in_bizzie_mode()) {
            $master_business = get_master_business();
            if ($deliveryNotice->business_id == $master_business->businessID) {
                $master_deliveryNotice = $deliveryNotice;
                $this->load->model("deliverynotice_model");
                $this->deliverynotice_model->options = array("base_id" => $master_deliveryNotice->deliveryNoticeID);
                $this->deliverynotice_model->delete();
                $master_deliveryNotice->delete();
                set_flash_message("success", "Deleted master delivery notice and all related franchisee delivery notices");
            } else {
                $deliveryNotice->delete();
                set_flash_message('success', 'Deleted Franchisee Notice');
            }
        } else {
            if ($deliveryNotice->business_id == $this->business_id) {
                $deliveryNotice->delete();
                set_flash_message('success', 'Notice has been deleted');
            } else {
                set_flash_message("error", "This notice does not belong to your business");
            }
        }

        $this->goBack("/admin/notices/");
    }

}