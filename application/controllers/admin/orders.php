<?php

use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Customer_Item;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\ItemIssue;
use App\Libraries\DroplockerObjects\ItemUpcharge;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderItem;
use App\Libraries\DroplockerObjects\OrderStatus;
use App\Libraries\DroplockerObjects\Picture;
use App\Libraries\DroplockerObjects\Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Product_Supplier;
use App\Libraries\DroplockerObjects\ProductCategory;
use App\Libraries\DroplockerObjects\QuickOrder;
use App\Libraries\DroplockerObjects\Supplier;
use App\Libraries\DroplockerObjects\UpchargeGroup;

use App\Libraries\DroplockerObjects\Validation_Exception;
use App\Libraries\DroplockerObjects\NotFound_Exception;

use DropLocker\Service\HomeDelivery\Factory;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\App\Libraries\DroplockerObjects;

/**
 * location admin/orders.php
 */

class Orders extends MY_Admin_Controller
{
    public $buser_id;
    public $business;
    public $business_id;
    public $pageTitle = "Orders";

    /**
     * The folllwing function is a custom code igniter form validaiton rule for determining if the supplied barcode is a valid length for the business.
     */
    public function validate_barcode_length($barcode)
    {
        $this->load->model("barcodelength_model");
        $this->form_validation->set_message("validate_barcode_length", "The barcode is not a valid length.");

        return $this->barcodelength_model->is_valid_barcode_length($barcode, $this->business_id);
    }

    public function validate_barcode_not_exists($barcode)
    {
        $this->load->model("barcode_model");
        $this->form_validation->set_message("validate_barcode_not_exists", "The barcode already exists.");

        return $this->barcode_model->barcode_not_exists($barcode, $this->business_id);
    }

    /**
     * URL segment 4 must be the order_id if any arguments are passed in the url
     */
    public function __construct()
    {
        parent::__construct();

        //in many cases we need the order model for this controller
        $this->load->model('order_model');
    }

    /**
     * The following action is intended to receive a request from the jQuery Editable plugin and update the specified order property
     * Expects the following POST parameters
     *  orderID int The order entity's primary key
     *  property string A property that belongs to the order entity
     * Can take the following POST parameter
     *  value string The value to set the order entity's property. If not passed request, then the order entity's property is set to empty
     */
    public function update()
    {
        $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');
        if ($this->input->is_ajax_request()) {
            try {
                $this->load->library("form_validation");
                $this->form_validation->set_rules("orderID", "Order ID", "required|is_natural_no_zero|does_order_exist");
                $this->form_validation->set_rules("property", "Property", "required|alpha");
                if ($this->form_validation->run()) {
                    if ($this->input->post("value") === false) {
                        $value = "";
                    } else {
                        $value = $this->input->post("value");
                        $orderID = $this->input->post("orderID");
                        $property = $this->input->post("property");
                        $order = new Order($orderID);

                        //The following
                        if ($property == "dueDate") {
                            date_default_timezone_set('UTC');
                            $time = strtotime($value);
                            if ($time === false) {
                                echo "Invalid date";
                            } else {
                                $formatted_date = strftime($dateFormat, $time);
                                // due date is stored with no timezone conversion, so BUSINESS LOCAL TIMEZONE
                                $order->$property = gmdate('Y-m-d', $time);
                                $order->save();
                                echo $formatted_date;
                            }
                        } else {
                            echo $value;
                            $order->$property = $value;
                            $order->save();
                        }

                    }
                } else {
                    echo validation_errors();
                }
            } catch (Validation_Exception $validation_exception) {
                echo $validation_exception->getMessage();
                send_exception_report($validation_exception);
            } catch (NotFound_Exception $notFound_exception) {
                echo $notFound_exception->getMessage();
                send_exception_report($notFound_exception);
            } catch (Exception $exception) {
                echo "Sorry, an internal error occurred";
                send_exception_report($exception);
            }
        } else {
            echo "This action can only be accessed by an AJAX request and should only be called from jEditable";
        }
    }

    /**
     * there is no UI for this function. It creates a new orders and then shows the order details page
     *
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function create_shoe_shine()
    {
        if (!$this->input->get("customerID")) {
            throw new \LogicException("'customerID' must be passed as a GET parameter.");
        }
        if (!$this->input->get("lockerID")) {
            throw new \InvalidArgumentException("'lockerID' must be passed as a GET parameter.");
        }
        $customerID = $this->input->get("customerID");

        try {
            $customer = new Customer($customerID, false);
        } catch (NotFound_Exception $e) {
            set_flash_message("error", "Customer ID '$customerID' does not exist");
            redirect($this->agent->referrer());
        }
        $lockerID = $this->input->get("lockerID");
        try {
            $locker = new Locker($lockerID);
        } catch (NotFound_Exception $e) {
            set_flash_message("error", "Locker ID '$lockerID' does not exist");
            redirect($this->agent->referrer());
        }

        $bag = new Bag(); //1050863
        $bag->customer_id = $customer->customerID;
        $bag->business_id = $customer->business_id;
        $bag->save();

        $due_date = $this->input->get("due_date");
        $due_date = empty($due_date)?null:date("Y-m-d H:i:s", strtotime($due_date));

        $order_id = $this->order_model->new_order($locker->location_id, $locker->lockerID, $bag->bagNumber, get_employee_id($this->session->userdata('buser_id')), '', $this->business_id, array("customer_id" => $bag->customer_id, 'llnotes'=>'Special Order', 'due_date'=>$due_date));
        set_flash_message("success", "Create new bag '{$bag->bagNumber}' for " . getPersonName($customer));
        redirect("/admin/orders/bag_barcode_details/{$order_id}/{$bag->bagNumber}");
    }

    /**
     * UI for closing all of clothespins orders.
     * This is laundry locker specific
     */
    public function close_wholesale()
    {
        $this->content = array();
        if (isset($_POST['submit'])) {

            $customerId = 18789;
            $select1 = "select * from orders
            where customer_id = $customerId
            and dateCreated < DATE_ADD(NOW(), INTERVAL -8 HOUR)
            and orderStatusOption_id <> 10
            and business_id = {$this->business_id}";
            $query = $this->db->query($select1);
            $count = 0;
            foreach ($query->result() as $line) {
                $options['order_id'] = $line->orderID;
                $options['orderStatusOption_id'] = 10;
                $options['business_id'] = $line->business_id;
                $options['orderPaymentStatusOption_id'] = 1;
                $options['locker_id'] = 8033;
                $this->order_model->update_order($options);
                $message[] =  'Order '.$line->orderID.' completed.<br>';
                $count++;
            }

            $this->session->set_flashdata('closedOrders', serialize($message));
            set_flash_message('success', 'Closed '.$count.' Wholesale orders');
            redirect($this->uri->uri_string());
        }

        $this->setPageTitle('Close Wholesale Orders');
        $this->renderAdmin('close_wholesale', $this->content);
    }

    public function print_assembly()
    {
        $this->content = array();
        if (isset($_POST['barcode'])) {
            $this->load->model('automat_model');
            $this->load->model('assembly_model');

            if (!is_numeric($_POST['barcode'])) {
                set_flash_message("error", get_translation("error_barcode_not_in_proper_format", "admin/orders/print_assembly", array(), 'Barcode not in proper format, must be numeric'));
                redirect($this->uri->uri_string());
            }

            $barcode = trim($_POST['barcode']);
            //some yahoos are using barcodes starting with 0's so I had to remove this check.  It means you won't be able to scan temp tags anymore
            //$barcode = ltrim($barcode, '0');

            //if this is a bag
            if (strlen($barcode) == 7) {
                $isBag = 1;
                //find the order for this bag
                $query1="SELECT max(orderID) as currOrder
                    FROM bag
                    JOIN orders on orders.bag_id = bagID
                    where bagNumber = '$barcode'
                    and orderStatusOption_id != 10
                    and orders.business_id = ".$this->business_id;
                $query = $this->db->query($query1);
                $line = $query->result();
                $order_id = $line[0]->currOrder;

                if (empty($order_id)) {
                    set_flash_message("error", get_translation("error_bag_not_found", "admin/orders/print_assembly", array(), 'The bag was not found on any open orders'));
                    redirect($this->uri->uri_string());
                } else {
                    //mark the item as manually scanned
                    $insert1="insert into automat (order_id, garcode, gardesc, fileDate, outPos, arm)
                            values ($order_id, $barcode, 'Scanned at Receipt Printer', now(), 0, 'Bag')";
                    $insert = $this->db->query($insert1);
                }
            } else {
                $barcodeObj = Barcode::search(array('barcode'=>$barcode, 'business_id'=>$this->business_id));

                if (empty($barcodeObj->item_id)) {
                    set_flash_message("error", get_translation("error_barcode_did_not_return_an_item", "admin/orders/print_assembly", array(), 'The barcode entered did not return an item'));
                    redirect($this->uri->uri_string());
                }

                $item = new Item($barcodeObj->item_id);

                $activeOrderId  = $barcodeObj->getActiveOrder();

                if (empty($activeOrderId)) {
                    // check to see if there are any other
                    $recentOrderId = $barcodeObj->getMostRecentOrder();

                    if ($recentOrderId) {
                        $this->content['error'] = $result['error'];
                        $this->automat_model->insertRecieptPrinter($barcode, $recentOrderId, $this->business_id);
                        $this->content['error'] = get_translation("error_item_is_part_of_a_completed_order", "admin/orders/print_assembly", array("recentOrderId"=>$recentOrderId), "This item is part of a completed order [%recentOrderId%]");
                        $this->content['hide'] = true;
                        $order_id = $recentOrderId;
                    }
                } else {
                    $counts = $this->automat_model->getOrderItemCounts($activeOrderId);
                    //insert into auomat that an item was manually scanned
                    $result = $this->automat_model->insertRecieptPrinter($barcode, $activeOrderId, $this->business_id);

                    if ($result['status']=='error') {
                        $this->content['error'] = $result['message'];
                    } else {
                        $counts['automatCount'] ++;
                        $automatID = $result['insert_id'];
                    }

                    $order_id = $activeOrderId;

                }
            }
            $order = new Order($order_id);
            $this->content['slot_number'] = $order->slot_number;
            $bag = $order->find('bag_id');
            $customer = $order->find('customer_id');
            $locker = $order->find('locker_id');
            $location = $locker->find('location_id');

            $this->assembly_model->order_id = $order_id;
            $assembly = $this->assembly_model->get();
            if ($assembly) {
                $position = $assembly[0]->position;
            } else {
                //get the last number
                if (empty($location->route_id)) {
                    set_flash_message("error", get_translation("error_order_locations_has_no_defined_route", "admin/orders/print_assembly", array(), "The order location has no defined route"));
                    redirect($this->agent->referrer());
                }
                $position = $this->assembly_model->getMaxPosition($location->route_id, $this->business_id);
                $position = $position + 1;
                $this->assembly_model->clear();
                $this->assembly_model->insert($this->business_id, $location->route_id, $order_id, $position, date('Y-m-d'));
            }
            //update auomat table with the position of this item
            $result = $this->automat_model->updateAutomatOutPos($automatID, $position);

            $this->content['complete'] = ($counts['automatCount'] == $counts['totalItems'])?"true":"";
            if ($isBag == 1) {
                $this->content['automatCount'] = '**BAG**';
                $this->content['totalItems'] = '**BAG**';
            } else {
                $this->content['automatCount'] = $counts['automatCount'];
                $this->content['totalItems'] = $counts['totalItems'];
            }
            
            $this->load->model('cash_register_model');
            $this->content['pos_enabled_business'] = count($this->cash_register_model->getForBusiness($this->business_id));
               
            $this->content['pos'] = $position;
            $this->content['route'] = $location->route_id;
            $isBag != 1 && $this->content['image'] = "https://s3.amazonaws.com/LaundryLocker/".$item->getPicture();
            $this->content['barcode'] = $barcode;
            $this->content['customer'] = getPersonName($customer);
            $this->content['customer_id'] = $customer->customerID;
            $this->content['order_id'] = $order_id;
            $this->content['bagNumber'] = $bag->bagNumber;
            $this->content['bagNotes'] = $bag->properties['notes'];
        }

        //get highlighted routes
        $this->content['selectedRoutes'] = get_business_meta($this->business_id, 'highlight_routes');

        //get all routes
        $getRoutes = "select distinct route_id from location where business_id = $this->business_id";
        $query = $this->db->query($getRoutes);
        $this->content['allRoutes'] = $query->result();

        $this->setPageTitle('Manual Assembly');
        $this->renderAdmin('print_assembly', $this->content);
    }
    
    public function update_slot_number()
    {
        try {
            if (!is_numeric($this->input->post('order_id'))) {
                throw new Exception(get_translation("error_no_order_id", "admin/orders/update_slot_number", array(), 'A valid order id must be provided'));
            }

            $order = new Order($this->input->post('order_id'));
            $order->slot_number = $this->input->post('slot_number');
            $order->save();
            
            $orderStatusOption_id = 9; // change status to Ready for customer pick up
            $options['not_complete'] = true; // but not roll through to completed
            
            $this->load->model('order_model');
            $this->order_model->update_order_status($this->input->post('order_id'), $orderStatusOption_id, $this->employee_id, $this->business_id, $options);
            
            set_flash_message("success", get_translation("data_updated", "admin/orders/update_slot_number", array('slot_number' => $this->input->post('slot_number'), 'order_id' => $this->input->post('order_id')), 'Set slot number %slot_number% for order %order_id%'));
        } catch (Exception $exc) {
            set_flash_message("error", $exc->getMessage());
        }
        
        redirect('/admin/orders/print_assembly');
    }

    /**
     * Unapplies all discounts/charges from an order, removes all items from an order, and changes the order pickup status to 'pickedup'.
     * Expects the order ID as segment 4.
     */
    public function gut_order($order_id = null)
    {
        $this->load->model('order_model');
        $orders = $this->order_model->get(array("orderID" => $order_id));
        $order = $orders[0];
        $this->load->model('orderStatus_model');
        $this->load->library("discounts");
        try {
            if (!is_numeric($order_id)) {
                throw new Exception("The order ID must be passed as segment 4 and must be numeric.");
            }
            $this->discounts->unapply_discounts($order);
            $this->order_model->remove_all_items($order_id);
            $this->orderStatus_model->update_orderStatus(array(
                'order_id' => $order_id,
                "orderStatusOption_id" => PICKED_UP_STATUS,
                "locker_id" => $order->locker_id,
                'employee_id' => $this->employee_id,
            ));

            $this->order_model->update_order_taxes($order_id);

            set_flash_message("success", "Successfully gutted order");
        } catch (Exception $e) {
            set_flash_message("error", "Could not gut order: {$e->getMessage()})");
            send_exception_report($e);
        }

        //the order also needs to be emptied from the wf log
        $queryDeleteLog = "DELETE FROM wfLog where order_id = ?";
        $query = $this->db->query($queryDeleteLog, array($order_id));

        //This statement strips any get parameters from the url and redirects to that URL..
        redirect(strtok($_SERVER['HTTP_REFERER'], "?"));
    }

    /**
     * Expects the following passed as URL segments
     * Segment 4:
     * Can take the following as optional URL segments
     * Segment 5: (optional) The order ID for which to render the items as a 'closet'
     *
     */
    public function view_order_as_closet($customer_id = null, $order_id = null)
    {
        $this->load->model('item_model');
        $this->content['items'] = $items = $this->item_model->get_closet($customer_id, $order_id);
        $this->content['order'] = $order_id;
        $this->content['customer_id'] = $customer_id;
        $this->template->add_css("/css/account/profile_closet.css");

        if ($order_id) {
            $this->setPageTitle('View Order as Closet');
        } else {
            $this->setPageTitle('View Closet');
        }
        $this->renderAdmin('closet', $this->content);
    }

    /**
     * main entry for orders
     */
    public function index()
    {
        //find out which widgets to show
        $getWidgets = "select * from dashboard
                        left join widget on widgetID = widget_id
                        where employee_id = $this->buser_id
                        and module = 'Orders'
                        order by location";
        $result = $this->db->query($getWidgets);
        $content['widgets'] = $result->result();

        $this->renderAdmin('/admin/index', $content);
    }

    /**
     * UI for showing all open orders for a business
     */
    public function view()
    {
        //get the locations for the business
        $this->load->model("location_model");
        $this->load->model("claim_model");
        $content['locations'] = array();

        $locations = $this->location_model->get_aprax(array(
            'business_id' => $this->business_id,
            'status' => 'active',
            'serviceType !=' => 'not in service'
        ));
        foreach ($locations as $l) {
            $content['locations'][$l->locationID] = sprintf('%s (%s) (%s-%s) [%s](%s)',
                substr($l->address,0,35),
                substr($l->companyName,0,35),
                $l->locationID,
                substr($l->accessCode,0,10),
                substr($l->serviceType, 0, 6),
                $l->route_id
            );
        }
        natsort($content['locations']);

        $this->load->model("order_model");
        $allOrders = $this->order_model->viewOrders($this->business_id);
        $content['orders'] = $this->_formatOrdersReport($allOrders);

        $this->setPageTitle('View All Open Orders');
        $this->renderAdmin('view_all', $content);
    }

    public function due_date()
    {
        $this->load->model("order_model");

        $due_date = !empty($_GET['due_date']) ? $_GET['due_date'] : date("Y-m-d");
        $past_orders = !empty($_GET['past_orders']) ? intval($_GET['past_orders']) : 0;

        $allOrders = $this->order_model->view_by_due_date($this->business_id, $due_date, $past_orders);
        $orders = $this->_formatOrdersReport($allOrders);

        $content = compact('due_date', 'past_orders', 'orders');

        $this->setPageTitle('Orders by Due Date');
        $this->renderAdmin('due_date', $content);
    }

    protected function _formatOrdersReport($allOrders)
    {
        $claim_customer_names = array();
        $max_length = 0;
        $orders = array();
        $order_count = array();

        $this->load->model('orderstatus_model');

        if (empty($allOrders)) {
            return $orders;
        }

        $customer_ids = array();
        foreach ($allOrders as $order) {
            if (!in_array($order->customer_id, $customer_ids)) {
                $customer_ids[] = $order->customer_id;
            }
        }

        // Get the order count for each customer
        $this->db->where_in('customer_id', $customer_ids);
        $this->db->where('bag_id > ', 0);
        $this->db->group_by('customer_id');
        $this->db->select('COUNT(*) AS total, customer_id');
        $counts = $this->db->get('orders')->result();

        foreach ($counts as $row) {
            $order_count[$row->customer_id] = $row->total;
        }

        $this->load->model('claim_model');

        foreach ($allOrders as $order) {
            $o = (array) $order;

            if (empty($order->originalLockerName)) {
                $o['original_locker'] = "Unknown Locker";
            } else {
                $o['original_locker'] = $order->originalLockerName;
            }

            if (is_numeric($o['original_locker'])) {
                $max_length = max($max_length, strlen($o['original_locker']));
            }

            if (!isset($order_count[$order->customer_id])) {
                $order_count[$order->customer_id] = 0;
            }

            // calculated in one query before
            $o['order_count'] = $order_count[$order->customer_id];

            if (is_numeric($order->claimID)) {
                if (!isset($temp[$order->claimID])) {
                    $claim_customer = $this->claim_model->get_claim_customer($order->claimID);
                    $claim_customer_names[$order->claimID] = getPersonName($claim_customer[0]);
                }

                $o['claimID'] = $claim_customer_names[$order->claimID];
            }

            $o['order_status'] = $this->_getOrdStat(substr($o['statusName'],0,50),$o['paymentStatusName'],$o['orderPaymentStatusOption_id'],$o['autoPay']);

            if ($o['orderID']) {
                $o['receipt'] = '<a href="/admin/printer/print_order_receipt/'.$o['orderID'].'" target="_blank">Print</a>&nbsp;&nbsp;&nbsp;<a target="_blank" href="/admin/orders/details/'.$o['orderID'].'">D</a>';
            } else {
                $o['receipt'] = '<a target="_blank" href="/admin/printer/receipt/'.$o['lockerID'].'">Print</a>';
            }

            $orders[] = $o;
        }

        //The following conditional will add padded 0s to any string which contains a numeric value.
        if ($max_length > 0) {
            foreach ($orders as $index => $order) {
                if (intval($order['original_locker'])) {
                    $orders[$index]['original_locker'] = str_pad($order['original_locker'], $max_length, 0, STR_PAD_LEFT);
                }
            }
        }

        return $orders;
    }

    /**
     * Renders the interface for managing the details of an order, such as the order properties and order items associated iwth the order.
     *
     * @param int $order_id
     */
    public function details($order_id = null)
    {

        $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');
        if (!is_numeric($order_id)) {
            set_flash_message("error", "'order_id' must be numeric and passed as segment 4 in the URL. ");

            return $this->goBack("/admin/orders");
        }

        $this->load->model("order_model");
        if ($this->order_model->does_exist($order_id) === false) {
            set_flash_message("error", "Order '{$order_id}' does not exist");

            return $this->goBack("/admin/orders");
        }

        $order = $this->order_model->get_by_primary_key($order_id);
        $content['order'] = $order;

        if ($order->business_id != $this->business_id) {
            if ($this->employee->developer_mode) {
                $this->load->model("business_model");
                $otherBusiness = $this->business_model->get_by_primary_key($order->business_id);
                set_flash_message("error", "Order '{$order->orderID}' belongs to  '$otherBusiness->companyName' ($otherBusiness->businessID)'");
            }
            else {
                set_flash_message("error", "This order does not belong to your business.");
            }


            return $this->goBack("/admin/orders");
        }

        //get statusoptions for dropdown
        $this->load->model('orderstatus_model');
        $this->load->model('orderstatusoption_model');
        $content['status'] = $this->orderstatusoption_model->get_aprax([
             'deletedAt' => null
        ]);

        foreach ( $content['status'] as $key => $theStatus) {
            $content['status'][$key]->name = get_translation($theStatus->name,"OrderStatusOptions",array(), $theStatus->name, $order->customer_id);
        }

        try {
            $content['customer'] = $customer = new Customer($order->customer_id, false);

            $content['bags']["0"] = "N/A";
            $this->load->model("bag_model");
            $bags = $this->bag_model->get_aprax(array("customer_id" => $customer->customerID));
            foreach ($bags as $bag) {
                $content['bags'][$bag->bagID] = $bag->bagNumber;
            }
        } catch (NotFound_Exception $e) {
            $content['customer'] = "";
        }

        $this->load->model("locker_model");

        $content['locker'] = $this->locker_model->get_by_primary_key($content['order']->locker_id);

        $this->load->model("location_model");
        if (isset($content['locker']->location_id)) {
            if (is_numeric($content['locker']->location_id)) {
                $content['location'] = $this->location_model->get_by_primary_key($content['locker']->location_id);
            } else {
                $content['location'] = null;
            }
        } else {
            $content['location'] = null;
        }

        //get order items/products

        $this->load->model("orderitem_model");

        $orderItems = $this->db->query("SELECT orderItem.product_process_id, orderItem.notes, orderItemID,
                unitPrice, product.name AS displayName, orderItem.item_id, orderItem.supplier_id,
                orderItem.qty, product.unitOfMeasurement, productCategory.slug, (
                  SELECT barcode
                  FROM barcode
                  WHERE item_id = orderItem.item_id
                  AND business_id = ?
                  ORDER BY barcodeID DESC
                  LIMIT 1
                ) AS barcode
            FROM orderItem
            LEFT JOIN product_process ON product_processID = product_process_id
            LEFT JOIN product ON (productID = product_id)
            LEFT JOIN productCategory ON (productCategoryID = productCategory_id)

            WHERE order_id = ?", array($this->business->businessID, $content['order']->orderID))->result();

        foreach ($orderItems as $key => $orderItem) {
            $orderItems[$key]->qty = OrderItem_Model::getRoundedQty(
                $this->business->businessID, $orderItem->qty, $orderItem->unitOfMeasurement
            );
        }

        $content['items'] = $orderItems;
        $get_qty_amount_result = $this->db->query("SELECT SUM(qty) AS qty FROM orderItem WHERE order_id=?", array($content['order']->orderID))->result();
        $content['qty'] = $get_qty_amount_result[0]->qty;
        $content['item_count'] = count($orderItems);
        //get products for adding a new item
        // also used in item table
        $this->load->model('product_model');
        $content['products'] = $products = $this->product_model->getBusinessProducts($this->business_id);
        foreach ($products  as $p) {
            $pa[$p->product_processID] = $p;
        }

        // pa = productarray
        $content['pa'] = $pa;

        $groups = array();
        $upchargeGroups = UpchargeGroup::search_aprax(array('business_id'=>$this->business_id));
        foreach ($upchargeGroups as $upchargeGroup) {
            $upchargeItems = $upchargeGroup->getGroupUpcharges();
            if(!empty($upchargeItems)) {
                $groups[$upchargeGroup->upchargeGroupID] = $upchargeItems;
            }
        }
        $content['upchargeGroups'] = $groups;
        //var_dump($content['upchargeGroups']); die();

        // get wash locations for adding a new item
        // also used in the item table
        $this->load->model('supplier_model');
        $content['suppliers'] = array("" => "") + $this->supplier_model->simple_array($this->business_id);

        //get order status history
        $this->load->model('orderstatus_model');

        $content['orderStatus_history'] = $this->order_model->get_history($order->orderID);

        // get the totals for the order
        $total = $this->orderitem_model->get_full_total($order_id, $this->business_id);
        $content['order_total'] = isset($total['total'])?$total['total']:0;

        $content['order_taxes'] = $this->order_model->get_order_taxes($order_id);

        $this->load->model('orderCharge_model');
        $this->orderCharge_model->order_id = $order_id;
        $content['charges'] = $charges = $this->orderCharge_model->get();
        $ctotal = 0;
        foreach ($charges as $c) {
            $ctotal = $ctotal+$c->chargeAmount;
        }
        $content['ctotal'] = $ctotal;

        $content['gross_total'] = $this->order_model->get_gross_total($order_id);
        $content['net_total'] = $this->order_model->get_net_total($order_id);
        $content['subtotal'] = $this->order_model->get_subtotal($order_id);
        $content['breakout_vat'] = $this->order_model->is_breakout_vat($order_id);

        $content['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
        $content['orderStatusOption_id'] = $order->orderStatusOption_id;
        $content['order_id'] = $order_id;

        // check if customer defaultWF is active
        $this->load->model('product_process_model');
        if ($this->product_process_model->is_active($customer->defaultWF)) {
            $content['default_wash_fold_product_process_id'] = $customer->defaultWF;
        } else {
            $content['default_wash_fold_product_process_id'] = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);
        }

        //get wf Log details for this order
        $getWfLog = "select * from wfLog where order_id = $order_id";
        $query = $this->db->query($getWfLog);
        $content['wfLog'] = $query->result();
        $content['notify_status_emails'] = $this->getNotifyOrderStatuses();

        //The following statements generate the select options for the duedate. This content variable is ment to be supplied to a jeditable instantiation for the select.
        $content['dueDate_options'] = array();
        if (empty($order->dueDate)) {
            $content['dueDate_options'][''] = "---Not Set---";

        } else {
            $content['dueDate_options'][$order->dueDate] = strftime($dateFormat, strtotime($order->dueDate));
        }

        // We check if this is the blank_customer for the current business
        $content['is_blank_customer'] = $this->business->blank_customer_id == $order->customer_id;
        $content['related_claims'] = array(); // claims from the same location
        if ($content['is_blank_customer']) {
            $locker = $this->locker_model->get_by_primary_key($order->locker_id);
            $content['related_claims'] = $this->db->query('SELECT claimID, customerID, firstName, lastName
                FROM claim
                JOIN locker ON locker_id = lockerID
                JOIN customer ON customer_id = customerID
                WHERE claim.active = 1 AND locker.location_id = ?',
                    $locker->location_id)->result();
        }

        if (!empty($customer)) {
            $this->load->model('customerdiscount_model');
            $content['activeCoupons'] = $this->customerdiscount_model->get_aprax(array(
                'customer_id' => $customer->customerID,
                'active' => 1,
            ));
        }

        $content['print_day_tags']        = get_business_meta($this->business_id, 'print_day_tags');
        $content['pay_with_cash']         = get_business_meta($this->business_id, 'pay_with_cash');
        $content['show_manual_discounts'] = get_business_meta($this->business_id, 'show_manual_discounts', in_bizzie_mode());

        // logic to decide if there is a note
        $content['show_notes_warning'] = false;
        if (!empty($_GET['new_order'])) {

            // if customer has internalNotes, show the warning
            if (!empty($content['customer']) && $content['customer']->internalNotes) {
                $content['show_notes_warning'] = true;
            } else {
                // search for notes to show
                $lines = explode("\n", $order->notes);
                foreach ($lines as $line) {
                    $line = trim($line);
                    $fields = explode(":", $line, 2);

                    // ignore empty lines
                    if (empty($line)) {
                        continue;
                    }

                    // this is not a field, so show notes and stop searching
                    if (count($fields) < 2) {
                        $content['show_notes_warning'] = true;
                        break;
                    }

                    // ignore empty notes, move to next line
                    if (empty($fields[1])) {
                        continue;
                    }

                    //if "ORDER TYPE: SMS", ignore this line
                    if (trim($fields[1]) == "SMS") {
                        continue;
                    }

                    // we have a note, stop here
                    $content['show_notes_warning'] = true;
                    break;
                }
            }

            if (!empty($content["customer"])) {
                $content["ada_deliver_to_lower_locker"] = get_customer_meta($content["customer"]->customerID, "ada_deliver_to_lower_locker", 0);
                if (!empty($content["ada_deliver_to_lower_locker"])) {
                    $content['show_notes_warning'] = true;
                }
            }
        }

        $content['custom_manual_discounts'] = json_decode(get_business_meta($this->business_id, 'custom_manual_discounts', '[]'));

        $this->template->add_js("/js/reassign_order.js");
        $this->template->add_js("/js/orderItems.js");

        $this->setPageTitle('Order '.$order_id);
        $this->renderAdmin('details', $content);
    }

    public function pay_with_cash($order_id)
    {
        if (empty($_POST)) {
            $this->goBack("/admin/orders/detail/$order_id");
        }

        $total = $this->order_model->get_net_total($order_id);
        $amount = $_POST['amount'];
        $employee_id = get_employee_id($this->session->userdata('buser_id'));

        $this->load->model('order_model');
        $result = $this->order_model->manual_capture($order_id, $amount, 'cash', $employee_id);
        set_flash_message($result['status'], $result['message']);

        $this->goBack("/admin/orders/details/$order_id");
    }

    public function pay_with_check($order_id)
    {
        if (empty($_POST['check'])) {
            set_flash_message('error', "Check number must not be empty");
            $this->goBack("/admin/orders/detail/$order_id");
        }

        $amount = $_POST['amount'];
        $check = $_POST['check'];
        $employee_id = get_employee_id($this->session->userdata('buser_id'));

        $this->load->model('order_model');
        $result = $this->order_model->manual_capture($order_id, $amount, 'check', $employee_id, $check);
        set_flash_message($result['status'], $result['message']);

        $this->goBack("/admin/orders/details/$order_id");
    }

    //returns the order statuses for email notifications
    private function getNotifyOrderStatuses()
    {
       $email_order_statuses = $this->db->query( "select orderStatusOption_id, emailAction_id, emailTemplate.smsText, emailText
                                                    from emailTemplate
                                                    join emailActions on emailAction_id = emailActionID
                                                    where business_id = {$this->business_id}
                                                    and orderStatusOption_id != 0 and (emailTemplate.smsText !='' or emailText !='') " );

       $email_statuses = $email_order_statuses->result();

       if (!empty($email_statuses)) {
           $status_array = array();

           foreach ($email_statuses as $email_status) {
               $status_array[] =  $email_status->orderStatusOption_id;
           }

           return $status_array;
       } else {
           return array();
       }

    }

    public function entry_error()
    {
        $order = new Order($_POST['orderID']);
        $this->logger->set('orderStatus', array('order_id' => $order->orderID, 'locker_id' => $order->locker_id, 'employee_id' => get_employee_id($this->buser_id), 'orderStatusOption_id' => $order->orderStatusOption_id, 'note' => "Order Entry Error [".$_POST['barcode']."]", 'visible' => 1));
    }

    public function receipt($order_id = null)
    {
        if ($this->input->post("partial_order")) {
            $content['partial'] = array();
            $content['partial']['order_id'] = $this->input->post("order_id");
            $content['partial']['number_short'] = $this->input->post("number_short");
            $content['partial']['description'] = $this->input->post("description");
            $content['partial']['reason'] = $this->input->post("reason");

        }

        if (!empty($_POST['changeToCaptured'])) {
            $this->mark_as_captured($order_id);
        }

        // get the suppliers
        $this->load->model('supplier_model');
        $suppliers = $this->supplier_model->simple_array($this->business_id);


        $content['order'] = $order = new Order($order_id);
        $content['customer'] = $customer = $order->find('customer_id');
        $content['bag'] = $bag = $order->find('bag_id');
        $content['locker'] = $locker = $order->find('locker_id');
        $content['location'] = $location = $locker->find('location_id');

        //get order items/products
        $items = $order->getData('orderItem');
        $qty = 0;
        for ($i=0; $i < count($items); $i++) {
            $productName = Product::getName($items[$i]->product_process_id);
            $items[$i]->displayName = $productName['name'];
            $items[$i]->supplier = $suppliers[$items[$i]->supplier_id];

            if ($items[$i]->item_id) {
                $items[$i]->barcode =  Barcode::search(array('item_id'=>$items[$i]->item_id))->barcode;
            }
            $qty = $qty + $items[$i]->qty;
        }
        $content['items'] = $items;
        $content['qty'] = $qty;

        //get any discounts
        $sqlGetDiscounts = "select * from orderCharge where order_id = $order_id;";
        $query = $this->db->query($sqlGetDiscounts);
        $content['discounts'] = $query->result();

        //get order status history
        $this->load->model('orderstatus_model');
        $content['status'] = $this->orderstatus_model->getHistory($order->orderID);

        $content['order_id'] = $order_id;

        $content['order_taxes'] = $this->order_model->get_order_taxes($order_id);
        $content['gross_total'] = $this->order_model->get_gross_total($order_id);
        $content['net_total'] = $this->order_model->get_net_total($order_id);
        $content['subtotal'] = $this->order_model->get_subtotal($order_id);
        $content['breakout_vat'] = $this->order_model->is_breakout_vat($order_id);

        $this->load->model("image_model");
        $content['detailedReceipt_logo_path'] = $this->image_model->get_placement_path("detailedReceipt");

        $this->load->view('print/orders_receipt', $content);

    }


    /**
     *
     *  Creates an order and then redirects to the new order detail page
     *  There is no view for this
     *
     *  Required: bag, location, customer
     *  Blank customer as customer
     *  Return to office location
     *  Bag: 0
     *
     *
     */
    public function redo_order()
    {
        $redo_id = get_business_meta($this->business_id, 'default_redo_id');
        if (empty($redo_id)) {
            set_flash_message('error', "Business must have a default redo product before making redo orders");
            redirect("/admin/admin/defaults");
        }

        $newOrderID = null;
        $locker_id = $this->business->returnToOfficeLockerId;

        if (empty($locker_id)) {
            set_flash_message('error', 'You must set the business return to office location');
            redirect('/admin/admin/defaults');
        }

        $locker = new Locker($locker_id);
        $location_id = $locker->location_id;
        $bagNumber = 0;
        $employee_id = get_employee_id($this->session->userdata('buser_id'));
        $notes = REDO_ORDER;
        $business_id = $this->business_id;
        $options['customer_id'] = $this->business->blank_customer_id;
        $options['llnotes'] = REDO_ORDER;
        $newOrderID = $this->order_model->new_order($location_id, $locker_id, $bagNumber, $employee_id, $notes, $business_id, $options);

        if ($newOrderID == null) {
            set_flash_message('error', "Redo Order was not created");
            redirect("/admin/orders");
        }

        set_flash_message('success', "Redo Order was created");
        redirect("/admin/orders/details/".$newOrderID);

    }

    /**
     * getorderStat formats the status messages
     *
     * @param string $orderStatus
     * @param string $orderPaymentStatus
     * @param int $orderPaymentStatusOption_id
     * @param int $autopay
     * @return string orderstatus
     *
     * $orderstatus needs to be truncated @ 6 chars
     */
    public function _getOrdStat($orderStatus,$orderPaymentStatus,$orderPaymentStatusOption_id,$autoPay)
    {
        $noErr = 0;
        //determine what time it is
        $currtime = date("H");
        $translatedOrderStatus = get_translation($orderStatus,"OrderStatusOptions",array(),  $orderStatus);
        //echo $currtime;
        //if it's the morning (before orders have been captured)
        if ($currtime <= 9) {
            if ($orderStatus == 'Invent' and $orderPaymentStatus == 'Authorized') {
                $noErr = 1;
            }
            if ($orderStatus == 'Invent' and $autoPay && $orderPaymentStatusOption_id == 1) {
                $noErr = 1;
            } // Auto-Pay
            if ($orderStatus == 'Partia' and $orderPaymentStatus == 'Captured') {
                $noErr = 1;
            }
            if ($noErr == 1) {
                return $translatedOrderStatus;
            } else {
                return '<b><font color="#FF0000">' . $translatedOrderStatus. '</font></b>';
            }
        }
        //if it's the morning (after orders have been captured)
        if ($currtime >= 10 and $currtime <= 14) {
            if ($orderStatus == 'Invent' and $orderPaymentStatus == 'Captured') {
                $noErr = 1;
            }
            //if ($params['record']['orderStatus'] == 'Invent' and$params['record']['autoPay'] && $params['record']['orderPaymentStatusOption_id'] == 1) {$noErr = 1;} // Auto-Pay
            if ($orderStatus == 'Picked' and $autoPay && $orderPaymentStatus_id == 1) {
                $noErr = 1;
            } // Auto-Pay
            if ($orderStatus == 'Picked' and $orderPaymentStatus == 'Unpaid') {
                $noErr = 1;
            }
            if ($orderStatus == 'Partia' and $orderPaymentStatus == 'Captured') {
                $noErr = 1;
            }
            if ($orderStatus == 'Out fo' and $orderPaymentStatus == 'Captured') {
                $noErr = 1;
            }
            if ($noErr == 1) {
                return $translatedOrderStatus;
            } else {
                return '<b><font color="#FF0000">' . $translatedOrderStatus . '</font></b>';
                return 'bgcolor=red';
            }
        }
        //if it's the afternoon (after all drivers are back)
        if ($currtime >= 15) {
            if ($orderStatus == 'Invent' and $autoPay && $orderPaymentStatus_id == 1) {
                $noErr = 1;
            } // Auto-Pay
            if ($orderStatus == 'Invent' and $orderPaymentStatus == 'Authorized') {
                $noErr = 1;
            }
            if ($orderStatus == 'Partia' and $orderPaymentStatus == 'Captured') {
                $noErr = 1;
            }
            if ($orderStatus == 'Invent' and $orderPaymentStatus == 'Unpaid') {
                $noErr = 1;
            }
            if ($noErr == 1) {
                return $translatedOrderStatus;
            } else {
                return '<b><font color="#FF0000">' . $translatedOrderStatus . '</font></b>';
            }
        }
    }

    /**
     * shows all the lockers in a location with the orders and claims
     * Expects the location ID as segment 4 of the URL.
     */
    public function view_location_orders($location_id = null)
    {
        if (!is_numeric($location_id)) {
            set_flash_message('error', "Missing location ID");
            redirect('/admin/orders/view');
        }

        // get the locations for the business
        // used in dropdown
        $this->load->model("location_model");
        $content['locations'] = $this->location_model->get(array('business_id'=>$this->business_id, 'sort'=>'address'));

        // get the specific location details and lockers
        $location = Location::search(array(
            'business_id'=>$this->business_id,
            'locationID'=>$location_id
        ));

        if (!$location) {
            set_flash_message('error', "Location not found.");
            redirect("/admin/orders/view");
        }

        $content["location"] = $location;

        //get the status array
        $this->load->model('orderstatus_model');
        $content['statusOptions'] = $this->orderstatus_model->simple_array();

        //get the payment status array
        $this->load->model('orderpaymentstatus_model');
        $content['paymentStatusOptions'] = $this->orderpaymentstatus_model->simple_array();

        //get locker status
        $this->load->model('lockerstatus_model');
        $content['lockerStatus'] = $this->lockerstatus_model->simple_array();

        //get lockers for this location
        $get_lockers_query_sql = "SELECT
        (select count(*) FROM orders WHERE locker_id = lockerID) as orders,
        (select max(orders.dateCreated) FROM orders WHERE locker_id = lockerID) as lastOrder,
        c2.customerID as order_customer_id,
        c2.firstName as order_firstName,
        c2.lastName as order_lastName,
        lockerName,
        lockerID,
        lockerStatus_id,
        bagNumber,
        orderStatusOption_id,
        orderPaymentStatusOption_id,
        orderID
        FROM (`locker`)
        JOIN `lockerStyle` ON `lockerStyle`.`lockerStyleID`=`locker`.`lockerStyle_id`
        JOIN `lockerLockType` ON `lockerLockType`.`lockerLockTypeID`=`locker`.`lockerLockType_id`
        LEFT JOIN orders ON `lockerID` = `orders`.`locker_id` AND `orderStatusOption_id` NOT IN (9, 10)
        LEFT JOIN customer c2 ON `orders`.`customer_id` = c2.customerID
        LEFT JOIN bag ON `orders`.`bag_id` = bagID
        LEFT JOIN orderStatusOption ON `orders`.`orderStatusOption_id` = orderStatusOptionID
        LEFT JOIN orderPaymentStatusOption ON `orders`.`orderPaymentStatusOption_id` = orderPaymentStatusOptionID
        WHERE `locker`.`location_id` = {$location_id}
        AND `lockerStatus_id` = 1
        ORDER BY CAST(lockerName AS SIGNED INTEGER)
        ";
        $get_lockers_query = $this->db->query($get_lockers_query_sql);


        $lockers = $get_lockers_query->result();

        foreach ($lockers as $locker) {
            if ($locker->lockerName =='InProcess') {
                $content['process'][]=$locker;
            } else {
                $content['lockers'][] = $locker;
            }
        }

        $lockers = $content['lockers'];

        // Pad integers with 0s to correct JS sorting, fixme: very dirty attempt at padding the integers
        $max_length = 1;
        $calc_max_length = function ($locker) use (&$max_length) {
            if (!preg_match('#[a-z]+#i', $locker->lockerName))
                $max_length = max($max_length, strlen($locker->lockerName));
        };

        array_walk($lockers, $calc_max_length);

        foreach ($lockers as $k => $locker) {
            if (!preg_match('#[a-z]+#i', $locker->lockerName)) {
                $locker->lockerName = str_pad($locker->lockerName, $max_length, '0', STR_PAD_LEFT);
                $lockers[$k] = $locker;
            }
        }

        $this->load->model('claim_model');
        $this->load->model('customer_model');
        if ($lockers) {
            foreach ($lockers as $index => $locker) {
                $content['lockers'][$index]->claims = $this->claim_model->get_claims_by_locker($locker->lockerID);
                foreach ($content['lockers'][$index]->claims as $claim_index => $claim) {
                    $customers = $this->customer_model->get_customer($claim->customer_id);
                    $customer = $customers[0];
                    $content['lockers'][$index]->claims[$claim_index]->customer_firstName = $customer->firstName;
                    $content['lockers'][$index]->claims[$claim_index]->customer_lastName = $customer->lastName;
                }
            }
        }

        //get any linked customers
        $getLinked = "select * from customer where linkedLocation_id = {$location_id} order by firstName, lastName";
        $get_linked_query = $this->db->query($getLinked);
        $content['linked'] = $get_linked_query->result();

        $content['claims'] = $this->claim_model->search("", "", $content['location']->address, $this->business_id);

        $this->setPageTitle('Location Details for '.$location->address);
        $this->renderAdmin('view_location_orders', $content);
    }



    /**
     * @deprecated THis * does not follow established coding standards and is buggy.
     * Note, most of the following is undocumented and poorly designed *. The following is what is note so far about this * code.
     * routing set up to handle args
     * The following * action requires the following segment parameters
     * Excpects segment 4 to be the locker name.
     * Expects segment 5 to be an unknown parameter called 'display type'.
     * Expects segment 6 to be the bag number
     *
     * Note, in some cases segment 7 of the url is used for the claim ID.
     *
     * Can take the following POST Parameters
     *  claimID int If not passed, then this function attempts to find an open claim for the specified locker
     *  locationID int Thgis parameter does not appear to ever be used
     * Note, some case, POST parameters are expected as well
     *  bagNumber int
     *  lockerID int
     */
    public function quick_order_locker($locker_id = null, $display = null, $bagNumber = '', $claim_id = null)
    {
        checkAccess('add_order');

        if (!empty($_POST)) {
            $this->create_order();
        }

        $content['bagNumber'] = $bagNumber ? $bagNumber : '';
        $content['locker'] = $locker = new Locker($locker_id, false);
        $content['location'] = $location = new Location($locker->location_id, false);
        $content['claim_id'] = $claim_id;

        if ($display == 'page') {
            $this->setPageTitle('Quick Order');
            $this->renderAdmin('quick_order_back', $content);
        } else {
            //we are just using the code
            $content['dialog'] = true;
            $this->load->view('admin/orders/quick_order_back', $content);
        }
    }

    public function create_order()
    {

        $this->load->library("form_validation");
        $this->form_validation->set_rules("lockerID", "Locker iD", "required|numeric");
        $this->form_validation->set_rules("bagNumber", "Bag Number", "required|numeric");

        // validate post fields
        if (!$this->form_validation->run()) {
            set_flash_message("error", validation_errors());
            $this->goBack("/admin/orders/quick_scan_bag");
        }

        // validate bag number length
        $bagNumber = ltrim($_POST['bagNumber'], "0");
        if (strlen($bagNumber) != 7) {
            set_flash_message('error', "You have entered an invalid bag number.");
            $this->goBack('/admin/orders/quick_scan_bag');
        }

        // get the bag
        $this->load->model('bag_model');
        $bag = $this->bag_model->get_one(array(
            'business_id' => $this->business_id,
            'bagNumber' => $bagNumber
        ));


        // check the customer is not on hold
        $customer_id = !empty($bag->customer_id) ? $bag->customer_id : '';
        $customer = new Customer($customer_id);
        if ($customer->hold == 1) {
            $customer_link = "<a style='text-decoration:underline; color: #F6FF00;' href='/admin/customers/detail/{$customer->{Customer::$primary_key}}'> " .  getPersonName($customer) . "</a>";
            set_flash_message("error", "Can not create order because there is a hold on {$customer_link}'s account");
            $this->goBack("/admin/orders/quick_scan_bag");
        }

        // make sure the bagnumber has been printed
        $maxBagNumber = Bag::get_max_bagNumber();
        if ($bagNumber > $maxBagNumber) {
            set_flash_message("error", "You have entered a bag that is greater then the highest printed bag. This will cause duplicate bag issues in the future. The highest bag number printed is [".$maxBagNumber."]. To solve this problem, <a target='_parent' href='/admin/orders/bag_tags'>print out a new bag number</a>.");
            $this->goBack("/admin/orders/quick_scan_bag");
        }

        //make sure there is not an open order for this bag
        $this->load->model('order_model');
        $openOrders = $this->order_model->getOpenOrders($bagNumber, $this->business_id);
        if ($openOrders) {
            set_flash_message("error", 'There is already an open order for this bag.  <a target="_top" href="/admin/orders/details/'.$openOrder[0]->orderID.'">Order '.$openOrder[0]->orderID.'</a>');
            $this->goBack("/admin/orders/quick_scan_bag");
        }

        $notes = "";

        // get the claim if a claimID is passed
        if (!empty($_POST['claimID'])) {
            $claim = new Claim($_POST['claimID']);
        } else {

            // if no claimID is passed try to search for open claims
            $claims = Claim::search_aprax(array(
                "business_id" => $this->business_id,
                "customer_id" => $customer_id,
                "locker_id" => $this->input->post("lockerID"),
                "active" => 1
            ));

            $claim = $claims ? $claims[0] : false;

            // check that there is only one claim at the locker
            if (count($claims) > 1) {
                $locker = new Locker($_POST['lockerID'], true);
                $error_message = "Multiple claims were found for customer '" . getPersonName($customer) . " ({$customer->customerID})' in locker '{$locker->lockerName}' at location '{$locker->relationships['Location'][0]->address}'. Only one claim should exist.";
                send_admin_notification("Customer has multiple claims error", $error_message);
                set_flash_message("error", $error_message);

                $this->goBack("/admin/orders/quick_scan_bag");
            }
        }

        $options['customer_id'] = $customer_id;
        $options['claimID'] = $claim ? $claim->claimID : null;
        $order_id = $this->order_model->new_order(
            $_POST['locationID'],
            $_POST['lockerID'],
            $bagNumber,
            get_employee_id($this->session->userdata('buser_id')),
            $notes,
            $this->business_id,
            $options
        );

        if ($order_id) {

            // if we have  a claim, deactivate it
            if ($claim) {
                $this->load->model("claim_model");
                $this->claim_model->deactivate_claim($claim->claimID);
            }

            // ajax dialog, load especial view
            if ($_POST['dialog']) {
                $content['order_id'] = $order_id;
                $this->load->view('admin/orders/quick_order_back', $content);
                $this->output->_display();
                exit;
            }

            // go to the order
            redirect("/admin/orders/details/$order_id?new_order=true");
        }
    }

    public function add_quick_order()
    {
        if (isset($_POST['submit'])) {

            $quickOrder = new QuickOrder();
            $quickOrder->business_id = $this->business_id;
            $quickOrder->name = $_POST['name'];
            $quickOrder->bagNumber = $_POST['bagNumber'];
            $quickOrder->save();

            set_flash_message('success', "Quick order has been added");
            redirect("/admin/orders/quick_order");
        }

        $content = array();

        $this->setPageTitle('Add Quick Order');
        $this->renderAdmin('add_quick_order', $content);
    }


    public function edit_quick_order($id = null)
    {
        $quickOrder = new QuickOrder($id);
        $content['quickOrder'] = $quickOrder;

        if (isset($_POST['submit'])) {
            $quickOrder->name = $_POST['name'];
            $quickOrder->bagNumber = $_POST['bagNumber'];
            $quickOrder->save();

            set_flash_message('success', "Quick order has been added");
            redirect("/admin/orders/quick_order");
        }

        $this->setPageTitle('Edit Quick Order');
        $this->renderAdmin('edit_quick_order', $content);
    }

    /**
     * Deletes a quick order. There is no view for this
     *
     * @throws \Exception
     */
    public function delete_quick_order($id = null)
    {
        $quickOrder = new QuickOrder($id);
        if (!$quickOrder) {
            throw new \Exception("Quick order not found");
        }

        if ($quickOrder->business_id != $this->business_id) {
            set_flash_message("error", "Cannot delete a quick order that does not belong to this business");
            redirect('admin/orders/quick_order');
        }

        $quickOrder->delete();
        set_flash_message("success", "Quick order has been deleted");
        redirect('admin/orders/quick_order');
    }

    public function quick_order()
    {
        $content['quickOrders']  = QuickOrder::search_aprax(array('business_id'=>$this->business_id), FALSE);

        $this->setPageTitle('Quick Orders');
        $this->renderAdmin('quick_order', $content);
    }

    public function quick_order_bag($bag_number = null)
    {
        $this->load->model('order_model');
        $select1 = "select orderID,orders.locker_id from orders
        join bag on bagID = orders.bag_id
        where bagNumber = '$bag_number'
        and orderStatusOption_id <> 10";
        $select = $this->db->query($select1);

        $message = '';
        foreach ($select->result() as $line) {
            $options = array();
            $options['order_id'] = $line->orderID;
            $options['orderStatusOption_id'] = 9;
            $options['locker_id'] = $line->locker_id;
            $this->order_model->update_order($options);
            //set it to ready

            $message .= 'order: '.$line->orderID.' changed to ready<br>';

            //set it to complete
            $options = array();
            $options['order_id'] = $line->orderID;
            $options['orderStatusOption_id'] = 10;
            $options['business_id'] = $line->business_id;
            $options['locker_id'] = $line->locker_id;
            $this->order_model->update_order($options);
            $message .= 'order: '.$line->orderID.' changed to completed<br>';
        }

        $select1 = "select locker.location_id, lockerID, orders.customer_id
        from orders
        join bag on bagID = orders.bag_id
        join locker on lockerID = orders.locker_id
        where bagNumber = '$bag_number'
        order by orderID desc
        limit 1";
        $select = $this->db->query($select1);
        $order = $select->row();

        if ($order) {
            $options = array();
            $options['customer_id'] = $order->customer_id;
            if (!$order_id = $this->order_model->new_order($order->location_id, $order->lockerID, $bag_number, null, 'Quick Order Created', $this->business_id, $options)) {
                show_error("Order not created");
            }


            $message .=  'order: '.$order_id.' created - <a href="/admin/orders/details/'.$order_id.'">edit order</a><br>';

            //Aramark
            if ($order->customer_id == '6941') {
                $form = '<br><br><form action="/admin/orders/quick_order_items" method="post">
                        <table>
                        <tr><td>Executive Laundry:</td><td><input type="text" name="576|Executive Laundry"></td><td>Bags:</td><td><input type="text" name="570|Bags"></td></tr>
                        <tr><td>Robes: </td><td><input type="text" name="570|Robes"></td></tr>
                        <tr><td>Spa (Face Covers): </td><td><input type="text" name="570|Spa"></td></tr>
                        <tr><td>Salon (Towels & Smocks): </td><td><input type="text" name="570|Salon"></td></tr>
                        <tr><td>Sheets & Blankets: </td><td><input type="text" name="570|Sheets / Blankets"></td></tr>
                        <tr><td><input type="hidden" name="orderId" value="'.$order_id.'"><input type="submit" name="addItems" value="Add Items"></td></tr>
                                </table></form>';
            }
            //Marvel Maids
            else if ($order->customer_id == '1202') {
                $form = '<br><br><form action="/admin/orders/quick_order_items" method="post">
                        <table>
                        <tr><td>Rags:</td><td><input type="text" name="576|Rags"></td></tr>
                        <tr><td><input type="hidden" name="orderId" value="'.$order_id.'"><input type="submit" name="addItems" value="Add Items"></td></tr>
                                </table></form>';
            } else {
                //redirect to the order
                redirect("/admin/orders/details/".$order_id);
            }
        } else {
            $message .= 'No prior order found.  Enter this order manually';
        }
        $content['message'] = $message;
        $content['form'] = $form;

        $this->renderAdmin('quick_order_bag', $content);
    }

    public function quick_order_items()
    {
        //Get Order Info
        $order_id = $_POST['orderId'];

        //Loop through all items in REQUEST
        foreach ($_POST as $key => $value) {
            $items = explode("|", $key);
            if (count($items) == 2) {
                if ($items[1] == 'Bags') {
                    $update1 = "update orderItem set notes = 'Executive_Laundry - Bags: $value' where order_id = $order_id and product_process_id = $items[0]";
                    $update = $this->db->query($update1);
                } elseif ($value) {
                    $this->load->model('orderitem_model');
                    $this->load->model('product_process_model');

                    //TODO: hardcoded $supplier_id 10
                    $this->orderitem_model->add_item(
                        $order_id,          // $order_id
                        0,                  // $item_id
                        $items[0],          // $product_process_id
                        $value,             // $qty
                        $this->employee_id, // $employee_id
                        10,                 // $supplier_id
                        0,                  // $price
                        $items[1]           // $notes
                    );
                }
            }
        }
        redirect("../admin/orders/details/{$_REQUEST[orderId]}");
    }

    /**
     * This function applies the discounts to an order and updates the orderStatus to inventoried, and then renders the view for scanning in another bag.
     * Expects the order ID as segment
     */
    public function process_order($order_id = null)
    {
        if (!$order_id) {
            set_flash_message("error", get_translation("missing_order_id", "admin/orders", array(), "Missing order ID"));
            $this->goBack("/admin/orders");
        }

        $order = $this->getOrderById($order_id);

        // If this order has no customer assign the order to the blank customer
        if (empty($order->customer_id)) {
            $order->customer_id = $this->business->blank_customer_id;
            $order->save();
        }

        if ($order->orderPaymentStatusOption_id == 3 && $order->pre_paid_amount <= 0) {
            set_flash_message("error", get_translation("captured_cannot_change_discounts", "admin/orders/details", array(), "Payment has been captured for this order. Can not change discounts."));
            $this->goBack("/admin/orders/details/$order_id");
        }

        try {
            // If the order is a home delivery order
            $locker = new Locker($order->locker_id);
            $location = new Location($locker->location_id);

            //TODO: handle the other home deliveries service types
            if ($location->serviceType == Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY) {
                // Try to re-use an existing home delivery request
                $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($order->business_id);
                if ($homeDeliveryService) {
                    // in case this fails, customer should schedule home delivery
                    $response = $homeDeliveryService->addOrderToExisingHomeDelivery($order);
                }
            }
        } catch (Exception $e) {
            send_exception_report($e);
        }

        $this->load->library("discounts");
        $applyRes = $this->discounts->apply_discounts($order);

        $this->order_model->update_order_taxes($order_id);

        if ($applyRes['status']=='success') {
            //set order to inventoried
            $options['orderStatusOption_id'] = 3;
            $options['order_id'] = $order_id;

            $result = $this->order_model->update_order($options);
            if ($result['status'] == 'success') {
                set_static_message("success", get_translation("success_finalized", "admin/orders/details", array("order_id"=>$order_id, "customer_id"=>$order->customer_id, "locker_id"=>$order->locker_id ), "Order[%order_id%] has been finalized - <a href='/admin/orders/details/%order_id%'>Back to order[%order_id%]</a><br><br><br><a href='/admin/orders/create_shoe_shine?customerID=%customer_id%&lockerID=%locker_id%'> Create Special Order for Same Customer (shoes, leather, etc.) </a>"));
            } else {
                set_static_message("error", get_translation("error_finalized", "admin/orders/details", array("order_id"=>$order_id, "message"=>$result['message']), "Error setting Order[%order_id%] to inventoried: %message%. <a href='/admin/orders/details/%order_id%'>Back to order[%order_id%]</a><br></a>"));
            }
        }

        set_flash_message("success", get_translation("success_order_processed", "admin/orders/quick_scan_bag", array("order_id"=>$order_id, "message"=>$result['message']), "Successfully Processed order"));

        redirect("/admin/orders/quick_scan_bag");
    }

    public function update_order_taxes($order_id)
    {
        $this->order_model->update_order_taxes($order_id);
        $this->goBack("/admin/orders/details/$order_id");
    }

    public function clear_order_items($order_id = null)
    {
        $this->load->model('orderitem_model');
        $this->orderitem_model->order_id = $order_id;
        if ($this->orderitem_model->delete()) {
            set_flash_message('success', "Order has been cleared out");
        } else {
            set_flash_message('error', "Item not deleted from order");
        }
        redirect("/admin/orders/details/".$order_id);

    }

    public function add_product_to_order()
    {
        try {
            $order_id = $this->input->post('order_id');
            $qty = $this->input->post('qty');
            $product_process_id = $this->input->post('product_process_id');
            $notes = $this->input->post('notes');
            $supplier_id = $this->input->post('supplier_id');

            if (!$order_id) {
                set_flash_message("error", "Missing 'order_id' parameter");
                $this->goBack("/admin/orders");
            }

            if (!$product_process_id) {
                set_flash_message("error", "Missing 'product_process_id' parameter.");
                $this->goBack("/admin/orders/$order_id");
            }

            if (!$this->input->post("qty")) {
                set_flash_message("error", "Missing 'qty' parameter.");
                $this->goBack("/admin/orders/$order_id");
            }

            $this->load->model('orderitem_model');

            $product_process = new Product_Process($product_process_id);
            $product = new Product($product_process->product_id);

            $item_id = (isset($_POST['item_id']))?$_POST['item_id']:0;

            $unitPrice = $product_process->price;

            $item = new Item($item_id, true);
            //get item issues
            //redirect to a confirmation page that prints out the issue
            if ($item_id !=0) {
                $this->load->model('itemissue_model');
                $this->itemissue_model->item_id = $item->itemID;
                $this->itemissue_model->status = 'Opened';
                $issues = $this->itemissue_model->get();
                if($issues) {
                    redirect("/admin/orders/item_issues_confirmation/".$item_id."/".$_POST['order_id']."/".$_POST['product_id']."/".$_POST['process_id']);
                }
            }

            $order = new Order($order_id);
            $customer = new Customer($order->customer_id);

            $upchargeCost = 0;

            if (!empty($_POST['upcharge'])) {

                $this->db->where_in('upchargeID', $_POST['upcharge']);
                $upcharges = $this->db->get("upcharge")->result();

                $basePrice = $unitPrice;

                foreach ($upcharges as $upcharge) {
                    //if this customer doesn't pay for upcharges make the price 0;
                    if ($customer->noUpcharge == 0) {
                        if ($upcharge->priceType == "dollars") {
                            $upchargeAmt = $upcharge->upchargePrice;
                            $unitPrice = $upcharge->upchargePrice + $unitPrice;
                        } elseif ($upcharge->priceType == "percent") {
                            $upchargeAmt = number_format((($upcharge->upchargePrice / 100) * $basePrice),2, '.', '');
                            $unitPrice = $unitPrice + $upchargeAmt;
                        }
                    } else {
                        $upchargeAmt = 0;
                    }

                    $notes = $notes . $upcharge->name . ' @ ' . format_money_symbol($this->business_id, '%.2n', $upchargeAmt) . '<br>';
                    // get the upcharge costs
                    $sql = "SELECT cost FROM upchargeSupplier WHERE upcharge_id = {$upcharge->upchargeID} and supplier_id = {$supplier_id};";
                    $row = $this->db->query($sql)->row();
                    if (isset($row->cost)) {
                        $upchargeCost = $upchargeCost + $row->cost;
                    }
                }
            }

            $res = $this->orderitem_model->add_item($order_id,
                $item_id,
                $product_process->product_processID,
                $qty,
                $this->employee_id,
                $supplier_id,
                $unitPrice,
                $notes,
                $autoOverwrite = 0,
                $upchargeCost
            );

            if ($res['status']=='success') {
                //is this a wash fold item
                $productCategory = new ProductCategory($product->productCategory_id);
                if ($productCategory->slug == WASH_AND_FOLD_CATEGORY_SLUG) {
                    $this->load->library('wash_fold_product');
                    $result = $this->wash_fold_product->add_wash_fold_preferences($res['orderItemID']);

                    //add to the WF Log is WF is added manually
                    $addToLog = "insert IGNORE into wfLog
                                (order_id, weightIn, weightInCust, weightInTime, weightInIP)
                                values (?, ?, 'WF Added from Order Entry', now(), ?)";
                    $this->db->query($addToLog, array($order_id, $qty, $_SERVER['REMOTE_ADDR']));
                }

                $sub_message = '';
                if ($product_process->taxable) {
                    $order = new Order($order_id);
                    $locker = new Locker($order->locker_id);
                    $location = new Location($locker->location_id);
                    if (empty($location->state)) {
                        switch ($location->country) {
                            case 'usa': $field = 'state'; break;
                            case 'australia': $field = 'state'; break;
                            case 'canada': $field = 'province'; break;
                            default: $field = 'locality'; break;
                        }
                        $sub_message = "<p class='warning'>You added a taxable item but this location does not have a $field so we could not calculate the tax.</p>";
                    }
                }

                if (empty($item->relationships['Barcode'][0]->barcode)) {
                    set_flash_message("success", "Added Item to Order $sub_message");
                } else {
                    set_flash_message('success', "Added Barcode '{$item->relationships['Barcode'][0]->barcode}' to Order $order_id. $sub_message");
                }

            } else {
                set_flash_message('error', $res['error']);
                redirect("/admin/orders/details/$order_id");
            }
            $next_barcode = $this->input->post("next_barcode");
            unset($_POST);
            if (!empty($next_barcode))

            {
                $order = new Order($order_id, false);
                $_POST['order_id'] = $order->orderID;
                $_POST['barcode'] = $next_barcode;
                $_POST['customer_id'] = $order->customer_id;
                $this->get_barcode();
            } else {
                redirect("/admin/orders/details/$order_id");
            }
        } catch (\User_Exception $e) {
            set_flash_message("error", "Could not add item: {$e->getMessage()}");

            $this->goBack("/admin/orders/detail/$order_id");
        }

    }

    public function update_product_process_and_add_item_to_order()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("orderID", "Order ID", "numeric|required");
        $this->form_validation->set_rules("barcode", "Barcode", "numeric|required|callback_validate_barcode_length");
        $this->form_validation->set_rules("product_process_id", "numeric|required");
        $this->form_validation->set_rules("itemID", "numeric|required");


        if ($this->form_validation->run()) {
            $orderID = $this->input->post("orderID");
            $barcode = $this->input->post("barcode");
            $product_process_id = $this->input->post("product_process_id");
            $itemID = $this->input->post("itemID");
            $item = new Item($itemID);
            $item->product_process_id = $product_process_id;
            $item->save();
            $this->add_new_item($orderID, $itemID);

        } else {
            set_flash_message("error", validation_errors());
            $this->goBack(is_numeric($orderID) ? "/admin/orders/details/$orderID" : "/admin/orders");
        }
    }

    /**
     * adds an item to an order from the "Add New Item" form on order_detail
     *
     *
     * $_POST values
     * ------------------------
     * order_id int required
     * qty int required
     * item_id int (Optional)
     * notes string optional
     * supplier_id int optional
     * product_process_id int required
     *
     */
    private function add_new_item($order_id, $item_id)
    {
        if (!is_numeric($item_id)) {
            throw new \InvalidArgumentException("'item_id' must be numeric.");
        }
        if (!is_numeric($order_id)) {
            throw new \InvalidArgumentException("'order_id' must be numeric.");
        }
        $item = new Item($item_id, false);

        $this->load->model("barcode_model");
        $barcode = $this->barcode_model->get_one(array(
            "item_id" => $item->itemID,
        ));

        $existing_items = OrderItem::search_aprax(array(
            "item_id" => $item_id,
            "order_id" => $order_id
        ));
        if (!empty($existing_items)) {
            set_flash_message("error","Item Barcode '{$barcode->barcode}' already exists in order '$order_id'");

            $this->goBack("/admin/orders/details/$order_id");
        }

        $order = new Order($order_id, false);
        $customer = new Customer($order->customer_id);

        try {
            $product_process = new Product_Process($item->product_process_id);
        } catch (NotFound_Exception $e) {
            $product_process = null;
        }

        if (empty($item->product_process_id) || is_null($product_process) || $product_process->active == 0) {
            $this->load->model("product_process_model");
            $content['product_processes'] = $this->product_process_model->get_as_codeigniter_dropdown($this->business->businessID);
            $content['barcode'] = $barcode->barcode;
            $content['orderID'] = $order->orderID;
            $content['itemID'] = $item->itemID;

            $this->setPageTitle('Inactive Product Error');
            $this->renderAdmin('/admin/items/inactive_product_process_error', $content);
        } else {
            $product_process_id = $item->product_process_id;

            $this->load->model('orderitem_model');

             //remove any one time specifications
             $this->load->model('itemspecification_model');
             $item_specifications = $this->itemspecification_model->remove_oneTimeSpecification($item_id);

             //remove any one time upcharges
             $this->load->model('itemupcharge_model');
             $upchargesRemoved = $this->itemupcharge_model->remove_oneTimeUpcharges($item_id);

            try {
                $product = new Product($product_process->product_id, false);
            } catch (NotFound_Exception $e) {
                $product = null;
            }

            // For business with breakout vat enabled, the tax is included in the item price.
            // So, if the customer is non-taxable, we have to remove the tax from the item price.
            $unitPrice = $product_process->price;
            if (!$customer->taxable) {
                $breakout_vat = get_business_meta($order->business_id, 'breakout_vat');
                if ($breakout_vat) {
                    $this->load->model('taxgroup_model');
                    $taxRate = $this->taxgroup_model->get_tax_rate($order_id, $product_process->taxGroup_id);
                    $unitPrice = $unitPrice / (1 + $taxRate);
                }
            }
            //get the issues for this item
            $this->load->model('itemissue_model');
            $issues = $this->itemissue_model->get_aprax(array(
                'item_id' => $item->itemID,
            ), 'updated');

           //begin add issues to assembly notes
           if($order->postAssembly != ''){
               $assemblyNotes = $order->postAssembly.' ';
           }else{
               $assemblyNotes = '';
           }

           $regex = "/".$barcode->barcode."\[(.*?)\]/i";
           if(!preg_match($regex, $assemblyNotes)){
               $newNotes = '';
               foreach($issues as $issue) {
                   if(trim($issue->note)!=''){
                        $newNotes .= $issue->note.' ';
                   }
                }
                if($newNotes!=''){
                    $assemblyNotes .= $barcode->barcode.'['.trim($newNotes).'] ';
                }
           }

            $where['orderID'] = $order_id;
            $options['postAssembly'] = trim($assemblyNotes);
            $this->order_model->update($options, $where);

            $res = $this->orderitem_model->add_item(
                $order_id,           // $order_id
                $item_id,            // $item_id
                $product_process_id, // $product_process_id
                1,                   // $qty
                $this->employee_id,  // $employee_id
                '',                  // $supplier_id
                $unitPrice           // $price
            );

            if ($res['status']=='success') {
                // Need to retake picture?
                $show_retake_picture_msg = $this->orderitem_model->retake_picture($item_id, $this->business_id);
                $retake_picture_msg = "";
                if ($show_retake_picture_msg) {
                    $retake_picture_msg = '<br/><br/><small style="margin-left:-5em;">'.get_translation("retake_picture_of_item", "(Please, retake picture of this item.)", array(), "(Please, retake picture of this item.)").'<small>';
                }                
                
                //is this a wash fold item
                $productCategory = new ProductCategory($product->productCategory_id);
                if ($productCategory->slug == WASH_AND_FOLD_CATEGORY_SLUG) {
                    $this->load->library('wash_fold_product');
                    $result = $this->wash_fold_product->add_wash_fold_preferences($res['orderItemID']);
                }

                if (empty($barcode)) {
                    set_flash_message("success", "Added Item to Order");

                } else {
                    set_flash_message('success', "Added Barcode '$barcode->barcode' to Order".$retake_picture_msg);
                }
                redirect("/admin/orders/item_details/order/".$order_id."/item_id/".$item->itemID);
            }
        }
    }


    /**
     * $a[] = array('orderItemID'=>'', 'qty'=>'','price'=>'')
     */
    public function edit_order_items($order_id = null)
    {
        if (!is_numeric($order_id)) {
            set_flash_message("error", "'order_id' must be numeric and passed as segment 4 in the URL. ");

            return $this->goBack("/admin/orders");
        }
        $orderItems = array();

        //this makes the first array
        foreach ($_POST as $key => $orderItem) {
            foreach ($orderItem as $property => $value) {
                $orderItems[$property][] = $value;
            }

        }

        $updateOptions = array();
        foreach ($orderItems as $orderItemID => $property) {

            $orderItemArray = array();
            $orderItemArray['orderItemID'] = $orderItemID;
            foreach ($property as $name=>$value) {
                foreach ($value as $key => $value) {
                    $orderItemArray[$key]=$value;
                }
            }

            $orderItem = new OrderItem($orderItemArray['orderItemID']);

            /*
             * when changing the supplier, we need to get the upchargeSupplier and the cost for all the upcharges
             *
             */

            $product_suppliers = Product_Supplier::search_aprax(array(
                "supplier_id" => $orderItemArray['supplier_id'],
                "product_process_id" => $orderItem->product_process_id
            ));

            if ($product_suppliers) {
                $product_supplier = $product_suppliers[0];
                $orderItemArray['cost'] = $product_supplier->cost;
            }

            //if order Item has upcharges in the itemUpcharge table get the sum of the costs and add to the orderItem
            $upcharges = ItemUpcharge::search_aprax(array('item_id'=>$orderItem->item_id));
            if ($upcharges) {

                // get the costs for each upcharge added to the item
                foreach ($upcharges as $upcharge) {
                    if ( !empty( $orderItemArray['supplier_id'] ) ) {
                        $sql = "SELECT cost FROM upchargeSupplier WHERE upcharge_id = {$upcharge->upcharge_id} and supplier_id = {$orderItemArray['supplier_id']}";
                        $row = $this->db->query($sql)->row();
                        $orderItemArray['cost'] = $orderItemArray['cost'] + $row->cost;
                    }
                }
            }

            if (strstr($orderItemArray['cost'], ',')) {
                //this is for currency format like: 1.200,50
                $cost = $orderItemArray['cost'];
                $cost = str_replace('.', '', $cost);
                $cost = str_replace(',', '.', $cost);
                $orderItemArray['cost'] = $cost;
            }

            if (strstr($orderItemArray['unitPrice'], ',')) {
                //this is for currency format like: 1.200,50
                $unitPrice = $orderItemArray['unitPrice'];
                $unitPrice = str_replace('.', '', $unitPrice);
                $unitPrice = str_replace(',', '.', $unitPrice);
                $orderItemArray['unitPrice'] = $unitPrice;
            }

            $updateOptions[] = $orderItemArray;

        } // end foreach

        $this->db->update_batch('orderItem', $updateOptions, 'orderItemID');
        if ($this->db->affected_rows()) {
            set_flash_message('success', "Order Items have been updated");
        } else {
            set_flash_message('error', "Order Items were not updated");
        }
        redirect("/admin/orders/details/".$order_id);
    }

    public function update_ticket($order_id = null)
    {
        if (isset($_POST['ticket_submit'])) {
            $where['orderID'] = $order_id;
            $options['ticketNum'] = $_POST['ticketNum'];
            $this->order_model->update($options, $where);

            set_flash_message('success', "Updated Ticket");
            redirect("/admin/orders/details/".$order_id);
        }
    }

    /**
     *
     */
    public function create_item()
    {
        $ids = $this->uri->uri_to_assoc(4);
        $order_id = $ids['order'];
        $barcode = $ids['barcode'];

        // Currently only checking if there is one
        if (!$barcode) {
            set_flash_message('error', get_translation("missing_barcode", "admin/orders/create_item", array(), "Missing Barcode"));
            redirect('/admin/orders/details/'.$order_id);
        }

        // if missing the order id
        // try to find it using the barcode
        if (!$order_id) {
            $sql = "select max(order_id) as order_id
            from orderItem
            JOIN barcode on barcode.item_id = orderItem.item_id
            where barcode = ?";

            $result=$this->db->query($sql, array($barcode))->result();

            if (!$result[0]->order_id) {
                set_flash_message("error", get_translation("barcode_not_attached_to_an_order", "admin/orders/create_item", array("barcode"=>$barcode), "The barcode '%barcode%' is not attached to an order.  Please rescan and try again or contact manager."));
                redirect_with_fallback('/admin/orders/view');

            } else {
                $order = new Order($result[0]->order_id);
            }
        } else { //get the order details
            $order = new Order($order_id);
        }

        if ($this->business_id != $order->business_id) {
            set_flash_message("error", get_translation("order_not_belong_to_business", "admin/orders/create_item", array("order_id"=>$$order_id), "Order %order_id% does not belong to this business"));
            redirect_with_fallback('/admin/orders/view');
        }


        // Does this barcode exists?
        $this->load->model('barcode_model');
        $item = $this->barcode_model->getBarcode($barcode, $this->business_id);

        if ($item) {
            //does this item belong to another customer?
            if ($item[0]->customerID != $order->customer_id) {
                // customer item mismatch
                $error = array(
                    'error' => 'customer_mismatch',
                    'content' => array($item[0]->customerID, $order->customer_id)
                );
            }
        }

        $content = $this->getBusinessItemSettings();

        $content['type'] = 'new';
        $content['barcode'] = $barcode;
        $content['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
        $content['orderStatusOption_id'] = $order->orderStatusOption_id;
        $content['order_id'] = $order_id;
        $content['customer_id'] = $order->customer_id;

        $this->load->model("customer_model");
        $content['customer'] = $this->customer_model->get_by_primary_key($order->customer_id);

        $this->load->model('customerpreference_model');
        $content['customerPreferences'] = $this->customerpreference_model->get_preferences($content['customer_id']);

        $content['item_preferences'] = array();

        // get the quickitem buttons
        $this->load->model('quickitem_model');
        $buttons =  $this->quickitem_model->get_aprax(array(
            'business_id' => $this->business_id
        ));

        $content['buttons'] = $buttons;


        $content['edit_item_subview'] = $this->load->view("/admin/subViews/item_edit", $content, true);

        $this->setPageTitle('Create Item');
        $this->renderAdmin('create_item', $content);
    }

    /**
     * Used for the coeigniter form validator rule callback to assert that the specified product process exists.
     * @param int $product_process_id
     * @return boolean
     */
    public function does_process_exist($process_id)
    {
        $this->load->model("process_model");
        //Note, a processs is optional. No process is sometimes passed incorrectly as '0' by * code.
        if ($process_id == 0 || $this->process_model->does_exist($process_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_process_exist", "Product process ID '$process_id' does not exist");

            return false;
        }
    }
    /**
     * Used for the coeigniter form validator rule callback to assert that the specified product process exists.
     * @param int $product_process_id
     * @return boolean
     */
    public function does_product_exist($product_id)
    {
        $this->load->model("product_model");
        if ($this->product_model->does_exist($product_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_product_exist", "Product ID '$product_id' does not exist");

            return false;
        }
    }


    /**
     * creates a new item
     *
     * Expects the following POST parameters:
     *  itemID
     *  customer_id
     *  product_id
     *  process_id
     */
    public function save_item()
    {
        $response = $this->createItem();
        set_flash_message($response['status'], $response['message']);

        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
            return;
        }

        if ($response['status'] == 'success') {
            redirect($response['redirect']);
        } else {
            redirect_with_fallback('/admin/orders');
        }
    }

    public function createItem()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("barcode", "Barcode", "required|integer");
        $this->form_validation->set_rules("process_id", "Process", "integer|does_process_exist");
        $this->form_validation->set_rules("product_id", "Product", "required|integer|does_product_exist");
        $this->form_validation->set_rules("customer_id", "Customer", "integer");

        if (!$this->form_validation->run()) {
            return array("status" => "error", "message" => validation_errors());
        }

        $process_id = $this->input->post("process_id");
        $product_id = $this->input->post("product_id");

        $customer_id = $this->input->post("customer_id");
        $order_id = $this->input->post("order_id");
        $barcode = $this->input->post("barcode");

        $options['color'] = $this->input->post('color');
        $options['manufacturer'] = $this->input->post('manufacturer');
        //$options['starchOnShirts_id'] = $this->input->post('starch');
        $options['note'] = $this->input->post('note');
        $options['business_id'] = $this->business_id;

        $upcharges = $this->input->post("upcharges");
        $specifications = $this->input->post("specifications");

        //get the product_processID
        $this->load->model("product_process_model");
        $product_process = $this->product_process_model->get_one(array(
            'product_id' => $product_id,
            'process_id' => $process_id
        ));

        if (empty($product_process)) {
            return array("status" => "error", "message" => "Could not find product process for product ID '$product_id' and process ID '$process_id'");
        }

        $options['product_process_id'] = $product_process->product_processID;

        $this->load->model('item_model');
        $item_id = $this->item_model->save($options);


        //check to see that the barcode does not exist
        $this->load->model('barcode_model');

        //insert into barcode
        $existing_barcodes_count = $this->barcode_model->count(array(
            "barcode" => $barcode,
            "business_id" => $this->business_id
        ));

        if ($existing_barcodes_count > 0) {
            return array("status" => "error", "message" => "Barcode '$barcode' already exists");
        }

        $this->barcode_model->save(array(
            'item_id' => $item_id,
            'barcode' => $barcode,
            'business_id' => $this->business_id,
        ));

        //insert into customer_item
        $this->load->model('customer_item_model');
        $this->customer_item_model->save(array(
            'customer_id' => $customer_id,
            'item_id' => $item_id,
        ));

        $user_id = get_employee_id($this->buser_id);

        //insert into upcharges, if any
        if ($upcharges) {
            $this->load->model('itemupcharge_model');
            $this->itemupcharge_model->update($item_id, $upcharges, $user_id);
        }

        //insert into specifications, if any
        if ($specifications) {
            $this->load->model('itemspecification_model');
            $this->itemspecification_model->update($item_id, $specifications);
        }

        //insert into itemhistory
        $this->load->model('itemhistory_model');

        $this->itemhistory_model->save(array(
            'item_id' => $item_id,
            'description' => 'Created Item',
            'creator' => get_employee_id($this->buser_id),
        ));




        if ($this->input->post('add_to_order') && $order_id) {
            $notes = $this->input->post('note') . "\n";
            $this->load->model('orderitem_model');


            $result = $this->orderitem_model->add_item(
                $order_id,                           // $order_id
                $item_id,                            // $item_id
                $product_process->product_processID, // $product_process_id
                1,                                   // $qty
                $this->employee_id,                  // $employee_id
                '',                                  // $supplier_id
                $product_process->price,             // $price
                $notes                               // $notes
            );

            $message = "Created Barcode '$barcode' and Added it to Order '$order_id'";
        } else {
            $message = "Created Barcode '$barcode'";
        }

        if ($this->input->post('redirect')) {
            $redirect = $this->input->post('redirect');
        } else {
            $redirect = "/admin/orders/edit_item?orderID=$order_id&itemID=$item_id";
        }

        return array(
            'status' => 'success',
            'message' => $message,
            'redirect' => $redirect,
        );
    }

    protected function getBusinessItemSettings($content = array())
    {
        //get the business specifications
        $this->load->model('specification_model');
        $content['specifications'] = $this->specification_model->get_aprax(array(
            'business_id' => $this->business_id
        ), 'description');

        // needed for the productCategory dropdown in the item_edit subview
        $this->load->model('productcategory_model');
        $content['productCategories'] = $this->productcategory_model->get_all_as_codeigniter_dropdown($this->business_id);
        $content['categories_products'] = $this->productcategory_model->get_productCategories_and_products($this->business_id);

        // get the processes for each product
        $this->load->model("product_process_model");
        $content['products_product_processes'] = $this->product_process_model->get_products_and_product_processes($this->business_id);

        // get the upcharge groups for the business
        $groups = array();
        $upchargeGroups = UpchargeGroup::search_aprax(array(
            'business_id' => $this->business_id
        ));
        foreach ($upchargeGroups as $upchargeGroup) {
            $upchargeItems = $upchargeGroup->getGroupUpcharges();
            if(!empty($upchargeItems)) {
                $groups[$upchargeGroup->upchargeGroupID] = $upchargeItems;
            }
        }
        $content['groups'] = $groups;

        $this->load->library('productslibrary');
        $content['businessPreferences'] = $this->productslibrary->getBusinessPreferences($this->business_id);

        $this->load->model('customoption_model');
        $business_languageID = get_customer_business_language_id();
        $business_languageID = empty($business_languageID)?$this->business->default_business_language_id:$business_languageID;
        $customOptions = $this->customoption_model->getCustomOptions($this->business_id, $business_languageID);

        // use custom colors or defaults if not set
        if (isset($customOptions['color_options'])) {
            foreach ($customOptions['color_options']->values as $colorValue) {
                $content['colors'][$colorValue->value] = $colorValue->name;
            }
            unset($customOptions['color_options']);
        } else {
            $content['colors'] = array(
                'white' => 'White',
                'black' => 'Black',
                'red' => 'Red',
                'blue' => 'Blue',
            );
        }

        // intercept brand options
        if (isset($customOptions['brand_options'])) {
            foreach ($customOptions['brand_options']->values as $brandValue) {
                $content['brands'][$brandValue->value] = $brandValue->name;
            }
            unset($customOptions['brand_options']);
        }

        $content['customOptions'] = $customOptions;

        return $content;
    }

    /**
     * Renders the interface for editing an item
     *
     * Can take the following GET parameters
     *  itemID (required): the item ID
     *  orderID (optional): the order ID that the item is associated with
     *
     * @param int $item_id
     * @param int $order_id
     */
    public function edit_item($order_id = null, $item_id = null)
    {
        if ($this->input->get("itemID")) {
            $item_id = $this->input->get("itemID");
        }

        if ($this->input->get("orderID")) {
            $order_id = $this->input->get("orderID");
        }

        if (!is_numeric($item_id)) {
            set_flash_message('error', get_translation("item_id_must_be_numeric_must_be_pased_as_segment_5", "admin/orders/edit_item", array(), "'item_id' must be numeric must be pased as segment 5 in the URL or as the GET parameter 'itemID'"));
            redirect_with_fallback('/orders');
        }

        $item = new Item($item_id);
        if ($item->business_id != $this->business_id) {
            set_flash_message("error", get_translation("item_id_does_not_belong_to_this_business", "admin/orders/edit_item", array("item_id"=>$item_id), "Item ID '%item_id%' does not belong to this business."));
            redirect_with_fallback("/admin/orders");
        }

        $content['item'] = $item;
        $content['pictures'] = Picture::search_aprax(array(
            'item_id' => $item->itemID,
        ));

        $content['closed'] = true;
        $content['order_id'] = $order_id;
        $content['type'] = 'edit';
        $content['business'] = $this->business;

        //if order_id is passed, we need to get the order object
        if ($order_id) {

            // get the order
            $content['order'] = $order = new Order($order_id);

            // get the customers that owns this item
            $content['customer_items'] = Customer_Item::search_aprax(array(
                "item_id" => $item->itemID,
                "customer_id" => $order->customer_id
            ), true);

            $content['closed'] = !in_array($order->orderStatusOption_id, array(1,2,3))
                    && $order->orderPaymentStatusOption_id == 3;

            $customer_id = $order->customer_id;
        } else {

            // Otherwise it retrieves all customers associated with the item.
            $content['customer_items'] = Customer_Item::search_aprax(array(
                "item_id" => $item->itemID
            ), true);

            // if only one customer owns the item, we can use that to load the DC preferences
            if (count($content['customer_items']) == 1) {
                $customer_id = $content['customer_items'][0]->customer_id;
            }
        }

        if (!empty($customer_id)) {
            $this->load->model("customer_model");
            $content['customer'] = $this->customer_model->get_by_primary_key($customer_id);
            $content['customer_id'] = $customer_id;

            $this->load->model('customerpreference_model');
            $content['customerPreferences'] = $this->customerpreference_model->get_preferences($content['customer_id']);
        } else {
            $content['customerPreferences'] = array();
            $content['customer_id'] = 0;
        }

        $this->load->model('itempreference_model');
        $content['item_preferences'] = $this->itempreference_model->get_preferences($item_id);

        $this->load->model('itemoption_model');
        $content['item_options'] = $this->itemoption_model->get_options($item_id);


        // get item specifications
        $this->load->model('itemspecification_model');
        $item_specifications = $this->itemspecification_model->get_aprax(array(
            'item_id' => $item_id,
        ));

        $content['item_specifications'] = array();
        foreach ($item_specifications as $item_specification) {
            $content['item_specifications'][$item_specification->specification_id] = 1;
        }

        // get item upcharges
        $this->load->model('itemupcharge_model');
        $upcharges = $this->itemupcharge_model->get_aprax(array(
            'item_id' => $item_id,
        ));

        $content['item_upcharges'] = array();
        foreach ($upcharges as $upcharge) {
            $content['item_upcharges'][$upcharge->upcharge_id] = 1;
        }

        // get the item barcode
        $this->load->model('barcode_model');
        $barcodes = $this->barcode_model->get_aprax(array(
            'item_id' => $item_id,
            ), 'barcodeID DESC'
        );

        $content['barcodes'] = $barcodes;

        //use the product_process_id from item to get product_id and process_id
        $this->load->model("product_process_model");
        $product_process = $this->product_process_model->get_by_primary_key($item->product_process_id);
        $content['product_id'] = $product_process->product_id;
        $content['process_id'] = $product_process->process_id;

        // get the item product
        if (!empty($content['product_id'])) {
            $currentProduct = Product::search(array(
                'productID' => $content['product_id']
            ));

            $content['currentProduct'] = $currentProduct;
            $content['currentProductCategory_id'] = $currentProduct->productCategory_id;
        } else {
            $content['currentProductCategory_id'] = null;
        }

        // append to $content the business settings
        $content = $this->getBusinessItemSettings($content);

        // get the item history
        $this->load->model('itemhistory_model');
        $this->itemhistory_model->item_id = $item_id;
        $this->itemhistory_model->leftjoin('employee ON employeeID = creator');
        $this->itemhistory_model->order_by = 'date DESC';
        $itemHistory = $this->itemhistory_model->get();

        // get the orderitem history
        $this->load->model('orderitem_model');
        $this->orderitem_model->item_id = $item_id;
        $this->orderitem_model->order_by = 'updated DESC';
        $orderItemHistory = $this->orderitem_model->get();

        $history = array();
        foreach ($itemHistory as $history_item) {
            $history[] = array(
                'date' => $history_item->date,
                'description' => $history_item->description,
                'firstName' => $history_item->firstName,
                'lastName' => $history_item->lastName
            );
        }

        foreach ($orderItemHistory as $history_item) {
            $history[] = array(
                    'date' => $history_item->updated,
                    'description' => 'Added to order',
                    'order_id' => $history_item->order_id
            );
        }

        usort($history, function($a, $b) {
            return strtotime($b['date']) - strtotime($a['date']);
        });

        $content['history'] = $history;

        $content['edit_item_subview'] = $this->load->view("/admin/subViews/item_edit", $content, true);

        $this->setPageTitle('Edit Item');
        $this->renderAdmin('edit_item', $content);
    }

    public function item_details()
    {
        try {
            $closed = true;
            $ids = $this->uri->uri_to_assoc(4);

            $order_id = $ids['order'];
            $item_id = $ids['item_id'];

            //if order_id is passed, we need to get the order object
            if ($order_id) {
                $orderObj = new Order($order_id, false);
                $content['order'] = $orderObj;
                $this->load->model("orderitem_model");

                $this->orderitem_model->options = array('order_id' => $orderObj->{Order::$primary_key});

                $content['orderItems'] = array_reverse($this->orderitem_model->get());
                $closed = false;
            }

            if ($orderObj->business_id != $this->business_id) {
                set_flash_message('error', "Sorry, but the order you are trying to view does not belong to your business");
                redirect("/admin/orders");
            }

            $content['closed'] = $closed;

            $content['item'] = new Item($item_id, false);

            $this->load->model("barcode_model");
            $this->barcode_model->options = array("item_id" => $content['item']->{Item::$primary_key});
            $barcodes = $this->barcode_model->get();
            $content['barcode'] = $barcodes[0];

            $this->load->model("itemhistory_model");

            $this->itemhistory_model->options = array("item_id" => $content['item']->{Item::$primary_key});
            $content['itemHistory'] = $this->itemhistory_model->get();

            $content['product_process'] = new Product_Process($content['item']->product_process_id, true);

            $this->load->model("picture_model");
            $this->picture_model->options = array("item_id" => $content['item']->{Item::$primary_key});
            $pictures = $this->picture_model->get();
            $picture = $pictures ? $pictures[0] : null;

            if (isset($picture->file)) {
                $content['picture_file'] = $picture->file;
            } else {
                $content['picture_file'] = null;
            }
            $open_itemIssues = ItemIssue::search_aprax(array(
                "item_id" => $content['item']->itemID,
                "status" => "Opened"
            ));

            $this->load->model('item_model');
            $content['item_upcharges'] = $this->item_model->get_item_upcharges($item_id);
            $content['item_specifications'] = $this->item_model->get_item_specifications($item_id);

            $this->load->model('itempreference_model');
            $content['item_preferences'] = $this->itempreference_model->get_preferences_names($item_id);

            $this->load->model("order_model");
            $order = $this->order_model->get_by_primary_key($order_id);

            $this->load->model("customer_model");
            $customer = $this->customer_model->get_by_primary_key($order->customer_id);

            $this->load->model('customerpreference_model');
            $customer_preferences = $this->customerpreference_model->get_preferences_names($order->customer_id);

            $content['changes'] = array();
            foreach ($content['item_preferences'] as $name => $value) {
                if (isset($customer_preferences) && $customer_preferences[$name] != $value) {
                    $content['changes'][$name] = "Note: Customer $name setting '{$customer_preferences[$name]}' does not match item DC setting '{$value}'";
                }
            }

            if (empty($open_itemIssues)) {
                $this->setPageTitle('Item Details');
                $this->renderAdmin('item_details', $content);
            } else {
                $content['itemIssues'] = $open_itemIssues;
                foreach ($open_itemIssues as $key => $itemIssue) {
                    $itemIssue->status="Closed";
                    $itemIssue->save();

                    //grab photo
                    $item_image_query = $this->db->get_where('picture', array( 'item_id' => $itemIssue->item_id ) );
                    $item_image = $item_image_query->row();
                    $content['itemIssues'][$key]->file = $item_image->file;
                }
                $content['product'] = new Product($content['product_process']->product_id, false);
                $this->load->view("admin/itemIssue/view", $content);
            }

        } catch (NotFound_Exception $e) {
            set_flash_message("error", $e->getMessage());
            redirect($this->agent->referrer());
        } catch (Exception $e) {
            send_exception_report($e);
            set_flash_message("error", "Could not retrieve item details.");
            redirect($this->agent->referrer());
        }
    }

    public function create_item_ajax()
    {
        $this->save_item();
    }

    public function edit_item_ajax()
    {
        $this->update_item();
    }

    /**
     * Updates an item
     *
     * Expects the following POST parameters:
     *  itemID
     *  order_id
     *  product_id
     *  process_id
     */
    public function update_item()
    {
        $response = $this->updateItem();
        set_flash_message($response['status'], $response['message']);

        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
            return;
        }

        if ($response['status'] == 'success') {
            redirect($response['redirect']);
        } else {
            redirect_with_fallback('/admin/orders');
        }
    }

    protected function updateItem()
    {
        $this->load->helper(array("form", "url", "ajax"));

        $this->load->library("form_validation");
        $this->form_validation->set_rules("itemID", "Item ID", "required");
        $this->form_validation->set_rules("product_id", "Product ID", "required");
        if ($this->input->post('newBarcode')) {
            $this->form_validation->set_rules(
                "newBarcode", "Barcode",
                'callback_validate_barcode_length|callback_validate_barcode_not_exists'
            );
        }

        if (!$this->form_validation->run()) {
            return array("status" => "error", "message" => validation_errors());
        }

        $item_id = $this->input->post('itemID');
        $order_id = $this->input->post('order_id');
        $product_id = intval($this->input->post('product_id'));
        $process_id = intval($this->input->post('process_id'));

        $this->load->model('product_process_model');
        $new_product_process = $this->product_process_model->get_one(array(
            'product_id' => $product_id,
            'process_id' => $process_id
        ));

        if (empty($new_product_process)) {
            return array("status" => "error", "message" => "Could not find product process for product ID '$product_id' and process ID '$process_id'");
        }

        $options['product_process_id'] = $new_product_process->product_processID;

        $options['color'] = $this->input->post('color');
        $options['manufacturer'] = $this->input->post('manufacturer');
        //$options['starchOnShirts_id'] = $this->input->post('starch');
        $options['note'] = $this->input->post('note');


        $this->load->model('item_model');
        $item = $this->item_model->get_by_primary_key($item_id);
        $product_process = $this->product_process_model->get_by_primary_key($item->product_process_id);

        $description = "";

        // check for changes to the item in order to log those changes to the item history.
        if (empty($product_process->product_id) || $product_process->product_id != $product_id) {
            $this->load->model('product_model');
            $product = $this->product_model->get_by_primary_key($product_id);
            $description .= "Product changed to '{$product->name}'.";
        }

        if (empty($product_process->process_id) || $product_process->process_id != $process_id) {
            $this->load->model('process_model');
            $process = $this->process_model->get_by_primary_key($process_id);
            $description .= " Process changed to '{$process->name}'.";
        }

        if ($item->color != $options['color']) {
            $description .= " Color changed to '{$options['color']}'.";
        }

        if ($item->manufacturer != $options['manufacturer']) {
            $description .= " Brand changed to '{$options['manufacturer']}'.";
        }

        if ($item->note != $options['note']) {
            $description .= " Notes changed to '{$options['note']}'.";
        }

        $options['itemID'] = $item_id;
        $options['business_id'] = $this->business_id;
        $this->item_model->save($options);

        // Update upcharges
        if ($this->input->post("upcharges")) {
            $upcharges = $this->input->post("upcharges");
        } else {
            $upcharges = array();
        }

        $this->load->model('itemupcharge_model');
        $item_upcharges = array();
        foreach ($this->itemupcharge_model->get_upcharges($item_id) as $upcharge) {
            $item_upcharges[] = $upcharge->upcharge_id;
        }

        $upcharges_changed = array_diff($upcharges, $item_upcharges) + array_diff($item_upcharges, $upcharges);

        // detect changes to the upcharges
        if ($upcharges_changed) {
            $user_id = get_employee_id($this->buser_id);
            $this->itemupcharge_model->update($item_id, $upcharges, $user_id);

            if (empty($upcharges)) {
                // special case, all upcharges removed
                $description .= " All upcharges were removed.";
            } else {
                // tell what upcharges we have now
                $item_upcharges = $this->itemupcharge_model->get_upcharges($item_id);
                $upcharge_changes = array();
                foreach ($item_upcharges as $item_upcharge) {
                    $upcharge_changes[] = $item_upcharge->name;
                }
                $description .= sprintf(" Upcharges changed to <b>%s</b>.", implode("</b>, <b>", $upcharge_changes));
            }
        }

        // update specifications
        if ($this->input->post("specifications")) {
            $specifications = $this->input->post("specifications");
        } else {
            $specifications = array();
        }

        $this->load->model('itemspecification_model');

        $item_specifications = array();
        foreach ($this->itemspecification_model->get_specifications($item_id) as $specification) {
            $item_specifications[] = $specification->specification_id;
        }

        $specifications_changed = array_diff($specifications, $item_specifications) + array_diff($item_specifications, $specifications);

        // detect changes to the specifications
        if ($specifications_changed) {

            $this->itemspecification_model->update($item_id, $specifications);

            if (empty($specifications)) {
                // special case, all specifications removed
                $description .= " All specifications were removed";
            } else {
                // tell what specifications we have now
                $item_specifications = $this->itemspecification_model->get_specifications($item_id);
                $specifications_changes = array();
                foreach ($item_specifications as $item_specification) {
                    $specifications_changes[] = $item_specification->description;
                }
                $description .= sprintf(" Specifications changed to <b>%s</b>.", implode("</b>, <b>", $specifications_changes));
            }
        }

        // update preferences
        if ($this->input->post("preference")) {
            $preferences = $this->input->post("preference");
        } else {
            $preferences = array();
        }

        $this->load->model('itempreference_model');
        $item_preferences = $this->itempreference_model->get_preferences($item_id);

        $preferences_changed = array_diff($preferences, $item_preferences) + array_diff($item_preferences, $preferences);

        if ($preferences_changed) {
            $this->itempreference_model->save_preferences($item_id, $preferences);

            if (empty($preferences)) {
                // special case, all specifications removed
                $description .= " All preferences were removed";
            } else {
                // tell what preferences we have now
                $preferences_desc = array();

                $item_preferences_names = $this->itempreference_model->get_preferences_names($item_id);
                foreach ($item_preferences_names as $category => $product) {
                    $preferences_desc[] = "<b>{$category}: {$product}</b>";
                }
                $description .= sprintf(" Preferences changed to %s", implode(', ', $preferences_desc));
            }
        }

        // update customOptions
        if ($this->input->post("customOption")) {
            $customOptions = $this->input->post("customOption");
        } else {
            $customOptions = array();
        }

        $this->load->model('itemoption_model');
        $item_options = $this->itemoption_model->get_options($item_id);

        $options_changed = array_diff($customOptions, $item_options) + array_diff($item_options, $customOptions);

        if ($options_changed) {
            $this->itemoption_model->save_options($item_id, $customOptions);

            if (empty($customOptions)) {
                // special case, all specifications removed
                $description .= " All options were removed";
            } else {
                // tell what preferences we have now
                $options_desc = array();

                $item_options = $this->itemoption_model->get_options($item_id);
                foreach ($item_options as $name => $value) {
                    $options_desc[] = "<b>{$name}: {$value}</b>";
                }
                $description .= sprintf(" Options changed to %s", implode(', ', $options_desc));
            }
        }

        // add new barcode
        if ($this->input->post('newBarcode')) {
            $this->load->model('barcode_model');
            $this->barcode_model->save(array(
                'item_id' => $item_id,
                'barcode' => $this->input->post('newBarcode'),
                'business_id' => $this->business_id,
            ));

            $description .= sprintf(" Added new barcode %s", $this->input->post('newBarcode'));
        }

        $user_id = get_employee_id($this->buser_id);

        if ($order_id && $this->input->post('add_to_order')) {
            $notes = $this->input->post('note') . "\n";
            $this->load->model('orderitem_model');

            $res = $this->orderitem_model->add_item(
                $order_id,                                // $order_id
                $item_id,                                 // $item_id
                $new_product_process->product_processID,  // $product_process_id
                1,                                        // $qty
                $this->employee_id,                       // $employee_id
                '',                                       // $supplier_id
                0,                                        // $price
                $notes,                                   // $notes
                1                                         // $autoOverwrite
            );
        }

        if (!empty($description)) {
            $this->load->model('itemhistory_model');
            $this->itemhistory_model->add($item_id, $description, $user_id);
        }

        if (empty($order_id)) {
            $message = "Updated item '$item_id'";
        } else {
            $message = "Updated item '$item_id' and order item in order '$order_id'";
        }

        if ($this->input->post('redirect')) {
            $redirect = $this->input->post('redirect');
        } else {
            $redirect = "/admin/orders/edit_item?orderID=$order_id&itemID=$item_id";
        }

        return array(
        	'status' => 'success',
            'message' => $message,
            'redirect' => $redirect,
        );
    }

    /**
     * Unloads an order from the delivery van
     * THere is no UI (view) for this, it redirects to order details page
     * updates the orderStatus and updates the order status to out for delivery (8)
     */
    public function unload($order_id = null)
    {
        if (empty($order_id)) {
            set_flash_message('error', "Missing order ID - Must be passed in the url");
            redirect("/admin/orders/view/");
        }

        $order = new Order($order_id);

        if (empty($order)) {
            set_flash_message('error', "Order not found");
            redirect("/admin/orders/details/".$order_id);
        }

        $order->loaded = 0;
        $order->orderStatusOption_id = 8; // Out for delivery
        $order->save();

        $employee = new Employee(get_employee_id($this->buser_id));

        $date = convert_to_gmt_aprax();

        $insert1 = "insert into orderStatus
        (order_id, orderStatusOption_id, date, customer_userName, employee_id, locker_id)
        values ('$order->orderID', 8, '".$date."', '$employee->username', '$employee->employeeID', '".$order->locker_id."');";
        $this->db->query($insert1);

        set_flash_message('success', "Order has been unloaded from van");
        redirect("/admin/orders/details/".$order_id);

    }


    /**
     * Unloads an order if the location is POS
     *
     */
    public function unload_conveyor($order_id = null)
    {

        $this->load->model('ConveyorOrderUnload_model');

        if (empty($order_id)) {
            set_flash_message('error', "Missing order ID - Must be passed in the url");
            redirect("/admin/orders/view/");
        }

        $order = $this->getOrderById($order_id);

        if (empty($order)) {
            set_flash_message('error', "Order not found");
            redirect("/admin/orders/details/".$order_id);
        }

        //check is order has been unloaded
        $unloadedOr = $this->ConveyorOrderUnload_model->get_aprax(array("order_id" => $order_id,"status" => "pending"));
        if (!empty($unloadedOr)) {
            set_flash_message('error', "There is a pending unload request for order ".$order_id."");
            redirect("/admin/orders/details/".$order_id);
        }

        $locker = new Locker($order->locker_id);

        $location = new Location($locker->location_id);
        if ($location->locationType_id != 12) {
            set_flash_message('error', "The selected order is not a POS order");
            redirect("/admin/orders/details/".$order_id);
        }

        //all ok, save it
        $this->ConveyorOrderUnload_model->set_unload(array(
            "order_id" => $order_id,
            "dateCreated" => gmdate('Y-m-d H:i:s'),
            "status" => "pending"
        ));

        set_flash_message('success', "The order ".$order_id." has been scheduled to be unloaded from the conveyor");
        redirect("/admin/orders/details/".$order_id);

    }

    /**
     * The following action is unknown
     */
    public function item_error()
    {
        $ids = $this->uri->uri_to_assoc(4);
        $order_id = $ids['order'];
        $barcode = $ids['barcode'];
        $error = $ids['error'];
        $item_customer_id = $ids['item'];
        $order_customer_id = $ids['order_customer'];

        //get the item_id from the barcode
        $this->load->model('barcode_model');
        $item = $this->barcode_model->get_one(array(
            "barcode" => $barcode,
            "business_id" => $this->business_id
        ));

        if (isset($_POST['submit'])) {
            //add to customer customer id and then redirect to edit item page
            if (!$this->input->post('item_id')) {
                set_flash_message('error', "Missing required fields");
                redirect($this->uri->uri_string());
            }

            $item_id = $this->input->post('item_id');
            $customer_id = $this->input->post('customer_id');

            $this->load->model('customer_item_model');
            $found_in_customer = $this->customer_item_model->get_one(array(
                'customer_id' => $customer_id,
                'item_id' => $item_id,
            ));

            if (!$found_in_customer) {
                $this->customer_item_model->save(array(
                    'customer_id' => $customer_id,
                    'item_id' => $item_id,
                ));

                // log customerItemHistory
                $this->customer_item_model->log_customer_item_history(array(
                    $item_customer_id,
                    $customer_id,
                    get_employee_id($this->session->userdata('buser_id')),
                    $barcode,
                    $order_id,
                    convert_to_gmt_aprax()
                ));
            }

            // remove item from the old customer closet
            $this->customer_item_model->delete_aprax(array(
                'customer_id' => $item_customer_id,
                'item_id' => $item_id,
            ));

            if ($order_id) {
                //add to order
                $this->add_new_item($order_id, $item_id);
            } else {
                set_flash_message('success', "Item '$barcode' has been re-assigned and added to order");
                redirect("/admin/orders/item_details/order/$order_id/item_id/$item_id");
            }
        }

        //need to get item customer and order customer


        switch ($error) {
            case "customer_mismatch":
                $this->load->model('customer_model');
                $this->customer_model->customerID = $item_customer_id;
                $this->customer_model->select = 'customerID, firstName, lastName';
                $customer_item = $this->customer_model->get();

                $this->customer_model->customerID = $order_customer_id;
                $this->customer_model->select = 'customerID, firstName, lastName';
                $order = $this->customer_model->get();


                $content['item_customer'] =  array($customer_item[0]->customerID,$customer_item[0]->firstName,$customer_item[0]->lastName);
                $content['order_customer'] = array($order[0]->customerID,$order[0]->firstName,$order[0]->lastName);
                $content['message'] = "<div class='warning'><div style='width:70px;float:left;'><img align='absmiddle' src='/images/icons/alert.png' /></div>
                        This item is assigned to <a target='_blank' href='/admin/customers/detail/".$customer_item[0]->customerID."'>". getPersonName($customer_item[0]) ."</a>, Do you want to attach it to <a  target='_blank' href='/admin/customers/detail/".$order[0]->customerID."'>".getPersonName($order[0])."</a> account?
                                <br style='clear:left'>
                                </div>";
                break;
        }
        $content['item_id'] = $item->item_id;
        $content['error'] = $error;

        $this->setPageTitle('Create Item Error');
        $this->renderAdmin('create_item_error', $content);
    }

    /**
     * Deletes an item from an order.
     * There is no UI for this, it only redirects tht the order detail page
     *
     * expects url segment 4 to be the order_id
     * expects url segment 5 to be the orderItemID
     *
     * @throws Exception for missing orderItemID
     *
     */
    public function delete_item($order_id = null, $orderItemID = null)
    {
        if (empty($orderItemID)) {
            set_flash_message('error', 'Missing orderItemID');
            redirect_with_fallback("/admin/orders/details/".$order_id);
        }

        $this->load->model('orderitem_model');
        $orderitem = new OrderItem($orderItemID, false);
        $affected_rows = $this->orderitem_model->delete_item($orderItemID, $this->employee->employeeID);

        if ($affected_rows) {
            //remove assambly notes in the order of the item
            $order = new Order($order_id, false);
            if($order->postAssembly != ''){
                $assemblyNotes = $order->postAssembly.' ';
            }else{
                $assemblyNotes = '';
            }
            $this->load->model("barcode_model");
            $barcode = $this->barcode_model->get_one(array(
                "item_id" => $orderitem->item_id,
            ));
            $regex = "/".$barcode->barcode."\[(.*?)\]/i";

            $newAssemblyNotes = preg_replace($regex, '', $order->postAssembly);
            $where['orderID'] = $order_id;
            $options['postAssembly'] = trim($newAssemblyNotes);
            $this->order_model->update($options, $where);

            set_flash_message("success", get_translation("item_was_deleted_from_order", "admin/orders/details", array(), "Item was deleted from order"));
        } else {
            set_flash_message("error", get_translation("item_was_not_deleted_from_order", "admin/orders/details", array(), "Item was NOT deleted from order"));
        }

       redirect("/admin/orders/details/".$order_id);
    }


    public function bag_tags()
    {
        $content['business_id'] = $this->business_id;

        $bagNumber = $this->input->get("bagNumber");

        if (!empty($bagNumber) && !is_numeric($bagNumber)) {
            set_flash_message('error', get_translation("bag_number_must_be_numeric", "admin/orders/bag_tags", array("bagNumber"=>$bagNumber), "Invalid bag number '%bagNumber%'"));
            $this->goBack("/admin/orders/bag_tags");
        }

        if ($bagNumber) {
            $bagNumber = trim($bagNumber);
            $bagNumber= ltrim($bagNumber, "0");
            //find out if it is WF or DC
            $sqlGetOrder = "select orderID, firstName, lastName, bagNumber, phone, orders.customer_id, orders.dateCreated
            from orders
            join bag on bagID = orders.bag_id
            join customer on customerID = orders.customer_id
            where bagNumber = ?
            and orders.business_id = ?
            order by orderID desc";
            $query = $this->db->query($sqlGetOrder, array($bagNumber, $this->business_id));
            $order = $query->result();

            if (count($order) > 1) { //this bag number has been used before.  Only reason it would be scanned is if it was returned without a permanent tag.
                $content['notice'] = get_translation("bag_number_has_been_used_before", "admin/orders/bag_tags", array("bagNumber"=>$bagNumber), "Bag %bagNumber% has been used before.  Why does it have a blank tag?  Give this bag to the manager.<br><br>");
                //show all orders
                $sqlAllOrders = "select orderID, customerID, firstName, lastName, bagNumber, phone, orders.customer_id, dateCreated, internalNotes, customerNotes
                from orders
                join customer on customerID = orders.customer_id
                join bag on bagID = orders.bag_id
                where bagNumber = ?
                and orders.business_id = ?";
                $query = $this->db->query($sqlAllOrders, array($bagNumber, $this->business_id));
                $content['allOrder'] = $allOrder = $query->result();

            } elseif ($order[0]->customer_id == '0') {
                $content['notice'] = get_translation("unknown_customer", "admin/orders/bag_tags", array(), "We do not know who the customer is yet.  Keep temporary tag on bag.");
            } else {
                if ($order[0]) {
                    $current_order = new Order($order[0]->orderID); //This is the current order
                    $customer = new Customer($current_order->customer_id);

                    $phone =  preg_replace("/[^0-9,.]/", "", $customer->phone);
                    $this->load->model('order_model');
                    $ordType = $current_order->getOrderType($current_order->orderID);
                    //now find out if this customer had any of the same type of order before

                    $current_order->dateCreated->setTimeZone(new DateTimeZone("GMT"));

                    $getPastOrders = "select *
                    FROM  orders
                    WHERE  customer_id = '{$current_order->customer_id}'
                    AND dateCreated != '{$current_order->dateCreated->format("Y-m-d H:i:s")}'
                    AND orders.business_id = $this->business_id
                    AND orders.bag_id != 0
                    ORDER BY dateCreated DESC;";

                    $query = $this->db->query($getPastOrders);
                    $pastOrders = $query->result();
                    if (count($pastOrders) < 50) {
                        $newBag = 1;
                        foreach ($pastOrders as $p) {
                            $past_order = new Order($p->orderID);
                            if ($past_order->dateCreated instanceof DateTime) {
                                $past_order_date = $past_order->dateCreated->format('m/d/y');
                            } else {
                                $past_order_date = "";
                            }
                            if ($ordType == $this->order_model->getOrderType($past_order->orderID)) {
                                $content['notice'] = "";
                                $content['notice'] = get_translation(
                                    "print_anyway", 
                                    "admin/orders/bag_tags", 
                                    array(
                                        "link1"           =>"<a href='/admin/customers/detail/{$customer->customerID}' target='_blank'> " . getPersonName($customer) . " </a>",
                                        "ordType"         =>$ordType,
                                        "past_order_date" => $past_order_date,
                                        "past_order_id"   => $past_order->orderID,
                                        "link2"           => "print anyway",
                                        "link3"           => "print anyway (With default location)",
                                        "customerID"      => $customer->customerID,
                                        "bagNumber"       => urlencode($bagNumber),
                                        "phone"           => urlencode($phone)
                                    ), 
                                    "%link1% has already had a %ordType% order on %past_order_date% Order ID: %past_order_id% Do not give new bag <br><br><a href='/admin/admin/print_bag_tag/%customerID%/%bagNumber%/%phone%'>%link2%</a><br><br><br><a href='/admin/admin/print_bag_tag/%customerID%/%bagNumber%/%phone%/with-default-location'>%link3%</a>"
                                );
                                $newBag = 0;
                                break;
                            }
                        }
                        if ($newBag == 1 || get_customer_meta($customer->customerID, "assign_new_bag_on_new_order")) {
                            $content['notice'] = '<a href="/admin/admin/print_bag_tag/'.$customer->customerID . '/'.urlencode($bagNumber).'/'.urlencode($phone).'">'.get_translation("Print New Tag For Customer", "admin/orders/bag_tags", array("personName"=>getPersonName($order[0]), "ordType"=>$ordType), 'Print New Tag</a> for %personName%.  %$ordType% Order').'<br><br>'.
                                     '<br /><a href="/admin/admin/print_bag_tag/'.$customer->customerID . '/'.urlencode($bagNumber).'/'.urlencode($phone).'/with-default-location">'.get_translation("Print New Tag For Customer With Default Location", "admin/orders/bag_tags", array("personName"=>getPersonName($order[0]), "ordType"=>$ordType), 'Print New Tag (With default location)</a> for %personName%.  %ordType% Order').'<br><br>';
                            $content['redirect'] = '/admin/admin/print_bag_tag/'. $customer->customerID .'/'.urlencode($bagNumber).'/'.urlencode($phone);
                        }
                    } else {
                        $content['notice'] = get_translation(
                            "Do not give new bag for this order", 
                            "admin/orders/bag_tags", 
                            array(
                                "ordType"       =>$ordType,
                                "personName"    =>getPersonName($order[0]), 
                                "customerID"    =>$customer->customerID,
                                "phone"         =>urlencode($phone),
                                "bagNumber"     =>urlencode($bagNumber)
                                
                            ), 
                            'Do not give new bag for this %ordType% order.  %personName% has used us over 50 times.<br><br><a href="/admin/admin/print_bag_tag/%customerID%/%bagNumber%/%phone%">print anyway</a><br /><br /><br /><a href="/admin/admin/print_bag_tag/%customerID%/%bagNumber%/%phone%/with-default-location">print anyway (With default location)</a>'
                            );
                    }
                } else {
                    $content['notice'] = get_translation(
                            "No record found for bag number", 
                            "admin/orders/bag_tags", 
                            array(
                                "bagNumber" => $bagNumber    
                            ), 
                            'No record found for bag number %bagNumber%<br>'
                            );
                    //see if this bag is in the db though
                    $getBag = "select bag.notes, customerID, firstName, lastName, phone, customerID
                    from bag
                    join customer on customerID = customer_id
                    where bagNumber = '$bagNumber'
                    and customer.business_id = $this->business_id";
                    $query = $this->db->query($getBag);
                    $bag = $query->result();

                    if ($bag) {
                        $content['notice'] .= get_translation(
                            "It is registered to customer", 
                            "admin/orders/bag_tags", 
                            array(
                                "customerID" => $bag[0]->customerID,
                                "personName" => getPersonName($bag[0]),
                                "bagNumber"  => $bagNumber,
                                "phone"      => urlencode($bag[0]->phone)
                            ), 
                            '<br>It is registered to <a href="/admin/customers/detail/%customerID%" target="_blank">%personName%</a><br><br><a href="/admin/admin/print_bag_tag/%customerID%/%bagNumber%/%phone%">print anyway</a><br /><br /><br /><a href="/admin/admin/print_bag_tag/%customerID%/%bagNumber%/%phone%/with-default-location">print anyway (With default location)</a>'
                            );
                    }
                }
            }
        }

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

        $this->setPageTitle('Print Bag Tags');
        $this->renderAdmin('bag_tags', $content);
    }

    /**
     * Renders the specified business logo for bag tags.
     */
    public function print_tag_logo()
    {
        $this->load->model("image_placement_model");
        $bagTag_image = $this->image_placement_model->get_by_name("bagTag", $this->business_id);
        if (!$bagTag_image) {
            set_flash_message("error", "Could not print tag logo: No bag tag image set for business.");
            return $this->goBack();
        }
        $content['bagTag_logo_url'] = BUSINESS_IMAGES_DIRECTORY . $bagTag_image->filename;

        $this->load->view('print/tag_logo', $content);
    }

    /**
     * This is used to take a bag barcode and get the orderID, then redirect to the order details page.
     * Note, any leading zero in the bag nubmer is stripped.
     * Expects the Following GET parameter:
     *  bag_number : The bag number property of the bag
     */
    public function bag_barcode_details($oder_id = 0, $bag_number = null)
    {
        //this is only used by bubble & stitch
        $this->load->helper('barcode_helper');
        $bag_number = parse_barcode($bag_number);

        // Order_id must be segment 4 in the url. In this case I set segment 4 to 0 since there is no order
        $bag_number = (int) $bag_number;

        // In case the bag_number is sent as a querystring variable
        if (empty($bag_number)) {
            $bag_number = (int) $_GET['bag_number'];
        }

        if (empty($bag_number)) {
            set_flash_message("error", "Bag number must be passed as segment 5 of the URL or as the GET parameter 'bag_number'");
            redirect_with_fallback("/admin/orders");
            return;
        }

        $content['bagNumber'] = $bag_number;
        //check to see if there is a claim for the customer that owns this bag
        $this->load->model('bag_model');
        $bag = $this->bag_model->get_one(array(
            'bagNumber' => $bag_number,
            'business_id' => $this->business_id
        ));

        if (empty($bag)) {
            set_flash_message("error", get_translation("bag_not_found", "admin/orders/quick_scan_bag", array(), "Bag not found"));
            redirect_with_fallback("/admin/orders");
            return;
        }

        if ($bag->business_id != $this->business_id) {
            set_flash_message("error", get_translation("bag_not_belong_to_business", "admin/orders/quick_scan_bag", array("business_id"=>$this->business_id), 'Bag does not belong to the current business_id [%business_id%]'));
            redirect_with_fallback("/admin/orders/quick_scan_bag");
            return;
        }

        if ($bag->customer_id != 0) {
            $customer = Customer::search(array(
                'customerID' => $bag->customer_id,
            ));

            if (empty($customer)) {
                set_flash_message("error", get_translation("customer_not_found", "admin/orders/quick_scan_bag", array("bag_number"=>$bag_number, "customer_id"=>$bag->customer_id), 'Bag %bag_number% belongs to a customer, but customer %customer_id% was not found'));
                redirect_with_fallback("/admin/orders/quick_scan_bag");
            }

            $content['customer'] = $customer;

            $claim = Claim::search(array(
                'customer_id' => $bag->customer_id,
                'business_id' => $this->business_id,
                'active' => 1,
            ));

            if ($claim) {
                $content['customer'] = $customer;
                try {
                    $locker = new Locker($claim->locker_id);
                    $content['claim'] = $claim;
                } catch (NotFound_Exception $e) {
                    $claim->delete();
                }
            } else { //customer is found but there is no claim
                $content['notice2'] = get_translation("no_claim_for_customer", "admin/orders/quick_scan_bag", array("customer_id"=>$customer->customerID, "first_name"=>$customer->firstName, "last_name"=>$customer->lastName), '<br><br>This bag is registered to <a href="/admin/customers/detail/%customer_id%">%first_name% %last_name%</a>.  Check their default location.');
            }

            if (!empty($locker)) {
                $content['location'] = new Location($locker->location_id);
                $content['locker'] = $locker;
            }
        }

        // find what order this bag is tied to
        $sqlGetOrder = "SELECT orderID, firstName, orders.notes, hold, customerID,
            customerNotes, internalNotes, orderStatusOption_id, dateCreated
        FROM orders
        JOIN bag on bagID = orders.bag_id
        LEFT JOIN customer on customerID = orders.customer_id
        WHERE
            bagNumber = ?
            AND bag.business_id = ?
            AND orderStatusOption_id != 10;";

        $query = $this->db->query($sqlGetOrder, array((string)$bag_number, $this->business_id));

        $order = $query->result();
        if ($order) {
            //make sure this bag belongs to this business
            if ($bag->business_id != $this->business_id) {
                $this->session->set_flashdata("error", get_translation("bag_not_belong_to_business", "admin/orders/quick_scan_bag", array(), 'That bag does not belong to this business.'));
                set_flash_message("error", get_translation("bag_not_belong_to_business", "admin/orders/quick_scan_bag", array(), 'That bag does not belong to this business.'));
                redirect('/admin/orders/quick_scan_bag');
            }

            foreach ($order as $o) {
                if ($o->hold == 1) {
                    $content['title'] = get_translation("account_on_hold", "admin/orders/quick_scan_bag", array("customer_id"=>$o->customerID), 'This <a href="/admin/customers/detail/%customer_id%" target="_blank">customer\'s account</a> is on hold.  Give to customer service');
                    return $this->adminError($content);
                }

                if (!$o->firstName) {
                    $content['title'] = get_translation("not_assigned_to_customer_title", "admin/orders/quick_scan_bag", array(), 'STOP!  This order is not assigned to a customer!');
                    $content['message'] = get_translation("not_assigned_to_customer_message", "admin/orders/quick_scan_bag", array("order_id"=>$o->orderID), 'Please check the bag and look every where to figure out who it is for.  Then <a href="/admin/orders/details/%order_id%">reassign the order.</a><br><br>If you absolutely can not figure out who it is for, you may <a href="/admin/orders/details/%order_id%">enter the order with no customer</a>.');
                    return $this->adminError($content);
                } else {
                    redirect("/admin/orders/details/{$order[0]->orderID}?new_order=true");
                }
            }
        } else {
            checkAccess('add_order');

            //if no open order is found, find the customers last order
            $content['notice'] = get_translation("no_open_order_for_bag", "admin/orders/quick_scan_bag", array("bag_number"=>$bag_number), 'There is no open order for bag %bag_number%.');
            //FIXME: hardcoded locationType name, locationType is used in the view
            $content['pastOrders'] = $this->order_model->getOrdersByBag($bag_number, $this->business_id);

            $this->setPageTitle('Create Order');
            $this->renderAdmin('create_order', $content);
        }
    }

    /**
     * The following function is unknown  .

     * -----------------------------------------
     * BEGIN
     * -----------------------------------------

     * Expects the following post parameters:
     * barcode
     * order_id
     *  customer_id

     * -----------------------------------------
     * END
     * -----------------------------------------
     */
    public function add_item_to_order()
    {
        // make sure we have the required fields
        $this->load->library("form_validation");
        $this->form_validation->set_rules("order_id", "Order ID", "numeric|required");
        $this->form_validation->set_rules("barcode", "Barcode", "required|callback_validate_barcode_length");

        $order_id = $this->input->post("order_id");
        $barcode = $this->input->post("barcode");

        if ($this->form_validation->run()) {
            $this->load->model('barcode_model');
            $items = $this->barcode_model->getBarcode($barcode, $this->business_id);

            $order = new Order($order_id);

            // redo orders have a blank customer id so when adding items the barcode will belong
            // to a different customer. We need to have a different process for adding items to redo orders
            if ($order->llNotes == REDO_ORDER) {
                $order->addRedoItem($items[0], $this->business_id);
                redirect("/admin/orders/details/$order_id");
            }

            //The following conditional displays the edit item interface if the barcode already exists in the system.
            if ($items) {
                foreach ($items as $item) {
                    if (($item->customerID == $order->customer_id) || ($item->customer_id==0 && $order->customer_id == 0)) {
                        $this->add_new_item($order->orderID, $item->itemID);

                        return;
                    }
                }
                //If we can't find an item that is already associated with the customer, we render the interface for reassigning the item.
                redirect("/admin/orders/item_error/order/".$order->orderID."/barcode/".$barcode."/error/customer_mismatch/item/".$items[0]->customerID."/order_customer/". $order->customer_id);
            }
            //Otherwise, the conditonal displays the create item interfac.
            else {
                //if all good, echo success for a redirect
                redirect("/admin/orders/create_item/order/$order_id/barcode/$barcode");
            }
        } else {
            set_flash_message("error", validation_errors());
            redirect("/admin/orders/details/$order_id");
        }
    }

    /**
     * Searches or displays all claimed orders in the system.
     * Can take the following POST parameters for searching claims
     *   firstName string The customer's first name
     *   lastName string  The customer's last name
     *   address string The claim's address
     */
    public function claimed()
    {
        $firstName = trim($this->input->post("firstName"));
        $lastName = trim($this->input->post("lastName"));
        $address = trim($this->input->post("address"));

        $this->load->model('servicetype_model');
        $serviceTypes = $this->servicetype_model->get_formatted();

        $this->load->model('claim_model');
        $claims = $this->claim_model->search($firstName, $lastName, $address, $this->session->userdata['business_id']);

        foreach ($claims as $index => $claim) {
            if ($claim->orderType) {
                $str = '';
                $type = unserialize($claim->orderType);
                if (is_array($type)) {
                    foreach ($type as $key => $value) {
                        if (array_key_exists($value, $serviceTypes)) {
                            $str .=  substr($serviceTypes[$value]['slug'],0,5)."<br>";
                        }
                    }
                } else {
                    $str = "SMS";
                }

                $content['ordType'][$claim->claimID] = rtrim($str, "<br>");
            }
        }
        $content['claims'] = $claims;
        //The following variables store the results of the user's search, so as to populate the search fields with the user's previous search values whne the view is rendered.
        $content['firstName'] = $firstName;
        $content['lastName'] = $lastName;
        $content['address'] = $address;

        $this->setPageTitle('Claimed Orders');
        $this->renderAdmin('/admin/claim/view', $content);
    }

    //no order was there to pickup
    /**
     * The following action set a claim to inactive and sends either a No Order Home Delivery or No Order e-mail to the customer associated with the claim.
     */
    public function claimed_noOrd($claimID = null)
    {
        if (!is_numeric($claimID)) {
            set_flash_message("error","The claim ID must be passed as segment 4 of the URL.");
        } else {
            //get details
            $getClaim = "select customerID, firstName, email, claim.updated, claimID FROM customer LEFT JOIN  claim ON customer_id = customerID WHERE claimID = ?";
            $claimInfo = $this->db->query($getClaim, array($claimID))->row();
            if (empty($claimInfo)) {
                set_flash_message('error', "Could not find claimed order");
            } else {
                $claim = new Claim($claimID, true);
                // The following query inactives the claim
                $claim->active = 0;
                $claim->save();

                //The following statements e-mail the customer to inform them that their pickup order was not found at the location.
                $parameters = array('customer_id' => $claimInfo->customerID, 'business_id' => $this->business_id);

                if (isset($claim->relationships['Locker'][0]->location_id)) {
                    $location = new Location($claim->relationships['Locker'][0]->location_id, true);
                }

                // The following conditional adds a 5 dollar charge and sends the No Order Home Delivery e-mail template if the location type is home deliverly.
                // If the location type is not home delivery, then the No Order e-mail template is sent to the customer.
                if (isset($location) && $location->relationships['LocationType'][0]->name == "Home Delivery") {
                    $customerDiscount = new CustomerDiscount();
                    $customerDiscount->customer_id = $claimInfo->customerID;
                    $customerDiscount->amount = -$location->no_order_fee;
                    $customerDiscount->amountType = "dollars";
                    $customerDiscount->frequency = "one time";
                    $customerDiscount->description = "Missed Pickup";
                    $customerDiscount->business_id = $this->business_id;
                    $customerDiscount->extendedDesc = "No order was left for claim {$claimInfo->claimID}";
                    $customerDiscount->active = 1;
                    $customerDiscount->origCreateDate = new DateTime();
                    $customerDiscount->updated = new DateTime();
                    $customerDiscount->expireDate = NULL;
                    $customerDiscount->save();
                    Email_Actions::send_transaction_emails(array("do_action" => "no_order_home", "customer_id" => $claimInfo->customerID, "business_id" => $this->business_id, "claim_id" => $claimID));
                    set_flash_message('success', "On Demand Home Delivery order cancelled and customer charged missed pickup fee");
                } else {
                    set_flash_message('success', "Order Cancelled and No Order email sent to customer");
                    Email_Actions::send_transaction_emails(array("do_action" => "no_order", "customer_id" => $claimInfo->customerID, "business_id" => $this->business_id, "claim_id" => $claimID));
                }
            }
        }


        redirect($this->agent->referrer());
    }

    //rescedule an order for today
    public function claimed_today($claimID = null)
    {
        //get a new time of one hour before their cut off time
        $newDay = local_date($this->business->timezone, '', 'm/d/y');
        $newTime = get_business_meta($this->business->businessID, 'cutOff');
        $newDate = $newDay .' '.$newTime;
        $newDate = strtotime($newDate .'-1 hour');
        $newDate = date("Y-m-d H:i:s", convert_to_gmt($newDate, $this->business->timezone));

        $update1 = "update claim
        set updated = '$newDate'
        where claimID = ".$claimID."
        and business_id = $this->business_id;";
        $update = $this->db->query($update1);
        if($update)
            set_static_message('success', "Order Pickup rescheduled for today");
        else
            set_static_message('error', "Error rescheduling order");
        redirect('/admin/orders/claimed');
    }

    public function claimed_delete($claimID = null)
    {
        $this->load->model('claim_model');

        try {
            if ($homePicukp = OrderHomePickup::search(array('claim_id' => $claimID))) {
                $homePicukp->cancel();
            }
        } catch (Exception $e) {
            send_exception_report($e);
            send_admin_notification("Error deactivating home pickup for claim #" . $claimID, $e->getMessage());
        }

        if($this->claim_model->deactivate_claim($claimID)) {
            set_flash_message('success', "Order Pickup Cancelled");
        } else {
            set_flash_message('error', "Error cancelling order");
        }

        redirect('/admin/orders/claimed');
    }


    /**
     * This is made to simulate a pickup from a locker
     */
    public function pickup()
    {
        $this->load->model('locker_model');
        $this->load->model('location_model');

        if (isset($_POST['submit'])) {

            // set up the vars
            $this->load->model('order_model');
            $locker_id = $_POST['locker_id'];
            $location_id = $_POST['location_id'];
            $bagNumber = $_POST['bag_number'];
            $employee_id = get_employee_id($this->session->userdata('buser_id'));

            //if the locker is empty, it is probably a counter dropoff. Try to get it from location_id
            if ($locker_id == '') {
                if ($lockers = $this->locker_model->get(array('location_id'=>$location_id))) {
                    $locker_id = $lockers[0]->lockerID;
                } else {
                    set_flash_message('error', "Can't find the locker ID for this location");
                    redirect('/admin/orders/pickup');
                }

            }


            // get the claim for this location and locker
            $this->load->model('customer_locker_model');
            $claim = $this->customer_locker_model->get_claim_by_location($location_id, $locker_id);

            if ($id = $this->order_model->new_order($location_id, $locker_id, $bagNumber, $employee_id, $notes, $this->business_id, $claim)) {
                set_flash_message('success', "Order: ".$id." has been created");
                redirect('/admin/orders/process_order/'.$bagNumber);
            } else {
                set_flash_message('error', "Error with order insert");
                redirect('/admin/orders/pickup');
            }

        }

        $this->template->add_js('js/new_order.js');

        $options['business_id'] = $this->business_id;
        $options['status'] = 'active';
        $locations = $this->location_model->get($options);

        $location_array = array();
        foreach ($locations as $c) {
            $location_array[$c->locationID] = $c->address;
        }
        natsort($location_array);
        $content['locations'] = $location_array;

        $this->setPageTitle('Simulate a Pickup');
        $this->renderAdmin('pickup', $content);
    }

    public function add_charge_to_order($order_id = null)
    {
        if (!is_numeric($order_id)) {
            set_flash_message('error', 'Order ID must be numeric.');
        }
        if (!$this->input->post('charge_amount')) {
            set_flash_message('error', 'Missing charge amount');
        } elseif (!$this->input->post('charge_type')) {
            set_flash_message('error', 'Missing charge amount');
        } else {
            $this->load->model('ordercharge_model');
            $this->ordercharge_model->order_id = $order_id;
            $this->ordercharge_model->chargeType = $_POST['charge_type'];
            $this->ordercharge_model->chargeAmount = str_replace(',','.',$_POST['charge_amount']);
            $insert_id = $this->ordercharge_model->insert();
            if ($insert_id) {
                $note = "Added Charge/Discount: '{$_POST['charge_type']}', Amount: {$_POST['charge_amount']}";
                $this->logger->set('orderStatus', array('order_id'=>$order_id,'locker_id'=>get_order_locker($order_id),'employee_id'=>get_employee_id($this->session->userdata('buser_id')), 'orderStatusOption_id'=>current_order_status($order_id), 'note'=>$note));
                set_flash_message('success', 'Charge has been added');
            } else {
                set_flash_message('error', 'Charge has not been added');
            }
        }
        redirect("/admin/orders/details/".$order_id);
    }

    public function delete_charge_from_order($charge_id = null, $order_id = null)
    {
        if (!$charge_id || !$order_id) {
            set_flash_message('error', "Missing orderID");
            redirect("/admin/orders/view");
        }


        $this->load->model('ordercharge_model');
        $this->ordercharge_model->clear();
        $this->ordercharge_model->orderChargeID = $charge_id;
        $charge = $this->ordercharge_model->get();
        $type = $charge[0]->chargeType;
        $amount = $charge[0]->chargeAmount;
        $this->ordercharge_model->delete();

        if ($charge[0]->customerDiscount_id > 0) {
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->customerDiscountID = $charge[0]->customerDiscount_id;
            $result = $this->customerdiscount_model->get();

            if (!empty($result)) {
                $result = $result[0];
                if ($result->amountType == "product_plan") {
                    //delete product plan discount percent if customer has one
                    $sql = "DELETE FROM customerDiscount WHERE customerDiscountID > ".$charge[0]->customerDiscount_id." AND active = 1 AND amountType = 'product_plan_percent' AND customer_id = ".$result->customer_id."";
                    $this->db->query($sql);
                }
            }

            $this->customerdiscount_model->where = array('customerDiscountID'=>$charge[0]->customerDiscount_id);
            $this->customerdiscount_model->active = 1;

            if(!$affected_rows = $this->customerdiscount_model->update())
                send_admin_notification("ISSUE ALERT: Charge removed from order, but discount not reset to active in customerDiscounts",
                        'while trying to remove all charges and reapply, this charge was removed from order. The charge was not updated to active again, so this charge will no be reapplied.<br><br>'. print_r($charge[0], 1));
        }

        $note = "Removed Charge/Discount: '{$type}', Amount: {$amount}";
        $this->logger->set('orderStatus', array('order_id'=>$order_id,'locker_id'=>get_order_locker($order_id),'employee_id'=>get_employee_id($this->session->userdata('buser_id')), 'orderStatusOption_id'=>current_order_status($order_id), 'note'=>$note));
        set_flash_message('success', "Charge has been deleted");
        redirect("/admin/orders/details/".$order_id);

    }

    public function update_order_location($location_id = null, $locker_id = null, $order_id = null)
    {
        if ($this->order_model->update(array('locker_id'=>$locker_id), array('orderID'=>$order_id))) {
            $note = "Changed location and locker";
            $this->logger->set('orderStatus', array('order_id'=>$order_id,'locker_id'=>$locker_id,'employee_id'=>get_employee_id($this->session->userdata('buser_id')), 'orderStatusOption_id'=>current_order_status($order_id), 'note'=>$note));
            set_flash_message("success", "Updated location and locker");
        } else {
            set_flash_message("error", "Error updating location and locker");
        }

        redirect('/admin/orders/details/'.$order_id);
    }


    /**
     * Updates an order's bag
     *
     * If business_id is not passed, the session business_id is used.
     *
     * @param int business_id Optional
     * @throws Exception missing business id
     * @throws Exception missing order id
     * @throws Exception missing bag id
     * @throws Exception Bag does not belong to business
     *
     */
    public function update_order_bag($order_id, $bag_id)
    {
        $bag = Bag::search(array(
            'bagID' => $bag_id,
            'business_id' => $this->business_id
        ));

        if ($bag_id && !$bag->bagID) {
            set_flash_message('error', "Bag not found");
            $this->goBack("/admin/orders/details/$order_id");
        }

        $updated = $this->order_model->update(array(
            'bag_id' => $bag_id
        ), array(
            'orderID' => $order_id,
            'business_id' => $this->business_id
        ));

        // do the update, make the note for the order history
        if ($updated) {
            $bagNumber = $bag ? $bag->bagNumber : 'N/A';
            $note = "Changed Bag for order to bagID: $bagNumber";

            $this->logger->set('orderStatus', array(
                'order_id' => $order_id,
                'locker_id' => get_order_locker($order_id),
                'employee_id' => get_employee_id($this->session->userdata('buser_id')),
                'orderStatusOption_id' => current_order_status($order_id),
                'note' => $note
            ));

            set_flash_message("success", "Updated The Bag for this Order");
        } else {
            set_flash_message("error", "Error updating order bag");
        }

        $this->goBack("/admin/orders/details/$order_id");
    }

    /**
     * calls order_model->update_order which does a lot
     */
    public function update_order_status($order_id = null, $orderStatusOption_id = null)
    {
        if (!is_numeric($order_id)) {
            set_flash_message("error", "'order_id' must be numeric and passed as segment 4 in the URL. ");
            return $this->goBack("/admin/orders");
        }

        $this->load->model("order_model");
        $order = $this->order_model->get_by_primary_key($order_id);

        if (empty($order)) {
            set_flash_message("error", "Order '{$order_id}' does not exist");
            return $this->goBack("/admin/orders");
        }

        if ($order->business_id != $this->business_id) {
            set_flash_message("error", "This order does not belong to your business.");
            return $this->goBack("/admin/orders");
        }

        $options['order'] = $order;
        $options['orderStatusOption_id'] = $orderStatusOption_id;
        $options['locker_id'] = $order->locker_id;
        $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
        $response = $this->order_model->update_order($options);
        set_flash_message($response['status'], $response['message']);

        redirect("/admin/orders/details/".$order_id);
    }

    /**
     * TODO: the view in this function seems deprecated and is not referenced anywhere
     *
     * created for access from orders/details
     *
     * POST variables
     * ------------------
     * customer_id
     * customer (full name)
     * bag_id
     * location (address)
     * location_id
     * locker_id
     * locker_name
     */
    public function reassign_order($order_id = null)
    {
        if (isset($_POST['reassign_submit'])) {

            // get the order...
            // get the old customer information from the order
            $options['orderID'] = $order_id;
            $options['with_customer'] = true;
            $options['with_location'] = true;
            $options['business_id'] = $this->business_id;
            $options['select'] = "bag_id, customerID, locationID, lockerID, firstName, lastName, location.address, lockerName, locker_id, orders.llNotes, orderStatusOption_id";
            $order = $this->order_model->get($options);
            if ($this->db->_error_message()) {
                throw new Exception($this->db->_error_message());
            }
            if (!$order) {
                set_flash_message('error', "Could not find order");
                redirect($this->uri->uri_string());
            }
            // make it easier to use
            $order = $order[0];

            $old_customerID = $order->customerID;

            //get the new customer for this order
            $this->load->model('customer_model');
            $this->customer_model->customerID = $_POST['customer_id'];
            $customer = $this->customer_model->get();
            $customer= $customer[0];

            $employee_name = get_employee_name($this->buser_id);

            // get the new locker
            $this->load->model('locker_model');
            $locker = $this->locker_model->get(array('lockerID'=>$order->lockerID));
            $locker = $locker[0];


            /**
             * Replace the old note system with the new
             */
            $this->load->model('orderstatus_model');
            $this->orderstatus_model->order_id = $order_id;
            $this->orderstatus_model->note = 'Reassigned from '.  getPersonName($order) . '/' .$order->address.'/'.$order->lockerName.' to '.  getPersonName($customer) .'/'.$_POST['location'].'/'.$locker->lockerName.' by '.$employee_name;
            $this->orderstatus_model->employee_id = get_employee_id($this->session->userdata('buser_id'));
            $this->orderstatus_model->orderStatusOption_id = $order->orderStatusOption_id;
            $this->orderstatus_model->locker_id = $order->lockerID;
            $this->orderstatus_model->insert();

            //get the old notes, if any
            //$oldNote = ($order->llNotes!='')? $order->llNotes.' / ':"";

            //set the updated note
            //$newNote = $oldNote . 'Reassigned from '.$order->firstName." ".$order->lastName. '/' .$order->address.'/'.$order->lockerName.' to '.$customer->firstName." ".$customer->lastName.'/'.$_POST['location'].'/'.$locker->lockerName.' by '.$employee_name.' @ '.date("m/d/y h:i:s");
            //die($newNote);

            $options = array();
            $options['llnotes'] = '';
            $options['locker_id'] = $order->lockerID;
            $options['customer_id'] = $_POST['customer_id'];
            $where['orderID'] = $order_id;
            $this->order_model->update($options, $where);

            //reassign the bag to the new user
            $this->load->model('bag_model');
            $this->bag_model->where = array('bagID'=>$order->bag_id);
            $this->bag_model->customer_id = $_POST['customer_id'];
            $this->bag_model->update();

            //assign all the items in the order to the new customer
            $this->load->model('orderitem_model');
            $this->orderitem_model->order_id = $order_id;
            $items = $this->orderitem_model->get();
            if ($items) {
                $this->load->model('customer_item_model');
                foreach ($items as $i) {
                    $this->customer_item_model->insert_ignore($_POST['customer_id'], $i->item_id);

                    // remove items from the old customer closet
                    $this->customer_item_model->delete_aprax(array(
                        'customer_id' => $old_customerID,
                        'item_id' => $i->item_id,
                    ));
                }
            }

            if (!empty($_POST['claim_id'])) {
                $this->load->model('claim_model');
                $claim = $this->claim_model->get_aprax(array('claimID' => $_POST['claim_id']));
                $claim = $claim[0];
                if ($claim) {
                    $this->order_model->update(array('notes' => $claim->notes), array('orderID' => $order_id));
                }
                $this->claim_model->deactivate_claim($_POST['claim_id']);
            }

            if ($_POST['AJAX']) {
                die("success");
            }

            set_flash_message("success", "Order has been Re-assigned to " . getPersonName($customer));
            redirect("/admin/orders/details/".$order_id);

        }

        $this->template->add_js("/js/search.js");
        $this->setPageTitle('Reassign Order');
        $this->renderAdmin('reassign_order', $content);
    }

    /**
     * TODO: this function seems deprecated and is not referenced anywhere
     */
    public function search_orders()
    {
        $this->setPageTitle('Search Orders');
        $this->renderAdmin('search_orders', $content);
    }

    /**
     *
     * @throws Exception
     */
    public function set_postAssembly_notes()
    {
        try {
            if (!$this->input->post("orderID")) {
                throw new Exception("'orderID' must be passed as a POST parameter.");
            }
            $orderID = $this->input->post("orderID");
            if (!is_numeric($orderID)) {
                throw new Exception("'orderID' must be numeric.");
            }
            if (!$this->input->post("postAssembly")) {
                throw new Exception("'postAssembly' must be passed as a POST parameter.");
            }
            $postAssembly = $this->input->post("postAssembly");
            $this->order_model->update(array("postAssembly" => $postAssembly), array("orderID" => $orderID));
            echo json_encode(array("status" => "success"));
        } catch (Exception $e) {
            send_exception_report($e);
            echo json_encode(array("status" => "error"));
        }
    }

    /**
     * TODO: this function seems deprecated and is not referenced anywhere
     *
     * uses ajax to return the results so the search form can stay visible
     */
    public function search_barcodes()
    {
        if (isset($_POST['submit'])) {
            $this->load->model('barcode_model');
            $content['result'] = $this->barcode_model->get_order_item_from_barcode($_POST['barcode']);
        }

        $this->setPageTitle('Search Barcodes');
        $this->renderAdmin('search_barcodes', $content);
    }

    /**
     * Simple form that allows the employee to select routes and them get the printed receipts
     */
    public function print_receipts()
    {
        $this->load->model('location_model');
        $content['routes'] = $this->location_model->get_routes($this->business_id);

        $this->setPageTitle('Print Receipts');
        $this->renderAdmin('print_receipts', $content);
    }

    public function update_notes()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("order_id", "Order ID", "required|is_natural_no_zero|does_order_exist");

        if ($this->form_validation->run()) {
            $order = new Order($this->input->post("order_id"), false);
            $order->notes = $this->input->post("notes");
            $order->postAssembly = $this->input->post("postAssembly");
            $order->save();
            set_flash_message("success", "Updated Order Notes");
            redirect_with_fallback("admin/orders/details/".$order_id);
        } else {
            set_flash_message("error", validation_errors());
            redirect_with_fallback("/admin/orders/view");
        }
        redirect();
    }

    /**
     * updates the notes for an order
     */
    public function update_notes_ajax()
    {
        if (isset($_POST['note'])) {
            //update the note
            $where['orderID'] = $_POST['order_id'];
            $options['llNotes'] = $_POST['note'];

            $affected_rows = $this->order_model->update($options, $where);
            if ($affected_rows) {
                $array = array('status'=>'success');
            } else {
                $array = array('status'=>'fail', 'error'=>"No update");
            }

            echo json_encode($array);
        }
    }

    public function get_product_processes_ajax()
    {
        if (isset($_POST['submit'])) {
            $product_id = $_POST['product_id'];
            $sql = "SELECT product_processID, name, price, process_id FROM product_process pp
            LEFT JOIN process p ON processID = process_id
            WHERE product_id = {$product_id}";
            $q = $this->db->query($sql);
            $processes = $q->result();
            echo json_encode($processes);
        }
    }

    /**
     * Expects 'product_process_id' to be passed as a POST parameter
     * If 'order_id' is passed as a POST parameter, then it retrieves the suppliers based on the order ID.
     */
    public function get_product_suppliers_ajax()
    {
        $this->load->library("form_validation");
        if ($this->form_validation->run("get_product_suppliers")) {
            $product_process_id = $this->input->post('product_process_id');

            if ($this->input->post("order_id")) {
                $order_id = $this->input->post("order_id");
                $this->load->model("product_supplier_model");
                $supplier = $this->product_supplier_model->getDefault($product_process_id, $order_id);
            } else {
                $sql = "SELECT companyName, supplier_id FROM product_process LEFT JOIN product_supplier ON product_supplier.product_process_id = product_processID INNER JOIN supplier ON product_supplier.supplier_id = supplier.supplierID INNER JOIN business ON businessID = supplier.supplierBusiness_id WHERE product_process.product_processID = ? AND `default` = 1";

                $query = $this->db->query($sql, array($product_process_id));
                if (!$supplier = $query->row()) {
                    $sql = "SELECT companyName, supplierID AS supplier_id FROM supplier INNER JOIN business ON supplierBusiness_id = businessID WHERE business_id = ? AND supplierBusiness_id = ?";
                    $query = $this->db->query($sql, array($this->business->businessID, $this->business->businessID));
                    if (!$supplier = $query->row()) {
                        $new_supplier = new Supplier();
                        $new_supplier->business_id = $this->business->businessID;
                        $new_supplier->supplierBusiness_id = $this->business->businessID;
                        $new_supplier->save();
                        $supplier = array('companyName'=>$this->business->companyName, 'supplier_id'=>$new_supplier->{Supplier::$primary_key});
                    }
                }
            }

            echo json_encode($supplier);
        } else {
            output_ajax_error_response(validation_errors());
        }
    }


    /**
     * Uncaptures an order
     * Expects the order ID as segment 4 of the URL.
     */
    public function uncapture($order_id = null)
    {
        if (is_numeric($order_id)) {
            if ($this->order_model->does_exist($order_id)) {
                if ($this->order_model->uncapture($order_id, $this->employee->employeeID)) {

                    $message = "Successfully uncaptured order $order_id";
                    $processor = get_business_meta($this->business_id, 'processor');
                    //this is required for the Transbank software audit, but we can remove it once approved.
                    if ($processor == 'transbank') {
                        $transbank = new \DropLocker\Processor\Transbank(array('business_id' => $this->business_id));
                        if ($transbank->codeReverse($order_id)) {
                            $message.= ' and the total amount was refunded to the customer.';
                        }
                    }
                    set_flash_message('success', $message);
                } else {
                    set_flash_message("error", "Failed to uncapture order $order_id");
                }
                redirect("/admin/orders/details/$order_id");
            } else {
                set_flash_message("error", "Order '$order_id' doe snot exist");
            }
        } else {
            set_flash_message("error", "'order_id' must be passed in segment 4 of the URL and must be numeric");
        }

        $this->goBack("/admin/orders");
    }


    /**
     * ##############################################
     * BEGIN
     * ##############################################
     *
     * Controller for making a payment on an order
     *
     * If we are processing a single order from the order detail page, this will redirect to the order detail page
     * If we are capturing all the orders for the day from the orders view all orders page, we build an array with all
     * the status messages and display in a listview
     *
     * @throws Exception Missing post data for capturing payments
     *
     * ##############################################
     * END
     * ##############################################
     *
     * Expects the following POST parameters:
     * process
     */
    public function capture($order_id = null)
    {
        if (!$this->input->post("process")) {
            set_flash_message("error", "'process' must be passed as a POST parameter");
            $this->goBack("/admin/orders/view");
        }

        foreach ($_POST['process'] as $order_id) {
            $response = $this->order_model->capture($order_id, $this->business_id);
            $order = new Order($order_id);
            if ($response['status'] == 'success') {

                $res[] = array(
                    'status' => "SUCCESS",
                    'order_id' => $order->orderID,
                    'message' => $response['message'],
                    'amount' => $response['amount'],
                );

                $orderStatus = new OrderStatus();
                $orderStatus->order_id             = $order_id;
                $orderStatus->locker_id            = $order->locker_id;
                $orderStatus->employee_id          = get_employee_id($this->session->userdata('buser_id'));
                $orderStatus->orderStatusOption_id = $order->orderStatusOption_id;
                $orderStatus->note                 = "Captured Payment";
                $orderStatus->save();

            } else {
                $res[] = array(
                    'status' => "FAIL",
                    'order_id' => $order->orderID,
                    'message' => $response['message'],
                );
            }
        }

        // IF this was a single capture, ie... button on order detail page is clicked
        // WE will redirect back to the detail page with a status massage
        if ($order_id) {
            if ($response['status'] == 'success') {
                set_flash_message('success', $response['message']);
            } else {
                $message = $response['message'];
                $message .= " <a target='_blank' href='/admin/customers/billing/{$order->customer_id}'>Update Customer Credit Card</a> information.";
                set_flash_message('error', $message);
            }
            redirect("/admin/orders/details/$order_id");
        }

        // This will display the list of orders that were processed and their statuses
        $content['payments'] = $res;

        $this->setPageTitle('Payment Results');
        $this->renderAdmin('capture', $content);
    }

    /**
     * Sets an order as captured
     *
     * @param int $order_id
     */
    public function mark_as_captured($order_id = null)
    {
        // just to check the order belongs to this business
        $order = $this->getOrderById($order_id);

        if (!empty($_POST)) {

            $employee_id = get_employee_id($this->session->userdata('buser_id'));
            $this->order_model->mark_as_captured($order_id, $employee_id);

            set_flash_message('success', "Order Has Been Manually Changed To Captured");
        }

        redirect_with_fallback("/admin/orders/details/$order_id");
    }

    public function wf_approve()
    {
        //get all orders that need to be approved
        $sqlGetOrders = "select orders.*, customer.firstName, customer.wfNotes, location.route_id,
            customer.lastName, bag.bagNumber, internalNotes, location.address, locationID, customer.hold,
            location.business_id, customer.defaultWF,
            (select supplier_id from supplierWFRoutes where supplierWFRoutes.route_id = location.route_id limit 1) as supplier_id
        from orders
        join bag on orders.bag_id = bagID
        left join customer on orders.customer_id = customerID
        join locker on lockerID = orders.locker_id
        join location on locationID = locker.location_id
        and location.business_id = $this->business_id
        where orderStatusOption_id = 1;";

        $q = $this->db->query($sqlGetOrders);
        $content['orders'] = $orders = $q->result();
        $sqlGetSuppliers = "select business.companyName, business.businessID, supplierID
        from supplier
        join business on businessID = supplierBusiness_id
        where supplier.business_id = $this->business_id
        order by companyName";
        $q = $this->db->query($sqlGetSuppliers);
        $content['suppliers'] = $orders = $q->result();

        //get what supplier does what route
        $sqlGetSupplierRoute = "SELECT route_id, businessID FROM supplierWFRoutes
        join supplier on supplier_id = supplierID
        join business on supplierBusiness_id = businessID
        where supplier.business_id = $this->business_id";
        $query = $this->db->query($sqlGetSupplierRoute);
        $supplierRoutes = $query->result();
        foreach ($supplierRoutes as $s) {
            $content['route'][$s->route_id] = $s->businessID;
        }

        $this->setPageTitle('Approve W&F');
        $this->renderAdmin('wf_approve', $content);
    }

    public function wf_approve_post()
    {
        for ($i = 1; $i <= $_POST['counter']; $i++) {

            $order = array();
            $order['orderID'] = $orderID = $_POST['order_'.$i];

            if (empty($order['orderID'])) {
                throw new Exception("POST parameter 'order_id' can not be empty.");
            }
            $order['customer_id'] = $_POST['customer_id_'.$i];

            // The following conditional checks to see if there is a customer associated with the order. If not, then we reassign the order to the blank customer.
            if (empty($order['customer_id'])) {
                $order['customer_id'] = $this->business->blank_customer_id;

                // The following query reassigns the order to the blank customer.
                $this->db->where("orderID", $order['orderID']);
                $this->db->update("orders", array("customer_id" => $order['customer_id']));
            }

            if (empty($_POST['qty_'.$i])) {
                continue;
            }

            $order['qty']             = $_POST['qty_'.$i];
            $order['washLocation_id'] = $_POST['supplierBusinessID_'.$i];
            $order['notes']           = $_POST['notes_'.$i];
            $order['wfProduct']       = $_POST['wfProduct_'.$i];

            //check to see if this order is already being processed by the laundromat
            //see if it is in status inventoried or waiting for service
            $getOrder = "select * from orders where orderID = ? and orderStatusOption_id in (2,3)";
            $processedOrder = $this->db->query($getOrder, array($orderID))->result();

            if ($processedOrder) {
                set_flash_message('error', get_translation("error", "admin/orders/wf_approve", array('order_id' => $orderID), "Order %order_id% is already being processed by the laundromat"));
                continue;
            }

            //The following query updates the customer notes.
            $this->db->where("customerID", $order['customer_id']);
            $this->db->update("customer", array("wfNotes" => $order['notes']));

            // The following query updates the order notes.
            $this->db->where("orderID", $orderID);
            $this->db->update("orders", array("notes" => $order['notes']));

            if ($order['qty'] == "on") {
                //change status to Waiting for Service (2)
                $options['order_id'] = $orderID;
                $options['orderStatusOption_id'] = 2;
                $this->order_model->update_order($options);
            } else {

                //add wf to the order
                $this->load->model('orderitem_model');
                $res = $this->orderitem_model->add_item(
                    $orderID,                   // $order_id
                    0,                          // $item_id
                    $order['wfProduct'],        // $product_process_id
                    $order['qty'],              // $qty
                    $this->employee_id,         // $employee_id
                    $order['washLocation_id']   // $supplier_id
                );

                $this->load->library('wash_fold_product');
                $result = $this->wash_fold_product->add_wash_fold_preferences($res['orderItemID']);

                //update to inventoried
                $options['order_id'] = $orderID;
                $options['orderStatusOption_id'] = 3;
                $this->order_model->update_order($options);

                //send an email to the WF provider
            }
        }

        set_flash_message("success", get_translation("success", "admin/orders/wf_approve", array('order_id' => $orderID), "Approved Wash & Fold Orders"));
        redirect('/admin/orders/wf_approve');
    }


    public function wholesale_upload()
    {
        //LOAD THE  LIBRARY
        $this->load->library('import');

        $getImportSettings = "select * from importSettings
                                join customer on customer_id = customerID
                                where customer.business_id = $this->business_id
                                and importSettings.active = 1
                                order by firstName";
        $query = $this->db->query($getImportSettings);
        $content['importSettings'] = $query->result();

        $content['customer'] = $customerId = null;
        if (!empty($_POST['customer'])) $content['customer'] = $customerId = $_POST['customer'];

        if (isset($customerId)) {
            $getLockerInfo = "select customer_id, importType, locker_id, location_id, address, lockerName, customerBusiness_id
                                from importSettings
                                join locker on lockerID = locker_id
                                join location on locationID = location_id
                                where customer_id = $customerId";
            $query = $this->db->query($getLockerInfo);
            $content['lockerInfo'] = $lockerInfo = $query->row();
        }

        if ( !empty($_POST['importType']) && $_POST['importType'] == 'database') {
            //get all orders in inventoried status for this customer
            $getCustomerOrders = "SELECT order_id, orders.customer_id, bagNumber, item_id
                    FROM `orderItem`
                    inner join orders on orderItem.order_id = orderID
                    left join bag on bagID = orders.bag_id
                    where orderStatusOption_id in (3,7)
                    and orders.business_id = ".$lockerInfo->customerBusiness_id."
                    and item_id > 0
                    order by orderItem.order_id;";
            $query = $this->db->query($getCustomerOrders);
            $customerOrders = $query->result();

            //go through and import each item
            $lastOrder = $customerOrders[0]->order_id;
            foreach ($customerOrders as $customerOrder) {
                //if this is a new order
                if ($customerOrder->order_id != $lastOrder) {
                    $importedOrder = $this->import->findImportedOrder($lastOrder, $lockerInfo->customer_id);
                    //if the last order is in picked up status
                    if ($importedOrder->orderStatusOption_id == 1) {
                        //inventory it
                        $options['order_id'] = $lastOrder;
                        $options['orderStatusOption_id'] = 3;
                        $this->order_model->update_order($options);
                    }
                }

                $options['importType'] = 'DropLocker';
                $options['customer_id'] = $lockerInfo->customer_id;
                $options['location'] = $lockerInfo->location_id;
                $options['locker'] = $lockerInfo->locker_id;

                $options['item'] = $customerOrder->item_id;
                $options['order'] = $customerOrder->order_id;
                $options['next'] = 0;

                $ret = $this->import->insertOrderItems($options);

                $options = array();
                $lastOrder = $customerOrder->order_id;
            }
            //inventory the final order
            $importedOrder = $this->import->findImportedOrder($lastOrder, $lockerInfo->customer_id);
            //if the last order is in picked up status
            if ($importedOrder->orderStatusOption_id == 1) {
                //inventory it
                $options['order_id'] = $lastOrder;
                $options['orderStatusOption_id'] = 3;
                $this->order_model->update_order($options);
            }
        } elseif (!empty($_POST['importType']) && $_POST['importType'] == 'file') {
            $ret = "";
            if (!empty($_FILES["uploadedfile"])) {
                if ($_FILES["uploadedfile"]["error"] > 0) {
                    $ret .= "ERROR: " . $_FILES["uploadedfile"]["error"] . "<br />";
                } else {
                    //$newFilename = date("Y-d-m-his") . '.txt';
                    $newFilename = $this->business_id.'@'.$_FILES["uploadedfile"]["name"];
                    if (file_exists("uploads/" . $newFilename)) {
                        $ret .= '<br>ERROR: '.$_FILES["uploadedfile"]["name"] . ' already exists.
                                <br>These orders may have already been uploaded.  If not, please rename the file and <a href="">try again</a>';
                    } else {
                        move_uploaded_file($_FILES["uploadedfile"]["tmp_name"], "uploads/" . $newFilename);
                        //now parse through the file.
                        $lines = file("uploads/".$newFilename);
                        $lineNumber = 0;

                        $this->load->library('order_import');
                        $this->load->model('order_model');
                        $this->load->model('item_model');
                        $this->load->model('orderitem_model');
                        $skipOrder = 0;

                        foreach ($lines as $line_num => $line) {
                            if (empty($line)) {
                                continue;
                            }
                            $options['customerBusiness_id'] = $lockerInfo->customerBusiness_id;
                            $options['customer_id'] = $lockerInfo->customer_id;
                            $options['location'] = $lockerInfo->location_id;
                            $options['locker'] = $lockerInfo->locker_id;

                            $options['line'] = str_replace( "\t" , "|", $line );
                            $options['delimiter'] = "|";
                            $delimiter = $options['delimiter'];
                            $options['item'] = explode($delimiter, $options['line']);
                            $item = $options['item'];
                            $options['order'] = $item[0];
                            $options['barcode'] = $item[1];
                            $options['method'] = trim($item[2]);
                            $options['product'] = trim($item[3]);
                            $product = $options['product'];
                            $options['next'] = 0;
                            $options['productPart'] = explode(":", $product);

                            $ret .= $this->import->insertOrderItems($options);
                            $options = array();
                            $options['lastOrder'] = $item[0];
                        }
                    }
                }
            }
           // die();
            if (strpos($ret, "ERROR:") === false) {
                $ret ="SUCCESS: All orders on file were imported<br/>";
            }
            $content["response"] = $ret.$error.$succes;
        }

        $this->setPageTitle('Wholesale Order Upload');
        $this->renderAdmin('wholesale_upload', $content);
    }

    /**
     * Gathers the data for viewing wholesale orders.
     */
    public function wholesale_view()
    {
        $content = array();
        $customer_id = $this->input->post("customer_id");
        $showOrders = $this->input->post("showOrders");
        if (!empty($_POST['showOrders'])) {
            //show order history
            //Note, the timezone in the database is storedd as GMT.
            $sqlOrderHistory = "select name, dateCreated, orderID, ticketNum, orders.*
            from orders
            join orderStatusOption on orderStatusOptionID= orders.orderStatusOption_id
            where customer_id = $customer_id
            and date_format(CONVERT_TZ(dateCreated, 'GMT', 'US/Pacific'), '%Y-%m-%d') = '$showOrders'
            and orders.business_id = $this->business_id
            order by ticketNum asc";
            $q = $this->db->query($sqlOrderHistory);
            $content['orderHistory'] = $orderHistory = $q->result();

            $this->load->model('orderitem_model');
            foreach ($orderHistory as $o) {
                $ordTot = 0;
                $ordTot = $this->orderitem_model->get_full_total($o->orderID, $this->business_id);
                $content['totalAmt'] += $ordTot['total'];
                $content['closedGross'][$o->orderID] = $ordTot;

                if ($loggedInUser <> 'clothespin') {
                    $sqlAutomat = "select * from automat where order_id = $o->orderID order by slot, outPos, fileDate";
                    $q = $this->db->query($sqlAutomat);
                    $content['automat'][$o->orderID] = $q->result();
                }
            }
        }

        $this->setPageTitle('View Wholesale Orders');
        $this->renderAdmin('wholesale_view', $content);
    }

    public function wholesale_mapping($custId = null)
    {
        $sqlWholesaleCustomers = "select *
                                    from customer
                                    where type = 'Wholesale'
                                    and customer.business_id = $this->business_id
                                    order by firstName";
        $q = $this->db->query($sqlWholesaleCustomers);
        $content['customers'] = $q->result();

        if ($_POST) {
            $custId = $_POST['customer_id'];
        }

        if ($custId) {
            $content['custId'] = $custId;

            //get settings
            $getSettings = "select * from importSettings where customer_id = $custId";
            $q = $this->db->query($getSettings);
            $content['settings'] = $settings = $q->row();

            $getLockers = "select lockerID, lockerName, location.address as locationName
                            from locker
                            join location on locationID = location_id where location.business_id = $this->business_id
                            order by location.address, lockerName;";
            $q = $this->db->query($getLockers);
            $content['lockers'] = $q->result();

            if (!empty($settings)) {
                $getProducts = "select product_processID, price, product.name as productName, process.name as processName
                                from product_process
                                join product on productID = product_id
                                join process on processID = process_id
                                where product.business_id = $this->business_id
                                and active = 1
                                order by product.name, process.name";
                $q = $this->db->query($getProducts);
                $content['product'] = $q->result();

                if ($settings->importType == 'DropLocker') {
                    //get the customer's products
                    $getCustomerProducts = "select product_processID, price, product.name as productName, process.name as processName
                                from product_process
                                join product on productID = product_id
                                join process on processID = process_id
                                where product.business_id = ".$settings->customerBusiness_id."
                                and product_processID not in (Select productName from importMapping where customer_id = ".$custId.")
                                and active = 1
                                order by product.name, process.name";
                    $q = $this->db->query($getCustomerProducts);
                    $content['customerProducts'] = $q->result();

                    $getMappings = "select importMappingID, importMapping.price, customer_id, custProduct.name as productName, custProcess.name as processName, myProduct.name as myProductName, myProcess.name as myProcessName
                                        from importMapping
                                        join product_process custPP on custPP.product_processID = importMapping.productName
                                        join product custProduct on custProduct.productID = custPP.product_id
                                        join process custProcess on custProcess.processID = custPP.process_id
                                        join product_process myPP on myPP.product_processID = importMapping.product_process_id
                                        join product myProduct on myProduct.productID = myPP.product_id
                                        join process myProcess on myProcess.processID = myPP.process_id
                                        where customer_id = $custId
                                        order by productName, processName";
                    $q = $this->db->query($getMappings);
                    $content['mapping'] = $q->result();
                } else {
                    $getMappings = "select importMappingID, productName, processName, importMapping.price, product.name as myProductName, process.name as myProcessName
                    from importMapping
                    join product_process on product_processID = product_process_id
                    join product on productID = product_id
                    join process on processID = process_id
                    where customer_id = $custId
                    order by productName, processName";
                    $q = $this->db->query($getMappings);
                    $content['mapping'] = $q->result();
                }
            }
        }

        $this->setPageTitle('Import Setup');
        $this->renderAdmin('wholesale_mapping', $content);
    }

    public function import_settings_update()
    {
        $updateSettings = "replace into importSettings (customer_id, importType, active, customerBusiness_id, locker_id)
                            values ('".$this->input->post("custId")."', '".$this->input->post("importType")."', '".$this->input->post("active")."', '".$this->input->post("customerBusiness_id")."', '".$this->input->post("locker_id")."')";
        $q = $this->db->query($updateSettings);
        if($q) set_flash_message("success", "Import Settings Updated");
        else set_flash_message("error", "Error updating settings");
        redirect('/admin/orders/wholesale_mapping/'.$this->input->post("custId"));
    }

    public function wholesale_mapping_add()
    {
        if (isset($_POST['productName'])) {
            $insert1 = "insert into importMapping (customer_id, productName, processName, product_process_id, price)
                    values ('".$_POST['customer_id']."', '".$_POST['productName']."', '".$_POST['processName']."', ".$_POST['product_process_id'].", '".$_POST['price']."')";
            $ret = $this->db->query($insert1);
            if($ret) set_flash_message("success", "Mapping has been created");
            else set_flash_message("error", "Error creating mapping");
        } elseif (isset($_POST['customer_product_id'])) {
            $insert1 = "insert into importMapping (customer_id, productName, product_process_id, price)
                    values ('".$_POST['customer_id']."', '".$_POST['customer_product_id']."', ".$_POST['product_process_id'].", '".$_POST['price']."')";
            $ret = $this->db->query($insert1);
            if($ret) set_flash_message("success", "Mapping has been created");
            else set_flash_message("error", "Error creating mapping");
        } else {
            set_flash_message("error", "Product not entered");
        }
        redirect('/admin/orders/wholesale_mapping/'.$_POST['customer_id']);
    }

    public function wholesale_mapping_delete()
    {
        $delete1 = "delete from importMapping where importMappingID = $_POST[deleteMapping]";
        $ret = $this->db->query($delete1);
        if($ret) set_flash_message("success", "Mapping has been deleted");
        else set_flash_message("error", "Error deleting mapping");
        redirect('/admin/orders/wholesale_mapping/'.$_POST['customer_id']);
    }

    public function scheduled()
    {
        $getClaims = "SELECT scheduledOrders.*, location.address, customer.firstName, customer.lastName, customerID, lockerID, locker.lockerName
            from scheduledOrders
            left join customer on customerID = scheduledOrders.customer_id
            left join locker on lockerID = scheduledOrders.locker_id
            left join location on locationID = locker.location_id
            where scheduledOrders.business_id = ?
            order by firstName, lastName";
        $q = $this->db->query($getClaims, array($this->business_id));
        $content['claims'] = $q->result();

        $this->load->model('location_model');
        $content['locations_dropdown'] = $this->location_model->get_all_as_codeigniter_dropdown_for_business($this->business_id);

        $this->template->add_js("js/search.js");

        $this->setPageTitle('Scheduled Orders');
        $this->renderAdmin('scheduled', $content);
    }

    public function scheduled_create()
    {
        for ($i=0; $i < count($_POST['dayOfWeek']); $i++) {
            $days .= $_POST['dayOfWeek'][$i];
        }

        $sql = "insert into scheduledOrders (customer_id, locker_id, dayOfWeek, business_id)
        values (?, ?, ?, ?)";
        $insert = $this->db->query($sql, array(
            $_POST['customer_id'],
            $_POST['locker_id'],
            $days,
            $this->business_id
        ));
        set_flash_message("success", "Scheduled Order Created");
        redirect('/admin/orders/scheduled');
    }

    public function scheduled_update()
    {
        for ($i=0; $i < count($_POST['dayOfWeek']); $i++) {
            $days .= $_POST['dayOfWeek'][$i];
        }
        if ($days) {
            $sql = "update scheduledOrders set
                    customer_id = ?,
                    locker_id = ?,
                    dayOfWeek = ?
                    where scheduledOrdersID = ?";
            $this->db->query($sql, array(
                $_POST['customer_id'],
                $_POST['locker_id'],
                $days, $_POST['updRec'])
            );
            set_flash_message("success", "Scheduled Order Updated");
        } else {
            $sql= "delete from scheduledOrders where scheduledOrdersID = ?";
            $this->db->query($sql, array($_POST['updRec']));
            set_flash_message("success", "Scheduled Order Deleted");
        }
        redirect('/admin/orders/scheduled');

    }

    /**
     * NOTE: segment 4 can only be the order_id
     *
     * @throws \Database_Exception
     */
    public function wf_processing($bagNumber = null)
    {
        $url = $this->uri->config->config['base_url'].$this->uri->uri_string;
        if ($bagNumber) {
            $_POST['bagNumber'] = $bagNumber;
        }

        if (isset($_POST['bagNumber'])) {
            $trimNumber = trim($_POST['bagNumber']);
            //strip out leading zeros
            $trimNumber = ltrim($trimNumber, "0");

            //see if there is an order in picked, waiting or inventoried
            $sqlOpenOrder = "select orderID, bagNumber, orders.customer_id, orders.notes, orderStatusOption_id, lockerType, firstName, lastName
            from orders
            join bag on bagID = orders.bag_id
            join locker on lockerID = orders.locker_id
            join location on locationID = locker.location_id
            join locationType on locationTypeID = location.locationType_id
            left join customer on customerID = orders.customer_id
            where bagNumber = ?
            and orders.business_id = ?
            and orderStatusOption_id in (1,2,3)";

            $content['order'] = $openOrder = $this->db->query($sqlOpenOrder, array($trimNumber, $this->business_id))->result();

            $content['noOrder'] = FALSE;

            if ($openOrder) {

                //FIXME: hardcoded locationType name
                //FIXME: 'Lockers' is capitalized, but lockerType value is 'lockers', this will never match

                //If we don't know who the customer is and it is not a locker location
                if ($openOrder[0]->customer_id == 0 and $openOrder[0]->lockerType <> 'Lockers') {

                    if (isset($_POST['noBag'])) {
                        $content['notice'] = get_translation("notice_no_bag", "admin/orders/wf_processing", array(), 'Customer Unknown. <br><br>An eMail has been sent to customer service.  This bag probably won\'t get washed tonight..<br><br>');

                        send_admin_notification('ISSUE ALERT: No order for bag ' . $trimNumber . 'Bag '.$trimNumber.' was scanned but there is no customer for this bag.<br><br>Attendant Notes: '.$_POST['bagInfo'].'<br><br>AfterNotes: ['.$afterNotes.']<br><br><a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details/0/'.$trimNumber);
                    } else {
                        unset($content['order']);
                        $content['notice'] = get_translation("notice_no_order", "admin/orders/wf_processing", array("trimNumber"=>$trimNumber), '<strong>Sorry, we do not know who this order is for so we can\'t wash it.  Bag: '.$trimNumber.'<br><br>
                                If you need this order created, please rescan the bag and an email will be sent to customer service.<br><br>
                                <form action="/admin/orders/wf_process_create"" method="post">
                                <input type="hidden" name="bagNumber" value="%$trimNumber%">
                                        Confirm Bag Number: <input type="text" name="noBag">
                                        Weight: <input type="text" name="weight" size="5">  ');
                        if (substr($orgBagNumber,0,1) == 0) {
                            $content['notice'] .=  get_translation("notice_no_order_append", "admin/orders/wf_processing", array(), '<br><br>Is there any name or info inside the bag? <input type="text" name="bagInfo" size="40"><br><br>');
                        }
                        $content['notice'] .=  '<input type="submit" name="submit" value="'.get_translation("notice_no_order_create_order", "admin/orders/wf_processing", array(), 'Please Create This Order').'"></strong><br><br></form>';
                    }

                } else {
                    if ($openOrder[0]->orderStatusOption_id == 1) { //if this is a picked order try to change it to Waiting

                        //find out if this customer is on hold
                        $sqlGetCustomer = "select * from customer where customerID = ".$openOrder[0]->customer_id;
                        $query = $this->db->query($sqlGetCustomer);
                        $content['customer'] = $customer = $query->result();
                        if ($customer) {
                            if ($customer[0]->hold == 1) {
                                echo get_translation("account_on_hold", "admin/orders/wf_processing", array("customer_id"=>$customer[0]->customerID), '<a href="/admin/customers/detail/%customer_id%" target="_blank">Account</a> on hold. <br><br>');
                                send_admin_notification("ISSUE ALERT", 'No order for bag '.$_POST['bagNumber']. '. Bag '.$_POST['bagNumber'].' was scanned but this account is on hold so it could not be automatically created.<br><br>Weight is <strong>'.$_POST['weight'].' lbs</strong><br><br>Attendant Notes: '.$_POST['bagInfo'].'<br><br>AfterNotes: ['.$afterNotes.']<br><br><a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber'].'">http://'.$_SERVER['HTTP_HOST']."/admin/orders/bag_barcode_details?bag_number={$_POST['bagNumber']}</a>");
                                echo get_translation("account_on_hold_email_sent", "admin/orders/wf_processing", array(), 'An eMail has been sent to customer service.  This bag will be entered shortly.<br><br>
                                        <a href="/admin/orders/wf_processing"><- Back to WF Processing</a></strong><br><br>');
                                exit;
                            } else {
                                //change it to waiting.
                                $sqlChangeStatus = "update orders set orderStatusOption_id = 2 where orderID = ".$openOrder[0]->orderID;
                                $this->db->query($sqlChangeStatus);
                            }
                        } else { //we could not find an account for this customer.
                            echo get_translation("no_account_found", "admin/orders/wf_processing", array(), 'No Account Found. <br><br>');
                            send_admin_notification('Customer account not found', 'No order for bag '.$_POST['bagNumber'], 'Bag '.$_REQUEST['bagNumber'].' was scanned but this order but could not find an account.<br><br>Attendant Notes: '.$_REQUEST['bagInfo'].'<br><br>AfterNotes: ['.$afterNotes.']<br><br><a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details/0/'.$_POST['bagNumber'].'</a>');
                            echo get_translation("no_account_found_email_sent", "admin/orders/wf_processing", array(), 'An eMail has been sent to customer service.  This bag will be entered shortly.<br><br>
                                    <a href="wfLog.php"><- Back to WF Log</a></strong><br><br>');
                            exit;
                        }
                    }

                }
            } else {
                $content['noOrder'] = TRUE;
            }
            //get WF preferences on order
            if ($openOrder) {
                $this->load->model('product_model');
                $this->load->model('orderItem_model');
                $content['wfPrefs'] = $this->product_model->getOrderWF($openOrder[0]->orderID);

                //get wfLog values
                $sqlWFLog = "select * from wfLog where order_id = ".$openOrder[0]->orderID;
                $query = $this->db->query($sqlWFLog);
                $content['wfLog'] = $query->result();

                $defaults = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);
                if (!empty($defaults)) {
                    $defaults .= ", " . WFTOWELSPRODUCTPROCESSID;
                } else {
                    $defaults = WFTOWELSPRODUCTPROCESSID;
                }

                $select2 = "select * from orderItem where order_id = {$openOrder[0]->orderID} and product_process_id in ($defaults)";
                $query2 = $this->db->query($select2);
                $content['wfPref'] = $query2->row();
            }
        }

        $content['business'] = $this->business;
        $this->setPageTitle('WF Processing');
        $this->renderAdmin('wf_processing', $content);
    }

    public function wf_process_create()
    {
        $url = $this->uri->config->config['base_url'].$this->uri->uri_string;
        if ($_POST['noBag']) {
            if ($_POST['noBag'] <> $_POST['bagNumber']) {
                $content['notice'] = get_translation("wf_process_create_no_bag", "admin/orders/wf_processing", array("no_bag"=>$_POST['noBag'], "bag_number"=>$_POST['bagNumber']), '<span class="error">ERROR!  You scanned two different bag numbers. %no_bag% and %bag_number%<br><br>
                        <a href="wfLog.php">Please Try Again</a></span><br><br>');
            } else {
                //see if an order can be auto created.
                //if there are no open orders and we are confident that we know what locker it goes in, create an order.

                //find their last orders
                $sqlPastOrders = "SELECT lockerID, orders.customer_id, location_id, count(orderID) as numOrd
                FROM orders
                inner join bag on orders.bag_id = bagID
                inner join locker on orders.locker_id = lockerID
                where bagNumber = '{$_POST['bagNumber']}'
                and orders.business_id = $this->business_id
                group by location_id, lockerLockType_id";
                $query = $this->db->query($sqlPastOrders);
                $pastOrders = $query->result();

                //If all past orders are all at the same location and it is a key locker, create an order.  Then see if there are notes so it can be automatically created
                if ($query->num_rows() == 1 and $pastOrders[0]->numOrd > 2 and $pastOrders[0]->lockerLockType_id <> 1) {
                    $this->load->model('order_model');
                    //$options['orderStatusOption_id'] = 1;
                    //$options['orderPaymentStatusOption_id'] = 1;
                    $options['customer_id'] = $pastOrders[0]->customer_id;
                    //$options['llNotes'] = 'order created at laundromat';
                    $ret = $this->order_model->new_order($pastOrders[0]->location_id, $pastOrders[0]->lockerID, $_POST['bagNumber'], get_employee_id($this->session->userdata('buser_id')), 'order created at laundromat', $this->business_id, $options);
                    $content['notice'] = get_translation("wf_process_create_order_created", "admin/orders/wf_processing", array(), 'The Order Was Successfully Created!  Please scan it again to continue processing.<br><br>');
                    redirect('/admin/orders/wf_processing/'.$_POST['bagNumber']);
                } else {
                    if (!$pastOrders) {
                        $content['notice'] = get_translation("wf_process_create_order_not_find_any past_orders_for_this_bag", "admin/orders/wf_processing", array(), 'We could not find any past orders for this bag.  It is probably a new customer. <br><br>');
                        send_admin_notification('ISSUE ALERT: No order for bag '.$_POST['bagNumber'], 'Bag '.$_POST['bagNumber'].' was scanned but the system could not find any past orders for this bag.  Probably a new customer. The order could not be created automatically.<br><br>Weight is <strong>'.$_POST['weight'].' lbs</strong><br><br>Attendant Notes: '.$_POST['bagInfo'].'<br><br>http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber']);
                    } elseif ($query->num_rows() > 1) {
                        $content['notice'] = get_translation("wf_process_create_order_customer_has_had_orders_in_more_than_one location", "admin/orders/wf_processing", array(), 'The customer has had orders in more than one location, so we could not automatically create an order for them. <br><br>');
                        send_admin_notification('ISSUE ALERT: No order for bag '.$_POST['bagNumber'], 'Bag '.$_POST['bagNumber'].' was scanned but there were past orders at too many different locations to automatically create the order.<br><br>Weight is <strong>'.$_POST['weight'].' lbs</strong><br><br>Attendant Notes: '.$_REQUEST['bagInfo'].'<br><br>http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber']);
                    } elseif ($pastOrders[0]->numOrd <= 2) {
                        $content['notice'] = get_translation("wf_process_create_order_customer_has_had_less_than_3_orders", "admin/orders/wf_processing", array(), 'This customer has had less than 3 orders so we don\'t want to automatically create an order in the wrong location.<br><br>');
                        send_admin_notification('ISSUE ALERT: No order for bag '.$_POST['bagNumber'], 'Bag '.$_POST['bagNumber'].' was scanned but there were not enough past orders to automatically create an order.<br><br>Weight is <strong>'.$_POST['weight'].' lbs</strong><br><br>Attendant Notes: '.$_POST['bagInfo'].'<br><br>http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber']);
                    } elseif ($pastOrders[0]->lockerLockType_id == 1) {
                        $content['notice'] = get_translation("wf_process_create_order_customer_uses_key_lockers", "admin/orders/wf_processing", array(), 'This customer uses key lockers so we don\'t know what locker they used and we can\t automatically create an order.<br><br>');
                        send_admin_notification('ISSUE ALERT: No order for bag '.$_POST['bagNumber'], 'Bag '.$_POST['bagNumber'].' was scanned but this customer uses a key location and we can\'t know what locker they used.<br><br>Weight is <strong>'.$_POST['weight'].' lbs</strong><br><br>Attendant Notes: '.$_POST['bagInfo'].'<br><br>http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber']);
                    } else {
                        $content['notice'] = get_translation("wf_process_create_order_unknown_error", "admin/orders/wf_processing", array(), 'There was an unknown error trying to create this order<br><br>');
                        send_admin_notification('ISSUE ALERT:
                                No order for bag '.$_POST['bagNumber'], '. Bag '.$_POST['bagNumber'].' was scanned but there is no open order and it could not be created automatically.<br><br>Weight is <strong><a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber'].'>http://'.$_SERVER['HTTP_HOST'].'/admin/orders/bag_barcode_details?bag_number='.$_POST['bagNumber'].'</a>');
                    }
                    $content['notice'] .= get_translation("wf_process_create_order_email_sent", "admin/orders/wf_processing", array(), '<br>An eMail has been sent to customer service.  This bag will be entered shortly.<br><br>');
                }
            }
        }

        $this->setPageTitle('WF Processing');
        $this->renderAdmin('wf_processing', $content);
    }

    public function wf_process_update()
    {

        if ($_POST['logData'] and !$_POST['user']) {
            $content['notice'] = '<span class="errorText" style="font-size: 20px;">'.get_translation("wf_process_update_employee_field_must_be_entered", "admin/orders/wf_processing", array(), 'ERROR!!!  EMPLOYEE field must be entered.').'</span><br><br>';
        } else {
            //get the order info
            $sqlThisOrder = "select *
            from orders
            join bag on bagID = orders.bag_id
            where orderID = '$_POST[orderId]'";
            $query = $this->db->query($sqlThisOrder);
            $thisOrder = $query->result();
            $bagNumber = $thisOrder[0]->bagNumber;

            if (!empty($_POST['logWash'])) {
                if (!$_POST['washStation']) {
                    $content['notice'] = '<span class="errorText" style="font-size: 20px;">'.get_translation("wf_process_update_washer_field_must_be_entered", "admin/orders/wf_processing", array(), 'ERROR!!!  WASHER field must be entered.').'</span><br><br>';
                } else {
                    $update = $this->db->query("update wfLog set washCust = ?, washTime = now(), washStation = ? where order_id = ?",
                        array($_POST['user'], $_POST['washStation'], $_POST['orderId']));

                    if ($update) {
                        set_flash_message('success', get_translation("wf_process_update_washing_logged", "admin/orders/wf_processing", array(), 'Washing logged'));
                    }
                }
            }

            if (!empty($_POST['logDry'])) {
                if (!$_POST['dryStation']) {
                    $content['notice'] = '<span class="errorText" style="font-size: 20px;">'.get_translation("wf_process_update_dryer_field_must_be_entered", "admin/orders/wf_processing", array(), 'ERROR!!!  DRYER field must be entered.').'</span><br><br>';
                } else {
                    $update = $this->db->query("update wfLog set dryCust = ?, dryTime = now(), dryStation = ? where order_id = ?", array(
                        $_POST['user'],
                        $_POST['dryStation'],
                        $_POST['orderId']
                    ));
                    if ($update) {
                        set_flash_message('success', get_translation("wf_process_update_drying_logged", "admin/orders/wf_processing", array(), 'Drying logged'));
                    }
                }
            }

            if ($_REQUEST['logFold']) {
                if (!$_POST['foldStation']) {
                    $content['notice'] = '<span class="errorText" style="font-size: 20px;">'.get_translation("wf_process_update_folder_station_field_must_be_entered", "admin/orders/wf_processing", array(), 'ERROR!!!  FOLDING STATION field must be entered.').'</span><br><br>';
                } else {
                    $update = $this->db->query("update wfLog set foldCust = ?, foldTime = now(), foldStation = ? where order_id = ?", array(
                        $_POST['user'],
                        $_POST['foldStation'],
                        $_POST['orderId']
                    ));
                    if ($update) {
                        set_flash_message('success', get_translation("wf_process_update_folding_logged", "admin/orders/wf_processing", array(), 'Folding logged'));
                    }
                }
            }

            if (!empty($_POST['weightOut'])) {
                $insert = $this->db->query("update wfLog set weightOutCust = ?, weightOutTime = now(), weightOut = ?, weightOutIP = ? where order_id = ?", array(
                    $_POST['user'],
                    $_POST['weightOut'],
                    $_SERVER['REMOTE_ADDR'],
                    $_POST['orderId']
                ));

                $this->load->model('product_model');

                //the supplier is set to the same business as the person weighing in the order.
                if ($this->employee->supplier_id) {
                    $supplier_id = $this->employee->supplier_id;
                } else {
                    $product_process_id = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);
                    $this->load->model("product_supplier_model");
                    if ($product_process_id) {
                        $default_supplier = $this->product_supplier_model->getDefault($product_process_id, $_POST['orderId']);
                        $supplier_id = $default_supplier->supplier_id;
                    }
                }

                if (!empty($supplier_id)) {
                    $update1 = "update orderItem set supplier_id = $supplier_id where order_id = {$_POST['orderId']}";
                    $update = $this->db->query($update1);
                }

                //if the weight out is more than 1.5 lbs different from weight in, email customer service
                //get wfLog values
                $defaults = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);
                if (!empty($defaults)) {
                    $defaults .= ", " . WFTOWELSPRODUCTPROCESSID;
                } else {
                    $defaults = WFTOWELSPRODUCTPROCESSID;
                }

                if ($default_wf_product_process_id) {
                    $processes[] = $default_wf_product_process_id;
                }

                $sqlWFLog = "select wfLog.notes, qty, weightIn from wfLog
                join orderItem on orderItem.order_id = wfLog.order_id
                where wfLog.order_id = {$_POST['orderId']}
                and product_process_id in (".$defaults.")";
                $query = $this->db->query($sqlWFLog);
                $wfLog = $query->result();

                $difference = $_POST['weightOut'] - $wfLog[0]->weightIn;

                $upper_tolerance = (float) get_business_meta($this->business_id, "weight_upper_tolerance", 1.5);
                $lower_tolerance = (float) get_business_meta($this->business_id, "weight_lower_tolerance", 0);
                if ($lower_tolerance > 0) {
                    $lower_tolerance = -$lower_tolerance;
                }

                if ($difference > $upper_tolerance) {
                    send_admin_notification('ISSUE ALERT: Weight Difference On Order '.$_POST['orderId'], 'The weight before washing is less than after washing.  There is very likely a problem and items have been added to this order.<br><br>Driver Weight: '.$wfLog[0]->qty.'<br>Start Weight: '.$wfLog[0]->weightIn.'<br>End Weight: '.$_POST['weightOut'].'<br><br>Notes:<br>'.$wfLog[0]->notes.'<br><br>Edit order here:  <a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/details/'.$_POST['orderId'].'</a>');
                    set_flash_message("error", get_translation("wf_process_error_weight_before_washing_is_less_than_after_washing", "admin/orders/wf_processing", array(), "CAUTION!!!  The weight before washing is less than after washing.."));
                }

                if ($difference < $lower_tolerance) {
                    send_admin_notification('ISSUE ALERT: Weight Difference On Order '.$_POST['orderId'], 'There is a significant weight difference from before and after washing.  Double check this order.<br><br>Driver Weight: '.$wfLog[0]->qty.'<br>Start Weight: '.$wfLog[0]->weightIn.'<br>End Weight: '.$_POST['weightOut'].'<br><br>Notes:<br>'.$wfLog['notes'].'<br><br>Edit order here:  <a href="http://'.$_SERVER['HTTP_HOST'].'/admin/orders/details/'.$_POST['orderId'].'</a>');
                    set_flash_message("error", get_translation("wf_process_error_large_weight_discrepance", "admin/orders/wf_processing", array(), "CAUTION!!!  Large weight discrepancy."));
                }

            }

            if ($_POST['weightIn']) {

                //if this order is in status Waiting for Service (2) inventory the order with weight entered.
                if ($thisOrder[0]->orderStatusOption_id == 2) {
                    $order = $_POST['orderId'];
                    $lbs = $_POST['weightIn'];
                    //$bagNumber = '';
                    $this->load->library('washfold');
                    $this->washfold->addWashFold($bagNumber, $lbs, $order, $this->business_id);
                } else {
                    set_flash_message('error', get_translation("wf_process_error_order_is_not_waiting_for_service", "admin/orders/wf_processing", array(), "This order is not in status 'Waiting for Service'. Therefore, it can not be processed. A notification has been sent to customer support"));
                    send_admin_notification('ISSUE ALERT: Unable to process order'.$_POST['orderId'], "This order was not in status 'Waiting for Service' and the WF attendant tried to weigh in the bag.  This might mean that they tried to add Wash & Fold to an old order or that the order has not been approved for cleaning.  Please double check the order.<br><br>http://droplocker.com/admin/orders/details/{$_POST['orderId']}");
                    redirect($_SERVER['HTTP_REFERER']);
                }

                $insert = $this->db->query("insert IGNORE into wfLog (order_id, weightIn, weightInCust, weightInTime, weightInIP) values (?, ?, ?, now(), ?)", array(
                    $_POST['orderId'],
                    $_POST['weightIn'],
                    $_POST['user'],
                    $_SERVER['REMOTE_ADDR']
                ));

                //redirect them to the print page
                redirect('/admin/orders/wf_print/'.$_POST['orderId']);
            }
        }

        if ($_POST['notes']) {
            $update = $this->db->query("update wfLog set notes = ? where order_id = ?", array(
                $_POST['notes'],
                $_POST['orderId']
            ));

            //if there are internal notes, email customer service
            send_admin_notification('ISSUE ALERT: WF Notes for Order '.$_POST['orderId'], 'The Wash & Fold attendant has entered the following Notes:<br><br><strong>'.$_REQUEST['notes'].'</strong><br><br>Edit order here:  http://'.$_SERVER['HTTP_HOST'].'/admin/orders/details/'.$_REQUEST['orderId']);

            if ($this->db->affected_rows() > 0) {
                set_flash_message('success', get_translation("wf_process_success_notes_updated", "admin/orders/wf_processing", array(), 'Notes updated'));
            }
        }

        if (!empty($_POST['orderId'])) {
            $this->order_model->update_order_taxes((int)$_POST['orderId']);
        }
        
        redirect('/admin/orders/wf_processing/'.$bagNumber);
    }

    public function wf_print($orderID = null)
    {
        //get WF preferences on order
        $this->load->model('product_model');
        $content['wfPref'] = $this->product_model->getOrderWF($orderID);

        //get order info
        $content['order'] = new Order($orderID, true);
        $content['customer'] = $content['order']->relationships['Customer'][0];
        $content['locker'] = $content['order']->relationships['Locker'][0];
        $content['location'] = new Location($content['locker']->location_id);

        $content['bag'] = $content['order']->relationships['Bag'][0];

        $wf_traveller_text = $this->business->wf_traveller_text;
        if ( empty( $wf_traveller_text ) ) {
            $wf_traveller_text = get_translation("wf_print_place_the_plastic_wrap_in_your_bag", "admin/orders/wf_processing", array(), 'PLEASE PLACE THE PLASTIC WRAP BACK IN YOUR BAG SO WE CAN RECYCLE IT FOR YOU, THANKS!');
        }
        $content['wf_traveller_text'] = $wf_traveller_text;

        $default = $this->product_model->get_default_product_process_in_category(WASH_AND_FOLD_CATEGORY_SLUG, $this->business_id);

        $getOtherItems = "select * from orderItem
                          join product_process on product_processID = product_process_id
                          join product on productID = product_id
                          where order_id = $orderID
                          and product_process_id not in ($default)
                          and productType != 'preference'; ";
        $query = $this->db->query($getOtherItems);
        $content['otherItems'] = $query->result();

        $this->load->view('print/wf_traveler', $content);
    }

    public function quick_scan_bag()
    {
        //show pph for people inventorying items
        $this->load->library('production_metrics');
        $this->load->model('itemupcharge_model');

        $employee_id = get_employee_id($this->session->userdata('buser_id'));

        $content['pph'] = $pph = $this->production_metrics->pphInventory($employee_id);
        $content['upcharges'] = $this->itemupcharge_model->get_employee_upcharges($employee_id);

        $lotInfo = "select lotCount, lotSize from lot
                    join business on business_id = businessID
                    where business_id = $this->business_id
                    and date_format(convert_tz(startTime, 'GMT', '".$this->business->timezone."'), '%Y-%m-%d') = date_format(convert_tz(now(), 'GMT', '".$this->business->timezone."'), '%Y-%m-%d')
                    order by lotNumber desc";
        $query = $this->db->query($lotInfo);
        $lot = $query->row();

        $content['thisLot'] = $pph['allItems'] - ($lot ? $lot->lotCount : 0);
        $content['lotSize'] = $lot ? $lot->lotSize : 0;

        $this->setPageTitle('Quick Scan');
        $this->renderAdmin('quick_scan_bag', $content);
    }


    /**
     * fixes all orders with out of range bag_id's
     * This was caused by a bug in mobile that that assigned the wrong bag_id to orders
     *
     * This will probably only be used once to fix the bags.
     */
    public function fixBags()
    {
        $sql = "select orderID, bag_id from orders where bag_id > 400000";
        $query = $this->db->query($sql);
        foreach ($query->result() as $order) {
            $sql = "SELECT d.bagNumber, b.bagID from driverLog d inner join bag b ON b.bagNumber = d.bagNumber where order_id = $order->orderID order by order_id DESC limit 1";
            $query = $this->db->query($sql);
            $log = $query->row();
            //echo "<br>ORDER: ".$order->bag_id." : ".$log->bagID;
            if ($this->order_model->update(array('bag_id'=>$log->bagID), array('orderID'=>$order->orderID))) {
                echo "<br>ORDER: ".$order->bag_id." : ".$log->bagID;
            } else {
                echo "<br>:::::FAILED::::ORDER: ".$order->bag_id." : ".$log->bagID;
            }
        }
    }

    public function add_linked_account()
    {
        $addLinkedAccount = "update customer set linkedLocation_id = $_POST[location_id]
        where customerID = $_POST[customerID]";
        $query = $this->db->query($addLinkedAccount);
        redirect("/admin/orders/view_location_orders/{$_POST[location_id]}");
    }

    public function barcode_lookup()
    {
        $content = array();
        $this->setPageTitle('Barcode Lookup');
        $this->renderAdmin('barcode_lookup', $content);
    }

    public function barcode_lookup_search()
    {
        $findOrder = "select * from orderItem
                join barcode on barcode.item_id = orderItem.item_id
                join orders on orderID = order_id
                where barcode = '".$this->input->post("barcode")."'
                and orderStatusOption_id != 10
                and orders.business_id = $this->business_id";
        $query = $this->db->query($findOrder);
        $order = $query->row();
        if($order)
            redirect("/admin/orders/edit_item?orderID=".$order->orderID."&itemID=".$order->item_id."");
        else {
             set_flash_message("error", "Could not find barcode ".$this->input->post("barcode")." on any open orders");
             redirect("/admin/orders/barcode_lookup");
        }
    }

    public function remove_item_from_customer()
    {
        $removeItem = "delete from customer_item
                where item_id = '".$this->input->get("item")."'
                and customer_id = '".$this->input->get("customer")."'";
        $query = $this->db->query($removeItem);

        set_flash_message("success", "Barcode removed from customer's account");
        redirect("/admin/orders/view_order_as_closet/".$this->input->get("customer"));
    }

    public function split_items()
    {
        $order = new Order($this->input->get("orderID"), true);
        $orderItems = $order->getData('orderItem');
        for ($i = 0; $i < sizeof($orderItems); $i++) {
            $productName = Product::getName($orderItems[$i]->product_process_id);
            $orderItems[$i]->displayName = $productName['name'];
            if ($orderItems[$i]->item_id > 0) {
                $bc = Barcode::search(array('item_id'=>$orderItems[$i]->item_id));
                if (!empty($bc)) {
                    $orderItems[$i]->barcode = $bc->barcode;
                }
            }
        }

        $content['orderItems'] = $orderItems;
        $this->setPageTitle('Split Order');
        $this->renderAdmin('split', $content);
    }

    public function split_order()
    {

        //create new bag
        $order = new Order($this->input->post("orderID"), true);
        $bag = new Bag();
        $bag->customer_id = $order->customer_id;
        $bag->business_id = $this->business_id;
        $bag->save();

        $original_order_id = $this->input->post("orderID");
        $original_order = $this->order_model->get(array('orderID' => $original_order_id));

        $combinedNotesNew = $original_order[0]->notes . "\n" .
            get_translation("split_from_order", "Split order",
                array('order' => $original_order_id),
                "Split from order %order%"
            );

        //create new order in status Waiting for Service
        $order_id = $this->order_model->new_order(
            $order->location_id,
            $order->locker_id,
            $bag->bagNumber,
            get_employee_id($this->session->userdata('buser_id')),
            $combinedNotesNew,
            $this->business_id,
            array(
                "customer_id" => $bag->customer_id,
                'orderStatusOption_id' => 2 //Waiting for Service
            )
        );
        
        $original_homeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $original_order_id,
        ));

        if ($original_homeDelivery) { 
            // set to 'inventoried to force home delivery on order'
            $employee_id = get_employee_id($this->session->userdata('buser_id'));
            $orderStatusOption_id = 3; //Inventoried
            $this->order_model->update_order_status($order_id, $orderStatusOption_id, $employee_id, $this->business_id);
        }

        $combinedNotesOriginal = $original_order[0]->notes . "\n" .
            get_translation("split_to_order", "Split order",
                array('order' => $original_order_id),
                "Split to order %order%"
            );

        //Update notes in original order
        $this->order_model->update(
            array("notes" => $combinedNotesOriginal),
            array("orderID" => $original_order[0]->orderID)
        );

        if (!empty($_POST['split_items'])) {
            foreach ($this->input->post("split_items") as $item) {
                //add the selected items to this order.  Do it manually so that it replicates the existing order
                $addItem = "insert into orderItem (order_id, qty, unitPrice, notes, item_id, cost, product_process_id)
                            select ?, 1, 0, notes, item_id, 0, product_process_id
                                from orderItem
                                where orderItemID = ?";
                $this->db->query($addItem, array($order_id, $item));

                $item_notes = get_translation("moved_to_order", "Split order",
                    array('order' => $order_id),
                    "Moved to order %order%"
                );

                //change items on existing order
                $updateItem = "update orderItem set notes = ? where orderItemID = ?";
                $this->db->query($updateItem, array($item_notes, $item));
                $splitCount++;
            }

            //upate the order so we know how many items were split off
            $updateOrder = "update orders set split = ? where orderID = ?";
            $this->db->query($updateOrder, array($splitCount, $original_order_id));

            //upate new order so we know it is an splited order
            $updateNewOrder = "update orders set split_from = ? where orderID = ?";
            $this->db->query($updateNewOrder, array($original_order_id, $order_id));

            $notesForOrigin = get_translation("split_to_order", "Split order",
                array('order' => $order_id),
                "Split to order %order%"
            );
            //add message to order status
            $dateCreated = convert_to_gmt_aprax();
            $this->logger->set('orderStatus', array('order_id'=>$original_order_id, 'date'=>$dateCreated, 'locker_id'=>get_order_locker($original_order_id),'employee_id'=>get_employee_id($this->session->userdata('buser_id')), 'orderStatusOption_id'=>current_order_status($original_order_id), 'note'=>$notesForOrigin));
        }

        //send email to customer
        Email_Actions::send_transaction_emails(array("do_action" => "order_splitted", "customer_id" => $order->customer_id, "business_id" => $this->business_id, "order_id" => $original_order_id));

        //redirect to new order
        set_flash_message("success", "New order created");
        redirect("/admin/orders/details/".$order_id);
    }

    public function highlight_routes()
    {
        update_business_meta($this->business_id, "highlight_routes", '');
        if (isset($_POST['routes'])) {
            $toHighlight = '';
            foreach ($_POST['routes'] as $route) {
                $toHighlight .= $route.'|';
            }
            update_business_meta($this->business_id, "highlight_routes", $toHighlight);
        }

        //get highlighted routes
        $selectedRoutes = get_business_meta($this->business_id, 'highlight_routes');
        $this->content['selectedRoutes'] = explode('|', $selectedRoutes);

        //get all routes
        $getRoutes = "select distinct route_id from location where business_id = $this->business_id";
        $query = $this->db->query($getRoutes);
        $this->content['allRoutes'] = $query->result();

        $this->setPageTitle('Highlighted Routes');
        $this->renderAdmin('highlight_routes', $this->content);
    }

    public function export_orders($selected = null)
    {
        $this->load->model('location_model');
        $routes = $this->location_model->get_routes($this->business_id);
        $content["routes"] = array();
        foreach($routes as $route) {
            $content["routes"][$route->route_id] = $route->route_id;
        }

        $content['start_date'] = date('Y-m-d');
        $content['end_date'] = date('Y-m-d');

        $content['softwares'] = array(
            'spot' => 'SPOT',
            'file' => 'File'
        );

        /*DROP-4397: changed POST with GET in order to know which params are client selecting when timeout error occurs.*/
        if (empty($_POST['export'])) {
            if (!empty($_GET['export'])) {
                $_POST = $_GET;
            }
        }

        if (!empty($_POST['export'])) {
            switch ($_POST['export']) {
                case 'spot':
                    $this->export_orders_to_spot( $content );
                    break;
                case 'file':
                    $this->export_orders_to_file( $content );
                    break;
                default:
                    break;
            }
        }

        $content['selected'] = $selected;

        $this->setPageTitle('Export Orders');
        $this->renderAdmin('export_orders', $content);
    }

    protected function export_orders_to_file( &$content )
    {
        $origin_business_id = $this->input->get("business_id");
        $start_date = $this->input->post('from');
        $end_date = $this->input->post('to');
        $routes = $this->input->post('routes');

        $content["start_date"] = $start_date;
        $content["end_date"] = $end_date;

        $start_date = convert_to_gmt_aprax($start_date . ' 00:00:00', $this->business->timezone, 'Y-m-d H:i:s');
        $end_date = convert_to_gmt_aprax($end_date . ' 23:59:59', $this->business->timezone, 'Y-m-d H:i:s');

        $this->load->library('export_orders');

        $result = $this->export_orders->export_to_file(array(
            'origin_business_id' => $this->business_id,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'routes' => $routes
        ));

        if ($result) {
            if ($result->status) {
                set_flash_message_now('success', $result->info);
            } else {
                set_flash_message_now('error', $result->info);
            }
            return;
        }
        set_flash_message_now('error', "Something went wrong. Please, try again.");
        return;
    }

    protected function export_orders_to_spot( &$content )
    {
        // This are spot settings
        $AccountKey = get_business_meta($this->business_id, 'spot_accountKey');
        $DeviceName = get_business_meta($this->business_id, 'spot_deviceName');
        $PriceTable = get_business_meta($this->business_id, 'spot_priceTable');
        $UserName   = get_business_meta($this->business_id, 'spot_userName');
        $url        = get_business_meta($this->business_id, 'spot_url');
        $prefix     = get_business_meta($this->business_id, 'spot_customer_prefix', '');

        $routes = $this->input->post('routes');
        $content['start_date'] = $_POST['from'];
        $content['end_date'] = $_POST['to'];

        $start_date = convert_to_gmt_aprax($_POST['from'] . ' 00:00:00', $this->business->timezone, 'Y-m-d H:i:s');
        $end_date = convert_to_gmt_aprax($_POST['to'] . ' 23:59:59', $this->business->timezone, 'Y-m-d H:i:s');
        $this->load->library('spot', array(
            'business_id' => $this->business_id,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'accountKey' => $AccountKey,
            'deviceName' => $DeviceName,
            'userName' => $UserName,
            'priceTable' => $PriceTable,
            'url' => $url,
            'customerPrefix' => $prefix,
            'routes' => $routes,
            ));

        $file_path = DEV ? "/tmp/" : "/home/droplocker/";
        $file_path .= $_POST['export'] . '/' . $this->business_id . '/';

        if (!is_dir($file_path)) {
            mkdir($file_path, 0777, true);
        }

        $file_path .= gmdate('Y-m-d_H.i.s_');

        $data = $this->spot->export(false, $totalInvoices);
        file_put_contents($file_path . 'payload.xml', $data);

        $request = null;
        $response = $this->spot->saveData($data, $request);

        file_put_contents($file_path . 'request.xml', $request);
        file_put_contents($file_path . 'response.xml', $response);

        $out = $this->spot->parseResponse($response, $data);

        $errors = array();
        $imported = array();
        foreach ($out['customers'] as $customer) {
            if ($customer['status'] == 'error') {
                $errors[] = sprintf('Customer <a href="/admin/customers/detail/%d" target="_blank">%d</a> not imported: %s',
                    $customer['id'],
                    $customer['id'],
                    $customer['message']
                    );
            }
        }

        $invoicesFailed = 0;
        $invoicesImported = 0;
        foreach ($out['invoices'] as $invoice) {
            if ($invoice['status'] == 'error') {
                $errors[] = sprintf('Order <a href="/admin/orders/details/%d" target="_blank">%d</a> not imported: %s',
                    $invoice['id'],
                    $invoice['id'],
                    $invoice['message']
                    );

                $invoicesFailed++;
            } else {
                $imported[] = sprintf('Order <a href="/admin/orders/details/%d" target="_blank">%d</a> imported: %s',
                    $invoice['id'],
                    $invoice['id'],
                    $invoice['message']
                    );

                $invoicesImported++;
            }
        }

        set_flash_message_now('success', "$totalInvoices orders selected, $invoicesImported orders imported, $invoicesFailed failed");
        $content['error_messages'] = $errors;
        $content['imported_messages'] = $imported;
    }

    public function export_settings($software = null)
    {
        if (isset($_POST['software'])) {
            redirect('/admin/orders/export_settings/' . $_POST['software']);
        }

        switch ($software) {
            case 'spot':
                $load_settings = array(
                   'spot_url',
                   'spot_accountKey',
                   'spot_deviceName',
                   'spot_priceTable',
                   'spot_userName',
                   'spot_customer_prefix',
                   'spot_add_split_item_count'
                );
                $defaults = array();
            break;

            case 'fabricare':
                $load_settings = array(
                    'fabricare_ready_by',
                    'fabricare_service_days',
                    'fabricare_suppliers'
                );
                $defaults = array(
                    'fabricare_ready_by' => 0,
                );

                $this->load->model('supplier_model');
                $content['suppliers'] = $this->supplier_model->simple_array($this->business_id);
               break;

            default:
                $load_settings = array();
                $defaults = array();
        }

        if (!empty($_POST)) {
            foreach ($load_settings as $key) {
                $value = $_POST[$key];
                if (is_array($value)) {
                    $value = serialize($value);
                }

                update_business_meta($this->business_id, $key, $value);
            }
            set_flash_message("success", "Settings have been updated");
            return redirect($this->uri->uri_string());
        }

        foreach ($load_settings as $key) {
            $default = isset($defaults[$key]) ? $defaults[$key] : null;
            $content[$key] = get_business_meta($this->business_id, $key, $default);
        }

        $content['software'] = $software;
        $content['softwares'] = array(
            '' => '--SELECT--',
            'fabricare' => 'Fabricare',
            'spot' => 'SPOT',
        );

        $this->setPageTitle('Export Settings');
        $this->renderAdmin('export_settings', $content);
    }

    /**
     * Searches or displays all booked orders in the system.
     */
    public function booked()
    {
        $this->load->model("order_model");

        $firstName = trim($this->input->post("firstName"));
        $lastName = trim($this->input->post("lastName"));
        $address = trim($this->input->post("address"));

        $firstName = (strlen($firstName) == 0)?false:$firstName;
        $lastName = (strlen($lastName) == 0)?false:$lastName;
        $address = (strlen($address) == 0)?false:$address;

        $this->load->model('servicetype_model');
        $serviceTypes = $this->servicetype_model->get_formatted();

        $orderHomeDeliveries = $this->order_model->getBookedHomeDeliveries($this->business_id,$firstName, $lastName, $address);

        $content['orderHomeDeliveries'] = $orderHomeDeliveries;

        //The following variables store the results of the user's search, so as to populate the search fields with the user's previous search values whne the view is rendered.
        $content['firstName'] = $firstName;
        $content['lastName'] = $lastName;
        $content['address'] = $address;

        $this->setPageTitle('Booked Orders');
        $this->renderAdmin('/admin/orders/booked', $content);
    }

    public function save_custom_manual_discount()
    {
        /*  $this->input->post() = 
            Array
            (
                [amount] => 10
                [description] => description
                [type] => _percent
            )
        */

        $custom_manual_discounts = json_decode(get_business_meta($this->business_id, 'custom_manual_discounts', '[]'));
        
        $exists = false;
        foreach ($custom_manual_discounts as $d) {
           if (
               ( $d->amount == $this->input->post('amount') && $d->description == $this->input->post('description') && $d->type == $this->input->post('type') ) 
               || 
               ( $d->description == $this->input->post('description'))
            ) {
               $exists = true;
           }
        }

        if (!$exists) {
            array_push($custom_manual_discounts, $this->input->post());
            update_business_meta($this->business_id, 'custom_manual_discounts', json_encode((array)$custom_manual_discounts));
            die('success');
        } else {
            die('exists');  
        }

        die('error');
    }

    public function delete_custom_manual_discount()
    {
        /*  $this->input->post() = 
            Array
            (
                [amount] => 10
                [description] => description
                [type] => _percent
            )
        */

        $custom_manual_discounts = json_decode(get_business_meta($this->business_id, 'custom_manual_discounts', '[]'));
        
        $exists = false;
        foreach ($custom_manual_discounts as $k=>$d) {
           if (
               ( $d->amount == $this->input->post('amount') && $d->description == $this->input->post('description') && $d->type == $this->input->post('type') ) 
               || 
               ( $d->description == $this->input->post('description'))
            ) {
               unset($custom_manual_discounts[$k]);
               update_business_meta($this->business_id, 'custom_manual_discounts', json_encode((array)$custom_manual_discounts));
               die('success');
           }
        }

        die('error');
    }

    protected function getOrderById($order_id, $redirect_to = "/admin/orders")
    {
        try {
            $order = new Order($order_id);
        } catch (Exception $e) {
            set_flash_message('error', "Order [$order_id] does not exist");
            redirect($redirect_to);
        }

        if ($order->business_id != $this->business_id) {
            set_flash_message("error", "This order does not belong to your business.");
            redirect($redirect_to);
        }

        return $order;
    }

}
