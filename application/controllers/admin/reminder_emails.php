<?php

use App\Libraries\DroplockerObjects\Reminder_Email;

class Reminder_Emails extends MY_Admin_Controller
{
    /**
     * Updates or creates a new reminder email entity
     * Can take the followinge POST parameters
     * reminder_emailID If not passed, then this function calls the create action.
     *
     * If reminder_emailID is passed as a POST parameter, then the following POST parameters are required
     *  reminder_id int
     *  business_language_id int
     *  subject string
     *  body string
     */
    public function update()
    {
        if ($this->input->post("reminder_emailID")) {
            $action = "Updated";
            try {
                $reminder_email = new Reminder_Email($this->input->post("reminder_emailID"));
            } catch (App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
                set_flash_message("error", $notFound_exception->getMessage());
                redirect_with_fallback("/admin/reminders");
            }
        } else {
            $action = "Created";
            $reminder_email = new Reminder_Email();
        }

        $redirectURL = "/admin/reminders/edit/$reminder_email->reminder_id";
        try {
            $reminder_email->setProperties($this->input->post());
        } catch (App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
            set_flash_message("error", $validation_exception->getMessage());
            redirect_with_fallback($redirectURL);
        }
        $this->load->model("business_language_model");
        $business_language = $this->business_language_model->get_by_primary_key($reminder_email->business_language_id);
        $this->load->model("reminder_model");
        $reminder = $this->reminder_model->get_by_primary_key($reminder_email->reminder_id);
        set_flash_message("success", "$action Reminder Email '$reminder_email->subject' ($reminder_email->reminder_emailID) in Language '$business_language->description' for  Reminder '$reminder->description' ($reminder->reminderID)");
        redirect_with_fallback($redirectURL);
    }

    /**
     * Deletes a reminder email
     * Expects the following GET parameter
     * reminder_emailID
     */
    public function delete()
    {
        if ($this->input->get("reminder_emailID")) {
            $reminder_emailID = $this->input->get("reminder_emailID");
            $reminder_email = new Reminder_Email($reminder_emailID);
            $reminder_email->delete();
            set_flash_message("success", "Deleted reminder email");
        } else {
            set_flash_message("error", "'reminder_emailID' must be passed as a GET parameter");
        }

        $this->goBack("/admin/reminders");
    }

}
