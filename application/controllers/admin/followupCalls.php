<?php

use App\Libraries\DroplockerObjects\Customer;
class FollowupCalls extends MY_Admin_Controller
{

    private $inactive_customers_dataTable_columns;
    private $new_signups_dataTable_columns;
    private $onhold_customers_dataTable_columns;
    private $firstOrder_customers_dataTable_columns;

    public function __construct()
    {
        parent::__construct();

        $this->inactive_customers_dataTable_columns = array(
            array("query_name" => "customerID", "display_name" => "Customer ID", "orderBy_name" => "customerID"),
            array("query_name" => "MAX(dateCreated) AS max_order", "display_name" => "Last Order", "search_name" => "max_order", "orderBy_name" => "max_order"),
            array("query_name" => "customer.firstName", "display_name" => "First Name", "orderBy_name" => "customer.firstName"),
            array('query_name' => "customer.lastName", "display_name" => "Last Name", "orderBy_name" => "customer.lastName")
        );
        $this->new_signups_dataTable_columns = array(
            array("query_name" => "customerID", "display_name" => "Customer ID", "orderBy_name" => "customerID"),
            array("query_name" => "firstName", "display_name" => "First Name", "orderBy_name" => "firstName"),
            array("query_name" => "lastName", "display_name" => "Last Name", "orderBy_name" => "lastName"),
            array("query_name" => "DATE_FORMAT(CONVERT_TZ(signupDate, 'GMT', '{$this->business->timezone}'), '%m/%d/%y %h:%i %p') AS signupDate", "display_name" => "Signup Date", "orderBy_name" => "signupDate"),
            array("query_name" => "email", "display_name" => "Email", "orderBy_name" => "email"),
            array("query_name" => "phone", "display_name" => "Phone", "orderBy_name" => "phone")
        );

        $this->firstOrder_customers_dataTable_columns = array(
            array("query_name" => "customerID", "display_name" => "Customer ID", "orderBy_name" => "customerID"),
            array("query_name" => "firstName", "display_name" => "First Name", "orderBy_name" => "firstName"),
            array("query_name" => "lastName", "display_name" => "Last Name", "orderBy_name" => "lastName"),
            array("query_name" => "DATE_FORMAT(CONVERT_TZ(MIN(dateCreated), 'GMT', '{$this->business->timezone}'), '%m/%d/%y %h:%i %p') AS dateCreated", "display_name" => "First Order", "orderBy_name" => "dateCreated")
        );

        $this->onhold_customers_dataTable_columns = array(
            array("query_name" => "customerID", "display_name" => "Customer ID", "orderBy_name" => "customerID"),
            array("query_name" => "firstName", "display_name" => "First Name", "orderBy_name" => "firstName"),
            array("query_name" => "lastName", "display_name" => "Last Name", "orderBy_name" => "lastName"),
            array("query_name" => "DATE_FORMAT(CONVERT_TZ(dateCreated, 'GMT', '{$this->business->timezone}'), '%m/%d/%y %h:%i %p') AS dateCreated", "display_name" => "Order Date", "orderBy_name" => "dateCreated"),
            array("query_name" => "orderID", "display_name" => "Order ID", "orderBy_name" => "orderID"),
            array("query_name" => "phone", "display_name" => "Phone", "orderBy_name" => "phone"),
            array("query_name" => "customerNotes", "display_name" => "Notes", "orderBy_name" => "customerNotes"),
            array("query_name" => "SUM(unitPrice * qty) as amt", "display_name" => "Amt", "orderBy_name" => "amt")
        );


        $this->load->model("followupcall_model");
    }

    /**
     * Retrieves the number of follow up calls for the current business.
     * Expects the following GET parameter:
     *  business_id
     * @throws \LogicException if the request does not contain the GET parameter 'business_id'
     * @throws \InvalidArgumentException if the request is not AJAX
     */
    public function get_total()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get('business_id')) {
                $business_id = $this->input->get('business_id');
                try {
                    $this->load->model('followupCall_model');
                    $followupCall_total = $this->followupCall_model->get_count($business_id);
                    $result = array('status' => 'success', 'total' => $followupCall_total);
                } catch (\Exception $e) {
                    $result = array('status' => 'error', 'message' => 'An error occurred attempting to get the total followup call count');
                    send_exception_report($e);
                }
                echo json_encode($result);
            } else {
                throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter");
            }
        } else {
            throw new \LogicException("This action can only be called with an ajax request");
        }
    }

    public function get_inactive_customers_dataTable_columns()
    {
        $columns = array();
        foreach ($this->inactive_customers_dataTable_columns as $index => $inactive_customers_dataTable_column) {
            $columns[$index]['sTitle'] = $inactive_customers_dataTable_column['display_name'];
        }

        $columns[]['sTitle'] = "Generate Call Script";

        echo json_encode($columns);
    }

    /**
     * Retrieves the data from a DataTables request. and returns a formatted result set for DataTables.
     * @throws Database_Exception
     */
    public function get_inactive_customers_dataTable_data()
    {
        try {
            $followupCallType_id = 2;
            if (!$this->input->get("threshold")) {
                $day_threshold = 10;
            } else {

                $day_threshold = $this->input->get("threshold");
            }

            /* Array of database columns which should be read and sent back to DataTables. Use a space where
             * you want to insert a non-database field (for example a counter or static image)
             */
            foreach ($this->inactive_customers_dataTable_columns as $inactive_customers_dataTable_column) {
                $aColumns[] = $inactive_customers_dataTable_column['query_name'];
            }

            /* Indexed column (used for fast and accurate table cardinality) */
            $sIndexColumn = "orders.customer_id";

            /*
             * Paging
             */
            $sLimit = "";
            if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
                $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " .
                        mysql_real_escape_string($_GET['iDisplayLength']);
            }

            /*
             * Ordering
             */
            $sOrder = "";
            if (isset($_GET['iSortCol_0'])) {
                $column_key = $this->input->get("iSortCol_0");
                if (isset($this->inactive_customers_dataTable_columns[$column_key])) {
                    $sOrder .= "ORDER BY " . $this->inactive_customers_dataTable_columns[$column_key]['orderBy_name'] . " " . mysql_real_escape_string($this->input->get('sSortDir_0'));
                }
            }


            /*
             * Filtering
             * NOTE this does not match the built-in DataTables filtering which does it
             * word by word on any field. It's possible to do here, but concerned about efficiency
             * on very large tables, and MySQL's regex functionality is very limited
             */
            $having = "";

            if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
                $having = " AND ";
                $having .= "(";
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (isset($this->inactive_customers_dataTable_columns[$i]['dataTable_options']['bSearchable']) && $this->inactive_customers_dataTable_columns[$i]['dataTable_options']['bSearchable'] == false) {
                        continue;
                    } else {
                        if (isset($this->inactive_customers_dataTable_columns[$i]['search_name'])) {
                            $having .= $this->inactive_customers_dataTable_columns[$i]['search_name'] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                        } else {
                            $having .= $this->inactive_customers_dataTable_columns[$i]['query_name'] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                        }
                    }
                }
                //The following conditional applies a string search for both the firstName and lastName column if both were specified in the columns parameter.
                if (in_array("firstName", $aColumns) && in_array("lastName", $aColumns)) {
                    $having .= "CONCAT_WS(' ', firstName, lastName) LIKE \"%{$this->input->get("sSearch")}%\" OR ";
                }
                $having = substr_replace($having, "", -3);
                $having .= ")";
            }

            /* DROP-4007: limit response to 180 days before now */        
            $having .= " AND max_order >= DATE_ADD(CURDATE(), INTERVAL -180 DAY)";

            /*
              //Individual column filtering
              for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
              if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
              $having .= " AND ";
              $having .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
              }
              }
             */

            /*
             * SQL queries
             * Get data to display
             */
            $select = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $aColumns) . ", max(dateCreated) AS max_order_date_raw";


            $sQuery = "
                $select
                FROM orders
                JOIN customer on customerID = orders.customer_id
                LEFT JOIN followupCall ON (customerID=followupCall.customer_id AND followupCallType_id = '$followupCallType_id')
                WHERE orders.business_id = '{$this->business_id}' AND orders.bag_id!=0
                AND followupCallType_id IS NULL
                GROUP BY orders.customer_id

                HAVING
                    max_order<= date_sub(now(), interval $day_threshold day)
                    $having

                $sOrder
                $sLimit
            ";

            $rQuery = $this->db->query($sQuery);

            $rResult = $rQuery->result_array();

            /* Data set length after filtering */
            $sQuery = "
                    SELECT FOUND_ROWS() AS found_rows
            ";

            $aResultFilterTotal = $this->db->query($sQuery)->row();
            $iFilteredTotal = $aResultFilterTotal->found_rows;

            /* Total data set length */
            $sQuery = $this->db->query("
                SELECT COUNT(DISTINCT customerID) AS total
                FROM orders
                JOIN customer on customerID = orders.customer_id
                LEFT JOIN followupCall ON (customerID=followupCall.customer_id AND followupCallType_id = '$followupCallType_id')
                WHERE
                    orders.business_id = '{$this->business_id}'
                    AND followupCallType_id IS NULL
                    AND orders.dateCreated <= date_sub(now(), interval $day_threshold day)


            ");
            $aResultTotal = $sQuery->row();
            $iTotal = $aResultTotal->total;

            /*
             * Output
             */
            $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
            );

            $dateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat");

            foreach ($rResult as $aRow) {
                $row = array();
                $customerID = $aRow['customerID'];
                //The following statement renders a clickable link to the customer's detail view.
                $aRow['customerID'] = "<a target='_blank' href='/admin/customers/detail/{$aRow['customerID']}'> {$aRow['customerID']} </a>";
                $aRow['max_order'] = convert_from_gmt_aprax($aRow['max_order'], $dateFormat);

                for ($i = 0; $i < count($aColumns); $i++) {
                    $keys = array_keys($aRow);
                    $row[] = $aRow[$keys[$i]];
                }
                
                //The following statements render the Generate Call Script button for the customer.
                $row[] = "<a href='/admin/followupCalls/generate_followup_call_script?customerID=$customerID&followupCallType_id=$followupCallType_id'> Generate Call Script </a>";
                $output['aaData'][] = $row;
            }
            echo json_encode($output);
        } catch (Exception $e) {
            echo json_encode(array("status" => "error", "message" => $e->getMessage()));
            send_exception_report($e);
        }
    }

    public function get_onhold_customers_dataTable_columns()
    {
        $columns = array();
        foreach ($this->onhold_customers_dataTable_columns as $index => $onhold_customers_dataTable_column) {
            $columns[$index]['sTitle'] = $onhold_customers_dataTable_column['display_name'];
        }

        $columns[]['sTitle'] = "Generate Call Script";

        echo json_encode($columns);
    }

    /**
     * Gathers the data and renders the view for viewing customers who have placed their first order.
     */
    public function view_firstOrder_customers()
    {
        $content = array();
        $this->data['content'] = $this->load->view('admin/followupCalls/view_firstOrder_customers', $content, true);
        $this->template->write("page_title", "View First Order Customers", true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function get_onhold_customers_dataTable_data()
    {
        $followupCallType_id = 4;
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        foreach ($this->onhold_customers_dataTable_columns as $onhold_customers_dataTable_column) {
            $aColumns[] = $onhold_customers_dataTable_column['query_name'];
        }

        array_filter(array_map("trim", $aColumns));

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "customerID";

        /*
         * Paging
         */
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " .
                    mysql_real_escape_string($_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $column_key = $this->input->get("iSortCol_0");
            if (isset($this->onhold_customers_dataTable_columns[$column_key])) {
                $sOrder .= "ORDER BY " . $this->onhold_customers_dataTable_columns[$column_key]['orderBy_name'] . " " . mysql_real_escape_string($this->input->get('sSortDir_0'));
            }
        }


        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = "HAVING (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($this->onhold_customers_dataTable_columns[$i]['dataTable_options']['bSearchable']) && $this->onhold_customers_dataTable_columns[$i]['dataTable_options']['bSearchable'] == false) {
                    continue;
                } else {
                    $sWhere .= $this->onhold_customers_dataTable_columns[$i]['query_name'] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                }
            }
            //The following conditional applies a string search for both the firstName and lastName column if both were specified in the columns parameter.
            if (in_array("firstName", $aColumns) && in_array("lastName", $aColumns)) {
                $sWhere .= "CONCAT_WS(' ', firstName, lastName) LIKE \"%{$this->input->get("sSearch")}%\" OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ")";
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                $sWhere .= "AND ";
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
            }
        }


        /*
         * SQL queries
         * Get data to display
         */
        $select = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $aColumns);
        //Note, "1' is expected to by the followup Call type ID of new customer.
        $sQuery = "
            $select, customerID
            FROM orders
            JOIN orderItem ON orderItem.order_id = orders.orderID
            JOIN customer ON customer.customerID = orders.customer_id
            LEFT JOIN followupCall ON (customerID = followupCall.customer_id AND followupCallType_id = $followupCallType_id)
            WHERE orderPaymentStatusOption_id = 1
            AND statusDate < date_sub(NOW(), INTERVAL 1 day)
            AND orderStatusOption_id = 3
            AND invoice != 1
            AND orders.business_id={$this->business_id}
            AND followupCallType_id IS NULL
            GROUP BY customerID

            $sWhere

            $sOrder
            $sLimit
        ";
        $rQuery = $this->db->query($sQuery);
        $rResult = $rQuery->result_array();

        /* Data set length after filtering */
        $sQuery = "
        SELECT FOUND_ROWS() AS found_rows
    ";

        $aResultFilterTotal = $this->db->query($sQuery)->row();
        $iFilteredTotal = $aResultFilterTotal->found_rows;

        /* Total data set length */
        //Note, "1' is expected to by the followup Call type ID of new customer.
        $sQuery = $this->db->query("
            SELECT COUNT(DISTINCT orders.customer_id) AS total
            FROM orders
            JOIN customer ON customerID = orders.customer_id
            JOIN orderItem ON orderItem.order_id = orders.orderID
            LEFT JOIN followupCall ON (customerID = followupCall.customer_id AND followupCallType_id = $followupCallType_id)
            WHERE orderPaymentStatusOption_id = 1
            AND statusDate < date_sub(NOW(), INTERVAL 1 day)
            AND orderStatusOption_id = 3
            AND invoice != 1
            AND orders.business_id={$this->business_id}
            AND followupCallType_id IS NULL
    ");
        $aResultTotal = $sQuery->row();
        $iTotal = $aResultTotal->total;

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult as $aRow) {
            $row = array();
            $customerID = $aRow['customerID'];
            //The following statement renders a clickable link to the customer's detail view.
            $aRow['customerID'] = "<a target='_blank' href='/admin/customers/detail/{$aRow['customerID']}'> {$aRow['customerID']} </a>";
            for ($i = 0; $i < count($aColumns); $i++) {
                $keys = array_keys($aRow);
                $row[] = $aRow[$keys[$i]];
            }
            //The following statements render the Generate Call Script button for the customer.
            $row[] = "<a href='/admin/followupCalls/generate_followup_call_script?customerID=$customerID&followupCallType_id=$followupCallType_id'> Generate Call Script </a>";
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function get_firstOrder_customers_dataTable_columns()
    {
        $columns = array();
        foreach ($this->firstOrder_customers_dataTable_columns as $index => $firstOrder_customers_dataTable_column) {
            $columns[$index]['sTitle'] = $firstOrder_customers_dataTable_column['display_name'];
        }
        $columns[]['sTitle'] = "Generate Call Script";
        echo json_encode($columns);
    }

    public function get_new_signups_dataTable_columns()
    {
        $columns = array();
        foreach ($this->new_signups_dataTable_columns as $index => $new_signups_dataTable_column) {
            $columns[$index]['sTitle'] = $new_signups_dataTable_column['display_name'];
        }
        $columns[]['sTitle'] = "Generate Call Script";

        echo json_encode($columns);
    }

    public function get_new_signups_dataTable_data()
    {
        $followupCallType_id = 1;

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        foreach ($this->new_signups_dataTable_columns as $new_signups_dataTable_column) {
            $aColumns[] = $new_signups_dataTable_column['query_name'];
        }

        array_filter(array_map("trim", $aColumns));

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "customerID";

        /*
         * Paging
         */
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " .
                    mysql_real_escape_string($_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $column_key = $this->input->get("iSortCol_0");
            if (isset($this->new_signups_dataTable_columns[$column_key])) {
                $sOrder .= "ORDER BY " . $this->new_signups_dataTable_columns[$column_key]['orderBy_name'] . " " . mysql_real_escape_string($this->input->get('sSortDir_0'));
            }
        }


        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = "AND (";
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($this->new_signups_dataTable_columns[$i]['dataTable_options']['bSearchable']) && $this->new_signups_dataTable_columns[$i]['dataTable_options']['bSearchable'] == false) {
                    continue;
                } else {
                    $sWhere .= $this->new_signups_dataTable_columns[$i]['query_name'] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                }
            }
            //The following conditional applies a string search for both the firstName and lastName column if both were specified in the columns parameter.
            if (in_array("firstName", $aColumns) && in_array("lastName", $aColumns)) {
                $sWhere .= "CONCAT_WS(' ', firstName, lastName) LIKE \"%{$this->input->get("sSearch")}%\" OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ")";
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                $sWhere .= "AND ";
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
            }
        }

        /* DROP-4007: limit response to 180 days before now */        
        $sWhere .= " AND signupDate >= DATE_ADD(CURDATE(), INTERVAL -180 DAY)";

        /*
         * SQL queries
         * Get data to display
         */
        $select = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $aColumns);
        //Note, "1' is expected to by the followup Call type ID of new customer.
        // This can be written using a JOIN AS:
        // LEFT JOIN orders ON (orders.customer_id = customerID AND orders.bag_id != 0)
        // GROUP BY customerID
        // HAVING orderID IS NULL
        $sQuery = "
            $select, customerID
            FROM customer
            LEFT JOIN followupCall ON (followupCall.customer_id = customerID)
            LEFT JOIN orders ON (orders.customer_id = customerID AND bag_id != 0)
            WHERE
                followupCallID IS NULL
                AND signupDate != '0000-00-00 00:00:00'
                AND customer.business_id={$this->business_id}
                AND orderID IS NULL
                $sWhere
            $sOrder
            $sLimit
        ";

        $rQuery = $this->db->query($sQuery);

        if ($this->db->_error_message()) {
            throw new Database_Exception($this->db->_error_message(), $this->db->_error_number(), $this->db->last_query());
        }
        $rResult = $rQuery->result_array();

        /* Data set length after filtering */
        $sQuery = "
        SELECT FOUND_ROWS() AS found_rows
    ";

        $aResultFilterTotal = $this->db->query($sQuery)->row();
        $iFilteredTotal = $aResultFilterTotal->found_rows;

        /* Total data set length */
        //Note, "1' is expected to by the followup Call type ID of new customer.

        $sQuery = $this->db->query("
            SELECT COUNT(`" . $sIndexColumn . "`) AS total
            FROM customer
            LEFT JOIN followupCall ON (followupCall.customer_id = customerID)
            LEFT JOIN orders ON (orders.customer_id = customerID AND bag_id != 0)
            WHERE
                followupCallID IS NULL
                AND signupDate != '0000-00-00 00:00:00'
                AND customer.business_id={$this->business_id}
                AND orderID IS NULL
        ");

        if ($this->db->_error_message()) {
            throw new Database_Exception($this->db->_error_message(), $this->db->_error_number(), $this->db->last_query());
        }
        $aResultTotal = $sQuery->row();
        $iTotal = $aResultTotal->total;

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        $dateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat");
        foreach ($rResult as $aRow) {
            $row = array();
            $customerID = $aRow['customerID'];
            //The following statement renders a clickable link to the customer's detail view.
            $aRow['customerID'] = "<a target='_blank' href='/admin/customers/detail/{$aRow['customerID']}'> {$aRow['customerID']} </a>";

            $aRow['signupDate'] = convert_from_gmt_aprax($aRow['signupDate'], $dateFormat);
            for ($i = 0; $i < count($aColumns); $i++) {
                $keys = array_keys($aRow);
                $row[] = $aRow[$keys[$i]];
            }
            //The following statements render the Generate Call Script button.
            $row[] = "<a href='/admin/followupCalls/generate_followup_call_script?customerID=$customerID&followupCallType_id=$followupCallType_id'> Generate Call Script </a>";
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function get_firstOrder_customers_dataTable_data()
    {
        $followupCallType_id = 3;
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        foreach ($this->firstOrder_customers_dataTable_columns as $firstOrder_customers_dataTable_column) {
            $aColumns[] = $firstOrder_customers_dataTable_column['query_name'];
        }
        array_filter(array_map("trim", $aColumns));

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "orderID";

        /*
         * Paging
         */
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " .
                    mysql_real_escape_string($_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $column_key = $this->input->get("iSortCol_0");
            if (isset($this->firstOrder_customers_dataTable_columns[$column_key])) {
                $sOrder .= "ORDER BY " . $this->firstOrder_customers_dataTable_columns[$column_key]['orderBy_name'] . " " . mysql_real_escape_string($this->input->get('sSortDir_0'));
            }
        }


        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = "HAVING (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $this->firstOrder_customers_dataTable_columns[$i]['query_name'] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
            }

            //The following conditional applies a string search for both the firstName and lastName column if both were specified in the columns parameter.
            if (in_array("firstName", $aColumns) && in_array("lastName", $aColumns)) {
                $sWhere .= "CONCAT_WS(' ', firstName, lastName) LIKE \"%{$this->input->get("sSearch")}%\" OR ";
            }

            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ")";
        }

        /* DROP-4007: limit response to 180 days before now */     
        $limitDaysCondition = " AND dateCreated >= DATE_ADD(CURDATE(), INTERVAL -180 DAY)";

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                $sWhere .= "AND ";
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
            }
        }


        /*
         * SQL queries
         * Get data to display
         */
        $select = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $aColumns);
        //Note, "3' is expected to by the followup Call type ID of first order.

        $sQuery = "
            $select
            FROM orders
            JOIN customer ON customerID = orders.customer_id
            LEFT JOIN followupCall ON (customerID = followupCall.customer_id AND followupCallType_id = $followupCallType_id)
            WHERE orderStatusOption_id = 10
            AND bag_id != 0
            AND locker_id != 0
            AND orders.business_id = {$this->business_id}
            AND followupCallType_id IS NULL
            $limitDaysCondition
            GROUP BY customerID

            $sWhere

            $sOrder
            $sLimit
        ";

        $rQuery = $this->db->query($sQuery);
        $rResult = $rQuery->result_array();

        /* Data set length after filtering */
        $sQuery = "
        SELECT FOUND_ROWS() AS found_rows
    ";

        $aResultFilterTotal = $this->db->query($sQuery)->row();
        $iFilteredTotal = $aResultFilterTotal->found_rows;

        /* Total data set length */
        //Note, "3' is expected to by the followup Call type ID of first order.
        $sQuery = $this->db->query("
            SELECT COUNT(DISTINCT customerID) AS total
            FROM orders
            JOIN customer ON customer.customerID = orders.customer_id
            LEFT JOIN followupCall ON (customerID = followupCall.customer_id AND followupCallType_id = $followupCallType_id)
            WHERE orderStatusOption_id = 10
            AND bag_id != 0
            AND locker_id != 0
            AND orders.business_id = {$this->business_id}
            AND followupCallType_id IS NULL
    ");
        $aResultTotal = $sQuery->row();
        $iTotal = $aResultTotal->total;

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult as $aRow) {
            $row = array();

            $dateFormat = get_business_meta($this->business_id, "shortDateDisplayFormat");
            $aRow['dateCreated'] = convert_from_gmt_aprax($aRow['dateCreated'], $dateFormat, $this->business_id);
                
            $customerID = $aRow['customerID'];
            //The following statement renders a clickable link to the customer's detail view.
            $aRow['customerID'] = "<a target='_blank' href='/admin/customers/detail/{$aRow['customerID']}'> {$aRow['customerID']} </a>";
            for ($i = 0; $i < count($aColumns); $i++) {
                $keys = array_keys($aRow);
                $row[] = $aRow[$keys[$i]];
            }
            $row[] = "<a href='/admin/followupCalls/generate_followup_call_script?customerID=$customerID&followupCallType_id=$followupCallType_id'> Generate Call Script </a>";
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    /**
     * The following action renders the interface for managing the followup call settings.
     * Note, this aciton expects that the followup call type IDs match as follows:
     *  First Time Folloup -> 3
     *  Inactive -> 2
     *  New Customer -> 1
     *  Payment Hold -> 4
     */
    public function index()
    {
        if (in_bizzie_mode() && !is_superadmin()) {
            set_flash_message("error", "Only superadmins can access this when in superadmin mode");

            return redirect_with_fallback("/admin/");
        }

        $this->load->model('followupcallscript_model');

        $content['first_time_followup_followupCallType_id'] = 3;
        $content['first_time_followup_script_text'] = $this->followupcallscript_model->get($content['first_time_followup_followupCallType_id'], $this->business_id);

        $content['inactive_followupCallType_id'] = 2;
        $content['inactive_script_text'] = $this->followupcallscript_model->get($content['inactive_followupCallType_id'], $this->business_id);

        $content['new_customer_followupCallType_id'] = 1;
        $content['new_customer_script_text'] = $this->followupcallscript_model->get($content['new_customer_followupCallType_id'], $this->business_id);

        $content['payment_hold_followupCallType_id'] = 4;
        $content['payment_hold_script_text'] = $this->followupcallscript_model->get($content['payment_hold_followupCallType_id'], $this->business_id);

        $this->data['content'] = $this->load->view('admin/followupCalls/index', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Renders the view for displaying newly signed up customers.
     */
    public function view_new_signups()
    {
        $content = array();

        $this->data['content'] = $this->load->view('admin/followupCalls/view_new_signups', $content, true);
        $this->template->write("page_title", "View New Signups", true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Gathers the data and renders the view for viewing inactive customers.
     */
    public function view_inactive_customers()
    {
        $content = array();
        $this->data['content'] = $this->load->view('admin/followupCalls/view_inactive_customers', $content, true);
        $this->template->write("page_title", "View Inactive Customers", true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function view_onhold_customers()
    {
        //$content = array();

        $getHoldCustomers = "select customer_id,  sum(closedGross + closedDisc) as amtOwed, firstName, lastName, (select dateCalled from followupCall where followupCall.customer_id = orders.customer_id and followupCallType_id = 4 limit 1) as lastCall
                FROM orders
                JOIN customer ON customer.customerID = orders.customer_id
                WHERE orderPaymentStatusOption_id = 1
                AND dateCreated < date_sub(NOW(), INTERVAL 3 day)
                AND invoice != 1
                AND orders.business_id = $this->business_id
                and closedGross + closedDisc > 0
                group by customer_id
                order by amtOwed desc";
        $query = $this->db->query($getHoldCustomers);
        $content['holdCustomers'] = $query->result();

        $this->data['content'] = $this->load->view('admin/followupCalls/onhold_customers', $content, true);
        $this->template->write("page_title", "View Onhold Customers", true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Logs a call to the followup log table.
     * Expects the folloinwg POST parameters:
     *      customer_id :
     *      call_type :
     * The following POST parameter is optional:
     *      note :
     * @throws Exception
     */
    public function log()
    {
        $this->load->model("customer_model");
        $this->load->model("followupcalltype_model");

        $this->load->library("form_validation");
        $this->form_validation->set_rules("customer_id", "Customer ID", "required|numeric");
        $this->form_validation->set_rules("followupCallType_id", "Followup Call Type ID", "required|numeric");
        $this->form_validation->set_rules("dateCalled", "Date Called", "required");

        if ($this->form_validation->run()) {
            $customer_id = $this->input->post("customer_id");
            $followupCallType_id = $this->input->post("followupCallType_id");
            $dateCalled = $this->input->post("dateCalled");

            if ($this->input->post("note")) {
                $note = $this->input->post("note");
            } else {
                $note = "";
            }

            $employee_id = get_employee_id($this->buser_id);
            $gmtdate = convert_to_gmt_aprax($dateCalled, $this->business->timezone);
            if ($this->followupcall_model->insert($customer_id, $employee_id, $followupCallType_id, $gmtdate, $this->business_id, $note)) {
                $this->load->model("customer_model");
                $customer = $this->customer_model->get_by_primary_key($customer_id);
                $this->load->model("followupcalltype_model");
                $followupCallType = $this->followupcalltype_model->get_by_primary_key($followupCallType_id);
                set_flash_message("success", "Logged '{$followupCallType->name}' followup call for '" .  getPersonName($customer) . " ({$customer->customerID})'");
            } else {
                set_flash_message("error", "Failed to log followup call");
            }
            switch ($followupCallType_id) {
                case 1:
                    redirect("/admin/followupCalls/view_new_signups");
                    break;
                case 2:
                    redirect("/admin/followupCalls/view_inactive_customers");
                    break;
                case 3:
                    redirect("/admin/followupCalls/view_firstOrder_customers");
                    break;
                case 4:
                    redirect("/admin/followupCalls/view_onHold_customers");
                    break;
                default:
                    $this->goBack("/admin/followupCalls");
                    break;
            }
        }

        $this->goBack("/admin/followupCalls");
    }

    /**
     *  Expects customerID and 'followup_type' as GET parameters.
     */
    public function generate_followup_call_script()
    {
        $this->template->write("page_title", "Followup Call Script", true);
        $this->load->model('customer_model');

        if (!$this->input->get("customerID")) {
            set_flash_message("error", "'customerID' must be passed as a GET parameter.");
        }

        $customer_id = $this->input->get("customerID");

        if (!is_numeric($customer_id)) {
            set_flash_message("error", "'customerID' must be numeric.");
        }
        $content['customer'] = $this->customer_model->get_by_primary_key($customer_id);
        if ($content['customer']->business_id != $this->business->businessID) {
            set_flash_message('error', "Customer does not belong to business {$this->business->companyName}");
            redirect("/admin/followupCalls");
        }
        if (!$this->input->get("followupCallType_id")) {
            set_flash_message("error", "'followupCallType_id' must be passed as a GET parameter.");
        }

        $content['followupCallType_id'] = $this->input->get("followupCallType_id");
        if (!is_numeric($content['followupCallType_id'])) {
            set_flash_message("error", "'followupCallType_id' must be numeric.");
        }

        if ($this->input->post("update")) {
            $customer = new Customer($customer_id);
            $customer->address1      = $this->input->post('address1');
            $customer->address2      = $this->input->post('address2');
            $customer->email         = $this->input->post('email');
            $customer->internalNotes = $this->input->post('internalNotes');

            $geo = geocode($customer->address1, $customer->city, $customer->state);
            $customer->lat = str_replace(',', '.', $geo['lat']);
            $customer->lon = str_replace(',', '.', $geo['lng']);

            $customer->save();

            $this->customer_model->update_customerNotes($customer_id, $this->input->post('custNotes'));

            set_flash_message('success', 'Customer has been updated');
            $this->goBack("/admin/customers/followupCalls");
        }


        $content['ordCount'] = $this->customer_model->get_order_count($customer_id);
        $content['ordTot'] = $this->customer_model->get_order_total($customer_id);
        $this->load->model("creditcard_model");
        $content['card'] = $this->creditcard_model->get_one(array("customer_id" => $customer_id));
        $content['lastOrder'] = $this->customer_model->get_lastOrder($customer_id);

        $this->load->model("customerdiscount_model");
        $content['discounts'] = $this->customerdiscount_model->get_discounts($customer_id, $this->business_id, true);
        $content['history'] = $this->customer_model->get_history($customer_id);

        $this->load->model('businesssetting_model');
        $this->businesssetting_model->setting = $this->input->get("callType") . 'Script';
        $this->businesssetting_model->business_id = $this->business_id;

        $this->load->model("followupcallscript_model");
        $content['followup_call_script'] = $this->followupcallscript_model->get($content['followupCallType_id'], $this->business_id);

        $this->db->join("locationType", "location.locationType_id=locationType.locationTypeID");
        $this->db->select("location.locationID,location.address, location.description, locationType.name AS locationTypeName");
        $content['defaultLocation'] = $this->db->get_where("location", array("locationID" => $content['customer']->location_id))->row();

        // The following statements retrieve the customer note and followup call history and combine the two histories into one array, sorted by dateCreated.
        $this->load->model("customerhistory_model");

        $customer_notes_history = $this->customerhistory_model->get_history($customer_id, $this->business_id);

        $this->load->model("followupcall_model");
        $followup_history = $this->followupcall_model->get_history($customer_id, $this->business_id);

        $content['pastCalls'] = $followup_history;

        $content['customer_notes_history'] = $customer_notes_history;

        $this->load->model("followupcalltype_model");
        $content['followupCallType_ids'] = $this->followupcalltype_model->get_all();
        $this->load->model("location_model");
        $content['locations'] = $this->location_model->get_all_as_codeigniter_dropdown_for_business($this->business->businessID);
        $content['business'] = $this->business;
        $this->data['content'] = $this->load->view('admin/followupCalls/generate_followup_call_script', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

}
