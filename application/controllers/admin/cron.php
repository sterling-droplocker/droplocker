<?php

set_time_limit(0);

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\CustomerHistory;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\DropLockerEmail;
use App\Libraries\DroplockerObjects\CronLog;
use App\Libraries\DroplockerObjects\ReminderLog;
use App\Libraries\DroplockerObjects\LaundryPlan;
use App\Libraries\DroplockerObjects\EverythingPlan;
use DropLocker\Util\Lock;
use App\Libraries\DroplockerObjects\App\Libraries\DroplockerObjects;

class Cron extends MY_Cron_Controller
{
    public $emails = array();

    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
    }

    /**
     * removes all the automat entries that are over a year old
     *
     * @return int affected_rows()
     */
    public function cleanAutomat()
    {
        $sql = 'DELETE FROM automat where fileDate < DATE_ADD(NOW(), INTERVAL -1 year);';
        $this->db->query($sql);
        echo $this->db->affected_rows();
    }

    /**
     * Sends out emails that are pending in the email table
     *
     * @return unknown
     */
    public function processEmailQueue()
    {
        $email = new DropLockerEmail();
        $results = $email->processEmails();

        return $results;
    }

    /**
     * sets the closed gross and closed discount fields in the orders table
     *
     * cron settings
     *  00 06 * * *
     *  00 06 * * *
     */
    public function closedDisc()
    {
        $sql = "UPDATE orders myOrderTable
            SET closedGross = (SELECT sum(qty * unitPrice)
            FROM orderItem
            WHERE order_id = myOrderTable.orderID
            GROUP BY order_id)";
        $this->db->query($sql);

        $sql = "UPDATE orders myOrderTable
            SET closedDisc = (SELECT sum(chargeAmount)
            FROM orderCharge
            WHERE order_id = myOrderTable.orderID
            GROUP BY order_id)";
        $this->db->query($sql);
    }

    /**
     * Emails a report to the customer support email when there are duplicate discounts
     * applied. customer support can then remove the duplicate discounts manually
     */
    public function find_orders_with_duplicate_discounts()
    {
        $orders_with_duplicate_discounts_query = $this->db->query("

            SELECT orderID, count(orderChargeID) as count, business_id
            FROM orderCharge
            JOIN orders ON orders.orderID=order_id
            WHERE orders.orderStatusOption_id=3
            GROUP BY order_id, customerDiscount_id, chargeType HAVING count > 1
            ORDER BY orderChargeID DESC ");

        $orders_with_duplicate_discounts_result = $orders_with_duplicate_discounts_query->result();

        $admin_url = get_admin_url();
        if ($orders_with_duplicate_discounts_result) {
            $businesses = array();

            foreach ($orders_with_duplicate_discounts_result as $order_with_duplicate_discount) {
                if (!array_key_exists($order_with_duplicate_discount->business_id, $businesses)) {
                    $businesses[$order_with_duplicate_discount->business_id] = "
                        <p> The following orders have duplicate discounts, please remove them. </p>
                        <ul>
                    ";
                }
                $businesses[$order_with_duplicate_discount->business_id] .= "<li> <a href='{$admin_url}/admin/orders/details/{$order_with_duplicate_discount->orderID}'> {$order_with_duplicate_discount->orderID} </a> </li>";
            }

            foreach ($businesses as $index => $business) {
                $businesses[$index] .= "</ul>";
                //echo "Sending email to business $index : {es[$index]}";
                queueEmail(SYSTEMEMAIL, "Droplocker Reports", get_business_meta($index, 'customerSupport'), 'Orders with Duplicate Discounts', $businesses[$index], array(), null, $index);
            }
        } else {
            echo("No orders with duplicate discounts found.");
        }
    }

    /**
     * Sends email to customer support if an order has been delivered with a blank customer
     */
    public function emailBlankCustomerNotification()
    {
        // get all the blank customer id's
        $sql = "SELECT blank_customer_id FROM business where blank_customer_id !=0";
        $query = $this->db->query($sql);
        foreach ($query->result() as $business) {
            $array[] = $business->blank_customer_id;
        }

        $this->db->join('bag', 'bag.bagID = orders.bag_id', 'left');
        $this->db->join('locker', 'lockerID = orders.locker_id', 'left');
        $this->db->where_in('orders.customer_id', $array);
        $this->db->where_in('orderStatusOption_id', array(9,10,13));
        $this->db->order_by('orders.dateCreated', 'DESC');
        $this->db->select('orders.business_id, lockerName, orders.locker_id, location_id,  bagNumber, orders.dateCreated, orderID');
        $query = $this->db->get('orders');
        $orders = $query->result();

        foreach ($orders as $order) {
            $this->db->where('order_id', $order->orderID);
            $this->db->where_in('orderStatus.orderStatusOption_id', array(9,13));
            $this->db->group_by('order_id');
            $query = $this->db->get('orderStatus');

            $problem = $query->row();
            if (!empty($problem)) {
                $order->deliveryDate = $problem->date;
                $problems[$order->business_id][] = $order;
            }
        }
        $emailIDs = array();
        $admin_url = get_admin_url();
        foreach ($problems as $business_id=>$orders) {
            $table = "<h2>Deliveries to blank customers</h2><table width='800'><tr style='background-color:#eaeaea'><th>ORDER</th><th>LOCATION</th><th>LOCKERID</th><th>BAGID</th><th>DATE</th><th>DELIVERY</th></tr>";
            foreach ($orders as $order) {
                try {
                    $location = new Location($order->location_id);
                    $table .= "<tr><td><a href='$admin_url/admin/orders/details/{$order->orderID}'>{$order->orderID}</td><td>{$location->address}</td><td>{$order->lockerName}</td><td>{$order->bagNumber}</td><td>".convert_from_gmt_aprax($order->dateCreated, 'm/d/y H:i', $order->business_id)."</td><td>".convert_from_gmt_aprax($order->deliveryDate, 'm/d/y H:i', $order->business_id)."</td></tr>";
                } catch (Exception $e) {
                    $table .= "<tr><td><a href='$admin_url/admin/orders/details/{$order->orderID}'>{$order->orderID}</td><td>LOCATION [{$order->location_id}] NOT FOUND</td><td>{$order->lockerName}</td><td>{$order->bagNumber}</td><td>".convert_from_gmt_aprax($order->dateCreated, 'm/d/y H:i', $order->business_id)."</td><td>".convert_from_gmt_aprax($order->deliveryDate, 'm/d/y H:i', $order->business_id)."</td></tr>";
                }
            }
            $table .= "</table><br><br>For more information on this email, visit <a href='http://droplocker.com/wiki/software/deliveries-to-blank-customers-report.html'>http://droplocker.com/wiki/software/deliveries-to-blank-customers-report.html</a>";

            $this->db->insert('cronLog', array('results'=>$table, 'job'=>__METHOD__));
            $emailID = queueEmail(SYSTEMEMAIL, "Droplocker Reports", get_business_meta($business_id, 'customerSupport'), 'Deliveries with blank customers report', $table, array(), null, $business_id);
        }

        echo $emailID;
    }

    /**
     * emails all the open tickets to the people on the ticket email list
     */
    public function email_tickets()
    {
        $ticketObj = new \App\Libraries\DroplockerObjects\Ticket();

        $getEmps = "select ticket.business_id, ticket.assignedTo  from ticket
                    inner join ticketStatus t_s ON t_s.ticketStatusID = ticket.ticketStatus_id
                    where t_s.description != 'Closed'
                    group by business_id, assignedTo";
        $query = $this->db->query($getEmps);
        $emps = $query->result();

        // loop all employees and send out the tickets
        foreach ($emps as $emp) {
            $results = array();
            $results['emailID'] = $ticketObj->sendDigest($emp->business_id, $emp->assignedTo);
        }
        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    /**
     * creates claims from entries in the scheduledOrders table
     * runs only for business with matchig scheduled_orders_time
     *
     * This needs to run hourly
     */
    public function schedule_claims($time = null)
    {
        if (is_null($time)) {
            $time = date('H:00:00'); // Get the current hour
        }

        $selected_business = $this->db->query("SELECT businessID, scheduled_orders_time FROM business WHERE scheduled_orders_time = ?", array($time))->result(); // select business that match the current hour
        foreach ($selected_business as $business) {
            $this->scheludeClaimsForBusiness($business->businessID);
        }
    }

    /**
     * NOT A CRON JOB
     * Creates claims from entries in the scheduledOrders table for the specified business_id
     *
     * @param int $business_id
     */
    protected function scheludeClaimsForBusiness($business_id)
    {
        $select1 = "select * from scheduledOrders where business_id = ?";
        $lines = $this->db->query($select1, array($business_id))->result();
        $results = array();
        foreach ($lines as $line) {
            //make sure a claim doesn't already exist
            $delete1 = "UPDATE claim SET active = 0 where customer_id = '$line->customer_id';";
            $delete = $this->db->query($delete1);

            //then create new claim
            $today = date("w");
            if (stristr($line->dayOfWeek, $today)) {
                $insert1 = "INSERT into claim (customer_id, locker_id, updated, `active`, business_id)
                    VALUES ('$line->customer_id', '$line->locker_id', now(), 1, ".$line->business_id.");";
                $this->db->query($insert1);
                $claim_id = $this->db->insert_id();
                $results[] = $claim_id;

                Email_Actions::send_transaction_emails(array(
                    'do_action' => 'scheduled_claim',
                    'customer_id' => $line->customer_id,
                    'claim_id' => $claim_id,
                    'business_id' => $line->business_id
                ));
            }
        }

        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    /**
     * Sends reminders to customers that have not had any activity is a certain period of time.
     * Set up in admin reminder section
     */
    public function inactiveCustomerReminders()
    {
        $reminders = Reminder::search_aprax(array('couponType'=>"inactiveCustomer", 'status'=>'active'));
        foreach ($reminders as $reminder) {
            $business = new Business($reminder->business_id);
            setlocale(LC_ALL, $business->locale);

            $this->load->model('language_model');

            $language_by_id = array();
            foreach ($this->language_model->findAllForBusiness($reminder->business_id) as $language) {
                $language_by_id[$language->business_languageID] =  $language;
            }

            // expire any discounts for this business
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->expireCustomerDiscount($reminder->business_id);

            $results = array();
            $expireDate = date("m/d/Y", mktime(date('h'), date('i'), date('s'), date('m'), date('d')+ ($reminder->expireDays + daysAdd()), date('Y')));

            $day_start = date('Y-m-d 00:00:00', strtotime("- {$reminder->days} days"));
            $day_end = date('Y-m-d 23:59:59', strtotime("- {$reminder->days} days"));

            //Find when their last order was completed and how many orders they have placed
            $inactiveCustomersQuery = "
            SELECT customerID, customer.email, o.orderID, o.customer_id, customer.business_id, customer.default_business_language_id,

                (SELECT count(o2.orderID) FROM orders o2
                    WHERE o2.customer_id = o.customer_id
                    AND o2.dateCreated > ?
                    AND o2.bag_id != 0
                ) AS orderTotal,

                (SELECT count(c.claimID) FROM claim c
                    WHERE c.customer_id = o.customer_id
                    AND c.created > ?
                    AND c.active = 1
                ) AS claimTotal,

                (SELECT count(customerDiscountID) FROM customerDiscount
                    WHERE frequency = 'every order'
                    AND customerDiscount.customer_id = o.customer_id
                ) as totalDiscounts

            FROM orders AS o
            INNER JOIN customer ON customerID = o.customer_id
            WHERE
                o.orderStatusOption_id = 10
                AND customer.type != 'wholesale'
                AND customer.noEmail != 1
                AND o.dateCreated >= ?
                AND o.dateCreated <= ?
                AND o.bag_id != 0
                AND o.business_id = ?
            HAVING
                orderTotal < 1
                AND claimTotal < 1
                AND totalDiscounts < 1";

            try {
                $note = 'Customer has not used us in '.$reminder->days.' days';
                $customers = $this->db->query($inactiveCustomersQuery, array(
                    $day_end,
                    $day_end,
                    $day_start,
                    $day_end,
                    $reminder->business_id
                ))->result();

                foreach ($customers as $customer) {
                    $discount = '';
                    // do not continue if a customer has an active laundry plan
                    $laundryPlans = LaundryPlan::getActiveLaundryPlan($customer->customer_id);
                    $everythingPlans = EverythingPlan::getActiveEverythingPlan($customer->customer_id);
                    if (count($laundryPlans) > 0 || count($everythingPlans) > 0) {
                        continue;
                    }

                    if (!empty($language_by_id[$customer->default_business_language_id]->locale)) {
                        setlocale(LC_TIME, $language_by_id[$customer->default_business_language_id]->locale);
                    } elseif (!empty($language_by_id[$business->default_business_language_id]->locale)) {
                        setlocale(LC_TIME, $language_by_id[$business->default_business_language_id]->locale);
                    } else {
                        setlocale(LC_TIME, $business->locale);
                    }

                    $discountNote = get_translation(
                        "inactive_customer_reminder",
                        "Email Reminders",
                        array('days' => $reminder->days,),
                        "Customer has not used us in %days% days",
                        $customer->customer_id
                    );

                    $totalExtDesc = $this->db->query("SELECT count(*) AS total FROM customerDiscount
                        WHERE extendedDesc = ? AND customer_id = ?", array(
                        $discountNote,
                        $customer->customer_id
                    ))->result();

                    if ($totalExtDesc[0]->total > 0) {
                        continue;
                    }

                    $activeDiscounts = CustomerDiscount::search_aprax(array(
                        'customer_id' => $customer->customer_id,
                        'business_id' => $customer->business_id,
                        'active'=>1
                    ));

                    if (!$reminder->noDiscount && !$activeDiscounts) {
                        if ($discount = $this->customerdiscount_model->addDiscount(
                            $customer->customer_id,
                            $reminder->amount,
                            $reminder->amountType,
                            'one time',
                            $discountNote,
                            $discountNote,
                            $expireDate,
                            0,
                            $reminder->maxAmt
                        )) {
                            $options['customerDiscount_id'] = $discount;
                        } else {
                            //FIXME: $discountNote will not be saved because of the continue bellow, is this OK?
                            $discountNote = "Error trying to create discount (Customer has not used us in '.$reminder->days.' days). Most likely the customer already has the same active discount in their account.";
                            continue;
                        }
                    }

                    /*
                     * $discount != false [discount was just created]
                     * sizeof($activeDiscounts) [The customer already has an active discount]
                     * $reminder->noDiscount [We are only sending an email, not creating a discount]
                     */
                    if ($discount || count($activeDiscounts) || $reminder->noDiscount) {
                        $business = new Business($reminder->business_id);
                        $options['customer_id'] = $customer->customer_id;
                        $options['business_id'] = $customer->business_id;
                        $data = get_macros($options);

                        $business_language_id = get_customer_business_language_id($customer->customer_id);
                        $this->load->model("reminder_email_model");
                        $reminder_email = $this->reminder_email_model->get_one(array(
                            "reminder_id" => $reminder->reminderID,
                            "business_language_id" => $business_language_id
                        ));

                        $subject = email_replace($reminder_email->subject, $data);
                        $body = email_replace($reminder_email->body, $data);

                        $domain = Business::getAccountDomain($reminder->business_id);
                        $unsuscribe = get_translation(
                            "unsuscribe",
                            "newsletter",
                            array(
                                'companyName' => $business->companyName,
                                'unsubscribe' => "https://".$domain."/main/unsubscribe",
                            ),
                            "To be removed from all %companyName% marketing emails: <a href='%unsubscribe%'>Unsubscribe</a>",
                            $customer->customer_id
                        );

                        $body .= "<br style='clear:left'><div style='position: absolute !important;bottom: 0px !important;clear:both;display:table;overflow: hidden;margin: 0 auto;visibility: visible;font-size:12px; padding:5px;'>$unsuscribe</div>";

                        $from = get_business_meta($reminder->business_id, 'customerSupport');
                        $from_name = get_business_meta($reminder->business_id, 'from_email_name');
                        if ($emailID = queueEmail($from, $from_name, $customer->email, $subject, $body, array(), null, $reminder->business_id, $customer->customer_id)) {
                            $customerHistory = new CustomerHistory();
                            $customerHistory->store($customer->customer_id, $customer->business_id, "Internal Note", "Automated email sent to customer [InactiveCustomerReminder]: ".$discountNote);
                        }

                        $results[$reminder->business_id][$reminder->couponType][$customer->email]['discount'] = $discount;

                        //log that this reminder was sent
                        $log = array();
                        $log = new ReminderLog();
                        $log->reminder_id = $reminder->reminderID;
                        $log->customerDiscount_id = $discount;
                        $log->customer_id = $customer->customer_id;
                        $log->email_id = $emailID;
                        $log->save();
                    }
                }

                $this->db->insert('cronLog', array('results'=>serialize($results), 'job'=>__METHOD__));
            } catch (Exception $e) {
                $body = "ERROR:<br>".$e->getMessage()."<BR><BR>DETAILS: <br>".formatArray($reminder);
                send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, 'Exception caught on inactiveCustomerReminders', $body);
            }
        }

        return $results;
    }

    /**
     * Sends a coupon reward to customer that have reached a certain order number.
     * Set up in admin reminder section
     */
    public function orderTotalReminders()
    {
        $reminders = Reminder::search_aprax(array('couponType'=>"orderTotal", 'status'=>'active'));
        foreach ($reminders as $reminder) {
            $business = new Business($reminder->business_id);
            setlocale(LC_ALL, $business->locale);

            $this->load->model('language_model');

            $language_by_id = array();
            foreach ($this->language_model->findAllForBusiness($reminder->business_id) as $language) {
                $language_by_id[$language->business_languageID] =  $language;
            }

            // expire any discounts for this business
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->expireCustomerDiscount($reminder->business_id);

            $results = array();
            $expireDate = date("m/d/Y", mktime(date('h'), date('i'), date('s'), date('m'), date('d')+ ($reminder->expireDays + daysAdd()), date('Y')));

            // find the customers that made an order X days ago and their order total = reward total
            $sql = "SELECT  distinct customer_id, customer.email, customer.business_id, customer.default_business_language_id,
                (SELECT count(distinct date_format(o.dateCreated, '%m-%d-%y')) from orders o WHERE o.customer_id = orders.customer_id and o.bag_id <> 0 and noEmail != 1) as orderTotal
                from orders
                INNER JOIN customer on customerID = orders.customer_id
                WHERE orders.dateCreated > DATE_SUB(CURDATE(), INTERVAL 5 DAY)
                AND orders.dateCreated < DATE_SUB(CURDATE(), INTERVAL 4 DAY) 
                AND orders.bag_id <> 0
                AND orders.orderStatusOption_id = 10
                AND orders.business_id = {$reminder->business_id}
                AND orders.customer_id IS NOT NULL
                AND orders.customer_id != 0
                AND customer.type <> 'wholesale'
                HAVING orderTotal = {$reminder->orders}";
            try {
                $customers = $this->db->query($sql)->result();
                $business = new Business($reminder->business_id);
                foreach ($customers as $customer) {
                    //if this customer already got this reminder, then skip
                    $findReminder = "select * from reminderLog where customer_id = $customer->customer_id and reminder_id = $reminder->reminderID";
                    $reminders = $this->db->query($findReminder)->result();
                    if (!empty($reminders)) {
                        continue;
                    }

                    $discount = 0;
                    // do not continue if a customer has an active laundry plan
                    $laundryPlans = \App\Libraries\DroplockerObjects\LaundryPlan::getActiveLaundryPlan($customer->customer_id);
                    $everythingPlans = \App\Libraries\DroplockerObjects\EverythingPlan::getActiveEverythingPlan($customer->customer_id);
                    if (sizeof($laundryPlans) > 0 || sizeof($everythingPlans) > 0) {
                        continue;
                    }

                    if (!empty($language_by_id[$customer->default_business_language_id]->locale)) {
                        setlocale(LC_TIME, $language_by_id[$customer->default_business_language_id]->locale);
                    } elseif (!empty($language_by_id[$business->default_business_language_id]->locale)) {
                        setlocale(LC_TIME, $language_by_id[$business->default_business_language_id]->locale);
                    } else {
                        setlocale(LC_TIME, $business->locale);
                    }

                    //if this reminder is supposed to have a discount, give it to the customer
                    $discountNote = get_translation(
                        "order_total_reminder",
                        "Email Reminders",
                        array('orders' => $reminder->orders,),
                        "Customer has placed %orders% orders auto coupon",
                        $customer->customer_id
                    );
                    if ($reminder->noDiscount == 0) {
                        try {
                            $discount = $this->customerdiscount_model->addDiscount(
                                $customer->customer_id,
                                $reminder->amount,
                                $reminder->amountType,
                                'one time',
                                $discountNote,
                                $discountNote,
                                $expireDate,
                                0,
                                $reminder->maxAmt
                            );
                        } catch (Exception $e) {
                            $body = "ERROR:<br>".$e->getMessage();
                            send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, 'Exception caught on addDiscount during cron orderTotalReminders.', $body);
                        }

                        if (!$discount) {
                            print_r("Discount not made");
                            //could not create coupon, usually because it already exists.  Therefore, don't send an email to this customer.
                            continue;
                        }
                        print_r("Created discount" . $customer->customer_id);
                    }

                    $options['customer_id'] = $customer->customer_id;
                    $options['business_id'] = $customer->business_id;
                    if ($discount != 0) {
                        $options['customerDiscount_id'] = $discount;
                    }
                    $data = get_macros($options);
                    $business_language_id = get_customer_business_language_id($customer->customer_id);

                    $this->load->model("reminder_email_model");
                    $reminder_email = $this->reminder_email_model->get_one(array("business_language_id" => $business_language_id, "reminder_id" => $reminder->reminderID));
                    $subject = email_replace($reminder_email->subject, $data);
                    $body = email_replace($reminder_email->body, $data);

                    $domain = Business::getAccountDomain($reminder->business_id);
                    $unsuscribe = get_translation(
                        "unsuscribe",
                        "newsletter",
                        array(
                            'companyName' => $business->companyName,
                            'unsubscribe' => "https://".$domain."/main/unsubscribe",
                        ),
                        "To be removed from all %companyName% marketing emails: <a href='%unsubscribe%'>Unsubscribe</a>",
                        $customer->customer_id
                    );

                    $body .= "<br style='clear:left'><div style='font-size:12px; padding:5px;'>$unsuscribe</div>";
                    if ($emailID = queueEmail(get_business_meta($reminder->business_id, 'customerSupport'), get_business_meta($reminder->business_id, 'from_email_name'), $customer->email, $subject, $body, array(), null, $reminder->business_id, $customer->customer_id)) {
                        $customerHistory = new CustomerHistory();
                        $customerHistory->store($customer->customer_id, $customer->business_id, "Internal Note", "Automated email sent to customer [orderTotalReminder]: ".$discountNote);
                    }
                    $results[$reminder->business_id][$reminder->couponType][$customer->email]['discount'] = $discount;

                    //log that this reminder was sent
                    $log = array();
                    $log = new ReminderLog();
                    $log->reminder_id = $reminder->reminderID;
                    $log->customerDiscount_id = $discount;
                    $log->customer_id = $customer->customer_id;
                    $log->email_id = $emailID;
                    $log->save();
                }
                $this->db->insert('cronLog', array('results'=>serialize($results), 'job'=>__METHOD__));
            } catch (Exception $e) {
                $body = "ERROR:<br>".$e->getMessage()."<BR><BR>DETAILS: <br>".$discountNote."<br>".formatArray($reminder)."<br>".formatArray($discount)."<br>".formatArray($customer)."<br>".formatArray($options);
                send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, 'Exception caught on orderTotalReminders', $body);
            }
        }

        return $results;
    }

    /**
     * sends reminders to new customer that have not used the service
     * Set up in admin reminder section
     */
    public function newCustomerReminders()
    {
        $reminders = Reminder::search_aprax(array('couponType'=>"newCustomer", 'status'=>'active'));
        foreach ($reminders as $reminder) {
            $business = new Business($reminder->business_id);
            setlocale(LC_ALL, $business->locale);

            $this->load->model('language_model');

            $language_by_id = array();
            foreach ($this->language_model->findAllForBusiness($reminder->business_id) as $language) {
                $language_by_id[$language->business_languageID] =  $language;
            }

            // expire any discounts for this business
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->expireCustomerDiscount($reminder->business_id);

            $results = array();
            $expireDate = date("m/d/Y", mktime(date('h'), date('i'), date('s'), date('m'), date('d')+ ($reminder->expireDays + daysAdd()), date('Y')));
            $query1 = "SELECT customer.*,

                    (SELECT count(orderID) FROM orders
                        WHERE customer_id = customerID
                        AND bag_id != 0
                    ) AS orderTotal,

                    (SELECT count(c.claimID) FROM claim c
                        WHERE c.customer_id = customerID
                        AND c.active = 1
                    ) AS claimTotal

                FROM customer
                WHERE
                    date_format(signupDate, '%Y-%m-%d') = date_format(DATE_SUB(CURDATE(), INTERVAL ".$reminder->days." DAY), '%Y-%m-%d')
                    AND type <> 'wholesale'
                    AND customer.business_id = {$reminder->business_id}
                    AND noEmail <> 1
                HAVING
                    orderTotal < 1
                    AND claimTotal < 1";

            try {
                $customers = $this->db->query($query1)->result();

                $this->load->helper('actions');
                foreach ($customers as $customer) {
                    $discount = '';
                    // do not continue if a customer has an active laundry plan
                    $laundryPlans = \App\Libraries\DroplockerObjects\LaundryPlan::getActiveLaundryPlan($customer->customer_id);
                    $everythingPlans = \App\Libraries\DroplockerObjects\EverythingPlan::getActiveEverythingPlan($customer->customer_id);
                    if (count($laundryPlans) > 0 || count($everythingPlans) > 0) {
                        continue;
                    }

                    if (!empty($language_by_id[$customer->default_business_language_id]->locale)) {
                        setlocale(LC_TIME, $language_by_id[$customer->default_business_language_id]->locale);
                    } elseif (!empty($language_by_id[$business->default_business_language_id]->locale)) {
                        setlocale(LC_TIME, $language_by_id[$business->default_business_language_id]->locale);
                    } else {
                        setlocale(LC_TIME, $business->locale);
                    }

                    //if this reminder is supposed to have a discount, give it to the customer
                    $discountNote = get_translation(
                        "new_customer_reminder",
                        "Email Reminders",
                        array('days' => $reminder->days,),
                        "Customer never used us %days% days after registering",
                        $customer->customerID
                    );

                    $activeDiscounts = CustomerDiscount::search_aprax(array('customer_id'=>$customer->customerID, 'business_id'=>$customer->business_id, 'active'=>1));
                    if (!$activeDiscounts) {
                        if (!$discount = $this->customerdiscount_model->addDiscount(
                            $customer->customerID,
                            $reminder->amount,
                            $reminder->amountType,
                            'one time',
                            $discountNote,
                            $discountNote,
                            $expireDate,
                            0,
                            $reminder->maxAmt
                        )) {
                            $discountNote = "Error trying to create discount (Customer never used us '.$reminder->days.' days after registering). Most likely the customer already has the same active discount in their account.";
                            continue;
                        } else {
                            $options['customerDiscount_id'] = $discount;
                        }
                    }

                    $business = new Business($reminder->business_id);
                    if ($discount!=false || sizeof($activeDiscounts)>0) {
                        $options['customer_id'] = $customer->customerID;
                        $options['business_id'] = $customer->business_id;
                        $data = get_macros($options);

                        $business_language_id = get_customer_business_language_id($customer->customerID);
                        $this->load->model("reminder_email_model");
                        $reminder_email = $this->reminder_email_model->get_one(array("reminder_id" => $reminder->reminderID, "business_language_id" => $business_language_id));

                        if (empty($reminder_email)) {
                            trigger_error("Could not find reminder email for reminder ID '{$reminder->reminderID}' and business language ID '$business_language_id'");
                        } else {
                            $subject = email_replace($reminder_email->subject, $data);
                            $body = email_replace($reminder_email->body, $data);

                            $domain = Business::getAccountDomain($reminder->business_id);
                            $unsuscribe = get_translation(
                                "unsuscribe",
                                "newsletter",
                                array(
                                    'companyName' => $business->companyName,
                                    'unsubscribe' => "https://".$domain."/main/unsubscribe",
                                ),
                                "To be removed from all %companyName% marketing emails: <a href='%unsubscribe%'>Unsubscribe</a>",
                                $customer->customerID
                            );

                            $body .= "<br style='clear:left'><div style='font-size:12px; padding:5px;'>$unsuscribe</div>";
                            if ($emailID = queueEmail(get_business_meta($reminder->business_id, 'customerSupport'), get_business_meta($reminder->business_id, 'from_email_name'), $customer->email, $subject, $body, array(), null, $reminder->business_id, $customer->customerID)) {
                                $customerHistory = new CustomerHistory();
                                $customerHistory->store($customer->customerID, $customer->business_id, "Internal Note", "Automated email sent to customer [newCustomerReminder]: ".$discountNote);
                            }

                            $results[$reminder->business_id][$reminder->couponType][$customer->email]['discount'] = $discount;
                            //log that this reminder was sent
                            $log = array();
                            $log = new ReminderLog();
                            $log->reminder_id = $reminder->reminderID;
                            $log->customerDiscount_id = $discount;
                            $log->customer_id = $customer->customerID;
                            $log->email_id = $emailID;
                            $log->save();
                        }
                    }
                }
            } catch (Exception $e) {
                $body = "ERROR:<br>".$e->getMessage()."<BR><BR>DETAILS: <br>".formatArray($reminder);
                send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, 'Exception caught on newCustomerReminders', $body);
            }
        }

        return $results;
    }

    /**
     * checks the email log for failed emails.
     * Sends a notification if more than 10 per hour
     */
    public function checkFailedEmails()
    {
        $sql = "SELECT count(*) as total FROM email WHERE status = 'fail'
                AND sentDateTime > date_add(NOW(), INTERVAL -1 HOUR)";

        $total = $this->db->query($sql)->row()->total;

        if ($total >= 10) {
            send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "There have been $total failed emails in the last hour", "There have been $total failed emails in the last hour.<br>Check the email server!! ");
        }
    }

    /**
     * Sends marketing newsletters
     */
    public function sendNewsletters()
    {
        // check if another instance running
        if (!Lock::acquire("sendNewsletters")) {
            return;
        }

        $sql = "SELECT newsletterQueueID, customerID, customer.business_id, customer.email, subject, body, newsletterID, companyName FROM newsletterQueue q
                INNER JOIN customer on customerID = q.customer_id
                INNER JOIN newsletter n ON newsletterID = q.newsletter_id
                INNER JOIN business ON businessID = customer.business_id
                ORDER BY newsletterQueueID
                LIMIT 200";
        $letters = $this->db->query($sql)->result();

        foreach ($letters as $letter) {
            $business = new Business($letter->business_id);
            setlocale(LC_ALL, $business->locale);

            $options['customer_id'] = $letter->customerID;
            $options['business_id'] = $letter->business_id;
            $data = get_macros($options);
            $subject = email_replace($letter->subject, $data);
            $body = email_replace($letter->body, $data);
            $domain = Business::getAccountDomain($letter->business_id);

            $unsuscribe = get_translation(
                "unsuscribe",
                "newsletter",
                array(
                    'companyName' => $letter->companyName,
                    'unsubscribe' => "https://".$domain."/main/unsubscribe",
                ),
                "To be removed from all %companyName% marketing emails: <a href='%unsubscribe%'>Unsubscribe</a>",
                $letter->customerID
            );

            $body .= "<br style='clear:left'><div style='font-size:10px'>$unsuscribe</div>";

            $this->db->delete('newsletterQueue', array('newsletterQueueID'=>$letter->newsletterQueueID));
            $from = get_business_meta($letter->business_id, 'customerSupport');
            $fromName = get_business_meta($letter->business_id, 'from_email_name');
            if (send_email($from, $fromName, $letter->email, $subject, $body)) {
                // success, so store it
                $status = 'success';
            } else {
                // if it fails, set status to fail
                $status = 'fail';
            }

            $email = new DropLockerEmail();
            $email->subject = $subject;
            $email->body = $body;
            $email->to = $letter->email;
            $email->fromEmail = $from;
            $email->fromName = $fromName;
            $email->dateCreated = date('Y-m-d H:i:s');
            $email->sendDateTime = date('Y-m-d H:i:s');
            $email->business_id = $letter->business_id;
            $email->status = $status;
            $email->statusNotes = "NewsletterID [".$letter->newsletterID."]";
            $email->emailType = (DEV)?'dev':'prod';
            $email->customer_id = $letter->customerID;
            $email->save();
        }
    }

    /**
     * Sends an email notification to customer who's credit card is about to expire
     */
    public function expiring_cards()
    {
        $expDate = mktime(0, 0, 0, date("m") + 1, date("d"), date("Y"));
        $expYr =  date("Y", $expDate);
        $expMo =  date("n", $expDate);

        $select1 = "SELECT customer.*, cardNumber FROM creditCard
        join customer on customerID = creditCard.customer_id
        where expYr = '$expYr'
        and expMo = '$expMo'
        and noEmail <> 1
        and customer.business_id = 3";
        $select = $this->db->query($select1);
        foreach ($select->result() as $line) {
            //don't contact inactive customers
            if ($line->inactive <> 1) {
                $select6 = "select count(distinct orderID) as numOrd,sum(closedGross) as ordAmt, sum(closedDisc) as discAmt, orders.business_id,
                max(orders.statusDate) as lastOrder
                from orders
                where customer_id  = $line->customerID
                and bag_id > 1
                group by customer_id;";
                $q = $this->db->query($select6);
                $custInfo = $q->row();
                //don't contact customers who have not used us in over 120 days
                if ($custInfo->daysSince <= 120 and $custInfo->numOrd >= 1) {
                    $results[] = $line->customerID;
                    $action_array = array('do_action' => 'expiring_credit_card', 'customer_id'=>$line->customerID, 'business_id'=>$custInfo->business_id);
                    Email_Actions::send_transaction_emails($action_array);
                }
            }
        }

        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    public function ready_for_pickup_reminder()
    {
        $results = array(
            'remainder_sent' => array(),
            'log' => array(),
            'remainder_skipped' => array(),
        );
        
        $orderStatusOption_id = 9; // Ready for Customer Pickup
        $source = Order::SOURCE_POS_APP; // pos-generated order
        
        $ready_orders_query = "SELECT orderID, customer_id, business_id, statusDate  
        FROM orders
        WHERE bag_id IS NOT NULL
        AND orderStatusOption_id = '$orderStatusOption_id'
        AND source = '$source'";
        
        $ready_orders = $this->db->query($ready_orders_query);
        foreach ($ready_orders->result() as $ready_order) {
            $waitDays = (int) get_business_meta($ready_order->business_id, 'pickup_remainder_after');
            if (!$waitDays) {
                $results['log'][] = "Skipped order {$ready_order->orderID}, business #{$ready_order->business_id} has not setup 'pickup_remainder_after'";
                $results['remainder_skipped'][] = $ready_order->orderID;
                
                continue;
            }
            
            $todayGmt = new DateTime('now', new DateTimeZone('GMT'));
            $statusDateGmt = new DateTime($ready_order->statusDate, new DateTimeZone('GMT'));
            
            $daysSinceStatus = (int) $todayGmt->diff($statusDateGmt)->format('%a');
            
            if ($daysSinceStatus >= $waitDays) {
                Email_Actions::send_transaction_emails(array(
                    'do_action' => 'pos_order_conveyored_remainder',
                    'customer_id' => $ready_order->customer_id,
                    'business_id' => $ready_order->business_id,
                    'order_id' => $ready_order->orderID,
                ));
                
                $results['log'][] = "Sent remainder for order {$ready_order->orderID}, $daysSinceStatus days since status change";
                $results['remainder_sent'][] = $ready_order->orderID;
            } else {
                $results['log'][] = "Skipped order {$ready_order->orderID}, remainder will be sent in ". ($waitDays - $daysSinceStatus)." days";
                $results['remainder_skipped'][] = $ready_order->orderID;
            }
        }

        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    /**
     * renews laundry plans
     */
    public function renew_laundryPlan()
    {
        // expire any plans that are not to be renewed
        $sql = "UPDATE laundryPlan SET active = 0 WHERE renew = 0 and active = 1 and endDate < now()";
        $this->db->query($sql);

        // expire plans that don't have a time period and are expired
        $sql = "UPDATE laundryPlan SET active = 0 WHERE days = 0 and active = 1 and endDate < now()";
        $this->db->query($sql);

        // find plans that need to be renewed today
        $sql = "SELECT 
                    `laundryPlan`.`customer_id`,
                    `laundryPlan`.`plan`,
                    `laundryPlan`.`price`,
                    `laundryPlan`.`days`,
                    `laundryPlan`.`startDate`,
                    `laundryPlan`.`endDate`,
                    `laundryPlan`.`active`,
                    `laundryPlan`.`description`,
                    `laundryPlan`.`renew`,
                    `laundryPlan`.`pounds`,
                    `laundryPlan`.`rollover`,
                    `laundryPlan`.`business_id`,
                    `laundryPlan`.`expired_price`,
                    `laundryPlan`.`credit_amount`,
                    `laundryPlan`.`type` as `plan_type`,
                    `laundryPlan`.`expired_discount_percent`,
                    `laundryPlan`.`products`,
                    `laundryPlan`.`businessLaundryPlan_id`, 
                    `businessLaundryPlan`.`updateExistingCustomerPlansWhenPlanChange`,
                    `businessLaundryPlan`.`price` as 'new_price',
                    `businessLaundryPlan`.`description` AS `new_description`,
                    `businessLaundryPlan`.`pounds` AS `new_pounds`,
                    `businessLaundryPlan`.`days` AS `new_days`,
                    `businessLaundryPlan`.`rollover` AS `new_rollover`,
                    `businessLaundryPlan`.`expired_price` AS `new_expired_price`,
                    `businessLaundryPlan`.`credit_amount` AS `new_credit_amount`,
                    `businessLaundryPlan`.`expired_discount_percent` AS `new_expired_discount_percent`,
                    `businessLaundryPlan`.`products` AS `new_products`
                FROM 
                    laundryPlan
                JOIN customer ON 
                    (laundryPlan.customer_id = customerID)
                JOIN businessLaundryPlan ON 
                    businessLaundryPlan.businessLaundryPlanID=laundryPlan.businessLaundryPlan_ID
                WHERE 
                    renew = 1
                    AND laundryPlan.days > 0
                    AND laundryPlan.endDate < now()
                    AND customer.inactive = 0";

        $laundryPlans = $this->db->query($sql)->result();

        $this->load->model("order_model");
        $results = array();
        foreach ($laundryPlans as $laundryPlan) {
            try {
                // find the price column to use for the renewals
                $updateExistingCustomerPlansWhenPlanChange = (int)$laundryPlan->updateExistingCustomerPlansWhenPlanChange;

                if ($updateExistingCustomerPlansWhenPlanChange) {
                    $laundryPlan->price = $laundryPlan->new_price;
                    $laundryPlan->description = $laundryPlan->new_description;
                    $laundryPlan->pounds = $laundryPlan->new_pounds;
                    $laundryPlan->days = $laundryPlan->new_days;
                    $laundryPlan->rollover = $laundryPlan->new_rollover;
                    $laundryPlan->expired_price = $laundryPlan->new_expired_price;
                    $laundryPlan->credit_amount = $laundryPlan->new_credit_amount;
                    $laundryPlan->expired_discount_percent = $laundryPlan->new_expired_discount_percent;
                    $laundryPlan->products = $laundryPlan->new_products;
                }

                if ($laundryPlan->plan_type == "laundry_plan") {
                    $result = $this->order_model->renew_laundry_plan($laundryPlan);
                } elseif ($laundryPlan->plan_type == "product_plan") {
                    $result = $this->order_model->renew_product_plan($laundryPlan);
                } else {
                    $result = $this->order_model->renew_everything_plan($laundryPlan);
                }
                
                if ($result['status'] != 'success') {
                    $results[$laundryPlan->customer_id]['error'] = 'Problem with cron job renew laundry plan';
                    send_admin_notification("Problem with cron job renew laundry plan", print_r($result, 1), 3);
                    send_development_email("Problem with Renew Laundry Plan Cron Job", print_r($result, 1));
                } else {
                    $results[$laundryPlan->customer_id]['order_id'] = $result['order_id'];
                }
            } catch (\InvalidArgumentException $exception) {
                send_exception_report($exception, print_r($laundryPlan, 1));
                continue;
            }
        }

        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    /**
     * Closes prepayments that have been paid for
     *
     * tested in tests/cron
     */
    public function close_prepayments()
    {
        $this->load->model('order_model');
        $getPrePays = "select * from orders
                                    join business on locker_id = prepayment_locker_id
                                    where orderPaymentStatusOption_id = 3
                                    and orderStatusOption_id = 3";
        $query = $this->db->query($getPrePays);
        foreach ($query->result() as $line) {
            if (!get_business_meta($line->business_id, "autocomplete_orders", 1)) {
                continue;
            }
            $options['order_id'] = $line->orderID;
            $options['orderStatusOption_id'] = 10;
            $options['business_id'] = $line->business_id;
            $options['employee_id'] = 1;
            $this->order_model->update_order($options);
            $results[] = $line->orderID;
        }

        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    /**
     * creates a formatted report for unpaid orders and emails to operations
     */
    public function unpaid_orders()
    {
        $this->load->model('orderitem_model');

        //run this for every business
        $getBusinesses = "SELECT * FROM business WHERE serviceTypes IS NOT NULL";
        $query = $this->db->query($getBusinesses);
        $businesses = $query->result();
        foreach ($businesses as $business) {
            $report = '<div style="padding-left: 10px; padding-right: 10px;"><table border="1" cellspacing="1" cellpadding="3">
            <tr>
            <td>Order</td>
            <td>Bag</td>
            <td>Customer</td>
            <td>Date</td>
            <td>Amount</td>
            <td>Paid</td>
            <td>CC Ref</td>
            </tr>';

            $sql = "select orders.*, firstName, lastName, invoice, bagNumber, invoice, pnref
                        from orders
                        join customer on customerID = orders.customer_id
                        join bag on bagID = bag_id
                        left join transaction on transaction.order_id = orderID
                        where orderPaymentStatusOption_id <> 3
                        and orders.dateCreated < date_sub(NOW(), INTERVAL 3 day)
                        and orders.business_id = ?
                        and invoice = 0
                        and customerID != ?
                        and orders.orderStatusOption_id = 10
                UNION
                    select orders.*, firstName, lastName, invoice, bagNumber, invoice, pnref
                            from orders
                            join customer on customerID = orders.customer_id
                            join bag on bagID = bag_id
                            left join transaction on transaction.order_id = orderID
                            where orderPaymentStatusOption_id = 3
                            and transactionID is NULL
                            and bag_id <> 0
                            and invoice = 0
                            and orders.business_id = ?
                            and orders.dateCreated > '2011-01-01'
                            and (closedGross + closedDisc) > 0.02
                            order by orderID desc
                            ";
            $query = $this->db->query($sql, array($business->businessID, $business->blank_customer_id, $business->businessID));

            $admin_url = get_admin_url();
            foreach ($query->result() as $line) {
                $orderTotal = 0;
                $orderTotal = $this->orderitem_model->get_item_total($line->orderID);
                if ($orderTotal['total'] > 0.02) {
                    $report .= '<tr>';
                    $report .= '<td><a href="'.$admin_url.'/admin/orders/details/'.$line->orderID.'" target="_blank">'.$line->orderID.'</a></td>';
                    $report .= '<td>'.$line->bagNumber.'</td>';
                    $report .= '<td>'.getPersonName($line).'('.$line->invoice.')</td>';
                    $report .= '<td>'.$line->statusDate.'</td>';
                    $report .= '<td>$'.$orderTotal['total'].'</td>';
                    $report .= '<td>'.$line->orderPaymentStatusOption_id.'</td>';
                    $report .= '<td>'.$line->pnref.'</td>';
                    $report .= '</tr>';
                }
            }
            $report .= '</table>';
            $results[$business->businessID] = $report;

            $to_email = get_business_meta($business->businessID, 'operations');
            if (empty($to_email)) {
                $to_email = get_business_meta($business->businessID, 'customerSupport');
            }

            queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, $to_email, "Unpaid Orders", $report, array(), null, $business->businessID);
        }

        $this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>serialize($results)));
    }

    /**
     * @deprecated
     */
    public function remove_kiosk_access()
    {
        $query1="SELECT customer.*, kioskAccess.*, kioskAccess.updated as kioskAdd
        FROM kioskAccess
        join customer on customerID = kioskAccess.customer_id";

        $query = $this->db->query($query1);
        $emailBody = '<table><tr>
                    <td>counter</td>
                    <td>customer</td>
                    <td>days</td>
                    <td>uses kiosk</td></tr>';
        foreach ($query->result() as $line2) {
            //don't do anything if the kiosk access was added in the last 60 days
            if (strtotime($line2->kioskAdd) <= strtotime("30 days ago")) {
                //get their last order in a kiosk
                $query2 = "select max(orderID) as lastOrder
                from orders
                WHERE orderStatusOption_id = '10'
                and customer_id = $line2->customer_id
                and bag_id <> 0";

                $query2 = $this->db->query($query2);
                $line = $query2->result();
                if ($line[0]->lastOrder) {
                    //find out the last time they used a kiosk
                    $query2 = "select orderID, orders.customer_id, orders.statusDate as lastOrder, locationType_id
                            from orders
                            join locker on lockerID = orders.locker_id
                            join location on locationID = locker.location_id
                            WHERE customer_id = $line2->customer_id
                            and bag_id <> 0
                            and locationType_id = 1
                            order by lastOrder desc
                            limit 1";
                    $query2 = $this->db->query($query2);
                    $line = $query2->row();
                } else { //customer hasn't had a real order yet.  Check signup date
                    //get the customer's info
                    $query2 = "select signupDate as lastOrder, address, locationType_id, companyName, location.business_id
                            from customer
                            left join location on locationID = customer.location_id
                            WHERE customerID = $line2->customer_id";
                    $query2 = $this->db->query($query2);
                    $line = $query2->row();
                }

                //if they user a kiosk don't delete them
                if ($line->locationType_id == 1) {
                    $kiosk = 1;
                } else {
                    $kiosk = 0;
                }

                $date = mktime(0, 0, 0, date("m"), date("d"), date("Y")); //Gets Unix timestamp for current date
                $timestamp = strtotime($line->lastOrder);
                $date2 = mktime(0, 0, 0, date("m", $timestamp), date("d", $timestamp), date("Y", $timestamp)); //Gets Unix timestamp for current date
                $difference = $date-$date2; //Calcuates Difference
                $days2 = floor($difference /60/60/24); //Calculates Days Old

                $counter ++;

                $admin_url = get_admin_url();
                if ($days2 > 120 or $kiosk == 0) {
                    if ($line2->type == 'customer') { //don't mess with employees
                        //remove from kiosk access
                        $update1 = "update customer set kioskAccess = 0 where customerID = $line2->customer_id;";
                        $update = $this->db->query($update1);
                        $delete1 = "delete from kioskAccess where customer_id = $line2->customer_id;";
                        $delete = $this->db->query($delete1);

                        $emailBody .=  '<tr>
                            <td>'.$counter.'</td>
                            <td><a href="'.$admin_url.'/admin/customers/detail/'.$line2->customerID.'" target="_blank">'.$line2->firstName.'</a></td>
                            <td>'.$days2.'('.$line->lastOrder.')</td>
                            <td>'.$kiosk.'</td>';
                    }
                }
            }
        }
        $emailBody .=  '</table>';
        queueEmail(SYSTEMEMAIL, "Removed Kiosk Access", get_business_meta(3, 'customerSupport'), 'The following customers have had their kiosk access removed', $emailBody, array(), null, 3);
    }

    /**
     * Adds non-normalized order data to the report_customer table
     * pass parameter load = 1 to reload the whole table.  Otherwise it just inserts new records
     */
    public function update_customer_report_table($full = 0, $verbose = 0, $missing = 0)
    {
        ini_set("display_errors", 1);
        error_reporting(E_ALL);
        while (!Lock::acquire("update_customer_report_table")) {
            if (!$full) {
                return;
            }

            echo "Failed to acquire lock, sleeping 60 seconds\n";

            sleep(60);
        }

        $this->load->model('reports_customer_model');
        try {
            $this->reports_customer_model->update_customers($full, $verbose, $missing);
        } catch (Exception $e) {
            $body = "ERROR:<br>".$e->getMessage();
            send_email(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, 'Exception caught on update_customer_report_table in cron.php', $body);
        }

        Lock::release("update_customer_report_table");
    }

    /**
     * Adds non-normalized order data to the report_orders table
     * pass parameter load = 1 to reload the whole table.  Otherwise it just inserts new records
     *
     * NOTE: CLI cannot get the url querystring variables, therefore you must use segments.
     * ie... load_report_order_table/1
     *
     * @param bool $full_table set to true to load full table
     */
    public function load_report_order_table($full_table = false)
    {
        if ($full_table) {
            //empty all data out of the table
            $this->db->query("TRUNCATE report_orders");
        } else {
            $justNew = "where orders.statusDate > (select max(updated) from report_orders)";
        }
        $addOrders = "REPLACE into report_orders (select orderID, customerID, customer.firstName, customer.lastName, bagID, bagNumber, lockerID,
            lockerName, locationID, location.address, orders.notes, orderStatusOptionID, orderStatusOption.name, orders.statusDate, orderPaymentStatusOptionID,
            orderPaymentStatusOption.name,  orders.orderPaymentStatusDate, orders.ticketNum, orders.dateCreated, orders.llNotes, orders.postAssembly, orders.closedGross,
            orders.closedDisc, orders.loaded, orders.delivered, businessID, business.companyName, now(), orderType
            from orders
            left join bag on bag_id = bagID
            left join locker on locker_id = lockerID
            left join location on location_id = locationID
            left join customer on orders.customer_id = customerID
            left join orderStatusOption on orderStatusOption_id = orderStatusOptionID
            left join orderPaymentStatusOption on orderPaymentStatusOption_id = orderPaymentStatusOptionID
            join business on orders.business_id = businessID
            $justNew);";
        $this->db->query($addOrders);
    }

    /**
     * check the time of any pending and see how long it's been,
     * if its past a certain time run the process and set it.
     *
     * This is to catch pickups that slip through the cracks
     */
    public function process_due_pickups()
    {
        $this->load->model('droidpostpickups_model');
        $now = date('Y-m-d H:i:s', strtotime('-1 minutes'));

        echo 'looking for unprocessed pickups older than ' . $now . "\n";

        $this->db->from('droid_postPickups');
        $this->db->where('dateCreated <=', $now);
        $this->db->where('status', 'pending');

        $ten_minute_pending_query = $this->db->get();
        $ten_minute_old_pickups = $ten_minute_pending_query->result();

        $lagging_pickup_count = 0;
        foreach ($ten_minute_old_pickups as $tmo_pickup) {
            $order_id = $this->droidpostpickups_model->processPickup($tmo_pickup);
            if (!empty($order_id)) {
                $lagging_pickup_count++;
            }
        }

        if ($lagging_pickup_count > 0) {
            echo $lagging_pickup_count . ' processed, had status set to \'pending\' older than ' . $now .' from dateCreated';
        }

        die('finished processing lagging pickups');
    }

    /**
     * Updates the wordpress wp_search table
     *
     * TODO: move this into wordpress, not part of DLA
     */
    public function update_wiki_search()
    {
        $this->db = $this->load->database("wiki", true);
        $emptyPosts = "delete from wp_search";
        $this->db->query($emptyPosts);

        $loadPosts = "insert into wp_search (title, content, url, source)
        select post_title, post_content, guid, 'Wiki' from wp_posts where post_status = 'publish'";
        $this->db->query($loadPosts);

        $loadComments = "insert into wp_search (title, content, url, source)
        SELECT post_title, comment_content, guid, 'comment' FROM wp_comments
        join wp_posts on wp_posts.ID = comment_post_ID";
        $this->db->query($loadComments);

        $loadForums = "insert into wp_search (title, content, url, source)
        select subject, text, concat('http://droplocker.com/wiki/forum?mingleforumaction=viewtopic&t=',parent_id), 'forum' from wp_forum_posts";
        $this->db->query($loadForums);
    }

    /**
     * Sends the new_customer alert, 5 minutes after customer signup
     * This is delayed, to get a better chance of the user filling their profile
     * and sending more useful information in the email.
     */
    public function send_delayed_new_customer_alerts()
    {
        $date = date('Y-m-d H:i:s', strtotime('-5 min'));

        $this->load->model('customer_model');
        $customers = $this->customer_model->get_aprax(array(
            'alertSent'  => 0,
            'signupDate < ' => $date
        ), null, "customerID, business_id");

        foreach ($customers as $customer) {
            try {

                // skipping because miscongigured business
                if (!get_business_meta($customer->business_id, "customerSupport")) {
                    continue;
                }

                Email_Actions::send_transaction_emails(array(
                    "do_action" => "new_customer",
                    "customer_id" => $customer->customerID,
                    "business_id" => $customer->business_id
                ));
                $this->db->query("UPDATE customer SET alertSent = 1 WHERE customerID = ?", array($customer->customerID));
            } catch (Exception $e) {
                send_exception_report($e);
            }
        }
    }
}
