<?php
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\EmailTemplate;
use App\Libraries\DroplockerObjects\Employee;
use DropLocker\Util\Random;

class Emails extends MY_Admin_Controller
{
    /**
     * Retrieves the total number of emails for the specified business
     * Expects the following GET parameter
     *  business_id
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    public function get_total()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get('business_id')) {
                try {
                    $business_id = $this->input->get('business_id');
                    $this->load->model('email_model');
                    $email_count = $this->email_model->get_count($business_id);
                    $result = array('status' => 'success', 'total' => $email_count);
                } catch (\Exception $e) {
                    $result = array('status' => 'error', 'message' => 'An error occurred retrieving the total email count');
                    send_exception_report($e);
                }

                echo json_encode($result);
            } else {
                throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter");
            }
        } else {
            throw new \LogicException("This action can only be called with an ajax request");
        }
    }
    /**
     * The following action sends a test email for the specified email template
     * Expects the following POST parameters:
     *  emailActionID The email action tempalte to use for sending the test email
     *  business_language_id The language to use for the email template
     */
    public function send_test_email()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("emailActionID", "Email Action ID", "required|is_natural_no_zero");
        $this->form_validation->set_rules("business_language_id", "Business Language ID", "required|is_natural_no_zero");
        if ($this->form_validation->run()) {
            $emailActionID = $this->input->post("emailActionID");
            $business_language_id = $this->input->post("business_language_id");
            $this->load->model("emailactions_model");
            $this->load->model("emailtemplate_model");
            $emailAction = $this->emailactions_model->get_by_primary_key($emailActionID);
            $emailTemplate = $this->emailtemplate_model->get_one(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id, "business_language_id" => $business_language_id));
            if (empty($emailTemplate)) {
                if (empty($emailAction)) {
                    set_flash_message("error", "Could not find email action ID $emailActionID");
                } else {
                    set_flash_message("error", "Could not find email template for email action '{$emailAction->name}'");
                }
            } else {
                 //The following statements retrieve a random customer from the database.
                $customer = Random::getCustomer($this->business_id);

                $order = Random::getObject("Order", false, array("business_id" => $this->business_id));

                $claim = Random::getObject("Claim", false, array("business_id" => $this->business_id));

                $coupon = Random::getObject("Coupon", false, array("business_id" => $this->business_id));

                $action_array = array('business_id' => $this->business_id, "customer_id" => $customer->customerID, "order_id" => $order->orderID, "claim_id" => $claim->claimID, "giftCard_id" => $coupon->couponID);

                $data = get_macros($action_array);
                $subject = email_replace($emailTemplate->emailSubject, $data);
                $body = email_replace($emailTemplate->emailText, $data);
                $to = $this->employee->email;

                send_email(get_business_meta($this->business_id, 'customerSupport'), get_business_meta($this->business_id, 'from_email_name'), $to, $subject, $body, array(), array(), null, $this->business_id, true);

                set_flash_message("success", "Sent test customer e-mail for email action '{$emailAction->name}'");
            }
        } else {
            set_flash_message('error', validation_errors());
        }
        redirect_with_fallback("/admin/admin/manage_email_template");
    }

    public function send_test_adminAlert_email()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("emailActionID", "Email Action ID", "required|is_natural_no_zero|does_emailAction_exist");
        if ($this->form_validation->run()) {
            $emailActionID = $this->input->post("emailActionID");
            $this->load->model("emailactions_model");
            $emailAction = $this->emailactions_model->get_by_primary_key($emailActionID);
            $this->load->model("emailtemplate_model");
            $emailTemplate = $this->emailtemplate_model->get_one(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id));
            if (empty($emailTemplate)) {
                set_flash_message("error", "Could not find email template for email action '{$emailAction->name}'");
            } else {
                 //The following statements retrieve a random customer from the database.
                $customer = Random::getCustomer($this->business_id);

                $order = Random::getObject("Order", false, array("business_id" => $this->business_id));

                $claim = Random::getObject("Claim", false, array("business_id" => $this->business_id));

                $coupon = Random::getObject("Coupon", false, array("business_id" => $this->business_id));

                $action_array = array('business_id' => $this->business_id, "customer_id" => $customer->customerID, "order_id" => $order->orderID, "claim_id" => $claim->claimID, "giftCard_id" => $coupon->couponID);

                $data = get_macros($action_array);
                $subject = email_replace($emailTemplate->adminSubject, $data);
                $body = email_replace($emailTemplate->adminText, $data);

                // This will get the current logged in employee and use there email address for the to address
                $to = $this->employee->email;

                send_email(get_business_meta($this->business_id, 'customerSupport'), get_business_meta($this->business_id, 'from_email_name'), $to, $subject, $body, array(), array(), null, $this->business_id, true);

                set_flash_message("success", "Sent test Admin Alert e-mail for email action '{$emailAction->name}'");
            }
        } else {
            set_flash_message('error', validation_errors());
        }
        redirect_with_fallback("/admin/admin/manage_email_template");
    }

}
