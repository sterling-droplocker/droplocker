<?php

use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Validation_Exception;

/**
 * location admin/employees.php
 */
class Employees extends MY_Admin_Controller
{

    public $buser_id;
    public $business_id;
    public $business;

    public $pageTitle = "Employees";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('employee_model');
    }

    public function index()
    {
        $this->load->view('admin/blank', '', true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Retrieves all the employees for a particular business and returns the data as a JSON structure to the client
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * Responses with a JSON structure in the following format:
     *  {
     *      employeeID1 : employee1FirstName employee1LastName
     *      employeeID2 : employee2FirstName employee2LastName,
     *      ...
     *  }
     */
    public function get_as_json()
    {
        if (!$this->input->get("business_id")) {
            throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter.");
        }
        if ($this->input->is_ajax_request()) {
            $business_id = $this->input->get("business_id");
            $employees = $this->employee_model->get_all_as_codeigniter_dropdown($business_id);
            echo json_encode($employees);
        } else {
            throw new \LogicException("This action is only accessible through an ajax request.");
        }
    }

    public function add_named_customer()
    {
        if (!$this->input->post("employeeID")) {
            throw new \InvalidArgumentException("'employeeID' must be passed as a POST parameter.");
        }
        if (!$this->input->post("customerID")) {
            throw new \InvalidArgumentException("'customerID' must be passed as a POST parameter.");
        }
        $employeeID = $this->input->post("employeeID");
        if (!is_numeric($employeeID)) {
            throw new \InvalidArgumentException("'employeeID' must be numeric.");
        }
        try {
            $employee = new Employee($employeeID, false);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Employee ID '{$employeeID}' not found.");
            redirect($this->agent->referrer());
        }
        $customerID = $this->input->post("customerID");
        if (!is_numeric($customerID)) {
            throw new \InvalidArgumentException("'customerID' must be numeric.");
        }
        try {
            $customer = new Customer($customerID, false);
        } catch (Exception $e) {
            set_flash_message("error", "Customer ID '{$customerID}' not found");
            redirect($this->agent->referrer());
        }
        $customer->owner_id = $employee->{Employee::$primary_key};
        $customer->commission_rate = $this->input->post("commission_rate");
        $customer->save();
        set_flash_message("success", "Added named account to employee.");
        redirect($this->agent->referrer());
    }

    /**
     * Renders the interface for adding a new employee to a business
     */
    public function add()
    {
        $this->template->write('page_title', 'Add Employee', true);

        $this->load->model('aclroles_model');
        $roles = $this->aclroles_model->get(array("business_id" => $this->business_id));
        $content['roles'] = $roles;

        $this->data['content'] = $this->load->view('admin/employees/manage', $content, true);
        $this->template->add_js("/js/jquery.validate-1.9.js", "source");
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Create a new employee
     * Required the following POST parameters:
     *  first_name
     *  last_name
     *  email
     */
    public function create()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run()) {
            $employee->firstName = $this->input->post("firstName");
            $employee->lastName = $this->input->post("lastName");
            $employee->email = $this->input->post("email");
            $employee->address = $this->input->post("address");
            $employee->city = $this->input->post("firstName");
            $employee->state = $this->input->post("firstName");
            $employee->zipcode = $this->input->post("zipcode");
            $employee->phoneHome = $this->input->post("phoneHome");
            $employee->phoneCell = $this->input->post("phoneCell");
            $employee->ssn = $this->input->post("ssn");
            $employee->role_id = $this->input->post("role_id");
            $employee->password = $this->input->post("password");
            $employee->createdBy = $this->buser_id;
            $employee->manage_id = $this->buser_id;
            $employee->type = "non exempt";
            $employee->startDate = new DateTime();

            try {
                $employee->save();

                $business_employee = new Business_Employee();
                $business_employee->employee_id = $employee->{Employee::$primary_key};
                $business_employee->aclRole_id = $this->input->post("aclRole_id");
                $business_employee->business_id = $this->business_id;
                $business_employee->default = 1;
                $business_employee->save();
            } catch (Validation_Exception $e) {
                set_flash_message('error', $e->getMessage());
                redirect_with_fallback("/admin/employees/manage/");
            }

            $username = $employee->firstName . $employee->lastName;

            if (ENVIRONMENT == 'production') {
                require_once APPPATH . "/third_party/xmlrpc.php";
                $client = new XMLRPC_Client("http://droplocker.com/wiki/xmlrpc.php");
                $available_methods = $client->call('droplocker.add_wiki_user', 1, 'admin', '=-09\][p', array('username' => $username, 'password' => $_POST['password'], 'email' => $_POST['email']));
            }

            set_flash_message('success', "Created new employee");

            redirect("/admin/employees/view");
        } else {
            set_flash_message("error", validation_errors());
        }
    }

    /**
     * Sets an employees status to "1", which has the buisness meaning of 'active'.
     * Expects the employee ID as segment 4 of the URL.
     * Redirects to the view action of the employee controller.
     */
    public function activate($employee_id = null)
    {
        try {
            $this->employee_model->activate($employee_id);
            set_flash_message("success", "Succesfully reactivated employee");
        } catch (Exception $e) {
            set_flash_message("error", "Could not reactivate employee");
        }
        redirect("/admin/employees/view");
    }

    /**
     * Sets an employee's stats to "0", which has a business meaning of 'inactive'.
     * Expects the employee ID as segment 4 of the URL.
     */
    public function deactivate($employee_id = null)
    {
        try {
            $employee = new Employee($employee_id);
            $this->employee_model->deactivate($employee_id);
            set_flash_message('success', sprintf("Successfully deactivated %s (%s)", getPersonName($employee), $employee->employeeID));

            $this->goBack("/admin/employees/view");
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Employee ID '$employee_id' not found");
        }
    }

    public function is_unique($str)
    {
        $employee_id = (int)$this->input->post("employee_id");
        $username = trim($this->input->post("username"));
        
        if (empty($username)) {
            return true;
        }

        $this->load->model('employee_model');

        $exists = $this->employee_model->get_aprax(array(
            'username' => $username,
            'employeeID !=' => $employee_id,
        ));

        if ($exists) {
            return false;
        }

        return true;
    }

    /**
     * Updates or creates an employee
     *
     * If passed, then updates the employee entity specified by the primary key. If not passed, then creates a new Employee entity.
     * If passed, and the current user is a superadmin, then sets the business that the employee is associated with.
     *
     * Expects the following POST parameters
     *  email string
     *
     * Can take the following POST parameters
     *  employee_id
     *  business_id
     *  firstName
     *  lastName
     *  username
     *  position
     *  password
     *  phoneHome
     *  phoneCell
     *  llphone
     *  address
     *  address2
     *  city
     *  state
     *  zipcode
     *  type
     *  ssn
     *  hourlyRate
     *  rateType
     *  note
     *  commission
     *  one_session
     *  session_expiration
     *  startDate
     *  endDate
     *  aclRole_id
     *  customer_id
     *  manager_id
     *  supplier_id
     */
    public function update()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("email", "Email", "required|email");

        $this->form_validation->set_rules('username', 'Username (For LaundryDroid)', 'trim|callback_is_unique');

        if ($this->form_validation->run()) {
            if ($this->input->post("employee_id")) {
                $employee = new Employee($this->input->post("employee_id"), true);
                $business_employee = $employee->relationships['Business_Employee'][0];
                set_flash_message('success', "Updated Employee");
            } else {
                $business_employee = new Business_Employee();
                $employee = new Employee();
                set_flash_message("success", "Created new employee");
            }

            $employee->firstName = $this->input->post("firstName");
            $employee->lastName = $this->input->post("lastName");
            $employee->email = $this->input->post("email");
            $employee->username = $this->input->post("username");
            $employee->position = $this->input->post("position");
            if ($this->input->post("password")) {
                $employee->password = $this->input->post("password");
            }
            $employee->phoneHome = $this->input->post("phoneHome");
            $employee->phoneCell = $this->input->post("phoneCell");
            $employee->llPhone = $this->input->post("llPhone");
            $employee->address = $this->input->post("address");
            $employee->address2 = $this->input->post("address2");
            $employee->city = $this->input->post("city");
            $employee->state = $this->input->post("state");
            $employee->zipcode = $this->input->post("zipcode");
            $employee->type = $this->input->post("type");
            $employee->ssn = $this->input->post("ssn");
            $employee->hourlyRate = $this->input->post("hourlyRate");
            $employee->rateType = $this->input->post("rateType");
            $employee->notes = $this->input->post("notes");
            $employee->manager_id = $this->input->post("manager_id");
            $employee->commission = $this->input->post("commission");
            $employee->one_session = $this->input->post("one_session");
            $employee->session_expiration = $this->input->post("session_expiration");
            $employee->business_language_id = $this->input->post("business_language_id");
            $employee->developer_mode = $this->input->post("developer_mode");
            $employee->supplier_id = $this->input->post("supplier_id");

            //The following conditional updates the business that the employee is associated with
            if ($this->input->post("business_id") && is_superadmin()) {
                $business = new \App\Libraries\DroplockerObjects\Business($this->input->post("business_id"));
                $business_employee->business_id = $business->businessID;
                if (!$this->input->post("employee_id")) {
                    set_flash_message("success", sprintf("Added %s to %s", getPersonName($employee), $business->companyName));
                }
            } else {
                $business = $this->business;
                $business_employee->business_id = $this->business->businessID;
            }

            if ($this->input->post("allowed_businesses_ids") && is_superadmin()) {
                $employee->allowed_businesses = json_encode($this->input->post("allowed_businesses_ids"));
            }

            if ($this->input->post("customer_id")) {
                $employee->customer_id = $this->input->post("customer_id");
            } else {
                $employee->customer_id = null;
            }

                $employee->startDate = new \DateTime($this->input->post("startDate"), new \DateTimeZone($business->timezone));
                $employee->endDate = new \DateTime($this->input->post("endDate"), new \DateTimeZone($business->timezone));

            try {
                $employee->save();
                $business_employee->aclRole_id = $this->input->post("aclRole_id");
                $business_employee->employee_id = $employee->{Employee::$primary_key};
                $business_employee->default = 1;
                $business_employee->save();
            } catch (Validation_Exception $e) {
                set_flash_message('error', $e->getMessage());
                $this->session->set_flashdata('form_data', $this->input->post());
                redirect_with_fallback("/admin/employees/manage/{$employee->employeeID}");
            }

            redirect("/admin/employees/manage/{$employee->employeeID}");
        } else {
            $this->session->set_flashdata('form_data', $this->input->post());
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/employees/manage/{$employee->employeeID}");
    }

    /**
     * Renders the interface for managing an existing employee or creating a new employee.
     */
    public function manage($employee_id = null)
    {
        checkAccess('manage_employees');

        $this->load->model("aclroles_model");
        $this->load->model("employee_model");
        $this->load->model("business_model");
        $this->load->model("business_language_model");
        $content['businesses'] = $this->business_model->get_all_as_codeigniter_dropdown();

        $this->load->model('supplier_model');
        $content['suppliers'] = $this->supplier_model->simple_array($this->business_id);

        //The following condtional checks to see if an employee ID was passed in the URL segment, if not, then the menus are populated with the results associated with the first business in the database. Otherwise, the menus are populated with fields related to the business of the employee.
        $content['business'] = $this->business;
        if (empty($employee_id)) {
            $content['title'] = "New Employee";
            $content['managers'] = $this->employee_model->get_all_as_codeigniter_dropdown($this->business->businessID);
            $content['roles'] = $this->aclroles_model->get_all_as_codeigniter_dropdown($this->business->businessID);
            $content['business_languages'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business->businessID);
        } else {
            $content['title'] = "Edit Employee";
            $this->load->model("business_employee_model");

            $search_options = array("employee_id" => $employee_id);
            if (!is_superadmin()) {
                $search_options["business_id"] = $this->business->businessID;
            }
            $content['business_employee'] = $this->business_employee_model->get_one($search_options);

            $content['employee'] = $this->employee_model->get_by_primary_key($employee_id);

            if (!is_superadmin() && $content['employee']->superAdmin) {
                set_flash_message("error", "You are not allowed to edit this user");
                $this->goBack("/admin/employees/view");           
            }

            $content['managers'] = $this->employee_model->get_all_as_codeigniter_dropdown($content['business_employee']->business_id);
            $content['roles'] = $this->aclroles_model->get_all_as_codeigniter_dropdown($content['business_employee']->business_id);
            $content['business_languages'] = $this->business_language_model->get_all_as_codeigniter_dropdown($content['business_employee']->business_id);
        }

        if (!empty($content['employee']->customer_id)) {
            $this->load->model("customer_model");
            $content['employee_customer'] = $this->customer_model->get_by_primary_key($content['employee']->customer_id);
        } else {
            $content['employee_customer'] = false;
        }

        if (!empty($content['employee']->allowed_businesses)) {
            $content['allowed_businesses_ids'] = json_decode($content['employee']->allowed_businesses);
        } else {
            $content['allowed_businesses_ids'] = array();
        }

        /* Restore form data after validation error */
        $form_data = $this->session->flashdata('form_data');
        if (!empty($form_data)) {
            $content["employee"] = new Employee();
            foreach ($form_data as $k => $v) {
                $content["employee"]->{$k} = $v;
                if ($k == "allowed_businesses_ids") {
                    $content["allowed_businesses_ids"] = $form_data[$k];
                }
            }
            if (!empty($employee_id)) {
                $content["employee"]->employeeID = (int)$employee_id;
            }
        }

        $this->renderAdmin('manage', $content);
    }

    /**
     * Updates an employee's e-mail address and/or displays the interface for updating an employee's e-mail address.
     * Expects the employee's ID as segment 4 of the URL.
     * Can take the following POST parameters:
     *  new_email string The employee's new email address; must be a valid e-mail address
     *
     */
    public function update_email($employee_id = null)
    {
        if (empty($employee_id)) {
            throw new Exception("Expected segment 4 to contain the ID of the employee.");
        }
        $options['employeeID'] = $employee_id;
        $options['select'] = 'employeeID, email';
        $employees = $this->employee_model->get($options);
        $employee = $employees[0];

        $content['employee'] = $employee;
        // The following conditional updates an employee's email if the post parameter 'new_email' is not empty.
        if ($this->input->post("new_email")) {
            $new_email = $this->input->post("new_email");
            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_email', 'New Email', 'required|valid_email');

            if ($this->form_validation->run()) {
                $this->employee_model->update_email($employee_id, $new_email);
                set_flash_message('success', "Updated Employee's E-mail");
            }
        }

        $this->renderAdmin('update_email', $content);
    }

    public function delete_pending_email()
    {
        $employee_id = get_employee_id($this->buser_id);
        $options['pendingEmailUpdate'] = '';
        $where['employeeID'] = $employee_id;
        $this->employee_model->update($options, $where);
        set_flash_message('success', "Pending email has been deleted");
        redirect("/admin/employees/update_email");
    }

    /**
     * Updates the password for a particular employee.
     * Can take the employee ID as segment 4 of the URL, or will retrieve the employee ID from the user session.
     */
    public function update_password($employee_id = null)
    {
        if (!$employee_id) {
            $employee_id = get_employee_id($this->buser_id);
        }

        $this->load->model('business_employee_model');
        $this->business_employee_model->business_id = $this->business_id;
        $this->business_employee_model->employee_id = $employee_id;
        $empBus = $this->business_employee_model->get();
        if ($empBus) {
            $this->load->model('employee_model');
            $options['employee_id'] = $employee_id;
            $employee = $this->employee_model->get($options);
            $content['employee'] = $employee;

            if (isset($_POST['submit'])) {

                if ($_POST['password1'] == '' || $_POST['password2'] == '') {
                    set_flash_message('error', "All fields are required");
                    redirect("/admin/employees/update_password");
                }


                if ($_POST['password1'] != $_POST['password2']) {
                    set_flash_message('error', "New Passwords do not match, please try again");
                    redirect("/admin/employees/update_password");
                }

                $options = array();
                $options['password'] = md5($_POST['password1']);
                $where['employeeID'] = $employee[0]->employeeID;
                $this->employee_model->update($options, $where);
                set_flash_message('success', "Password has been updated");
                redirect("/admin/employees/update_password/{$employee[0]->employeeID}");
            }
        } else {
            set_flash_message('error', "Sorry you can not edit this employee because they do not belong to your busines.");
            redirect('/admin/employees/view/');
        }

        $this->renderAdmin('update_password', $content);
    }

    /**
     * view method lists the employees of a business and their roles within that business
     */
    public function view()
    {
        $this->template->write('page_title', 'View Employees', true);

        $exclude_super_admins = "";
        if (!is_superadmin()) {
            $exclude_super_admins = " AND (IFNULL(superAdmin,0) = 0 ||  superAdmin = 0)";
        }

        $get_active_employees_query = "select *
                                            from employee
                                            join business_employee on employee_id = employeeID
                                            where business_id = $this->business_id AND empStatus = 1 {$exclude_super_admins}
                                            order by firstName";

        $active_employees = $this->db->query($get_active_employees_query);

        $content['active_employees'] = $active_employees->result();

        $get_inactive_employees_query = "select *
                from employee
                join business_employee on employee_id = employeeID
                where business_id = $this->business_id AND empStatus = 0 {$exclude_super_admins}
                order by firstName";

        $inactive_employees = $this->db->query($get_inactive_employees_query);
        $content['inactive_employees'] = $inactive_employees->result();

        $this->renderAdmin('view', $content);
    }

    /**
     * Allows one employee to log in as another. This not a UI.
     * The link to this function is located in the employee_view
     */
    public function switch_to_employee($employeeID = null)
    {
        try {
            $current_employee = new Employee($this->session->userdata('employee_id'));
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Employee not logged in");
            send_exception_report($e);

            $this->goBack("/admin/login");
        }

        try {
            $employee = new Employee($employeeID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Could not find employee $employeeID");
            send_exception_report($e);

            $this->goBack("/admin/employees");
        }

        $business_employees = Business_Employee::search_aprax(array("employee_id" => $employee->{Employee::$primary_key}));
        if (empty($business_employees)) {
            throw new \Exception("Could not find the associated business employee entry for the employee.");
        }
        
        if (!is_superadmin() && $employee->superAdmin) {
            set_flash_message("error", "You are not allowed to login as superadmin user");
            $this->goBack("/admin/employees/view");
        }

        $this->session->unset_userdata('buser_id');
        $this->session->unset_userdata('buser_fname');
        $this->session->unset_userdata('buser_lname');
        $this->session->unset_userdata('business_id');

        $business_employee = $business_employees[0];

        $this->load->model('employee_log_model');
        // Log successful user switch
        $this->employee_log_model->switch_user(
            $business_employee->business_id,
            $current_employee->{Employee::$primary_key},
            $employee->{Employee::$primary_key}
        );



        $this->session->set_userdata(array('business_id' => $business_employee->business_id));
        $this->session->set_userdata(array('buser_id' => $business_employee->{Business_Employee::$primary_key}));

        $this->session->set_userdata('employee_id', $employee->{Employee::$primary_key});
        $this->session->set_userdata('buser_fname', $employee->firstName);
        $this->session->set_userdata('buser_lname', $employee->lastName);
        $this->session->set_userdata('timezone', $employee->timezone);
        $this->session->set_userdata('super_admin', $employee->superAdmin);

        $business = new Business($business_employee->business_id);
        $this->session->set_userdata("companyName", sprintf("%s - %s, %s", $business->companyName, $business->city, $business->state));

        $redirect = ($_POST['redirect'] != '') ? $_POST['redirect'] : "/admin";

        redirect($redirect);
    }

    /**
     * UI for managing timecards
     */
    public function timecard()
    {
        $content['business'] = $this->business;
        $this->data['content'] = $this->load->view('timecards/timecard', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function finance()
    {
        checkAccess('view_timercards');
        $content['business'] = $this->business;
        $this->data['content'] = $this->load->view('timecards/finance', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * roles controller allows the user to edit the roles for resources
     */
    public function roles()
    {
        $this->template->add_js('js/update_roles.js');
        $this->load->model('aclroles_model');
        //Note, when in bizzie mode, only the employee roles of the master business are manageable. Changes made to the master business's employee roles should be propogated to all businesses.
        if (in_bizzie_mode()) {
            $this->template->write('page_title', "Manage All Business' Employees Roles");
            $master_business = get_master_business();
            $content['roles'] = $this->aclroles_model->get(array("business_id" => $master_business->businessID));
            $business_id = $master_business->businessID;
        } else {
            $this->template->write('page_title', "Manage Employees Roles");
            $content['roles'] = $this->aclroles_model->get(array('business_id' => $this->business_id));
            $business_id = $this->business_id;
        }
        //$this->load->model("aclresource_model");
        //$aclresources = $this->aclresource_model->get_aprax(array("business_id" => $this->business->businessID));
        $aclresources = $this->aclroles_model->get_resources();
        $formatted_aclresources = array();
        foreach ($aclresources as $aclresource) {
            $formatted_aclresources[$aclresource->aclgroup][] = array('resource_id' => $aclresource->id, 'resource' => $aclresource->resource, 'description' => $aclresource->description);
        }

        $content['resources'] = $formatted_aclresources;
        $content['business_id'] = $business_id;
        $this->data['content'] = $this->load->view('admin/employees/roles', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * UI for managing employee level resource access
     */
    public function employee_roles($employee_id = null)
    {
        if (!is_numeric($employee_id)) {
            throw new \InvalidArgumentException("The employee ID must be passed as segment 4 of the URL");
        }
        $this->load->model('aclroles_model');
        $employee = new Employee($employee_id);
        $content['business_employee_id'] = $employee->getBusinessEmployeeID();
        $content['employee'] = $employee;

        $this->load->model("aclresource_model");
        $resources = $this->aclresource_model->get_aprax();
        $resource = array();
        foreach ($resources as $r) {
            $resource[$r->aclgroup][] = array('resource_id' => $r->id, 'resource' => $r->resource, 'desription' => $r->description);
        }
        $content['resources'] = $resource;
        $content['business_id'] = $this->business_id;
        $this->template->add_js('js/update_roles.js');
        $this->template->write('page_title', 'Employee Roles');

        $this->renderAdmin('employee_roles', $content);
    }

    /**
     * function that handles the post from the employee_roles view and echos the response.
     * This is a function that is called using ajax
     *
     * $_POST values
     * -------------
     * resource_id
     * buser_id
     *
     */
    public function update_resource_access_ajax()
    {

    }

    /**
     * Deletes a timecard
     */
    public function timecard_delete($delete = null, $extra = null)
    {
        $content['business'] = $this->business;
        if ($delete) {

            //get all the info for this timecard
            $getTimecard = "select * from timeCard
                            join timeCardDetail on timeCard_id = timeCardID
                            where timeCardDetailID = $delete";
            $q = $this->db->query($getTimecard);
            $content['timecard'] = $timecard = $q->result();

            $delete1 = "DELETE FROM timeCardDetail WHERE timeCardDetailID = '$delete';";
            $q = $this->db->query($delete1);
            set_flash_message('success', "Punch deleted");

            redirect('/admin/employees/timecard/' . $timecard[0]->timeCardID . '/' . $extra);
        }
    }

    /**
     *
     *
     * @throws \Database_Exception
     */
    public function timeoff()
    {
        try {
            $sqlGetEmp = "select manager_id, firstName, lastName, email from business_employee be INNER JOIN employee e ON employeeID = employee_id where ID = $this->buser_id";
            $q = $this->db->query($sqlGetEmp);
            if ($this->db->_error_message()) {
                throw new \Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $content['emp'] = $emp = $q->result();


            if ($_POST) {
                $timeoff = get_business_meta($this->business_id, 'timeoff');
                $emails = $timeoff;
                $subject = 'Time Off For ' . getPersonName($emp[0]);
                $body = 'Time Off Request For ' . getPersonName($emp[0]) . '<br>Email: ' . $emp[0]->email . '<br><br><strong>Requested Dates<br>From:</strong> ' . $_POST['from'] . '<br><strong>To:</strong>' . $_POST['to'] . '<br><strong>Reason:</strong> ' . $_POST['reason'];

                queueEmail(SYSTEMEMAIL, getPersonName($emp[0]), $emails, $subject, $body, array(), '', $this->business_id);
                set_flash_message('success', "Your time off request has been sent to " . $emails);
                redirect($this->uri->uri_string());
            }

            $this->template->add_js('js/jquery-ui-1.8.16.custom.min.js');
            $this->template->add_css('css/blue_custom/jquery-ui-1.8.17.custom.css');
            $this->template->write('page_title', 'Time Off Request');
            $this->renderAdmin('timeOff', $content);
        } catch (\Exception $e) {
            send_exception_report($e);
        }
    }

    public function details($emp = null)
    {
        checkAccess('manage_employees');

        $sqlGetEmp = "select * from employee where employeeID = $emp";
        $q = $this->db->query($sqlGetEmp);
        $content['emp'] = $emp = $q->result();

        $this->template->write('page_title', 'Employee Details');
        $this->renderAdmin('details', $content);
    }

    public function presser_scans($lastItem = null)
    {
        $currentEmp = get_employee_id($this->buser_id);

        $this->load->library('timecards');
        $this->load->library('production_metrics');

        if (isset($_POST['change'])) {
            //expire a cookie
            setcookie("pressingStation", "", time() - 3600);
            set_flash_message('success', "Cookie Cleared");
            redirect('/admin/employees/presser_scans');
        }
        if (isset($_POST['pressingStation'])) {
            setCookie("pressingStation", $_POST['pressingStation'], 0);
            set_flash_message('success', "Cookie Set");
            redirect('/admin/employees/presser_scans');
        }

        $this->load->model('pressingstation_model');

        if (!isset($_COOKIE['pressingStation']) or $_COOKIE['pressingStation'] == '') {
            //get pressing stations
            $content['stations'] = $this->pressingstation_model->get_aprax(array(
                'business_id' => $this->business_id,
            ));
        } else {
            //get the info for this pressing station
            $content['stationInfo'] = $this->pressingstation_model->get_by_primary_key($_COOKIE['pressingStation']);

            //if it is before 10am pass it yesterday
            if (date('H') < '10') {
                $pphDate = date('Y-m-d', strtotime("-1 day"));
            } else {
                $pphDate = date('Y-m-d');
            }

            //get employee's PPH
            $content['todaysHours'] = $this->timecards->todaysHours($currentEmp, $pphDate);

            //get past 6 days
            for ($i = 0; $i <= 5; $i++) {
                $histDate = date('Y-m-d', strtotime($pphDate . "-$i day"));
                $content['history'][$histDate] = $this->production_metrics->pphPressing($currentEmp, $histDate);
            }

            $content['pph'] = $pph = $this->production_metrics->pphPressing($currentEmp, $pphDate);
            $content['actualPPH'] = 0;

            if ($pph['itemCount'] > 0 && $content['todaysHours']['todayHrs'] > 0) {
                $content['actualPPH'] = number_format($pph['itemCount'] / $content['todaysHours']['todayHrs'], 1, '.', '');
            }

            $content['lastItem'] = $lastItem;
            $content['picture'] = array();

            if ($lastItem) {
                $this->load->model('picture_model');
                // print out the picture of the item
                $content['picture'] = $this->picture_model->get_aprax(array(
                    'item_id' => $lastItem,
                ));
            }
        }

        $this->template->write('page_title', 'Presser Scans');
        $this->renderAdmin('presser_scans', $content);
    }

    public function presser_scans_add()
    {
        $barcode = trim($this->input->post("barcode"));

        if (empty($barcode)) {
            set_flash_message('error', "You did not enter a barcode!");
            redirect_with_fallback('/admin/employees/presser_scans/');
        }

        $getItem = "SELECT * FROM barcode
                    JOIN item ON (itemID = item_id)
                    WHERE barcode = ?
                    AND barcode.business_id = ?
                    ORDER BY item_id DESC
                    LIMIT 1";
        $q = $this->db->query($getItem, array($barcode, $this->business_id));
        $item = $q->row();

        if (!$item) {
            set_flash_message('error', "Barcode $barcode Not Found.  Please scan again");
            redirect_with_fallback('/admin/employees/presser_scans/');
        }

        //see if this item has special specifications.  If so, show that
        $this->load->model('item_model');
        $specifications = $this->item_model->get_item_specifications($item->itemID);

        if ($specifications[0] && !$this->input->post("specificationConfirm")) {
            $content['specifications'] = $specifications;
            $content['barcode'] = $_POST['barcode'];

            $this->setPageTitle('Confirm Specifications');
            $this->renderAdmin('confirm_specifications', $content);
            return;
        }

        $this->load->model('pressingstation_model');
        $station = $this->pressingstation_model->get_by_primary_key($_COOKIE['pressingStation']);
        $description = "{$station->description}  ({$station->pressingStationID})";

        $currentEmp = get_employee_id($this->buser_id);

        // don't let the same person scan the same item on the same machinen twice
        $getHistory = "select * from itemHistory
                                where item_id = ?
                                and date_format(date, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d')
                                and creator = ?
                                and description = ?";
        $q = $this->db->query($getHistory, array($item->item_id, $currentEmp, $description));
        $history = $q->result();

        if ($history) {
            set_flash_message('error', "This barcode has already been scanned");
        } else {
            $this->load->model('itemhistory_model');
            $this->itemhistory_model->save(array(
                'item_id' => $item->item_id,
                'description' => $description,
                'date' => gmdate('Y-m-d H:i:s'),
                'creator' => $currentEmp,
                'rework' => $station->rework,
            ));
            set_flash_message('success', "Barcode scanned");
        }

        redirect('/admin/employees/presser_scans/' . $item->item_id);
    }

}
