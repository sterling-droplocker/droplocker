<?php

class Barcode_Lengths extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('barcodelength_model');
    }

    /**
     * Expects an AJAX request
     */
    public function get_valid_barcode_lengths()
    {
        echo json_encode($this->barcodelength_model->get_all($this->business_id));
    }
    /**
     * Expects 'length' as a GET parameter
     * @throws Exception
     */
    public function delete()
    {
        if (!in_bizzie_mode() || is_superadmin()) {
            try {
                if (!$this->input->get("length")) {
                    throw new Exception("'length' must be passed as a GET parameter.");
                }
                $length = $this->input->get("length");
                if (!is_numeric($length)) {
                    throw new Exception("'length' must be numeric.");
                }
                $result = $this->barcodelength_model->delete($length, $this->business_id);
                if ($result) {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array("status" => "success", "message" => "Deleted barcode length"));
                    } else {
                        set_flash_message("success", "Deleted barcode length.");
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                } else {
                    throw new Exception("The barcode was not deleted.");
                }
            } catch (Exception $e) {
                send_exception_report($e);
                if ($this->input->is_ajax_request()) {
                    echo json_encode(array("status" => "error", "message" => "Could not delete barcode length."));
                } else {
                    set_flash_message("error", "Could not delete barcode length.");
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        } else {
            throw new LogicException("Only superadmin can delete barcode lengths when the application in bizzie mode");
        }
    }
    /**
     * Expects 'length' as a POST parameter
     * @throws Exception
     */
    public function add()
    {
        if (!in_bizzie_mode() || is_superadmin()) {
            try {
                if (!$this->input->post("length")) {
                    throw new Exception("'length' must be passed as a POST parameter.");
                }
                $length = $this->input->post("length");
                if (!is_numeric($length)) {
                    throw new Exception("'length' must be numeric.");
                }
                $result = $this->barcodelength_model->insert($length, $this->business_id);
                if ($result) {
                    if ($this->input->is_ajax_request()) {
                        echo json_encode(array("status" => "success", "message" => "Added new barcode length"));
                    } else {
                        set_flash_message("success", "Added new barcode length");
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                } else {
                    throw new Exception("Could not add new barcode length.");
                }
            } catch (Exception $e) {
               if ($e->getCode() == 1062) {
                   if ($this->input->is_ajax_request()) {
                       echo json_encode(array("status" => "error", "message" => "Barcode length already exists."));
                   } else {
                       set_flash_message("error", "Barcode length already exists");
                       redirect($_SERVER['HTTP_REFERER']);
                   }
               }
               send_exception_report($e);
               if ($this->input->is_ajax_request()) {
                   echo json_encode(array("status" => "error", "message" => "Could not add barcode."));
               } else {
                   set_flash_message("error", "Could not add barcode.");
                   redirect($_SERVER['HTTP_REFERER']);
               }
            }
        } else {
            throw new \LogicException("Only superadmin can add new barcode lengths when the server is in Bizzie mode");
        }
    }
}
