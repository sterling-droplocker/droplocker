<?php

use App\Libraries\DroplockerObjects\Order;

class CreditCards extends MY_Admin_Controller
{

    /**
     * Updates a customer's billing information
     * Expects take the following POST parameters
     *  kioskAccess
     *  cardNumber
     *  expMo two digit month format
     *  expYr two digit month format
     *  firstName   
     *  lastName
     *  address
     *  city
     *  state
     *  zip 
     *  csc
     *  customerID
     * 
     *  Note, this action redirects to the request's HTTP_REFERER
     *  The results of the authorization are set in a flash message
     */
    public function update()
    {
        $this->load->library("creditcard");
        try {
            $results = $this->creditcard->authorizePOST();
        } catch (CreditCardProcessorException $creditCardProcessorException) {
            set_flash_message("error", $creditCardProcessorException->getMessage());
            $this->session->set_flashdata("post", serialize($_POST));
            return redirect($this->agent->referrer());
        }

        $capture = ($this->input->post("capture") == "false")?false:true;

        if ($results['status'] == "success" && $capture) {
            if (!empty($results['data']['captureResults'])) {
                $paidMessage = "<h3> The following orders have been paid for: </h3>";
                $errorMessage = null;
                foreach ($results['data']['captureResults'] as $captureResult) {
                    if (isset($captureResult['status']) &&$captureResult['status'] == 'success') {
                        $paidMessage .= "Order: <a href='/account/orders/order_details/" . $captureResult['order_id'] . "'>" . $captureResult['order_id'] . "</a><br>";
                    } else {
                        $errorMessage .= "Order: <a href='/account/orders/order_details/" . $captureResult['order_id'] . "'>" . $captureResult['order_id'] . "</a><br>";
                    }
                }
                if (!empty($errorMessage)) {
                    $errorMessage = "<h3> The following orders still need to be paid for: </h3> $errorMessage";
                }
            }
            $successMessage = sprintf("%s %s %s", $results['message'], $paidMessage, $errorMessage);
            set_flash_message('success', $successMessage);
        } else {
            $this->session->set_flashdata("post", serialize($_POST));
            set_flash_message("error", "We are sorry, but we received the following response from your credit card company: {$results['message']}");
        }
        redirect($this->agent->referrer());
    }

    function transbank_return()
    {   
        if (empty($_POST)) {
            exit;
        }

        if (isset($_POST['TBK_TOKEN'])) {
            $transbank = new \DropLocker\Processor\Transbank(array('business_id' => $this->business_id));
            $result = $transbank->finishInscription($_POST['TBK_TOKEN'], $_GET['params'], $this->business_id);
           
            set_flash_message($result['status'], $result['message']);

            $params_array = explode(',', $_GET['params']);
            $order_id = $params_array[0];
            $order = new Order($order_id);
            redirect('/admin/customers/billing/'.$order->customer_id);
        }
    }





}
