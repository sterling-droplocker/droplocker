<?php

use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Item;

class Picture extends MY_Admin_Controller
{
    /**
     * Deletes a picture
     * The user session business ID must match the business ID of the picture.
     */
    public function delete()
    {
        if (!$this->input->get("pictureID")) {
            set_flash_message("error", "'pictureID' must be passed as a GET parameter.");
        } else {

            $pictureID = $this->input->get("pictureID");
            try {
                $picture = new \App\Libraries\DroplockerObjects\Picture($pictureID);
                if ($picture->business_id != $this->business_id) {
                    set_flash_message("error", "This picture does not belong to your business");
                } else {
                    $picture->delete();
                }
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Picture not found");
            }

        }
        
        $referrer = $this->agent->referrer();
        $query_string_vars = array();
        parse_str($referrer, $query_string_vars);
        $referrer = str_replace('&addPic='.$query_string_vars["addPic"], '', $referrer);
        redirect($referrer);
    }

    public function index()
    {
        $request = $this->input->get(NULL, TRUE);

        if (!isset($request['itemId']) and !isset($request['barcode'])) {
            echo 'ERROR: An item ID or Barcode needs to be passed to this form. <br><br><- Return to <a href="/admin/orders/details/'.$request['order'].'">Edit Items</a><br><br>';
            return;
        }

        $this->load->model('item_model');
        $this->load->model('barcode_model');
        $this->load->model('picture_model');

        $select1 = "select * from barcode
            join item on barcode.item_id = itemID
            where barcode = ?
            and barcode.business_id = ?";

        $select = $this->db->query($select1, array($request['barcode'], $this->business_id));
        $line = $select->row();

        //if this barcode is not in the system already
        if (empty($line)) {
            if (isset($request['create'])) {

                //create the item in the item table
                $itemID = $this->item_model->save(array(
                    'product_process_id' => 0,
                    'business_id' => $this->business_id,
                    'note' => $request['notes'],
                ));

                $this->barcode_model->save(array(
                    'item_id' => $itemID,
                    'business_id' => $this->business_id,
                    'barcode' => $request['barcode'],
                ));

                $line = $this->item_model->get_by_primary_key($itemID);

            } else {
                $content['barcode'] = $request['barcode'];
                $this->template->set_template('admin');
                $this->template->write('page_title','Edit Item');
                $this->data['content'] = $this->load->view('/admin/items/barcode_not_found', $content, true);
                $this->template->write_view('content', 'inc/full_column', $this->data);
                $this->template->render();
                return;
            }
        }

        //Show an overview of the item
        if (!empty($request['addPic'])) {
            $this->picture_model->save(array(
                'item_id' => $line->itemID,
                'file' => $request['addPic'],
                'employee_id' => $this->buser_id,
                'business_id' => $this->business_id,
            ));
        }

        $content['item'] = $this->item_model->get_by_primary_key($line->itemID);
        $content['item_histories'] = $this->item_model->get_item_history($line->itemID);
        $content['upcharges'] = $this->item_model->get_item_upcharges($line->itemID);
        $content['specifications'] = $this->item_model->get_item_specifications($line->itemID);

        $this->load->model('itempreference_model');
        $content['item_preferences'] = $this->itempreference_model->get_preferences_names($line->itemID);

        $content['order_id'] = null;


        $content['pictures'] = $this->picture_model->get_aprax(array("item_id" => $content['item']->itemID));

        $this->load->model("product_process_model");
        $product_process = $this->product_process_model->get_by_primary_key($content['item']->product_process_id);
        
        if (empty($product_process)) {
            echo 'ERROR: A valid Barcode needs to be passed. Please, try again.';
            return;
        }

        $this->load->model("product_model");
        $content['product'] = $this->product_model->get_by_primary_key($product_process->product_id);

        $this->load->model("process_model");
        $content['process'] = $this->process_model->get_by_primary_key($product_process->process_id);

        $this->load->model("itemissue_model");
        $content['itemIssues'] = $this->itemissue_model->get_aprax(array("item_id" => $content['item']->itemID));

        $this->load->model("barcode_model");
        $content['barcode'] = $this->barcode_model->get_one(array("item_id" => $content['item']->itemID));

        $this->load->model("customer_item_model");
        $content['customer_items'] = $this->customer_item_model->get_with_customer_information(array("item_id" => $content['item']->itemID));

        // show customer's DC prefs in item
        if (count($content['customer_items']) == 1) {
            $customer_id = $content['customer_items'][0]->customer_id;

            $this->load->model('customerpreference_model');
            $customer_preferences = $this->customerpreference_model->get_preferences_names_for_cateogry($customer_id, $content['product']->productCategory_id);

            // check that item preferences matches customer preferences
            $content['changes'] = array();
            foreach ($customer_preferences as $name => $value) {
                if (isset($content['item_preferences'][$name]) && $content['item_preferences'][$name] != $value) {
                    $content['changes'][$name] = "Note: Customer's $name setting '{$customer_preferences[$name]}' does not match item DC setting '{$content['item_preferences'][$name]}'";
                } else {
                    $content['item_preferences'][$name] = $value . '*';
                }
            }
        }


        $this->template->set_template('admin');
        $this->template->write('page_title','Edit Item');

        $view = $this->agent->is_browser("Internet Explorer") && $this->agent->version < 8 ? 'view_internet_explorer' : 'view';
        $this->data['view'] = $view;
        $this->data['content'] = $this->load->view("/admin/items/$view", $content, true);
        $this->template->write_view('content', 'inc/full_column', $this->data);
        $this->template->render();
    }

}
