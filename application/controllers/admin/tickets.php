<?php

use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\TicketLabel;
use App\Libraries\DroplockerObjects\TicketStatus;
use App\Libraries\DroplockerObjects\TicketType;
use App\Libraries\Settings;

class Tickets extends MY_Admin_Controller
{
    public $submenu = "admin";

    // UI for generating index page
    public function index()
    {
        $this->load->model('employee_model');
        $content['employees'] = $this->employee_model->get(array('business_id' => $this->business_id, 'order_by' => 'firstName', 'active' => '1'));
        $sql = 'SELECT * FROM ticketStatus WHERE business_id = '.intval($this->business_id);
        $query = $this->db->query($sql);
        $content['ticketStatus'] = $query->result();       
        $this->template->write('page_title', 'DL Ticketing System', true);
        $this->renderAdmin("index", $content);
    }

    // Primary function for generating JSON data object for displaying
    // tickets on index page
    public function get_filter_tickets()
    {
        $this->output->enable_profiler(FALSE);


        $order_id       = $this->input->post('order_id');
        $filterStatus   = $this->input->post('filterStatus');
        $filterEmployee = $this->input->post('filterEmployee');

        $whereClause = "";

        $this->load->model('ticketstatus_model');
        $closedStatusId = $this->ticketstatus_model->getClosedStatusId($this->business_id);
     
        if (!empty($filterStatus)) {
            $whereClause .=" AND ticket.ticketstatus_id = $filterStatus";
        } elseif (empty($order_id)) {
            $whereClause .=" AND ticket.ticketstatus_id !=$closedStatusId";
        }

        if (!empty($filterEmployee) && $filterEmployee != 'All Employees' && $filterEmployee
            != 'Unassigned') {
            $whereClause .=" AND ticket.employee_id = $filterEmployee";
        }

        if ($filterEmployee == 'Unassigned') {
            $whereClause .=" AND ticket.employee_id = 0";
        }

        if (!empty($order_id)) {
            $whereClause .=" AND ticket.order_id = $order_id";
        }


        $getTickets = "SELECT
                        ticketID, ticketSource, IFNULL(ticketType.description,'--') as ticketType,
                        (select count(*) FROM ticketNote where noteType in ('Customer','Customer Reply', 'Internal') AND ticketNote.ticket_id = ticket.ticketID) as numEmail,
                        ifNull(ticket.customerName,'') customerName,
                        ticketStatus.description as status,
                        employee_id,
                        ticket.dateUpdated,
                        ticketStatus.ticketStatusType_id,
                        issue,
                        title,
                        if(ticket.employee_id=0,'Unassigned', concat(employee.firstName, ' ', employee.lastName) ) as assignedTo,
                        nextActionDate,
                        priority
                        FROM ticket
                        INNER JOIN ticketStatus ON ticket.ticketstatus_id = ticketStatus.ticketstatusID
                        INNER JOIN ticketStatusType on ticketStatus.ticketStatusType_id = ticketStatusType.ticketStatusTypeID
                        LEFT JOIN ticketType ON ticket.ticketType_id = ticketType.ticketTypeID
                        LEFT join employee on employeeID = ticket.employee_id
                        WHERE ticket.business_id = $this->business_id
                        ".$whereClause."
                        ORDER BY dateUpdated desc";

        
        $query      = $this->db->query($getTickets);
        $tickets    = $query->result();

        for ($i = 0; $i < count($tickets); $i++) {

            $dateUpdate     = $tickets[$i]->dateUpdated;
            $dateNextAction = $tickets[$i]->nextActionDate;

            $localDateUpdate = new DateTime(convert_from_gmt_aprax($dateUpdate, "Y-m-d H:i:s"));
            $localNextAction = new DateTime(convert_from_gmt_aprax($dateNextAction, "Y-m-d H:i:s"));

            $currentTime                 = new DateTime();
            $diffObj                     = $currentTime->diff($localDateUpdate);
            $tickets[$i]->updatedSeconds = $diffObj->s + $diffObj->i * 60 + $diffObj->h * 3600 + $diffObj->d * 86400;
            if ($currentTime > $localNextAction) {
                $tickets[$i]->overDue = 1;
            } else {
                $tickets[$i]->overDue = 0;
            }

            // Assign the correct css to the status button
            switch ($tickets[$i]->ticketStatusType_id) {
                case 1:
                    $tickets[$i]->statusCSS = "New";
                    break;
                case 3:
                    $tickets[$i]->statusCSS = "Closed";
                    break;
                default;
                    $tickets[$i]->statusCSS = "Open";
            }

            // In the event that the ticket was initially created by email
            // increment the communication counter by one
            if ($tickets[$i]->ticketSource == "email") {
                $tickets[$i]->numEmail++;
            }

            // Format the display labels:  convert from seconds to minutes.hours,days
            $tickets[$i]->displayUpdate = $this->generateLastUpdatedTime($tickets[$i]->updatedSeconds);
            // Obtain labels associated with the ticket
            $tickets[$i]->ticketLabels  = $this->getTicketLabels($tickets[$i]->ticketID);
        }

        $this->outputJSONsuccess("success", json_encode($tickets));
    }

    // returns available labels for business
    public function getLabels()
    {
        $this->output->enable_profiler(FALSE);
        $this->load->model('ticketlabel_model');
        $labelData = $this->ticketlabel_model->get_aprax(array('business_id' => $this->business_id), 'description', 'description as label');

        return json_encode($labelData);
    }

    // searches for customer
    public function getCustomerById()
    {
        $this->output->enable_profiler(FALSE);
        $customer_id = $this->input->post('customer_id');

        $this->load->model('customer_model');
        $customerData = $this->customer_model->get_aprax(array('business_id' => $this->business_id, 'customerID' => $customer_id));

        if (sizeof($customerData) > 0) {
            $this->outputJSONsuccess("success", json_encode($customerData));
        } else {
            $this->outputJSONerror('No matching customer');
        }
    }

    // searches for matching orders
    public function searchOrders()
    {
        $this->output->enable_profiler(FALSE);

        $order_id = $this->input->post('order_id');

        $sql = "SELECT orders.orderID, orders.customer_id, customer.firstName, customer.lastName, bag.bagNumber,customer.phone,customer.email,
                locker.lockerName, locker.lockerID, location.locationID, location.address
                FROM `orders`
                LEFT JOIN customer ON customer.customerID = orders.customer_id
                INNER JOIN bag ON bag.bagID = orders.bag_id
                INNER JOIN locker ON locker.lockerID = orders.locker_id
                INNER JOIN location ON location.locationID = locker.location_id
                WHERE orders.orderID = $order_id";

        $query = $this->db->query($sql);

        $order = $query->result();
        if ($order) {
            $array = array('status' => 'success',
                'bagNumber' => $order[0]->bagNumber,
                'firstName' => $order[0]->firstName,
                'lastName' => $order[0]->lastName,
                'completeName' => getPersonName($order[0]),
                'customer_id' => $order[0]->customer_id,
                'locker' => $order[0]->lockerName,
                'locker_id' => $order[0]->lockerID,
                'location' => $order[0]->address,
                'phone' => $order[0]->phone,
                'email' => $order[0]->email,
                'location_id' => $order[0]->locationID,
                'orderID' => $order[0]->orderID);
        } else {
            $array = array('status' => 'fail', 'error' => 'Order not found');
        }

        echo json_encode($array);
    }

    // function for closing tickets, accessed via AJAX call
    public function closeTicket()
    {
        $this->output->enable_profiler(FALSE);

        $ticketID = $this->input->post('ticketID');

        // validate existence of ticketID
        if (empty($ticketID)) {
            $this->outputJSONerror('No ticketId present');
        }

        $this->load->model('ticketstatus_model');
        $closedStatusId = $this->ticketstatus_model->getClosedStatusId($this->business_id);

        // Close ticket
        $this->load->model('ticket_model');
        $this->ticket_model->save(array("ticketID" => $ticketID,
            "dateUpdated" => convert_to_gmt_aprax(),
            "ticketstatus_id" => $closedStatusId));

        $this->outputJSONsuccess("success", "");
    }

    // function for generating ticket lables
    protected function getTicketLabels($ticketId)
    {

        $getLabels = "SELECT description, color
					  FROM ticketLabel, ticketLabelMap
					  WHERE
					  ticketLabel.ticketLabelID = ticketLabelMap.ticketLabel_ID and
					  ticketLabelMap.ticket_ID = $ticketId";

        $query     = $this->db->query($getLabels);
        $labelData = $query->result();

        $outStr = "";

        //format label before returning it
        foreach ($labelData as $label) {
            $outStr .= "<div style=padding:2px;margin-right:10px;margin-bottom:10px;display:inline;background:".$label->color.";'>".$label->description."</div>";
        }

        return $outStr;
    }

    // function for formatting time
    protected function generateLastUpdatedTime($numSeconds)
    {
        // If the ticket came in less then 60 seconds ago, label it in seconds
        if ($numSeconds < 60) {
            $displayNum = $numSeconds;
            $displayStr = "$displayNum second";
            // If the ticket came in less then 60 minutes ago, label it in minutes
        } elseif ($numSeconds > 60 && $numSeconds <= 3600) {
            $displayNum = floor($numSeconds / 60);
            $displayStr = "$displayNum minute";
            // If the ticket cam in less then 24 hours ago, label it in hours
        } elseif ($numSeconds > 3600 & $numSeconds < 86400) {
            $displayNum = floor($numSeconds / 3600);
            $displayStr = "$displayNum hour";
            // Otherwise label it in days
        } else {
            $displayNum = floor($numSeconds / 86400);
            $displayStr = "$displayNum day";
        }

        if ($displayNum == 1) {
            $displayStr .= " ago";
        } else {
            $displayStr .= "s ago";
        }

        return $displayStr;
    }

    /**
     * UI for adding a new ticket
     */
    public function add_ticket()
    {
        
        $this->load->model('employee_model');
        $this->load->model('tickettype_model');
        $this->load->model('employee_model');

        //die(var_dump($this->input->post()));
        //role filter
        if ($this->input->post('typeFilter')) {
            $ticketType = $this->tickettype_model->get_by_primary_key($this->input->post('typeFilter'));
            $roles = explode(',', $ticketType->roles);
            $employees = $this->employee_model->get_emp_by_role($roles);
            echo json_encode($employees);
            exit;
        }

        $content['ticketTypes'] = $this->tickettype_model->get_by_business_id($this->business_id);
        $content['employees'] = $this->employee_model->get(array('business_id' => $this->business_id, 'order_by' => 'firstName', 'active' => '1'));

        // Load ticket status for business
        $this->load->model('ticketstatus_model');
        $content['ticketStatus'] = $this->ticketstatus_model->get_aprax(array('business_id' => $this->business_id));


        // Generate location and sort locations for business
        $this->load->model('location_model');
        $content['locations'] = array();

        $locations = $this->location_model->get_aprax(array('business_id' => $this->business_id));
        foreach ($locations as $l) {
            $content['locations'][$l->locationID] = sprintf('%s (%s)', substr($l->address, 0, 35), substr($l->companyName, 0, 35));
        }
        natsort($content['locations']);

        // Generate ticket labels associated with business
        $this->load->model('ticketlabel_model');
        $content['availableLabels'] = $this->ticketlabel_model->get_aprax(array('business_id' => $this->business_id), 'description', '');

        $this->template->add_css('css/admin/style.css');
        $this->template->add_js("js/ticket.js");
        $this->template->write('page_title', 'Add Ticket', true);

        $this->renderAdmin('add_ticket', $content);
    }

    public function create_ticket()
    {
      
        // Extract the email address
        $email = $this->input->post('email');

        // If there's a last name present, then we have a phone call
        // otherwise create a employee ticket
        $customer = $this->input->post('customer');

        $ticketSource = "Employee";
        if (!empty($customer)) {
            $ticketSource = "Phone";
        }

        // reformat time string for mysql
        $oTimeString = $this->input->post('nextActionDate');
        $date1       = DateTime::createFromFormat('m/d/Y H:i', $oTimeString);

        if ($date1 == false) {
            set_flash_message('error', "Incorrect date format on 'Expected' field $oTimeString");
            redirect('/admin/tickets/add_ticket');
        }

        $insertTimeString = $date1->format('Y-m-d H:i:s');

        // Create ticket
        $ticket                  = new \App\Libraries\DroplockerObjects\Ticket();
        $ticket->business_id     = $this->business_id;
        $ticket->order_id        = $this->input->post('order_id');
        $ticket->customer_id     = $this->input->post('customer_id');
        $ticket->employee_id     = $this->input->post('employee_id');
        $ticket->location_id     = $this->input->post('location_id');
        $ticket->locker_id       = $this->input->post('locker_id');
        $ticket->bag_id          = $this->input->post('bag_id');
        $ticket->title           = $this->input->post('subject');
        $ticket->issue           = $this->input->post('description');
        $ticket->customerPhone   = $this->input->post('phone');
        $ticket->dateCreated     = convert_to_gmt_aprax();
        $ticket->dateUpdated     = convert_to_gmt_aprax();
        $ticket->ticketstatus_id = $this->input->post('ticketstatusid');
        $ticket->nextActionDate  = convert_to_gmt_aprax($insertTimeString);
        $ticket->customerEmail   = $email;
        $ticket->customerName    = $customer;
        $ticket->priority        = $this->input->post('priority');
        $ticket->ticketSource    = $ticketSource;
        $ticket->ticketType_id   = $this->input->post('ticketType_id');
        $insert_id               = $ticket->save();


        $formAction      = $this->input->post('formAction');
        $formActionArray = explode(',', $formAction);


        // This is being done as a switch for consistency/to allow
        // for easy extension down the road
        foreach ($formActionArray as $action) {
            switch ($action) {
                case "Acknowledge":

                    if ($email) {
                        $this->load->helper('customer');

                        send_ticket_system_acknowledgement($email, $insert_id, $this->business_id, $ticket->customer_id);
                    }
                    break;
            }
        }
		
		if ($ticket->employee_id) {
			$this->load->helper('employee');
            send_ticket_system_email_employee($ticket->employee_id, $insert_id, $this->business_id);
	
		}

        // Get labels
        $labelList = $this->input->post('labelArray');

        // Assign labels to ticket
        if ($labelList) {
            $this->assignTicketLabels($insert_id, $this->business_id,
                json_decode($labelList));
        }
        redirect('admin/tickets');
    }

    // function for assigning ticket labels to tickets
    function assignTicketLabels($ticket_id, $business_id, $labelArray)
    {
        $this->load->model('ticketlabelmap_model');

        foreach ($labelArray as $labelID) {
            $this->ticketlabelmap_model->save(array("ticket_id" => $ticket_id,
                "ticketLabel_id" => $labelID));
        }
    }

    // function for assigning ticket labels to tickets
    function updateTicketLabels($ticket_id, $business_id, $labelArray)
    {
        $this->load->model('ticketlabelmap_model');

        $this->ticketlabelmap_model->delete_aprax(array("ticket_id" => $ticket_id));

        $this->assignTicketLabels($ticket_id, $business_id, $labelArray);
    }

    protected function getTicketNotes($ticket_id)
    {

        $sql = "SELECT note, dateCreated, noteType, firstName, lastName
                FROM ticketNote tn LEFT JOIN employee e ON (tn.employee_id = e.employeeID)
                WHERE
                tn.ticket_id = $ticket_id order by dateCreated desc;";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function edit_ticket($ticket_id = null)
    {
        if (!$ticket_id) {
            set_flash_message('error', "Missing ticket ID");
            redirect('admin/tickets');
        }

        $this->load->model('ticketnote_model');
        $this->load->model('tickettype_model');

        $content['ticket_id'] = $ticket_id;
        $ticket               = \App\Libraries\DroplockerObjects\Ticket::getTicket($ticket_id);
        $content['ticket']    = $ticket;
        $content['ticketTypes'] = $this->tickettype_model->get_by_business_id($this->business_id);
     
        $allNotes = $this->getTicketNotes($ticket_id);
        
        $internalNotes = [];
        $emailNotes    = [];

        foreach ($allNotes as $oneNote) {
            $oneNote->localCreateTime = convert_from_gmt_aprax($oneNote->dateCreated, "Y-m-d H:i:s");
            if ($oneNote->noteType == "Internal") {
                array_push($internalNotes, $oneNote);
            } else {
                array_push($emailNotes, $oneNote);
            }
        }

        $content['internalNotes'] = $internalNotes;
        $content['emailNotes']    = $emailNotes;

        //get the employee assigned to the ticket
        if ($ticket->employee_id != 0) {
            $employee             = new Employee($ticket->employee_id);
            $content['firstName'] = $employee->firstName;
            $content['lastName']  = $employee->lastName;
        }
        $content['employees'] = $this->employee_model->get(array('business_id' => $this->business_id, 'order_by' => 'firstName', 'active' => '1'));

        // convert time to local time
        $content['nextActionDate'] = convert_from_gmt_aprax($ticket->nextActionDate,
            "Y-m-d H:i:s");
        $content['dateCreated']    = convert_from_gmt_aprax($ticket->dateCreated,
            "Y-m-d H:i:s");
        $content['dateUpdated']    = convert_from_gmt_aprax($ticket->dateUpdated,
            "Y-m-d H:i:s");

        // performs calculate to display "ago" time
        $localDateUpdate = new DateTime($content['dateUpdated']);
        $currentTime     = new DateTime();
        $diffObj         = $currentTime->diff($localDateUpdate);
        $updatedSeconds  = $diffObj->s + $diffObj->i * 60 + $diffObj->h * 3600 + $diffObj->d
            * 86400;

        $content['displayUpdate'] = $this->generateLastUpdatedTime($updatedSeconds);

        // load available ticket status
        $this->load->model('ticketstatus_model');
        $content['ticketStatus'] = $this->ticketstatus_model->get_non_new_status($this->business_id);

        // load canned responses
        $this->load->model('ticketcannedresponse_model');
        $content['cannedResponses'] = $this->ticketcannedresponse_model->get_aprax(array(
            'business_id' => $this->business_id));

        // load all available labels
        $this->load->model('ticketlabel_model');
        $content['availableLabels'] = $this->ticketlabel_model->get_aprax(array(
            'business_id' => $this->business_id), 'description', '');

        // get associated labels with this ticket
        // load all available labels
        $this->load->model('ticketlabelmap_model');
        $selectedLabels = $this->ticketlabelmap_model->get_aprax(array('ticket_id' => $ticket_id));

        $selectLabelArray = array();
        foreach ($selectedLabels as $selectedLabel) {
            array_push($selectLabelArray, $selectedLabel->ticketLabel_id);
        }

        $content['selectedLabels'] = $selectLabelArray;

        // load all locations
        $this->load->model('location_model');
        $locations = $this->location_model->get_aprax(array(
            'business_id' => $this->business_id,
        ));
        foreach ($locations as $l) {
            $content['locations'][$l->locationID] = sprintf('%s (%s)',
                substr($l->address, 0, 35), substr($l->companyName, 0, 35));
        }
        natsort($content['locations']);

        // load all location lockers
        if($ticket->locationID){
            $this->load->model('locker_model');
            $lockers = $this->locker_model->get_aprax(array(
                'location_id' => $ticket->locationID,
            ));
            foreach ($lockers as $l) {
                $content['lockers'][$l->lockerID] = $l->lockerName;
            }
            natsort($content['lockers']);
        }
        $this->template->add_js("js/ticket.js");
        $this->template->write('page_title', 'Edit Ticket', true);

        $this->renderAdmin('edit_ticket', $content);
    }

    public function update_ticket()
    {
 
        //Sanity fields
        $ticket_id           = $this->input->post('ticket_id');
        $title               = $this->input->post('title');
        $ticketstatus_id     = $this->input->post('ticketstatus_id');
        $ticketType_id       = $this->input->post('ticketType_id');
        $priority            = $this->input->post('priority');
        $nextActionDate      = $this->input->post('nextActionDate');
        $assignedEmployee_id = $this->input->post('employee_id');
        $order_id            = $this->input->post('order_id');
        $locker_id           = $this->input->post('locker_id');
        $location_id         = $this->input->post('location_id');
        $bag_id              = $this->input->post('bag_id');
        $customerName        = $this->input->post('customerName');
        $customerEmail       = $this->input->post('customerEmail');
        $customerPhone       = $this->input->post('customerPhone');
        $emailBody           = $this->input->post('emailBody');
        $internalNote        = $this->input->post('internalNote');
        $formAction          = $this->input->post('formAction');
        $employee_id         = get_employee_id($this->buser_id);
        
        //$customer_id = $this->input->post('customer_id');

        $formActionArray = explode(',', $formAction);

        $this->load->model('ticket_model');

        $this->ticket_model->save(array("ticketID" => $ticket_id,
            "order_id" => $order_id,
            "ticketstatus_id" => $ticketstatus_id,
            "ticketType_id" => $ticketType_id,
            "employee_id" => $assignedEmployee_id,
            "location_id" => $location_id,
            "locker_id" => $locker_id,
            "bag_id" => $bag_id,
            "title" => $title,
            "customerPhone" => $customerPhone,
            "dateUpdated" => convert_to_gmt_aprax(),
            "ticketstatus_id" => $ticketstatus_id,
            "nextActionDate" => convert_to_gmt_aprax($nextActionDate),
            "customerEmail" => $customerEmail,
            "customerName" => $customerName,
            "priority" => $priority
        ));

        foreach ($formActionArray as $action) {
            switch ($action) {
                case "close":
                    $this->load->model('ticketstatus_model');
                    $closedStatusId = $this->ticketstatus_model->getClosedStatusId($this->business_id);

                    $this->ticket_model->save(array("ticketID" => $ticket_id,
                        "dateUpdated" => convert_to_gmt_aprax(),
                        "ticketstatus_id" => $closedStatusId));
                    break;
                case "email":

                    $this->sendAndSaveEmail($ticket_id, $emailBody, $customerEmail, $employee_id);

                    break;
                case "driverNote":
                    $this->load->model('drivernotes_model');
                    $this->drivernotes_model->save(array(
                        "created" => convert_to_gmt_aprax(),
                        "location_id" => $location_id,
                        "note" => $internalNote,
                        "active" => 1,
                        "business_id" => $this->business_id,
                        "createdBy" => $employee_id));
                    break;
                case "internalNote":

                    $this->saveInternalNote($ticket_id, $internalNote,
                        $employee_id);

                    break;
            }
        }

        // Get labels
        $labelList = $this->input->post('labelArray');

        // Assign labels to ticket
        if ($labelList) {
            $this->updateTicketLabels($ticket_id, $this->business_id,
                json_decode($labelList));
        }

        $this->index();
    }

    function saveInternalNote($ticket_id, $internalNote, $employee_id)
    {

        $this->load->model('ticketnote_model');

        $this->ticketnote_model->save(array(
            "ticket_id" => $ticket_id,
            "note" => $internalNote,
            "dateCreated" => convert_to_gmt_aprax(),
            "employee_id" => $employee_id,
            "noteType" => "Internal"));
    }

    function sendAndSaveEmail($ticket_id, $emailBody, $customerEmail, $employee_id, $customer_id = null)
    {

        $this->load->model('ticketnote_model');
        $emailHistoryData = $this->ticketnote_model->get_aprax(array('ticket_id' => $ticket_id), 'dateCreated desc', '');

        if ($emailHistoryData) {
            $emailHistory = $this->formatEmailHistory($emailHistoryData);
        } else {
            $emailHistory = "";
        }
        $saveResult = $this->ticketnote_model->save(
            array(
                "ticket_id" => $ticket_id,
                "note" => $emailBody,
                "dateCreated" => convert_to_gmt_aprax(),
                "employee_id" => $employee_id,
                "noteType" => "Customer")
            );

        if (!$saveResult) {

            set_flash_message("error", "Error updating ticket");
        } else {
            $from_email_name  = get_business_meta($this->business_id, 'from_email_name');
            $customer_support = get_business_meta($this->business_id, 'customerSupport');
            // if we have a comma in the from field, lets just pull the first email
            $customer_support = current(explode(',', $customer_support));
            $this->load->helper('customer');
            $result = send_ticket_system_email($customer_support, $from_email_name, $customerEmail, $ticket_id, $emailBody, $emailHistory, $this->business_id, $customer_id);
            if ($result) {
                set_flash_message("success", 'Ticket updated and email sent');
            } else {
                set_flash_message("error", 'The ticket was updated but the email was not sent.');
            }
        }
    }

    function formatEmailHistory($emailHistoryData)
    {

        $formatedData = "<table>
                            <tr><td colspan=2><HR></td></tr>
                            <tr><td colspan=2>Ticket History</td></tr>
                            <tr><td colspan=2><HR></td></tr>
                        ";

        foreach ($emailHistoryData as $emailRecord) {
            $emailType   = $emailRecord->noteType;
            $dateCreated = $emailRecord->dateCreated;
            $note        = $emailRecord->note;



            if ($emailType != "Internal") {

                $formatedDate = convert_from_gmt_aprax($dateCreated,
                    "Y-m-d H:i:s");

                if ($emailType == "Customer") {
                    $displayName = "Customer Representative";
                } else {
                    $displayName = "Customer";
                }

                $formatedData .= "<tr><td>Sender</td><td>$displayName</td></tr>
                                  <tr><td>Date</td><td>$formatedDate</td></tr>
                                  <tr><td colspan=2><pre style=\"font-size: 14px; !important; font-family: Arial, Helvetica, sans-serif !important;\">$note</pre></td></tr>
                                  <tr><td colspan=2><HR></td></tr>";
            }
        }

        $formatedData .= "</table>";
        return $formatedData;
    }

    function settings()
    {
        $this->load->model('ticketlabel_model');
        $this->load->model('ticketstatus_model');
        $this->load->model('tickettype_model');
        $this->load->model('aclroles_model');

        $content = array();
        $content['labels'] = $this->ticketlabel_model->get_aprax(array('business_id' => $this->business_id));
        $content['statuses'] = $this->ticketstatus_model->getWithStatusTypes($this->business_id);
        $content['types'] = $this->tickettype_model->get_aprax(array('business_id' => $this->business_id));
        $content['roles'] = $this->aclroles_model->get_all_as_codeigniter_dropdown($this->business_id);

        
        $this->renderAdmin('settings', $content);
    }

    function add_label()
    {
        $this->renderAdmin('label_form', array('label' => new stdClass()));
    }
    
    function edit_label($ticketLabelID)
    {
        $this->load->model('ticketlabel_model');
        $label = $this->ticketlabel_model->get_by_primary_key($ticketLabelID);
        $this->renderAdmin('label_form', compact('label'));
    }

    function save_label()
    {
        
        if ($this->input->post("ticketLabelID")) {
            $label = new TicketLabel($this->input->post("ticketLabelID"));
            $message = "Updated Label";
            $error_redirect_url = '/admin/tickets/edit_label/'.$this->input->post('ticketLabelId');
        } else {
            $label = new TicketLabel();
            $message = "Added Label";
            $error_redirect_url = '/admin/tickets/add_label';
        }

        if (!$this->input->post('description')) {
            set_flash_message('error', "Missing required field 'description'");
            redirect_with_fallback($error_redirect_url);
        }


        $label->description = $this->input->post('description');
        $label->color = $this->input->post("color");
        $label->business_id = $this->business_id;

        $label->save();
        set_flash_message("success", $message);

        redirect("/admin/tickets/settings");
    }

    function add_status()
    {
        $this->load->model('ticketstatustype_model');
        $status_types = $this->ticketstatustype_model->get_all_as_codeigniter_dropdown();
        $this->renderAdmin('status_form', array('status' => new stdClass(), 'status_types' => $status_types));
    }

    function edit_status($ticketStatusID)
    {
        $this->load->model('ticketstatus_model');
        $this->load->model('ticketstatustype_model');

        $status = $this->ticketstatus_model->get_by_primary_key($ticketStatusID);
        $status_types = $this->ticketstatustype_model->get_all_as_codeigniter_dropdown();
        $this->renderAdmin('status_form', compact('status', 'status_types'));
    }

    function save_status()
    {

        if ($this->input->post("ticketStatusID")) {
            $status = new TicketStatus($this->input->post("ticketStatusID"));
            $message = "Updated Status";
            $error_redirect_url = '/admin/tickets/edit_status/'.$this->input->post('ticketStatusId');
        } else {
            $status = new TicketStatus();
            $message = "Added Status";
            $error_redirect_url = '/admin/tickets/add_status';
        }

        if (!$this->input->post('description')) {
            set_flash_message('error', "Missing required field 'Description'");
            redirect_with_fallback($error_redirect_url);
        }

        if (!$this->input->post('responseTime')) {
            set_flash_message('error', "Missing required field 'Repsonse Time'");
            redirect_with_fallback($error_redirect_url);
        }

        $status->description = $this->input->post('description');
        $status->responseTime = $this->input->post('responseTime');
        $status->ticketStatusType_id = $this->input->post('ticketStatusType_id');
        $status->business_id = $this->business_id;

        $status->save();
        set_flash_message("success", $message);

        redirect("/admin/tickets/settings");
    }

    function add_ticket_type($ticketTypeID = null)
    {
        if ($this->input->post()) {
            $this->save_ticketType($ticketTypeID);
        }

        $this->load->model('tickettype_model');
        $this->load->model('aclroles_model');
        $roles = $this->aclroles_model->get_all_as_codeigniter_dropdown($this->business_id);
        $ticketType = new stdClass();
        $checkedRoles = array();
        
        if ($ticketTypeID) {
            if ($this->input->post()) {
                $ticketType->description = $this->input->post('description');
                $ticketType->roles = $this->input->post('roles') ? implode(',', $this->input->post('roles')):'';
            } else {
                $ticketType = $this->tickettype_model->get_by_primary_key($ticketTypeID);
            }

            if ($ticketType->roles) {              
                $ticketTypeRoles = explode(",", $ticketType->roles);
                foreach ($ticketTypeRoles as $r) {
                    $checkedRoles[] = $r;
                }
            }
        }
        $this->renderAdmin('ticketType_form', compact('status', 'status_types', 'ticketTypeID', 'roles', 'checkedRoles', 'ticketType', 'ticketTypeID'));
    }

    function save_ticketType($ticketType_id = null)
    {
        if ($ticketType_id) {
            $ticketType = new TicketType($this->input->post($ticketType_id));
            $ticketType->ticketTypeID = $ticketType_id;
            $message = "Updated Ticket Type";
            $error_redirect_url = '/admin/tickets/add_ticket_type/'.$ticketType_id;
            $success_redirect_url = $error_redirect_url;
        } else {
            $ticketType = new TicketType();
            $message = "Added Ticket Type";
            $error_redirect_url = '/admin/tickets/add_ticket_type';
            $success_redirect_url = '/admin/tickets/settings';
        }
     
        $this->load->library("form_validation");
        $this->form_validation->set_rules("description", "Description", "required|string");
        $this->form_validation->set_rules("roles", "Roles", "required");

        // validate post fields
        if ($this->form_validation->run()) {           
            $ticketType->description = $this->input->post('description');
            $ticketType->roles = implode(',', $this->input->post('roles'));
            $ticketType->business_id = $this->business_id;

            if ($ticketType->save()) {
                set_flash_message("success", $message);
                redirect($success_redirect_url);
            } 

            set_flash_message('error', 'Unexpected error when saving ticket type, please try again later.');
            redirect($error_redirect_url);          
        } 
       
        set_flash_message_now("error", validation_errors());        
    }

    public function type_filter()
    {
        $this->load->model('tickettype_model');
        $this->load->model('employee_model');

        //die(var_dump($this->input->post()));
        //role filter
        if ($this->input->post('type_filter')) {
            $ticketType = $this->tickettype_model->get_by_primary_key($this->input->post('type_filter'));
            $roles = explode(',', $ticketType->roles);
            $employees = $this->employee_model->get_emp_by_role($roles);
            
            foreach ($employees as $key => $employee) {
                $employees[$key]->completeName = getPersonName($employee);
            }
            
            echo json_encode($employees);
        }
        die();
    }
}