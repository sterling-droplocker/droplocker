<?php
use App\Libraries\DroplockerObjects\TaxTable;
use App\Libraries\DroplockerObjects\TaxGroup;

class TaxTables extends MY_Admin_Controller
{
    public $submenu = 'admin';

    public function index()
    {
        $this->load->model("taxgroup_model");
        $this->load->model("taxtable_model");

        $content['taxGroups'] = $this->taxgroup_model->get_aprax(array(
            "business_id" => $this->business_id
        ));

        foreach ($content['taxGroups'] as $taxGroup) {
            $table = $this->taxtable_model->get_aprax(array(
                "business_id" => $this->business_id,
                "taxGroup_id" => $taxGroup->taxGroupID,
            ), 'zipcode');

            $content['taxTables'][$taxGroup->taxGroupID] = $table;
            $content['taxGroupsParents'][$taxGroup->taxGroupID] = array();
            foreach ($content['taxGroups'] as $parentTaxGroup) {
                if ($parentTaxGroup->taxGroupID != $taxGroup->taxGroupID) {
                    $content['taxGroupsParents'][$taxGroup->taxGroupID][$parentTaxGroup->taxGroupID] = $parentTaxGroup->name;
                }
            }
        }

        $content['zipcode'] = isset($_GET['zipcode']) ? $_GET['zipcode'] : '';

        $this->renderAdmin('manage_taxTables', $content);
    }

    public function save_default()
    {
        $default_taxGroup = $this->input->post('default_taxGroup');
        update_business_meta($this->business_id, 'default_taxGroup', $default_taxGroup);
        set_flash_message('success', 'Default Tax Group updated');
        redirect('/admin/taxTables/');
    }

    /**
     * Note, this action expects a request from Jquery.Jeditable
     */
    public function update()
    {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("id", "ID", "required|is_natural_no_zero");
        $this->form_validation->set_rules("property", "Property", "required");
        $this->form_validation->set_rules("value", "Value", "required");

        if (!$this->form_validation->run()) {
            echo validation_errors();
            return;
        }

        $id = $this->input->post("id");
        try {
            $taxTable = new TaxTable($this->input->post("id"));
            if ($taxTable->business_id != $this->business->businessID) {
                echo "Tax Table '$id' does not belong to your business";
                return;
            }

            $property = $this->input->post("property");
            $value = $this->input->post("value");
            $taxTable->$property = $value;
            $taxTable->save();
            if ($property == "taxRate") {
                echo number_format($value, 5, '.', '');
            } else {
                echo $value;
            }

        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
            echo "Tax Table '$id' not found.";
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
            echo $validation_exception->getMessage();
        }
    }

    public function create()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("zipcode", "Zipcode", "required");
        $this->form_validation->set_rules("taxRate", "Tax Rate", "numeric|required");
        $this->form_validation->set_rules("taxGroup_id", "Tax Group", "numeric|required");

        if ($this->form_validation->run()) {
            $taxTable = new TaxTable();
            $taxTable->zipcode     = $this->input->post("zipcode");
            $taxTable->taxRate     = $this->input->post("taxRate");
            $taxTable->taxGroup_id = $this->input->post("taxGroup_id");
            $taxTable->business_id = $this->business_id;
            try {
                $taxTable->save();
                set_flash_message("success", "Created new Tax Rate");
            } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                set_flash_message("error", $validation_exception->getMessage());
            }

        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/taxTables");
    }

    public function delete($id = null)
    {
        if (empty($_POST)) {
            redirect("/admin/taxTables");
        }

        try {
            $taxTable = new TaxTable($id);

            if ($taxTable->business_id == $this->business_id) {
                $taxTable->delete();
                set_flash_message('success', "Deleted tax rate for state '{$taxTable->zipcode}'");
            } else {
                set_flash_message('error', "Tax rate '$id' does not belong to your business");
            }
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
            set_flash_message("error", "Tax rate '$id' not found");
        }

        $this->goBack("/admin/taxTables");
    }

    public function add_group()
    {
        if (empty($_POST['name'])) {
            set_flash_message('error', "Name must not be empty");
            redirect("/admin/taxTables");
            return;
        }

        $taxGroup = new TaxGroup($group_id);
        $taxGroup->name = $_POST['name'];
        $taxGroup->business_id = $this->business_id;
        $taxGroup->save();

        // if the fist one, save as default
        $taxGroups = TaxGroup::search_aprax(array('business_id' => $this->business_id));
        if (count($taxGroups) == 1) {
            update_business_meta($this->business_id, 'default_taxGroup', $taxGroup->taxGroupID);
        }

        $taxRate = $this->input->post('taxRate');
        if (strlen($taxRate)) {
            $locations = $this->db->query("SELECT DISTINCT zipcode FROM location
                WHERE zipcode != '' AND business_id = ?",
                array($this->business_id))->result();

            foreach ($locations as $location) {
                $taxTable = new TaxTable();
                $taxTable->zipcode     = $location->zipcode;
                $taxTable->taxRate     = $taxRate;
                $taxTable->taxGroup_id = $taxGroup->taxGroupID;
                $taxTable->business_id = $this->business_id;
                $taxTable->save();
            }
        }

        set_flash_message('success', "Tax group has been created");

        redirect("/admin/taxTables");
    }

    public function update_group($group_id = null)
    {
        if (empty($_POST['name'])) {
            set_flash_message('error', "Name must not be empty");
            redirect("/admin/taxTables");
            return;
        }

        $taxGroup = new TaxGroup($group_id);
        $taxGroup->name = $_POST['name'];
        $taxGroup->parent_id = $_POST['parent_id'];
        $taxGroup->is_sub_tax = $_POST['parent_id'] ? $_POST['is_sub_tax'] : 0;
        $taxGroup->save();
        set_flash_message('success', "Tax group has been updated");

        redirect("/admin/taxTables");
    }

    public function delete_group($group_id = null)
    {
        if (empty($_POST)) {
            redirect("/admin/taxTables");
        }

        $taxGroup = new TaxGroup($group_id);
        $taxGroup->delete();

        $this->load->model('taxtable_model');
        $this->taxtable_model->delete_aprax(array(
            "business_id" => $this->business_id,
            "taxGroup_id" => $taxGroup->taxGroupID,
        ));

        set_flash_message('success', "Tax group has been deleted");
        redirect("/admin/taxTables");
    }
}
