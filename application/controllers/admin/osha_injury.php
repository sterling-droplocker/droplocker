<?php

class Osha_Injury extends MY_Admin_Controller
{

    public $submenu = 'admin';

    public function index()
    {
        $sqlGetLog = "select * from oshaInjury
                                join employee on employeeID = employeeInjured_id
                                where business_id = ?
                                order by caseNo desc";
        $query = $this->db->query($sqlGetLog, array($this->business_id));
        $content['injuryLog'] = $query->result();

        $content = $this->getData($content);

        $this->renderAdmin('index', $content);
    }

    public function add()
    {
        if (isset($_POST['add'])) {
            $this->load->model('oshaInjury_model');

            $this->oshaInjury_model->save(array(
                'caseNo' => $_POST['caseNo'],
                'employeeInjured_id' => $_POST['employeeInjured_id'],
                'title' => $_POST['title'],
                'injuryDate' => date("Y-m-d", strtotime($_POST['injuryDate'])),
                'location' => $_POST['location'],
                'description' => $_POST['description'],
                'classification' => $_POST['classification'],
                'daysAway' => $_POST['daysAway'],
                'daysRestricted' => $_POST['daysRestricted'],
                'classification' => $_POST['classification'],
                'injury' => $_POST['injury'],
                'business_id' => $this->business_id,
            ));

            set_flash_message('success', "OSHA Injury Report Added");
        }

        redirect("/admin/osha_injury");
    }

    public function edit($oshaInjuryID = null)
    {
        if (!empty($_POST)) {
            $this->load->model('oshaInjury_model');
            $where['oshaInjuryID'] = $_POST['oshaInjuryID'];
            foreach ($_POST as $s => $value) {
                if ($s == 'injuryDate') {
                    $option[$s] = date("Y-m-d", strtotime($_POST['injuryDate']));
                } else {
                    $option[$s] = $value;
                }
            }

            $this->db->update('oshaInjury', $option, $where);
            set_flash_message('success', 'OSHA Injury Report Updated');
            redirect("/admin/osha_injury/edit/$oshaInjuryID");
        }

        $sqlGetLog = "select * from oshaInjury
                    join employee on employeeID = employeeInjured_id
                    where oshaInjuryID = ?" ;
        $query = $this->db->query($sqlGetLog, array($oshaInjuryID));
        $content['injuryLog'] = $query->result();

        $content = $this->getData($content);

        $this->renderAdmin('edit', $content);
    }

    protected function getData($content)
    {
        $this->load->model('employee_model');
        $content['employees'] = $this->employee_model->get_all_as_codeigniter_dropdown($this->business_id);

        $content['classifications'] = array(
        	'Death' => 'Death',
            'Days away from work' => 'Days away from work',
            'Job Transfer' => 'Job Transfer',
            'Other' => 'Other',
        );

        $content['injuries'] = array(
        	'Injury' => 'Injury',
            'Skin Disorder' => 'Skin Disorder',
            'Respiratory Condition' => 'Respiratory Condition',
            'Poisoning' => 'Poisoning',
            'Hearing Loss' => 'Hearing Loss',
            'All Others' => 'All Others',
        );

        return $content;
    }

}