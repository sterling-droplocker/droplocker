<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\DroidLoadVan;
use App\Libraries\DroplockerObjects\Droid_PostDeliveries;
use App\Libraries\DroplockerObjects\Droid_PostPickup;
use DropLocker\Util\Lock;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Locker;

class Mobile_2_5 extends CI_Controller
{
    private $get;
    public $business_id;
    public $employee_id;
    private $companyName;
    private $deliveryTime;

    const VERSION = '2.5';

    function __construct(){
        header("Access-Control-Allow-Origin: *");
        //$this->output->set_header("Access-Control-Expose-Headers: Access-Control-Allow-Origin");
        parent::__construct();

        $this->load->model('driver_log_model');
        $this->load->model('order_model');
        $this->load->model('customerhistory_model');
        $this->load->model('customer_model');

        $this->get = $this->input->get();

        $this->username             = '';
        $this->password             = '';
        $this->business_id          = '';

        if ( isset($this->get['BUSINESSID']) ) {
            $this->business_id = $this->get['BUSINESSID'];
        }

        if ( isset($this->get['DELMANID']) ) {
            $this->username = $this->get['DELMANID'];
        }

        if ( isset( $this->get['PASSWORD'])) {
            $this->password = $this->get['PASSWORD'];
        }

        if (isset($this->get['EMPLOYEEID'])) {
            $this->employee_id          = $this->get['EMPLOYEEID'];
        }

        if (isset($this->get['CLAIMID'])) {
            $this->claim_id             = $this->get['CLAIMID'];
        }

        if (isset($this->get['ROUTES'])) {
           $routes = urldecode($this->get['ROUTES']);
           $routes = trim($routes);
           $this->routes = rtrim($routes, ",");
        }

        if (isset($this->get['BAGNUMBER'])) {
        $this->fullBagNumber        = $this->get['BAGNUMBER'];
        $this->bagNumber            = $this->get['BAGNUMBER'];
        }
        if (isset($this->get['LOCATIONID'])) {
            $this->location_id          = ltrim($this->get['LOCATIONID'], "0"); // trim off the leading 0 if found
        }
        if (isset($this->get['LOCKERNAME'])) {
            $this->lockerName           = ltrim(trim(urldecode($this->get['LOCKERNAME'])), "0"); // Trim if there is a leading 0 on the locker name
        }
        if (isset($this->get['NOTES'])) {
            $this->notes                =  urldecode($this->get['NOTES']);
        }
        if (isset($this->get['ORDERID'])) {
            $this->order_id             = $this->get['ORDERID'];
        }
        if (isset($this->get['WASHFOLD'])) {
            $this->washFold             = $this->get['WASHFOLD'];
        }
        if (isset($this->get['PARTIAL'])) {
            $this->partial              = $this->get['PARTIAL'];
        }
        if (isset($this->get['INOUT'])) {
            $this->inout                = $this->get['INOUT'];
        }
        if (isset($this->get['CLOCKTIME'])) {
            $this->clockTime            = urldecode($this->get['CLOCKTIME']);
        }
        if (isset($this->get['NOTEID'])) {
            $this->note_id              = $this->get['NOTEID'];
        }
        if (isset($this->get['JOB'])) {
            $this->job                  = $this->get['JOB'];
        }
        if (isset($this->get['TIMECARD'])) {
            $this->timecard_id          = $this->get['TIMECARDID'];
        }
        if (isset($this->get['ISSUEID'])) {
            $this->issue_id             = $this->get['ISSUEID'];
        }

        if (isset($this->get['ERROR'])) {
            $this->errorMessage = $this->get['ERROR'];
        }

        if (isset($this->get['NOTETYPE'])) {
            $this->noteType             = $this->get['NOTETYPE'];
        }
        if (isset($this->get['TRANSID'])) {
            $this->transaction_id       = $this->get['TRANSID'];
        }
        if (isset($this->get['NOTE'])) {
            $this->driverNote           = urldecode($this->get['NOTE']);
        }
        if (isset($this->get['CUSTOMER'])) {
            $this->customerName         = urldecode($this->get['CUSTOMER']);
        }
        if (isset($this->get['PHONEVERSION'])) {
            $this->phoneVersion         = urldecode($this->get['PHONEVERSION']);
        }
        if (isset($this->get['PHONENUMBER'])) {
            $this->phoneNumber         = urldecode($this->get['PHONENUMBER']);
        }

        $this->location_id = 0;
        if (isset($this->get['LOCATIONID'])) {
            if (strlen($this->get['LOCATIONID']>1)) {
                $this->location_id = ltrim($this->get['LOCATIONID'], "0"); // trim off the leading 0 if found
            } else {
                $this->location_id = $this->get['LOCATIONID'];
            }
        }

        $this->lockerName = '';
        if (isset($this->get['LOCKERNAME'])) {
            if (strlen($this->get['LOCKERNAME']>1)) {
                $this->lockerName = ltrim($this->get['LOCKERNAME'], "0"); // trim off the leading 0 if found
            } else {
                $this->lockerName = $this->get['LOCKERNAME'];
            }
        }

        //if (!empty($this->business_id)) {
        //    $this->deliveryTime         = $this->formatDate(urldecode($this->get['DELTIME']), $this->business_id);
        //} else {
            if ( isset( $this->get['DELTIME'] ) ) {
                $this->deliveryTime = urldecode( $this->get['DELTIME'] );
            }
        //}

        if (!$this->input->is_cli_request()) {
           if (!$this->auth()) {
               //echo json_encode(array('status'=>'FAIL', 'message'=>"Username or password is incorrect"));
               //die();
           }
        }



    }

    public function validate()
    {
        if ($this->auth()) {
            $response['business_id'] = $this->business_id;
            $response['employee_id'] = $this->employee_id;
            $response['companyName'] = $this->companyName;
            echo json_encode(array('status'=>"SUCCESS", 'response'=>$response));
        } else {
            echo json_encode(array('status'=>"FAIL", 'message'=>"Username or password is incorrect"));
        }
    }

    public function getBusineses()
    {
        $this->load->model("business_model");
        output_ajax_success_response($this->business_model->findAllCompleteBusinesses());
    }
    public function getTranslations()
    {
        $this->validateRequest(array(
            "business_id" => array('notEmpty', 'numeric', 'entityMustExist'),
            'tag' => array("notEmpty")

        ));
        $business = $this->business_model->get_by_primary_key($this->input->get("business_id"));
        $tag = $this->input->get("tag");
        $this->load->model("language_model");
        $language = $this->language_model->findByTag($tag);
        if (empty($language)) {
            output_ajax_error_response("Language tag '$tag' does not exist");
        }
        $this->load->model("languagekey_business_language_model");
        $this->languagekey_business_language_model->getTranslationsForLanguageViewNameForLanguageNameForBusiness($language->languageID, $business->businessID, array(
            "dropDroid/",
            "dropDroid/",
            "dropDroid/",
            "dropDroid/",
            "dropDroid/"
        ));
    }
    /**
     * checks to see if the phone has the most up to date version.
     * If the phone is out of date, an alert will be shown and
     * a link provided to get the newest apk
     *
     * the version is a config setting
     */
    public function checkVersion()
    {
        //$query = $this->db->get('droidVersion');
        //$result = $query->row();
        echo json_encode(array('version'=>$this->config->item('version'),'link'=>$this->config->item('link') ));
    }

    /**
    * saveSettings method is used to validate a driver
    *
    * @param string $username
    * @param string $password (md5 string will be passed)
    *
    * echo JSON response for result
    */
    public function saveSettings()
    {
        //authorize the driver
        if ($this->auth()) {
           $response['business_id'] = $this->business_id;
           $response['employee_id'] = $this->employee_id;
           $response['companyName'] = $this->companyName;
           echo json_encode(array('status'=>"SUCCESS", 'response'=>$response));
        } else {
           echo json_encode(array('status'=>"FAIL", 'message'=>"Username or password is incorrect"));
        }
    }

    /**
     * Auth is used to verify an employee logging into the application
     * from a phone
     *
     * Set user values on the phone
     * -----------
     * business_id
     * employee_id
     * companyName
     *
     * @return boolean - sets the employee_id and business_id
     */
    public function auth()
    {
        $sql = "SELECT employeeID, business_id, companyName
                    FROM employee
                    INNER JOIN business_employee be ON be.employee_id = employeeID
                    INNER JOIN business b ON b.businessID = be.business_id
                    AND `default` = 1
                    WHERE username = ?
                    AND password = ?
                    AND empStatus = ?";

        $query = $this->db->query($sql, array($this->username, $this->password, 1));
        if ($result = $query->row()) {
            $this->employee_id = $result->employeeID;
            $this->business_id = $result->business_id;
            $this->companyName = $result->companyName;

            return true;
        }

        return false;
    }

    /**
     * inserts data sent from the phones into the droid_postPickups table
     */
    public function postPickups()
    {
        $this->load->model('droidpostpickups_model');
        $options['transaction_id'] = $this->transaction_id;
        $options['bagNumber'] = $this->bagNumber;
        $options['location_id'] = $this->location_id;
        $options['lockerName'] = $this->lockerName;
        $options['employeeUsername'] = $this->username;
        $options['deliveryTime'] = $this->formatDate($this->deliveryTime, $this->business_id);
        $options['washFold'] = $this->washFold;
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;
        $options['notes'] = $this->notes;
        $options['claim_id'] = $this->claim_id;
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        try {
            if ($insert_id = $this->droidpostpickups_model->insert($options)) {
                echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$options['transaction_id'], 'insert_id'=>$insert_id));
            } else {
                echo json_encode(array('status'=>'FAIL', 'transaction_id'=>$options['transaction_id'], 'message'=>'Failed to insert postPickup'));
            }
        } catch (Database_Exception $database_exception) {
            if ($database_exception->get_error_number() == 1062) {
                echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$options['transaction_id'], 'message'=>'post pickup transaction already exists'));
            }
        }
    }


    /**
     * inserts data sent from the phones into the database
     */
    public function postPickupReminders()
    {
        $business = new Business($this->business_id);

       //look for unpaid orders
       $sql = "SELECT orderID, bag.customer_id, bag_id, orderPaymentStatusOption_id FROM orders
               INNER JOIN bag ON bag.bagID = orders.bag_id
               WHERE bag.bagNumber = {$this->bagNumber}  ORDER BY orders.orderID DESC limit 1";
        $lastOrder = $this->db->query($sql)->row();
        $orderIsUnpaid = false;

        if ( is_object($lastOrder) ) {
            if( $lastOrder->orderPaymentStatusOption_id == '1' ||$lastOrder->orderPaymentStatusOption_id == '4') {
                $orderIsUnpaid = true;
            }

            $customer = new Customer($lastOrder->customer_id);
            if (!empty($customer->invoice)) {
                if ($customer->invoice == 1) {
                    $orderIsUnpaid = false;
                }
            }
        }

        if($orderIsUnpaid) {
                $emailActionID = 7;
                $business_language_id = $business->default_business_language_id;
                $this->load->model("emailactions_model");
                $this->load->model("emailtemplate_model");
                $emailAction = $this->emailactions_model->get_by_primary_key($emailActionID);
                $emailTemplate = $this->emailtemplate_model->get_one(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id, "business_language_id" => $business_language_id));
                if (empty($emailTemplate)) {
                    if (empty($emailAction)) {
                        set_flash_message("error", "Could not find email action ID $emailActionID");
                    } else {
                        set_flash_message("error", "Could not find email template for email action '{$emailAction->name}'");
                    }
                } else {

                    $action_array = array(
                        'do_action' => 'payment_hold',
                        'customer_id' => $lastOrder->customer_id,
                        'business_id' => $this->business_id,
                        'order_id' => $lastOrder->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);

                }
        } else {
            $sql = "INSERT IGNORE INTO droid_postPickupReminders (transaction_id, bagNumber, location_id, lockerName, employeeUsername, deliveryTime, business_id, employee_id, version, phoneVersion, phoneNumber)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $values = array($this->transaction_id, $this->bagNumber,$this->location_id,$this->lockerName,$this->username,$this->deliveryTime,$this->business_id,$this->employee_id,self::VERSION,$this->phoneVersion,$this->phoneNumber);
            $this->db->query($sql,$values);
            $id = $this->db->insert_id();

            if (!$id) {
                echo json_encode(array('status'=>'FAIL', 'transaction_id'=>$this->transaction_id, 'message'=>'duplicate post pickup reminder error for bag ['.$this->bagNumber.']'));
                die();
            }
        }
        echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$this->transaction_id));
    }


    /**
     * errors from the phones are stored on the server and then emailed
     * to development
     */
    public function postErrors()
    {
        $this->load->model('droidError_model');

        $this->droidError_model->save(array(
            'transaction_id' => $this->transaction_id,
            'username'       => $this->username,
            'business_id'    => $this->business_id,
            'errorMessage'   => urldecode($this->errorMessage),
            'errorDate'      => gmdate('Y-m-d H:i:s'),
            'employee_id'    => $this->employee_id,
            'version'        => self::VERSION,
        ), true);

        echo json_encode(array(
            'transaction_id' => $this->transaction_id,
            'status'         => 'SUCCESS',
        ));
    }

    /**
     * gets all the lockers that are in locations on the driver route
     *
     * Used for validation when confirming a delivery of an order
     */
    public function getLockersInRouteLocations()
    {
        $sql = "SELECT lockerName, lockerID, location_id FROM locker
                INNER JOIN location ON locationID = location_id
                WHERE ( lockerStatus_id = 1 ) AND route_id in ({$this->routes}) ";
        $lockers = $this->db->query($sql)->result();

        echo json_encode($lockers);
    }


    /**
     * inserts data sent from the phones into the database
     */
    public function postReturnToOffice()
    {
        $sql = "INSERT IGNORE INTO droid_postReturnToOffice (transaction_id, bagNumber, location_id, lockerName, employeeUsername, deliveryTime, business_id, employee_id, version, phoneVersion, phoneNumber)
        VALUES ('".$this->transaction_id."', '".$this->bagNumber."','".$this->location_id."','".$this->lockerName."','".$this->username."','".$this->deliveryTime."', '".$this->business_id."','".$this->employee_id."', '".self::VERSION."', '".$this->phoneVersion."', '".$this->phoneNumber."')";
        $this->db->query($sql);


        $id = $this->db->insert_id();

        if ($id) {
            echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$this->transaction_id));

        } else {
            echo json_encode(array('status'=>'FAIL', 'transaction_id'=>$this->transaction_id, 'message'=>'Failed to insert postPickup'));
        }
    }


    /**
     * method for handling scan bag post pickups
     */
    public function postScanBag()
    {
        try {

            if (empty($this->bagNumber)) {
                throw new Exception("Missing bag number");
            }

            if (empty($this->business_id)) {
                throw new Exception("Missing business_id");
            }

            $sql = "select max(orderID) as orderID from orders
            join bag on bagID= orders.bag_id
            where bagNumber = '$this->bagNumber'
            AND orders.business_id = {$this->business_id}";

            $query = $this->db->query($sql);
            $order = $query->result();

            if (empty($order[0]->orderID)) {

                echo json_encode(array('status'=>'FAIL', 'transaction_id'=>$this->transaction_id, 'message'=>'Order not found'));
                return;
            }

            echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$this->transaction_id));

            //Log that this bag was scanned in the automat table
            $insert="insert into automat
            (order_id, garcode, gardesc, fileDate)
            values ({$order[0]->orderID}, $this->bagNumber, 'Scanned to unload van by handheld', now())";
            $query = $this->db->query($insert);

            $deliveryTime = $this->deliveryTime;
            $username = $this->username;
            $employee_id = $this->employee_id;

            $insert2 = "insert into orderStatus
            (order_id, orderStatusOption_id, date, customer_userName, employee_id)
            values ('$order_id', '15', '$deliveryTime', '$username', '$employee_id');";
            $query2 = $this->db->query($insert2);
        } catch (Exception $e) {
            send_exception_report($e);
        }
    }

    /**
     * fires the cron job and process all orders the route passed
     *
     * requires
     * ------------
     * $this->routes
     * $this->business_id
     *
     * echos json object
     * -------------
     * status (SUCCESS|FAIL)
     * message
     */
    public function processOrders()
    {
        if (!Lock::acquire(__METHOD__)) {
            $this->load->library("log");
            $this->log->write_log('error', "failed to acquire lock, EMPLOYEEID[$this->employee_id]:BUSINESS_ID[$this->business_id] {$_SERVER['REMOTE_ADDR']}");
            echo json_encode(array('status'=>'ERROR', 'message'=> "ERROR: failed to acquire lock"));
            return;
        }

        $result = "STARTED CRON:EMPLOYEEID[$this->employee_id]:BUSINESS_ID[$this->business_id]:";
        $this->load->model('droidpostdeliveries_model');
        $deliveries = $this->droidpostdeliveries_model->get(array('status'=>'pending', 'process_status' => 'not_processed', 'employee_id' => $this->employee_id, 'business_id'=>$this->business_id, 'version'=>'2.5'));

        if (!$deliveries) {
            $result .= "NO ORDERS TO PROCESS";
        }


        foreach ($deliveries as $delivery) {
            $droid_postDelivery = new Droid_PostDeliveries($delivery->ID);
            if ($droid_postDelivery->process_status == "not_processed") { //Note, we verify that the devliery has still not been processed after retrieving the initial result set.
                $droid_postDelivery->process_status = "processing";
                $droid_postDelivery->save();
                $this->droidpostdeliveries_model->processDelivery($delivery);
                $result .= $delivery->order_id.",";
                $droid_postDelivery->process_status = "processed";
                $droid_postDelivery->save();
            } else {
                //send_admin_notification("Duplicate process delivery call from phone ({$droid_postDelivery->{Droid_PostDeliveries::$primary_key}})", print_r($droid_postDelivery,1));
            }
        }
        $message = $result;
        echo json_encode(array('status'=>'SUCCESS', 'message'=>$message));

        Lock::release(__METHOD__);
    }


    public function processPickups()
    {
        $result = "STARTED CRON:EMPLOYEEID[$this->employee_id]:BUSINESS_ID[$this->business_id]:";
        $this->load->model('droidpostpickups_model');

        $key = 'pickups_' . $this->employee_id  . '_' . date('YmdHi');
        if (Lock::acquire($key, true)) {
            $pickups = $this->droidpostpickups_model->get(array('status'=>'pending', 'employee_id'=>$this->employee_id, 'business_id'=>$this->business_id));

            if (!$pickups) {
                $result .= "NOPICKUPS TO PROCESS";
            }

            foreach ($pickups as $pickup) {
                $droid_postPickup = new Droid_PostPickup($pickup->ID);
                //The following conditional verifies that another process from the faulty Android App is not already processing or has already processed this pickup.
                if ($droid_postPickup->process_status == "not_processed") {
                    $droid_postPickup->process_status = "processing";
                    $droid_postPickup->save();
                    $this->droidpostpickups_model->processPickup($pickup);
                    $result .= $pickup->bagNumber.",";
                    $droid_postPickup->process_status = "processed";
                    $droid_postPickup->save();
                }

            }

        }

        $message = $result;

        echo json_encode(array('status'=>'SUCCESS', 'message'=>$message));
    }

    /**
     *  inserts data sent from the phones into the database
     */
    public function postDeliveries()
    {
        $this->load->model('droidpostdeliveries_model');
        $options['transaction_id'] = $this->transaction_id;
        $existing_postDelivery = $this->droidpostdeliveries_model->get($options);

        /**
         * The following conditional checks to see if there is an existing post delivery with the same transaction ID.
         * If such a post delivery exists, then we return a success response with a message saying that that post delivery already exists.
         * Otherwise, we attempt to create the new post delivery.
         */
        if ($existing_postDelivery) {
            echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$options['transaction_id'], 'message'=>'transaction_id already inserted in database'));
        } else {
            $options = array();
            $options['transaction_id'] = $this->transaction_id;
            $options['bagNumber'] = $this->bagNumber;
            $options['order_id'] = $this->order_id;
            $options['lockerName'] = $this->lockerName;
            $options['employeeUsername'] = $this->username;
            $options['deliveryTime'] = $this->formatDate($this->deliveryTime, $this->business_id);
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;
            $options['version'] = self::VERSION;
            $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
            $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
            try {
                if ($this->droidpostdeliveries_model->insert($options)) {
                    echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$options['transaction_id'], 'message'=>'inserted into postDeliveries'));
                } else {
                    echo json_encode(array('status'=>'FAIL', 'transaction_id'=>$options['transaction_id'], 'message'=>'Failed to insert postDeliveries'));
                }
            } catch (Database_Exception $database_exception) {
                if ($database_exception->get_error_number() == 1062) {
                    echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$options['transaction_id'], 'message'=>'post delivery transaction already exists'));
                }
            }
        }

    }

    /**
     * driver has sent a note
     *
     * TODO: add test
     */
    public function postDriverNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $foundNotes = $this->droidpostdrivernotes_model->get(array('transaction_id'=>$this->transaction_id));
        if (!$foundNotes) {
            $options['noteType'] = 'driverNote';
            $options['employeeUsername'] = $this->username;
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;
            $options['transaction_id'] = $this->transaction_id;
            $options['deliveryTime'] = $this->deliveryTime;
            $options['notes'] = $this->driverNote;
            $options['note_id'] = $this->note_id;
            $options['order_id'] = $this->order_id;
            $options['location_id'] = $this->location_id;
            $options['version'] = self::VERSION;
            $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
            $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
            $this->droidpostdrivernotes_model->insert($options);
        }

        echo json_encode(array('status'=>"SUCCESS", "transaction_id"=>$this->transaction_id));


    }

    /**
     * getClaimed method gets all the claimed orders
     */
    public function getClaimed()
    {
        $sql = "SELECT claimID as claim_id,
                    locationID as location_id,
                    concat(firstName, ' ', lastName) as customer_name,
                    lockerName,
                    address as street_address,
                    serviceType,
                    customer.address1 as customer_address1,
                    customer.address2 as customer_address2,
                    phone as customer_phone_number,
                    UNIX_TIMESTAMP(claim.updated) as updated,
                    sortOrder as route_sort_order,
                    locationType.name as service_method
                    FROM claim
                    JOIN customer on customerID = claim.customer_id
                    JOIN locker on lockerID = claim.locker_id
                    JOIN location on locationID = locker.location_id
                    JOIN locationType ON locationType_id = locationTypeID
                    where route_id in ($this->routes)
                    AND claim.active = 1
                    AND claim.business_id = {$this->business_id}
                    order by sortOrder;";

        $query = $this->db->query($sql);
        if ($this->db->_error_message()) {
            throw new Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $claims = $query->result();

        for ($i=0; $i<sizeof($claims); $i++) {
            foreach ($claims[$i] as $k=>$v) {
                if ($k=='street_address') {
                    $arr = explode(" ", $v);
                    $claims[$i]->street_number = $arr[0];
                }

            }
        }

        echo json_encode($claims);
    }

    /**
     * gets locations
     */
    public function getLocations()
    {
        $sql = "SELECT
                    locationID as location_id,
                    address,
                    accessCode as access_code,
                    lat as latitude,
                    lon as longitude,
                    route_id,
                    sortOrder
                    from location where location.business_id = {$this->business_id}
                    order by address";

        $query = $this->db->query($sql);
        $locations = $query->result();
        $formatted_leading_integer = array();
        foreach ($locations as $location) {
            if ( !is_numeric( substr( $location->address, 0, 1 ) ) ) {
                $location->address = '0' . $location->address;
            }
            $formatted_leading_integer[] = $location;
        }
        echo json_encode( $formatted_leading_integer );
    }


    public function getLocationReport()
    {
        $sql = "SELECT bagNumber, order_id, deliveryTime, lockerName, status, statusNotes
        FROM droid_postDeliveries
        WHERE DATE_ADD(NOW(), INTERVAL -1 HOUR) <= deliveryTime
        AND location_id = ".$this->location_id;
        $results = $this->db->query($sql)->result();
        foreach ($results as $result) {

            $sql = "SELECT phone FROM customer
                    INNER JOIN bag ON customerID = customer_id
                    WHERE bagNumber = {$result->bagNumber}";
            $customer = $this->db->query($sql)->row();
            $phone = $customer->phone;
            $result->code = substr($phone, sizeof($phone)-5,4);

            if ($result->statusNotes!='') {
                $notes = unserialize($result->statusNotes);
                $result->statusNotes = $notes;
            }

            $r[] = $result;
        }

        echo json_encode($r);

    }

    /**
     * Gets orders
     */
    public function getOrders()
    {
       $business = new Business($this->business_id);
       $CI = get_instance();
       $blankCustomer = $business->blank_customer_id;

        $sql = "SELECT
        orderID as order_id,
        loaded as loaded_on_van,
        delivered,
        orderPaymentStatusOption.name as payStatus,
        orderStatusOption.name as name,
        orderStatusOption_id as ordStat,
        orders.dateCreated,
        bagNumber as bag_number,
        location.address as location_name,
        location.accessCode,
        location.sortOrder as sort_order,
        locationID as location_id,
        location.serviceType,
        lockerName as locker_id,
        firstName as first_name,
        lastName as last_name,
        email,
        customer.phone as phone,
        customer.address1 as address1,
        customer.address2 as address2,(SELECT COUNT( * )
        FROM orderItem
        INNER JOIN product_process ON product_processID = orderItem.product_process_id
        INNER JOIN product ON productID = product_process.product_id
        INNER JOIN alertItem ON alertItem.product_id = productID
        WHERE order_id = orderID
        )
        as specialItemTotal
        FROM orders
        LEFT JOIN customer on customerID = orders.customer_id
        JOIN locker on lockerID = orders.locker_id
        JOIN location on locationID = locker.location_id
        JOIN bag on bagID = orders.bag_id
        JOIN orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
        JOIN orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id
        WHERE (orderStatusOption_id NOT IN (1,2,10,13))
        and orders.customer_id != ".$blankCustomer."
        AND route_id in ($this->routes)
        AND delivered = 0
        AND location.business_id = {$this->business_id}
        AND orders.customer_id != 0";
        //echo $sql;
        $orders = $this->db->query($sql)->result();

        $this->load->model('order_model');
        $count = 0;
        foreach ($orders as $o) {
            $type = $this->order_model->getOrderType($o->order_id);
            $orders[$count]->order_type = $type;
            $count++;
        }

        echo json_encode($orders);
    }

    /**
     * Gets orders for wash and fold
     */
    public function getWashFold()
    {
        $sql = "SELECT orderID as order_id,
        postAssembly,
        orders.dateCreated,
        bagNumber as bag_number,
        location.address as street,
        concat(firstName,' ',lastName) as customer_name,
        location.route_id,
        position
        FROM orders
        LEFT JOIN assembly ON assembly.order_id = orderID
        JOIN customer on customerID = orders.customer_id
        JOIN locker on lockerID = orders.locker_id
        JOIN location on locationID = locker.location_id
        JOIN bag on bagID = orders.bag_id
        WHERE orders.orderStatusOption_id NOT IN (1, 10)
        AND orders.business_id = {$this->business_id}";



        $orders = $this->db->query($sql)->result();
        foreach ($orders as $order) {
            if ($order->postAssembly =='') {
                $order->postAssembly = 'null';
            }
            $out[] = $order;
        }
        echo json_encode($out);
    }

    /**
     * gets the phone number of the bag owner so the driver can close
     * the locker and enter the proper code
     */
    public function getLockerCode()
    {
        $sql = "SELECT phone, bagID from customer
        JOIN bag on bag.customer_id = customerID
        WHERE bagNumber = '".ltrim(trim($this->bagNumber), '0')."'
        AND customer.business_id = {$this->business_id}";
        $query = $this->db->query($sql);
        if ($phone = $query->row()) {

            // get the last order for the bag
            $sql = "SELECT dateCreated, bag_id from orders where bag_id = $phone->bagID order by orderID DESC limit 1";
            $query = $this->db->query($sql);
            $order = $query->row();

            //respond that they can now place an order
            $business = new Business($this->business_id);
            $CI = get_instance();
            $CI->business->timezone = $business->timezone;

            // get the last order date, if over 3 days old we display the date with a red background on the phone
            // to notify the driver the order needs some action (pickup reminder)
            $lastOrderDate = convert_from_gmt_aprax($order->dateCreated, STANDARD_DATE_FORMAT);
            $threeDays = strtotime ( '-3 day' , strtotime ( date("Y-m-d") ) ) ;
            if (strtotime($lastOrderDate)<$threeDays) {
                $style='background-color:#cc0000; color:#fff;padding:10px';
            }
            $response = 'Locker Code: '.substr($phone->phone,-4);
            $response .='<hr><br><div style="'.$style.'">Last order date: '.$lastOrderDate.'</div>';
        } else {
            $response = 'Sorry, we could not find an account with bag number: '.$this->fullBagNumber;

        }
        echo $response;
    }

    /**
     * gets driver notes
     */
    public function getDriverNotes()
    {
        $sql = "SELECT
        DriverNotesID as note_id,
        created as date_created,
        location_id,
        note,
        active,
        employee.firstName, employee.lastName
        FROM driverNotes
        INNER JOIN location l ON driverNotes.location_id = locationID
        INNER JOIN business_employee on ID = createdBy
        INNER JOIN employee ON employee_id = employeeID
        WHERE active = 1
        AND l.route_id in ({$this->routes})
        AND driverNotes.business_id = {$this->business_id}";
        $notes = $this->db->query($sql)->result();
        echo json_encode($notes);
    }

    public function getWashFoldLog()
    {
        if ($this->job == 'get') {
            $sql = "SELECT orderID as order_id, bagNumber as bag_number
            FROM orders
            INNER JOIN bag on bagID = bag_id
            WHERE orderStatusOption_id != 10 AND orderPaymentStatusOption_id != 3
            AND orders.business_id = {$this->business_id}";
            $orders = $this->db->query($sql)->result();
            echo json_encode($orders);

            return true;
        }

        if ($this->job =='post') {

            if (!$this->bagNumber) {
                die("Missing Bag Number or invalid");
            }

            $bag = Bag::search(array(
                'bagNumber' => $this->bagNumber,
                'business_id' => $this->business_id,
            ));

            if (!$bag) {
                echo 'Bag Not Found. ';
                return false;
            }

            // common to all email messages
            $subject = "Dirty WF Scan {$this->fullBagNumber}";
            $host = $_SERVER['HTTP_HOST'];

            $bag_url = "https://$host/admin/orders/bag_barcode_details/0/{$this->bagNumber}";
            $bag_link = "<a href='$bag_url'>{$this->bagNumber}</a>";

            // check for customer
            if (!$bag->customer_id) {
                echo 'Bag has no customer, give to customer service.';
                $message = "Bag {$this->fullBagNumber} was scanned on the android phone "
                         . "but the system could not find any customer for this bag. <br><br>"
                         . $bag_link;
                send_admin_notification($subject, $message, $this->business_id);
                return false;
            }

            $customer = new Customer($bag->customer_id);
            $customer_url = "$host/admin/customers/detail/{$customer->customerID}";
            $customer_link = "<a href='$customer_url'>{$customer->firstName} {$customer->lastName} ({$customer->customerID})</a>";

            if ($customer->hold == 1) {
                echo 'Customer on hold, give to customer service.';
                $message = "Can not create order because there is a hold on {$customer_link}'s account<br><br>"
                         . "Bag: $bag_link";
                send_admin_notification($subject, $message, $this->business_id);
                return false;
            }

            $this->load->model('order_model');

            $claim = false;
            $claims = Claim::search_aprax(array(
                'customer_id' => $bag->customer_id,
                'business_id' => $this->business_id,
                "active" => 1,
            ));

            if ($claims) {

                // check that there is only one claim at the locker
                if (count($claims) > 1) {
                    echo 'Multiple claims were found, give to customer service.';
                    $message = "Multiple claims were found for customer $customer_link. "
                             . "Only one claim should exist.<br><br>"
                             . "Bag: $bag_link";
                    send_admin_notification($subject, $message, $this->business_id);

                    return false;
                }

                $claim = $claims[0];
                $locker = new Locker($claim->locker_id);

            // If no claim, try to use the last order
            } else if ($orders = $this->order_model->getOrdersByBag($this->bagNumber, $this->business_id)) {

                $pastOrder = $orders[0];

                //FIXME: hardcoded locationType name
                if ($pastOrder->lockerType == "lockers" && $pastOrder->lockType == "Electronic") {
                    $locker = new Locker($pastOrder->locker_id);
                }
            }

            //$location_url = "$host/admin/orders/view_location_orders/$location->locationID";
            //$location_link = "<a href='$location_url'>{$location->address}</a>";

            if (!empty($locker)) {
                // create the order
                $notes = "";
                $options['customer_id'] = $bag->customer_id;
                $options['claimID'] = $claim ? $claim->claimID : null;
                $order_id = $this->order_model->new_order(
                    $locker->location_id,
                    $locker->lockerID,
                    $this->bagNumber,
                    $this->employee_id,
                    $notes,
                    $this->business_id,
                    $options
                );

                // if order was created
                if ($order_id) {
                    echo "Order created: #$order_id. ";
                    return true;
                }
            }

            echo 'No open order, give to customer service.';
            $message = "Bag {$this->fullBagNumber} was scanned on the android phone "
                     . "but the system could not find any orders for this bag. <br><br>"
                     . "Bag: $bag_link";
            send_admin_notification($subject, $message, $this->business_id);

        }
    }

    /**
     * creates a ticket from an issue sent from a phone
     */
    public function postIssue()
    {
        $this->load->model('droidpostdrivernotes_model');
        $options['noteType'] = 'issue';
        $options['employeeUsername'] = $this->username;
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;
        $options['transaction_id'] = $this->transaction_id;
        $options['notes'] = urldecode($this->driverNote);
        $options['location_id'] = $this->location_id;
        $options['lockerName'] = $this->lockerName;
        $options['bagNumber'] = $this->bagNumber;
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        $this->droidpostdrivernotes_model->insert($options);

        echo "SUCCESS|".$this->transaction_id;

    }

    /**
     * sets the order to status of loaded
     *
     * @throws Exception
     */
    public function postLoadVan()
    {
        try {
            if (!$this->order_id) {
                throw new Exception("Missing order id");
            }

            $this->deliveryTime = $this->formatDate(urldecode($this->get['DELTIME']), $this->business_id);

            $loadVanObj = new DroidLoadVan();
            $result = $loadVanObj->loadVan($this->order_id, $this->deliveryTime, $this->bagNumber, $this->username, $this->employee_id, $this->business_id);
        } catch (Exception $e) {
            $result = $e->getmessage();
        }

        echo $result;

    }

    /**
     * driver sends a note about a claim
     */
    public function postClaimNote()
    {
        $this->load->model('droidpostdrivernotes_model');
        $options['noteType'] = 'claimNote';
        $options['employeeUsername'] = $this->username;
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;
        $options['transaction_id'] = $this->transaction_id;
        $options['notes'] = $this->driverNote;
        $options['claim_id'] = $this->claim_id;
        $options['location_id'] = $this->location_id;
        $options['lockerName'] = $this->lockerName;
        $options['customerName'] = $this->customerName;
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        $this->droidpostdrivernotes_model->insert($options);

        //echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$this->transaction_id));
        echo "SUCCESS|".$this->transaction_id;

    }

    /**
     * driver sends a note about a claim
     */
    public function postDeliveryNote()
    {
        $order = new Order($this->order_id);
        $customer = $order->find('customer_id');
        $locker = $order->find('locker_id');

        $this->load->model('droidpostdrivernotes_model');
        $options['noteType'] = $this->noteType;
        $options['employeeUsername'] = $this->username;
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;
        $options['transaction_id'] = $this->transaction_id;
        $options['notes'] = $this->driverNote;
        $options['customerName'] = $customer->firstName." ".$customer->lastName;
        $options['order_id'] = $this->order_id;
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        $this->droidpostdrivernotes_model->insert($options);

        echo "SUCCESS|".$this->transaction_id;

    }

    /**
     * @deprecated
     * @throws Exception
     */
    public function postNote()
    {
        try {

        if (empty($this->username)) {
        throw new Exception("Username is required");
        }

        if (empty($this->order_id) || !is_numeric($this->order_id)) {
                throw new Exception("Order ID is required and must be numeric");
        }

        if (empty($this->driverNote)) {
        throw new Exception("Missing note");
        }

        $body = (DEV) ? '(DEV MESSAGE, PLEASE IGNORE) ' : '';

        $body .= "Driver: {$this->username} had a problem with order: {$this->order_id} on ".date(STANDARD_DATE_FORMAT)."<br />";
        $body .= "NOTE: ". $this->driverNote;

        send_admin_notification('Driver had a problem with order', $body,$this->business_id);
        echo "SUCCESS";

        //$options = array();
        //$options['postNotes'] = "Delivery Note Sent Successfully";
        //$options['methodCalled'] = __METHOD__;
        //$options['delType'] = 'DeliveryNote';
        //$this->driver_log_model->update($options, array('driverLogID'=>$this->transaction_id));

        } catch (Exception $e) {
        send_exception_report($e);
        }

    }

    /**
     * Driver clockes in or out
     *
     * We always return a success even if failed to get the data off the phone
     */
    public function postTimecard()
    {
        try {

            $sql = "INSERT IGNORE INTO droid_postTimeCards (transaction_id, `inout`, clockTime, employee_id, business_id, status, version, phoneVersion, phoneNumber)
                    VALUES ('{$this->transaction_id}', '{$this->inout}', '".$this->formatDate($this->clockTime, $this->business_id)."','{$this->employee_id}', '{$this->business_id}', 'pending', '".self::VERSION."', '".$this->phoneVersion."', '".$this->phoneNumber."')";
            $this->db->query($sql);
            $timeCardID = $this->db->insert_id();

            if (empty($timeCardID)) {
                try {
                    throw new Exception ("Duplicate entry while saving the timecard for employee_id [". $this->employee_id."]");
                } catch (Exception $e) {
                    send_exception_report($e);
                }

                echo 'SUCCESS|'.$this->transaction_id.'|';

                return false;
            }

            //echo json_encode(array('status'=>"SUCCESS", 'transaction_id'=>$this->transaction_id));
            echo 'SUCCESS|'.$this->transaction_id.'|';

        } catch (\Exception $e) {
            //echo json_encode(array('status'=>"SUCCESS", 'transaction_id'=>$this->transaction_id));
            echo 'SUCCESS|'.$this->transaction_id.'|';
            send_exception_report($e);
        }

      }

    /**
     * converts the date from the phone to GMT dateTime
     *
     * NOTE: if testing and sending requests from a computer that is set up to
     * use GMT, this function will create wrong dateTimes. Basically it will be
     * double converting the GMT date
     *
     * @param date $date
     * @param int $business_id
     * @return date gmtDate;
     */
    function formatDate($date, $business_id)
    {
        $CI = get_instance();
        $business = new Business($business_id);
        $gmtDate = convert_to_gmt_aprax($date, $business->timezone, $format = "Y-m-d H:i:s");

        return $gmtDate;
    }

}
