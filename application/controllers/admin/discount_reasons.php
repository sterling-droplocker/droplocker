<?php

/**
 * Description of discountReasons
 *
 * @author aprax
 */
class Discount_Reasons extends MY_Admin_Controller
{
    public function get_total()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get('business_id')) {
                $business_id = $this->input->get('business_id');
                try {
                    $this->load->model('discountreason_model');
                    $discountReasons_total = $this->discountreason_model->get_count($business_id);
                    $result = array('status' => 'success', 'total' => $discountReasons_total);
                } catch (\Exception $e) {
                    $result = array('status' => 'error', 'message' => "An error occurred attempting to retrieve the discount reasons total");
                    send_exception_report($e);
                }
                echo json_encode($result);
            } else {
                throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter");
            }
        } else {
            throw new \LogicException("This action can only be called with an ajax request");
        }
    }
}
