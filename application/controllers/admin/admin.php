<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\UpchargeGroup;
use App\Libraries\DroplockerObjects\UpchargeSupplier;
use App\Libraries\DroplockerObjects\Upcharge;
use App\Libraries\DroplockerObjects\Supplier;
use App\Libraries\DroplockerObjects\ServiceType;
use App\Libraries\DroplockerObjects\Location_Discount;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Acl;
use App\Libraries\DroplockerObjects\AclRoles;
use App\Libraries\DroplockerObjects\AclResource;
use App\Libraries\DroplockerObjects\DropLockerEmail;

class Admin extends MY_Admin_Controller
{
    public $buser_id;
    public $business_id;
    public $business;

    /**
     * The following action renders the interface for managing business lnaguages.
     */
    public function manage_business_languages()
    {
        $this->load->model("business_language_model");

        $content['business_languages'] = $this->business_language_model->get_all_for_business($this->business_id);
        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $content['default_business_language_id'] = $this->business->default_business_language_id;

        $this->load->model("language_model");
        $content['languages_dropdown'] = $this->language_model->get_all_as_codeigniter_dropdown();
        $content['locales'] = $this->config->item('locales');
        $this->renderAdmin('manage_business_languages', $content);
    }

    /**
     * The following action renders the interface for managing business language translations.
     */
    public function manage_languageKeys()
    {
        $this->load->model("language_model");
        $this->load->model("business_language_model");

        $content['business_languages_by_language_dropdown'] = $this->language_model->get_all_as_codeigniter_dropdown_for_business($this->business_id);

        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $content['languages_dropdown'] = $this->language_model->get_all_as_codeigniter_dropdown();

        $content['business_languages'] = $this->business_language_model->get_all_for_business($this->business_id);
        $this->load->model("languageView_model");
        $content['languageViews'] = $this->languageView_model->get_all_as_codeigniter_dropdown();
        natcasesort($content['languageViews']);

        $content['languageViews_dropdown'] = array();
        foreach ($content['languageViews'] as $key => $value) {
            if (strpos($value, '/') === false) {
                $content['languageViews_dropdown'][$key] = $value;
                continue;
            }

            list($prefix,$name) = explode('/', $value, 2);
            $content['languageViews_dropdown']["$prefix /"][$key] = $name;
        }

        $this->load->model("languageKey_business_language_model");
        $content['languageViews_languages_and_languageKeys'] = $this->languageKey_business_language_model->get_languageKeys_for_languages_for_languageViews_for_business($this->business_id);
        $content['business'] = $this->business;

        $this->renderAdmin('manage_languageKeys', $content);
    }

    /**
     * UI for setting a business defaults
     */
    public function defaults()
    {
        try {
            $content['customer'] = new \App\Libraries\DroplockerObjects\Customer($this->business->blank_customer_id, false);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $content['customer'] = new Customer();
        }

        $this->load->model("locker_model");
        $this->load->model("location_model");

        $content['locations_dropdown'] = $this->location_model->get_all_as_codeigniter_dropdown_for_business($this->business_id);
        reset($content['locations_dropdown']);
        $first_locationID = key($content['locations_dropdown']);

        $returnToOffice_locker = $this->locker_model->get_one(array(
            "lockerID" => $this->business->returnToOfficeLockerId
        ));

        if (empty($returnToOffice_locker)) {
            // If there is no defined return to office locker, then the return to office menu in the view is populated with the lockers of the first location specified in the locations dropdown.
            $content['returnToOffice_locker_id'] = null;
            $content['returnToOffice_location_id'] = null;

            if (empty($content['locations_dropdown'])) {
                // If there are no locations for the business, then the lockers dropdown content variable is set to an empty array.
                $content['lockers_dropdown'] = array();
            } else {
                $content['lockers_dropdown'] = array("" => "---SELECT---") + $this->locker_model->get_all_as_codeigniter_dropdown_for_location($first_locationID);
            }
        } else {
            //If there is a defined return to office locker, then the return to office menu in the view is populated with the lockers of the designed default location.
            $content['returnToOffice_location_id'] = $returnToOffice_locker->location_id;
            $content['returnToOffice_locker_id'] = $returnToOffice_locker->lockerID;
            $content['lockers_dropdown'] = $this->locker_model->get_all_as_codeigniter_dropdown_for_location($content['returnToOffice_location_id']);
        }

        //The following variable stores all the lockers associated with this business.
        //$content['all_lockers'] = $this->locker_model->get_all_as_codeigniter_dropdown($this->business_id);

        $prepayment_locker = $this->locker_model->get_one(array(
            "lockerID" => $this->business->prepayment_locker_id
        ));

        if ($prepayment_locker) {
            $content['prepayment_location_id'] = $prepayment_locker->location_id;
            $content['prepayment_locker_id'] = $prepayment_locker->lockerID;
            $content['prepayment_dropdown'] = $this->locker_model->get_all_as_codeigniter_dropdown_for_location($content['prepayment_location_id']);
        } else {
            $content['prepayment_locker_id'] = null;
            $content['prepayment_location_id'] = null;
            $content['prepayment_dropdown'] = array("" => "---SELECT---") + $this->locker_model->get_all_as_codeigniter_dropdown_for_location($first_locationID);
        }

        $content['alertItems'] = \App\Libraries\DroplockerObjects\AlertItem::get($this->business_id);
        $content['business'] = $this->business;
        $content['map_options'] = array(
            0 => 'No Map',
            1 => 'Map v1',
            2 => 'Map v2',
            3 => 'Map v3',
        );

        $this->load->model('product_model');

        $content['product_options'] = $this->product_model->get_all_as_codeigniter_dropdown($this->business_id);

        $content['print_barcode_options'] = array(
            'bag' => 'Bag Number',
            'order' => 'Order ID',
            'item' => 'First Item',
        );
        
        $this->load->library('units');
        $content['units_dropdown'] = $this->units->get_distance_units_for_dropdown();
        
        $this->template->add_js('js/admin/defaults.js');
        $this->renderAdmin('defaults', $content);
    }

    /**
     * updates the defaults for a business. There is no view, only redirects back to the defaults function
     */
    public function update_defaults()
    {
        $business = new Business($this->business_id);
        $business->blank_customer_id = $this->input->post("blank_customer_id");
        $business->returnToOfficeLockerId = $this->input->post("returnToOffice_locker_id");
        $business->store_access = $this->input->post("store_access");
        $business->store_access = empty($business->store_access)?0:$business->store_access;
        $business->prepayment_locker_id = $this->input->post("prepayment_locker_id");
        $business->howitworks = $this->input->post("howitworks");
        $business->wf_traveller_text = $this->input->post("wf_traveller_text");
        $business->lotSize = $this->input->post("lotSize");
        $business->assemblyBar = $this->input->post("assemblyBar");
        $business->turnaround = $this->input->post("turnaround");
        $business->scheduled_orders_time = $this->input->post("scheduled_orders_time");
        $business->deactivate_hd_claims_time = $this->input->post("deactivate_hd_claims_time");

        $card_required = $this->input->post("require_card_on_order");
        if ( empty( $card_required ) ) {
            $business->require_card_on_order = 0;
        } else {
            $business->require_card_on_order = 1;
        }

        $business->show_locations_map = $this->input->post("show_locations_map");
        $business->show_contact_link  = $this->input->post("show_contact_link");

        $wf_min_type   = $this->input->post('wf_min_type');
        $wf_min_amount = $this->input->post('wf_min_amount');
        $dc_min_type   = $this->input->post('dc_min_type');
        $dc_min_amount = $this->input->post('dc_min_amount');

        $business->wf_min_type   = '';
        $business->wf_min_amount = '';
        $business->dc_min_type   = '';
        $business->dc_min_amount = '';

        if ( !empty($wf_min_type) && !empty($wf_min_amount) ) {
            $business->wf_min_type   = $wf_min_type;
            $business->wf_min_amount = $wf_min_amount;
        }

        if ( !empty($dc_min_type) && !empty($dc_min_amount) ) {
            $business->dc_min_type   = $dc_min_type;
            $business->dc_min_amount = $dc_min_amount;
        }



        $home_charge_amount = $this->input->post('home_delivery_charge_amount');
        $home_charge_type   = $this->input->post('home_delivery_charge_type');

        if ( !empty( $home_charge_type ) && !empty( $home_charge_amount ) ) {
            $business->home_delivery_charge_type   = $home_charge_type;
            $business->home_delivery_charge_amount = $home_charge_amount;
        } else {
            $business->home_delivery_charge_type   = '';
            $business->home_delivery_charge_amount = '';
        }

        $business->save();

        /* Map api key */
        update_business_meta($this->business_id, 'location_hd_exclusion_radius', $this->input->post("location_hd_exclusion_radius"));
        update_business_meta($this->business_id, 'location_hd_exclusion_radius_unit', $this->input->post("location_hd_exclusion_radius_unit") ? $this->input->post("location_hd_exclusion_radius_unit") : Units::DISTANCE_MILES);
        update_business_meta($this->business_id, 'location_distance_display_unit', $this->input->post("location_distance_display_unit") ? $this->input->post("location_distance_display_unit") : Units::DISTANCE_MILES);
        update_business_meta($this->business_id, 'maps_api_key', $this->input->post("maps_api_key"));

        update_business_meta($this->business_id, 'default_package_delivery_id', $this->input->post("default_package_delivery_id"));
        update_business_meta($this->business_id, 'default_redo_id', $this->input->post("default_redo_id"));
        update_business_meta($this->business_id, 'invoiceNotes', $this->input->post("invoice"));
        update_business_meta($this->business_id, 'order_limit', $this->input->post("order_limit"));
        update_business_meta($this->business_id, 'hide_home_delivery_locations', $this->input->post("hide_home_delivery_locations"));
        update_business_meta($this->business_id, 'show_manual_discounts', $this->input->post("show_manual_discounts"));
        update_business_meta($this->business_id, 'max_receipt_items', $this->input->post("max_receipt_items"));
        update_business_meta($this->business_id, 'require_address2', $this->input->post("require_address2"));

        /* Locations options */
        update_business_meta($this->business_id, 'location_result_limit', $this->input->post("location_result_limit"));
        update_business_meta($this->business_id, 'private_location_radius', $this->input->post("private_location_radius"));
        update_business_meta($this->business_id, 'private_location_radius_unit', $this->input->post("private_location_radius_unit") ? $this->input->post("private_location_radius_unit") : Units::DISTANCE_FEET);

        /* European VAT */
        update_business_meta($this->business_id, 'breakout_vat', $this->input->post("breakout_vat"));

        /* Print Day Tags */
        update_business_meta($this->business_id, 'print_day_tags', $this->input->post("print_day_tags"));

        /* Weight tolerance */
        update_business_meta($this->business_id, "weight_upper_tolerance", $this->input->post('weight_upper_tolerance'));
        update_business_meta($this->business_id, "weight_lower_tolerance", $this->input->post('weight_lower_tolerance'));

        /* Pay with cash */
        update_business_meta($this->business_id, 'pay_with_cash', $this->input->post("pay_with_cash"));

        /* Item edit options */
        update_business_meta($this->business_id, 'displayBrand', $this->input->post("displayBrand"));
        update_business_meta($this->business_id, 'displayColorOptions', $this->input->post("displayColorOptions"));

        /* hide business in POS */
        update_business_meta($this->business_id, 'hide_business_in_pos', $this->input->post("hide_business_in_pos"));

        /* Print bardcode in POS */
        update_business_meta($this->business_id, 'print_barcode_pos', $this->input->post("print_barcode_pos"));

        /* send_reassigned_barcode_report_daily */
        update_business_meta($this->business_id, 'send_reassigned_barcode_report_daily', $this->input->post("send_reassigned_barcode_report_daily"));

        /* send_reassigned_barcode_report_email */
        update_business_meta($this->business_id, 'send_reassigned_barcode_report_email', $this->input->post("send_reassigned_barcode_report_email"));

        /* Print bardcode in POS */
        update_business_meta($this->business_id, 'duplicate_receipt_pos', $this->input->post("duplicate_receipt_pos"));

        /* Show business address on receipts */
        update_business_meta($this->business_id, 'show_business_address_on_receipts', $this->input->post("show_business_address_on_receipts"));
        
        /* Retake Picture Alert after X times submitted for cleaning */
        update_business_meta($this->business_id, 'retake_picture_after_x_times_submitted_for_cleaning', $this->input->post("retake_picture_after_x_times_submitted_for_cleaning"));

        /* Require Time Window Home Delivery Customer to select time window for delivery */
        update_business_meta($this->business_id, 'hd_time_window_require_customer_delivery_selection', $this->input->post("hd_time_window_require_customer_delivery_selection"));

        /* Require Time Window Home Delivery Customer to select time window for delivery */
        update_business_meta($this->business_id, 'limit_deliveries_by_due_date', $this->input->post("limit_deliveries_by_due_date"));


        update_business_meta($this->business_id, 'home_delivery_hide_driver_notes', $this->input->post("home_delivery_hide_driver_notes"));


        update_business_meta($this->business_id, 'home_delivery_enable_order_reschedule', $this->input->post("home_delivery_enable_order_reschedule"));

        update_business_meta($this->business_id, 'pickup_remainder_after', $this->input->post("pickup_remainder_after"));
               
        update_business_meta($this->business_id, 'pos_receipt_include_due_date', $this->input->post("pos_receipt_include_due_date"));
               
        update_business_meta($this->business_id, 'delivery_receipt_include_due_date', $this->input->post("delivery_receipt_include_due_date"));
               

        /* Set Wash & Fold Rounding Precision */
        update_business_meta($this->business_id, 'wf_rounding_precision', $this->input->post("wf_rounding_precision"));

        update_business_meta($this->business_id, 'autocomplete_orders', $this->input->post("autocomplete_orders"));

        set_flash_message('success', 'Defaults have been updated');

        $this->goBack('/admin/admin/defaults');
    }

    /**
     * updates the default emails.
     * There is no view, this redirects back to defaults.
     *
     *  If there is no $_POST then redirect back to the defaults
     */
    public function update_email_defaults()
    {
        if (!isset($_POST['submit'])) {
            set_flash_message('error', 'No request data sent');
            redirect('admin/admin/defaults');
        }

        $email_fields = array('timeoff','customerSupport','operations', 'accounting', 'sales');
        $validate = '';
        foreach ($email_fields as $e) {
            //check if more of one email in the field;
            $_POST[$e] = preg_replace('/\s+/', '', $_POST[$e]);
            $emailsArray = explode(',', $_POST[$e]);
            foreach ($emailsArray as $emailValue) {
                if ( (!empty($emailValue)) && (!filter_var($emailValue, FILTER_VALIDATE_EMAIL)) ) {
                    $validate.= $e.', ';
                }
            }
        }

        if ($validate != '') {
            set_flash_message('error', 'Please, correct the following Email fields: '.substr($validate,0,-2));
            redirect('admin/admin/defaults');
        }

        update_business_meta($this->business->businessID, 'timeOff', isset($_POST['timeoff'])?$_POST['timeoff']:'');
        update_business_meta($this->business->businessID, 'from_email_name', isset($_POST['from_email_name'])?$_POST['from_email_name']:'');
        update_business_meta($this->business->businessID, 'testEmail', isset($_POST['testEmail'])?$_POST['testEmail']:'');
        update_business_meta($this->business->businessID, 'customerSupport', isset($_POST['customerSupport'])?$_POST['customerSupport']:'');
        update_business_meta($this->business->businessID, 'operations', isset($_POST['operations'])?$_POST['operations']:'');
        update_business_meta($this->business->businessID, 'accounting', isset($_POST['accounting'])?$_POST['accounting']:'');
        update_business_meta($this->business->businessID, 'sales', isset($_POST['sales'])?$_POST['sales']:'');
        update_business_meta($this->business->businessID, 'send_reassigned_barcode_report_email', isset($_POST['send_reassigned_barcode_report_email'])?$_POST['send_reassigned_barcode_report_email']:'');

        set_flash_message('success', 'Default Emails have been updated');

        $this->goBack('/admin/admin/defaults');
    }

    /**
     * updates the default recaptcha settings.
     * There is no view, this redirects back to defaults.
     *
     *  If there is no $_POST then redirect back to the defaults
     */
    public function update_recaptcha_defaults()
    {
        if (!isset($_POST['submit'])) {
            set_flash_message('error', 'No request data sent');
            redirect('admin/admin/defaults');
        }

        $validate = array();
        if ($_POST["recaptcha_enabled"]) {
            if (empty($_POST["recaptcha_public_key"])) {
                $validate[] = "site key";
            }
            if (empty($_POST["recaptcha_private_key"])) {
                $validate[] = "secret key";
            }
        }

        if (count($validate) > 0) {
            set_flash_message('error', 'Please, correct the following recaptcha fields: '.implode(", ", $validate));
            redirect('admin/admin/defaults');
        }

        update_business_meta($this->business_id, 'recaptcha_enabled', $_POST['recaptcha_enabled']);
        update_business_meta($this->business_id, 'recaptcha_public_key', trim($_POST['recaptcha_public_key']));
        update_business_meta($this->business_id, 'recaptcha_private_key', trim($_POST['recaptcha_private_key']));

        set_flash_message('success', 'Default recaptcha settings have been updated');

        $this->goBack('/admin/admin/defaults');
    }

    /**
     * adds an alertItem to the database
     */
    public function add_alert_items()
    {
        if (isset($_POST['product_id'])) {
            foreach ($_POST['product_id'] as $product_id) {
                $sql = "INSERT IGNORE INTO alertItem (business_id, product_id) VALUES (".$this->business_id.", $product_id)";
                $this->db->query($sql);
            }
            set_flash_message("success", "Alert item has been added");
        } else {
            set_flash_message('error', 'No request data sent');
        }

        $this->goBack('/admin/admin/defaults');
    }

    /**
     * Deletes an alertItem from the database
     */
    public function delete_alert_item()
    {
        if (isset($_POST['delete_alert_submit'])) {
            $alertItem = new \App\Libraries\DroplockerObjects\AlertItem($_POST['alertItem_id']);
            if ($alertItem->delete()) {
                set_flash_message('success', "Alert item was deleted");
            } else {
                set_flash_message('error', "Alert item was NOT deleted");
            }
        }

        $this->goBack('/admin/admin/defaults');
    }

    public function toggleAllowToRedeem($lockerLootConfigID = null, $emableFlag  = null)
    {
        $lockerLootConfig = new LockerLootConfig($lockerLootConfigID);

        if ($lockerLootConfig->business_id != $this->business_id) {
            set_flash_message("error", "Business mismatch");
            redirect("/admin/admin/locker_loot");
        }

        $lockerLootConfig->allowToRedeem = $emableFlag;
        if ($lockerLootConfig->save()) {
            set_flash_message("success", "Allow to redeem has been updated");
        } else {
            set_flash_message('error','Failed updating allow to redeem');
        }

        redirect("/admin/admin/locker_loot");
    }

    public function locker_loot()
    {
        $this->load->model("lockerLootConfig_model");

        $content['lockerLootConfig'] = $this->lockerLootConfig_model->get_one(array(
            'business_id' => $this->business_id
        ));

        $this->setPageTitle('Customer Reward program');
        $this->renderAdmin('locker_loot', $content);
    }

    public function locker_loot_terms()
    {
        if (isset($_POST['submit'])) {
            $lockerLootConfig = new LockerLootConfig($_POST['id']);
            $lockerLootConfig->terms = $_POST['body'];
            if ($lockerLootConfig->save()) {
                set_flash_message('success', 'Locker Loot terms have been updated');
            } else {
                set_flash_message('error', 'Locker Loot terms have not been updated');
            }

            redirect($this->uri->uri_string());
        }

        $lockerLootConfig = LockerLootConfig::search_aprax(array('business_id'=>$this->business_id));
        $lockerLootConfig = $lockerLootConfig[0];

        $content['terms'] = $lockerLootConfig->terms;
        $content['id'] = $lockerLootConfig->lockerLootConfigID;

        $this->renderAdmin('locker_loot_terms', $content);
    }

    //@TODO move into locations controller
    public function location_details($locationID = null)
    {
        $post = unserialize($this->session->flashdata('post'));
        if ($locationID) {
            try {
                $location = new \App\Libraries\DroplockerObjects\Location($locationID);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Location ID '$locationID' not found");
                redirect($_SERVER['HTTP_REFERER']);
            }
        } elseif (!empty($post)) {
            $location = (object) $post;
        } else {
            $location = new \App\Libraries\DroplockerObjects\LocationType;
        }

        $this->load->model('location_model');
        $this->load->model('locker_model');


        //location locker info
        //get all locker types
        $form_data['locker_types'] = $this->locker_model->get_locker_style(array('select'=>'name, lockerStyleID'));

        //get all lock types
        $form_data['lock_types'] = $this->locker_model->get_locker_lock_type(array('select'=>'name, lockerLockTypeID'));

        //get the lockers in this location
        $content['lockers'] = $this->locker_model->get( array( 'lockerStatus_id' => 1,
                                                               'with_status'     => true,
                                                               'location_id'     => $location->properties['locationID'],
                                                               'select'          => 'lockerID,lockerStyle.name,lockerLockType.name as lockName,lockerName, lockerStatus.name as lockerStatus') );

        //displays different forms depending on the lockertype
        switch ($location->lockerType) {
            case('lockers'):
                // add_locker_form
                $this->template->add_js('js/add_locker_form.js');
                $content['include_form'] = $this->load->view('inc/add_locker_form', $form_data, true);
            break;

            case('units'):
                // add_unit_form
                $this->template->add_js('js/add_unit_form.js');
                $content['include_form'] = $this->load->view('inc/add_unit_form', $form_data, true);
            break;

            case('description'):
                // add_description_form
                $content['include_form'] = $this->load->view('inc/add_description_form', $form_data, true);
            break;
        }

        //get customers that have ever had an order here.
        $getCustomer = "select customer_id, reports_customer.*
                        from report_orders
                        join reports_customer on customerID = customer_id
                        where report_orders.location_id = $locationID
                        group by customer_id
                        order by firstName";
        $q = $this->db->query($getCustomer);
        $content['customers'] = $q->result();

        //get all notes
        $getNotes = "select locationNote.*, firstName, lastName
                        from locationNote
                        join business_employee on locationNote.employee_id = business_employee.ID
                        join employee on employeeID = business_employee.employee_id
                        where location_id = $locationID
                        order by updated desc";
        $q = $this->db->query($getNotes);
        $content['notes'] = $q->result();

        $this->template->add_js('https://maps.googleapis.com/maps/api/js?sensor=false&key=' . get_default_gmaps_api_key(),'source');
        $content['locationTypes'] = \App\Libraries\DroplockerObjects\LocationType::get_all();
        $content['location'] = $location;

        $this->template->write('page_title','Location Details', true);

        $this->data['content'] = $this->load->view('admin/locations/details', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function item_specifications()
    {
        $getSpecifications = "select * from specification where business_id = $this->business_id order by description";
        $query = $this->db->query($getSpecifications);
        $content['specifications'] = $query->result();

        $this->template->write('page_title','Item Specifications', true);

        $this->renderAdmin('item_specifications', $content);
    }

    public function item_specifications_add()
    {
        $addSpecifications = "insert into specification (description, everyOrder, business_id) values (?, ?, ?)";
        $query = $this->db->query($addSpecifications, array($_POST['specification'], $_POST['everyOrder'], $this->business_id));
        if($query) {
            set_flash_message('success', "New specification added");
        } else {
            set_flash_message('error', "Error adding specification");
        }
        redirect("/admin/admin/item_specifications");
    }

    public function item_specifications_delete()
    {
        $getItems = "select * from item_specification where specification_id = ? limit 1";
        $query = $this->db->query($getItems, array($_GET['delete']));

        if ($query->num_rows) {
            set_flash_message('error', "Can not delete this specification because it is already added to an item");
        } else {
            $delSpecifications = "delete from specification where specificationID = ? and business_id = ?";
            $query = $this->db->query($delSpecifications, array($_GET['delete'], $this->business_id));
            if($query) {
                set_flash_message('success', "Specification deleted");
            } else {
                set_flash_message('error', "Error deleting specification");
            }
        }
        redirect("/admin/admin/item_specifications");
    }

    //TODO move into pressing controller
    public function pressing_stations()
    {
        $getStations = "select * from pressingStation where business_id = $this->business_id order by description, location";
        $query = $this->db->query($getStations);
        $content['stations'] = $query->result();

        $this->template->write('page_title','Pressing Stations', true);
        $this->renderAdmin('pressing_stations', $content);
    }

    //TODO move into pressing controller
    public function pressing_station_add()
    {
        $addStation = "insert into pressingStation (description, location, targetPPH, business_id, rework)
                        values ('$_POST[description]','$_POST[location]', '$_POST[targetPPH]', $this->business_id, '$_POST[rework]')";
        $query = $this->db->query($addStation);
        set_flash_message('success', "Station Added");
        redirect("/admin/admin/pressing_stations");
    }

    //TODO move into location controller
    public function location_details_add_note()
    {
        $addNote = "insert into locationNote (location_id, note, employee_id) values (".$this->input->post("locationID").", '".mysql_real_escape_string($this->input->post("notes"))."', $this->buser_id)";
        $query = $this->db->query($addNote);
        set_flash_message('success', "Note Added");
        redirect("/admin/admin/location_details/".$this->input->post("locationID"));
    }

    public function bulk_pricing_update()
    {
        //get all pricing for this company
        $getProducts = "select product.name as prodName, product_process.price, process.name as procName, productCategory.name as catName
                    from product
                    join product_process on product_id = productID
                    join process on process_id = processID
                    join productCategory on productCategoryID = productCategory_id
                    where product.business_id = $this->business_id
                    order by productCategory.name, product.name, process.name";
        $query = $this->db->query($getProducts);
        $content['products'] = $query->result();

        $this->template->write('page_title','Bulk Pricing Update', true);

        $this->renderAdmin('bulk_pricing_update', $content);
    }

    public function bulk_pricing_percent_run()
    {
        if ($this->input->post('percent') > 100 or $this->input->post('percent') < 1) {
            set_flash_message('error', "Percent must be between 1 and 100");
            redirect("/admin/admin/bulk_pricing_update");
        } else {
            $getProducts = "select product_processID, product.name as prodName, product_process.price, process.name as procName, productCategory.name as catName
                        from product
                        join product_process on product_id = productID
                        join process on process_id = processID
                        join productCategory on productCategoryID = productCategory_id
                        where product.business_id = $this->business_id
                        order by productCategory.name, product.name, process.name";
            $query = $this->db->query($getProducts);
            $products = $query->result();

            $percent = ($this->input->post('percent') / 100) + 1;
            foreach ($products as $product) {
                $newPrice = 0;
                $newPrice = number_format($product->price * $percent,2, '.', '');
                $updatePrice = "update product_process set price = $newPrice where product_processID = $product->product_processID";
                $this->db->query($updatePrice);
            }

            if ($this->input->post('perLocation') == 1) {
                //get Per location pricing
                $getLocProduct = "select * from product_location_price where business_id = $this->business_id";
                $query = $this->db->query($getLocProduct);
                $locProducts = $query->result();

                foreach ($locProducts as $locProduct) {
                    $newPrice = 0;
                    $newPrice = number_format($locProduct->price * $percent,2, '.', '');
                    $updatePrice = "update product_location_price set price = $newPrice where product_location_priceID = $locProduct->product_location_priceID";
                    $this->db->query($updatePrice);
                }
            }

            set_flash_message('success', "Prices Updated");
            redirect("/admin/admin/bulk_pricing_update");
        }
    }



    /**
     * services method allows the user to update the services they provide to the customers
     *
     * Example Services
     * -------------------
     * wash and fold
     * dry cleaning
     * shoe shine
     *
     * Each service is actually a product. Each product will have a module to manage that products rules
     */
    public function services()
    {
        $this->load->model('product_model');

        $type_array        = array();
        $master_type_array = array();
        $content['serviceTypes'] = $serviceTypes = ServiceType::search_aprax(array(
            'visible' => 1,
            'business_id = 0 OR business_id =' => $this->business_id,
        ));

        foreach ($serviceTypes as $serviceType) {
            $type_array[$serviceType->serviceTypeID] = $serviceType->name;
            $master_type_array[$serviceType->serviceTypeID] = $serviceType;
        }

        if (isset($_POST['submit'])) {
            $displayNames = $this->input->post('displayName');    //was a displayName added?

            foreach ($_POST['type'] as $t) {
                $displayName = $displayNames[$t];
                if ( empty( $displayName ) ) {    
                    continue; // If empty, then the business does not have this serviceType.
                }
                $store[$t] = array(
                    'serviceType_id' => $t,
                    'active' => 1,  //not sure where this falls, throwin' it in
                    'service' => $type_array[$t],
                    'displayName' => $displayName
                );
            }

            $types = serialize($store);

            if ($this->business_model->update(array('serviceTypes'=>$types), array('businessID'=>$this->business_id))) {
                set_flash_message("success", "Updated Service Types for {$this->business->companyName}");
                redirect('/admin/admin/services');
            }

        }

        $business = $this->business_model->get(array('select' => 'serviceTypes', 'businessID' => $this->business_id));

        if ($business[0]->serviceTypes) {
            $content['current_types'] = unserialize($business[0]->serviceTypes);
        }

        //order the serviceTypes accordingly - for rendering the sorting
        $system_types  = $type_array;
        $sorted_output = array();
        foreach ($content['current_types'] as $id => $ordered_type) {
            $sorted_output[] = $id;
            unset( $system_types[ $id ] );
        }
        foreach ($system_types as $s_id => $system_type) {
            $sorted_output[] = $s_id;
        }

        $new_service_order = array();
        $services_is_use = array();
        $this->load->model("location_services_model");
        foreach ($sorted_output as $id) {
            $new_service_order[$id] = $master_type_array[$id];
            if ($this->location_services_model->in_use($id, $this->business_id)) {
                $services_is_use[] = $id;
            }
        }
        $content['serviceTypes'] = $new_service_order;
        $content['services_is_use'] = $services_is_use;

        if (in_bizzie_mode()) {
            $content['header'] = "Service Types for {$this->business->companyName}";
        } else {
            $content['header'] = "Service Types";
        }

        $this->renderAdmin('services', $content);
    }

    function create_service()
    {
        if (empty($_POST['name']) || empty($_POST['slug'])) {
            set_flash_message('error', 'Missing required fields');

            return redirect('/admin/admin/services');
        }

        $Service = new ServiceType();
        if (!in_bizzie_mode()) {
            $Service->business_id = $this->business_id;
        }
        $Service->visible = 1;
        $Service->slug = $_POST['slug'];
        $Service->name = $_POST['name'];
        $Service->save();

        set_flash_message('success', 'Service Type created');

        redirect('/admin/admin/services');
    }

    public function service_add_remove_to_all_locations()
    {
        $service_id = $this->input->post("service_id");
        $action = $this->input->post("action");

        if (empty($service_id) || empty($action)) {
            set_flash_message('error', 'Missing required fields');

            return redirect('/admin/admin/services');
        }

        $this->load->model("location_services_model");
        $this->load->model("location_model");
        $business_locations = $this->location_model->get(array("business_id"=>$this->business_id));

        if (empty($business_locations)) {
            set_flash_message('error', 'No locations found for this business');
            redirect('/admin/admin/services');
        }

        $valid_actions = array("add_to_all_locations", "remove_from_all_locations");
        
        if (!in_array($action, $valid_actions)) {
            set_flash_message('error', 'Wrong action name');
            return redirect('/admin/admin/services');            
        }
        
        foreach ($business_locations as $bl) {
            if ($action == "add_to_all_locations") {
                $this->location_services_model->add_to_location($bl->locationID, $service_id, $this->business_id);
            } elseif ($action == "remove_from_all_locations") {
                $this->location_services_model->remove_from_location($bl->locationID, $service_id, $this->business_id);
            }
        }     

        if ($action == "add_to_all_locations") {
            set_flash_message('success', 'Service Type added to '.count($business_locations).'  locations');
        } elseif ($action == "remove_from_all_locations") {
            set_flash_message('success', 'Service Type removed from '.count($business_locations).' locations');
        }

        redirect('/admin/admin/services');
    }

    /**
     * Note, the following function is  .
    * edit business details like address
    */
    public function edit()
    {
        $content['business'] = $this->business;

        $business = new Business($this->business_id);

        if (isset($_POST['submit'])) {
            if (is_superadmin() && $this->input->post("businessID")) {
                $business = new Business($this->input->post("businessID"));
            }
            if ($this->input->post("printer_key")) {
                update_business_meta($business->businessID, "printer_key", $this->input->post("printer_key"));
                unset($_POST['printer_key']);
            }

            // Update business meta
            update_business_meta($business->businessID, 'pos_product_images', isset($_POST['pos_product_images'])?$_POST['pos_product_images']:'');
            update_business_meta($business->businessID, 'driver_app_sync_time', isset($_POST['driver_app_sync_time'])?$_POST['driver_app_sync_time']:'');
            update_business_meta($business->businessID, 'timeOff', isset($_POST['timeoff'])?$_POST['timeoff']:'');
            update_business_meta($business->businessID, 'cutOff', isset($_POST['cutOff'])?$_POST['cutOff']:'');
            update_business_meta($business->businessID, 'metalproGrouping', isset($_POST['metalproGrouping'])?$_POST['metalproGrouping']:'');
            update_business_meta($business->businessID, 'loadBags', isset($_POST['loadBags'])?$_POST['loadBags']:'');
            update_business_meta($business->businessID, 'standardDateDisplayFormat', isset($_POST['standardDateDisplayFormat'])?$_POST['standardDateDisplayFormat']:'');
            update_business_meta($business->businessID, 'shortDateDisplayFormat', isset($_POST['shortDateDisplayFormat'])?$_POST['shortDateDisplayFormat']:'');
            update_business_meta($business->businessID, 'fullDateDisplayFormat', isset($_POST['fullDateDisplayFormat'])?$_POST['fullDateDisplayFormat']:'');

            update_business_meta($business->businessID, 'standardDateLocaleFormat', isset($_POST['standardDateLocaleFormat'])?$_POST['standardDateLocaleFormat']:'%A, %B %e');
            update_business_meta($business->businessID, 'shortDateLocaleFormat', isset($_POST['shortDateLocaleFormat'])?$_POST['shortDateLocaleFormat']:'%b %e, %Y');
            update_business_meta($business->businessID, 'fullDateLocaleFormat', isset($_POST['fullDateLocaleFormat'])?$_POST['fullDateLocaleFormat']:'%a, %b %d %Y');
            update_business_meta($business->businessID, 'shortWDDateLocaleFormat', isset($_POST['shortWDDateLocaleFormat'])?$_POST['shortWDDateLocaleFormat']:'%a %m/%d');

            update_business_meta($business->businessID, 'hourLocaleFormat', isset($_POST['hourLocaleFormat'])?$_POST['hourLocaleFormat']:'%l:%M %p');

            /*Bitly config*/
            update_business_meta($business->businessID, 'bitly_client_id', isset($_POST['bitly_client_id'])?$_POST['bitly_client_id']:'');
            update_business_meta($business->businessID, 'bitly_client_secret', isset($_POST['bitly_client_secret'])?$_POST['bitly_client_secret']:'');
            update_business_meta($business->businessID, 'bitly_access_token', isset($_POST['bitly_access_token'])?$_POST['bitly_access_token']:'');

            /* Email Colors */
            update_business_meta($this->business->businessID, 'header_bgcolor', $this->input->post("header_bgcolor"));
            update_business_meta($this->business->businessID, 'header_color', $this->input->post("header_color"));
            update_business_meta($this->business->businessID, 'row_bgcolor', $this->input->post("row_bgcolor"));
            update_business_meta($this->business->businessID, 'row_color', $this->input->post("row_color"));
            update_business_meta($this->business->businessID, 'row_alt_bgcolor', $this->input->post("row_alt_bgcolor"));
            update_business_meta($this->business->businessID, 'row_alt_color', $this->input->post("row_alt_color"));

            //automatic claim deactivation setting
            update_business_meta($business->businessID, 'claim_deactivation', isset($_POST['claim_deactivation'])?$_POST['claim_deactivation']:'','1');

            //money symbol setting
            update_business_meta($business->businessID, 'money_symbol_str', isset($_POST['money_symbol_str'])?$_POST['money_symbol_str']:'','');
            update_business_meta($business->businessID, 'money_symbol_position', isset($_POST['money_symbol_position'])?$_POST['money_symbol_position']:'','1');

            update_business_meta($business->businessID, 'decimal_positions', isset($_POST['decimal_positions'])?$_POST['decimal_positions']:'2');

            //autuomatic laundry plans order capture
            update_business_meta($business->businessID, 'inmediate_order_capture', isset($_POST['inmediate_order_capture'])?$_POST['inmediate_order_capture']:'','0');

            // Show URL on receipt
            update_business_meta($business->businessID, 'hideURLOnReceipt', isset($_POST['hideURLOnReceipt']) ? $_POST['hideURLOnReceipt'] : '', '1');

            update_business_meta($business->businessID, 'hideWWWOnURLOnReceipt', isset($_POST['hideWWWOnURLOnReceipt']) ? $_POST['hideWWWOnURLOnReceipt'] : FALSE);

            update_business_meta($business->businessID, 'invoice_right_header', isset($_POST['invoice_right_header']) ? $_POST['invoice_right_header'] : FALSE);

            update_business_meta($business->businessID, 'invoice_pdf_message', isset($_POST['invoice_pdf_message']) ? $_POST['invoice_pdf_message'] : FALSE);

            if (is_superadmin()) {
                update_business_meta($business->businessID, 'sag_key_set', isset($_POST['sag_key_set']) ? $_POST['sag_key_set'] : FALSE);
            }

            $timezones = DateTimeZone::listIdentifiers();
            $_POST['timezone'] = $timezones[$this->input->post("timezone")];
            $_POST['store_access'] = $this->input->post("store_access");
            $_POST['wash_fold_tracking'] = $this->input->post("wash_fold_tracking");

            //update the business table
            $business->print_temp_tag_header = $_POST["print_temp_tag_header"];
            $business->companyName = $_POST["companyName"];
            $business->address = $_POST["address"];
            $business->address2 = $_POST["address2"];
            $business->city = $_POST["city"];
            $business->state = $_POST["state"];
            $business->country = $_POST["country"];
            $business->zipcode = $_POST["zipcode"];
            $business->email = $_POST["email"];
            $business->phone = $_POST["phone"];


            if (is_superadmin()) {
                if (isset($_POST["website_url"])) {
                    $business->website_url = $_POST["website_url"];
                }
                if (isset($_POST["subdomain"])) {
                    $business->subdomain = $_POST["subdomain"];
                }
            }

            if (isset($_POST["terms"])) {
                $business->terms = $_POST["terms"];
            }
            if ($this->input->post("show_in_droplocker_app")) {
                $business->show_in_droplocker_app = 1;
            } else {
                $business->show_in_droplocker_app = 0;
            }
            $business->locale = $_POST["locale"];
            $business->timezone = $_POST["timezone"];
            $business->store_access = $_POST["store_access"];
            $business->wash_fold_tracking = $_POST["wash_fold_tracking"];
            $business->facebook_api_key = $_POST["facebook_api_key"];
            $business->facebook_secret_key = $_POST["facebook_secret_key"];
            if ($this->input->post("open_facebook_register_in_new_tab")) {
                $business->open_facebook_register_in_new_tab = 1;
            } else {
                $business->open_facebook_register_in_new_tab = 0;
            }

            $business->hq_location_id = $_POST["hq_location_id"];
            $business->active = $_POST['active'];
            
            $business->save();
            set_flash_message('success', "Updated {$business->companyName} Settings");

            $this->goBack("/admin/admin/edit");
        }
        
        $this->load->model('location_model');
        $content['locations'] = $this->location_model->get_all_as_codeigniter_dropdown_for_business($business->businessID);

        $content['locales'] = $this->config->item('locales');
        $content['dateFormats'] = $this->config->item('dateFormats');

        /* Tables colors */
        $content['header_bgcolor']       = get_business_meta($this->business_id, 'header_bgcolor', '#6699cc');
        $content['header_color']         = get_business_meta($this->business_id, 'header_color', 'white');
        $content['row_bgcolor']          = get_business_meta($this->business_id, 'row_bgcolor', '#eaeaea');
        $content['row_color']            = get_business_meta($this->business_id, 'row_color', 'black');
        $content['row_alt_bgcolor']      = get_business_meta($this->business_id, 'row_alt_bgcolor', '#eaeaea');
        $content['row_alt_color']        = get_business_meta($this->business_id, 'row_alt_color', 'black');

        /*Bitly config*/
        $content['bitly_client_id'] = get_business_meta($this->business_id, 'bitly_client_id');
        $content['bitly_client_secret'] = get_business_meta($this->business_id, 'bitly_client_secret');
        $content['bitly_access_token'] = get_business_meta($this->business_id, 'bitly_access_token');

        //automatic claim deactivation setting
        $content['claim_deactivation'] = get_business_meta($this->business_id, 'claim_deactivation','1');

        //money symbol setting
        $content['money_symbol_str'] = get_business_meta($this->business_id, 'money_symbol_str','');
        $content['money_symbol_position'] = get_business_meta($this->business_id, 'money_symbol_position','1');

        $content['decimal_positions'] = get_business_meta($this->business_id, 'decimal_positions','2');

        $content['inmediate_order_capture'] = get_business_meta($this->business_id, 'inmediate_order_capture','0');

        $this->template->add_js('js/underscore.js');

        $this->renderAdmin('edit', $content);
    }
    
    /**
     * If not a post request, this action renders the interface for managing the payment processor for the current business
     * Can take the following POST parameters
     */
    public function processor($processor = null)
    {
        // This is used to get the proper form since all processors require different settings
        if ($this->input->post("processor_submit")) {
            update_business_meta($this->business_id, 'processor', $_POST['processor']);
            update_business_meta($this->business_id, 'paypal_button', $_POST['paypal_button']);
            update_business_meta($this->business_id, 'bitcoin_button', $_POST['bitcoin_button']);
            redirect_with_fallback(("admin/admin/processor"));
        }

        if ($this->input->post("submit")) {
            if (!in_bizzie_mode() || is_superadmin()) {
                unset($_POST['submit']);

                foreach ($_POST as $key => $value) {
                    update_business_meta($this->business_id, $key, $value);
                }
                set_flash_message("success", "Settings have been updated");
            } else {
                set_flash_message("error", "Only the superadmin can update the payment processor properties when the server is in Bizzie mode");
            }
            redirect_with_fallback(("admin/admin/processor"));
        }

        $accepted_cards = get_business_meta($this->business_id, 'acceptedCards');
        $content['accepted_cards'] = $accepted_cards ? unserialize($accepted_cards) : array();

        $content['current_processor'] = get_business_meta($this->business_id, 'processor');
        $content['processor'] = $processor ? $processor : $content['current_processor'];

        $this->load->library('creditcardprocessor');
        $content['processors_list'] = $this->creditcardprocessor->getProcessorsList();

        $this->renderAdmin('processor', $content);
    }

    //this will update what creditcards the business is accepting
    public function accepted_cards()
    {
        if ( !empty( $_POST ) ) {
            $accepted_cards = $this->input->post('cards');
            $cards_data = serialize($accepted_cards);
            update_business_meta($this->business_id,'acceptedCards',$cards_data);

        } else {
            update_business_meta($this->business_id,'acceptedCards','');
        }
        redirect('/admin/admin/processor');
    }

    //TODO: move into locations controller
    // performs validation for adding a location discount
    // unique for location?multiple discounts for a single location?? probably not
    private function validate_location_discount()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('expiration', 'Expiration', 'required|trim');
        $this->form_validation->set_rules('description', 'Description', 'required|trim');
        $this->form_validation->set_rules('percentage', 'Percentage', 'trim|is_numeric');
        $this->form_validation->set_rules('dollar_amount', 'Dollar Amount', 'trim|is_numeric');
        return $this->form_validation->run();
    }

    //TODO: move into locations controller
    // add a discount to the specific location
    public function add_location_discount($location_id = null)
    {
        if (! empty($_POST)) {

            $location_discount_id = '';
            if (empty($location_id) || ! is_numeric($location_id)) {
                throw new \InvalidArgumentException("Invalid 'location_id'");
            }

            $data = array();
            $discount_update = array();

            if ($this->validate_location_discount()) { // check form validation
                $data['percent'] = $this->input->post('percentage');
                $data['dollar_amount'] = $this->input->post('dollar_amount');
                $data['percent'] = empty($data['dollar_amount'])?$data['percent']:"";
                $data['description'] = $this->input->post('description');
                $data['location_id'] = $location_id;

                $expire_date = $this->input->post('expiration');
                $data['expire_date'] = date('Y-m-d H:i:s', strtotime($expire_date));

                if (! $combine = $this->input->post('combine')) {
                    $data['combine'] = 0;
                } else {
                    $data['combine'] = 1;
                }

                if (! $only_apply_if_minimum_is_not_met = $this->input->post('only_apply_if_minimum_is_not_met')) {
                    $data['only_apply_if_minimum_is_not_met'] = 0;
                } else {
                    $data['only_apply_if_minimum_is_not_met'] = 1;
                }

                $locationDiscount = current(Location_Discount::search_aprax(compact('location_id')));
                if (empty($locationDiscount)) {
                    $locationDiscount = new Location_Discount();
                }

                $locationDiscount->setProperties($data);
                $locationDiscount->save();

                set_flash_message("success", "Location discount updated for location_id: " . $location_id);
            } else {
                $errors = validation_errors();
                set_flash_message("error", $errors);
            }
        }

        redirect("/admin/admin/location_pricing/" . $location_id);
    }

    /**
     * The following function deletes a product location price
     */
    //TODO: move into locations controller
    public function delete_location_discount($location_discount_id = null)
    {
        if (empty($location_discount_id) || ! is_numeric($location_discount_id)) {
            throw new \InvalidArgumentException("Invalid 'location_discount_id'");
        }

        $locationDiscount = new Location_Discount($location_discount_id);

        if ($locationDiscount->delete()) {
            set_flash_message("success", "Discount Deleted");
        } else {
            set_flash_message("error", "Discount Not Deleted");
        }

        redirect("/admin/admin/location_pricing/" . $locationDiscount->location_id);
    }

    //TODO: move into locations controller
    public function location_pricing($location_id = null)
    {
        $content['jsonProducts'] = "'x'";
        if ($location_id) {
            // need to pull location discount - via location_discount table
            $discount_sql = $this->db->get_where('location_discount', array(
                'location_id' => $location_id
            ));
            $content['location_discount'] = $discount_sql->row();

            // get location to display address
            $sqlGetLocation = "select locationID, address
                from location
                where locationID = $location_id";
            $query = $this->db->query($sqlGetLocation);
            $content['location'] = $location = $query->result();

            // get a list of all products
            $sqlAllProducts = "Select product_processID, product.name as prodName, process.name as procName
                from product_process
                join product on productID = product_id
                left join process on processID = process_id
                where product.business_id = $this->business_id
                order by product.name";
            $query = $this->db->query($sqlAllProducts);
            $content['products'] = $products = $query->result();

            // get location pricing
            $sqlGetLocPricing = "SELECT p.name, productID, pl.product_location_priceID, product_processID, pl.price as locPrice, pp.price as listPrice, pr.name as process_name, displayName, pl.updated
                FROM product_location_price pl
                JOIN product_process pp ON pp.product_processID = pl.product_process_id
                LEFT JOIN process pr ON processID = pp.process_id
                JOIN product p ON productID = pp.product_id
                WHERE location_id = $location_id
                ORDER BY p.name";
            $query = $this->db->query($sqlGetLocPricing);
            $content['pricing'] = $products = $query->result();
        }

        // pull location_discount for this
        if ($this->input->post("submit")) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("location_id", "Location ID", "required|is_natural_no_zero");

            if ($this->form_validation->run()) {
                $location_id = $this->input->post("location_id");
                redirect("/admin/admin/location_pricing/$location_id");
            } else {
                set_flash_message("error", validation_errors());

                $this->goBack("/admin/admin/location_pricing");
            }
        }

        if (empty($location_id)) {
            $this->load->model("product_location_price_model");
            $locations = $this->product_location_price_model->locationsWithPricing();
            $content['locations'] = $locations;
        }
        
        $this->renderAdmin('location_pricing', $content);
    }

    //TODO: move into locations controller
    public function add_product_location_price()
    {
        if ($_POST) {
            // insert location pricing
            $this->load->model("product_location_price_model");
            $this->product_location_price_model->location_id = $this->input->post('location');
            $this->product_location_price_model->business_id = $this->business_id;
            $this->product_location_price_model->product_process_id = $this->input->post('product_processID');
            $this->product_location_price_model->price = $this->input->post('price');

            $ret = $this->product_location_price_model->insert();
            if ($this->db->_error_number()) {
                set_flash_message("error", "Pricing is already setup for this item!");
            } else {
                if ($ret) {
                    set_flash_message("success", "New pricing added");
                } else {
                    set_flash_message("error", "Error adding pricing");
                }
            }
            redirect("/admin/admin/location_pricing/" . $_POST['location']);
        }
    }

    /**
     * The following function deletes a product location price
     */
    //TODO: move into locations controller
    public function delete_product_location_price($pricing_id = null, $location_id = null)
    {
        // delete location pricing
        $this->load->model("product_location_price_model");
        $this->product_location_price_model->product_location_priceID = $pricing_id;
        $this->product_location_price_model->business_id = $this->business_id;
        $ret = $this->product_location_price_model->delete();
        if ($ret) {
            set_flash_message("success", "Item Deleted");
        } else {
            set_flash_message("error", "Item Not Deleted");
        }

        $this->goBack("/admin/admin/location_pricing/" . $location_id);
    }


    public function print_bag_tag($customerID = null, $bag = null, $phone = null, $with_default_location = null)
    {
        $customer = new Customer($customerID);
        $content['customer'] = getPersonName($customer);
        $content['bag'] = $bag;
        $content['phone'] = $phone;

        //for translation
        $content['customerId'] = $customer->customerID;

        update_customer_meta($customer->customerID, "assign_new_bag_on_new_order", "");

        if($with_default_location){
            //add default location
            $get_default_location_query = $this->db->get_where("location", array("locationID" => $customer->location_id));
            $default_location_row = $get_default_location_query->row();
            $default_location= $default_location_row->address;
            $content['default_location'] = $default_location;
        }

        if (empty($bag) || !is_numeric($bag)) {
            set_flash_message('error', "Invalid bag number '$bag'");
            $this->goBack("/admin/orders/bag_tags?bagNumber=" . urlencode($bag));
        }

        //make sure this bag is assigned to this company
        $getCustomer = "select * from  bag
        join customer on customerID = customer_id
        where bagNumber = ?
        and customer.business_id = $this->business_id";
        $query = $this->db->query($getCustomer, array($bag));
        $customer = $query->result();

        if ($customer) {
            //see if there are notes on this bag, if so, print them
            $sqlNotes = "select * from bag where bagNumber = $bag";
            $query = $this->db->query($sqlNotes);
            $content['notes'] = $query->result();

            //update the db to note that this bag has been printed
            $update1 = "update bag set printed = 1 where bagNumber = $bag";
            $update = $this->db->query($update1);
        } else {
            $content['notice'] = 'This bag is not associated to a customer that is registered with your business';
        }
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

        $this->load->view('print/bag_tag', $content);
    }

    /**
     * The following action retrieves the data and renders the display for all locations associated with a business.
     */
    //TODO: move into locations controller
    public function view_all_locations($show_deleted = 0)
    {
        $searchLocationAddress = $this->input->post("locationAddress");

        $this->template->write('page_title','View Locations', true);
        $this->load->model('location_model');

        $this->db->join('locationType', 'locationType_id=locationTypeID', 'LEFT');
        if (!$show_deleted) {
            $this->db->where("status", 'active');
        } else {
            $this->db->where("status", 'deleted');
        }
        $this->db->where('business_id', $this->business->businessID);

        if (!empty($searchLocationAddress)) {
            $this->db->like("CONCAT(locationID, ' ', address, ' ', address2, ' ', companyName, ' ', city, ' ', zipcode, ' ', location, ' ', serviceType)", $searchLocationAddress);
        }

        $content['locations'] = $this->db->get('location')->result();

        $get_pickup_location_serviceDays_query = "SELECT location_id,day FROM location_serviceDay INNER JOIN location ON location_id=locationID WHERE business_id=? AND location_serviceType_id=2;";
        $get_pickup_location_serviceDays_query_result = $this->db->query($get_pickup_location_serviceDays_query, array($this->business_id))->result();

        $content['pickup_location_serviceDays'] = array();
        foreach ($get_pickup_location_serviceDays_query_result as $get_pickup_location_serviceDay_query_result) {
            $content['pickup_location_serviceDays'][$get_pickup_location_serviceDay_query_result->location_id][] = $get_pickup_location_serviceDay_query_result->day;
        }
        $get_dropoff_location_serviceDays_query = "SELECT location_id,day FROM location_serviceDay INNER JOIN location ON location_id=locationID WHERE business_id = ? AND location_serviceType_id=1";
        $dropoff_location_serviceDays_query_result = $this->db->query($get_dropoff_location_serviceDays_query, array($this->business_id))->result();

        $content['dropoff_location_serviceDays'] = array();
        foreach ($dropoff_location_serviceDays_query_result as $dropoff_location_serviceDay_query_result) {
            $content['dropoff_location_serviceDays'][$dropoff_location_serviceDay_query_result->location_id][] = $dropoff_location_serviceDay_query_result->day;
        }
        $get_location_lockers_total_query_result = $this->db->query("SELECT locationID, COUNT(lockerID) AS total FROM location LEFT JOIN locker ON location_id=locationID AND lockerStatus_id = 1 GROUP BY locationID")->result();
        $location_lockers_total = array();
        foreach ($get_location_lockers_total_query_result as $get_location_locker_total_query_result) {
            $location_lockers_total[$get_location_locker_total_query_result->locationID] = $get_location_locker_total_query_result->total;
        }

        foreach ($content['locations'] as $location) {
            $location->type = $location->name ." (". $location_lockers_total[$location->locationID] .")";
        }

        $content['show_deleted'] = $show_deleted;

        $this->data['content'] = $this->load->view("admin/admin/view_all_locations", $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * The following action is used for performing a multiple of search types, or simply just rendering the search interface view.
     * If the 'search_type' parameter is set, then a 'result' array is passed to a view
     * This action expects one of the following 'search_type' parameters passed as a POST request:
     *      barcode
     *      order
     *      locker
     *      customer
     * This action may take the following POST parameters that specifices the location of the view to render.
     *  view : the view to render
     *  layout: the layout to render
     */
    public function search()
    {
        $content = array();
        //The following conditional handles the barcode search.
        if ($this->input->post('search_type') == "barcode") {
            $this->load->model('barcode_model');

            $results = $this->barcode_model->get_order_item_from_barcode($this->input->post('barcode_id'));
            foreach ($results as $index => $result) {
                $content['results'][$index] = array(
                    'Barcode' => '<a href="/admin/items/view/?itemID='.$result->itemID.'" target="_blank">'.$result->barcode.'</a>',
                    'Customer Name' => '<a href="/admin/customers/detail/'.$result->customerID.'" target="_blank">'.getPersonName($result).'</a>',
                    'Product' => $result->displayName,
                    'Order' => '<a href="/admin/orders/details/'.$result->order_id.'" target="_blank">'.$result->order_id.'</a>',
                    'Price' => $result->unitPrice,
                    'Order Created' => convert_from_gmt_aprax($result->order_date, "m/d/y g:ia"),
                    'Order Status' => $result->order_status,
                    "Bag Number" => $result->bagNumber
                );

            }
        }
        //The following conditional handles the order search.
        else if ($this->input->post('search_type') == "order") {
            $this->load->model('order_model');
            if ($this->input->post("bag_id")) {
                $bag_id = (int) $this->input->post("bag_id");
            } else {
                $bag_id = null;
            }
            if ($this->input->post("order_id")) {
                $order_id = (int) $this->input->post("order_id");
            } else {
                $order_id = null;
            }
            if ($this->input->post("ticketNum")) {
                $ticketNum = (int) $this->input->post("ticketNum");
            } else {
                $ticketNum = null;
            }
            if ($this->input->post("customer_name")) {
                $customer_name = trim($this->input->post("customer_name"));
            } else {
                $customer_name = null;
            }
            if ($this->input->post("dueDate")) {
                $dueDate = $this->input->post("dueDate");
            } else {
                $dueDate = null;
            }
            if ($this->input->post("brand")) {
                $brand = $this->input->post("brand");
            } else {
                $brand = null;
            }

            $dateFormat = get_business_meta($this->business_id, "shortWDDateLocaleFormat",'%a %m/%d');

            $results = $this->order_model->getOrder($bag_id, $order_id, $ticketNum, $customer_name, $dueDate, $brand);

            foreach ($results as $index => $result) {
                $data = array(
                    'bag #' => $result->bagNumber,
                    'customer' => "<a href='/admin/customers/detail/{$result->customerID}' target='_blank'>{$result->customerName} </a>",
                    'order' => "<a href='/admin/orders/details/{$result->orderID}' target='_blank'> {$result->orderID} </a>",
                    'location' => $result->location,
                    'status' => $result->orderStatus,
                    'payment' => $result->orderPaymentStatus,
                    'ticket #' => $result->ticketNum,
                    'Due Date' => empty($result->dueDate)?"-":strftime($dateFormat, strtotime($result->dueDate)),
                    'more' => "<a href='/admin/reports/metalprogetti_history/{$result->orderID}' target='_blank'>Assembly</a>"
                );
                
                if (!empty($brand)) {
                    $items = $this->order_model->getItems($result->orderID, array("manufacturer" => $brand));
                    foreach($items as $i) {
                        $data['Items'] .= '
                            ['.strtolower($i->manufacturer).'] 
                             <a href="/admin/orders/edit_item?orderID='.$result->orderID.'&itemID='.$i->itemID.'" target="_blank">
                                    '.$i->displayName.'
                            </a><br/>';
                    }
                }

                $content['results'][$index] = $data;
            }
        }
        //THe following conditional handles the locker search.
        else if ($this->input->post('search_type') == "locker" or $this->input->get('search_type') == "locker" ) {
            if ($this->input->post("lockerName")) {
                $lockerName = $this->input->post("lockerName");
            } else {
                $lockerName = $this->input->get("lockerName");
            }
            $this->db->select("lockerID");
            $this->db->join("location", "location.locationID=locker.location_id");
            $this->db->like("lockerName", $lockerName);

            $get_lockers_query = $this->db->get_where("locker", array("location.business_id" => $this->business_id));

            if ($this->db->_error_message()) {
                throw new Exception($this->db->_error_message());
            }

            $lockers = $get_lockers_query->result();
            foreach ($lockers as $locker) {
                $locker = new Locker($locker->lockerID, true);
                $content['results'][] = array(
                    "Street" => $locker->relationships['Location'][0]->address,
                    "Locker" => '<a href="/admin/reports/locker_history/'.$locker->lockerID.'" target="_blank">'.$locker->lockerName.'</a>',
                    "Type" => $locker->relationships['LockerLockType'][0]->name,
                    "Service Type" => $locker->relationships['Location'][0]->serviceType,
                    "Service Method" => $locker->relationships['LockerStyle'][0]->name,
                    "Status" => $locker->relationships['LockerStatus'][0]->name
                );
            }
        }
        //The following conditional handles the customer search.
        else if ($this->input->post('search_type') == "customer") {
            $this->load->model('customer_model');

            $search_options = array('business_id' => $this->business_id);

            if ($this->input->post('firstName')) {
                $search_options['firstName'] = $this->input->post('firstName');
            }

            if ($this->input->post('lastName')) {
                $search_options['lastName'] = $this->input->post('lastName');
            }

            if ($this->input->post('address')) {
                $search_options['address'] = $this->input->post('address');
            }

            if ($this->input->post('phone')) {
                $search_options['phone_or_sms'] = $this->input->post('phone');
            }

            if ($this->input->post('email')) {
                $search_options['email'] = $this->input->post('email');
            }

            if ($this->input->post('customerNotes')) {
                $search_options['customerNotes'] = $this->input->post('customerNotes');
            }

            if ($this->input->post('customerID')) {
                $search_options['customerID'] = $this->input->post('customerID');
            }

            $results = $this->customer_model->search_customer($search_options);

            foreach ($results as $index => $result) {
                $this->load->model("order_model");
                $orders_count = $this->order_model->count(array("customer_id" => $result->customerID));
                //The following conditional determines whether or not to show the delete button. The delete button should only be shown if the customer has no orders.
                if ($orders_count > 0) {
                    $delete = "";
                } else {

                    if (in_bizzie_mode()) {
                        $delete = "<a class='delete' href='/admin/customers/disable/{$result->customerID}'><img src='/images/icons/cross.png' alt='Disable Customer' /></a>";
                    } else {
                        $delete = "<a class='delete' href='/admin/customers/delete/{$result->customerID}'><img src='/images/icons/cross.png' alt='Delete Customer' /></a>";
                    }
                }

                !empty($_POST['customerID']) &&  $content['results'][$index]['Customer ID'] = $result->customerID;
                $content['results'][$index]['Name'] = getPersonName($result) . " (<a href='/admin/customers/detail/{$result->customerID}'>view</a>)";
                $content['results'][$index]['Address'] = $result->address1 . " " . $result->address2;
                $content['results'][$index]['Phone'] = $result->phone;
                $content['results'][$index]['Email'] = $result->email;
                !empty($_POST['customerNotes']) &&  $content['results'][$index]['Notes'] = $result->customerNotes;
                $content['results'][$index]['Login'] = "<a target='_blank' href='/admin/customers/switch_to_customer/{$result->customerID}'><img src='/images/icons/user_go.png' alt='Login as Customer' /></a>";
                $content['results'][$index]['Delete?'] = $delete;
            }
        } elseif ($this->input->post("search_type") == "bag") {
            if (!$this->input->post("bagNumber")) {
                set_flash_message("error", "You must specify a bagnumber.");
            } else {
                $bags = Bag::search_aprax(array("bagNumber" => $this->input->post("bagNumber"), "business_id" => $this->business_id), true);

                foreach ($bags as $bag) {

                    $content['results'][] = array(
                        "Bag Number" => $bag->bagNumber,
                        "Customer Name" => "<a href='/admin/customers/detail/{$bag->relationships['Customer'][0]->customerID}'>" . getPersonName($bag->relationships['Customer'][0]) . " </a>"
                    );
                }
            }

        }

        $this->load->helper('form');
        $this->template->add_js("/js/jquery.validate-1.9.js", "source");
        if ($this->input->post('view')) {
            $view = $this->input->post('view');
        } else {
            $view = "admin/admin/search"; //This is the default view to render
        }
        if ($this->input->post('layout')) {
            $layout = $this->input->post('layout');
        } else {
            $layout = 'inc/full_column';
        }

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

        $this->data['content'] = $this->load->view($view, $content, true);
        $this->template->write_view('content', $layout, $this->data);
        $this->template->render();
    }

    public function email_macros($type = null)
    {
        $macros = $this->config->item('macros');
        $content['dd_macros'] = $macros;
        $content['macros'] = $macros[$type];
        $content['type'] = $type;

        $this->load->view('admin/admin/email_macros', $content);
    }

    /**
     * UI for business owners to download phone APK
     */
    public function mobile()
    {
        $content = array();
        $this->renderAdmin('mobile', $content);
    }

    public function shoe_tags()
    {
        $this->template->write('page_title','Print Shoe Tags', true);
        if (!empty($_GET['barcode'])) {
            $this->load->view('admin/admin/print_shoe_tags');
            return;
        }

        $content = array();
        $this->renderAdmin('shoe_tags', $content);
    }

    public function reporting_parameters()
    {
        $content['business'] = new Business($this->business_id);
        $this->renderAdmin('reporting_parameters', $content);
    }

    public function timecard_setup()
    {
        //set to off by default
        $ret = get_business_meta($this->business_id, 'timecard_onOff');
        if (!$ret) { update_business_meta($this->business_id, 'timecard_onOff', '0'); }
        if ($_POST) { update_business_meta($this->business_id, 'timecard_onOff', $_POST['onOff']); }
        $content['onOff'] = get_business_meta($this->business_id, 'timecard_onOff');

        $ret = get_business_meta($this->business_id, 'timecard_overtime');
        if (!$ret) { update_business_meta($this->business_id, 'timecard_overtime', '0'); }
        if ($_POST) { 
            update_business_meta($this->business_id, 'timecard_overtime', $_POST['overtime']);
            update_business_meta($this->business_id, 'timecards_start_of_week', $this->input->post("timecards_start_of_week")); 
        }
        $content['overtime'] = get_business_meta($this->business_id, 'timecard_overtime');

        $content['timecards_start_of_week_options'] = array(
            'Sun' => get_translation('Sunday', 'admin/admin/defaults', array(), "Sunday"),
            'Mon' => get_translation('Monday', 'admin/admin/defaults', array(), "Monday"),
            'Tue' => get_translation('Tuesday', 'admin/admin/defaults', array(), "Tuesday"),
            'Wed' => get_translation('Wednesday', 'admin/admin/defaults', array(), "Wednesday"),
            'Thu' => get_translation('Thursday', 'admin/admin/defaults', array(), "Thursday"),
            'Fri' => get_translation('Friday', 'admin/admin/defaults', array(), "Friday"),
            'Sat' => get_translation('Saturday', 'admin/admin/defaults', array(), "Saturday")
        );

        $this->renderAdmin('timecard_setup', $content);
    }

    /**
     * The following action Renders the interface for managing barcode lengths.
     */
    public function manage_barcodes()
    {
        $this->load->model("barcodelength_model");
        $content['barcode_lengths'] = $this->barcodelength_model->get_all($this->business_id);
        $this->renderAdmin('manage_barcodes', $content);
    }

    /**
     * Ui for managing the sending of emails
     */
    public function email_admin()
    {
        // get the setting to state whether we are sending all pending emails or only emails that are older than the current server time
        $sendAllEmails = \App\Libraries\DroplockerObjects\Settings::getSendAllEmails();
        $content['sendAllEmails'] = ($sendAllEmails == 1) ? "ON <a class='button blue' href='/admin/superadmin/toggleSend/0'>Turn OFF</a>" : "OFF  <a class='button red' href='/admin/superadmin/toggleSend/1'>Turn ON</a>";

        $content['emails'] = $emails = \App\Libraries\DroplockerObjects\DropLockerEmail::getAllPendingEmails($this->business_id);
        $content['totalEmails'] = sizeof($emails);

        if (sizeof($emails) >= 40) {
            $content['class'] = "emailWarning";
        }

        if (sizeof($emails) >= 50) {
            $content['class'] = "emailError";
        }

        $content['emailLimit'] = \App\Libraries\DroplockerObjects\Settings::getEmailLimit();

        // used for redirect
        $this->session->set_flashdata('referral', $this->uri->uri_string());
        $this->renderAdmin('admin/superadmin/email_admin', $content);
    }

    /**
     * Manually sends emails from the email_admin page.
     *
     * There is no view for this function
     */
    public function manualProcess()
    {
        \App\Libraries\DroplockerObjects\Settings::setManualEmailProcessing(1);
        $emailObj = new DropLockerEmail();

        if ((isset($_POST['submit'])) && sizeof($_POST['emails'] > 1)) {

            $emails = $_POST['emails'];
        } elseif (isset($_POST['submitAll'])) {

            $business_id = (is_numeric($_POST['business_id'])) ? $_POST['business_id'] : null;
            $limit = \App\Libraries\DroplockerObjects\Settings::getEmailLimit();
            $emails = $emailObj->getAllPendingEmails($business_id, $limit);
        } else {

            redirect($this->session->flashdata('referral'));
        }

        $content['results'] = $emailObj->manualProcessEmails($_POST['emails']);

        \App\Libraries\DroplockerObjects\Settings::setManualEmailProcessing(0);

        $this->data['content'] = $this->load->view('admin/admin/manual_email_report', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * This method gets called with ajax and updates the acl table
     *
     * $_POST Values
     * -------------------
     * business_id
     * type_id
     * resource_id
     * type
     *
     * This echo's the results because it's ajax
     */
    public function update_role_ajax()
    {
        try {
            $this->load->model('aclroles_model');
            $resource_id = $this->input->post("resource_id");
            $type = $this->input->post("type");
            $type_id = $this->input->post("type_id");
            //The following conditional updates the all businesses' ACL roles if the server is in Bizzie mode
            if (in_bizzie_mode()) {
                $this->load->model("business_model");
                $master_business = get_master_business();

                echo $this->aclroles_model->update_acl(array(
                    "business_id" => $master_business->businessID,
                    "type_id" => $type_id,
                    "type" => $type,
                    "resource_id" => $resource_id));

                $master_acls = Acl::search_aprax(array("business_id" => $master_business->{Business::$primary_key},
                "type_id" => $type_id,
                "type" => $type,
                "resource_id" => $resource_id));
                $master_acl = $master_acls[0];

                $slave_businesses = $this->business_model->get_all_slave_businesses();
                foreach ($slave_businesses as $slave_business) {
                    $master_aclresource = new AclResource($resource_id);
                    $slave_aclresource = $master_aclresource; // aclresources are now unique for all business


                    $master_aclrole = new \App\Libraries\DroplockerObjects\AclRoles($type_id);

                    $slave_aclroles = AclRoles::search_aprax(array("name" => $master_aclrole->name, "business_id" => $slave_business->{Business::$primary_key}));

                    if (empty($slave_aclroles)) {
                        $slave_aclrole = $master_aclrole->copy();
                        $slave_aclrole->business_id = $slave_business->{Business::$primary_key};
                        $slave_aclrole->save();
                    } else {
                        $slave_aclrole = $slave_aclroles[0];
                    }

                    $slave_acls = Acl::search_aprax(array(
                        "business_id" => $slave_business->{Business::$primary_key},
                        "type_id" => $slave_aclrole->{AclRoles::$primary_key},
                        "type" => "role",
                        "resource_id" => $slave_aclresource->{AclResource::$primary_key}
                    ));
                    if (empty($slave_acls)) {
                        $slave_acl = $master_acl->copy();
                    } else {
                        $slave_acl = $slave_acls[0];
                    }
                    $slave_acl->type_id = $slave_aclrole->{AclRoles::$primary_key};
                    $slave_acl->business_id = $slave_business->{Business::$primary_key};
                    $slave_acl->resource_id = $slave_aclresource->{AclResource::$primary_key};
                    $slave_acl->action = $master_acl->action;
                    $slave_acl->save();
                }
            } else {
                echo $this->aclroles_model->update_acl(array(
                    "business_id" => $this->business_id,
                    "type_id" => $this->input->post("type_id"),
                    "type" => $this->input->post("type"),
                    "resource_id" => $this->input->post("resource_id")));
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            send_exception_report($e);
        }
    }

    public function print_temp_tags()
    {
        redirect('/admin/orders/bag_tags');
    }

    /**
     * UI for display of active and inactive coupons
     *
     * Also allows user to add a new coupon
     */
    public function manage_coupons()
    {
        $this->load->model('coupon_model');
        $content['active_coupons'] = $this->coupon_model->get_active_coupons($this->business_id);
        $content['inactive_coupons'] = $this->coupon_model->get_inactive_coupons($this->business_id);

        $content['bizzie_superadmin'] = false;
        $content['initialSearch'] = $this->input->get('search');
        $content['business'] = $this->business;

        //get the products for the dropdown with amountType = item
        $options['order_by'] = "displayName";
        $options['group_by'] = "displayName";
        $options['business_id'] = $this->business_id;
        $options['select'] = 'product.displayName, productID';
        $this->load->model("product_model");
        $products = $this->product_model->get($options);
        $content['products'] = $products;

        //get the processes for the dropdown with amountType = item and product dry cleaning
        $this->load->model("process_model");
        $content['processes'] = $this->process_model->get_as_codeigniter_dropdown();
        $content['processes'][0] = get_translation('label_process_all', 'Processes', array(), "All");

        $content['frequencies'] = Coupon::get_frequencies();
        $content['amountTypes'] = Coupon::get_amountTypes();
        $content['first_time_fields'] = Coupon::get_first_time_fields();
        $content['recurring_types'] = Coupon::get_recurring_types();

        $this->setPageTitle('page_title', 'Manage Coupons');
        $this->renderAdmin("/admin/coupon/manage", $content);
    }

    /**
     * TODO: this should be in other controller
     * Takes an array of orderItemID and sets the order to out for partial delivery and prints travellers.
     */
    public function process_partials()
    {
        if ($this->input->post("partial")) {
            $this->load->model('orderstatus_model');
            $this->load->model('order_model');

            $orderItems = implode(",", $this->input->post("partial"));
            //get an array or orders and barcodes based on the orderItemId
            $getItems = "select orderItem.*, (select barcode from barcode where orderItem.item_id = barcode.item_id limit 1) as barcode, product.*
            from orderItem
            join product_process on product_processID = product_process_id
            join product on productID = product_id
            where orderItemID in ($orderItems)";
            $q = $this->db->query($getItems);
            $items = $q->result();

            foreach ($items as $item) {
                $orderArray[$item->order_id][$item->barcode] = $item;
            }

            foreach ($orderArray as $order) {
                $message = "The following items are being held for extra service:<br>";
                $qty = 0;
                foreach ($order as $orderItem) {
                    //set the message in the order history to a description of the missing items
                    $message .= "<a href='/admin/items/view/?itemID=" . $orderItem->item_id . "'>" . $orderItem->barcode . "</a> - " . $orderItem->displayName . "<br>";
                    $qty ++;
                }

                //set order to out for Partial Delivery and put note in history
                $orderTemp = $this->order_model->get(array("orderID" => $orderItem->order_id, "business_id" => $this->business_id));
                //Note, order status option 12 is expected to be "Out for partial delivery".
                $this->orderstatus_model->update_orderStatus(array(
                    "order_id" => $orderItem->order_id,
                    "orderStatusOption_id" => 12,
                    "locker_id" => $orderTemp[0]->locker_id,
                    "employee_id" => $this->employee_id,
                    "note" => $message
                ));

                $request['order'] = $orderItem->order_id;
                $request['routes'] = '';
                $request['barcode'] = '';
                $request['showYesterday'] = '';
                $request['short'] = 0;
                $request['dc'] = '';
                $request['orderDate'] = '';
                $request['partial'] = $qty;
                $request['item'] = '';
                $request['reason'] = $message;

                $data['request'] = $request;

                $settings = $this->business_model->get(array('businessID' => $this->business_id));
                $this->settings = $settings[0];

                $this->load->view('admin/print_receipts', $data);
            }
        } else {
            set_flash_message('error', "No orders selected to process partials");
            redirect("/admin/reports/supplier_manifest");
        }
    }

    /**
     * Check all required business setings are setup
     */
    public function diagnose()
    {
        $diagnose = array();

        //cc processors
        $sql = 'SELECT count(*) as total_processors FROM businessMeta WHERE `key` = ? AND business_id = ? ';
        $query = $this->db->query($sql, array('processor', $this->business_id));
        $processors = $query->result();
        if (intval($processors[0]->total_processors) == 0) {
           $diagnose[] = array('status' => 'error', 'message' => 'Credit Card Processor: You need to add a credit card processor', 'solution' =>  array('message' => 'Add a Processor', 'url' => '/admin/admin/processor'));
        } else {
           $diagnose[] = array('status' => 'success', 'message' => 'Credit Card Processor: Good, you have registered a credit card processor.');
        }


        //service types
        $serviceTypes = unserialize($this->business->serviceTypes);
        if (!is_array($serviceTypes)) {
            $diagnose[] = array('status' => 'error', 'message' => "Service Type: You need to setup business service types" , 'solution' =>  array('message' => 'Configure Service Types', 'url' => '/admin/admin/services'));
        } else {
            $diagnose[] = array('status' => 'success', 'message' => 'Service Type: You have setted up service types correctly.');
        }

        //locations and lockers
        $sql = 'SELECT count(*) as total_locations FROM location WHERE business_id = ? AND status = ?';
        $locations = $this->db->query($sql, array($this->business_id, 'active'))->result();

        if ($locations[0]->total_locations == 0) {
            $diagnose[] = array('status' => 'error', 'message' => "Locations and Lockers: You need to add a location and then a locker", 'solution' => array('message' => 'Add a location', 'url' => '/admin/locations/display_update_form'));
        } else {
            $diagnose[] = array('status' => 'success', 'message' => 'Locations: Good, you added a location for your business.');
            $sql        = 'SELECT count(*) as total_lockers FROM locker WHERE location_id IN (SELECT locationID FROM location WHERE business_id = ? AND status = ?)';
            $lockers    = $this->db->query($sql, array($this->business_id, 'active'))->result();

            if ($lockers[0]->total_lockers == 0) {
                $diagnose[] = array('status' => 'error', 'message' => "Lockers: You added a location, now you need to add a locker", 'solution' => array('message' => 'Add a locker', 'url' => '/admin/admin/view_all_locations'));
            } else {
                $diagnose[] = array('status' => 'success', 'message' => 'Lockers: Good, you added lockers.');

                //return to office locker
                $returnToOfficeLocker = Locker::search(array('lockerID' => $this->business->returnToOfficeLockerId));
                if (empty($returnToOfficeLocker)) {
                    $diagnose[] = array('status' => 'error', 'message' => "Return To Office Locker: You need to set a Return To Office Locker", 'solution' => array('message' => 'Add a Return To Office locker', 'url' => '/admin/admin/defaults'));
                } else {
                    $diagnose[] = array('status' => 'success', 'message' => 'Return To Office Locker: Good, you have setted up your Return To Office Locker.');
                }
            }
        }

        //
        $sql = 'SELECT count(*) as total_emails FROM businessMeta WHERE `key` = ? AND business_id = ? ';
        $query = $this->db->query($sql, array('customerSupport', $this->business_id));
        $supportEmail = $query->result();
        if (intval($supportEmail[0]->total_emails) == 0) {
           $diagnose[] = array('status' => 'error', 'message' => 'Customer Support Email: You need to add a default customer suppoert email', 'solution' =>  array('message' => 'Add Customer Support Email', 'url' => '/admin/admin/defaults'));
        } else {

            $sql = 'SELECT * FROM businessMeta WHERE `key` = ? AND business_id = ?';
            $supportEmail = $this->db->query($sql, array('customerSupport', $this->business_id))->result();

            if (!valid_email($supportEmail[0]->value)) {
                $diagnose[] = array('status' => 'error', 'message' => 'Customer Support Email: Your customer support email is invalid.' , 'solution' =>  array('message' => 'Correct Email', 'url' => '/admin/admin/defaults'));
            } else {
                $diagnose[] = array('status' => 'success', 'message' => 'Customer Support Email: Good, you have a valid customer support email.');
            }


        }

        $this->load->model("image_placement_model");
        $receipt_image = $this->image_placement_model->get_by_name("receipt", $this->business_id);
        if (!$receipt_image) {
           $diagnose[] = array('status' => 'error', 'message' => 'Receipt Image: Please, add a receipt image.' , 'solution' =>  array('message' => 'Add one image', 'url' => '/admin/website/edit_logo'));
        } else {
           $diagnose[] = array('status' => 'success', 'message' => 'Receipt: Good, you have a receipt image.');

        }

        if ($this->business->facebook_api_key == '') {
           $diagnose[] = array('status' => 'error', 'message' => 'Facebook Id: You do not have a facebook app key' , 'solution' =>  array('message' => 'Set up facebook app key', 'url' => '/admin/admin/edit'));
        } else {
           $diagnose[] = array('status' => 'success', 'message' => 'Receipt: Good, you have setted up a facebook app key.');
        }

        $sql = 'SELECT count(*) as total_cat FROM productCategory WHERE slug = ? AND business_id = ? ';
        $pc = $this->db->query($sql, array('wf', $this->business_id))->result();

        if ($pc[0]->total_cat > 1) {
            $diagnose[] = array('status' => 'error', 'message' => 'Wash and Fold Category: You have more than one wash and fold category.' , 'solution' =>  array('message' => 'Remove duplicates categories', 'url' => '/admin/admin/edit'));
        } else if ($pc[0]->total_cat == 1) {
            $diagnose[] = array('status' => 'success', 'message' => 'Wash and Fold Category: Good, you have one.');
        } else {
            $diagnose[] = array('status' => 'error', 'message' => "You need to add a Wash and Fold category with slug 'wf'." , 'solution' =>  array('message' => 'Add Wash and Fold category', 'url' => '/admin/admin/edit'));
        }

        $sql = 'SELECT count(*) as total_cat FROM productCategory WHERE slug = ? AND business_id = ? ';
        $pc = $this->db->query($sql, array('loads', $this->business_id))->result();

        if ($pc[0]->total_cat > 1) {
            $diagnose[] = array('status' => 'error', 'message' => 'Loads Category: You need a "loads" category for products.' , 'solution' =>  array('message' => 'Add category', 'url' => '/admin/admin/edit'));
        } else {
            $diagnose[] = array('status' => 'success', 'message' => 'Good, you have Loads category for products.');
        }

        $blankCustomer = Customer::search(array('customerID' => $this->business->blank_customer_id));
        if (empty($blankCustomer)) {
            $diagnose[] = array('status' => 'error', 'message' => 'Blank customer: You need to setup the blank customer in your business.' , 'solution' =>  array('message' => 'Add blank customer', 'url' => '/admin/admin/defaults'));
        } else {
            $diagnose[] = array('status' => 'success', 'message' => 'Blank customer is setup in your business.');
        }

        $PrepaymentLocker = Locker::search(array('lockerID' => $this->business->prepayment_locker_id));
        if (empty($PrepaymentLocker)) {
            $diagnose[] = array('status' => 'error', 'message' => 'Laundry Plan Locker: You need to setup the Laundry Plan Locker in your business.' , 'solution' =>  array('message' => 'Add blank customer', 'url' => '/admin/admin/defaults'));
        } else {
            $diagnose[] = array('status' => 'success', 'message' => 'Laundry Plan Locker is setup in your business.');
        }

        $maps_api_key = get_business_meta($this->business_id, 'maps_api_key');
        if (empty($maps_api_key)) {
            $diagnose[] = array('status' => 'error', 'message' => 'Map api key: You need to setup the Google Map api key for your business.' , 'solution' =>  array('message' => 'Add new Google Map api key', 'url' => '/admin/admin/defaults'));
        } else {
            $diagnose[] = array('status' => 'success', 'message' => 'Google Map Api Key is setup in your business.');
        }

        $this->renderAdmin('/admin/admin/diagnose', array('diagnose' => $diagnose));

    }

    /**
     * updates the default monthly invoices payment settings.
     * There is no view, this redirects back to defaults.
     *
     *  If there is no $_POST then redirect back to the defaults
     */
    public function update_monthly_invoices_defaults()
    {
        if (!isset($_POST['submit'])) {
            set_flash_message('error', 'No request data sent');
            redirect('admin/admin/defaults');
        }

        update_business_meta($this->business_id, 'droplocker_invoices_default_location', $this->input->post('droplocker_invoices_default_location'));
        update_business_meta($this->business_id, 'droplocker_invoices_default_product', $this->input->post('droplocker_invoices_default_product'));


        set_flash_message('success', 'Default monthly invoices settings updated');

        $this->goBack('/admin/admin/defaults');
    }
}
