<?php

/**
 * Description of upchargeGroups
 *
 * @author aprax
 */
class Upcharge_Groups extends MY_Admin_Controller
{
    public function get_total()
    {
        if ($this->input->get("business_id")) {
            try {
                $business_id = $this->input->get("business_id");
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($business_id);
                $query = "SELECT count(upchargeGroupID) AS total FROM upchargeGroup WHERE business_id=?";
                $result = $this->db->query($query, array($business_id))->row();
                output_ajax_success_response(array("total" => $result->total), "Total upcharge groups for '{$business->companyName} ({$business->businessID})");
            } catch (Exception $e) {
                send_exception_report($e);
                output_ajax_error_response("An internal error occurred attempting to retrieve the upcharge total");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter.");
        }
    }
}
