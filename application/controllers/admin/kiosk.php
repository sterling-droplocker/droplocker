<?php

class Kiosk extends CI_Controller
{
    public $business_id;

    public function __construct()
    {
        parent::__construct();

        $this->business_id = $this->session->userdata('business_id');

        $resource = $this->uri->segment(2)."_".$this->uri->segment(3);

        if (!$this->zacl->check_acl($resource)) {
            $this->load->model('menus/admin_menu');
            $this->load->library('template');
            $this->template->set_template('admin');
            $this->template->write('page_title','Business Admin');
            $this->template->write_view('menu', 'inc/admin_menu', array('menu'=>$this->admin_menu->menu));
            $this->data['submenu'] = $this->load->view('inc/admin_submenu', array("submenu"=>$this->admin_menu->get_submenu()), true);
            $this->data['content'] = '';

            $this->data['content'] = $this->load->view('admin/access_denied', $content, true);
            $this->template->write_view('content', 'inc/dual_column_left', $this->data);
            $this->template->render();
        }

    }


    public function index()
    {
        $request = $this->input->get();

        if (!empty($request['businessID'])) {
            $activeBus = (int)$request['businessID'];
        } else {
            $activeBus = 3;
        }

        if (!is_numeric($request['businessID'])) {
            die("Missing businessID, please check your settings");

        }

        if ($request['error']) {
                    queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, get_business_meta($activeBus, 'customerSupport'), $request['errorLoc'].': Kiosk Access Issue', urldecode(urldecode($request['error'])), array(), null, $activeBus);

                    return true;
        }

        //find out if an update to the card reader is necessary
        $numToUpdate = get_business_meta($activeBus ,'kioskLeftToUpdate');
        $newValue = $numToUpdate -1;
        if ($numToUpdate > 0 or $request['manual'] or $request['cards'] == 'all') {
            if ($request['admin']) {
                $select1 = "SELECT distinct customer_id, cardNumber, expYr, expMo
                FROM kioskAccess
                join customer on customerID = kioskAccess.customer_id
                where customer.type = 'employee'
                and customer.business_id = $activeBus
                order by cardNumber";
            } else {
                if ($request['cards'] == 'new') {
                    //only show cards from today
                    $today = date("m/d/y");
                    $getTimezone = "select timezone from business where businessID = $activeBus";
                    $query = $this->db->query($getTimezone);
                    $timezone = $query->result();

                    $select1 = "SELECT distinct customer_id, cardNumber, expYr, expMo
                                FROM kioskAccess
                                join customer on customerID = kioskAccess.customer_id
                                where date_format(convert_tz(updated,'GMT','".$timezone[0]->timezone."'), '%m/%d/%y') = '$today'
                                and customer.business_id = $activeBus
                                order by cardNumber";
                } elseif ($request['cards'] == 'all') {
                    //send all cards
                    $select1 = "SELECT distinct customer_id, cardNumber, expYr, expMo
                                FROM kioskAccess
                                join customer on customerID = kioskAccess.customer_id
                                where customer.business_id = $activeBus
                                order by cardNumber";
                } else {
                    //echo 'ERROR: Wrong paramaters sent';
                    return true;
                }
            }

            $query = $this->db->query($select1);
            foreach ($query->result() as $line) {
                    echo $line->cardNumber . $line->expYr . str_pad($line->expMo, 2, "0", STR_PAD_LEFT) . chr(13) . chr(10);
            }

            if (!$request['manual']) {
                //decrement the setting by 1 so that this data isn't pulled again until there is a new card.
               update_business_meta($activeBus, "kioskLeftToUpdate", $newValue);
            }
        } else {
            echo 'NO UPDATE';
        }


}

}
