<?php
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\Business;

class Coupons extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("coupon_model");
    }
    /**
     * Retrieves the total coupon count for a business
     * Expects the following GET parameter:
     *  business_id
     * @throws \InvalidArgumentException If 'business_id' is not passed
     * @throws \LogicException if the request is not ajax
     */
    public function get_total()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get('business_id')) {
                $business_id = $this->input->get('business_id');
                try {
                    $coupon_total = $this->coupon_model->get_count($business_id);
                    $result = array('status' => 'success', 'total' => $coupon_total);
                } catch (\Exception $e) {
                    $result = array('status' => 'error', 'message' => 'An error occurred attempting to retrieve the total coupons');
                    send_exception_report($e);
                }
                echo json_encode($result);
            } else {
                throw new \InvalidArgumentException("'busines_id' must be passed as a GET parameter.");
            }
        } else {
            throw new \LogicException("This action can only be called with an ajax request");
        }
    }
    /**
     * Updates the a Coupon's property
     * Expects the following POST parameters
     *  couponID
     *  property
     *  value
     * @return JSON
     *
     */
    public function update_property()
    {
        try {
            $this->load->library("form_validation");
            if ($this->form_validation->run("update_coupon_ajax")) {
                $couponID = trim($this->input->post("couponID"));

                $value = trim($_POST['value']);
                $property = trim($this->input->post("property"));

                //maxamount check - set the value to zero if we don't have a value
                if ( 'maxamt' == strtolower( $property ) || 'amount' == strtolower( $property ) ) {
                    $value = str_replace( ',', '.', $value );
                    if ( empty( $value ) ) {
                        $value = 0;
                    }
                }
                if ($property == "prefix") {
                    if (in_array($value, array("SR", "CR"))) {
                        output_ajax_error_response("Prefix can not be 'SR' or 'CR'");
                    }
                }
                $coupon = new Coupon($couponID);
                if ($property == "startDate" || $property == "endDate") {
                    //The following conditional handles the special case where the user can set an end date to 'NEVER', which should set the end date property to null in the database.
                    if ($property == "endDate" && ($value == "NEVER" || empty($value)) ) {
                        $coupon->$property = NULL;
                        $coupon->save();
                        output_ajax_success_response(array("property" => $property, "value" => $value), "Updated coupon $couponID property '$property' to '{$coupon->$property}'");
                    } else {
                        try {
                            $coupon->$property = new \DateTime($value, new DateTimeZone($this->business->timezone));
                        } catch (\Exception $e) {
                            output_ajax_error_response("Invalid date format");
                        }
                        $coupon->save();

                        output_ajax_success_response(array("property" => $property, "value" => $value), $coupon->$property->format("m/d/y"));
                    }
                } else {
                    $coupon->$property = $value;
                    $coupon->save();
                    output_ajax_success_response(array("property" => $property, "value" => $value), "Updated $property to '{$coupon->$property}'");
                }
            } else {
                output_ajax_error_response(validation_errors());
            }
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
            output_ajax_error_response($validation_exception->getMessage());
        }
    }
    /**
     * Adds an existing customer discount from a coupon to an existing customer.
     * @throws Exception
     */
    public function add_existing_coupon()
    {
        if (!$this->input->post("couponCode")) {
            throw new Exception("'couponCode' must be passed as a POST parameter.");
        }
        $couponCode = $this->input->post("couponCode");
        if (!$this->input->post("customer_id")) {
            throw new Exception("'customer_id' must be passed as a POST parameter.");
        }
        $customer_id = $this->input->post("customer_id");
        $this->load->library('coupon');
        $response = $this->coupon->applyCoupon($couponCode, $customer_id, $this->business_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Creates a new coupon
     * Can take the following parameters
     *  endDate: a valid PHP date format in the timezone of the current business
     * @throws \Database_Exception
     *
     */
    public function add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("recurring_type_id", "Recurring Type", "required|is_natural_no_zero");
        $this->form_validation->set_rules("amount", "Amount", "required|numeric");
        $this->form_validation->set_rules("amountType", "Amount Type", "required");
        $this->form_validation->set_rules("startDate", "Start Date", "required");
        if ($this->form_validation->run()) {
            if (in_bizzie_mode()) {
                $master_business = get_master_business();
            }
            if (in_bizzie_mode() && is_superadmin() && $this->input->post("add_to_master_business")) {
                $business = $master_business;
                $business_id = $master_business->{Business::$primary_key};
            } else {
                $business = $this->business;
                $business_id = $this->business_id;
            }
            $prefix = strtoupper(substr($_POST['where'], 0, 2));
            if ($prefix == "CR") {
                set_flash_message('error', 'Cannot create a CR coupon');
                redirect('/admin/admin/manage_coupons');
            }

            $getLastCode = "select code from coupon where prefix = '{$_POST['where']}' order by code desc";
            $q = $this->db->query($getLastCode);
            $lastCode = $q->result();

            $newCode = '1001';
            //loop through each code until you find an integer
            foreach ($lastCode as $code) {
                if (is_numeric($code->code)) {
                    //then add one
                    $newCode = $code->code + 1;
                    break;
                }
            }


            $description = '';
            if ($_POST['amountType'] == 'dollars') {
                $description .= format_money_symbol($this->business_id, '%.2n', $_POST['amount']) . ' off';
            } elseif ($_POST['amountType'] == 'percent') {
                $description .= $_POST['amount'] . '%  off';
            } elseif (substr($_POST['amountType'], 0, 4) == 'item') {
                //do a bunch of stuff here for item level discounts
                if (!isset($_POST['product_id'])) {
                    throw new Exception("'product_id' must be passed as a POST parameter.");
                }
                $product_id = $this->input->post("product_id");
                if ($product_id == 'dc') {
                    $product->name = 'Dry Cleaning';
                } else {
                    if (!is_numeric($product_id)) {
                        throw new Exception("'product_id' must be numeric.");
                    }

                    $getProduct = "select name from product where productID = $product_id";
                    $query = $this->db->query($getProduct);
                    $product = $query->row();
                }

                if ($_POST['amountType'] == 'item') {
                    $_POST['amountType'] = 'item['.$_POST['product_id'].']';
                    $description = format_money_symbol($this->business_id, '%.2n', $_POST['amount'] ) . ' '.$product->name;
                    $_POST['extendedDesc'] = $description .' - '.$_POST['extendedDesc'];
                } else {
                     $_POST['amountType'] = 'item[%'.$_POST['product_id'].']';
                     $description = $_POST['amount'] . '% off '.$product->name;
                     $_POST['extendedDesc'] = $description .' - '.$_POST['extendedDesc'];
                }

            } else {
                $description .= $_POST['amount'] . ' ' .$_POST['amountType'] . ' off';
            }

            if($this->input->post("process_id")){
                $this->load->model("process_model");
                $process_name = $this->process_model->getName($this->input->post("process_id"));

                $onlyProcessMsg = get_translation('only_for_process', 'Processes', array('process' => $process_name), "ONLY for %process% process");
                $_POST['extendedDesc'] .= "($onlyProcessMsg)";
            }

            if ($this->input->post("firstTime")) {
                $description .= ' first order';
            } else {
                $description .= ' next order';
            }

            if ($this->input->post("combine")) {
                $combine = 1;
            } else {
                $combine = 0;
            }
            $startDate = $this->input->post("startDate");
            $recurring_type_id = $this->input->post("recurring_type_id");


            if ($this->input->post("maxAmt")) {
                $maxAmount = $this->input->post("maxAmt");
            } else {
                $maxAmount = 0;
            }
            if ($this->input->post("firstTime")) {
                $firstTime = $this->input->post("firstTime");
            } else {
                $firstTime = 0;
            }


            $coupon = new Coupon();
            $coupon->business_id = $this->business_id;
            $coupon->created_by = $this->session->userdata('employee_id');
            $coupon->recurring_type_id = $recurring_type_id;
            $coupon->prefix = $this->input->post("where");
            $coupon->code = $newCode;
            $coupon->amount = $this->input->post("amount");
            $coupon->amountType = $this->input->post("amountType");
            $coupon->frequency = $this->input->post("frequency");
            $coupon->description = $description;
            $coupon->extendedDesc = $this->input->post("extendedDesc");
            $coupon->combine = $combine;
            $coupon->firstTime = $firstTime;
            $coupon->maxAmt    = $maxAmount;
            $coupon->process_id = $this->input->post("process_id");

            $coupon->startDate = new DateTime($startDate, new DateTimeZone($this->business->timezone));
            //THe following conditional checks if the recurring type is set to "monthly". If so, then we set the end date to the end of the month.
            if ( strtolower( $this->input->post("endDate") ) == "never" || !$this->input->post("endDate")) {
                $coupon->endDate = NULL;
            } elseif ($recurring_type_id == 2) {
                $coupon->endDate = new DateTime(date("Y-m-t"), new DateTimeZone($this->business->timezone));
            } else {
                $coupon->endDate = new DateTime($this->input->post("endDate"), new DateTimeZone($this->business->timezone));
            }

            $coupon->save();

            set_flash_message('success', 'Code '.$_POST['where'].$newCode." created for {$business->companyName}");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/coupons/manage");
    }

    /**
     * Applies a single coupon to all customers in the business.
     * @throws InvalidArgumentExceptionException
     * @throws \Database_Exception
     */
    public function apply_to_all_customers()
    {
        set_time_limit(0);
        
        try {
            if (!$this->input->post("couponID")) {
                throw new InvalidArgumentException("'couponID' must be passed as a GET parameter.");
            }
            $couponID = $this->input->post("couponID");

            if (!is_numeric($couponID)) {
                throw new InvalidArgumentExceptionException("'couponID' must be numeric.");
            }

            $this->load->model("customer_model");
            $this->customer_model->options = array("business_id" => $this->business_id);
            

            $filterID = $this->input->post("filterID");
            if ($filterID != "all") {
                $getFilters = "select query from filter where business_id = ? and filterID = ?";
                $query = $this->db->query($getFilters, array($this->business_id, $filterID));
                $filter_query = $query->row("query");
                $customers = $this->db->query($filter_query)->result();
            } else {
                $customers = $this->customer_model->get();
            }

            $this->coupon_model->options = array("couponID" => $couponID);
            $coupons = $this->coupon_model->get();
            $coupon = $coupons[0];

            $this->load->library("coupon");

            $data = array(
                "errors"  => array(),
                "success" => array()
            );

            foreach ($customers as $customer) {
                $r = $this->coupon->applyCoupon($coupon->prefix . $coupon->code, $customer->customerID, $this->business_id);
                if ($r["status"] == "fail") {
                    $data["errors"][] = get_translation(
                        'coupon_apply_error', 
                        'Error applying coupon ', 
                        array(
                            'name'      =>  $coupon->prefix.$coupon->code, 
                            'customer'  =>  getPersonName($customer), 
                            'error'     =>  $r["error"]
                        ), 
                        "Coupon '%name%' was not applied to %customer% (%error%)"
                    );
                } else {
                    $data["success"][] = get_translation(
                        'coupon_apply_success', 
                        'Applied coupon ', 
                        array(
                            'name'      =>  $coupon->prefix.$coupon->code, 
                            'customer'  =>  getPersonName($customer)
                        ), 
                        "Applied coupon '%name%' to %customer%"
                    );
                }
            }
        } catch (Exception $e) {
            send_exception_report($e);
            output_ajax_error_response();
        }

        output_ajax_success_response($data);
    }

    public function delete()
    {
        $couponID = $this->input->get("couponID");

        $coupon = new Coupon($couponID);
        $coupon->delete();

        set_flash_message("success", "Deleted coupon {$coupon->prefix}{$coupon->code}");
        redirect($this->agent->referrer());
    }
}
