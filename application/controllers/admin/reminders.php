<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CustomerDiscount;


class Reminders extends MY_Admin_Controller
{

    public $submenu = 'admin';

    /**
     * Renders the interface for managing email reminders
     */
    public function index()
    {
        $business_id = $this->business_id;
        $this->template->write('page_title','Email Reminders', true);
        $sql = "SELECT * FROM reminder WHERE business_id = {$business_id} ORDER BY couponType, days, orders";
        $reminders = $this->db->query($sql)->result();
        $content['reminders'] = $reminders;

        $this->renderAdmin('index', $content);
    }

    /**
     * The following action renders the interface for creating a new customer reminder.
     * if the POST variable 'submit' is set, adds a new master reminder into the database.
     */
    public function add()
    {
        $this->load->helper("coupon_helper");
        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $noDiscount = ($this->input->post("noDiscount") == 1) ? $_POST['noDiscount'] : 0;
            try {
                $reminder = Reminder::create( array(
                    "business_id" => $this->business_id,
                    "couponType" => $this->input->post("couponType"),
                    "noDiscount" => $noDiscount,
                    "days" => $this->input->post("days"),
                    "orders" => $this->input->post("orders"),
                    "amountType" => $this->input->post("amountType"),
                    "amount" => $this->input->post("amount"),
                    "maxAmt" => $this->input->post("maxAmt"),
                    "expireDays" => $this->input->post("expireDays"),
                    "dateCreated" => date("Y-m-d H:i:s"),
                    "description" => $this->input->post("description"),
                    "status" => $this->input->post("status")
                ));
            } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                set_flash_message("error", $validation_exception->getMessage());
                $this->session->set_flashdata("post", $_POST);
                redirect_with_fallback("/admin/reminders/add");
            }
            set_flash_message('success', "Created new Reminder '{$reminder->reminderID}'");
            redirect("/admin/reminders/edit/{$reminder->reminderID}", "location", 302);

        }
        $expireDays = array("1" => "1 Day");
        for ($day = 2; $day <= 30; $day++) {
            $expireDays[$day] = "$day Days";
        }

        $content = array("expireDays" => $expireDays);

        $this->template->write('page_title', 'Add Reminder');
        $this->renderAdmin('add', $content);
    }



    /**
     * Renders the interface for modifying a reminder or updates an rexisting reminder in the database.
     * Expects the reminder ID to be passed as segment 4 of the URL
     * @throws \InvalidArgumentException
     */
    public function edit($reminderID = null)
    {

        if (!is_numeric($reminderID)) {
            set_flash_message("error", 'reminderID must be passed as segment 4 of the URL and must be numeric');
            return $this->goBack("/admin/reminders");
        }
        try {
            $reminder = new Reminder($reminderID);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
            set_flash_message("error", $notFound_exception->getMessage());
            return $this->goBack("/admin/reminders");
        }

        if ($reminder->business_id != $this->business_id) {
            set_flash_message("error", 'The reminder does not belong to this business.');
            return redirect("/admin/reminders");
        }

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            //var_dump($_POST); die();
            $reminder->noDiscount = $this->input->post("noDiscount") ? 1 : 0;
            $reminder->setProperties($this->input->post(), TRUE);
            redirect_with_fallback("/admin/reminders/edit/$reminder->reminderID");
        }

        //If the request is not a post, then we display the current fields for the reminder.
        $content['reminder'] = $reminder;
        $this->template->write('page_title','Edit Reminder', true);
        $this->load->model("business_language_model");
        $content['business_languages'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $this->load->model("reminder_email_model");
        $content['reminder_emails_for_reminder_by_business_language'] = $this->reminder_email_model->get_all_for_reminder_by_business_language($reminder->reminderID);
        $this->renderAdmin('edit', $content);
    }



    public function delete($reminderID = null)
    {
        if (!is_numeric($reminderID)) {
            throw new \InvalidArgumentException("'reminderID' must be passed as segment 4 of the URL and must be numeric");
        }

        $reminder = new Reminder($reminderID);

        if ($reminder->business_id != $this->business_id) {
            set_flash_message('error', "Reminder does not belong to this business");
        } else {
            set_flash_message('success', "Reminder has been deleted");
            $reminder->delete();
        }

        $this->goBack("/admin/reminders");
    }


    /**
     * send a test email for reminders
     *
     * @return boolean
     */
    public function send_test_reminder_ajax()
    {
        $customer = new Customer($_POST['customer_id']);
        $reminder = new Reminder($_POST['reminder_id']);
        $expireDate = date("m/d/Y", strtotime("+{$reminder->expireDays} days"));

        $this->load->model('customerdiscount_model');
        $this->customerdiscount_model->expireCustomerDiscount($this->business_id);

        $activeDiscounts = CustomerDiscount::search_aprax(array(
            'customer_id' => $customer->customerID,
            'business_id '=> $customer->business_id,
            'active' => 1
        ));

        if (empty($activeDiscounts) && !$reminder->noDiscount) {

            switch ($reminder->couponType) {
            	case "newCustomer":
            	    $desciption = 'Customer never used us '.$reminder->days.' days after registering';
            	    break;
            	case "inactiveCustomer":
            	    $desciption = 'Customer has not used us in '.$reminder->days.' days';
            	    break;
            	case "orderTotal":
            	    $desciption = 'Customer has placed '.$reminder->orders.' orders auto coupon';
            	    break;
                default:
                    $description = '';
                    break;
            }

            $discount = $this->customerdiscount_model->addDiscount(
                $customer->customerID, $reminder->amount, $reminder->amountType, 'one time', $reminder->description,
                $description, $expireDate, 0, $reminder->maxAmt
            );
        }

        // discount will be false if we try to make a discount and it fails
        if ($discount || !empty($activeDiscounts) || $reminder->noDiscount) {

            if (!empty($discount)) {
                $options['customerDiscount_id'] = $discount;
            }
            $options['customer_id'] = $customer->customerID;
            $options['business_id'] = $customer->business_id;
            $data = get_macros($options);

            $business_language_id = get_customer_business_language_id($customer->customerID);
            $this->load->model("reminder_email_model");
            $reminder_email = $this->reminder_email_model->get_one(array("reminder_id" => $reminder->reminderID, "business_language_id" => $business_language_id));

            $subject = email_replace($reminder_email->subject, $data);
            $body = email_replace($reminder_email->body, $data);

            if (queueEmail(get_business_meta($reminder->business_id, 'customerSupport'), get_business_meta($reminder->business_id, 'from_email_name'), $customer->email, $subject, $body)) {
                echo "success";

                return true;
            } else {
                echo "Email not sent";

                return false;
            }
        } else {
            echo "Error creating discount. Most likely there is already an active discount with the same description. Email not sent";
        }
    }


    /**
     * Returns the total number of reminders for the current business
     * Expects the following GET parameter:
     *  business_id
     * @throws \LogicException if the request is not AJAX
     * @throws \InvalidArgumentException if the request does not include the GET parameter 'business_id'
     */
    public function get_total()
    {
        if (!$this->input->is_ajax_request()) {
            throw new \LogicException("This action can only be called with an ajax request");
        }

        $business_id = $this->input->get('business_id');
        if (!$business_id) {
            throw new \InvalidArgumentException("'business_id' must be passed as a GET parameter");
        }

        try {
            $this->load->model("reminder_model");
            $reminder_total = $this->reminder_model->get_count($business_id);
            $result = array('status' => 'success', 'total' => $reminder_total);
        } catch (\Exception $e) {
            $result = array('status' => 'error', 'message' => "An error occurred attempting to get the total reminder count");
            send_exception_report($e);
        }

        echo json_encode($result);
    }
}
