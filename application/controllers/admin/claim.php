<?php
class Claim extends MY_Admin_Controller
{
    private $inactive_claims_dataTable_columns;

    public function __construct()
    {
        parent::__construct();

        $this->load->model("claim_model");

        $this->inactive_claims_dataTable_columns = array(
            array("display_name" => "Claim ID", "query_name" => "claimID", "key" => "claimID"),
            array("display_name" => "Location", "query_name" => "location.address", "key" => "address"),
            array("display_name" => "Locker", "query_name" => "lockerName", "key" => "lockerName"),
            array("display_name" => "Customer", "query_name" => "concat(customer.firstName, ' ', customer.lastName)", "key" => "customer"),
            array("display_name" => "When Placed", "query_name" => "claim.created", "key" => "created"),
            array("display_name" => "Type", "query_name" => "claim.orderType", "key" => "orderType"),
            array("display_name" => "Notes", "query_name" => "claim.notes", "key" => "notes"),
            array("display_name" => "Employee name", "query_name" => "concat(employee.firstName, ' ', employee.lastName)", "key" => "employee"),
            array("display_name" => "Last Updated", "query_name" => "claim.updated", "key" => "lastUpdated"),
            array("display_name" => "customer_notes", "query_name" => "customer.customerNotes", "key" => "customerNotes"),
            array("display_name" => "email", "query_name" => "customer.email", "key" => "email"),
            array("display_name" => "phone", "query_name" => "customer.phone", "key" => "phone"),
            array("display_name" => "address1", "query_name" => "customer.address1", "key" => "address1"),
            array("display_name" => "employee_firstName", "query_name" => "employee.firstName", "key" => "employee_firstName"),
            array("display_name" => "employee_lastName", "query_name" => "employee.lastName", "key" => "employee_lastName"),
            array("display_name" => "customer_firstName", "query_name" => "customer.firstName", "key" => "customer_firstName"),
            array("display_name" => "customer_lastName", "query_name" => "customer.lastName", "key" => "customer_lastName"),
        );
    }
    public function get_inactive_claims_in_dataTables_format()
    {
        $aColumns = array();
        $aKeys = array();
        foreach ($this->inactive_claims_dataTable_columns as $column) {
            $aColumns[] = $column['query_name'] . ' AS ' . $column['key'];
            $aKeys[] = $column['query_name'];
        }

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "claimID";

        /*
            * Paging
            */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
                            $sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
                            mysql_real_escape_string( $_GET['iDisplayLength'] );
        }


        /*
        * Ordering
        */
        $sOrder = "ORDER BY created DESC";
        if ( isset( $_GET['iSortCol_0'] ) ) {
            $column_key = $this->input->get("iSortCol_0");
            $valid_directions = array('asc' => 'ASC', 'desc' => 'DESC');
            $direction = isset($valid_directions[$this->input->get("sSortDir_0")]) ?  $valid_directions[$this->input->get("sSortDir_0")] : 'DESC';

            $sOrder = "ORDER BY  ". $this->inactive_claims_dataTable_columns[$column_key]['query_name'] . " " .  $direction;
            if (in_array($this->inactive_claims_dataTable_columns[$column_key]['key'], array('lastUpdated', 'created'))) {
                $sOrder = "ORDER BY  claim.updated " . $direction;
            }
        }


            /*
                * Filtering
                * NOTE this does not match the built-in DataTables filtering which does it
                * word by word on any field. It's possible to do here, but concerned about efficiency
                * on very large tables, and MySQL's regex functionality is very limited
                */
            $sWhere = "";
            if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
                $sWhere = "AND (";
                for ( $i=0 ; $i<count($aKeys) ; $i++ ) {
                    $sWhere .= $aKeys[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
                }

                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ")";
            }

            /* Individual column filtering */
            for ( $i=0 ; $i<count($aKeys) ; $i++ ) {
                if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
                    $sWhere .= "AND ";
                    $sWhere .= $aKeys[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
                }
            }


    /*
     * SQL queries
     * Get data to display
     */
        $select = "SELECT SQL_CALC_FOUND_ROWS ".  implode(",", $aColumns);
    $sQuery = "
                                $select, claim.customer_id, locationID, claim.employee_id 
                                FROM claim
                                INNER JOIN locker ON locker.lockerID=claim.locker_id
                                INNER JOIN location ON location.locationID=locker.location_id
                                LEFT JOIN customer on claim.customer_id=customer.customerID
                                LEFT JOIN employee on claim.employee_id = employee.employeeID
                                WHERE claim.business_id={$this->business_id} AND claim.active = 0 $sWhere
                                $sOrder
                                $sLimit
        ";

        $rQuery = $this->db->query($sQuery);
        if ($this->db->_error_message()) {
            throw new Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $rResult = $rQuery->result_array();

    /* Data set length after filtering */
    $sQuery = "
        SELECT FOUND_ROWS() AS found_rows
    ";

        $aResultFilterTotal = $this->db->query($sQuery)->row();
    $iFilteredTotal = $aResultFilterTotal->found_rows;

    /* Total data set length */
    $sQuery = "
        SELECT COUNT(`".$sIndexColumn."`) AS total
        FROM claim
                WHERE business_id = {$this->business_id}
    ";
    $aResultTotal = $this->db->query($sQuery)->row();
    $iTotal = $aResultTotal->total;

    /*
     * Output
     */
    $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
    );

    $this->load->model('servicetype_model');
    $serviceTypes = $this->servicetype_model->get_formatted();

    $dateFormat = get_business_meta($this->business_id, "fullDateDisplayFormat");
    foreach ($rResult as $aRow) {
            $row = array();
            foreach ($this->inactive_claims_dataTable_columns as $column) {
                switch ($column['key']) {
                    case 'customer':
                        $person = new stdClass;
                        $person->firstName = $aRow['customer_firstName'];
                        $person->lastName  = $aRow['customer_lastName'];

                        $tooltip = "Email:".$aRow['email']."<br/>Phone: ".$aRow['phone']."<br/>Address: ".$aRow['address1']."<br/>Customer Notes: ".$aRow['customer_notes'];
                        $row[] = '<a data-tooltip="'.$tooltip.'" href="/admin/customers/detail/'.$aRow['customer_id'].'" target="blank" style="color: blue;">' . getPersonName($person) . '</a>';
                        break;
                    case 'address':
                        $row[] = '<a href="/admin/orders/view_location_orders/'.$aRow['locationID'].'" target="blank" style="color: blue;">' . $aRow['address'] . '</a>';
                        break;
                    case 'orderType':
                        $str = '';
                        $type = unserialize($aRow['orderType']);
                        if (is_array($type)) {
                            foreach ($type as $key => $value) {
                                if (array_key_exists($value, $serviceTypes)) {
                                    $str .=  substr($serviceTypes[$value]['slug'],0,5)."<br>";
                                }
                            }
                        } else {
                            $str = "SMS";
                        }
                        $row[] = $str;
                        break;
                    case 'employee': 
                        $person = new stdClass;
                        $person->firstName = $aRow['employee_firstName'];
                        $person->lastName  = $aRow['employee_lastName'];
                        $row[] = '<a href="/admin/employees/manage/'.$aRow['employee_id'].'" target="blank" style="color: blue;">' . getPersonName($person) . '</a>';
                        break;
                    case 'lastUpdated':
                    case 'created':
                        $row[] = convert_from_gmt_aprax($aRow[$column['key']], $dateFormat);
                        break;                       
                    default:
                        $row[] = $aRow[$column['key']];
                }
            }

            $output['aaData'][] = $row;
    }

    echo json_encode($output);


    }

    public function update()
    {
        try {
            if (!$this->input->post("claimID")) {
                throw new \InvalidArgumentException("'claimID' must be passed as a POST parameter.");

            }
            $claimID = trim($this->input->post("claimID"));
            if (!is_numeric($claimID)) {
                throw new \InvalidArgumentException("'claimID' must be numeric.");
            }

            if (!$this->input->post("property")) {
                throw new \InvalidArgumentException("'property' must be passed as a POST parameter.");
            }
            if (!array_key_exists("value", $_POST)) {
                throw new \InvalidArgumentException("'value' must be passed as a POST parameter");
            }
            $value = trim($this->input->post("value"));
            $property = trim($this->input->post("property"));
            try {
                $claim = new \App\Libraries\DroplockerObjects\Claim($claimID);
                $claim->$property = $value;
                $claim->save();
                echo $claim->$property;
            } catch (App\Libraries\DroplockerObjects\Validation_Exception $e) {
                echo "Invalid Value";
            }
        } catch (Exception $e) {
            echo "Sorry, an error occurred";
            send_exception_report($e);
        }
    }

}
