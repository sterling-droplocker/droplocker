<?php

class Api extends CI_Controller
{
    public function get_all_locations()
    {
        $this->load->model('location_model');
        $locations = $this->location_model->get_all_with_business();
        $is_bizzie = in_bizzie_mode();

        foreach ($locations as $key => $location) {
            $filename = $_SERVER['DOCUMENT_ROOT'].'/images/location_'.$location->locationID.'.jpg';
            if (file_exists($filename)) {
                $img = getimagesize($filename);
                $locations[$key]->image = array(
                    'url' => 'https://' . $_SERVER['HTTP_HOST'] . '/images/location_'.$location->locationID.'.jpg',
                    'width' => $img[0],
                    'height' => $img[1],
                );
            }
            $locations[$key]->bizzie = $is_bizzie;
            if ($is_bizzie) {
                $locations[$key]->business_id = 'B-' . $locations[$key]->business_id;
                $locations[$key]->locationID = 'B-' . $locations[$key]->locationID;
                $locations[$key]->business = 'Bizzie: ' . $locations[$key]->business;
            }
        }

        die(json_encode($locations));
    }

    public function get_business_list()
    {
        $is_bizzie = in_bizzie_mode();
        $businessArray = $this->business_model->get_all_as_codeigniter_dropdown();

        $out = array();
        foreach ($businessArray as $id => $name) {
            if ($is_bizzie) {
                $id = 'B-' . $id;
                $name = 'Bizzie: ' . $name;
            }
            $out[$id] = $name;
        }

        die(json_encode($out));
    }
}