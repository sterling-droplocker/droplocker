<?php

use App\Libraries\DroplockerObjects\CustomerDiscount;

class Customer_Discounts extends MY_Admin_Controller
{

    public function deactivate()
    {
        if (!$this->input->get("customerDiscountID")) {
            throw new User_Exception("'customerDiscountID must be numeric.");
        }
        $customerDiscountID = $this->input->get("customerDiscountID");
        $customerDiscount = new CustomerDiscount($customerDiscountID, false);
        $customerDiscount->recurring_type_id = 1;
        $customerDiscount->active = 0;
        $customerDiscount->save();

        set_flash_message("success", "Deactivated customer discount");
        $this->goBack("/admin/customers/discounts/{$customerDiscount->customer_id}");
    }

    /**
     *
     * @throws \InvalidArgumentException
     */
    public function update()
    {
        if (!$this->input->post("customerDiscountID")) {
            throw new \InvalidArgumentException("'customerDiscountID' must be passed as a POST parameter");

        }
        $customerDiscountID = $this->input->post("customerDiscountID");
        if (!$this->input->post("property")) {
            throw new \InvalidArgumentException("'property' must be passed as a POST parameter.");
        }

        if (!array_key_exists("value", $_POST)) {
            throw new \InvalidArgumentException("'value' must be passed as a POST parameter");
        }
        $value = trim($_POST['value']);
        $property = trim($this->input->post("property"));
        try {
            $customerDiscount = new CustomerDiscount($customerDiscountID);
            $customerDiscount->$property = $value;
            $customerDiscount->save();
            echo $customerDiscount->$property;
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $e) {
            echo "Invalid Value: " . $e->getMessage();
        }
    }

}
