<?php

class Main extends MY_Admin_Controller
{
    public $publicAccess = true;

    public function __construct()
    {
        parent::__construct();

        if ($page = $this->uri->segment(4)) {
            $this->data['submenu'] = $this->load->view('inc/admin_submenu', array("submenu" => $this->admin_menu->get_submenu($page)), true);
        }
    }

    public function request_permission($page = null, $resource = null)
    {
        $emailLogID = $this->zacl->requestPermission($page, $resource, $_POST);
        $this->renderAdmin('/admin/admin/request_sent', $content);
    }

    public function denied($page = null, $resource = null)
    {
        $content['postValues'] = json_encode($this->session->flashdata('postValues'));
        $content['getValues'] = $this->session->flashdata('getValues');
        $content['page'] = $page;
        $content['resource'] = $resource;

        $this->load->model('business_model');
        $this->load->model('order_model');
        $this->buser_id = $this->session->userdata('buser_id');

        $business_id = $this->session->userdata('business_id');
        if (!$business_id) {
            $this->session->set_flashdata('login_redirect_url', $_SERVER['REQUEST_URI']);
            redirect("/admin/login");
        }

        $this->business = $this->business_model->get_by_primary_key($business_id);
        $this->business_id = $this->business->businessID;

        if ($this->input->is_ajax_request()) {
            $this->load->view('admin/admin/denied', $content);
            return;
        }

        $this->renderAdmin('/admin/admin/denied', $content);
    }

}
