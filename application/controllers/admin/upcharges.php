<?php

use App\Libraries\DroplockerObjects\Upcharge;
use App\Libraries\DroplockerObjects\UpchargeGroup;
use App\Libraries\DroplockerObjects\UpchargeSupplier;
use App\Libraries\DroplockerObjects\Supplier;


class Upcharges extends MY_Admin_Controller
{

    public $submenu = 'admin';

    /**
     * List upcharges
     */
    public function index()
    {
        // get the upcharge groups
        $groups = UpchargeGroup::search_aprax(array(
            "business_id" => $this->business_id
        ));

        // get the upcharges in each group
        foreach ($groups as $group) {
            $group->upcharges = $group->getGroupUpcharges();
            $allGroups[] = $group;
        }

        $content['upchargeGroups'] = $allGroups;
        $content['suppliers'] = Supplier::getBusinessSuppliers($this->business_id);

        $this->renderAdmin('index', $content);
    }

    /**
     * no UI redirects to upcharges
     */
    public function add_group()
    {
        $group = new UpchargeGroup();
        if (empty($_POST['name'])) {
            set_flash_message('error', "Name must not be empty");
            redirect("/admin/upcharges");
            return;
        }

        if ($group->addGroup($_POST['name'], $this->business_id)) {
            set_flash_message('success', "New upcharge group has been added");
        } else {
            set_flash_message('error', "Failed creating a new upcharge group");
        }
        redirect("/admin/upcharges");
    }

    /**
     * Updates an upcharge group
     *
     * @param int $group_id
     */
    public function update_group($group_id = null)
    {
        if (empty($_POST['name'])) {
            set_flash_message('error', "Name must not be empty");
            redirect("/admin/upcharges");
        }

        $group = new UpchargeGroup($group_id);
        $group->name = $_POST['name'];
        $group->save();
        set_flash_message('success', "Upcharge group has been updated");

        redirect("/admin/upcharges");
    }

    /**
     * adds an upcharge to a group.
     * There is no UI for this
     */
    public function add()
    {
        if (isset($_POST['add_upcharge'])) {

            if (empty($_POST['upchargePrice']) || !is_numeric($_POST['upchargePrice'])) {
                $upchargePrice = 0;
            } else {
                $upchargePrice = $_POST['upchargePrice'];
            }


            if (empty($_POST['name'])) {
                set_flash_message('error', 'Missing Upcharge Name');
                redirect("/admin/upcharges");
            } else {
                $upchargeName = $_POST['name'];
            }


            if (empty($_POST['cost']) || !is_numeric($_POST['cost'])) {
                $cost = 0;
            } else {
                $cost = $_POST['cost'];
            }


            $upcharge = new Upcharge();
            $upcharge->upchargeGroup_id = $this->input->post('upchargeGroup_id');
            $upcharge->name = $upchargeName;
            $upcharge->upchargePrice = $upchargePrice;
            $upcharge->priceType = $this->input->post('priceType');
            $upcharge->everyOrder = $this->input->post('everyOrder');

            $upcharge->save();

            $upchargeSupplier = new UpchargeSupplier();
            $upchargeSupplier->cost = $cost;
            $upchargeSupplier->supplier_id = $this->input->post('supplier_id');
            $upchargeSupplier->default = 1;
            $upchargeSupplier->upcharge_id = $upcharge->{Upcharge::$primary_key};
            $upchargeSupplier->save();

            set_flash_message('success', "Added upcharge '$upcharge->name'");
            redirect("/admin/upcharges");
        }
    }

    /**
     * Displays the UI for updating an upcharge
     *
     * @param int $upcharge_id
     */
    public function edit($upcharge_id = null)
    {
        $upchargeObj = new Upcharge($upcharge_id);
        $content['name'] = $upchargeObj->name;
        $content['price'] = $upchargeObj->upchargePrice;
        $content['name'] = $upchargeObj->name;
        $content['everyOrder'] = $upchargeObj->everyOrder;
        $content['suppliers'] = Supplier::getBusinessSuppliers($this->business_id);
        $upchargeSupplier = UpchargeSupplier::search_aprax(array('upcharge_id'=>$upcharge_id));
        $content['supplier_id'] = $upchargeSupplier[0]->supplier_id;
        $content['cost'] = $upchargeSupplier[0]->cost;
        $content['upcharge_id'] = $upcharge_id;

        // render ajax form
        $this->load->view("admin/upcharges/edit_upcharge", $content);
    }

    /**
     * Updates an upcharge
     *
     * @param int $upcharge_id
     */
    public function update($upcharge_id = null)
    {
        if (isset($_POST['submit'])) {

            $upcharge = new Upcharge($upcharge_id);
            $upcharge->name = $_POST['name'];
            $upcharge->upchargePrice = $_POST['price'];
            $upcharge->everyOrder = $_POST['everyOrder'];

            if ($upcharge->save()) {
                set_flash_message('success', "Upcharge has been updated");
            } else {
                set_flash_message('error', "Upcharge has NOT been updated");
            }

            redirect("/admin/upcharges");
        }
    }

    /**
     * Deletes an upcharge
     * There is no UI for this
     */
    public function delete()
    {
        $upcharge_id = $_POST['upcharge_id'];

        try {
            $upchargeObj = new Upcharge($upcharge_id);
            if ($upchargeObj->delete()) {
                set_flash_message('success', 'upcharge has been deleted');
            } else {
                set_flash_message('error', 'upcharge has NOT been deleted');
            }
        } catch (Exception $e) {
            set_flash_message('error', $e->getMessage());
        }

        redirect("/admin/upcharges");
    }

    /**
     * Shows UI to edit the suppliers
     * @param string $upcharge_id
     */
    public function edit_suppliers($upcharge_id = null)
    {
        $upcharge = new Upcharge($upcharge_id);
        $group = new UpchargeGroup($upcharge->upchargeGroup_id);
        $content['groupName'] = $group->name;
        $content['name'] = $upcharge->name;
        $content['suppliers'] = $upcharge->getSuppliers();
        $content['upchargeID'] = $upcharge_id;
        $this->load->view('/admin/upcharges/edit_suppliers', $content);
    }

    /**
     * Updates the suppliers
     * @param string $upcharge_id
     */
    public function update_suppliers($upcharge_id = null)
    {
        if (isset($_POST['submit'])) {
            UpchargeSupplier::deleteUpchargeSuppliers($upcharge_id);

            foreach ($_POST['cost'] as $supplier_id=>$cost) {
                if ($cost[0] > 0 || $supplier_id == $_POST['default']) {
                    $supplier = new UpchargeSupplier();
                    $supplier->supplier_id = $supplier_id;
                    $supplier->cost = $cost[0];
                    if ($supplier_id == $_POST['default']) {
                        $supplier->default = '1';
                    } else {
                        $supplier->default = '0';
                    }
                    $supplier->upcharge_id = $upcharge_id;
                    $supplier->dateCreated = convert_to_gmt_aprax();
                    $supplier->save();
                }
            }
            set_flash_message("success", "Suppliers have been updated");
            redirect("/admin/upcharges");
        }
    }

    /**
     * Gets the upcharge count for a business
     */
    public function get_total()
    {
        if ($this->input->get("business_id")) {
            try {
                $business_id = $this->input->get("business_id");
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($business_id);
                $query = "SELECT COUNT(upchargeID) AS total FROM upcharge INNER JOIN upchargeGroup ON upchargeGroup_id=upchargeGroupID WHERE upchargeGroup.business_id=?";
                $result = $this->db->query($query, array($business_id))->row();
                output_ajax_success_response(array("total" => $result->total), "Total upcharges for '{$business->companyName} ({$business->businessID})");
            } catch (Exception $e) {
                send_exception_report($e);
                output_ajax_error_response("An internal error occurred attempting to retrieve the upcharge total");
            }
        }
    }

}
