<?php

use DropLocker\Util\Lock;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\CardReader;
use App\Libraries\DroplockerObjects\CardReaderSyncLog;
use App\Libraries\DroplockerObjects\CardReaderAccessLog;
use App\Libraries\DroplockerObjects\Validation_Exception;

class Card_Readers extends MY_Admin_Controller
{

    public $submenu = 'admin';

    /**
     *
     * @var CardReaderLibrary
     */
    public $cardreaderlibrary;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('cardreaderlibrary');
    }

    public function index()
    {
        $getReaders = "
            select cardReader.*, location.address as locAddr,
                syncMessage, syncTime, success
                from cardReader
                join location ON (locationID = location_id)
                left join
                    (SELECT * FROM cardReaderSyncLog order by cardReaderSyncLogID DESC) as cardReaderSyncLog
                    ON (cardReaderSyncLog.location_id = cardReader.location_id)
                where location.business_id = ?
                group by cardReaderID
            ";

        $q = $this->db->query($getReaders, array($this->business_id));
        $content['readers'] = $q->result();

        $this->load->model('location_model');
        $content['locations'] = $this->location_model->get_all_as_codeigniter_dropdown_for_business($this->business_id);

        $content['models'] = CardReader::listModels();
        $content['modes'] = CardReader::listModes();

        $this->setPageTitle('Manage Card Readers');
        $this->renderAdmin('index', $content);
    }

    public function add()
    {
        $cardReader = new CardReader();
        try {
            $cardReader->setProperties(array(
                'location_id' => $this->input->post('location_id'),
                'address' => $this->input->post('address'),
                'port' => $this->input->post('port'),
                'masterPort' => $this->input->post('masterPort'),
                'password' => $this->input->post('password'),
                'model' => $this->input->post('model'),
                'mode' => $this->input->post('mode'),
                'windowStart' => $this->input->post('windowStart'),
                'windowEnd' => $this->input->post('windowEnd'),
            ));
        } catch (Validation_Exception $e) {
            set_flash_message('error', $e->getMessage());
            redirect("/admin/card_readers");
        }
        set_flash_message('success', "Reader Added");

        redirect("/admin/card_readers");
    }

    public function edit($cardReaderID = null)
    {
        $cardReader = new CardReader($cardReaderID);
        $content['cardReader'] = $cardReader;

        $this->load->model('location_model');
        $content['locations'] = $this->location_model->get_all_as_codeigniter_dropdown_for_business($this->business_id);
        $content['models'] = CardReader::listModels();

        $this->renderAdmin('edit', $content);
    }

    public function update($cardReaderID = null)
    {
        try {
            $cardReader = new CardReader($cardReaderID);
            $cardReader->setProperties($_POST);
        } catch (Validation_Exception $e) {
            set_flash_message('error', $e->getMessage());
            redirect("/admin/card_readers/edit/$cardReaderID");
        }

        set_flash_message('success', "Card reader updated");
        redirect("/admin/card_readers");
    }

    public function change_mode()
    {
        $cardReaderID = $this->input->post('cardReaderID');
        $cardReader = new CardReader($cardReaderID);

        $this->lock($cardReaderID);

        $windowStart = $this->input->post('windowStart');
        $windowEnd = $this->input->post('windowEnd');

        $mode = $this->input->post('mode');

        //$response = $this->cardreaderlibrary->reset($cardReader->address, $cardReader->port);
        $response = $this->cardreaderlibrary->setMode($cardReader->address, $cardReader->port,
            $mode, $windowStart, $windowEnd);

        if ($response['result'] == 'success') {

            // if it was open, then it needs a full load of cards
            if ($mode != 'open' && $cardReader->mode == 'open') {
                $cardReader->full_upload = 1;
            }

            $cardReader->mode = $mode;
            $cardReader->windowStart = $windowStart;
            $cardReader->windowEnd = $windowEnd;
            $cardReader->current_mode = $response['data'];
            $cardReader->save();
        }

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => $response['message'],
        ));

        $this->unlock($cardReaderID);

        set_flash_message($response['result'], $response['message']);

        redirect("/admin/card_readers");
    }

    public function open()
    {
        $cardReaderID = $this->input->post('cardReaderID');
        $cardReader = new CardReader($cardReaderID);

        $this->lock($cardReaderID);

        $response = $this->cardreaderlibrary->open($cardReader->address, $cardReader->port);

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => $response['message'],
        ));

        $this->unlock($cardReaderID);

        set_flash_message($response['result'], $response['message']);
        redirect("/admin/card_readers");
    }

    public function reset()
    {
        $cardReaderID = $this->input->post('cardReaderID');
        $cardReader = new CardReader($cardReaderID);

        $this->lock($cardReaderID);

        $response = $this->cardreaderlibrary->reset($cardReader->address, $cardReader->port);
        $response = $this->cardreaderlibrary->setMode($cardReader->address, $cardReader->port,
            $cardReader->mode, $cardReader->windowStart, $cardReader->windowEnd);

        if ($response['result'] == 'success') {

            if ($cardReader->mode != 'open') {
                $cardReader->full_upload = 1;
            }

            $cardReader->current_mode = $response['data'];
            $cardReader->save();
        }

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => $response['message'],
        ));

        $this->unlock($cardReaderID);

        set_flash_message($response['result'], $response['message']);
        redirect("/admin/card_readers");
    }

    public function reboot()
    {
        $cardReaderID = $this->input->post('cardReaderID');
        $cardReader = new CardReader($cardReaderID);

        //$this->lock($cardReaderID);

        $response = $this->cardreaderlibrary->reboot($cardReader->address, $cardReader->masterPort, $cardReader->password);

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => $response['message'],
        ));

        //$this->unlock($cardReaderID);

        set_flash_message($response['result'], $response['message']);
        redirect("/admin/card_readers");
    }

    public function sync()
    {
        $cardReaderID = $this->input->post('cardReaderID');
        $cardReader = new CardReader($cardReaderID);
        $location = new Location($cardReader->location_id);

        $this->lock($cardReaderID);

        $full = !$this->input->post('newCards');
        $syncTime = gmdate('Y-m-d H:i:s');

        //get the access log
        $response = $this->cardreaderlibrary->getLog($cardReader->address, $cardReader->port);

        if ($response['result'] == 'success' && !empty($response['data'])) {
            foreach ($response['data'] as $row) {

                $accesTime = $row['accessTime'];

                $access = new CardReaderAccessLog();
                $access->setProperties(array(
                    'location_id' => $cardReader->location_id,
                    'accessTime' => $accesTime,
                    'card' => $row['card'],
                    'exp' => $row['exp'],
                    'success' => $row['success'],
                    'raw' => $row['raw'],
                ));
            }
        }

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => $response['message'],
        ));

        set_flash_message($response['result'], $response['message']);

        //if this is open to only registered cards
        if ($cardReader->mode != 'open') {

            $today = null;
            if (!$full) {

                // get last time it was updated
                $lastUpdate = CardReaderSyncLog::search(array(
                    'location_id' => $cardReader->location_id,
                    'cardsAdded >' => 0,
                    'success' => 1,
                ), false, 'syncTime DESC');

                // use last updated to get only newer cards
                if ($lastUpdate) {
                    $lastUpdate->syncTime->setTimeZone(new DateTimeZone('GMT'));
                    $today = $lastUpdate->syncTime->format('Y-m-d H:i:s');
                }
            }

            $syncTime = gmdate('Y-m-d H:i:s');
            $cards = $this->cardreaderlibrary->getCards($location->business_id, $today);

            if ($full) {
                //reset the reader
                $this->cardreaderlibrary->reset($cardReader->address, $cardReader->port);

                // set the mode
                $response = $this->cardreaderlibrary->setMode($cardReader->address, $cardReader->port,
                    $cardReader->mode, $cardReader->windowStart, $cardReader->windowEnd);

                // update the current mode
                if ($response['result'] == 'success') {
                    $cardReader->current_mode = $response['data'];
                }
            }

            // load cards
            $response = $this->cardreaderlibrary->addCards($cardReader->address, $cardReader->port, $cards);

            if ($full && $response['result'] == 'success') {
                // no full load necessary, just did one
                $cardReader->full_upload = 0;
                $cardReader->save();
            }

            // log the request
            $log = new CardReaderSyncLog();
            $log->setProperties(array(
                'location_id' => $cardReader->location_id,
                'success' => $response['result'] == 'success' ? 1 : 0,
                'syncMessage' => $response['message'],
                'cardsAdded' => count($response['data']['cardsAdded']),
                'syncTime' => $syncTime,
            ));

            if ($response['result'] != 'success') {
                send_admin_notification("Error with card reader sync at {$location->address}", $response['message'], $location->business_id);
            } else if (!empty($response['data']['errors'])) {
                send_admin_notification("Error with card reader sync at {$location->address}", $response['message'] . '<br>' . print_r($response['data']['errors'], true), $location->business_id);
            }

            set_flash_message($response['result'], $response['message']);
        }

        $this->unlock($cardReaderID);

        redirect("/admin/card_readers");
    }

    public function log($location_id = null)
    {
        $location = new Location($location_id);

        $content['address'] = $location->address;
        $content['location'] = $location;

        $this->setPageTitle('Card Reader Access Log');
        $this->renderAdmin('log', $content);
    }

    public function log_data($location_id)
    {
        $columns = array(
            0 => 'uploaded',
            1 => 'success',
            2 => 'accessTime',
            3 => 'card',
            4 => 'exp',
            5 => 'customers',
            6 => 'raw',
        );

        $sorts = array(
            'asc' => 'asc',
            'desc' => 'desc',
        );

        $sOrder = "ORDER BY cardReaderAccessLogID DESC";
        if (isset($_GET['iSortCol_0'])) {
            $column = $columns[$_GET['iSortCol_0']];
            $sort = $sorts[$_GET['sSortDir_0']];

            $sOrder = "ORDER BY $column $sort";
        }

        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $start = (int) $_GET['iDisplayStart'];
            $length = (int) $_GET['iDisplayLength'];
            $sLimit = "LIMIT $start, $length";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS *
                    FROM cardReaderAccessLog
                    JOIN location on locationID = location_id
                    WHERE location.business_id = ?
                    AND locationID = ?
                    $sOrder
                    $sLimit";

        $q = $this->db->query($sql, array($this->business_id, $location_id));
        $log = $q->result();

        $q = $this->db->query("SELECT FOUND_ROWS() AS found_rows");
        $row = $q->row();
        $total = $row->found_rows;
        $filteredTotal = $total;

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $total,
            "iTotalDisplayRecords" => $filteredTotal,
            "aaData" => array()
        );

        foreach ($log as $row) {

            $customers = array();

            $expYr = substr($row->exp, 0, 2);
            $expMo = substr($row->exp, 2, 2);

            $matches = array();

            $matches = $this->db->query("SELECT firstName, lastName, customerID, expYr, expMo, 'kiosk' AS type
                    FROM customer
                    JOIN kioskAccess ON (customer_id = customerID)
                    WHERE kioskAccess.business_id = ?
                    AND cardNumber = ?
                    AND expYr = ?
                    AND expMo = ?
                ", array($this->business_id, $row->card, $expYr, $expMo))->result();

            if (empty($matches)) {
                $matches = $this->db->query("SELECT firstName, lastName, customerID, SUBSTRING(expYr, 3) AS expYr, expMo, 'card' AS type
                    FROM customer
                    JOIN creditCard ON (customer_id = customerID)
                    WHERE creditCard.business_id = ?
                    AND cardNumber = ?
                    AND SUBSTRING(expYr, 3) = ?
                    AND expMo = ?
                ", array($this->business_id, $row->card, $expYr, $expMo))->result();
            }

            foreach ($matches as $match) {
                $customers[] = sprintf(
                    '%s: <a href="/admin/customers/detail/%s" target="_blank"> %s %s</a>',
                    $match->type,
                    $match->customerID,
                    getPersonName($match),
                    ($match->expMo != $expMo || $match->expYr != $expYr) ? '<span style="color:red;">*</span>' : ''
                );
            }

            $row = array(
                convert_from_gmt_aprax($row->uploaded, "m/d/y g:ia"),
                $row->success ? 'Success' : 'Failure',
                convert_from_gmt_aprax($row->accessTime, "m/d/y g:ia"),
                $row->card,
                preg_replace('/([0-9]{2})([0-9]{2})/', '\1/\2', $row->exp),
                implode('<br>', $customers),
                $row->raw,
            );
            $output['aaData'][] = $row;
        }
        echo json_encode($output);


    }

    public function history($location_id = null)
    {
        $location = new Location($location_id);

        $content['address'] = $location->address;
        $content['location'] = $location;

        $this->setPageTitle('Card Reader History');
        $this->renderAdmin('history', $content);
    }

    public function history_data($location_id)
    {
        $columns = array(
            0 => 'syncTime',
            1 => 'success',
            2 => 'message',
            3 => 'cardsAdded',
        );

        $sorts = array(
            'asc' => 'asc',
            'desc' => 'desc',
        );

        $sOrder = "ORDER BY cardReaderSyncLogID DESC";
        if (isset($_GET['iSortCol_0'])) {
            $column = $columns[$_GET['iSortCol_0']];
            $sort = $sorts[$_GET['sSortDir_0']];

            $sOrder = "ORDER BY $column $sort";
        }

        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $start = (int) $_GET['iDisplayStart'];
            $length = (int) $_GET['iDisplayLength'];
            $sLimit = "LIMIT $start, $length";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS *
                    FROM cardReaderSyncLog
                    JOIN location ON (locationID = location_id)
                    WHERE location.business_id = ?
                    AND locationID = ?
                    $sOrder
                    $sLimit";

        $q = $this->db->query($sql, array($this->business_id, $location_id));
        $history = $q->result();

        $q = $this->db->query("SELECT FOUND_ROWS() AS found_rows");
        $row = $q->row();

        $total = $row->found_rows;

        $filteredTotal = $total;

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $total,
            "iTotalDisplayRecords" => $filteredTotal,
            "aaData" => array()
        );

        foreach ($history as $row) {
            $row = array(
                convert_from_gmt_aprax($row->syncTime, "m/d/y g:ia"),
                $row->success ? 'Success' : 'Failure',
                '<div class="'.($row->success ? 'alert-success' : 'alert-error').'">'.$row->syncMessage.'</div>',
                $row->cardsAdded,
            );
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function delete($cardReaderID = null)
    {
        $cardReader = new CardReader($cardReaderID);
        $cardReader->delete();

        set_flash_message('success', "Reader Deleted");
        redirect("/admin/card_readers");
    }

    public function disable($cardReaderID = null)
    {
        $cardReader = new CardReader($cardReaderID);
        $cardReader->status = 'disabled';
        $cardReader->save();

        set_flash_message('success', "Reader disabled");
        redirect("/admin/card_readers");
    }

    public function enable($cardReaderID = null)
    {
        $cardReader = new CardReader($cardReaderID);
        $cardReader->status = 'enabled';
        $cardReader->save();

        set_flash_message('success', "Reader enabled");
        redirect("/admin/card_readers");
    }

    public function command()
    {
        $content['command'] = '';
        $content['response'] = '';
        $content['log'] = array();

        $content['ip'] = $this->input->get_post('ip');

        $command = $this->input->post('command');
        $command = str_replace('~', "\0", $command);

        if (!empty($command)) {
            list($ip, $port) = explode(':', $this->input->get_post('ip'));
            $response = $this->cardreaderlibrary->sendCommand($ip, $port, $command);

            $content['log'] = $this->input->post('log');
            $content['log'][] = array(
                'command' => $command,
                'response' => $response,
            );
        }

        $this->setPageTitle('Card Reader Commands');
        $this->renderAdmin('command', $content);
    }

    protected function lock($cardReaderID)
    {
        while(!Lock::acquire("card_reader_{$cardReaderID}")) {
            set_flash_message("error", "Failed to acquire lock");
            redirect_with_fallback("/admin/card_readers");
        }
    }

    protected function unlock($cardReaderID)
    {
        Lock::release("card_reader_{$cardReaderID}");
    }

}