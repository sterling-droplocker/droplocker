<?php

use DropLocker\Service\HomeDelivery\Factory;
use App\Libraries\DroplockerObjects\Location;

class Home_Delivery extends MY_Admin_Controller
{
    public $submenu = 'admin';

    public $valid_settings = array(
        'deliv' => array(
            'deliv_api_key',
            'deliv_sandbox_key',
            'deliv_mode',
            'deliv_fee',
            'deliv_ready_by',
            'deliv_window_notes',
        ),
        'rickshaw' => array(
            'rickshaw_api_key',
            'rickshaw_mode',
            'rickshaw_contact_name',
            'rickshaw_contact_phone',
            'rickshaw_delivery_address',
            'rickshaw_delivery_notes',
            'rickshaw_pickup_addresses',
            'rickshaw_pickup_notes',
        ),
        'boxture' => array(
            'boxture_api_url',
            'boxture_api_key',
            'boxture_mode',
            'boxture_contact_name',
            'boxture_contact_phone',
            'boxture_delivery_address',
            'boxture_delivery_notes',
            'boxture_pickup_addresses',
            'boxture_pickup_notes',
            'boxture_qa_api_url',
            'boxture_qa_api_key',
            'boxture_show_quantity_selector',
            'boxture_quantity_selector_options',
            'home_delivery_enable_debug',
        ),
    );

    public function settings($service = null)
    {
        $current_service = get_business_meta($this->business_id, 'home_delivery_service');
        if (empty($service)) {
            $service = $current_service;
        }

        $content = array();
        $content['current_service'] = $current_service;
        $content['service'] = $service;
        $content['services'] = Factory::listServices();

        if (isset($this->valid_settings[$service])) {
            foreach ($this->valid_settings[$service] as $key) {
                $content[$key] = get_business_meta($this->business_id, $key);
            }
        }

        if ($service == 'droplocker') {
            $content = $this->setupDLHomeDelivery($content);
        }

        $this->setPageTitle('Home Delivery Settings');
        $this->renderAdmin('settings', $content);
    }

    protected function setupDLHomeDelivery($content)
    {
        $this->load->model('deliveryzone_model');
        $this->load->model('deliverywindow_model');
        $this->load->model('location_model');

        $content['delivery_zones'] = array();
        $zones = $this->deliveryzone_model->get_aprax(array('business_id' => $this->business_id));

        foreach ($zones as $zone) {
            $zone->points = json_decode($zone->points);
            $zone->windows = array();

            $windows = $this->deliverywindow_model->get_aprax(array(
                'delivery_zone_id' => $zone->delivery_zoneID,
            ));

            foreach ($windows as $window) {
                $zone->windows[] = array(
                    'delivery_windowID' => $window->delivery_windowID,
                    'start' => $window->start,
                    'end' => $window->end,
                    'route' => $window->route,
                    'capacity' => array(
                        'monday' => $window->monday,
                        'tuesday' => $window->tuesday,
                        'wednesday' => $window->wednesday,
                        'thursday' => $window->thursday,
                        'friday' => $window->friday,
                        'saturday' => $window->saturday,
                        'sunday' => $window->sunday,
                    ),
                );
            }

            $content['delivery_zones'][] = $zone;
        }

        $locations = $this->location_model->get_aprax(array(
            'business_id' => $this->business_id,
            'serviceType' => Location::getHomeDeliveryServiceTypes(),
        ));

        $serviceTypes = Location::listServiceTypes();

        $locationsArray = array();
        foreach ($locations as $location) {
            $key = $serviceTypes[$location->serviceType];
            $locationsArray[$key][$location->locationID] = $location->address .' ['.$location->city.']';
            natcasesort($locationsArray[$key]);
        }

        $content['locations'] = $locationsArray;
        $content['center'] = $this->getCenterCoords($locations);

        return $content;
    }

    protected function getCenterCoords($locations)
    {
        $point = (object) array('lat' => 0, 'lon' => 0);
        $min_lat = 999;
        $min_lon = 999;
        $max_lat = -999;
        $max_lon = -999;

        foreach ($locations as $location) {
            if ($location->lat == '' || $location->lon == '') {
                continue;
            }

            $min_lat = min($min_lat, $location->lat);
            $min_lon = min($min_lon, $location->lon);
            $max_lat = max($max_lat, $location->lat);
            $max_lon = max($max_lon, $location->lon);
        }

        $point->lat = ($min_lat + $max_lat) / 2;
        $point->lon = ($min_lon + $max_lon) / 2;

        return $point;
    }

    public function set_current_service()
    {
        update_business_meta($this->business_id, 'home_delivery_service', $_POST['home_delivery_service']);
        set_flash_message('success', 'Home delivery service has been updated');

        redirect_with_fallback(("/admin/home_delivery/settings"));
    }

    public function update_settings($service)
    {
        if (!empty($_POST)) {

            foreach ($this->valid_settings[$service] as $key) {

                switch ($key) {
                    case 'rickshaw_pickup_addresses':
                    case 'boxture_pickup_addresses':
                        $value = array_filter($_POST[$key], function ($value) { return !empty($value['address']); });
                        $value = serialize($value);
                    break;

                    case 'deliv_window_notes':
                        $value = array_filter($_POST[$key], function ($value) { return !empty($value['notes']) && !empty($value['days']); });
                        $value = serialize($value);
                    break;

                    default: $value = $_POST[$key];
                }

                update_business_meta($this->business_id, $key, $value);

            }
            set_flash_message('success', 'Home delivery settings had been updated');
        }

        redirect_with_fallback(("/admin/home_delivery/settings"));
    }

    public function add_geofences()
    {

        $this->load->model('deliveryzone_model');
        $this->load->model('deliverywindow_model');

        // We store the existing ids to know if we need to insert, update or delete
        $delivery_zones = array();
        $delivery_windows = array();
        foreach ($this->deliveryzone_model->get_delivery_zones($this->business_id) as $row) {
            $delivery_zones[$row->delivery_zoneID] = $row->delivery_zoneID;
            $delivery_windows[$row->delivery_windowID] = $row->delivery_windowID;
        }

        $days = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');
        $zones = $this->input->post('zones');
        foreach ($zones as $zone) {

            $data = array(
                'business_id' => $this->business_id,
                'location_id' => $zone['location_id'],
                'color'       => $zone['color'],
                'points'      => $zone['points'],
            );

            // Its an existing zone, we'll do an update
            if (!empty($zone['delivery_zoneID'])) {
                $data['delivery_zoneID'] = $zone['delivery_zoneID'];
                $this->deliveryzone_model->save($data);

                // store the existing id
                $zone_id = $zone['delivery_zoneID'];

                // remve from the array of unused zones
                unset($delivery_zones[$zone['delivery_zoneID']]);
            } else {
                $zone_id = $this->deliveryzone_model->save($data);
            }

            if (!empty($zone['windows'])) {
                foreach ($zone['windows'] as $window) {

                    $data = array(
                        'delivery_zone_id' => $zone_id,
                        'start' => $window['start'],
                        'end' => $window['end'],
                        'route' => $window['route'],
                        'monday' => $window['capacity']['monday'],
                        'tuesday' => $window['capacity']['tuesday'],
                        'wednesday' => $window['capacity']['wednesday'],
                        'thursday' => $window['capacity']['thursday'],
                        'friday' => $window['capacity']['friday'],
                        'saturday' => $window['capacity']['saturday'],
                        'sunday' => $window['capacity']['sunday'],
                    );

                    if (!empty($window['delivery_windowID'])) {

                        $data['delivery_windowID'] = $window['delivery_windowID'];
                        unset($delivery_windows[$window['delivery_windowID']]);
                    }

                    $this->deliverywindow_model->save($data);
                }
            }
        }

        foreach ($delivery_windows as $delivery_windowID) {
            $this->deliverywindow_model->delete_aprax(array('delivery_windowID' => $delivery_windowID));
        }

        foreach ($delivery_zones as $delivery_zoneID) {
            $this->deliveryzone_model->delete_aprax(array('delivery_zoneID' => $delivery_zoneID));
        }

        redirect_with_fallback(("/admin/home_delivery/settings"));
    }

    public function locations()
    {
        $deliv = $this->getDeliv();

        $content = array();
        $content['stores'] = $deliv->get('/stores');

        if (!empty($content['stores']->error)) {
            $error = $content['stores']->error;
            set_flash_message_now('error', "Deliv API Error: <b>'$error'</b>. Check <a href='/admin/home_delivery/settings/deliv'>Deliv API settings</a>");
            $content['stores'] = array();
        }

        foreach ($content['stores'] as $store) {
            $store->locations = Location::search_aprax(array('deliv_id' => $store->id));

            $address = trim(str_replace(array('Street', 'Avenue', 'Boulevard'), '', $store->address_line_1));
            $store->matchedLocations = Location::search_aprax(array(
                'address LIKE' => '%' . $address . '%',
                'business_id' => $this->business_id,
                "COALESCE(deliv_id, '') != " => $store->id,
            ));
        }

        $this->renderAdmin('locations', $content);
    }

    /**
     * Creates an instance of Deliv Client
     *
     * @return \DropLocker\RestClient\DelivAPI
     */
    protected function getDeliv()
    {
        $sandbox = DEV || (get_business_meta($this->business_id, "deliv_mode") == 'test');
        if ($sandbox) {
            $api_key = get_business_meta($this->business_id, "deliv_sandbox_key");
        } else {
            $api_key = get_business_meta($this->business_id, "deliv_api_key");
        }

        if (empty($api_key)) {
            set_flash_message('error', 'Missing deliv API key, please setup Deliv first');
            redirect('/admin/home_delivery/settings');
        }

        $deliv = new \DropLocker\RestClient\DelivAPI($api_key, $sandbox);
        $deliv->version = 2;

        return $deliv;
    }
}