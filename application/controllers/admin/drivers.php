<?php

class Drivers extends MY_Admin_Controller
{

    public function manifest()
    {
        $routes = $this->input->get('routes');
        if (empty($routes)) {
            $routes = array();
        }

        $day = $this->input->get('day');
        if (empty($day)) {
            $day = date("N");
        }

        $type = $this->input->get('type');
        if (empty($type)) {
            $type = 'both';
        }

        $no_map = $this->input->get('no_map');
        $loaded = $this->input->get('loaded');

        //$updateSort = $type == 'both';  WHY???? Removed by dwb 2017-06-01
        $updateSort = false; 

        $this->load->model('location_model');

        $stops = array();
        $driverNotes = array();

        if (!empty($routes)) {
            $this->load->library('routelibrary');
            $stops = $this->routelibrary->getStops($routes, $this->business_id, $day, $type, $loaded, $updateSort);
            $driverNotes = $this->routelibrary->getDriverNotes($routes, $this->business_id);
        }

        $ordersByType = array();
        foreach ($stops as $stop) {
            foreach ($stop->deliveries as $order) {

                if (!isset($ordersByType[$order->orderType])) {
                    $ordersByType[$order->orderType] = array();
                }

                $ordersByType[$order->orderType][] = $order;
            }
        }


        $content                    = array();
        $content['companyName']     = $this->business->companyName;
        $content['routes']          = $this->location_model->get_routes($this->business_id);
        $content['selected_routes'] = $routes;
        $content['selected_day']    = $day;
        $content['selected_type']   = $type;
        $content['loaded']          = $loaded;
        $content['no_map']          = $no_map;
        $content['types']           = array('both' => 'Both', 'pickups' => 'Pickups', 'deliveries' => 'Deliveries');
        $content['stops']           = $stops;
        $content['center']          = $this->getCenterCoords($stops);
        $content['ordersByType']    = $ordersByType;
        $content['driverNotes']     = $driverNotes;

        $this->data['content'] = $this->load->view('admin/drivers/manifest', $content);
    }

    protected function getCenterCoords($stops)
    {
        $point = (object) array('lat' => 0, 'lon' => 0);
        $min_lat = 999;
        $min_lon = 999;
        $max_lat = -999;
        $max_lon = -999;

        foreach ($stops as $stop) {
            if ($stop->lat == '' || $stop->lon == '') {
                continue;
            }

            $min_lat = min($min_lat, $stop->lat);
            $min_lon = min($min_lon, $stop->lon);
            $max_lat = max($max_lat, $stop->lat);
            $max_lon = max($max_lon, $stop->lon);
        }

        $point->lat = ($min_lat + $max_lat) / 2;
        $point->lon = ($min_lon + $max_lon) / 2;

        return $point;
    }

    public function update_stops()
    {
        $stops = $this->input->post('stops');

        $this->load->library('routelibrary');

        $reload = false;
        foreach ($stops as $stop) {
            if ($objectType == 'Location' && $route != $currentStop->route_id) {
                $currentStop->route_id = $route;
                $currentStop->sortOrder = $sortOrder;
                $currentStop->save();
            } else {
                $this->routelibrary->updateSortOrder($stop['objectType'], $stop['sortOrder_id'], $stop['sortOrder']);
            }
        }

        $this->outputJSONsuccess('Updated sortOrder', array('reload' => $reload));
    }

}