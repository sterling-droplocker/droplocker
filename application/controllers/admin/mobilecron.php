<?php

use DropLocker\Util\Lock;

class MobileCron extends CI_Controller
{

    public function processTimeCards()
    {
        if (!Lock::acquire('processTimeCards')) {
            throw new \Exception("TimeCard lockfile present. Cannot processes new timecard entries");
        }
        $timeCard = new \App\Libraries\DroplockerObjects\TimeCard();
        $droidTimeCardEntry = new \App\Libraries\DroplockerObjects\DroidPostTimeCard();
        $entries = $droidTimeCardEntry->getPending();

        foreach ($entries as $entry) {
            try {
                $timeCard->processEntry($entry);
            } catch (\Exception $e) {
                send_exception_report($e);
            }
        }
    }


    /**
    * gets all the pickups reminders from the database and processes them
    *
    * @return boolean
    */
    public function processAllPickupReminders()
    {
        $this->load->model('droidpostpickupreminders_model');
        $reminders = $this->droidpostpickupreminders_model->get(array('status'=>'pending'));

        if (!$reminders) {
            return true;
        }

        foreach ($reminders as $reminder) {
            $this->droidpostpickupreminders_model->processPickupReminder($reminder);
        }
    }


    /**
    * Processes all the return to office reminders
    *
    * @return boolean
    */
    public function processAllReturnToOffices()
    {
        $this->load->model('droidpostreturntooffice_model');
        $reminders = $this->droidpostreturntooffice_model->get(array('status'=>'pending'));

        if (!$reminders) {
            return true;
        }

        foreach ($reminders as $reminder) {
            $this->droidpostreturntooffice_model->processReturnToOffice($reminder);
        }
    }


    /**
     * processes all claim notes
     */
    public function processClaimNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllClaimNotes();
    }

    /**
     * Process all the delivery notes
     */
    public function processDeliveryNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllDeliveryNotes();
    }


    /**
     * processes all the driver notes
     *
     * @return boolean
     */
    public function processDriverNotes()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllDriverNotes();
    }

    /**
     * processes all issues sent from drivers
     *
     * @return boolean
     */
    public function processIssues()
    {
        $this->load->model('droidpostdrivernotes_model');
        $this->droidpostdrivernotes_model->processAllIssues();

    }



}
