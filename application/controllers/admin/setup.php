<?php

header('HTTP/1.0 404 Not Found');
exit();

use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\Business;

class Setup extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        //template and display
        $this->load->model('menus/admin_menu');
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write('page_title','Server Setup');
        $this->template->write_view('menu', 'inc/admin_menu', array('menu'=>$this->admin_menu->menu));
        $this->data['submenu'] = $this->load->view('inc/admin_submenu', array("submenu"=>$this->admin_menu->get_submenu()), true);


    }
    public function index()
    {
        $content = array();
        $content['modes'] = Server_Meta::get_valid_modes_as_codeigniter_dropdown();
        $this->data['content'] = $this->load->view("admin/setup/index", $content, true);

        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    public function update_server_metadata()
    {
        $this->load->library("form_validation");

        $this->load->model("customer_model");

        $this->form_validation->set_rules("mode", "Mode", "required");
        $this->form_validation->set_rules("email_host", "Email Host", "required");
        $this->form_validation->set_rules("email_user", "Email User", "required");
        $this->form_validation->set_rules("email_password", "Email Password", "required");
        $this->form_validation->set_rules("email", "Superuser Email", "required");
        $this->form_validation->set_rules("password", "Superuser Password", "Required");

        $this->form_validation->set_rules("companyName", "Company Name", "required");


        if ($this->form_validation->run()) {
            $mode = $this->input->post("mode");

            $email_host = $this->input->post("email_host");
            $email_user = $this->input->post("email_user");
            $email_password = $this->input->post("email_password");

            $employee = new Employee();
            $employee->email = $this->input->post("email");
            $employee->manager_id = 0;
            if ($this->input->post("firstName")) {
                $employee->firstName = $this->input->post("firstName");
            }
            if ($this->input->post("lastName")) {
                $employee->lastName = $this->input->post("lastName");
            }

            $employee->superadmin = 1;

            $employee->password = $this->input->post("password");
            $employee->save();


            $business = new Business();
            $business->companyName = $this->input->post("companyName");
            $business->blank_customer_id = 0;
            $business->timezone = "America/Los_Angeles";
            $business->save();


            $business_employee = new Business_Employee();
            $business_employee->employee_id = $employee->{Employee::$primary_key};
            $business_employee->aclRole_id = 41; //Note, 41 is expected to be the role of 'Admin'
            $business_employee->default = 1;
            $business_employee->business_id= $business->{Business::$primary_key};
            $business_employee->save();

            $this->business_id = $business->{Business::$primary_key};

            Server_Meta::set("mode", $mode);
            Server_Meta::set("email_host", $email_host);
            Server_Meta::set("email_user", $email_user);
            Server_Meta::set("email_password", $email_password);
            set_flash_message("success", "Sucessfully set up server metadata and created Superadmin account");

            $this->load->helper("employee_helper");

            switch_to_employee($employee->{Employee::$primary_key});

            redirect("/admin/superadmin");
        } else {
            set_flash_message("error", validation_errors());
            redirect($this->agent->referrer());
        }
    }
}
