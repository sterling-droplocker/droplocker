<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\DropLockerEmail;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\BusinessLaundryPlan;
use App\Libraries\DroplockerObjects\BusinessEverythingPlan;
use App\Libraries\DroplockerObjects\BusinessProductPlan;
use App\Libraries\DroplockerObjects\CreditCard;

class Customers extends MY_Admin_Controller
{
    public $pageTitle = "Customers";

    private $customer_report_dataTable_columns;

    public function __construct()
    {
        parent::__construct();

        // if the third segment is numeric, we redirect to the customer details page.
        // On that page if the customer is not found it redirects to this page without the third segment
        $customer_id = $this->uri->segment(3);
        if (is_numeric($customer_id)) {
            redirect("/admin/customers/detail/" . $customer_id);
        }

        $this->load->model("customer_model");

        $this->customer_report_dataTable_columns = array(
            array("display_name" => "CustomerID", "query_name" => "customerID"),
            array("display_name" => "First Name", "query_name" => "firstName"),
            array("display_name" => "Last Name", "query_name" => "lastName"),
            array("display_name" => "Address 1", "query_name" => "address1"),
            array("display_name" => "Address 2", "query_name" => "address2"),
            array("display_name" => "City", "query_name" => "city"),
            array("display_name" => "State", "query_name" => "state"),
            array("display_name" => "ZIP", "query_name" => "zip"),
            array("display_name" => "Email", "query_name" => "email"),
            array("display_name" => "Phone", "query_name" => "phone"),
            array("display_name" => "Username", "query_name" => "username"),
            array("display_name" => "Signup Date", "query_name" => 'signupDate'),
                //"CONVERT_TZ(signupDate, 'GMT', '{$this->business->timezone}') AS formatted_signupDate"),
            array("display_name" => "SMS", "query_name" => "sms"),
            array("display_name" => "How Referred", "query_name" => "how"),
            array("display_name" => "Wash and Fold Notes", "query_name" => "wfNotes"),
            array("display_name" => "Dry Clean Notes", "query_name" => "dcNotes"),
            array("display_name" => "Latitude", "query_name" => "lat"),
            array("display_name" => "Longitude", "query_name" => "lon"),
            array("display_name" => "Birthday", "query_name" => "birthday"),
            array("display_name" => "Gender", "query_name" => "sex"),
            array("display_name" => "Kiosk Access", "query_name" => "kioskAccess"),
            array("display_name" => "Referred By", "query_name" => "referredBy"),
            array("display_name" => "Default Wash and Fold", "query_name" => "defaultWF"),
            array("display_name" => "On Hold", "query_name" => "hold"),
            array("display_name" => "Customer Notes", "query_name" => "customerNotes"),
            array("display_name" => "Preferences", "query_name" => "preferences"),
            array("display_name" => "Internal Notes", "query_name" => "internalNotes"),
            array("display_name" => "Language", "query_name" => "default_business_language_id"),
            array("display_name" => "Mail List", "query_name" => "CASE WHEN noEmail = 1 THEN 'NO' ELSE '' END AS noEmail_result")
        );
    }

    public function apply_coupon_to_all_form()
    {
        //filters
        $getFilters = "select * from filter where business_id = ?";
        $query = $this->db->query($getFilters, array($this->business_id));
        $content['filters'] = array();
        $content['filters']["all"] = "All";
        foreach ($query->result() as $r) {
            $content['filters'][$r->filterID] = $r->name;
        }

        $this->load->model('coupon_model');
        $coupons = $this->coupon_model->get_active_coupons($this->business_id);

        $content['coupons'] = array();
        foreach ($coupons as $coupon) {
            $content['coupons'][$coupon->couponID] = sprintf("%s%s", $coupon->prefix, $coupon->code);
        }
        $this->data['content'] = $this->load->view('admin/customers/apply_coupon_to_all_form', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    /**
     * Used to retrieve the columns for the customers report.
     */
    public function get_customer_report_dataTables_columns()
    {
        $result = array();

        $counter = 0;
        foreach ($this->customer_report_dataTable_columns as $index => $aColumn) {
            $counter = $index;
            $result[$index]['sTitle'] = $aColumn['display_name'];
        }
        $result[++$counter]['sTitle'] = "# of Orders"; //Note, these columns are custom fields.
        $result[$counter]['bSortable'] = false;
        $result[++$counter]['sTitle'] = "Days since Last Order";
        $result[$counter]['bSortable'] = false;
        $result[++$counter]['sTitle'] = "Gross Total Spent";
        $result[$counter]['bSortable'] = false;
        $result[++$counter]['sTitle'] = "Total Discounts";
        $result[$counter]['bSortable'] = false;
        echo json_encode($result);
    }
    /**
     * Used for server-side datatables data processing.
     * @throws Exception
     */
    public function get_customer_report_dataTables_data()
    {
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */

        $aColumns = array();
        foreach ($this->customer_report_dataTable_columns as $customer_report_dataTable_column) {
            $aColumns[] = $customer_report_dataTable_column['query_name'];
        }

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "customerID";

        /*
         * Paging
         */
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1' && is_numeric($_GET['iDisplayLength'])) {
            $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
        }

        /*
         * Ordering
         */
        $sOrder = "ORDER BY date DESC";
        if (isset($_GET['iSortCol_0'])) {
            $column_key = $this->input->get("iSortCol_0");
            //The following regular expression retrieves any alias used for the query. If there is an alias, then we use that alias in the ORDER BY clause
            preg_match("/ AS (.*)$/i", $this->customer_report_dataTable_columns[$column_key]['query_name'], $matches);
            if (empty($matches)) {
                $sOrder = "ORDER BY  " . $this->customer_report_dataTable_columns[$column_key]['query_name'] . " " . $this->input->get("sSortDir_0");
            } else {
                $alias = $matches[1];
                $sOrder = "ORDER BY  " . $alias . " " . $this->input->get("sSortDir_0");
            }
        }

        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */

        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sHaving = "HAVING (";
            $customer_report_dataTable_column_length = count($this->customer_report_dataTable_columns);
            for ($i = 0; $i < $customer_report_dataTable_column_length; $i++) {
                preg_match("/ AS (.*)$/i", $this->customer_report_dataTable_columns[$i]['query_name'], $matches);
                if (empty($matches)) {
                    $column_name = $this->customer_report_dataTable_columns[$i]['query_name'];
                } else {
                    $alias = $matches[1];
                    $column_name = $alias;
                }
                $sHaving .= "`$column_name` LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
            }
            //The following conditional applies a string search for both the firstName and lastName column if both were specified in the columns parameter.
            if (in_array("firstName", $aColumns) && in_array("lastName", $aColumns)) {
                $sHaving .= "CONCAT_WS(' ', firstName, lastName) LIKE \"%{$this->input->get("sSearch")}%\" OR ";
            }

            $sHaving = substr_replace($sHaving, "", -3);
            $sHaving .= ")";
        } else {
            $sHaving = "";
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                $sHaving .= "AND ";
                $sHaving .= "`" . $aColumns[$i] . "` LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
            }
        }

        /*
         * SQL queries
         * Get data to display
         */
        $select = "SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $aColumns);
        $sQuery = trim("
        $select
        FROM   customer
        WHERE business_id={$this->business_id}
                $sHaving
        $sOrder
        $sLimit
        ");
        $rQuery = $this->db->query($sQuery);
        $rResult = $rQuery->result();


        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() AS found_rows";

        $aResultFilterTotal = $this->db->query($sQuery)->row();
        $iFilteredTotal = $aResultFilterTotal->found_rows;

        /* Total data set length */
        $sQuery = "SELECT COUNT(`" . $sIndexColumn . "`) AS total FROM customer WHERE business_id = {$this->business_id}";
        $aResultTotal = $this->db->query($sQuery)->row();
        $iTotal = $aResultTotal->total;


        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        $date_format = get_business_meta($this->business_id, "fullDateDisplayFormat");
        foreach ($rResult as $aRow) {
            $row = array();
            $aColumns_total = count($aColumns);
            for ($i = 0; $i < $aColumns_total; $i++) {
                $regex = "/ AS (.*)$/i";
                preg_match($regex, $aColumns[$i], $matches);
                if (empty($matches)) {
                    if ($aColumns[$i] == 'signupDate') {
                        $row[] = convert_from_gmt_aprax($aRow->{$aColumns[$i]}, $date_format, $this->business_id);
                    } else {
                        $row[] = $aRow->{$aColumns[$i]};
                    }
                } else {
                    $alias = $matches[1];
                    $row[] = $aRow->$alias;
                }
            }



            //The following statement adds additional column data for the custom fields '# of Orders', 'Days since Last Order', 'Gross Total Spent', and 'Total Discounts'
            $customer_order_count = $this->customer_model->get_customer_order_count($aRow->customerID);
            $row[] = $customer_order_count;

            $date = $this->customer_model->get_last_order_date($aRow->customerID);
            $last_order_date = new DateTime($date, new DateTimeZone("GMT"));
            $current_date = new DateTime("now", new DateTimeZone("GMT"));
            $date_difference = date_diff($current_date, $last_order_date);
            $row[] = $date_difference->format("%a");

            $total_spent = $this->customer_model->get_order_total($aRow->customerID);
            $row[] = "$" . $total_spent->gross;
            $row[] = "$" . $total_spent->disc;
            $output['aaData'][] = $row;
        }

        $output['columns'] = $aColumns;
        echo json_encode($output);
    }

    public function assign_new_bag_upon_new_order()
    {
        if (!$this->input->get("customerID")) {
            throw new \InvalidArgumentException("'customerID' must be passed as a GET parameter.");
        }
        $customerID = $this->input->get("customerID");
        $customer = $this->getCustomerById($customerID);

        update_customer_meta($customer->customerID, "assign_new_bag_on_new_order", 1);
        set_flash_message("success", "Set " . getPersonName($customer) . " to receive new bag upon new order");
        redirect($this->agent->referrer());
    }
    /**
     * Expects the customer ID as the first URL parameter, and the location ID as the second URL parameter.
     */
    public function create_package_order($customer_id = null, $location_id = null)
    {
        //get the business default package delivery id
        $packageDeliveryID = get_business_meta($this->business_id, 'default_package_delivery_id');
        if (empty($packageDeliveryID)) {
            try {
                throw new \Exception("default_package_delivery_id is not set in the business_meta table for this business");
            } catch (\Exception $e) {
                send_exception_report($e);
                set_flash_message('error', 'You must set the package delivery product id before creating a package delivery order');
                redirect("/admin/admin/defaults");
            }
        }

        $this->load->model('order_model');

        $customer = $this->getCustomerById($customer_id);

        $get_last_order_information_query = $this->db->query("
            select lockerID, bagNumber from `orders`
            join locker on locker.lockerID = `orders`.locker_id
            join customer on customer.customerID = `orders`.customer_id
            join bag on bag.bagID = `orders`.bag_id
            where `orders`.customer_id = $customer_id
            and bag_id != 0
            order by `orders`.orderID desc
            limit 1");
        if ($this->db->_error_message()) {
            throw new \Database_Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $last_order_information = $get_last_order_information_query->result();
        if (empty($last_order_information)) {
            set_flash_message("error", "Customer must have placed at least one order in order to create a package.");
            redirect($this->agent->referrer());
        }

        $lockers = $this->db->query("select * from locker where location_id = $location_id and lockerName = 'inprocess'");
        if (!$lockers) {
            $this->session->set_flashdata("error", "The customer's default location has no electronic lockers.");
            redirect("/admin/customers/detail/$customer_id");
        } else {
            $bag = new Bag();
            $bag->customer_id = $customer_id;
            $bag->business_id = $this->business_id;
            $bag->save();
            $new_order_id = $this->order_model->new_order($location_id, $last_order_information[0]->lockerID, $bag->bagNumber, $this->session->userdata['employee_id'], "", $this->business_id, array("customer" => $customer_id));
            $this->load->model('orderitem_model');

            if ($this->input->post("tracking_number")) {
                $tracking_number_notes = "Tracking Number: " . $this->input->post("tracking_number");
            }

            // TODO: hardcoded $supplier_id with 10
            $result = $this->orderitem_model->add_item(
                $new_order_id,          // $order_id
                0,                      // $item_id
                $packageDeliveryID,     // $product_process_id
                1,                      // $qty
                $this->employee_id,     // $employee_id
                10,                     // $supplier_id
                0,                      // $price
                $tracking_number_notes  // $notes = null
            );

            if ($result['status'] == "success") {
                set_flash_message('success', 'Created package order');
            } else {
                set_flash_message('error', "Customer ID not found on detail page");
            }
            redirect("/admin/orders/details/$new_order_id");
        }
    }
    /**
     * Deletes a customer from the database.
     * Expects the customerID as segment 4 of the URL.
     * @throws Exception
     */
    public function delete($customer_id = null)
    {
        if (!is_numeric($customer_id)) {
            set_flash_message("error", "The customer ID must be numeric and passed as segment 4 of the URL.");
            $this->goBack("/admin/customers");
        } else {
            $customer = $this->getCustomerById($customer_id);

            $this->load->model("order_model");
            $order_count = $this->order_model->count(array("customer_id" => $customer->{Customer::$primary_key}));
            $this->load->model("claim_model");
            $claim_count = $this->claim_model->count(array("customer_id" => $customer->{Customer::$primary_key}, "active" => 1));
            if ($order_count > 0) {
                set_flash_message("error", "Can not delete customer because they have $order_count orders");
                $this->goBack("/admin/customers");
            } elseif ($claim_count > 0) {
                set_flash_message("error", "Can not delete customer because they have $claim_count active claims");
                $this->goBack("/admin/customers");
            } else {
                $this->load->model("claim_model");

                $customer->delete();
                set_flash_message("success", "Deleted customer '" . getPersonName($customer) . "'");

                //The following conditional checks if we are deleting the customer from the customer's own detail page, if so, then we redirect back to the main customer page, otherwise, we redirect back to the previous page.
                if (strstr($this->agent->referrer(), "/admin/customers/detail/$customer_id") === false) {
                    $this->goBack("/admin/customers");
                } else {
                    redirect("/admin/customers");
                }
            }
        }
    }
    /**
     * Note, A customer is considered 'disabled' if their email is null and autoPay is set to 0.
     * Expects the customer ID as segment of the URL
     * @throws \InvalidArgumentException
     */
    public function disable($customer_id = null)
    {
        if (!is_numeric($customer_id)) {
            throw new \InvalidArgumentException("'customer_id' must be numeric and passed as segment 4 of the URL");
        }

        // used only to check it belogns to this business
        $customer = $this->getCustomerById($customer_id, '/admin/customers');

        // don't use a DroplockerObject beause setting email to NULL causes a validate error
        $this->customer_model->save(array(
            'customerID' => $customer_id,
            'email' => null,
            'autoPay' => 0,
        ));

        set_flash_message("success", "Disabled customer " . getPersonName($customer) . " ({$customer->customerID})");
        $this->goBack("/admin/customers");
    }
    /**
     * UI for viewing all customers
     */
    public function index()
    {
        //find out which widgets to show
        $getWidgets = "select * from dashboard
                        left join widget on widgetID = widget_id
                        where employee_id = $this->buser_id
                        and module = 'Customers'
                        order by location";
        $result = $this->db->query($getWidgets);
        $content['widgets'] = $result->result();


        $this->data['content'] = $this->load->view('admin/index', $content, true);
        //$this->data['submenu'] = $this->load->view('inc/admin_submenu_minimized', array("submenu"=>$this->admin_menu->get_submenu()), true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Allows employees to log in as customers
     *
     * Domain name rules
     * ---------------
     * a domain will either be
     * 1. laundrylocker.com
     * or
     * 2. myaccount.<domain>.<tld>
     * or
     * 3. <slug>.droplocker.<tld>
     *
     * This is set in the business table website_url
     * If website_url is blank there domain name is rule 3
     *
     */
    public function switch_to_customer($customer_id = null)
    {
        $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);

        $url = get_business_account_url($this->business, '');
        $redirectUrl = "http://" . $url . "/main/set_user/$customer_id/$this->buser_id";

        redirect($redirectUrl);
    }

    /**
     * There is no functionality to add a customer to a business from the admin section
     * This UI is just a placeholder with a blank page.
     */
    public function add()
    {
        $this->load->model('customer_location_model');
        $business_locations = Location::search_aprax(array("business_id" => $this->business_id, "status" => "active"));

        $locations = array("" => "--None--");
        foreach ($business_locations as $business_location) {
            $locations[$business_location->locationID] = $business_location->address;
        }

        natsort($locations);
        $content['locations'] = $locations;

        $this->load->model("business_language_model");
        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $content['selected_business_language_id'] = $this->business->default_business_language_id;

        /* Load email templates into editor */
        $this->load->model("emailactions_model");
        $content['emailAction'] = current($this->emailactions_model->get_aprax(array('action' => 'new_customer')));
        $content['employee'] = $this->employee;

        $this->load->model("emailTemplate_model");
        $content['emailTemplates_by_business_language_for_action'] = $this->emailTemplate_model->get_templates_by_business_language_for_action_for_business($content['emailAction']->emailActionID, $this->business_id);

        $this->renderAdmin('add', $content);
    }

    public function add_submit()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("email", "Email", "required|valid_email");
        $this->form_validation->set_rules("password", "Password", "required");
        $this->form_validation->set_rules("default_business_language_id", "is_natural_not_zero");

        if ($this->form_validation->run()) {
            $this->load->model('customer_model');

            $email = trim($this->input->post("email"));
            $password = $this->input->post("password");
            $how = $this->input->post("how");
            $signup = "admin add";

            $sql = "SELECT email FROM customer WHERE business_id = ? AND email = ?";
            $q = $this->db->query($sql, array($this->business_id, $email));
            if ($res = $q->result()) {
                if (strtolower($res[0]->email) == strtolower($email)) {
                    set_flash_message("error", 'Email is already registered');
                    redirect("/admin/customers/add");
                }
            } else {
                if ($newCustomer_id = $this->customer_model->new_customer($email, $password, $this->business_id, $how, $signup)) {
                    if ($this->input->post("promo") != '') {
                        $this->load->library('coupon');
                        $response = $this->coupon->applyCoupon($this->input->post("promo"), $newCustomer_id, $this->business_id);
                        if ($response['status'] == 'fail') {
                            set_flash_message('error', "Promotional code " . $this->input->post("promo") . " was invalid.");
                        } else {
                            $this->load->model('coupon_model');
                            $discount = $this->coupon_model->get_discount_value_as_string($this->input->post("promo"), $this->business_id);
                            $successful_discount_application_message = "Promotional code '" . $this->input->post("promo") . " - $discount' was applied to your new account.";
                            set_flash_message('success', $successful_discount_application_message);
                        }
                    } else {//If no promotional code was entered, we display the default welcome message.
                        set_flash_message('success', 'New customer account created');
                    }

                    $new_customer = new Customer($newCustomer_id);

                    $new_customer->firstName = $this->input->post("firstName");
                    $new_customer->lastName = $this->input->post("lastName");
                    $new_customer->phone = $this->input->post("phone");
                    $new_customer->sms = $this->input->post("sms");
                    $new_customer->address1 = $this->input->post("address1");
                    $new_customer->address2 = $this->input->post("address2");
                    $new_customer->city = $this->input->post("city");
                    $new_customer->state = $this->input->post("state");
                    $new_customer->zip = $this->input->post("zip");
                    $new_customer->location_id = $this->input->post("location_id");
                    $new_customer->how = $this->input->post("how");
                    $new_customer->alertSent = 1;

                    $copy_sms = get_business_meta($this->business_id, 'copy_phone_to_sms');
                    if ($copy_sms && empty($new_customer->sms)) {
                        $new_customer->sms = $new_customer->phone;
                    }

                    if ($this->input->post("address1") && $this->input->post("city") && $this->input->post("state")) {
                        $geo = geocode($this->input->post("address1"), $this->input->post("city"), $this->input->post("state"));
                        $new_customer->lat = str_replace(',', '.', $geo['lat']);
                        $new_customer->lon = str_replace(',', '.', $geo['lng']);
                    }

                    if ($this->input->post("default_business_language_id")) {
                        $new_customer->default_business_language_id = $this->input->post("default_business_language_id");
                    } else {
                        $new_customer->default_business_language_id = $this->business->default_business_language_id;
                    }
                    $new_customer->save();
                    $this->session->set_userdata('user_id', $newCustomer_id);

                    if ($this->input->post("welcomeEmail") == 1) {
                        $emailTemplate = null;
                        // Use submited email template if not empty
                        if ($this->input->post("emailText") || $this->input->post("smsText")) {
                            $emailTemplate = (object)array(
                                'emailSubject' => $this->input->post("emailSubject"),
                                'emailText' => $this->input->post("emailText"),
                                'smsText' => $this->input->post("smsText"),
                            );
                        }

                        $action_array = array('do_action' => 'new_customer', 'customer_id' => $newCustomer_id, 'business_id' => $this->business_id, 'emailTemplate' => $emailTemplate);
                        Email_Actions::send_transaction_emails($action_array);
                    }

                    update_customer_meta($newCustomer_id, 'company', $this->input->post('company'));

                    redirect("/admin/customers/add");
                } else {
                    throw new Exception("Failed to create new Customer");
                }
            }
        } else {
            set_flash_message("error", validation_errors());
            redirect("/admin/customers/add");
        }
    }

    public function import()
    {
        if (!empty($_POST)) {
            $this->load->library("form_validation");

            $how = $_POST['how'];
            $promo = $_POST['promo'];
            $location_id = $_POST['location_id'];
            if (!empty($_POST['default_business_language_id'])) {
                $default_business_language_id = $_POST['default_business_language_id'];
            } else {
                $default_business_language_id = $this->business->default_business_language_id;
            }
            $signup = "admin import";

            $emailTemplate = null;
            // Use submited email template if not empty
            if ($_POST['emailText'] || $_POST['smsText']) {
                $emailTemplate = (object)array(
                    'emailSubject' => $_POST['emailSubject'],
                    'emailText' => $_POST['emailText'],
                    'smsText' => $_POST['smsText'],
                );
            }

            if ($promo) {
                $this->load->library('coupon');
                $this->load->model('coupon_model');
                $discount = $this->coupon_model->get_discount_value_as_string($promo, $this->business_id);
            }

            $data = $_POST['data'];
            if (!empty($_FILES['file']['tmp_name']) && $_FILES['file']['error'] == UPLOAD_ERR_OK) {
                $data = file_get_contents($_FILES['file']['tmp_name']);
            }

            $keys = array(
                'email', 'password', 'firstName', 'lastName', 'phone', 'sms', 'address1', 'address2',
                'city', 'state', 'zip'
            );

            $copy_sms = get_business_meta($this->business_id, 'copy_phone_to_sms');

            $imported = 0;
            $skipNotValid = 0;
            $skipAlreadyExists = 0;
            $errors = array();
            $messages = array();
            $lines = explode("\n", $data);
            foreach ($lines as $line) {
                $fields = explode(',', $line);

                if (empty($fields[0]) || trim($fields[0]) == 'Email*') {
                    continue;
                }

                $fields = array_combine(array_slice($keys, 0, count($fields)), $fields);

                $email = trim($fields['email']);
                $password = $fields['password'];
                if (empty($password)) {
                    $password = md5(uniqid());
                }

                unset($fields['email']);
                unset($fields['password']);

                if (!$this->form_validation->valid_email($email)) {
                    $errors[] = "'$email' is not a valid email address.";
                    $skipNotValid++;
                    continue;
                }

                $sql = "SELECT email FROM customer WHERE business_id = ? AND email = ?";
                $found = $this->db->query($sql, array($this->business_id, $email))->result();
                if ($found) {
                    $errors[] = "A customer with email $email is already registered.";
                    $skipAlreadyExists++;
                    continue;
                }

                if (!$newCustomer_id = $this->customer_model->new_customer($email, $password, $this->business_id, $how, $signup)) {
                    throw new Exception("Failed to create new Customer");
                }

                $imported++;

                $link = "<a href='/customers/detail/$newCustomer_id'>" . getPersonName($fields) . "</a>";
                $messages[] = "Customer $link with email $email created";

                if ($promo) {
                    $response = $this->coupon->applyCoupon($promo, $newCustomer_id, $this->business_id);
                    if ($response['status'] == 'fail') {
                        $errors[] = "Promotional code $promo not applied to $link.";
                    } else {
                        $messages[] = "Promotional code '$promo - $discount' was applied to $link.";
                    }
                }

                $fields['location_id'] = $location_id;
                $fields['how'] = $how;
                $fields['alertSent'] = 1;
                $fields['default_business_language_id'] = $default_business_language_id;

                if ($copy_sms && empty($fields['sms'])) {
                    $fields['sms'] = $fields['phone'];
                }

                if (!empty($fields['address1']) && !empty($fields['city']) && !empty($fields['state'])) {
                    $geo = geocode($fields['address1'], $fields['city'], $fields['state']);
                    $fields['lat'] = str_replace(',', '.', $geo['lat']);
                    $fields['lon'] = str_replace(',', '.', $geo['lng']);
                }

                $fields['customerID'] = $newCustomer_id;
                $this->customer_model->save($fields);

                if ($_POST['welcomeEmail'] == 1) {
                    Email_Actions::send_transaction_emails(array(
                        'do_action' => 'new_customer',
                        'customer_id' => $newCustomer_id,
                        'business_id' => $this->business_id,
                        'emailTemplate' => $emailTemplate
                    ));
                }
            }

            if ($imported) {
                set_flash_message("success", "$imported customers imported, Not valid $skipNotValid, Already exists $skipAlreadyExists.");
            } else {
                set_flash_message("error", "No customers imported, Not valid $skipNotValid, Already exists $skipAlreadyExists.");
            }

            $this->goBack('/customers/import');
        }


        $business_locations = Location::search_aprax(array(
            "business_id" => $this->business_id,
            "status" => "active"
        ));

        $locations = array("" => "--None--");
        foreach ($business_locations as $business_location) {
            $locations[$business_location->locationID] = $business_location->address;
        }
        $content['locations'] = $locations;

        $this->load->model("business_language_model");
        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);
        $content['selected_business_language_id'] = $this->business->default_business_language_id;

        /* Load email templates into editor */
        $this->load->model("emailactions_model");
        $content['emailAction'] = current($this->emailactions_model->get_aprax(array('action' => 'new_customer')));
        $content['employee'] = $this->employee;

        $this->load->model("emailTemplate_model");
        $content['emailTemplates_by_business_language_for_action'] = $this->emailTemplate_model->get_templates_by_business_language_for_action_for_business($content['emailAction']->emailActionID, $this->business_id);


        $this->renderAdmin('import', $content);
    }


    /**
     * display the transaction for a customer
     */
    public function transactions($customer_id = null)
    {
        $customer = $this->getCustomerById($customer_id);

        $this->load->model('transaction_model');
        $content['transactions'] = $this->transaction_model->get_aprax(array('customer_id' => $customer_id));
        $content['customer_name'] = getPersonName($customer);

        $this->renderAdmin('transactions', $content);
    }

    /**
     * UI for viewing the customers
     */
    public function view($not_used = 50, $offset = 0)
    {
        $limit = 20;
        $total = $this->customer_model->customerCount();
        $this->load->library('pagination');
        $config['base_url'] = '/admin/customers/view/50/';
        $config['total_rows'] = $total;
        $config['per_page'] = $limit;
        $config['num_links'] = 10;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);

        $content['total'] = $total;
        $content['paging'] = $this->pagination->create_links();

        $options['business_id'] = $this->business_id;
        $options['limit'] = $limit;
        $options['offset'] = $offset;
        $options['sort'] = 'lastName';
        $options['select'] = 'customerID, customer.firstName, customer.lastName, customer.phone, customer.email, customer.username, customer.signupDate, customer.sms, customer.default_business_language_id ';
        $content['customers'] = $this->customer_model->get_business_customers($options);
        $content['timezone'] = $this->business->timezone;

        $this->renderAdmin('view', $content);
    }


    public function removeBounceBlock($customer_id = null)
    {
        if (empty($customer_id)) {
            set_flash_message('error', "Missing the customer ID");
            redirect("/admin/customers");
        }

        $customer = $this->getCustomerById($customer_id);
        $customer->bounced = 0;
        $customer->save();

        set_flash_message('success', "The email account for this customer has been un-blocked");
        redirect("/admin/customers/edit/" . $customer_id);
    }

    public function list_emails($customer_id = null)
    {
        $customer = $this->getCustomerById($customer_id);

        $content['customer'] = $customer;
        $content['customer_id'] = $customer_id;

        $content['emails'] = DroplockerEmail::getCustomerEmailsAndSMS($customer);

        $this->data['content'] = $this->load->view('admin/customers/list_emails', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * UI for viewing the details about an email
     *
     * requires url segment 4 to be set with the emailID associated with the email table
     *
     */
    public function email_detail($email_id = null)
    {
        if (!is_numeric($email_id)) {
            $message = get_translation(
                'required_numeric_email_id',
                'admin_customers/email_detail',
                array(),
                "Email id should be numeric"
            );

            set_flash_message('error', $message);
            redirect("/admin/customers");
        }

        $content['email'] = $email = new DropLockerEmail($email_id);

        if ($email->business_id != $this->business_id) {
            set_flash_message("error", "This email does not belong to your business.");
            redirect("/admin/customers");
        }

        $this->data['content'] = $this->load->view('admin/customers/email_detail', $content, true);
        $this->template->write('page_title', 'Email Details');
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * displays the details for a customer or adds a bag to this customer
     * including order details
     * Can take the following POST parameters
     *  bagNumber
     *  notes
     *
     */
    public function detail($customer_id = null)
    {
        if (!is_numeric($customer_id)) {
            set_flash_message("error", "Customer ID must be passed as segment 4 and must be an integer");
            return $this->goBack("/admin/customers");
        }

        if ($this->input->is_ajax_request()) {
            $this->ajax_detail_get_orders_history($customer_id);
            die();
        }

        //get customer details
        $content['customer'] = $customer = $this->getCustomerById($customer_id);

        if ($customer->referredBy) {
            try {
                $referral = new Customer($customer->referredBy);
                $content['referral'] = $referral;
            } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                $customer->referredBy = null;
                $customer->save();
            }
        }

        $getCustTotals = "select * from reports_customer where customerID = ?";
        $query = $this->db->query($getCustTotals, array($customer_id));
        $content['custTotals'] = $query->result();

        // get customer bags
        $this->load->model('bag_model');
        $this->bag_model->customer_id = $customer_id;
        $content['bags'] = $this->bag_model->get();

        try {
            $content['defaultLocation'] = new Location($customer->location_id);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $content['defaultLocation'] = null;
        }

        // get customer notesHistory
        $this->load->model("customerhistory_model");
        $history = $this->customerhistory_model->get_history($customer_id, $this->business_id);
        $content['history'] = $history;
        $content['notes']['internalNotes'] = @$history[0]->note;

        $this->load->model("followupCall_model");

        $content['followupCalls'] = $this->followupCall_model->get_history($customer_id, $this->business_id, 10);

        //get creditcard
        $this->load->model('creditcard_model');
        $this->creditcard_model->select = 'cardNumber, expMo, expYr';
        $this->creditcard_model->customer_id = $customer_id;
        $content['cc'] = $this->creditcard_model->get();

        // get the customer email history
        $emailObj = new DropLockerEmail();
        $content['emailCount'] = $emailObj->count($customer->email, $customer_id, $this->business_id);
        $content['emails'] = $emailObj->getCustomerEmailsAndSMS($customer, 10);

        $content['business'] = $this->business;

        $content['rewardsViolation'] = get_customer_meta($customer_id, 'rewardsViolation');

        $this->load->model('delivery_customersubscription_model');
        $content['home_delivery_subscriptions'] = $this->delivery_customersubscription_model->get_subscriptions_array($customer_id);

        // used to display the delete button
        $this->db->where(array("customer_id" => $customer->customerID));
        $this->db->from("orders");
        $content['order_count'] = $this->db->count_all_results();

        $this->template->add_js("js/search.js");
        $this->renderAdmin('detail', $content);
    }

    public function edit_subscriptions($customer_id = null)
    {
        $content['customer'] = $customer = $this->getCustomerById($customer_id);

        $this->load->model('delivery_customersubscription_model');
        $this->load->model('deliveryzone_model');

        $subscriptions = $this->delivery_customersubscription_model->get_subscriptions($customer_id);
        $customer_subscriptions = array();
        foreach ($subscriptions as $subscription) {
            $customer_subscriptions[$subscription->delivery_zone_id] = $subscription->delivery_customerSubscriptionID;
        }

        $customer = new Customer($customer_id);
        if (empty($customer->lat) || empty($customer->lon)) {
            $geo = geocode($customer->address1, $customer->city, $customer->state);
            $customer->lat = str_replace(',', '.', $geo['lat']);
            $customer->lon = str_replace(',', '.', $geo['lng']);

            $customer->save();
        }

        if (empty($customer->lat) || empty($customer->lon)) {
            set_flash_message('error', "Unable to geocode the customer's address. Please update the customer address and try again.");
            return $this->goBack("/customers/detail/$customer_id");
        }

        if (!empty($_POST)) {
            foreach ($_POST['routes'] as $delivery_zone_id) {
                if (empty($delivery_zone_id)) {
                    continue;
                }

                // if subscription exists, skip it
                if (!empty($customer_subscriptions[$delivery_zone_id])) {
                    $this->delivery_customersubscription_model->save(array(
                        'delivery_customerSubscriptionID' => $customer_subscriptions[$delivery_zone_id],
                        'notes' => $this->input->post('notes'),
                    ));

                    unset($customer_subscriptions[$delivery_zone_id]);
                    continue;
                }

                $this->delivery_customersubscription_model->save(array(
                    'customer_id' => $customer->customerID,
                    'delivery_zone_id' => $delivery_zone_id,
                    'address1' => $customer->address1,
                    'address2' => $customer->address2,
                    'city' => $customer->city,
                    'state' => $customer->state,
                    'zip' => $customer->zip,
                    'lat' => $customer->lat,
                    'lon' => $customer->lon,
                    'sortOrder' => 0,
                    'notes' => $this->input->post('notes'),
                ));
            }

            // Delete all unchecked subscriptions
            foreach ($customer_subscriptions as $zone_id => $delivery_customerSubscriptionID) {
                $this->delivery_customersubscription_model->delete_aprax(array('delivery_customerSubscriptionID' => $delivery_customerSubscriptionID));
            }

            set_flash_message('success', 'Cusotmer subscriptions updated');
            redirect("/admin/customers/edit_subscriptions/$customer_id");
        }

        $first = current($customer_subscriptions);

        $content['customer_subscriptions'] = $customer_subscriptions;
        $content['notes'] = $first ? $subscriptions[$first]->notes : '';

        $content['routes'] = $this->deliveryzone_model->get_all_routes_array($this->business_id);

        $this->renderAdmin('edit_subscriptions', $content);
    }

    /**
     * Displays customer orders formatted for dataTables
     * @param  int  $customer_id
     * @return json string
     */
    protected function ajax_detail_get_orders_history($customer_id = null)
    {
        $customer_id = (int)$customer_id;

        //sort columns
        $orderBy = "orderID";
        $orderDirection = "DESC";

        $colSelected = $this->input->get('iSortCol_0');
        $colDirection = $this->input->get('sSortDir_0');
        if (!empty($colDirection)) {
            $orderDirection = $colDirection;
        }
        switch (intval($colSelected)) {
            case 0:
                $orderBy = "orderID";
                break;
            case 1:
                  //$orderBy = "orderType";
                break;
            case 2:
                $orderBy = "dateCreated";
                break;
            case 3:
                $orderBy = "bagNumber";
                break;
            case 4:
                $orderBy = "orderPaymentStatusOption_id";
                break;
            case 5:
                $orderBy = "orderID";
                break;
        }

        $records = array(
            "sEcho" => (int)$this->input->get("sEcho"),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );
        $dateFormat = $this->input->get("dateFormat");
        $page_from = $this->input->get("iDisplayStart");
        $limit = $this->input->get("iDisplayLength");

        //get paginated results
        $records["aaData"] = $this->customer_model->getOrderHistory($customer_id, $orderBy, $orderDirection, $limit, $page_from);

        //add custom data/format to rows
        $this->load->model('order_model');
        foreach ($records["aaData"] as $index => $order) {
            if ($order->isClaim) {
                $order->order_type = "Claim";
                $order->orderLink = '<a href="/admin/orders/claimed">' . $order->orderID . '</a>';
                $order->dateCreated = convert_from_gmt_aprax($order->dateCreated, $dateFormat);
            } else {
                $order->order_type = $this->order_model->getOrderType($order->orderID);
                $order->orderLink = '<a href="/admin/orders/details/' . $order->orderID . '">' . $order->orderID . '</a>';
                $order->dateCreated = convert_from_gmt_aprax($order->dateCreated, $dateFormat);
            }

            if (!empty($order->notes)) {
                $order->bagNumber .= "\n(" . $order->notes . ")";
            }


            if ($order->orderPaymentStatusOption_id == 3) {
                $order->orderPaymentStatusOptionIcon = '<img src="/images/icons/accept.png" />';
            } else {
                $order->orderPaymentStatusOptionIcon = '<img src="/images/icons/error.png" />';
            }
        }

        //return datables info
        $records["iTotalRecords"] = $this->customer_model->getOrderHistoryCount($customer_id);
        $records["iTotalDisplayRecords"] = $records["iTotalRecords"];

        echo json_encode($records);
    }

    /**
     * turns on or off the customers lockerloot status
     * @throws \Exception
     */
    public function toggleLockerLoot()
    {
        $customer_id = intval($_POST['customer_id']);
        $customer = $this->getCustomerById($customer_id);

        if (isset($_POST['rewardSubmit'])) {
            if ($_POST['status'] == "OFF") {
                update_customer_meta($customer_id, "rewardsViolation", 1);
                set_flash_message('success', 'Customer reward program status has been turned OFF');
            } else {
                update_customer_meta($customer_id, "rewardsViolation", '');
                set_flash_message('success', 'Customer reward program status has been turned ON');
            }

            redirect("/admin/customers/detail/" . $customer_id);
        }
    }

    /**
     * Updates the customer notes in customerPreference table and updates the customer.llNotes
     * If there is a llNote for this customer, we move it to the customerHistory table
     * Expects the following POST Parameters:
     *  customer_id
     * Can take the following optional parameters
     *  customerNotes
     *  permAssemblyNotes
     */
    public function update_notes()
    {
        if (!$this->input->post("customer_id")) {
            throw new Exception("Missing 'customer_id' POST parameter.");
        }
        $customer_id = $this->input->post("customer_id");
        if (!is_numeric($customer_id)) {
            throw new Exception("'customer_id' must be numeric.");
        }

        $customer = $this->getCustomerById($customer_id);

        $this->load->model('customer_model');
        $this->load->model("customerhistory_model");
        $new_customerNotes = trim($this->input->post("customerNotes"));
        if ($customer->customerNotes != $new_customerNotes) {
            if (empty($new_customerNotes)) {
                $customer->customerNotes = "";
            } else {
                $customerNotes = trim($this->input->post("customerNotes"));
                $customer->customerNotes = $customerNotes;
            }

            $customer->save();
            $this->customerhistory_model->insert($customerNotes, $customer_id, 'Customer Note', $this->business_id);
        }

        /**
         * permAssemblyNotes are added to the customer table and then archived in the customerHistoryTable
         */
        if ($this->input->post("permAssemblyNotes")) {
            $this->customer_model->updatePermAssemblyNotes($_POST['permAssemblyNotes'], $_POST['customer_id'], $this->business_id);
        } else {
            $customer->permAssemblyNotes = "";
            $customer->save();
        }


        /**
         * update the internal note
         */
        if ($this->input->post("internalNotes")) {
            $internalNotes = trim($this->input->post("internalNotes"));
            if (empty($internalNotes)) {
                $customer = new Customer($customer_id);
                $customer->internalNotes = "";
                $customer->save();
            } else {
                $this->customer_model->internalNotes = $internalNotes;
                $this->customer_model->where = array('customerID' => $customer_id);

                if ($this->customer_model->update()) {
                    $this->customerhistory_model->insert($internalNotes, $customer_id, 'Internal Note', $this->business_id);
                }
            }
        } else {
            $customer = new Customer($customer_id);
            $customer->internalNotes = "";
            $customer->save();
        }

        //log a call
        if (isset($_POST['callLog'])) {
            $this->customerhistory_model->insert(trim($this->input->post("callLog")), $customer_id, 'Call Log', $this->business_id);
        }

        //set hold
        $this->customer_model->clear();
        $this->customer_model->hold = $_POST['hold'];
        $this->customer_model->where = array('customerID' => $_POST['customer_id']);
        $hid = $this->customer_model->update();

        set_flash_message('success', "Notes have been updated");
        if ($this->input->is_ajax_request()) {
            echo json_encode(array("status" => "success"));
        } else {
            redirect("/admin/customers/detail/$customer_id");
        }
    }


    /**
     * UI for editing a customer
     */
    public function edit($customer_id = null)
    {
        if (!is_numeric($customer_id)) {
            set_flash_message("error", "Customer ID must be numeric and passed as segment 4 of the URL.");
            return $this->goBack("/admin/customers");
        }

        $customer = $this->getCustomerById($customer_id);

        if ($this->input->post()) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("email", "Email", "required|valid_email");

            if (!$this->form_validation->run()) {
                set_flash_message("error", validation_errors());
                return redirect("/admin/customers/edit/$customer_id");
            }

            $phone_changed = $customer->phone != $this->input->post('phone');
            $sms_changed = $customer->sms != $this->input->post('sms');
            $copy_sms = get_business_meta($this->business_id, 'copy_phone_to_sms');

            $fields = array(
                'email', 'inactive', 'firstName', 'lastName', 'phone', 'sms', 'sms2', 'username', 'address1', 'address2',
                'city', 'state', 'zip', 'type', 'invoice', 'hold', 'defaultWF', 'location_id', 'autoPay', 'shirts',
                'starchOnShirts_id', 'noEmail', 'referredBy', 'kioskAccess', 'deliverTo_location_id', 'wfNotes',
                'dcNotes', 'customerNotes', 'internalNotes', 'permAssemblyNotes', 'how', 'lockerLootTerms',
                'mgrAgreement', 'ip', 'lat', 'lon', 'w9', 'owner_id', 'birthday', 'sex', 'noUpcharge',
                'orderWithNoCC', 'taxable', 'default_business_language_id', 'customer_deliver_on_hold'
            );

            $_POST['sms'] = clean_phone_number($_POST['sms']);
            $_POST['sms2'] = clean_phone_number($_POST['sms2']);
            $_POST['phone'] = clean_phone_number($_POST['phone']);

            foreach ($fields as $field) {
                if ($field == 'email') {
                    $customer->$field = trim($this->input->post($field));
                } else {
                    $customer->$field = $this->input->post($field);
                }
            }

            if ($copy_sms && $phone_changed && !$sms_changed) {
                $customer->sms = $this->input->post('phone');
            }

            if ($this->input->post("password")) {
                $customer->password = $this->input->post("password");
            }

            if ($this->input->post("address1") && $this->input->post("city") && $this->input->post("state")) {
                $geo = geocode($this->input->post("address1"), $this->input->post("city"), $this->input->post("state"));
                $customer->lat = str_replace(',', '.', $geo['lat']);
                $customer->lon = str_replace(',', '.', $geo['lng']);
            }

            update_customer_meta($customer->customerID, 'detailed_receipt', empty($_POST['detailed_receipt']) ? 0 : 1);

            update_customer_meta($customer->customerID, 'company', $this->input->post('company'));

            if (is_superadmin()) {
                $pay_droplocker_invoices = $this->input->post('pay_droplocker_invoices');
                update_customer_meta($customer->customerID, 'pay_droplocker_invoices', $pay_droplocker_invoices);
            }

            try {
                //add this location to the customer's locations
                if ($this->input->post("location_id")) {
                    $addLocation = "replace into customer_location (customer_id, location_id) values (?, ?)";
                    $query = $this->db->query($addLocation, array($customer_id, $this->input->post("location_id")));
                }

                if ($customer->save()) {
                    $this->load->model("creditcard_model");
                    $this->creditcard_model->updateBillingInfo($customer, $this->business_id);
                }

                set_flash_message('success', "Updated Customer");
            } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                set_flash_message("error", $validation_exception->getMessage());
            }

            return redirect("/admin/customers/edit/" . $customer_id);
        }

        //get wash & fold products
        $sqlWFProducts = "select product.*, product_process.*
            from product
            join product_process on productID = product_id
            join productCategory on productCategoryID = productCategory_id
            where productCategory.slug = 'wf' and product.business_id = $this->business_id";

        $content['wfProducts'] = $this->db->query($sqlWFProducts)->result();

        //get starch
        $sqlStarch = "select * from starchOnShirts order by starchOnShirtsID";
        $content['starch'] = $this->db->query($sqlStarch)->result();

        $sql = "SELECT c.*, r.firstName as rfname,r.lastName as rlname,r.customerID as rid
                    FROM customer c
                    LEFT JOIN customer r ON r.customerID = c.referredBy
                    WHERE c.customerID = {$customer_id}";

        $customers = $this->db->query($sql)->result();

        $customers[0]->rCompleteName = getPersonName($customers[0], true, 'rfname', 'rlname');
        $content['customer'] = $customers[0];

        // Load lockerLootConfig, fixme: move functionality elsewhere?
        $content['lockerLootConfig'] = LockerLootConfig::search_aprax(array('business_id' => $this->business_id));

        $this->load->model('customer_location_model');
        $business_locations = Location::search_aprax(array("business_id" => $this->business_id, "status" => "active"), false, 'address');

        $locations = array("" => "--None--");
        foreach ($business_locations as $business_location) {
            $locations[$business_location->locationID] = $business_location->address;
        }
        natsort($locations);
        //This is used for the default locaiton dropdown menu.
        $content['locations'] = $locations;

        $this->db->select("firstName, lastName, employeeID");
        $this->db->join("employee", "business_employee.employee_id=employee.employeeID");
        $get_employees_query = $this->db->get_where("business_employee", array("business_id" => $this->business_id, "empStatus" => 1));
        $employees_result = $get_employees_query->result();
        $content['employees'] = array(); //This is used for the dropdown menu.
        foreach ($employees_result as $employee) {
            $content['employees'][$employee->employeeID] = getPersonName($employee);
        }

        $content['detailed_receipt'] = get_customer_meta($customer->customerID, 'detailed_receipt');

        $this->load->model("business_language_model");
        $content['business_languages_dropdown'] = $this->business_language_model->get_all_as_codeigniter_dropdown($this->business_id);

        $this->load->model("business_model");
        $content['business_dropdown'] = $this->business_model->get_all_as_codeigniter_dropdown();

        $this->renderAdmin('edit', $content);
    }

    /**
     * Renders the interface for updating a customer's credit card.
     * @param int $customer_id
     */
    public function billing($customer_id = null)
    {
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($customer_id);
        $this->load->model("creditcard_model");
        $card = $this->creditcard_model->get_one(array("customer_id" => $customer_id));

        // restore submitted post data for use with set_value()

        if ($post_flash = $this->session->flashdata('post')) {
            $_POST = unserialize($post_flash);
        }

        $card_types = get_business_meta($this->business_id, 'acceptedCards');
        $accepted_cards = empty($card_types) ? array() : unserialize($card_types);

        $this->load->model('location_model');
        $showKioskCheckbox = $this->location_model->isOnlyKiosks($this->business_id);

        $content = compact('customer', 'card', 'showKioskCheckbox', 'accepted_cards');

        $this->renderAdmin('billing', $content);
    }

    public function delete_card($customer_id = null)
    {
        $customer = $this->getCustomerById($customer_id);

        if (!empty($_POST['delete'])) {
            $this->load->model('creditcard_model');
            $this->creditcard_model->delete_card($customer_id, $this->business_id);

            $this->load->model('customer_model');
            $customer->autoPay = 0;
            $customer->save();

            set_flash_message('success', "The credit card has been deleted");
        }

        redirect("/admin/customers/billing/$customer_id");
    }

    /**
     * UI for setting up invoicing
     *
     * customer_id is set from url segment 4
     */
    public function setupInvoicing($customer_id = null)
    {
        $this->load->model('invoicing_model');

        $this->getCustomerById($customer_id);

        if ($this->input->post("submit")) {
            $where['customer_id'] = $customer_id;
            unset($_POST['submit']);
            $customer = $this->input->post("customer_id");

            $res = $this->invoicing_model->update_settings($_POST, $where);
            if ($res) {
                set_flash_message('success', "Invoicing Record Updated");
            } else {
                set_flash_message('error', "Invoicing Record Not Updated");
            }
            redirect("/admin/customers/setupInvoicing/" . $customer_id);
        }

        $this->customer_model->customerID = $customer_id;
        $content['customer'] = $this->customer_model->get();

        $this->invoicing_model->customer_id = $customer_id;
        $content['invoicing'] = $this->invoicing_model->get();

        if (!$content['invoicing']) {
            $this->invoicing_model->customer_id = $customer_id;
            $this->invoicing_model->insert();
            $content['invoicing'] = $this->invoicing_model->get();
        }

        $this->renderAdmin('setupInvoicing', $content);
    }

    /**
     * applies a coupon code to a customer
     *
     */
    public function apply_coupon()
    {
        if (isset($_POST['submit'])) {
            $code = $_POST['couponCode'];
            $customer_id = $_POST['customer_id'];
            $customer = $this->getCustomerById($customer_id);

            $this->load->library('coupon');
            $res = $this->coupon->applyCoupon($code, $customer_id, $this->business_id);

            if ($res['status'] == 'success') {
                set_static_message('success', $res['message']);
                redirect('/admin/customers/discounts/' . $customer_id);
            } else {
                set_static_message('error', $res['error']);
                redirect('/admin/customers/discounts/' . $customer_id);
            }
        }
    }

    /**
     * The following action renders the interface for viewing discounts associated to a particular customer.
     * customer _id set from url segment 4
     *
     * Can take a POST request to create a new customer discount
     * Requires the following POST parameters:
     */
    public function discounts($customer_id = null)
    {
        if (!is_numeric($customer_id)) {
            set_flash_message("error", "The customer ID must be passed as segment 4 of the URL and must be an integer.");
            return $this->goBack("/admin/customers");
        }
        $customer = $this->getCustomerById($customer_id);

        //if this customer has any expired discounts, make them inactive
        $this->load->model('customerdiscount_model');
        $this->customerdiscount_model->expireCustomerDiscount($this->business_id, $customer_id);

        $this->load->model('customerdiscount_model');
        $this->load->model('product_model');

        // The following conditional checks to see if we are adding a new coupon.
        if (!empty($_POST)) {
            $_POST['description'] = (!empty($_POST['new_description'])) ? $_POST['new_description'] : $_POST['description'];
            if ($_POST['amountType'] == 'dollars') {
                $description = format_money_symbol($this->business_id, '%.2n', $_POST['amount']) . ' ' . $_POST['description'];
                $active = 1;
            }
            if ($_POST['amountType'] == 'percent') {
                $description = $_POST['amount'] . '% ' . $_POST['description'];
                $active = 1;
            }
            if ($_POST['amountType'] == 'lockerloot') {
                $description = format_money_symbol($this->business_id, '%.2n', $_POST['amount']) . ' Locker Loot - ' . $_POST['description'];
                $active = 0;

                //add discount as Locker Loot
                $this->load->model('lockerloot_model');
                $this->lockerloot_model->points = $_POST['amount'];
                $this->lockerloot_model->customer_id = $customer_id;
                $this->lockerloot_model->insert();
            }
            if ($_POST['amountType'] == 'pounds') {
                $description = $_POST['amount'] . ' lb ' . $_POST['description'];
                $active = 1;
            }
            if (substr($_POST['amountType'], 0, 4) == 'item') {
                //make sure we have a product ID too!
                if (!$this->input->post("product_id")) {
                    throw new Exception("'product_id' must be passed as a POST parameter.");
                }
                $product_id = $this->input->post("product_id");
                if ($product_id == 'dc') {
                    $product->name = 'Dry Cleaning';

                    if ($this->input->post("process_id")) {
                        $this->load->model("process_model");
                        $process_name = $this->process_model->getName($this->input->post("process_id"));

                        $onlyProcessMsg = get_translation('only_for_process', 'Processes', array('process' => $process_name), "ONLY for %process% process");
                        $_POST['extendedDesc'] .= "($onlyProcessMsg)";
                    }
                } else {
                    if (!is_numeric($product_id)) {
                        throw new Exception("'product_id' must be numeric.");
                    }

                    $getProduct = "select name from product where productID = $product_id";
                    $query = $this->db->query($getProduct);
                    $product = $query->row();
                }
                if ($_POST['amountType'] == 'item') {
                    $_POST['amountType'] = 'item[' . $_POST['product_id'] . ']';
                    $description = $_POST['description'];
                    $_POST['extendedDesc'] = format_money_symbol($this->business_id, '%.2n', $_POST['amount']) . ' ' . $product->name . ' - ' . $_POST['extendedDesc'];
                } else {
                    $_POST['amountType'] = 'item[%' . $_POST['product_id'] . ']';
                    $description = $_POST['description'];
                    $_POST['extendedDesc'] = $_POST['amount'] . '% off ' . $product->name . ' - ' . $_POST['extendedDesc'];
                }
                $active = 1;
            }

            if ($_POST['description'] == 'employer match') {
                if (empty($_POST['customer_id']) || !is_numeric($_POST['customer_id'])) {
                    set_static_message('error', "Discount has NOT been created, missing Customer to Pay");

                    redirect("/admin/customers/discounts/{$customer_id}");
                }

                $_POST['extendedDesc'] = $_POST['amount'] . ' ' . $_POST['amountType'] . ' off ' . $_POST['frequency'] . ' - ' . $_POST['description'];
                $description = $_POST['description'] . '[' . $_POST['customer_id'] . ']';
                $active = 1;
            }

            if (!empty($_POST['new_description'])) {
                if ($discount_reason_id = $this->customerdiscount_model->insertDiscountReason($this->business_id, $_POST['new_description'])) {
                    $this->customerdiscount_model->discount_reason_id = $discount_reason_id;
                }
            } else {
                if ($discount_reason = current($this->customerdiscount_model->getDiscountReasons($this->business_id, $_POST['description']))) {
                    $this->customerdiscount_model->discount_reason_id = $discount_reason->discountReasonID;
                }
            }

            if ($this->input->post("combine")) {
                $combine = 1;
            } else {
                $combine = 0;
            }
            $recurring_type_id = $this->input->post("recurring_type_id");

            $this->customerdiscount_model->customer_id = $customer_id;
            $this->customerdiscount_model->amount = $_POST['amount'];
            $this->customerdiscount_model->amountType = $_POST['amountType'];
            $this->customerdiscount_model->frequency = $_POST['frequency'];

            //handle the currency symbol in the description //safe w/out mb_strlower
            $descLowerDecode = utf8_decode($description);
            $descLower = strtolower($descLowerDecode);
            $utfDescLower = utf8_encode($descLower);
            $this->customerdiscount_model->description = $utfDescLower;

            $this->customerdiscount_model->extendedDesc = $_POST['extendedDesc'];
            $this->customerdiscount_model->location_id = $_POST['location_id'];

            //The following conditional checks to see if the recurring type is 'monthly', if so, then we send the expiration date to be the end of the current month.
            if ($recurring_type_id == 2) {
                $this->customerdiscount_model->expireDate = date("Y-m-t");
            }
            $this->customerdiscount_model->recurring_type_id = $recurring_type_id;
            $this->customerdiscount_model->active = $active;
            $this->customerdiscount_model->origCreateDate = date("Y-m-d H:i:s");
            $this->customerdiscount_model->createdBy = $this->buser_id;
            $this->customerdiscount_model->combine = $combine;
            $this->customerdiscount_model->process_id = $this->input->post("process_id");

            // added if because the $_POST was null, but getting set to 0000-00-00
            // The system does not handle discounts properly if the date is 0000-00-00
            if ($_POST['expireDate']) {
                $this->customerdiscount_model->expireDate = $_POST['expireDate'];
            }

            $this->customerdiscount_model->business_id = $this->business_id;
            $insert_id = $this->customerdiscount_model->insert();

            if ($insert_id) {
                if ($_POST['frequency'] == 'every order') {
                    $this->customer_model->clear();
                    $this->customer_model->mgrAgreement = 1;
                    $this->customer_model->where = array('customerID' => $customer_id);
                    $this->customer_model->update();
                }

                // send email to arik if the amount is over $25
                if ($_POST['amountType'] == 'dollars' && $_POST['amount'] >= 25) {
                    $this->customer_model->customerID = $customer_id;
                    $thisCust = $this->customer_model->get();
                    send_admin_notification('Large Discount Created', "A " . format_money_symbol($this->business_id, '%.2n', $_POST['amount']) . " discount was just created for customer: <a href='http://" . $_SERVER['HTTP_HOST'] . "/admin/customers/detail/" . $customer_id . "'>" . getPersonName($thisCust[0]) . "<a><br><br><a href='http://" . $_SERVER['HTTP_HOST'] . "/admin/customers/discounts/" . $customer_id . "'>View Discount</a>");
                }

                set_flash_message('success', "Discount has been created");
            } else {
                set_flash_message('error', "Discount has NOT been created");
            }

            return $this->goBack("/admin/customers/discounts/{$customer_id}");
        }

        $this->load->model("recurring_type_model");
        $content['recurring_types'] = $this->recurring_type_model->get_recurring_types_as_codeigniter_dropdown();

        $content['customer'] = $this->customer_model->get_aprax(array(
            'customerID' => $customer_id,
        ));
        $content['customer_id'] = $customer_id;

        $sql = "SELECT customer_id, customerDiscountID, amount, combine, amountType,
            frequency, customerDiscount.description, extendedDesc, active, origCreateDate,
            updated, order_id, location_id,amountApplied, couponCode, expireDate, createdBy,
            recurring_type.name, recurring_type_id
        FROM customerDiscount
        LEFT JOIN location ON (locationID = location_id)
        LEFT JOIN recurring_type ON (recurring_type_id = recurring_typeID)
        WHERE customer_id = ?
            AND customerDiscount.business_id = ?
        ORDER BY active DESC, origCreateDate DESC, updated DESC";

        $content['discounts'] = $this->db->query($sql, array($customer_id, $this->business_id))->result();
        $content['reasons'] = $this->customerdiscount_model->getDiscountReasons($this->business_id);

        // get the locations for this business
        $this->load->model('location_model');
        $content['locations'] = $this->location_model->simple_array(array('business_id' => $this->business_id, 'sort' => "address"), "-- SELECT --");

        //get the products for the dropdown with amountType = item
        $options['order_by'] = "displayName";
        $options['group_by'] = "displayName";
        $options['business_id'] = $this->business_id;
        $options['select'] = 'product.displayName, productID';
        $products = $this->product_model->get($options);
        $content['products'] = json_encode($products);

        //get the processes for the dropdown with amountType = item and product dry cleaning
        $this->load->model("process_model");
        $content['processes'] = $this->process_model->get_as_codeigniter_dropdown();
        $content['processes'][0] = get_translation('label_process_all', '/admin/customers/discounts', array(), "All");

        $this->load->model("coupon_model");
        $content['coupons'] = $this->coupon_model->get_active_coupons_as_codeigniter_menu($this->business_id);

        $this->renderAdmin('discounts', $content);
    }

    /**
     * Updates a discount's expiration date and sets the activate property to 1.
     * Expects the following POST parameters:
     *      customerDiscountID int The customer discount ID
     *      expireDate string A MysQL formatted date
     */
    public function update_discount_expireDate()
    {
        $this->load->model('customerdiscount_model');
        $this->customerdiscount_model->options['where'] = array("customerDiscountID" => $this->input->post("customerDiscountID"), 'business_id' => $this->business_id);
        $this->customerdiscount_model->expireDate = $this->input->post("expireDate");
        $this->customerdiscount_model->active = 1;
        $result = $this->customerdiscount_model->update();
        if ($result) {
            set_flash_message('success', "Discount Extended");
        } else {
            set_flash_message("error", "Failed to Extend Discount");
        }
        $this->customerdiscount_model->clear();
        $this->customerdiscount_model->options['customerDiscountID'] = $this->input->post("customerDiscountID");
        $discount = $this->customerdiscount_model->get();
        redirect("/admin/customers/discounts/{$discount[0]->customer_id}");
    }

    /**
     * Sets a discount's activation status
     * Expects the following POST parameters
     *      customerDiscountID int The customer discount ID
     *      active bool The activation status of the customer discount
     */
    public function update_discount_activation_status()
    {
        $active = $this->input->post("active");
        $this->load->model("customerdiscount_model");
        $this->customerdiscount_model->options['where'] = array("customerDiscountID" => $this->input->post("customerDiscountID"), 'business_id' => $this->business_id);
        $this->customerdiscount_model->active = $active;
        $result = $this->customerdiscount_model->update();
        if ($result) {
            set_flash_message("success", "Activation Update Successful");
        } else {
            set_flash_message("error", "Failed to Update the Activation Status");
        }
        $this->customerdiscount_model->clear();
        $this->customerdiscount_model->options['customerDiscountID'] = $this->input->post("customerDiscountID");
        $discount = $this->customerdiscount_model->get();
        redirect("/admin/customers/discounts/{$discount[0]->customer_id}");
    }

    public function delete_discount()
    {
        $this->load->model('customerdiscount_model');
        if (isset($_POST['delete'])) {
            $this->customerdiscount_model->customerDiscountID = $_POST['customerDiscountID'];
            $this->customerdiscount_model->business_id = $this->business_id; // Only delete discounts from my business
            $this->customerdiscount_model->delete();

            set_flash_message('success', "Discount has been deleted");
            if ($_POST['json']) {
                echo json_encode(array('status' => 'success'));
            } else {
                redirect('/admin/customers/discounts/' . $_POST['customer_id']);
            }
        }
    }
    /**
     * Expects the following POST parameters
     *  couponCode: the prefix and code for the coupon
     *  customer_id: the Customer ID to whom to apply the discount
     */
    public function add_code_discount()
    {
        if (!$this->input->post("customer_id")) {
            set_flash_message('error', "'customer_id' must be passed as a POST parameter.");
            $this->goBack("/admin/customers/");
        }

        if (!$this->input->post("couponCode") && !$this->input->post("coupon")) {
            set_flash_message('error', "You must specify a coupon code to apply to the customer");
            $this->goBack("/admin/customers/discounts/" . $_POST['customer_id']);
        }

        $customer_id = $this->input->post("customer_id");
        $customer = $this->getCustomerById($customer_id);
        $couponCode = $this->input->post("couponCode");
        if (!$couponCode && $this->input->post("coupon")) {
            $couponCode = $this->input->post("coupon");
        }

        $this->load->library('coupon');
        $response = $this->coupon->applyCoupon($couponCode, $customer_id, $this->business_id);
        if ($response['status'] == 'fail') {
            set_flash_message('error', $response['error']);
        } else {
            set_flash_message('success', 'Discount Added');
        }

        $this->goBack("/admin/customers/discounts/$customer_id");
    }

    public function search_customers()
    {
        if ($_POST) {
            unset($_POST['submit']);
            $search_options = array_filter($_POST);
            $search_options['business_id'] = $this->business_id;

            $this->load->model('customer_model');
            $content['customer'] = $this->customer_model->search_customer($search_options);
        }

        $this->renderAdmin('search_customers', $content);
    }

    public function laundry_plans()
    {
        $this->load->model('businesslaundryplan_model');
        $this->businesslaundryplan_model->business_id = $this->business_id;
        $content['business'] = $this->business;
        if (isset($_POST['add_submit'])) {
            unset($_POST['add_submit']);
            foreach ($_POST as $k => $p) {
                $this->businesslaundryplan_model->$k = $p;
            }
            if ($this->businesslaundryplan_model->insert()) {
                set_flash_message("success", "New Laundry Plan has been Added");
            } else {
                set_flash_message('error', 'Plan NOT Added');
            }
            redirect("/admin/customers/laundry_plans");
        }

        $content['businessLaundryPlans'] = $this->businesslaundryplan_model->get();

        $content['title'] = "Manage Laundry Plans";

        $this->renderAdmin('laundry_plans', $content);
    }

    public function customer_plans()
    {
        $this->load->model('laundryplan_model');
        $content['plans'] = $this->laundryplan_model->get_active_laundry_plans_by_business_id($this->business_id);

        $this->template->write('page_title', 'Active Laundry Plans');

        $this->renderAdmin('plans', $content);
    }

    public function does_businessLaundryPlan_exist($businessLaundryPlanID)
    {
        $this->load->model("businessLaundryPlan_model");
        if ($this->businessLaundryPlan_model->does_exist($businessLaundryPlanID)) {
            return true;
        } else {
            $this->form_validation->set_message("does_businessLaundryPlan_exist", "Business Laundry Plan '$businessLaundryPlanID' does not exist");
        }
    }

    /**
     * Note, a * design error has both viewing and updating a business luandry plan done by this same action.
     * Note, a * design error has the business Laundry ID passed as the fourth segment in the request URL, instead of passed as a GET variable.
     */
    public function edit_plan($businessLaundryPlanID = null)
    {
        $this->load->model('businesslaundryplan_model');
        $content['title'] = "Edit Business Laundry Plan";
        $content['business'] = $this->business;
        if (!is_numeric($businessLaundryPlanID)) {
            set_flash_message("error", "The businesss laundry plan ID must be passed as segment 4 in the URL and must be numeric.");
            redirect("/admin/customers/laundry_plans");
        } else {
            try {
                $businessLaundryPlan = new BusinessLaundryPlan($businessLaundryPlanID);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Business Laundry Plan '$businessLaundryPlanID' does not exist");
                redirect("/admin/customers/laundry_plans");
            }

            if ($businessLaundryPlan->business_id != $this->business_id) {
                set_flash_message("error", "This laundry plan does not belong to your business.");
                redirect("/admin/customers/laundry_plans");
            }

            // THe following * conditional checks to see if any post variables are set, if so, then the  specified business laundry plan properties are updated with what is contained in the post variables.
            if (!empty($_POST)) {
                $this->load->library("form_validation");
                $this->form_validation->set_rules('name', "Name", "required");
                $this->form_validation->set_rules("price", "Price", "required|numeric");
                $this->form_validation->set_rules("pounds", "Pounds", "required|is_natural_no_zero");
                if ($this->form_validation->run()) {
                    $businessLaundryPlan->name = $this->input->post("name");
                    $businessLaundryPlan->caption = $this->input->post("caption");
                    $businessLaundryPlan->description = $this->input->post("description");
                    $businessLaundryPlan->price = $this->input->post("price");
                    $businessLaundryPlan->pounds = $this->input->post("pounds");
                    $businessLaundryPlan->poundsDescription = $this->input->post("poundsDescription");
                    $businessLaundryPlan->days = $this->input->post("days");
                    $businessLaundryPlan->endDate = $this->input->post("endDate") ? new DateTime($this->input->post("endDate")) : "";
                    $businessLaundryPlan->rollover = $this->input->post("rollover");
                    $businessLaundryPlan->grouping = $this->input->post("grouping");
                    $businessLaundryPlan->visible = $this->input->post("visible");
                    $businessLaundryPlan->expired_price = $this->input->post("expired_price");
                    $businessLaundryPlan->updateExistingCustomerPlansWhenPlanChange = $this->input->post("updateExistingCustomerPlansWhenPlanChange");

                    $businessLaundryPlan->save();
                    set_flash_message("success", "Updated Business Laundry Plan");
                } else {
                    set_flash_message("error", validation_errors());
                }

                $this->goBack("/admin/customers/edit_plan/" . $businessLaundryPlanID);
            }
        }
        $content['plan_url'] = get_business_account_url($this->business) . '/main/plans/' . $businessLaundryPlan->businessLaundryPlanID;
        $content['businessLaundryPlan'] = $businessLaundryPlan;

        $this->renderAdmin('edit_plan', $content);
    }

    /**
     * Deletes a laundry plan from a business.
     * Note, a * design error expects the business laundry plan ID to be passed as segment 4 of the URL.
     */
    public function delete_plan($businessLaundryPlanID = null)
    {
        if (!is_numeric($businessLaundryPlanID)) {
            set_flash_message("error", "The business laundry plan ID must be passed as segment 4 of the URL and must be numeric");
            $this->goBack("/admin/admin");
        }

        try {
            $businessLaundryPlan = new BusinessLaundryPlan($businessLaundryPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Business laundry plan ID '$businessLaundryPlanID' not found");
            $this->goBack("/admin/customers/laundry_plan");
        }

        if ($businessLaundryPlan->business_id != $this->business_id) {
            set_flash_message("error", "This laundry plan does not belong to your business.");
            $this->goBack("/admin/customers/laundry_plan");
        }

        $businessLaundryPlan->delete();
        set_flash_message("success", "Deleted laundry plan '{$businessLaundryPlan->name}'");
        $this->goBack("/admin/customers/laundry_plans");
    }

    public function everything_plans()
    {
        $content['business'] = $this->business;

        if (!empty($this->input->post('add_submit'))) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('name', "Name", "required");
            $this->form_validation->set_rules("price", "Price", "required|numeric");
            $this->form_validation->set_rules("credit_amount", "Credit Purchased", "required|is_natural_no_zero");

            if ($this->form_validation->run()) {
                if ($this->update_everything_plan()) {
                    set_flash_message("success", "Updated Business Everything Plan");
                    redirect("/admin/customers/everything_plans");
                } else {
                    set_flash_message('error', 'Plan NOT Added');
                    redirect("/admin/customers/everything_plans");
                }
            }
        }

        $content['businessEverythingPlans'] = businessEverythingPlan::search_aprax(array(
            "business_id" => $this->business_id,
            "type" => "everything_plan"
        ));

        $content['title'] = "Manage Everything Plans (Beta)";

        $this->renderAdmin('everything_plans', $content);
    }

    public function edit_everything_plan($businessEverythingPlanID = null)
    {
        $content['title'] = "Edit Business Everything Plan";
        $content['business'] = $this->business;
        if (!is_numeric($businessEverythingPlanID)) {
            set_flash_message("error", "The business everything plan ID must be passed as segment 4 in the URL and must be numeric.");
            redirect("/admin/customers/everything_plans");
        } else {
            try {
                $businessEverythingPlan = new businessEverythingPlan($businessEverythingPlanID);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Business Everything Plan '$businessEverythingPlanID' does not exist");
                redirect("/admin/customers/everything_plans");
            }

            if ($businessEverythingPlan->business_id != $this->business_id) {
                set_flash_message("error", "This everything plan does not belong to your business.");
                redirect("/admin/customers/everything_plans");
            }

            if (!empty($this->input->post())) {
                $this->load->library("form_validation");
                $this->form_validation->set_rules('name', "Name", "required");
                $this->form_validation->set_rules("price", "Price", "required|numeric");
                $this->form_validation->set_rules("credit_amount", "Credit Purchased", "required|numeric");

                if ($this->form_validation->run()) {
                    if ($this->update_everything_plan($businessEverythingPlanID)) {
                        set_flash_message("success", "Updated Business Everything Plan");
                    } else {
                        set_flash_message('error', 'Error when trying to update Plan');
                    }
                }

                $this->goBack("/admin/customers/edit_everything_plan/$businessEverythingPlanID");
            }
        }

        $content['plan_url'] = get_business_account_url($this->business) . '/main/everything_plans/' . $businessEverythingPlan->businessEverythingPlanID;
        $content['businessEverythingPlan'] = $businessEverythingPlan;

        $this->renderAdmin('edit_everything_plan', $content);
    }

    protected function update_everything_plan($businessEverythingPlanID = null)
    {
        try {
            $businessEverythingPlan = new businessEverythingPlan($businessEverythingPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Error creating/editing Business Everything Plan");
            redirect("/admin/customers/everything_plans");
        }

        if (!empty($this->input->post())) {
            $businessEverythingPlan->business_id = $this->business_id;
            $businessEverythingPlan->name = $this->input->post("name");
            $businessEverythingPlan->caption = $this->input->post("caption");
            $businessEverythingPlan->description = $this->input->post("description");
            $businessEverythingPlan->price = $this->input->post("price");
            $businessEverythingPlan->credit_amount = $this->input->post("credit_amount");
            $businessEverythingPlan->poundsDescription = $this->input->post("poundsDescription");
            $businessEverythingPlan->days = $this->input->post("days");
            $businessEverythingPlan->endDate = $this->input->post("endDate") ? new DateTime($this->input->post("endDate")) : "";
            $businessEverythingPlan->rollover = $this->input->post("rollover");
            $businessEverythingPlan->grouping = $this->input->post("grouping");
            $businessEverythingPlan->visible = $this->input->post("visible");
            $businessEverythingPlan->expired_discount_percent = $this->input->post("expired_discount_percent");
            $businessEverythingPlan->type = "everything_plan";
            $businessEverythingPlan->updateExistingCustomerPlansWhenPlanChange = $this->input->post("updateExistingCustomerPlansWhenPlanChange");

            if ($businessEverythingPlan->save()) {
                return true;
            }
        }

        return false;
    }

    public function delete_everything_plan($businessEverythingPlanID = null)
    {
        if (!is_numeric($businessEverythingPlanID)) {
            set_flash_message("error", "The business everything plan ID must be passed as segment 4 of the URL and must be numeric");
            $this->goBack("/admin/customers/everything_plans");
        }

        try {
            $businessEverythingPlan = new BusinessEverythingPlan($businessEverythingPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Business everything plan ID '$businessEverythingPlanID' not found");
            $this->goBack("/admin/customers/everything_plans");
        }

        if ($businessEverythingPlan->business_id != $this->business_id) {
            set_flash_message("error", "This everything plan does not belong to your business.");
            $this->goBack("/admin/customers/everything_plans");
        }

        $businessEverythingPlan->delete();
        set_flash_message("success", "Deleted everything plan '{$businessEverythingPlan->name}'");
        $this->goBack("/admin/customers/everything_plans");
    }

    public function customer_everything_plans()
    {
        $content['plans'] = \App\Libraries\DroplockerObjects\EverythingPlan::get_active_plans($this->business_id);
        $this->template->write('page_title', 'Active Everything Plans');
        $this->renderAdmin('list_everything_plans', $content);
    }

    public function delete_customer_everything_plan($everythingPlanID = null)
    {
        if (!is_numeric($everythingPlanID)) {
            set_flash_message("error", "The customer everything plan ID must be passed as segment 4 of the URL and must be numeric");
            $this->goBack("/admin/customers/customer_everything_plans");
        }

        try {
            $everythingPlan = new \App\Libraries\DroplockerObjects\EverythingPlan($everythingPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Customer everything plan ID '$everythingPlanID' not found");
            $this->goBack("/admin/customers/customer_everything_plans");
        }

        if ($everythingPlan->business_id != $this->business_id) {
            set_flash_message("error", "This everything plan does not belong to your business.");
            $this->goBack("/admin/customers/customer_everything_plans");
        }

        $everythingPlan->delete();
        set_flash_message("success", "Deleted everything plan '{$everythingPlan->description}'");
        $this->goBack("/admin/customers/customer_everything_plans");
    }

    public function product_plans()
    {
        $content['business'] = $this->business;

        if (!empty($this->input->post('add_submit'))) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('name', "Name", "required");
            $this->form_validation->set_rules("price", "Price", "required|numeric");
            $this->form_validation->set_rules("credit_amount", "Credit Purchased", "required|is_natural_no_zero");

            if ($this->form_validation->run()) {
                if ($this->update_product_plan()) {
                    set_flash_message("success", "Updated Product Plan");
                    redirect("/admin/customers/product_plans");
                } else {
                    set_flash_message('error', 'Plan NOT Added');
                    redirect("/admin/customers/product_plans");
                }
            }
        }

        $content['businessProductPlans'] = businessProductPlan::search_aprax(array(
            "business_id" => $this->business_id,
            "type" => "product_plan"
        ));

        $this->load->model("product_process_model");
        $content['products'] = $this->product_process_model->get_as_codeigniter_dropdown($this->business_id);
        $content['products']["-1"] = "All";
        $content['selected_products'] = array("-1");

        $content['title'] = "Manage Product Plans (Beta)";

        $this->renderAdmin('product_plans', $content);
    }

    public function edit_product_plan($businessProductPlanID = null)
    {
        $content['title'] = "Edit Business Product Plan";
        $content['business'] = $this->business;
        if (!is_numeric($businessProductPlanID)) {
            set_flash_message("error", "The business product plan ID must be passed as segment 4 in the URL and must be numeric.");
            redirect("/admin/customers/product_plans");
        } else {
            try {
                $businessProductPlan = new businessProductPlan($businessProductPlanID);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Business Product Plan '$businessProductPlanID' does not exist");
                redirect("/admin/customers/product_plans");
            }

            if ($businessProductPlan->business_id != $this->business_id) {
                set_flash_message("error", "This product plan does not belong to your business.");
                redirect("/admin/customers/product_plans");
            }

            if (!empty($this->input->post())) {
                $this->load->library("form_validation");
                $this->form_validation->set_rules('name', "Name", "required");
                $this->form_validation->set_rules("price", "Price", "required|numeric");
                $this->form_validation->set_rules("credit_amount", "Allowed items of clothing", "required|numeric");

                if ($this->form_validation->run()) {
                    if ($this->update_product_plan($businessProductPlanID)) {
                        set_flash_message("success", "Updated Business Product Plan");
                    } else {
                        set_flash_message('error', 'Error when trying to update Plan');
                    }
                }

                $this->goBack("/admin/customers/edit_product_plan/$businessProductPlanID");
            }
        }

        $this->load->model("product_process_model");
        $content['products'] = $this->product_process_model->get_as_codeigniter_dropdown($this->business_id);
        $content['products']["-1"] = "All";
        $businessProductPlan->selected_products = json_decode($businessProductPlan->products);

        $content['plan_url'] = get_business_account_url($this->business) . '/main/product_plans/' . $businessProductPlan->businessProductPlanID;
        $content['businessProductPlan'] = $businessProductPlan;

        $this->renderAdmin('edit_product_plan', $content);
    }

    protected function update_product_plan($businessProductPlanID = null)
    {
        try {
            $businessProductPlan = new businessProductPlan($businessProductPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Error creating/editing Business Product Plan");
            redirect("/admin/customers/product_plans");
        }

        if (!empty($this->input->post())) {
            $businessProductPlan->business_id = $this->business_id;
            $businessProductPlan->name = $this->input->post("name");
            $businessProductPlan->caption = $this->input->post("caption");
            $businessProductPlan->description = $this->input->post("description");
            $businessProductPlan->price = $this->input->post("price");
            $businessProductPlan->credit_amount = $this->input->post("credit_amount");
            $businessProductPlan->poundsDescription = $this->input->post("poundsDescription");
            $businessProductPlan->days = $this->input->post("days");
            $businessProductPlan->endDate = !empty($this->input->post("endDate")) ? new DateTime($this->input->post("endDate")) : "0000-00-00";
            if (!empty($businessProductPlan->days)) {
                $businessProductPlan->endDate = "0000-00-00";
            }
            $businessProductPlan->rollover = $this->input->post("rollover");
            $businessProductPlan->grouping = $this->input->post("grouping");
            $businessProductPlan->visible = $this->input->post("visible");
            $businessProductPlan->expired_discount_percent = $this->input->post("expired_discount_percent");
            if (empty($this->input->post("available_products_ids"))) {
                $businessProductPlan->products = json_encode(array("-1"));
            } else {
                $businessProductPlan->products = json_encode($this->input->post("available_products_ids"));
            }
            $businessProductPlan->type = "product_plan";
            $businessProductPlan->updateExistingCustomerPlansWhenPlanChange = $this->input->post("updateExistingCustomerPlansWhenPlanChange");

            if ($businessProductPlan->save()) {
                return true;
            }
        }

        return false;
    }

    public function delete_product_plan($businessProductPlanID = null)
    {
        if (!is_numeric($businessProductPlanID)) {
            set_flash_message("error", "The business product plan ID must be passed as segment 4 of the URL and must be numeric");
            $this->goBack("/admin/customers/product_plans");
        }

        try {
            $businessProductPlan = new businessProductPlan($businessProductPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Business product plan ID '$businessProductPlanID' not found");
            $this->goBack("/admin/customers/product_plans");
        }

        if ($businessProductPlan->business_id != $this->business_id) {
            set_flash_message("error", "This product plan does not belong to your business.");
            $this->goBack("/admin/customers/product_plans");
        }

        $businessProductPlan->delete();
        set_flash_message("success", "Deleted product plan '{$businessProductPlan->name}'");
        $this->goBack("/admin/customers/product_plans");
    }

    public function customer_product_plans()
    {
        $content['plans'] = \App\Libraries\DroplockerObjects\ProductPlan::get_active_plans($this->business_id);
        $this->template->write('page_title', 'Active Product Plans');
        $this->renderAdmin('list_product_plans', $content);
    }

    public function delete_customer_product_plan($productPlanID = null)
    {
        if (!is_numeric($productPlanID)) {
            set_flash_message("error", "The customer product plan ID must be passed as segment 4 of the URL and must be numeric");
            $this->goBack("/admin/customers/customer_product_plans");
        }

        try {
            $productPlan = new \App\Libraries\DroplockerObjects\productPlan($productPlanID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Customer product plan ID '$productPlanID' not found");
            $this->goBack("/admin/customers/customer_product_plans");
        }

        if ($productPlan->business_id != $this->business_id) {
            set_flash_message("error", "This product plan does not belong to your business.");
            $this->goBack("/admin/customers/customer_product_plans");
        }

        $productPlan->delete();
        set_flash_message("success", "Deleted product plan '{$productPlan->description}'");
        $this->goBack("/admin/customers/customer_product_plans");
    }

    public function renew_plan($businessLaundryPlan_id, $laundryPlanID)
    {
        if (!is_numeric($businessLaundryPlan_id)) {
            set_flash_message("error", "The customer business plan ID must be passed as segment 4 of the URL and must be numeric");
            $this->goBack();
        }

        if (!is_numeric($laundryPlanID)) {
            set_flash_message("error", "The customer plan ID must be passed as segment 5 of the URL and must be numeric");
            $this->goBack();
        }

        // expire plan
        $sql = "UPDATE laundryPlan SET active = 0 WHERE businessLaundryPlan_id=? AND laundryPlanID=?";
        $this->db->query($sql, array($businessLaundryPlan_id, $laundryPlanID));

        // find the plan
        $sql = "SELECT 
                    `laundryPlan`.`customer_id`,
                    `laundryPlan`.`plan`,
                    `laundryPlan`.`price`,
                    `laundryPlan`.`days`,
                    `laundryPlan`.`startDate`,
                    `laundryPlan`.`endDate`,
                    `laundryPlan`.`active`,
                    `laundryPlan`.`description`,
                    `laundryPlan`.`renew`,
                    `laundryPlan`.`pounds`,
                    `laundryPlan`.`rollover`,
                    `laundryPlan`.`business_id`,
                    `laundryPlan`.`expired_price`,
                    `laundryPlan`.`credit_amount`,
                    `laundryPlan`.`type` as `plan_type`,
                    `laundryPlan`.`expired_discount_percent`,
                    `laundryPlan`.`products`,
                    `laundryPlan`.`businessLaundryPlan_id`, 
                    `businessLaundryPlan`.`updateExistingCustomerPlansWhenPlanChange`,
                    `businessLaundryPlan`.`price` as 'new_price',
                    `businessLaundryPlan`.`description` AS `new_description`,
                    `businessLaundryPlan`.`pounds` AS `new_pounds`,
                    `businessLaundryPlan`.`days` AS `new_days`,
                    `businessLaundryPlan`.`rollover` AS `new_rollover`,
                    `businessLaundryPlan`.`expired_price` AS `new_expired_price`,
                    `businessLaundryPlan`.`credit_amount` AS `new_credit_amount`,
                    `businessLaundryPlan`.`expired_discount_percent` AS `new_expired_discount_percent`,
                    `businessLaundryPlan`.`products` AS `new_products`
                FROM 
                    laundryPlan
                JOIN customer ON 
                    (laundryPlan.customer_id = customerID)
                JOIN businessLaundryPlan ON 
                    businessLaundryPlan.businessLaundryPlanID=laundryPlan.businessLaundryPlan_ID
                WHERE 
                laundryPlan.businessLaundryPlan_id=? AND laundryPlan.laundryPlanID=?";

        $laundryPlan = $this->db->query($sql, array($businessLaundryPlan_id, $laundryPlanID))->result();
        $laundryPlan = $laundryPlan[0];

        $this->load->model("order_model");

        $goBack = '';
        try {
            
            // find the price column to use for the renewals
            $updateExistingCustomerPlansWhenPlanChange = (int)$laundryPlan->updateExistingCustomerPlansWhenPlanChange;

            if ($updateExistingCustomerPlansWhenPlanChange) {
                $laundryPlan->price = $laundryPlan->new_price;
                $laundryPlan->description = $laundryPlan->new_description;
                $laundryPlan->pounds = $laundryPlan->new_pounds;
                $laundryPlan->days = $laundryPlan->new_days;
                $laundryPlan->rollover = $laundryPlan->new_rollover;
                $laundryPlan->expired_price = $laundryPlan->new_expired_price;
                $laundryPlan->credit_amount = $laundryPlan->new_credit_amount;
                $laundryPlan->expired_discount_percent = $laundryPlan->new_expired_discount_percent;
                $laundryPlan->products = $laundryPlan->new_products;
            }

            if ($laundryPlan->plan_type == "laundry_plan") {
                $goBack = '/admin/customers/customer_plans';
                $result = $this->order_model->renew_laundry_plan($laundryPlan);
            } elseif ($laundryPlan->plan_type == "product_plan") {
                $goBack = '/admin/customers/customer_everything_plans';
                $result = $this->order_model->renew_product_plan($laundryPlan);
            } else {
                $goBack = '/admin/customers/customer_product_plans';
                $result = $this->order_model->renew_everything_plan($laundryPlan);
            }

            if ($result['status'] != 'success') {
                set_flash_message("error", "There was an error. Please, try again." . print_r($result, true));
            } else {
                set_flash_message("success", 'The customer plan ID has been updated. <a target="_blank" href="/admin/orders/details/'.$result["order_id"].'"><small>View order</small></a>');
            }
        } catch (\InvalidArgumentException $exception) {
            set_flash_message("error", "There was an error. Please, try again. Error: " . $exception->getMessage());
        }
        
        $this->goBack($goBack);
    }

    /**
     *  Updates a property in the customer instance.
     * The property is allowed to be updated only if it is defined in $allowed_properties.
     * Expects the following POST parameters
     *  customerID
     *  property
     *  value
     */
    public function update()
    {
        if (!$this->input->post("customerID")) {
            output_ajax_error_response("'customerID' must be passed as a POST parameter.");
        }
        $customerID = $this->input->post("customerID");
        if (!is_numeric($customerID)) {
            output_ajax_error_response("'customerID' must be numeric.");
        }

        if (!$this->input->post("property")) {
            output_ajax_error_response("'property' must be passed as a POST parameter.");
        }
        if ($this->input->post("value") === false) {
            output_ajax_error_response("'value' must be passed as a POST parameter.");
        }
        $value = trim($this->input->post("value"));
        $property = trim($this->input->post("property"));
        try {
            $customer = new Customer($customerID);
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            output_ajax_error_response("Customer not found");
        }
        if ($customer->business_id != $this->business_id) {
            output_ajax_error_response("This customer does not belong to your business");
        }
        $customer->$property = $value;
        try {
            $customer->save();
        } catch (App\Libraries\DroplockerObjects\Validation_Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
        output_ajax_success_response(array("value" => $value, "message" => "Updated " . getPersonName($customer) . " $customer->customerID"));
    }


    /**
     * Sets the internal notes property of the customer.
     * Expects the following POST parameters
     *  customerID
     *  internalNotes
     * @throws Exception
     */
    public function set_internal_notes()
    {
        try {
            if (!$this->input->post("customerID")) {
                throw new Exception("'customerID' must be numeric.");
            }
            $customerID = $this->input->post("customerID");
            if (!is_numeric($customerID)) {
                throw new Exception("'customerID' must be numeric.");
            }

            $internalNotes = $this->input->post("internalNotes");
            $this->customer_model->options = array("internalNotes" => $internalNotes, "where" => array("customerID" => $customerID, 'business_id' => $this->business_id));
            $this->customer_model->update();

            if ($this->input->is_ajax_request()) {
                echo json_encode(array("status" => "success"));
            } else {
                set_flash_message("Succesfsully set the internal notes.");
                redirect($_SERVER['HTTP_REFERER']);
            }
        } catch (Exception $e) {
            send_exception_report($e);
            if ($this->input->is_ajax_request()) {
                echo json_encode(array("status" => "error"));
            } else {
                set_flash_message("Could not set internal notes.");
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    public function outOfPattern()
    {
        if ($this->input->post("Run")) {
            $getCustomers = "select * from reports_customer
                                where business_id = ?
                                and (totGross - totDisc) > ?
                                and date_sub(now(), INTERVAL avgOrderLapse + ? DAY) > lastOrder
                                and lastOrder >= date_sub(now(), INTERVAL 60 DAY)";
            $query = $this->db->query($getCustomers, array(
                $this->business_id,
                $this->input->post("spend"),
                $this->input->post("overDays")
            ));
            $content['customers'] = $query->result();
        }
        $this->template->write('page_title', ' - Out of Pattern Report');
        $this->data['content'] = $this->load->view('admin/customers/outOfPattern', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    //this will make a new email that's a copy of the old one, and then queue it up
    public function resend_customer_email($original_email_id = 0)
    {
        if (empty($original_email_id)) {
            set_flash_message('error', 'There was an error resending the email');
            redirect($this->agent->referrer());
        }

        $email_query = $this->db->get_where('email', array('emailID' => $original_email_id));
        $original_email = $email_query->row();

        if ($original_email->business_id != $this->business_id) {
            set_flash_message('error', 'This email does not belong to your business.');
            redirect($this->agent->referrer());
        }

        $validation_check = array();

        if (empty($original_email->to)) {
            array_push($validation_check, 'To');
        }

        if (empty($original_email->body)) {
            array_push($validation_check, 'Body');
        }

        if (empty($validation_check)) {
            if (!empty($original_email->subject)) {
                queueEmail($original_email->fromEmail, $original_email->fromName, $original_email->to, $original_email->subject, $original_email->body, $cc = array(), null, $original_email->business_id, $original_email->customer_id);
                set_flash_message("success", "The email was resent!");
            } else {
                $customer = new Customer($original_email->customer_id);
                Email_Actions::send_sms($customer, $original_email->body, $original_email->business_id, array());
                set_flash_message("success", "The SMS was resent!");
            }

            redirect($this->agent->referrer());
        } else {
            //set a flash message to go back, this way we can show a little error message
            set_flash_message('error', 'Error, email missing fields: ' . implode(',', $validation_check));
            redirect($this->agent->referrer());
        }
    }

    public function filters($field = null, $comparison = null, $value = null, $offset = 0)
    {
        $limit = 25;
        $comparators = array(
            1 => '=',
            2 => '!=',
            3 => '>',
            4 => '<',
            5 => '>=',
            6 => '<=',
            7 => 'LIKE',
            8 => 'NOT LIKE',
            9 => 'IN',
            10 => 'NOT IN',
        );

        if (!empty($field)) {
            $field = explode("|", urldecode($field));
            $comparison = explode("|", urldecode($comparison));
            $value = explode("|", urldecode($value));
        }

        if ($this->input->post('field')) {
            $field = $this->input->post('field');
            $comparison = $this->input->post('comparison');
            $value = $this->input->post('amount');
        }

        if ($field != null) {
            $offset = intval($offset);

            //is this being used at all?
            if (!empty($_GET['select'])) {
                $content['select'] = $_GET['select'];
            }

            $where = array();
            foreach ($field as $k => $f) {
                $rawValue = $value[$k];

                if ($comparators[$comparison[$k]] == 'IN' || $comparators[$comparison[$k]] == 'NOT IN') {
                    $values = array();
                    foreach (explode(',', $value[$k]) as $v) {
                        $values[] = $this->db->escape($v);
                    }
                    $fvalue = '(' . implode(',', $values) . ')';
                } else {
                    $fvalue = $this->db->escape($value[$k]);
                }

                // These are custom columns
                switch ($f) {
                    case "daysSinceSignUp":
                        $f = "DATEDIFF(NOW(), signupDate)";
                        break;
                }

                $where[] = $this->db->protect_identifiers($f) . ' ' . $comparators[$comparison[$k]] . ' ' . $fvalue;
            }

            //query to get total rows
            $getCount = sprintf(
                'select count(*) as total from reports_customer where %s and business_id = %d',
                implode(" AND ", $where),
                $this->business_id
            );
            $countQuery = $this->db->query($getCount);
            $countResult = $countQuery->result();
            $total = $countResult[0]->total;

            //query that will be shown on view
            $queryToShow = sprintf(
                'select * from reports_customer where %s and business_id = %d',
                implode(" AND ", $where),
                $this->business_id
            );

            //paginated query
            $getResults = $queryToShow . ' LIMIT ?,?';
            $query = $this->db->query($getResults, array($offset, $limit));

            //pagination settings
            $this->load->library('pagination');

            //clean values for pagination
            foreach ($value as $k => $v) {
                $value[$k] = urlencode($v);
            }
            $config['base_url'] = "/admin/customers/filters/" . implode("|", $field) . "/" . implode("|", $comparison) . "/" . implode("|", $value) . "/";
            $config['total_rows'] = $total;
            $config['per_page'] = $limit;
            $config['num_links'] = 10;
            $config['uri_segment'] = 7;
            $this->pagination->initialize($config);

            $content['total'] = $total;
            $content['paging'] = $this->pagination->create_links();
            $content['query'] = $queryToShow;
            $content['rowCount'] = $content['showing'] = $query->num_rows();
            $content['total'] = $total;
            $content['results'] = $query->result();
            $content['queryToShow'] = $queryToShow;
            $content['firstRecord'] = $offset + 1;
            $content['lastRecord'] = $offset + $content['rowCount'];
        }

        $content['comparators'] = $comparators;

        $getCustomerFields = "select * from reports_customer where business_id = $this->business_id limit 1";

        $query = $this->db->query($getCustomerFields);
        $content['fields'] = (array)$query->row();
        $content['fields']["daysSinceSignUp"] = "1";

        $getFilters = "select * from filter where business_id = $this->business_id";
        $query = $this->db->query($getFilters);
        $content['filters'] = $query->result();

        $this->template->write('page_title', ' - Create Filters');
        $this->data['content'] = $this->load->view('admin/customers/filters', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function view_filter($id = null, $offset = 0)
    {
        $limit = 25;

        if (!empty($id)) {
            $content["id"] = $id;
            $offset = intval($offset);

            $selectedFilter = "select * from filter where filterID = ? limit 0,1";
            $query = $this->db->query($selectedFilter, array($id));
            $filter = $query->result();
            $queryToShow = $filter[0]->query;

            if ($filter[0]->business_id != $this->business_id) {
                set_flash_message("error", "This filter does not belong to your business.");
                redirect("/admin/customers/filters");
            }

            //query to get total rows
            $countQuery = $this->db->query($queryToShow);
            $total = $countQuery->num_rows();

            //paginated query
            $getResults = $queryToShow . ' LIMIT ?,?';
            $query = $this->db->query($getResults, array($offset, $limit));

            //pagination settings
            $this->load->library('pagination');
            $config['base_url'] = "/admin/customers/view_filter/" . $id;
            $config['total_rows'] = $total;
            $config['per_page'] = $limit;
            $config['num_links'] = 10;
            $config['uri_segment'] = 5;
            $this->pagination->initialize($config);

            $content['total'] = $total;
            $content['paging'] = $this->pagination->create_links();
            $content['query'] = $queryToShow;
            $content['rowCount'] = $content['showing'] = $query->num_rows();
            $content['total'] = $total;
            $content['results'] = $query->result();
            $content['queryToShow'] = $queryToShow;
            $content['firstRecord'] = $offset + 1;
            $content['lastRecord'] = $offset + $content['rowCount'];
            $content['filterID'] = $id;
        }

        $getCustomerFields = "select * from reports_customer where business_id = $this->business_id limit 1";
        $query = $this->db->query($getCustomerFields);
        $content['fields'] = (array)$query->row();
        $content['fields']["daysSinceSignUp"] = "1";

        //coupons
        $this->load->model('coupon_model');
        $coupons = $this->coupon_model->get_active_coupons($this->business_id);

        $content['coupons'] = array();
        foreach ($coupons as $coupon) {
            $content['coupons'][$coupon->couponID] = sprintf("%s%s", $coupon->prefix, $coupon->code);
        }

        $this->template->write('page_title', ' - View Filter');
        $this->data['content'] = $this->load->view('admin/customers/filters', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function filter_data_dump($id = null)
    {
        $this->reporting_database = $this->load->database("reporting", true);
        if (DEV) {
            $this->reporting_database = $this->db;
        }

        $selectedFilter = "select * from filter where filterID = ? limit 0,1";
        $query = $this->reporting_database->query($selectedFilter, array($id));
        $filter = $query->result();
        $queryToDump = $filter[0]->query;

        if ($filter[0]->business_id != $this->business_id) {
            set_flash_message("error", "This filter does not belong to your business.");
            redirect("/admin/customers/filters");
        }

        $query = $this->reporting_database->query($queryToDump);

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');

        $output = fopen('php://output', 'w');

        $result = $query->result();
        if (!empty($result)) {
            fputcsv($output, array_keys((array)$result[0]));

            foreach ($result as $row) {
                fputcsv($output, (array)$row);
            }
        }

        die();
    }

    public function add_filter()
    {
        $addFilter = "insert into filter (name, query, business_id) values (?, ?, ?)";
        $query = $this->db->query($addFilter, array($this->input->post("name"), $this->input->post("query"), $this->business_id));

        set_flash_message('success', 'Filter Created');
        redirect("/admin/customers/filters");
    }

    public function delete_filter()
    {
        $deleteFilter = "delete from filter where filterID = " . $_GET['id'];
        $query = $this->db->query($deleteFilter);

        set_flash_message('success', 'Filter Deleted');
        redirect("/admin/customers/filters");
    }

    protected function getCustomerById($customer_id, $redirect_to = "/admin/customers")
    {
        try {
            $customer = new Customer($customer_id, false);
        } catch (Exception $e) {
            set_flash_message('error', "Customer [$customer_id] does not exist");
            redirect($redirect_to);
        }

        if ($customer->business_id != $this->business_id) {
            set_flash_message("error", "This customer does not belong to your business.");
            redirect($redirect_to);
        }

        return $customer;
    }
}
