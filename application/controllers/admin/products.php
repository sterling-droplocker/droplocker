<?php
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\ProductCategory;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\UpchargeGroup;


class Products extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("product_model");
    }

    /**
     * Retrieves the total number of products for the current business
     * @throws \LogicException if the request is not AJAX
     * @throws \InvalidArgumentException if the request does noti nclude a GET parameter for the business ID
     */
    public function get_total()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->get('business_id')) {
                $business_id = $this->input->get('business_id');
                try {
                    $product_total = $this->product_model->get_count($business_id);
                    $result = array("status" => "success", "total" => $product_total);
                } catch (\Exception $e) {
                    $result = array("status" => "error", "message" => "An error occurred getting the total products for the business");
                    send_exception_report($e);
                }
            } else {
                throw new \InvalidArmuentException("'business_id' must be passed as a GET parameter.");
            }

            echo json_encode($result);
        } else {
            throw new \LogicException("This action can only be called with an ajax request");
        }
    }

    /**
     * Updates a product's product category
     *
     */
    public function update_productCategory()
    {
        $product_id = $this->input->post("product_id");
        $new_productCategory_id = $this->input->post("productCategory_id");

        //get max sortOrder of new category and assign it to product
        $sql = "select ifnull(max(sortOrder)+1, 0) as sort_order from product where productCategory_id = ? AND business_id=? limit 0,1";
        $sortOrder = $this->db->query($sql, array($new_productCategory_id, $this->session->userdata['business_id']))->result();
        $sortOrder = empty($sortOrder)?0:$sortOrder[0]->sort_order;

        //assign new category to product
        $this->db->query("UPDATE product SET 
            productCategory_id = ?,
            sortOrder = ? 
            WHERE productID=? AND business_id=?", array($new_productCategory_id, $sortOrder, $product_id, $this->session->userdata['business_id']));

        echo json_encode(array("status" => "success"));
    }
    
    public function update_product_image()
    {
        $business_id = $this->session->userdata['business_id'];
        $product_id = $this->input->post('product_id');
        $image_id = $this->input->post('image_id');

        $this->load->model('product_model');
        $res = $this->product_model->update(array(
                'image_id' => $image_id
            ), array(
                'productID' => $product_id, 
                'business_id' => $business_id
        ));
        
        if ($res) {
            set_flash_message('success', 'Product image updated');
        } else {
            set_flash_message('error', 'Error while updating product image');
        }
        
        $this->goBack("/admin/admin");
    }

    /**
     * The following action retrieves all the products for the specified product category and returns the results in a JSON structure, with the product ID as the index and the produt display name as the value.
     * Note, this action can also take a special value "show_all" for showing every product associated with the business, instead of associated with a specific category
     * Expects 'productCategory_id' as a GET parameter that specifies which product category to retrieve the products
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * Product categoriy needs to be sorted
     * 'Group' needs to be called display name
     * The product menu should have tab priority over the product category
     */
    public function get_products_in_category()
    {
        if ($this->input->is_ajax_request()) {
            if (!$this->input->get("productCategory_id")) {
                throw new \InvalidArgumentException("'productCategory_id' must be passed as a GET parameter");
            }
            $productCategory_id = $this->input->get("productCategory_id");
            if ($productCategory_id == "show_all") {
                $get_products_in_category_query = $this->db->query("SELECT productID,product.name,upchargeGroup_id FROM product WHERE business_id = ? ORDER BY name", $this->business_id);
            } else {
                $get_products_in_category_query = $this->db->query("SELECT productID,product.name,upchargeGroup_id FROM product INNER JOIN productCategory ON productCategory_id=productCategoryID WHERE productCategory_id = ? AND product.business_id = ? ORDER BY name", array($productCategory_id, $this->business_id));
            }
            $products = $get_products_in_category_query->result();
            $result = array();
            foreach ($products as $product) {
                $result[$product->productID]['name'] = $product->name;
                $result[$product->productID]['upchargeGroup_id'] = $product->upchargeGroup_id;
            }
            echo json_encode($result);
        } else {
            throw new \LogicException("This action can only be accessed through an AJAX request");
        }
    }

    /**
     * This action ios intended to be only used when the server is in Bizze mode
     * Adds a master product, and then copies that product to all slave businesses.
     * Expects the following POST parameters
     *  name:
     *  displayName:
     *  productCategory_id:
     *  notes:
     *  unitOfMeasurement
     *  productType:
     *  price:
     *
     * @throws \Exception
     */
    public function add_master_product()
    {

        if (!$this->input->post("name")) {
            throw new \Exception("Missing 'name' parameter");
        }
        $name = $this->input->post("name");

        if (!$this->input->post("displayName")) {
            throw new \Exception("Missing 'displayName' parameter");
        }
        $displayName = $this->input->post("displayName");

        if (!$this->input->post("productCategory_id")) {
            throw new \Exception("Missing 'productCategory_id' parameter.");
        }
        $productCategory_id = $this->input->post('productCategory_id');

        if ($this->input->post("notes")) {
            $notes = $this->input->post("notes");
        } else {
            $notes = "";
        }

        if (!$this->input->post("unitOfMeasurement")) {
            throw new \Exception("Missing 'unitOfMeasurement' parameter.");
        }
        $unitOfMeasurement = $this->input->post("unitOfMeasurement");

        if (!$this->input->post("productType")) {
            throw new \Exception("Missing 'productType' parameter.");
        }

        $product_process_id = $this->input->post("product_process_id");

        if ($this->input->post("price")) {
            $price = $this->input->post("price");
        } else {
            $price = "0.00";
        }
        $productType = $this->input->post("productType");
        $master_business = get_master_business();

        $maximum_sortOrder = $this->db->query("SELECT MAX(sortOrder) AS sortOrder FROM product WHERE productCategory_id = ? AND business_id= ?", array($productCategory_id, $master_business->businessID))->result();
        $master_product = new Product();
        $master_product->name = $name;
        $master_product->notes = $notes;
        $master_product->unitOfMeasurement = $unitOfMeasurement;
        $master_product->displayName = $displayName;
        $master_product->productType = $productType;
        $master_product->sortOrder = $maximum_sortOrder[0]->sortOrder + 1;
        $master_product->productCategory_id = $productCategory_id;
        $master_product->business_id = $master_business->businessID;
        $master_product->active = 1;
        $master_product->save();

        $master_product_process = new Product_Process();
        $master_product_process->product_id = $master_product->{Product::$primary_key};
        $master_product_process->process_id = $product_process_id;
        $master_product_process->price = $price;
        $master_product_process->active = 1;
        $master_product_process->save();

        $this->load->model("business_model");
        $slave_businesses = $this->business_model->get_all_slave_businesses();
        foreach ($slave_businesses as $slave_business) {
            $slave_product = $master_product->copy();
            $slave_product->business_id = $slave_business->businessID;
            $slave_product->save();

            $slave_product_process = $master_product_process->copy();
            $slave_product_process->product_id = $slave_product->{Product::$primary_key};
            $slave_product_process->save();
        }
        set_flash_message("success","Added product {$master_product->name} ({$master_product->productID}) to all businesses");

        $this->goBack("/admin/superadmin/manage_dryclean_module");
    }

    /**
     * The following function is a codeigniter form validator callback rule that asserts that the specified product category exists.
     * @param int $productCategory_id
     * @return boolean
     */
    public function does_productCategory_exist($productCategory_id)
    {
        $this->load->model("productCategory_model");
        if ($this->productCategory_model->does_exist($productCategory_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_product_category_exist","Product Category ID '$productCategory_id' does not exist");

            return false;
        }
    }

    /**
     * Used for codeigniter form validator rule callback to assert that the specified upcharge group exists
     * @param int $upchargeGroup_id
     * @return boolean
     */
    public function does_upchargeGroup_exist($upchargeGroup_id)
    {
        $this->load->model("upchargeGroup_model");
        //Note, upcharge group is optional. No upcharge group s sometimes passed incorrectly as '0' by * code.
        if ($upchargeGroup_id == 0 || $this->upchargeGroup_model->does_exist($upchargeGroup_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_upchargeGroup_exist","Upcharge Group ID '$upchargeGroup_id' does not exist");

            return false;
        }
    }

    /**
     * Used for the coeigniter form validator rule callback to assert that the specified product process exists.
     * @param int $product_process_id
     * @return boolean
     */
    public function does_process_exist($process_id)
    {
        $this->load->model("process_model");
        //Note process is optional. A product with no process is sometimes passed incorrectly as '0' by * code.
        if ($process_id == 0 || $this->process_model->does_exist($process_id)) {
            return true;
        } else {
            $this->form_validation->set_message("does_process_exist", "Product process ID '$process_id' does not exist");

            return false;
        }
    }

    /**
     * The following action adds a product to the database
     * @throws Exception
     * Expects the following POST parameters:
     *  name: the product name
     *  displayName: the product display name
     *  productCategory_id: the product category ID
     *  notes: the product notes
     *  unitOfMeasurement:
     *  productType: the product type
     *  price: the product price, must be a numeric value
     */
    public function add_product()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("name", "Product Name",  "required");
        $this->form_validation->set_rules("displayName", "Display Name", "required");
        $this->form_validation->set_rules("productCategory_id", "Product Category", "required|numeric|callback_does_productCategory_exist");
        $this->form_validation->set_rules("unitOfMeasurement", "Unit of Measurement", "required");
        $this->form_validation->set_rules("productType", "Product Type", "required");
        $this->form_validation->set_rules("price", "Price", "numeric");
        $this->form_validation->set_rules("upchargeGroup_id", "Upcharge Group",  "integer|callback_does_upchargeGroup_exist");
        $this->form_validation->set_rules("process_id", "Process", "integer|callback_does_process_exist");

        if ($this->form_validation->run()) {
            $product = new Product();

            if ($this->input->post("price")) {
                $price = $this->input->post("price");
            } else {
                $price = "0.00";
            }

            if ($this->input->post("notes")) {
                $product->notes = $this->input->post("notes");
            } else {
                $product->notes = "";

            }
            if ($this->input->post("upchargeGroup_id")) {
                $product->upchargeGroup_id = $this->input->post("upchargeGroup_id");
            }

            $product->name = $this->input->post("name");
            $product->displayName = $this->input->post("displayName");
            $product->unitOfMeasurement = $this->input->post("unitOfMeasurement");
            $productType = $this->input->post("productType");

            $get_maximum_sortOrder_query = $this->db->query("SELECT MAX(sortOrder) AS sortOrder FROM product WHERE productCategory_id = ? AND business_id = ?", array($this->input->post("productCategory_id"), $this->business_id));
            $maximum_sortOrder_result = $get_maximum_sortOrder_query->result();

            $product->productType = $productType;
            $product->sortOrder = $maximum_sortOrder_result[0]->sortOrder + 1;
            $product->productCategory_id = $this->input->post('productCategory_id');
            $product->image_id = $this->input->post('image_id');
            $product->business_id = $this->business_id;
            $product->save();

            $product->get_relationships();

            $product_process = new Product_Process();
            $product_process->product_id = $product->{Product::$primary_key};
            $product_process->process_id = $this->input->post("process_id");
            $product_process->price = $price;
            $product_process->save();
            $product_process->get_relationships();
            set_flash_message("success", "Successfully created product '{$product->displayName}' with process '{$product_process->relationships['Process'][0]->name}' for product category '{$product->relationships['ProductCategory'][0]->name}'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/admin");
    }

    /**
     * Updates a product's properties specified from an incoming AJAX request from the jQuery Jeditable plugin
     * Required post parameters:
     *  productID: the product's unique identifier
     *  property: The name of the column that will be updated
     *  value: the value to set the product's property to
     * @throws \InvalidArgumentException
     */
    public function update()
    {
        try {
            if (!$this->input->post("productID")) {
                throw new \InvalidArgumentException("'productID' must be passed as a POST parameter.");
            }
            $productID = trim($this->input->post("productID"));

            if (!is_numeric($productID)) {
                throw new \InvalidArgumentException("'productID' must be numeric.");
            }
            if (!$this->input->post("property")) {
                throw new \InvalidArgumentException("'property' must be passed as a POST parameter.");
            }
            if (!array_key_exists("value", $_POST)) {
                throw new \InvalidArgumentException("'value' must be passed as a POST parameter.");
            }
            $value = trim($this->input->post("value"));
            $property = trim($this->input->post("property"));

            $product = new \App\Libraries\DroplockerObjects\Product($productID);

            // If the server is in Bizzie mode and the user is a superadmin,
            // update all the product properties for every business with the properties
            // in the master product
            if (in_bizzie_mode() && empty($product->base_id) && is_superadmin()) {
                $master_product = $product;
                $master_product->$property = $value;
                $master_product->save();

                $this->load->model("business_model");
                $slave_businesses = $this->business_model->get_all_slave_businesses();
                foreach ($slave_businesses as $slave_business) {
                    $slave_products = Product::search_aprax(array(
                        "base_id" => $master_product->productID,
                        "business_id" => $slave_business->businessID
                    ));

                    if (empty($slave_products)) {
                        $slave_product = $master_product->copy();
                        $slave_product->business_id = $slave_business->businessID;
                        $slave_product->base_id = $master_product->productID;
                    } else {
                        $slave_product = $slave_products[0];
                        $slave_product->$property = $master_product->$property;
                    }
                    $slave_product->save();
                }

            } else {
                // only the specified product for the user session's business isupdated.
                $product->$property = $value;
                $product->save();
            }

            switch ($property) {
                case 'upchargeGroup_id':

                    if ($value) {
                        $upchargeGroup = new UpchargeGroup($value);
                        echo $upchargeGroup->name;
                    } else {
                        echo "None";
                    }
                break;

                default:
                    echo $value;
            }

        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo "Invalid Value";
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo "Product not found";
            send_exception_report($e);
        } catch (Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo "Sorry, an error occurred";
            send_exception_report($e);
        }
    }

    /**
     * Updates the product sort order
     */
    public function update_product_sort_order_ajax()
    {
        $count = 1;
        foreach ($_POST['items'] as $a) {
            $item = explode("-", $a);
            $cat = $item[0];
            $productID = $item[1];
            $sql = "UPDATE product SET sortOrder = {$count} WHERE productID = {$productID}";
            echo $sql . ";";
            $this->db->query($sql);
            $count++;
        }
    }

    /**
     * Updates a product via ajax
     */
    public function update_product_ajax()
    {
        if (!$_POST) {
            echo json_encode(array('status' => 'fail', 'message' => 'No Post Data'));
            die();
        }

        $this->load->model('product_model');
        $this->input->post('productID');

        $product_id = $_POST['productID'];
        unset($_POST['productID']);
        $res = $this->product_model->update($_POST, array('productID' => $product_id));
        if ($res) {
            $value = '';

            if (isset($_POST['upchargeGroup_id'])) {
                $upchargeGroup = new UpchargeGroup($_POST['upchargeGroup_id']);
                $value = $upchargeGroup->name;
            }
            echo json_encode(array('status' => 'success', 'affected_rows' => $res, 'value' => $value));
        } else {
            echo json_encode(array('status' => 'fail', 'message' => 'Old Value = New Value'));
        }
    }

    /**
     * Deletes a product via ajax
     */
    public function delete_product_ajax()
    {
        if (!$_POST) {
            echo json_encode(array('status' => 'fail', 'message' => 'No Post Data'));
            die();
        }

        $this->load->model('product_model');
        $res = $this->product_model->delete($_POST);
        if ($res)
            echo json_encode(array('status' => 'success', 'affected_rows' => $res));
        else
            echo json_encode(array('status' => 'fail', 'message' => 'Could not delete ' . $_POST['productID']));
    }

    /**
     * Set the default supplier and also sets the prices for each supplier of the product process.
     */
    public function set_supplier_product_properties($product_id = null, $product_process_id = null)
    {
        try {
            $default_supplier_id = $this->input->post('default');

            if (!is_numeric($default_supplier_id)) {
                throw new \InvalidArgumentException("'default' must be numeric.");
            }

            if (!is_numeric($product_id)) {
                throw new \InvalidArgumentException("'product_id' must be numeric nad passed as segment 4 in the URL.");
            }

            if (!is_numeric($product_process_id)) {
                throw new \InvalidArgumentException("'product_process_id' must be numeric and passed as segment 4 in the URL.");
            }

            $query = "SELECT count(product_supplier.product_supplierID) as total FROM product_supplier
                INNER JOIN supplier ON supplier.supplierID=product_supplier.supplier_id
                WHERE product_supplier.supplier_id = $default_supplier_id
                    AND supplier.business_id={$this->business_id}
                    AND product_supplier.product_process_id = $product_process_id
            ";

            $result = $this->db->query($query);

            $total = $result->result();

            if ((int) $total[0]->total > 0) {
                //The following query sets the default product_supplier based on the specified supplier ID.
                $query = "UPDATE product_supplier
                    INNER JOIN supplier ON supplier.supplierID=product_supplier.supplier_id
                    SET `product_supplier`.`default` = 1
                    WHERE `product_supplier`.`supplier_id` = $default_supplier_id
                        AND supplier.business_id={$this->business_id}
                        AND product_supplier.product_process_id = $product_process_id
                ";
                $this->db->query($query);
            } else {
                $query = "INSERT INTO product_supplier(supplier_id,`default`, product_process_id) VALUES ($default_supplier_id, 1, $product_process_id)";
                $this->db->query($query);
            }
            //The following query sets all other product suppliers to NOT be the default that are not the current default supplier.
            $query = "UPDATE product_supplier
                INNER JOIN supplier ON supplier.supplierID=product_supplier.supplier_id
                SET product_supplier.`default` = 0
                WHERE product_supplier.supplier_id != $default_supplier_id
                    AND supplier.business_id = {$this->business_id}
                    AND product_supplier.product_process_id = $product_process_id";

            $this->db->query($query);
            $suppliers = $this->input->post('suppliers');
            foreach ($suppliers as $supplier_id => $cost) {
                $query = "SELECT count(product_supplier.product_supplierID) AS total FROM product_supplier
                    INNER JOIN supplier ON supplier.supplierID=product_supplier.supplier_id
                    WHERE product_supplier.supplier_id=$supplier_id
                        AND supplier.business_id={$this->business_id}
                        AND product_supplier.product_process_id = $product_process_id
                ";
                $result = $this->db->query($query);
                $total = $result->result();
                if ($total[0]->total > 0) {
                    $query = "UPDATE product_supplier
                        INNER JOIN supplier ON supplier.supplierID=product_supplier.supplier_id
                        SET cost = '$cost'
                        WHERE product_supplier.supplier_id=$supplier_id
                            AND supplier.business_id={$this->business_id}
                            AND product_supplier.product_process_id = $product_process_id

                        ";
                    $result = $this->db->query($query);
                } else {
                    $query = "INSERT INTO product_supplier(supplier_id,`default`, product_process_id, cost) VALUES ($supplier_id, 0, $product_process_id, '$cost')";
                    $result = $this->db->query($query);
                }
            }
            if ($this->input->is_ajax_request()) {
                echo json_encode(array("status" => "success"));
            } else {
                set_flash_message('success', "Update Successful!");
                redirect($this->agent->referrer());
            }
        } catch (Exception $e) {
            send_exception_report($e);
            if ($this->input->is_ajax_request()) {
                echo json_encode(array("status" => "error"));
            } else {
                set_flash_message("error", "Could not update the product's suppliers.");
                redirect($this->agent->referrer());
            }
        }
    }


}
