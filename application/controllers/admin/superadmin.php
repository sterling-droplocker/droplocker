<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\CronLog;
use App\Libraries\DroplockerObjects\DropLockerEmail;
use App\Libraries\DroplockerObjects\Settings;
use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\Image;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\DidYouKnow;
use App\Libraries\DroplockerObjects\BusinessLaundryPlan;
use App\Libraries\DroplockerObjects\QuickItem;
use App\Libraries\DroplockerObjects\DeliveryNotice;
use App\Libraries\DroplockerObjects\Language;
use App\Libraries\DroplockerObjects\Reminder_Email;
use App\Libraries\DroplockerObjects\ServiceType;

class SuperAdmin extends MY_Admin_Controller
{
    public $buser_id;
    public $business_id;
    public $pageTitle = "Super Admin -";

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('super_admin') && $this->router->fetch_method() != "change_business") {
            set_flash_message("error", "Only superadmin users can access this page");

            $this->goBack("/admin");
        }

        $this->load->model('employee_model');
        $this->load->model('business_model');
        $this->load->model('serviceType_model');
    }

    public function view_api_request_logs($offset = null)
    {
        $this->load->model("api_request_log_model");
        $this->load->library("pagination");
        $config['base_url'] = "/admin/superadmin/view_api_request_logs";
        $config['total_rows'] = $this->api_request_log_model->count();
        $config['per_page']= 100;
        $config['num_links'] = 5;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $content['pagination'] = $this->pagination->create_links();
        $content['api_request_logs'] = $this->api_request_log_model->get_aprax(array(), "api_request_logID DESC", null, $config['per_page'], $offset);
        $this->data['content'] = $this->load->view("admin/superadmin/view_api_request_logs", $content, true);
        $this->template->write("page_title", "API Request Logs", true);
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }

    public function manage_aclresources()
    {
        $this->load->model("aclresource_model");
        $content['aclResources'] = $this->aclresource_model->get_aprax();

        $this->data['content'] = $this->load->view('admin/superadmin/manage_aclresources', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    /**
     * The following action copies the subject and body properties of each reminder to new the new reminder_email entities.
     */
    public function migrate_to_reminder_emails()
    {
        $this->load->model("reminder_model");
        $reminders = $this->reminder_model->get_all();
        $this->load->model("business_model");
        $this->load->model("reminder_email_model");
        foreach ($reminders as $reminder) {
            $business = $this->business_model->get_by_primary_key($reminder->business_id);
            if (is_numeric($business->default_business_language_id)) {
                $reminder_email = $this->reminder_email_model->get_one(array("business_language_id" => $business->default_business_language_id, "reminder_id" => $reminder->reminderID));
                if (empty($reminder_email) && !empty($reminder->subject) && !empty($reminder->body)) {
                    Reminder_Email::create($business->default_business_language_id, $reminder->reminderID, $reminder->subject, $reminder->body);
                }
            } else {
                echo("<p> Business '{$business->companyName}' ({$business->businessID}) does not have a default business language ID </p>");
            }
        }
    }
    /**
     * The following action retrieves the language subtag registry, parses the text, and creates new language entities based on the registry information.
     */
    public function import_language_subtag_registry()
    {
        $language_subtag_registry = file_get_contents("http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry");
        $languages = explode("%%", $language_subtag_registry);
        foreach ($languages as $language) {
            $properties = explode("\n", $language);
            $type_string = $properties[1];
            $type_array = explode(":", $type_string);
            $type = trim($type_array[1]);

            $subtag_string = $properties[2];
            $subtag_array = explode(":", $subtag_string);
            $subtag = trim($subtag_array[1]);

            $description_string = $properties[3];
            $description_array = explode(":", $description_string);
            $description = trim($description_array[1]);

            if ($type == "language") {
                $this->load->model("language_model");
                $language_count= $this->language_model->count(array("tag" => $subtag));
                if ($language_count < 1) {
                    $new_language_entity = new Language();
                    $new_language_entity->tag = $subtag;
                    $new_language_entity->description = $description;
                    $new_language_entity->save();
                    echo "Added new language tag '$subtag' with description '$description' \n \n";
                } else {
                    echo "A language with tag '$subtag' already exists \n \n";
                }
            } else {
                continue;
            }
        }
    }

    public function manage_master_did_you_knows()
    {
        parent::did_you_know(true);
    }
    /**
     * Renders the superadmin interface for managing print notices.
     * This action is only intended to be accessed with the server is in Bizzie mode.
     */
    public function manage_print_notices()
    {
        if (!in_bizzie_mode()) {
            set_flash_message("error", "This action can only be accessed when the application is in Bizzie mode.");
            $this->goBack("/admin");
        }

        $content = array();
        $content['master_business'] = get_master_business();
        $business_id = $content['master_business']->businessID;
        $this->setPageTitle("Manage Master Print Notices");

        $content['printer_key'] = get_business_meta($this->business_id, 'printer_key');
        $content['deliveryNotices'] = DeliveryNotice::search_aprax(array('business_id' => $business_id));
        $content['business_id'] = $business_id;

        $this->renderAdmin('/admin/notices/list_print_notice', $content);
    }

    /**
     * The following action is *, do not use.
     * This is a hidden page that allows arik to credit a customer
     * NOTE: these are hardcoded... We do not want to allow other businesses to use this yet
     */
    public function credit()
    {
        if (isset($_POST['submit'])) {
            // credit the customer
            $this->load->library('creditcardprocessor');
            $processor = $this->creditcardprocessor->factory('paypro', 3);

            $amount = $_POST['amount'];
            $customer_id = $_POST['customer_id'];
            $business_id = 3;
            $response = $processor->credit($amount, $customer_id, $business_id, $order_id = 0);

            if ($response->get_status() == "SUCCESS") {
                set_flash_message("success", "Customer has been credited");
            } else {
                set_flash_message("error", $response->getMessage());
            }
            redirect($this->uri->uri_string());
        }

        $this->data['content'] = $this->load->view('admin/superadmin/credit', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function add_master_deliveryNotice()
    {
        $content = array();
        $deliveryNotice = new DeliveryNotice();
        if ($this->input->post("submit")) {
            $master_business = get_master_business();
            $deliveryNotice = new DeliveryNotice;
            $master_deliveryNotice = $deliveryNotice;
            $master_deliveryNotice->business_id = $master_business->{Business::$primary_key};
            if ($this->input->post("barcodeText")) {
                $master_deliveryNotice->barcodeText = $this->input->post("barcodeText");
            }
            if ($this->input->post("noticeText")) {
                $master_deliveryNotice->noticeText = $this->input->post("noticeText");
            }
            $master_deliveryNotice->save();
            $this->load->model("business_model");
            $slave_businesses = $this->business_model->get_all_slave_businesses();
            foreach ($slave_businesses as $slave_business) {
                $slave_deliveryNotice = $master_deliveryNotice->copy();
                $slave_deliveryNotice->base_id = $master_deliveryNotice->{DeliveryNotice::$primary_key};
                $slave_deliveryNotice->business_id = $slave_business->{Business::$primary_key};
                $slave_deliveryNotice->save();
            }
            set_flash_message("success", "Created new delivery notice for all businesses");
            redirect("/admin/superadmin/manage_print_notices");
        }
        $this->data['content'] = $this->load->view('admin/admin/add_notice', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Note, Bizzie only uses the dryclean module, so the interface displays only products under the dryclean module
     * This action can only be accessed if the server is in Bizzie mode.
     */
    public function manage_dryclean_module()
    {
        if (!in_bizzie_mode()) {
            set_flash_message("error", "The server must be in Bizzie mode to access this page");
            $this->goBack("/admin");
        } else {
            redirect("/admin/modules/preferences/dry_clean/products/1");
        }
    }

    /**
     * Changes the business that the superadmin is logged into
     * @throws \InvalidArgumentException
     */
    public function change_business()
    {
        $this->load->model('employee_log_model');
        $this->employee_log_model->switch_business(
            $this->input->post("current_business_id"),
            $this->employee->employeeID,
            $this->agent->referrer()
        );
        
        $is_super_admin = is_superadmin();
        $allowed_businesses = allowed_businesses();

        if (!$is_super_admin && !$allowed_businesses) {
            set_flash_message("error", "You are not allowed to change between businesses");
            $this->goBack("/admin");
        }

        if (!$this->input->post("current_business_id")) {
            set_flash_message("error", "'current_business_id' must be passed as a POST parameter");
            return $this->goBack("/admin");
        }

        $business_id = $this->input->post("current_business_id");
        if (!is_numeric($business_id)) {
            set_flash_message("error", "'current_business_id' must be numeric");
            return $this->goBack("/admin");
        }

        if (!$is_super_admin && !in_array($business_id, $allowed_businesses)) {
            set_flash_message("error", "You do not have access to that businesses");
            $this->goBack("/admin");
        }

        $business = new Business($business_id);
        $business_employees = Business_Employee::search_aprax(array("employee_id" => $this->employee->employeeID));
        if (empty($business_employees)) {
            $business_employee = new Business_Employee();
        } else {
            $business_employee = $business_employees[0];
        }
        $business_employee->default = 1;
        $business_employee->business_id = $business_id;
        $business_employee->save();

        $this->session->set_userdata('business_id', $business_employee->business_id);
        $this->session->set_userdata('buser_id', $business_employee->{Business_Employee::$primary_key});
        $this->session->set_userdata("companyName", sprintf("%s - %s, %s", $business->companyName, $business->city, $business->state));

        $this->session->set_userdata('employee_id', $this->employee->employeeID);
        $this->session->set_userdata('buser_fname', $this->employee->firstName);
        $this->session->set_userdata('buser_lname', $this->employee->lastName);
        $this->session->set_userdata('timezone', $this->employee->timezone);
        $this->session->set_userdata('super_admin', $this->employee->superAdmin);

        set_flash_message("success", "Switched to business {$business->companyName} ({$business->city})");
        redirect($this->agent->referrer());
    }


    /**
     * The following function manages the did you know notices across all businesses. This action is intended to be only used when the server is in Bizzie mode.
     */
    public function manage_did_you_know()
    {
        $master_business = get_master_business();
        $this->load->model("business_model");
        $slave_businesses = $this->business_model->get_all_slave_businesses();
        if ($this->input->post("update")) {
            $master_didYouKnow = new DidYouKnow($this->input->post("id"));
            $master_didYouKnow->note = $this->input->post("note");
            $master_didYouKnow->save();
            foreach ($slave_businesses as $slave_business) {
                $didYouKnows = DidYouKnow::search_aprax(array("base_id"=> $master_didYouKnow->{DidYouKnow::$primary_key}, "business_id" => $slave_business->businessID));
                if (empty($didYouKnows)) {
                    $didYouKnow = new DidYouKnow();
                    $didYouKnow->business_id = $slave_business->businessID;
                    $didYouKnow->base_id = $master_didYouKnow->{DidYouKnow::$primary_key};
                } else {
                    $didYouKnow = $didYouKnows[0];
                }

                $didYouKnow->note = $this->input->post("note");
                $didYouKnow->save();
            }

            set_flash_message("success", "Updated Did You Know notice for all business");
            redirect("/admin/superadmin/manage_did_you_know");
        } elseif ($this->input->post("add")) {
            $master_didYouKnow = new DidYouKnow();
            $master_didYouKnow->note = $this->input->post("note");
            $master_didYouKnow->business_id = $master_business->{Business::$primary_key};
            $master_didYouKnow->save();
            foreach ($slave_businesses as $slave_business) {
                $didYouKnow = new DidYouKnow();
                $didYouKnow->business_id = $slave_business->businessID;
                $didYouKnow->note = $this->input->post("note");
                $didYouKnow->base_id = $master_didYouKnow->{DidYouKnow::$primary_key};
                $didYouKnow->save();
            }
            set_flash_message("success", "Created new Did You Know notice for all businesses");
            redirect("/admin/superadmin/manage_did_you_know");
        } else {
            $this->template->write('page_title','Manage Did You Know', true);
            $content = array();
            $content['didYouKnows'] = $this->db->query("SELECT * FROM didYouKnow WHERE business_id = ?", $master_business->businessID)->result();
            $this->data['content'] = $this->load->view('admin/admin_did_you_know', $content, true);

            $this->template->write_view('content', 'inc/dual_column_left', $this->data);
            $this->template->render();
        }
    }
    /**
        * Default entry page. I do not this is used. If needed, use this page as a control panel to see a snapshot of the system
        */
    public function index()
    {
        $this->load->view('admin/blank', '', true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }


    /**
     * The following action si *.
     */
    public function manage_rewards_programs()
    {
        $content = array();

        $lockerLootConfigs = LockerLootConfig::search_aprax(array('business_id'=>$this->business_id));
        $content['lockerLootConfig'] = $lockerLootConfigs[0];

        $this->template->write('page_title','Manage Business Reward Programs', true);
        $this->data['content'] = $this->load->view('admin/admin/locker_loot', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
    * toggles the value for sending All Emails
    *
    * There is no view for this function
    */
    public function toggleSend($status = null)
    {
        Settings::setSendAllEmails($status);
        set_flash_message('success', 'Send All Emails status has been set');
        redirect("/admin/superadmin/email_admin");
    }

    /**
     * Renders the interface for managing the master business for the server
     */
    public function manage_master_business()
    {
        $this->load->model("business_model");
        $content['businesses'] = $this->business_model->get_all_as_codeigniter_dropdown();
        try {
            $content['master_business'] = get_master_business();
        } catch (\User_Exception $e) {
            $content['master_business'] = null;
        }
        $this->template->write('page_title','Manage Master Business', true);
        $this->data['content'] = $this->load->view('admin/superadmin/manage_master_business', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Updates the master business in the server mewta
     * @throws \InvalidArgumentException Thrown if the business ID is not numeric
     * @throws \LogicException Thrown if the server is not in Bizzie mode
     */
    public function update_master_business()
    {
        if (!$this->input->post("business_id")) {
            throw new \InvalidArgumentException("'business_id' must be passed as a POST parameter.");
        }
        $business_id = $this->input->post("business_id");
        if (in_bizzie_mode()) {
            Server_Meta::set("master_business_id", $business_id);
        } else {
            throw new \LogicException("The master business can only be set when the server is in 'bizzie' mode");
        }
        $new_master_business = new Business($business_id);
        set_flash_message("success", "Updated master business to '$new_master_business->companyName'");

        $this->goBack("/admin/superadmin/manage_master_business");
    }

    /**
     * The following action is *.
    * toggles the value for sending All Emails
    *
    * There is no view for this function
    */
    public function toggleEmailCron($status = null)
    {
        Settings::setEmailCronStatus($status);
        set_flash_message('success', 'Email cron status has been set');
        redirect("/admin/superadmin/email_admin");
    }

    /**
     * The following action is *.
     * toggles the value for sending All Emails
     *
     * There is no view for this function
     */
    public function toggleManualEmailProcessing($status = null)
    {
        Settings::setManualEmailProcessing($status);
        set_flash_message('success', 'Email cron status has been set');
        redirect("/admin/superadmin/email_admin");
    }


    /**
     * The following action is *.
     */
    public function updateLimit()
    {
        Settings::setEmailLimit(mysql_escape_string($_POST['emailLimit']));
        set_flash_message('success', 'Email limit has been set');
        redirect("/admin/superadmin/email_admin");
    }

    public function view_email_log()
    {
        $this->template->write('page_title','Email Log', true);

        $content = null;

        $this->data['content'] = $this->load->view('admin/superadmin/view_email_log', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * UI for managing emails
     */
    public function email_admin()
    {
        // get the setting to state whether we are sending all pending emails or only emails that are older than the current server time
        $sendAllEmails = Settings::getSendAllEmails();
        $content['sendAllEmails'] = ($sendAllEmails==1)?"ON </td><td width='110px'><a class='button green' href='/admin/superadmin/toggleSend/0'>Turn OFF</a>":"OFF  </td><td width='110px'><a class='button red' href='/admin/superadmin/toggleSend/1'>Turn ON</a>";

        $emailCronStatus = Settings::getEmailCronStatus();
        $content['emailCronStatus'] = ($emailCronStatus==1)?"ON </td><td><a class='button red' href='/admin/superadmin/toggleEmailCron/0'>Turn OFF</a>":"OFF  </td><td><a class='button green' href='/admin/superadmin/toggleEmailCron/1'>Turn ON</a>";

        $manualEmailProcessing = Settings::getManualEmailProcessing();
        $content['manualEmailProcessing'] = ($manualEmailProcessing==1)?"ON </td><td><a class='button green' href='/admin/superadmin/toggleManualEmailProcessing/0'>Turn OFF</a>":"OFF  </td><td><a class='button red' href='/admin/superadmin/toggleManualEmailProcessing/1'>Turn ON</a>";

        $content['emailLimit'] = Settings::getEmailLimit();

        $content['emails'] = $emails = DropLockerEmail::getAllPendingEmails();
        $content['totalEmails'] = sizeof($emails);

        if (sizeof($emails) >= 40) {
            $content['class'] = "emailWarning";
        }

        if (sizeof($emails) >= 50) {
            $content['class'] = "emailError";
        }

        $this->session->set_flashdata('referral',$this->uri->uri_string());
        $this->data['content'] = $this->load->view('admin/superadmin/email_admin', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function update_amazon_ses_keys()
    {
        if (isset($_POST['submit'])) {
            update_server_meta('amazon_ses_username', isset($_POST['amazon_ses_username'])?$_POST['amazon_ses_username']:'');
            update_server_meta('amazon_ses_password', isset($_POST['amazon_ses_password'])?$_POST['amazon_ses_password']:'');
            set_flash_message("success", "Amazon keys have been updated");
            redirect("/admin/superadmin/email_admin");
        } else {
            set_flash_message("error", "No post sent");
            redirect("/admin/superadmin/email_admin");
        }
    }

    /**
     * Renders the interface for viewing the properties of email templates
     */
    public function manage_email_templates()
    {
        $master_business = get_master_business();
        $this->load->model("emailactions_model");

        $content['emails'] = $this->emailactions_model->getBusinessActions($master_business->businessID);

        $this->setPageTitle('Email Templates for All Businesses');
        $this->renderAdmin('/admin/superadmin/manage_email_templates', $content);
    }

    /**
     * The following action renders the interface for editing an email template for the master business
     */
    public function edit_email_template($emailAction_id = null)
    {
        if (!is_numeric($emailAction_id)) {
            throw new \InvalidArgumentException("'emailAction_id' must be passed as segment 4 in the URL and must be numeric.");
        }

        $master_business = get_master_business();

        $this->load->model('emailactions_model');
        $email = $this->emailactions_model->getEmailTemplate($master_business->businessID, $emailAction_id);
        $content['employee'] = $this->employee;
        $content['email'] = $email;
        $content['name'] = $email->name;
        $content['message'] = ($email->customEmail=='')?$email->defaultEmail:$email->customEmail;
        $content['subject'] = ($email->customEmailSubject=='')?$email->defaultEmailSubject:$email->customEmailSubject;
        $content['textBody'] = (empty($email->customEmailText)) ? $email->defaultEmailText : $email->customEmailText;

        $content['emailActionID'] = $emailAction_id;
        //The following variable is used to specify which controller action that the form should submit a request.
        $content['update_action'] = "/admin/email_templates/update_master_email_template/$emailAction_id";

        $this->setPageTitle('Edit Email Template for All Businesses');
        $this->renderAdmin('/admin/email_templates/edit_email', $content);
    }
    /**
     * The following function renders the interface for editing an sms template for the master business
     *
     * @throws \InvalidArgumentException
     */
    public function edit_sms_template($emailAction_id = null)
    {
        if (!is_numeric($emailAction_id)) {
            set_flash_message("error", "'emailAction_id' must be passed as segment 4 of the URL and must be numeric.");
            $this->goBack("/admin/superadmin");
        }

        $this->load->model('emailactions_model');
        $sms = $this->emailactions_model->getEmailTemplate($this->business_id, $sms_id);
        $content['sms'] = $sms;
        $content['name'] = $sms->name;
        $content['message'] = ($sms->customText=='')?$sms->defaultText:$sms->customText;
        $content['subject'] = ($sms->customSmsSubject=='')?$sms->defaultSmsSubject:$sms->customSmsSubject;
        $content['update_action'] = "/admin/email_templates/update_master_sms_template/$sms_id";

        $this->load->model("emailTemplate_model");
        $content['emailTemplates_by_business_language_for_action_for_business'] = $this->emailTemplate_model->get_templates_by_business_language_for_action_for_business($emailAction_id, $this->business_id);

        $this->setPageTitle('Edit SMS Template for All Businesses');
        $this->renderAdmin('/admin/email_templates/edit_sms', $content);
    }

    public function edit_adminalert_template($emailActionID = null)
    {
        $master_business = get_master_business();

        if (!is_numeric($emailActionID)) {
            throw new \InvalidArgumentException("'emailActionID' must be passed as segment 4 of the URL and must be numeric.");
        }
        $this->load->model('emailactions_model');

        $emailTemplates = \App\Libraries\DroplockerObjects\EmailTemplate::search_aprax(array("business_id" => $master_business->businessID, "emailAction_id" => $emailActionID), true);
        if (empty($emailTemplates)) {
            $emailTemplate = new \App\Libraries\DroplockerObjects\EmailTemplate();

            $emailTemplate->business_id = $this->business_id;
            $emailTemplate->emailAction_id = $emailActionID;
        } else {
            $emailTemplate = $emailTemplates[0];
        }

        $content['business_id'] = $master_business->businessID;
        $emailTemplate = $this->emailactions_model->getEmailTemplate($master_business->businessID, $emailActionID);
        $content['sms'] = $emailTemplate;
        $content['name'] = $emailTemplate->name;
        $content['recipients'] = unserialize($emailTemplate->adminRecipients);
        $content['message'] = ($emailTemplate->customAdmin=='')?$emailTemplate->defaultAdmin:$emailTemplate->customAdmin;
        $content['subject'] = ($emailTemplate->customAdminSubject=='')?$emailTemplate->defaultAdminSubject:$emailTemplate->customAdminSubject;
        $content['update_action'] = "/admin/email_templates/update_master_adminAlert_template/$emailActionID";

        $this->setPageTitle('Edit Admin Alert Template for All Businesses');
        $this->renderAdmin('/admin/email_templates/edit_adminAlert', $content);
    }


    /**
     * ui for viewing the cron jobs
     */
    public function recent_cron_jobs()
    {
        $cronLog = new CronLog();
        $content['jobs'] = $cronLog->getAdminLog();
        $this->data['content'] = $this->load->view('admin/superadmin/list_recent_cron_jobs', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * The following action is *.
     *
     * ##################################################
     * BEGIN
     * ##################################################
     *
    * change_default method updates an employee's default business
    *
    * No UI, performs task and redirects to employee_details
     *
     * ##################################################
     * END
     * ##################################################
    */
    public function change_default($id = null, $emp_id = null)
    {
            $this->load->model('business_employee_model');

            $this->business_employee_model->where = array('employee_id'=>$emp_id);
            $this->business_employee_model->default = 0;
            $this->business_employee_model->update();

            $this->business_employee_model->clear();
            $this->business_employee_model->where = array('employee_id'=>$emp_id, 'business_id'=>$id);
            $this->business_employee_model->default = 1;
            $this->business_employee_model->update();

            set_flash_message('success', "Changed default business");
            redirect("/admin/superadmin/employee_details/".$emp_id);
    }

    /**
     * The following action shows all employees fror every business.
     */
    public function view_employees()
    {
        $content['employees'] = $this->employee_model->get_all();
        //get all businesses this employee is part of
        $getEmpBus = "SELECT * FROM business_employee
                        join business on business_id = businessID
                        join aclroles on aclRole_id = aclID";
        $q = $this->db->query($getEmpBus);
        //Check to see if anything went wrong with the query.
        $empBus = $q->result();
        $content['empBus'] = array();
        foreach ($empBus as $emp) {
            $content['empBus'][$emp->employee_id] = $emp->companyName." (".$emp->name.") ";
        }

        $this->data['content'] = $this->load->view('admin/superadmin/view_employees', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * The following action shows all employees fror every business.
     */
    public function view_active_accounts()
    {
        $this->load->model("Business_Order_Model");
        
        $content['business_orders'] = $this->Business_Order_Model->get_all();
        //get all businesses this employee is part of
        /* $getEmpBus = "SELECT * FROM business_employee
                        join business on business_id = businessID
                        join aclroles on aclRole_id = aclID";
        $q = $this->db->query($getEmpBus);
        //Check to see if anything went wrong with the query.
        $empBus = $q->result();
        $content['empBus'] = array();
        foreach ($empBus as $emp) {
            $content['empBus'][$emp->employee_id] = $emp->companyName." (".$emp->name.") ";
        } */
        
        $this->data['content'] = $this->load->view('admin/superadmin/view_active_accounts', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
    /**
     * The following action renders the interface for copying data from one business to another.
     */
    public function setup_business()
    {
        $this->load->model("business_model");
        $content['businesses'] = $this->business_model->get_all_as_codeigniter_dropdown();

        $this->template->write('page_title','Copy Tables', true);
        $this->data['content'] = $this->load->view('admin/superadmin/setup_business', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
    * UI for adding a new business to the system
    */
    public function add_business()
    {
        $content['mode'] = Server_Meta::get("mode");

        $content['locales'] = $this->config->item('locales');
        $this->load->model('servicetype_model');
        $content['serviceTypes'] =  $this->servicetype_model->get_formatted(array('business_id' => 0));
        $content['serviceTypes'] = $serviceTypes = ServiceType::search_aprax(array(
            'visible' => 1,
            'business_id' => 0,
        ));

        $this->load->model("language_model");
        $content['languages'] = $this->language_model->get_all_as_codeigniter_dropdown();
        $this->data['content'] = $this->load->view('admin/superadmin/add_business', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * The following action is *.
     * '
     * ##################################################
     * BEGIN
     * ##################################################
     *
     * UI for adding an employee to a business
     * $POST values
     * ---------------
     * employee_id
     * business_id
     * aclRole_id
     * default (optional)
     * submit
     *
     * ##################################################
     * END SCUMBAT * WHARRGARBLE
     * ##################################################
     */
    public function add_employee_to_business()
    {
    // insert the employee into the business_employee table
        if (isset($_POST['business_id'])) {
            $this->load->model('business_employee_model');
            $this->business_employee_model->business_id = $_POST['business_id'];
            $this->business_employee_model->employee_id = $_POST['employee_id'];
            $this->business_employee_model->aclRole_id = $_POST['aclRole_id'];
            $this->business_employee_model->default = 0;
            $insert_id = $this->business_employee_model->insert();

            // if default is set to 1
            if ($_POST['default']) {
                // if this employee belongs to any other businesses, we need to set them all to default = 0
                $options = array();
                    $where = array('employee_id'=>$_POST['employee_id']);
                    $options['default'] = 0;
                    $this->business_employee_model->update($options, $where);

                    $options = array();
                    // set this business to default = 1 from the insert
                    $where = array('ID'=> $insert_id);
                    $options['default'] = 1;
                    $this->business_employee_model->update($options, $where);

            }

            set_flash_message('success', "Added employee to business");
            redirect("/admin/superadmin/employee_details/".$_POST['employee_id']);
        }
    }

    /**
     * The following action is *.
     *
     * ##################################################
     * BEGIN *
     * ##################################################
     *
     * deletes a customer from a business
     *
     * ##################################################
     * END
     * ##################################################
     */
    public function delete_employee_from_business($business_employeeID = null, $emp_id = null)
    {
        if ($business_employeeID) {
            $this->load->model('business_employee_model');
            $this->business_employee_model->ID = $business_employeeID;
            $res = $this->business_employee_model->get();
            if ($res[0]->default==0) {
                    $this->business_employee_model->delete();
                    set_flash_message('success', "Deleted employee from business");
            } else {
                    set_flash_message('error', "Cannot delete the default business for this employee");
            }


            redirect("/admin/superadmin/employee_details/".$emp_id);
        }
    }

    /**
     * The following action renders the interface for managing all businesses in the system.
     */
    public function view_businesses()
    {
        $content['businesses'] = $this->business_model->get();
        $this->template->write('page_title','View Businesses', true);
        $this->data['content'] = $this->load->view('admin/superadmin/view_businesses', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * ##################################################
     * BEGIN
     * ##################################################
     *
    * Page for editing details of a business
     *
     * ##################################################
     * END
     * ##################################################
    */
    public function edit_business($business_id = null)
    {
        $content = array();
        $content['business'] = new Business($business_id);
        $content['locales'] = getLocales();
        $content['dateFormats'] = $this->config->item('dateFormats');

        $this->template->write('page_title','Edit Business', true);
        $this->data['content'] = $this->load->view('admin/admin/edit', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
         * ##################################################
         * BEGIN
         * ##################################################
         *
     * login_as method allows a superadmin to log in as an employee and view the admin section
     *
     * This is called with ajax from the employee_details method in the superadmin controller
     *
     * @todo remove ajax
         *
         * ##################################################
         * END
         * ##################################################
     */
    public function login_as()
    {
        if (isset($_POST['submit'])) {

            $options['employeeID'] = $_POST['employee_id'];
            $options['select'] = 'employeeID, firstName, lastName,superAdmin,ID';
            if ($user = $this->employee_model->get($options)) {

                $businesses = $this->employee_model->get_employee_businesses(array('employee_id'=>$user[0]->employeeID));
                    foreach ($businesses as $b) {
                        if($b->default ==1)
                        $this->session->set_userdata('business_id', $b->businessID);
                        $this->session->set_userdata('buser_id', $b->id);
                    }

            $this->session->set_userdata('user_id', $user[0]->employeeID);
            $this->session->set_userdata('buser_fname', $user[0]->firstName);
            $this->session->set_userdata('buser_lname', $user[0]->lastName);
            $this->session->set_userdata('businesses',$businesses);
            }
            echo "success";
        }
    }

    public function license_invoicing()
    {
        $getBusinesses = "SELECT 
                                *, li.licenseInvoiceID, lid.amount
                            FROM
                                business b
                                    LEFT JOIN
                                (SELECT 
                                    licenseInvoice.business_id,
                                        MAX(licenseInvoice.licenseInvoiceID) AS licenseInvoiceID
                                FROM
                                    licenseInvoice
                                GROUP BY licenseInvoice.business_id) li ON li.business_id = b.businessID
                                    LEFT JOIN
                                (SELECT 
                                    licenseInvoice_id, SUM(price) AS amount
                                FROM
                                    licenseInvoiceDetail
                                GROUP BY licenseInvoice_id) lid ON lid.licenseInvoice_id = li.licenseInvoiceID
                            WHERE
                                b.website_url != '' AND b.active = 1
                            ORDER BY b.companyName ASC";

        $q = $this->db->query($getBusinesses);
        //Check to see if anything went wrong with the query.
        if ($this->db->_error_message()) {
              throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }

        $content['businesses'] = $q->result();

        //Get amount to pay for last generated invoice
        $this->load->model("business_model");
        foreach ($content['businesses'] as $k=>$b) {           
            $content['businesses'][$k]->amount = empty($b->licenseInvoiceID)?0:(empty($b->amount)?0:$b->amount);
            $content['businesses'][$k]->has_default_business_card = $this->business_model->get_invoicing_credit_card($b->businessID);
        }

        //find out what invoices have already been generated
        $getExistingInvoices = "select * from licenseInvoice";
        $q = $this->db->query($getExistingInvoices);
        $existingInvoices = $q->result();
        foreach ($existingInvoices as $invoice) {
            $content['existingInvoices'][$invoice->business_id][date("Y-m", strtotime($invoice->month))] = 1;
        }

        $this->template->write('page_title','License Invoicing', true);
        $this->renderAdmin('license_invoicing', $content);
    }

    public function license_invoicing_generate()
    {
        $business_id = $this->input->post("business_id");
        $content['invMonth'] = $invMonth = $_POST['invMonth'];

        //check to see if this invoice has already been generated.
        $getInv = "select licenseInvoiceID from licenseInvoice where business_id = $business_id and month = '".$invMonth."-01'";
        $q = $this->db->query($getInv);
        //Check to see if anything went wrong with the query.
        if ($this->db->_error_message()) {
              throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $invoice = $q->result();
        $invId = $invoice[0]->licenseInvoiceID;

        $license_princing_location_types = get_business_meta($business_id, 'license_princing_location_types', '-1');
        $license_princing_location_types = explode(",", $license_princing_location_types);
        $license_princing_location_types_array = $license_princing_location_types;
        if (in_array('-1', $license_princing_location_types)) {
            $license_princing_location_types = "";
        } else {
            $license_princing_location_types = " AND locationTypeID IN " . "('".implode("','", $license_princing_location_types)."')";
        }

        //if the invoice has not already been generated, create all the data an store it in the tables.
        if (!$invId) {
            //first create an invoice
            $addInvoice = "insert into licenseInvoice (business_id, month) values ('$business_id', '$invMonth-01')";
            $q = $this->db->query($addInvoice);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $invId = $this->db->insert_id();

            //add a list of all lockers that are billable
            $addLockers = "insert into licenseInvoiceDetail (licenseInvoice_id, detailType, detailValue)
                            select ".$invId.", 'locker_id', lockerID
                            from locker
                            join location on location_id = locationID
                            join locationType on locationTypeID = locationType_id
                            where business_id = $business_id
                            and lockerType = 'lockers'
                            and lockerStatus_id = 1
                            and lockerName != 'inprocess'
                            and lockerStyle_id != 9
                            $license_princing_location_types
                            order by location.address, lockerName";

            $q = $this->db->query($addLockers);
            //find out how many rows were inserted.
            $numLock = "SELECT ROW_COUNT() as count;";
            $q = $this->db->query($numLock);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $lockerCount = $q->result();

            //get locker price
            $getLockerPrice = "select * from licensePricing
                        join business on business_id = businessID
                        join licenseType on licenseTypeID = licenseType_id
                        where business_id = $business_id
                        and maxQty > ".$lockerCount[0]->count."
                        and licenseType_id = 1
                        order by maxQty
                        limit 1";

            $q = $this->db->query($getLockerPrice);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $lockerPrice = $q->result();

            //update licenseInvoiceDetail table with pricing
            $updateLockPrice = "update licenseInvoiceDetail
                                set price = '".$lockerPrice[0]->price."'
                                where licenseInvoice_id = '".$invId."'
                                and detailType = 'locker_id'";
            $q = $this->db->query($updateLockPrice);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }

            //add a list of all orders not in lockers
            $addTrans = "insert into licenseInvoiceDetail (licenseInvoice_id, detailType, detailValue)
                          select distinct ".$invId.", 'order_id', orderID
                          from orders
                          join locker on locker_id = lockerID
                          join location on location_id = locationID
                          join locationType on locationTypeID = locationType_id
                          join customer on customer_id = customerID
                          join orderItem oi on oi.order_id = orders.orderID
                          where orders.business_id = $business_id
                          and lockerType != 'lockers'
                          and lockerStyle_id != 9
                          and bag_id != 0
                          and date_format(convert_tz(orders.dateCreated, 'GMT', '".$this->business->timezone."'), '%Y-%m') = '$invMonth'
                          and isnull(orders.split_from)  
                          $license_princing_location_types
                          order by orders.dateCreated desc";

            $q = $this->db->query($addTrans);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }

            $numTran = "SELECT ROW_COUNT() as count;";
            $q = $this->db->query($numTran);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $tranCount = $q->result();

            //get per transaction pricing
            $getTranPrice = "select * from licensePricing
                        join business on business_id = businessID
                        join licenseType on licenseTypeID = licenseType_id
                        where business_id = $business_id
                        and licenseType_id = 2
                        order by maxQty";
            $q = $this->db->query($getTranPrice);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $tranPrice = $q->result();

            //go through each transaction and calculate the license price for this transaction
            $getTransactions = "select * from licenseInvoiceDetail where licenseInvoice_id = $invId and detailType = 'order_id' ";
            $q = $this->db->query($getTransactions);
            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
                  throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
            $transactions = $q->result();

            foreach ($transactions as $transaction) {
                if ($tranPrice[0]->priceType == 'Dollars') {
                    $updateInvoice = "update licenseInvoiceDetail set price = ".$tranPrice[0]->price." where licenseInvoiceDetailID = ".$transaction->licenseInvoiceDetailID;
                    $q = $this->db->query($updateInvoice);
                } elseif ($tranPrice[0]->priceType == 'Percent') {
                    //get the live (not completed) price of this order
                    $order = new Order($transaction->detailValue, FALSE);
                    // get the location type id for this order
                    $getLocationType = "SELECT locationType_id FROM location JOIN locker ON locker.location_id=location.locationID
                                        WHERE locker.lockerID = ? LIMIT 0,1";
                    $q = $this->db->query($getLocationType, $order->locker_id)->result();
                    if (!empty($q)) {
                        $locationTypeID = $q[0]->locationType_id;
                        if (!in_array('-1', $license_princing_location_types_array)) {
                            if (!in_array($locationTypeID, $license_princing_location_types_array)) {
                                continue;
                            }
                        }
                    }

                    $itemTotal = $order->get_gross_total();
                    $itemDisc = $order->get_discount_total();
                    $orderTotal = $itemTotal + $itemDisc;
                    //calculate the commission
                    $commission = ($tranPrice[0]->price/100) * $orderTotal;
                    //update record
                    $updateInvoice = "update licenseInvoiceDetail set price = ".$commission." where licenseInvoiceDetailID = ".$transaction->licenseInvoiceDetailID;
                    $q = $this->db->query($updateInvoice);
                }
            }


            //Calculate revenues
            $this->addInvoiceRevenues($invId, $invMonth, $business_id);

            //find out how much this invoice was for an make sure it meets the minimum
            $minimum = get_business_meta($business_id, 'licenseMinimum');
            $getInvoiceAmount = "SELECT sum(price) as total FROM licenseInvoiceDetail WHERE licenseInvoice_id = $invId";
            $q = $this->db->query($getInvoiceAmount);
            $invTotal = $q->result();
            if ($invTotal[0]->total < $minimum) {
                $difference = $minimum - $invTotal[0]->total;
                $addMin = "insert into licenseInvoiceDetail (licenseInvoice_id, detailType, price)
                            value ('$invId', 'minimum', '$difference') ";
                $q = $this->db->query($addMin);
            }
        }

        $extra = '';
        if ($this->input->post('quickbooks')) {
            $extra = '&export=quickbooks';
        }

        redirect("/admin/superadmin/license_invoicing_view/?invId=".$invId . $extra);
    }

    public function license_invoicing_capture()
    {
        $process = $this->input->post("process");

        if (!$process) {
            set_flash_message("error", "'process' must be passed as a POST parameter");
            $this->goBack("/admin/superadmin/license_invoicing");
        }

        $this->load->model("business_model");

        $process["amount"] = str_replace(",", "", $process["amount"]);
        $response = $this->business_model->capture($process["business_id"], $process["amount"]);

        $business = new Business($process["business_id"]);

        if ($response['status'] == 'success') {
            $res[] = array(
                    'status' => "SUCCESS",
                    'message' => "Invoice Payment",
                    'amount' => $response['amount'],
                    'business' => $business
                );
        } else {
            $res[] = array(
                    'status' => "FAIL",
                    'message' => $response['message'],
                    'business' => $business
                );
        }

        // This will display the list of orders that were processed and their statuses
        $content['payments'] = $res;

        $this->template->write('page_title','License Invoicing Payment', true);
        $this->renderAdmin('license_invoicing_capture', $content);
    }

    //get revenues if apply
    protected function addInvoiceRevenues($invId, $invMonth, $business_id)
    {
        $business = new Business($business_id);

        $licenseType_id = 3;
        $existsLicenseRevenueType= "select * from licensePricing where licenseType_id = '".$licenseType_id."' and business_id = '".$business_id."' limit 1";

        $q = $this->db->query($existsLicenseRevenueType);
        $q->result();
        if ($q->num_rows() == 0) {
            return;
        }

        $license_princing_location_types = get_business_meta($business_id, 'license_princing_location_types', '-1');
        $license_princing_location_types = explode(",", $license_princing_location_types);

        if (in_array('-1', $license_princing_location_types)) {
            $license_princing_location_types = "";
        } else {
            $license_princing_location_types = " AND location.locationType_id IN " . "('".implode("','", $license_princing_location_types)."')";
        }

        //get the revenue for selected business
        $getRevenue = "SELECT 
        SUM(orderItem.qty * orderItem.unitPrice) as revenue
        FROM
        orders
        JOIN
        orderItem ON orderItem.order_id = orderID
        JOIN locker ON orders.locker_id=locker.lockerID
        JOIN location ON locker.location_id=location.locationID
        WHERE
        date_format(CONVERT_TZ(orders.dateCreated,
        'GMT',
        '".$business->timezone."'), '%Y-%m') = ?
        AND orders.business_id = ".$business_id."
        AND bag_id <> 0
        $license_princing_location_types";

        $q = $this->db->query($getRevenue, array($invMonth));

        $revenue = $q->result();            
        $revenue = empty($revenue[0]->revenue)?0:$revenue[0]->revenue;

        //get discounts 
        $getDiscounts = "SELECT sum(chargeAmount) AS discount
        FROM orderCharge
        JOIN orders ON orderID = order_id
        JOIN locker ON orders.locker_id=locker.lockerID
        JOIN location ON locker.location_id=location.locationID
        WHERE chargeType <> 'basic authorization'
        AND orders.business_id = ".$business_id." 
        AND DATE_FORMAT(CONVERT_TZ(orders.dateCreated, 'GMT', '".$business->timezone."'), '%Y-%m') = ?
        $license_princing_location_types";

        $q = $this->db->query($getDiscounts, array($invMonth));
        $discount = $q->result();

        $discount = empty($discount[0]->discount)?0:$discount[0]->discount;


        $totalRevenue = $revenue + $discount;

        //get revenues if apply
        $getRevenueToApply = "select * from licensePricing
        join business on business_id = businessID
        join licenseType on licenseTypeID = licenseType_id
        where business_id = ".$business_id."
        and maxQty > ".str_replace(",", ".", $totalRevenue)."
        and licenseType_id = '".$licenseType_id."'
        order by maxQty
        limit 1";

        $q = $this->db->query($getRevenueToApply);
        $revenueToApply = $q->result();

        if ($q->num_rows() > 0) {
            $price = empty($revenueToApply[0]->price)?0:$revenueToApply[0]->price;                
            $price = str_replace(",", ".", $price);
            if ($revenueToApply[0]->priceType == "Percent")
            {
                $price = $totalRevenue * ($price/100);
            } // will be a fixed price for "Dollar"
                
            //insert licenseInvoiceDetail table with pricing
            $insertRevenue = "INSERT INTO licenseInvoiceDetail (licenseInvoice_id, detailType, price) VALUES (".$invId.", 'revenue', '".$price."');";
            $q = $this->db->query($insertRevenue);

            //Check to see if anything went wrong with the query.
            if ($this->db->_error_message()) {
              throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
            }
        }
    }

    public function license_invoicing_view()
    {
        $invId = $this->input->get_post("invId");
        $content['details'] = $this->input->get_post("details");

        $this->load->library('license_invoicing');
        $content = $this->license_invoicing->showInvoice($invId);

        if ($this->input->get('export') == 'quickbooks') {
            $this->quickbooks = new DropLocker\Export\QuickBooks();

            $invoice = $content['invoice'][0];

            $invoices = array();
            $invoices[] = array(
                'total' => $content['totalDue'],
                'date' => $invoice->month,
                'customer' => $invoice->companyName,
                'number' => $invoice->licenseInvoiceID,
                'title' => sprintf('License Invoice for %s for %s', $invoice->companyName, date("M Y", strtotime($invoice->month))),
                'items' => array(
                    array(
                        'description' => 'Locker License Fee',
                        'amount' => $content['lockerTotal'],
                        'price' => $content['lockerTotal'],
                        'quantity' => 1,
                    ),
                    array(
                        'description' => 'Transaction License Fee',
                        'amount' => $content['tranTotal'],
                        'price' => $content['tranTotal'],
                        'quantity' => 1,
                    ),
                    array(
                        'description' => 'Revenue License Fee',
                        'amount' => $content['revenuesTotal'],
                        'price' => $content['revenuesTotal'],
                        'quantity' => 1,
                    )
                ),
            );

            if (!empty($content['minimum'])) {
                $invoices[0]['items'][] = array(
                    'description' => 'Monthly Minimum',
                    'amount' => $content['minimum'][0]->price,
                    'price' => $content['minimum'][0]->price,
                    'quantity' => 1,
                );
            }

            $iif = $this->quickbooks->getIIF($invoices);
            $filename = "invoice-{$invoice->month}-{$invoice->companyName}.iif";

            header("Content-Type: text/plain;");
            header("Content-Disposition: attachment; filename='$filename'");
            die($iif);
        }


        $this->template->write('page_title','View License Invoice', true);
        $this->data['content'] = $this->load->view('admin/reports/license_invoice_view', $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function license_invoicing_delete()
    {
        $invoice_id = $this->input->get_post("invId");
        $content['details'] = $this->input->get_post("details");

        $delete1 = "delete from licenseInvoice where licenseInvoiceID = $invoice_id";
        $q = $this->db->query($delete1);
        //Check to see if anything went wrong with the query.
        if ($this->db->_error_message()) {
              throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }
        $delete2 = "delete from licenseInvoiceDetail where licenseInvoice_id = $invoice_id";
        $q = $this->db->query($delete2);
        //Check to see if anything went wrong with the query.
        if ($this->db->_error_message()) {
              throw new Exception($this->db->_error_message(), $this->db->last_query(), $this->db->_error_number());
        }

        set_flash_message('success', "Invoice Deleted");
        redirect("/admin/superadmin/license_invoicing");
    }

    public function license_invoicing_delete_item($item_id = null)
    {
        $invoice_id = $this->input->get_post("invId");

        $delete1 = "delete from licenseInvoiceDetail where licenseInvoiceDetailID = $item_id";
        $q = $this->db->query($delete1);

        set_flash_message('success', "Item Deleted");
        redirect("/admin/reports/dl_invoices_view?details=1&invId=".$invoice_id );
    }


    public function license_pricing($business_id = null)
    {
        $getPrice = "select * from licensePricing
                    join business on business_id = businessID
                    join licenseType on licenseTypeID = licenseType_id
                    where business_id = $business_id
                    order by name, maxQty";
        $q = $this->db->query($getPrice);

        $content['pricing'] = $pricing = $q->result();
        $content['business'] = $this->business_model->get(array("businessID" => $business_id));
        $content['minimum'] = get_business_meta($business_id, 'licenseMinimum');
        $content['notes'] = get_business_meta($business_id, 'licenseNotes');

        // license types
        $getTypes = "select * from licenseType order by name";
        $q = $this->db->query($getTypes);
        $content['types'] = $q->result();

        // location types
        $getLocationTypes = "select locationTypeID, name from locationType order by name";
        $q = $this->db->query($getLocationTypes);
        $content['locationsTypes'] = $q->result();

        // selected location types
        $license_princing_location_types = get_business_meta($business_id, 'license_princing_location_types', '-1');
        $license_princing_location_types = explode(',', $license_princing_location_types);
        $content['license_princing_location_types'] = $license_princing_location_types;
        
        $this->template->write('page_title','License Pricing', true);
        $this->renderAdmin('license_pricing', $content);
    }

    public function license_pricing_add()
    {
            $insert1 = "insert into licensePricing (business_id, licenseType_id, maxQty, price, priceType)
                        values (?, ?, ?, ?, ?)";
            $q = $this->db->query(
                $insert1, 
                array(
                    $this->input->post('business_id'), 
                    $this->input->post('licenseType_id'), 
                    $this->input->post('maxQty'), 
                    $this->input->post('price'), 
                    $this->input->post("priceType")
                )
            );

            set_flash_message("success", 'Pricing Added');
            redirect("/admin/superadmin/license_pricing/".$_POST['business_id']);
    }

    public function license_pricing_add_minimum()
    {
            $business_id = $this->input->post('business_id');

            update_business_meta($this->input->post('business_id'), 'licenseMinimum', $this->input->post('minimum'));
            update_business_meta($this->input->post('business_id'), 'licenseNotes', $this->input->post('notes'));
            
            $license_princing_location_types = empty($this->input->post('locationType_id'))?array('-1'):$this->input->post('locationType_id');
            //print_r($license_princing_location_types );echo "<br>";
            // if size greater than 1 then we need to remove '-1' value
            if (!empty($license_princing_location_types)) {
                if (count($license_princing_location_types) >1 ) {
                    if (($key = array_search('-1', $license_princing_location_types)) !== false) {
                        unset($license_princing_location_types[$key]);
                    }
                }
            }
            //print_r($license_princing_location_types );echo "<br>";
            $license_princing_location_types = implode(',', $license_princing_location_types);
            update_business_meta($business_id, 'license_princing_location_types', $license_princing_location_types);
            
            $content['license_princing_location_types'] = get_business_meta($business_id, 'license_princing_location_types', '-1');
            $content['license_princing_location_types'] = explode(',', $content['license_princing_location_types']);

            set_flash_message("success", 'Location Pricing Updated');
            redirect("/admin/superadmin/license_pricing/".$business_id);
    }

    public function license_pricing_delete($licensePricingID = null, $business_id  = null)
    {
        $delete1 = "delete from licensePricing where licensePricingID = $licensePricingID";
        $q = $this->db->query($delete1);

        redirect("/admin/superadmin/license_pricing/".$business_id);
    }
    /**
     *The following action renders the interface for managing the server mode.
     */
    public function manage_server_mode()
    {
        $content['modes'] = Server_Meta::get_valid_modes_as_codeigniter_dropdown();
        $content['mode'] = Server_Meta::get("mode");
        $this->template->write("page_title", "Manage Server Mode");
        $this->data['content'] = $this->load->view("admin/superadmin/manage_server_mode", $content, true);
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }
    /**
     * The following action updates the server mode.
     */
    public function update_server_mode()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("mode", "mode", "required");

        if ($this->form_validation->run()) {
            $mode = $this->input->post("mode");
            set_flash_message("success", "Updated server mode");
            Server_Meta::set("mode", $mode);
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_server_mode");
    }
    /**
     * The following function renders the interface for managing the email server settings
     */
    public function view_email_server_settings()
    {
        $content = null;

        $content['email_host'] = Server_Meta::get("email_host");
        $content['email_user'] = Server_Meta::get("email_user");
        $this->template->write("page_title", "Email Server Settings", true);
        $this->data['content'] = $this->load->view("admin/superadmin/view_email_server_settings", $content, true);
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }
    /**
     *The following action updates the email server settings for the application server.
     */
    public function update_email_server_settings()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("email_host", "Email Host", "required");
        $this->form_validation->set_rules("email_user", "Email User", "required");
        $this->form_validation->set_rules("email_password", "Email Password", "required");


        if ($this->form_validation->run()) {
            $email_password = $this->input->post("email_password");
            $email_user = $this->input->post("email_user");
            $email_host = $this->input->post("email_host");

            Server_Meta::set("email_password", $email_password);
            Server_Meta::set("email_user", $email_user);
            Server_Meta::set("email_host", $email_host);

            set_flash_message("success", "Update email server settings");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/view_email_server_settings");
    }

    /**
     * Renders the itnerface for managing coupons in the superadmin view
     */
    public function manage_coupons()
    {
        $this->load->model('coupon_model');
        $content['active_coupons'] = $this->coupon_model->get_active_coupons($this->business_id);
        $content['inactive_coupons'] = $this->coupon_model->get_inactive_coupons($this->business_id);

        $content['bizzie_superadmin'] = true;
        $content['initialSearch'] = $this->input->get('search');
        $content['business'] = $this->business;

        //get the products for the dropdown with amountType = item
        $options['order_by'] = "displayName";
        $options['group_by'] = "displayName";
        $options['business_id'] = $this->business_id;
        $options['select'] = 'product.displayName, productID';
        $this->load->model("product_model");
        $products = $this->product_model->get($options);
        $content['products'] = $products;

        $content['frequencies'] = Coupon::get_frequencies();
        $content['amountTypes'] = Coupon::get_amountTypes();
        $content['first_time_fields'] = Coupon::get_first_time_fields();
        $content['recurring_types'] = Coupon::get_recurring_types();

        $this->setPageTitle('page_title', 'Manage Coupons');
        $this->renderAdmin("/admin/coupon/manage", $content);
    }

    /**
     * The following action is *; do not use
     */
    public function shipment_overview()
    {
        $getShipments = "select * from shipment
                        where business_id = $this->business_id ";
              !$this->input->post('completed')? $getShipments .= "and status != 'completed'" :"";
                        $getShipments .= " order by placedDate";
        $query = $this->db->query($getShipments);
        $content['shipments'] = $shipments = $query->result();

        foreach ($shipments as $shipment) {
            $shipment->placedDate != '0000-00-00' ? $content['placedDate'][$shipment->shipmentID] = $placed = date(SHORT_DATE_FORMAT,strtotime($shipment->placedDate)):'';
            $shipment->estShipDate != '0000-00-00' ? $content['estShipDate'][$shipment->shipmentID] = $estShip = date(SHORT_DATE_FORMAT,strtotime($shipment->estShipDate)):'';
            $shipment->shipDate != '0000-00-00' ? $content['shipDate'][$shipment->shipmentID] = $ship = date(SHORT_DATE_FORMAT,strtotime($shipment->shipDate)):'';
            $shipment->estDelDate != '0000-00-00' ? $content['estDelDate'][$shipment->shipmentID] = $estDel = date(SHORT_DATE_FORMAT,strtotime($shipment->estDelDate)):'';

            $content['weekEvents'][$shipment->shipmentID][date("W", strtotime($shipment->placedDate))] .= '<table><tr><td>Placed: '.$placed.'</td></tr></table>';
            $content['weekEvents'][$shipment->shipmentID][date("W", strtotime($shipment->estShipDate))] .= '<table><tr><td>Est Ship: '.$estShip.'</td></tr></table>';
            $content['weekEvents'][$shipment->shipmentID][date("W", strtotime($shipment->shipDate))] .= '<table><tr><td>Shipped: '.$ship.'</td></tr></table>';
            $content['weekEvents'][$shipment->shipmentID][date("W", strtotime($shipment->estDelDate))] .= '<table><tr><td>Est Del: '.$estDel.'</td></tr></table>';
        }
        $content['shipmentNotes'] = get_business_meta($this->business_id, 'shipmentNotes');

        $this->template->write("page_title", "Shipment Overview", true);
        $this->data['content'] = $this->load->view("admin/superadmin/shipment_overview", $content, true);
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }
    /**
     * The following action is *; do not use.
     */
    public function shipment_edit()
    {
        $getShipment = "select * from shipment
                        where shipmentID = '".$this->input->post('edit')."'";
        $query = $this->db->query($getShipment);
        $content['shipment'] = $query->row();

        $this->template->write("page_title", "Shipment Edit", true);
        $this->data['content'] = $this->load->view("admin/superadmin/shipment_edit", $content, true);
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }

    /**
     * The following action is *; do not use.
     */
    public function shipment_update()
    {
        $notes = mysql_real_escape_string($this->input->post('notes'));
        $what = mysql_real_escape_string($this->input->post('what'));
        $this->input->post('placedDate') >= '2010-01-01' ? $placedDate = date('Y-m-d',strtotime($this->input->post('placedDate'))) : '';
        $this->input->post('estShipDate') >= '2010-01-01' ? $estShipDate = date('Y-m-d',strtotime($this->input->post('estShipDate'))) : '';
        $this->input->post('shipDate') >= '2010-01-01' ? $shipDate = date('Y-m-d',strtotime($this->input->post('shipDate'))) : '';
        $this->input->post('estDelDate') >= '2010-01-01' ? $estDelDate = date('Y-m-d',strtotime($this->input->post('estDelDate'))) : '';

        if ($this->input->post('new') == 1) {
            $addShipment = "insert into shipment (status, what, customer, po, invoice, placedDate, estShipDate, shipDate, estDelDate, notes, business_id)
                            values ('".$this->input->post('status')."', '".$what."','".$this->input->post('customer')."','".$this->input->post('po')."','".$this->input->post('invoice')."','".$placedDate."',
                                        '".$estShipDate."','".$shipDate."','".$estDelDate."','".$notes."','".$this->business_id."')";
            $this->db->query($addShipment);
            set_flash_message("success", "Shipment Addes");
        } else {
            $updateShipment = "update shipment set
                            status = '".$this->input->post('status')."',
                            what = '".$what."',
                            customer = '".$this->input->post('customer')."',
                            po = '".$this->input->post('po')."',
                            invoice = '".$this->input->post('invoice')."',
                            placedDate = '".$placedDate."',
                            estShipDate = '".$estShipDate."',
                            shipDate = '".$shipDate."',
                            estDelDate = '".$estDelDate."',
                            notes ='".$notes."'
                            where shipmentID = '".$this->input->post('edit')."'";
            $this->db->query($updateShipment);

            set_flash_message("success", "Shipment Updated");
        }

        redirect("/admin/superadmin/shipment_overview");
    }

    /**
     * The following action is Mattt Dunlap excrement, do not use.
     */
    public function shipment_notes()
    {
        update_business_meta($this->business_id, 'shipmentNotes', $this->input->post('shipmentNotes'));
        set_flash_message("success", "Notes Updated");
        redirect("/admin/superadmin/shipment_overview");
    }


    /**
     * The following action renders the interface for adding a language key to a language view or deleting a langugae key from a language view.
     */
    public function manage_languageKeys_and_languageViews()
    {
        $this->load->model("languageView_model");

        $get_all_languageViews_query_result = $this->languageView_model->get_all();

        $this->load->model("languageKey_model");
        $languageViews = array();
        foreach ($get_all_languageViews_query_result as $languageView) {
            $get_languageKeys_for_languageView_result = $this->languageKey_model->get_all_for_languageView($languageView->languageViewID);
            $languageKeys = array();
            foreach ($get_languageKeys_for_languageView_result as $languageKey) {
                $languageKeys[$languageKey->languageKeyID] = array("name" => $languageKey->name, "defaultText" => $languageKey->defaultText);
            }
            $languageViews[$languageView->languageViewID] = array("name" => $languageView->name, "languageKeys" => $languageKeys);
        }
        //The following statement populates any POST fields stored in the flash data, which is expected to have been sent from another action taht delt with performing an operation with some entity based on some request POST parameters.
        $_POST = (array)unserialize($this->session->flashdata("post"));

        $content['languageViews_and_languageKeys'] = $languageViews;

        $content['languageViews'] = $this->languageView_model->get_all();
        $content['languageView_dropdown'] = $this->languageView_model->get_all_as_codeigniter_dropdown();
        $this->renderAdmin("manage_languageKeys_and_languageViews", $content);
    }

    public function manage_languages()
    {
        $this->load->model("language_model");
        $content['languages'] = $this->language_model->get_all();
        $this->data['content'] = $this->load->view("/admin/superadmin/manage_languages", $content, true);
        $this->template->write("page_title", "Manage Languages");
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }

    public function product_import()
    {
        $process = "select * from process";
        $query = $this->db->query($process);
        $content['processes'] = array();
        if ($query->result()) {
            foreach($query->result() as $process) {
                $content['processes'][$process->processID] = $process->name;
            }
        }

        $this->data['content'] = $this->load->view("/admin/superadmin/product_import", $content, true);
        $this->template->write("page_title", "Product Import");
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }

    public function product_import_run()
    {
        $processes = $this->input->post('process');
        if (empty($processes)) {
            set_flash_message("error", "Please, select at least one process before import.");
            redirect("/admin/superadmin/product_import");
        }
               
        if (empty($_FILES['importFile'])) {
            set_flash_message("error", "No file was uploaded.  Please try again.");
            redirect("/admin/superadmin/product_import");
        } else {
            //go through all data an load into an array
            $header = NULL;
            $data = array();

            $handle = file_get_contents($_FILES['importFile']['tmp_name']);
            $handle = str_replace(array("\r\n", "\r"), "\n", $handle);

            $datax = explode("\n", $handle);
            foreach ($datax as $row) {
                if (!trim($row)) {
                    continue;
                }

                if(!$header) {
                    $header = str_getcsv($row, ",");
                } else {
                    $data[] = array_combine($header, str_getcsv($row, ",", '"'));
                }
            }

            //first create product categories
            foreach ($data as $value) {
                $category[trim($value['Category'])] = 1;
            }

            foreach ($category as $cat => $value) {
                $queryParams = array(
                        $cat, 
                        convert_to_slug($cat), 
                        $this->business_id,
                        3
                    );

                // check if exists
                $sql = "SELECT productCategoryID FROM productCategory WHERE 
                            name = ? AND
                            slug = ? AND
                            business_id = ? AND
                            module_id = ?";
                $query = $this->db->query($sql, $queryParams);
                $result = $query->result();

                if (empty($result)) {
                    $addCategory = "INSERT INTO productCategory (name, slug, business_id, module_id) VALUES (?, ?, ?, ?)";
                    $query = $this->db->query($addCategory, $queryParams);
                    $lookupCat[$cat] = $this->db->insert_id();
                } else {
                    $lookupCat[$cat] = $result[0]->productCategoryID;
                }
            }

            //now add the products
            foreach ($data as $value) {
                $cleanCat = trim($value['Category']);

                $queryParams = array(
                    $value['Product'], 
                    $value['Product'], 
                    'each',
                    3,
                    $lookupCat[$cleanCat],
                    $this->business_id,
                    'product'
                );

                // check if exists
                $sql = "SELECT productID FROM product WHERE 
                            name = ? AND 
                            displayName = ? AND
                            unitOfMeasurement = ? AND
                            module_id = ? AND
                            productCategory_id = ? AND
                            business_id = ? AND 
                            productType = ?";
                $query = $this->db->query($sql, $queryParams);
                $result = $query->result();

                if (empty($result)) {
                    $addProduct = "INSERT INTO 
                                    product (name, displayName, unitOfMeasurement, module_id, productCategory_id, business_id, productType)
                                   VALUES
                                    (?, ?, ?, ?, ?, ?, ?)";
                    $query = $this->db->query($addProduct, $queryParams);

                    //strip spaces out of array keys
                    $keys = str_replace( ' ', '', array_keys($value) );
                    $values = array_values($value);
                    $value = array_combine($keys, $values);

                    //finally create the product processes
                    $product_id = $this->db->insert_id();
                } else {
                    $product_id = $result[0]->productID;
                }
                
                foreach ($processes as $process_id => $name) {
                    $key = (int)$key;

                    // check if exists
                    $sql = "SELECT product_processID FROM product_process WHERE 
                                product_id = ? AND 
                                process_id = ?";
                    
                    $query = $this->db->query(
                        $sql, 
                        array($product_id, $process_id)
                    );

                    $result = $query->result();     
                    if (empty($result)) {
                        // make the insert
                        $sql = "INSERT INTO product_process( product_id, process_id, price ) VALUES (?, ?, ?)";
                        $query = $this->db->query(
                            $sql, 
                            array($product_id, $process_id, $value['Price'])
                        );
                    }
                }
            }

            set_flash_message("success", "All products loaded successfully");
            redirect("/admin/modules/dry_clean/products/");
        }
    }

    function locations()
    {
        $content = array();

        $this->load->model('location_model');
        //$content['locations'] = $this->_setInfoBoxImages($this->location_model->get_all_with_business());

        $local_url = 'http://' . $_SERVER['HTTP_HOST'] . '/admin/api/';
        if (in_bizzie_mode()) {
            $remote_url = 'https://droplocker.com/admin/api/';
        } else {
            $remote_url = 'https://admin.bizzie.com/admin/api/';
        }

        $locations = json_decode(file_get_contents($local_url . 'get_all_locations'));
        $remote_loc = json_decode(file_get_contents($remote_url . 'get_all_locations'));
        if ($remote_loc) {
            $locations = array_merge($locations, $remote_loc);
        }
        $content['locations'] = $this->_formatLocations($locations);

        $businessArray = json_decode(file_get_contents($local_url . 'get_business_list'), true);
        $remote_business = json_decode(file_get_contents($remote_url . 'get_business_list'), true);
        if ($remote_business) {
            $businessArray = array_merge($businessArray, $remote_business);
        }
        $content['businessArray'] = $businessArray;

        // Load GMaps js
        $this->template->add_js('https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=' . get_default_gmaps_api_key(),'source');
        $this->template->add_js("/js/keydragzoom_packed.js");

        $this->data['content'] = $this->load->view("/admin/superadmin/locations", $content, true);
        $this->template->write("page_title", "All Locations");
        $this->template->write_view("content", "inc/dual_column_left", $this->data);
        $this->template->render();
    }

    protected function _formatLocations($locations)
    {

        $out = array();
        foreach ($locations as $key => $location) {

            $info = '<table border="0" style="width:300px">';
            if (!empty($location->image)) {
                $info .= '<td rowspan="4" style="padding:5px;"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src="'.$location->image->url.'" width='.$location->image->width.' height='.$location->image->height.'></td>';
            }
            $info .= ($location->business)?'<tr><td valign=top><span class=bodyBold><b>'.$location->business.'</b></span></td></tr>':"";
            $info .= ($location->companyName)?'<tr><td valign=top><span class=bodyBold><u>'.$location->companyName.'</u></span></td></tr>':"";
            $info .= ($location->address)?'<tr><td><span class=bodyBold>'.$location->address.'</span><span class=bodyReg><br>'.$location->location.'</span></td></tr>':"";
            $info .= ($location->hours)?'<tr><td><span class=bodyBold>Hours: </span><span class=bodyReg>'.$location->hours.'</apan></td></tr>':"";
            $info .= '</table>';

            $location->info = $info;
            $out[$location->locationID] = $location;
        }

        return $out;
    }


}
