<?php

class Invoices extends MY_Admin_Controller
{
    protected function getInvoiceData($invNumber, $bagDetail = true)
    {
        $breakout_vat = null;

        $content['newInvoice']['number'] = $invNumber;
        $getInvNumber = "SELECT * FROM invoice JOIN orders ON orderID = invoice.order_id WHERE invoiceNumber = ? and invoice.business_id = ? and orders.business_id = ?";
        $query = $this->db->query($getInvNumber, array($invNumber, $this->business_id, $this->business_id));
        $invoices = $query->result();

        $content['invDate'] = $invDate = $invoices[0]->dateInvoiced;
        $custID = $invoices[0]->customer_id;

        foreach ($invoices as $invoice) {
            $orders[] = $invoice->order_id;
        }

        $this->load->model('invoicing_model');
        $this->invoicing_model->customer_id = $custID;
        $content['customer'] = $customer = $this->invoicing_model->get();
        $business_language_id = get_customer_business_language_id($custID);
        if (empty($business_language_id)) {
            $business_language_id = $this->business->default_business_language_id;
        }

        $this->load->model('invoicecustomfield_model');

        $customFields =  $this->invoicecustomfield_model->getInvoiceCustomFields($this->business->businessID, $business_language_id);

        $content['customFields'] = $customFields;
        $this->load->model('orderitem_model');
        $this->load->model('ordercharge_model');
        $this->load->model('order_model');

        $content['newInvoice']['subTotal'] = 0;
        $content['newInvoice']['taxTotal'] = 0;
        $content['newInvoice']['amtPaid'] = 0;
        $content['newInvoice']['taxDetail'] = array();

        foreach ($orders as $order_id) {
            $order = $this->order_model->get_by_primary_key($order_id);
            $ordTotal = $this->order_model->get_net_total($order_id);
            $subTotal = $this->order_model->get_gross_total($order_id) + $this->order_model->get_orderCharge_total($order_id);

            if (!empty($invoices)) {
                foreach($invoices as $row) {
                    if ($row->order_id==$order_id){
                        $ordTotal = $row->amount;
                        $subTotal = $ordTotal;
                    }
                }
            }

            $pay_with_credit_card = !empty($content['customer'][0]->terms)?($content['customer'][0]->terms=="Credit Card"?true:false):false;
            if ($ordTotal <> 0 || $pay_with_credit_card) {

                $content['newInvoice']['subTotal'] += $subTotal;
                $content['newInvoice']['detail'][$order_id]['dateCreated'] = $order->dateCreated;
                $content['newInvoice']['detail'][$order_id]['ordPayment'] = $order->orderPaymentStatusOption_id;
                $content['newInvoice']['detail'][$order_id]['ordNotes'] = $order->notes;
                $content['newInvoice']['detail'][$order_id]['ordTotal'] = $subTotal;
                $content['newInvoice']['detail'][$order_id]['tax'] = 0;

                $taxItems = $this->order_model->get_stored_taxes($order_id);
                foreach ($taxItems as $taxItem) {

                    if (is_null($breakout_vat)) {
                        $breakout_vat = $taxItem['vat_breakout'];
                    } else if ($breakout_vat != $taxItem['vat_breakout']) {
                        $content['newInvoice']['detail'][$order_id]['error'] = 'VAT mismatch for this order, check this invoice';
                    }

                    $content['newInvoice']['detail'][$order_id]['tax'] += $taxItem['amount'];

                    if (!isset($content['newInvoice']['taxDetail'][$taxItem['taxGroup_id']])) {
                        $content['newInvoice']['taxDetail'][$taxItem['taxGroup_id']] = 0;
                    }
                    $content['newInvoice']['taxDetail'][$taxItem['taxGroup_id']] += $taxItem['amount'];
                }

                $content['newInvoice']['taxTotal'] += $content['newInvoice']['detail'][$order_id]['tax'];

                if ($order->orderPaymentStatusOption_id == 3) {
                    $content['newInvoice']['amtPaid'] += $ordTotal;
                }

                //if we need to show details for the order
                if ($bagDetail && $content['customer'][0]->bagDetail != 0) {
                    $this->orderitem_model->clear();
                    $this->orderitem_model->order_id = $order_id;
                    $this->db->select('orderItem.notes, qty, unitPrice, product.name, barcode, itemID');
                    $this->orderitem_model->join("product_process ON product_processID = product_process_id");
                    $this->orderitem_model->join("product ON product_id = productID");
                    $this->orderitem_model->leftjoin("item ON item_id = itemID");
                    $this->orderitem_model->leftjoin("barcode ON item.itemID = barcode.item_id");
                    $content['items'][$order_id] = $this->orderitem_model->get();

                    $this->ordercharge_model->clear();
                    $this->ordercharge_model->order_id = $order_id;
                    $content['charges'][$order_id] = $this->ordercharge_model->get();

                    //for customers that need a breakdown by employee
                    if ($content['customer'][0]->bagDetail >= 1) {
                        $bagNumber = ' ';
                        $notes = '';

                        if ($order->bag_id) {
                            $getBagInfo = "select * from bag where bagID = ".$order->bag_id;
                            $query = $this->db->query($getBagInfo);
                            $bagInfo = $query->result();
                            if ($bagInfo) {
                                $bagNumber = $bagInfo[0]->bagNumber;
                                $notes = $bagInfo[0]->notes;
                            }
                        }

                        if (!isset($content['byBag'][$bagNumber]['amt'])) {
                            $content['byBag'][$bagNumber]['amt'] = 0;
                        }

                        $content['byBag'][$bagNumber]['amt'] += $ordTotal;
                        $content['byBag'][$bagNumber]['name'] = $notes;

                        $content['newInvoice']['detail'][$order_id]['bag'] = $bagNumber;
                        $content['newInvoice']['detail'][$order_id]['notes'] = $notes;
                    }
                }
            }
        }

        $this->load->model('taxgroup_model');
        $content['newInvoice']['taxGroups'] = array();
        foreach ($content['newInvoice']['taxDetail'] as $taxGroup_id => $amount) {
            if ($taxGroup = $this->taxgroup_model->get_by_primary_key($taxGroup_id)) {
                $content['newInvoice']['taxGroups'][$taxGroup_id] = $taxGroup;
            }
        }

        $content['newInvoice']['total'] = $content['newInvoice']['subTotal'] + $content['newInvoice']['taxTotal'];

        $content['date_format'] = get_business_meta($this->business_id, "shortDateDisplayFormat");
        $content['breakout_vat'] = $breakout_vat;

        if ($content['breakout_vat']) {
            $content['newInvoice']['total'] = $content['newInvoice']['subTotal'];
            $content['newInvoice']['subTotal'] -= $content['newInvoice']['taxTotal'];
        }

        return $content;
    }

    /**
     * Expects the following GET parameter
     *  invoiceNumber
     */
    public function download()
    {
        if ($this->input->get("invoiceNumber") == false || !is_numeric($this->input->get("invoiceNumber"))) {
            set_flash_message("error", "Invoice Nubmer must be passed as a GET parameter and must be numeric");
            return $this->goBack("/admin/reports");
        }

        $invNumber = $this->input->get("invoiceNumber");
        $content = $this->getInvoiceData($invNumber);
        $customer = $content['customer'];

        // override employee language
        $this->employee->business_language_id = get_customer_business_language_id($customer[0]->customer_id);

        //when printing a PDF it usese the admin/reports_invoice_ view to determine what it will look like.
        $this->load->library('pdf');

        $font_set = $this->config->item("pdf_font_set");
        $font_set = empty($font_set[$this->business->locale]) ? 'ascii' : $font_set[$this->business->locale];

        $this->pdf->invoice($this->load->view('admin/reports/invoice_pdf',$content, true), $invNumber, $this->business, 'save', $customer[0]->customer_id, $content['customFields'], $font_set);
        die(0);
    }

    /**
     * Expects the following GET parameter
     *  invoiceNumber
     */
    public function export_quickbooks()
    {
        if ($this->input->get("invoiceNumber") == false || !is_numeric($this->input->get("invoiceNumber"))) {
            set_flash_message("error", "Invoice Nubmer must be passed as a GET parameter and must be numeric");
            return $this->goBack("/admin/reports");
        }

        $invNumber = $this->input->get("invoiceNumber");
        $content = $this->getInvoiceData($invNumber, false);
        $customer = $content['customer'];

        $invoices = array(
            array(
                'total' => $content['newInvoice']['total'],
                'date' => $content['invDate'],
                'customer' => $customer[0]->billingName,
                'number' => $invNumber,
                'title' => sprintf('Invoice for %s for %s', $customer[0]->billingName, date($content['date_format'], strtotime($content['invDate']))),
                'items' => array(),
            ),
        );

        $tax_total = 0;
        foreach($content['newInvoice']['detail'] as $orderId => $detail) {
            $invoices[0]['items'][] = array(
                'description' => 'Order ' . $orderId,
                'amount' => $detail['ordTotal'],
                'price' => $detail['ordTotal'],
                'quantity' => 1,
                'taxable' => true,
            );

            $tax_total += $detail['tax'];
        }

        if ($tax_total > 0) {
            $invoices[0]['items'][] = array(
                'description' => 'Order ' . $orderId,
                'amount' => $tax_total,
                'price' => $tax_total,
                'quantity' => 1,
                'taxable' => false,
                'name' => 'TaxAgencyVendor',
                'account' => 'SalesTaxPayable',
                'item' => 'Sales Tax'
            );
        }

        //echo "<pre>"; print_r($invoices); die();
        //echo "<pre>"; print_r($content); die();


        $this->quickbooks = new DropLocker\Export\QuickBooks();
        $iif = $this->quickbooks->getIIF($invoices);
        $filename = "invoice-{$invNumber}-{$this->business->companyName}.iif";

        header("Content-Type: text/plain;");
        header("Content-Disposition: attachment; filename='$filename'");
        die($iif);

    }
}
