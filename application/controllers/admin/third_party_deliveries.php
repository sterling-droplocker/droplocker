<?php

use App\Libraries\DroplockerObjects\OrderExternalDelivery;
class Third_Party_Deliveries extends MY_Admin_Controller
{
    public $submenu = 'orders';

    /**
     * Shows orders and claims that can be sent to 3rd party
     *
     * @param string $service
     * @param int $route
     */
    public function index($service = 'rickshaw', $route = null)
    {
        if ($this->input->post('service')) {
            $service = $this->input->post('service');
            $route = $this->input->post('route');
            redirect("/admin/third_party_deliveries/index/$service/$route");
        }

        $content = array();
        $content['service'] = $service;
        $content['services'] = \DropLocker\Service\Delivery\Factory::listServices();

        // if no route in the url, use the service default route
        if (is_null($route)) {
            $route = get_business_meta($this->business_id, "{$service}_route");
        }
        $content['route'] = $route;

        $this->load->model('location_model');
        $content['routes'] = $this->location_model->get_routes_dropdown($this->business_id);

        // setup locations, and set default location
        $this->load->model('droplocation_model');
        $content['drop_locations'] = $this->droplocation_model->get_list($this->business_id);
        $content['default_location'] = $this->droplocation_model->get_default($this->business_id);

        $content['deliveries'] = $this->getDeliveries($route);
        $content['pickups'] = $this->getPickups($route);

        $this->renderAdmin('index', $content);
    }

    /**
     * Gets orders that are or can be sent to 3rd party
     *
     * @param int $route
     * @return array
     */
    protected function getDeliveries($route)
    {
        $get_deliveries = "SELECT orders.*, customerID, firstName, lastName,
                locationID, location.address, location.accessCode,
                orderExternalDelivery.dateCreated AS deliveryDate,
                orderExternalDelivery.third_party_id,
                orderExternalDelivery.service,
                orderExternalDeliveryID,
                (SELECT count(*) FROM orderExternalDelivery AS o2
                    WHERE o2.third_party_id = orderExternalDelivery.third_party_id
                ) AS grouped
            FROM orders
            JOIN customer ON (customerID = orders.customer_id)
            JOIN locker ON (lockerID = orders.locker_id)
            JOIN location ON (locationID = locker.location_id)
            LEFT JOIN orderExternalDelivery ON (orderID = orderExternalDelivery.order_id)
            WHERE
                orders.business_id = ?
                AND orders.orderStatusOption_id NOT IN (10, 13, 1, 2)
                AND customer_id != ?
                AND location.route_id = ?
            ORDER BY location.address";

        return $this->db->query($get_deliveries, array(
            $this->business_id,
            $this->business->blank_customer_id,
            $route
        ))->result();
    }

    /**
     * Gets claims that are or can be sent to 3rd party
     *
     * @param int $route
     * @return array
     */
    protected function getPickups($route)
    {
        $get_pickups = "SELECT claim.*, customerID, firstName, lastName,
                locationID, location.address, location.accessCode,
                claimExternalPickup.dateCreated AS deliveryDate,
                claimExternalPickup.third_party_id,
                claimExternalPickup.service,
                claimExternalPickup.drop_address
            FROM claim
            JOIN customer ON (customerID = claim.customer_id)
            JOIN locker ON (lockerID = claim.locker_id)
            JOIN location ON (locationID = locker.location_id)
            LEFT JOIN claimExternalPickup ON (claimID = claimExternalPickup.claim_id)
            WHERE
                claim.business_id = ?
                AND claim.active = 1
                AND location.route_id = ?
            ORDER BY location.address";

        return $this->db->query($get_pickups, array(
            $this->business_id,
            $route
        ))->result();
    }

    /**
     * Re-sends a failed delivery
     *
     * @param int $orderExternalDeliveryID
     */
    public function resend_delivery($orderExternalDeliveryID)
    {
        $orderExternalDelivery = new OrderExternalDelivery($orderExternalDeliveryID);
        $third_party_id = $orderExternalDelivery->third_party_id;

        // disable the old requests
        $sql = "UPDATE orderExternalDelivery SET disabled = 1 WHERE third_party_id = ?";
        $this->db->query($sql, array($third_party_id));

        // get the orders
        $sql = "SELECT order_id FROM orderExternalDelivery WHERE third_party_id = ?";
        $deliveries = $this->db->query($sql, array($third_party_id))->result();

        $_POST['service'] = $orderExternalDelivery->service;
        $_POST['deliveries'] = array();
        foreach ($deliveries as $delivery) {
            $_POST['deliveries'][] = $delivery->order_id;
        }

        $this->send();
    }

    /**
     * Allows to configure 3rd parties parameters
     *
     * @param string $service
     */
    public function setup($service = null)
    {
        if ($this->input->post('service')) {
            $service = $this->input->post('service');
            redirect("/admin/third_party_deliveries/setup/$service");
        }

        $content = array();
        $content['services'] = \DropLocker\Service\Delivery\Factory::listServices();
        $content['service'] = $service;
        $this->load->model('location_model');
        $content['routes'] = $this->location_model->get_routes_dropdown($this->business_id);

        if ($this->input->post()) {
            foreach ($this->input->post() as $key => $value) {
                update_business_meta($this->business_id, $key, $value);
            }

            $serviceName = $content['services'][$service];
            set_flash_message('success', "Updated $serviceName settings");
            redirect("/admin/third_party_deliveries/setup/$service");
        }

        $this->renderAdmin('setup', $content);
    }

    /**
     * Sends claims and orders to a 3rd party for delivery/pickup
     */
    public function send()
    {
        $service = $this->input->post('service');
        $services = \DropLocker\Service\Delivery\Factory::listServices();
        $route = $this->input->post('route');

        $deliveryService = \DropLocker\Service\Delivery\Factory::getService($service, $this->business_id);

        $deliveries_status = $this->sendDeliveries($deliveryService);
        $pickup_status = $this->sendPickups($deliveryService);

        $content = compact('pickup_status', 'deliveries_status', 'service', 'services', 'route');
        $this->renderAdmin('send', $content);
    }

    /**
     * Process the deliveries
     *
     * @param \DropLocker\Service\Delivery\IDeliveryService $deliveryService
     * @return array
     */
    protected function sendDeliveries($deliveryService)
    {
        $deliveries = $this->input->post('deliveries');
        if (!$deliveries) {
            return array();
        }

        // sanitize to int
        $order_ids = array_map('intval', $deliveries);

        // group orders by customer_id:location_id
        $groups = $this->groupOrdersByCustomerAndLocation($order_ids);

        $deliveries_status = array();
        foreach ($groups as $key => $order_ids) {
            $id_list = implode(', ', $order_ids);

            if ($id = $deliveryService->delivery($order_ids)) {
                $deliveries_status[$id_list] = array(
                    'status' => 'success',
                    'data' => $id,
                    'message' => "Order $id_list sent as delivery job '$id'"
                );
            } else {
                $deliveries_status[$id_list] = array(
                    'status' => 'error',
                    'data' => $deliveryService->getError(),
                    'message' => $deliveryService->getErrorMessage(),
                );
            }
        }

        return $deliveries_status;
    }

    /**
     * Groups orders that are for the same customer and from the same location
     *
     * @param array $order_ids
     * @return array
     */
    protected function groupOrdersByCustomerAndLocation($order_ids)
    {
        $order_ids = implode(",", $order_ids);

        // group orders by location_id, customer_id
        $orders = $this->db->query("SELECT orderID, customer_id, location_id
            FROM orders
            JOIN locker ON (lockerID = locker_id)
            WHERE orderID IN ($order_ids)
            ORDER BY customer_id, location_id
            ")->result();

        $groups = array();
        foreach ($orders as $order) {
            $key = "{$order->customer_id}:{$order->location_id}";

            if (!isset($groups[$key])) {
                $groups[$key] = array();
            }
            $groups[$key][] = $order->orderID;
        }

        return $groups;
    }

    /**
     * Process the pickups
     *
     * @param \DropLocker\Service\Delivery\IDeliveryService $deliveryService
     * @return array
     */
    protected function sendPickups($deliveryService)
    {
        $pickups = $this->input->post('pickups');
        if (!$pickups) {
            return array();
        }

        $dropLocations = $this->input->post('dropLocation');
        $this->load->model('droplocation_model');

        $pickup_status = array();
        foreach ($pickups as $claim_id) {

            $drop_address = null;
            if (!empty($dropLocations[$claim_id])) {
                $drop_location = $this->droplocation_model->get_by_primary_key($dropLocations[$claim_id]);
                $drop_address = $drop_location->address;
            }

            if ($id = $deliveryService->pickup($claim_id, $drop_address)) {
                $pickup_status[$claim_id] = array(
                    'status' => 'success',
                    'data' => $id,
                    'message' => "Claim $claim_id sent as pickup job '$id'",
                );
            } else {
                $pickup_status[$claim_id] = array(
                    'status' => 'error',
                    'data' => $deliveryService->getError(),
                    'message' => $deliveryService->getErrorMessage(),
                );
            }
        }

        return $pickup_status;
    }

    /**
     * Shows the business Drop Locations
     */
    public function drop_locations()
    {
        $this->load->model('droplocation_model');
        $content['dropLocations'] = $this->droplocation_model->get_aprax(array(
            'business_id' => $this->business_id,
        ));

        $this->renderAdmin('drop_locations', $content);
    }

    /**
     * Saves a Drop Location
     */
    public function drop_locations_save()
    {
        $this->load->model('droplocation_model');

        $default = $this->input->post('default') ? 1 : 0;
        $dropLocationID = $this->input->post('dropLocationID');

        $options = array(
            'business_id' => $this->business_id,
            'address' => $this->input->post('address'),
            'default' => $default,
        );

        // is an update ?
        if ($dropLocationID) {
            $options['dropLocationID'] = $dropLocationID;
            $this->droplocation_model->save($options);
            $message = "Drop Location updated";
        } else {
            $dropLocationID = $this->droplocation_model->save($options);
            $message = "Drop Location added";
        }

        // do not allow other defaults if this is a default
        if ($default) {
            $this->droplocation_model->update_all(array(
                'default' => 0,
            ), array(
                'business_id' => $this->business_id,
                'dropLocationID !=' => $dropLocationID,
            ));
        }

        set_flash_message('success', $message);
        redirect_with_fallback('/admin/third_party_deliveries/drop_locations');
    }

    /**
     * Shows the from to edit a Drop Location
     *
     * @param int $dropLocationID
     */
    public function drop_locations_edit($dropLocationID = null)
    {
        $this->load->model('droplocation_model');
        $content['dropLocation'] = $this->droplocation_model->get_by_primary_key($dropLocationID);

        $this->renderAdmin('drop_locations_edit', $content);
    }

    /**
     * Deletes a Drop Location
     *
     * @param int $dropLocationID
     */
    public function drop_locations_delete($dropLocationID = null)
    {
        $this->load->model('droplocation_model');
        $dropLocation = $this->droplocation_model->get_by_primary_key($dropLocationID);

        $this->droplocation_model->delete_aprax(array(
            'dropLocationID' => $dropLocationID,
            'business_id' => $this->business_id,
        ));

        set_flash_message('success', 'Drop Location deleted');
        redirect_with_fallback('/admin/third_party_deliveries/drop_locations');
    }

}
