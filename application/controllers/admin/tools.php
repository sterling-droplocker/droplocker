<?php


class Tools extends MY_Admin_Controller
{
    public $buser_id;
    public $business_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function test_sms_claim()
    {
        $this->db->select('smsSettings.*,business.*');
        $this->db->from('smsSettings');
        $this->db->join('business','business.businessID=smsSettings.business_id');
        $all_sms_settings_query = $this->db->get();
        $all_sms_businesses = $all_sms_settings_query->result();

        $business_sms_settings = array();
        foreach ($all_sms_businesses as $business) {
            $business_sms_settings[$business->provider][$business->businessID] = array('name' => $business->companyName, 'sms' => $business->number );
        }

        $business_sms_settings['EmailToSMS'] = array();

        echo $this->load->view('tools/sms_test_tool', array('businesses' => $business_sms_settings ) );
        //exit;
    }


    //one time fix for a bunch of orders that did not get inventoried
    public function updateOrderStatus()
    {
        $getOrders = "select orders.orderID, orders.orderStatusOption_id, orders.dateCreated from orderStatus
            join orders on orderID = order_id
            where orderStatus.orderStatusOption_id = 3
            and orders.orderStatusOption_id = 1
            and orders.orderID >=482281
            order by order_id desc ";

            $result = $this->db->query($getOrders);
        $orders = $result->result();

        foreach ($orders as $order) {
            $updateOrders = "update orders set orderStatusOption_id = 3 where orderID = $order->orderID";
            $result = $this->db->query($updateOrders);
            echo $order->orderID .' updated<br>';
        }
    }
}
