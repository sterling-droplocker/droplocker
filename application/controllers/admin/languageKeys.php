<?php
use App\Libraries\DroplockerObjects\LanguageKey;
class LanguageKeys extends MY_Admin_Controller
{
    /**
     * Adds a new language key to the database
     * Expects the following POST parameters
     *  name : The name of the key
     *  languageView_id : the primary key of the languagView that the language key is to be associated with
     *  defaultText : the default text to display if the company has no translation for it
     */
    public function add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("name", "Name", "required");
        $this->form_validation->set_rules("defaultText", "Default Text", "required");
        $this->form_validation->set_rules("languageView_id", "Language View", "required|is_natural_no_zero|does_languageView_exist");
        if ($this->form_validation->run()) {
            $languageKey = new LanguageKey();
            $languageKey->name = $this->input->post("name");
            $languageKey->defaultText = $this->input->post("defaultText");
            $languageKey->languageView_id = $this->input->post("languageView_id");
            $languageKey->save();
            $this->load->model("languageView_model");
            $languageView = $this->languageView_model->get_by_primary_key($languageKey->languageView_id);
            set_flash_message("success", "Created language key '{$languageKey->name}' for language view '$languageView->name'");
        } else {
           set_flash_message("error", validation_errors());
        }
        $this->session->set_flashdata("post", serialize($_POST));

        $this->goBack("/admin/superadmin/manage_languageKeys_and_languageViews");
    }

    public function delete()
    {
        if (isset($_GET)) {
            $_POST = $_GET;
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules("languageKeyID", "Language Key ID", "required|is_natural_no_zero|does_languageKey_exist|is_languageKey_deletable"); //is_languageKey_deletable
        if ($this->form_validation->run()) {
            $languageKey = new LanguageKey($this->input->post("languageKeyID"));
            $languageKey->delete();
            set_flash_message("success", "Deleted language key '{$languageKey->name}'");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_languageKeys_and_languageViews");
    }

    /**
     * Updates an existing language key's property. This action is intended to be used with the jquery.editable library.
     * Expects the following POST parameters:
     *  languageKeyID
     *  property
     *  value
     * Returns a result response formatted for jquery.editable
     */
    public function update()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("languageKeyID", "Language Key ID", "required|is_natural_no_zero|does_languageKey_exist");
            $this->form_validation->set_rules("property", "Property", "required|alpha");
            $this->form_validation->set_rules("value", "Value", "required");

            if ($this->form_validation->run()) {
                $property = $this->input->post("property");
                $value = $this->input->post("value");
                $languageKeyID = $this->input->post("languageKeyID");
                $languageKey = new LanguageKey($languageKeyID);
                $languageKey->$property = $value;
                $languageKey->save();
                //Note the jquery .editable needs the html characters encoded so as to display the text as literal html tags instead of rendered html tags.
                echo htmlspecialchars($value);
            } else {
                echo validation_errors();
            }
        } else {
            echo "This action only be accessed with an AJAX request";
        }

    }
}
