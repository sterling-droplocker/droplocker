<?php
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Testimonial;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Location;

/**
 * TODO: remove empty unused controller
 */
class Mobile extends MY_Admin_Controller
{
    public $buser_id;
    public $business_id;
    public $business;

    public function index()
    {
        $this->load->view('admin/blank', '', true);
        $this->data['content'] = $this->load->view('admin/index', array(), true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    public function view_claims_table()
    {
        $this->data['content'] = $this->load->view('admin/mobile_view_claims_table', array(), true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }
}
