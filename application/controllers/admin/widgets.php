<?php

use App\Libraries\DroplockerObjects\Location;

class Widgets extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        // utility_helper that will redirect to denied page if the user does not have the proper permission
        check_acl();
    }

    //this function returns open support tickets assigned to a specified user
    public function tickets_user()
    {
        $getSettings = "select setting from dashboard where employee_id = $this->buser_id and widget_id = (select widgetID from widget where fileName = 'tickets_user')";
        $query = $this->db->query($getSettings);
        $users = $query->row();
        $user = $users->setting;

        !isset($user) && $user = 0;
        $getTickets = "SELECT `ticketID`, `customer`.`firstName`, `customer`.`lastName`, `location`.`address`, `lockerName`, `ticket`.`status`, `employee_id`, `ticket`.`dateCreated`, `issue`, `title`, `bag_id`, `locker_id`, `locationID`, ticket.customer_id, concat(employee.firstName, ' ', employee.lastName) as assignedTo
                        FROM (`ticket`)
                        LEFT JOIN `bag` ON `bagID` = `bag_id`
                        LEFT JOIN `locker` ON `lockerID` = `ticket`.`locker_id`
                        LEFT JOIN `location` ON `locationID` = `ticket`.`location_id`
                        LEFT JOIN `customer` ON `customerID` = `ticket`.`customer_id`
                        LEFT join employee on employeeID = `ticket`.assignedTo
                        WHERE `ticket`.`business_id` = $this->business_id
                        and ticket.status != 'Closed'
                        and assignedTo = ".$user."
                        ORDER BY `dateCreated` desc";
        $query = $this->db->query($getTickets);
        $content['tickets'] = $query->result();

        $content['user'] = $user;
        $this->load->model('employee_model');
        $content['employees'] = $this->employee_model->get(array('business_id'=>$this->business_id, 'order_by'=>'firstName', 'active'=>'1'));

        $this->load->view('admin/widgets/tickets_user', $content);
    }

    public function tickets_user_set()
    {
        $updateSetting = "update dashboard set setting = '".$_POST['employee']."' where employee_id = $this->buser_id and widget_id = (select widgetID from widget where fileName = 'tickets_user')";
        $query = $this->db->query($updateSetting);

        set_flash_message('success', "Ticket Users Widget Updated");
        redirect($_SERVER['HTTP_REFERER']);
    }

    //this shows a list of tickets by problem type
    public function tickets_problem_types()
    {
        $getSettings = "select setting from dashboard where employee_id = $this->buser_id and widget_id = (select widgetID from widget where fileName = 'tickets_problem_types')";
        $query = $this->db->query($getSettings);
        $types = $query->row();
        $selectedTypes = implode(",",json_decode($types->setting));
        $content['selectedTypes'] = json_decode($types->setting);

        if (isset($selectedTypes)) {
            $getTickets = "SELECT `ticketID`, `customer`.`firstName`, `customer`.`lastName`, `location`.`address`, `lockerName`, `ticket`.`status`, `employee_id`, `ticket`.`dateCreated`, `issue`, `title`, `bag_id`, `locker_id`, `locationID`, ticket.customer_id, concat(employee.firstName, ' ', employee.lastName) as assignedTo
                            FROM (`ticket`)
                            LEFT JOIN `bag` ON `bagID` = `bag_id`
                            LEFT JOIN `locker` ON `lockerID` = `ticket`.`locker_id`
                            LEFT JOIN `location` ON `locationID` = `ticket`.`location_id`
                            LEFT JOIN `customer` ON `customerID` = `ticket`.`customer_id`
                            LEFT join employee on employeeID = `ticket`.assignedTo
                            WHERE `ticket`.`business_id` = $this->business_id
                            and ticket.status != 'Closed'
                            and ticketProblem_id in ($selectedTypes)
                            ORDER BY `dateCreated` desc";
            $query = $this->db->query($getTickets);
            $content['tickets'] = $query->result();
        }
        $getTypes = "Select * from ticketProblem where business_id in (0, $this->business_id)";
        $query = $this->db->query($getTypes);
        $content['types'] = $query->result();

        $this->load->view('admin/widgets/tickets_problem_types', $content);
    }

    public function tickets_problem_types_set()
    {
        $serial = json_encode($_POST['type']);
        $updateSetting = "update dashboard set setting = '".$serial."' where employee_id = $this->buser_id and widget_id = (select widgetID from widget where fileName = 'tickets_problem_types')";
        $query = $this->db->query($updateSetting);

        set_flash_message('success', "Ticket Problems Widget Updated");
        redirect($_SERVER['HTTP_REFERER']);
    }

    //this shows a list of tickets by problem type
    public function tickets_overXdays()
    {
        $getSettings = "select setting from dashboard where employee_id = $this->buser_id and widget_id = (select widgetID from widget where fileName = 'tickets_overXdays')";
        $query = $this->db->query($getSettings);
        $content['days'] = $days = $query->row();

        if (isset($days->setting)) {
            $getTickets = "SELECT `ticketID`, `customer`.`firstName`, `customer`.`lastName`, `location`.`address`, `lockerName`, `ticket`.`status`, `employee_id`, `ticket`.`dateCreated`, `issue`, `title`, `bag_id`, `locker_id`, `locationID`, ticket.customer_id, concat(employee.firstName, ' ', employee.lastName) as assignedTo
                            FROM (`ticket`)
                            LEFT JOIN `bag` ON `bagID` = `bag_id`
                            LEFT JOIN `locker` ON `lockerID` = `ticket`.`locker_id`
                            LEFT JOIN `location` ON `locationID` = `ticket`.`location_id`
                            LEFT JOIN `customer` ON `customerID` = `ticket`.`customer_id`
                            LEFT join employee on employeeID = `ticket`.assignedTo
                            WHERE `ticket`.`business_id` = $this->business_id
                            and ticket.status != 'Closed'
                            and ticket.dateCreated < date_sub(now(), INTERVAL $days->setting DAY)
                            ORDER BY `dateCreated` desc";
            $query = $this->db->query($getTickets);
            $content['tickets'] = $query->result();
        }

        $getTypes = "Select * from ticketProblem where business_id in (0, $this->business_id)";
        $query = $this->db->query($getTypes);
        $content['types'] = $query->result();

        $this->load->view('admin/widgets/tickets_overXdays', $content);
    }

    public function tickets_overXdays_set()
    {
        $updateSetting = "update dashboard set setting = ".$_POST['days']." where employee_id = $this->buser_id and widget_id = (select widgetID from widget where fileName = 'tickets_overXdays')";
        $query = $this->db->query($updateSetting);

        set_flash_message('success', "Ticket Over Days Widget Updated");
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function customer_stats()
    {
        $this->load->model('customer_model');
        $content['total'] = $this->customer_model->customerCount();
        $content['customerOrders'] = $this->customer_model->customerCountOrder();
        $this->load->model('item_model');
        $content['total_items'] = $this->item_model->item_count();

        $this->load->view('admin/widgets/customer_stats', $content);
    }

    public function customer_search()
    {
        @$this->load->view('admin/widgets/customer_search', $content);
    }

    public function order_stats()
    {
        $this->load->model('order_model');
        $content['total'] = $this->order_model->orderStats();
        $this->load->view('admin/widgets/order_stats', $content);
    }

    public function retention_rate_lockers()
    {
        $this->get_retention_rate("Lockers");
    }

    public function retention_rate_concierge()
    {
        $this->get_retention_rate("Concierge");
    }

    public function retention_rate_kiosk()
    {
        $this->get_retention_rate("Kiosk");
    }

    public function retention_rate_office()
    {
        $this->get_retention_rate("Offices");
    }

    public function retention_rate_overall()
    {
        $this->get_retention_rate("Overall");
    }

    protected function get_retention_rate($location_type)
    {
        $this->load->model("customer_location_model");
        $customers_last_month = $this->customer_location_model->get_customers_by_location($this->business_id, $location_type);
        $customers_this_month = $this->customer_location_model->get_customers_by_location($this->business_id, $location_type, "customers_this_month");
        $new_customers_this_month = $this->customer_location_model->get_customers_by_location($this->business_id, $location_type, "new_customers_this_month");

        $content["retention_rate"] = ($customers_this_month-$new_customers_this_month)/$customers_last_month;

        $content["location_type"] = $location_type;
        $this->load->view('admin/widgets/retention_rate', $content);        
    }

}
