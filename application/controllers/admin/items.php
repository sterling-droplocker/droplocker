<?php

class Items extends MY_Admin_Controller
{
    public $submenu = 'orders';

    public function __construct()
    {
        parent::__construct();

        $this->business_id = $this->session->userdata('business_id');
    }

    /**
     * Requires the following GET parameters
     *  itemID int
     * Can take the following GET parameters
     *  order_idn int
     *
     * @throws \InvalidArgumentException
     */
    public function view()
    {
        $itemID = $this->input->get("itemID");

        if (!is_numeric($itemID)) {
            throw new \InvalidArgumentException("'itemID' must be passed as a GET parameter and must be numeric.");
        }

        $this->load->model("item_model");
        $item = $this->item_model->get_by_primary_key($itemID);
        $content['item'] = $item;

        // Redirect if business does not own this item
        if ($item->business_id != $this->business_id) {
            set_flash_message("error", "This item does not belong to your business.");
            redirect($this->agent->referrer());
        }

        $this->load->model("customer_item_model");
        $content['customer_items'] = $this->customer_item_model->get_with_customer_information(array("item_id" => $itemID));

        if ($this->input->get("orderID")) {
            $content['order_id'] = $this->input->get("orderID");

            $this->load->model("order_model");
            $order = $this->order_model->get_by_primary_key($content['order_id']);

            $customer_id = $order->customer_id;

        } else {
            $content['order_id'] = 0;

            // if only one customer owns the item, we can use that to load the DC preferences
            if (count($content['customer_items']) == 1) {
                $customer_id = $content['customer_items'][0]->customer_id;
            }
        }

        $content['item_histories'] = $this->item_model->get_item_history($itemID);
        $content['upcharges'] = $this->item_model->get_item_upcharges($itemID);
        $content['specifications'] = $this->item_model->get_item_specifications($itemID);

        $this->load->model("product_process_model");
        $product_process = $this->product_process_model->get_by_primary_key($item->product_process_id);

        $this->load->model("product_model");
        $content['product'] = $this->product_model->get_by_primary_key($product_process->product_id);

        $this->load->model("process_model");
        $content['process'] = $this->process_model->get_by_primary_key($product_process->process_id);

        $this->load->model("picture_model");
        $content['pictures'] = $this->picture_model->get_aprax(array("item_id" => $itemID));

        $this->load->model("itemissue_model");
        $content['itemIssues'] = $this->itemissue_model->get_aprax(array("item_id" => $itemID));

        $this->load->model("barcode_model");
        $content['barcode'] = $this->barcode_model->get_one(array("item_id" => $itemID));



        $this->load->model('itempreference_model');
        $content['item_preferences'] = $this->itempreference_model->get_preferences_names($itemID);

        // show customer's DC prefs in item
        if (!empty($customer_id)) {
            $this->load->model('customerpreference_model');
            $customer_preferences = $this->customerpreference_model->get_preferences_names_for_cateogry($customer_id, $content['product']->productCategory_id);

            // check that item preferences matches customer preferences
            $content['changes'] = array();
            foreach ($customer_preferences as $name => $value) {
                if (isset($content['item_preferences'][$name]) && $content['item_preferences'][$name] != $value) {
                    $content['changes'][$name] = "Note: Customer's $name setting '{$customer_preferences[$name]}' does not match item DC setting '{$content['item_preferences'][$name]}'";
                } else {
                    $content['item_preferences'][$name] = $value . '*';
                }
            }
        }

        $this->renderAdmin('view', $content);
    }

    /**
     * Can take the following URL segments:
     *  segment 4: Item ID
     *  segment 5: Optional, the item issue ID
     * @throws \InvalidArgumentException
     */
    public function add_issue($itemID = null, $order_id = null)
    {
        if ($this->input->get("item_id")) {
            $itemID = $this->input->get("item_id");
        }

        $content['order_id'] = $order_id;

        if (!is_numeric($itemID)) {
            throw new \InvalidArgumentException("'itemID' must be passed as segment 4 of the URL or as the GET parameter 'item_id'");
        }
        if ($this->input->get("item_issue_id")) {
            $item_issue_id = $this->input->get("item_issue_id");
        }

        $content['itemIssueID'] = $item_issue_id;
        $this->load->model('item_model');
        $this->load->helper('item_picture');

        $item = $this->item_model->get_customer_items(array('select'=>'itemID,barcode,displayName,process.name as processName, file','itemID'=>$itemID));
        $content['item'] = $item[0];

        //get the issues for this item
        $this->load->model('itemissue_model');
        $sql = "SELECT * FROM itemIssue WHERE note != '' and item_id = ? ORDER BY updated";
        $q = $this->db->query($sql, array($content['item']->itemID));

        $issues = $q->result();

        if ($issues) {
            for ($i=0; $i<sizeof($issues); $i++) {
                if ($issues[$i]->status=='Closed') {
                    $content['closed'][] = $issues[$i];
                } elseif ($issues[$i]->status == "Opened") {
                    $content['open'][] = $issues[$i];
                }
            }
        }

        if ($item_issue_id) {
            $this->itemissue_model->clear();
            $this->itemissue_model->itemIssueID = $item_issue_id;
            $current_issue = $this->itemissue_model->get();
            $content['x'] = $current_issue[0]->x;
            $content['y'] = $current_issue[0]->y;
            $content['issueType'] = $current_issue[0]->issueType;
            $content['note'] = $current_issue[0]->note;
            $content['frontBack'] = $current_issue[0]->frontBack;
        } else {
            $content['x'] = "";
            $content['y'] = "";
            $content['issueType'] = "";
            $content['note'] = "";
            $content['frontBack'] = "";
        }

        $this->data['content'] = $this->load->view("/admin/items/add_issue", $content, true);
        $this->template->write_view('content', 'inc/dual_column_left', $this->data);
        $this->template->render();
    }

    /**
     * Ui for editing an item issue
     * This gets called when the user clicks on the image of clothes
     */
    public function edit_issue()
    {
        $item_id = $_GET['item_id'];
        $order_id = $_GET['order_id'];
        if ($_POST['x']) {
            $note = $_POST['note'];
            if ($_POST['itemIssueID']) {
                $update1 = "update itemIssue
                    set item_id = ?,
                    frontBack = ?,
                    x = ?,
                    y = ?,
                    issueType = ?,
                    note = ?,
                    created = now()
                    where itemIssueID = ?";
                $update = $this->db->query($update1,
                            array($item_id, $_POST['frontBack'], $_POST['x'], $_POST['y'],
                                    $_POST['issueType'], $note, $_POST['itemIssueID'])
                );
                $insert_id = $_POST['itemIssueID'];
                set_flash_message("success", "Updated Issue");
            } else {
                $insert1 = "insert into itemIssue
                    (item_id, frontBack, x, y, issueType, note, created, status)
                    values
                    (?, ?, ?, ?, ?, ?, now(), 'Opened')";

                $q = $this->db->query($insert1,
                        array($item_id, $_POST['frontBack'], $_POST['x'], $_POST['y'],
                            $_POST['issueType'], $note)
                );

                $insert_id = $this->db->insert_id();
                set_flash_message("success", "Added New Issue");
            }

            redirect("/admin/items/add_issue/$item_id");
        }

        if ($_POST['delete']) {
                $delete1 = "delete from itemIssue where itemIssueID = ?";
                $delete = $this->db->query($delete1, array($_POST['issueItemID']));
        }

        redirect($this->agent->referrer());
    }

    /**
    * Deletes an itemIssue
    *
    * item_id is set from url segment 4
    * itemIssueID is set from url segment 5
    */
    public function delete_issue()
    {
        $item_id = $this->input->get('item_id');
        $itemIssueID = $this->input->get('item_issue_id');
        $order_id = $this->input->get('order_id');
        $delete1 = "delete from itemIssue where itemIssueID = {$itemIssueID}";
        $delete = $this->db->query($delete1);
        set_flash_message("success", "Deleted Issue");
        redirect("/admin/items/add_issue/?item_id=".$item_id."&order_id=".$order_id);
    }
}
