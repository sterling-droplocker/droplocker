<?php

//aliased to avoid collision with Product_Process controller
use App\Libraries\DroplockerObjects\Product_Process as Product_Process_Object;

use App\Libraries\DroplockerObjects\Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\TaxGroup;

//TODO rename this to plural and avoid collision with DroplockerObject Product_Process
class Product_Process extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("product_process_model");
    }

    /**
     * Adds another product process ID for a particular product.
     * Expects 'product_id', and 'process_id' as post parameters.
     */
    public function add_process()
    {
        try {
            if (!$this->input->post('product_id')) {
                throw new Exception("Missing 'product_id' parameter.");
            }
            $product_id = $this->input->post("product_id");

            $process_id = $this->input->post("process_id");
            try {
                $product = new Product($product_id);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Could not find product ID '$product_id'");
            }
            try {
                $process = new Process($process_id);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
                set_flash_message("error", "Could not find process ID '$process_id'");
            }

            $this->load->model("product_process_model");
            $existing_product_processes = $this->product_process_model->get_aprax(array("product_id" => $product_id, "process_id" => $process_id));

            if (empty($existing_product_processes)) {
                $product_process = new Product_Process_Object();
                $product_process->product_id = $product_id;
                $product_process->process_id = $process_id;
                $product_process->save();
                set_flash_message("success", "Added process '$process->name' to product '$product->name'");

            } else {
                set_flash_message("error", "Process '{$process->name}' already exists for '$product->name'");
            }

        } catch (Exception $e) {
            set_flash_message("error", "Could not update process: {$e->getMessage()}");
            send_exception_report($e);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Updates the process ID for a particular product.
     * Expects 'product_processID', and 'process_id' as post parameters.
     */
    public function update_process()
    {
        if (!$this->input->post('product_processID')) {
            throw new Exception("Missing 'product_processID' POST parameter.");
        }
        $product_processID = $this->input->post("product_processID");
        if (!is_numeric($product_processID)) {
            set_flash_message("error", "'product_processID' must be numeric.");
        }
        try {
            $product_process = new Product_Process_Object($product_processID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            set_flash_message("error", "Product Process ID '$product_processID' not found in database");

            return;
        }

        $process_id = $this->input->post("process_id");

        $this->load->model("process_model");
        $this->load->model("product_model");
        $process = $this->process_model->get_by_primary_key($process_id);
        $product = $this->product_model->get_by_primary_key($product_process->product_id);

        $existing_product_processes = $this->product_process_model->get_aprax(array("product_id" => $product->productID, "process_id" => $process->processID));
        if (empty($existing_product_processes)) {
            $product_process->process_id = $process_id;
            $product_process->save();
            set_flash_message("success", "Updated product '{$product->name}' process to '{$process->name}'");

        } else {
            set_flash_message("error", "Process '{$process->name}' already exists for '$product->name'");
        }

        $this->goBack("/admin/admin");
    }

    public function get_total()
    {
        if ($this->input->get("business_id")) {
            try {
                $business_id = $this->input->get("business_id");
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($business_id);
                $query = "SELECT COUNT(product_processID) AS total FROM product_process INNER JOIN product ON product.productiD=product_process.product_id WHERE product.business_id=?";
                $result = $this->db->query($query, array($business_id))->row();
                output_ajax_success_response(array("total" => $result->total), "Total product processes for '{$business->companyName} ({$business->businessID})'");
            } catch (Exception $e) {
                send_exception_report($e);
                output_ajax_error_response("An internal error occurred attempting to retrieve the product processes total");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter.");
        }
    }
    /**
     * Updates a property in an existing product process. This action is intended to be used with requests initiated from the jquery.editable library.
     * Expects the following POST parameters:
     *  product_processID
     *  property
     *  value
     * Returns a response message formatted for the jquery.editable library
     * @throws \InvalidArgumentException
     */
    public function update()
    {
        if (!$this->input->is_ajax_request()) {
            echo "This action can only be accessed through an AJAX request";
        }

        try {
            if (!$this->input->post("product_processID")) {
                throw new \InvalidArgumentException("'product_processID' must be passed as a POST parameter.");
            }
            $product_processID = trim($this->input->post("product_processID"));

            if (!is_numeric($product_processID)) {
                throw new \InvalidArgumentException("'product_processID' must be numeric.");
            }
            if (!$this->input->post("property")) {
                throw new \InvalidArgumentException("'property' must be passed as a POST parameter.");
            }
            if (!array_key_exists("value", $_POST)) {
                throw new \InvalidArgumentException("'value' must be passed as a POST parameter.");
            }
            $value = trim($this->input->post("value"));
            $property = trim($this->input->post("property"));

            $product_process = new Product_Process_Object($product_processID);
            $product_process->$property = $value;

            if ($property == "process_id") {
                $existing_product_processes = $this->product_process_model->get_aprax(array(
                    "product_id" => $product_process->product_id,
                    "process_id" => $value
                ));

                if (!empty($existing_product_processes)) {
                    $process = new Process($value);
                    $product = new Product($product_process->product_id);

                    throw new \App\Libraries\DroplockerObjects\Validation_Exception("Process '{$process->name}' already exists for '$product->name'");
                }
            }

            // set to non-taxable if no taxGroup
            if ($property == "taxGroup_id") {
                $product_process->taxable = $value ? 1 : 0;
            }

            $product_process->save();

            switch ($property) {
            	case "price":
            	    echo format_money_symbol($this->business_id, '%.2n', $product_process->$property);
            	break;

            	case "taxGroup_id":
            	    if ($value) {
            	        $taxGroup = new TaxGroup($value);
            	        echo $taxGroup->name;
            	    } else {
            	        echo 'Non Taxable';
            	    }
                break;

            	case "process_id":
            	    if ($product_process->process_id) {
            	       $process = new Process($product_process->process_id);
            	       echo $process->name;
            	    } else {
                        echo "None";
            	    }

            	break;

                default:
                    echo $product_process->$property;
            }

        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo $e->getMessage();
        } catch (App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo "Product Process not found";
            send_exception_report($e);
        } catch (Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo $e->getMessage();
            send_exception_report($e);
        }
    }

    /**
     * Retrieves the available product processes for a particular product.
     * Expects the following GET parameter passed as an AJAX request
     *  productID
     * @throws Exception
     */
    public function get_available_product_processes()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->helper("ajax");
            if (!$this->input->get("productID")) {
                output_ajax_error_response("'productID' must be passed as a GET parameter");
            }
            $productID = $this->input->get("productID");
            if (!is_numeric($productID)) {
                output_ajax_error_response("'productID' must be numeric");
            }
            $this->load->model("product_process_model");
            $product_processes = $this->product_process_model->get_aprax(array("product_id" => $productID));

            $get_product_processes_query = $this->db->query("
                SELECT processID,process.name
                FROM product_process
                INNER JOIN process ON product_process.process_id=processID
                WHERE product_id = ?
            ", array($productID));
            $processes = $get_product_processes_query->result();
            output_ajax_success_response($processes);
        } else {
            echo("This action is available only available through an ajax request");
        }

    }
}
