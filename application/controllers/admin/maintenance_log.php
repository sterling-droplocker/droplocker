<?php

use App\Libraries\DroplockerObjects\MaintenanceLog;

class Maintenance_Log extends MY_Admin_Controller
{
    public $submenu = 'admin';

    public function index()
    {
        $content['maintenanceLogs'] = MaintenanceLog::search_aprax(array(
            "business_id" => $this->business_id
        ), true, "datePerformed DESC");

        $this->renderAdmin('index', $content);
    }

    public function add()
    {
        try {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("machine", "Machine", "required");
            $this->form_validation->set_rules("workDone", "Work Completed", "required");

            if ($this->form_validation->run()) {
                $maintenanceLog = new MaintenanceLog();
                $maintenanceLog->business_id = $this->business_id;
                $maintenanceLog->employee_id = $this->employee->employeeID;
                $maintenanceLog->datePerformed = new \DateTime(null, new DateTimeZone($this->business->timezone));
                $maintenanceLog->machine = $this->input->post("machine");
                $maintenanceLog->workPerformed = $this->input->post("workDone");
                $maintenanceLog->save();
                set_flash_message('success', "Maintenance Log Added");
            } else {
                set_flash_message("error", validation_errors());
            }
        } catch (Exception $e) {
            send_exception_report($e);
            set_flash_message("error", "Error adding to maintenance log");
        }

        redirect("/admin/maintenance_log");
    }

}