<?php
use App\Libraries\DroplockerObjects\Business_Language;

class Business_Languages extends MY_Admin_Controller
{
    public function get_as_json()
    {
        if ($this->input->get("business_id")) {
            $this->load->model("business_language_model");
            $business_id = $this->input->get("business_id");
            $business_languages = $this->business_language_model->get_all_as_codeigniter_dropdown($business_id);
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($business_id);
            output_ajax_success_response($business_languages, "All business languages for {$business->companyName}");

        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }
    }
    /**
     * The following action creates a new buisness language.
     * Expects the following POST parameters:
     * language_id
     */
    public function add()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("language_id", "Language", "is_natural_no_zero|required|does_language_exist");
        if ($this->form_validation->run()) {
            $this->load->model("business_language_model");
            $language_id = $this->input->post("language_id");
            $locale = $this->input->post("locale");
            $existing_business_languages = $this->business_language_model->count(array("business_id" => $this->business_id, "language_id" => $language_id));

            if ($existing_business_languages > 0) {
                $this->load->model("language_model");
                $language = $this->language_model->get_by_primary_key($language_id);
                set_flash_message("error", "Language '{$language->tag} - {$language->description}' is already defined for '{$this->business->companyName}'");
            } else {
                $business_language = new Business_Language();
                $business_language->business_id = $this->business_id;
                $business_language->language_id = $language_id;
                $business_language->locale = $locale;
                $business_language->save();
                $this->load->model("language_model");
                $language = $this->language_model->get_by_primary_key($business_language->language_id);
                set_flash_message("success", "Added language '{$language->tag} - {$language->description}' to '{$this->business->companyName}'");
            }
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/admin/manage_languageKeys");
    }

    public function delete()
    {
        if (isset($_GET)) {
            $_POST = $_GET;
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules("business_languageID", "Business Language", "is_natural_no_zero|required");
        if ($this->form_validation->run()) {
            $business_language = new Business_Language($this->input->post("business_languageID"), true);
            $this->load->model("customer_model");
            $customers_with_business_language_as_default = $this->customer_model->count(array("default_business_language_id" => $business_language->{Business_Language::$primary_key}));

            $this->load->model("reminder_email_model");
            $reminder_emails_for_business_language_count = $this->reminder_email_model->count(array("business_language_id" => $business_language->{Business_Language::$primary_key}));
            if ($business_language->{Business_Language::$primary_key} == $this->business->default_business_language_id) {
                set_flash_message("error", "Can not delete the default business language");
            } elseif ($customers_with_business_language_as_default > 0) {
                set_flash_message("error", "Can not delete business language '{$business_language->relationships['Language'][0]->tag}' because $customers_with_business_language_as_default customers have this language specified as the default");
            } elseif ($reminder_emails_for_business_language_count > 0) {
                set_flash_message("error", "Can not delete because there are $reminder_emails_for_business_language_count reminder emails associated with this business language");
            } else {
                $business_language->delete();
                set_flash_message("success", "Deleted business language '{$business_language->relationships['Language'][0]->tag}'");
            }
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/admin/manage_languageKeys");
    }

    /**
     * Updates the specified property of the selected business_language
     */
    public function update()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("default_business_language_id", "Business Language", "is_natural_no_zero|required");
        $this->form_validation->set_rules("locale", "Business Language Locale", "required");

        $language_id = $this->input->post("default_business_language_id");
        $locale = $this->input->post("locale");

        if ($this->form_validation->run()) {
            $business_language = new Business_Language($language_id, true);
            $business_language->locale = $locale;
            $business_language->save();
            if ($this->input->is_ajax_request()) {
                echo $value;
            } else {
                set_flash_message("success", "Updated property locale to ".$locale."");
                $this->goBack("/admin/admin/manage_business_languages");
            }
         } else {
            if ($this->input->is_ajax_request()) {
                echo validation_errors();
            } else {
                set_flash_message("error", validation_errors());

                $this->goBack("/admin/admin/manage_business_languages");
            }
        }
    }
}
