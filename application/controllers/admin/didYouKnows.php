<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\DidYouKnow;

class DidYouKnows extends MY_Admin_Controller
{

    public $submenu = 'admin';

    /**
     * Renders the interface for maanging Did You Know notices or updates/adds a Did You Know notice
     * @param bool $bizzie_superadmin If the user is accessing the action as a Bizzie superadmin
     */
    public function index($bizzie_superadmin = false)
    {
        $this->load->model('did_you_know_model');

        if ($bizzie_superadmin) {
            $content['add_action'] = "/admin/didYouKnows/add_to_all_businesses";
            $content['update_action'] = "/admin/didYouKnows/update_all_businesses";
            $this->template->write("page_title", "Manage Master Did You Know Notices");
        } else {
            $content['add_action'] = "/admin/didYouKnows/add";
            $content['update_action'] = "/admin/didYouKnows/update";

            $this->template->write("page_title", "Manage Did You Know Notices");
        }

        $this->did_you_know_model->business_id = $this->business_id;
        $content['didYouKnows'] = $this->did_you_know_model->get();

        $this->renderAdmin('index', $content);
    }

    public function add()
    {
        //adding new notes
        if ($this->input->post("add")) {

            $didYouKnow = new DidYouKnow();
            $didYouKnow->note = $_POST['note'];
            $didYouKnow->business_id = $this->business_id;
            $didYouKnow->save();

            set_flash_message('success', "Note Added");

            redirect("/admin/didYouKnows/");
        }
    }

    public function update()
    {
        //updating notes
        if ($this->input->post("update")) {
            $didYouKnow = new DidYouKnow($_POST['id']);
            $didYouKnow->note = $_POST['note'];
            $didYouKnow->save();

            set_flash_message('success', "Note Updated");
            redirect("/admin/didYouKnows/");
        }
    }

    public function delete_dyk($id = null)
    {
        $this->load->model('did_you_know_model');
        $this->did_you_know_model->didYouKnowID = $id;
        if ($this->did_you_know_model->delete()) {
            set_flash_message('success', "Deleted Note");

        } else {
            set_flash_message('error', "Note not deleted");
        }

        $this->goBack("/admin/admin/did_you_know");
    }


    /**
     * The following action deletes a Did You Know notice for a single business if the server not in Bizzie Mode.
     * If the server is in Bizzie mode and the did you know notice to be deleted belongs to the master business, then all related franchisee Did You Knows are deleted as well.
     * If the server is in Bizzie mode and the Did You Know to be deleted belongs to a franchisee, then only the franchisee DId You Know is deleted.
     * @throws \User_Exception
     */
    public function delete($didYouKnowID = null)
    {
        if (in_bizzie_mode()) {
            $master_business = get_master_business();
        }

        if (!is_numeric($didYouKnowID)) {
            throw new \User_Exception("didYouKnowID must be passed as segment 4 of the URL and must be numeric");
        }
        try {
            $didYouKnow = new DidYouKnow($didYouKnowID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            throw new \User_Exception("DidYouKnow ID '{$didYouKnowID}' not found in database");
        }
        if (in_bizzie_mode() && $didYouKnow->business_id == $master_business->businessID) {
            $this->load->model("did_you_know_model");
            $this->did_you_know_model->options = array("base_id" => $didYouKnow->didYouKnowID);
            $this->did_you_know_model->delete();
            set_flash_message("success", "Deleted master Did You Know notice ({$didYouKnow->didYouKnowID}) and from all franchises");
        } else {
            set_flash_message("success", "Deleted Did You Know notice from {$this->business->companyName}");
        }

        $didYouKnow->delete();

        $this->goBack("/admin/superadmin/manage_master_did_you_know_notices");
    }

    /**
     * Adds a did you know notice to all businesses. Note, the slave businesses base_id property is setto the primary key of the master did you know notice
     * @throws \Exception
     */
    public function add_to_all_businesses()
    {
        if (!in_bizzie_mode() || !is_superadmin()) {
            throw new \Exception("This action can only be accessed when the server is in Bizzie mode and the user is a superadmin.");
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules("note", "Note", "required");

        if ($this->form_validation->run()) {
            $this->load->model("business_model");

            $master_business = get_master_business();
            $master_didYouKnow = new DidYouKnow();
            $master_didYouKnow->business_id = $master_business->{Business::$primary_key};
            $master_didYouKnow->note = $this->input->post("note");
            $master_didYouKnow->save();

            $slave_businesses = $this->business_model->get_all_slave_businesses();
            foreach ($slave_businesses as $slave_business) {
                $slave_didYouKnow = new DidYouKnow();
                $slave_didYouKnow->business_id = $slave_business->{Business::$primary_key};
                $slave_didYouKnow->note = $master_didYouKnow->note;
                $slave_didYouKnow->base_id = $master_didYouKnow->{DidYouKnow::$primary_key};
                $slave_didYouKnow->save();
            }
            set_flash_message("success","Added the Did You Know notice to all businesses");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_master_did_you_know_notices");
    }

    /**
     * The following action updates all the DidYouKNow notices in the slave businesses if the Did You Know is a master
     * @throws \Exception Thrown if the server is not in Bizzie mode or the user is not a superadmin
     */
    public function update_all_businesses()
    {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("note", "Note", "required");
        $this->form_validation->set_rules("id", "ID","required");
        if (!in_bizzie_mode() || !is_superadmin()) {
            throw new \Exception("This action can only be accessed when the server is in Bizzie mode and the user is a superadmin.");
        }
        if ($this->form_validation->run()) {
            $didYouKnow = new DidYouKnow($this->input->post("id"));

            $master_business = get_master_business();
            if ($didYouKnow->business_id == $master_business->{Business::$primary_key}) {
                $master_didYouKnow = $didYouKnow;
                $master_didYouKnow->note = $this->input->post("note");
                $master_didYouKnow->save();

                $this->load->model("business_model");
                $slave_businesses = $this->business_model->get_all_slave_businesses();
                foreach ($slave_businesses as $slave_business) {
                    $slave_didYouKnows = DidYouKnow::search_aprax(array("business_id" => $slave_business->{Business::$primary_key}, "base_id" => $master_didYouKnow->{DidYouKnow::$primary_key}));
                    if (empty($slave_didYouKnows)) {
                        $slave_didYouKnow = $master_didYouKnow->copy();
                    } else {
                        $old_slave_DidYouKnow = $slave_didYouKnows[0];
                        $slave_didYouKnow = $master_didYouKnow->copy();
                        $slave_didYouKnow->{DidYouKnow::$primary_key} = $old_slave_DidYouKnow->{DidYouKnow::$primary_key};
                    }
                    $slave_didYouKnow->business_id = $slave_business->{Business::$primary_key};
                    $slave_didYouKnow->base_id = $master_didYouKnow->{DidYouKnow::$primary_key};
                    $slave_didYouKnow->save();
                }

            } else {
                throw new \Exception("Only a master Did You Know update can be applied to all franchises");
            }
            set_flash_message("success","Updated master Did You Know notice and for all franchises");
        } else {
            set_flash_message("error", validation_errors());
        }

        $this->goBack("/admin/superadmin/manage_master_did_you_knows");
    }
}
