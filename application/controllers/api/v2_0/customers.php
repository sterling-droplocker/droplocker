<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\ApiSession;
use App\Libraries\DroplockerObjects\KioskAccess;

class Customers extends MY_Api_Controller
{
    /**
     * Logs a customer into the system
     *
     * Expects the following GET parameters
     *  email
     *  password
     *
     * Returns the following JSON structure
     *  {
     *      status
     *      message
     *      customer_id
     *      sessionToken
     *      sessionTokenExpireDate
     *  }
     */
    public function validate()
    {
        $data = $this->data;
        $this->secretKey = $data['secretKey'];
        unset($data['secretKey']);

        if (empty($data['data']['username'])) {
            $this->api->responseError("Email or username is required");
        } elseif (empty($data['data']['business_id'])) {
            $this->api->responseError("Business ID is required");
        } elseif (empty($data['data']['password'])) {
            $this->api->responseError("Password is required");
        } else {

            $this->load->model('customer_model');
            $customer = $this->customer_model->login($data['data']['username'], md5($data['data']['password']), $data['data']['business_id']);
            if ($customer) {
                $plusDays = (empty($data['data']['sessionTokenExpireDays'])) ? 1 : $data['data']['sessionTokenExpireDays'];
                $expireDate = date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + $plusDays, date('Y')));
                if ($result = $this->db->get_where('apiSession', array('customer_id' => $customer->customerID, 'business_id' => $data['data']['business_id']))->row()) {
                    $this->db->update('apiSession', array('expireDate' => $expireDate), array('customer_id' => $customer->customerID));
                    $token = $result->token;
                } else {
                    $token = md5(uniqid(rand(0, 100000)));
                    $this->db->insert('apiSession', array('customer_id' => $customer->customerID,
                        'token' => $token,
                        'expireDate' => $expireDate,
                        'business_id' => $data['data']['business_id']));
                }

                $output['customer_id'] = $customer->customerID;
                $output['sessionToken'] = $token;
                $output['sessionTokenExpireDate'] = $expireDate;
                $this->api->response($output);
            } else {
                $this->api->responseError("Wrong username or password");
            }
        }
    }

    /**
     * Retrieves the logged in customer's preferences
     * Returns the following JSON structure
     *  {
     *      customerPreference1
     *      customerPreference2
     *      ....
     *  }
     */
    public function laundryPreferences()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }

        $this->load->model('customerpreference_model');
        $preferences = $this->customerpreference_model->get_preferences($session->customer_id);

        $customer = new Customer($session->customer_id, FALSE);
        $results['preferences'] = $preferences;
        $results['notes'] = $customer->customerNotes;
        $this->api->response($results);
    }

    /**
     * Retreives the properties and associated entities of a customer
     *
     * Returns a JSON structure in the following format:
     *  creditCard => {
     *      expireMonth
     *      expireYear
     *      lastFourCardNumber
     * },
     * customerDetails = {
     *      w9
     *      address1
     *      address2
     *      autoPay
     *      birthday
     *      bouncedDate
     *      business_id
     *      city
     *      comission_rate
     *      cusotmerID
     *      customerNotes
     *      dcNotes
     *      defaultWF
     *      email
     *      emailWindowStart
     *      emailWindowStop
     *      facebook_id
     *      firstName
     *      hold
     *      how
     *      inactive
     *      internalNotes
     *      invoice
     *      ip
     *      kioskAccess
     *      lastName
     *      lastUpdated
     *      lat
     *      linkedLocation_id
     *      location_id
     *      lockerLootTerms
     *      lon
     *      mgrAgreemnt
     *      noEmail
     *      noUpcharge
     *      owner_id
     *      password
     *      pendingEmailUpdate
     *      permAssemblyNotes
     *      phone
     *      preferences
     *      referredBy
     *      sex
     *      shirts
     *      signupDate
     *      sms
     *      startchOnShirts_id
     *      state
     *      twitterId
     *      type
     *      username
     *      wfNotes
     *      zip
     * },
     *
     * customerPreferences => {
     *      preferences
     * }
     * kiosk_access : Whether or not the customer has kiosk access for the card. Note, if the business does not have kiosk access enabled, the this parameter is absent from the response.
     * status
     */
    public function details()
    {//Warning, most of the following function still uses code written by *.
        $sessionToken = $this->input->get("sessionToken");
        if (!$this->input->get("sessionToken")) {
            output_ajax_error_response("'sessionToken' must be passed as a GET parameter");
        } else if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Sessiontoken not found");
        } else {
            $this->load->model("customer_model");

            $customer = $this->customer_model->get_by_primary_key($session->customer_id);

            $details['customerDetails'] = $customer;

            $this->load->library('productslibrary');
            $details['customerPreferences'] = $this->productslibrary->getCustomerPreferencesForAPI($customer->customerID);

            $this->load->model('creditCard_model');

            $creditCard = $this->creditCard_model->get_one(array("customer_id" => $customer->customerID));

            $business = $this->business_model->get_by_primary_key($customer->business_id);

            if (!empty($creditCard)) {
                $details['creditCard']['lastFourCardNumber'] = $creditCard->cardNumber;
                $details['creditCard']['expireMonth'] = $creditCard->expMo;
                $details['creditCard']['expireYear'] = $creditCard->expYr;
                $last_four_of_card_number = substr($creditCard->cardNumber, -4); //Note, only the last 4 digits of the card number are stored in the kiosk table.
            } else {
                $details['creditCard']['lastFourCardNumber'] = null;
                $details['creditCard']['expireMonth'] = null;
                $details['creditCard']['expireYear'] = null;
                $last_four_of_card_number = null;
            }
            if ($business->store_access == 1) {
                $this->load->model("kioskAccess_model");
                $details['kiosk_access'] = $this->kioskAccess_model->has_kiosk_access($last_four_of_card_number, $customer->customerID, $customer->business_id);
            }

            output_ajax_success_response($details, "Properties, creditcard, and laundry preferences for customer ".getPersonName($customer)." ({$customer->customerID})");
        }
    }

    /**
     * Retrieves the credit card information for the currently authenticated customer.
     *
     * Returns the following structure in the data parameter of the JSON response
     *
     *  {
     *      lastFourCardNumber
     *      expireMonth
     *      expireYear
     *  }
     */
    public function getCreditCard()
    {
        $token = $this->input->get("sessionToken");

        if (!$session = $this->api->validateSessionToken($token)) {
            output_ajax_error_response("Session not authenticated");
        } else {
            $this->load->model('creditCard_model');
            $creditCard = $this->creditCard_model->get_one(array("customer_id" => $session->customer_id, "business_id" => $session->business_id));
            $output = array();
            if (!empty($creditCard)) {
                $output['lastFourCardNumber'] = $creditCard->cardNumber;
                $output['expireMonth'] = $creditCard->expMo;
                $output['expireYear'] = $creditCard->expYr;
                $last_four_of_card_number = substr($creditCard->cardNumber, -4); //Note, only the last 4 digits of the card number are stored in the kiosk table.
            } else {
                $output['lastFourCardNumber'] = null;
                $output['expireMonth'] = null;
                $output['expireYear'] = null;
                $last_four_of_card_number = null;
            }
            $this->load->model("customer_model");
            $customer = $this->customer_model->get_by_primary_key($session->customer_id);
            output_ajax_success_response($output, "Credit card details for '".getPersonName($customer)." ({$customer->customerID})");
        }
    }

    /**
     * Retrieves all the assocaited discounts of the currently logged in customer
     * Expects the following GET parameters:
     *  sessionToken
     * Returns the following JSON stgructure
     *  {
     *      pending_discounts : {
     *          {
     *              customerDiscountID,
     *              amount,
     *              amountType,
     *              frequency,
     *              description,
     *              extendedDesc,
     *              origCreateDate,
     *              updated,
     *              order_id
     *              location_id,
     *              amountApplied,
     *              couponCode,
     *              expireDate,
     *              createdBy,\
     *          },
     *          { ... },
     *          ...
     *      },
     *      applied_discounts : {
     *          {
     *              orderID,
     *              chargeAmount,
     *              chargeType,
     *              customerDiscount_id,
     *              dateCreated
     *          },
     *          { ... },
     *          ...
     *      }
     *  }
     */
    public function discounts()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        } else {
            $customer = new Customer($session->customer_id, FALSE);

            //if this customer has any expired discounts, make them inactive
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->expireCustomerDiscount($customer->business_id, $session->customer_id);

            $discounts = $this->customerdiscount_model->get_pending($session->customer_id);

            foreach ($discounts as $discount) {
                unset($discount->createdBy);
                unset($discount->combine);
                unset($discount->recurring_type_id);
                unset($discount->business_id);
                unset($discount->customer_id);
                unset($discount->active);
                $discountArray['pending_discounts'] = $discount;
            }

            $discountArray['applied_discounts'] = $this->customerdiscount_model->get_applied($session->customer_id);
            echo json_encode($discountArray);
        }
    }

    /**
     * The following action creates a new customer for the specfied business.
     * Expects the following GET parameters
     * email
     * password
     * business_id
     * The following are optional parameters
     * promoCode
     * how
     *
     * Returns the following
     * customer_id
     * sessionToken
     * sessionTokenExpireDate
     * message
     * Creates a new customer for a business
     */
    public function create()
    {
        $data = $this->data;

        $this->load->model('customer_model');
        $this->load->model("business_model");

        $email = trim($this->input->get("email"));
        $password = $this->input->get("password");
        $business_id = $this->input->get("business_id");
        $promocode = coalesce($this->input->get("promoCode"), $this->input->get("promocode"));

        if (empty($email)) {
            $this->api->output_api_error_response("'email' must be passed as a GET parameter and can not be empty.");
        } elseif (empty($password)) {
            $this->api->output_api_error_response("'password' must be passed as a GET parameter and can not be empty");
        } elseif (!is_numeric($business_id)) {
            $this->api->output_api_error_response("'business_id' must be passed as a GET parameter and can not be empty");
        } else {
            $business = $this->business_model->get_by_primary_key($business_id);
            if (empty($business)) {
                $this->api->output_api_error_response("Business ID '$business_id' does not exist");
            } else {
                $existing_customers = $this->customer_model->get_aprax(array("business_id" => $business_id, "email" => $email));
                if (empty($existing_customers)) {
                    $newCustomer = new Customer();
                    $newCustomer->email = $email;
                    $newCustomer->password = $password;
                    $newCustomer->business_id = $business_id;
                    $newCustomer->default_business_language_id = $business->default_business_language_id;
                    $newCustomer->signup_method = $this->client;

                    try {
                        $newCustomer->save();
                    } catch (\App\Libraries\DroplockerObjects\Validation_Exception $e) {
                        send_exception_report($e);
                        $this->api->output_api_error_response("An internal validation error occurred when attempting to save the customer information");
                        return;
                    }

                    $message = "Successfully created new account for {$business->companyName}";
                    //Make the token
                    $expireDate = date("Y-m-d H:i:s", mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y')));
                    $token = md5(uniqid(rand(0, 100000)));
                    $this->db->insert('apiSession', array('customer_id' => $newCustomer->customerID, 'token' => $token, 'expireDate' => $expireDate, 'business_id' => $business_id));

                    $result['customer_id'] = $newCustomer->customerID;
                    $result['sessionToken'] = $token;
                    //Note, we supress a notice error here to workaround to * coding errors.
                    $result['sessionTokenExpireDate'] = @date("Y-m-d H:i:s", $expireDate);
                    if (empty($promocode)) { //If no promotional code was entered, we display the default welcome message.
                        $this->api->output_api_success_response($result, $message);
                    } else {
                        $this->load->library('coupon');
                        $response = $this->coupon->applyCoupon($promocode, $newCustomer->customerID, $business_id);
                        if ($response['status'] == 'fail') {
                            $this->api->output_api_error_response("Customer was created but promotional code was invalid. The customer can try the promo code again in their account section");
                        } else {
                            $this->load->model('coupon_model');
                            $discount = $this->coupon_model->get_discount_value_as_string($promocode, $business_id);
                            $this->api->output_api_success_response($result, "Customer was created and promotional code [$promocode - $discount] was applied to the new account");
                        }
                    }
                } else {
                    $existing_customer = $existing_customers[0];

                    $this->api->output_api_error_response("A customer with the email {$existing_customer->email} is already registered to {$business->companyName}");
                }
            }
        }
    }

    /**
     * Sends a forgot password email to the email address associated with the specified business
     * Expects the following GET parameters
     *  email string
     *  business_id int
     */
    public function sendforgotPasswordEmail()
    {
        $data = $this->data;
        $this->load->model('customer_model');

        if (!isset($data['data']['email'])) {
            $this->api->output_api_error_response("Email is required");
        } elseif (!isset($data['data']['business_id'])) {
            $this->api->output_api_error_response("business_id is required");
        } else {
            $this->load->model("business_model");
            $business_id = $data['data']['business_id'];

            $business = $this->business_model->get_by_primary_key($business_id);
            if (empty($business)) {
                output_ajax_error_response("Could not find business with id '$business_id'");
            } else {
                $email = $data['data']['email'];
                $business_id = $data['data']['business_id'];

                $customer = $this->customer_model->get_one(array("email" => $email, "business_id" => $business_id));

                if (empty($customer)) {
                    $this->api->output_api_error_response("Could not find cutomer with specified email '$email' for business '{$business->companyName}'");
                } else {
                    $this->load->helper("customer_helper");
                    if (send_forgot_password_email($customer)) {
                        $this->api->output_api_success_response(array(), "An email has been sent to {$customer->email} with instructions on how to change your password");
                    } else {
                        $this->api->output_api_error_response("An error ocurred trying to send an email with instructions on how to change your password");
                    }
                }
            }
        }
    }

    /**
     * Updates a customer profile with the specified values and property names
     * Expects the following GET parameters:
     *  sessionToken
     *
     * Can take one of the following GET parameters
     *  data array Can consist of the following key values
     *      firstName
     *      lastName
     *      email
     *      phone
     *      signupDate
     *      autoPay
     *      location_id
     *      sms
     *      type
     *      shirts
     *      address1
     *      address2
     *      city
     *      state
     *      zip
     *      starchOnShirts_id
     *      invoice
     *      noEmail
     *      referredBy
     *      kioskAccess
     *      wfNotes
     *      birthday
     *      customerNotes
     *
     */
    public function updateProfile()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("'sessionToken' must be passed as a GET parameter");
        }
        try {
            $customer = new Customer($session->customer_id, FALSE);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $this->api->output_api_error_response("Customer ID {$session->customer_id} not found");
        }

        $phone_changed = $customer->phone != $this->data['data']['phone'];
        $sms_changed = $customer->sms != $this->data['data']['sms'];
        $copy_sms = get_business_meta($session->business_id, 'copy_phone_to_sms');

        if ($copy_sms && $phone_changed && !$sms_changed) {
            $this->data['data']['sms'] = $this->data['data']['phone'];
        }

        foreach ($this->data['data'] as $key => $value) {
            if ($this->db->field_exists($key, 'customer')) {
                $customer->$key = $value;
            }
        }
        $customer->rules['firstName'] = array("required" => array("userMessage" => "Please enter your first name"));
        $customer->rules['lastName'] = array("required" => array("userMessage" => "Please enter your last name"));
        $customer->rules['phone'] = array("required" => array("userMessage" => "Please enter you phone number"));
        try {

            $customer->save();
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validationException) {
            output_ajax_error_response($validationException->getUserMessage());
        } catch (Exception $customer_save_exception) {
            send_exception_report($customer_save_exception);
            $this->api->output_api_error_response("A server side error occurred attempting to save the customer settings.");
        }
        $this->api->output_api_success_response(array(), "Updated customer");
    }

    /**
     * Updates the customer's billing information
     * Expects the following GET parameters
     *  csc
     *  kiosk_access
     *  expYear
     *  expMonth
     *  cardNumber
     * Returns the following JSON strucgture
     *      {
     *          paidOrders :
     *          unpaidOrders :
     *          status :
     *          order_id :
     *          creditcardID :
     *          refNumber :
     *          message :
     *      }
     */
    public function updateBilling()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Session not authenticated");
        }
        $this->load->model("customer_model");
        if (!$this->input->get("cardNumber")) {
            output_ajax_error_response("'cardNumber' must be passed as a GET parameter");
        }
        if (!$this->input->get("expMonth")) {
            output_ajax_error_response("'expMonth' must be passed as a GET parameter");
        }
        if (!$this->input->get("expYear")) {
            output_ajax_error_response("'expYear' must be passed as a GET parameter");
        }
        if (!$this->input->get("csc")) {
            output_ajax_error_response("'csc' must be passed as a GET parameter");
        }
        $csc = $this->input->get("csc");
        $kiosk_access = $this->input->get("kiosk_access");
        $expirationMonth = $this->input->get("expMonth");
        $expirationYear = $this->input->get("expYear");
        $cardNumber = preg_replace('/[^0-9]/', '', $this->input->get("cardNumber"));
        $this->load->model("customer_model");
        $customer = new Customer($session->customer_id);

        $this->load->model("app_model");
        $app = $this->app_model->get_by_business_id($session->business_id);
        if (isset($app->development_mode) && $app->development_mode == true) {
            $development_mode = true;
        } else {
            $development_mode = false;
        }

        $this->load->library("creditcard");
        $authorizationResult = $this->creditcard->authorize($customer->customerID, $kiosk_access, $cardNumber, $expirationMonth, $expirationYear, $customer->firstName, $customer->lastName, $customer->address1, $customer->city, $customer->state, $customer->zip, null, $csc, $development_mode);
        $paidMessage = "";
        $errorMessage = "";
        if ($authorizationResult['status'] == 'success') {
            if (sizeof($authorizationResult['data']['captureResults']) > 0) {
                foreach ($authorizationResult['data']['captureResults'] as $captureResult) {
                    if ($captureResult['status'] == 'success') {
                        $paidMessage .= "{$captureResult['order_id']} ";
                    } else {
                        $errorMessage .= "{$captureResult['order_id']} ";
                    }
                }
            }
            if (empty($paidMessage)) {
                $captureResult['paidOrders'] = null;
            } else {
                $captureResult['paidOrders'] = "The following orders have been paid for: $paidMessage";
            }
            if (empty($errorMessage)) {
                $captureResult['unpaidOrders'] = null;
            } else {
                $captureResult['unpaidOrders'] = "The following order still need to be paid for: $errorMessage";
            }
        }
        echo json_encode($authorizationResult);
    }

    /**
     * Expects the session token as a GET parameter.
     *
     * Returns the following JSON structure
     *  {
     *      message
     *  }
     */
    public function updateLaundryPreferences()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }
        $this->load->model('customer_model');
        $where['business_id'] = $session->business_id;
        $where['customerID'] = $session->customer_id;
        if (!isset($this->data['data']['preferences'])) {
            $this->api->output_api_error_response("'preferences' must be passed as a GET parameter");
        } else {

            if (isset($this->data['data']['notes'])) {
                $this->customer_model->update_customerNotes($session->customer_id, $this->data['data']['notes']);
            }

            $this->load->model('customerpreference_model');

            $preferences = json_decode($this->data['data']['preferences'], true);
            $this->customerpreference_model->save_preferences($session->customer_id, $preferences);

            $result['message'] = "Updated Customer Preferences";
            $this->api->response($result);
        }
    }

    /**
     * Expects the following GET parameters
     *  productCategory_slug THe wash and fold preference categoiry sliug
     *  productID The product ID of the wash and fold product
     * Returns the following as a JSON structure
     *  message
     *  status
     */
    public function updateLaundryPreference()
    {
        $token = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        try {
            $customer = new Customer($session->customer_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            return $this->api->output_api_error_response("Customer ID {$session->customer_id}");
        }

        if (!$this->input->get("productCategory_slug")) {
            return $this->api->output_api_error_response("'productCategory_slug' must be passed as a GET parameter");
        }

        if (!$this->input->get("productID")) {
            return $this->api->output_api_error_response("'productID' must be passed as a GET parameter");
        }

        $incoming_productCategory_slug = $this->input->get("productCategory_slug");
        $incoming_productID = $this->input->get("productID");

        // check the product exists
        $this->load->model("product_model");
        $product = $this->product_model->get_by_primary_key($incoming_productID);
        if (empty($product)) {
            return $this->api->output_api_error_response("product ID '$incoming_productID' does not exist");
        }

        $this->load->model("productCategory_model");
        $productCategory = $this->productCategory_model->get_by_primary_key($product->productCategory_id);

        // get the customer preferences
        $this->load->model('customerpreference_model');
        $preferences = $this->customerpreference_model->get_preferences($session->customer_id);

        // save the new value
        $preferences[$productCategory->slug] = $incoming_productID;
        $this->customerpreference_model->save_preferences($session->customer_id, $preferences);

        $this->api->output_api_success_response(array(), "Updated preference to product '{$product->name}'");
    }

    /**
     * Retrieves all items associated with the currently logged in customer
     * Expects the following GET parameter:
     *  sessionToken
     * Can take the following GET parameters
     *
     * Returns the following structure in the 'data' parameter of the JSON response
     */
    public function get_closet_items()
    {
        $width = 300;
        $height = 300;
        $ac = 1;

        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        } else {

            $this->load->model('customer_model');
            $customer = $this->customer_model->get_by_primary_key($session->customer_id);

            $get_closet_items_query = $this->db->query("SELECT itemID, displayName, product.name, file, (select barcode from barcode where item_id = itemID limit 1) as barcode
            FROM item
            INNER JOIN customer_item ON customer_item.item_id = itemID
            INNER JOIN product_process ON product_processID = item.product_process_id
            INNER JOIN product ON productID = product_process.product_id
            LEFT JOIN picture ON picture.item_id = itemID
            WHERE
                customer_item.customer_id = ?
                AND item.business_id = ?;
           ", array($customer->customerID, $customer->business_id));

            $get_closet_items_query_result = $get_closet_items_query->result();
            $closet = array();

            if (!empty($this->data['data']['imageWidth'])) {
                $width = $this->data['data']['imageWidth'];
            }

            if (!empty($this->data['data']['imageHeight'])) {
                $height = $this->data['data']['imageHeight'];
            }

            if (!empty($this->data['data']['autoCrop'])) {
                $ac = ($this->data['data']['autoCrop'] == true) ? 1 : 0;
            }

            foreach ($get_closet_items_query_result as $closet_item) {
                $file = "https://droplocker.com/picture/resize?w={$width}&h={$height}&ac={$ac}&src=https://s3.amazonaws.com/LaundryLocker/{$closet_item->file}";
                $closet[$closet_item->displayName][$closet_item->itemID] = array('itemID' => $closet_item->itemID, 'file' => $file, 'barcode' => $closet_item->barcode);
            }

            $this->api->output_api_success_response($closet);
        }
    }

    /**
     * Retrieves the claims for the current customer's session
     * Expects the folloiwng parameters:
     *  signature: The expected request signature
     *  token: The API token
     *  sessionToken: The customer session token
     * Returns the claims for the customers in the following format:
     *  data: array
     *    claims: an array that contains all the claims for the customer in the following format:
     *    [0] => {
     *      address
     *      dateCreated
     *      lockerName
     *    },
     *    [1] => {
     *      address,
     *      dateCreated
     *      lockerName
     *    },
     *    ...
     */
    public function getClaims()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }
        $this->load->model('claim_model');
        /** Begin * */
        $options = array();
        $options['customer_id'] = $session->customer_id;
        $options['business_id'] = $session->business_id;
        $options['with_locker'] = true;
        $options['active'] = 1;
        $options['select'] = "lockerName, address, updated, orderType";
        $claims = $this->claim_model->get($options);
        /** End * */
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($session->customer_id);
        $this->api->output_api_success_response(array("claims" => $claims), "Claims for ".getPersonName($customer));
    }

    /**
     * Gets the orders for the current customer's session
     * Expects the following parameters
     *  signature: The expected request signature
     *  token: The API token
     *  sessionToken: The customer session token
     * Returns the orders for the customer in the folloiwng format:
     *  data: an array that lists all the orders for the customer in the following format:
     *      [0] => {
     *      accessCode
     *      address
     *      dateCreated
     *      locker_id
     *      notes
     *      orderID
     *      orderTotal
     *      orderType
     *      payment
     *      status
     *      },
     *      [1] => {
     *      accessCode
     *      address
     *      dateCreated
     *      locker_id
     *      notes
     *      orderID
     *      orderTotal
     *      orderType
     *      payment
     *      status
     *      },
     *      ...
     *  message:
     *  status:
     */
    public function getOrders()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (empty($sessionToken)) {
            $this->api->output_api_error_response("'sessionToken' must be passed as a GET parmeter");
        } else {
            if (!$session = $this->api->validateSessionToken($sessionToken)) {
                $this->api->responseError("Session not authenticated");
            }
            $this->load->model("order_model");

            try {
                $customer = new Customer($session->customer_id);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
                output_ajax_error_response(("Customer '{$session->customer_id}' not found"));
            }
            $orders = $this->order_model->getCustomerOrderHistory($session->customer_id, $session->business_id, 100, false, false);
            $orders_dunlap = $this->_sanitizeOrders($orders, $customer);

            $this->api->output_api_success_response($orders_dunlap, "Orders for ".getPersonName($customer));
        }
    }

    /**
     * ###################################################
     * BEGIN *
     * ###################################################
     *
     * gets locations and lockers that the customer has selected as drop-off locations
     *
     * ###################################################
     * END *
     * ###################################################
     */
    public function lockers()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }

        $customer = new Customer($session->customer_id);
        $result = $customer->lockers();
        $this->api->response($result);
    }

    /**
     * The following function is unknown *, do not use.
     * @param type $orders
     * @param type $customer
     * @return type
     */
    public function _sanitizeOrders($orders, $customer = '')
    {

        foreach ($orders as $order) {
            $is_unpaid = in_array($order->orderStatusOption_id, array(7, 13));
            if ($is_unpaid) {
                $order->accessCode = '';
                $order->lockerName = '';
            } else {
                $order->accessCode = substr(trim($customer->phone), -4);
            }
        }

        return $orders;
    }

}
