<?php
use App\Libraries\DroplockerObjects\Location;
class Lockers extends MY_Api_Controller
{
    /**
     * Retrieves the lockers in a location
     *
     * Expects the following GET parameters
     *  location_id
     *
     * Returns the following JSON structure
     *  {
     *      {
     *          lockerID
     *          lockerName
     *          lockerLockType
     *          lockerStyle
     *          lockerStatus
     *      },
     *      {...},
     *      ...
     *  }
     */
    public function get()
    {
        if (empty($this->data['data']['location_id'])) {
            output_ajax_error_response("'location_id' must be passed as a GET parameter");
        } else {
            try {
                $location = new Location($this->data['data']['location_id']);
                $this->api->response($location->lockers());
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
                output_ajax_error_response($notFound_exception->getMessage());
            }

        }
    }
}
