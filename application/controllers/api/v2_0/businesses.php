<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\LocationType;
use App\Libraries\DroplockerObjects\Business as BusinessObj;


/**
 * The business controller provides the following functionality:
 *  Retrieving all locations associated with a business
 *  Retrieving the business starch on shirts settings
 *  Retrieving all business properties
 *  Retrieving the business service types
 *  Retrieving the business settings.
 */
class Businesses extends MY_Api_Controller
{
    /**
     * Expects the following GET parameters
     *  tag
     *  business_id
     * @return array
     */
    public function getLanguageViewTranslationsForLanguage()
    {
        $business_id = $this->input->get("business_id");
        if (!is_numeric($business_id)) {
            output_ajax_error_response(("'business_id' must be numeric"));
        }
        $business = $this->business_model->get_by_primary_key($business_id);
        if (empty($business)) {
            output_ajax_error_response(("Business ID '$business_id' does not exist"));
        }
        $tag = $this->input->get("tag");
        if (empty($tag)) {
            output_ajax_error_response("'tag' must be passed as a GET parameter and can not be empty");
        } else {
            $tag = current(explode('-', $tag));
        }
        $this->load->model("language_model");
        $language = $this->language_model->findByTag($tag);
        if (empty($language)) {
            output_ajax_error_response("Language tag '$tag' does not exist");
        }
        $this->load->model("languagekey_business_language_model");
        $iPhoneLanguageViewsAndTranslations = $this->languagekey_business_language_model->getTranslationsForLanguageViewNameForLanguageNameForBusiness( $language->languageID, $business->businessID, array(
            "iPhone/SettingTabs",
            "iPhone/MainMenu",
            "iPhone/AccountSettings",
            "iPhone/AppDelegate",
            "iPhone/ChangeBusiness",
            "iPhone/ItemDetails",
            "iPhone/CreditCard",
            "iPhone/ClosetView",
            "iPhone/ExpirationDate",
            "iPhone/ForgotPassword",
            "iPhone/Locations",
            "iPhone/LaundrySettings",
            "iPhone/LaundryOptions",
            "iPhone/OrderDetails",
            "iPhone/PlaceOrder",
            "iPhone/PlaceOrderViewOptions",
            "iPhone/RecentOrders",
            "iPhone/Registration",
            "iPhone/SignIn",
            "iPhone/ToolbarController"
        ));
        output_ajax_success_response($iPhoneLanguageViewsAndTranslations, "'$business->companyName' translations for '$language->tag - $language->description'");
    }

    /**
     * Retreives properties for a paritcular business
     * Expects the following POST parameters:
        * businessID: The unique ID of the business
     */
    public function get_business_properties()
    {
        if (!$this->input->get("businessID")) {
            $this->api->output_api_error_response("BusinessID must be passed as a GET parameter");
        }
        $businessID = $this->input->get("businessID");

        try {
            $business = new \App\Libraries\DroplockerObjects\Business($businessID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            output_ajax_error_response("Business ID {$businessID} not found");
        }
        $this->load->model("smssettings_model");
        $smsSetting = $this->smssettings_model->get_settings($businessID);
        if ($smsSetting) {
            $sms_provider = $smsSetting->provider;
        } else {
            $sms_provider = "mosio";
        }
        $response = array('companyName' => $business->companyName, 'website_url' => $business->website_url, 'sms_provider' => $sms_provider, "country" => $business->country, "require_card_on_order" => $business->require_card_on_order);
        $this->api->output_api_success_response($response);
    }
    /**
     * The following function retrieves all businesses in the system. A business is considered a 'business' if it has defined service types.
     *
     * The response format is as follows:
     */
    public function get()
    {
        $this->db->select("businessID, companyName, city, state, website_url");
        $this->db->join("businessMeta", "businessMeta.business_id=business.businessID AND businessMeta.key='processor' AND businessMeta.value!=''"); // Only show businesses with a payment processor
        $this->db->where("serviceTypes IS NOT NULL AND show_in_droplocker_app = 1"); //Only show businesses with defined service types and configured to show in the droplocker application
        $this->db->order_by('companyName');
        $businesses = $this->db->get('business')->result();

        output_ajax_success_response($businesses, "All Businesses");

    }


    /**
     * Returns the service types in the following format:
     *  {
        *  [serviceTypeID1] => displayName1
        *  [serviceTypeID2 => displayName2,
        *  ...
     *  }
     */
    public function get_serviceTypes()
    {
        if ($this->input->get('business_id')) {
            $business_id = $this->input->get("business_id");
            $business = $this->business_model->get_by_primary_key($this->input->get("business_id"));
            if (empty($business)) {
                output_ajax_error_response("Business ID '$business_id' not found");
            } else {
                //Note, * stores the service types as a serialized array in the 'serviceTypes' column of the business table.
                $serviceTypes = unserialize($business->serviceTypes);
                asort($serviceTypes);
                $result = array();
                foreach ($serviceTypes as $serviceType) {
                    $result[$serviceType['serviceType_id']] = $serviceType['displayName'];
                }
                output_ajax_success_response($result, "Service types for business '{$business->companyName}'");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }
    }

    /**
     *
     * Note, the following function was written by * and is undocumented, incorrectly named, and has unknown behavior.
     * Note, this function returns the service types, not the modules.
     * @deprecated Use get_service_types()
     */
    public function modules()
    {
        if (empty($this->data['business_id'])) {
            $this->api->responseError('Missing business_id');
        }

        $business = $this->db->get_where('business', array('businessID'=>$this->data['business_id']))->row();

        if ($business) {
            $allModules = array();
            foreach (unserialize($business->serviceTypes) as $module_id => $moduleAttributeArray) {
                $modules['module_id'] = $module_id;
                foreach ($moduleAttributeArray as $attributeKey => $attributeValue) {
                    $modules[$attributeKey] = $attributeValue;
                }

                $allModules[] = $modules;
            }

            $result['modules'] = $allModules;
        } else {
           $this->api->responseError("Business modules not found");
        }
        $this->api->response($result);

    }

    /**
     * Retrieves all active locations for a business ordered by address. The business is derived from the user's session token.
     * Expects the following parameters:
     *  sessionToken
     * data: consits of the location in the following format
        * [0] =>
     *      locationID
     *      address
     *      city
     *      state
     *      zip
     *
        * [1] => [location properties]
        * ...
        * [n] => [location properties]
     */
    public function getLocations()
    {
        $CI = get_instance();
        $CI->load->model("location_model");
        $this->load->model("apisession_model");

        if (!isset($this->data['data']['sessionToken'])) {
            $this->api->output_api_error_response("'sessionToken' must be assed as a GET parameter");
        } else {
            $apisession  = $this->apisession_model->get_one(array("token" => $this->data['data']['sessionToken']));
            if (empty($apisession)) {
                $this->api->output_api_error_response("session token {$this->data['data']['sessionToken']} not found");
            } else {
                $locations = $this->db->query("select location.locationID, location.address, location.city, location.state,location.zipcode FROM location
                    INNER JOIN locker ON locker.location_id=location.locationID AND locker.lockerName != 'InProcess' AND locker.lockerstyle_id NOT IN (7,9)
                    WHERE location.business_id=? AND location.status=?
                    GROUP BY location.locationID ORDER BY location.address", array($apisession->business_id, "active"))->result();
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($apisession->business_id);
                $this->api->output_api_success_response($locations, "Locations for business {$business->companyName}");
            }
        }
    }



    /**
     * Returns the avialable starch on shirt options for a business, or returns an empty datga set if the business does not have starch options enabled.
     * The response is in the following format
     * {
     *  status : The value shall be 'success'/'error
     *  message : If the business does not have starch on shirts enabled, this will be empty. Otherwise, it will contain an array of starch on shirts options.
     *  data :
     * }
     */
    public function getStarchOnShirts()
    {
        if ($this->input->get("business_id")) {
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($this->input->get("business_id"));
            $starchOnShirts = array();
            if (get_business_meta($business->businessID, "displayStarchOptions")) {
                $this->load->model("starchonshirts_model");
                $starchOnShirts = $this->starchonshirts_model->get();
            }
            $this->api->output_api_success_response($starchOnShirts, "Starch on shirt preferences for business");
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }
    }

    /**
     * Retrieves the available mdoules and associated products for a particular business.
     * Expets 'business_id' as a GET parameter
     * The return format is unknown.
     */
    public function settings()
    {
        $this->load->model("product_model");
        $business_id = $this->input->get("business_id");
        if (!is_numeric($business_id)) {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        } else {
            $business_id = $this->input->get("business_id");
            $this->load->model("business_model");
            $business = new \App\Libraries\DroplockerObjects\Business($business_id);
            $tag = $this->input->get("tag");

            if ($tag) {
                $tag = current(explode('-', $tag));
            }

            $this->load->library('productslibrary');
            $modules_and_associated_products = $this->productslibrary->getBusinessPreferencesForAPI($business_id, $tag);

            output_ajax_success_response($modules_and_associated_products, "Modules and associated product categories and associated products for {$business->companyName}");

        }
    }

}
