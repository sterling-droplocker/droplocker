<?php
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker as lockerObj;

class Locker extends MY_Api_Controller
{
    public function get()
    {
        if (empty($this->data['data']['location_id'])) {
            output_ajax_error_response("Missing location_id");
        }

        $location = new Location($this->data['data']['location_id']);
        $this->api->response($location->lockers());
    }



}
