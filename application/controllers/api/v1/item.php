<?php
use App\Libraries\DroplockerObjects\Item as ItemObj;
use App\Libraries\DroplockerObjects\Picture;


class Item extends MY_Api_Controller
{
    /**
     * ##########################################
     * BEGIN
     * ##########################################
     *
     * gets details about an item
     *
     * ##########################################
     * END
     * ##########################################
     */
    public function details()
    {
        if (empty($this->data['data']['item_id'])) {
            $this->api->responseError("Missing item_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        $item = new ItemObj($this->data['data']['item_id'], FALSE);
        $item->width = 300;
        $item->height = 300;
        $item->ac = 1;
        if (!empty($this->data['data']['imageWidth'])) {
            $item->width = $this->data['data']['imageWidth'];
        }

        if (!empty($this->data['data']['imageHeight'])) {
            $item->height = $this->data['data']['imageHeight'];
        }

        if (!empty($this->data['data']['autoCrop'])) {
            $item->ac = ($this->data['data']['autoCrop']==true)?1:0;
        }


        $this->api->response($this->_sanitizeItem($item, $session->customer_id));
    }

    /**
     * The following function is * filth.
     */
    private function _sanitizeItem($item, $customer_id)
    {
        $sql = "SELECT barcode, product.name as productName, process.name as processName FROM product_process
                INNER JOIN product ON productID = product_id
                INNER JOIN process ON process_id = processID
                INNER JOIN barcode ON item_id = {$item->itemID}
                WHERE product_processID = ".$item->product_process_id;
        $query = $this->db->query($sql);

        $product = $query->row();
        $picture = Picture::search_aprax(array('item_id'=>$item->itemID), FALSE);
        if ($picture) {
            $picture = "https://droplocker.com/picture/resize?w={$item->width}&h={$item->height}&ac={$item->ac}&src=https://s3.amazonaws.com/LaundryLocker/".$picture[0]->file;
        } else {
            $picture = "";
        }

        $results['item']['itemID'] = $item->itemID;
        $results['item']['color'] = $item->color;
        $results['item']['size'] = $item->size;
        $results['item']['manufacturer'] = $item->manufacturer;
        $results['item']['note'] = $item->note;
        if ($item->updated instanceOf \DateTime) {
            $results['item']['updated'] = $item->updated->format("Y-m-d H:m:s");
        } else {
            $results['item']['updated'] = null;
        }
        $results['item']['product'] = $product->productName;
        $results['item']['process'] = $product->processName;
        $results['item']['picture'] = $picture;
        $results['item']['barcode'] = $product->barcode;

        // get the previous orders this item was in

        $sql = "SELECT orderID, dateCreated
            FROM orderItem
            INNER JOIN orders ON orders.orderID = orderItem.order_id
            WHERE orderItem.item_id = ?
            AND orders.customer_id = ?
            ORDER BY dateCreated DESC";

        $orders = $this->db->query($sql, array($item->itemID, $customer_id))->result();
        if ($orders) {
            $count = 0;
            foreach ($orders as $order) {
                $results['item']['orders'][$count]['order_id'] = $order->orderID;
                $results['item']['orders'][$count]['orderDate'] = $order->dateCreated;
                $count++;
            }
        }

        return $results;
    }

}
