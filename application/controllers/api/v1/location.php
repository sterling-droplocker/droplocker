<?php
use App\Libraries\DroplockerObjects\Location as locationObj;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\LocationType;
use App\Libraries\DroplockerObjects\Business;

class Location extends MY_Api_Controller
{
    /**
     * Adds a location to a customer account
     * Expects the following additional GET parameters
        * 'location_id'
     */
    public function save()
    {
        if (empty($this->data['data']['location_id'])) {
            $this->api->responseError("Missing location_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        $customer = new Customer($session->customer_id);
        if ($customer->addLocation($this->data['data']['location_id'])) {
            $result['message'] = 'Location has been added to customer';
        } else {
            $result['status'] = 'fail';
            $result['message'] = 'Location already exists for this customer';
        }

        $this->api->response($result);

    }

    /**
     * Removes a location from a customer account
     * Expects the following GET parameters
     *  'location_id'
     */
    public function remove()
    {
        if (empty($this->data['data']['location_id'])) {
            $this->api->responseError("Missing location_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        $customer = new Customer($session->customer_id);
        if (!$customer->removeLocation($this->data['data']['location_id'])) {
             $this->api->responseError("location was not removed from customer account OR the location was not in the customer account");
        }

        $this->api->response(array('message'=>"Location has been removed from customer account"));
    }

    /**
     * serarches for a location based on address string and radius
     * Expects one of the following additional GET parameters
     *  'address'
     *  'geolocation'
     * Returns the following parameters
     *  - locations: a JSON encoded array of locations that match the query
     *  - type: Unknown *
     *  - status: "success", if the request was successful. If an error occurred during the attempt to retrieve a location, then "error" is returned.
     */
    public function search()
    {
       // either address or geolocation must be passed
        if (empty($this->data['data']['address']) && empty($this->data['data']['geoLocation'])) {
            $this->api->responseError("Either address or geoLocation (latitude/longitude) must be provided");
        }

        $business_id = $this->data['data']['business_id'];
        $exclude = array(8); // undefined
        if (get_business_meta($business_id, "hide_home_delivery_locations")) {
            $exclude[] = 4; // home delivery
        }
        $exclude = implode(", ", $exclude);

        $this->load->model('customer_model');
        $this->load->model('location_model');
        $location = $this->location_model->get_aprax(array(
            "address" => $this->data['data']['address'],
            "business_id" => $this->data['data']['business_id'],
            'status' => 'active',
            "locationType_id NOT IN ($exclude)" => NULL,
            'serviceType !=' => "not in service",
            'lat IS NOT NULL' => NULL,
            'lon IS NOT NULL' => NULL
        ));

        /**
         * Search using the old code. No geocode
         */
        $business_id = $this->data['data']['business_id'];
        $kiosks = $this->location_model->get_public_kiosks($business_id);
        $business = new Business($business_id);
        $suffixes = array(" blvd", " street", " avenue", " ave", ".", "st");

        $address = strtolower($this->data['data']['address']);
        $address = str_replace($suffixes, "", $address);
        $address_parts = explode(" ", $address);

        // sql query arguments
        $params = array();

        $locationSql = '';
        if ($this->data['data']['business_id']) {
            $locationSql = 'AND location.business_id = ?';
            $params[] = $this->data['data']['business_id'];
        }

        $search = array();
        foreach ($address_parts as $part) {
            if (strlen($part) > 2 || is_numeric($part)) {
                $search[] = 'location.address LIKE ?';
                $params[] = '%'.trim($part).'%';
            }
        }
        if (!empty($search)) {
            $search = "(". implode(" OR ", $search) .")";
        } else {
            $search = 'location.address LIKE ?';
            $params[] = '%'.trim($address).'%';
        }

        $sql = "SELECT COUNT(lockerID) AS total, location.*, business.companyName as businessName FROM location
            LEFT JOIN locker l ON l.location_id = locationID AND l.lockerStatus_id = 1
            JOIN business ON businessID = business_id
            WHERE location.address != 'None'
                $locationSql
                AND locationType_id NOT IN ($exclude)
                AND $search
            GROUP BY l.location_id
            HAVING total > 0";
        $query = $this->db->query($sql, $params);

        $found_locations = $query->result();

        foreach ($kiosks as $kiosk) {
            if (!array_key_exists($kiosk->locationID, $found_locations)) {
                $found_locations[$kiosk->locationID] = $kiosk;
            }
        }

        $results['type'] = "Name Match";
        foreach ($found_locations as $location) {
            $results['locations'][] = $this->_sanitizeLocations($location);
        }
        $this->api->response($results);
    }

    /**
     * Retrieves a list of locations for a business based on the user's current location.
     * Expects the following additional GET parameters
     *  latitude float The user's current latitude coordinate. Must be in degrees
     *  longitude float The user's current longitude coordinate. Must be in degress
     *  business_id int The business from which to search for locations from
     */
    public function get_by_distance_for_business()
    {
        // either address or geolocation must be passed
        if (empty($this->data['data']['latitude'])) {
            output_ajax_error_response("latitude must be passed as a GET parameter");
        } elseif (empty($this->data['data']['longitude'])) {
            output_ajax_error_response("longitude must be passed as a GET parameter");
        } elseif ($this->input->get("business_id") === false) {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        } else {
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($this->input->get("business_id"));

            $latitude_degrees = $this->input->get("latitude");
            $longitude_degrees = $this->input->get("longitude");
            $this->load->model('location_model');
            $found_public_locations = $this->location_model->get_all_by_distance_for_business($latitude_degrees, $longitude_degrees, 8, $business->businessID, true, 20);
            $results = array("locations" => array());
            if (!empty($found_public_locations)) {

                foreach ($found_public_locations as $public_location) {
                    $results['locations'][] = $this->_sanitizeLocations($public_location);
                }

            }
            $found_private_locations = $this->location_model->get_all_by_distance_for_business($latitude_degrees, $longitude_degrees, .3, $business->businessID, false, 20);
            if (!empty($found_private_locations)) {
                foreach ($found_private_locations as $private_location) {
                    $results['locations'][] = $this->_sanitizeLocations($private_location);
                }
            }
            if (empty($results)) {
                    output_ajax_error_response('No Locations found near the search location');
            } else {
                output_ajax_success_response($results, "Locations for '{$business->companyName}'");
            }
        }


    }

    /**
     * Gets the details for a location
     * Expects the following GET parameters
     *  'location_id'
     *  'sessionToken'
     * Returns a JSON structure in the following format
     *
     */
    public function details() {
        if ($this->input->get("location_id") === false) {
            output_ajax_error_response("'location_id' must be passed as a GET parameter");
        }
        if ($this->input->get("sessionToken") === false) {
            output_ajax_error_response("'sessionToken' must be passed as a GET parameter");
        }

        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Session not authenticated");
        }
        $this->load->model("location_model");
        $location_id = $this->input->get("location_id");
        $location = $this->location_model->get_by_primary_key($location_id);

        if (empty($location)) {
            output_ajax_error_response("Location '$location_id' not found");
        }

        output_ajax_success_response($location, "Data properties for location $location->address, $location->city, $location->state, $location->zip ($location->locationID)");
    }

    public function _sanitizeLocations($location)
    {
        $locationType = new LocationType($location->locationType_id);
        $results['location']['location_id'] = $location->locationID;
        $results['location']['business_id'] = $location->business_id;
        $results['location']['companyName'] = $location->companyName;
        $results['location']['address'] = $location->address;
        $results['location']['address2'] = $location->address2;
        $results['location']['city'] = $location->city;
        $results['location']['state'] = $location->state;
        $results['location']['zipcode'] = $location->zipcode;
        $results['location']['lat'] = $location->lat;
        $results['location']['lng'] = $location->lon;
        $results['location']['serviceType'] = $location->serviceType;
        $results['location']['public'] = (string)$location->public;
        $results['location']['locationType'] = $locationType->name;

        return $results;
    }



}
