<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\ApiSession;
use App\Libraries\DroplockerObjects\KioskAccess;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\OrderPaymentStatus;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\LocationType;

/**
 *
 */
class Customers extends MY_Api_Controller
{
    /**
     * Logs a customer into the system
     *
     * Expects the following GET parameters
     *  email
     *  password
     *
     * Can take the following GET parameter
     *  tag BNCP 74 formatted language tag to set the customer's preferred language
     *
     * Returns the following JSON structure
       {
           status
           message
           customer_id
           sessionToken
           sessionTokenExpireDate
       }
     */
    public function validate()
    {
        $data = $this->data;
        $this->secretKey = $data['secretKey'];
        unset($data['secretKey']);

        $business_id = $this->input->get("business_id");
        $username = $this->input->get("username");
        $password = $this->input->get("password");
        $sessionTokenExpireDays = $this->input->get("sessionTokenExpireDays");
        $tag = $this->input->get("tag");
        
        if (empty($business_id)) {
            output_ajax_error_response($this->getRequiredParamMessage('business_id'));
        }

        if (empty($username)) {
            output_ajax_error_response($this->getRequiredParamMessage('username'));
        }

        if (empty($password)) {
            output_ajax_error_response($this->getRequiredParamMessage('password'));
        }

        if (!$this->input->get("hashedPass")) {
            $password = md5($password);
        }

        $this->load->model('customer_model');
        $loggedCustomer = $this->customer_model->login($username, $password, $business_id);

        if (!$loggedCustomer) {
            output_ajax_error_response(get_translation("customers_validate_wrongCredentials","mobile/api", array(), "Wrong username or password", null, $this->input->get("business_languageID")));
        }

        $plusDays = intval($sessionTokenExpireDays ? $sessionTokenExpireDays : 1);
        $expireDate = gmdate('Y-m-d H:i:s', strtotime("+$plusDays days"));

        $data = $this->setupCustomerSession($business_id, $loggedCustomer->customerID, $expireDate);

        // If there is a preffered language sent
        if ($tag) {
            $this->load->model("business_language_model");
            $business_language = $this->business_language_model->findByTag($tag, $business_id);

            if (!empty($business_language)) {
                try {
                    $customer = new Customer($loggedCustomer->customerID);
                    $customer->default_business_language_id = $business_language->business_languageID;
                    $customer->save();
                } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validation_exception) {
                    send_exception_report($validation_exception);
                }
            }
        }

        output_ajax_success_response($data, get_translation("customers_validate_success","mobile/api", array(), 'Customer logged in', null, $this->input->get("business_languageID")));
    }

    /**
     * Expects the following GET parameters:
     *  business_id
     *  facebook_id
     *
     * Can optionally take the following GET parameters:
     *  email
     *  firstName
     *  lastName
     */
    public function facebook_login()
    {
        $business_id = $this->input->get("business_id");
        $facebook_id = $this->input->get('facebook_id');
        if (empty($business_id)) {
            output_ajax_error_response($this->getRequiredParamMessage('business_id'));
        }

        if (empty($facebook_id)) {
            output_ajax_error_response($this->getRequiredParamMessage('facebook_id'));
        }

        $business = new Business($business_id);

        // create a FacebookLogin object
        $this->load->library('facebookloginlibrary', array(
            'appId' => $business->facebook_api_key,
            'secret' => $business->facebook_secret_key,
            'business_id' => $business->businessID,
        ));

        $customerData = array();

        $fields = array(
            'email' => 'email',
            'firstName' => 'first_name',
            'lastName' => 'last_name',
        );
        foreach ($fields as $field => $key) {
            if ($this->input->get($field)) {
                $customerData[$key] = $this->input->get($field);
            }
        }

        $result = $this->facebookloginlibrary->loginOrCreateCustomerWithFacebookId($facebook_id, $customerData);
        if ($result['status'] != 'success') {
            output_ajax_error_response($result['message']);
        }

        $session = $this->setupCustomerSession($business_id, $result['data']['customer']['customerID']);

        $result['data']['sessionToken'] = $session['sessionToken'];
        $result['data']['sessionTokenExpireDate'] = $session['sessionTokenExpireDate'];

        output_ajax_success_response($result['data'], $result['message']);
    }

    /**
     * Creates or renews a customer api session
     *
     * @param int $business_id
     * @param int $customer_id
     * @param string $expireDate
     * @return array
     */
    protected function setupCustomerSession($business_id, $customer_id, $expireDate = null)
    {
        // default expires in 24hs
        if (empty($expireDate)) {
            $expireDate = gmdate('Y-m-d H:i:s', time() + 86400);
        }

        $this->load->model('apisession_model');

        $data = array(
            'token' => md5(uniqid(rand(0, 100000))),
            'expireDate' => $expireDate,
            'business_id' => $business_id,
            'customer_id' => $customer_id,
        );

        $existingSession = $this->apisession_model->get_one(array(
            'customer_id' => $customer_id,
            'business_id' => $business_id,

        ));

        if ($existingSession) {
            $data['apiSessionID'] = $existingSession->apiSessionID;
            $data['token'] = $existingSession->token;
        }

        $this->apisession_model->save($data);

        return array(
            'customer_id' => $customer_id,
            'sessionToken' => $data['token'],
            'sessionTokenExpireDate' => $expireDate,
        );
    }

    /**
     * Retrieves the authenticated customer's preferences
     * Returns the following JSON structure
       {
           customerPreference1 : preferenceValue1
           customerPreference2 : preferenceValue2
           ....
       }
     */
    public function laundryPreferences()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }

        $this->load->model('customerpreference_model');
        $preferences = $this->customerpreference_model->get_preferences($session->customer_id);

        $customer = new Customer($session->customer_id, FALSE);
        $results['preferences'] = $preferences;
        $results['notes'] = $customer->customerNotes;
        $this->api->response($results);
    }

    /**
     * Retreives the properties and associated entities of a customer
     *
     * Returns a JSON structure in the following format:
     {
        creditCard : {
            expireMonth
            expireYear
            lastFourCardNumber
       },
       customerDetails = {
            w9
            address1
            address2
            autoPay
            birthday
            bouncedDate
            business_id
            city
            comission_rate
            cusotmerID
            customerNotes
            dcNotes
            defaultWF
            email
            emailWindowStart
            emailWindowStop
            facebook_id
            firstName
            hold
            how
            inactive
            internalNotes
            invoice
            ip
            kioskAccess
            lastName
            lastUpdated
            lat
            linkedLocation_id
            location_id
            lockerLootTerms
            lon
            mgrAgreemnt
            noEmail
            noUpcharge
            owner_id
            password
            pendingEmailUpdate
            permAssemblyNotes
            phone
            preferences
            referredBy
            sex
            shirts
            signupDate
            sms
            startchOnShirts_id
            state
            twitterId
            type
            username
            wfNotes
            zip
       },
       customerPreferences : {
            preferences
       }
       kiosk_access : Whether or not the customer has kiosk access for the card. Note, if the business does not have kiosk access enabled, the this parameter is absent from the response.
       status
      }
     */
    public function details()
    {//Warning, most of the following function still uses code written by *.
        $sessionToken = $this->input->get("sessionToken");
        if (!$this->input->get("sessionToken")) {
            output_ajax_error_response($this->getRequiredParamMessage('sessionToken'));
        } else if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Sessiontoken not found");
        } else if (!is_numeric($session->customer_id)) {
            output_ajax_error_response("Numeric customer_id not found in session");
        } else {
            $this->load->model("customer_model");
            $customer = $this->customer_model->get_by_primary_key($session->customer_id);

            $this->load->library('productslibrary');
            $customer->customerPreferences = $this->productslibrary->getCustomerPreferencesForAPI($customer->customerID);

            $this->load->model('creditCard_model');
            $creditCard = $this->creditCard_model->get_one(array("customer_id" => $customer->customerID));

            if (!empty($creditCard)) {
                $customer->creditCard['lastFourCardNumber'] = $creditCard->cardNumber;
                $customer->creditCard['expireMonth'] = $creditCard->expMo;
                $customer->creditCard['expireYear'] = $creditCard->expYr;
                $last_four_of_card_number = substr($creditCard->cardNumber, -4); //Note, only the last 4 digits of the card number are stored in the kiosk table.
            } else {
                $customer->creditCard['lastFourCardNumber'] = null;
                $customer->creditCard['expireMonth'] = null;
                $customer->creditCard['expireYear'] = null;
                $last_four_of_card_number = null;
            }

            $business = $this->business_model->get_by_primary_key($customer->business_id);
            if ($business->store_access == 1) {
                $this->load->model("kioskAccess_model");
                $customer->kiosk_access = $this->kioskAccess_model->has_kiosk_access($last_four_of_card_number, $customer->customerID, $customer->business_id);
            }

            $this->load->model('customer_location_model');
            $customer->locations = $this->customer_location_model->get_customer_locations($customer->customerID, $customer->business_id);
            foreach($customer->locations as $key => $custLocation){
                $customer->locations[$key] = $this->_sanitizeLocation($custLocation);
            }

            $this->load->model("order_model");
            if(!$customer->location_id){
                $lastOrders = $this->order_model->getCustomerOrderHistory($session->customer_id, $session->business_id, 1, true, false, false);
                if(count($lastOrders)){
                    $customer->location_id = $lastOrders[0]->locationID;
                }
            }

            if($customer->location_id){
                $defaultLocation = new Location($customer->location_id);
                $defaultLocation = $this->_sanitizeLocation($defaultLocation);

                $customer->defaultLocation = $defaultLocation->to_array();
            } else {
                $customer->defaultLocation = null;
            }
            
            //$customer->hasOrdersOnPaymentHold = $this->customer_model->IsOnPaymentHold($session->customer_id);

            $details['customerDetails'] = $customer;

            output_ajax_success_response($details, "Properties, creditcard, and laundry preferences for customer ".getPersonName($customer)." ({$customer->customerID})");
        }
    }

    /**
     * Retrieves the credit card information for the currently authenticated customer.
     *
     * Returns the following structure in the data parameter of the JSON response
     *
        {
            status,
            message,
            data : {
                lastFourCardNumber.
                expireMonth,
                expireYear
            }
        }
     */
    public function getCreditCard()
    {
        $token = $this->input->get("sessionToken");

        if (!$session = $this->api->validateSessionToken($token)) {
            output_ajax_error_response("Session not authenticated");
        } else {
            $this->load->model('creditCard_model');
            $creditCard = $this->creditCard_model->get_one(array("customer_id" => $session->customer_id, "business_id" => $session->business_id));
            $output = array();
            if (!empty($creditCard)) {
                $output['lastFourCardNumber'] = $creditCard->cardNumber;
                $output['expireMonth'] = $creditCard->expMo;
                $output['expireYear'] = $creditCard->expYr;
                $output['type'] = $creditCard->cardType;
                $last_four_of_card_number = substr($creditCard->cardNumber, -4); //Note, only the last 4 digits of the card number are stored in the kiosk table.
            } else {
                $output['lastFourCardNumber'] = null;
                $output['expireMonth'] = null;
                $output['expireYear'] = null;
                $last_four_of_card_number = null;
            }
            $this->load->model("customer_model");
            $customer = $this->customer_model->get_by_primary_key($session->customer_id);
            output_ajax_success_response($output, "Credit card details for '".getPersonName($customer)." ({$customer->customerID})");
        }
    }

    public function deleteCreditCard()
    {
        $session = $this->getSession();
        $customer = new Customer($session->customer_id);
        $customer_id = $customer->customerID;
        $business_id =  $customer->business_id;

        $creditCard = CreditCard::search(array(
            "customer_id" => $customer_id
        ));

        if ($creditCard) {
            $processor = get_business_meta($business_id, 'processor');
            //this is required for the Transbank software audit, but we can remove it once approved.
            if ($processor == 'transbank') {
                $transbank = new \DropLocker\Processor\Transbank(array('business_id' => $business_id));
                if ($transbank->removeUser($creditCard)) {
                    $creditCard->delete();
                    $customer->autoPay = 0;
                    $customer->save();
                    output_ajax_success_response(array(), 'Credit card removed');
                } else {
                    output_ajax_error_response('Error when trying to remove credit card');
                }
            } else {
                $creditCard->delete();
                $customer->autoPay = 0;
                $customer->save();
                output_ajax_success_response(array(), get_translation("success", "business_account/profile_billing", array('cardNumber' => $creditCard->cardNumber), "Deleted card ending with %cardNumber%", $customer_id));
            }
        } else {
            output_ajax_error_response(get_translation("no_cc_on_file", "business_account/profile_billing", array(), "You have no credit cards on file", $customer_id));
        }
    }

    /**
     * Initializes coinbase oauth
     */
    public function initiate_coinbase()
    {
        $session = $this->getSession();
        $business_id =  $session->business_id;

        $params = $this->requiredParameters(array(
            'successCallback',
            'cancelCallback',
        ));

        $this->load->library('creditcardprocessor');
        $coinbase = $this->creditcardprocessor->factory('coinbase', $business_id);

        $response = $coinbase->oauth($params['successCallback']);
        if ($response['status'] != 'success') {
            output_ajax_error_response(
                'Problem while initiating bitcoin checkout: ' . $response['message']
            );
        }

        output_ajax_success_response(array("checkout_url" => $response['url']), "Bitcoin URL obtained");
    }

    /**
     * Retrieves all the associated discounts of the currently logged in customer
     * Expects the following GET parameters:
     *  sessionToken
     * Returns the following JSON stgructure
       {
           pending_discounts : {
               {
                   customerDiscountID,
                   amount,
                   amountType,
                   frequency,
                   description,
                   extendedDesc,
                   origCreateDate,
                   updated,
                   order_id
                   location_id,
                   amountApplied,
                   couponCode,
                   expireDate,
                   createdBy,\
               },
               { ... },
               ...
           },
           applied_discounts : {
               {
                   orderID,
                   chargeAmount,
                   chargeType,
                   customerDiscount_id,
                   dateCreated
               },
               { ... },
               ...
           }
       }
     */
    public function discounts()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        } else {
            $customer = new Customer($session->customer_id, FALSE);

            //if this customer has any expired discounts, make them inactive
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->expireCustomerDiscount($customer->business_id, $session->customer_id);

            $discounts = $this->customerdiscount_model->get_pending($session->customer_id);

            foreach ($discounts as $discount) {
                unset($discount->createdBy);
                unset($discount->combine);
                unset($discount->recurring_type_id);
                unset($discount->business_id);
                unset($discount->customer_id);
                unset($discount->active);
                $discountArray['pending_discounts'] = $discount;
            }

            $discountArray['applied_discounts'] = $this->customerdiscount_model->get_applied($session->customer_id);
            echo json_encode($discountArray);
        }
    }

    /**
     * The following action creates a new customer for the specfied business.
     * Expects the following GET parameters
     * email
     * password
     * business_id
     * The following are optional parameters
     * promoCode
     * how
     *
     * Returns the following data structure
        {
            status
            message
            data : {
                customer_id
                sessionToken
                sessionTokenExpireDate
                message
                status
            }
        }
     */
    public function create()
    {
        $data = $this->data;

        $this->load->model('customer_model');
        $this->load->model("business_model");

        $email = trim($this->input->get("email"));
        $password = $this->input->get("password");
        $business_id = $this->input->get("business_id");
        $promocode = coalesce($this->input->get("promoCode"), $this->input->get("promocode"));

        if (empty($email)) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('email'));
        } elseif (empty($password)) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('password'));
        } elseif (!is_numeric($business_id)) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('business_id'));
        } else {
            $business = $this->business_model->get_by_primary_key($business_id);
            if (empty($business)) {
                $this->api->output_api_error_response("Business ID '$business_id' does not exist");
            } else {
                $existing_customers = $this->customer_model->get_aprax(array("business_id" => $business_id, "email" => $email));
                if (empty($existing_customers)) {
                    $default_business_language_id = $this->input->get("business_languageID");
                    if (!$default_business_language_id) {
                        $default_business_language_id = $business->default_business_language_id;
                    }
                    
                    $newCustomer = new Customer();
                    $newCustomer->email = $email;
                    $newCustomer->password = $password;
                    $newCustomer->business_id = $business_id;
                    $newCustomer->default_business_language_id = $default_business_language_id;
                    $newCustomer->signup_method = $this->client;

                    try {
                        $newCustomer->save();
                    } catch (\App\Libraries\DroplockerObjects\Validation_Exception $e) {
                        send_exception_report($e);
                        $this->api->output_api_error_response("An internal validation error occurred when attempting to save the customer information");
                        return;
                    }

                    $message = "Successfully created new account for {$business->companyName}";
                    //Make the token
                    $expireDate = date("Y-m-d H:i:s", mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y')));
                    $token = md5(uniqid(rand(0, 100000)));
                    $this->db->insert('apiSession', array('customer_id' => $newCustomer->customerID, 'token' => $token, 'expireDate' => $expireDate, 'business_id' => $business_id));

                    $result['customer_id'] = $newCustomer->customerID;
                    $result['sessionToken'] = $token;
                    //Note, we supress a notice error here to workaround to * coding errors.
                    $result['sessionTokenExpireDate'] = @date("Y-m-d H:i:s", $expireDate);
                    if (empty($promocode)) { //If no promotional code was entered, we display the default welcome message.
                        $this->api->output_api_success_response($result, $message);
                    } else {
                        $this->load->library('coupon');
                        $response = $this->coupon->applyCoupon($promocode, $newCustomer->customerID, $business_id);
                        if ($response['status'] == 'fail') {
                            $this->api->output_api_error_response("Customer was created but promotional code was invalid. The customer can try the promo code again in their account section");
                        } else {
                            $this->load->model('coupon_model');
                            $discount = $this->coupon_model->get_discount_value_as_string($promocode, $business_id);
                            $this->api->output_api_success_response($result, "Customer was created and promotional code [$promocode - $discount] was applied to the new account");
                        }
                    }
                } else {
                    $existing_customer = $existing_customers[0];

                    $this->api->output_api_error_response("A customer with the email {$existing_customer->email} is already registered to {$business->companyName}");
                }
            }
        }
    }

    /**
     * Expects the following GET parameters:
     *  sessionToken
     *  promoCode
     * Returns the following JSON stgructure
        {
            status
            message
            data: {
                promoCode: string,
                discount: string,
            }
        }
     */
    public function addCoupon()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Session not authenticated");
        }

        $customer = new Customer($session->customer_id, FALSE);

        $promocode = coalesce($this->input->get("promoCode"), $this->input->get("promocode"));
        if (empty($promocode)) {
            output_ajax_error_response($this->getRequiredParamMessage('promoCode'));
        }

        $this->load->library('coupon');
        $response = $this->coupon->applyCoupon($promocode, $customer->customerID, $customer->business_id);
        if ($response['status'] == 'fail') {
            output_ajax_error_response(get_translation("Promotional code is invalid", $this->translation_domain, array('error' => $response['error']), "Promotional code is invalid: %error%", null, $this->input->get("business_languageID")));
        }

        $this->load->model('coupon_model');
        $discount = $this->coupon_model->get_discount_value_as_string($promocode, $customer->business_id);
        $result = array(
            'promoCode' => $promocode,
            'discount' => $discount,
        );
        output_ajax_success_response($result, "Promotional code [$promocode - $discount] was applied to your account");
    }

    /**
     * Sends a forgot password email to the email address associated with the specified business
     * Expects the following GET parameters
     *  email string
     *  business_id int
     * Returns a JSON structure in thef following format
     {
        status
        data
        message
     }
     */
    public function sendforgotPasswordEmail()
    {
        $data = $this->data;
        $this->load->model('customer_model');

        if (!isset($data['data']['email'])) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('email'));
        } elseif (!isset($data['data']['business_id'])) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('business_id'));
        } else {
            $this->load->model("business_model");
            $business_id = $data['data']['business_id'];

            $business = $this->business_model->get_by_primary_key($business_id);
            if (empty($business)) {
                output_ajax_error_response(get_translation("BusinessNotFoundMessage", $this->translation_domain, array("business_id" => $business_id), "Could not find business with id '%business_id%'", null, $this->input->get("business_languageID")));
            } else {
                $email = $data['data']['email'];
                $business_id = $data['data']['business_id'];

                $customer = $this->customer_model->get_one(array("email" => $email, "business_id" => $business_id));

                if (empty($customer)) {
                    $this->api->output_api_error_response(get_translation("noCustomerErrorMessage", "iPhone/ForgotPassword", array("email" => $email, "businessName" => $business->companyName), "Could not find customer with specified email '%email%' for business '%businessName%'", null, $this->input->get("business_languageID")));
                } else {
                    $this->load->helper("customer_helper");
                    if (send_forgot_password_email($customer)) {
                        $this->api->output_api_success_response(array(), get_translation("successMessage", "iPhone/ForgotPassword", array("email" => $customer->email), "An email has been sent to %email% with instructions on how to change your password", $customer->customerID));
                    } else {
                        $this->api->output_api_error_response(get_translation("internalErrorMessage", "iPhone/ForgotPassword", array(), "An error occurred trying to send an email with instructions on how to change your password", $customer->customerID));
                    }
                }
            }
        }
    }

    /**
     * Updates a customer profile with the specified values and property names.
     * Expects the following GET parameters:
     *  sessionToken
     *
     * Can take one of the following GET parameters
     *  data array Can consist of the following key values
     *      firstName
     *      lastName
     *      email
     *      phone
     *      signupDate
     *      autoPay
     *      location_id
     *      sms
     *      type
     *      shirts
     *      address1
     *      address2
     *      city
     *      state
     *      zip
     *      starchOnShirts_id
     *      invoice
     *      noEmail
     *      referredBy
     *      kioskAccess
     *      wfNotes
     *      birthday
     *      customerNotes
     * Returns the following JSON stucture:
     {
        status
        message
     }
     */
    public function updateProfile()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response($this->getRequiredParamMessage('sessionToken'));
        }
        try {
            $customer = new Customer($session->customer_id, FALSE);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            $this->api->output_api_error_response("Customer ID {$session->customer_id} not found");
        }

        $phone_changed = $customer->phone != $this->input->get('phone');
        $sms_changed = $customer->sms != $this->input->get('sms');
        $copy_sms = get_business_meta($session->business_id, 'copy_phone_to_sms');

        if ($copy_sms && $phone_changed && !$sms_changed) {
            $this->data['data']['sms'] = $this->data['data']['phone'];
        }

        foreach ($this->data['data'] as $key => $value) {
            if ($this->db->field_exists($key, 'customer')) {
                $customer->$key = $value;
            }
        }

        if ($this->data['data']['address1'] && !($this->data['data']['lat'] && $this->data['data']['lon'])) {
            // geocode new address
            $geo = geocode($customer->address1, $customer->city, $customer->state);
            $customer->lat = $geo['lat'];
            $customer->lon = $geo['lng'];
        }

        $customer->rules['firstName'] = array("required" => array("userMessage" => get_translation("firstNameValidationErrorMessage", "iPhone/AccountSettings", array(), "Please enter your first name", $customer->customerID)));
        $customer->rules['lastName'] = array("required" => array("userMessage" => get_translation("lastNameValidationErrorMessage", "iPhone/AccountSettings", array(), "Please enter your last name", $customer->customerID)));
        $customer->rules['phone'] = array("required" => array("userMessage" => get_translation("phoneValidationErrorMessage", "iPhone/AccountSettings", array(), "Please enter your phone number")));
        try {
            $customer->save();
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validationException) {
            output_ajax_error_response($validationException->getUserMessage());
        } catch (Exception $customer_save_exception) {
            send_exception_report($customer_save_exception);
            $this->api->output_api_error_response("A server side error occurred attempting to save the customer settings.");
        }
        $this->api->output_api_success_response(array(), "Updated customer");
    }

    /**
     * Updates the customer's billing information
     * Expects the following GET parameters
     *  csc
     *  kiosk_access
     *  expYear
     *  expMonth
     *  cardNumber
     * Returns the following JSON structure
        {
            paidOrders :
            unpaidOrders :
            status :
            order_id :
            creditcardID :
            refNumber :
            message :
        }
     */
    public function updateBilling()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            output_ajax_error_response("Session not authenticated");
        }
        $this->load->model("customer_model");
        if (!$this->input->get("cardNumber")) {
            output_ajax_error_response($this->getRequiredParamMessage('cardNumber'));
        }
        if (!$this->input->get("expMonth")) {
            output_ajax_error_response($this->getRequiredParamMessage('expMonth'));
        }
        if (!$this->input->get("expYear")) {
            output_ajax_error_response($this->getRequiredParamMessage('expYear'));
        }
        if (!$this->input->get("csc")) {
            output_ajax_error_response($this->getRequiredParamMessage('csc'));
        }
        $csc = $this->input->get("csc");
        $kiosk_access = $this->input->get("kiosk_access");
        $expirationMonth = $this->input->get("expMonth");
        $expirationYear = $this->input->get("expYear");
        $cardNumber = $this->input->get("cardNumber");
        
        $countryCode = null;

        if($expirationYear < 100){
            $expirationYear += 2000;
        }

        $this->load->model("customer_model");
        $customer = new Customer($session->customer_id);

        $this->load->model("app_model");
        $app = $this->app_model->get_by_business_id($session->business_id);
        if (isset($app->development_mode) && $app->development_mode == true) {
            $development_mode = true;
        } else {
            $development_mode = false;
        }

        $this->load->library("creditcard");
        $authorizationResult = $this->creditcard->authorize(
            $customer->customerID, 
            $kiosk_access, 
            $cardNumber, 
            $expirationMonth, 
            $expirationYear, 
            $this->input->get("firstName"),
            $this->input->get("lastName"),
            $this->input->get("address1"),
            $this->input->get("city"),
            $this->input->get("state"),
            $this->input->get("zip"),
            $countryCode, 
            $csc, 
            $development_mode);
        $paidMessage = "";
        $errorMessage = "";
        if ($authorizationResult['status'] == 'success') {
            if (sizeof($authorizationResult['data']['captureResults']) > 0) {
                foreach ($authorizationResult['data']['captureResults'] as $captureResult) {
                    if ($captureResult['status'] == 'success') {
                        $paidMessage .= "{$captureResult['order_id']} ";
                    } else {
                        $errorMessage .= "{$captureResult['order_id']} ";
                    }
                }
            }
            if (empty($paidMessage)) {
                $captureResult['paidOrders'] = null;
            } else {
                $captureResult['paidOrders'] = "The following orders have been paid for: $paidMessage";
            }
            if (empty($errorMessage)) {
                $captureResult['unpaidOrders'] = null;
            } else {
                $captureResult['unpaidOrders'] = "The following order still need to be paid for: $errorMessage";
            }
        }
        echo json_encode($authorizationResult);
    }

    /**
     * Expects the session token as a GET parameter.
     *
     * Returns the following JSON structure
     *  {
     *      message
     *  }
     */
    public function updateLaundryPreferences()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }
        $this->load->model('customer_model');
        $where['business_id'] = $session->business_id;
        $where['customerID'] = $session->customer_id;
        if (!isset($this->data['data']['preferences'])) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('preferences'));
        } else {

            if (isset($this->data['data']['notes'])) {
                $this->customer_model->update_customerNotes($session->customer_id, $this->data['data']['notes']);
            }

            $this->load->model('customerpreference_model');

            $preferences = json_decode($this->data['data']['preferences'], true);
            $this->customerpreference_model->save_preferences($session->customer_id, $preferences);

            $result['message'] = "Updated Customer Preferences";
            $this->api->response($result);
        }
    }

    /**
     * Updates a laundry preference of the authenticated customer.
     * Expects the following GET parameters
     *  sessionToken
     *  productCategory_slug THe wash and fold preference categoiry sliug
     *  productID The product ID of the wash and fold product
     * Returns the following as a JSON structure
     {
        message
        status
     }
     */
    public function updateLaundryPreference()
    {
        $token = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        try {
            $customer = new Customer($session->customer_id);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            return $this->api->output_api_error_response("Customer ID {$session->customer_id}");
        }

        if (!$this->input->get("productCategory_slug")) {
            return $this->api->output_api_error_response($this->getRequiredParamMessage('productCategory_slug'));
        }

        if (!$this->input->get("productID")) {
            return $this->api->output_api_error_response($this->getRequiredParamMessage('productID'));
        }

        $incoming_productCategory_slug = $this->input->get("productCategory_slug");
        $incoming_productID = $this->input->get("productID");

        // check the product exists
        $this->load->model("product_model");
        $product = $this->product_model->get_by_primary_key($incoming_productID);
        if (empty($product)) {
            return $this->api->output_api_error_response("product ID '$incoming_productID' does not exist");
        }

        $this->load->model("productCategory_model");
        $productCategory = $this->productCategory_model->get_by_primary_key($product->productCategory_id);

        // get the customer preferences
        $this->load->model('customerpreference_model');
        $preferences = $this->customerpreference_model->get_preferences($session->customer_id);

        // save the new value
        $preferences[$productCategory->slug] = $incoming_productID;
        $this->customerpreference_model->save_preferences($session->customer_id, $preferences);

        $this->api->output_api_success_response(array(), "Updated preference to product '{$product->name}'");
    }

    /**
     * Retrieves all items associated with the currently authenticated customer
     * Expects the following GET parameter:
     *  sessionToken
     * Returns the following JSON structure
     {
        status
        message
        data {
            displayName1 : {
                itemID,
                file,
                barcode
            },
             ...
        }
     }
     */
    public function get_closet_items()
    {
        $width = 300;
        $height = 300;
        $ac = 1;

        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        } else {

            $this->load->model('customer_model');
            $customer = $this->customer_model->get_by_primary_key($session->customer_id);

            $get_closet_items_query = $this->db->query("SELECT itemID, displayName, product.name, file, (select barcode from barcode where item_id = itemID limit 1) as barcode
            FROM item
            INNER JOIN customer_item ON customer_item.item_id = itemID
            INNER JOIN product_process ON product_processID = item.product_process_id
            INNER JOIN product ON productID = product_process.product_id
            LEFT JOIN picture ON picture.item_id = itemID
            WHERE
                customer_item.customer_id = ?
                AND item.business_id = ?;
           ", array($customer->customerID, $customer->business_id));

            $get_closet_items_query_result = $get_closet_items_query->result();
            $closet = array();

            if (!empty($this->data['data']['imageWidth'])) {
                $width = $this->data['data']['imageWidth'];
            }

            if (!empty($this->data['data']['imageHeight'])) {
                $height = $this->data['data']['imageHeight'];
            }

            if (!empty($this->data['data']['autoCrop'])) {
                $ac = ($this->data['data']['autoCrop'] == true) ? 1 : 0;
            }

            foreach ($get_closet_items_query_result as $closet_item) {
                $file = $closet_item->file;
                $closet[$closet_item->displayName][$closet_item->itemID] = array('itemID' => $closet_item->itemID, 'file' => $file, 'barcode' => $closet_item->barcode);
            }

            $this->api->output_api_success_response($closet);
        }
    }

    /**
     * Retrieves the claims for the current customer's session
     * Expects the folloiwng parameters:
     *  signature: The expected request signature
     *  token: The API token
     *  sessionToken: The customer session token
     * Returns the claims for the customers in the following format:
        {
            status,
            message,
            data : {
                {
                    address,
                    dateCreated,
                    lockerName
                },
                {
                    ...
                },
                ...
            }
        }
     */
    public function getClaims()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }
        $this->load->model('claim_model');
        /** Begin * */
        $options = array();
        $options['customer_id'] = $session->customer_id;
        $options['business_id'] = $session->business_id;
        $options['with_locker'] = true;
        $options['active'] = 1;
        $options['select'] = "claimID, lockerName, address, updated, orderType";
        $claims = $this->claim_model->get($options);
        /** End * */
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($session->customer_id);
        $this->api->output_api_success_response(array("claims" => $claims), "Claims for ".getPersonName($customer));
    }

    public function getCompleteClaims()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }
        $this->load->model('claim_model');

        /** Begin * */
        $options = array();
        $options['customer_id'] = $session->customer_id;
        $options['business_id'] = $session->business_id;
        $options['with_locker'] = true;
        $options['active'] = 1;

        $options['with_locker'] = true;
        $options['select'] = "*"
            . ", location.serviceType location_serviceType"
            . ", locationType.name location_locationType"
            . ", location.description location_description"

            . ", claim.created referenceDate"
            . ", claim.claimID orderID"
            . ", CONCAT('claim') orderStatusOption_id"
            . ", CONCAT('claim') orderType"
            . ", CONCAT(1) isClaim";

        $options['order_by'] = 'claim.created DESC';

        $claims = $this->claim_model->get($options);
        /** End * */
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($session->customer_id);
        $this->api->output_api_success_response(array("claims" => $claims), "Claims for ".getPersonName($customer));
    }

    /**
     * Gets the orders for the current customer's session
     * Expects the following parameters
     *  sessionToken: The customer session token
     * Returns the orders for the customer in the following format:
     {
       data: {
           [0] : {
            accessCode
            address
            dateCreated
            locker_id
            notes
            orderID
            orderTotal
            orderType
            payment
            status
            },
            [1] => {
            accessCode
            address
            dateCreated
            locker_id
            notes
            orderID
            orderTotal
            orderType
            payment
            status
           },
           ...
       message
       status
     }
     */
    public function getOrders()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (empty($sessionToken)) {
            $this->api->output_api_error_response($this->getRequiredParamMessage('sessionToken'));
        } else {
            if (!$session = $this->api->validateSessionToken($sessionToken)) {
                $this->api->responseError("Session not authenticated");
            }
            $this->load->model("order_model");

            try {
                $customer = new Customer($session->customer_id);
            } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $notFound_exception) {
                output_ajax_error_response(("Customer '{$session->customer_id}' not found"));
            }
            $orders = $this->order_model->getCustomerOrderHistory($session->customer_id, $session->business_id, 100, false, false);
            $orders_dunlap = $this->_sanitizeOrders($orders, $customer);

            $this->api->output_api_success_response($orders_dunlap, "Orders for ".getPersonName($customer));
        }
    }

    /**
     * ###################################################
     * BEGIN *
     * ###################################################
     *
     * gets locations and lockers that the customer has selected as drop-off locations
     *
     * ###################################################
     * END *
     * ###################################################
     */
    public function lockers()
    {
        $sessionToken = $this->input->get("sessionToken");
        if (!$session = $this->api->validateSessionToken($sessionToken)) {
            $this->api->responseError("Session not authenticated");
        }

        $customer = new Customer($session->customer_id);
        $result = $customer->lockers();
        $this->api->response($result);
    }

    /**
     * @deprecated
     * The following function is unknown *, do not use.
     * @param type $orders
     * @param type $customer
     * @return type
     */
    public function _sanitizeOrders($orders, $customer = '')
    {
        foreach ($orders as $order) {
            $is_unpaid = in_array($order->orderStatusOption_id, array(7, 13));
            if ($is_unpaid) {
                $order->accessCode = '';
                $order->lockerName = '';
            } else {
                $order->accessCode = substr(trim($customer->phone), -4);
            }
        }

        return $orders;
    }

    /**
     * Stores a customer rating
     *
     * Expects the following parameters
     *  sessionToken: The customer session token
     *  rating: the rating as an integer from 1 to 5
     *
     * Optionally takes the following parameters
     *  comment: a text to be attached to the rating
     *  order_id: an order related to the rating
     */
    public function save_customer_rating()
    {
        $customer = $this->getCustomer();

        $rating = $this->input->get_post('rating');
        if ($rating < 1 || $rating > 5) {
            output_ajax_error_response("'rating' must be an integer from 1 to 5");
        }

        $comment = $this->input->get_post('comment');
        $order_id = $this->input->get_post('order_id');

        $this->load->model('order_model');

        if ($order_id) {
            $foundOrder = $this->order_model->get_one(array(
                'orderID' => $order_id,
                'customer_id' => $customer->customerID
            ));

            if (!$foundOrder) {
                output_ajax_error_response("Order $order_id does not exist or belongs to a different customer.");
            }
        }

        $this->load->model('customerrating_model');
        $customerRatingID = $this->customerrating_model->save_customer_rating($customer->customerID, $rating, $comment, $order_id);
        $data = $this->customerrating_model->get_by_primary_key($customerRatingID);

        output_ajax_success_response($data, 'Customer Rating saved');
    }

    /**
     * Initializes paypal billing agreement
     *
     * This is used to bind a paypal account as a credit card.
     * This will redirect the user to paypal.
     *
     * If the user cancels on paypal it will be redirected back to the
     * paypal_cancel action.
     *
     * If the user accepts on paypal it will be redirected to the
     * paypal_success action.
     */
    public function initiate_paypal()
    {
        $customer = $this->getCustomer();
        $params = $this->requiredParameters(array(
            'successCallback',
            'cancelCallback',
        ));

        $this->load->library('creditcardprocessor');
        $paypal = $this->creditcardprocessor->factory('paypalprocessor', $customer->business_id);

        $response = $paypal->setExpressCheckout($params['successCallback'], $params['cancelCallback']);

        if ($response['status'] != 'SUCCESS') {
            output_ajax_error_response(
                'Problem while initiating paypal checkout: ' . $response['message']
            );
        }

        output_ajax_success_response(array("checkout_url" => $response['url']), "Paypal URL obtained");
    }

    /**
     * Setup a paypal billing agreement
     *
     * This receives the token via get and proceeds to setup the customer
     * paypal account as a credit card.
     */
    public function paypal_success()
    {
        $customer = $this->getCustomer();
        $params = $this->requiredParameters(array(
            'payment_token',
            'payment_type'
        ));

        $types = array(
            'paypal' => array(
                'processor' => 'paypalprocessor',
                'cardType' => 'PayPal',
                'label' => 'PayPal',
            ),
            'coinbase' => array(
                'processor' => 'coinbase',
                'cardType' => 'Coinbase',
                'label' => 'Bitcoin',
            ),
        );

        $type = $types[$params['payment_type']];

        $token = $params['payment_token'];

        // TODO: All this needs to be moved into a library, i.e. PaypalLibrary and reused in Profile controller and here
        $customer_id = $customer->customerID;
        $business_id =  $customer->business_id;

        $this->load->library('creditcardprocessor');
        $processor = $this->creditcardprocessor->factory($type['processor'], $business_id);

        $authorizationOrder = Order::create(array(
            "customer_id" => $customer_id,
            "locker_id" => 0, // set to $business->returnToOfficeLockerId in the order
            "orderPaymentStatusOption_id" => 1,
            "orderStatusOption_id" => 3,
            "notes" => "Authorizing Card",
            "business_id" => $business_id,
            "bag_id" => null,
        ));
        $order_id = $authorizationOrder->orderID;

        switch ($params['payment_type']) {
            case 'coinbase':
                $returnURL = $this->input->get('return_url');
                $response = $processor->authorizeToken($token, $returnURL, $order_id, $customer_id, $business_id);
                break;

            default:
                $response = $processor->authorizeToken($token, $order_id, $customer_id, $business_id);
        }

        if ($response->get_status() != "SUCCESS") {

            $authorizationOrder->delete();

            output_ajax_error_response(get_translation("failed_{$params['payment_type']}_authorization", "business_account/profile_billing",
                        array("authorization_message" => $response->get_message()),
                        "We are sorry, but we received the following response from {$type['label']}: %authorization_message%",
                        $customer_id));
        }

        switch ($params['payment_type']) {
            case 'coinbase':
                $tokens = json_decode($response->get_refNumber(), true);

                $details = $processor->getCustomerDetails($tokens, $returnURL);
                $cardNumber = $details['email'];
                break;

            default:
                $details = $processor->getCustomerDetails($token);
                $cardNumber = empty($details['EMAIL']) ? $details['PAYERID'] : $details['EMAIL'];
        }

        $this->load->model('creditcard_model');
        $creditCardID = $this->creditcard_model->save_card(array(
            'cardNumber' => $cardNumber,
            'expMo' => 12,
            'expYr' => date('Y') + 10,
            'payer_id' => $response->get_refNumber(),
            'customer_id' => $customer_id,
            'business_id' => $business_id,
            'processor' => $processor->getProcessor(),
            'cardType' => $type['cardType'],
        ));

        if (!is_numeric($creditCardID)) {
            throw new Exception('Error inserting creditCard data into db following successful card authorization');
        }

        $authorizationOrder->setProperties(array(
            "statusDate" => date("Y-m-d H:i:s"),
            "orderStatusOption_id" => 10,
            "orderPaymentStatusOption_id" => 3
        ));

        $orderPaymentStatus = OrderPaymentStatus::create(array(
            "order_id" => $authorizationOrder->orderID,
            "orderPaymentStatusOption_id" => 3
        ));


        $customer = new Customer($customer_id);
        $this->afterCreditCard($customer, $customer_id, $business_id);

        $message = "Customer '".getPersonName($customer)." ({$customer->customerID})' had unpaid orders and has just linked a {$type['label']} account";
        $captureResult = $this->checkAdditionalOrders($message, $customer_id, $business_id);

        output_ajax_success_response($captureResult, get_translation("successful_{$params['payment_type']}_authorization", "business_account/profile_billing",
            array(), "Your {$type['label']} account has been linked", $customer_id));
    }

    /**
     * Remove customer from payment hold and log CC authorization
     *
     * Called after a customer enters a credit card
     */
    protected function afterCreditCard($customer, $customer_id, $business_id)
    {
        // setup autopay
        $customer->autoPay = 1;

        //get rid of hold on account if we have successful authorization
        //log it in the notes
        $note_text = 'Successful authorization';
        if ($customer->hold == 1) {
            $note_text .= ', removed payment hold from customer account';
        }
        $customer->internalNotes = str_replace('ON PAYMENT HOLD', '', $customer->internalNotes);
        $customer->hold = 0;
        $customer->save();

        $this->db->insert('customerHistory', array(
            'customer_id' => $customer_id,
            'business_id' => $business_id,
            'historyType' => 'Internal Note',
            'note' => $note_text
        ));
    }


    /**
     * Try to capture non captured orders
     *
     * It takes a message to use as subject of the notification email sent
     *
     * @param sting $message
     */
    protected function checkAdditionalOrders($message, $customer_id, $business_id)
    {
        $this->load->model("order_model");

        $results = $this->order_model->checkAdditionalOrders(
            $customer_id,
            $business_id,
            '',
            $message,
            true
        );

        $captured = array();
        $failed = array();

        foreach ($results['captureResults'] as $captureResult) {
            if ($captureResult['status'] == 'success') {
                $captured[] = $captureResult['order_id'];
            } else {
                $failed[] = $captureResult['order_id'];
            }
        }

        return array('captureResult' => compact('captured', 'failed'));
    }

    /**
     * Updates a customer's password
     *
     * Expects the following parameters
     *  sessionToken: The customer session token
     *  current_pass: The customers current password
     *  new_pass: The new password
     */
    public function update_password()
    {
        $session = $this->getSession();

        $data = $this->requiredParameters(array(
            'current_pass',
            'new_pass',
        ));

        try {
            $customer = new Customer($session->customer_id, FALSE);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            output_ajax_error_response("Customer ID {$session->customer_id} not found");
        }

        if ($customer->password != md5($data['current_pass'])) {
            output_ajax_error_response("Incorrect customer password");
        }

        $customer->password = $data['new_pass'];

        try {
            $customer->save();
        } catch (\App\Libraries\DroplockerObjects\Validation_Exception $validationException) {
            output_ajax_error_response($validationException->getUserMessage());
        } catch (Exception $customer_save_exception) {
            send_exception_report($customer_save_exception);
            $this->api->output_api_error_response("A server side error occurred attempting to save the customer settings.");
        }

        output_ajax_success_response(array(), "Updated customer's password");
    }

    /**
     * Gets the customer kiosk access info
     *
     * Expects the following parameters
     *  sessionToken: The customer session token
     */
    public function get_kiosk_access()
    {
        $session = $this->getSession();

        $this->load->model('kioskaccess_model');
        $kioskAccess = $this->kioskaccess_model->get_one(array(
            'customer_id' => $session->customer_id,
        ));

        output_ajax_success_response($kioskAccess, "Customer's kiosk access");
    }

    /**
     * Removes the customer kiosk access
     *
     * Expects the following parameters
     *  sessionToken: The customer session token
     */
    public function delete_kiosk_access()
    {
        $session = $this->getSession();

        $this->load->model('kioskaccess_model');
        $this->kioskaccess_model->delete_aprax(array(
            'customer_id' => $session->customer_id,
        ));

        output_ajax_success_response(array(), "Customer's kiosk access removed");
    }

    /**
     * Setups a customer's kiosk access
     *
     * Expects the following parameters
     *  sessionToken: The customer session token
     *  cardNumber: last four digits of the CC
     *  expMonth: the expiration month of the CC
     *  expYear: the expiration year of the CC
     */
    public function set_kiosk_access()
    {
        $customer = $this->getCustomer();

        $data = $this->requiredParameters(array('cardNumber', 'expMo', 'expYr',));

        $this->load->model("kioskaccess_model");
        $this->kioskaccess_model->update_access($customer->customerID, $customer->business_id, $data['cardNumber'], $data['expMo'], $data['expYr']);
        $kioskAccess = $this->kioskaccess_model->get_one(array(
            'customer_id' => $customer->customerID,
        ));

        output_ajax_success_response($kioskAccess, "Customer's kiosk access updated");
    }

    public function _sanitizeLocation($location)
    {
        $locationType = new LocationType($location->locationType_id);

        $location->location_id = $location->locationID;
        $location->lng = $location->lon;
        $location->public = (string)$location->public;
        $location->locationType = $locationType->name;
        $location->isHomeDelivery = in_array($location->serviceType, Location::getHomeDeliveryServiceTypes());

        return $location;
    }
    
    public function getList()
    {
        try {
            $session = $this->getSession();
            $business_id =  $session->business_id;

            $this->load->model('customer_model');
            $customers = $this->customer_model->get_business_customers(array(
                'business_id' => $business_id,
                'only_active' => true,
                'select' => array(
                    'customerID',
                    'email',
                ),
            ));
        } catch (Exception $exc) {
            output_ajax_error_response(
                "Problem while retrieving customers for business $business_id: " . $exc->getMessage() 
            );
        }

        output_ajax_success_response($customers, "Found ".  count($customers) . " active customers for business $business_id");
    }
}
