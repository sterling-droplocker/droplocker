<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\LocationType;
use App\Libraries\DroplockerObjects\Business as BusinessObj;
use App\Libraries\DroplockerObjects\Business;


/**
 * The business controller provides the following functionality:
 *  Retrieving all locations associated with a business
 *  Retrieving the business starch on shirts settings
 *  Retrieving all business properties
 *  Retrieving the business service types
 *  Retrieving the business settings.
 */
class Businesses extends MY_Api_Controller
{
    /**
     * Expects the following GET parameters
     *  tag
     *  business_id
     * @return array
     */
    public function getLanguageViewTranslationsForLanguage()
    {
        $business_id = $this->input->get("business_id");
        if (!is_numeric($business_id)) {
            output_ajax_error_response(("'business_id' must be numeric"));
        }
        $business = $this->business_model->get_by_primary_key($business_id);
        if (empty($business)) {
            output_ajax_error_response(("Business ID '$business_id' does not exist"));
        }
        
        $this->load->model("language_model");
        $tag = $this->input->get("tag");
        if (empty($tag)) {
            $this->load->model("business_language_model");
            $default_business_language = $this->business_language_model->get_one(array(
                'business_languageID' => $business->default_business_language_id,
            ));
            
            $language = $this->language_model->get_one(array(
                'languageID' => $default_business_language->language_id,
            ));
            
            if (!$language) {
                output_ajax_error_response("'tag' not sent and default language not found for business");
            }
        } else {
            $tag = current(explode('-', $tag));
            $language = $this->language_model->findByTag($tag);
            
            if (empty($language)) {
                output_ajax_error_response("Language tag '$tag' does not exist");
            }
        }

        $this->load->model("languagekey_business_language_model");
        $iPhoneLanguageViewsAndTranslations = $this->languagekey_business_language_model->getTranslationsForLanguageViewNameForLanguageNameForBusiness( $language->languageID, $business->businessID, array(
            "mobile/address",
            "mobile/address_detail",
            "mobile/basic_view",
            "mobile/business",
            "mobile/change_password",
            "mobile/credit_card",
            "mobile/customer_language",
            "mobile/forgot_password",
            "mobile/global",
            "mobile/home",
            "mobile/kiosk_access",
            "mobile/language",
            "mobile/menu",
            "mobile/my_closet",
            "mobile/order",
            "mobile/order_placed",
            "mobile/order_status",
            "mobile/orderdetail",
            "mobile/orders",
            "mobile/payment",
            "mobile/payment_update",
            "mobile/preferences",
            "mobile/settings",
            "mobile/signin",
            "mobile/signup",
            "global",
        ));
        
        $iPhoneLanguageViewsAndTranslations['global']['nameComponents'] = explode('||', $iPhoneLanguageViewsAndTranslations['global']['nameComponents']);
        $iPhoneLanguageViewsAndTranslations['global']['addressComponents'] = explode('||', $iPhoneLanguageViewsAndTranslations['global']['addressComponents']);
                
        $message = "'$business->companyName' translations for '$language->tag - $language->description'.";
        if(!$tag) {
            $message .= " Using default language.";
        }
        
        output_ajax_success_response($iPhoneLanguageViewsAndTranslations, $message);
    }

    /**
     * Retreives properties for a particular business
     * Expects the following GET parameters:
        * businessID: The unique ID of the business
     * Output is in the following format:
    {
        status
        message
        data : {
            companyName,
            website_url,
            sms_provider
        }
    }
     */
    public function get_business_properties()
    {
        if (!$this->input->get("businessID")) {
            $this->api->output_api_error_response("BusinessID must be passed as a GET parameter");
        }
        $businessID = $this->input->get("businessID");

        try {
            $business = new \App\Libraries\DroplockerObjects\Business($businessID);
        } catch (\App\Libraries\DroplockerObjects\NotFound_Exception $e) {
            output_ajax_error_response("Business ID {$businessID} not found");
        }
        $this->load->model("smssettings_model");
        $smsSetting = $this->smssettings_model->get_settings($businessID);
        if ($smsSetting) {
            $sms_provider = $smsSetting->provider;
        } else {
            $sms_provider = "mosio";
        }

        $supports_mobile_cc_auth = false;
        $enable_cc = false;

        // check if the processor works from the mobile app
        $this->load->library('creditcardprocessor');
        $processor = get_business_meta($businessID, 'processor');
        if ($processor) {
            $creditCardProcessor = $this->creditcardprocessor->factory($processor, $businessID);
            $supports_mobile_cc_auth = $creditCardProcessor->supportsMobileAuth();

            if ($supports_mobile_cc_auth && stristr($processor, 'coinbase') === FALSE) {
                $enable_cc = true;
            }
        };

        $business = new Business($businessID);
        $facebook_login = null;
        if ($business->facebook_api_key) {
            $facebook_login = get_business_account_url($business) . '/account/main/start_mobile_login';
        }

        $this->load->model("business_language_model");
        $lang_list = $this->business_language_model->get_all_for_business($businessID);

        $date_format = get_business_meta($businessID, "shortWDDateLocaleFormat",'%a %m/%d');
        $time_format = get_business_meta($businessID, "hourLocaleFormat",'%l:%M %p');
        $date_time_format = $date_format . " " . $time_format;
        $timezone_offset = convert_from_gmt_aprax(null, 'P', $businessID);
        
        $response = array(
            'businessID' => $business->businessID,
            'companyName' => $business->companyName,
            'city' => $business->city,
            'state' => $business->state,
            'country' => $business->country,
            'phone' => $business->phone,
            'email' => $business->email,
            'website_url' => $business->website_url,
            'sms_provider' => $sms_provider,
            'require_card_on_order' => $business->require_card_on_order,
            'account_url' => get_business_account_url($business),
            'require_card_on_order' => $business->require_card_on_order,
            'supports_mobile_cc_auth' => $supports_mobile_cc_auth,
            'enable_cc' => $enable_cc,
            'enable_paypal' => get_business_meta($businessID, 'paypal_button') == true,
            'enable_bitcoin' => get_business_meta($businessID, 'bitcoin_button') == true,
            'lang_count' => count($lang_list),
            'lang_list' => $lang_list,
            'facebook_api_key' => $business->facebook_api_key,
            'enable_facebook' => $business->facebook_api_key && $business->facebook_secret_key,
            'facebook_login' => $facebook_login,
            'limit_geo_country' => get_business_meta($businessID, 'limit_geo_country'),
            'enable_kiosk_access' => $business->store_access == 1,
            'gmaps_api_key' => get_default_gmaps_api_key(),
            'date_format' => $date_format,
            'time_format' => $time_format,
            'date_time_format' => $date_time_format,
            'timezone_offset' => $timezone_offset,
            'require_address2' => get_business_meta($businessID, 'require_address2'),
        );

        $this->api->output_api_success_response($response);
    }
    /**
     * The following function retrieves all businesses in the system. A business is considered a 'business' if it has defined service types.
     *
     * The response format is as follows:
     *
        {
            {
                businessID
                companyName
                city
                state
                website_url
            },
            {
                ...
            },
            ...
        }
     */
    public function get()
    {
        $this->db->select(
              "businessID, "
            . "companyName, "
            . "city,"
            . "state, "
            . "website_url, "
            . "phone, "
            . "email, "
            . "require_card_on_order, "
            . "facebook_api_key, "
            . "( length(trim(facebook_api_key)) > 0 AND length(trim(facebook_secret_key)) > 0 ) enable_facebook");
        $this->db->where("serviceTypes IS NOT NULL AND show_in_droplocker_app = 1"); //Only show businesses with defined service types and configured to show in the droplocker application
        $this->db->order_by('companyName');
        $businesses = $this->db->get('business')->result();

        output_ajax_success_response($businesses, "All Businesses");

    }

    /**
     * Gets business sorted by distance to a point
     *
     * Expects the following GET parameters
     *  - business_id
     *  - lat
     *  - lon
     */
    public function get_by_geo()
    {
        $lat = $this->input->get("lat");
        if (!is_numeric($lat)) {
            output_ajax_error_response("'lat' must be numeric");
        }

        $lon = $this->input->get("lon");
        if (!is_numeric($lon)) {
            output_ajax_error_response("'lon' must be numeric");
        }

        $this->load->model('business_model');
        $businesses = $this->business_model->get_all_by_distance_for_app($lat, $lon);

        output_ajax_success_response($businesses, "All Businesses sorted by distance to ($lat, $lon)");
    }

    /**
     * Expects 'business_id' as a GET parameter
     * Returns the service types in the following format:
       {
          [serviceTypeID1] => displayName1
          [serviceTypeID2 => displayName2,
          ...
       }
     */
    public function get_serviceTypes()
    {
        if ($this->input->get('business_id')) {
            $business_id = $this->input->get("business_id");
            $business = $this->business_model->get_by_primary_key($this->input->get("business_id"));
            if (empty($business)) {
                output_ajax_error_response("Business ID '$business_id' not found");
            } else {
                //Note, * stores the service types as a serialized array in the 'serviceTypes' column of the business table.
                $serviceTypes = unserialize($business->serviceTypes);
                asort($serviceTypes);
                $result = array();
                foreach ($serviceTypes as $serviceType) {
                    $result[$serviceType['serviceType_id']] = get_translation($serviceType['displayName'], "business_account/orders_new_order_service_types", array(), $serviceType['displayName'], null, $this->input->get("business_languageID"));
                }
                output_ajax_success_response($result, "Service types for business '{$business->companyName}'");
            }
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }
    }

    /**
     *
     * Note, the following function was written by * and is undocumented, incorrectly named, and has unknown behavior.
     * Note, this function returns the service types, not the modules.
     * @deprecated Use get_service_types()
     */
    public function modules()
    {
        if (empty($this->data['business_id'])) {
            $this->api->responseError('Missing business_id');
        }

        $business = $this->db->get_where('business', array('businessID'=>$this->data['business_id']))->row();

        if ($business) {
            $allModules = array();
            foreach (unserialize($business->serviceTypes) as $module_id => $moduleAttributeArray) {
                $modules['module_id'] = $module_id;
                foreach ($moduleAttributeArray as $attributeKey => $attributeValue) {
                    $modules[$attributeKey] = $attributeValue;
                }

                $allModules[] = $modules;
            }

            $result['modules'] = $allModules;
        } else {
           $this->api->responseError("Business modules not found");
        }
        $this->api->response($result);

    }

    /**
     * Retrieves all active locations for a business ordered by address. The business is derived from the user's session token.
     * Expects the following parameters:
     *  sessionToken
     * data: consits of the location in the following format
        {
            status,
            message,
            data : {
                {
                    locationID,
                    address,
                    city,
                    state,
                    zipcode
                },
                {
                    ...
                },
                ...
            }
        }
     */
    public function getLocations()
    {
        $CI = get_instance();
        $CI->load->model("location_model");
        $this->load->model("apisession_model");

        if (!isset($this->data['data']['sessionToken'])) {
            $this->api->output_api_error_response("'sessionToken' must be assed as a GET parameter");
        } else {
            $apisession  = $this->apisession_model->get_one(array("token" => $this->data['data']['sessionToken']));
            if (empty($apisession)) {
                $this->api->output_api_error_response("session token {$this->data['data']['sessionToken']} not found");
            } else {
                $locations = $this->db->query("select location.locationID, location.address, location.city, location.state,location.zipcode FROM location
                    INNER JOIN locker ON locker.location_id=location.locationID AND locker.lockerName != 'InProcess' AND locker.lockerstyle_id NOT IN (7,9)
                    WHERE location.business_id=? AND location.status=?
                    GROUP BY location.locationID ORDER BY location.address", array($apisession->business_id, "active"))->result();
                $this->load->model("business_model");
                $business = $this->business_model->get_by_primary_key($apisession->business_id);
                $this->api->output_api_success_response($locations, "Locations for business {$business->companyName}");
            }
        }
    }



    /**
     * Returns the available starch on shirt options for a business, or returns an empty data set if the business does not have starch options enabled.
     * Expects 'business_id' as a GET parameter
     * The response is in the following format
     {
       status,
       message,
       data : {
           id1 : name,
           id2 : name
           ...
       }
     }
     */
    public function getStarchOnShirts()
    {
        if ($this->input->get("business_id")) {
            $this->load->model("business_model");
            $business = $this->business_model->get_by_primary_key($this->input->get("business_id"));
            $starchOnShirts = array();
            if (get_business_meta($business->businessID, "displayStarchOptions")) {
                $this->load->model("starchonshirts_model");
                $starchOnShirts = $this->starchonshirts_model->get();
            }
            $this->api->output_api_success_response($starchOnShirts, "Starch on shirt preferences for business");
        } else {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        }
    }

    /**
     * Retrieves the available mdoules and associated products for a particular business.
     * Expets 'business_id' as a GET parameter
     * The return format is may be as follows:
     *
    {
        <module name 1> : {
            <product 1> : {
                displayName
                name
                productID,
                productCategory_name,
                productCategory_slug
            },
            <product 2> : {
                ...
            },
            ...
        <module name 2> : {
            ...
        },
        ...
    }
     */
    public function settings()
    {
        $this->load->model("product_model");
        $business_id = $this->input->get("business_id");
        if (!is_numeric($business_id)) {
            output_ajax_error_response("'business_id' must be passed as a GET parameter");
        } else {
            $business_id = $this->input->get("business_id");
            $this->load->model("business_model");
            $business = new \App\Libraries\DroplockerObjects\Business($business_id);
            $tag = $this->input->get("tag");
            if ($tag) {
                $tag = current(explode('-', $tag));
            }

            $this->load->library('productslibrary');
            $modules_and_associated_products = $this->productslibrary->getBusinessPreferencesForAPI($business_id, $tag);
            
            foreach ($modules_and_associated_products as $module_name => $value) {
                // create translation
                get_translation("module_$module_name", "mobile/global", array(), $module_name, null, $this->input->get("business_languageID"));
            }

            output_ajax_success_response($modules_and_associated_products, "Modules and associated product categories and associated products for {$business->companyName}");

        }
    }

}
