<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\OrderHomePickup;


class Claims extends MY_Api_Controller
{
    private $customerSession;
    public function __construct()
    {
        parent::__construct();
        $token = $this->input->get("sessionToken");
        if (!($this->customerSession = $this->api->validateSessionToken($token))) {
            output_ajax_error_response("Invalid session token");
        }
    }
    /**
     * Creates a new claim
     * Exects the following GET parameters
     *  lockerName
     *  orderType
     * Can take the following GET parameters
     *  orderNotes
     * Returns a JSON structure in the following format
     * {
     *      status : string success|error,
     *      message: string
     *      data : array()
     * }
     */
    public function create()
    {
        $validationResult = $this->validateRequest(array(
            "lockerName" => array("notEmpty"),
            "orderType" => array("notEmpty")
        ));
        if ($validationResult['status'] !== "success") {
            $this->outputJSONerror("The following validation errors occurred", $validationResult['errors']);
        }
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($this->customerSession->customer_id);
        $this->load->model("business_model");
        $business = $this->business_model->get_by_primary_key($this->customerSession->business_id);

        $this->db->join("location", "location.locationID=locker.location_id");
        $lockers = $this->db->get_where("locker", array("locker.lockerName" => $this->input->get("lockerName"), "location.business_id" => $this->customerSession->business_id))->result();

        if (isset($lockers[1])) {
            $this->outputJSONerror("There are multiple lockers with the locker name '{$this->input->get("lockerName")}' for business '$business->companyName' ($business->businessID)");
        }
        if (empty($lockers)) {
            $this->outputJSONerror("There are no lockers found for locker name '{$this->input->get("lockerName")} for business '$business->companyName' ($business->businessID)");
        }
        $locker = $lockers[0];
        $claimOptions = array();

        $orderNotes = $this->input->get("orderNotes");
        if (!empty($orderNotes)) {
            $claimOptions['notes'] = $orderNotes;
        }
        $claimOptions['service_type_serialized'] = serialize(explode(",", $this->input->get("orderType")));

        $claim = new Claim();
        $claimCreationResult = $claim->newClaim($locker->lockerID, $this->customerSession->customer_id, $this->customerSession->business_id, $claimOptions);

        switch ($claimCreationResult['status']) {
            case 'success':
                $this->outputJSONsuccess("Created new claim '{$claimCreationResult['claimID']}' for customer '".getPersonName($customer)."' ($customer->customerID) in locker '$locker->lockerName' ($locker->lockerID) in location '$locker->address' ($locker->locationID) for business '$business->companyName' ($business->businessID)");
                break;
            case 'error':
                $this->outputJSONerror("Could not create new claim", $claimCreationResult['message']);
            default:
                $this->outputJSONerror("An Unknown server side error occurred");
                break;
        }
    }

    /*
     *  Method: claims/getDetails
     *
     *  Author: Luciano Ropero <luciano@laundrylocker.com; lropero@gmail.com>
     *
     *  Description:
     *      Retrieves details of a claim
     *
     *  Exects the following GET parameters:
     *      (int) claim_id
     *      (stirng) sessionToken
     *
     *  Returns the following JSON structure:
     *      {
     *          locker_id: value
     *          ...
     *      }
     */
    public function getDetails() {

        if (empty($this->data["data"]["claim_id"])) {
            $this->outputJSONerror("Missing claim_id");
        }

        $token = $this->data["data"]["sessionToken"];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->outputJSONerror("Session not authenticated");
        }

	    $this->load->model("claim_model");
	    $claim = $this->claim_model->get_by_primary_key($this->data["data"]["claim_id"]);

            $pickupInfo = OrderHomePickup::search(array('claim_id' => $this->data["data"]["claim_id"]));
            if ($pickupInfo){
                $claim->home_pickup = $pickupInfo->to_array();
                $claim->home_pickup['wStartDate'] = $pickupInfo->windowStart;
                $claim->home_pickup['wEndDate'] = $pickupInfo->windowEnd;

                $claim->home_pickup['local_start_time'] = convert_from_gmt_aprax($pickupInfo->windowStart, 'H:i:s', $session->business_id);
                $claim->home_pickup['local_end_time'] = convert_from_gmt_aprax($pickupInfo->windowEnd, 'H:i:s', $session->business_id);
            }

            if ($claim->locker_id){
                $this->load->model("locker_model");
                $locker = $this->locker_model->get_by_primary_key($claim->locker_id);

                $location = new Location($locker->location_id, FALSE);
                $location->locationType = $location->getLocationTypeName();
                $claim->location = ($location) ? $location->to_array() : false;
                $claim->locationAddress = $location->address;
                $claim->locker = $locker;
            }

            $claim->dateCreated = new DateTime($claim->created, new DateTimeZone("GMT"));

	    if ($claim && $locker) {
            $this->outputJSONsuccess("Found claim with ID " . $this->data["data"]["claim_id"] . ".", array('claim' => $claim, 'locker' => $locker));
        } else {
            $this->outputJSONerror("Claim with ID " . $this->data["data"]["claim_id"] . " not found.");
        }
    }
}
