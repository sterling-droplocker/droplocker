<?php

use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Claim;

class Home_Delivery extends MY_Api_Controller
{
    public function __construct()
    {
        parent::__construct();
        $session = $this->getSession();
        $this->business_id =  $session->business_id;
    }

    /**
     * Tells if a customer can request home delivery
     */
    public function can_request_home_delivery()
    {
        $customer = $this->getCustomer();

        $data = array(
            'cusotmer_id' => $customer->customerID,
            'can_request_home_delivery' => false,
        );

        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
        if ($homeDeliveryService) {
            $data['can_request_home_delivery'] = $homeDeliveryService->canCustomerRequestHomeDelivery($customer);
            
            $this->load->library('units');
            $exclusionRadius = get_business_meta($customer->business_id, 'location_hd_exclusion_radius', 0);
            $exclusionRadius_unit = get_business_meta($this->business_id, 'location_hd_exclusion_radius_unit', Units::DISTANCE_MILES);
            $exclusionRadius_km = $this->units->convert_distance($exclusionRadius, $exclusionRadius_unit, Units::DISTANCE_KM);
          
            if ($exclusionRadius_km) {
                $this->load->model('location_model');
                $found_near_locations = $this->location_model->get_all_by_distance_for_business(
                    $customer->lat, 
                    $customer->lon, 
                    $exclusionRadius_km, 
                    $customer->business_id, 
                    'both', 
                    1);

                if (count($found_near_locations)) {
                    $data['can_request_home_delivery'] = false;
                    $data['exclusion_radius_km'] = $exclusionRadius_km;
                    $data['location_on_exclusion_radius'] = $found_near_locations;
                    $data['exclusion_radius_setup'] = "$exclusionRadius $exclusionRadius_unit";
                 }
            }
        } else {
            $data['can_request_home_delivery'] = false;
        }

        output_ajax_success_response($data, "Customer Home Delivery ".getPersonName($customer)." ({$customer->customerID})");
    }


    /**
     * Adds a customer's day based delivery
     *
     * Expects the following POST parameters
     *  - delivery_zone_id
     *  - delivery_window_id
     *  - lat
     *  - lon
     *  - address1
     *  - address2
     *
     * Optional GET parameters
     *  - city
     *  - state
     *  - zip
     */
    public function add_day_based_delivery()
    {
        try {
            $customer = $this->getCustomer();

            $delivery_zone_id   = $this->input->get_post("delivery_zone_id");
            $delivery_window_id = $this->input->get_post("delivery_window_id");
            $lat                = $this->input->get_post("lat");
            $lon                = $this->input->get_post("lon");
            $address1           = $this->input->get_post("address1");
            $address2           = $this->input->get_post("address2");
            $city               = $this->input->get_post("city");
            $state              = $this->input->get_post("state");
            $zip                = $this->input->get_post("zip");

            if (empty($delivery_window_id) && empty($delivery_zone_id)) {
                output_ajax_error_response("'delivery_window_id' or 'delivery_zone_id' must be set");
            }

            if (empty($address1)) {
                output_ajax_error_response("'address1' must be set");
            }

            if (!is_numeric($lat)) {
                output_ajax_error_response("'lat' must be numeric");
            }

            if (!is_numeric($lon)) {
                output_ajax_error_response("'lon' must be numeric");
            }

            if ($delivery_window_id) {
                $this->load->model('deliverywindow_model');
                $delivery_window = $this->deliverywindow_model->get_by_primary_key($delivery_window_id);

                if (empty($delivery_window)) {
                    output_ajax_error_response("Invalid Delivery Window");
                }

                $delivery_zone_id = $delivery_window->delivery_zone_id;
            }

            $this->load->model('deliveryzone_model');
            $delivery_zone = $this->deliveryzone_model->get_by_primary_key($delivery_zone_id);

            if (empty($delivery_zone)) {
                output_ajax_error_response("Invalid Delivery Zone");
            }

            $this->load->model('delivery_customersubscription_model');
            $id = $this->delivery_customersubscription_model->save(array(
                'customer_id'      => $customer->customerID,
                'delivery_zone_id' => $delivery_zone_id,
                'address1'         => $address1,
                'address2'         => $address2,
                'city'             => $city,
                'state'            => $state,
                'zip'              => $zip,
                'lat'              => $lat,
                'lon'              => $lon,
                'sortOrder'        => 0,
            ));

            $customerSubscription = $this->delivery_customersubscription_model->get_by_primary_key($id);

            output_ajax_success_response($customerSubscription, "Day based delivery added for customer {$customer->customerID}");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Deletes a customer's day based delivery
     *
     * Expects the following POST parameters
     *  - business_id
     *  - day_based_delivery_id
     */
    public function delete_day_based_delivery()
    {
        try {
            $customer = $this->getCustomer();

            $day_based_delivery_id = $this->input->get_post("day_based_delivery_id");

            $this->load->model('delivery_customersubscription_model');
            $customerSubscription = $this->delivery_customersubscription_model->get_by_primary_key($id);

            if (empty($customerSubscription) || $customerSubscription->customer_id != $customer->customerID) {
                output_ajax_error_response("Invalid day_based_delivery_id");
            }

            $this->delivery_customersubscription_model->delete_aprax(array('delivery_customerSubscriptionID' => $customerSubscription->delivery_customerSubscriptionID));

            output_ajax_success_response($customerSubscription, "Day based delivery deleted for customer {$customer->customerID}");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Gets the customer's day based deliveries
     *
     * Expects the following GET parameters
     *  - business_id
     *  - day_based_delivery_id
     */
    public function get_day_based_deliveries()
    {
        try {
            $customer = $this->getCustomer();

            $day_based_delivery_id = $this->input->get("day_based_delivery_id");

            $this->load->model('delivery_customersubscription_model');
            $customerSubscriptions = $this->delivery_customersubscription_model->get_aprax(array('customer_id' => $customer->customerID));

            $customerSubscriptions = $this->enhanceDayBasedDeliveries($customerSubscriptions);

            output_ajax_success_response($customerSubscriptions, "Day based deliveries for customer {$customer->customerID}");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    protected function enhanceDayBasedDeliveries($deliveryList)
    {
        $enhanced = array();

        $this->load->model('deliverywindow_model');
        $this->load->model('deliveryzone_model');

        foreach ($deliveryList as $delivery) {
            $deliveryZone = $this->deliveryzone_model->get_by_primary_key($delivery->delivery_zone_id);
            $location = new Location($deliveryZone->location_id);
            $delivery->deliveryZone = $deliveryZone;

            $deliveryWindows = $this->deliverywindow_model->get_aprax(array('delivery_zone_id' => $delivery->delivery_zone_id));
            $delivery->deliveryWindow = $deliveryWindows[0];

            $delivery->location = $location->to_array();

            $enhanced[] = $delivery;
        }

        return $enhanced;
    }

    /**
     * Gets locations available at a point
     *
     * Expects the following GET parameters
     *  - business_id
     *  - lat
     *  - lon
     */
    public function get_locations_by_geo()
    {
        $business_id = $this->input->get("business_id");
        if (!is_numeric($business_id)) {
            output_ajax_error_response("'business_id' must be numeric");
        }

        $lat = $this->input->get("lat");
        if (!is_numeric($lat)) {
            output_ajax_error_response("'lat' must be numeric");
        }

        $lon = $this->input->get("lon");
        if (!is_numeric($lon)) {
            output_ajax_error_response("'lon' must be numeric");
        }

        try {
            $locations = $this->getLocationsByPoint($lat, $lon, $business_id);
            output_ajax_success_response($locations, "Locations for ($lat, $lon)");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Gets locations available at an address
     *
     * Expects the following GET parameters
     *  - business_id
     *  - address
     *  - city
     *  - state
     */

    public function get_locations_by_address()
    {
        $business_id = $this->input->get("business_id");
        if (!is_numeric($business_id)) {
            output_ajax_error_response("'business_id' must be numeric");
        }

        $address = $this->input->get("address");
        if (empty($address)) {
            output_ajax_error_response("'address' must be set");
        }

        $city = $this->input->get("city");
        if (empty($city)) {
            output_ajax_error_response("'city' must be set");
        }

        $state = $this->input->get("state");
        if (empty($state)) {
            output_ajax_error_response("'state' must be set");
        }

        try {
            $geo = geocode($address, $city, $state);
            $locations = $this->getLocationsByPoint($geo['lat'], $geo['lng'], $business_id);
            output_ajax_success_response($locations, "Locations for '$address, $city, $state'");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Helper function to get the home delivery locations for a point
     *
     * @param float $lat
     * @param float $lon
     * @param int $business_id
     * @return array
     */
    protected function getLocationsByPoint($lat, $lon, $business_id)
    {
        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($business_id);
        $locations = array();

        if ($homeDeliveryService) {
            $customer = $this->getCustomer();
            if (empty($customer)) {
                $customer = new stdClass();
            }
            $customer->lat = $lat;
            $customer->lon = $lon;

            $locations = $homeDeliveryService->getLocationAvailablesForCustomer($customer);
        }

        return $locations;
    }

    /**
     * Gets the available pickup windows
     *
     * Expects the following GET parameters
     *   location_id
     */
    public function get_pickup_windows()
    {
        try {
            $location_id = $this->input->get("location_id");
            if (!is_numeric($location_id)) {
                output_ajax_error_response("'location_id' must be numeric");
            }

            $location = new Location($location_id);
            $customer = $this->getCustomer();

            $data = array();
            $data['location'] = $location;
            $homePickup = $this->getHomePickup($customer, $location_id);

            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            $windows_by_date = $homeDeliveryService->getPickupWindows($homePickup);

            $data = array();
            $data['pickup_dates'] = array_keys($windows_by_date);
            $data['pickup_windows'] = $homeDeliveryService->splitWindowsByFees($windows_by_date);

            output_ajax_success_response($data, "Pickup windows for '{$homePickup->address1}'");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Schedules a Home Pickup
     *
     * Expects the following POST parameters
     *  - claim_id
     *  - pickup_window_id
     *  - notes
     */
    public function schedule_home_pickup()
    {
        try {
            $customer = $this->getCustomer();

            $claim_id = $this->input->get_post("claim_id");
            if (!is_numeric($claim_id)) {
                output_ajax_error_response("'claim_id' must be numeric");
            }

            $pickup_window_id = $this->input->get_post("pickup_window_id");
            if (empty($pickup_window_id)) {
                output_ajax_error_response("'pickup_window_id' must be set");
            }

            $date = $this->input->get_post("date");
            if (empty($date)) {
                output_ajax_error_response("'date' must be set");
            }

            $start_time = $this->input->get_post("start_time");
            if (empty($start_time)) {
                output_ajax_error_response("'start_time' must be set");
            }

            $end_time = $this->input->get_post("end_time");
            if (empty($end_time)) {
                output_ajax_error_response("'end_time' must be set");
            }

            $notes = $this->input->get_post("notes");

            $delivery_date = $this->input->get_post("delivery_date");
            $delivery_start_time = $this->input->get_post("delivery_start_time");
            $delivery_end_time = $this->input->get_post("delivery_end_time");

            $claim = new Claim($claim_id);
            $locker = new Locker($claim->locker_id);
            $location = new Location($locker->location_id);

            $homePickup = $this->getHomePickup($customer, $locker->location_id);

            if ($homePickup->pickup_id) {
                $data = array();
                $data['homePickup'] = $homePickup->to_array();
                output_ajax_success_response($data, "Pickup already scheduled for claim {$claim->claimID}");
            }

            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            $pickup_windows = $this->getPickupWindowsByID($homeDeliveryService, $homePickup);

            if (empty($pickup_windows[$pickup_window_id])) {
                output_ajax_error_response("Invalid Pickup Window");
            }
            
            $pickupWindow = $pickup_windows[$pickup_window_id];

            $homePickup->claim_id = $claim_id;
            $homePickup->notes = $notes;
            $homePickup->pickup_window_id = $pickup_window_id;
            $homePickup->pickupDate = $date;
            $homePickup->windowStart = $this->convertToMysql($date, $start_time);
            $homePickup->windowEnd = $this->convertToMysql($date, $end_time);
            $homePickup->fee = $pickupWindow['fee'];
            
            if (!empty($delivery_date) && !empty($delivery_start_time) && !empty($delivery_end_time)) {
                $homePickup->delivery_start_time = $this->convertToMysql($delivery_date, $delivery_start_time);
                $homePickup->delivery_end_time = $this->convertToMysql($delivery_date, $delivery_end_time);
            }

            $homePickup->save();

            $homeDeliveryService->scheduleHomePickup($homePickup);

            $action = "new_home_pickup";

            $action_array = array(
                'do_action'     => $action, 
                'customer_id'   => $customer->customerID,
                'claim_id'      => $claim_id, 
                'business_id'   => $this->business_id
            );

            \Email_Actions::send_transaction_emails($action_array);

            $data = array();
            $data['homePickup'] = $homePickup->to_array();

            output_ajax_success_response($data, "Pickup scheduled for claim {$claim->claimID}");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Gets the available delivery windows
     *
     * Expects the following GET parameters
     *  - order_id
     */
    public function get_delivery_windows()
    {
        try {
            $order_id = $this->input->get("order_id");
            if (!is_numeric($order_id)) {
                output_ajax_error_response("'order_id' must be numeric");
            }

            $order = new Order($order_id);
            $locker = new Locker($order->locker_id);
            $location = new Location($locker->location_id);
            $customer = $this->getCustomer();

            $data = array();
            $data['location'] = $location;
            $homeDelivery = $this->getHomeDelivery($customer, $locker->location_id, $order_id);

            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            $windows_by_date = $homeDeliveryService->getDeliveryWindows($homeDelivery);

            $data = array();
            $data['pickup_dates'] = array_keys($windows_by_date);
            $data['pickup_windows'] = $homeDeliveryService->splitWindowsByFees($windows_by_date);

            output_ajax_success_response($data, "Delivery windows for {$homeDelivery->address1}");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Schedules a Home Delivery
     *
     * Expects the following POST parameters
     *  - order_id
     *  - delivery_window_id
     *  - notes
     */
    public function schedule_home_delivery()
    {
        try {
            $customer = $this->getCustomer();

            $order_id = $this->input->get_post("order_id");
            if (!is_numeric($order_id)) {
                output_ajax_error_response("'order_id' must be numeric");
            }

            $delivery_window_id = $this->input->get_post("delivery_window_id");
            if (empty($delivery_window_id)) {
                output_ajax_error_response("'delivery_window_id' must be set");
            }

            $notes = $this->input->get_post("notes");

            $order = new Order($order_id);
            $locker = new Locker($order->locker_id);
            $location = new Location($locker->location_id);

            $homeDelivery = $this->getHomeDelivery($customer, $locker->location_id, $order_id);

            if ($homeDelivery->delivery_id) {
                $data = array();
                $data['homeDelivery'] = $homeDelivery->to_array();
                output_ajax_success_response($data, "Delivery already scheduled for order {$order->orderID}");
            }

            $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($this->business_id);
            $delivery_windows = $this->getDeliveryWindowsByID($homeDeliveryService, $homeDelivery);

            if (empty($delivery_windows[$delivery_window_id])) {
                output_ajax_error_response("Invalid Delivery Window");
            }
            $deliveryWindow = $delivery_windows[$delivery_window_id];

            $homeDelivery->notes = $notes;
            $homeDelivery->delivery_window_id = $delivery_window_id;
            $homeDelivery->deliveryDate = $deliveryWindow['date'];
            $homeDelivery->windowStart = $this->convertToMysql($deliveryWindow['date'], $deliveryWindow['start_time']);
            $homeDelivery->windowEnd = $this->convertToMysql($deliveryWindow['date'], $deliveryWindow['end_time']);
            $homeDelivery->fee = $deliveryWindow['fee'];
            $homeDelivery->save();

            $homeDeliveryService->scheduleHomeDelivery($homeDelivery);

            $data = array();
            $data['homeDelivery'] = $homeDelivery->to_array();

            output_ajax_success_response($data, "Delivery scheduled for order {$order->orderID}");
        } catch (Exception $e) {
            output_ajax_error_response($e->getMessage());
        }
    }

    /**
     * Finds or creates a HomePickup object for the customer
     *
     * @param object $customer
     * @param int $location_id
     * @return App\Libraries\DroplockerObjects\OrderHomePickup
     */
    protected function getHomePickup($customer, $location_id)
    {
        $service = get_business_meta($this->business_id, 'home_delivery_service');

        $homePickup = OrderHomePickup::search(array(
            'claim_id' => 0,
            'location_id' => $location_id,
            'customer_id' => $customer->customerID,
            'service' => $service,
        ));

        if (!$homePickup) {
            $homePickup = new OrderHomePickup();
        }

        $homePickup->business_id = $this->business_id;
        $homePickup->customer_id = $customer->customerID;
        $homePickup->order_id    = 0;
        $homePickup->location_id = $location_id;
        $homePickup->state       = $customer->state;
        $homePickup->city        = $customer->city;
        $homePickup->address1    = $customer->address1;
        $homePickup->address2    = $customer->address2;
        $homePickup->zip         = $customer->zip;
        $homePickup->lat         = $customer->lat;
        $homePickup->lon         = $customer->lon;
        $homePickup->service     = $service;
        $homePickup->save();

        return $homePickup;

    }

    /**
     * Finds or creates a HomeDelivery object for the customer
     *
     * @param object $customer
     * @param integer $location_id
     * @param integer $order_id
     * @return App\Libraries\DroplockerObjects\OrderHomeDelivery
     */
    protected function getHomeDelivery($customer, $location_id, $order_id)
    {
        $service = get_business_meta($this->business_id, 'home_delivery_service');

        $homeDelivery = OrderHomeDelivery::search(array(
            'order_id' => $order_id,
            'service'  => $service,
        ));

        if (!$homeDelivery) {
            $homeDelivery = new OrderHomeDelivery();
        }

        $homeDelivery->business_id = $this->business_id;
        $homeDelivery->customer_id = $customer->customerID;
        $homeDelivery->order_id    = $order_id;
        $homeDelivery->location_id = $location_id;
        $homeDelivery->state       = $customer->state;
        $homeDelivery->city        = $customer->city;
        $homeDelivery->address1    = $customer->address1;
        $homeDelivery->address2    = $customer->address2;
        $homeDelivery->zip         = $customer->zip;
        $homeDelivery->lat         = $customer->lat;
        $homeDelivery->lon         = $customer->lon;
        $homeDelivery->service     = $service;

        $homeDelivery->save();

        return $homeDelivery;
    }

    /**
     * Gets an array of all delivery windows sorted by ID
     *
     * @param \DropLocker\Service\HomeDelivery\IHomeDeliveryService $homeDeliveryService
     * @param object $homeDelivery
     * @return array
     */
    protected function getPickupWindowsByID($homeDeliveryService, $homePickup)
    {
        $windows_by_date = $homeDeliveryService->getPickupWindows($homePickup);

        $pickup_windows_by_id = array();
        foreach ($windows_by_date as $date => $windows) {
            foreach ($windows as $window) {
                $pickup_windows_by_id[$window['id']] = $window;
            }
        }

        return $pickup_windows_by_id;
    }

    /**
     * Gets an array of all delivery windows sorted by ID
     *
     * @param \DropLocker\Service\HomeDelivery\IHomeDeliveryService $homeDeliveryService
     * @param object $homeDelivery
     * @return array
     */
    protected function getDeliveryWindowsByID($homeDeliveryService, $homeDelivery)
    {
        $windows_by_date = $homeDeliveryService->getDeliveryWindows($homeDelivery);

        $delivery_windows_by_id = array();
        foreach ($windows_by_date as $date => $windows) {
            foreach ($windows as $window) {
                $delivery_windows_by_id[$window['id']] = $window;
            }
        }

        return $delivery_windows_by_id;
    }

     /**
     * Convert separate date and hour to mysql date
     *
     * @param date(y-m-d) $date
     * @param hour(H:i) $hour
     * @return mysql date
     */
    protected function convertToMysql($date, $hour)
    {
        $hour = trim($hour);
        
        //if $hour contains a date, discard it
        if (strpos($hour, ' ') !== FALSE) {
            $hourComps = split(' ', $hour);
            $hour = $hourComps[count($hourComps) - 1];
        }
        
        $date = date("Y-m-d", strtotime($date));
        return date("Y-m-d H:i:s", strtotime($date." ".$hour));
    }   
}