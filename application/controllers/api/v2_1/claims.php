<?php
use App\Libraries\DroplockerObjects\Claim;
class Claims extends MY_Api_Controller
{
    private $customerSession;
    public function __construct()
    {
        parent::__construct();
        $token = $this->input->get("sessionToken");
        if (!($this->customerSession = $this->api->validateSessionToken($token))) {
            output_ajax_error_response("Invalid session token");
        }
    }
    /**
     * Creates a new claim
     * Exects the following GET parameters
     *  lockerName
     *  orderType
     * Can take the following GET parameters
     *  orderNotes
     * Returns a JSON structure in the following format
     * {
     *      status : string success|error,
     *      message: string
     *      data : array()
     * }
     */
    public function create()
    {
        $validationResult = $this->validateRequest(array(
            "lockerName" => array("notEmpty"),
            "orderType" => array("notEmpty")
        ));
        if ($validationResult['status'] !== "success") {
            $this->outputJSONerror("The following validation errors occurred", $validationResult['errors']);
        }
        $this->load->model("customer_model");
        $customer = $this->customer_model->get_by_primary_key($this->customerSession->customer_id);
        $this->load->model("business_model");
        $business = $this->business_model->get_by_primary_key($this->customerSession->business_id);

        $this->db->join("location", "location.locationID=locker.location_id");
        $lockers = $this->db->get_where("locker", array("locker.lockerName" => $this->input->get("lockerName"), "location.business_id" => $this->customerSession->business_id))->result();

        if (isset($lockers[1])) {
            $this->outputJSONerror("There are multiple lockers with the locker name '{$this->input->get("lockerName")}' for business '$business->companyName' ($business->businessID)");
        }
        if (empty($lockers)) {
            $this->outputJSONerror("There are no lockers found for locker name '{$this->input->get("lockerName")} for business '$business->companyName' ($business->businessID)");
        }
        $locker = $lockers[0];
        $claimOptions = array();

        $orderNotes = $this->input->get("orderNotes");
        if (!empty($orderNotes)) {
            $claimOptions['notes'] = $orderNotes;
        }
        $claimOptions['service_type_serialized'] = serialize(explode(",", $this->input->get("orderType")));

        $claim = new Claim();
        $claimCreationResult = $claim->newClaim($locker->lockerID, $this->customerSession->customer_id, $this->customerSession->business_id, $claimOptions);

        switch ($claimCreationResult['status']) {
            case 'success':
                $this->outputJSONsuccess("Created new claim '{$claimCreationResult['claimID']}' for customer '".getPersonName($customer)."' ($customer->customerID) in locker '$locker->lockerName' ($locker->lockerID) in location '$locker->address' ($locker->locationID) for business '$business->companyName' ($business->businessID)");
                break;
            case 'error':
                $this->outputJSONerror("Could not create new claim", $claimCreationResult['message']);
            default:
                $this->outputJSONerror("An Unknown server side error occurred");
                break;
        }
    }
}
