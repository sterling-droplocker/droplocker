<?php
use App\Libraries\DroplockerObjects\Order as OrderObj;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\OrderStatus;
use App\Libraries\DroplockerObjects\OrderStatusOption;
use App\Libraries\DroplockerObjects\OrderPaymentStatusOption;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Picture;
use App\Libraries\DroplockerObjects\Barcode;

/**
 * Note, * incorrectly named this controller class a singular noun instead of a plural as specified in the coding standards.
 * The Order controller provides functionality to create a new order, retrieve the properties of an existing order, retireve the status history of an order, and to retrieve the status of an existing order
 */
class Orders extends MY_Api_Controller
{
    /**
     * @deprecated Use claims/create instead of this
     * The following function creates an claim .
     *
     * Expects the following GET parameters
     *  locker_id
     *  sessionToken
     *  orderType
     * Can take the following GET parameters
     *  orderNotes
     */
    public function create()
    {
        if (empty($this->data['data']['locker_id'])) {
            $this->api->responseError("Missing locker_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        $locker = new Locker($this->data['data']['locker_id'],FALSE);
        $location = new Location($locker->location_id, FALSE);

        if ($location->business_id != $session->business_id) {
            $this->api->responseError("The locker does not belong to the business");
        }

        if ($this->data['data']['orderType']=='') {
            $this->api->responseError("At least one orderType must be selected");
        }

        if (!empty($this->data['data']['orderNotes'])) {
            $options['notes'] = $this->data['data']['orderNotes'];
        }

        $orderTypeArray = explode(",", $this->data['data']['orderType']);
        $options['service_type_serialized'] = serialize($orderTypeArray);

        $claim = new Claim();
        $result = $claim->newClaim($this->data['data']['locker_id'], $session->customer_id, $session->business_id, $options);
        if ($result['status']=='success') {
            $this->api->response(array('claim_id'=>$result['claimID']));
        } else {
            $this->api->responseError(strip_tags($result['message']));
        }
    }

    /**
     * Retrieves the properties of an order, including items and charges..
     *
     * Expects the following GET parameters
     *  order_id int
     *  sessionToken string
     *
     * Returns the following JSON structure
        {
            status,
            message,
            data: {
                    order : {
                        order_id
                        bag_id
                        bagNumber
                        locationAddress
                        lockerName
                        orderType
                        netTotal
                        orderStatus
                        orderPaymentStatus
                        ticketNumber
                        dateCreated
                        tax
                    },
                    items : {
                        {
                            qty,
                            unitPrice,
                            subTotal,
                            item_id,
                            notes,
                            updated,
                            displayName
                            orderItemID
                        },
                        { ... },
                        ...
                    },
                    charges : {
                        {
                            chargeType
                            chargeAmount
                            updated
                            customerDiscount_id
                        },
                        { ... },
                        ...
                    }
                }
            }
     */
    public function details()
    {
        if (empty($this->data['data']['order_id'])) {
            $this->api->responseError("Missing order_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        $orderObj = new OrderObj($this->data['data']['order_id'], FALSE);
        $customer = new Customer($session->customer_id);

        $this->load->model("business_model");
        $business = $this->business_model->get_by_primary_key($customer->business_id);
        setlocale(LC_ALL, $business->locale);

        if ($customer->customerID != $orderObj->customer_id) {
            $this->api->responseError('This order does not belong to the customer');
        }
        $order = $this->_sanitizeOrder($orderObj, $customer);

        $items = @$orderObj->getItems();
        foreach ($items as $item) {
            $barcodes = Barcode::search_aprax(array('item_id'=>$item->item_id));
            if (empty($barcodes)) {
                $item->barcode = "";
            } else {
                $item->barcode = $barcodes[0]->barcode;
            }
            $item->unitPrice = format_money_symbol($customer->business_id, '%.2n', $item->unitPrice);
            $item->subTotal = format_money_symbol($customer->business_id, '%.2n', $item->subTotal);
            $item->notes = strip_tags($item->notes);
        }
        $order['items'] = $items;

        // get order charges
        $orderChargesArray = array();
        $charges = \App\Libraries\DroplockerObjects\OrderCharge::search_aprax(array('order_id'=>$orderObj->orderID));
        foreach ($charges as $orderCharge) {
            $charge = array('chargeType' => $orderCharge->chargeType,
                            'chargeAmount' => format_money_symbol($customer->business_id, '%.2n', $orderCharge->chargeAmount),
                            'updated' => $orderCharge->updated,
                            'customerDiscount_id' => $orderCharge->CustomerDiscount_id);
            $orderChargesArray[] = $charge;
        }
        $order['charges'] = $orderChargesArray;

        $this->api->output_api_success_response($order, "Order details for order ID {$orderObj->orderID}");
    }



    /**
     * The following function retrieves the status of an order
     * Expects the following parameters
     *  order_id
     *  sessionToken
     * Returns the following JSON structure
       {
           "orderStatus" : "<order status name>"
       }
     */
    public function status()
    {
        if (empty($this->data['data']['order_id'])) {
            $this->api->responseError("Missing  order_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        // if the orderID is not found the dropLockerObject with throw an exception. WE need to catch it for the api
        try {
            $order = new OrderObj($this->data['data']['order_id'], FALSE);
            $customer = new Customer($session->customer_id);
            if ($customer->customerID != $order->customer_id) {
                $this->api->responseError('This order does not belong to the customer');
            }

            $orderStatus = new OrderStatusOption($order->orderStatusOption_id, FALSE);
            $this->api->response(array('orderStatus'=>$orderStatus->name));
        } catch (Exception $e) {
            $this->api->responseError($e->getMessage());
        }
    }



    /**
     * The following function retrieves the order status history of an order.
     *
     * Expects the following GET parameters
     *  order_id
     *  sessionToken
     *
     * Returns the following JSON structure
       {
           {
               customer
               date
               name
               note
               lockerID
           },
           {
               ...
           },
           ...
       }
     */
    public function history()
    {
        if (empty($this->data['data']['order_id'])) {
            $this->api->responseError("Missing order_id");
        }

        $token = $this->data['data']['sessionToken'];
        if (!$session = $this->api->validateSessionToken($token)) {
            $this->api->responseError("Session not authenticated");
        }

        try {
            $order = new OrderObj($this->data['data']['order_id'], FALSE);
            $customer = new Customer($session->customer_id);
            if ($customer->customerID != $order->customer_id) {
                $this->api->responseError('This order does not belong to the customer');
            }

            $this->load->model('orderstatus_model');
            $orderStatus = $this->orderstatus_model->getHistory($order->orderID, 1);
            $this->api->response($orderStatus);
        } catch (Exception $e) {
            $this->api->responseError($e->getMessage());
        }
    }

    /**
     * The following function is unknown
     * @param unknonw $order
     * @param unknown $customer
     * @return unknown
     */
    public function _sanitizeOrder($order, $customer='')
    {
        $this->load->model('order_model');
        $orderStatus = new OrderStatusOption($order->orderStatusOption_id, FALSE);
        $orderPaymentStatus = new OrderPaymentStatusOption($order->orderPaymentStatusOption_id, FALSE);
        $bag = new Bag($order->bag_id, FALSE);
        $locker = new Locker($order->locker_id, FALSE);
        $location = new Location($locker->location_id, FALSE);
        $results = array();

        $results['order']['order_id'] = $order->orderID;
        $results['order']['bag_id'] = $bag->bagID;
        $results['order']['bagNumber'] = $bag->bagNumber;
        $results['order']['locationAddress'] = $location->address;
        $results['order']['orderType'] = $this->order_model->getOrderType($order->orderID);

        $is_unpaid = in_array($order->orderStatusOption_id, array(7, 13));
        if ($is_unpaid) {
            $results['order']['lockerName'] = '';
            $results['order']['accessCode'] = '';
        } else {
            $results['order']['lockerName'] = $locker->lockerName;

            if (!empty($customer)) {
                $results['order']['accessCode'] = substr(trim($customer->phone), -4);
            }
        }

        if(!empty($customer)) {
            $business_id = $customer->business_id;
        } else {
            $business_id = '';
        }

        $results['order']['netTotal'] = format_money_symbol($business_id, '%.2n', $this->order_model->get_net_total($order->orderID));

        $results['order']['orderStatus'] = $orderStatus->name;
        $results['order']['orderPaymentStatus'] = $orderPaymentStatus->name;
        $results['order']['ticketNumber'] = $order->ticketNum;
        $results['order']['dateCreated'] = $order->dateCreated;
        $results['order']['tax'] = format_money_symbol($business_id, '%.2n', $order->tax);

        return $results;
    }

}
