<?php
class utility extends CI_Controller
{
    /**
     * Expects the following POST parameters
     * lineNumber
     * message
     * url
     */
    public function report_javascript_error()
    {
        $this->load->helper("utility_helper");
        $lineNumber = $this->input->post("lineNumber");
        $message = $this->input->post("message");
        $url = $this->input->post("url");
        $browser = $this->input->post("browser");
        $body = "
            <pre>
                Line Number : $lineNumber
                Message: $message
                URL: $url
                Browser: $browser
            </pre>

            <pre>
                Client IP Address: {$this->session->userdata['ip_address']}
            </pre>
        ";
        if (isset($this->customer_id)) {
            $this->load->model("customer_model");
            $customers = $this->customer_model->get_customer($this->customer_id);
            $customer = $customers[0];

            $body .= "
                <pre>
                    Customer Information

                        Name: ".  getPersonName($customer)."

                        Phone: {$customer->phone}

                        SMS: {$customer->sms}

                        E-mail: {$customer->email}

                        Customer Address: {$customer->address1}, {$customer->city} {$customer->state} {$customer->zip}

                </pre>
            ";
        }
        if (isset($this->business)) {
            $body .= "
                <pre>
                    Business Information

                        Name: {$this->business->companyName}

                        Website: {$this->business->website_url}
                </pre>
            ";
        }
        if (isset($this->buser_id)) {
            $employeeID = get_employee_id($this->buser_id);

            $this->load->model("employee_model");

            $employee = $this->employee_model->get_by_id($employeeID);

            $body .= "
                <pre>
                    Employee Information

                        Name : ".  getPersonName($employee)."

                        Email: {$employee->email}
                </pre>";
        }

        if (!DEV) {
            queueEmail(SYSTEMEMAIL, SYSTEMFROMNAME, DEVEMAIL, "Javascript Error Report", $body, array(),'', 3);
        }
        echo "Reported Javascript error.";
    }
}
