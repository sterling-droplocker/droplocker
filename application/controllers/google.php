<?php
use App\Libraries\DroplockerObjects\Customer;
class Google extends CI_Controller
{
    public function get_customer_link_by_email()
    {
        try {

            if (!$this->input->post("email")) {
                throw new \InvalidArgumentException("'email' must be passed as a POST parameter.");
            }
            $email = $this->input->post("email");

            $result = array();
            $customers = Customer::search_aprax(array("email" => $email), false);
            if (!empty($customers)) {
                $customer = $customers[0];
                $customerID = $customer->{Customer::$primary_key};
                $result['name'] = getPersonName($customer);
                $result['link'] = "http://droplocker.com/admin/customers/detail/$customerID";
            }
            echo json_encode($result);
        } catch (\InvalidArgumentException $e) {
            echo json_encode(array("status" => "error", "message" => $e->getMessage()));
            send_exception_report($e);
        } catch (\Exception $e) {
            echo json_encode(array("status" => "error", "message" => "System Error"));
            send_exception_report($e);

        }
    }
}
