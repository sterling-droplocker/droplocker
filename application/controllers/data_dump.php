<?php
class data_dump extends My_Controller
{
    /**
     * Expects the following POST parameters
     * lineNumber
     * message
     * url
     */

    private $business_id;
    private $records;
    public $business;
    private $reporting_database;

    public function __construct()
    {
        parent::__construct();

        // load reporting databse
        $this->reporting_database = $this->load->database("reporting", true);
    }


    public function zoho()
    {
        $tables = array('claims', 'customers', 'orders', 'order_items', 'order_charges', 'driver_notes', 'locations','timeCards');

        $table = $this->input->get('t');

        if (in_array($table, $tables)) {

            $hash = $this->input->get('bid');

            $r = $this->input->get('r');
            $this->records = isset($r) ? intval($r):5000;

            if ($this->records > 50000)  {
                $this->records = 50000;
            }

            $this->load->model('business_model');
            $this->business = $this->business_model->get_by_hash($hash);

            $this->business_id = $this->business->businessID;

            $this->$table();
        }

    }

     /**
     * Exports claim table
     */
    public function claims()
    {
        // convert datetime columns into local time
        $fields = $this->getFields('claim');
        $fields = implode(", ", $fields);

        $conditions = array("business_id = ?");
        $params = array($this->business_id);


        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM claim WHERE $conditions ORDER BY claimID DESC LIMIT $this->records";
        $this->dumpQuery($sql, $params, 'claim', 'Claims');
    }

    /**
     * Exports reports_customer table
     */
    public function customers()
    {
        // convert datetime columns into local time
        $fields = $this->getFields('reports_customer');


        $fields = implode(", ", $fields);

        $conditions = array("business_id = ?");
        $params = array($this->business_id);

        /*if (!empty($_GET['from'])) {
            $conditions[] = "signupDate >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "signupDate <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }*/

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM reports_customer WHERE business_id = $this->business_id ORDER BY customerID DESC LIMIT $this->records";



        $this->dumpQuery($sql, $params, 'customers', 'Customer');
    }

    /**
     * Exports report_orders table
     */
    public function orders()
    {
        // convert datetime columns into local time
        $fields = $this->getFields('report_orders');
        $fields[] = 'lat, lon';

        $fields = implode(", ", $fields);

        $conditions = array("report_orders.business_id = ?");
        $params = array($this->business_id);

        /*if (!empty($_GET['from'])) {
            $conditions[] = "report_orders.dateCreated >= ?";
            $date = $_GET['from'] . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($_GET['to'])) {
            $conditions[] = "report_orders.dateCreated <= ?";
            $date = $_GET['to'] . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }*/

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM report_orders
            LEFT JOIN location ON (location_id = locationID)
            WHERE $conditions ORDER BY orderID DESC LIMIT $this->records";

        $this->dumpQuery($sql, $params, 'orders', 'Order');
    }

    /**
     * Exports orderItem table
     */
    public function order_items()
    {
        $timezone = $this->business->timezone;

        $conditions = array("orders.business_id = ?");
        $params = array($this->business_id);

        $from = $this->input->get("from");
        if (!empty($from)) {
            $conditions[] = "orders.dateCreated >= ?";
            $date = $from . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $to = $this->input->get("to");
        if (!empty($to)) {
            $conditions[] = "orders.dateCreated <= ?";
            $date = $to . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);

        $sql = "SELECT orderItemID, order_id, qty, unitPrice, orderItem.notes AS itemNotes,
            convert_tz(orderItem.updated, 'GMT', '{$timezone}') AS updated,
            cost, product.productID as productID, product.name as productName, productType,
            productCategory.productCategoryID AS productCategoryID, productCategory.name AS productCategoryName,
            supplierID, business.companyName AS supplierCompanyName, location.companyName As locationName, location.locationID AS locationID, location.route_id AS routeID
        FROM orderItem
        JOIN orders ON (orderID = order_id)
        JOIN locker ON (lockerID = locker_id)
        JOIN location ON (locationID = location_ID)
        JOIN product_process ON (product_processID = product_process_id)
        JOIN product ON (productID = product_id)
        LEFT JOIN supplier ON (supplier_id = supplierID)
        JOIN productCategory ON (productCategoryID = productCategory_id)
        LEFT JOIN business ON (supplier.supplierBusiness_id = business.businessID)
        WHERE $conditions ORDER BY orderItemID DESC LIMIT $this->records";

        $this->dumpQuery($sql, $params, 'order_items', 'OrderItem');
    }

    /**
     * Exports orderCharge table
     */
    public function order_charges()
    {
        $timezone = $this->business->timezone;

        $conditions = array("orders.business_id = ?");
        $params = array($this->business_id);

        $from = $this->input->get("from");
        if (!empty($from)) {
            $conditions[] = "orders.dateCreated >= ?";
            $date = $from . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $to = $this->input->get("to");
        if (!empty($to)) {
            $conditions[] = "orders.dateCreated <= ?";
            $date = $to . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);

        $sql = "SELECT orderChargeID, orderCharge.order_id, chargeType, chargeAmount,
        convert_tz(orderCharge.updated, 'GMT', '{$timezone}') AS updated,
        customerDiscount_id,
        customerDiscount.amount AS discountAmount,
        customerDiscount.amountType AS discountAmountType,
        customerDiscount.amountApplied AS discountAmountApplied,
        customerDiscount.description AS discountDescription,
        customerDiscount.extendedDesc AS discountExtendedDescription,
        customerDiscount.couponCode AS discountcouponCode,
        convert_tz(customerDiscount.expireDate , 'GMT', '{$timezone}') AS discountExpireDate,
        customerID, customer.firstName,  customer.lastName,
        orders.closedGross,
        locationID,
        location.companyName AS locationName,
        location.address AS locationAddress
        FROM orderCharge
        JOIN orders ON (orderID = orderCharge.order_id)
        LEFT JOIN customerDiscount ON (customerDiscountID = customerDiscount_id)
        LEFT JOIN customer ON (customerID = orders.customer_id)
        LEFT JOIN locker ON (lockerID = orders.locker_id)
        LEFT JOIN location ON (locationID = locker.location_id)
        WHERE $conditions ORDER BY orderChargeID DESC LIMIT $this->records";

        $this->dumpQuery($sql, $params, 'order_items', 'OrderItem');
    }

    /**
     * Exports driverNotes table
     */
    public function driver_notes()
    {
        $timezone = $this->business->timezone;

        $conditions = array("driverNotes.business_id = ?");
        $params = array($this->business_id);

        $from = $this->input->get("from");
        if (!empty($from)) {
            $conditions[] = "driverNotes.created >= ?";
            $date = $from . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $to = $this->input->get("to");
        if (!empty($to)) {
            $conditions[] = "driverNotes.created <= ?";
            $date = $to . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);

        $sql = "SELECT driverNotes.*, location.address, firstName, lastName,
        convert_tz(driverNotes.created, 'GMT', '{$timezone}') AS created,
        convert_tz(driverNotes.completed, 'GMT', '{$timezone}') AS completed
        FROM driverNotes
        JOIN location ON (location_id = locationID)
        JOIN employee ON (employeeID = completedBy)
        WHERE $conditions ORDER BY driverNotesID DESC LIMIT $this->records";

        $this->dumpQuery($sql, $params, 'order_items', 'OrderItem');
    }

     /**
     * Exports locations data
     */
    public function locations()
    {
        $selectedGroup = "month";
        if ($this->input->get("grouping")) {
            $selectedGroup = $this->input->get("grouping");
        }
        $data['grouping'] = $selectedGroup;

        $data['months'] = array_combine(range(1, 12), range(1, 12));
        $data['years'] = array_combine(range(date('Y'), 2000, -1), range(date('Y'), 2000, -1));
        
        $time = mktime(0, 0, 0, date('m') - 5, 1, date('Y'));
        $data['end_month'] = date('m');
        $data['end_year'] = date('Y');
        $data['start_month'] = date('m', $time);
        $data['start_year'] = date('Y', $time);

        if ($this->input->get()) {
            $data['start_month'] = $this->input->get('start_month');
            $data['start_year'] = $this->input->get('start_year');
            $data['end_month'] = $this->input->get('end_month');
            $data['end_year'] = $this->input->get('end_year');
        }

        // calculate the number of months
        $numMonths = $data['end_year'] * 12 + $data['end_month']
                    - 12 * $data['start_year'] - $data['start_month'] + 1;


        $increment = 1;
        if ($selectedGroup == 'quarter') {
            $increment = 3;
        } else if ($selectedGroup == 'week') {
            $numMonths = $numMonths * 4;
        }

        $data['increment'] = $increment;
        $data['numMonths'] = $numMonths;

        $this->load->model("data_dump_model");
        $data['locList'] = $this->input->post('locList');
        $results = $this->data_dump_model->dump_by_location($data, $this->business);

        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($results);
    }

    /**
     * Exports time cards table
     */
    public function timeCards()
    {
        // convert datetime columns into local time
        $fields = array("timeCard.weekOf", "employee.firstName", "employee.lastName", "timeCardDetail.day", "timeCardDetail.timeInOut", "timeCardDetail.in_out", "timeCardDetail.note");
        $fields = implode(", ", $fields);

        $conditions = array(
                "timeCardDetail.type = ?",
                "business_employee.business_id = ?"
            );

        $params = array('Regular', $this->business_id);

        if (!empty($this->input->get("from"))) {
            $conditions[] = "date(timeCardDetail.day) >= ?";
            $date = $this->input->get("from") . " 00:00:00";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        if (!empty($this->input->get("to"))) {
            $conditions[] = "date(timeCardDetail.day) <= ?";
            $date = $this->input->get("to") . " 23:59:59";
            $params[] = convert_to_gmt_aprax($date, $this->business->timezone);
        }

        $conditions = implode(" AND ", $conditions);
        $sql = "SELECT $fields FROM timeCard 
            JOIN employee ON employee.employeeID = timeCard.employee_id
            JOIN timeCardDetail ON timeCardDetail.timeCard_id = timeCard.timeCardID
            JOIN business_employee ON business_employee.employee_id = employee.employeeID
            WHERE $conditions";
        $this->dumpQuery($sql, $params, 'timeCard', 'Time Cards');
    }
    
    /**
     * Returns an array of the columns for the table with dates converted into local timezone
     *
     * @param string $table
     * @return array
     */
    protected function getFields($table)
    {
        $timezone = $this->business->timezone;

        $fields = array();
        foreach ($this->reporting_database->field_data($table) as $column) {
            if (in_array($column->type, array("datetime", "timestamp"))) {
                $fields[] = "convert_tz({$table}.{$column->name}, 'GMT', '{$timezone}') AS {$column->name}";
                continue;
            }

            $fields[] = "{$table}.{$column->name}";
        }

        return $fields;
    }

    protected function dumpQuery($sql, $params = array())
    {
        set_time_limit(0); //dump can take long

        $query = $this->db->query($sql, $params);


        header("Content-Type: application/json; charset=utf-8");
        $this->exportJSON($query);
    }


     /* Exports a query in JSON format
     *
     * @param CI_DB_mysql_result $query
     */
    protected function exportJSON($query)
    {
        if (empty($query)) {
            return false;
        }

        echo "[\n";

        for($i = 0 ; $i < $query->num_rows; $i++) {
            $row = $query->row_array($i);
            echo $i ? ",\n " : " ";
            echo json_encode($row);
        }

        echo "\n]";

        return $query->num_rows;
    }


}
