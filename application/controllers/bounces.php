<?php
use App\Libraries\DroplockerObjects\Customer;
class Bounces extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $myFile = "/tmp/sns.txt";
        $fh = fopen($myFile, 'a') or die("can't open file");

        $post = file_get_contents('php://input');


        $sns = json_decode($post);


        /**
         * handle subscriptions
         */
        if ($sns->Type == "SubscriptionConfirmation") {
            $this->_validate($sns->SubscribeURL);

            return TRUE;
        }


        /**
         * handle bounces and complaints
         */
        if ($sns->Type == "Notification") {
            $message = json_decode($sns->Message);

            if ($message->notificationType == "Bounce") {

                foreach ($message->bounce->bouncedRecipients as $email) {

                    //$bounces = $this->db->get_where('bounce', array('email'=>$email->emailAddress))->result();
                    $customerEmails = Customer::search_aprax(array('email'=>$email->emailAddress));
                    if ($customerEmails) {
                        foreach ($customerEmails as $customer) {
                            $sql = "UPDATE customer set noEmail = 1, bounced = 1 where customerID = '{$customer->customerID}'";
                            $this->db->query($sql);
                            $sql = "INSERT INTO customerHistory (customer_id, business_id, historyType, note) VALUES ({$customer->customerID}, {$customer->business_id}, 'Internal Note', 'Email address bounced. Customer will not receive any marketing emails. [noEmail=1]')";
                            $this->db->query($sql);
                        }

                    }

                    $customer = $customerEmails[0];
                    $options['customer_id'] = $customer->customerID;
                    $options['email'] = $email->emailAddress;
                    $options['diagnosticCode'] = (empty($email->diagnosticCode))?"Diagnostic code was not returned":$email->diagnosticCode;
                    $options['bounceType'] = $message->bounce->bounceType;
                    $options['bounceSubType'] = $message->bounce->bounceSubType;
                    $options['status'] = $email->status;
                    $options['action'] = $email->action;
                    $this->db->insert('bounce', $options);

                    //$this->db->update('customer', array('bounced'=>1, 'bouncedDate'=>date("Y-m-d H:i:s")), array('email'=>$email->emailAddress));
                    fwrite($fh, $email->emailAddress."\n");
                }

            }

            if ($message->notificationType == "Complaint") {
                //print_r($message);
                foreach ($message->complaint->complainedRecipients as $email) {
                    fwrite($fh, 'Complaint: '.$email->emailAddress."\n");
                }

            }
        }


        fclose($fh);

    }

    public function _validate($url)
    {
        file_get_contents($url);
    }
}
