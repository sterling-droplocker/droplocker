<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\UpchargeGroup;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;

/**
 * Description of PosApi
 *
 * @author leo
 */
class PosApi extends MY_Mobile_Api_Controller
{
    protected $app = 'pos';
    protected $order_source = '';

    function __construct(){
        parent::__construct();
        $this->order_source = Order::SOURCE_POS_APP;
    }

    protected function validateLoginData($employeeData)
    {
        $cash_register_id = $this->params['cash_register_id'];
        
        $this->load->model('cash_register_record_model');
        $this->cash_register_record_model->login(array('amount'=>0), $cash_register_id, $employeeData);
        
        if (count($employeeData->business->locations) < 2) {
            $message = get_translation(
                "no_pos_locations", 
                "pos/login", 
                array('business_id' => $employeeData->business_id), 
                "No POS-enabled locations found on business %business_id%. In order to POS-enable a location, go to DLA and setup at least one cash register for the location.");
            throw new Exception($message);
        }
    }

    public function onLogOut()
    {
        $cash_register_id = $this->params['cash_register_id'];
        
        $this->load->model('cash_register_record_model');
        $this->cash_register_record_model->logout(array('amount'=>0), $cash_register_id, $this->employee);
    }
    
    public function customer_autocomplete()
    {
        try {
            $this->checkRequiredParameters(array(
                'term',
            ));

            $this->loadSession();

            //-----

            $this->load->model('customer_model');
            $customers = $this->customer_model->autocomplete_full($this->params['term'], $this->business_id);

            $data = array(
                'customers' => $this->formatCustomers($customers)
            );

            output_ajax_success_response($data, count($data['customers']) . " results retrieved for '{$this->params['term']}'");
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function is_barcode_available()
    {
        try {
            $this->checkRequiredParameters(array(
                'barcode',
            ));

            $this->loadSession();

            //-----

            $this->load->model('barcode_model');
            $items = $this->barcode_model->getBarcode($this->params['barcode'], $this->business_id);
            
            if ($items) {
                output_ajax_error_response(get_translation("barcode_not_available", "pos/sale_item_add", array('barcode' => $this->params['barcode']), "Barcode %barcode% is already in use, please select a different one"));
            } else {
                output_ajax_success_response(array(), get_translation("barcode_available", "pos/sale_item_add", array('barcode' => $this->params['barcode']), "Barcode %barcode% is available"));
            }
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    protected function formatCustomers($customers) {
        $formatted = array();
        
        foreach ($customers as $customer) {
            $formatted[] = array(
                "id" => $customer->customerID, 
                "label" => "{$customer->firstName} {$customer->lastName} ({$customer->customerID} - {$customer->address1})",
                "email"=>"{$customer->email}",
                "phone"=>"{$customer->phone}",
                "customerID"=>"{$customer->customerID}"
            );
        }
        
        return $formatted;
    }
    
    /**
     * first step to create order for customer:
     *   create a new bag for customer 
     */
    public function create_bag_for_customer()
    {
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
            ));

            $this->checkAcl(array(
                'add_order',
            ));

            $customer = $this->getCustomerData($this->params['customer_id'], $this->business_id);

            //-----

            $this->load->model('bag_model');
            $bagNumber = $this->bag_model->newBagForCustomer($this->params['customer_id'], $this->business_id);

            $result = array(
                'bag_number' => $bagNumber,
                'customer_id' => $this->params['customer_id'],
                'customer' => $customer,
           );

            output_ajax_success_response($result, "A new bag with number $bagNumber was added to {$customer['firstName']} {$customer['lastName']}'s account.");
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }

    public function get_customer()
    {
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
            ));

            $this->loadSession();

            $customer = $this->getCustomerData($this->params['customer_id'], $this->business_id);

            $result = array(
                'customer_id' => $this->params['customer_id'],
                'customer' => $customer,
           );

            output_ajax_success_response($result, "Data for customer {$customer['firstName']} {$customer['lastName']} retrieved.");
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    protected function getCustomerData($customer_id, $business_id)
    {
        $customer = $this->checkCustomer($customer_id, $business_id);

        $this->load->model('customerpreference_model');
        $customer->product_preferences = array_change_key_case($this->customerpreference_model->get_preferences($customer_id));

        $customer_for_ui = $customer->to_array();
        $customer_for_ui['emailWindowStart'] = ($customer_for_ui['emailWindowStart'] == '00:00:00') ? 0 : convert_from_gmt_aprax($customer_for_ui['emailWindowStart'], "G", $business_id);
        $customer_for_ui['emailWindowStop'] = ($customer_for_ui['emailWindowStop'] == '00:00:00') ? 24 : convert_from_gmt_aprax($customer_for_ui['emailWindowStop'], "G", $business_id);
        if ($customer_for_ui['emailWindowStop'] == 0) {
            $customer_for_ui['emailWindowStop'] = 24;
        }

        $this->load->model('barcode_model');
        $customer_for_ui['barcode'] = $this->barcode_model->getBarcodesForCustomer($customer_id, $business_id);

        return $customer_for_ui;
    }

    protected function checkCustomer($customer_id, $business_id) {
        $this->load->model('customer_model');
        $result = $this->customer_model->getCustomerForBusiness($customer_id, $business_id);
        
        if (!$result['success']) {
            output_ajax_error_response($result['message']);
        }
        
        return $result['data'];
    }

    /**
     * first step to create order for customer:
     *   create a new customer
     */
    public function create_customer()
    {
        try {
            $this->loadEmployee();

            $this->loadDefaults(array(
                'firstName' => 'Anonymous',
                'lastName' => 'Customer',
                'phone' => '',
                'sms' => '',
                'promo_code' => null,
                'email' => 'temp' . time() . '@' . $this->business->website_url,
            ));

            //-----
            
            $customer_id = $this->params['customerID'];

            // check existent email
            $existentCustomer = Customer::search(array(
                'business_id' => $this->business_id,
                'email' => $this->params['email'],
            ));

            if ($existentCustomer && $existentCustomer->customerID != $customer_id) {
                $message = "Customer #{$existentCustomer->customerID} has already registered email {$this->params['email']}";
                output_ajax_error_response($message);
            }
            
            $this->load->model('customer_model');
            if ( ! $customer_id ) {
                //create customer
                $password = md5(uniqid());
                $how = "POS signup";
                $signup = "Mobile POS";

                $customer_id = $this->customer_model->new_customer($this->params['email'], $password, $this->business_id, $how, $signup);
                if (!$customer_id) {
                    $message = "An error occured creating the customer account.";
                    output_ajax_error_response($message);
                }

                //save default_business_language_id
                $customer = new Customer($customer_id);
                $customer->default_business_language_id = $this->business->default_business_language_id;
                $customer->save();
                
                $message = "A new customer account has been created for {$customer->firstName} {$customer->lastName} (#$customer_id).";
            } else {
                $customer = new Customer($customer_id);
                $message = "Data has been modified for {$customer->firstName} {$customer->lastName} (#$customer_id).";
            }

            // copy phone to sms
            if (!empty($this->params['copy'])) {
                $this->params['sms'] = $this->params['phone'];
            }

            if ($this->params['emailWindowStart'] == 0 && $this->params['emailWindowStop'] == 24) {
                $this->params['emailWindowStart'] = "00:00:00";
                $this->params['emailWindowStop'] = "00:00:00";
            } else {
                $this->params['emailWindowStart'] = convert_to_gmt_aprax("{$this->params['emailWindowStart']}:00:00", $this->business->timezone, "H:i:s");
                $this->params['emailWindowStop'] = convert_to_gmt_aprax("{$this->params['emailWindowStop']}:00:00", $this->business->timezone, "H:i:s");
            }

            // save customer first and last names
            $this->customer_model->save(array(
                'customerID' => $customer_id,
                'firstName'  => $this->params['firstName'],
                'lastName'   => $this->params['lastName'],
                'sms'        => $this->params['sms'],
                'phone'      => $this->params['phone'],
                'noEmail'      => $this->params['noEmail'],
                'emailWindowStart' => $this->params['emailWindowStart'],
                'emailWindowStop' => $this->params['emailWindowStop'],
            ));

            //apply promo code
            if ($this->params['promo_code']) {
                $this->load->library('coupon');
                $response = $this->coupon->applyCoupon($this->params['promo_code'], $customer_id, $this->business_id);
            }

            $customer = $this->getCustomerData($customer_id, $this->business_id);

            $result = array(
                'customer_id' => $customer_id,
                'customer' => $customer,
            );

            output_ajax_success_response($result, $message);
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function update_customer_internal_notes()
    {
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
            ));
            
            $this->loadEmployee();

            //-----
            
            $customer_id = $this->params['customer_id'];
            
            $this->load->model('customer_model');
            $res = $this->customer_model->update_internalNotes($customer_id, $this->params['notes'], $this->business_id);

            $customer = $this->getCustomerData($customer_id, $this->business_id);

            $result = array(
                'customer_id' => $customer_id,
                'customer' => $customer,
            );

            output_ajax_success_response($result, "Internal notes updated for {$customer['firstName']} {$customer['lastName']} (#$customer_id).");
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function update_customer_notes()
    {
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
            ));
            
            $this->loadEmployee();

            //-----
            
            $customer_id = $this->params['customer_id'];
            $this->checkCustomer($customer_id, $this->business_id);
            
            $this->load->model('customer_model');
            $res = $this->customer_model->update_customerNotes($customer_id, $this->params['notes']);

            $customer = $this->getCustomerData($customer_id, $this->business_id);

            $result = array(
                'customer_id' => $customer_id,
                'customer' => $customer,
            );

            output_ajax_success_response($result, "Customer notes updated for {$customer['firstName']} {$customer['lastName']} (#$customer_id).");
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function update_laundry_preference()
    {
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
                'product_id',
            ));
            
            $this->loadEmployee();

            //-----
            
            $customer_id = $this->params['customer_id'];
            $this->checkCustomer($customer_id, $this->business_id);
            
            $product_id = $this->params["product_id"];

            // check the product exists
            $this->load->model("product_model");
            $product = $this->product_model->get_by_primary_key($product_id);
            if (empty($product)) {
                throw new Exception("product ID '$product_id' does not exist");
            }

            $this->load->model("productCategory_model");
            $productCategory = $this->productCategory_model->get_by_primary_key($product->productCategory_id);

            // get the customer preferences
            $this->load->model('customerpreference_model');
            $preferences = $this->customerpreference_model->get_preferences($customer_id);

            // save the new value
            $preferences[$productCategory->slug] = $product_id;
            $this->customerpreference_model->save_preferences($customer_id, $preferences);

            $message = "Updated preference to product '{$product->name}'";

            $customer = $this->getCustomerData($customer_id, $this->business_id);

            $result = array(
                'customer_id' => $customer_id,
                'customer' => $customer,
            );

            output_ajax_success_response($result, $message);
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function create_order_for_customer()
    {
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
                'location_id',
            ));

            //side effect: calls loadEmployee()
            $this->checkAcl(array(
                'add_order',
            ));

            $due_date = null;
            $notes = "";
            $source = $this->order_source;

            //-----

            $this->load->model('order_model');
            $result = $this->order_model->create_order_for_customer($this->params['customer_id'], $this->business_id, $this->params['location_id'], $this->employee_id, $due_date, $notes, $source);
            $message = $result['message'];

            if ($result['success']) {
                $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $result['order_id']);
                if (!$orderResult['success']) {
                    $message .= ". {$orderResult['message']}";
                    output_ajax_error_response($message);
                }

                $result['order'] = $orderResult['data'];

                output_ajax_success_response($result, $message);
            } else {
                output_ajax_error_response($message);
            }
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function create_order_by_bag()
    {
        try {
            $this->checkRequiredParameters(array(
                'bag_number',
                'location_id',
            ));

            //side effect: calls loadEmployee()
            $this->checkAcl(array(
                'add_order',
            ));
            
            $due_date = null;
            $notes = "";
            $source = $this->order_source;

            //-----

            $this->load->model('order_model');
            $result = $this->order_model->newOrderFromBag(ltrim($this->params['bag_number'], "0"), $this->params['location_id'], $this->business_id, $this->employee_id, $due_date, $notes, $source);
            $message = $result['message'];

            if ($result['success']) {
                $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $result['order_id']);
                if (!$orderResult['success']) {
                    $message .= ". {$orderResult['message']}";
                    output_ajax_error_response($message);
                }

                $result['order'] = $orderResult['data'];

                output_ajax_success_response($result, $message);
            } else {
                output_ajax_error_response($message);
            }
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    public function get_order_info()
    {
        try {
            $this->checkRequiredParameters(array(
                'order_id',
            ));

            $this->loadEmployee();

            //-----

            $this->load->model('order_model');
            $result = $this->order_model->getOrderDetails($this->business, $this->employee, $this->params['order_id']);

            if ($result['success']) {
                $res = array(
                    'order_id' => $result['data']['order_id'],
                    'order' => $result['data'],
                );
                output_ajax_success_response($res, "Order {$res['order_id']} found");
            } else {
                output_ajax_error_response($result['message']);
            }
        } catch (Exception $exc) {
            output_ajax_error_response($exc->getMessage());
        }
    }
    
    protected function getBusinessData($business_id) 
    {
        $business = parent::getBusinessData($business_id);
        $business_id = $business->businessID;
        $business_languageID = get_customer_business_language_id();
        
        $this->load->model('customoption_model');
        $customOptions = $this->customoption_model->getCustomOptions($business_id, $business_languageID);
         
        $business->displayColorOptions = get_business_meta($business_id, 'displayColorOptions');
        if ($business->displayColorOptions) {
            // use custom colors or defaults if not set
            if (isset($customOptions['color_options'])) {
                foreach ($customOptions['color_options']->values as $colorValue) {
                    $colors[] = array(
                        'id' => $colorValue->value,
                        'label' => $colorValue->name,
                    );
                }
            } else {
                $colors = array(
                    array(
                        'id' => 'white',
                        'label' => 'White',
                    ),
                    array(
                        'id' => 'black',
                        'label' => 'Black',
                    ),
                    array(
                        'id' => 'red',
                        'label' => 'Red',
                    ),
                    array(
                        'id' => 'blue',
                        'label' => 'Blue',
                    ),
                );
            }
            $business->colors = $colors;
        } 
        unset($customOptions['color_options']);
        
        // intercept brand options
        $business->displayBrand = get_business_meta($business_id, 'displayBrand');
        if ($business->displayBrand) {
            $brands = array();
        
            if (isset($customOptions['brand_options'])) {
                foreach ($customOptions['brand_options']->values as $brandValue) {
                    $brands[] = array(
                        'id' => $brandValue->value,
                        'label' => $brandValue->name,
                    );
                }
            }
            $business->brands = $brands;
        }
        unset($customOptions['brand_options']);
        
        $business->customOptions = $customOptions;
        
        //get the business specials
        $this->load->model('specification_model');
        $specials = $this->specification_model->get_aprax(array(
            'business_id' => $business_id
        ), 'description');
        foreach ($specials as $key => $value) {
            $specials[$key]->id = $value->specificationID;
            $specials[$key]->label = $value->description;
        }
        $business->specials = $specials;

        $productCategoriesList = array();
        $this->load->model('productcategory_model');
        $productCategories = $this->productcategory_model->get_all_as_codeigniter_dropdown($business_id);
        foreach ($productCategories as $key => $value) {
            if ($key == 'show_all') {
                $key = null;
                $value = get_translation("select_all_products", "pos/drop_off", array(), "-- All Products --");
            }
            
            $productCategoriesList[] = array(
                'id' => $key,
                'label' => $value,
            );
        }
        $business->productCategories = $productCategoriesList;
        
        $business->product_image_path = Product::$product_image_path;
        //get products for adding a new item
        // also used in item table
        $this->load->model('product_model');
        $product_process = $this->product_model->getBusinessProducts($business_id);
        
        $products = array();
        foreach ($product_process as $pp) {
            $id = $pp->productID;
            
            if (!$products[$id]) {
                $products[$id] = array_columns($pp, array(
                    'productID',
                    'displayName',
                    'name',
                    'productCategory_id',
                    'upchargeGroup_id',
                    'unitOfMeasurement',
                    'module',
                    'module_slug',
                    'module_name',
                    'image',
                ));
                
                $products[$id]['id'] = $id;
                $products[$id]['label'] = $products[$id]['name'];
                
            }
            
            $pp_data = array_columns($pp, array(
               'product_processID',
               'process_id',
               'price',
               'process_name',
               'supplier_id',
            ));
            
            if (!$pp_data['process_name']) {
                $pp_data['process_name'] = 'none';
            }
            
            $products[$id]['product_process'][] = $pp_data;
        }
        
        $business->products = $products;
        
        //get upcharge_groups
        $groups = array();
        $upchargeGroups = UpchargeGroup::search_aprax(array('business_id'=>$business_id));
        foreach ($upchargeGroups as $upchargeGroup) {
            $upchargeItems = $upchargeGroup->getGroupUpcharges();
            if(!empty($upchargeItems)) {
                $groups[$upchargeGroup->upchargeGroupID] = $upchargeItems;
            }
        }
        $business->upchargeGroups = $groups;
        
        // get wash locations for adding a new item
        // also used in the item table
        $this->load->model('supplier_model');
        $business->suppliers = array("" => "") + $this->supplier_model->simple_array($business_id);
        
        // get the quickitem buttons
        $this->load->model('quickitem_model');
        $buttons =  $this->quickitem_model->get_aprax(array(
            'business_id' => $business_id
        ));

        $business->quick_items = $buttons;

        $supports_mobile_cc_auth = false;
        $enable_cc = false;

        // check if the processor works from the mobile app
        $this->load->library('creditcardprocessor');
        $processor = get_business_meta($business_id, 'processor');
        if ($processor) {
            $creditCardProcessor = $this->creditcardprocessor->factory($processor, $business_id);
            $supports_mobile_cc_auth = $creditCardProcessor->supportsMobileAuth();

            if ($supports_mobile_cc_auth && stristr($processor, 'coinbase') === FALSE) {
                $enable_cc = true;
            }
        };
        $business->supports_mobile_cc_auth = $supports_mobile_cc_auth;
        $business->enable_cc = $enable_cc;
        
        $this->load->model("coupon_model");
        $business->coupons = $this->coupon_model->get_active_coupons_list($business_id);
        
        $this->load->model("process_model");
        $business->processes = $this->process_model->get_as_codeigniter_dropdown();
        
        $business->pos_product_images = get_business_meta($business_id, 'pos_product_images');
        $business->date_format = get_business_meta($business_id, "shortWDDateLocaleFormat",'%a %m/%d');
        $business->time_format = get_business_meta($business_id, "hourLocaleFormat",'%l:%M %p');
        $business->date_time_format = $business->date_format . " " . $business->time_format;
        $business->timezone_offset = convert_from_gmt_aprax(null, 'P', $business_id);
        
        $this->load->model('cash_register_model');
        $business->locations = $this->cash_register_model->getEnabledLocationsForSelect($business_id);
        foreach ($business->locations as $key => $location) {
            $cash_registers = $this->cash_register_model->getForLocation($location->id);
            array_unshift($cash_registers, array('cash_registerID' => 0, 'name' => get_translation("select_cash_register", "pos/cash_register", array(), "Select Cash Register")));
            
            $business->locations[$key]->cash_registers = $cash_registers;
        }

        $this->load->library('productslibrary');
        $business->product_preferences = $this->productslibrary->getBusinessPreferencesForAPI($business_id);

        return $business;
    }
    
    protected function getEmployeeData($employee_id) {
        $employee = parent::getEmployeeData($employee_id);
        
        $this->load->model('cash_register_record_model');
        $employee->is_pos_manager = $this->cash_register_record_model->is_pos_manager($employee);
        
        return $employee;
    }

    public function quick_drop() 
    {
        $success = false;
        $message = '';
        
        try {
            $this->loadEmployee();

            $this->checkRequiredParameters(array(
                'order_id',
                'customer_id'
            ));
            
            $order = new Order($this->params['order_id']);
            if ($order->business_id !== $this->business_id) {
                throw new Exception(get_translation("order_wrong_business", "pos/quick_drop", array(), "Order not found for business"));
            }
            
            if ($this->params['add_quick_drop_notes']) {
                $quick_drop_notes .= get_translation("receipt_quick_drop_notes", "pos/quick_drop", array(), "Quick Drop Notes") . ": \n";

                $quick_drop_notes .= get_translation("Cleaning Method", "pos/quick_drop", array(), "Cleaning Method") . " : ";
                $quick_drop_notes .= $this->params['cleaning_method'] . ", \n";

                $quick_drop_notes .= get_translation("Quantity", "pos/quick_drop", array(), "Quantity") . " : ";
                $quick_drop_notes .= $this->params['items'] . ", \n";

                $quick_drop_notes .= get_translation("Due Date", "pos/quick_drop", array(), "Due Date") . " : ";
                $quick_drop_notes .= $this->params['due_date'] . ", \n";

                $quick_drop_notes .= get_translation("Notes", "pos/quick_drop", array(), "Notes") . " : ";
                $quick_drop_notes .= $this->params['notes'] . "\n";

                $quick_drop_notes .= "---------------\n";
            
                $order->notes = $quick_drop_notes . $order->notes;

            } else if($this->params['notes']) {
                $order->notes = $this->params['notes'];
            };
            
            if ($this->params['due_date']) {
                //from orders->update
                date_default_timezone_set('UTC');
                $time = strtotime($this->params['due_date']);
                if ($time === false) {
                    throw new Exception(get_translation("invalid_due_date", "pos/quick_drop", array(), "Invalid due date"));
                } else {
                    $order->dueDate = gmdate('Y-m-d', $time);
                };
            }
            
            $order->save();
            
            $success = true;
            if ($this->params['add_quick_drop_notes']) {
                $message = get_translation("quick_drop_success", "pos/quick_drop", array('order_id' => $this->params['order_id']), "Quick drop info added to order %order_id%");
            } else {
                $message = get_translation("update_success", "pos/quick_drop", array('order_id' => $this->params['order_id']), "Updated order %order_id%");
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            $this->load->model('order_model');
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $this->params['order_id']);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                output_ajax_error_response($message);
             }

            $data = array(
                'order_id' => $res['order_id'],
                'order' => $orderResult['data'],
            );

            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    /*
     * based on method used on inline order detail add
     * **/
    public function add_product_to_order()
    {
        $success = false;
        $message = '';
        
        try {
            $this->loadEmployee();
            
            $this->checkRequiredParameters(array(
                'product_id',
                'qty',
                'customer_id',
                'location_id',
            ));
            
            $this->loadDefaults(array(
                'item_id' => 0,
                'process_id' => false,
                'source' => $this->order_source,
            ));
            
            $options = $this->params;
            
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;
            
            //-----------------------
            $this->load->model('order_model');
            $res = $this->order_model->add_product_to_order_or_create($options);
            $success = $res['success'];
            $message = $res['message'];
            
        } catch (Exception $e) {
            $message =  "Could not add item: {$e->getMessage()}";
        }
        
        if ($success) {
            $processResult = $this->process_order($res['order_id']);
            if ($processResult['success']) {
                $message .= $processResult['message'];
            } else {
                $warning .= $processResult['message'];
            }
            
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $res['order_id']);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                output_ajax_error_response($message);
             }

            $data = array(
                'order_id' => $res['order_id'],
                'order' => $orderResult['data'],
                'orderItemID' => $res['orderItemID'],
                '__warning' => $warning,
            );
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function delete_item()
    {
        $success = false;
        $message = '';
        
        try {
            $this->loadEmployee();

            $this->checkRequiredParameters(array(
                'orderItemID',
                'order_id',
            ));
            
            $this->load->model('orderitem_model');
            $res = $this->orderitem_model->delete_item_and_clean($this->params['orderItemID'], $this->params['order_id'], $this->employee_id);
            
            $success = $res['success'];
            $message = $res['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            $processResult = $this->process_order($this->params['order_id']);
            if ($processResult['success']) {
                $message .= $processResult['message'];
            } else {
                $warning .= $processResult['message'];
            }
            
            $this->load->model('order_model');
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $this->params['order_id']);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                output_ajax_error_response($message);
            }

            $data = array(
                'order_id' => $this->params['order_id'],
                'order' => $orderResult['data'],
                'orderItemID' => $this->params['orderItemID'],
                '__warning' => $warning,
            );

            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function print_bag_tag()
    {
        $success = true;
        $message = '';
        
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
                'bag_number',
            ));
            $this->loadEmployee();

            $customerID = $this->params['customer_id'];
            $bag_number = $this->params['bag_number'];
            $business_id = $this->business_id;
            $with_default_location = $this->params['with_default_location'];
            
            $this->load->model('bag_model');
            $data = $this->bag_model->print_bag_tag($customerID, $bag_number, $business_id, $with_default_location);
            $success = $data['success'];
            $message = $data['message'];
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }


    public function add_existent_item_to_order()
    {
        $success = false;
        $message = '';
        $error_code = '';
        
        try {
            $this->loadEmployee();
            
            $this->checkRequiredParameters(array(
                'barcode',
            ));
            
            $this->loadDefaults(array(
                'source' => $this->order_source,
            ));
            
            $options = $this->params;
            
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;
           
            //-----------------------------------------------
            $this->load->model('order_model');
            $res = $this->order_model->add_barcode_to_order_or_create($options);
            $success = $res['success'];
            $message = $res['message'];
            $order_id = $res['order_id'];
            
        } catch (Exception $e) {
            $message =  "Could not add item: {$e->getMessage()}";
        }
        
        if ($success) {
            $processResult = $this->process_order($order_id);
            if ($processResult['success']) {
                $message .= $processResult['message'];
            } else {
                $warning .= $processResult['message'];
            }
            
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $order_id);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                output_ajax_error_response($message);
             }

            $data = array(
                'order_id' => $order_id,
                'order' => $orderResult['data'],
                'barcode' => $this->params['barcode'],
                '__warning' => $warning,
            );
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function add_new_item_to_order()
    {
        $success = false;
        $message = '';
        $error_code = '';
        
        try {
            $this->loadEmployee();
            
            $this->checkRequiredParameters(array(
//                'barcode',
                'product_id',
            ));
            
            $this->loadDefaults(array(
                'source' => $this->order_source,
            ));
            
            //-----------------------------------------------
            $order_id = $this->params['order_id'];
            
            $this->load->model('order_model');
            $res = $this->order_model->createItem($this->params, $this->employee_id, $this->business_id);
            $success = $res['status'] === 'success';
            $message = $res['message'];
            
        } catch (Exception $e) {
            $message =  "Could not add item: {$e->getMessage()}";
        }
        
        if ($success) {
            $processResult = $this->process_order($order_id);
            if ($processResult['success']) {
                $message .= $processResult['message'];
            } else {
                $warning .= $processResult['message'];
            }
            
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $order_id);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                $this->output_ajax_error_response($message);
             }

            $data = array(
                'order_id' => $order_id,
                'order' => $orderResult['data'],
                'barcode' => $this->params['barcode'],
                '__warning' => $warning,
            );
            $this->output_ajax_success_response($data, $message);
        } else {
            $this->output_ajax_error_response($message);
        }
    }
    
    public function get_location_due_date()
    {
        $success = true;
        $message = '';
        
        try {
            $this->checkRequiredParameters(array(
                'location_id',
            ));
            $this->loadEmployee();
            $this->loadBusinessTime();

            $this->load->model('location_model');
            $data = $this->location_model->getDueDateForDay($this->params['location_id'], $this->business_id);
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function get_customer_orders_in_progress()
    {
        $success = false;
        $message = '';
        
        try {
            $this->loadEmployee();

            $this->checkRequiredParameters(array(
                'customer_id',
            ));
            
            $this->load->model('order_model');
            $data = $this->order_model->getCustomerOrdersInProgress($this->business, $this->employee, $this->params['customer_id']);
            $count = count($data);
            
            $success = true;
            $message = "Found $count orders in progress for customer #{$this->params['customer_id']} in business #{$this->business_id}";
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_customer_orders_completed($customer_id, $business_id, $limit = 20)
    {
        $success = false;
        $message = '';
        
        try {
            $this->loadEmployee();

            $this->checkRequiredParameters(array(
                'customer_id',
            ));
            
            $this->load->model('order_model');
            $data = $this->order_model->getCustomerOrdersCompleted($this->business, $this->employee, $this->params['customer_id']);
            $count = count($data);
            
            $success = true;
            $message = "Found $count orders completed for customer #{$this->params['customer_id']} in business #{$this->business_id}";
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_customer_orders_ready($customer_id, $business_id)
    {
        $success = false;
        $message = '';
        
        try {
            $this->loadEmployee();

            $this->checkRequiredParameters(array(
                'customer_id',
            ));
            
            $this->load->model('order_model');
            $data = $this->order_model->getCustomerOrdersReady($this->business, $this->employee, $this->params['customer_id']);
            $count = count($data);
            
            $success = true;
            $message = "Found $count orders ready for customer #{$this->params['customer_id']} in business #{$this->business_id}";
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function delete_order()
    {
        $success = false;
        $message = '';
        
        try {
            $this->checkRequiredParameters(array(
                'order_id',
            ));
            $this->loadEmployee();

            $this->load->model('order_model');
            $res = $this->order_model->gut_order($this->params['order_id'], $this->employee_id);
            
            $success = $res['success'];
            $message = $res['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $this->params['order_id']);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                output_ajax_error_response($message);
             }

            $data = array(
                'order_id' => $this->params['order_id'],
                'order' => $orderResult['data'],
            );
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function update_order_status()
    {
        $success = false;
        $message = '';
        
        try {
            $this->checkRequiredParameters(array(
                'order_id_list',
                'orderStatusOption_id',
            ));
            $this->loadEmployee();
            
            $this->load->model("order_model");
            $response = $this->order_model->update_all_statuses($this->params['order_id_list'], $this->params['orderStatusOption_id'], $this->employee_id, $this->business_id);

            $success = $response['success'];
            $message = $response['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($response, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function pay_all_with_cash()
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'order_id_list',
                'amount',
                'cash_register_id',
            ));
            
            $this->loadEmployee();
            
            $isPrePayment = true;
            $this->load->model('order_model');
            $result = $this->order_model->pay_all_with_cash($this->params['order_id_list'], $this->params['amount'], $this->employee_id, $isPrePayment);
            
            $success = $result['success'];
            $message = $result['message'];
            $data = $result['data'];
            
            if (count($data['paid_orders'])) {
                $cashRegisterMessage = "Orders paid with cash: " . implode(', ', $data['paid_orders']);
                
                $this->load->model('cash_register_record_model');
                $this->cash_register_record_model->addCashSale($data, $this->params['cash_register_id'], $this->employee_id, $cashRegisterMessage);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_closed_register_status() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
                'close_record_id',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->getClosedCashStatus($this->params['cash_register_id'], $this->employee_id, $this->params['close_record_id']);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function get_cash_register_status() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->getCurrentCashStatus($this->params['cash_register_id'], $this->employee_id);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function get_drawer_history() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->getCloseHistory($this->params['cash_register_id'], $this->employee);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_report() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
                'from_date',
                'to_date',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->getReport($this->params['cash_register_id'], $this->employee, $this->params);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_cash_register_orders() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_order_model');
            $data = $this->cash_register_record_order_model->getOrders(
                    $this->params['cash_register_id'], 
                    $this->employee, 
                    array (
                        'from_date' => $this->params['from_date'],
                        'to_date' => $this->params['to_date'],
                    ), 
                    $this->params['payment_method_slug'], 
                    $this->params['record_type_slug']);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_orders_receipts() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'order_id_list',
                'customer_id',
            ));
            
            $this->loadEmployee();
            
            $show_payment = TRUE;
            $this->load->library('printerlibrary');
            $data = $this->printerlibrary->get_orders_receipt_list($this->params['order_id_list'], $this->business_id, $this->params['customer_id'], array(), $show_payment);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function get_cash_register_reportable_dates() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
                'from_date',
                'to_date',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->getReportableDates($this->params['cash_register_id'], $this->employee, $this->params);
            
            $success = $data['success'];
            $message = $data['message'];
            $data = $data['data'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function refund_order() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
                'order_id',
                'amount',
                'paymentMethodSlug',
            ));
            
            $this->loadEmployee();
            
            $notes = $this->params['notes'];
            if ($notes) {
                $dla_notes = "POS notes: $notes.";
            } else {
                $dla_notes = "Requested from POS.";
            }

            $this->load->model('order_model');
            $success = $this->order_model->uncapture($this->params['order_id'], $this->employee_id, $dla_notes);
            
            if ($success) {
                $message = "Order {$this->params['order_id']} uncaptured successfuly.";
                $pos_notes = "$message $notes";
                
                $this->load->model('cash_register_record_model');
                $data = $this->cash_register_record_model->addRefund($this->params, $this->params['cash_register_id'], $this->employee, $this->params['paymentMethodSlug'], $pos_notes);
                $success = $data['success'];
                $message .= "\n " . $data['message'];
            } else {
                $message = "Could not uncapture order {$this->params['order_id']}}";
            }
        } catch (Exception $e) {
            $success = false;
            $message .= $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function start_drawer() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->startDrawer($this->params, $this->params['cash_register_id'], $this->employee);
            
            $success = $data['success'];
            $message = $data['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function end_drawer() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
            ));
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->endDrawer($this->params, $this->params['cash_register_id'], $this->employee);
            
            $success = $data['success'];
            $message = $data['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function open_drawer() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->checkRequiredParameters(array(
                'cash_register_id',
            ));
            
            $this->loadEmployee();
            
            //-----------------------------

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->openDrawer($this->params, $this->params['cash_register_id'], $this->employee);
            
            $success = $data['success'];
            $message = $data['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function switch_cash_register() 
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $allowFalsy = function($arg) {
                return !isset($arg);
            };
            
            $this->checkRequiredParameters(array(
                'old_cash_register_id',
                'new_cash_register_id',
            ), $allowFalsy);
            
            $this->loadEmployee();

            $this->load->model('cash_register_record_model');
            $data = $this->cash_register_record_model->switchCashRegister($this->params["old_cash_register_id"], $this->params["new_cash_register_id"], $this->employee);
            
            $success = $data['success'];
            $message = $data['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }

    public function updateBilling()
    {
        $success = false;
        $message = '';
        $data = array();
        
        try {
            $this->loadEmployee();
            
            $this->checkRequiredParameters(array(
                'customer_id',
                'cardNumber',
                'expireMonth',
                'expireYear',
                'csc',
            ));
            
            $customer = new Customer($this->params["customer_id"]);
        
            $this->loadDefaults(array(
                'firstName' => $customer->firstName, 
                'lastName' => $customer->lastName, 
                'address1' => $customer->address1, 
                'city' => $customer->city, 
                'state' => $customer->state, 
                'zip' => $customer->zip,
                'is_one_time' => 0,
            ));

            $testMode = NULL;
            $phone_number = NULL;

            $this->load->library("creditcard");
            $authorizationResult = $this->creditcard->authorize(
                $this->params["customer_id"],
                $this->params["kiosk_access"], //unused
                $this->params["cardNumber"],
                $this->params["expireMonth"],
                $this->params["expireYear"],
                $this->params["firstName"],
                $this->params["lastName"],
                $this->params["address1"],
                $this->params["city"],
                $this->params["state"],
                $this->params["zip"],
                null, //country code
                $this->params["csc"],
                $testMode,
                $phone_number,    
                $this->params["is_one_time"]    
            );

            $paidMessage = "";
            $errorMessage = "";
            if ($authorizationResult['status'] == 'success') {
                if (sizeof($authorizationResult['data']['captureResults']) > 0) {
                    foreach ($authorizationResult['data']['captureResults'] as $captureResult) {
                        if ($captureResult['status'] == 'success') {
                            $paidMessage .= "{$captureResult['order_id']} ";
                        } else {
                            $errorMessage .= "{$captureResult['order_id']} ";
                        }
                    }
                }
                if (empty($paidMessage)) {
                    $captureResult['paidOrders'] = null;
                } else {
                    $captureResult['paidOrders'] = "The following orders have been paid for: $paidMessage";
                }
                if (empty($errorMessage)) {
                    $captureResult['unpaidOrders'] = null;
                } else {
                    $captureResult['unpaidOrders'] = "The following order still need to be paid for: $errorMessage";
                }
            }
            
            $success = $authorizationResult['status'] == 'success';
            $message = $authorizationResult['message'];
            $data = $authorizationResult['data'];
            $data['customer'] = $this->getCustomerData($this->params["customer_id"], $this->business_id);
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }

        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
        
    }

    public function deleteCreditCard()
    {
        $success = false;
        $message = '';
        
        try {
            $this->checkRequiredParameters(array(
                'customer_id',
            ));
            
            $this->loadEmployee();
            
            $customer_id = $this->params["customer_id"];
            $business_id =  $this->business_id;
            $this->checkCustomer($customer_id, $business_id);
            $customer = new Customer($customer_id);

            $creditCard = CreditCard::search(array(
                "customer_id" => $customer_id
            ));

            if ($creditCard) {
                $processor = get_business_meta($business_id, 'processor');
                //this is required for the Transbank software audit, but we can remove it once approved.
                if ($processor == 'transbank') {
                    $transbank = new \DropLocker\Processor\Transbank(array('business_id' => $business_id));
                    if ($transbank->removeUser($creditCard)) {
                        $creditCard->delete();
                        $customer->autoPay = 0;
                        $customer->save();
                        
                        $success = true;
                        $message = 'Credit card removed';
                    } else {
                        $success = false;
                        $message = 'Transbank: Error when trying to remove credit card';
                    }
                } else {
                    $creditCard->delete();
                    $customer->autoPay = 0;
                    $customer->save();
                    
                    $success = true;
                    $message = get_translation("success", "business_account/profile_billing", array('cardNumber' => $creditCard->cardNumber), "Deleted card ending with %cardNumber%", $customer_id);
                }
            } else {
                $success = false;
                $message = get_translation("no_cc_on_file", "business_account/profile_billing", array(), "You have no credit cards on file", $customer_id);
            }
            
            $data = array (
                'customer' => $this->getCustomerData($customer_id, $business_id),
            );
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }
        
        
        if ($success) {
            output_ajax_success_response($data, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function capture_all()
    {
        $success = false;
        $message = '';
        
        try {
            $this->checkRequiredParameters(array(
                'order_id_list',
                'cash_register_id',
            ));
        
            $this->loadDefaults(array(
                'is_one_time' => 0,
            ));

            $this->loadEmployee();

            $isPrePayment = true;
            $this->load->model('order_model');
            $result = $this->order_model->captureMany($this->params['order_id_list'], $this->business_id, $this->employee_id, $isPrePayment, $this->params['is_one_time']);
            
            $success = $result['success'];
            $message = $result['message'];
            
            $result_data = $result['data'];
            if (count($result_data['paid_orders'])) {
                $cashRegisterMessage = "Orders captured: " . implode(', ', $result_data['paid_orders']);
                
                $this->load->model('cash_register_record_model');
                $this->cash_register_record_model->addCreditCardSale($result_data, $this->params['cash_register_id'], $this->employee_id, $cashRegisterMessage);
            }
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }
        
        if ($success) {
            $this->output_ajax_success_response($result, $message);
        } else {
            $this->output_ajax_error_response($message, $result);
        }
    }
    
    public function add_coupon()
    {
        $success = false;
        $message = '';
       
        try {
            $this->checkRequiredParameters(array(
                'promo_code',
                'customer_id',
            ));
            $this->loadEmployee();
            $customer = $this->checkCustomer($this->params['customer_id'], $this->business_id);
            $promocode = $this->params['promo_code'];

            $this->load->library('coupon');
            $response = $this->coupon->applyCoupon($promocode, $this->params['customer_id'], $this->business_id);
            if ($response['status'] == 'fail') {
                $success = false;
                $message = "Promotional code is invalid: {$response['error']}";
            } else {
                $this->load->model('coupon_model');
                $discount = $this->coupon_model->get_discount_value_as_string($promocode, $this->business_id);
                $result = array(
                    'promoCode' => $promocode,
                    'discount' => $discount,
                );

                $success = TRUE;
                $message = "Promotional code [$promocode - $discount] was applied to account {$this->params['customer_id']}. {$response['message']}";
            }
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }
        
        if ($success) {
            output_ajax_success_response($result, $message);
        } else {
            output_ajax_error_response($message);
        }
    }
    
    public function order_done() 
    {
        $success = false;
        $message = '';
        $data = array();
        $order_id = null;
        
        try {
            $this->checkRequiredParameters(array(
                'order_id',
            ));
            $order_id = $this->params['order_id'];
            
            $this->loadEmployee();

            $data = $this->process_order($order_id, TRUE);
            
            $success = $data['success'];
            $message = $data['message'];
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        if ($success) {
            $this->load->model('order_model');
            $orderResult = $this->order_model->getOrderDetails($this->business, $this->employee, $order_id);
            if (!$orderResult['success']) {
                $message .= ". {$orderResult['message']}";
                $this->output_ajax_error_response($message);
             }

            $data = array(
                'order_id' => $order_id,
                'order' => $orderResult['data'],
            );
            $this->output_ajax_success_response($data, $message);
        } else {
            $this->output_ajax_error_response($message);
        }
    }

    /**
     * Based on orders->process_order
     * This function applies the discounts to an order and updates the orderStatus to inventoried
     */
    protected function process_order($order_id, $force)
    {
        benchmark_me_start();
        
        $message = '';
        
        try {
            if ($this->params['skip_order_processing'] && !$force) {
                benchmark_me_end();
                return array(
                    'success' => TRUE,
                    'message' => " (Order #$order_id processing skipped) ",
                );
            }
            
            $order = $this->getOrder($order_id);
            
            // If this order has no customer, assign the order to the blank customer
            if (empty($order->customer_id)) {
                $order->customer_id = $this->business->blank_customer_id;
                $order->save();
            }

            if ($order->orderPaymentStatusOption_id == 3) {
                $message = get_translation("captured_cannot_change_discounts", "pos/global", array("order_id" => $order_id), "Payment has been captured for order #%order_id%. Can not change discounts.");
                return array(
                    'success' => FALSE,
                    'message' => $message,
                );
            }

            // If the order is a home delivery order
            $locker = new Locker($order->locker_id);
            $location = new Location($locker->location_id);

            //TODO: handle the other home deliveries service types
            if ($location->serviceType == Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY) {
                // Try to re-use an existing home delivery request
                $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($order->business_id);
                if ($homeDeliveryService) {
                    // in case this fails, customer should schedule home delivery
                    $response = $homeDeliveryService->addOrderToExisingHomeDelivery($order);
                }
            }

            $this->load->library("discounts");
            $applyRes = $this->discounts->apply_discounts($order);

            $this->load->model("order_model");
            $this->order_model->update_order_taxes($order_id);

            if ($applyRes['status']=='success') {
                //set order to inventoried
                $options['orderStatusOption_id'] = 3;
                $options['order_id'] = $order_id;

                $result = $this->order_model->update_order($options);
                if ($result['status'] == 'success') {
                } else {
                    $message .= "\n" . get_translation("error_set_inventoried", "pos/global", array("order_id"=>$order_id, "message"=>$result['message']), "Error setting Order[%order_id%] to inventoried: %message%.");
                }
            }
        } catch (Exception $exc) {
            send_exception_report($exc);
            return array(
                'success' => FALSE,
                'message' => $exc->getMessage(),
            );
        }

        $message .= "\n" . get_translation("success_order_processed", "pos/global", array("order_id"=>$order_id, "message"=>$result['message']), "Successfully Processed order #%order_id%");
        
        benchmark_me_end();
        
        return array(
            'success' => true,
            'message' => $message,
        );
    }
    
    protected function getOrder($order_id)
    {
        $order = new Order($order_id);
        if ($order->business_id !== $this->business_id) {
            throw new Exception(get_translation("order_wrong_business", "pos/global", array(), "Order not found for business"));
        }

        return $order;
    }

}
