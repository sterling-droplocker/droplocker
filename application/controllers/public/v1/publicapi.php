<?php

/**
 * Description of Api
 *
 * @author leo
 * 
 * TODO: once enhanced api controller from POS proyect is available, refactor to use that
 */
class PublicApi extends MY_Controller
{
    public function __construct() {
        parent::__construct();

        header("Access-Control-Allow-Headers: origin, x-requested-with, content-type");
        header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
        header("Access-Control-Allow-Origin: *");
    }

    public function get_all_private_locations_if_one_near()
    {
        $business_id = $this->input->get("business_id");
        $lat = $this->input->get("lat");
        $lon = $this->input->get("lon");
        
        if(!($business_id && $lat && $lon))
        {
            output_ajax_error_response("Missing parameters");
        }
        
        $this->load->model('location_model');
        
        $tolerancy = .05;//km around, so you don.t need the EXACT coordinates
        $limit = 1;//we just need one to make sure the address is correct
        $private_location_near = $this->location_model->get_all_by_distance_for_business($lat, $lon, $tolerancy, $business_id, false, $limit);
        if (empty($private_location_near)) {
            output_ajax_error_response("No private location found for provided data");
        } else {
            $tolerancy = 6000;//the whole world
            $limit = 1000;//fairly all
            $private_locations = $this->location_model->get_all_by_distance_for_business($lat, $lon, $tolerancy, $business_id, false, $limit);
                        
            foreach ($private_locations as $key => $location) {
                $private_locations[$key]->is_public = ((strtolower($location->public) === "yes") ? 1 : 0);
                $typeLabel = '(Private)';
                if ($private_locations[$key]->is_public) {
                    $typeLabel = '(Public)';
                }
                
                $info = '<table><tr><td valign=top><span class=bodyBold><u>' . $location->companyName . " $typeLabel " . '</u>
              <br><br>' . $location->address . '</span><span class=bodyReg><br>' . $location->location . '</span>';
                $info .= ($location->hours) ? '<br><br><span class=bodyBold>Hours: </span><span class=bodyReg>' . $location->hours . '</apan></td>' : "";
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $location->locationID . '.jpg')) {
                    $img = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/images/location_' . $location->locationID . '.jpg');
                    $info .= '<td style="padding:5px;"><img style="border:1px solid #ccc;margin:3px; padding:2px; background-color:#fff;" src=/images/location_' . $location->locationID . '.jpg width=' . $img[0] . ' height=' . $img[1] . '></td>';
                }
                $info .= '</tr></table>';
                
                $private_locations[$key]->info = $info;
            }
            
            output_ajax_success_response($private_locations, count($private_locations) . " private locations for business $business_id");
        }
        
    }
}
