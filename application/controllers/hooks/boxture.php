<?php

use App\Libraries\DroplockerObjects\OrderHomePickup;
use App\Libraries\DroplockerObjects\OrderHomeDelivery;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Bag;


class Boxture extends MY_Controller
{
    /**
     * Endpoint for Boxture delivery notifications
     *
     * Will decode the format and try to look for deliveries or pickups
     * matching the Boxtures's UUID
     *
     * Expetcs JSON object in the POST body with this format: http://docs.boxture.com/shipments/retrieve-a-shipment/
     *
     * NOTE: JSON object comes with tons of data but we only use: $data->shipment->human_id
     */
    public function notifications($business_id = null)
    {
        if (empty($business_id)) {
            die("Error: invalid Business ID");
        }

        $post = file_get_contents("php://input");
        $data = json_decode($post);
        
        //waybill_nr = bag_number
        if (!empty($data->shipment->lines[0]->waybill_nr)) {
            $data->shipment->waybill_nr = $data->shipment->lines[0]->waybill_nr;
        }
        
        if (empty($data->shipment->waybill_nr)) {
            $subject = "Boxture hook error: waybill_nr (bag number) can't be empty";
            $message = "<pre>$post</pre>";
            send_admin_notification($subject, $message, $business_id);

            die("Error: invalid JSON object: waybill_nr (bag number) can't be empty.");
        }

        if (empty($data->shipment->human_id)) {
            $subject = "Boxture hook error: human_id (tracking number) can't be empty";
            $message = "<pre>$post</pre>";
            send_admin_notification($subject, $message, $business_id);

            die("Error: invalid JSON object: human_id (tracking number) can't be empty.");
        }

        // many orders can go with one rickshaw request
        $deliveries = OrderHomeDelivery::search_aprax(array(
            'service' => 'boxture',
            'trackingCode' => $data->shipment->human_id,
            'business_id'   => $business_id
        ));

        if ($deliveries) {
            foreach($deliveries as $d) {
                $this->processDelivery($d, $data);
            }
            return;
        }

        // only one pickup per boxture request
        $pickups = OrderHomePickup::search_aprax(array(
            'service' => 'boxture',
            'trackingCode' => $data->shipment->human_id,
            'business_id'   => $business_id
        ));

        if ($pickups) {
            foreach($pickups as $p) {
                $this->processPickup($p, $data);
            }
            return;
        }

        $subject = "Boxture hook error: No record found for human_id (tracking number): {$data->shipment->human_id}";
        $message = "<pre>$post</pre>";
        send_admin_notification($subject, $message, $business_id);

        die("Error: No record found for human_id (tracking number): {$data->shipment->human_id}");
    }

    /**
     * Process notification of an order delivered
     *
     * @param OrderExternalDelivery $externalDelivery
     */
    protected function processDelivery($delivery, $data)
    {
        $order = new Order($delivery->order_id);
        $customer = new Customer($order->customer_id);

        if ($order->orderPaymentStatusOption_id != 3 && $customer->invoice == 0) {
            $this->load->model('order_model');
            $response = $this->order_model->capture($order->orderID, $order->business_id);

            // did not capture successfully
            if ($response['status'] != 'success') {
                return;
            }
        }

        $this->load->model('order_model');
        $this->order_model->update_order(array(
            'order' => $order,
            'orderStatusOption_id' => 9,
            'locker_id' => $order->locker_id,
            'orderPaymentStatusOption_id' => $order->orderPaymentStatusOption_id,
            'employee_id' => 0,
        ));
    }

    /**
     * Process notification of a claim picked up
     *
     * @param ClaimExternalPickup $externalPickup
     */
    protected function processPickup($pickup, $data)
    {
        $claim    = new Claim($pickup->claim_id);
        $customer = new Customer($claim->customer_id);
        $locker   = new Locker($claim->locker_id);
        $location = new Location($locker->location_id);

        $bagNumber = $data->shipment->waybill_nr;
        $tracking  = $data->shipment->human_id;

        $bag = Bag::search_aprax(array(
            "customer_id" => $customer->customerID,
            "bagNumber" => $bagNumber,
        ));      

        if (empty($bag)) {
            $this->load->model('bag_model');
            $bagId = $this->bag_model->addBagToCustomer($customer->customerID, $bagNumber, $customer->business_id, "boxture", false);
            
            if (empty($bagId)) {
                $subject = "Error with Claim {$claim->claimID} picked up by Boxture";
                $message = "Can't create an order because bag does not belong to a customer and we were not able to create a new one. Bag: {$bagNumber}, Tracking: {$tracking}";

                send_admin_notification($subject, $message, $claim->business_id);
            }
        }

        $notes = trim($pickup->notes);
        $options = array('claimID' => $pickup->claim_id);

        // Create the new order
        $this->load->model("order_model");
        $order_id = $this->order_model->new_order($pickup->location_id,
            $locker->lockerID,
            $bagNumber,
            null,
            $notes,
            $pickup->business_id,
            $options);

        if (!empty($order_id)) {
            $subject = "Claim {$claim->claimID} picked up by Boxture";
            $message = "
                ClaimID: {$claim->claimID}
                CustomerID: {$customer->customerID}
                Customer: ".  getPersonName($customer)."
                Locker: {$locker->lockerName}
                Location: {$location->address}
                Bag: {$bagNumber}
                Tracking: {$tracking}
            ";
        } else {
            $subject = "Claim {$claim->claimID} [ERROR]";
            $message = "
                ClaimID: {$claim->claimID}
                CustomerID: {$customer->customerID}
                Customer: ".  getPersonName($customer)."
                Locker: {$locker->lockerName}
                Location: {$location->address}
                Bag: {$bagNumber}
                Tracking: {$tracking}
            ";
        }
        send_admin_notification($subject, $message, $claim->business_id);
    }
}