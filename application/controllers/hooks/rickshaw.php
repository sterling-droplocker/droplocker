<?php

use App\Libraries\DroplockerObjects\OrderExternalDelivery;
use App\Libraries\DroplockerObjects\ClaimExternalPickup;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;


class Rickshaw extends MY_Controller
{

    /**
     * Endpoint for Rickshaw delivery notifications
     *
     * Will decode the format and try to look for delvieries or pickups
     * matching the Rickshaw's UUID
     *
     * Expetcs JSON object in the POST body with this format:
     *
     * 'category': the rickshaw notification category corresponding to this event (PKUP,DLVD,etc)
     * 'uuid': the rickshaw uuid corresponding to the order
     * 'foreign_id': the id in your system (if included in the original order)
     * 'done_at': ISO8601 formatted datetime string indicating when this was done
     */
    public function notifications()
    {

        $post = file_get_contents("php://input");
        $data = json_decode($post);

        if (empty($data->uuid)) {
            die("Error: invalid JSON object");
        }

        // ignore notifications that are not 'delivery'
        if ($data->category != 'DLVD') {
            return;
        }

        // many orders can go with one rickshaw request
        $externalDeliveries = OrderExternalDelivery::search_aprax(array(
            'service' => 'rickshaw',
            'third_party_id' => $data->uuid
        ));

        if ($externalDeliveries) {
            foreach ($externalDeliveries as $externalDelivery) {
                $this->processDelivery($externalDelivery);
            }

            return;
        }

        // only one pickup per rickshaw request
        $externalPickup = ClaimExternalPickup::search(array(
            'service' => 'rickshaw',
            'third_party_id' => $data->uuid
        ));

        if ($externalPickup) {
            $this->processPickup($externalPickup);

            return;
        }

        echo "ERROR: no record found for UUID: {$data->uuid}";
    }

    /**
     * Process notification of an order delivered
     *
     * @param OrderExternalDelivery $externalDelivery
     */
    protected function processDelivery($externalDelivery)
    {
        $order = new Order($externalDelivery->order_id);
        $customer = new Customer($order->customer_id);

        if ($order->orderPaymentStatusOption_id != 3 && $customer->invoice == 0) {
            $this->load->model('order_model');
            $response = $this->order_model->capture($order->orderID, $order->business_id);

            // did not capture successfully
            if ($response['status'] != 'success') {
                return;
            }
        }

        $this->load->model('order_model');
        $this->order_model->update_order(array(
            'order' => $order,
            'orderStatusOption_id' => 9,
            'locker_id' => $order->locker_id,
            'orderPaymentStatusOption_id' => $order->orderPaymentStatusOption_id,
            'employee_id' => 0,
        ));
    }

    /**
     * Process notification of a claim picked up
     *
     * @param ClaimExternalPickup $externalPickup
     */
    protected function processPickup($externalPickup)
    {
        $claim    = new Claim($externalPickup->claim_id);
        $customer = new Customer($claim->customer_id);
        $locker   = new Locker($claim->locker_id);
        $location = new Location($locker->location_id);

        $subject = "Claim {$claim->claimID} picked up by Rickshaw";
        $message = "
            ClaimID: {$claim->claimID}
            CustomerID: {$customer->customerID}
            Customer: ".  getPersonName($customer)."
            Locker: {$locker->lockerName}
            Location: {$location->address}
        ";

        send_admin_notification($subject, $message, $claim->business_id);
    }
}