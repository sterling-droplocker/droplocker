<?php
require_once APPPATH . 'libraries/sms_claim.php';

class Twilio extends MY_Controller
{

    private $sms_keyword;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
        $this->sms_keyword = \App\Libraries\DroplockerObjects\Server_Meta::get("sms_keyword");
        if (empty($this->sms_keyword)) {
            $this->sms_keyword = "LAUNDRY";
        }
    }

    public function claim()
    {
        //if getting twilio response, this will be business number
        $business_number = $this->input->get('To');

        if (strpos($business_number, '%') !== false) {
            $business_number = urldecode($business_number);
        }

        //check to make sure no extra space
        $business_number = trim($business_number);

        if (strpos($business_number, '+') === false) {
            $business_number = '+' . $business_number;
        }

        $business_settings = $this->get_provider_settings($business_number);

        if (empty($business_settings->business_id)) {
           throw new Validation_Exception("Business settings for '{$this->business_id}' not found");
        }

        //send a copy of this SMS to the businesses support email
        $sms_body        = $this->input->get("Body");
        $customer_number = $this->input->get("From");
        $sms_number      = clean_phone_number($customer_number);
        $sms_number      = ltrim($sms_number, '1');

        //send a copy of this SMS to the businesses support email
        $this->send_copy_to_customer_support($customer_number, $sms_body, $business_settings->business_id);

        //load the sms_claim library
        $this->load->library('twilio_claim', array(
            'business_id' => $business_settings->business_id,
            'sms_keyword' => $business_settings->keyword,
            'business_sms_number' => $business_number,
            'use_api' => true
        ));

        // process the incoming message
        $this->twilio_claim->process_message($sms_number, $sms_body);

        // log the SMS message to email history
        $this->twilio_claim->logHistory($sms_number, $sms_body);

    }

    /**
     * Sends a copy of the sms message to customer support
     *
     * @param string $customer_number
     * @param string $sms_body
     * @param int $business_id
     */
    protected function send_copy_to_customer_support($customer_number, $sms_body, $business_id)
    {
        $this_is_sms_copy_reminder = "<p style='color:red;font-size:32px;'>This is a copy of a SMS do not respond to this email.</p>";

        send_admin_notification(
            "Incoming Customer SMS",
            "SMS FROM: " . $customer_number . "<br>\n\n" . $sms_body . "<br>\n\n" . $this_is_sms_copy_reminder,
            $business_id
        );
    }

    /**
     * Retrieves the business sms settings using the phone number
     *
     * @param string $number
     * @return object
     */
    protected function get_provider_settings($number)
    {
        $number_query = $this->db->get_where('smsSettings', array('number' => $number));
        $business_sms_settings = $number_query->row();

        return $business_sms_settings;
    }
}