<?php
use App\Libraries\DroplockerObjects\Customer;
/* 
Example Request from mailchimp:

$type = unsubscribe
$data = Array
(
    [action] => unsub
    [reason] => manual
    [id] => 6bf99e64e0
    [email] => f13edfb98195a048ed6f19b902afb7d2@droplocker.com
    [email_type] => html
    [ip_opt] => 152.231.61.181
    [web_id] => 48130511
    [merges] => Array
        (
            [EMAIL] => f13edfb98195a048ed6f19b902afb7d2@droplocker.com
            [FNAME] => Véronique
            [LNAME] => SAMSON
            [ADDRESS] => 
            [PHONE] => 
            [CUSTOMERID] => 79141
            [ADDRESS1] => 6 che des trois ruisseaux
            [LASTORDER] => 2016-08-09
            [LOCATION_I] => 1519
            [ROUTE_ID] => 1
            [AVGORDERLA] => 721
            [ZIP] => 
            [KIOSKACCES] => 
            [INACTIVE] => 
            [SEX] => F
            [TOTGROSS] => 97
            [VISITS] => 2
            [SIGNUPDATE] => 2014-08-11
            [ORDERCOUNT] => 2
            [BUSINESS_I] => 96
            [FIRSTORDER] => 2014-08-18
            [CITY] => Boullay-Lès-Troux
            [STATE] => 33.14
            [IP] => 193.164.156.12
            [BIRTHDAY] => 1966-10-08
            [PLANSTATUS] => None
        )

    [list_id] => c6e24e48b6
)
*/
class Mailchimp extends MY_Controller
{
    /**
    * Webhook for processing mailchimp subscribes/unsubscribes
    */
    public function index() {
        $customer = $this->getCustomerByEmail($this->input->get('email'));
        $action = $this->input->post('type');
        $data = $this->input->post('data');
        $updated_customer = false;

        if (!empty($action) && !empty($data)) {
            $customerID = false;

            if (!empty($data['merges']['CUSTOMERID'])) {
                $customerID = $data['merges']['CUSTOMERID'];
            }
            
            if (!empty($data['merges']['BUSINESS_I'])) {
                $businessID = $data['merges']['BUSINESS_I'];
            }

            if (!$customerID) {
                $customer = $this->getCustomerByEmail($data["email"]);
                if (!empty($customer)) {
                    $customerID = $customer->customerID;
                    $businessID = $customer->business_id;
                    $data['MATCHED_CUSTOMERID'] = $customerID;
                }
            }
            
            if ($customerID) {
                $this->db->update('customer',
                    array('noEmail' => ($action == "subscribe") ? 0 : 1),
                    array('customerID' => $customerID)
                );
                $updated_customer = true;
            }

            $this->db->insert('mailchimpLog', array(
                'business_id' => $businessID,
                'requestUrl' => current_url(),
                'response' => json_encode($data),
                'batchMembers' => $updated ? 1 : 0,
            ));
        }
    }

    /**
     * Gets a customer by its email number
     *
     * @param string $number
     * @return Customer
     */
    protected function getCustomerByEmail($email)
    {
        // check if customer already exists in this business
        $customer = Customer::search(array(
            "email" => $email
        ));

        return $customer;
    }
}
