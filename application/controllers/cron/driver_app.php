<?php
/**
 *
 */
class Driver_App extends MY_Cron_Controller
{
    public function sync_all($business_id = null, $day = null, $force = false)
    {
        $time = time();
        
        if (!$this->shutdownSetup) {
            $this->handleShutdown();
            $this->shutdownSetup = true;
        }
        
        //all in same process to keep execution order and prevent concurrency issues
        $this->businessToFirebase($business_id, null, $day, $force);
        $this->requestQueueToApi();
        $this->droidToDroplocker($business_id);
        $this->driverJournalToFirebase($business_id, $day);
        
        $time = secondsToHours(time() - $time);
        $memory_profile = getMemoryInfo();
        
        $res = array(
            "time" => $time,
            'memory' => $memory_profile['summary'],
            "message" => "Execution finished",
            "memory_profile" => $memory_profile,
        );
        
        $this->logResults(__METHOD__, $res);
    }
    
    public function requestQueueToApi()
    {
        $this->load->library('driverappsynclibrary');
        $res = $this->driverappsynclibrary->requestQueueToApi();
        
        $this->logResults(__METHOD__, $res);
    }
    
    /**
     * executes mobilecron
     */
    public function droidToDroplocker($business_id = null, $employee_id = null)
    {
        $this->load->library('driverappsynclibrary');
        $res = $this->driverappsynclibrary->droidToDroplocker($business_id, $employee_id);
        
        $this->logResults(__METHOD__, $res);
    }
    
    /* 
     * driver_app_journal table ==> Firebase
     * Update data related to a specific entity (order or claim) at a route level
     * It will update routes for all entities that have changed since previous sync
     */
    public function driverJournalToFirebase($business_id = null, $day = null)
    {
        $this->load->library('driverappsynclibrary');
        $res = $this->driverappsynclibrary->driverJournalToFirebase($business_id, $day, array($this, 'logResultsAuth'), array($this->requestLogAuth(), __METHOD__));
        $this->logAuth = null;
        
        $this->logResults(__METHOD__, $res);
    }
    
    /* 
     * Update data related to businesses
     * Updates the whole business node, if route list provided, business node will only contain those routes
     */
    public function businessToFirebase($business_id = null, $routes = null, $day = null, $force = false)
    {
        $time = time();
        
        $sucess = TRUE;
        
        try {
            if (!ob_get_level()) { 
                ob_start();
            }

            $this->load->library('driverappsynclibrary');
            
            if($routes)
            {
                $routes = explode(',', str_replace(' ', '', $routes));
            }

            $res = $this->driverappsynclibrary->syncBusinesses($business_id, $day, $routes, $force, 'cronLog', array(__METHOD__));
            
            $log = $this->getOutputLog();
        } catch (Exception $e) {
            $sucess = FALSE;
            $errorMsg = "Error while updating routes for business {$business_id} on day $day. Routes: " . implode(',', $routes) . ". Error: {$e->getMessage()}";
        };
        
        $time = secondsToHours(time() - $time);
        $memory_profile = getMemoryInfo();
        
        $msg = $res['message'] ? $res['message'] : "Routes updated";
        if ($errorMsg) {
            $msg = $errorMsg;
        }
        
        $result = array(
            'time' => $time,
            'memory' => $memory_profile['summary'],
            'success' => $sucess,
            'message' => $msg,
            'business_id' => $business_id, 
            'routes' => $routes, 
            'day' => $day,
            'log' =>$log,
            "memory_profile" => $memory_profile,
        );
        
        $this->logResults(__METHOD__, $result);
    }
    
    protected function out($msg)
    {
        echo gmdate('Y-m-d H:i:s') . " -- $msg<br />\n";
    }
    
    protected function getOutputLog()
    {
        $log = ob_get_contents();
        ob_end_clean();

        $log = explode("\n", str_replace('<br />', '', $log));
        
        return $log;
    }
    
    private $logAuth = null;
    private function requestLogAuth() {
        $salt = '$2a$07$TbyMaJBqSUo2mLuFokGRxew7liRW9FMHVtOuwld2gzTsGglyyG18G';
        $this->logAuth = md5($salt . time());
        
        return $this->logAuth;
    }
    
    public function logResultsAuth($logAuth, $method, $res)
    {//non concurrent
        if (!$logAuth || !$this->logAuth || $logAuth !== $this->logAuth) {
            return;
        }
        
        $this->logResults($method, $res);
    }
    
    protected $shutdownSetup = false;

    public $emergencyMemory = null;
    protected $emergencyMemoryAmount = 2;

    protected function handleShutdown() {
        $this->emergencyMemory = str_repeat('*', $this->emergencyMemoryAmount * 1024 * 1024);
        register_shutdown_function(array('Driver_App', 'shutdown'), $this);
    }
    
    public function shutdown($me) {
        $me->emergencyMemory = null;//free backup memory
        
        $last_error = error_get_last();
        if($last_error['type'] === E_ERROR) {
            $last_error['log'] = ob_get_contents();
            cronLog('DriverApp->error', $last_error);
        }
    }
    
    protected function logResults($method, $res)
    {
        $this->db->insert('cronLog', array('job' => $method, 'results' => serialize($res)));
    }
}
