<?php

set_time_limit(0);

use DropLocker\Util\Lock;
use App\Libraries\DroplockerObjects\CardReader;
use App\Libraries\DroplockerObjects\CardReaderSyncLog;
use App\Libraries\DroplockerObjects\CardReaderAccessLog;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Business;

class CardReaders extends MY_Cron_Controller
{
    /**
     *
     * @var CardReaderLibrary
     */
    public $cardreaderlibrary;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('cardreaderlibrary');
    }

    /**
     * Syncs all card readers
     *
     * If $full is true will reset the card reader and
     * upload all cards, if not then only newer cards are uploaded.
     *
     * @param bool $full
     */
    public function sync_all($full = false)
    {
        $cardreaders = CardReader::search_aprax(array(
            'mode !=' => 'open',
            'status' => 'enabled',
        ));

        $command = $this->getControllerCall('cron/cardreaders/sync %s %s');

        $tasks = array();
        foreach ($cardreaders as $cardReader) {
            $tasks[$cardReader->cardReaderID] = sprintf($command, $cardReader->cardReaderID, $full ? 1 : 0);
        }

        $this->fork($tasks, 'sync_all');
    }

    /**
     * Syncs a card reader
     *
     * If $full is true will reset the card reader and
     * upload all cards, if not then only newer cards are uploaded.
     *
     * @param int $cardReaderID
     * @param bool $full
     */
    public function sync($cardReaderID, $full = false)
    {
        $cardReader = new CardReader($cardReaderID);
        $location = new Location($cardReader->location_id);
        $business = new Business($location->business_id);

        if ($full) {
            $cardReader->full_upload = 1;
            $cardReader->save();
        }

        while(!Lock::acquire("card_reader_{$cardReaderID}")) {
            echo "Failed to acquire lock\n";
            return;
        }

        // need to set this in a cronjob
        date_default_timezone_set($business->timezone);

        $this->checkReboot($cardReader);

        // forced full load
        if ($cardReader->full_upload) {
            $full = true;
        }

        //get the access log
        $response = $this->cardreaderlibrary->getLog($cardReader->address, $cardReader->port);

        // save the access log
        if ($response['result'] == 'success' && !empty($response['data'])) {
            foreach ($response['data'] as $row) {

                $accesTime = $row['accessTime'];

                $access = new CardReaderAccessLog();
                $access->setProperties(array(
                    'location_id' => $cardReader->location_id,
                    'accessTime' => $accesTime,
                    'card' => $row['card'],
                    'exp' => $row['exp'],
                    'success' => $row['success'],
                    'raw' => $row['raw'],
                ));
            }
        }

        // only log if there were results or an error
        if ($response['result'] != 'success' || !empty($response['data'])) {
            // log the request
            $log = new CardReaderSyncLog();
            $log->setProperties(array(
                'location_id' => $cardReader->location_id,
                'success' => $response['result'] == 'success' ? 1 : 0,
                'syncMessage' => 'CRON:' . $response['message'],
            ));
        }

        // on error, reboot and exit
        $this->checkAndReboot($cardReader, $response);

        //if this is open to only registered cards
        if ($cardReader->mode != 'open') {

            $today = null;
            if (!$full) {

                // get last time it was updated
                $lastUpdate = CardReaderSyncLog::search(array(
                    'location_id' => $cardReader->location_id,
                    'cardsAdded >' => 0,
                    'success' => 1,
                ), false, 'syncTime DESC');

                // use last updated to get only newer cards
                if ($lastUpdate) {
                    $lastUpdate->syncTime->setTimeZone(new DateTimeZone('GMT'));
                    $today = $lastUpdate->syncTime->format('Y-m-d H:i:s');
                }
            }

            $syncTime = gmdate('Y-m-d H:i:s');
            $cards = $this->cardreaderlibrary->getCards($location->business_id, $today);

            if ($full) {
                //reset the reader
                $response = $this->cardreaderlibrary->reset($cardReader->address, $cardReader->port);

                // on error, reboot and exit
                $this->checkAndReboot($cardReader, $response);

                // set the mode
                $response = $this->cardreaderlibrary->setMode($cardReader->address, $cardReader->port,
                    $cardReader->mode, $cardReader->windowStart, $cardReader->windowEnd);

                // update the current mode
                if ($response['result'] == 'success') {
                    $cardReader->current_mode = $response['data'];
                    $cardReader->save();
                }

                // on error, reboot and exit
                $this->checkAndReboot($cardReader, $response);
            }

            $response = $this->cardreaderlibrary->addCards($cardReader->address, $cardReader->port, $cards);

            if ($full && $response['result'] == 'success') {
                // no full load necessary, just did one
                $cardReader->full_upload = 0;
                $cardReader->save();
            }

            // log only if cards were added or there was an error
            if ($response['result'] != 'success' || !empty($cards)) {
                // log the request
                $log = new CardReaderSyncLog();
                $log->setProperties(array(
                    'location_id' => $cardReader->location_id,
                    'success' => $response['result'] == 'success' ? 1 : 0,
                    'syncMessage' => 'CRON:'.$response['message'],
                    'cardsAdded' => !empty($response['data']) ? count($response['data']['cardsAdded']) : 0,
                    'syncTime' => $syncTime,
                ));
            }

            if ($response['result'] != 'success') {
                send_admin_notification("Error with reader sync at {$location->address}", $response['message'], $location->business_id);

                // on error, reboot and exit
                $this->checkAndReboot($cardReader, $response);

            } else if (!empty($response['data']['errors'])) {
                send_admin_notification("Error with card reader sync at {$location->address}", $response['message'] . '<br>' . print_r($response['data']['errors'], true), $location->business_id);
            }
        }

        Lock::release("card_reader_{$cardReaderID}");
    }

    /**
     * Changes the mode of all card readers based on their windows
     */
    public function update_all_windows()
    {
        $now = gmdate('H:i:s');

        $cardreaders = CardReader::search_aprax(array(
            'mode' => 'window',
            'status' => 'enabled',
        ));

        $command = $this->getControllerCall('cron/cardreaders/update_window %s');

        $tasks = array();
        foreach ($cardreaders as $cardReader) {
            try {
                $location = new Location($cardReader->location_id);
                $business = new Business($location->business_id);

                // need to set this in a cronjob
                date_default_timezone_set($business->timezone);

                $now = date('H:i:s');
                if ($now < $cardReader->windowStart || $now > $cardReader->windowEnd) {
                    $mode = 'registered';
                } else {
                    $mode = 'open';
                }

                echo "($cardReader->cardReaderID) now:$now start:$cardReader->windowStart end:$cardReader->windowEnd mode:$cardReader->current_mode => $mode\n";

                // just entered or leaved the time window, update the mode
                if ($cardReader->current_mode != $mode) {
                    $tasks[$cardReader->cardReaderID] = sprintf($command, $cardReader->cardReaderID);
                }

            } catch (Exception $e) {
                send_exception_report($e);
                continue;
            }
        }

        $this->fork($tasks, 'update_all_windows');
    }

    /**
     * Changes the mode of a card reader based on it's window
     *
     * @param int $cardReaderID
     */
    public function update_window($cardReaderID)
    {
        while(!Lock::acquire("card_reader_{$cardReaderID}")) {
            echo "Failed to acquire lock\n";
            return;
        }

        $cardReader = new CardReader($cardReaderID);
        $location = new Location($cardReader->location_id);
        $business = new Business($location->business_id);

        // need to set this in a cronjob
        date_default_timezone_set($business->timezone);

        $this->checkReboot($cardReader);

        $response = $this->cardreaderlibrary->setMode($cardReader->address, $cardReader->port,
            $cardReader->mode, $cardReader->windowStart, $cardReader->windowEnd);

        if ($response['result'] == 'success') {
            $cardReader->current_mode = $response['data'];
            $cardReader->save();
        }

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'cardsAdded' => 0,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => 'CRON:'.$response['message'],
        ));

        // on error, reboot and exit
        $this->checkAndReboot($cardReader, $response);

        Lock::release("card_reader_{$cardReaderID}");
    }

    public function update_all($full = false)
    {
        $now = gmdate('H:i:s');
        $cardreaders = CardReader::search_aprax(array(
            'status' => 'enabled',
        ));

        $command = $this->getControllerCall('cron/cardreaders/update %s');

        $tasks = array();
        foreach ($cardreaders as $cardReader) {
            $tasks[$cardReader->cardReaderID] = sprintf($command, $cardReader->cardReaderID, $full ? 1 : 0);
        }

        $this->fork($tasks, 'update_all');
    }

    public function update($cardReaderID, $full = false)
    {
        $cardReader = new CardReader($cardReaderID);
        $location = new Location($cardReader->location_id);
        $business = new Business($location->business_id);

        // if it's in widowed mode
        if ($cardReader->mode == 'window') {

            // need to set this in a cronjob
            date_default_timezone_set($business->timezone);
            $now = date('H:i:s');

            if ($now < $cardReader->windowStart || $now > $cardReader->windowEnd) {
                $mode = 'registered';
            } else {
                $mode = 'open';
            }

            // just entered or leaved the time window, update the mode
            if ($cardReader->current_mode != $mode) {
                $this->update_window($cardReaderID);
            }
        }

        // sync the reader
        $this->sync($cardReaderID, $full);
    }


    /**
     * Reboots a card reader
     *
     * @param int $cardReaderID
     * @return array
     */
    public function reboot($cardReaderID)
    {
        $cardReader = new CardReader($cardReaderID);

        $response = $this->cardreaderlibrary->reboot($cardReader->address, $cardReader->masterPort, $cardReader->password);

        // log the request
        $log = new CardReaderSyncLog();
        $log->setProperties(array(
            'location_id' => $cardReader->location_id,
            'success' => $response['result'] == 'success' ? 1 : 0,
            'syncMessage' => 'CRON:'.$response['message'],
            'syncTime' => gmdate('Y-m-d H:i:s'),
        ));

        return $response;
    }

    /**
     * Checks the result of an operation and reboots the card reader on error
     *
     * This will release locks and terminate execution on error.
     *
     * @param CardReader $cardReader
     * @param array $response
     * @return boolean
     */
    protected function checkAndReboot($cardReader, $response)
    {
        // last operation succeeded, do nothing
        if ($response['result'] == 'success') {
            return true;
        }

        $this->checkReboot($cardReader);

        // if there isn't a recent reboot, then reboot
        $response = $this->reboot($cardReader->cardReaderID, true);

        // notify admin about the reboot
        $location = new Location($cardReader->location_id);
        send_admin_notification("Reader reboot at {$location->address}", $response['message'], $location->business_id);

        Lock::release("card_reader_{$cardReader->cardReaderID}");
        exit(1);
    }

    /**
     * Prevent access to a recently rebooted card reader
     *
     * @param cardReader $cardReader
     * @return boolean
     */
    protected function checkReboot($cardReader)
    {
        if ($this->getRecentReboots($cardReader)) {
            echo "Card reader is rebooting\n";

            Lock::release("card_reader_{$cardReader->cardReaderID}");
            exit(2);
        }

        return false;
    }

    /**
     * Gets reboots in the log for the last 2 minutes
     *
     * @param CardReader $cardReader
     * @return array
     */
    protected function getRecentReboots($cardReader)
    {
        $log = CardReaderSyncLog::search(array(
            'location_id' => $cardReader->location_id,
            'success' => 1,
            'syncMessage LIKE' => '%Reader Reboot%',
            'syncTime >=' => gmdate('Y-m-d H:i:s', strtotime('-2 min'))
        ));

        return $log;
    }

}