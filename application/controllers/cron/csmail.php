<?php

use App\Libraries\DroplockerObjects\Customer;

// While it will be executed from the cron_tab, this controller must be publically accessable
class CSMail extends MY_Controller
{

	private $processLabelId;

	// This value corresponds to the location from which this script is being called.  It must be a publically
	// accessable url.  For development purposes this can be set to http://localhost
	// All redirect URI must be set in the Google Dev App
	
	public function process_all_google_inbox(){
		$this->load->model('tickettoken_model');
	
		$activeTokens = $this->tickettoken_model->get_aprax( array('active'=>'1') );
		
		foreach($activeTokens as $activeToken){
			$this->process_one_google_inbox($activeToken->ticketTokenID);
		}
	}
		
	public function process_one_google_inbox($ticketTokenID = null)
	{
		
		require_once APPPATH . '/third_party/google-api-php-client/autoload.php';
		require_once APPPATH . '/third_party/google-api-php-client/src/Google/Client.php';   
		require_once APPPATH . '/third_party/google-api-php-client/src/Google/Service/Gmail.php';


		// Load settings for this google developer settings for this integration		
		$this->load->model('googleappsecret_model');
		$googleAppSecret = $this->googleappsecret_model->get_one( array('application'=>'DP Gmail Integration') );
		
		// We must use oAuth for authorizing the cron job.
		// It initially needs to be executed at the command line in
		// order to obtain a validation code.  The validation code
		// must be redemmed with in 30 seconds of being issued.
		
		// This section should execute once the initial Code fect has been requested
		if(!empty( $_GET['code'] )){

			$client = new Google_Client();
			$client->setAuthConfig($googleAppSecret->clientSecretJSON);
			$client->setRedirectUri($googleAppSecret->redirectURL);
			$client->setAccessType($googleAppSecret->accessType);
			$client->setApprovalPrompt($googleAppSecret->approvalPrompt);
			$client->setScopes(array('https://www.googleapis.com/auth/gmail.modify'));
	
			// Authenticate the code that was generated
			$client->authenticate($_GET['code']);
						
			// Obtain the token.  This token is only good till it times out			
			$accessToken = $client->getAccessToken();
			
			// We must request a refresh token
			$accessObj = json_decode($accessToken);
			
			$this->load->model('tickettoken_model');
			// A refresh token has been generated.  
			if( array_key_exists('refresh_token', $accessObj) ){
				$refreshToken = $accessObj->refresh_token;

				$this->tickettoken_model->save( array('access_token'=>$accessToken,'refresh_token'=>$refreshToken ) );
			}else {
			// We did not get a refresh token back
				$this->tickettoken_model->save( array('access_token'=>$accessToken) );		

				//throw new \Exception("We did not receive a refresh token");

				$this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>"We did not receive a refresh token"));
			}
		
		Print "Your token has been populated in the database.  Please configure your businessId and CS employee.  Reference accessToken [$accessToken]";	
		exit;
		} 
		
		// There is no token present in the database
		// we must request a authorization url to generate a code for this user
		if( is_Null($ticketTokenID) ){
			$this->load->model('tickettoken_model');
			$ticketToken = $this->tickettoken_model->get_aprax(array ('ticketTokenID'=>$ticketTokenID) );
	
			
			print "This email account has not been configured before:<BR>";
			$client = new Google_Client();
		
			$client->setAuthConfig($googleAppSecret->clientSecretJSON);
			$client->setAccessType($googleAppSecret->accessType);
			$client->setApprovalPrompt($googleAppSecret->approvalPrompt);
			$client->setRedirectUri($googleAppSecret->redirectURL);
			$client->setScopes(array('https://www.googleapis.com/auth/gmail.modify'));
	
			$authUrl = $client->createAuthUrl();

			print "<BR><BR>Please visit:\n<a href=\"$authUrl\" target=\"blank\">$authUrl</a>\n\n";
			exit;
		}else {
			// This is a token present.	

			$this->load->model('tickettoken_model');
			$ticketToken = $this->tickettoken_model->get_by_primary_key($ticketTokenID);
	
			$client = new Google_Client();
			$client->setAuthConfig($googleAppSecret->clientSecretJSON);
			$client->setApprovalPrompt($googleAppSecret->approvalPrompt);
			$client->setRedirectUri($googleAppSecret->redirectURL);
			$client->setScopes(array('https://www.googleapis.com/auth/gmail.modify'));
			$activeToken  = $ticketToken->access_token;
			$client->setAccessToken($activeToken);

			if(strlen($ticketToken->refresh_token) ){
				$refreshToken = $ticketToken->refresh_token;
				
				// Our token has expired, use the refresh token
				if( $client->isAccessTokenExpired() ) {
					$client->refreshToken($refreshToken);
				
					// Obtain the token.  This token is only good till it times out			
					$accessToken = $client->getAccessToken();
					
					// We must request a refresh token
					$accessObj = json_decode($accessToken);
					
					$refreshToken = $accessObj->refresh_token;
					
					$this->load->model('tickettoken_model');
					// A refresh token has been generated.  
					if($refreshToken){
						$this->tickettoken_model->save( array('ticketTokenID'=>$ticketTokenID,'access_token'=>$accessToken,'refresh_token'=>$refreshToken ) );
					}else {
					// We did not get a refresh token back
						$this->tickettoken_model->save( array('access_token'=>$accessToken) );		
		
					//	throw new \Exception("We did not receive a refresh token");
						$this->db->insert('cronLog', array('job'=>__METHOD__, 'results'=>"We did not receive a refresh token"));
					}
				}	
			}
		}
	
		$gmailService = new Google_Service_Gmail($client);	
	
		$optParams = [];

		// This can be used to control the number of emails being process 
		// allowing incoming emails to be processed in patches
		
        $optParams['maxResults'] = 5; // Return Only 5 Messages
		
		// we common filters
		//unread and in inbox: 'is:unread label:inbox'
		//in primary tab = 'category:primary' 

		$optParams['q'] = $ticketToken->emailFilterRule;
		
		// Search for only tickets in the primary tab
        $messages = $gmailService->users_messages->listUsersMessages($ticketToken->emailAccount,$optParams);
	    
	    	
		//obtain all messages
		$list = $messages->getMessages();

		// for completness in case we want to use additional paramters        
		$optParamsGet = [];
        
        // checks for existance and creates user defined processed tag
        
        $this->createProcessLabel($gmailService,$ticketToken->processLabel,$ticketToken->emailAccount);
        
        $this->load->model('ticketstatus_model');
        $newStatusId = $this->ticketstatus_model->getNewStatusId($ticketToken->business_id);
   
        
        // begin loop over emails
		foreach($list as $messageHeader){
            $message = $gmailService->users_messages->get($ticketToken->emailAccount,$messageHeader->getId() );
			
			$messagePayload = $message->getPayload();
			$headers = $message->getPayload()->getHeaders();
			$parts = $message->getPayload()->getParts();
   
            if( sizeof($parts) > 0){
 		        $body = $parts[0]['body'];
   	            $rawData = $body->data;
			} else {
                $rawData = $message["payload"]["body"]["data"];
			}
                      
            $sanitizedData = strtr($rawData,'-_', '+/');
            $decodedMessage = base64_decode($sanitizedData);
			
			$body = $decodedMessage;
						
			//$emailLines = explode("\n", $decodedMessage);
			
			/*Code to pull out the first reemail response
			$storeEmailBody = [];
			foreach($emailLines as $line){
    			    print strpos($line, '--- Forwarded message ---');
    			if ( strpos($line, '--- Forwarded message ---') ) {
        			break;
        		} else{
        			array_push($storeEmailBody, $line);
    			}
			}
			
			$decodedMessage = implode("\n", $storeEmailBody)
			*/

            // Transforms Header information into hash
			$valHash = $this->createHeaderHash($headers);
			
			// Parses sender into customer and email 
			$valHash = $this->parseSenderHash($valHash);
					

			// creates Object to assign processing label
			$mods = new Google_Service_Gmail_ModifyMessageRequest();
			$mods->setAddLabelIds( array($this->processLabelId) );
			
			// For Archieving we need to remove the Inbox label as well
			// sets email to be unread
			if($ticketToken->autoArchive == 0){
				$mods->setRemoveLabelIds( array("UNREAD") );
			} else {
				$mods->setRemoveLabelIds( array("UNREAD","INBOX") );			
			}
			try {
				// adds new label and removes UNREAD label
		    	$message = $gmailService->users_messages->modify($ticketToken->emailAccount, $message->getId(), $mods);
				
		  	} catch (Exception $e) {
		    	print 'An error occurred: ' . $e->getMessage();
		  	}	

		  	// Prepares data for insert		
			$DATA = $this->hashToCustomerData($valHash,$body,$ticketToken->business_id);

			// Check to see if we can extract a ticket number from the subject line of the email
		  	$ticketNum = $this->getTicketNumberFromEmail($DATA['title']);


		  	if ($ticketNum == 0) {
			
		  		// create a new ticket
		  	 	$ticketID = $this->addTicket($ticketToken->business_id, $DATA);
		  	 	
		  	} else {
			  	//append to existing ticket

			  	$this->load->model('ticketnote_model');
    
			  	$this->ticketnote_model->save(array("ticket_id"  => $ticketNum,
    								"note"        => $DATA['description'],
                                    "dateCreated" => convert_to_gmt_aprax(),
    								"noteType"    => "Customer Reply" ));
    								
                  								
				$this->load->model('ticket_model');
   
                
				$this->ticket_model->save(array("ticketID"    => $ticketNum,
				                                "dateUpdated" => convert_to_gmt_aprax(),
    											"status"	  => $newStatusId ));		

            }
								
		} // message ForEach
	}


	// 	function for creatin processed label on server
	protected function createProcessLabel($gmailService,$processLabel,$emailAccount)
	{
		$labelObj = new Google_Service_Gmail_Label();
		$labelObj->setName($processLabel);
		
		// Retrieves all labels from server
		$labelsResponse = $gmailService->users_labels->listUsersLabels($emailAccount);
		
		$create = True;
		$labels =array();
		
		// Transforms label into array of objects
		if ($labelsResponse->getLabels()) {
			$labels = array_merge($labels, $labelsResponse->getLabels());
    	}

		// Loop over array in order to determine if the processing label exists
		foreach ($labels as $label) {
			if( $label->getName() == $processLabel){
				// Processing label found, we do not need to create one
				$this->processLabelId = $label->getId();
				$create = False;
			}
    	}		
    	
    	// Begin label creation
		if($create){
			$labelObj = new Google_Service_Gmail_Label();

			$labelObj->setName($processLabel);
			
			try {
				$labelCreate = $gmailService->users_labels->create($emailAccount, $labelObj);
				$this->processLabelId = $labelCreate->getId();
			} catch (Exception $e) {
				print 'An error creating label occurred: ' . $e->getMessage();
				exit;
  			}
  		
		}

	}
	
	
	// function for transforming body of header information into $DATA for creation of ticket
	protected function hashToCustomerData($valHash,$body,$business_id)
	{		
        
        $this->load->model('ticketstatus_model');
		$openStatusId = $this->ticketstatus_model->getNewStatusId($business_id);

        
        $DATA['title'] = $valHash['Subject'];
		$DATA['description'] = $body;
		$DATA['ticketstatus_id'] = $openStatusId;
		$DATA['dateUpdated'] = $valHash['Date'];
		$DATA['locker_id'] = 0;
		$DATA['bag'] = "";
		$DATA['order_id'] = 0;
		$DATA['customer_id'] = 0;
		$DATA['location_id'] = 0;
		$DATA['priority'] = "Medium";
	  	$DATA['customerName'] = $valHash['SendName'];
	  	$DATA['customerEmail'] = $valHash['SendEmail'];
	  	
	    $this->load->model('customer_model');
        //try to find this email
        $customer = $this->customer_model->get_one(array("business_id" => $business_id, "email" => $DATA['customerEmail']));

        // Did we successfully create a customer
        if (empty($customer)) {
            $DATA['customer_id'] = 0;  	
	  	} else {
            $DATA['customer_id'] = $customer->customerID;
		} 

		return $DATA;
		
	}
	
	
	// Extract sender name and email address 
	protected function parseSenderHash($valHash)
	{

		// Get Sender Email
		$email = $valHash['From'];
		preg_match("/\<(.*?)\>/", $email, $output_array);
		$valHash['SendEmail'] = $output_array[1];
		
		// Get Sender Name
		preg_match("/^(.*?) ?\</", $email, $output_array);
		$valHash['SendName'] = $output_array[1];
	
		return $valHash;
	}

	// helper function	
	protected function createHeaderHash($data)
	{
		foreach($data as $row){
			$valHash[$row->getName()] = $row->getValue();
		}		
	
		return $valHash;
	}
	
	// function for parsing the ticket number from the subject of the email
	protected function getTicketNumberFromEmail($text)
	{
      preg_match("/Ticket:(\d+)/", $text, $output_array);
      
	  if($output_array){
      	return $output_array[1];
	  } else {
		return 0;  
	  }
    }
	
	// function for adding a ticket
    protected static function addTicket($business_id, $DATA)
    {
    
        $CI = get_instance();

        if (!is_numeric($business_id)) {
            throw new \Exception("Missing business_id");
        }


        $ticket = new \App\Libraries\DroplockerObjects\Ticket();
        $ticket->business_id = $business_id;
        $ticket->order_id = $DATA['order_id'];
        $ticket->customer_id = $DATA['customer_id'];
        $ticket->employee_id = 0;
        $ticket->title = addslashes($DATA['title']);
        $ticket->issue = addslashes($DATA['description']);
        $ticket->dateCreated = convert_to_gmt_aprax();
        $ticket->dateUpdated = convert_to_gmt_aprax();        
        $ticket->ticketstatus_id = $DATA['ticketstatus_id'];
        $ticket->customerEmail = $DATA['customerEmail'];
        $ticket->customerName = $DATA['customerName'];
        $ticket->ticketSource = 'email';
        $ticket->priority = 'Medium';
        $insert_id = $ticket->save();

        return $insert_id;
    }
    
}