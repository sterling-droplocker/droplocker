<?php

set_time_limit(0);

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\CustomerHistory;
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\ReminderLog;
use App\Libraries\DroplockerObjects\Reminder_Email;
use App\Libraries\DroplockerObjects\LaundryPlan;
use App\Libraries\DroplockerObjects\EverythingPlan;

class Reminders extends MY_Cron_Controller
{
    /**
     * sends birthday coupon reminder to a customer
     * Set up in admin reminder section
     */
    public function birthday($test_customer_id = false)
    {
        $reminders = Reminder::search_aprax(array(
            'couponType' => "birthdayCoupon",
            'status'=>'active'
        ));

        $this->load->helper('actions');
        $this->load->model('customer_model');

        $results = array();
        foreach ($reminders as $reminder) {

            $business = new Business($reminder->business_id);
            setlocale(LC_ALL, $business->locale);

            $this->load->model('language_model');

            $language_by_id = array();
            foreach ($this->language_model->findAllForBusiness($reminder->business_id) as $language) {
                $language_by_id[$language->business_languageID] =  $language;
            }

            // expire any discounts for this business
            $this->load->model('customerdiscount_model');
            $this->customerdiscount_model->expireCustomerDiscount($reminder->business_id);

            $customers = $this->customer_model->get_aprax(array(
                'business_id' => $reminder->business_id,
                'inactive' => 0,
                'type !=' => 'wholesale',
                'noEmail !=' => '1',
                'MONTH(birthday) = MONTH(NOW())' => null,
                'DAY(birthday) = DAY(NOW())' => null,
            ));

            if (!empty($test_customer_id)) {
                 $customers = $this->customer_model->get_aprax(array(
                    "customerID" => $test_customer_id
                ));
            }

            try {
                $this->sendBirthdayEmailsAndAddDiscounts($customers, $language_by_id, $business, $reminder);
            } catch (Exception $e) {
                send_exception_report($e);
            }

        }

        return $results;
    }

    /**
     * @param $customers
     * @param $language_by_id
     * @param $business
     * @param $reminder
     */
    private function  sendBirthdayEmailsAndAddDiscounts($customers, $language_by_id, $business, $reminder)
    {
        $expireDate = date("Y-m-d", strtotime("+{$reminder->expireDays} days"));
        if (is_array($customers)) {
            foreach ($customers as $customer) {
                $discount = null;
                $options = array();

                // do not continue if a customer has an active laundry plan
                $laundryPlans = LaundryPlan::getActiveLaundryPlan($customer->customerID);
                $everythingPlan = EverythingPlan::getActiveEverythingPlan($customer->customerID);
                if ($laundryPlans || $everythingPlan) {
                    continue;
                }

                if (!empty($language_by_id[$customer->default_business_language_id]->locale)) {
                    setlocale(LC_TIME, $language_by_id[$customer->default_business_language_id]->locale);
                } else if (!empty($language_by_id[$business->default_business_language_id]->locale)) {
                    setlocale(LC_TIME, $language_by_id[$business->default_business_language_id]->locale);
                } else {
                    setlocale(LC_TIME, $business->locale);
                }

                $discountNote = get_translation("birthday_reminder", "Email Reminders", array(), "Birthday Coupon", $customer->customerID);

                // if a discount is required
                if (!$reminder->noDiscount) {
                   $discount = $this->customerdiscount_model->addDiscount(
                        $customer->customerID,
                        $reminder->amount,
                        $reminder->amountType,
                        'one time',
                        $discountNote,
                        $discountNote,
                        $expireDate,
                        1,
                        $reminder->maxAmt
                    );

                    $options['customerDiscount_id'] = $discount;

                    if (!$discount) {
                        //FIXME: this is going nowhere, should be stored or emailed to admin
                        $discountNote = "Error trying to create discount (Birthday reminder). Most likely the customer already has the same active discount in their account.";
                        continue;
                            }
                    }
                }
                if (!empty($reminder->subject) && !empty($reminder->body)) {
                    $options['customer_id'] = $customer->customerID;
                    $options['business_id'] = $customer->business_id;

                    $emailID = $this->sendEmail($business, $reminder, $customer, $options);

                    if ($emailID) {
                        $customerHistory = new CustomerHistory();
                        $customerHistory->store($customer->customerID, $customer->business_id, "Internal Note", "Automated email sent to customer [birthdayReminder]: ".$discountNote);
                    }

                    $this->logReminder($reminder, $customer, $emailID, $discount);

                    $results[$reminder->business_id][$reminder->couponType][$customer->email]['discount'] = $discount;
                } else {
                    $body = "WARNING: Email and Discount not applied for Reminder: ". $reminder->id ."<br>";
                    $emailFrom = get_business_meta($reminder->business_id, 'customerSupport');
                    send_email($emailFrom, SYSTEMFROMNAME, DEVEMAIL, 'Birthday Reminder no applied.', $body);
                }
            }
        }
    }

    /**
     * Sends a reminder email
     *
     * @param Business $business
     * @param Reminder $reminder
     * @param Customer $customer
     * @param array $options
     * @throws Exception
     * @return integer the email id
     */
    protected function sendEmail($business, $reminder, $customer, $options)
    {
        $business_language_id = get_customer_business_language_id($customer->customerID);
        $reminder_email = Reminder_Email::search(array(
            "reminder_id" => $reminder->reminderID,
            "business_language_id" => $business_language_id
        ));

        if (empty($reminder_email)) {
            throw new Exception("Could not find reminder email for reminder ID '{$reminder->reminderID}' and business language ID '$business_language_id'");
        }

        // format body
        $data = get_macros($options);
        $subject = email_replace($reminder_email->subject, $data);
        $body = email_replace($reminder_email->body, $data);

        // get unsuscribe link
        $domain = Business::getAccountDomain($reminder->business_id);
        $unsuscribe = get_translation("unsuscribe", "newsletter",
            array(
                'companyName' => $business->companyName,
                'unsubscribe' => "https://".$domain."/main/unsubscribe",
            ),
            "To be removed from all %companyName% marketing emails: <a href='%unsubscribe%'>Unsubscribe</a>",
            $customer->customerID);

        $body .= "<br style='clear:left'><div style='font-size:12px; padding:5px;'>$unsuscribe</div>";

        // send the email
        $emailID = queueEmail(get_business_meta($reminder->business_id, 'customerSupport'),
                              get_business_meta($reminder->business_id, 'from_email_name'),
                              $customer->email, $subject, $body, array(), null, $reminder->business_id, $customer->customerID);

        return $emailID;
    }


    /**
     * Logs the reminder
     * @param Reminder $reminder
     * @param Customer $customer
     * @param integer $email_id
     * @param integer $discount_id
     * @return ReminderLog
     */
    protected function logReminder($reminder, $customer, $email_id, $discount_id = null)
    {
        //log that this reminder was sent
        $log = new ReminderLog();
        $log->reminder_id = $reminder->reminderID;
        $log->customer_id = $customer->customerID;
        $log->email_id = $email_id;
        $log->customerDiscount_id = $discount_id;
        $log->save();

        return $log;
    }

}