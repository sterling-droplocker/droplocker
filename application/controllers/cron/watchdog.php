<?php

class Watchdog extends MY_Cron_Controller
{

    public function set($name = null)
    {
        if (empty($name)) {
            echo "ERROR: Missing service name\nUsage: php index.php cron/watchdog/set service-name\n";
            exit(1);
        }

        $this->db->query("REPLACE INTO cronWatchdog (name, lastRun) VALUES (?, NOW())", array($name));
    }

    public function check($name = null, $time = '2 min')
    {
        if (empty($name)) {
            echo "ERROR: Missing service name\nUsage: php index.php cron/watchdog/check service-name time\n";
            exit(1);
        }

        $record = $this->db->query("SELECT * FROM cronWatchdog WHERE NAME = ?", array($name))->row();
        $date = gmdate("Y-m-d H:i:s");
        $hostname = gethostname();

        if (empty($record)) {
            $subject = "Service $name is not running ($date)";
            $body = "The server at $hostname could not found a record in watchdog table for the service '$name'\nDate: $date";
            send_development_email($subject, $body);
        }

        $timestamp = strtotime('-' . $time);

        if (strtotime($record->lastRun) < $timestamp) {
            $subject = "Service $name is not running ($date)";
            $body = "The timestamp is outside the $time window for the service '$name'\nDate: $date\nLastRun: {$record->lastRun}";
            send_development_email($subject, $body);
        }
    }

}