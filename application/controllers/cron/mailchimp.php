<?php

/**
 * Class Mailchimp
 *
 * This cron updates customers from DLA to Mailchimp
 *
 * Sample configuration:
 * 0 2 * * * php /var/www/html/reports.droplocker.com/index.php cron/mailchimp
 */

class Mailchimp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // load reporting databse
        $this->reporting_database = $this->load->database("reporting", true);
    }

    public function index()
    {
        $startTime = convert_to_gmt_aprax();

        $this->load->model('business_model');
        $businesses = $this->business_model->get_all();

        foreach ($businesses as $business) {
            $params = array(
                'business' => $business,
                'api_key' => get_business_meta($business->businessID, 'mailchimp_api'),
                'enabled' => get_business_meta($business->businessID, 'mailchimp_enabled'),
                'list_name' => trim(get_business_meta($business->businessID, 'mailchimp_list')),
                'last_update' => get_business_meta($business->businessID, 'mailchimp_last_update')
            );

            $this->load->library('Mailchimplibrary', $params);

            // hack to rerun constructor
            $this->mailchimplibrary = new Mailchimplibrary($params);

            if ($this->isMailchimpAutomaticUpdate($business->businessID)) {
                $list_info = $this->mailchimplibrary->get_list_info();

                if (empty($list_info)) {
                    $this->load->model('business_language_model');
                    $language = $this->business_language_model->get_by_primary_key($business->default_business_language_id);
                    $this->mailchimplibrary->createDroplockerList($language);
                    $list_info = $this->mailchimplibrary->get_list_info();

                    $this->mailchimplibrary->createMergeFields();
                }

                $this->batchUpdateMembers($business, $list_info);

                // Since the customer_report is updated every night, now -1 day will ensure records customers won't be missed.
                update_business_meta($business->businessID, 'mailchimp_last_update', convert_to_gmt_aprax("now -1 day"));
            }
        }

        $endTime = convert_to_gmt_aprax();
        $this->db->insert('cronLog', array(
            'job' => __METHOD__,
            'results' => serialize(array(
                'startTime' => $startTime,
                'endTime' => $endTime
            ))
        ));
    }


    /**
     * Update members on mailchimp
     *
     * @param object $business
     * @param string $list_info
     */
    public function batchUpdateMembers($business, $list_info)
    {
        $i = 0;
        $processed = 0;
        $lastUpdate = get_business_meta($business->businessID, 'mailchimp_last_update');
        do {
            $members = array();
            $batchStart = $i * 500;

            $query = $this->reporting_database->query(
                "SELECT c.customerID, c.firstName, c.lastName, c.email, c.address1, rc.lastOrder, c.location_id, 
                        rc.route_id, rc.avgOrderLapse, c.zip, c.kioskAccess, c.inactive, c.sex, rc.totGross, rc.visits, 
                        c.signupDate, rc.orderCount, c.business_id, rc.firstOrder, c.city, c.state, c.ip, c.birthday, rc.planStatus
                      FROM reports_customer rc
                      INNER JOIN customer c ON c.customerID = rc.customerID 
                      WHERE rc.business_id = ? AND c.lastUpdated > COALESCE(c.mailchimp_sync_date, NOW() - INTERVAL 2 WEEK) LIMIT ?, 500",
                array($business->businessID, $batchStart)
            );

            $customers = $query->result();

            if ($customers) {
                $processed += $this->mailchimplibrary->createBatchUpdateMembers($customers);
            }

            $i++;
        } while (!empty($customers));

        update_business_meta($business->businessID, 'mailchimp_last_update_qty', $processed);
    }

    /**
     * Check if mailchimp is enabled and automatic updates are enabled
     *
     * @param int $business_id
     * @return bool
     */
    public function isMailchimpAutomaticUpdate($business_id)
    {
        $mailchimp_enabled = get_business_meta($business_id, 'mailchimp_enabled');
        $mailchimp_automatic_update = get_business_meta($business_id, 'mailchimp_automatic_update');

        return $mailchimp_enabled && $mailchimp_automatic_update;
    }

}
