<?php

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\Bag;
class Tools extends MY_Cron_Controller
{

    public function move_business($source, $destination)
    {

        $srcBusiness = new Business($source);
        $destBusiness = new Business($destination);

        // check for duplicate bags
        $duplicates = $this->db->query("SELECT b1.* FROM bag AS b1
            JOIN (SELECT * FROM bag WHERE business_id = ?) AS b2
            ON (b1.bagNumber = b2.bagNumber)
            WHERE b1.business_id = ?", array($destination, $source))->result();

        foreach ($duplicates as $duplicate) {
            $new = Bag::get_new_bagNumber();
            $this->db->query("UPDATE bag SET bagNumber = ? WHERE barcodeID = ?", array($new, $duplicate->barcodeID));
            echo ".   Updated bag {$duplicate->bagNumber} to {$new}\n";
        }

        // check for duplicate barcodes
        echo "Moving bags\n";
        $this->db->query("UPDATE bag SET business_id = ? WHERE business_id = ?", array($destination, $source));


        $duplicates = $this->db->query("SELECT b1.* FROM barcode AS b1
            JOIN (SELECT * FROM barcode WHERE business_id = ?) AS b2
            ON (b1.barcode = b2.barcode)
            WHERE b1.business_id = ?", array($destination, $source))->result();

        $new = $destBusiness->get_next_barcode();
        foreach ($duplicates as $duplicate) {
            $this->db->query("UPDATE barcode SET barcode = ? WHERE barcodeID = ?", array($new, $duplicate->barcodeID));
            echo ".   Updated barcode {$duplicate->barcode} to {$new}\n";
            $new++;
        }

        echo "Moving barcodes\n";
        $this->db->query("UPDATE barcode SET business_id = ? WHERE business_id = ?", array($destination, $source));


        // Items
        echo "Moving items\n";
        $this->db->query("UPDATE item SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving item pictures\n";
        $this->db->query("UPDATE picture SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving item specifications\n";
        $this->db->query("UPDATE specification SET business_id = ? WHERE business_id = ?", array($destination, $source));

        // Item's settings
        echo "Moving custom options\n";
        $this->db->query("UPDATE customOption SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving custom option values\n";
        $this->db->query("UPDATE customOptionValue SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // Customers
        echo "Moving customers\n";

        $duplicates = $this->db->query("SELECT c1.* FROM customer AS c1
            JOIN (SELECT * FROM customer WHERE business_id = ?) AS c2
            ON (c1.email = c2.email)
            WHERE c1.business_id = ?", array($destination, $source))->result();

        foreach ($duplicates as $duplicate) {
            $new = 'duplicate-' . $duplicate->email;
            $this->db->query("UPDATE customer SET email = ? WHERE customerID = ?", array($new, $duplicate->customerID));
            echo ".   Updated customer email {$duplicate->email} to {$new}\n";
        }

        $this->db->query("UPDATE customer SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving claims\n";
        $this->db->query("UPDATE claim SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving customer discounts\n";
        $this->db->query("UPDATE customerDiscount SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving customer history\n";
        $this->db->query("UPDATE customerHistory SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving follow up calls\n";
        $this->db->query("UPDATE followupCall SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving invoices\n";
        $this->db->query("UPDATE invoice SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving customer laundry plans\n";
        $this->db->query("UPDATE laundryPlan SET business_id = ? WHERE business_id = ? and type='laundry_plan'", array($destination, $source));

        echo "Moving customer everything plans\n";
        $this->db->query("UPDATE laundryPlan SET business_id = ? WHERE business_id = ? and type='everything_plan'", array($destination, $source));

        echo "Moving customer product plans\n";
        $this->db->query("UPDATE laundryPlan SET business_id = ? WHERE business_id = ? and type='product_plan'", array($destination, $source));

        echo "Moving orders\n";
        $this->db->query("UPDATE orders SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving home deliveries\n";
        $this->db->query("UPDATE orderHomeDelivery SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving home pickups\n";
        $this->db->query("UPDATE orderHomePickup SET business_id = ? WHERE business_id = ?", array($destination, $source));


        // credit cards
        echo "Moving credit cards\n";
        $this->db->query("UPDATE creditCard SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving kiosk access\n";
        $this->db->query("UPDATE kioskAccess SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving credit card transactions\n";
        $this->db->query("UPDATE creditCardTransaction SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving credit card transactions\n";
        $this->db->query("UPDATE transaction SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // Business settings
        echo "Moving business laundry plans\n";
        $this->db->query("UPDATE businessLaundryPlan SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving coupons\n";
        $this->db->query("UPDATE coupon SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving notes\n";
        $this->db->query("UPDATE dayNotes SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving discount reasons\n";
        $this->db->query("UPDATE discountReason SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving report filters\n";
        $this->db->query("UPDATE filter SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving images\n";
        $this->db->query("UPDATE image SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving locations\n";

        $duplicates = $this->db->query("SELECT l1.* FROM location AS l1
            JOIN (SELECT * FROM location WHERE business_id = ?) AS l2
            ON (l1.address = l2.address)
            WHERE l1.business_id = ?", array($destination, $source))->result();

        foreach ($duplicates as $duplicate) {
            $new = 'duplicate ' . $duplicate->address;
            $this->db->query("UPDATE location SET address = ? WHERE locationID = ?", array($new, $duplicate->locationID));
            echo ".   Updated location address '{$duplicate->address}' to '{$new}'\n";
        }

        $this->db->query("UPDATE location SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving location neighborhoods\n";
        $this->db->query("UPDATE location_neighborhood SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving newsletters\n";
        $this->db->query("UPDATE newsletter SET business_id = ? WHERE business_id = ?", array($destination, $source));

        //echo "Moving reminders\n";
        //$this->db->query("UPDATE reminder SET business_id = ?, status = 'paused' WHERE business_id = ?", array($destination, $source));

        echo "Moving scheduled orders\n";
        $this->db->query("UPDATE scheduledOrders SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving shipments\n";
        $this->db->query("UPDATE shipment SET business_id = ? WHERE business_id = ?", array($destination, $source));


        echo "Moving tax groups\n";
        $this->db->query("UPDATE taxGroup SET business_id = ?, name = CONCAT('copied-', name) WHERE business_id = ?", array($destination, $source));

        echo "Moving tax tables\n";
        $this->db->query("UPDATE taxTable SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // Products
        echo "Moving products\n";
        $this->db->query("UPDATE product SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving product categories\n";
        $this->db->query("UPDATE productCategory SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving product location prices\n";
        $this->db->query("UPDATE product_location_price SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving suppliers\n";
        $this->db->query("UPDATE supplier SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving upcharge groups\n";
        $this->db->query("UPDATE upchargeGroup SET business_id = ? WHERE business_id = ?", array($destination, $source));


        // employees
        echo "Moving employees\n";
        $this->db->query("UPDATE business_employee SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving employee routes\n";
        $this->db->query("UPDATE empRoutes SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving osha injury log\n";
        $this->db->query("UPDATE oshaInjury SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // Drivers
        echo "Moving driver log\n";
        $this->db->query("UPDATE driverLog SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving driver notes\n";
        $this->db->query("UPDATE driverNotes SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // Droid
        echo "Moving droid errors\n";
        $this->db->query("UPDATE droidError SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid deliveries\n";
        $this->db->query("UPDATE droid_postDeliveries SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid driver notes\n";
        $this->db->query("UPDATE droid_postDriverNotes SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid pickup reminders\n";
        $this->db->query("UPDATE droid_postPickupReminders SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid pickups\n";
        $this->db->query("UPDATE droid_postPickups SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid return to office\n";
        $this->db->query("UPDATE droid_postReturnToOffice SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid time cards\n";
        $this->db->query("UPDATE droid_postTimeCards SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving droid time cards\n";
        $this->db->query("UPDATE droid_postTimeCards SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // Machines
        echo "Moving lot\n";
        $this->db->query("UPDATE lot SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving machines\n";
        $this->db->query("UPDATE machine SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving maintenance logs\n";
        $this->db->query("UPDATE maintenanceLog SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving pressing stations\n";
        $this->db->query("UPDATE pressingStation SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // reports
        echo "Moving report orders\n";
        $this->db->query("UPDATE report_orders SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving report customers\n";
        $this->db->query("UPDATE reports_customer SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // tickets
        echo "Moving support tickets\n";
        $this->db->query("UPDATE ticket SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving ticket labels\n";
        $this->db->query("UPDATE ticketLabel SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving ticket problems\n";
        $this->db->query("UPDATE ticketProblem SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving ticket status\n";
        $this->db->query("UPDATE ticketStatus SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving ticket subscribers\n";
        $this->db->query("UPDATE ticketSubscribers SET business_id = ? WHERE business_id = ?", array($destination, $source));



        // HEAVY TABLES
        echo "Moving emails\n";
        $this->db->query("UPDATE email SET business_id = ? WHERE business_id = ?", array($destination, $source));

        echo "Moving email logs\n";
        $this->db->query("UPDATE emailLog SET business_id = ? WHERE business_id = ?", array($destination, $source));

    }

}