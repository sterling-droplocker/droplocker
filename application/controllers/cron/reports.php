<?php

set_time_limit(0);

use App\Libraries\DroplockerObjects\Business;

class Reports extends MY_Cron_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }
    /**
     * Sends email to business with daily reassigned barcodes report
     * cmd: php index.php cron/reports email_reassigned_barcodes_report 
     */
    public function email_reassigned_barcodes_report()
    {
        $this->load->model("customer_item_model");
        
        $start_date = date("Y-m-d", time() - 60 * 60 * 24);
        $end_date   = date("Y-m-d");

        $sql = "select bm.business_id from businessMeta bm where bm.key = 'send_reassigned_barcode_report_daily' and bm.value = 1";

        $query = $this->db->query($sql);
        $result = $query->result();

        foreach ($result as $bmeta) {
            $business = new Business($bmeta->business_id);
            $email = get_business_meta($business->businessID, 'send_reassigned_barcode_report_email');
            if (!$email) {
                continue;
            }

            $result = $this->customer_item_model->get_customer_item_history($business->businessID, false, $limit, $page_from, $start_date, $end_date);
            $records = $result["items"];

            // stop sending blank emails when not reassigned items were found
            if (empty($records)) continue;

            $table = "<h2>Reassigned Barcode Report</h2><table width='800'><tr style='background-color:#eaeaea'><th>Barcode</th><th>Barcode Reassigned From</th><th>Barcode Reassigned To</th><th>Employee Name</th><th style='width:15em'>Order</th><th>Picture</th></tr>";
            $admin_url = get_admin_url();
            $this->load->model("picture_model");
            $this->load->model("barcode_model");

            

            foreach ( $records as $index => $item ) {
                
                $barcode = $this->barcode_model->getBarcode($item->barcode, $business->businessID);
                $table .= '<tr>';
                $table .= '<td><a href="/admin/orders/edit_item?orderID='.$item->orderID.'&itemID='.$barcode[0]->itemID.'">'.$item->barcode.'</a></td>';
                $table .= '<td><a target="_blank" href="'.$admin_url.'/admin/customers/detail/'.$item->from_customer_id.'">'.  getPersonName($item, TRUE, 'from_customer_firstName','from_customer_lastName').'</a></td>';
                $table .= '<td><a target="_blank" href="'.$admin_url.'/admin/customers/detail/'.$item->to_customer_id.'">'.  getPersonName($item, TRUE, 'to_customer_firstName','to_customer_lastName').'</a></td>';
                $table .= '<td><a target="_blank" href="'.$admin_url.'/admin/employees/manage/'.$item->employee_id.'">'.  getPersonName($item, TRUE, 'employee_firstName','employee_lastName').'</a></td>';
                $table .= '<td><a target="_blank" href="'.$admin_url.'/admin/orders/details/'.$item->orderID.'" target="_blank">'.$item->orderID.'</a><br/>'.local_date($business->timezone, $item->date, 'n/d/y g:i:s a').'</td>';
                
                //get item picture
                $pictures = $this->picture_model->get_aprax(array("item_id" => $barcode[0]->itemID));
                $picture = end($pictures);
                if (!empty($picture->file)) {
                    $table .= "<td><a target='_blank' href='https://s3.amazonaws.com/LaundryLocker/{$picture->file}'><img style='width:100px' src='https://s3.amazonaws.com/LaundryLocker/{$picture->file}' alt='Current Picture' /></a></td>";
                }
                else {
                    $table .= '<td>NO PICTURE</td>';
                } 
                $table .= '</tr>';
            }
            $table .= "</table><br><br>";

            $this->db->insert('cronLog', array('results'=>$table, 'job'=>__METHOD__));
            $emailID = queueEmail(SYSTEMEMAIL,  "Reassigned Barcode Report",  $email,'Reassigned Barcode Report', $table, array(), null, $business->businessID);
            
            echo $emailID;
           
        }
    }
}