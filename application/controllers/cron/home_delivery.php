<?php

use App\Libraries\DroplockerObjects\OrderHomePickup;

set_time_limit(0);

use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;

class Home_Delivery extends MY_Cron_Controller
{
    /**
     * Inactivate claims at HD location
     *
     * @param string $currentTime
     */
    public function deactivate_claims($currentTime = null)
    {
        if (is_null($currentTime)) {
            $currentTime = gmdate('H:00:00'); // Get the current hour
        }

        // select business that match the current hour
        $selected_business = $this->db->query("SELECT businessID, deactivate_hd_claims_time
            FROM business
            WHERE deactivate_hd_claims_time = ?",
            array($currentTime))->result();

        foreach ($selected_business as $business) {
            $results = $this->deactivateHomeDeliveryClaimsForBusiness($business->businessID);
            $this->db->insert('cronLog', array('job' => __METHOD__, 'results' => serialize($results)));
        }
    }

    protected function deactivateHomeDeliveryClaimsForBusiness($business_id)
    {
        $homeDeliveryServiceTypesList = "'" . implode("', '", Location::getHomeDeliveryServiceTypes()) . "'";

        $q = "UPDATE claim
              JOIN locker ON claim.locker_id = locker.lockerID
              JOIN location ON locker.location_id = location.locationID
              SET active = 0
              WHERE claim.business_id = ?
              AND active = 1
              AND location.serviceType IN ($homeDeliveryServiceTypesList)
              AND claim.notes IS NULL"; //Keep active claims that have notes and were created manually using myaccount page

        $result = $this->db->query($q, array($business_id));

        return $result?true:false;
    }

    /**
     * Schedules the claims for a delivery route
     *
     * @param string $time
     * @param string $date
     */
    public function schedule_claims($time = null, $date = null)
    {
        if (is_null($time)) {
            $time = gmdate('H:00:00'); // Get the current hour
        }

        if (is_null($date)) {
            $date = gmdate('Y-m-d');
        }

        // select business that match the current hour
        $selected_business = $this->db->query("SELECT businessID, scheduled_orders_time
            FROM business
            WHERE scheduled_orders_time = ?",
            array($time))->result();

        foreach ($selected_business as $business) {
            $results = $this->scheludeClaimsForBusiness($business->businessID, "$date $time GMT");
            $this->db->insert('cronLog', array('job' => __METHOD__, 'results' => serialize($results)));
        }
    }

    /**
     * Schedules the claims for a business on a date
     *
     * @param int $business_id
     * @param string $date
     */
    protected function scheludeClaimsForBusiness($business_id, $date)
    {
        $service = get_business_meta($business_id, 'home_delivery_service');
        $homeDeliveryService = DropLocker\Service\HomeDelivery\Factory::getDefaultService($business_id);

        // Only active for droplocker home delivery provider
        if ($service != 'droplocker') {
            return array();
        }

        $this->load->model('claim_model');
        $this->load->model('locker_model');

        $day = strtolower(convert_from_gmt_aprax($date, 'l', $business_id));

        $sql = "SELECT customer.*, delivery_zone.location_id AS home_delivery_location_id,
                    delivery_customerSubscription.*, delivery_windowID,
                    customer.lat AS customer_lat,
                    customer.lon AS customer_lon,
                    customer.address1 AS customer_address1,
                    customer.address2 AS customer_address2,
                    customer.zip AS customer_zip,
                    customer.city AS customer_city,
                    customer.state AS customer_state
                FROM delivery_customerSubscription
                JOIN customer ON (customer_id = customerID)
                JOIN delivery_zone ON (delivery_customerSubscription.delivery_zone_id = delivery_zoneID)
                JOIN delivery_window ON (delivery_window.delivery_zone_id = delivery_zoneID)
                WHERE customer.business_id = ?
                      AND $day > 0";
        //$sql .= " AND customerID = 170892";

        $customers = $this->db->query($sql, array($business_id))->result();
        $results = array();

        foreach ($customers as $customer) {            
            //get notes from the newest claim before deactivating claims
            $newest_claim = $this->claim_model->get_claims_by_customer($customer->customerID, array("column"=>"claimID", "direction"=>"desc"));
            $recurring_claim_notes = false;
            if (!empty($newest_claim)) {
                $recurring_claim_notes = $newest_claim[0]->notes;
            }

            //make sure a claim doesn't already exist
            $this->claim_model->deactivate_customer_claims($customer->customerID);

            $customer = $this->updateCustomerCoords($customer);

            $locker = $this->locker_model->get_one(array('location_id' => $customer->home_delivery_location_id));

            if (empty($locker)) {
                $results[] = "Error: missing locker at location {$customer->home_delivery_location_id}";

                send_admin_notification("Missing locker in location {$customer->home_delivery_location_id}",
                    "Can't schedule home delivery claim for customer ".getPersonName($customer)." ID: {$customer->customerID}", $customer->business_id);

                continue;
            }

            if (empty($customer->lat) || empty($customer->lon)) {
                $results[] = "Error: missing coords for customer {$customer->customerID}";

                send_admin_notification("Missing missing coords for customer {$customer->customerID}",
                    "Can't schedule home delivery claim for customer ".getPersonName($customer)." ID: {$customer->customerID}", $customer->business_id);

                //continue;
            }

            //then create new claim
            $data = array(
                'customer_id' => $customer->customerID,
                'business_id' => $customer->business_id,
                'locker_id'   => $locker->lockerID
            );
            
            if ($recurring_claim_notes) {
                $data['recurring_claim_notes'] = $recurring_claim_notes;
            }

            $claim_id = $this->claim_model->new_claim($data);


            $homePickup = new OrderHomePickup();

            $homePickup->claim_id    = $claim_id;
            $homePickup->business_id = $business_id;
            $homePickup->customer_id = $customer->customerID;
            $homePickup->order_id    = 0;
            $homePickup->location_id = $customer->home_delivery_location_id;
            $homePickup->state       = $customer->state;
            $homePickup->city        = $customer->city;
            $homePickup->address1    = $customer->address1;
            $homePickup->address2    = $customer->address2;
            $homePickup->zip         = $customer->zip;
            $homePickup->lat         = (float)$customer->lat;
            $homePickup->lon         = (float)$customer->lon;
            $homePickup->service     = $service;
            $homePickup->sortOrder   = $customer->sortOrder;
            $homePickup->notes       = $customer->notes;
            $homePickup->save();

            $pickupWindow = $homeDeliveryService->getPickupWindowByID($homePickup, $customer->delivery_windowID);
            if ($pickupWindow) {
                $homePickup->pickup_window_id = $customer->delivery_windowID;
                $homePickup->pickupDate       = gmdate('Y-m-d', strtotime($pickupWindow->starts_at));
                $homePickup->windowStart      = gmdate('Y-m-d H:i:s', strtotime($pickupWindow->starts_at));
                $homePickup->windowEnd        = gmdate('Y-m-d H:i:s', strtotime($pickupWindow->ends_at));
                $homePickup->fee              = 0;
            }

            $homePickup->save();

            try {
                $homeDeliveryService->scheduleHomePickup($homePickup);
            } catch (Exception $e) {
                send_exception_report($e);
            }

            $results[] = $claim_id;

            Email_Actions::send_transaction_emails(array(
                'do_action' => 'scheduled_home_delivery_claim',
                'customer_id' => $customer->customerID,
                'claim_id' => $claim_id,
                'business_id' => $customer->business_id
           ));
        }

        return $results;
    }

    protected function updateCustomerCoords($customer)
    {
        $this->load->model('delivery_customersubscription_model');
        $this->load->model('customer_model');

        if ($customer->lat != $customer->customer_lat || $customer->lon != $customer->customer_lon) {

            $this->delivery_customersubscription_model->update_all(array(
                'lat'      => $customer->customer_lat,
                'lon'      => $customer->customer_lon,
                'address1' => $customer->customer_address1,
                'address2' => $customer->customer_address2,
                'zip'      => $customer->customer_zip,
                'city'     => $customer->customer_city,
                'state'    => $customer->customer_state,
            ), array(
                'delivery_customersubscriptionID' => $customer->delivery_customerSubscriptionID
            ));

            send_admin_notification('Customer address updated for delivery route',
                getPersonName($customer)." ID: {$customer->customerID} has a subscription to a delivery route and changed its address to: {$customer->customer_address1} {$customer->customer_address2}.",
                $customer->business_id, $customer->customerID);

            $customer->lat      = $customer->customer_lat;
            $customer->lon      = $customer->customer_lon;
            $customer->address1 = $customer->customer_address1;
            $customer->address2 = $customer->customer_address2;
            $customer->zip      = $customer->customer_zip;
            $customer->city     = $customer->customer_city;
            $customer->state    = $customer->customer_state;
        }

        if (empty($customer->lat) || empty($customer->lon)) {
            $res = geocode($customer->customer_address1, $customer->customer_city, $customer->customer_state);

            if ( empty($res) || empty($res['lat']) || empty($res['lng']) ) {

                return $customer;
            }

            $this->customer_model->update_all(array(
                'lat' => $res['lat'],
                'lon' => $res['lng'],
            ), array(
                'customerID' => $customer->customerID,
            ));

            $this->delivery_customersubscription_model->update_all(array(
                'lat'      => $res['lat'],
                'lon'      => $res['lng'],
                'address1' => $customer->customer_address1,
                'address2' => $customer->customer_address2,
                'zip'      => $customer->customer_zip,
                'city'     => $customer->customer_city,
                'state'    => $customer->customer_state,
            ), array(
                'delivery_customerSubscriptionID' => $customer->delivery_customerSubscriptionID
            ));

            $customer->lat      = $res['lat'];
            $customer->lon      = $res['lng'];
            $customer->address1 = $customer->customer_address1;
            $customer->address2 = $customer->customer_address2;
            $customer->zip      = $customer->customer_zip;
            $customer->city     = $customer->customer_city;
            $customer->state    = $customer->customer_state;

        }

        return $customer;
    }

    /**
     * Gets an array of all delivery windows sorted by ID
     *
     * @param \DropLocker\Service\HomeDelivery\IHomeDeliveryService $homeDeliveryService
     * @param object $homeDelivery
     * @return array
     */
    protected function getPickupWindowsByID($homeDeliveryService, $homePickup)
    {
        $windows_by_date = $homeDeliveryService->getPickupWindows($homePickup);

        $pickup_windows_by_id = array();
        foreach ($windows_by_date as $date => $windows) {
            foreach ($windows as $window) {
                $pickup_windows_by_id[$window['id']] = $window;
            }
        }

        return $pickup_windows_by_id;
    }

}