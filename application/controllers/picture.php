<?php
require("{$_SERVER['DOCUMENT_ROOT']}/application/third_party/timthumb.php");
class Picture extends CI_Controller
{
    /**
     * Can take the following GET parameters
     *  ac : Optional. "1", to enable zoomed crop. "0", to disable zoomed crop. Default is "0".
     *  w: Optional. The width of the image in pixels
     *  h: Optional. The height of the image in pixels
     *  src: Required. The image path
     */
    public function resize()
    {
        timthumb::start();
    }
}
