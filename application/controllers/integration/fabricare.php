<?php
require 'CI_Integration.php';

use App\Libraries\DroplockerObjects\Business;

class Fabricare extends CI_Integration
{
    /**
     *
     * @var FabricareLibrary
     */
    public $fabricarelibrary;

    public function get_orders($bag_number = 0, $save_on_server = false)
    {
        date_default_timezone_set($this->business->timezone);

        $this->load->library('FabricareLibrary', array(
            'business_id' => $this->business_id,
            'bag_number' => $bag_number,
            'file_path' => $this->path . 'Import.XML',
        ));

        $this->fabricarelibrary->export(false, true, $save_on_server);

        exit();
    }

    public function process_upload($file = null)
    {
        $files = scandir($this->path);
        foreach($files as $file) {
            if (is_file($this->path.$file)) {
                if (strtoupper($file) == 'RESPONSE.XML') { // interested in RESPONSE.XML only
                    echo "Processing file: " . $this->path . $file . "<br>";
                    $this->process_file($file);
                }
            }
        }
    }

    protected function process_file($file)
    {
        // backup proccessed files here
        $file_path = DEV ? "/tmp/" : "/home/droplocker/";
        $file_path .= "fabricare/{$this->business_id}/";

        // create dir if not exists
        if (!is_dir($file_path)) {
            mkdir($file_path, 0777, true);
        }

        // read original file
        $xml_file_string = file_get_contents($this->path.$file);

        // save a copy
        file_put_contents($file_path . gmdate('Y-m-d_H.i.s_') . 'response.xml', $xml_file_string);

        //process the response into the automat table
        $this->load->library('FabricareLibrary', array('business_id' => $this->business_id));
        $result = $this->fabricarelibrary->parseResponse($xml_file_string);
    }
}
