<?php

require 'CI_Integration.php';

class MetalPro extends CI_Integration
{
    public function empty_conveyor($barcode = null)
    {
        if($barcode) {
	       $sql = "SELECT DISTINCT order_id AS orderID
	           FROM orderItem
	           JOIN item ON itemID = orderItem.item_id
	           JOIN barcode ON barcode.item_id = itemID
	           WHERE barcode = ? AND barcode.business_id = ?";
	       $params = array($barcode, $this->business_id);

        } else {
            //tells the conveyor to offload all the orders for today
            //don't count any box shirts
            $sql = "SELECT orderID
                    FROM orders
                    WHERE orderStatusOption_id = 3
                    AND business_id = ?";
            $params = array($this->business_id);
        }

        $orders = $this->db->query($sql, $params)->result();
        $data = array();
        foreach ($orders as $line) {
            $data[] = str_pad($line->orderID,20).str_pad("",58).'P            ' . "\r\n";
        }

        $data = implode('', $data);
        file_put_contents($this->path . "AUTOMAT.IN", $data);

        header("Content-type: text/plain");
        echo $data;
    }

    public function get_orders($bagNumber = null)
    {
        if($bagNumber) {
            //removed and product_id not in (34, 179, 180) replaced with and notes NOT LIKE '%box%'
            $sql = "SELECT DISTINCT order_id, orders.customer_id, bagNumber, firstName, lastName, phone, route_id, sortOrder
                    FROM orderItem
                    JOIN orders ON (orderItem.order_id = orderID)
                    JOIN bag ON (bagID = orders.bag_id)
                    JOIN customer ON (customerID = orders.customer_id)
                    LEFT JOIN locker ON (lockerID = orders.locker_id)
                    LEFT JOIN location ON (locationID = locker.location_id)
                    WHERE orderStatusOption_id != 10
                    AND item_id > 0
                    AND bag.bagNumber = ? AND bag.business_id = ?
                    ORDER BY orderItem.order_id";

            $params = array($bagNumber, $this->business_id);
        } else {
            //get all orders in status inventoried or on payment hold
            $sql = "SELECT DISTINCT order_id, orders.customer_id, bagNumber, firstName, lastName, phone, route_id, sortOrder
                    FROM orderItem
                    JOIN orders ON orderItem.order_id = orderID
                    LEFT JOIN bag on bagID = orders.bag_id
                    JOIN customer ON (customerID = orders.customer_id)
                    LEFT JOIN locker ON (lockerID = orders.locker_id)
                    LEFT JOIN location ON (locationID = locker.location_id)
                    WHERE orderStatusOption_id in (3,5,7)
                    AND orders.business_id = ?
                    AND item_id > 0
                    ORDER BY orderItem.order_id";

            $params = array($this->business_id);
        }

        $orders = $this->db->query($sql, $params)->result();
        $loadBags = get_business_meta($this->business_id, 'loadBags');

        $data = array();

        $format = array(
            'lot' => '%-20d',
            'ngarm' => '%02d',
            'garment' => '%02d',
            'garcode' => '%-20d',
            'gardesc' => '%-30s',
            'perc' => '%3d',
            'hung' => '%s',
            'status' => '%s',
            'mag' => '%2s',
            'slot' => '%5s',
            'out' => '%5s',
            'customer' => '%20s',
            'rfid' => '%20s',
            'arm' => '%03d',
            'labelinfo' => '%-186s',
        );

        $line_format = implode('', $format);


        $label = array(
            'customer' => '%.30s',
            'phone' => '%.30s',
            'order' => '%.30s',
            'route_id' => '%.30s',
            'sortOrder' => '%.30s',
        );
        $label_format = implode(';', $label) . ';';

        foreach ($orders as $order) {

            if (!$bagNumber) {
                $checkAutomat = "SELECT automatID FROM automat WHERE order_id = ? AND slot != ''";
                $found = $this->db->query($checkAutomat, array($order->order_id))->row();
                if ($found) {
                    continue;
                }
            }

            $getItems = "SELECT name, displayName, itemID, orderItem.notes, barcode, width
                        FROM orderItem
                        JOIN item ON (itemID = orderItem.item_id)
                        JOIN product_process ON (product_processID = item.product_process_id)
                        JOIN product ON (productID = product_process.product_id)
                        LEFT JOIN barcode ON (barcode.item_id = itemID)
                        WHERE order_id = ?
                        AND product.name not like '%box%'
                        AND orderItem.notes not like '%box%'";

            $items = $this->db->query($getItems, array($order->order_id))->result();

            $ngarm = count($items);

            $labelinfo = vsprintf($label_format, array(
                'customer' => getPersonName($order),
                'phone' => $order->phone,
                'order' => $order->order_id,
                'route_id' => $order->route_id,
                'sortOrder' => $order->sortOrder,
            ));

            $ordItemCount = 1;
            if ($loadBags && $order->customer_id != 18789) {
                $ngarm += 1;

                $data[] = vsprintf($line_format, array(
                    'lot' => $order->order_id,
                    'ngarm' => $ngarm,
                    'garment' => $ordItemCount,
                    'garcode' => $order->bagNumber,
                    'gardesc' => 'bag',
                    'perc' => 100,
                    'hung' => 'S',
                    'status' => 'R',
                    'mag' => '',
                    'slot' => '',
                    'out' => '',
                    'customer' => $order->customer_id,
                    'rfid' => '',
                    'arm' => 0,
                    'labelinfo' => $labelinfo,
                )) . "\r\n";

                $ordItemCount++;
            }

            foreach ($items as $item) {
                $data[] = vsprintf($line_format, array(
                    'lot' => $order->order_id,
                    'ngarm' => $ngarm,
                    'garment' => $ordItemCount,
                    'garcode' => $item->barcode,
                    'gardesc' => $item->displayName,
                    'perc' => $item->width ? $item->width : 100,
                    'hung' => 'S',
                    'status' => 'R',
                    'mag' => '',
                    'slot' => '',
                    'out' => '',
                    'customer' => $order->customer_id,
                    'rfid' => '',
                    'arm' => 0,
                    'labelinfo' => $labelinfo,
                )) . "\r\n";

                $ordItemCount++;
            }
        }


        $data = implode('', $data);
        file_put_contents($this->path . "AUTOMAT.IN", $data);

        header("Content-type: text/plain");
        echo $data;
    }

    public function process_upload($file = null)
    {
        $files = scandir($this->path);
        foreach($files as $file) {
            if (is_file($this->path.$file)) {
                $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                if (in_array($extension, array('log', 'out'))) { //interested in log files only
                    echo "Processing file: {$this->path}{$file}<br>";
                    $this->process_file($file);
                }
            }
        }
    }

    protected function process_file($file)
    {
        $this->load->model('automat_model');

        $is_log = preg_match('/\.log$/i', $file);

        $lines = file($this->path.$file);
        $lineNumber = 0;
        foreach ($lines as $lineNumber => $line) {

            if ($is_log) {

                // only process lines from AUTOMAT.TMP
                $fileName = substr($line, 0, 21);
                If ($fileName != "C:\\MAPEXC\\AUTOMAT.TMP") {
                    continue;
                }

                // parse the date, format 31/12/2015 12:05:46
                $day = substr($line, 22, 2);
                $month = substr($line, 25, 2);
                $year = substr($line, 28, 4);
                $hour = substr($line, 33, 2);
                $min = substr($line, 36, 2);
                $sec = substr($line, 39, 2);

                // the rest of the line is the same as automat.out data
                $line = substr($line, 50);
            } else {
                $fileName = "C:\\MAPEXC\\$file";

                if (preg_match('/[^0-9]+([0-9]+)\.out/i', $file)) {
                    //Extract date from file name, i.e. aut_2015111212273233.out
                    $date = preg_replace('/.*aut_+([0-9]+)\.out/i', '$1', $file);
                } else {
                    // This is automat.out file
                    $date = convert_from_gmt_aprax('now', 'YmdHis', $this->business_id);
                }

                extract(unpack('A4year/A2month/A2day/A2hour/A2min/A2sec', $date));
            }

            $fileDate = "$year-$month-$day $hour:$min:$sec";
            try {
                $fileDate = convert_to_gmt_aprax($fileDate, $this->business->timezone);
            } catch (Exception $e) {
                $fileDate = gmdate('Y-m-d H:i:s');
            }

            $data = unpack('A20order_id/A2ngarm/A2garment/A20garcode/A30gardesc/A3perc/A1hung/A1stat/A2mag/A5slot/A5outPos/A20customer/A8rfid/A13discard/A2arm', $line);
            unset($data['discard']);

            If (empty($data['order_id'])) {
                continue;
            }

            If ($data['garment'] == "0") {
                continue;
            }

            $data['fileDate'] = $fileDate;
            $data['fileName'] = $fileName;
            $data['lineNumber'] = $lineNumber;

            $this->automat_model->save($data, true);

            echo "Order: ".$data['order_id'] . ' - SUCCESS<br>';
        }

        $backup = $this->path . '/bak/' . gmdate('Y-m-d') . '/';
        if (!is_dir($backup)) {
            mkdir($backup, 0777, true);
        }

        copy($this->path.$file, $backup.$file);
        if (unlink($this->path.$file)) {
            echo "Deleted File: ".$this->path.$file." At: ".date("c")."<br>";
        }
    }

}
