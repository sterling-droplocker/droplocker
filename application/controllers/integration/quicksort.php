<?php
/*
 * Test urls:
 * pos.txt: http://droplocker.loc/integration/quicksort/get_orders/?business_id=3&printer_key=arik
 * load.txt|update.txt|print.txt: http://droplocker.loc/integration/quicksort/process_upload/?business_id=3&printer_key=arik
 *
 */
require 'CI_Integration.php';
use App\Libraries\DroplockerObjects\Business;

class Quicksort extends CI_Integration
{

    public function empty_conveyor($barcode = null)
    {
        if ($barcode) {
            $sql = "SELECT DISTINCT order_id AS orderID
	           FROM orderItem
	           JOIN item ON itemID = orderItem.item_id
	           JOIN barcode ON barcode.item_id = itemID
	           WHERE barcode = ? AND barcode.business_id = ?";
            $params = array(
                $barcode,
                $this->business_id
            );
        } else {
            // tells the conveyor to offload all the orders for today
            // don't count any box shirts
            $sql = "SELECT orderID
                    FROM orders
                    WHERE orderStatusOption_id = 3
                    AND business_id = ?";
            $params = array(
                $this->business_id
            );
        }

        $orders = $this->db->query($sql, $params)->result();
        $data = array();
        foreach ($orders as $line) {
            $data[] = str_pad($line->orderID, 20) . str_pad("", 58) . 'P            ' . "\r\n";
        }

        $data = implode('', $data);
        file_put_contents($this->path . "AUTOMAT.IN", $data);

        header("Content-type: text/plain");
        echo $data;
    }

    public function get_orders($bagNumber = null)
    {
        if ($bagNumber) {
            // removed and product_id not in (34, 179, 180) replaced with and notes NOT LIKE '%box%'
            $sql = "SELECT DISTINCT order_id, orders.customer_id, bagNumber, firstName, lastName, phone, route_id, sortOrder
                    FROM orderItem
                    JOIN orders ON (orderItem.order_id = orderID)
                    JOIN bag ON (bagID = orders.bag_id)
                    JOIN customer ON (customerID = orders.customer_id)
                    LEFT JOIN locker ON (lockerID = orders.locker_id)
                    LEFT JOIN location ON (locationID = locker.location_id)
                    WHERE orderStatusOption_id != 10
                    AND item_id > 0
                    AND bag.bagNumber = ? AND bag.business_id = ?
                    ORDER BY orderItem.order_id";

            $params = array(
                $bagNumber,
                $this->business_id
            );
        } else {
            // get all orders in status inventoried or on payment hold
            $sql = "SELECT DISTINCT order_id, orders.customer_id, bagNumber, firstName, lastName, phone, route_id, sortOrder, statusDate
                    FROM orderItem
                    JOIN orders ON orderItem.order_id = orderID
                    LEFT JOIN bag on bagID = orders.bag_id
                    JOIN customer ON (customerID = orders.customer_id)
                    LEFT JOIN locker ON (lockerID = orders.locker_id)
                    LEFT JOIN location ON (locationID = locker.location_id)
                    WHERE orderStatusOption_id in (3,5,7)
                    AND orders.business_id = ?
                    AND item_id > 0
                    ORDER BY orderItem.order_id";

            $params = array(
                $this->business_id
            );
        }

        $orders = $this->db->query($sql, $params)->result();
        $loadBags = get_business_meta($this->business_id, 'loadBags');

        $file_contents = array();
        foreach ($orders as $order) {

            if (! $bagNumber) {
                $checkAutomat = "SELECT automatID FROM automat WHERE order_id = ? AND slot != ''";
                $found = $this->db->query($checkAutomat, array(
                    $order->order_id
                ))->row();
                if ($found) {
                    continue;
                }
            }

            $getItems = "SELECT name, displayName, itemID, orderItem.notes, barcode, width
                        FROM orderItem
                        JOIN item ON (itemID = orderItem.item_id)
                        JOIN product_process ON (product_processID = item.product_process_id)
                        JOIN product ON (productID = product_process.product_id)
                        LEFT JOIN barcode ON (barcode.item_id = itemID)
                        WHERE order_id = ?
                        AND product.name not like '%box%'
                        AND orderItem.notes not like '%box%'";

            $items = $this->db->query($getItems, array(
                $order->order_id
            ))->result();
            $ngarm = count($items);

            if ($loadBags) {
                $data = array(
                    "ac_id" => "01",
                    "transaction_code" => "1", // New invoice
                    "order_id" => $order->order_id,
                    "store_id" => "1",
                    "route_number" => $order->route_id,
                    "customer_first_name" => $order->firstName,
                    "customer_last_name" => $order->lastName,
                    "customer_number" => $order->customer_id,
                    "order_due_date" => date("m/d/Y g:i:s A", strtotime($order->statusDate)),
                    "order_mark_in_date" => date("m/d/Y g:i:s A", strtotime($order->statusDate)),
                    "transaction_date" => date("m/d/Y g:i:s A", strtotime($order->statusDate)),
                    "order_total_pieces" => $ngarm,
                    "item_barcode" => $order->bagNumber,
                    "item_description" => "bag",
                    "notes" => ""
                );

                $file_contents[] = '"' . implode('","', $data) . '"';
            }

            foreach ($items as $item) {
                $data = array(
                    "ac_id" => "01",
                    "transaction_code" => "1", // New invoice
                    "order_id" => $order->order_id,
                    "store_id" => "1",
                    "route_number" => $order->route_id,
                    "customer_first_name" => $order->firstName,
                    "customer_last_name" => $order->lastName,
                    "customer_number" => $order->customer_id,
                    "order_due_date" => date("m/d/Y g:i:s A", strtotime($order->statusDate)),
                    "order_mark_in_date" => date("m/d/Y g:i:s A", strtotime($order->statusDate)),
                    "transaction_date" => date("m/d/Y g:i:s A", strtotime($order->statusDate)),
                    "order_total_pieces" => $ngarm,
                    "item_barcode" => $item->barcode,
                    "item_description" => $item->name,
                    "notes" => $item->notes
                );

                $file_contents[] = '"' . implode('","', $data) . '"';
            }
        }

        $file_contents = implode("\r\n", $file_contents);

        header("Content-type: text/plain");
        file_put_contents($this->path . "pos.txt", $file_contents);
        echo $file_contents;
    }

    public function process_upload($file = null)
    {
        $files = scandir($this->path);
        foreach ($files as $file) {
            if (is_file($this->path . $file)) {
                if (preg_match('/^(load|update|print).*\.txt/i', $file)) {
                    echo "Processing file: {$this->path}{$file}<br>";
                    $this->process_file($file);
                }
            }
        }
    }

    protected function process_file($file)
    {
        $this->load->model('automat_model');
        if (preg_match('/^load.*\.txt/i', $file)) {
            // This happens when a garment is loaded onto the conveyor
            $this->process_load_file($file);
        } elseif (preg_match('/^update.*\.txt/i', $file)) {
            // We are ignoring the update.txt file since it provides the same information as the print.txt file
        } elseif (preg_match('/^print.*\.txt/i', $file)) {
            // This happens when full order is offloaded from the conveyor
            $this->process_print_file($file);
        } else {
            die("Unknown file: $file");
        }

        $backup = $this->path . '/bak/' . gmdate('Y-m-d') . '/';
        if (! is_dir($backup)) {
            mkdir($backup, 0777, true);
        }

        copy($this->path . $file, $backup . $file);
        if (unlink($this->path . $file)) {
            echo "Deleted File: " . $this->path . $file . " At: " . date("c") . "<br>";
        }
    }

    /**
     * Parses the load.txt file
     *
     * @param string $file
     */
    protected function process_load_file($file)
    {
        $lines = file($this->path . $file);
        $lineNumber = 0;
        $garment = 0;
        $last_order_id = 0;

        $item_counts = array();
        $this->load->model("order_model");
        foreach ($lines as $lineNumber => $line) {
            /*
             * $line = array(
             * 01 = ID No. of AC is 1
             * 1234 = Ticket Number
             * 60056982 = Barcode number of garment
             * 21 = Slot number that garment was placed on
             * 09/25/2008 08:12:00 AM = Time and date garment was loaded onto the conveyor
             * Blank = There is no operator ID associated with the POS system
             * 01 = Store ID Number
             * 00234 = POS system customer number
             * );
             */

            $line = str_getcsv($line);

            $order_id = $line[1];

            // No order id
            if (empty($order_id)) {
                continue;
            }

            // here we count the items in the order
            if ($last_order_id != $order_id) {
                $garment = 0;
                $last_order_id = $order_id;
            }

            if (! isset($item_counts[$order_id])) {
                // count items in the order with *barcode*
                $item_counts[$order_id] = $this->order_model->count_order_items($order_id);
            }

            $data = array(
                'order_id' => $order_id,
                'ngarm' => $item_counts[$order_id], // total items in order: 10, 10, 10, 10, ...
                'garment' => ++ $garment, // item number: 1, 2, 3, 4, ....
                'garcode' => $line[2],
                'gardesc' => '',
                'perc' => "",
                'hung' => "S",
                'stat' => "U",
                'mag' => '',
                'slot' => $line[3],
                'outPos' => sprintf('#%03d#', 1), // #001#, #002#, #003#, ... #$garment#
                'customer' => ltrim($line[7], 0),
                'rfid' => '',
                'arm' => '01',
                'fileName' => $this->path . $file,
                'fileDate' => convert_to_gmt_aprax($line[4], $this->business->timezone),
                'lineNumber' => $lineNumber,
                'printed' => 0
            );
            $this->automat_model->save($data, true);

            echo "Order: " . $data['order_id'] . ' - SUCCESS<br>';
        }
    }

    /**
     * Parses the print.txt file
     *
     * @param string $file
     */
    protected function process_print_file($file)
    {
        $lines = file($this->path . $file);
        $lineNumber = 0;
        $garment = 0;
        $last_order_id = 0;

        $item_counts = array();
        $this->load->model("order_model");

        //search items for this order in orderItem
        //search items for this order in automat
        //get only those items that are in OrderItem and NOT in automat
        //insert those items (really one) in automat.. DONE
        foreach ($lines as $lineNumber => $line) {
            /*
             * 01 = Id number of AC is 1
             * 01 = Store ID number
             * 1234 = Print ticket number 1234
             * 09/25/2008 08:12:00 AM = Transaction date and time
             * 00234 = Customer ID No.
             * 0 = Order Completed (only used by some POS systems)
             */

            $line = str_getcsv($line);

            $order_id = $line[2];

            // No order id
            if (empty($order_id)) {
                continue;
            }

            // we will pull the slot from the automat entry
            $sql = "SELECT slot, customer FROM automat WHERE order_id = ? AND stat = 'U' AND slot > 0 ORDER BY automatID DESC LIMIT 1";
            $result = $this->db->query($sql, array($order_id))->result();

            $slot = 0;
            $customer = '';

            if ($result) {
                $slot = $result[0]->slot;
                $customer = $result[0]->customer;
            }

            //get all items
            $sql = "SELECT name, displayName, itemID, orderItem.notes, barcode, width
                        FROM orderItem
                        JOIN item ON (itemID = orderItem.item_id)
                        JOIN product_process ON (product_processID = item.product_process_id)
                        JOIN product ON (productID = product_process.product_id)
                        LEFT JOIN barcode ON (barcode.item_id = itemID)
                        WHERE order_id = ?";

            $items = $this->db->query($sql, array($order_id))->result();

            $count = count($items);
            foreach ($items as $i => $item) {

                $garment = $i + 1;
                $data = array(
                    'order_id'   => $order_id,
                    'ngarm'      => $count, // total items in order: 10, 10, 10, 10, ...
                    'garment'    => $garment, // item number: 1, 2, 3, 4, ....
                    'garcode'    => $item->barcode,
                    'gardesc'    => '',
                    'perc'       => "",
                    'hung'       => "S",
                    'stat'       => "L",
                    'mag'        => '',
                    'slot'       => $slot,
                    'outPos'     => sprintf('#%03d#', 1), // #001#, #002#, #003#, ... #$garment#
                    'customer'   => $item->customer,
                    'rfid'       => '',
                    'arm'        => '01',
                    'fileName'   => $this->path . $file,
                    'fileDate'   => convert_to_gmt_aprax($line[3 ], $this->business->timezone),
                    'lineNumber' => $i + 1,
                    'printed'    => 0,
                );

                $this->automat_model->save($data, true);
            }

            echo "Order: " . $order_id . ' - SUCCESS<br/>';
        }
    }

    /**
     * Uploads files into integration
     */
    public function upload_file()
    {
        if (empty($_FILES)) {
            die("Missing file");
        }

        foreach ($_FILES as $key => $file) {
            if ($file['error'] != UPLOAD_ERR_OK) {
                die("Error uploading file: Error #" . $file['error']);
            }

            $business = new Business($this->business_id);
            $path = $this->path . basename(strtolower($file['name']), '.txt') . '_' . date('YmdHis') . '.txt';

            move_uploaded_file($file['tmp_name'], $path);
        }
    }
}
