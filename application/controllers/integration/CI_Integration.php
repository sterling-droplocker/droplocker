<?php

use App\Libraries\DroplockerObjects\Business;

abstract class CI_Integration extends CI_Controller
{
    /**
     *
     * @var integer
     */
    public $business_id;

    /**
     *
     * @var Business
     */
    public $business;

    /**
     * Path where the files are to be stored for this business
     * @var string
     */
    public $path;

    public function __construct()
    {
        parent::__construct();

        //authorization
        $printer_key = $this->input->get('printer_key');
        $this->business_id = $this->input->get('business_id');

        if (empty($this->business_id)) {
            die("Missing business_id");
        }

        if (empty($printer_key)) {
            die("Missing printer_key");
        }

        $key = get_business_meta($this->business_id , 'printer_key');
        if ($printer_key != $key) {
            die("The printer key in the Settings of this application does not match the printer key in your DropLocker admin panel");
        }

        $this->business = new Business($this->business_id);

        $this->path = "/home/droplocker/integration/{$this->business_id}/";
        if (DEV) {
            $this->path = "/tmp/integration/";
        }

        if (!is_dir($this->path)) {
            mkdir($this->path, 0777, true);
        }

    }

    /**
     * Generates the file for the orders
     *
     * @param string $bagNumber
     */
    abstract public function get_orders($bagNumber = null);

    /**
     * Retrieves a file from the server
     *
     * @param string $file
     */
    public function download($file = null)
    {
        $filename = $this->path . basename($file);

        if (!file_exists($filename)) {
            die("File $file not found");
        }

        header('Content-Type: text/plain');
        $fh = fopen($filename, "rb");
        fpassthru($fh);
    }

    /**
     * Uploads files into integration
     */
    public function upload_file()
    {
        if (empty($_FILES)) {
            die("Missing file");
        }

        foreach ($_FILES as $key => $file) {
            if ($file['error'] != UPLOAD_ERR_OK) {
                die("Error uploading file: Error #" . $file['error']);
            }

            $business = new Business($this->business_id);
            $path = $this->path . basename($file['name']);

            move_uploaded_file($file['tmp_name'], $path);
        }
    }

    /**
     * Process files uploaded with ::upload_file()
     * @param string $file
     */
    abstract public function process_upload($file = null);

}