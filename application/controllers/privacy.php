<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\LockerLootConfig;

class Privacy extends MY_Controller
{

    public $business;
    public $business_id;
    protected $data;

    public function __construct()
    {
        parent::__construct();
        $this->data = array();
        $this->data['business_id'] = $this->business_id;
    }

    public function index()
    {
        $url = current_url();
        if (strpos($url, 'laundrylocker.') !== false) {
            $this->data["company"] = "Laundry Locker";
        } else{
            $this->data["company"] = "Drop Locker";
        }
        
        $this->template->write('title', "Privacy Policy - " . $this->data["company"], true);
        $this->template->write_view('content', "privacy", $this->data, true);

        $this->template->render();
    }
}
