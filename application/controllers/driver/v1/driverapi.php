<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\DroidLoadVan;
use App\Libraries\DroplockerObjects\Droid_PostDeliveries;
use App\Libraries\DroplockerObjects\Droid_PostPickup;
use DropLocker\Util\Lock;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\App\Libraries\DroplockerObjects;
use App\Libraries\DroplockerObjects\Location;

//Old class Mobile_2_6 extends CI_Controller
class DriverApi extends MY_Driver_Api_Controller
{
    protected $get;
    public $business_id;
    public $employee_id;
    private $companyName;
    private $deliveryTime;

    const VERSION = '3.0';
    private $version = 3.0;

    function __construct(){
        parent::__construct();

        $this->load->model('driver_log_model');
        $this->load->model('order_model');
        $this->load->model('customerhistory_model');
        $this->load->model('customer_model');

        $this->get = $this->input->get();

        $this->username             = '';
        $this->password             = '';
        $this->business_id          = '';

        if ( isset($this->get['BUSINESSID']) ) {
            $this->business_id = $this->get['BUSINESSID'];
        }

        if ( isset($this->get['DELMANID']) ) {
            $this->username = $this->get['DELMANID'];
        }

        if ( isset( $this->get['PASSWORD'])) {
            $this->password = $this->get['PASSWORD'];
        }

        if (isset($this->get['EMPLOYEEID'])) {
            $this->employee_id          = $this->get['EMPLOYEEID'];
        }

        if (isset($this->get['CLAIMID'])) {
            $this->claim_id             = $this->get['CLAIMID'];
        }

        if (isset($this->get['ROUTES'])) {
           $routes = urldecode($this->get['ROUTES']);
           $routes = trim($routes);
           $this->routes = rtrim($routes, ",");
        }

        if (isset($this->get['BAGNUMBER'])) {
        $this->fullBagNumber        = $this->get['BAGNUMBER'];
        $this->bagNumber            = $this->get['BAGNUMBER'];
        }
        if (isset($this->get['LOCATIONID'])) {
            $this->location_id          = ltrim($this->get['LOCATIONID'], "0"); // trim off the leading 0 if found
        }
        if (isset($this->get['LOCKERNAME'])) {
            $this->lockerName           = ltrim(trim(urldecode($this->get['LOCKERNAME'])), "0"); // Trim if there is a leading 0 on the locker name
        }
        if (isset($this->get['NOTES'])) {
            $this->notes                = $this->input->get("NOTES");
        }
        if (isset($this->get['ORDERID'])) {
            $this->order_id             = $this->get['ORDERID'];
        }
        if (isset($this->get['WASHFOLD'])) {
            $this->washFold             = $this->get['WASHFOLD'];
        }
        if (isset($this->get['PARTIAL'])) {
            $this->partial              = $this->get['PARTIAL'];
        }
        if (isset($this->get['INOUT'])) {
            $this->inout                = $this->get['INOUT'];
        }
        if (isset($this->get['CLOCKTIME'])) {
            $this->clockTime            = urldecode($this->get['CLOCKTIME']);
        } else if (isset($this->get['request_time'])) {
            $this->clockTime            = urldecode($this->get['request_time']);
        }
        
        if (isset($this->get['NOTEID'])) {
            $this->note_id              = $this->get['NOTEID'];
        }
        if (isset($this->get['JOB'])) {
            $this->job                  = $this->get['JOB'];
        }
        if (isset($this->get['TIMECARD'])) {
            $this->timecard_id          = $this->get['TIMECARDID'];
        }
        if (isset($this->get['ISSUEID'])) {
            $this->issue_id             = $this->get['ISSUEID'];
        }

        if (isset($this->get['ERROR'])) {
            $this->errorMessage = $this->get['ERROR'];
        }

        if (isset($this->get['NOTETYPE'])) {
            $this->noteType             = $this->get['NOTETYPE'];
        }
        if (isset($this->get['signature'])) {
            $this->transaction_id       = $this->get['signature'];
        }
        if (isset($this->get['NOTE'])) {
            $this->driverNote           = urldecode($this->get['NOTE']);
        }
        if (isset($this->get['CUSTOMER'])) {
            $this->customerName         = urldecode($this->get['CUSTOMER']);
        }
        if (isset($this->get['PHONEVERSION'])) {
            $this->phoneVersion         = urldecode($this->get['PHONEVERSION']);
        } else {
            $this->phoneVersion = 0;
        }
        if (isset($this->get['PHONENUMBER'])) {
            $this->phoneNumber         = urldecode($this->get['PHONENUMBER']);
        } else {
            $this->phoneNumber = 0;
        }

        $this->location_id = 0;
        if (isset($this->get['LOCATIONID'])) {
            if (strlen($this->get['LOCATIONID']>1)) {
                $this->location_id = ltrim($this->get['LOCATIONID'], "0"); // trim off the leading 0 if found
            } else {
                $this->location_id = $this->get['LOCATIONID'];
            }
        }

        $this->lockerName = '';
        if (isset($this->get['LOCKERNAME'])) {
            if (strlen($this->get['LOCKERNAME']>1)) {
                $this->lockerName = ltrim($this->get['LOCKERNAME'], "0"); // trim off the leading 0 if found
            } else {
                $this->lockerName = $this->get['LOCKERNAME'];
            }
        }

        //if (!empty($this->business_id)) {
        //    $this->deliveryTime         = $this->formatDate(urldecode($this->get['DELTIME']), $this->business_id);
        //} else {
            if ( isset( $this->get['request_time'] ) ) {
                $this->deliveryTime = urldecode( $this->get['request_time'] );
            }
        //}

        if (!$this->input->is_cli_request()) {
           if (!$this->auth()) {
               //echo json_encode(array('status'=>'FAIL', 'message'=>"Username or password is incorrect"));
               //die();
           }
        }
    }

    public function validate()
    {
        if ($this->auth()) {
            $response['business_id'] = $this->business_id;
            $response['employee_id'] = $this->employee_id;
            $response['companyName'] = $this->companyName;
            echo json_encode(array('status'=>"SUCCESS", 'response'=>$response));
        } else {
            echo json_encode(array('status'=>"FAIL", 'message'=>"Username or password is incorrect"));
        }
    }

    /**
     * Retrieves all 'complete' businesses in the system.
     */
    public function getBusineses()
    {
        $this->load->model("business_model");
        output_ajax_success_response($this->business_model->findAllCompleteBusinesses());
    }
    /**
     * Retrieves the transaction for the specified business
     * Expects the GET parameter business_id
     */
    public function getTranslations()
    {
        $business_id = $this->input->get("business_id");
        $business = $this->business_model->get_by_primary_key($business_id);
        $tag = $this->input->get("tag");
        
        if (!($business && $tag)) {
            output_ajax_error_response('Valid business id and language tag needs to be provided');
        };
        
        $this->load->model("language_model");
        $language = $this->language_model->findByTag($tag);
        if (empty($language)) {
            output_ajax_error_response("Language tag '$tag' does not exist");
        }
        
        $this->load->model("languagekey_business_language_model");
        $translations = $this->languagekey_business_language_model->getTranslationsForLanguageViewNameForLanguageNameForBusiness($language->languageID, $business->businessID, array(
            "driver_app/global",
            "driver_app/menu",
            "driver_app/bag",
            "driver_app/bag_sorting",
            "driver_app/building_access_code",
            "driver_app/clock_in",
            "driver_app/clock_out",
            "driver_app/customer_locker_code",
            "driver_app/delivery",
            "driver_app/delivery_confirm",
            "driver_app/driver_app_updates",
            "driver_app/edit_routes",
            "driver_app/load_van",
            "driver_app/login",
            "driver_app/manifest",
            "driver_app/map",
            "driver_app/nfc_programming",
            "driver_app/pickup",
            "driver_app/pickup_reminder",
            "driver_app/process_wash_fold",
            "driver_app/program_bags",
            "driver_app/program_lockers",
            "driver_app/read_nfc",
            "driver_app/report_issue",
            "driver_app/scan_barcode",
            "driver_app/signup",
            "driver_app/stop",
            "driver_app/stop_detail",
            "driver_app/stored_items",
            "driver_app/timecard",
            "driver_app/menu",
        ));
        output_ajax_success_response($translations, "'$business->companyName' (#$business_id) translations for '$language->tag - $language->description'");
    }
    /**
     * checks to see if the phone has the most up to date version.
     * If the phone is out of date, an alert will be shown and
     * a link provided to get the newest apk
     *
     * the version is a config setting
     */
    public function checkVersion()
    {
        //$query = $this->db->get('droidVersion');
        //$result = $query->row();
        echo json_encode(array('version'=>$this->config->item('version'),'link'=>$this->config->item('link') ));
    }

    /**
    * saveSettings method is used to validate a driver
    *
    * @param string $username
    * @param string $password (md5 string will be passed)
    *
    * echo JSON response for result
    */
    public function saveSettings()
    {
        //authorize the driver
        if ($this->auth()) {
           $response['business_id'] = $this->business_id;
           $response['employee_id'] = $this->employee_id;
           $response['companyName'] = $this->companyName;
           echo json_encode(array('status'=>"SUCCESS", 'response'=>$response));
        } else {
           echo json_encode(array('status'=>"FAIL", 'message'=>"Username or password is incorrect"));
        }
    }

    /**
     * Auth is used to verify an employee logging into the application
     * from a phone
     *
     * Set user values on the phone
     * -----------
     * business_id
     * employee_id
     * companyName
     *
     * @return boolean - sets the employee_id and business_id
     */
    public function auth()
    {
        $sql = "SELECT employeeID, business_id, companyName
                    FROM employee
                    INNER JOIN business_employee be ON be.employee_id = employeeID
                    INNER JOIN business b ON b.businessID = be.business_id
                    AND `default` = 1
                    WHERE username = ?
                    AND password = ?
                    AND empStatus = ?";

        $query = $this->db->query($sql, array($this->username, $this->password, 1));
        if ($result = $query->row()) {
            $this->employee_id = $result->employeeID;
            $this->business_id = $result->business_id;
            $this->companyName = $result->companyName;

            return true;
        }

        return false;
    }

    /**
     * inserts data sent from the phones into the droid_postPickups table
     * creates new pending pickup
     * 
     * @deferred
     */
    public function postPickups()
    {
        $msg2 = '';
        
        $employee = $this->getEmployee();
        
        $this->load->model('droidpostpickups_model');
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;
        $options['employeeUsername'] = $employee->username;

        $options['transaction_id'] = $this->transaction_id;
        $options['deliveryTime'] = $this->formatDate($this->deliveryTime, $this->business_id);
        
        $options['bagNumber'] = $this->bagNumber;
        $options['location_id'] = $this->location_id;
        $options['lockerName'] = $this->lockerName;
        $options['washFold'] = $this->washFold;
        $options['notes'] = ($this->washFold ? "1 " : "") . (isset($this->notes) ? $this->notes : null);
        $options['claim_id'] = $this->claim_id;
        
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        
        try {
            if (!$options['lockerName'] && $options['claim_id']){
                $msg2 .= "Missing locker name. ";
                $this->load->model('location_model');
                $this->load->model('claim_model');
                if($this->location_model->isConcierge($options['location_id'])) {
                    $options['lockerName'] = $this->claim_model->getLockerName($options['claim_id']);
                    $msg2 .= "Using '{$options['lockerName']}' from claim #{$options['claim_id']}.";
                } else {
                    $msg2 .= "Location #{$options['location_id']} is not concierge. Skipping.";
                }
            }
        } catch (Exception $exc) {
            //could not find locker, continue;
        }

        try {
            if ($insert_id = $this->droidpostpickups_model->insert($options)) {
                $msg = "Post pickup transaction inserted correctly. $msg2";
                $data = array('transaction_id'=>$options['transaction_id'], 'insert_id'=>$insert_id);
                output_ajax_success_response($data, $msg);
            } else {
                $msg = 'Failed to insert postPickup, transaction_id: ' . $options['transaction_id'];
                output_ajax_error_response($msg);
            }
        } catch (Database_Exception $database_exception) {
            if ($database_exception->get_error_number() == 1062) {
                $msg = 'post pickup transaction already exists';
                $data = array('transaction_id'=>$options['transaction_id']);
                output_ajax_success_response($data, $msg);
            }
        };
    }


    /**
     * inserts data sent from the phones into the database
     */
    public function postPickupReminders()
    {
        $employee = $this->getEmployee();
        
        $business = new Business($this->business_id);

       //look for unpaid orders
       $sql = "SELECT orderID, bag.customer_id, bag_id, orderPaymentStatusOption_id, locker.location_id FROM orders
               INNER JOIN bag ON bag.bagID = orders.bag_id
               INNER JOIN locker ON locker.lockerID = orders.locker_id
               WHERE bag.bagNumber = {$this->bagNumber} AND bag.business_id = {$this->business_id}  ORDER BY orders.orderID DESC limit 1";
        $lastOrder = $this->db->query($sql)->row();
        $orderIsUnpaid = false;

        if (is_object($lastOrder)) {
            if( $lastOrder->orderPaymentStatusOption_id == '1' ||$lastOrder->orderPaymentStatusOption_id == '4') {
                $orderIsUnpaid = true;
            }

            $customer = new Customer($lastOrder->customer_id);
            if (!empty($customer->invoice)) {
                if ($customer->invoice == 1) {
                    $orderIsUnpaid = false;
                }
            }
        }
        
        if (!$this->location_id) {
            if(is_object($lastOrder) && $lastOrder->location_id) {
                $this->location_id = $lastOrder->location_id;
            }
        }
        $id = '';

        if($orderIsUnpaid) {
                $emailActionID = 7;
                $business_language_id = $business->default_business_language_id;
                $this->load->model("emailactions_model");
                $this->load->model("emailtemplate_model");
                $emailAction = $this->emailactions_model->get_by_primary_key($emailActionID);
                $emailTemplate = $this->emailtemplate_model->get_one(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id, "business_language_id" => $business_language_id));
                if (empty($emailTemplate)) {
                    if (empty($emailAction)) {
                        output_ajax_error_response("Could not find email action ID $emailActionID");
                    } else {
                        output_ajax_error_response("Could not find email template for email action '{$emailAction->name}'");
                    }
                } else {
                    $action_array = array(
                        'do_action' => 'payment_hold',
                        'customer_id' => $lastOrder->customer_id,
                        'business_id' => $this->business_id,
                        'order_id' => $lastOrder->orderID
                    );
                    Email_Actions::send_transaction_emails($action_array);
                    output_ajax_error_response("Unpaid order {$lastOrder->orderID} for bag {$this->bagNumber}, notification email sent");
                }
        } else {
            $sql = "INSERT IGNORE INTO droid_postPickupReminders (transaction_id, bagNumber, location_id, lockerName, employeeUsername, deliveryTime, business_id, employee_id, version, phoneVersion, phoneNumber)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $values = array(
                $this->transaction_id, 
                $this->bagNumber,
                $this->location_id,
                $this->lockerName,
                $employee->username,
                $this->deliveryTime,
                $this->business_id,
                $this->employee_id,
                self::VERSION,
                $this->phoneVersion,
                $this->phoneNumber);
            $this->db->query($sql,$values);
            $id = $this->db->insert_id();

            if (!$id) {
                output_ajax_error_response('duplicate post pickup reminder error for bag ['.$this->bagNumber.']');
            }
        }
        
        output_ajax_success_response(array('transaction_id'=>$this->transaction_id, 'id' => $id), "postPickupReminders added for bag {$this->bagNumber}");
    }


    /**
     * errors from the phones are stored on the server and then emailed
     * to development
     */
    public function postErrors()
    {

        $this->droidError_model->save(array(
            'transaction_id' => $this->transaction_id,
            'username'       => $this->username,
            'business_id'    => $this->business_id,
            'errorMessage'   => urldecode($this->errorMessage),
            'errorDate'      => gmdate('Y-m-d H:i:s'),
            'employee_id'    => $this->employee_id,
            'version'        => self::VERSION,
        ), true);

        echo json_encode(array(
            'transaction_id' => $this->transaction_id,
            'status'         => 'SUCCESS',
        ));
    }

    /**
     * gets all the lockers that are in locations on the driver route
     *
     * Used for validation when confirming a delivery of an order
     */
    public function getLockersInRouteLocations()
    {
        $sql = "SELECT lockerName, lockerID, location_id FROM locker
                INNER JOIN location ON locationID = location_id
                WHERE ( lockerStatus_id = 1 ) AND route_id in ({$this->routes}) and business_id = ?";
        $lockers = $this->db->query($sql, array($this->business_id))->result();

        echo json_encode($lockers);
    }


    /**
     * inserts data sent from the phones into the database
     */
    public function postReturnToOffice()
    {
        $employee = $this->getEmployee();
                
        if (!$this->location_id) {
            $sql = "SELECT orderID, bag.customer_id, bag_id, orderPaymentStatusOption_id, locker.location_id FROM orders
                   INNER JOIN bag ON bag.bagID = orders.bag_id
                   INNER JOIN locker ON locker.lockerID = orders.locker_id
                   WHERE bag.bagNumber = {$this->bagNumber} AND bag.business_id = {$this->business_id}  ORDER BY orders.orderID DESC limit 1";
            $lastOrder = $this->db->query($sql)->row();
            
            if(is_object($lastOrder) && $lastOrder->location_id) {
                $this->location_id = $lastOrder->location_id;
            }
        }
        
        $sql = "INSERT IGNORE INTO droid_postReturnToOffice (transaction_id, bagNumber, location_id, lockerName, employeeUsername, deliveryTime, business_id, employee_id, version, phoneVersion, phoneNumber)
        VALUES ('"
            .$this->transaction_id."', '"
            .$this->bagNumber."','"
            .$this->location_id."','"
            .$this->lockerName."','"
            .$employee->username."','"
            .$this->deliveryTime."', '"
            .$this->business_id."','"
            .$this->employee_id."', '"
            .self::VERSION."', '"
            .$this->phoneVersion."', '"
            .$this->phoneNumber."')";
        $this->db->query($sql);


        $id = $this->db->insert_id();
        
        if ($id) {
            output_ajax_success_response(array('transaction_id'=>$this->transaction_id, 'id' => $id), "postReturnToOffice added for bag {$this->bagNumber}");

        } else {
            output_ajax_error_response("Failed to insert postReturnToOffice. 'transaction_id'=>{$this->transaction_id}");
        }
    }


    /**
     * method for handling scan bag post pickups
     */
    public function postScanBag()
    {
        try {

            if (empty($this->bagNumber)) {
                throw new Exception("Missing bag number");
            }

            if (empty($this->business_id)) {
                throw new Exception("Missing business_id");
            }

            $sql = "select max(orderID) as orderID from orders
            join bag on bagID= orders.bag_id
            where bagNumber = '$this->bagNumber'
            AND orders.business_id = {$this->business_id}";

            $query = $this->db->query($sql);
            $order = $query->result();

            if (empty($order[0]->orderID)) {
                echo json_encode(array('status'=>'FAIL', 'transaction_id'=>$this->transaction_id, 'message'=>'Order not found'));
                return;
            }

            echo json_encode(array('status'=>'SUCCESS', 'transaction_id'=>$this->transaction_id));

            //Log that this bag was scanned in the automat table
            $insert="insert into automat
            (order_id, garcode, gardesc, fileDate)
            values ({$order[0]->orderID}, $this->bagNumber, 'Scanned to unload van by handheld', now())";
            $query = $this->db->query($insert);

            $deliveryTime = $this->deliveryTime;
            $username = $this->username;
            $employee_id = $this->employee_id;

            $insert2 = "insert into orderStatus
            (order_id, orderStatusOption_id, date, customer_userName, employee_id)
            values ('$order_id', '15', '$deliveryTime', '$username', '$employee_id');";
            $query2 = $this->db->query($insert2);
        } catch (Exception $e) {
            send_exception_report($e);
        }
    }

    /**
     * fires the cron job and process all orders the route passed
     *
     * requires
     * ------------
     * $this->routes
     * $this->business_id
     *
     * echos json object
     * -------------
     * status (SUCCESS|FAIL)
     * message
     */
    public function processOrders()
    {
        $this->loadSession();

        /**
        if (!Lock::acquire(__METHOD__)) {
            $this->load->library("log");
            $this->log->write_log('error', "failed to acquire lock, EMPLOYEEID[$this->employee_id]:BUSINESS_ID[$this->business_id] {$_SERVER['REMOTE_ADDR']}");
            echo json_encode(array('status'=>'ERROR', 'message'=> "ERROR: failed to acquire lock"));
            return;
        }
        */
        $result = "STARTED CRON:EMPLOYEEID[$this->employee_id]:BUSINESS_ID[$this->business_id]:";
        $this->load->model('droidpostdeliveries_model');
        $deliveries = $this->droidpostdeliveries_model->get(array('status'=>'pending', 'process_status' => 'not_processed', 'employee_id' => $this->employee_id, 'business_id'=>$this->business_id, 'version' => $this->version));

        if (!$deliveries) {
            $result .= "NO ORDERS TO PROCESS";
        }


        foreach ($deliveries as $delivery) {
            $droid_postDelivery = new Droid_PostDeliveries($delivery->ID);
            if ($droid_postDelivery->process_status == "not_processed") { //Note, we verify that the devliery has still not been processed after retrieving the initial result set.
                $droid_postDelivery->process_status = "processing";
                $droid_postDelivery->save();
                $this->droidpostdeliveries_model->processDelivery($delivery);
                $result .= $delivery->order_id.",";
                $droid_postDelivery->process_status = "processed";
                $droid_postDelivery->save();
            } else {
                //send_admin_notification("Duplicate process delivery call from phone ({$droid_postDelivery->{Droid_PostDeliveries::$primary_key}})", print_r($droid_postDelivery,1));
            }
        }
        $message = $result;
        echo json_encode(array('status'=>'SUCCESS', 'message'=>$message));

        Lock::release(__METHOD__);
    }


    public function processPickups()
    {
        $this->loadSession();

        $result = "STARTED CRON:EMPLOYEEID[$this->employee_id]:BUSINESS_ID[$this->business_id]:";
        $this->load->model('droidpostpickups_model');

        $key = 'pickups_' . $this->employee_id  . '_' . date('YmdHi');
        if (Lock::acquire($key, true)) {
            $pickups = $this->droidpostpickups_model->get(array('status'=>'pending', 'employee_id'=>$this->employee_id, 'business_id'=>$this->business_id));

            if (!$pickups) {
                $result .= "NOPICKUPS TO PROCESS";
            }

            foreach ($pickups as $pickup) {
                $droid_postPickup = new Droid_PostPickup($pickup->ID);
                //The following conditional verifies that another process from the faulty Android App is not already processing or has already processed this pickup.
                if ($droid_postPickup->process_status == "not_processed") {
                    $droid_postPickup->process_status = "processing";
                    $droid_postPickup->save();
                    $this->droidpostpickups_model->processPickup($pickup);
                    $result .= $pickup->bagNumber.",";
                    $droid_postPickup->process_status = "processed";
                    $droid_postPickup->save();
                }
            }
        }

        $message = $result;

        echo json_encode(array('status'=>'SUCCESS', 'message'=>$message));
    }

    /**
     *  inserts data sent from the phones into the database
     * 
     * @deferred
     */
    public function postDeliveries()
    {
        $employee = $this->getEmployee();
        
        $this->load->model('droidpostdeliveries_model');
        $options['transaction_id'] = $this->transaction_id;
        $existing_postDelivery = $this->droidpostdeliveries_model->get($options);

        /**
         * The following conditional checks to see if there is an existing post delivery with the same transaction ID.
         * If such a post delivery exists, then we return a success response with a message saying that that post delivery already exists.
         * Otherwise, we attempt to create the new post delivery.
         */
        if ($existing_postDelivery) {
            $msg = 'transaction_id already inserted in database';
            $data = array('transaction_id'=>$options['transaction_id']);
            output_ajax_success_response($data, $msg);
        } else {
            $options = array();
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;
            $options['employeeUsername'] = $employee->username;
            
            $options['transaction_id'] = $this->transaction_id;
            $options['deliveryTime'] = $this->formatDate($this->deliveryTime, $this->business_id);
            
            $options['bagNumber'] = $this->bagNumber;
            $options['order_id'] = $this->order_id;
            $options['lockerName'] = $this->lockerName;
            
            $options['version'] = self::VERSION;
            $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
            $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
            
            try {
                if ($insert_id = $this->droidpostdeliveries_model->insert($options)) {
                    $msg = 'inserted into postDeliveries';
                    $data = array('transaction_id'=>$options['transaction_id'], 'insert_id'=>$insert_id);
                    output_ajax_success_response($data, $msg);
                } else {
                    $msg = 'Failed to insert postDeliveries, transaction_id: ' . $options['transaction_id'];
                    output_ajax_error_response($msg);
                }
            } catch (Database_Exception $database_exception) {
                if ($database_exception->get_error_number() == 1062) {
                    $msg = 'post delivery transaction already exists';
                    $data = array('transaction_id'=>$options['transaction_id']);
                    output_ajax_success_response($data, $msg);
                }
            };
        }

    }

    /**
     * driver has sent a note
     * 
     * @deferred
     * 
     * TODO: add test
     */
    public function postDriverNotes()
    {
        $employee = $this->getEmployee();
        
        $this->load->model('droidpostdrivernotes_model');
        $foundNotes = $this->droidpostdrivernotes_model->get(array('transaction_id'=>$this->transaction_id));
        if (!$foundNotes) {
            $options['noteType'] = 'driverNote';
            $options['employeeUsername'] = $employee->username;
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;
            
            $options['transaction_id'] = $this->transaction_id;            
            $options['deliveryTime'] = $this->deliveryTime;

            $options['notes'] = $this->driverNote;
            $options['note_id'] = $this->note_id;
            $options['order_id'] = isset($this->order_id) ? $this->order_id : null;
            $options['location_id'] = $this->location_id;

            $options['version'] = self::VERSION;
            $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
            $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
            $this->droidpostdrivernotes_model->insert($options);
            
            $msg = "Response logged for note {$this->note_id}";
        } else {
            $msg = "Response already logged for note {$this->note_id}. Nothing to do here";
        }
        
        output_ajax_success_response(array("transaction_id"=>$this->transaction_id), $msg);


    }

    /**
     * getClaimed method gets all the claimed orders
     */
    public function getClaimed()
    {
        $routes = explode(',', $this->routes);
        $routes = array_map('intval', $routes);
        $routes = implode(',', $routes);

        list($timeStart, $timeEnd) = $this->getTimeWindows();

        $sql = "SELECT
                    lockerName,
                    serviceType,
                    COALESCE(CONCAT(orderHomePickup.address1, ', ', orderHomePickup.address2), location.address) AS street_address,
                    COALESCE(orderHomePickup.sortOrder,location.sortOrder) AS route_sort_order,
                    COALESCE(route, route_id)                              AS route_number,

                    locationID                       AS location_id,
                    locationType.name                AS service_method,

                    claimID                          AS claim_id,
                    UNIX_TIMESTAMP(claim.updated)    AS updated,

                    concat(firstName, ' ', lastName) AS customer_name,
                    customer.address1                AS customer_address1,
                    customer.address2                AS customer_address2,
                    phone                            AS customer_phone_number,

                    orderHomePickup.service          AS pickup_service,
                    pickup_window_id,
                    orderHomePickup.windowStart,
                    orderHomePickup.windowEnd,

                    (
                        (orderHomePickup.windowStart >= ? AND orderHomePickup.windowEnd <= ?) OR
                        (orderHomePickupID IS NULL AND location.serviceType != ?)
                    ) AS valid_window


                FROM claim
                    JOIN customer             ON (customerID = claim.customer_id)
                    JOIN locker               ON (lockerID = claim.locker_id)
                    JOIN location             ON (locationID = locker.location_id)
                    JOIN locationType         ON (locationType_id = locationTypeID)
                    LEFT JOIN orderHomePickup ON (claimID = orderHomePickup.claim_id)
                    LEFT JOIN delivery_window ON (delivery_windowID = orderHomePickup.pickup_window_id)
                WHERE claim.business_id = ?
                    AND claim.active = 1
                    AND (orderHomePickup.service = 'droplocker' OR orderHomePickup.service IS NULL)

                HAVING route_number IN ($routes) AND valid_window > 0
                ORDER BY route_sort_order
            ";

        $query = $this->db->query($sql, array(
            $timeStart,
            $timeEnd,
            Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY,
            $this->business_id,
            $this->business_id,
        ));

        $claims = $query->result();

        foreach ($claims as $key => $claim) {
            $claims[$key]->street_number = preg_replace('/^([0-9]+) .*/', '$1', $claim->street_address);
            $claims[$key]->lockerName = str_replace("'", '&#39;', $claims[$key]->lockerName);

            if ($claim->pickup_window_id) {
                $start = convert_from_gmt_aprax($claim->windowStart, 'g:iA', $this->business_id);
                $end = convert_from_gmt_aprax($claim->windowEnd, 'g:iA', $this->business_id);

                $claims[$key]->lockerName = "($start - $end)";
            }

        }

        header("Content-type: application/json");
        echo json_encode($claims);
    }

    /**
     * gets locations
     */
    public function getLocations()
    {
        $sql = "SELECT locationID AS location_id, address,
                    accessCode AS access_code, lat AS latitude, lon AS longitude,
                    COALESCE(route, route_id) AS route_id, sortOrder, start, end, delivery_windowID
                FROM location
                LEFT JOIN delivery_zone ON (delivery_zone.location_id = locationID)
                LEFT JOIN delivery_window ON (delivery_window.delivery_zone_id = delivery_zoneID)

                WHERE location.business_id = ?
                ORDER BY address";

        $this->business = new Business($this->business_id);

        $query = $this->db->query($sql, array($this->business_id));
        $locations = $query->result();

        $formatted_leading_integer = array();
        foreach ($locations as $location) {
            if ( !is_numeric( substr( $location->address, 0, 1 ) ) ) {
                $location->address = '0' . $location->address;
            }

            if ($location->delivery_windowID) {
                $start = date('g:iA', strtotime($location->start));
                $end = date('g:iA', strtotime($location->end));

                $location->address = "0Home Delivery - $start-$end";
            }

            $formatted_leading_integer[$location->location_id] = $location;
        }

        header("Content-type: application/json");
        echo json_encode( $formatted_leading_integer );
    }


    public function getLocationReport()
    {
        $sql = "SELECT bagNumber, order_id, deliveryTime, lockerName, status, statusNotes
        FROM droid_postDeliveries
        WHERE DATE_ADD(NOW(), INTERVAL -1 HOUR) <= deliveryTime
        AND location_id = ".$this->location_id;
        $results = $this->db->query($sql)->result();
        foreach ($results as $result) {

            $sql = "SELECT phone FROM customer
                    INNER JOIN bag ON customerID = customer_id
                    WHERE bagNumber = {$result->bagNumber}";
            $customer = $this->db->query($sql)->row();
            $phone = $customer->phone;
            $result->code = substr($phone, sizeof($phone)-5,4);

            if ($result->statusNotes!='') {
                $notes = unserialize($result->statusNotes);
                $result->statusNotes = $notes;
            }

            $r[] = $result;
        }

        echo json_encode($r);

    }

    /**
     * Gets orders
     */
    public function getOrders()
    {
       $business = new Business($this->business_id);
       $CI = get_instance();
       $blankCustomer = $business->blank_customer_id;

       $routes = explode(',', $this->routes);
       $routes = array_map('intval', $routes);
       $routes = implode(',', $routes);

       list($timeStart, $timeEnd) = $this->getTimeWindows();

       $sql = "SELECT
                    orderID AS order_id,
                    loaded AS loaded_on_van,
                    delivered,
                    orderPaymentStatusOption.name AS payStatus,
                    orderStatusOption.name AS name,
                    orderStatusOption_id AS ordStat,
                    orders.dateCreated,
                    bagNumber AS bag_number,
                    COALESCE(CONCAT(orderHomeDelivery.address1, ', ', orderHomeDelivery.address2), location.address) AS address,
                    COALESCE(orderHomeDelivery.sortOrder,location.sortOrder) AS sort_order,
                    COALESCE(route, route_id)                                AS route_id,
                    location.accessCode,
                    locationID AS location_id,
                    location.serviceType,
                    lockerName AS locker_id,

                    firstName AS first_name,
                    lastName AS last_name,
                    email,
                    customer.phone AS phone,
                    customer.address1 AS address1,
                    customer.address2 AS address2,
                    delivery_window_id,

                    (
                        (orderHomeDelivery.windowStart >= ? AND orderHomeDelivery.windowEnd <= ?) OR
                        (orderHomeDeliveryID IS NULL AND location.serviceType != ?)
                    ) AS valid_window,

                    (SELECT COUNT( * )
                        FROM orderItem
                        INNER JOIN product_process ON product_processID = orderItem.product_process_id
                        INNER JOIN product ON productID = product_process.product_id
                        INNER JOIN alertItem ON alertItem.product_id = productID
                        WHERE order_id = orderID
                    ) AS specialItemTotal

                FROM orders
                LEFT JOIN customer ON (customerID = orders.customer_id)
                INNER JOIN locker ON (lockerID = orders.locker_id)
                INNER JOIN location on locationID = locker.location_id
                INNER JOIN bag on bagID = orders.bag_id
                INNER JOIN orderStatusOption on orderStatusOptionID = orders.orderStatusOption_id
                INNER JOIN orderPaymentStatusOption on orderPaymentStatusOptionID = orders.orderPaymentStatusOption_id

                LEFT JOIN orderHomeDelivery ON (orderID = orderHomeDelivery.order_id)
                LEFT JOIN delivery_window ON (delivery_windowID = orderHomeDelivery.delivery_window_id)

                WHERE orderStatusOption_id NOT IN (1,2,10,13)
                AND orders.customer_id != ?
                AND delivered = 0
                AND location.business_id = ?
                AND orders.customer_id != 0
                GROUP BY orderID
                HAVING route_id IN ($routes) AND valid_window > 0";

        $orders = $this->db->query($sql, array(
            $timeStart,
            $timeEnd,
            Location::SERVICE_TYPE_TIME_WINDOW_HOME_DELIVERY,
            $blankCustomer,
            $this->business_id,
        ))->result();

        $this->load->model('order_model');

        foreach ($orders as $key => $order) {
            $type = $this->order_model->getOrderType($order->order_id);
            $orders[$key]->order_type = $type;
            $orders[$key]->locker_id = str_replace("'", '&#39;', $order->locker_id);

            if ($order->delivery_window_id) {
                $full_name = $order->first_name . ' ' . $order->last_name;
                $address = $order->address1 . $order->address2;

                $orders[$key]->first_name = $full_name . '<br>';
                $orders[$key]->last_name = $address;
            }

        }

        header("Content-type: application/json");
        echo json_encode($orders);
    }

    /**
     * Gets orders for wash and fold
     */
    public function getWashFold()
    {
        $sql = "SELECT orderID as order_id,
        postAssembly,
        orders.dateCreated,
        bagNumber as bag_number,
        location.address as street,
        concat(firstName,' ',lastName) as customer_name,
        location.route_id,
        position
        FROM orders
        LEFT JOIN assembly ON assembly.order_id = orderID
        JOIN customer on customerID = orders.customer_id
        JOIN locker on lockerID = orders.locker_id
        JOIN location on locationID = locker.location_id
        JOIN bag on bagID = orders.bag_id
        WHERE orders.orderStatusOption_id NOT IN (1, 10)
        AND orders.business_id = {$this->business_id}";



        $orders = $this->db->query($sql)->result();
        foreach ($orders as $order) {
            if ($order->postAssembly =='') {
                $order->postAssembly = 'null';
            }
            $out[] = $order;
        }
        echo json_encode($out);
    }

    /**
     * gets the phone number of the bag owner so the driver can close
     * the locker and enter the proper code
     */
    public function getLockerCode()
    {
        $sql = "SELECT phone, bagID from customer
        JOIN bag on bag.customer_id = customerID
        WHERE bagNumber = '".ltrim(trim($this->bagNumber), '0')."'
        AND customer.business_id = {$this->business_id}";
        $query = $this->db->query($sql);
        if ($phone = $query->row()) {

            // get the last order for the bag
            $sql = "SELECT dateCreated, bag_id from orders where bag_id = $phone->bagID order by orderID DESC limit 1";
            $query = $this->db->query($sql);
            $order = $query->row();

            //respond that they can now place an order
            $business = new Business($this->business_id);
            $CI = get_instance();
            $CI->business->timezone = $business->timezone;

            // get the last order date, if over 3 days old we display the date with a red background on the phone
            // to notify the driver the order needs some action (pickup reminder)
            $lastOrderDate = convert_from_gmt_aprax($order->dateCreated, STANDARD_DATE_FORMAT);
            $threeDays = strtotime ( '-3 day' , strtotime ( date("Y-m-d") ) ) ;
            if (strtotime($lastOrderDate)<$threeDays) {
                $style='background-color:#cc0000; color:#fff;padding:10px';
            }
            $response = 'Locker Code: '.substr($phone->phone,-4);
            $response .='<hr><br><div style="'.$style.'">Last order date: '.$lastOrderDate.'</div>';
        } else {
            $response = 'Sorry, we could not find an account with bag number: '.$this->fullBagNumber;

        }
        echo $response;
    }

    /**
     * gets driver notes
     */
    public function getDriverNotes()
    {
        $sql = "SELECT
        DriverNotesID as note_id,
        created as date_created,
        location_id,
        note,
        active,
        employee.firstName, employee.lastName
        FROM driverNotes
        INNER JOIN location l ON driverNotes.location_id = locationID
        INNER JOIN business_employee on ID = createdBy
        INNER JOIN employee ON employee_id = employeeID
        WHERE active = 1
        AND l.route_id in ({$this->routes})
        AND driverNotes.business_id = {$this->business_id}";
        $notes = $this->db->query($sql)->result();
        echo json_encode($notes);
    }

    public function getWashFoldLog()
    {
        if ($this->job == 'get') {
            $sql = "SELECT orderID as order_id, bagNumber as bag_number
            FROM orders
            INNER JOIN bag on bagID = bag_id
            WHERE orderStatusOption_id != 10 AND orderPaymentStatusOption_id != 3
            AND orders.business_id = {$this->business_id}";
            $orders = $this->db->query($sql)->result();
            echo json_encode($orders);

            return true;
        }

        if ($this->job =='post') {

            if (!$this->bagNumber) {
                die("Missing Bag Number or invalid");
            }

            $bag = Bag::search(array(
                'bagNumber' => $this->bagNumber,
                'business_id' => $this->business_id,
            ));

            if (!$bag) {
                echo 'Bag Not Found. ';
                return false;
            }

            // common to all email messages
            $subject = "Dirty WF Scan {$this->fullBagNumber}";
            $host = $_SERVER['HTTP_HOST'];

            $bag_url = "https://$host/admin/orders/bag_barcode_details/0/{$this->bagNumber}";
            $bag_link = "<a href='$bag_url'>{$this->bagNumber}</a>";

            // check for customer
            if (!$bag->customer_id) {
                echo 'Bag has no customer, give to customer service.';
                $message = "Bag {$this->fullBagNumber} was scanned on the android phone "
                         . "but the system could not find any customer for this bag. <br><br>"
                         . $bag_link;
                send_admin_notification($subject, $message, $this->business_id);
                return false;
            }

            $customer = new Customer($bag->customer_id);
            $customer_url = "$host/admin/customers/detail/{$customer->customerID}";
            $customer_link = "<a href='$customer_url'>{$customer->firstName} {$customer->lastName} ({$customer->customerID})</a>";

            if ($customer->hold == 1) {
                echo 'Customer on hold, give to customer service.';
                $message = "Can not create order because there is a hold on {$customer_link}'s account<br><br>"
                         . "Bag: $bag_link";
                send_admin_notification($subject, $message, $this->business_id);
                return false;
            }

            $this->load->model('order_model');

            $claim = false;
            $claims = Claim::search_aprax(array(
                'customer_id' => $bag->customer_id,
                'business_id' => $this->business_id,
                "active" => 1,
            ));

            if ($claims) {

                // check that there is only one claim at the locker
                if (count($claims) > 1) {
                    echo 'Multiple claims were found, give to customer service.';
                    $message = "Multiple claims were found for customer $customer_link. "
                             . "Only one claim should exist.<br><br>"
                             . "Bag: $bag_link";
                    send_admin_notification($subject, $message, $this->business_id);

                    return false;
                }

                $claim = $claims[0];
                $locker = new Locker($claim->locker_id);

            // If no claim, try to use the last order
            } else if ($orders = $this->order_model->getOrdersByBag($this->bagNumber, $this->business_id)) {

                $pastOrder = $orders[0];

                //FIXME: hardcoded locationType name
                if ($pastOrder->lockerType == "lockers" && $pastOrder->lockType == "Electronic") {
                    $locker = new Locker($pastOrder->locker_id);
                }
            }

            //$location_url = "$host/admin/orders/view_location_orders/$location->locationID";
            //$location_link = "<a href='$location_url'>{$location->address}</a>";

            if (!empty($locker)) {
                // create the order
                $notes = "";
                $options['customer_id'] = $bag->customer_id;
                $options['claimID'] = $claim ? $claim->claimID : null;
                $order_id = $this->order_model->new_order(
                    $locker->location_id,
                    $locker->lockerID,
                    $this->bagNumber,
                    $this->employee_id,
                    $notes,
                    $this->business_id,
                    $options
                );

                // if order was created
                if ($order_id) {
                    echo "Order created: #$order_id. ";
                    return true;
                }
            }

            echo 'No open order, give to customer service.';
            $message = "Bag {$this->fullBagNumber} was scanned on the android phone "
                     . "but the system could not find any orders for this bag. <br><br>"
                     . "Bag: $bag_link";
            send_admin_notification($subject, $message, $this->business_id);

        }
    }

    /**
     * creates a ticket from an issue sent from a phone
     */
    public function postIssue()
    {
        $this->load->model('droidpostdrivernotes_model');
        $options['noteType'] = 'issue';
        $options['employeeUsername'] = $this->username;
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;
        $options['transaction_id'] = $this->transaction_id;
        $options['notes'] = urldecode($this->driverNote);
        $options['location_id'] = $this->location_id;
        $options['lockerName'] = $this->lockerName;
        $options['bagNumber'] = $this->bagNumber;
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        $this->droidpostdrivernotes_model->insert($options);

        echo "SUCCESS|".$this->transaction_id;

    }

    /**
     * sets the order to status of loaded
     * 
     * @realtime
     * 
     * Modified for new driver app
     * @throws Exception
     */
    public function postLoadVan()
    {
        $employee = $this->getEmployee();
        $data = $this->requiredParameters(array(
            'order_id',
            'bagNumber',
        ));
        
        try {
            $loadVanObj = new DroidLoadVan();
            $result = $loadVanObj->loadVan($data['order_id'], $this->deliveryTime, $data['bagNumber'], $employee->username, $this->employee_id, $this->business_id);
            
            $result = json_decode($result);
            
            $this->load->model('orderstatus_model');
            $status = $this->orderstatus_model->getItem($result->order_status_id);
            
            $msg = get_translation("load_van_success", "mobile/driver", array('bagNumber' => $data['bagNumber'], 'order_id' => $data['order_id']), "Bag number %bagNumber% from order %order_id% set as loaded", null, $this->input->get("business_languageID"));
            output_ajax_success_response(array('orderStatus' => $status[0]), $msg);
        } catch (Exception $e) {
            $msg = get_translation("load_van_error","mobile/driver", array('bagNumber' => $data['bagNumber'], 'order_id' => $data['order_id'], 'msg' => $e->getmessage()), "Error while registering van load for bag %bagNumber%, order %order_id%: %msg%" , null, $this->input->get("business_languageID"));
            output_ajax_error_response($msg);
        }
    }

    /**
     * driver sends a note about a claim
     */
    public function postClaimNote()
    {
        $employee = $this->getEmployee();
        
        $this->load->model('droidpostdrivernotes_model');
        $foundNotes = $this->droidpostdrivernotes_model->get(array('transaction_id'=>$this->transaction_id));
        if (!$foundNotes) {
            $options['noteType'] = 'claimNote';
            $options['employeeUsername'] = $employee->username;
            $options['business_id'] = $this->business_id;
            $options['employee_id'] = $this->employee_id;

            $options['transaction_id'] = $this->transaction_id;

            $options['notes'] = $this->driverNote;
            $options['claim_id'] = $this->claim_id;
            $options['location_id'] = $this->location_id;
            $options['lockerName'] = $this->lockerName;
            $options['customerName'] = $this->customerName;

            $options['version'] = self::VERSION;
            $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
            $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
            $this->droidpostdrivernotes_model->insert($options);

            $msg = "Logged note for claim {$this->claim_id}";
        } else {
            $msg = "Note already logged for claim {$this->claim_id}. Nothing to do here";
        }
        
        output_ajax_success_response(array("transaction_id"=>$this->transaction_id), $msg);
    }

    /**
     * driver sends a note about a claim
     * 
     * @deferred
     */
    public function postDeliveryNote()
    {
        $employee = $this->getEmployee();
        
        $order = new Order($this->order_id);
        $customer = $order->find('customer_id');
        $locker = $order->find('locker_id');

        $this->load->model('droidpostdrivernotes_model');
        $options['employeeUsername'] = $employee->username;
        $options['business_id'] = $this->business_id;
        $options['employee_id'] = $this->employee_id;

        $options['transaction_id'] = $this->transaction_id;
        $options['customerName'] = $customer->firstName." ".$customer->lastName;
        
        $options['noteType'] = $this->noteType;
        $options['notes'] = $this->driverNote;
        $options['order_id'] = $this->order_id;
        
        $options['version'] = self::VERSION;
        $options['phoneVersion'] = isset($this->phoneVersion)?$this->phoneVersion:0;
        $options['phoneNumber'] = isset($this->phoneNumber)?$this->phoneNumber:0;
        $this->droidpostdrivernotes_model->insert($options);

        output_ajax_success_response(array("transaction_id"=>$this->transaction_id), "Delivery note send to droidpostdrivernotes table");

    }

    /**
     * @deprecated
     * @throws Exception
     */
    public function postNote()
    {
        try {

        if (empty($this->username)) {
        throw new Exception("Username is required");
        }

        if (empty($this->order_id) || !is_numeric($this->order_id)) {
                throw new Exception("Order ID is required and must be numeric");
        }

        if (empty($this->driverNote)) {
        throw new Exception("Missing note");
        }

        $body = (DEV) ? '(DEV MESSAGE, PLEASE IGNORE) ' : '';

        $body .= "Driver: {$this->username} had a problem with order: {$this->order_id} on ".date(STANDARD_DATE_FORMAT)."<br />";
        $body .= "NOTE: ". $this->driverNote;

        send_admin_notification('Driver had a problem with order', $body,$this->business_id);
        echo "SUCCESS";

        //$options = array();
        //$options['postNotes'] = "Delivery Note Sent Successfully";
        //$options['methodCalled'] = __METHOD__;
        //$options['delType'] = 'DeliveryNote';
        //$this->driver_log_model->update($options, array('driverLogID'=>$this->transaction_id));

        } catch (Exception $e) {
        send_exception_report($e);
        }

    }

    /**
     * Driver clockes in or out
     * 
     * @deferred
     *
     * We always return a success even if failed to get the data off the phone
     */
    public function postTimecard()
    {
        $employee = $this->getEmployee();

        $data = array (
            'transaction_id' => $this->transaction_id,
        );
        $msg = '';

        try {

            $sql = "INSERT IGNORE INTO droid_postTimeCards (transaction_id, `inout`, clockTime, employee_id, business_id, status, version, phoneVersion, phoneNumber)
                    VALUES ('{$this->transaction_id}', '{$this->inout}', '".$this->formatDate($this->clockTime, $this->business_id)."','{$this->employee_id}', '{$this->business_id}', 'pending', '".self::VERSION."', '".$this->phoneVersion."', '".$this->phoneNumber."')";
            $this->db->query($sql);
            $timeCardID = $this->db->insert_id();
            
            if (empty($timeCardID)) {
                try {
                    throw new Exception ("Duplicate entry while saving the timecard for employee_id [". $this->employee_id."]");
                } catch (Exception $e) {
                    $msg = $e->getMessage();
                    send_exception_report($e);
                }
                
                output_ajax_success_response($data, $msg);
            } 
            
            $data['insert_id'] = $timeCardID;
            $msg = "Timecard save for employee_id [". $this->employee_id."]";
            output_ajax_success_response($data, $msg);
        } catch (\Exception $e) {
            send_exception_report($e);
            
            $msg = "There was an exception, but driver should never know. " . $e->getMessage();
            output_ajax_success_response($data, $msg);
        }

      }


      /**
     * converts the date from the phone to GMT dateTime
     *
     * NOTE: if testing and sending requests from a computer that is set up to
     * use GMT, this function will create wrong dateTimes. Basically it will be
     * double converting the GMT date
     *
     * @param date $date
     * @param int $business_id
     * @return date gmtDate;
     */
    protected function formatDate($date, $business_id)
    {
        $CI = get_instance();
        $business = new Business($business_id);
        $gmtDate = convert_to_gmt_aprax($date, $business->timezone, $format = "Y-m-d H:i:s");

        return $gmtDate;
    }


    /**
     * Generate the time windows for the day
     */
    protected function getTimeWindows()
    {
        $timeStart   = strtotime("00:00:00");
        $timeEnd     = strtotime("23:59:59");
        $windowStart = gmdate('Y-m-d H:i:s', $timeStart);
        $windowEnd   = gmdate('Y-m-d H:i:s', $timeEnd);

        return array($windowStart, $windowEnd);
    }

}
