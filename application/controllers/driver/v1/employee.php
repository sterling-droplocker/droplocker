<?php

/**
 *
 */
class Employee extends MY_Driver_Api_Controller
{
    public function login()
    {
        header('Access-Control-Allow-Origin: *');
        
        $data = $this->requiredParameters(array(
            'username',
            'password',
        ));
        
        $this->load->model('employee_model');
        $loggedData = $this->employee_model->login($data['username'] , $data['password']);

        if (!$loggedData) {
            output_ajax_error_response(get_translation("employee_validate_wrongCredentials","mobile/driver", array(), 
                "Wrong username or password", null, $this->input->get("business_languageID")));
        }
        
        $loggedData = $this->employee_model->get(array(
            'employeeID' => $loggedData->employeeID,
        ));
        $loggedData = $loggedData[0];
        
        $business_id = $loggedData->business_id;

        $sessionTokenExpireDays = $this->input->get("sessionTokenExpireDays");
        $plusDays = intval($sessionTokenExpireDays ? $sessionTokenExpireDays : 1);
        $expireDate = gmdate('Y-m-d H:i:s', strtotime("+$plusDays days"));

        $sessionData = $this->setupSession($business_id, $loggedData->employeeID, $expireDate);
        
        $loggedData->business = $this->getBusinessData($business_id);
        
        $sessionData['employee'] = $loggedData;
        
        output_ajax_success_response($sessionData, 'Employee logged in');
    }
    
    protected function getBusinessData($business_id)
    {
        $this->load->model('business_model');
        $business = $this->business_model->get(array(
            'businessID' => $business_id,
        ));
        
        $business = $business[0];
        $indexedLanguages = array();
        
        $this->load->model("business_language_model");
        $lang_list = $this->business_language_model->get_all_for_business($business_id);
        foreach ($lang_list as $lang) {
            $indexedLanguages[$lang->business_languageID] = $lang;
        }
        
        $business->lang_list = $indexedLanguages;
        
        return $business;
    }

        /**
     * Creates or renews an api session
     *
     * @param int $business_id
     * @param int $employee_id
     * @param string $expireDate
     * @return array
     */
    protected function setupSession($business_id, $employee_id, $expireDate = null)
    {
        // default expires in 24hs
        if (empty($expireDate)) {
            $expireDate = gmdate('Y-m-d H:i:s', time() + 86400);
        }

        $this->load->model('driver_apisession_model');

        $data = array(
            'token' => md5(uniqid(rand(0, 100000))),
            'expireDate' => $expireDate,
            'business_id' => $business_id,
            'employee_id' => $employee_id,
        );

        $existingSession = $this->driver_apisession_model->get_one(array(
            'employee_id' => $employee_id,
            'business_id' => $business_id,

        ));

        if ($existingSession) {
            $data['driver_apiSessionID'] = $existingSession->driver_apiSessionID;
            $data['token'] = $existingSession->token;
        }

        $this->driver_apisession_model->save($data);

        return array(
            'employee_id' => $employee_id,
            'business_id' => $business_id,
            'sessionToken' => $data['token'],
            'sessionTokenExpireDate' => $expireDate,
        );
    }

}
