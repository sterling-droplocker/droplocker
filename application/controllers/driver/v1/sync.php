<?php
/**
 *
 */
class Sync extends MY_Driver_Api_Controller
{
    public function droidToDroplocker()
    {
        $this->load->library('driverappsynclibrary');
        $res = $this->driverappsynclibrary->droidToDroplocker($this->input->get('business_id'), $this->input->get('employee_id'));
        
        output_ajax_success_response($res, "droidToDroplocker finished");
    }
    
    
    /* 
     * driver_app_journal table ==> Firebase
     * Update data related to a specific entity (order or claim) at a route level
     * It will update routes for all entities that have changed since previous sync
     */
    public function driverJournalToFirebase()
    {
        $this->load->library('driverappsynclibrary');
        
        $res = $this->driverappsynclibrary->driverJournalToFirebase($this->input->get('business_id'), $this->input->get('day'));
        
        if ($res['success']) {
            output_ajax_success_response($res['data'], $res['message']);
        } else {
            output_ajax_error_response($res['message']);
        }
    }
    
    /* 
     * Update data related to a specific business
     * Upadtes the whole business node, if route list provided, business node will only contain those routes
     */
    public function business()
    {
        $this->load->library('driverappsynclibrary');
        
        $business_id = $this->input->get('business_id');
        $day = $this->input->get('day');

        $routes = null;
        if($this->input->get('routes'))
        {
            $routes = explode(',', str_replace(' ', '', $this->input->get('routes')));
        }
        
        $force = $this->input->get('force');

        $res = $this->driverappsynclibrary->syncBusinesses($business_id, $day, $routes, $force);

        if ($res['success']) {
            output_ajax_success_response(array(
                'business_id' => $business_id, 
                'routes' => $routes, 
                'day' => $day,
                'time' => $res['time'],
                'detail' => $res,
                ), "Routes updated"
            );
        } else {
            output_ajax_error_response($res['message']);
        }
    }
    
    /* 
     * Update data related to a specific entity (order or claim) at a route level 
     */
    public function routeForEntity()
    {
        $this->load->library('driverappsynclibrary');
        $data = $this->requiredParameters(array(
            'table',
            'id',
            'business_id'
        ));
        
        $day = $this->input->get('day');
        if ($day === FALSE) {
            $day = $this->driverappsynclibrary->getTodayDayNumber($data['business_id']);
        }

        $res = $this->driverappsynclibrary->syncRouteForEntity($day, $data['table'], $data['id']);
            
        if ($res['success']) {
            output_ajax_success_response(array(
                'table' => $data['table'], 
                'id' => $data['id'], 
                'day' => $day,
                'log' =>$res['log'],
                ), "Routes updated, data inserted at {$res['path']}"
            );
        } else {
            output_ajax_error_response($res['message']);
        }
    }
    /* 
     * Update data related to a specific entity (order or claim) at a route level 
    */
    public function route()
    {
        $this->load->library('driverappsynclibrary');
        $data = $this->requiredParameters(array(
            'business_id',
            'route',
        ));
        
        $day = $this->input->get('day');
        if ($day === FALSE) {
            $day = $this->driverappsynclibrary->getTodayDayNumber($data['business_id']);
       }

        $res = $this->driverappsynclibrary->syncRouteForBusiness($day, $data['business_id'], $data['route']);
            
        if ($res['success'] || $this->input->get('debug')) {
            output_ajax_success_response(array(
                'business_id' => $data['business_id'], 
                'route' => $data['route'], 
                'day' => $day,
                'time' =>$res['time'],
                'log' =>$res['log'],
                ), "Routes updated, data inserted at {$res['path']}"
            );
        } else {
            output_ajax_error_response($res['message']);
        }
    }
       
    
    /* 
     * Update data related to a specific entity (order or claim)
     */
    public function entity()
    {
        $this->load->library('driverappsynclibrary');
        $data = $this->requiredParameters(array(
            'table',
            'id',
            'business_id'
        ));
        
        $day = $this->input->get('day');
        if ($day === FALSE) {
            $day = $this->driverappsynclibrary->getTodayDayNumber($data['business_id']);
        }
        
        if ($data['table'] == 'driverNotes') {
            $res = $this->driverappsynclibrary->syncDriverNote($day, $data['table'], $data['id'], $data['business_id']);
        } else {
            $res = $this->driverappsynclibrary->syncEntity($day, $data['table'], $data['id']);
        }
            
        if ($res['success']) {
            output_ajax_success_response(array(
                'table' => $data['table'], 
                'id' => $data['id'], 
                'day' => $day,
                'log' =>$res['log'],
                ), "Request processed" . (isset($res['path']) ? " data inserted at {$res['path']}" : "")
            );
        } else {
            output_ajax_error_response($res['message']);
        }
    }
    
    public function requestQueueToApi()
    {
        $this->load->library('driverappsynclibrary');
        $res = $this->driverappsynclibrary->requestQueueToApi();
        
        if ($res['success']) {
            output_ajax_success_response($res['data'], $res['message']);
        } else {
            output_ajax_error_response($res['message']);
        }
    }
    
    protected function out($msg)
    {
        echo gmdate('Y-m-d H:i:s') . " -- $msg<br />\n";
    }
    
    protected function getOutputLog()
    {
        $log = ob_get_contents();
        ob_end_clean();

        $log = explode("\n", str_replace('<br />', '', $log));
        
        return $log;
    }
}
