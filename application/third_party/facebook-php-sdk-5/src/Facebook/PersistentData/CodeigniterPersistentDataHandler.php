<?php

use Facebook\PersistentData\PersistentDataInterface;

class CodeigniterPersistentDataHandler implements PersistentDataInterface
{
    /**
     * @var string Prefix to use for session variables.
     */
    protected $sessionPrefix = 'FBRLH_';

    /**
     * @inheritdoc
     */
    public function get($key)
    {
        $CI = get_instance();
        return $CI->session->userdata($this->sessionPrefix . $key);
    }

    /**
     * @inheritdoc
     */
    public function set($key, $value)
    {
        $CI = get_instance();
        $CI->session->set_userdata($this->sessionPrefix . $key, $value);
    }
}
