<?php

class CyberSource_SoapClient extends SoapClient
{

    protected $mode = 'live';
    protected $merchantID = null;
    protected $transactionKey = null;

    /**
     * Constructs a client
     *
     * @param string $merchantID
     * @param string $transactionKey
     * @param string $mode
     */
    function __construct($merchantID, $transactionKey, $mode = 'live')
    {
        $this->mode = $mode;
        $this->merchantID = $merchantID;
        $this->transactionKey = $transactionKey;

        if ($this->mode == 'live') {
            $wsdl = 'https://ics2ws.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.105.wsdl';
        } else {
            $wsdl = 'https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.105.wsdl';
        }

        parent::__construct($wsdl);
    }

    /**
     * Performs a SOAP request
     *
     * Overwritten to add auth data before doing the request
     *
     * @see \SoapClient::__doRequest()
     * @param string $request
     * @param string $location
     * @param string $action
     * @param int $version
     * @param int $one_way
     */
    function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        // This section inserts the UsernameToken information in the outgoing SOAP message.
        $soapHeader = "<SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">
            <wsse:Security SOAP-ENV:mustUnderstand=\"1\">
                <wsse:UsernameToken>
                    <wsse:Username>{$this->merchantID}</wsse:Username>
                    <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">{$this->transactionKey}</wsse:Password>
                </wsse:UsernameToken>
            </wsse:Security>
        </SOAP-ENV:Header>";

        $requestDOM = new DOMDocument('1.0');
        $soapHeaderDOM = new DOMDocument('1.0');

        $requestDOM->loadXML($request);
        $soapHeaderDOM->loadXML($soapHeader);

        $node = $requestDOM->importNode($soapHeaderDOM->firstChild, true);
        $requestDOM->firstChild->insertBefore($node, $requestDOM->firstChild->firstChild);

        $request = $requestDOM->saveXML();

        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }
}