<?php
/**
 *	@package Paygateway
 */
class GiftCardResponse extends TransactionResponseBase {

	function GiftCardResponse($argResponseString){
		parent::TransactionResponse($argResponseString);
	}

        function getApprovedAmount() {
            return $this->getProperty(APPROVED_AMOUNT);
        }
        function getRemainingBalance() {
            return $this->getProperty(REMAINING_BALANCE);
        }
//        function getLocalTimeStamp() {
//            return $this->getProperty(LOCAL_TIMESTAMP);
//        }
        function getAccountCurrency() {
            return $this->getProperty(ACCOUNT_CURRENCY);
        }
        function getTransactionState() {
            return $this->getProperty(TRANSACTION_STATE);
        }
        function getPan() {
            return $this->getProperty(PAN);
        }
        function getPersonalIdentificationNumber() {
            return $this->getProperty(PERSONAL_IDENTIFICATION_NUMBER);
        }
} // end GiftCardResponse
?>
