<?php
	/**
	 *	@package Paygateway
	 */
	class DebitCardResponse extends TransactionResponseBase {

		function DebitCardResponse($argResponseString){
			parent::TransactionResponse($argResponseString);
		}

		function getOrderId() {
			return $this->getProperty(ORDER_ID);
		}
		function GetBatchId()
		{
		  return $this->getProperty(BATCH_ID);
		}
		function GetReferenceId()
		{
		  return $this->getProperty(CC_REFERENCE_ID);
		}
		function GetBankApprovalCode()
		{
		  return $this->getProperty(BANK_APPROVAL_CODE);
		}
		function GetPurchasedAmount()
		{
		  return $this->getProperty(PURCHASED_AMOUNT);
		}
		function GetRefundedAmount()
		{
		  return $this->getProperty(REFUNDED_AMOUNT);
		}
		function GetCashBackAmount()
		{
		  return $this->getProperty(CASH_BACK_AMOUNT);
		}
		function GetCashBenefitBalance()
		{
		  return $this->getProperty(CASH_BENEFIT_BALANCE);
		}
		function GetFoodStampBalance()
		{
		  return $this->getProperty(FOOD_STAMP_BALANCE);
		}
		function GetAccountType()
		{
		  return $this->getProperty(ACCOUNT_TYPE);
		}
		function GetVoucherNumber()
		{
		  return $this->getProperty(VOUCHER_NUMBER);
		}
		function GetTransactionConditionCode()
		{
		  return $this->getProperty(TRANSACTION_CONDITION_CODE);
		}
		function GetState()
		{
		  return $this->getProperty(STATE);
		}
		function GetTimeStampCreated()
		{
		  return $this->getProperty(TIME_STAMP_CREATED);
		}
		
		function GetDebitReceiptNumber()
		{
		  return $this->getProperty(DEBIT_RECEIPT_NUMBER);
		}
		
		function GetReversalStatus()
		{
		  return $this->getProperty(REVERSAL_STATUS);
		}

	} // end DebitCardResponse
?>
