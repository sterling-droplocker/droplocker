<?php
	/**
	 *	@package Paygateway
	 */	 
	class RecurringResponse extends TransactionResponseBase {

		function RecurringResponse($argResponseString){
			parent::TransactionResponse($argResponseString);
		}

		function getPayerDataManagedUntilDate() {
			return $this->getProperty(PAYER_DATA_MANAGED_UNTIL_DATE);
		}
	}
?>
