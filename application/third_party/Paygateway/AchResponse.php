<?php
	/**
	 *	@package Paygateway
	 */
	class ACHResponse extends TransactionResponseBase {

		function ACHResponse($argResponseString){
			parent::TransactionResponse($argResponseString);
		}

		function getReferenceId() {
			return $this->getProperty(ACH_REFERENCE_ID);
		}

		function getOrderId() {
			return $this->getProperty(ORDER_ID);
		}

		function getTransactionStatus() {
			return $this->getProperty(TRANSACTION_STATUS);
		}

		function getBatchID() {
			return $this->getProperty(BATCH_ID);
		}

		function getIsResubmittable() {
			return $this->getProperty(IS_RESUBMITTABLE);
		}

		function getOriginalReferenceId() {
			return $this->getProperty(ORIGINAL_REFERENCE_ID);
		}

		function getAmount() {
			return $this->getProperty(AMOUNT);
		}

		function getReturnReasonCode() {
			return $this->getProperty(RETURN_REASON_CODE);
		}
		function getReturnReasonText() {
			return $this->getProperty(RETURN_REASON_TEXT);
		}

		function getPayerIdentifier() {
			return $this->getProperty(PAYER_IDENTIFIER);
		}

		function getManagedPayerDataResponseCode() {
			return $this->getProperty(MANAGED_PAYER_DATA_RESPONSE_CODE);
		}

		function getManagedPayerDataResponseCodeText() {
			return $this->getProperty(MANAGED_PAYER_DATA_RESPONSE_CODE_TEXT);
		}

		function getPayerDataManagedUntilDate() {
			return $this->getProperty(PAYER_DATA_MANAGED_UNTIL_DATE);
		}
	} // end ACHResponse
?>
