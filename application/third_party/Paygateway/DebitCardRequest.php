<?php
	/**
	 *	@package Paygateway
	 */
	class DebitCardRequest extends TransactionRequestBase {


		function DebitCardRequest() {
		}

		function doTransaction() {
			$this->setTransactionType(DEBIT_CARD);
			return $this->executeTransaction();
		}

		/************************************************************
		 ************************************************************
		 *  Setter functions for Debit request fields
		 ************************************************************
		 ************************************************************/

		/**
		*	Only these values are allowed:
		*		- PURCHASE
		*		- REFUND
		*		- BALANCE_INQUIRY
		*		- VOUCHER_CLEAR
		*		- QUERY_PURCHASE
		*		- QUERY_REFUND
		*       - REVERSE_PURCHASE
		*       - REVERSE_REFUND
		*
		*	@param string $argChargeType
		*/
		function setChargeType($argChargeType) {
			$result = false;

			$this->setProperty(CHARGE_TYPE, $argChargeType);
			$this->clearError();
			$result = true;

			return $result;
		}

		/**
		*	Only these values are allowed:
		*		- DEFAULT = 2
		*		- CASH_BENEFIT = 3
		*		- FOOD_STAMP = 4
		*
		*	@param string $argAccountType
		*/
		function setAccountType($argAccountType) {
			$result = false;

			$this->setProperty(ACCOUNT_TYPE, $argAccountType);
			$this->clearError();
			$result = true;

			return $result;
		}

		function setPrimaryAccountNumber($argPrimaryAccountNumber) {
			$this->setProperty(PRIMARY_ACCOUNT_NUMBER, $argPrimaryAccountNumber);
			$this->clearError();
			return true;
		}

		function setTrack2($argTrack2) {
			$this->setProperty(TRACK2, $argTrack2);
			$this->clearError();
			return true;
		}



		/**
		*	- Format :  2 digits (ex.: February = "02")
		*
		*	@param numeric $argExpireMonth
		*/
		function setExpireMonth($argExpireMonth) {
			$result = false;

			if ((strlen($argExpireMonth) == 1 ||
				strlen($argExpireMonth) == 2) &&
				is_numeric($argExpireMonth) &&
				settype($argExpireMonth, "integer") &&
				$argExpireMonth > 0 &&
				$argExpireMonth < 13) {
				// Valid
				$this->setProperty(EXPIRE_MONTH, $argExpireMonth);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid expire month");
			}

			return $result;
		}

		/**
		*	- Format :  4 digits (ex.: "2006")
		*
		*	@param numeric $argExpireYear
		*/
		function setExpireYear($argExpireYear) {
			$result = false;

			if (strlen($argExpireYear) == 4 &&
				is_numeric($argExpireYear)) {
				// Valid
				$this->setProperty(EXPIRE_YEAR, $argExpireYear);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid expire year");
			}

			return $result;
		}

		function setEncryptedPIN($argEncryptedPIN) {
			$this->setProperty(PERSONAL_IDENTIFICATION_NUMBER, $argEncryptedPIN);
			$this->clearError();
			return true;
		}

		function setKeySerialNumber($argKeySerialNumber) {
			$this->setProperty(KEY_SERIAL_NUMBER, $argKeySerialNumber);
			$this->clearError();
			return true;
		}

		/**
		*	- Numeric format :  "1000.00"
		*
		*	@param numeric $argCashBackAmount
		*/
		function setCashBackAmount($argCashBackAmount) {
			$result = false;

			if(is_numeric($argCashBackAmount)) {
				// Valid
				$this->setProperty(CASH_BACK_AMOUNT, $argCashBackAmount);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid Cash Back Amount");
			}
			return $result;
		}

		function setBuyerCode($argBuyerCode) {
			$this->setProperty(BUYER_CODE, $argBuyerCode);
			$this->clearError();
			return true;
		}

		function setOrderCustomerId($argOrderCustomerId) {
			$this->setProperty(ORDER_CUSTOMER_ID, $argOrderCustomerId);
			$this->clearError();
			return true;
		}

		function setOrderDescription($argOrderDescription) {
			$this->setProperty(ORDER_DESCRIPTION, $argOrderDescription);
			$this->clearError();
			return true;
		}

		function setOrderId($argOrderId) {
			$this->setProperty(ORDER_ID, $argOrderId);
			$this->clearError();
			return true;
		}

		function setInvoiceNumber($argInvoiceNumber) {
			$this->setProperty(INVOICE_NUMBER, $argInvoiceNumber);
			$this->clearError();
			return true;
		}

		function setVoucherNumber($argVoucherNumber) {
			$this->setProperty(VOUCHER_NUMBER, $argVoucherNumber);
			$this->clearError();
			return true;
		}

		function setClerkId($argClerkId) {
			$this->setProperty(CLERK_ID, $argClerkId);
			$this->clearError();
			return true;
		}

		function setDeviceTerminalId($argDeviceTerminalId) {
			$this->setProperty(DEVICE_TERMINAL_ID, $argDeviceTerminalId);
			$this->clearError();
			return true;
		}

		function setPurchaseOrderNumber($argPurchaseOrderNumber) {
			$this->setProperty(PURCHASE_ORDER_NUMBER, $argPurchaseOrderNumber);
			$this->clearError();
			return true;
		}

		function setOrderUserId($argOrderUserId) {
			$this->setProperty(ORDER_USER_ID, $argOrderUserId);
			$this->clearError();
			return true;
		}

		function setReferenceId($argReferenceId) {
			$this->setProperty(REFERENCE_ID, $argReferenceId);
			$this->clearError();
			return true;
		}

		/**
		*	Possible values
        *    - CARDHOLDER_PRESENT_RETAIL_ORDER_KEYED = 9
        *    - DEBIT_CARDHOLDER_PRESENT_SWIPED_PIN_SUPPLIED = 60
        *    - DEBIT_CARDHOLDER_PRESENT_KEYED_PIN_SUPPLIED = 61
		*
		*	@param numeric $argTCC
		*/
		function setTransactionConditionCode($argTransactionConditionCode) {
			$result = false;

			if(is_numeric($argTransactionConditionCode)) {
				$this->setProperty(TRANSACTION_CONDITION_CODE, $argTransactionConditionCode);
				$this->clearError();
				$result = true;
			} else {
				$this->setError("Invalid transaction condition code");
			}

			return $result;
		}

		function setTaxExempt($argTaxExempt) {
			$this->setProperty(TAX_EXEMPT, $argTaxExempt);
			$this->clearError();
			return true;
		}

		/**
		*	- Numeric format :  "1000.00"
		*
		*	@param numeric $argChargeTotal
		*/
		function setChargeTotal($argChargeTotal) {
			$result = false;

			if(is_numeric($argChargeTotal)) {
				// Valid
				$this->setProperty(CHARGE_TOTAL, $argChargeTotal);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid charge total");
			}
			return $result;
		}

		/**
		*	- Numeric format :  "1000.00"
		*
		*	@param numeric $argShippingCharge
		*/
		function setShippingCharge($argShippingCharge) {
			$result = false;

			if(is_numeric($argShippingCharge)) {
				// Valid
				$this->setProperty(SHIPPING_CHARGE, $argShippingCharge);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid Shipping Charge");
			}
			return $result;
		}

		/**
		*	- Numeric format :  "1000.00"
		*
		*	@param numeric $argTaxAmount
		*/
		function setTaxAmount($argTaxAmount) {
			$result = false;

			if(is_numeric($argTaxAmount)) {
				// Valid
				$this->setProperty(TAX_AMOUNT, $argTaxAmount);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid Tax Amount");
			}
			return $result;
		}

		function setBankApprovalCode($argBankApprovalCode) {
			$this->setProperty(BANK_APPROVAL_CODE, $argBankApprovalCode);
			$this->clearError();
			return true;
		}

		function setDuplicateCheck($argDuplicateCheck) {
			$this->setProperty(DUPLICATE_CHECK, $argDuplicateCheck);
			$this->clearError();
			return true;
		}


		// Billing Fields
		function setBillFirstName($argBillFirstName) {
			$this->setProperty(BILL_FIRST_NAME, $argBillFirstName);
			$this->clearError();
			return true;
		}

		function setBillMiddleName($argBillMiddleName) {
			$this->setProperty(BILL_MIDDLE_NAME,$argBillMiddleName);
			$this->clearError();
			return true;
		}

		function setBillLastName($argBillLastName) {
			$this->setProperty(BILL_LAST_NAME, $argBillLastName);
			$this->clearError();
			return true;
		}

		function setBillCustomerTitle($argBillCustomerTitle) {
			$this->setProperty(BILL_CUSTOMER_TITLE,$argBillCustomerTitle);
			$this->clearError();
			return true;
		}

		function setBillCompanyName($argBillCompanyName) {
			$this->setProperty(BILL_COMPANY_NAME,$argBillCompanyName);
			$this->clearError();
			return true;
		}

		function setBillEmail($argBillEmail) {
			$this->setProperty(BILL_EMAIL, $argBillEmail);
			$this->clearError();
			return true;
		}

		function setBillAddressOne($argBillAddressOne)  {
			$this->setProperty(BILL_ADDRESS_ONE, $argBillAddressOne);
			$this->clearError();
			return true;
		}

		function setBillAddressTwo($argBillAddressTwo) {
			$this->setProperty(BILL_ADDRESS_TWO, $argBillAddressTwo);
			$this->clearError();
			return true;
		}

		function setBillCity($argBillCity) {
			$this->setProperty(BILL_CITY, $argBillCity);
			$this->clearError();
			return true;
		}

		function setBillStateOrProvince($argBillStateOrProvince) {
			$this->setProperty(BILL_STATE_OR_PROVINCE, $argBillStateOrProvince);
			$this->clearError();
			return true;
		}

		function setBillZipOrPostalCode($argBillPostalCode) {
			$this->setProperty(BILL_ZIP_OR_POSTAL_CODE, $argBillPostalCode);
			$this->clearError();
			return true;
		}

		/**
		*	- Accept only 2 characters country code
		*
		*	@param string $argBillCountryCode
		*/
		function setBillCountryCode($argBillCountryCode) {
			$result = false;

			if (strlen($argBillCountryCode) == 2) {
				// Valid code
				$this->setProperty(BILL_COUNTRY_CODE, $argBillCountryCode);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid bill country code.");
			}

			return $result;
		}

		function setBillPhone($argBillPhone) {
			$this->setProperty(BILL_PHONE,$argBillPhone);
			$this->clearError();
			return true;
		}

		function setBillFax($argBillFax) {
			$this->setProperty(BILL_FAX,$argBillFax);
			$this->clearError();
			return true;
		}

		function setBillNote($argBillNote) {
			$this->setProperty(BILL_NOTE,$argBillNote);
			$this->clearError();
			return true;
		}


		// Shipping Fields
		function setShipFirstName($argShipFirstName) {
			$this->setProperty(SHIP_FIRST_NAME,$argShipFirstName);
			$this->clearError();
			return true;
		}

		function setShipMiddleName($argShipMiddleName) {
			$this->setProperty(SHIP_MIDDLE_NAME,$argShipMiddleName);
			$this->clearError();
			return true;
		}

		function setShipLastName($argShipLastName) {
			$this->setProperty(SHIP_LAST_NAME,$argShipLastName);
			$this->clearError();
			return true;
		}

		function setShipCustomerTitle($argShipCustomerTitle) {
			$this->setProperty(SHIP_CUSTOMER_TITLE,$argShipCustomerTitle);
			$this->clearError();
			return true;
		}

		function setShipCompanyName($argShipCompanyName) {
			$this->setProperty(SHIP_COMPANY_NAME,$argShipCompanyName);
			$this->clearError();
			return true;
		}

		function setShipEmail($argShipEmail) {
			$this->setProperty(SHIP_EMAIL,$argShipEmail);
			$this->clearError();
			return true;
		}

		function setShipAddressOne($argShipAddressOne) {
			$this->setProperty(SHIP_ADDRESS_ONE,$argShipAddressOne);
			$this->clearError();
			return true;
		}

		function setShipAddressTwo($argShipAddressTwo) {
			$this->setProperty(SHIP_ADDRESS_TWO,$argShipAddressTwo);
			$this->clearError();
			return true;
		}

		function setShipCity($argShipCity) {
			$this->setProperty(SHIP_CITY,$argShipCity);
			$this->clearError();
			return true;
		}

		function setShipStateOrProvince($argShipStateOrProvince) {
			$this->setProperty(SHIP_STATE_OR_PROVINCE,$argShipStateOrProvince);
			$this->clearError();
			return true;
		}

		function setShipZipOrPostalCode($argShipPostalCode) {
			$this->setProperty(SHIP_POSTAL_CODE,$argShipPostalCode);
			$this->clearError();
			return true;
		}

		/**
		*	- Accept only 2 characters country code
		*
		*	@param string $argShipCountryCode
		*/
		function setShipCountryCode($argShipCountryCode) {
			$result = false;

			if (strlen($argShipCountryCode) == 2) {
				// Valid code
				$this->setProperty(SHIP_COUNTRY_CODE, $argShipCountryCode);
				$this->clearError();
				$result = true;
			} else {
				// Invalid
				$this->setError("Invalid ship country code.");
			}

			return $result;
		}

		function setShipPhone($argShipPhone) {
			$this->setProperty(SHIP_PHONE,$argShipPhone);
			$this->clearError();
			return true;
		}

		function setShipFax($argShipFax) {
			$this->setProperty(SHIP_FAX,$argShipFax);
			$this->clearError();
			return true;
		}

		function setShipNote($argShipNote) {
			$this->setProperty(SHIP_NOTE,$argShipNote);
			$this->clearError();
			return true;
		}
		
		function getChargeType() {
			return $this->getProperty(CHARGE_TYPE);
		}

		function getAccountType() {
			return $this->getProperty(ACCOUNT_TYPE);
		}

		function getPrimaryAccountNumber() {
			return $this->getProperty(PRIMARY_ACCOUNT_NUMBER);
		}

		function getTrack2() {
			return $this->getProperty(TRACK2);
		}

		function getExpireMonth() {
			return $this->getProperty(EXPIRE_MONTH);
		}

		function getExpireYear() {
			return $this->getProperty(EXPIRE_YEAR);
		}

		function getEncryptedPIN() {
			return $this->getProperty(PERSONAL_IDENTIFICATION_NUMBER);
		}

		function getKeySerialNumber() {
			return $this->getProperty(KEY_SERIAL_NUMBER);
		}

		function getCashBackAmount() {
			return $this->getProperty(CASH_BACK_AMOUNT);
		}

		function getBuyerCode() {
			return $this->getProperty(BUYER_CODE);
		}

		function getOrderCustomerId() {
			return $this->getProperty(ORDER_CUSTOMER_ID);
		}

		function getOrderDescription() {
			return $this->getProperty(ORDER_DESCRIPTION);
		}

		function getOrderId() {
			return $this->getProperty(ORDER_ID);
		}

		function getInvoiceNumber() {
			return $this->getProperty(INVOICE_NUMBER);
		}

		function getVoucherNumber() {
			return $this->getProperty(VOUCHER_NUMBER);
		}

		function getClerkId() {
			return $this->getProperty(CLERK_ID);
		}

		function getDeviceTerminalId() {
			return $this->getProperty(DEVICE_TERMINAL_ID);
		}

		function getPurchaseOrderNumber() {
			return $this->getProperty(PURCHASE_ORDER_NUMBER);
		}

		function getOrderUserId() {
			return $this->getProperty(ORDER_USER_ID);
		}

		function getReferenceId() {
			return $this->getProperty(REFERENCE_ID);
		}

		function getTransactionConditionCode() {
			return $this->getProperty(TRANSACTION_CONDITION_CODE);
		}

		function getTaxExempt() {
			return $this->getProperty(TAX_EXEMPT);
		}

		function getChargeTotal() {
			return $this->getProperty(CHARGE_TOTAL);
		}

		function getShippingCharge() {
			return $this->getProperty(SHIPPING_CHARGE);
		}

		function getTaxAmount() {
			return $this->getProperty(TAX_AMOUNT);
		}

		function getBankApprovalCode() {
			return $this->getProperty(BANK_APPROVAL_CODE);
		}

		function getDuplicateCheck() {
			return $this->getProperty(DUPLICATE_CHECK);
		}


		function getBillCustomerTitle() {
			return $this->getProperty(BILL_CUSTOMER_TITLE);
		}

		function getBillFirstName() {
			return $this->getProperty(BILL_FIRST_NAME);
		}

		function getBillMiddleName() {
			return $this->getProperty(BILL_MIDDLE_NAME);
		}

		function getBillLastName() {
			return $this->getProperty(BILL_LAST_NAME);
		}

		function getBillCompanyName() {
			return $this->getProperty(BILL_COMPANY_NAME);
		}

		function getBillAddressOne()  {
			return $this->getProperty(BILL_ADDRESS_ONE);
		}

		function getBillAddressTwo() {
			return $this->getProperty(BILL_ADDRESS_TWO);
		}

		function getBillCity() {
			return $this->getProperty(BILL_CITY);
		}

		function getBillStateOrProvince() {
			return $this->getProperty(BILL_STATE_OR_PROVINCE);
		}

		function getBillCountryCode() {
			return $this->getProperty(BILL_COUNTRY_CODE);
		}

		function getBillZipOrPostalCode() {
			return $this->getProperty(BILL_ZIP_OR_POSTAL_CODE);
		}

		function getBillPhone() {
			return $this->getProperty(BILL_PHONE);
		}

		function getBillFax() {
			return $this->getProperty(BILL_FAX);
		}

		function getBillEmail() {
			return $this->getProperty(BILL_EMAIL);
		}

		function getBillNote() {
			return $this->getProperty(BILL_NOTE);
		}


		function getShipCustomerTitle() {
			return $this->getProperty(SHIP_CUSTOMER_TITLE);
		}

		function getShipFirstName() {
			return $this->getProperty(SHIP_FIRST_NAME);
		}

		function getShipMiddleName() {
			return $this->getProperty(SHIP_MIDDLE_NAME);
		}

		function getShipLastName() {
			return $this->getProperty(SHIP_LAST_NAME);
		}

		function getShipCompanyName() {
			return $this->getProperty(SHIP_COMPANY_NAME);
		}

		function getShipAddressOne() {
			return $this->getProperty(SHIP_ADDRESS_ONE);
		}

		function getShipAddressTwo() {
			return $this->getProperty(SHIP_ADDRESS_TWO);
		}

		function getShipCity() {
			return $this->getProperty(SHIP_CITY);
		}

		function getShipStateOrProvince() {
			return $this->getProperty(SHIP_STATE_OR_PROVINCE);
		}

		function getShipCountryCode() {
			return $this->getProperty(SHIP_COUNTRY_CODE);
		}

		function getShipZipOrPostalCode() {
			return $this->getProperty(SHIP_POSTAL_CODE);
		}

		function getShipPhone() {
			return $this->getProperty(SHIP_PHONE);
		}

		function getShipFax() {
			return $this->getProperty(SHIP_FAX);
		}

		function getShipEmail() {
			return $this->getProperty(SHIP_EMAIL);
		}

		function getShipNote() {
			return $this->getProperty(SHIP_NOTE);
		}

	} // end DebitCardRequest
?>
