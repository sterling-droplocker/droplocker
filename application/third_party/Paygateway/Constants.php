<?php
	// Copyright 2006, Payment Processing Inc

	/**
	 *	@package Paygateway
	 */

	// ***************************************************
	// Constants common to all types of transactions
	// ***************************************************
	define("VERSION", "PHP Plug v12.10.0");
	define("PROT_VERSION", "12");
	define("PROTOCOL_VERSION", "protocol_version");
	define("PARAMETER_SEPARATOR", "&");
	define("POST_URL", "https://etrans.paygateway.com/TransactionManager");
	define("ACCOUNT_TOKEN", "account_token");
	define("VERSION_ID", "version_id");
	define("TRANSACTION_TYPE", "transaction_type");
	define("TRANSACTION_CONDITION_CODE", "transaction_condition_code");
	define("ORDER_USER_ID", "order_user_id");
	define("INVOICE_NUMBER", "invoice_number");
	define("DUPLICATE_CHECK", "duplicate_check");
	define("ORDER_ID", "order_id");
	define("REFERENCE_ID", "capture_reference_id");
	define("CHARGE_TOTAL", "charge_total");
	define("CHARGE_TYPE", "charge_type");

	// *******************
	// Transaction Types
	// *******************
	define("CREDIT_CARD", "CREDIT_CARD");
	define("DEBIT_CARD", "DEBIT_CARD");
	define("BATCH", "BATCH");
	define("RECURRING", "RECURRING");
	define("AUTHENTICATION", "AUTHENTICATION");
	define("ACH", "ACH");
    define("ACH_BATCH", "ACH_BATCH");
    define("GIFT_CARD", "GIFT");
    
    

	// ********************
	// Charge Types
	// ********************	
	define("CREDIT", "CREDIT");  											// Ach
	define("DEBIT", "DEBIT");			
	define("ADJUSTMENT", "ADJUSTMENT");								// Credit Card
	define("AUTH", "AUTH");
	define("CANCEL_ORDER", "CANCEL_ORDER");
	define("CAPTURE", "CAPTURE");
	define("CLOSE_ORDER", "CLOSE_ORDER");
	define("CREATE_ORDER", "CREATE_ORDER");
	define("FORCE_AUTH", "FORCE_AUTH");
	define("FORCE_SALE", "FORCE_SALE");
	define("QUERY", "QUERY");
	define("QUERY_CREDIT", "QUERY_CREDIT");
	define("QUERY_PAYMENT", "QUERY_PAYMENT");
	define("SALE", "SALE");	
	define("VOID", "VOID");
	define("VOID_AUTH", "VOID_AUTH");
	define("VOID_CAPTURE", "VOID_CAPTURE");
	define("VOID_CREDIT", "VOID_CREDIT");	
	define("BALANCE_INQUIRY", "BALANCE_INQUIRY");			//Debit Card
	define("PURCHASE", "PURCHASE");
	define("QUERY_PURCHASE", "QUERY_PURCHASE");
	define("QUERY_REFUND", "QUERY_REFUND");
	define("REFUND", "REFUND");
	define("VOUCHER_CLEAR", "VOUCHER_CLEAR");	
	define("SETTLE", "SETTLE");												//Batch
	define("PURGE", "PURGE");
	define("TOTALS", "TOTALS");
	define("REVERSE_PURCHASE", "REVERSE_PURCHASE");
	define("REVERSE_REFUND", "REVERSE_REFUND");
    define("DELETE_CUSTOMER", "DELETE_CUSTOMER");              // RECURRING_COMMAND
    // Gift card charge types
    define("RELOAD", "RELOAD");
    define("ISSUE", "ISSUE");
    define("ISSUE_VIRTUAL", "ISSUE_VIRTUAL");
    define("ACTIVATE", "ACTIVATE");
    define("MERCHANDISE_RETURN", "MERCHANDISE_RETURN");
    define("REDEEM", "REDEEM");
    define("TIP", "TIP");
    define("CASH_OUT", "CASH_OUT");
    define("CANCEL", "CANCEL");
    define("REVERSE", "REVERSE");

	// *****************************
  // Transaction Condition Codes
	// *****************************
	define("TCC_DEFAULT" , 0);
	define("TCC_CARDHOLDER_NOT_PRESENT_MAIL_FAX_ORDER" , 1);
	define("TCC_CARDHOLDER_NOT_PRESENT_TELEPHONE_ORDER", 2);
	define("TCC_CARDHOLDER_NOT_PRESENT_INSTALLMENT", 3);
	define("TCC_CARDHOLDER_NOT_PRESENT_PAYER_AUTHENTICATION", 4);
	define("TCC_CARDHOLDER_NOT_PRESENT_SECURE_ECOMMERCE", 5);
	define("TCC_CARDHOLDER_NOT_PRESENT_RECURRING_BILLING", 6);
	define("TCC_CARDHOLDER_PRESENT_RETAIL_ORDER", 7);
	define("TCC_CARDHOLDER_PRESENT_RETAIL_ORDER_WITHOUT_SIGNATURE", 8);
	define("TCC_CARDHOLDER_PRESENT_RETAIL_ORDER_KEYED", 9);
	define("TCC_CARDHOLDER_NOT_PRESENT_PAYER_AUTHENTICATION_ATTEMPTED", 10);
	define("ACH_PREARRANGED_PAYMENT_AND_DEPOSIT", 50);
	define("ACH_TELEPHONE", 51);
	define("ACH_WEB", 52);
	define("ACH_CASH_CONCENTRATION_OR_DISBURSEMENT_ORDER", 53);
	define("TCC_DEBIT_CARDHOLDER_PRESENT_SWIPED_PIN_SUPPLIED", 60 );
	define("TCC_DEBIT_CARDHOLDER_PRESENT_KEYED_PIN_SUPPLIED", 61 );

	// **************
	// Account Types
	// **************
	define("SAVINGS", 0);
	define("CHECKING", 1);
	define("DEFAULT", 2 );
	define("CASH_BENEFIT", 3 );
	define("FOOD_STAMP", 4 );

	// ********************
	// ACH ACCOUNT_CLASS
	// ********************
	define("PERSONAL", 0);
	define("CORPORATE", 1);

	// *************************
	// Duplicate check values
	// *************************
	define("CHECK", "CHECK");
	define("OVERRIDE", "OVERRIDE");
	define("NO_CHECK", "NO_CHECK");

	// ***************************
	// Industries
	// ***************************
	define("DIRECT_MARKETING", "DIRECT_MARKETING");
	define("RETAIL", "RETAIL");
	define("LODGING", "LODGING");
	define("RESTAURANT", "RESTAURANT");

	// ********************
	// Credit card brands
	// ********************
	define("VISA", "VISA");
	define("MASTERCARD", "MASTERCARD");
	define("DISCOVER", "DISCOVER");
	define("NOVA", "NOVA");
	define("AMEX", "AMEX");
	define("DINERS", "DINERS");
	define("EUROCARD", "EUROCARD");

	// *******************
	// Billing info
	// *******************	
	define("BILL_COMPANY_NAME", "bill_company_name");
	define("BILL_CUSTOMER_TITLE", "bill_customer_title");
	define("BILL_FIRST_NAME", "bill_first_name");
	define("BILL_MIDDLE_NAME", "bill_middle_name");
	define("BILL_LAST_NAME", "bill_last_name");
	define("BILL_COMPANY", "bill_company");
	define("BILL_ADDRESS_ONE", "bill_address_one");
	define("BILL_ADDRESS_TWO", "bill_address_two");
	define("BILL_CITY", "bill_city");
	define("BILL_STATE_OR_PROVINCE", "bill_state_or_province");
	define("BILL_ZIP_OR_POSTAL_CODE", "bill_postal_code");
	define("BILL_COUNTRY_CODE", "bill_country_code");
	define("BILL_EMAIL", "bill_email");
	define("BILL_PHONE", "bill_phone");
	define("BILL_FAX", "bill_fax");
	define("BILL_NOTE", "bill_note");
	
	// *******************
	// Shipping info
	// *******************
	define("SHIP_COMPANY_NAME", "ship_company_name");
	define("SHIP_CUSTOMER_TITLE", "ship_customer_title");
	define("SHIP_FIRST_NAME", "ship_first_name");
	define("SHIP_MIDDLE_NAME", "ship_middle_name");
	define("SHIP_LAST_NAME", "ship_last_name");
	define("SHIP_COMPANY", "ship_company");
	define("SHIP_ADDRESS_ONE", "ship_address_one");
	define("SHIP_ADDRESS_TWO", "ship_address_two");
	define("SHIP_CITY", "ship_city");
	define("SHIP_STATE_OR_PROVINCE", "ship_state_or_province");
	define("SHIP_ZIP_OR_POSTAL_CODE", "ship_postal_code");
	define("SHIP_POSTAL_CODE", "ship_postal_code");
	define("SHIP_COUNTRY_CODE", "ship_country_code");
	define("SHIP_EMAIL", "ship_email");
	define("SHIP_PHONE", "ship_phone");
	define("SHIP_FAX", "ship_fax");
	define("SHIP_NOTE", "ship_note");

	// ********************************
	// Credit Card Requests
	// ********************************
	define("CREDIT_CARD_NUMBER", "credit_card_number");
	define("EXPIRE_MONTH", "expire_month");
	define("EXPIRE_YEAR", "expire_year");
	define("CREDIT_CARD_VERIFICATION_NUMBER", "credit_card_verification_number");
	define("ECOMMERCE_INDICATOR", "ecommerce_indicator");
	define("CARD_BRAND", "card_brand");
	define("CC_REFERENCE_ID", "reference_id");
	define("ORDER_DESCRIPTION", "order_description");
	define("TAX_AMOUNT", "tax_amount");
	define("SHIPPING_CHARGE", "shipping_charge");
	define("CARTRIDGE_TYPE", "cartridge_type");
	define("PO_NUMBER", "purchase_order_number");
	define("CURRENCY", "currency");
	define("PURCHASE_ORDER_NUMBER", "purchase_order_number");
	define("CAVV", "cavv");
	define("XID", "x_id");
	define("CUSTOMER_IP_ADDRESS", "customer_ip_address");
	define("ORDER_CUSTOMER_ID", "order_customer_id");
	define("STATE_TAX", "state_tax");
	define("TRACK1", "track1");
	define("TRACK2", "track2");
	define("TAX_EXEMPT", "tax_exempt");
	define("BUYER_CODE", "buyer_code");
	define("BANK_APPROVAL_CODE", "bank_approval_code");
	define("PARTIAL_APPROVAL_FLAG", "partial_approval_flag");

	// ***************************
	// Credit Card Service Fields
	// ***************************
	define("FOLIO_NUMBER", "folio_number");
	define("INDUSTRY", "industry");
	define("CHARGE_TOTAL_INCLUDES_RESTAURANT", "charge_total_incl_restaurant");
	define("CHARGE_TOTAL_INCLUDES_GIFTSHOP", "charge_total_incl_giftshop");
	define("CHARGE_TOTAL_INCLUDES_MINIBAR", "charge_total_incl_minibar");
	define("CHARGE_TOTAL_INCLUDES_PHONE", "charge_total_incl_phone");
	define("CHARGE_TOTAL_INCLUDES_LAUNDRY", "charge_total_incl_laundry");
	define("CHARGE_TOTAL_INCLUDES_OTHER", "charge_total_incl_other");
	define("SERVICE_RATE", "service_rate");
	define("SERVICE_START_YEAR", "service_start_year");
	define("SERVICE_START_MONTH", "service_start_month");
	define("SERVICE_START_DAY", "service_start_day");
	define("SERVICE_END_YEAR", "service_end_year");
	define("SERVICE_END_MONTH", "service_end_month");
	define("SERVICE_END_DAY", "service_end_day");
	define("SERVICE_NO_SHOW", "service_no_show");

	// ********************
	// Credit Card Queries
	// ********************
	define("AVS_CODE", "avs_code");
	define("CREDIT_CARD_VERIFICATION_RESPONSE", "credit_card_verification_response");
	define("STATE", "state");
	define("AUTHORIZED_AMOUNT", "authorized_amount");
	define("ORIGINAL_AUTHORIZED_AMOUNT", "original_authorized_amount");
	define("CAPTURED_AMOUNT", "captured_amount");
	define("CREDITED_AMOUNT", "credited_amount");
	define("TIME_STAMP_CREATED", "time_stamp_created");
	define("REQUESTED_AMOUNT", "requested_amount");

	// ********************
	// Debit Card Requests
	// ********************
	define("ACCOUNT_TYPE", "account_type");
	define("PRIMARY_ACCOUNT_NUMBER", "primary_account_number");
	define("PERSONAL_IDENTIFICATION_NUMBER", "personal_identification_number");
	define("KEY_SERIAL_NUMBER", "key_serial_number");
	define("CASH_BACK_AMOUNT", "cash_back_amount");
	define("VOUCHER_NUMBER", "voucher_number");
	define("DEVICE_TERMINAL_ID", "device_terminal_id");

	// ********************
	// Debit Card Queries
	// ********************
	define("PURCHASED_AMOUNT", "purchased_amount");
	define("REFUNDED_AMOUNT", "refunded_amount");
	define("CASH_BENEFIT_BALANCE", "cash_benefit_balance");
	define("FOOD_STAMP_BALANCE", "food_stamp_balance");
	define("DEBIT_RECEIPT_NUMBER", "debit_receipt_number");
	define("REVERSAL_STATUS", "reversal_status");
    
	// ****************************
	// Debit Card Reversal Statuses
	// ****************************
	define("REVERSAL_NOT_REQUIRED", "reversal_status");
	define("REVERSAL_REQUIRED", "reversal_required");
	define("REVERSAL_FAILED", "reversal_failed");
	define("REVERSAL_SUCCEEDED", "reversal_succeeded");
    
     // ********************
    // Gift Card Requests   constants
    // ********************
    define("TRANSACTION_ID", "transaction_id");
    define("PREVIOUS_CHARGE_TYPE", "previous_charge_type");
 
    
     // ********************
    // Gift Card Response constants
    // ********************   
    define("APPROVED_AMOUNT", "approved_amount");
    define("REMAINING_BALANCE", "remaining_balance");
    define("LOCAL_TIMESTAMP", "local_timestamp");
    define("ACCOUNT_CURRENCY", "account_currency");
    define("TRANSACTION_STATE", "transaction_state");
    define("PAN", "primary_account_number");                                      // both res and req
   

	// ******************************
	// ACH Requests and Responses
	// ******************************
	define("ACCOUNT_NUMBER", "account_number");
	define("ACCOUNT_CLASS", "account_class");
	define("BILL_BIRTH_DAY", "bill_birth_day");
	define("BILL_BIRTH_MONTH", "bill_birth_month");
	define("BILL_BIRTH_YEAR", "bill_birth_year");
	define("BILL_DRIVER_LICENSE_NUMBER", "bill_driver_license_number");
	define("BILL_DRIVER_LICENSE_STATE_CODE", "bill_driver_license_state_code");
	define("BILL_DRIVER_LICENSE_SWIPE", "bill_driver_license_swipe");
	define("BILL_SOCIAL_SECURITY_NUMBER", "bill_social_security_number");
	define("CHECK_NUMBER", "check_number");
	define("CLERK_ID", "clerk_id");
	define("ORIGINAL_REFERENCE_ID", "original_reference_id");
	define("ACH_REFERENCE_ID", "reference_id"); 
	define("ROUTING_NUMBER", "routing_number");
	define("NUMBER_OF_DEBITS", "number_of_debits");
	define("DEBIT_TOTAL", "debit_total");
	define("NUMBER_OF_VOIDS", "number_of_voids");
	define("VOID_TOTAL", "void_total");
	define("NUMBER_OF_DECLINES", "number_of_declines");
	define("DECLINE_TOTAL", "decline_total");
	define("TRANSACTION_STATUS", "transaction_status");
	define("IS_RESUBMITTABLE", "is_resubmittable");
	define("RETURN_REASON_CODE", "return_reason_code");
	define("RETURN_REASON_TEXT", "return_reason_text");

	// ***************************************************
	// Constants for recurring requests
	// ***************************************************
	define("ADD_CUSTOMER", "ADD_CUSTOMER");
	define("ADD_RECURRENCE", "ADD_RECURRENCE");
	define("ADD_CUSTOMER_AND_RECURRENCE", "ADD_CUSTOMER_AND_RECURRENCE");
	define("RECUR_COMMAND", "command");
	define("RECUR_CUSTOMER_ID", "customer_id");
	define("RECUR_ACCOUNT_TYPE", "account_type");
	define("RECUR_CUSTOMER_NAME", "customer_name");
	define("RECUR_EMAIL_ADDRESS", "email_address");
	define("RECUR_CREDIT_CARD_NUMBER", "credit_card_number");
	define("RECUR_EXPIRE_YEAR", "expire_year");
	define("RECUR_EXPIRE_MONTH", "expire_month");
	define("RECUR_BILLING_ADDRESS", "billing_address");
	define("RECUR_POSTAL_CODE", "postal_code");
	define("RECUR_COUNTRY_CODE", "country_code");
	define("RECUR_RECURRENCE_ID", "recurrence_id");
	define("RECUR_DESCRIPTION", "description");
	define("RECUR_CHARGE_TOTAL", "charge_total");
	define("RECUR_NOTIFY_CUSTOMER", "notify_customer");
	define("RECUR_PERIOD", "period");
	define("RECUR_NUMBER_OF_RETRIES", "number_of_retries");
	define("RECUR_START_DAY", "start_day");
	define("RECUR_START_MONTH", "start_month");
	define("RECUR_START_YEAR", "start_year");
	define("RECUR_END_DAY", "end_day");
	define("RECUR_END_MONTH", "end_month");
	define("RECUR_END_YEAR", "end_year");
	define("PERIOD_WEEKLY", 1);
	define("PERIOD_BIWEEKLY", 2);
	define("PERIOD_SEMIMONTHLY", 3);
	define("PERIOD_MONTHLY", 4);
	define("PERIOD_QUARTERLY", 5);
	define("PERIOD_ANNUAL", 6);

	// *********************************************
	// Constants for Payer Authentication Requests
	// *********************************************
	define("AUTHENTICATION_ACTION", "action");
	define("MAF_PASSWORD",  "maf_password");
	define("BROWSER_HEADER",  "browser_header");
	define("USER_AGENT",  "user_agent");
	define("IS_RECURRING",  "is_recurring");
	define("RECURRING_PERIOD",  "recurrence_period");
	define("RECURRING_END_DAY", "recurrence_end_day");
	define("RECURRING_END_MONTH",  "recurrence_end_month");
	define("RECURRING_END_YEAR",  "recurrence_end_year");
	define("INSTALLMENT",  "installment");
	define("LOOKUP", "LOOKUP");
	define("AUTHENTICATE","AUTHENTICATE");
	define("AUTHENTICATION_TRANSACTION_ID", "authentication_transaction_id");
	define("AUTHENTICATION_PAYLOAD", "authentication_payload");
	define("AUTHENTICATION_INCONCLUSIVE", "success_on_authentication_inconclusive");
	define("AUTHENTICATION_RESPONSE_CODE","authentication_response_code");
	define("AUTHENTICATION_RESPONSE_CODE_TEXT","authentication_response_code_text");
	define("AUTHENTICATION_TIME_STAMP","authentication_time_stamp");
	define("AUTHENTICATION_RETRY_RECOMMENDED","authentication_retry_recommended");
	define("AUTHENTICATION_CAVV","authentication_cavv");
	define("AUTHENTICATION_X_ID","authentication_x_id");
	define("AUTHENTICATION_STATUS","authentication_status");
	define("AUTHENTICATION_TRANSACTION_CONDITION_CODE","authentication_transaction_condition_code");
	define("LOOKUP_PAYLOAD",  "lookup_payload");
	define("HIDDEN_FIELDS",  "hidden_fields");
	define("STATUS",  "status");
	define("AUTHENTICATION_URL",  "authentication_url");
	define("STATUS_ENROLLED" , "Y");
	define("STATUS_NOT_ENROLLED", "N");
	define("STATUS_ENROLLED_BUT_AUTHENTICATION_UNAVAILABLE", "U");

	// ********************
	// Batch Transactions
	// ********************
	define("BATCH_ID", "batch_id");
	define("PAYMENT_TOTAL", "payment_total");
	define("CREDIT_TOTAL", "credit_total");
	define("NUMBER_OF_PAYMENTS", "number_of_payments");
	define("NUMBER_OF_CREDITS", "number_of_credits");
	define("BATCH_STATE", "batch_state");
	define("BATCH_BALANCE_STATE", "batch_balance_state");
	define("ACTION", "action");
	define("AMOUNT", "amount");
	define("BATCH_OPENED_TIME_STAMP", "batch_opened_time_stamp");
	define("BATCH_CLOSED_TIME_STAMP", "batch_closed_time_stamp");

	// *************************************
	// Response Constants
	// *************************************
	define("RESPONSE_CODE", "response_code");
	define("SECONDARY_RESPONSE_CODE", "secondary_response_code");
	define("RESPONSE_CODE_TEXT", "response_code_text");
	define("TIME_STAMP", "time_stamp");
	define("RETRY_RECOMMENDED", "retry_recommended");
	define("ISO_CODE", "iso_code");
	define("BANK_TRANSACTION_ID", "bank_transaction_id");
	
	//define("REMAINING_BALANCE", "remaining_balance");

	// ******************
	// Response Codes
	// ******************
	define("RC_SUCCESSFUL_TRANSACTION", 1);
	define("RC_CREDIT_CARD_DECLINED", 100);
	define("RC_TRANSACTION_NOT_POSSIBLE", 6);
	define("RC_ILLEGAL_TRANSACTION_REQUEST", 4);
	define("RC_MISSING_REQUIRED_REQUEST_FIELD", 2);
	define("RC_MISSING_REQUIRED_RESPONSE_FIELD", 8);
	define("RC_INVALID_REQUEST_FIELD", 3);
	define("RC_INVALID_RESPONSE_FIELD", 9);
	define("RC_TRANSACTION_CLIENT_ERROR", 10);
	define("RC_PAYMENT_ENGINE_ERROR", 102);
	define("RC_ACQUIRER_GATEWAY_ERROR", 101);
	define("RC_TRANSACTION_SERVER_ERROR", 5);
	define("RC_INVALID_VERSION", 7);

	//****************************
	// Managed Payer Data
	//*******************************
	// request
	define("SECURE_PRIMARY_ACCOUNT_NUMBER", "span");
	define("MANAGE_PAYER_DATA", "manage_payer_data");
    define("PAYER_IDENTIFIER", "payer_identifier");

    // response
    define("MANAGED_PAYER_DATA_RESPONSE_CODE", "mpd_response_code");
    define("MANAGED_PAYER_DATA_RESPONSE_CODE_TEXT", "mpd_response_code_text");
    define("PAYER_DATA_MANAGED_UNTIL_DATE", "manage_until");

?>
