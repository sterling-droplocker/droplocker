<?php
/**
 *	@package Paygateway
 */
class GiftCardRequest extends TransactionRequestBase {

	function GiftCardRequest() {
	}

	function doTransaction() {
		$this->setTransactionType(GIFT_CARD);
		return $this->executeTransaction();
	}

	/************************************************************
	 ************************************************************
	 *  Setter functions for Gift request fields
	 ************************************************************
	 ************************************************************/
        function setTransactionID($argTransactionID) {
            $this->setProperty(TRANSACTION_ID, $argTransactionID);
            $this->clearError();
            return true;
        }
        
        function setDeviceTerminalID($argDeviceTerminalID) {
             $this->setProperty(DEVICE_TERMINAL_ID, $argDeviceTerminalID);
             $this->clearError();
             return true;
        }
  
        function setChargeType($argChargeType) {
            $this->setProperty(CHARGE_TYPE, $argChargeType);
            $this->clearError();
            return true;
        }
        
        function setPreviousChargeType($argPreviousChargeType) {
            $this->setProperty(PREVIOUS_CHARGE_TYPE, $argPreviousChargeType);
            $this->clearError();
            return true;
        }
       
        function setPan($argPan) {
            $this->setProperty(PAN, $argPan);
            $this->clearError();
            return true;
        }
        
        function setPersonalIdentificationNumber($argPin) {
            $this->setProperty(PERSONAL_IDENTIFICATION_NUMBER, $argPin);
            $this->clearError();
            return true;
        }
		
		function setChargeTotal($argChargeTotal) {
			$this->setProperty(CHARGE_TOTAL, $argChargeTotal);
			$this->clearError();
			return true;
		}
		
		function setOrderID($argOrderId) {
			$this->setProperty(ORDER_ID, $argOrderId);
			$this->clearError();
			return true;
		}

		function setTrack1($argTrack1) {
			$this->setProperty(TRACK1, $argTrack1);
			$this->clearError();
			return true;
		}

		function setTrack2($argTrack2) {
			$this->setProperty(TRACK2, $argTrack2);
			$this->clearError();
			return true;
		}

		function setPurchaseOrderNumber($argPurchaseOrderNumber) {
			$this->setProperty(PO_NUMBER, $argPurchaseOrderNumber);
			$this->clearError();
			return true;
		}

    /************************************************************
     ************************************************************
     *  Setter functions for Gift request fields
     ************************************************************/
        function getTransactionID() {
            return $this->getProperty(TRANSACTION_ID);
        }
        
        function getDeviceTerminalID() {
            return $this->getProperty(DEVICE_TERMINAL_ID);
        }

        function getPan() {
            return $this->getProperty(PAN);
        }
        
        function getPersonalIdentificationNumber() {
            return $this->getProperty(PERSONAL_IDENTIFICATION_NUMBER);
        }
		
		function getChargeTotal() {
			return $this->getProperty(CHARGE_TOTAL);
		}
		
		function getOrderID() {
			return $this->getProperty(ORDER_ID);
		}

		function getTrack1() {
			return $this->getProperty(TRACK1);
		}

		function getTrack2() {
			return $this->getProperty(TRACK2);
		}

		function getPurchaseOrderNumber() {
			return $this->getProperty(PO_NUMBER);
		}

}
?>
