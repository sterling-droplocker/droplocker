<html>
<head>
   <title>Demo Store Sale Results</title>
   <link rel="STYLESHEET" type="text/css" href="main.css">
</head>
<body bgcolor="#FFFFFF" text="#000000">

<?php
	include("Paygateway.php");

	// enter your token here
	define("TEST_TOKEN", "---Token Here---");

	$errorMessages = array();

	$creditCardRequest = new TransactionRequest();

	$creditCardRequest->setAccountToken(TEST_TOKEN);


	// Populate request with data from web form
	$buyerCode = $_POST["buyer_code"];
	if($buyerCode != "") {
		if(!$creditCardRequest->setBuyerCode($buyerCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$invoiceNumber = $_POST["invoice_number"];
	if($invoiceNumber != "") {
		if(!$creditCardRequest->setInvoiceNumber($invoiceNumber)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}
	$billAddressOne = $_POST["bill_address_one"];
	if($billAddressOne != "") {
		if(!$creditCardRequest->setBillAddressOne($billAddressOne)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billAddressTwo = $_POST["bill_address_two"];
	if($billAddressTwo != "") {
		if(!$creditCardRequest->setBillAddressTwo($billAddressTwo))	{
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billCity = $_POST["bill_city"];
	if($billCity != "") {
		if(!$creditCardRequest->setBillCity($billCity)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billCompany = $_POST["bill_company"];
	if($billCompany != "") {
		if(!$creditCardRequest->setBillCompany($billCompany)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billCountryCode = $_POST["bill_country_code"];
	if($billCountryCode != "") {
		if(!$creditCardRequest->setBillCountryCode($billCountryCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billCustomerTitle = $_POST["bill_customer_title"];
	if($billCustomerTitle != "") {
		if(!$creditCardRequest->setBillCustomerTitle($billCustomerTitle)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billEmail = $_POST["bill_email"];
	if($billEmail != "") {
		if(!$creditCardRequest->setBillEmail($billEmail)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billFax = $_POST["bill_fax"];
	if($billFax != "") {
		if(!$creditCardRequest->setBillFax($billFax)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billFirstName = $_POST["bill_first_name"];
	if($billFirstName != "") {
		if(!$creditCardRequest->setBillFirstName($billFirstName)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billLastName = $_POST["bill_last_name"];
	if($billLastName != "") {
		if(!$creditCardRequest->setBillLastName($billLastName)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billMiddleName = $_POST["bill_middle_name"];
	if($billMiddleName != "") {
		if(!$creditCardRequest->setBillMiddleName($billMiddleName)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billNote = $_POST["bill_note"];
	if($billNote != "") {
		if(!$creditCardRequest->setBillNote($billNote)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billPhone = $_POST["bill_phone"];
	if($billPhone != "") {
		if(!$creditCardRequest->setBillPhone($billPhone)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billZipOrPostalCode = $_POST["bill_zip_or_postal_code"];
	if($billZipOrPostalCode != "") {
		if(!$creditCardRequest->setBillZipOrPostalCode($billZipOrPostalCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$billStateOrProvince = $_POST["bill_state_or_province"];
	if($billStateOrProvince != "") {
		if(!$creditCardRequest->setBillStateOrProvince($billStateOrProvince)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$cardBrand = $_POST["card_brand"];
	if($cardBrand != "") {
		if(!$creditCardRequest->setCardBrand($cardBrand)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$chargeTotal = $_POST["charge_total"];
	if($chargeTotal != "") {
		if(!$creditCardRequest->setChargeTotal($chargeTotal)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$chargeType = $_POST["charge_type"];
	if ($chargeType != "") {
		if (!$creditCardRequest->setChargeType($chargeType)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$partialApprovalFlag = $_POST["partial_auth"];
	if ($partialApprovalFlag != "") {
		if (!$creditCardRequest->setPartialApprovalFlag($partialApprovalFlag)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$creditCardNumber = $_POST["credit_card_number"];
	if($creditCardNumber != "") {
		if(!$creditCardRequest->setCreditCardNumber($creditCardNumber)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$creditCardVerificationNumber = $_POST["credit_card_verification_number"];
	if($creditCardVerificationNumber != "") {
		if(!$creditCardRequest->setCreditCardVerificationNumber($creditCardVerificationNumber)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$duplicateCheck = $_POST["duplicate_check"];
	if($duplicateCheck != "") {
		if(!$creditCardRequest->setDuplicateCheck($duplicateCheck)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$purchaseOrderNumber = $_POST["purchase_order_number"];
	if($purchaseOrderNumber != "") {
		if(!$creditCardRequest->setPurchaseOrderNumber($purchaseOrderNumber)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$expireMonth = $_POST["expire_month"];
	if($expireMonth != "") {
		if(!$creditCardRequest->setExpireMonth($expireMonth)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$expireYear = $_POST["expire_year"];
	if($expireYear != "") {
		if(!$creditCardRequest->setExpireYear($expireYear)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$orderDescription = $_POST["order_description"];
	if($orderDescription != "") {
		if(!$creditCardRequest->setOrderDescription($orderDescription)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$orderID = $_POST["order_id"];
	if($orderID != "") {
		if(!$creditCardRequest->setOrderID($orderID)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$orderUserID = $_POST["order_user_id"];
	if($orderUserID != "") {
		if(!$creditCardRequest->setOrderUserID($orderUserID)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipAddressOne = $_POST["ship_address_one"];
	if($shipAddressOne != "") {
		if(!$creditCardRequest->setShipAddressOne($shipAddressOne)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipAddressTwo = $_POST["ship_address_two"];
	if($shipAddressTwo != "") {
		if(!$creditCardRequest->setShipAddressTwo($shipAddressTwo))	{
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipCity = $_POST["ship_city"];
	if($shipCity != "") {
		if(!$creditCardRequest->setShipCity($shipCity)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipCompany = $_POST["ship_company"];
	if($shipCompany != "") {
		if(!$creditCardRequest->setShipCompany($shipCompany)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipCountryCode = $_POST["ship_country_code"];
	if($shipCountryCode != "") {
		if(!$creditCardRequest->setShipCountryCode($shipCountryCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipCustomerTitle = $_POST["ship_customer_title"];
	if($shipCustomerTitle != "") {
		if(!$creditCardRequest->setShipCustomerTitle($shipCustomerTitle)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipEmail = $_POST["ship_email"];
	if($shipEmail != "") {
		if(!$creditCardRequest->setShipEmail($shipEmail)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipFax = $_POST["ship_fax"];
	if($shipFax != "") {
		if(!$creditCardRequest->setShipFax($shipFax)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipFirstName = $_POST["ship_first_name"];
	if($shipFirstName != "") {
		if(!$creditCardRequest->setShipFirstName($shipFirstName)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipLastName = $_POST["ship_last_name"];
	if($shipLastName != "") {
		if(!$creditCardRequest->setShipLastName($shipLastName)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipMiddleName = $_POST["ship_middle_name"];
	if($shipMiddleName != "") {
		if(!$creditCardRequest->setShipMiddleName($shipMiddleName)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipNote = $_POST["ship_note"];
	if($shipNote != "") {
		if(!$creditCardRequest->setShipNote($shipNote)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipPhone = $_POST["ship_phone"];
	if($shipPhone != "") {
		if(!$creditCardRequest->setShipPhone($shipPhone)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipZipOrPostalCode = $_POST["ship_zip_or_postal_code"];
	if($shipZipOrPostalCode != "") {
		if(!$creditCardRequest->setShipZipOrPostalCode($shipZipOrPostalCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shipStateOrProvince = $_POST["ship_state_or_province"];
	if($shipStateOrProvince != "") {
		if(!$creditCardRequest->setShipStateOrProvince($shipStateOrProvince)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$shippingCharge = $_POST["shipping_charge"];
	if($shippingCharge != "") {
		if(!$creditCardRequest->setShippingCharge($shippingCharge)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$taxAmount = $_POST["tax_amount"];
	if($taxAmount != "") {
		if(!$creditCardRequest->setTaxAmount($taxAmount)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$transactionConditionCode = $_POST["transaction_condition_code"];
	if($transactionConditionCode != "") {
		if(!$creditCardRequest->setTransactionConditionCode($transactionConditionCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$taxExempt = $_POST["tax_exempt"];
	if ($taxExempt != "") {
		if (!$creditCardRequest->setTaxExempt(($taxExempt == "true"))) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$stateTax = $_POST["state_tax"];
	if ($stateTax != "") {
		if (!$creditCardRequest->setStateTax($stateTax)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$bankApprovalCode = $_POST["bank_approval_code"];
	if ($bankApprovalCode != "") {
		if (!$creditCardRequest->setBankApprovalCode($bankApprovalCode)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$referenceID = $_POST["reference_id"];
	if ($referenceID != "") {
		if (!$creditCardRequest->setReferenceID($referenceID)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}


	// optional service details
	$folioNumber = $_POST["folio_number"];
	if ($folioNumber != "") {
		if (!$creditCardRequest->setFolioNumber($folioNumber)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}


	$industry = $_POST["industry"];
	if ($industry != "") {
		if (!$creditCardRequest->setIndustry($industry)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}


	$chargeTotalIncludesRestaurant = $_POST["charge_total_incl_restaurant"];
	if ($chargeTotalIncludesRestaurant != "") {
		if (!$creditCardRequest->setChargeTotalIncludesRestaurant($chargeTotalIncludesRestaurant)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$chargeTotalIncludesGiftshop = $_POST["charge_total_incl_giftshop"];
	if ($chargeTotalIncludesGiftshop != "") {
		if (!$creditCardRequest->setChargeTotalIncludesGiftshop($chargeTotalIncludesGiftshop)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$chargeTotalIncludesMinibar = $_POST["charge_total_incl_minibar"];
	if ($chargeTotalIncludesMinibar != "") {
		if (!$creditCardRequest->setChargeTotalIncludesMinibar($chargeTotalIncludesMinibar)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

		$chargeTotalIncludesPhone = $_POST["charge_total_incl_phone"];
	if ($chargeTotalIncludesPhone != "") {
		if (!$creditCardRequest->setChargeTotalIncludesPhone($chargeTotalIncludesPhone)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$chargeTotalIncludesLaundry = $_POST["charge_total_incl_laundry"];
	if ($chargeTotalIncludesLaundry != "") {
		if (!$creditCardRequest->setChargeTotalIncludesLaundry($chargeTotalIncludesLaundry)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$chargeTotalIncludesOther = $_POST["charge_total_incl_other"];
	if ($chargeTotalIncludesOther != "") {
		if (!$creditCardRequest->setChargeTotalIncludesOther($chargeTotalIncludesOther)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceRate = $_POST["service_rate"];
	if ($serviceRate != "") {
		if (!$creditCardRequest->setServiceRate($serviceRate)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceStartDay = $_POST["service_start_day"];
	if ($serviceStartDay != "") {
		if (!$creditCardRequest->setServiceStartDay($serviceStartDay)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceStartMonth = $_POST["service_start_month"];
	if ($serviceStartMonth != "") {
		if (!$creditCardRequest->setServiceStartMonth($serviceStartMonth)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceStartYear = $_POST["service_start_year"];
	if ($serviceStartYear != "") {
		if (!$creditCardRequest->setServiceStartYear($serviceStartYear)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceEndDay = $_POST["service_end_day"];
	if ($serviceEndDay != "") {
		if (!$creditCardRequest->setServiceEndDay($serviceEndDay)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceEndMonth = $_POST["service_end_month"];
	if ($serviceEndMonth != "") {
		if (!$creditCardRequest->setServiceEndMonth($serviceEndMonth)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceEndYear = $_POST["service_end_year"];
	if ($serviceEndYear != "") {
		if (!$creditCardRequest->setServiceEndYear($serviceEndYear)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

	$serviceNoShow = $_POST["service_no_show"];
	if ($serviceNoShow != "") {
		if (!$creditCardRequest->setServiceNoShow($serviceNoShow)) {
			$errorMessages[] = $creditCardRequest->getError();
		}
	}

    $partial_approval_flag = $_POST["partial_approval_flag"];
    if ($partial_approval_flag != "") {
        if (!$creditCardRequest->setPartialApprovalFlag($partial_approval_flag)) {
            $errorMessages[] = $creditCardRequest->getError();
        }
    }
    $span = $_POST["span"];
    if ($span != "") {
        if (!$creditCardRequest->setSecurePrimaryAccountNumber($span)) {
            $errorMessages[] = $creditCardRequest->getError();
        }
    }
    $manage_payer_data = $_POST["manage_payer_data"];
    if ($manage_payer_data != "") {
        if (!$creditCardRequest->setManagePayerData($manage_payer_data)) {
            $errorMessages[] = $creditCardRequest->getError();
        }
    }
    $payer_identifier = $_POST["payer_identifier"];
    if ($payer_identifier != "") {
        if (!$creditCardRequest->setPayerIdentifier($payer_identifier)) {
            $errorMessages[] = $creditCardRequest->getError();
        }
    }

	if(sizeof($errorMessages) != 0) {
		// Print out all the errors that happened when setting.
		print("<h1><font color=red>Test Transaction Not Attempted</font></h1>");
		print("<p>There was an error setting the fields of the TransactionRequest object.");
		print("The following errors were found:");
		print("<ul>");

		foreach ($errorMessages as $error) {
			print("   <li>$error</li>");
		}

		print("</ul>");

	} else {
		// No errors setting the values; perform the transaction
		$creditCardResponse = $creditCardRequest->doTransaction();

		// If there was a communication failure, then the response
		// object will be false.
		if($creditCardResponse) {
			print("<br>");
			print("<table align='center' border = '0' cellspacing = '0' cellpadding = '0'>");
			print("  <tr>");
			print("    <td width='200' align='left' valign='top'>&nbsp;</td>");
			print("    <td align='center' valign='top'><h3><u>Transaction Results</u></h3></td>");
			print("    <td width='200' align='right' valign='top'><a href='demopaypage.html' class = 'header'><strong>Enter New Payment</strong></a></td>");
			print("  </tr>");
			print("</table>");

			print("<hr>");
			print("<center>");
			print("  <TABLE BORDER='0'>");
			print("    <TR ALIGN='CENTER'>"	);
			print("      <TD><b>Order User ID:</b>" . $creditCardRequest->GetOrderUserID() . "</TD>");
			print("      <TD><b>Order ID:</b>" . $creditCardResponse->GetOrderID() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");
			print("  <HR>");
			print("  <br>");
			print("  <TABLE CELLPADDING='2' CELLSPACING='2' BORDER='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Response Fields</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Response Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetResponseCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Response Text:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetResponseCodeText() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Timestamp:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetTimeStamp() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>AVS Response Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->getAVSCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Bank Approval Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetBankApprovalCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Bank Transaction ID:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetBankTransactionID() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Batch ID:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetBatchID() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Reference ID:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetReferenceID() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Credit Card Verification Response:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetCreditCardVerificationResponse() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>ISO Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetISOCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>State:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetState() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Requested Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetRequestedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' >");
			print("      <TH align='right' width='200' valign='top'>Authorized Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetAuthorizedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Original Authorized Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetOriginalAuthorizedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' >");
			print("      <TH align='right' width='200' valign='top'>Captured Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetCapturedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Credited Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetCreditedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Time Stamp Created:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetTimeStampCreated() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'  bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Payer Identifier:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->getPayerIdentifier() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Managed Payer Data Response Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->getManagedPayerDataResponseCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Managed Payer Data Response Code Text:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->getManagedPayerDataResponseCodeText() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Payer Data Managed Until Date:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->getPayerDataManagedUntilDate() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");

			print("  <p>&nbsp;</p>");
			print("  <table cellpadding='2' cellspacing='2' border='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Service Details</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200'>Folio Number:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getFolioNumber() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Industry:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . ucwords(strtolower(str_replace("_", " ", $creditCardRequest->getIndustry()))) . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Charge Total Includes Restaurant</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getChargeTotalIncludesRestaurant() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Charge Total Includes Gift Shop</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getChargeTotalIncludesGiftshop() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Charge Total Includes Minibar</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getChargeTotalIncludesMinibar() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Charge Total Includes Phone</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getChargeTotalIncludesPhone() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Charge Total Includes Laundry</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getChargeTotalIncludesLaundry() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Charge Total Includes Other</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getChargeTotalIncludesOther() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Service Rate:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getServiceRate() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Service Start Day:</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getServiceStartDay() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Service Start Month:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getServiceStartMonth() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Service Start Year:</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getServiceStartYear() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Service End Day:</th>");
			print("      <TD width='300' >&nbsp;" . $creditCardRequest->getServiceEndDay() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Service End Month:</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getServiceStartMonth() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Service End Year:</th>");
			print("      <TD width='300' >&nbsp;" . $creditCardRequest->getServiceStartYear() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Service No Show:</th>");
			print("      <TD width='300' bgcolor='#EEEEEE'>&nbsp;" . $creditCardRequest->getServiceNoShow() . "</td>");
			print("    </tr>");
			print("  </table>");

			print("  <p>&nbsp;</p>");
			print("  <table cellpadding='2' cellspacing='2' border='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Purchase Details</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200'>Charge Total:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetChargeTotal() . "</td>");
			print("    </tr>");

			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200' bgcolor='#EEEEEE'>Requested Amount:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardResponse->GetRequestedAmount() . "</td>");
			print("    </tr>");

			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200'>Authorized Amount:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetAuthorizedAmount() . "</td>");
			print("    </tr>");

			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200' bgcolor='#EEEEEE'>Captured Amount:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardResponse->GetCapturedAmount() . "</td>");
			print("    </tr>");

			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200'>Remaining Balance:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardResponse->GetRemainingBalance() . "</td>");
			print("    </tr>");

			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Tax Amount:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetTaxAmount() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th valign='top' align='right' width='200'>State Tax:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetStateTax() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Tax Exempt:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetTaxExempt() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Shipping Charge:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShippingCharge() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>Charge Type:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetChargeType() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Credit Card Number:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetCreditCardNumber() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th bgcolor='#EEEEEE' align='right' width='200' valign='top'>Expiry Date (MM/YYYY):</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetExpireMonth() . "/" . $creditCardRequest->GetExpireYear() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Duplicate Check:</th>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetDuplicateCheck() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th bgcolor='#EEEEEE' align='right' width='200' valign='top'>Details:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetOrderDescription() . "</td>");
			print("    </tr>");
			print("  </table>");
			print("  <p>&nbsp;</p>");
			print("  <TABLE CELLPADDING='2' CELLSPACING='2' BORDER='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Billing Information</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH valign='top' align='right' width='200'>Customer Title:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetBillCustomerTitle() . "</TD>");
			print("    </TR>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>First Name:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillFirstName() . "</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Last Name:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetBillLastName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Middle Name:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillMiddleName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Company:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetBillCompany() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Address One:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillAddressOne() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Address Two:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetBillAddressTwo() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>City:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillCity() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>State or Province:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetBillStateOrProvince() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Country Code:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillCountryCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Zip or Postal Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->getBillZipOrPostalCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Phone:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillPhone() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' valign='top'>Fax:</TH>");
			print("      <TD>&nbsp;" . $creditCardRequest->GetBillFax() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Email:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetBillEmail() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Note:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetBillNote() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");
			print("  <p>&nbsp;</p>");
			print("  <TABLE CELLPADDING='2' CELLSPACING='2' BORDER='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Shipping Information</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH valign='top' align='right' width='200'>Customer Title:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipCustomerTitle() . "</TD>");
			print("    </TR>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>First Name:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipFirstName() . "</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Last Name:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipLastName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Middle Name:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipMiddleName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Company:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipCompany() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Address One:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipAddressOne() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Address Two:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipAddressTwo() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>City:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipCity() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>State or Province:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipStateOrProvince() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Country Code:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipCountryCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Zip or Postal Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipZipOrPostalCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Phone:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipPhone() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' valign='top'>Fax:</TH>");
			print("      <TD>&nbsp;" . $creditCardRequest->GetShipFax() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Email:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $creditCardRequest->GetShipEmail() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Note:</TH>");
			print("      <TD width='300'>&nbsp;" . $creditCardRequest->GetShipNote() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");
			print("  <BR>");
			print("  <HR>");
			print("</center>");
		} else {
			print("<h1><font color=red>Test Transaction Failed</font></h1>A communication error occurred.");
			print("<p>Error: ". $creditCardRequest->getError());
		}
	}
?>

</body>
</html>

