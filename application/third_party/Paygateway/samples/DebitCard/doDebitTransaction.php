<html>
<head>
   <title>Demo Store Sale - Debit Card Results</title>
   <link rel="STYLESHEET" type="text/css" href="main.css">
</head>
<body bgcolor="#FFFFFF" text="#000000">

<?php
	include("Paygateway.php");

	// enter your token here
	define("TEST_TOKEN", "token");

	$errorMessages = array();
	$debitCardRequest = new DebitCardRequest();
	$debitCardRequest->setAccountToken(TEST_TOKEN);

	// Populate request with data from web form
	if(isset($_POST["order_id"])){
		$orderID = $_POST["order_id"];
		if(!$debitCardRequest->setOrderID($orderID)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["purchase_order_number"])){
		$purchaseOrderNumber = $_POST["purchase_order_number"];
		if(!$debitCardRequest->setPurchaseOrderNumber($purchaseOrderNumber)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["order_user_id"])){
		$orderUserID = $_POST["order_user_id"];
		if(!$debitCardRequest->setOrderUserID($orderUserID)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["invoice_number"])){
		$invoiceNumber = $_POST["invoice_number"];
		if(!$debitCardRequest->setInvoiceNumber($invoiceNumber)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bank_approval_code"])){
		$bankApprovalCode = $_POST["bank_approval_code"];
		if (!$debitCardRequest->setBankApprovalCode($bankApprovalCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["reference_id"])){
		$referenceID = $_POST["reference_id"];
		if (!$debitCardRequest->setReferenceID($referenceID)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["clerk_id"])){
		$clerkId = $_POST["clerk_id"];
		if (!$debitCardRequest->setClerkId($clerkId)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["track2"])){
		$track2 = $_POST["track2"];
		if (!$debitCardRequest->setTrack2($track2)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["primary_account_number"])){
		$primaryAccountNumber = $_POST["primary_account_number"];
		if (!$debitCardRequest->setPrimaryAccountNumber($primaryAccountNumber)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["expire_month"]) && $_POST["expire_month"] != ""){
		$expireMonth = $_POST["expire_month"];
		if(!$debitCardRequest->setExpireMonth($expireMonth)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["expire_year"]) && $_POST["expire_year"] != ""){
		$expireYear = $_POST["expire_year"];
		if(!$debitCardRequest->setExpireYear($expireYear)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	// Insert your own code here to interface with the PINPad to obtain Encrypted PIN and KSN
	if(isset($_POST["personal_identification_number"])){
		$encryptedPIN = $_POST["personal_identification_number"];
		if(!$debitCardRequest->setEncryptedPIN($encryptedPIN)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["key_serial_number"])){
		$keySerialNumber = $_POST["key_serial_number"];
		if(!$debitCardRequest->setKeySerialNumber($keySerialNumber)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["account_type"])){
		$accountType = $_POST["account_type"];
		if(!$debitCardRequest->setAccountType($accountType)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["device_terminal_id"])){
		$deviceTerminalId = $_POST["device_terminal_id"];
		if(!$debitCardRequest->setDeviceTerminalId($deviceTerminalId)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["duplicate_check"])){
		$duplicateCheck = $_POST["duplicate_check"];
		if(!$debitCardRequest->setDuplicateCheck($duplicateCheck)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["charge_total"])){
		$chargeTotal = $_POST["charge_total"];
		if(!$debitCardRequest->setChargeTotal($chargeTotal)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["charge_type"])){
		$chargeType = $_POST["charge_type"];
		switch ($chargeType) {
			case "PURCHASE":
				$chargeType = PURCHASE;
				break;
			case "REFUND":
				$chargeType = REFUND;
				break;
			case "BALANCE_INQUIRY":
				$chargeType = BALANCE_INQUIRY;
				break;
			case "VOUCHER_CLEAR":
				$chargeType = VOUCHER_CLEAR;
				break;
			case "QUERY_PURCHASE":
				$chargeType = QUERY_PURCHASE;
				break;
			case "QUERY_REFUND":
				$chargeType = QUERY_REFUND;
				break;
		}
		if (!$debitCardRequest->setChargeType($chargeType)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["tax_amount"]) && $_POST["tax_amount"] != ""){
		$taxAmount = $_POST["tax_amount"];
		if(!$debitCardRequest->setTaxAmount($taxAmount)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["shipping_charge"]) && $_POST["shipping_charge"] != ""){
		$shippingCharge = $_POST["shipping_charge"];
		if(!$debitCardRequest->setShippingCharge($shippingCharge)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["tax_exempt"])){
		$taxExempt = $_POST["tax_exempt"];
		if (!$debitCardRequest->setTaxExempt(($taxExempt == "true"))) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["cash_back_amount"]) && $_POST["cash_back_amount"] != ""){
		$cashBackAmount = $_POST["cash_back_amount"];
		if (!$debitCardRequest->setCashBackAmount($cashBackAmount)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["transaction_condition_code"]) && $_POST["transaction_condition_code"] != ""){
		$transactionConditionCode = $_POST["transaction_condition_code"];
		if(!$debitCardRequest->setTransactionConditionCode($transactionConditionCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["buyer_code"])){
		$buyerCode = $_POST["buyer_code"];
		if(!$debitCardRequest->setBuyerCode($buyerCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["order_description"])){
		$orderDescription = $_POST["order_description"];
		if(!$debitCardRequest->setOrderDescription($orderDescription)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["order_customer_id"])){
		$orderCustomerId = $_POST["order_customer_id"];
		if(!$debitCardRequest->setOrderCustomerId($orderCustomerId)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["voucher_number"])){
		$voucherNumber = $_POST["voucher_number"];
		if(!$debitCardRequest->setVoucherNumber($voucherNumber)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}


    // billing fields
	if(isset($_POST["bill_customer_title"])){
		$billCustomerTitle = $_POST["bill_customer_title"];
		if(!$debitCardRequest->setBillCustomerTitle($billCustomerTitle)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_first_name"])){
		$billFirstName = $_POST["bill_first_name"];
		if(!$debitCardRequest->setBillFirstName($billFirstName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_middle_name"])){
		$billMiddleName = $_POST["bill_middle_name"];
		if(!$debitCardRequest->setBillMiddleName($billMiddleName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_last_name"])){
		$billLastName = $_POST["bill_last_name"];
		if(!$debitCardRequest->setBillLastName($billLastName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_company_name"])){
		$billCompanyName = $_POST["bill_company_name"];
		if(!$debitCardRequest->setBillCompanyName($billCompanyName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_address_one"])){
		$billAddressOne = $_POST["bill_address_one"];
		if(!$debitCardRequest->setBillAddressOne($billAddressOne)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_address_two"])){
		$billAddressTwo = $_POST["bill_address_two"];
		if(!$debitCardRequest->setBillAddressTwo($billAddressTwo))	{
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_city"])){
		$billCity = $_POST["bill_city"];
		if(!$debitCardRequest->setBillCity($billCity)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_state_or_province"])){
		$billStateOrProvince = $_POST["bill_state_or_province"];
		if(!$debitCardRequest->setBillStateOrProvince($billStateOrProvince)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_country_code"])){
		$billCountryCode = $_POST["bill_country_code"];
		if(!$debitCardRequest->setBillCountryCode($billCountryCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_zip_or_postal_code"])){
		$billZipOrPostalCode = $_POST["bill_zip_or_postal_code"];
		if(!$debitCardRequest->setBillZipOrPostalCode($billZipOrPostalCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_phone"])){
		$billPhone = $_POST["bill_phone"];
		if(!$debitCardRequest->setBillPhone($billPhone)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_fax"])){
		$billFax = $_POST["bill_fax"];
		if(!$debitCardRequest->setBillFax($billFax)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_email"])){
		$billEmail = $_POST["bill_email"];
		if(!$debitCardRequest->setBillEmail($billEmail)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["bill_note"])){
		$billNote = $_POST["bill_note"];
		if(!$debitCardRequest->setBillNote($billNote)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}



    //shipping fields:
	if(isset($_POST["ship_customer_title"])){
		$shipCustomerTitle = $_POST["ship_customer_title"];
		if(!$debitCardRequest->setShipCustomerTitle($shipCustomerTitle)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_first_name"])){
		$shipFirstName = $_POST["ship_first_name"];
		if(!$debitCardRequest->setShipFirstName($shipFirstName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_middle_name"])){
		$shipMiddleName = $_POST["ship_middle_name"];
		if(!$debitCardRequest->setShipMiddleName($shipMiddleName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_last_name"])){
		$shipLastName = $_POST["ship_last_name"];
		if(!$debitCardRequest->setShipLastName($shipLastName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_company_name"])){
		$shipCompanyName = $_POST["ship_company_name"];
		if(!$debitCardRequest->setShipCompanyName($shipCompanyName)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_address_one"])){
		$shipAddressOne = $_POST["ship_address_one"];
		if(!$debitCardRequest->setShipAddressOne($shipAddressOne)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_address_two"])){
		$shipAddressTwo = $_POST["ship_address_two"];
		if(!$debitCardRequest->setShipAddressTwo($shipAddressTwo))	{
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_city"])){
		$shipCity = $_POST["ship_city"];
		if(!$debitCardRequest->setShipCity($shipCity)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_state_or_province"])){
		$shipStateOrProvince = $_POST["ship_state_or_province"];
		if(!$debitCardRequest->setShipStateOrProvince($shipStateOrProvince)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_country_code"])){
		$shipCountryCode = $_POST["ship_country_code"];
		if(!$debitCardRequest->setShipCountryCode($shipCountryCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_zip_or_postal_code"])){
		$shipZipOrPostalCode = $_POST["ship_zip_or_postal_code"];
		if(!$debitCardRequest->setShipZipOrPostalCode($shipZipOrPostalCode)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_phone"])){
		$shipPhone = $_POST["ship_phone"];
		if(!$debitCardRequest->setShipPhone($shipPhone)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_fax"])){
		$shipFax = $_POST["ship_fax"];
		if(!$debitCardRequest->setShipFax($shipFax)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_email"])){
		$shipEmail = $_POST["ship_email"];
		if(!$debitCardRequest->setShipEmail($shipEmail)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

	if(isset($_POST["ship_note"])){
		$shipNote = $_POST["ship_note"];
		if(!$debitCardRequest->setShipNote($shipNote)) {
			$errorMessages[] = $debitCardRequest->getError();
		}
	}

    // result page output
	if(sizeof($errorMessages) != 0) {
		// Print out all the errors that happened when setting.
		print("<h1><font color=red>Test Transaction Not Attempted</font></h1>");
		print("<p>There was an error setting the fields of the TransactionRequest object.");
		print("The following errors were found:");
		print("<ul>");

		foreach ($errorMessages as $error) {
			print("   <li>$error</li>");
		}

		print("</ul>");

	} else {
		// No errors setting the values; perform the transaction
		$debitCardResponse = $debitCardRequest->doTransaction();

		// If there was a communication failure, then the response
		// object will be false.
		if($debitCardResponse) {
			print("<br>");
			print("<table align='center' border = '0' cellspacing = '0' cellpadding = '0'>");
			print("  <tr>");
			print("    <td width='200' align='left' valign='top'>&nbsp;</td>");
			print("    <td align='center' valign='top'><h3><u>Debit Card Transaction Results</u></h3></td>");
			print("    <td width='200' align='right' valign='top'><a href='demopaypage.html' class = 'header'><strong>Enter New Payment</strong></a></td>");
			print("  </tr>");
			print("</table>");

			print("<hr>");
			print("<center>");
			print("  <TABLE BORDER='0'>");
			print("    <TR ALIGN='CENTER'>"	);
			print("      <TD><b>Order User ID:</b>" . $debitCardRequest->GetOrderUserID() . "</TD>");
			print("      <TD><b>Order ID:</b>" . $debitCardResponse->GetOrderID() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");
			print("  <HR>");
			print("  <br>");
			print("  <TABLE CELLPADDING='2' CELLSPACING='2' BORDER='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Response Fields</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Response Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetResponseCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Secondary Response Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetSecondaryResponseCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Response Text:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetResponseCodeText() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Timestamp:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetTimeStamp() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Retry Recommended:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetRetryRecommended() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Bank Approval Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetBankApprovalCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'");
			print("      <TH align='right' width='200' valign='top'>Batch ID:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetBatchID() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Reference ID:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetReferenceID() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>State of Payment/Credit:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetState() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Purchased Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetPurchasedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Cash Back Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetCashBackAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Cash Benefit Balance:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetCashBenefitBalance() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Food Stamp Balance:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetFoodStampBalance() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Account Type:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetAccountType() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Refunded Amount:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetRefundedAmount() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Voucher Number:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetVoucherNumber() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Transaction Condition Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetTransactionConditionCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Time Stamp Created:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetTimeStampCreated() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Debit Receipt Number:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetDebitReceiptNumber() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT' bgcolor='#EEEEEE'>");
			print("      <TH align='right' width='200' valign='top'>Reversal Status:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardResponse->GetReversalStatus() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");

			print("  <p>&nbsp;</p>");
			print("  <table cellpadding='2' cellspacing='2' border='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Financial Details</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Charge Total:</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getChargeTotal() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT' bgcolor='#EEEEEE'>");
			print("      <th align='right' width='200' valign='top'>Tax Amount:</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getTaxAmount() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Shipping Charge</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getShippingCharge() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT' bgcolor='#EEEEEE'>");
			print("      <th align='right' width='200' valign='top'>Charge Type</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getChargeType() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Primary Account Number</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getPrimaryAccountNumber() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT' bgcolor='#EEEEEE'>");
			print("      <th align='right' width='200' valign='top'>Expiry Date (MM/YYYY):</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetExpireMonth() . "/" . $debitCardRequest->GetExpireYear() . "</td>");
			print("    </tr>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top'>Order Description</th>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getOrderDescription() . "</td>");
			print("    </tr>");
			print("  </table>");

			print("  <p>&nbsp;</p>");
			print("  <TABLE CELLPADDING='2' CELLSPACING='2' BORDER='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Billing Information</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Customer Title:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillCustomerTitle() . "</TD>");
			print("    </TR>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>First Name:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillFirstName() . "</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Middle Name:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillMiddleName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Last Name:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillLastName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Company Name:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillCompanyName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Address One:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillAddressOne() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Address Two:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillAddressTwo() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>City:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillCity() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>State or Province:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillStateOrProvince() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Country Code:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillCountryCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Zip or Postal Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->getBillZipOrPostalCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Phone:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillPhone() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Fax:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillFax() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Email:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetBillEmail() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Note:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetBillNote() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");

			print("  <p>&nbsp;</p>");
			print("  <TABLE CELLPADDING='2' CELLSPACING='2' BORDER='0' width='500'>");
			print("    <tr class = 'header'>");
			print("      <td colspan=2>&nbsp;Shipping Information</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Customer Title:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipCustomerTitle() . "</TD>");
			print("    </TR>");
			print("    <tr align='LEFT'>");
			print("      <th align='right' width='200' valign='top' bgcolor='#EEEEEE'>First Name:</th>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipFirstName() . "</td>");
			print("    </tr>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Middle Name:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipMiddleName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Last Name:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipLastName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Company Name:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipCompanyName() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Address One:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipAddressOne() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Address Two:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipAddressTwo() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>City:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipCity() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>State or Province:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipStateOrProvince() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Country Code:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipCountryCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Zip or Postal Code:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipZipOrPostalCode() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Phone:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipPhone() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Fax:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipFax() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top' bgcolor='#EEEEEE'>Email:</TH>");
			print("      <TD bgcolor='#EEEEEE' width='300'>&nbsp;" . $debitCardRequest->GetShipEmail() . "</TD>");
			print("    </TR>");
			print("    <TR ALIGN='LEFT'>");
			print("      <TH align='right' width='200' valign='top'>Note:</TH>");
			print("      <TD width='300'>&nbsp;" . $debitCardRequest->GetShipNote() . "</TD>");
			print("    </TR>");
			print("  </TABLE>");
			print("  <BR>");
			print("  <HR>");
			print("</center>");
		} else {
			print("<h1><font color=red>Test Transaction Failed</font></h1>A communication error occurred.");
			print("<p>Error: ". $debitCardRequest->getError());
		}
	}
?>

</body>
</html>

