<html>
<head>
   <title>Demo Store Sale - Gift Card Results</title>
   <link rel="STYLESHEET" type="text/css" href="main.css">
   <style>
       tr.darkRow   { background-color:  #EEEEEE ;  }
       tr.headRow   { background-color:  #D8D8D8 ;  }
   </style>
</head>
<body bgcolor="#FFFFFF" text="#000000">

<?php
	include("Paygateway.php");

	// enter your token here
	define("TEST_TOKEN", "75FA60D5CE67560A3588ECFB8754AC66C2C92A9E4EB3C7FEE5A44E2282DC5EEB98583D8CC1D95B");

	$errorMessages = array();
	$giftCardRequest = new GiftCardRequest();
	$giftCardRequest->setAccountToken(TEST_TOKEN);

	// Populate request with data from web form

    if(isset($_POST[TRANSACTION_TYPE])){
        if(!$giftCardRequest->setTransactionType($_POST[TRANSACTION_TYPE])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[CHARGE_TYPE])){
        if(!$giftCardRequest->setChargeType($_POST[CHARGE_TYPE])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[CHARGE_TOTAL])){
        if(!$giftCardRequest->setChargeTotal($_POST[CHARGE_TOTAL])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[ORDER_ID])){
        if(!$giftCardRequest->setOrderID($_POST[ORDER_ID])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[PURCHASE_ORDER_NUMBER])){
        if(!$giftCardRequest->setPurchaseOrderNumber($_POST[PURCHASE_ORDER_NUMBER])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[TRANSACTION_ID])){
        if(!$giftCardRequest->setTransactionID($_POST[TRANSACTION_ID])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[DEVICE_TERMINAL_ID])){
        if(!$giftCardRequest->setDeviceTerminalID($_POST[DEVICE_TERMINAL_ID])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[PREVIOUS_CHARGE_TYPE])){
        if(!$giftCardRequest->setPreviousChargeType($_POST[PREVIOUS_CHARGE_TYPE])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[TRACK1])){
        if (!$giftCardRequest->setTrack1($_POST[TRACK1])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }


	if(isset($_POST[TRACK2])){
		if (!$giftCardRequest->setTrack2($_POST[TRACK2])) {
			$errorMessages[] = $giftCardRequest->getError();
		}
	}

    if(isset($_POST[PAN])){
        if(!$giftCardRequest->setPan($_POST[PAN])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }

    if(isset($_POST[PERSONAL_IDENTIFICATION_NUMBER])){
        if(!$giftCardRequest->setPersonalIdentificationNumber($_POST[PERSONAL_IDENTIFICATION_NUMBER])) {
            $errorMessages[] = $giftCardRequest->getError();
        }
    }                                       


    // result page output
	if(sizeof($errorMessages) != 0) {
		// Print out all the errors that happened when setting.
		print("<h1><font color=red>Test Transaction Not Attempted</font></h1>");
		print("<p>There was an error setting the fields of the TransactionRequest object.");
		print("The following errors were found:");
		print("<ul>");

		foreach ($errorMessages as $error) {
			print("   <li>$error</li>");
		}

		print("</ul>");

	} else {
		// No errors setting the values; perform the transaction
        $giftCardResponse = $giftCardRequest->doTransaction();

		// If there was a communication failure, then the response
		// object will be false.
		if($giftCardResponse) { ?>

      <br>
      <TABLE align="center" CELLPADDING="2" CELLSPACING="2" BORDER="0" width="70%" >
        <tr class="headRow" height="20px" >
          <TH align="center"   height="20px" colspan="2">Gift Card Tranaction Test Response Fields &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    </TH>

        </tr>
        <TR>
          <TH  align="right" width="30%" valign="top">Response Code:</TH>
          <TD width="70%">&nbsp;<?php print($giftCardResponse->getResponseCode()) ?></TD>
        </TR>
        <TR class="darkRow" >
          <TH align="right"  valign="top">Response Text:</TH>
          <TD >&nbsp; <font color=blue><?php print($giftCardResponse->getResponseCodeText() ) ?></font></TD>
        </TR>
        <TR >
          <TH align="right"  valign="top">Secondary Response Code:</TH>
          <TD >&nbsp;<?php print($giftCardResponse->getSecondaryResponseCode() ) ?></TD>
        </TR>
        <TR class="darkRow" >
          <TH align="right"  valign="top">Timestamp:</TH>
          <TD >&nbsp;<?php print($giftCardResponse->getTimeStamp() ) ?></TD>
        </TR>
        <TR >
          <TH align="right"  valign="top">Retry Recommended:</TH>
          <TD >&nbsp;<?php print($giftCardResponse->getRetryRecommended() ) ?>   </TD>
        <tr class="darkRow">
          <th valign="top" align="right" >PAN:</th>
          <TD >&nbsp;<?php print($giftCardResponse->getPan() ) ?></td>
        </tr>
        <tr >
          <th valign="top" align="right" >PIN:</th>
          <TD >&nbsp;<?php print($giftCardResponse->getPersonalIdentificationNumber() ) ?></td>
        </tr>
        <TR class="darkRow" >
          <th align="right"  valign="top">Approved Amount:</th>
          <TD >&nbsp;<?php print($giftCardResponse->getApprovedAmount() ) ?></td>
        </tr>
        <TR  >
          <th align="right"  valign="top">Remaining Balance:</th>
          <TD >&nbsp;<?php print($giftCardResponse->getRemainingBalance() ) ?></td>
        </tr>
        <tr  class="darkRow">
          <th align="right"  valign="top">Account Currency:</th>
          <TD >&nbsp;<?php print($giftCardResponse->getAccountCurrency() ) ?></td>
        </tr>
        <TR  >
          <th align="right"  valign="top">Transaction State:</th>
          <TD >&nbsp;<?php print($giftCardResponse->getTransactionState() ) ?>
          </td>
        </tr>
        
       <TR  >
          <TD colspan="2" align="center" >    <br/>    <br/> 
           <input type="button" value="Go back to enter another transaction" onclick="javascript:  history.go(-1)"></td>
        </tr>

      </TABLE>
 <?php
        } else { ?>
             <h1><font color=red>Test Transaction Failed</font></h1>
              A communication error occurred.   ;
             <p>Error: <?php $giftCardRequest->getError();  ?>   <br>  <b> <br>
             <input type="button" value="Go back to enter another transaction" onclick="javascript:  history.go(-1)">
             <?php
        }
    }
?>

</body>
</html>
