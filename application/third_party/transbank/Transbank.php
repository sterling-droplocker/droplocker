<?php
include('xmlseclibs.php');
include('soap-validation.php');
include('soap-wsse.php');

class removeUser{
    var $arg0;
    //oneClickRemoveUserInput
}
class oneClickRemoveUserInput{
    var $tbkUser;
    //string
    var $username;
    //string
}
class baseBean{
}
class removeUserResponse{
    var $return;
    //boolean
}
class initInscription{
    var $arg0;
    //oneClickInscriptionInput
}
class oneClickInscriptionInput{
    var $email;
    //string
    var $responseURL;
    //string
    var $username;
    //string
}
class initInscriptionResponse{
    var $return;
    //oneClickInscriptionOutput
}
class oneClickInscriptionOutput{
    var $token;
    //string
    var $urlWebpay;
    //string
}
class finishInscription{
    var $arg0;
    //oneClickFinishInscriptionInput
}
class oneClickFinishInscriptionInput{
    var $token;
    //string
}
class finishInscriptionResponse{
    var $return;
    //oneClickFinishInscriptionOutput
}
class oneClickFinishInscriptionOutput{
    var $authCode;
    //string
    var $creditCardType;
    //creditCardType
    var $last4CardDigits;
    //string
    var $responseCode;
    //int
    var $tbkUser;
    //string
}
class codeReverseOneClick{
    var $arg0;
    //oneClickReverseInput
}
class oneClickReverseInput{
    var $buyorder;
    //long
}
class codeReverseOneClickResponse{
    var $return;
    //oneClickReverseOutput
}
class oneClickReverseOutput{
    var $reverseCode;
    //long
    var $reversed;
    //boolean
}
class authorize{
    var $arg0;
    //oneClickPayInput
}
class oneClickPayInput{
    var $amount;
    //decimal
    var $buyOrder;
    //long
    var $tbkUser;
    //string
    var $username;
    //string
}
class authorizeResponse{
    var $return;
    //oneClickPayOutput
}
class oneClickPayOutput{
    var $authorizationCode;
    //string
    var $creditCardType;
    //creditCardType
    var $last4CardDigits;
    //string
    var $responseCode;
    //int
    var $transactionId;
    //long
}
class reverse{
    var $arg0;
    //oneClickReverseInput
}
class reverseResponse{
    var $return;
    //boolean
}

class SoapWrapper extends SoapClient {


    function __doRequest($request, $location, $saction, $version)
    {
        $doc = new DOMDocument('1.0');
        $doc->loadXML($request);
        $objWSSE = new WSSESoap($doc);
        $objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1,array('type' => 'private'));
        $objKey->loadKey(PRIVATE_KEY, TRUE);
        $options = array("insertBefore" => TRUE);
        $objWSSE->signSoapDoc($objKey, $options);
        $objWSSE->addIssuerSerial(CERT_FILE);
        $objKey = new XMLSecurityKey(XMLSecurityKey::AES256_CBC);
        $objKey->generateSessionKey();
        $retVal = parent::__doRequest($objWSSE->saveXML(), $location, $saction,
        $version);
        $doc = new DOMDocument();
        $doc->loadXML($retVal);
        return $doc->saveXML();
    }
}

class Transbank
{
    var $soapClient;

    private static $classmap = array(
        'removeUser'=>'removeUser'
        ,'oneClickRemoveUserInput'=>'oneClickRemoveUserInput'
        ,'baseBean'=>'baseBean'
        ,'removeUserResponse'=>'removeUserResponse'
        ,'initInscription'=>'initInscription'
        ,'oneClickInscriptionInput'=>'oneClickInscriptionInput'
        ,'initInscriptionResponse'=>'initInscriptionResponse'
        ,'oneClickInscriptionOutput'=>'oneClickInscriptionOutput'
        ,'finishInscription'=>'finishInscription'
        ,'oneClickFinishInscriptionInput'=>'oneClickFinishInscriptionInput'
        ,'finishInscriptionResponse'=>'finishInscriptionResponse'
        ,'oneClickFinishInscriptionOutput'=>'oneClickFinishInscriptionOutput'
        ,'codeReverseOneClick'=>'codeReverseOneClick'
        ,'oneClickReverseInput'=>'oneClickReverseInput'
        ,'codeReverseOneClickResponse'=>'codeReverseOneClickResponse'
        ,'oneClickReverseOutput'=>'oneClickReverseOutput'
        ,'authorize'=>'authorize'
        ,'oneClickPayInput'=>'oneClickPayInput'
        ,'authorizeResponse'=>'authorizeResponse'
        ,'oneClickPayOutput'=>'oneClickPayOutput'
        ,'reverse'=>'reverse'
        ,'reverseResponse'=>'reverseResponse'
    );

    function __construct($url='https://webpay3gint.transbank.cl/webpayserver/wswebpay/OneClickPaymentService?wsdl', $private_key = null, $cert_file = null, $server_cert = '/certs/certificate_server.crt')
    {
        if (!defined('PRIVATE_KEY')) {
            define('PRIVATE_KEY', dirname(__FILE__).$private_key);
        }

        if (!defined('CERT_FILE')) {
            define('CERT_FILE', dirname(__FILE__).$cert_file);
        }

        if (!defined('SERVER_CERT')) {
            define('SERVER_CERT',dirname(__FILE__).$server_cert);
        }

        $this->soapClient = new SoapWrapper($url, array("classmap" => self::$classmap, "trace" => true, "exceptions" => false));
    }

    function removeUser($removeUser)
    {

        $removeUserResponse = $this->soapClient->removeUser($removeUser);
        return $removeUserResponse;

    }
    function initInscription($initInscription)
    {

        $initInscriptionResponse = $this->soapClient->initInscription($initInscription);
        return $initInscriptionResponse;

    }
    function finishInscription($finishInscription)
    {

        $finishInscriptionResponse = $this->soapClient->finishInscription($finishInscription);
        return $finishInscriptionResponse;

    }
    function authorize($authorize)
    {

        $authorizeResponse = $this->soapClient->authorize($authorize);
        return $authorizeResponse;

    }
    function codeReverseOneClick($codeReverseOneClick)
    {

        $codeReverseOneClickResponse = $this->soapClient->codeReverseOneClick($codeReverseOneClick);
        return $codeReverseOneClickResponse;

    }
    function reverse($reverse)
    {

        $reverseResponse = $this->soapClient->reverse($reverse);
        return $reverseResponse;

    }
}
