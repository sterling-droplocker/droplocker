<?php 
ini_set("display_errors", true); error_reporting(E_ALL);
$lib = "application/third_party/qrcode";

require_once("$lib/autoload_register.php");

$width = isset($_GET['width'])?$_GET['width']:256;
$height = isset($_GET['height'])?$_GET['height']:256;
$qrcode = isset($_GET['qrcode'])?$_GET['qrcode']:"No qrcode";

$renderer = new \BaconQrCode\Renderer\Image\Png();
$renderer->setHeight($height);
$renderer->setWidth($width);
$writer = new \BaconQrCode\Writer($renderer);

$imgStr = $writer->writeString($qrcode);
header('Content-type: image/png');
echo $imgStr;
