<?php

/**
 * To be able to switch to dev env regardless of gethostname() result
 * The path should be APPPATH, but those constants are defined later
 */
@include_once('./application/config/env.php');

/**
 * Force secure cookies for PCI compliance
 */

ini_set('session.cookie_httponly', 1); // cookies can not be read on client side
ini_set('session.cookie_secure', 1); // only send cookies to https domains

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
if( (!defined('FORCE_DEV') || !FORCE_DEV) && gethostname()=='master'){
    define('ENVIRONMENT', 'production');
    define("DEV", false);
} else {
    define('ENVIRONMENT', 'development');
    define("DEV", true);
}

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */
switch (ENVIRONMENT) {
    case 'development':
        ini_set("display_errors", 1);
        error_reporting(E_ALL ^E_NOTICE ^E_WARNING);
        //error_reporting(E_ALL);
        error_reporting(1);
    break;

    case 'production':
        ini_set("display_errors", 0);
        //error_reporting(E_ALL);
        error_reporting(1);
    break;

    default:
        exit('The application environment is not set correctly.');
}

        ini_set("display_errors", 1);
        error_reporting(E_ALL);
        //error_reporting(E_ALL);
        error_reporting(1);

/*
 *---------------------------------------------------------------
 * SYSTEM FOLDER NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" folder.
 * Include the path if the folder is not in the same  directory
 * as this file.
 *
 */
    $system_path = 'system';

/*
 *---------------------------------------------------------------
 * APPLICATION FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder then the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server.  If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 *
 */
    $application_folder = 'application';

/*
 * --------------------------------------------------------------------
 * DEFAULT CONTROLLER
 * --------------------------------------------------------------------
 *
 * Normally you will set your default controller in the routes.php file.
 * You can, however, force a custom routing by hard-coding a
 * specific controller class/function here.  For most applications, you
 * WILL NOT set your routing here, but it's an option for those
 * special instances where you might want to override the standard
 * routing in a specific front controller that shares a common CI installation.
 *
 * IMPORTANT:  If you set the routing here, NO OTHER controller will be
 * callable. In essence, this preference limits your application to ONE
 * specific controller.  Leave the function name blank if you need
 * to call functions dynamically via the URI.
 *
 * Un-comment the $routing array below to use this feature
 *
 */
    // The directory name, relative to the "controllers" folder.  Leave blank
    // if your controller is not in a sub-folder within the "controllers" folder
    // $routing['directory'] = '';

    // The controller class file name.  Example:  Mycontroller
    // $routing['controller'] = '';

    // The controller function you wish to be called.
    // $routing['function'] = '';


/*
 * -------------------------------------------------------------------
 *  CUSTOM CONFIG VALUES
 * -------------------------------------------------------------------
 *
 * The $assign_to_config array below will be passed dynamically to the
 * config class when initialized. This allows you to set custom config
 * items or override any default config values found in the config.php file.
 * This can be handy as it permits you to share one application between
 * multiple front controller files, with each file containing different
 * config values.
 *
 * Un-comment the $assign_to_config array below to use this feature
 *
 */
    // $assign_to_config['name_of_config_item'] = 'value of config item';



// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */


    // Set the current directory correctly for CLI requests
    if (defined('STDIN'))
    {
            chdir(dirname(__FILE__));
    }

    if (realpath($system_path) !== FALSE)
    {
            $system_path = realpath($system_path).'/';
    }

    // ensure there's a trailing slash
    $system_path = rtrim($system_path, '/').'/';

    // Is the system path correct?
    if ( ! is_dir($system_path))
    {
            exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
    }

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
    // The name of THIS file
    define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

    // The PHP file extension
    // this global constant is deprecated.
    define('EXT', '.php');

    // Path to the system folder
    define('BASEPATH', str_replace("\\", "/", $system_path));

    // Path to the front controller (this file)
    define('FCPATH', str_replace(SELF, '', __FILE__));

    // Name of the "system folder"
    define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


    // The path to the "application" folder
    if (is_dir($application_folder))
    {
            define('APPPATH', $application_folder.'/');
    }
    else
    {
            if ( ! is_dir(BASEPATH.$application_folder.'/'))
            {
                exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
            }

            define('APPPATH', BASEPATH.$application_folder.'/');
    }


if( ! ini_get('date.timezone') )
{
   date_default_timezone_set('UTC');
}

/**
 * This is the catch all handler for any uncaught exceptions. Note that this function will redirect to the previous page from which the request URI was called.
 * @param type $exception
 */
function exception_handler($exception){
    $CI = get_instance();
    switch ($CI->uri->segment(1)) {
        case 'admin':   $url = '/admin';   break;
        case 'account': $url = '/account'; break;
        default:        $url = '/'; break;
    }
    $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $url;
    $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
    $hide_errors = ENVIRONMENT == "production"  && $extension != "qa";

    try {
        send_exception_report($exception);
        if (isset($CI->session)) {
            if ($CI->input->is_ajax_request()) {
                if ($hide_errors) {
                    $error_message = array("status" => "error", "message" => "Sorry, an internal error occurred with your request, please contact Droplocker support at development@droplocker.com.");
                    die(json_encode($error_message));
                } else {
                    die("<div>" . $exception->getMessage() . "</div> <pre>" . var_dump($exception->getTraceAsString()) . "</pre>");
                }
            }

            if ($hide_errors) {
                set_flash_message("error", "Sorry, an internal error occurred attempting to process your request. Please contact Droplocker Support at development@droplocker.com");
            } else {
                die("<pre style='text-align:left;'><code>" . print_r($exception, true) . "</code></pre>");
            }
        }
        header("Location: ".$url, TRUE, $http_response_code);

    } catch (Exception $e) {
        if ($hide_errors) {
            set_flash_message("error", "Sorry, an internal error occurred attempting to process your request. Please contact Droplocker Support at development@droplocker.com");
        } else {
            echo "<pre style='text-align:left;'><code>" . print_r($e, true) . "</code></pre>";
            die("<pre style='text-align:left;'><code>" . print_r($exception, true) . "</code></pre>");
        }
        header("Location: ".$url, TRUE, $http_response_code);

        send_exception_report($e, "Exception catch while sending another exception: " . $exception->getMessage(), false);
        send_exception_report($exception, "Origial exception that thowed another exception", false);
    }
}
set_exception_handler("exception_handler");


//Athe following conditional creates the attachments directory if it does not already exist.
if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/attachments") && php_sapi_name() != 'cli') {
    if (!mkdir($_SERVER['DOCUMENT_ROOT'] . "/attachments")) {
        trigger_error("Could not create attachments directory", E_USER_WARNING);
    }
}
/**
if (!chmod($_SERVER['DOCUMENT_ROOT'] . "/attachments", 0777))
{
    trigger_error("Could not set the access permissions on the attachments directory", E_USER_WARNING);
}
*/
/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 *
 */
require_once BASEPATH.'core/CodeIgniter.php';


class User_Exception extends Exception
{

}

/* End of file index.php */
/* Location: ./index.php */