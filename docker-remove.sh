#!/bin/bash

set -e

echo
echo "-- Removing droplocker-5.6 server"
sudo docker rm -f -v droplocker-5.6; sleep 5
sudo docker rmi -f droplocker-5.6
