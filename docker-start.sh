#!/bin/bash
APP_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

if [ $# -eq 0 ]
then
    echo "Error: No arguments supplied. Eg: ./docker-start.sh MYSQL_HOST MYSQL_USER MYSQL_PASSWORD MYSQL_DB_NAME"
    exit 1
fi
if [ -z "$1" ]
  then
    echo "Please enter MYSQL_HOST"
    exit 1
fi
if [ -z "$2" ]
  then
    echo "Please enter MYSQL_USER"
    exit 3
fi
if [ -z "$3" ]
  then
    echo "Please enter MYSQL_PASSWORD"
    exit 1
fi
if [ -z "$4" ]
  then
    echo "Please enter MYSQL_DB_NAME"
    exit 1
fi

MYSQL_DRIVER=mysql
MYSQL_HOST=$1
MYSQL_USER=$2
MYSQL_PASSWORD=$3
MYSQL_DB_NAME=$4

echo "-- droplocker-5.6  server is starting in current path: $APP_PATH"
echo "-- droplocker-5.6  server is running"
sudo docker run -e "MYSQL_DRIVER=$MYSQL_DRIVER" -e "MYSQL_HOST=$MYSQL_HOST" -e "MYSQL_USER=$MYSQL_USER" -e "MYSQL_PASSWORD=$MYSQL_PASSWORD" -e "MYSQL_DB_NAME=$MYSQL_DB_NAME" \
-v $APP_PATH:/var/www/app --name droplocker-5.6 -d -p 80:80 -p 443:443 droplocker-5.6 
