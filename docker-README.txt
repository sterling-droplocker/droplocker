* Install docker
    https://docs.docker.com/install/

* Allow container to access your local mysql server (no needed if you use remote mysql server):

    1) [host] In mysql my.cnf file inside the host add this:
        bind-address = 0.0.0.0

    2) [host] Reload mysql
        sudo service mysql reload

    3) [host] Open mysql shell and grant perms to user in all ips
        GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'mes12345' WITH GRANT OPTION;

* Add required domains to your host "/etc/hosts" file.
    For example if you only want to run dla you need to add "127.0.0.1 droplocker.loc"

* Copy application/config/database-docker.php to application/config/database.php

* Open docker-start.sh script an set needed domains and MYSQL_HOST ip (probably you dont need to do this)

-- Build and run the docker images

* ./docker-build.sh (only the first time)
  ./docker-start.sh (this starts the image)
  ./docker-stop.sh (if you want to stop the image)

* Create and .httaccess file:
    cp .httaccess_droplocker .httaccess
    
* Open your browser an go to: https://droplocker.loc/admin/admin
    obs.. You need to accept the ssl warnings

Obs.. you DONT need to stop and start the docker instance after every code change...
You can see all code changes live.. Just press ctrl+f5 in your browser
