#!/bin/bash

set -e

echo
echo
echo "-- Building Droplocker Apache 2.4 + PHP 5.6 image"
sudo docker build -t droplocker-5.6 -f docker/php5.6/Dockerfile .
