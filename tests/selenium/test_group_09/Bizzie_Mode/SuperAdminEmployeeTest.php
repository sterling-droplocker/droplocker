<?php

class SuperAdminEmployeeTest extends BizzieSuperAdminUnitTest
{
    public function setUp()
    {
        parent::setUp();
        $this->login_as_bizzie_superadmin();
    }
    /**
     * The following test verifies the functionality for adding a new employee through the superadmin interface.
     */
    public function testAddEmployeeToBusiness()
    {
        $states = get_states_as_array();
        $aclRoles = get_roles_as_codeigniter_dropdown($this->business_id);
        
        $this->driver->get_element("link text=Add Employee to Business")->click();
        $new_employee = new stdClass();
        $new_employee->firstName = UnitTest::get_random_word();
        $new_employee->lastName = UnitTest::get_random_word();
        $new_employee->password = UnitTest::get_random_word();
        $new_employee->ssn = rand(100000000,999999999);
        $word = UnitTest::get_random_word();
        $new_employee->email = "$word@example.com";
        $new_employee->phoneHome = rand(1000000000, 9999999999);
        $new_employee->phoneCell = rand(1000000000, 9999999999);
        $new_employee->address = rand(100, 999) . UnitTest::get_random_word(2);
        $new_employee->state = array_rand($states);
        $new_employee->zipcode = rand(10000, 99999);
        $new_employee->city = UnitTest::get_random_word();
        $new_employee->type = 'non exempt';
        
        $now = new DateTime("now");
        $new_employee->startDate = $now;
        $new_employee->endDate = $now;
        $aclRole_id = array_rand($aclRoles);
        
        $this->driver->get_element("name=firstName")->send_keys($new_employee->firstName);
        $this->driver->get_element("name=lastName")->send_keys($new_employee->lastName);
        $this->driver->get_element("name=password")->send_keys($new_employee->password);
        $this->driver->get_element("name=ssn")->send_keys($new_employee->ssn);
        $this->driver->get_element("name=email")->send_keys($new_employee->email);
        $this->driver->get_element("name=phoneHome")->send_keys($new_employee->phoneHome);
        $this->driver->get_element("name=phoneCell")->send_keys($new_employee->phoneCell);
        $this->driver->get_element("name=address")->send_keys($new_employee->address);
        $this->driver->get_element("name=city")->send_keys($new_employee->city);
        $this->driver->get_element("name=state")->select_value($new_employee->state);
        $this->driver->get_element("name=startDate")->send_keys($new_employee->startDate->format("Y-m-d H:i:s"));
        $this->driver->get_element("name=endDate")->send_keys($new_employee->endDate->format("Y-m-d H:i:s"));
        $this->driver->get_element("name=zipcode")->send_keys($new_employee->zipcode);
        $this->driver->get_element("name=type")->select_value($new_employee->type);
        
        //Note, there is a bug in Selenium Webdriver that prevents the business menu from selecting an option.
        $this->driver->get_element("name=business_id")->select_value($this->business_id);
        $this->wait_for_ajax_to_complete();
        //The following statements are disabled in selenium because the selector does not select he correct dropdown.
        //$this->driver->get_element("manager_id")->select_value(1);
        //$this->driver->get_element("name=aclRole_id")->select_value($aclRole_id);
        $this->driver->get_element("name=business_id")->submit();
        $this->driver->get_element("css=div.success")->assert_text(sprintf("Added %s %s to %s", $new_employee->firstName, $new_employee->lastName, $this->business->companyName));
        
        $employees = \App\Libraries\DroplockerObjects\Employee::search_aprax(array("email" => $new_employee->email));
        $this->assertNotEmpty($employees, "Could not find the newly created employee in the database.");
        $this->assertEquals(1,count($employees), "Multiple records were found for the same new employee");
        $employee = $employees[0];
        $this->assertEquals($new_employee->firstName, $employee->firstName, "The employee's first name is not the expected value");
        $this->assertEquals($new_employee->lastName, $employee->lastName, "The employee's last name is not the expected value");
        $this->assertEquals($new_employee->ssn, $employee->ssn, "The employee's SSN is not the expected value");
        $this->assertEquals($new_employee->phoneHome, $employee->phoneHome, "The employee's home phone is not the expected value");
        $this->assertEquals($new_employee->phoneCell, $employee->phoneCell, "The employee's cell phone is not the expected value");
        $this->assertEquals($new_employee->address, $employee->address, "The employee's address is not the expected value");
        $this->assertEquals($new_employee->state, $employee->state, "The employee's state is not the expected value");
        $this->assertEquals($new_employee->zipcode, $employee->zipcode, "The employee's zip code is not the expected value");
        $this->assertEquals($new_employee->startDate, $employee->startDate, "The employee's start date time is not the expected value.");
        $this->assertEquals($new_employee->endDate, $employee->endDate, "The employee's end date time is not the expected value");
        $this->assertEquals($new_employee->type, $employee->type, "The employee type[exempt/non-exempt] was not the expected value");

        
        $business_employees = \App\Libraries\DroplockerObjects\Business_Employee::search_aprax(array("employee_id" => $employee->employeeID));
        $this->assertNotEmpty($business_employees, "Could not find the newly created business employee entry in the database.");
        $this->assertEquals(1, count($business_employees), "Multiple entries were found for the same business employee");
        $business_employee = $business_employees[0];
        $this->assertEquals($this->business_id, $business_employee->business_id);
    }    
}

?>
