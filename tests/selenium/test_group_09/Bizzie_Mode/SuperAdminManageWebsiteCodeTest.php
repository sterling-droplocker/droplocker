<?php
use App\Libraries\DroplockerObjects\Website;
use App\Libraries\DroplockerObjects\Business;
class SuperAdminManageWebsiteCodeTest extends BizzieSuperAdminUnitTest
{
    public function setUp()
    {
        parent::setUp();
    }
    
    /**
     * The following test verifies that the 'Website' menu does not appear when the employee is not an Bizzie Superadin.
     */
    public function testNonSuperAdminLimitedAccess()
    {
        $this->login_as_slave_admin();
        $this->driver->assert_element_not_present("link text=Website");
    }


}

?>
