<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Server_Meta;
use App\Libraries\DroplockerObjects\Image;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
/**
 * The following test suite verifies the fucntionality for managing businesses when the server is in Bizzie mode.
 */
class SuperAdminManageBusinessTest extends BizzieSuperAdminUnitTest
{
    protected $test_business;

    protected $states;
    protected $master_image_01;
    public function setUp()
    {
        parent::setUp();
        $this->login_as_bizzie_superadmin();
        $this->states = get_states_as_array();
        //The following statement sets the server to bizzie mode.
        
        $this->master_image_01 = UnitTest::create_random_image($this->master_business->{Business::$primary_key});
        
        $this->test_business = new Business();
        $this->test_business->companyName = UnitTest::get_random_word(2);
        $this->test_business->address = UnitTest::get_random_word(3);
        $this->test_business->address2 = UnitTest::get_random_word(2);
        $this->test_business->city = UnitTest::get_random_word(2);

        $this->test_business->state = array_rand($this->states);
        $this->test_business->zipcode = rand(10000,99999);
        $this->test_business->phone = rand(10000000000, 9999999999);
        $this->test_business->email = unitTest::get_random_word(1) . "@example.com";
        $this->test_business->blank_customer_id = 0;
        $this->test_business->timezone = "America/Los_Angeles";
        $this->test_business->subdomain = UnitTest::get_random_word();
    }
    
    /*
    * This test verifies the functionality to add a business in the super admin
    */
    public function testAddBusiness() 
    {
        # Populate add business form
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/superadmin/add_business");

        $word = UnitTest::get_random_word(1);
        
        $admin_email = "webdriver_$word@example.com";
        $admin_password = "webdriver";
        $admin_firstName = "webdriver_" . UnitTest::get_random_word(1);
        $admin_lastName = "webdriver_" . UnitTest::get_random_word(1);
        
        $this->driver->get_element('name=companyName')->send_keys($this->test_business->companyName);
        $this->driver->get_element('name=address')->send_keys($this->test_business->address);
        $this->driver->get_element('name=address2')->send_keys($this->test_business->address2);
        $this->driver->get_element('name=city')->send_keys($this->test_business->city);
        $this->driver->get_element('name=state')->select_value($this->test_business->state);
        $this->driver->get_element('name=zipcode')->send_keys($this->test_business->zipcode);
        $this->driver->get_element('name=phone')->send_keys($this->test_business->phone);
        $this->driver->get_element('name=email')->send_keys($this->test_business->email);
        $this->driver->get_element("name=subdomain")->send_keys($this->test_business->subdomain);
        $this->driver->get_element('name=admin_email')->send_keys($admin_email);
        $this->driver->get_element('name=admin_password')->send_keys($admin_password);
        $this->driver->get_element('name=admin_firstName')->send_keys($admin_firstName);
        $this->driver->get_element('name=admin_lastName')->send_keys($admin_lastName);
        
        $checkboxes = $this->driver->get_all_elements('name=serviceTypes[]');
        $checkbox_values = array();
        foreach ($checkboxes as $index => $checkbox) 
        {
            $checkbox_values[$index] = $checkbox->get_value();
            $checkbox->select();
        }
        
        $this->driver->get_element('css=input[type="submit"]')->click();
        
        # Verify business made it to the DB
        $new_businesses = Business::search_aprax(array('companyName' => $this->test_business->companyName, "email" => $this->test_business->email));
        $this->assertNotEmpty($new_businesses, "Could not find the new business in the {Business::$table_name} table");
        $new_business = $new_businesses[0]; # There should only be one business
        $this->assertEquals($this->test_business->companyName, $new_business->companyName);
        $this->assertEquals($this->test_business->address, $new_business->address);
        $this->assertEquals($this->test_business->address2, $new_business->address2);
        $this->assertEquals($this->test_business->city, $new_business->city);
        $this->assertEquals($this->test_business->state, $new_business->state);
        $this->assertEquals($this->test_business->zipcode, $new_business->zipcode);
        $this->assertEquals($this->test_business->phone, $new_business->phone);
        $this->assertEquals($this->test_business->email, $new_business->email);
        $this->assertEquals($this->test_business->subdomain, $new_business->subdomain);
        
        $new_admins = Employee::search_aprax(array("email" => $admin_email, "firstName" => $admin_firstName, "lastName" => $admin_lastName));
        $this->assertNotEmpty($new_admins, "Could not find the new administratove account for the new business");
        $new_admin = $new_admins[0];
        
        $this->CI->load->model("business_employee_model");
        $business_employees = $this->CI->business_employee_model->get_aprax(array("employee_id" => $new_admin->{Employee::$primary_key}, 'business_id' => $new_business->{Business::$primary_key}));
        $this->assertNotEmpty($business_employees, "Could not find the new administrative business employee.");

        $this->driver->get_element('id=employee_name')->assert_text($admin_firstName . " " . $admin_lastName);
        
        # Make sure all checkbox data came through
        $serviceTypes = unserialize($new_business->serviceTypes);
        $this->assertEquals($checkbox_values, $serviceTypes);
        $this->test_business = $new_business;
        
        //The following statements verify that the images form the master business were succesfully copied to the new business.
        $master_images = Image::search_aprax(array("business_id" => $this->business_id));
        foreach ($master_images as $master_image)
        {
            $new_business_images = Image::search_aprax(array("filename" => $master_image->filename, "path" => $master_image->path, "business_id" => $new_business->{Business::$primary_key}));
            $this->assertNotEmpty($new_business_images, "Could not find the copied image for the new business");
            $this->assertEquals(1, count($new_business_images), "Multiple businesss images were found for the given filename and path.");
        }
    }    
    
    /**
     * The following test verifies the functionality for changing a business' properties 
     */
    public function testModifyBusiness()
    {        
        $date_formats = $this->CI->config->item("dateFormats");
        $standardDateDisplayFormat = $date_formats[array_rand($date_formats)];
//        $shortDateDisplayFormat = $date_formats[array_rand($date_formats)];
//        $fullDateDisplayFormat = $date_formats[array_rand($date_formats)];
        $states = get_states_as_array();
        $timezones = DateTimeZone::listIdentifiers();
        
        $companyName = UnitTest::get_random_word(2);
        $address = UnitTest::get_random_word(3);
        $address2 = UnitTest::get_random_word(2);
        $city = UnitTest::get_random_word(2);
        $state = array_rand($states);
        $zipcode = rand(10000,99999);
        $phone = 8888881234;
        $email = UnitTest::get_random_word(1) . "@example.com";
        $subdomain = UnitTest::get_random_word(1);
        $website_url = UnitTest::get_random_word(3);
        $cutOff = rand(1,500);
        $printer_key = UnitTest::get_random_word(1);
        $metalProGrouping = UnitTest::get_random_word(1);
        $facebook_api_key = UnitTest::get_random_word(1);
        $facebook_secret_key = UnitTest::get_random_word(1);
        $terms = UnitTest::get_random_word(50);
        $timezone = $timezones[array_rand($timezones)];
        
        $this->test_business->save();
        $this->driver->load(sprintf("http://droplocker.%s/admin/superadmin/edit_business/%s", $this->domain_extension, $this->test_business->{Business::$primary_key}));
        $this->driver->get_element("name=companyName")->clear();
        $this->driver->get_element("name=companyName")->send_keys($companyName);
        $this->driver->get_element("name=address")->clear();
        $this->driver->get_element("name=address")->send_keys($address);
        $this->driver->get_element("name=address2")->clear();
        $this->driver->get_element("name=address2")->send_keys($address2);
        $this->driver->get_element("name=city")->clear();
        $this->driver->get_element("name=city")->send_keys($city);
        $this->driver->get_element("name=state")->select_value($state);
        $this->driver->get_element("name=zipcode")->clear();
        $this->driver->get_element("name=zipcode")->send_keys($zipcode);
        $this->driver->get_element('name=phone')->clear();
        $this->driver->get_element('name=phone')->send_keys($phone);
        $this->driver->get_element('name=email')->clear();
        $this->driver->get_element('name=email')->send_keys($email);
        $this->driver->get_element("name=website_url")->send_keys($website_url);
        $this->driver->get_element("name=timezone")->select_label($timezone);
        $this->driver->get_element("name=standardDateDisplayFormat")->select_value($standardDateDisplayFormat);


        $this->driver->get_element("name=cutOff")->clear();
        $this->driver->get_element("name=cutOff")->send_keys($cutOff);
        $this->driver->get_element("name=printer_key")->clear();
        $this->driver->get_element("name=printer_key")->send_keys($printer_key);
        $this->driver->get_element("name=metalproGrouping")->clear();
        $this->driver->get_element("name=metalproGrouping")->send_keys($metalProGrouping);
        $this->driver->get_element("name=facebook_api_key")->clear();
        $this->driver->get_element("name=facebook_api_key")->send_keys($facebook_api_key);
        $this->driver->get_element("name=facebook_secret_key")->clear();
        $this->driver->get_element("name=facebook_secret_key")->send_keys($facebook_secret_key);
        $this->driver->get_element("name=terms")->clear();
        $this->driver->get_element("name=terms")->send_keys($terms);
        $this->driver->get_element("name=subdomain")->clear();
        $this->driver->get_element("name=subdomain")->send_keys($subdomain);
        
        
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text("Updated $companyName Settings");
        $updated_businesses = Business::search_aprax(array("email" => $email));
        $updated_business = $updated_businesses[0];
        
        $this->assertEquals($companyName, $updated_business->companyName, "The company name is not the expected value");
        $this->assertEquals($address, $updated_business->address, "The address is not the expected value");
        $this->assertEquals($address2, $updated_business->address2, "The address 2 is not the expected value");
        $this->assertEquals($city, $updated_business->city, "The city is not the expected value");
        $this->assertEquals($state, $updated_business->state, "The state is not the expected value");
        $this->assertEquals($zipcode, $updated_business->zipcode, "The zipcode is not the expected value");
        $this->assertEquals($phone, $updated_business->phone, "The phone is not the expected value");
        $this->assertEquals($email, $updated_business->email, "The email is not the expected value");
        $this->assertEquals($website_url, $updated_business->website_url, "The website URL is not the expected value");
        $this->assertEquals($timezone, $updated_business->timezone, "The timezone is not the expected value");
        $this->assertEquals($standardDateDisplayFormat, get_business_meta($this->test_business->businessID, "standardDateDisplayFormat"));
        $this->assertEquals($cutOff, get_business_meta($this->test_business->businessID, "cutOff"));
        $this->assertEquals($printer_key, get_business_meta($this->test_business->businessID, "printer_key"));
        $this->assertEquals($metalProGrouping, get_business_meta($this->test_business->businessID, 'metalproGrouping'));
        $this->assertEquals($facebook_api_key, $updated_business->facebook_api_key);
        $this->assertEquals($facebook_secret_key, $updated_business->facebook_secret_key);
        $this->assertEquals($subdomain, $updated_business->subdomain, "The subdomain is not the expected value");
    }
    


    /**
     * The following test verifies the functinality for changing the master business when the server is in Bizzie mode. 
     */
    public function testSetMasterBusiness()
    {
        $this->markTestSkipped("This test can not be run in parallel");
        $this->test_business->save();
        $this->driver->get_element("link text=Set Master Business")->click();
        $this->driver->get_element("name=business_id")->select_value($this->test_business->{Business::$primary_key});
        $this->driver->get_element("name=business_id")->submit();
        $this->driver->get_element("css=div.success")->assert_text("Updated master business to '{$this->test_business->companyName}'");
        $master_business = get_master_business();
        $this->assertEquals($this->test_business->{Business::$primary_key}, $master_business->{Business::$primary_key});
        
    }


    public function tearDown () {

        if ($this->test_business instanceOf Business && $this->test_business->businessID != 3)
        {
            $this->test_business->delete();
        }
        $this->CI->load->model("image_model");
        $this->CI->image_model->options = array("business_id" => $this->test_business->{Business::$primary_key});
        $this->CI->image_model->delete();
        if ($this->master_image_01 instanceOf \App\Libraries\DroplockerObjects\Image)
        {
            $this->master_image_01->delete();
        }
        parent::tearDown();
    }
}

?>
