<?php
use App\Libraries\DroplockerObjects\LockerLootConfig;

class SuperAdminLockerLootTest extends BizzieSuperAdminUnitTest
{
    private $lockerLootConfig;
    public function setUp()
    {
        parent::setUp();
        $this->login_as_bizzie_superadmin();
        $lockerLootConfigs = LockerLootConfig::search_aprax(array("business_id" => $this->business_id));
        array_map(function($lockerLootConfig) {
            $lockerLootConfig->delete();    
        }, $lockerLootConfigs);
            
        $this->driver->get_element("link text=Manage Rewards Programs")->click();
    }
    public function testManageLockerLoot()
    {
        $lockerLootStatus = 1;
        $terms = UnitTest::get_random_word(100);
        $programName = UnitTest::get_random_word();
        $percentPerOrder = rand(0,100);
        $allowToRedeem = 1;
        $redeemPercent = rand(1,100);
        $minRedeemAmount = rand(1,50);
        $referralAmountPerReward = rand(0,50);
        $referralNumberOrders = rand(0,1000);
        $referralOrderAmount = rand(0,1000);
        
        if ($lockerLootStatus == 1)
        {
            $this->driver->get_element("name=lockerLootStatus")->click();
        }
        $this->driver->get_element('name=terms')->clear();
        $this->driver->get_element("name=terms")->send_keys($terms);
        $this->driver->get_element("name=programName")->clear();
        $this->driver->get_element("name=programName")->send_keys($programName);
        $this->driver->get_element("name=percentPerOrder")->clear();
        $this->driver->get_element("name=percentPerOrder")->send_keys($percentPerOrder);
        if ($allowToRedeem == 1)
        {
            $this->driver->get_element("name=allowToRedeem")->click();
        }
        $this->driver->get_element("name=redeemPercent")->clear();
        $this->driver->get_element("name=redeemPercent")->send_keys($redeemPercent);
        $this->driver->get_element("name=minRedeemAmount")->clear();
        $this->driver->get_element("name=minRedeemAmount")->send_keys($minRedeemAmount);
        $this->driver->get_element("name=referralAmountPerReward")->clear();
        $this->driver->get_element("name=referralAmountPerReward")->send_keys($referralAmountPerReward);
        $this->driver->get_element("name=referralNumberOrders")->clear();
        $this->driver->get_element("name=referralNumberOrders")->send_keys($referralNumberOrders);
        $this->driver->get_element("name=referralOrderAmount")->clear();
        $this->driver->get_element("name=referralOrderAmount")->send_keys($referralOrderAmount);
        $this->driver->get_element("name=referralOrderAmount")->submit();
        
        $this->driver->get_element("css=div.success")->assert_text("Updated Locker Loot Configuration");
        
        $this->assertTrue((bool) $this->driver->get_element("name=lockerLootStatus")->get_attribute_value("checked"), "The locker loot status box is not checked");

            
        $this->driver->get_element("name=redeemPercent")->assert_value($redeemPercent);
        $this->assertTrue((bool) $this->driver->get_element("name=allowToRedeem")->get_attribute_value("checked"));

        $this->driver->get_element("name=minRedeemAmount")->assert_value($minRedeemAmount);
        $this->driver->get_element("name=referralAmountPerReward")->assert_value($referralAmountPerReward);
        $this->driver->get_element("name=referralNumberOrders")->assert_value($referralNumberOrders);
        $this->driver->get_element("name=referralOrderAmount")->assert_value($referralOrderAmount);     
        
        $this->CI->load->model("lockerlootconfig_model");
        
        $lockerLootConfigs = LockerLootConfig::search_aprax(array("business_id" => $this->business_id));
        $lockerLootConfig = $lockerLootConfigs[0];
        
        $this->assertEquals("active", $lockerLootConfig->lockerLootStatus);
        $this->assertEquals($terms, $lockerLootConfig->terms);
        $this->assertEquals($programName, $lockerLootConfig->programName);
        $this->assertEquals($percentPerOrder, $lockerLootConfig->percentPerOrder);
        $this->assertEquals($allowToRedeem, $lockerLootConfig->allowToRedeem);
        $this->assertEquals($redeemPercent, $lockerLootConfig->redeemPercent);
        $this->assertEquals($minRedeemAmount, $lockerLootConfig->minRedeemAmount);
        $this->assertEquals($referralAmountPerReward, $lockerLootConfig->referralAmountPerReward);
        $this->assertEquals($referralNumberOrders, $lockerLootConfig->referralNumberOrders, "The 'referralNumberOrders' property is not tehe expected value.");
        $this->assertEquals($referralOrderAmount, $lockerLootConfig->referralOrderAmount);
    }
    
    public function tearDown()
    {
        parent::tearDown();
    }
}

?>
