<?php

class SuperAdminSetupBusinessTest extends BizzieSuperAdminUnitTest{
    private $source_business;
    private $destination_business;
    private $source_emailTemplate;
    private $source_reminder;
    public function setUp() {
        parent::setUp();
        $this->login_as_bizzie_superadmin();
        $this->source_business = UnitTest::create_random_business();
        $this->destination_business = UnitTest::create_random_business();
        
        $this->source_reminder = UnitTest::create_random_reminder($this->source_business->businessID);
        $this->source_emailTemplate = UnitTest::create_random_emailTemplate($this->source_business->businessID);
        
        $this->driver->get_element("link text=Setup Business")->click();
        
    }
    /**
     * The following test verifies the funcitonality for copying data from one business to another.
     */
    public function testCopyBusinessData() {
        $this->driver->get_element("source_business_id")->select_value($this->source_business->businessID);
        $destination_businessID = $this->destination_business->businessID;
        $destination_business_menu = $this->driver->get_element("destination_business_id");
        $destination_business_menu->select_value($destination_businessID);
        $this->driver->get_element("name=acl")->click();

        $this->driver->get_element("name=products")->click();
        $this->driver->get_element("name=reminder")->click();
        $this->driver->get_element("name=email")->click();
        $this->driver->get_element("name=coupon")->click();
        $this->driver->get_element("name=reasons")->click();
        $this->driver->get_element("name=followup")->click();
        $this->driver->get_element("name=followup")->submit();
        
        $this->driver->assert_element_present("css=div.success");
        $this->CI->load->model("emailtemplate_model");
        $this->CI->load->model("reminder_model");
        
        $copied_emailTemplate = $this->CI->emailtemplate_model->get_one(array("business_id" => $this->destination_business->businessID));
        $this->assertEquals($this->source_emailTemplate->emailAction_id, $copied_emailTemplate->emailAction_id);
        $this->assertEquals($this->source_emailTemplate->emailSubject, $copied_emailTemplate->emailSubject);
        $this->assertEquals($this->source_emailTemplate->emailText, $copied_emailTemplate->emailText);
        $this->assertEquals($this->source_emailTemplate->bodyText, $copied_emailTemplate->bodyText);
        $this->assertEquals($this->source_emailTemplate->smsSubject, $copied_emailTemplate->smsSubject);
        $this->assertEquals($this->source_emailTemplate->smsText, $copied_emailTemplate->smsText);
        $this->assertEquals($this->source_emailTemplate->adminSubject, $copied_emailTemplate->adminSubject);
        $this->assertEquals($this->source_emailTemplate->adminText, $copied_emailTemplate->adminText);
        $this->assertEquals($this->source_emailTemplate->doNotSend, $copied_emailTemplate->doNotSend);
        $this->assertEquals($this->source_emailTemplate->bcc, $copied_emailTemplate->bcc);
        $this->assertEquals($this->source_emailTemplate->adminRecipients, $copied_emailTemplate->adminRecipients);
        
        
        $copied_reminder = $this->CI->reminder_model->get_one(array("business_id" => $this->destination_business->businessID));
        $this->assertEquals($this->source_reminder->couponType, $copied_reminder->couponType);
        $this->assertEquals($this->source_reminder->days, $copied_reminder->days);
        $this->assertEquals($this->source_reminder->orders, $copied_reminder->orders);
        $this->assertEquals($this->source_reminder->amountType, $copied_reminder->AmountType);
        $this->assertEquals($this->source_reminder->amount, $copied_reminder->amount);
        $this->assertEquals($this->source_reminder->expireDays, $copied_reminder->expireDays);
        $this->assertEquals($this->source_reminder->subject, $copied_reminder->subject);
        $this->assertEquals($this->source_reminder->body, $copied_reminder->body);
        $this->assertEquals($this->source_reminder->noDiscount, $copied_reminder->noDiscount);
        $this->assertEquals($this->source_reminder->description, $copied_reminder->description);
        $this->assertEquals($this->source_reminder->status, $copied_reminder->status);
        $this->assertEquals($this->source_reminder->base_id, $copied_reminder->base_id);
    }
}

?>
