<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Customer;
class DisableCustomerBizzieModeTest extends BizzieSuperAdminUnitTest
{
    public function setUp()
    {
        parent::setUp();
        $this->login_as_slave_admin();
        $this->test_customer = UnitTest::create_random_customer_on_autopay($this->slave_business->{Business::$primary_key});
    }
    public function testDisableCustomerInDetailView()
    {
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/customers/detail/{$this->test_customer->{Customer::$primary_key}}");
        $this->driver->get_element("css=.delete")->click();
        $this->driver->assert_alert_text("Are you sure you want to disable this customer?");
        $this->driver->accept_alert();
        $this->driver->get_element("css=div.success")->assert_text("Disabled customer {$this->test_customer->firstName} {$this->test_customer->lastName} ({$this->test_customer->{Customer::$primary_key}})");
        $this->test_customer->refresh();
        $this->assertEmpty($this->test_customer->email);
        $this->assertEquals($this->test_customer->autopay, 0);
    }    
    public function testDisableCustomerInSearchView()
    {
        $this->driver->get_element("link text=SEARCH")->click();
        $this->driver->get_element("name=email")->send_keys($this->test_customer->email);
        $this->driver->get_element("name=email")->submit();
        $this->driver->get_element("css=.delete")->click();
        $this->driver->assert_alert_text("Are you sure you want to disable this customer?");
        $this->driver->accept_alert();
        $this->driver->get_element("css=div.success")->assert_text("Disabled customer {$this->test_customer->firstName} {$this->test_customer->lastName} ({$this->test_customer->{Customer::$primary_key}})");
        $this->test_customer->refresh();
        $this->assertEmpty($this->test_customer->email);
        $this->assertEquals($this->test_customer->autopay, 0);
    }

    
    
    public function tearDown()
    {
        parent::tearDown();
        if ($this->test_customer instanceof \App\Libraries\DroplockerObjects\Customer)
        {
            $this->test_customer->delete();
        }
    }
}

?>
