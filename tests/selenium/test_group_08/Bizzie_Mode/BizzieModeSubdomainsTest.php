<?php
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Server_Meta;
/**
 * The following test suite verifies that slave businesses can succesfully access their customer facing websites with their respective subdomain.
 */
class BizzieModeSubdomainsTest extends BizzieSuperAdminUnitTest
{
    private $subdomain;
    private $states;
    public function setUp(){
        parent::setUp();
        
        $this->states = get_states_as_array();
        
        $subdomain1 = "wasabi"; //Note, 'wasabi.bizzie.loc' must be defined in the hosts file.
        $subdomain2 = "snowflake";
        
        $businesses = Business::search_aprax(array("subdomain" => $subdomain1));
        array_map(function($business) {
            $business->delete();
        }, $businesses);
        
        $businesses = Business::search_aprax(array("subdomain" => $subdomain2));
        array_map(function($business) {
            $business->delete();
        },$businesses);
        
        $this->business1 = new Business();
        $this->business1->companyName = UnitTest::get_random_word(2);
        $this->business1->address = UnitTest::get_random_word(3);
        $this->business1->address2 = UnitTest::get_random_word(2);
        $this->business1->city = UnitTest::get_random_word(2);
        $this->business1->subdomain = $subdomain1;
        $this->business1->state = array_rand($this->states);
        $this->business1->zipcode = rand(10000,99999);
        $this->business1->phone = 8888881234;
        $this->business1->email = unitTest::get_random_word(1) . "@example.com";
        $this->business1->blank_customer_id = 0;
        $this->business1->timezone = "America/Los_Angeles";
        $this->business1->save();
        
        $this->business2 = new Business();
        $this->business2->companyName = UnitTest::get_random_word(2);
        $this->business2->address = UnitTest::get_random_word(3);
        $this->business2->address2 = UnitTest::get_random_word(2);
        $this->business2->city = UnitTest::get_random_word(2);
        $this->business2->subdomain = $subdomain2;
        $this->business2->state = array_rand($this->states);
        $this->business2->zipcode = rand(10000,99999);
        $this->business2->phone = 8888881234;
        $this->business2->email = unitTest::get_random_word(1) . "@example.com";
        $this->business2->blank_customer_id = 0;
        $this->business2->timezone = "America/Los_Angeles";
        $this->business2->save();
        
        $this->login_as_bizzie_superadmin();
       
    }
    /**
     * The following test verifies that business subdomains map to the correct customer facing site for that business when the server is in 'bizzie' mode. 
     */
    public function testSubdomainMapping()
    {
        $this->driver->load(sprintf("http://%s.bizzie.%s", $this->business1->subdomain, $this->domain_extension)); 
        $this->driver->assert_string_present("First Time User");
        $this->driver->load(sprintf("http://%s.bizzie.%s", $this->business2->subdomain, $this->domain_extension)); 
        $this->driver->assert_string_present("First Time User");
    }
    public function tearDown()
    {
        if ($this->business1->instance)
        parent::tearDown();
        
    }
}

?>
