<?php
use App\Libraries\DroplockerObjects\Reminder;
use App\Libraries\DroplockerObjects\Business;

class BizzieModeMasterReminderTest extends BizzieSuperAdminUnitTest
{
    private $amountTypes;
    private $couponTypes;
    private $statuses;
    public function setUp()
    {
        parent::setUp();
        $this->amountTypes = array("percent" => "Percent", "dollars" => "Dollars");
        $this->couponTypes = array("newCustomer" => "New Customer", "inactiveCustomer" => "Inactive Customer", "orderTotal" => "Order Total Rewards");
        $this->statuses = array("paused", "active");
    }
    public function testAddAndDeleteMasterReminder()
    {
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
        $this->driver->get_element("link text=Manage Email Reminders")->click();
        $this->driver->get_element("link text=Add New Master Reminder")->click();
        $this->driver->assert_string_present("Add Master Reminder");
        
        $description = UnitTest::get_random_word(10);
        $days = rand(1,10);

        $amountType = array_rand($this->amountTypes);
        $couponType = array_rand($this->couponTypes);
        $amount = rand(1,35);
        $orders = rand(1,50);
        $status = $this->statuses[array_rand($this->statuses)];
        
        $this->driver->get_element("name=couponType")->select_value($couponType);
        $this->driver->get_element("name=description")->send_keys($description);
        
        if ($couponType == "orderTotal")
        {
            $this->driver->get_element("name=orders")->send_keys($orders);
        }
        else
        {
            $this->driver->get_element("name=days")->send_keys($days);
        }
        
        $this->driver->get_element("name=amountType")->select_value($amountType);
        $this->driver->get_element("name=amount")->send_keys($amount);
        $this->driver->get_element("name=status")->send_keys($status);
        $this->driver->get_element("name=submit")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("Added master reminder for all franchisees");
        
        $master_reminders = Reminder::search_aprax(array("description" => $description, "amountType" => $amountType, 
            "couponType" => $couponType, "amount" => $amount, "business_id" => $this->business_id, "status" => $status));
        
        $this->assertNotEmpty($master_reminders, "Could not find expected newly created master reminder");
        $master_reminder = $master_reminders[0];
        
        $this->CI->load->model("business_model");
        
        $slave_reminders = Reminder::search_aprax(array("base_id" => $master_reminder->{Reminder::$primary_key}, "business_id" => $this->slave_business->{Business::$primary_key}));
        $this->assertNotEmpty($slave_reminders, "Could not find expected reminders for the slave business");
        $slave_reminder = $slave_reminders[0];

        if ($couponType == "orderTotal")
        {
            $this->assertEquals($master_reminder->orders, $orders);
        }
        else
        {
            $this->assertEquals($master_reminder->days, $days);
        }
        $this->assertEquals($master_reminder->amountType, $slave_reminder->amountType);
        $this->assertEquals($master_reminder->description, $slave_reminder->description);
        $this->assertEquals($master_reminder->amount, $slave_reminder->amount);
        $this->assertEquals($master_reminder->days, $slave_reminder->days);
        $this->assertEquals($master_reminder->orders, $slave_reminder->orders);

        
        //The following code verifies the functionality for deleting a reminder
        $this->driver->get_element("link text=Manage Email Reminders")->click();
        $this->driver->get_element("id=delete-{$master_reminder->{Reminder::$primary_key}}")->click();
        $this->driver->accept_alert();
        $this->driver->get_element("css=div.success")->assert_text("Deleted master reminder and in all related franchises");
        
        $deleted_slave_reminders = Reminder::search_aprax(array("base_id" => $master_reminder->{Reminder::$primary_key}));
        $this->assertEmpty($deleted_slave_reminders, "The slave reminders were not deleted as expected");
        
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new Reminder($master_reminder->{Reminder::$primary_key});
    }
}

?>
