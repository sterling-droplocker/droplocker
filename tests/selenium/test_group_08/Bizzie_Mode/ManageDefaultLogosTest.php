<?php
use App\Libraries\DroplockerObjects\Image;
use App\Libraries\DroplockerObjects\Image_Placement;

class ManageDefaultLogosTest extends BizzieSuperAdminUnitTest 
{

    private $image1;

    public function setUp() {
        parent::setUp();
        $this->login_as_bizzie_superadmin();
        $raw_image = imagecreatetruecolor(100, 100);
        $this->image1 = "/tmp/test1.png";
        imagepng($raw_image, $this->image1);

        $this->driver->get_element("link text=Manage Default Logos")->click();
    }
    /**
     * The following test veirfies the fucntionality for deleting a logo
     */
    public function testDeleteLogo()
    {
        $image = new Image();
        $image->filename = "test1";
        $image->path = "test";
        $image->business_id = $this->business_id;
        $image->save();
        $this->driver->reload();
        $this->driver->get_element("id=image-{$image->{Image::$primary_key}}")->click();
        $this->driver->accept_alert();
        $this->driver->assert_element_present("css=div.success");
        $this->driver->assert_element_not_present("id=image-{$image->{Image::$primary_key}}");
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new Image($image->{Image::$primary_key});
    }
    /**
     * The following test verifies the functionality for adding a default logo
     */
    public function testAddDefaultLogo() 
    {
        $this->driver->assert_string_present("Manage Master Business Logos");
        $this->driver->get_element("name=userfile")->send_keys($this->image1);
        $this->driver->get_element("name=submit")->click();
        $this->driver->assert_element_present("css=div.success");
        
    }

    public function tearDown() {
        unlink($this->image1);
        parent::tearDown();
    }

}

?>
