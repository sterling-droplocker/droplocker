<?php
use App\Libraries\DroplockerObjects\Business;
class BizzieModeManageServiceTypesTest extends BizzieSuperAdminUnitTest 
{
    public function testReadOnlyForFranchiseUsers()
    {
        $this->login_as_slave_admin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Add/Edit Service Types")->click();
        $serviceTypes = $this->driver->get_all_elements('name=type[]');
        foreach ($serviceTypes as $serviceType)
        {
            $serviceType->assert_disabled();
        }
        $this->driver->assert_element_not_present("css=form");
        $this->driver->assert_element_not_present("name=submit");
    }
    /**
     * The following test verifies the functionality for updating the service types for a franchise businesses.
     */
    public function testUpdateServiceTypes()
    {
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Add/Edit Service Types")->click();
        
        //The following statements verify that unchecking service types works.
        $serviceTypes = $this->driver->get_all_elements('name=type[]'); 
        //The following loop unchecks all the service types
        foreach ($serviceTypes as $serviceType)
        {
            if ($serviceType->is_selected())
            {
                $serviceType->click();
            }
        }
        $serviceTypes[0]->click(); //Note, we must have at least one service type selected
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text("Updated Service Types for {$this->slave_business->companyName}");
        $serviceTypes = $this->driver->get_all_elements('name=type[]');
        // The following loop verifies that all the service types are unchecked after we update the settings.
        foreach ($serviceTypes as $index => $serviceType)
        {
            if ($index == 0)
            {
                $serviceType->assert_selected();
            }
            else
            {
                $serviceType->assert_not_selected();
            }
        }
        
        $serviceTypes = $this->driver->get_all_elements('name=type[]');
        foreach ($serviceTypes as $serviceType)
        {
            if (!$serviceType->is_selected())
            {
                $serviceType->select();
            }
        }
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text("Updated Service Types for {$this->slave_business->companyName}");
        $serviceTypes = $this->driver->get_all_elements('name=type[]');
        foreach ($serviceTypes as $serviceType)
        {
            $serviceType->assert_selected();
        }
        
        //The following loop unchecks all the service type boxes
        
        foreach ($serviceTypes as $serviceType)
        {
            if ($serviceType->is_selected())
            {
                $serviceType->select();
            }
        }
        
        //The following statements verify that slecting part of the available service types works
        
        $number_of_serviceTypes = count($serviceTypes);
        $number_of_selected_serviceTypes = rand(2, $number_of_serviceTypes-1);
        
        $selected_serviceTypes_keys = array_rand($serviceTypes, $number_of_selected_serviceTypes);
        
        $selected_serviceTypes = array();
        //The following loop retrieves the service types that we intend to select
        foreach ($selected_serviceTypes_keys as $selected_serviceType_key)
        {
            $selected_serviceTypes[] = $serviceTypes[$selected_serviceType_key];
        }
        
        $checked_values = array();
        //The following loop selects the intended selected service types that we are testing
        foreach ($selected_serviceTypes as $selected_serviceType)
        {
            if (!$selected_serviceType->is_selected())
            {
                $selected_serviceType->click();
            }
            
            $checked_values[] = $selected_serviceType->get_value();
        }
        
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text("Updated Service Types for {$this->slave_business->companyName}");
        
        $updated_serviceTypes = $this->driver->get_all_elements("name=serviceType[]");
        foreach ($updated_serviceTypes as $updated_serviceType)
        {
            if (in_array($updated_serviceType->get_value(), $checked_values))
            {
                $updated_serviceType->assert_selected();
            }
        }
        
        
    }
}

?>
