<?php
use App\Libraries\DroplockerObjects\DeliveryNotice;
use App\Libraries\DroplockerObjects\Business;
class BizzieModeManagePrintNoticesTest extends BizzieSuperAdminUnitTest
{
    private $master_deliveryNotice;
    public function setUp()
    {
        parent::setUp();
        $this->master_deliveryNotice = new DeliveryNotice();
        $this->master_deliveryNotice->noticeText = UnitTest::get_random_word(5);
        $this->master_deliveryNotice->barcodeText = "notice" . UnitTest::get_random_word(1);
        $this->master_deliveryNotice->business_id = $this->business_id;
        $this->master_deliveryNotice->save();
    }
    
    /** 
     * The following test verifies the functionality for adding and deleting a print notice
     */
    public function testAddAndDeleteMasterPrintNoticeAsSuperadmin()
    {
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
        $this->driver->get_element("link text=Manage Master Print Notices")->click();
        $this->driver->get_element("link text=Add Master Business Notice")->click();
        
        $barcodeText = "notice" . UnitTest::get_random_word(1);
        $noticeText = UnitTest::get_random_word(5);
        
        $this->driver->get_element("name=barcodeText")->send_keys($barcodeText);
        $this->driver->get_element("name=noticeText")->send_keys($noticeText);
        
        $this->driver->get_element("name=submit")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("Created new delivery notice for all businesses");
        
        $master_deliveryNotices = DeliveryNotice::search_aprax(array("noticeText" => $noticeText, "barcodeText" => $barcodeText, "business_id" => $this->business_id));
        $this->assertNotEmpty($master_deliveryNotices);
        $master_deliveryNotice = $master_deliveryNotices[0];
        
        
        $this->CI->load->model('business_model');
        
        
        $slave_deliveryNotices = DeliveryNotice::search_aprax(array("base_id" => $master_deliveryNotice->{DeliveryNotice::$primary_key}, "business_id" => $this->slave_business->{Business::$primary_key}));
        $slave_deliveryNotice = $slave_deliveryNotices[0];

        $this->assertEquals($master_deliveryNotice->barcodeText, $slave_deliveryNotice->barcodeText);
        $this->assertEquals($master_deliveryNotice->noticeText, $slave_deliveryNotice->noticeText);           
        
        $this->driver->get_element("id=delete-{$master_deliveryNotice->{DeliveryNotice::$primary_key}}")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("Deleted master delivery notice and all related franchisee delivery notices");
        $deleted_slave_deliveryNotices = DeliveryNotice::search_aprax(array("base_id" => $master_deliveryNotice->{DeliveryNotice::$primary_key}));
        $this->assertEmpty($deleted_slave_deliveryNotices);
        
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new DeliveryNotice($master_deliveryNotice->{DeliveryNotice::$primary_key});
    }    
    
    /**
     * The following test verifies the funcitonality for adding and deleting a franchisee delivery notice
     */
    public function testAddAndDeleteFranchiseeDeliveryNotice()
    {
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Print Notices")->click();
        $this->driver->get_element("link text=Add Notice")->click();
       
        $barcodeText = "notice" . UnitTest::get_random_word(1);
        
        $this->driver->get_element("name=barcodeText")->send_keys($barcodeText);

        $this->driver->get_element("name=submit")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("Created new delivery notice");
        $deliveryNotices = DeliveryNotice::search_aprax(array("business_id" => $this->slave_business->{Business::$primary_key}, 
            "barcodeText" => $barcodeText));
        $this->assertNotEmpty($deliveryNotices, "Could not find updated franchisee delivery notice");
        $deliveryNotice = $deliveryNotices[0];
        
        $this->driver->get_element("id=delete-{$deliveryNotice->{DeliveryNotice::$primary_key}}")->click();
        
        $this->driver->get_element("css=div.success")->assert_Text("Deleted Franchisee Notice");
    }
    
    /**
     * The following test verifies the functionality for updating a master print notice.
     */
    public function testEditMasterPrintNoticeAsSuperadmin()
    {
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
        $this->driver->get_element("link text=Manage Master Print Notices")->click();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=edit-{$this->master_deliveryNotice->{DeliveryNotice::$primary_key}}")->click();
        
        $barcodeText = "notice" . UnitTest::get_random_word(1);
        $this->driver->get_element("name=barcodeText")->clear();
        $this->driver->get_element("name=barcodeText")->send_keys($barcodeText);
        $this->driver->get_element("name=submit")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("Updated master delivery notice and all related franchisee delivery notices");
        $this->CI->load->model("business_model");
        $this->master_deliveryNotice->refresh();
        $this->assertEquals($this->master_deliveryNotice->barcodeText, $barcodeText);
        

        $slave_deliveryNotices = DeliveryNotice::search_aprax(array("base_id" => $this->master_deliveryNotice->{DeliveryNotice::$primary_key}, "business_id" => $this->slave_business->{Business::$primary_key}));
        $this->assertNotEmpty($slave_deliveryNotices, "Could not find slave delivery notice associated with the master delivery notice");
        $slave_deliveryNotice = $slave_deliveryNotices[0];
        $this->assertEquals($slave_deliveryNotice->barcodeText, $this->master_deliveryNotice->barcodeText);
        $this->assertEquals($slave_deliveryNotice->noticeText, $this->master_deliveryNotice->noticeText);
    }
    

    
    public function tearDown()
    {
        if ($this->master_deliveryNotice instanceOf DeliveryNotice)
        {
            $this->master_deliveryNotice->delete();
        }
        parent::tearDown();
        
    }

}

?>
