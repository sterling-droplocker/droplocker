<?php
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Location;
/**
 * The following test suite verifies the funcitonality of claims.
 *
 * @author ariklevy
 */
class ClaimsTest extends EmployeeUnitTest
{
    private $claim;
    private $locker;
    private $customer;
    public function setUp()
    {
        parent::setUp();
        $this->claim = new Claim();
        $this->customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});
        
        $this->claim->customer_id = $this->customer->customerID;
        $this->claim->business_id = $this->business_id;
        $this->claim->notes = "Selenium Test Claim " . UnitTest::get_random_word();
        $this->claim->locker_id = $this->locker->lockerID;
        $this->claim->orderType = "a:1:{i:0;i:2;}";
        $this->claim->active = 1;
        $this->claim->save();
        $this->login();
        $this->driver->get_element("link text=ORDERS")->click();
        $this->driver->get_element("link text=Claimed Orders")->click(); 
        
    }
    /**
     * The following test verifies the functionality for canceling a claim on a home delivery. 
     */
    public function testNoOrderHomeDelivery()
    {
        $this->location->locationType_id = 4; //This should be the home delivery type
        $this->location->save();
        
        $no_ord_link = $this->driver->get_element("id=noOrd_{$this->claim->claimID}");
        $href = $no_ord_link->get_attribute_value("href");
        $href_pieces = explode("/", $href);
        $claimID = end($href_pieces);
        $no_ord_link->click();
        
        $this->driver->get_element("css=div.success")->assert_text("On Demand Home Delivery order cancelled and customer charged missed pickup fee");
        $customerDiscounts = App\Libraries\DroplockerObjects\CustomerDiscount::search_aprax(array("extendedDesc" => "No order was left for claim {$this->claim->claimID}"));
        $this->assertNotEmpty($customerDiscounts, "Did not find expected missed pickup fee for cancelled home delivery.");
        
        $claim = new Claim($this->claim->claimID);
        $this->assertEquals(0, $claim->active, "The claim is not the expected status of inactive."); 
    }
    /**
     * The following test verifies the funcitonality for canceling an on demand delivery 
     */
    public function testNoOrderOnDemandDelivery()
    {
        $this->location->locationType_id = 2; //THis should be the locker type
        $this->location->save();
        
        $no_ord_link = $this->driver->get_element("id=noOrd_{$this->claim->claimID}");
        $href = $no_ord_link->get_attribute_value("href");
        $href_pieces = explode("/", $href);
        $claimID = end($href_pieces);
        $no_ord_link->click();
        
        $this->driver->get_element("css=div.success")->assert_text("On Demand Order Cancelled");
        
        $claim = new Claim($this->claim->claimID);
        $this->assertEquals(0, $claim->active, "The claim is not the expected status of inactive.");       
    }
    /**
     * 
     */
    public function tearDown()
    {
        if ($this->customer instanceOf \App\Libraries\DroplockerObjects\Customer)
        {
            $this->customer->delete();
        }
        if ($this->claim instanceOf \App\Libraries\DroplockerObjects\Claim)
        {
            $this->claim->delete();
        }
        parent::tearDown();
    }
}

?>
