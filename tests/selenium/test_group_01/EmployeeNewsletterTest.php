<?php
use App\Libraries\DroplockerObjects\Newsletter;

class EmployeeNewsletterTest extends EmployeeUnitTest
{
 
    public function setUp()
    {
        $this->markTestSkipped("Not completed");
        parent::setUp();
        $this->login();
    }
    /**
     * Adds a newsletter, then activated it, then delete it
     * 
     */
    public function testManageNewsletter()
    {
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/admin/newsletters");
        $this->driver->get_element("id=addNewsletterLink")->click();
        
        $this->driver->get_element("name=name")->send_keys('testNewsletter - '.date("Y-m-d"));
        $this->driver->get_element("name=subject")->send_keys('testNewsletterSubject - '.date("Y-m-d"));
        $this->driver->get_element("name=body")->clear();
        $this->driver->get_element("name=body")->send_keys('Newsletter body');
        $this->driver->get_element("name=submit")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("Newsletter has been added");

    }    
    

}