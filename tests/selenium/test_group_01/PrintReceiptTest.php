<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Image_Placement;
use App\Libraries\DroplockerObjects\Placement;
use App\Libraries\DroplockerObjects\Image;

class PrintReceiptTest extends EmployeeUnitTest
{
    private $image;
    private $image_placement;
    private $locker;
    private $location;
    private $customer;
    private $order;
    public function setUp()
    {
        parent::setUp();
        
        $this->login();
        
        $this->image = new Image();
        $this->image->path = "test";
        $this->image->filename = "test";
        $this->image->business_id= $this->business_id;
        $this->image->save();
        
        $receipt_placements = Placement::search_aprax(array("place" => "receipt"));
        if (empty($receipt_placements))
        {
            $receipt_placement = new Placement();
            $receipt_placement->place = "receipt";
            $receipt_placement->save();
        }
        else
        {
            $receipt_placement = $receipt_placements[0];
        }
        
        $this->image_placement = new Image_Placement();
        $this->image_placement->image_id = $this->image->{Image::$primary_key};
        $this->image_placement->placement_id = $receipt_placement->{Placement::$primary_key};
        $this->image_placement->save();
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});
        
        $this->order = UnitTest::create_random_order($this->business_id, $this->customer->{Customer::$primary_key}, $this->locker->{Locker::$primary_key});
    }
    public function testPrintReceiptDryClean() {   
        $url = "http://droplocker.{$this->domain_extension}/admin/printer/print_order_receipt/{$this->order->{Order::$primary_key}}";
        $this->driver->load($url);

        $this->driver->assert_string_present((string) trim($this->customer->phone));
        $this->driver->assert_string_present((string) trim($this->customer->firstName));
        $this->driver->assert_string_present((string) trim($this->location->address));

        $this->driver->assert_string_present((string) trim($this->customer->lastName));
        $now = new DateTime("now", new DateTimeZone($this->business->timezone));
        $this->driver->assert_string_present(sprintf("Date Printed: %s", $now->format("m/d/y")));
        $this->driver->assert_string_present( sprintf("Order: " . $this->order->dateCreated->format("m/d/y") . " - %s", $this->order->orderID));       
    }   

    public function tearDown()
    {
        if ($this->image instanceOf \App\Libraries\DroplockerObjects\Image)
        {
            $this->image->delete();
        }
        if ($this->image_placement instanceOf \App\Libraries\DroplockerObjects\Image_Placement)
        {
            $this->image_placement->delete();
        }
        if ($this->locker instanceOf \App\Libraries\DroplockerObjects\Locker)
        {
            $this->locker->delete();
        }
        if ($this->customer instanceOf \App\Libraries\DroplockerObjects\Customer)
        {
            $this->customer->delete();
        }
        if ($this->order instanceOf \App\Libraries\DroplockerObjects\Order)
        {
            $this->order->delete();
        }
    }
}

?>
