<?php

class FollowupCallsTest extends EmployeeUnitTest
{
    public function setUp() 
    {
        parent::setUp();
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin");
        $this->login();
        $this->driver->get_element("link text=CUSTOMERS")->click();
    }
    public function testInactiveCustomers()
    {
        $this->driver->get_element("link text=Inactive Customers")->click();
        $this->driver->assert_string_present("Inactive Customers");
    }
    public function testNewCustomers()
    {
        $this->driver->get_element("link text=New Customers")->click();
        $this->driver->assert_string_present("New Customers");
    }  
    public function testFirstOrderCustomers()
    {
        $this->driver->get_element("link text=First Order Customers")->click();
        $this->driver->assert_string_present("First Order Customers");
        
    }
    public function testOnHoldCustomers()
    {
        $this->driver->get_element("link text=Onhold Customers");
        $this->driver->assert_string_present("Onhold Customers");
    }
}

?>
