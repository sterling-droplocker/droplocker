<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\BusinessLaundryPlan;

class LaundryPlanTest extends SeleniumUnitTest
{
    private $businessLaundryPlan;
    
    public function setUp()
    {
        parent::setUp();
        $this->business_id = 3;

        $this->CI->load->model("customer_model");
        $this->CI->load->model("claim_model");
        $this->CI->load->model("order_model");
        
        //The following code creates a test user in the database.
        $this->email = "webdriver_" . UnitTest::get_random_word() . "@testing.com";
        $this->password = "webdriver";
        $this->firstName = "webdriver";
        $this->lastName = "webdriver";
        $this->address = "872 Risse Rd";
        $this->address2 = "Apt 15";
        $this->city = "North Pole";
        $this->state = "AK";
        $this->zip = "99705";
        $this->phone = "907-488-5555";
        $this->sms = "907-978-4432";
        $this->cellProvider = "ATT";

        $this->base_url = "http://laundrylocker.droplocker.{$this->domain_extension}";
        $customers = Customer::search_aprax(array("email" => $this->email));
        foreach ($customers as $customer)
        {
            $customer->delete();
        }
        
        $this->businessLaundryPlan = UnitTest::create_random_businessLaundryPlan($this->business_id);
        
        $this->driver->load($this->base_url);    
    }
    /**
     * The following test verifies the functionality for a customer to purchase a 'small' laundry plan.
     * @throws Exception 
     */
    public function testBuyLaundryPlan()
    {
        $customer = new Customer();
        $customer->firstName = $this->firstName;
        $customer->lastName = $this->lastName;
        $customer->address1 = $this->address;
        $customer->email = $this->email;
        $customer->phone = $this->phone;
        $customer->business_id = $this->business_id;
        $customer->password = $this->password;
        $customer->save();            
        $creditCard = new CreditCard();
        $creditCard->customer_id = $customer->{Customer::$primary_key};
        $creditCard->business_id = $customer->business_id;
        $creditCard->cardNumber = 4111111111111111;
        $creditCard->expMo = 2;
        $creditCard->expYr = 2022;
        $creditCard->csc = 437;
        $creditCard->save();

        $this->login();
        if ($this->browser == "chrome")
        {
            $this->driver->load("http://laundrylocker.droplocker.{$this->domain_extension}/main/plans");
        }
        else
        {
            $this->driver->get_element("link text=Laundry Plans")->click();
        }
        $this->driver->get_element("id={$this->businessLaundryPlan->{BusinessLaundryPlan::$primary_key}}")->click(); //THis is the small laundry plan
        $this->driver->assert_string_present($this->businessLaundryPlan->description); //Note, Plan Name in tyhe customer view is actually the plan description. 
        $this->driver->assert_string_present("{$this->businessLaundryPlan->pounds} Lbs");
        $this->driver->assert_string_present("{$this->businessLaundryPlan->days} Days");
        $this->driver->assert_string_present( money_format('%.2n', $this->businessLaundryPlan->price ) );
        $this->driver->assert_string_present("By clicking below you agree to the laundry plan terms and conditions.");
        $this->driver->get_element("name=submit")->click(); //THis is the I agree Sign mne Up! button.
        $this->driver->assert_string_present("Your Order Is Complete");
        $this->driver->assert_string_present($this->businessLaundryPlan->description);
        
    }
        /**
     * Note, this function assumes that the account already exists in the database.
     */
    public function login() 
    {
        //$this->driver->get_element("id=login_logout")->click();
        $this->driver->get_element("name=username")->send_keys($this->email);
        $this->driver->get_element("name=password")->send_keys($this->password);
        $this->driver->get_element("name=password")->submit();

        sleep(5);
        $this->driver->get_element("css=div.alert-success")->assert_text_contains("Welcome Back"); //This code verifies that the "Welcome Back" flash message is present when an existing customer logs in.
    }
    
    public function tearDown()
    {
        parent::tearDown();
        if ($this->businessLaundryPlan instanceOf \App\Libraries\DroplockerObjects\BusinessLaundryPlan)
        {
            $this->businessLaundryPlan->delete();
        }
    }
}
