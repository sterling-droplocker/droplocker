<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Customer;
/**
 * Description of EmployeeCustomerProfileTest
 *
 * @author ariklevy
 */
class EmployeeCustomerProfileTest extends EmployeeUnitTest
{
    private $customer;
    public function setUp()
    {
        parent::setUp();
        $this->customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        //$this->customer = new Customer(27693);
        $this->login();
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/customers/detail/{$this->customer->customerID}");
    }
    /**
     * The following test verifies that the customer profile displays the correct information. 
     */
    public function testCustomerProfile()
    {
        
        $this->driver->assert_string_present($this->customer->email);
        if (!empty($this->customer->firstName))
        {
            $this->driver->assert_string_present(ucwords(trim($this->customer->firstName . " " . $this->customer->lastName)));
        }
        if (!empty($this->customer->phone))
        {
            $this->driver->assert_string_present(trim($this->customer->phone));
        }
        if (!empty($this->customer->address1))
        {
            $this->driver->assert_string_present(trim($this->customer->address1));
        }
        if (!empty($this->customer->address2))
        {
            $this->driver->assert_string_present(trim($this->customer->address2));
        }

        $bags = Bag::search_aprax(array("customer_id" => $this->customer->customerID));
        if ($bags)
        {
            foreach ($bags as $bag)
            {
                $this->driver->assert_string_present($bag->bagNumber);
            }
        }
        $this->CI->load->model("customer_model");
        
        $orders = $this->CI->customer_model->get_order_history($this->customer->customerID, true);
        if ($orders)
        {
            foreach ($orders as $order)
            {
                $this->driver->assert_string_present($order->orderID);
                if (!empty($order->bagNumber))
                {
                    $this->driver->assert_string_present($order->bagNumber);
                }
            }
            $number_of_orders = sprintf("# Orders: %s", count($orders));
            $this->driver->assert_string_present($number_of_orders);
        }
    }
    /**
     * The following test verifies the functionality for reassigning a bag to the blank customer 
     */
    public function testDeleteBag()
    {
        $bags = Bag::search_aprax(array("customer_id" => $this->customer->customerID));
        //If our random customer has no bag, then we create a bag for the customer.
        if (empty($bags))
        {
            $bag = new Bag();
            $bag->customer_id = $this->customer->customerID;
            $bag->business_id = $this->customer->business_id;
            $bag->save();
            $this->driver->reload();
            $bags = Bag::search_aprax(array("customer_id" => $this->customer->customerID));
        }
        $bag = $bags[0];
        $this->driver->get_element("delete-{$bag->bagID}")->click();
        $this->driver->assert_alert_text("Are you sure you want to reassign this bag to the blank customer?");
        $this->driver->accept_alert();
        sleep(3);
        $this->driver->get_element("css=div.success")->get_text("Reassigned bag '{$bag->bagNumber}' to the designated Blank Customer");
    }
}

?>
