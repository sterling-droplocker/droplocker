<?php

class WebsiteConfigurationTest extends EmployeeUnitTest
{
     /**
     * The following test verifies that updating the website settings successfully when the server is in Droplocker mode.
     */
    public function testUpdateWebsiteSettings()
    {
        $this->login();
        $title = UnitTest::get_random_word(5);
        $javascript = UnitTest::get_random_word(5);
        $stylescript = UnitTest::get_random_word(5);
        $header = UnitTest::get_random_word(5);
        $footer = UnitTest::get_random_word(5);
        
        $this->driver->get_element("link text=WEBSITE")->click();
        $this->driver->get_element("link text=Manage Customer Website Template")->click();
        $this->driver->get_element("name=title")->clear();
        $this->driver->get_element("name=title")->send_keys($title);
        $this->driver->get_element("name=javascript")->clear();
        $this->driver->get_element("name=javascript")->send_keys($javascript);
        $this->driver->get_element("name=stylescript")->clear();
        $this->driver->get_element("name=stylescript")->send_keys($stylescript);
        $this->driver->get_element("name=header")->clear();
        $this->driver->get_element("name=header")->send_keys($header);
        $this->driver->get_element("name=footer")->clear();
        $this->driver->get_element("name=footer")->send_keys($footer);
        $this->driver->get_element("name=footer")->submit();
        
        $websites = \App\Libraries\DroplockerObjects\Website::search_aprax(array("business_id" => $this->business->{\App\Libraries\DroplockerObjects\Business::$primary_key}));
        $this->assertNotEmpty($websites);
        $website = $websites[0];
        
        $this->assertEquals($title, $website->title, "The master title is not the expected value");
        $this->assertEquals($javascript, $website->javascript, "The master javascript is not the expected value");
        $this->assertEquals($stylescript, $website->stylescript, "The master stylescript is not the expected value");
        $this->assertEquals($header, $website->header, "The master header is not the expected value");
        $this->assertEquals($footer, $website->footer, "The master footer is not the expected value");

    }
}

?>
