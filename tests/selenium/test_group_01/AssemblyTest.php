<?php
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Bag;
class AssemblyTest extends EmployeeUnitTest
{
    private $order;
    private $orderItem;
    private $barcode;
    private $item;
    public function setUp()
    {
        parent::setUp();
        $this->order = UnitTest::create_random_order();
        $this->orderItem = UnitTest::create_random_orderItem($this->order->orderID);
        $this->item = new Item($this->orderItem->item_id, true);
        $this->barcode = UnitTest::create_random_barcode($this->business_id, $this->orderItem->item_id);
        $this->login();
    }
    
    /**
     * The following test verifies the funcitonality for printing assembly notes. 
     */
    public function testPrintAssembly()
    {
        $this->driver->get_element("link text=ORDERS")->click();
        
        $this->driver->get_element("link text=Manual Assembly")->click();
        
        $this->driver->get_element("barcode")->send_keys($this->barcode->barcode);
        $this->driver->get_element("name=submit")->click();
        try 
        {
            $this->driver->assert_string_present($this->order->orderID); 
            $this->driver->assert_string_present($this->order->relationships['Customer'][0]->firstName);
            $this->driver->assert_string_present($this->order->relationships['Customer'][0]->lastName);
            if (!empty($this->order->relationships['Bag'][0]->bagNumber))
            {
                $this->driver->assert_string_present($this->order->relationships['Bag'][0]->bagNumber);
            }
            $this->driver->assert_string_present($this->barcode->barcode);
        }
        catch (\Exception $e)
        {
            $image = $this->driver->get_screenshot();
            $filename = "/tmp/selenium_screenshot.png";
            $body = "
                <pre>
                    URL: {$this->driver->get_url()}
                    barcode: {$barcode->barcode}
                    {$e->getMessage()}
                    {$e->getFile()} : {$e->getLine()} "

                    . htmlspecialchars($e->xdebug_message) . "

                </pre>
            ";
            file_put_contents($filename, $image);
            send_email(SYSTEMEMAIL, SYSTEMFROMNAME, "development@droplocker.com", "Selenium Error (Test Name: {$this->getName()}) (Browser: {$this->browser})", $body, array(), array(), $filename);
            $this->tearDown();
            $this->fail($e);
        }
        

    }         
    
    public function tearDown()
    {
        if ($this->order instanceOf \App\Libraries\DroplockerObjects\Order)
        {
            $this->order->delete();
        }
        if ($this->orderItem instanceOf \App\Libraries\DroplockerObjects\OrderItem)
        {
            $this->orderItem->delete();
        }
        if ($this->barcode instanceOf \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode->delete();
        }
        parent::tearDown();
    }
}

?>
