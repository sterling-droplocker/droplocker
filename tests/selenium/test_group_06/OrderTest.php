<?php

use \App\Libraries\DroplockerObjects\Bag;
use \App\Libraries\DroplockerObjects\Customer;
use \App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\OrderItem;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Customer_Item;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Business_Employee;

class OrderTest extends EmployeeUnitTest {

    private $test_customer;
    private $product_process;
    private $product;
    private $order;

    public function setUp() {
        parent::setUp();

        $this->test_customer = UnitTest::create_random_customer_on_autopay($this->business_id);

        $this->order = UnitTest::create_random_order($this->business_id, $this->test_customer->{Customer::$primary_key});

        $this->product = UnitTest::create_random_product($this->business_id);
        $this->product->productType = "preference";
        $this->product->sortOrder = 1;
        $this->product->save();

        //For some reason, ther is * code that hcecks for a product process with a process ID of 0 for some unknown reason.
        $this->product_process = new Product_Process();
        $this->product_process->product_id = $this->product->{Product::$primary_key};
        $this->product_process->process_id = 0;
        $this->product_process->price = 5.00;
        $this->product_process->active = 1;
        $this->product_process->save();
        $this->login();
    }

    /**
     * The following test verifies the functionality for creating and deleting a location.
     */
    public function testCreateAndApproveAndCleanWashAndFoldOrder() {
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/wf_processing");

        //Note, an order is considered to be ready for WF approval if the order is in picked up status and the order notes start with an integer.
        $this->order->notes = 1;

        $this->order->orderStatusOption_id = 1; //This is the picked up status.

        UnitTest::create_random_orderItem($this->order->{Order::$primary_key});

        $this->order->save();

        //Note, a bug in Selenium prevents it from seeing the Approve WF Orders link.
        //$this->driver->get_element("link text=Approve WF Orders")->click();

        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/wf_approve");

        $this->driver->get_element("id={$this->order->{Order::$primary_key}}_checkbox")->click();
        $this->driver->get_element("id={$this->order->{Order::$primary_key}}_notes")->assert_value($this->order->relationships['Customer'][0]->wfNotes); //The notes box should be prefilled with the customer notes.
        $this->driver->get_element("css=input[type='submit']")->click();
        $this->driver->assert_element_not_present("id={$this->order->orderID}_checkbox");

        $this->order = new Order($this->order->orderID, true);
        $this->assertEquals(2, $this->order->orderStatusOption_id, "The order status option is not the expected option 'Waiting for Service'");

        $this->driver->get_element("link text=Clean WF Orders")->click();
        $this->driver->get_element("id=bagNumber")->send_keys($this->order->relationships['Bag'][0]->bagNumber);
        $this->driver->get_element("name=find")->click();
        $user = "Test User";
        $weightIn = 5;
        if ($this->driver->is_element_present("name=user")) 
        {
            $this->driver->get_element("name=user")->send_keys($user);
        }
        if ($this->driver->is_element_present("name=weightIn")) {
            $this->driver->get_element("name=weightIn")->send_keys($weightIn);
        }

        $this->driver->get_element("name=update")->click();
        sleep(5);
        $wfNotes = trim($this->order->relationships['customer'][0]->wfNotes);

        if (!empty($wfNotes)) {
            $this->driver->assert_string_present($wfNotes);
        }
        $this->driver->assert_string_present($this->order->relationships['Bag'][0]->bagNumber);
        $this->driver->assert_string_present(trim($this->order->relationships['Customer'][0]->firstName));
        $this->driver->assert_string_present(trim($this->order->relationships['Customer'][0]->lastName));
    }

    /**
     * The following test verifies the fucntionality for capturing an order.
     */
    public function testCaptureOrder() {

        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);

        //The following statements verify that capturing the order works as expected.
        $this->driver->get_element("css=form#capture_payment input[type='submit']")->click();

        $this->driver->assert_alert_text("Are you sure you want to CAPTURE this order?");
        $this->driver->accept_alert();
        sleep(5); //Note, this sleep statement is necessary because selenium does not seem to wait for the page to load after the javascript redirect.
        $this->driver->get_element("css=div.success")->assert_text("Funds captured for order {$this->order->orderID}");

        //The following statements verify that uncpaturing the order works as expected.
        $this->driver->get_element("uncapture_payment")->click();


        $this->driver->assert_alert_text("Are you sure you want to uncapture this order?");
        $this->driver->accept_alert();
        $this->driver->get_element("css=div.success")->assert_text("Successfully uncaptured order {$this->order->orderID}");
    }

    /**
     * The following test verifies that the 'gut' functionality works as expected. 
     */
    public function testGutOrder() {
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}");
        //The follwing statements verify that the gut order functionality works.
        $this->driver->get_element("gut_order")->click();
        $this->driver->accept_alert();
        sleep(5);
        $this->driver->get_element("css=div.success")->assert_text("Successfully gutted order");
    }

    /**
     * The following test verifies that the order history table shows the correct entries for an open order. 
     */
    public function testOrderHistory() {
        for ($x = 0; $x < 3; $x++) {
            UnitTest::create_random_orderStatus($this->order->{Order::$primary_key});
        }

        for ($x = 0; $x < 3; $x++) {
            UnitTest::create_random_transaction($this->order->{Order::$primary_key});
        }

        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);



        $history_items = $this->order->get_history();
        //The following loop verifies that the order history appears in the view.
        foreach ($history_items as $history_item) {
            // Note, I have not figured out a way to validate transaction notes without bogus Selenium test failures.
            if (!strstr($history_item['name'], "Paid")) {
                $history_item = $this->prepare_string_for_search($history_item['name']);
                if (!empty($history_item)) {
                    $this->driver->assert_string_present($history_item);
                }
                $note = $this->prepare_string_for_search($history_item['note']);
                if (!empty($note)) {
                    $this->driver->assert_string_present($note);
                }
            }
            $locker = $this->prepare_string_for_search($history_item['locker']);
            if (!empty($locker)) {
                $this->driver->assert_string_present($locker);
            }

            $employee = $this->prepare_string_for_search($history_item['employee']);
            if (!empty($employee)) {
                $this->driver->assert_string_present($employee);
            }
        }
    }

    public function testOrderNotes() {
        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);

        //The following statements vierfy the functionality for updating and viewing the order notes.
        $order_notes = "Test Order Notes";
        $assembly_notes = "Test Assembly Notes";
        $this->driver->get_element("id=order_notes")->clear();
        $this->driver->get_element("id=order_notes")->send_keys($order_notes);


        $this->driver->get_element("id=assembly_notes")->clear();
        $this->driver->get_element("id=assembly_notes")->send_keys($assembly_notes);
        $this->driver->get_element("name=order_notes_submit")->click();


        $this->driver->get_element("id=order_notes")->assert_value($order_notes);
        $this->driver->get_element("id=assembly_notes")->assert_value($assembly_notes);

        $this->driver->get_element("css=div.success")->assert_text("Updated Order Notes");
    }

    public function tearDown() {
        if ($this->order instanceof \App\Libraries\DroplockerObjects\Order) {
            $this->order->delete();
        }
        if ($this->product instanceof \App\Libraries\DroplockerObjects\Product) {
            $this->product->delete();
        }
        if ($this->product_process instanceOf \App\Libraries\DroplockerObjects\Product_Process) {
            $this->product_process->delete();
        }
        parent::tearDown();
    }

}

?>
