<?php
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\OrderItem;
use App\Libraries\DroplockerObjects\Product_process;
use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Employee;

class CheckInWorkflowTest extends EmployeeUnitTest
{
    private $barcode1;
    private $barcode2;
    
    private $barcode3;
    private $barcode4;
    private $barcode5;
    private $order;
    private $customer;
    private $bag;
    private $location;
    private $locker;
    private $product;
    private $productCategory;
    private $product_process;
    public function setUp() {
        parent::setUp();
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});
        $this->order = UnitTest::create_random_order($this->business_id, null, $this->locker->{Locker::$primary_key});
        $this->customer = new Customer($this->order->customer_id);
        $this->customer->hold = 0;
        $this->customer->invoice = 0;
        $this->customer->autoPay = 1;
        $this->customer->location_id = $this->location->{Location::$primary_key};
        $word = UnitTest::get_random_word();
        $this->customer->email = "webdriver_test_$word";
        $this->customer->save();
        $this->bag = new Bag($this->order->bag_id);
        
        $this->barcode1 = new stdClass();
        $this->barcode1->barcode = rand(10000000, 99999999);
        
        $this->CI->load->model("barcode_model");
        $this->CI->barcode_model->delete(array("barcode" => $this->barcode1->barcode));
        
        $this->barcode2 = new stdClass();
        $this->barcode2->barcode = rand(10000000, 99999999);
        $this->CI->barcode_model->delete(array("barcode" => $this->barcode2->barcode));
        
        
        $this->barcode3 = UnitTest::create_random_barcode($this->business_id, null, $this->order->customer_id);
        $this->barcode4 = UnitTest::create_random_barcode($this->business_id, null, $this->order->customer_id);
        $this->barcode5 = UnitTest::create_random_barcode($this->business_id, null, $this->order->customer_id);
        
        $this->productCategory = UnitTest::create_random_productCategory($this->business_id, 3); //"3" is expected to be the dryclean module.
        $this->product = UnitTest::create_random_product($this->business_id, $this->productCategory); 
        
        $product_processes = Product_Process::search_aprax(array("product_id" => $this->product->{Product::$primary_key}), true);
        $this->product_process = $product_processes[0];
        $this->login();
    }      
     /**
     * The following test verifies the functionality for scanning in a bag to a order, gutting the order, adding items to an order, adding a wash and fold product to the order, and capturing an order.
     * @throws Exception 
     */
    public function testScanBagAndAddItems()
    {   
        $this->driver->get_element("link text=ORDERS")->click();
        $this->driver->get_element("link text=Quick Scan Bag")->click();

        $this->driver->get_element("id=bag_number")->send_keys($this->bag->bagNumber);
        $this->driver->get_element("submit")->click();

        $this->driver->set_implicit_wait(5000);

        //The following statements check for any warning dialogs, and deal with the warning dialogs accordingly.
        if ($this->driver->is_element_present("css=.order_notes_warning_checkbox"))
        {
            $warning_checkboxes = $this->driver->get_all_elements("css=.order_notes_warning_checkbox");
            foreach ($warning_checkboxes as $warning_checkbox)
            {
                $warning_checkbox->click();
            }
        }
        if ($this->driver->is_element_present("css=button.ui-button"))
        {
            $this->driver->get_element("css=button.ui-button")->click();
        }
        if ($this->driver->is_element_present("link text=enter the order with no customer"))
        {
            $this->driver->get_element("link text=enter the order with no customer")->click();
        }
        $this->driver->set_implicit_wait($this->default_implicit_wait);

        //This statement verifies that the order locker is displayed correctly in the view.
        $this->driver->get_element("id=locker_holder")->assert_text($this->locker->lockerName); 

        //This statement verifies that the order status option is displayed correctly in the view.
        $this->driver->get_element("id=status")->assert_value($this->order->orderStatusOption_id);

        //This statement verifies that the customer is displayed correctly in the view.
        $this->driver->get_element("id=user_holder")->assert_text(sprintf("%s %s", trim($this->customer->firstName), trim($this->customer->lastName)));

        //This statement verifies that the location address is displayed correctly in the view.
        $this->driver->get_element("id=location_holder")->assert_text(sprintf("%s %s, %s", $this->location->address, $this->location->city, $this->location->state)); 

        //The following loop verifies that the order items' properties appear correctly in the view.
        foreach ($this->order->relationships['OrderItem'] as $orderItem)
        {
            $this->driver->get_element("name=qty[{$orderItem->{OrderItem::$primary_key}}][qty]")->assert_value($orderItem->qty);
            $this->driver->get_element("name=unitPrice[{$orderItem->{OrderItem::$primary_key}}][unitPrice]")->assert_value($orderItem->unitPrice);
            $this->driver->get_element("name=notes[{$orderItem->{OrderItem::$primary_key}}][notes]")->assert_value($orderItem->notes);
            $this->driver->get_element("name=supplier_id[{$orderItem->{OrderItem::$primary_key}}][supplier_id]")->assert_value($orderItem->supplier_id);
        }

        $this->driver->get_element("barcode")->send_keys($this->barcode1->barcode);
        $this->driver->get_element("barcode_submit")->click();

        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("name=product_id")->select_value($this->product->productID);
        
        //$this->driver->get_element("id=process-1")->click();
        $this->wait_for_ajax_to_complete();
        //The follwoing statements select the upcharges for this item.
        $barcode1_notes = "Test Notes";

        $this->driver->get_element("link text=medium")->click(); // This statement selects the startch type.
        $this->driver->get_element("name=notes")->send_keys($barcode1_notes);

        $this->driver->get_element("create_item_button")->click();
        
        /** THis check is disabled because of a bug in selenium 
        sleep(20); //This statement is used to workaround sloppy javascript code for adding an item to an order.
        $this->driver->get_element("css=div.message")->assert_text("Created Barcode '$barcode1' and Added it to Order '{$order->orderID}'");
         * 
         */
        sleep(5);
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("name=barcode"); //Note, we need to wait for the item to be added to the database before we queyr for it.

        $this->CI->db->select("orderItemID, barcode.item_id, product_id, product_processID, orderItemID");
        $this->CI->db->join("orderItem", "barcode.item_id=orderItem.item_id");
        $this->CI->db->join("item", "itemID=orderItem.item_id");
        $this->CI->db->join("product_process", "product_processID=item.product_process_id");
        $get_item_information_query = $this->CI->db->get_where("barcode", array("barcode" => $this->barcode1->barcode));
        
        $item_information = $get_item_information_query->row();

        $orderItem = new OrderItem($item_information->orderItemID);
        $product = new Product($item_information->product_id, false);
        $product_process = new Product_Process($item_information->product_processID);

        $this->driver->get_element("xpath=//*[@id='row-{$orderItem->{OrderItem::$primary_key}}']/td[1]")->assert_text(sprintf("%s (\$%s)", $product->name, $orderItem->unitPrice) );

        $this->driver->get_element("name=unitPrice[{$orderItem->{OrderItem::$primary_key}}][unitPrice]")->assert_value($orderItem->unitPrice);

        $this->driver->get_element("name=barcode")->send_keys($this->barcode2->barcode);

        $this->driver->get_element("barcode_submit")->click();

        $this->wait_for_ajax_to_complete();
        
        $this->driver->get_element("name=product_id")->select_value($this->product->{Product::$primary_key});
        $this->driver->get_element("id=create_item_button")->click();

        sleep(10);
        //Note, this test is disabled because of a bug in Selenium.
        //$this->driver->get_element("css=div.success")->assert_text("Created Barcode '$barcode2' and Added it to Order '{$order->orderID}'");
        $this->driver->get_element_reload("quickwf")->click();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("add_item_button")->click();

        //THe following statements verify the functionality for editing an exisiting item in an order.
        $this->driver->get_element("link text={$this->barcode2->barcode}")->click();

        $barcode2_notes = "Test 2 Notes";      

        //The following statements select custom properties for this item.

        $this->driver->get_element("name=notes")->send_keys($barcode2_notes);       

        $this->driver->get_element("id=edit_item_order_button")->click();   

        $this->driver->accept_alert();

        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("partial link text={$this->barcode2->barcode}")->click();

        $this->CI->db->select("orderItemID, itemID, product_id, product_processID, orderItemID");
        $this->CI->db->join("orderItem", "barcode.item_id=orderItem.item_id");
        $this->CI->db->join("item", "itemID=orderItem.item_id");
        $this->CI->db->join("product_process", "product_processID=item.product_process_id");
        $get_item_information_query = $this->CI->db->get_where("barcode", array("barcode" => $this->barcode2->barcode));
        $item_information = $get_item_information_query->row();

        $product = new Product($item_information->product_id);
        $item = new Item($item_information->itemID, true);

        $this->driver->get_element("name=notes")->assert_value($barcode2_notes);

        $this->driver->go_back();

        //The following statements verify that processing the next order works as expected.
        $this->driver->get_element("link text=Process Next Order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
    }        
    
    
    /**
     * The following test verifies the fucnitonality for scanning a bag in to create an order and using the 'rapid' interface for adding items to an order
     * @throws \Database_Exception 
     */
    public function testScanBagAndRapidAddItems()
    {
        $this->driver->get_element("link text=ORDERS")->click();
        sleep(3);
        $this->driver->get_element("link text=Quick Scan Bag")->click();

        $this->driver->get_element("id=bag_number")->send_keys($this->bag->bagNumber);
        $this->driver->get_element("submit")->click();

        $this->driver->get_element("id=barcode")->send_keys($this->barcode3->barcode);
        $this->driver->get_element("barcode_submit")->click();            

        $this->driver->get_element("css=div.success")->assert_text("Added Barcode '{$this->barcode3->barcode}' to Order");
        //The following statements verify that the Barcode 3 properties appear correctly in the edit order item view.
        $this->CI->db->select("orderItemID, itemID, product_id, product_processID, orderItemID");
        $this->CI->db->join("orderItem", "barcode.item_id=orderItem.item_id");
        $this->CI->db->join("item", "itemID=orderItem.item_id");
        $this->CI->db->join("product_process", "product_processID=item.product_process_id");
        $get_item_information_query = $this->CI->db->get_where("barcode", array("barcode" => $this->barcode3->barcode));
        
        $item_information = $get_item_information_query->row();

        $product_process = new Product_Process($item_information->product_processID, true);

        $this->driver->assert_string_present($product_process->relationships['Product'][0]->name);
        $this->driver->assert_string_present($product_process->relationships['Process'][0]->name);
        $item = new Item($item_information->itemID, true);
        if (!empty($item->crease))
        {
            $this->driver->assert_string_present($item->crease);
        }
        if (!empty($item->note))
        {
            $this->driver->assert_string_present($item->note);
        }

        $this->driver->get_element("name=barcode")->send_keys($this->barcode4->barcode);
        $this->driver->get_element("name=barcode")->submit();
        $this->driver->get_element("css=div.success")->assert_text("Added Barcode '{$this->barcode4->barcode}' to Order");

        $this->driver->get_element("name=barcode")->send_keys($this->barcode5->barcode);
        $this->driver->get_element("name=barcode")->submit();     

        $this->driver->get_element("css=div.success")->assert_text("Added Barcode '{$this->barcode5->barcode}' to Order");

        $this->driver->get_element("link text=Return to Order")->click();


        $this->driver->assert_element_present("link text={$this->barcode4->barcode}");
        $this->driver->assert_element_present("link text={$this->barcode5->barcode}");
        
        //The following statements verify that processing the next order works as expected.
        $this->driver->get_element("link text=Process Next Order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();

   }    
    
    public function tearDown()
    {
        if ($this->barcode1 instanceof \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode1->delete();
        }
        
        if ($this->barcode2 instanceof \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode2->delete();
        }
        
        if ($this->barcode3 instanceof \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode3->delete();
        }
        
        if ($this->barcode4 instanceof \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode4->delete();
        }
        
        if ($this->barcode5 instanceof \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode5->delete();
        }
        if ($this->order instanceof \App\Libraries\DroplockerObjects\Order)
        {
            $this->order->delete();
        }
        if ($this->customer instanceof \App\Libraries\DroplockerObjects\Customer)
        {
            $this->customer->delete();
        }
        if ($this->bag instanceof \App\Libraries\DroplockerObjects\Bag)
        {
            $this->bag->delete();
        }
        if ($this->location instanceof \App\Libraries\DroplockerObjects\Location)
        {
            $this->location->delete();
        }
        if ($this->product instanceof \App\Libraries\DroplockerObjects\Product)
        {
            $this->product->delete();
        }
        if ($this->product_process instanceof \App\Libraries\DroplockerObjects\Product_Process)
        {
            $this->product_process->delete();
        }
        parent::tearDown();
    }
}

?>
