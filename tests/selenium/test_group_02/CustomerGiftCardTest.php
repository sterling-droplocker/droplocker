<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Bag;

class CustomerGiftCardTest extends CustomerUnitTest
{
    
    protected $email;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $address;
    protected $address2;
    protected $city;
    protected $state;
    protected $phone;
    protected $sms;
    protected $cellProvider;
    
    protected $customerID;
    
    
    public function setUp()
    {
        parent::setUp();
        
        $this->business_id = 3;
        
        // place credit card processor to test mode or else the test credit card will fail
        update_business_meta($this->business_id, 'paypro_mode', 'test');
        update_business_meta($this->business_id, "paypro_test_token", "3374397FCE62500F388AEDFF8D56AB6DCFC1259444B0C3F6E8A7492787DA5AE49A56BB50CDF186E798");
        
        $this->business = new Business($this->business_id);
        
        $this->CI->load->model("customer_model");
        $this->CI->load->model("claim_model");
        $this->CI->load->model("order_model");
        
        //The following code creates a test user in the database.
         
        $word = UnitTest::get_random_word(1);
        $this->email = "webdriver_$word@testing.com";
        $this->password = "webdriver";
        $this->firstName = "webdriver_$word";
        $this->lastName = "webdriver_$word";
        $this->address = "872 Risse Rd";
        $this->address2 = "Apt 15";
        $this->city = "North Pole";
        $this->state = "AK";
        $this->zip = "99705";
        $this->phone = "907-488-5555";
        $this->sms = "907-978-4432";
        $this->cellProvider = "ATT";
        $this->cardNumber = '1234';
        $this->month = '4';
        $this->year = '16';
        
        $customers = \App\Libraries\DroplockerObjects\Customer::search_aprax(array("email" => $this->email));
        foreach ($customers as $customer)
        {
            $customer->delete();
        
        }
        
    }
    
    
    /**
     * TODO: 
     * add span with id attr to card on file values to test that the proper card is stored
     * add span with id for expire data too
     * 
     * This view needs to be update to not use javascript for updating the kiosk access
     */
    public function testBuyGiftCardLaundryLocker(){
        
        //The following code creates the webdriver customer account.
        $this->customerID = $this->CI->customer_model->new_customer($this->email, $this->password, $this->business_id);
       
        $this->login($this->email, $this->password);
        if ($this->browser == "chrome")
        {
            $this->driver->load("{$this->base_url}/account/programs/gift_cards");
        }
        else
        {
            $this->driver->get_element("link text=Gift Cards")->click();
        }
        
        $this->driver->get_element("css=div.alert-error")->assert_text_contains("You must have a credit card on file before purchasing a gift card");
        $this->driver->get_element("name=address1")->send_keys($this->address);    
        $this->driver->get_element("name=cardNumber")->send_keys("4217651111111119"); //This is the test credit card fdor paypro.
        $this->driver->get_element("name=city")->send_keys("San Francisco");
        $this->driver->get_element("name=expMo")->select_value("02");
        $this->driver->get_element("name=expYr")->select_value("2022");
        $this->driver->get_element("name=csc")->send_keys("437");
        $this->driver->get_element("name=submit")->click();

        
        // back to account page
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account");
        }
        else {
            $this->driver->get_element("link text=Account Home")->click();
        }
        
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account/programs/gift_cards");
        }
        else {
            $this->driver->get_element("link text=Gift Cards")->click();
        }
        
        $this->driver->get_element("name=to[]")->send_keys("*");
        $this->driver->get_element("name=email[]")->send_keys("411matt@gmail.com");
        $this->driver->get_element("name=from")->send_keys($this->firstName." ".$this->lastName);
        $this->driver->get_element("name=amount[]")->send_keys("10.00");
        $this->driver->get_element("name=submit")->click();
        
        $this->driver->set_implicit_wait(20000);
        $this->wait_for_ajax_to_complete();
        
        $this->driver->get_element("link text=Purchase Now")->click();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("css=div.span7 div.span7 h2")->assert_text("Thank You For Ordering A Gift Card!");
    }

}