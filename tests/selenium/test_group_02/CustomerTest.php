<?php

use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\LockerStyle;
use App\Libraries\DroplockerObjects\LockerLockType;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;

class CustomerTest extends CustomerUnitTest
{

    protected $email;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $address;
    protected $address2;
    protected $city;
    protected $state;
    protected $phone;
    protected $sms;
    protected $cellProvider;
    
    protected $customerID;    
    
    protected $location;
    protected $locker;
    protected $inProcess_locker;
    
    public function setUp()
    {
        parent::setUp();
        $this->business_id=3;
        
        // place credit card processor to test mode or else the test credit card will fail
        update_business_meta($this->business_id, 'paypro_mode', 'test');
        update_business_meta($this->business_id, "paypro_test_token", "3374397FCE62500F388AEDFF8D56AB6DCFC1259444B0C3F6E8A7492787DA5AE49A56BB50CDF186E798");
        
        $this->business = new Business($this->business_id);
        
        $this->CI->load->model("customer_model");
        $this->CI->load->model("claim_model");
        $this->CI->load->model("order_model");
        
        //The following code creates a test user in the database.
       
        $word = UnitTest::get_random_word(1);
        $this->email = "webdriver_$word@testing.com";
        $this->password = "webdriver";
        $this->firstName = "webdriver_$word";
        $this->lastName = "webdriver_$word";
        $this->address = "872 Risse Rd";
        $this->address2 = "Apt 15";
        $this->city = "North Pole";
        $this->state = "AK";
        $this->zip = "99705";
        $this->phone = "907-488-5555";
        $this->sms = "907-978-4432";
        $this->cellProvider = "ATT";
        
        $customers = \App\Libraries\DroplockerObjects\Customer::search_aprax(array("email" => $this->email));
        
        foreach ($customers as $customer) {
            $customer->delete();
        }
    }

    /**
     * The following test verifies the functionality for an existing customer to log in an place an order at a kiosk location and also verifies that the customer and order notes are copied succesfully to the order creatd from the claim.
     * @throws Exception 
     */
    public function testLoginAndPlaceOrder() {
        //The following code creates the webdriver customer account.
        $this->customerID = $this->CI->customer_model->new_customer($this->email, $this->password, $this->business_id);

        $bag = new Bag();
        $bag->customer_id = $this->customerID;
        $bag->business_id = $this->business_id;
        $bag->save();

        $this->login($this->email, $this->password);
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account/orders/new_order");
        }
        else {
            $this->driver->get_element("link text=Place a New Order")->click();
        }

        //checks the sms settings of that particular business
        $check_carrier = false;
        if( $this->checkSMSProvider() !== false ){
            $check_carrier = true;
        }
        
        //We expect to be redirected to the profile edit view because we are a new user.
        $this->driver->get_element("css=div.alert-error")->assert_text_contains("Profile must be complete before placing an order.");   
        $this->driver->get_element("name=firstName")->send_keys($this->firstName);
        $this->driver->get_element("name=lastName")->send_keys($this->lastName);
        $this->driver->get_element("name=address")->send_keys($this->address);
        $this->driver->get_element("name=address2")->send_keys($this->address2);
        $this->driver->get_element("name=city")->send_keys($this->city);
        //Note,the following statement expects that the business that we interface with is 'American', which is defined when the business property 'country' is set to USA. An American business shall have a dropdown of states of the United States.
        //Note that if the business is not american, then the view renders a text field.
        //The following statement expects that the state field is a dropdown of American states.
        $this->driver->get_element("name=state")->select_value($this->state);
        $this->driver->get_element("name=phone")->send_keys($this->phone);
        $this->driver->get_element("name=sms_number")->send_keys($this->sms);
        
        //no sms setting, send over the cell provider
        if($check_carrier == true ){
            $this->driver->get_element("name=cellProvider")->select_value($this->cellProvider);
        }
        
        $this->driver->get_element("name=phone")->submit();
        
        //The following statements verify that the entered data was successfully saved.
        $this->driver->get_element("name=firstName")->assert_value($this->firstName);
        $this->driver->get_element("name=lastName")->assert_value($this->lastName);
        $this->driver->get_element("name=address")->assert_value($this->address);
        $this->driver->get_element("name=address2")->assert_value($this->address2);
        $this->driver->get_element("name=city")->assert_value($this->city);
        $this->driver->get_element("name=state")->assert_value($this->state);
        $this->driver->get_element("name=phone")->assert_value($this->phone);
        $this->driver->get_element("name=sms_number")->assert_value($this->sms);
        
        //if there is no SMS settings, check the carrier
        if( $check_carrier == true ){
            $this->driver->get_element("name=cellProvider")->assert_value($this->cellProvider); 
        }
        
        // Note, We also need to put in a credit card in order to place an order.
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account/profile/billing");
        }
        else {
            $this->driver->get_element("link text=Credit Card Settings")->click();
        }
        $this->driver->get_element("name=cardNumber")->send_keys(UnitTest::$paypro_test_credit_card); //This is the test credit card fdor paypro.
        $this->driver->get_element("name=expMo")->select_value("02");
        $this->driver->get_element("name=expYr")->select_value(2022);
        $this->driver->get_element("name=csc")->send_keys("437");
        $this->driver->get_element("name=submit")->click();


        $this->driver->get_element("link text=Place Order Now")->click();
        
        $total_business_locations = $this->checkBusinessLocationCount();
        
        
        if( $total_business_locations > 10 ){
            //locations - for new customer, essentially
            //a few things need to happen here.
            // 1.) wait for the locations to load - 
            // 2.) Is there a list of locations to choose from?
            // 3.) pick a location at random from the results?
            sleep(8); //wait to load

            //if this next line fails, it means that the locations didn't load via ajax, might need more sleep time
            $this->driver->assert_element_present("id=locations_result_table_body");     //some kind of locations result

            if( $this->driver->is_element_present("link text=Place a New Order") ) {  //there is a location already added

                //place the order
                $this->driver->get_element("link text=Place a New Order")->click();

            }
            else {
                //they need to add a location, click on one.
                $found_locations = $this->driver->get_all_elements('css=a.add_button');
                $total_locations_found = count( $found_locations );

                //add random one, and wait 5 seconds
                $location_to_use = $found_locations[ rand(0, $total_locations_found-1 ) ];
                $location_to_use->click();
                sleep(3);

                $this->driver->get_element("link text=Place a New Order")->click();
            }
        }
        
        //the actual order page
        $order_notes = "These are some notes that are specific to this order";

        //pick random location first
        $order_page_location_options = $this->driver->get_all_elements("//select[@id='location_id']/option");
        $order_page_location_to_use = $order_page_location_options[ rand( 0, count( $order_page_location_options ) - 1 ) ];
        $order_page_location_to_use->click();
        
        //this will now pick a random locker from the dropdown
        
        $locker_menu = $this->driver->get_element("lockers");
        $locker_menu->select_index(2);
        $service_types = $this->driver->get_all_elements("css=.service_type");
        $service_types[0]->click(); //This selects the Wash and Fold service type.
        $this->driver->get_element("name=notes")->send_keys($order_notes);
        $this->driver->get_element("id=submit_button")->click();

        $this->driver->get_element("name=submit")->click();

        $this->driver->assert_string_present("Thank you for your order!");

        $employeeUnitTest = new EmployeeUnitTest($this->driver);
        $employeeUnitTest->setup();
        $employeeUnitTest->login();
        $this->driver->get_element("link text=ORDERS")->click();

        $this->driver->get_element("link text=Quick Scan Bag")->click();
        $this->driver->get_element("name=bag_number")->send_keys($bag->bagNumber);
        $this->driver->get_element("name=button")->click();
        $this->driver->get_element("link text=Create Order")->click();

        $this->driver->get_element("name=submit")->click();

        //The following statements verfies that the warning dialog for the order notes appears..
        $this->driver->assert_element_present("css=.order_notes_warning_checkbox");

        $warning_checkboxes = $this->driver->get_all_elements("css=.order_notes_warning_checkbox");
        foreach ($warning_checkboxes as $warning_checkbox) {
            $warning_checkbox->click();
        }
        $this->driver->get_element("css=button.ui-button")->click(); //This is the 'Proceed button' in the order notes warning dialog.
        
        $this->driver->assert_string_present("ORDER TYPE: Wash and Fold");
        $this->driver->assert_string_present("THIS ORDER ONLY: $order_notes");
    }    
   
    /* this is a check to see if we should test the 'carrier' field in the customer test'
     * 
     */
    private function checkSMSProvider(){
        $_ci =& get_instance();
        $business_sms_check_query = $_ci->db->get_where('smsSettings', array('business_id' => $this->business_id ) );
        $smsSettings = $business_sms_check_query->row();
        
        //if we don't have an external sms provider, we need to check the carrier field
        if( empty( $smsSettings ) || $smsSettings->provider != 'twilio' ){
            return true;
        }else{
            return false;
        }
    }
   
    private function checkBusinessLocationCount(){
        $_ci =& get_instance();
        $_ci->load->model('location_model');
        $options = array();
        $options['business_id'] = $this->business_id;
        $options['select'] = "(SELECT name from locationType where locationTypeID = locationType_id) as locationType, (SELECT count(lockerID) from locker where location_id = locationID) as total,lockerType,locationID,description,locationType_id,address,city,state,zipcode";
        $options['with_location_type'] = true;
        $locs = $_ci->location_model->get($options);
        return sizeof($locs);
    }
    
    public function tearDown()
    {
        if ($this->locker instanceof \App\Libraries\DroplockerObjects\Locker)
        {
            $this->locker->delete();
        }
        if ($this->inProcess_locker instanceof \App\Libraries\DroplockerObjects\Locker)
        {
            $this->inProcess_locker->delete();
        }
        if ($this->location instanceof \App\Libraries\DroplockerObjects\Location)
        {
            $this->location->delete();
        }
        $creditCards = \App\Libraries\DroplockerObjects\CreditCard::search_aprax(array("customer_id" => $this->customerID), false);
        foreach ($creditCards as $creditCard)
        {
            $creditCard->delete();
        }
        $customer_locations = \App\Libraries\DroplockerObjects\Customer_Location::search_aprax(array("customer_id" => $this->customerID));
        array_map(function($customer_location) {
            $customer_location->delete();
        }, $customer_locations);
        
        parent::tearDown();
    }

}
