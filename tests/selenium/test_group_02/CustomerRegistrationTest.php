<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;
class CustomerRegistrationTest extends CustomerUnitTest{
    protected $email;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $address;
    protected $address2;
    protected $city;
    protected $state;
    protected $phone;
    protected $sms;
    protected $cellProvider;
    
    public function setUp() 
    {
        parent::setUp();

        
        //The following code creates a test user in the database.
       
        $word = UnitTest::get_random_word(1);
        $this->email = "webdriver_$word@testing.com";
        $this->password = "webdriver";
        $this->firstName = "webdriver_$word";
        $this->lastName = "webdriver_$word";
        $this->address = "872 Risse Rd";
        $this->address2 = "Apt 15";
        $this->city = "North Pole";
        $this->state = "AK";
        $this->zip = "99705";
        $this->phone = "907-488-5555";
        $this->sms = "907-978-4432";
        $this->cellProvider = "ATT";
        
        $customers = \App\Libraries\DroplockerObjects\Customer::search_aprax(array("email" => $this->email));
        foreach ($customers as $customer)
        {
            $customer->delete();
        }
        
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        $this->coupon = new Coupon();
        $this->coupon->prefix = "XY";
        $this->coupon->code = "7985";
        $this->coupon->amount = rand(1,20);
        $this->coupon->business_id = $this->business_id;
        $this->coupon->endDate = $endDate;
        $this->coupon->combine = 0;
        $this->coupon->amountType = "dollars";
        $this->coupon->description  = sprintf("\$%s off every order", $this->coupon->amount);
        $this->coupon->frequency = "every order";
        
        $existing_coupons = Coupon::search_aprax(array("code" => $this->coupon->code, "prefix" => $this->coupon->prefix));
        array_map(function($existing_coupon) {
            $existing_coupon->delete();}, $existing_coupons);
        
        $this->coupon->save();
    }
    
    /**
     * The following test verifies the functionality of registering with a discount code and changing password.
     * @throws Exception 
    */
    public function testRegisterAndRedeemEveryOrderCouponAndChangePassword()
    {
        $promotional_code= $this->coupon->prefix . $this->coupon->code;
        $this->driver->get_element("link text=Register Now")->click();
        
        $this->driver->get_element("name=email")->send_keys($this->email);
        $this->driver->get_element("name=password")->send_keys($this->password);
        $this->driver->get_element("name=promo_code")->send_keys($promotional_code);
        $this->driver->get_element("name=agree")->click();
        $this->driver->get_element("name=agree")->submit();
        
        $this->driver->get_element("css=div.alert-success")->assert_text_contains("Promotional code '$promotional_code - {$this->coupon->description}' was applied to your new account.");
        $today = new DateTime("now");
        
        //The following statements verify that the coupon correct appears in the customer's account.
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account");
        }
        else {
            $this->driver->get_element("link text=My Account")->click();
        }
        
        if ($this->browser == "chrome")
        {
            $this->driver->load("{$this->base_url}/account/programs/discounts");
        }
        else
        {
            $this->driver->get_element("link text=Discounts")->click();
        }      
        
        $customers = Customer::search_aprax(array("email" => $this->email, "business_id" => $this->business_id));
        $this->assertNotEmpty($customers, "Could not find the newly registered customer.");
        $customer = $customers[0];
        
        
        
        $customer->signupDate->setTimeZone(new DateTimeZone("GMT"));
        
        $this->assertEquals($today->format("Y-m-d H"), $customer->signupDate->format("Y-m-d H"), "The customer signup date is not the expected time");
        
        $customerDiscounts = CustomerDiscount::search_aprax(array("couponCode" => $this->coupon->prefix . $this->coupon->code, "customer_id" => $customer->customerID));
        $this->assertNotEmpty($customerDiscounts, "Could not find the customer discount in the database");            

        $customerDiscount = $customerDiscounts[0];

        $this->driver->assert_string_present($this->coupon->frequency, "The expected coupon frequency was not found in the view");
        $this->driver->assert_string_present($this->coupon->description, "The coupon expected description was not found in the view");

        $customerDiscount->expireDate->setTimezone(new DateTimeZone($this->business->timezone));

        $this->driver->assert_string_present($customerDiscount->expireDate->format("F j, Y"));


        //The following statements change the client's password.
        $new_password = "asdf123";
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account/profile/edit");
        }
        else {
            $this->driver->get_element("link text=My Account Settings")->click();
        }
        $this->driver->get_element("link text=Update Password")->click();
        $this->driver->get_element("name=new_password")->send_keys($new_password);
        $this->driver->get_element("name=new_password_again")->send_keys($new_password);
        $this->driver->get_element("name=submit")->click();
        //The following statements verify that the client can log into his or her account with his or her new password.
        $this->driver->get_element("css=.login_logout")->click();
        $this->driver->get_element("name=username")->send_keys($this->email);
        $this->driver->get_element("name=password")->send_keys($new_password);
        $this->driver->get_element("name=password")->submit();
        $this->driver->get_element("css=div.alert-success")->assert_text_contains("Welcome Back");
    } 
    
    public function tearDown()
    {
        if ($this->coupon instanceOf \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon->delete();
        }
        if (isset($this->customerID) and is_int($this->customerID))
        {
            $customers = Customer::search_aprax(array("email" => $this->email));
            array_map(function($customer) {
                $customer->delete();}, $customers);
                
        }
        
    }
}

?>
