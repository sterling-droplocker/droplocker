<?php

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Bag;

class CustomerMyOrdersTest extends CustomerUnitTest {

    protected $email;
    protected $password;
    protected $firstName;
    protected $lastName;
    protected $address;
    protected $address2;
    protected $city;
    protected $state;
    protected $phone;
    protected $sms;
    protected $cellProvider;
    protected $customerID;

    public function setUp() {
        parent::setUp();

        $this->business_id = 3;

        $this->business = new Business($this->business_id);

        $this->CI->load->model("customer_model");
        $this->CI->load->model("claim_model");
        $this->CI->load->model("order_model");

        //The following code creates a test user in the database.

        $word = UnitTest::get_random_word(1);
        $this->email = "webdriver_$word@testing.com";
        $this->password = "webdriver";
        $this->firstName = "webdriver_$word";
        $this->lastName = "webdriver_$word";
        $this->address = "872 Risse Rd";
        $this->address2 = "Apt 15";
        $this->city = "North Pole";
        $this->state = "AK";
        $this->zip = "99705";
        $this->phone = "907-488-5555";
        $this->sms = "907-978-4432";
        $this->cellProvider = "ATT";
        $this->cardNumber = '1234';
        $this->month = '4';
        $this->year = '16';

        $customers = \App\Libraries\DroplockerObjects\Customer::search_aprax(array("email" => $this->email));
        foreach ($customers as $customer) {
            $customer->delete();
        }
    }

    /**
     * TODO: 
     * add span with id attr to card on file values to test that the proper card is stored
     * add span with id for expire data too
     * 
     * This view needs to be update to not use javascript for updating the kiosk access
     */
    public function testMyOrdersLaundryLocker() {

        //The following code creates the webdriver customer account.
        $this->customerID = $this->CI->customer_model->new_customer($this->email, $this->password, $this->business_id);

        $this->login($this->email, $this->password);
        if ($this->browser == "firefox") 
        {
            $this->driver->get_element("link text=My Orders")->click();
        } 
        else 
        {
            $this->driver->load("{$this->base_url}/account/orders/view_orders");
        }


        $this->driver->get_element("css=h2.page-header")->assert_text("My Orders");
    }

}