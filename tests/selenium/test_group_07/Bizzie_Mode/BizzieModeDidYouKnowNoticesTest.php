<?php

use App\Libraries\DroplockerObjects\DidYouKnow;
use App\Libraries\DroplockerObjects\Business;
class BizzieModeDidYouKnowTest extends BizzieSuperAdminUnitTest 
{
    private $test_slave_didYouKnow;
    private $test_master_didYouKnow;
    public function setUp()
    {
        parent::setUp();
        $this->CI->db->truncate("didYouKnow");
        
        $this->test_master_didYouKnow = new DidYouKnow();
        $this->test_master_didYouKnow->business_id = $this->master_business->{Business::$primary_key};
        $this->test_master_didYouKnow->note = UnitTest::get_random_word(10);
        $this->test_master_didYouKnow->save();
        
        $this->test_slave_didYouKnow = new DidYouKnow();
        $this->test_slave_didYouKnow->business_id = $this->slave_business->{Business::$primary_key};
        $this->test_slave_didYouKnow->note = $this->test_master_didYouKnow->note;
        $this->test_slave_didYouKnow->save();
    }
    
    /**
     * THe following test verifies the fucntinality for crewating a new master print notice that is also applied for each slave businesses.
     */
    public function testCreateDidYouKnow()
    {
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
        $this->driver->get_element("link text=Manage Master Did You Know Notices")->click();
        
        $note = UnitTest::get_random_word(20);
        $this->driver->get_element("css=#add_form textarea")->send_keys($note);
        $this->driver->get_element("name=add")->click();
        $this->driver->get_element("css=div.success")->assert_text("Added the Did You Know notice to all businesses");
        $this->driver->assert_string_present($note);
        
        $master_didYouKnows = DidYouKnow::search_aprax(array("business_id" => $this->business_id, "note" => $note));
        $this->assertNotEmpty($master_didYouKnows);
        $master_didYouKnow = $master_didYouKnows[0];
        $this->CI->load->model("business_model");

        $slave_didYouKnows = DidYouKnow::search_aprax(array("business_id" => $this->slave_business->{Business::$primary_key}, "base_id" => $master_didYouKnow->{DidYouKnow::$primary_key}));
        $this->assertNotEmpty($slave_didYouKnows);
        $slave_didYouKnow = $slave_didYouKnows[0];
        $this->assertEquals($slave_didYouKnow->note, $master_didYouKnow->note);
    }
    
    
    /**
     * The following test verifies the functionality for updating master Did you Know Notice. This test verifies that all slave businesses Did you Know notices associated with the master Did You Know notice are updated as well.
     */
    public function testUpdateDidYouKnow()
    {
        $note = UnitTest::get_random_word(20);
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
        $this->driver->get_element("link text=Manage Master Did You Know Notices")->click();
        $this->driver->get_element("css=#update_form textarea")->clear();
        $this->driver->get_element("css=#update_form textarea")->send_keys($note);
        $this->driver->get_element("name=update")->click();
        $this->driver->get_element("css=div.success")->assert_text("Updated master Did You Know notice and for all franchises");
        
        $this->test_master_didYouKnow->refresh();
        $this->assertEquals($note, $this->test_master_didYouKnow->note);

        $slave_didYouKnows = DidYouKnow::search_aprax(array("business_id" => $this->slave_business->{Business::$primary_key}, "base_id" => $this->test_master_didYouKnow->{DidYouKnow::$primary_key}));
        $this->assertNotEmpty($slave_didYouKnows);
        $slave_didYouKnow = $slave_didYouKnows[0];
        $this->assertEquals($slave_didYouKnow->note, $this->test_master_didYouKnow->note);
        $this->assertEquals($slave_didYouKnow->base_id, $this->test_master_didYouKnow->{DidYouKnow::$primary_key});
    }
    
    
    
    /**
     * The following test verifies the functionality for deleting a Did You Know notice at the franchisee level.
     */
    public function testDeleteDidYouKnowAtFranchiseeLevel()
    {
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Manage Did You Know Notices")->click();
        $this->driver->get_element("id=delete-{$this->test_slave_didYouKnow->{DidYouKnow::$primary_key}}")->click();
        $this->driver->accept_alert();
        $this->driver->get_element("css=div.success")->assert_text("Deleted Did You Know notice from {$this->slave_business->companyName}");
        $this->driver->assert_string_not_present($this->test_slave_didYouKnow->note);
        
        $slave_didYouKnows = DidYouKnow::search_aprax(array(DidYouKnow::$primary_key => $this->test_slave_didYouKnow->{DidYouKnow::$primary_key}));
        $this->assertEmpty($slave_didYouKnows);
    }    
    
    /**
     * The following test verifies the functionality for creating new Did You Know notices at the franchisee level.
     */
    public function testCreateDidYouKnowatFranchiseeLevel()
    {
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        
        $this->login_as_bizzie_superadmin();
        $note = UnitTest::get_random_word(20);
        
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Manage Did You Know Notices")->click();
        
        $this->driver->get_element("css=#add_form textarea")->send_keys($note);
        $this->driver->get_element("name=add")->click();
        $this->driver->get_element("css=div.success")->assert_text("Note Added");
        $this->driver->assert_string_present($note);
        
        $slave_didYouKnows = DidYouKnow::search_aprax(array("note" => $note, "business_id" => $this->slave_business->{Business::$primary_key}));
        $this->assertNotEmpty($slave_didYouKnows);
        $slave_didYouKnow = $slave_didYouKnows[0];
        
        $this->assertEquals($note, $slave_didYouKnow->note);
    }    
    

    /**
     * The following test verifies the functionality for updating an existing Did You KNow notice in a franchiseee
     */
    public function testUpdateDidYouKnowatFranchiseeLevel()
    {
        $note = UnitTest::get_random_word(25);
        
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Manage Did You Know Notices")->click();
        $this->driver->get_element("css=#update_form textarea")->clear();
        $this->driver->get_element("css=#update_form textarea")->send_keys($note);
        $this->driver->get_element("name=update")->click();
        $this->driver->get_element("css=div.success")->assert_text("Note Updated");
        $slave_didYouKnows = DidYouKnow::search_aprax(array(DidYouKnow::$primary_key => $this->test_slave_didYouKnow->{DidYouKnow::$primary_key}));
        
        $slave_didYouKnow = $slave_didYouKnows[0];
        $this->assertEquals($note, $slave_didYouKnow->note);
    }
    
    /**
     * The following test verifies the funcitonality for deleting master Did You Know notices. This test verifies that all slave business Did You Know notices whose base ID matches the primary key of the deleted master Did You Know noticea are deleted as well.
     */
    public function testDeleteDidYouKnow()
    {
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=SUPER ADMIN")->click();
        $this->driver->get_element("link text=Manage Master Did You Know Notices")->click();
        $this->driver->get_element("id=delete-{$this->test_master_didYouKnow->{DidYouKnow::$primary_key}}")->click();
        $this->driver->accept_alert();
        $this->driver->get_element("css=div.success")->assert_text("Deleted master Did You Know notice ({$this->test_master_didYouKnow->{DidYouKnow::$primary_key}}) and from all franchises");
        
        $slave_didYouKnows = DidYouKnow::search_aprax(array("base_id" => $this->test_master_didYouKnow->{DidYouKnow::$primary_key}));
        $this->assertEmpty($slave_didYouKnows);
        
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new DidYouKnow($this->test_master_didYouKnow->{DidYouKnow::$primary_key});
        
    }    
    

    /**
     * The following test verifies that franchise users are restricted to read only access in the Did You Know interface when the server is in Bizzie mode.
     */
    public function testSlaveUserReadOnlyAccess()
    {
        $this->login_as_slave_admin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Manage Did You Know Notices")->click();
        $this->driver->assert_string_present($this->test_slave_didYouKnow->note);
        $this->driver->assert_element_not_present("add_form");
        $this->driver->assert_element_not_present("update_form");
        $this->driver->assert_element_not_present("name=note");
        $this->driver->assert_element_not_present("name=add");
        $this->driver->assert_element_not_present("name=update");
    }

}

?>
