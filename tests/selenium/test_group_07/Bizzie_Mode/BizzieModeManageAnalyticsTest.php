<?php

use App\Libraries\DroplockerObjects\Business;
class BizzieModeManageAnalyticsTest extends BizzieSuperAdminUnitTest
{
    public function setUp()
    {
        parent::setUp();
        
    }
    /**
     * The follooing test verifies the functionality for the superadmin to update a franchisee's analaytics settings.
     */
    public function testUpdateAnalytics()
    {
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        $this->login_as_bizzie_superadmin();
        $word = UnitTest::get_random_word();
        $this->driver->get_element("link text=WEBSITE")->click();
        $this->driver->get_element("link text=Website Analytics")->click();
        $this->driver->get_element("name=ga")->send_keys($word);
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text("Updated website analytics");
        $this->assertEquals($word, get_business_meta($this->slave_business->{Business::$primary_key}, "google_analytics"));
    }
    
    /**
     * The following test verifies that the franchisee has read only access to the analytics interface.
     */
    public function testReadOnlyAnalytics()
    {
        $word = UnitTest::get_random_word();
        update_business_meta($this->slave_business->{Business::$primary_key}, "google_analytics", $word);
        $this->login_as_slave_admin();
        $this->driver->get_element("link text=WEBSITE")->click();
        $this->driver->get_element("link text=Website Analytics")->click();
        $this->driver->assert_element_not_present("name=ga");
        $this->driver->assert_element_not_present("name=submit");
        $this->driver->assert_string_present($word);
    }
}

?>
