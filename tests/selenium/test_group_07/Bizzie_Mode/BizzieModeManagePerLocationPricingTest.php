<?php
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Product_Location_Price;
use App\Libraries\DroplockerObjects\Location;
Use App\Libraries\DroplockerObjects\Product;
class BizzieModeManagePerLocationPricingTest extends BizzieSuperAdminUnitTest
{
    private $location;
    private $product_process;
    private $product_process_2; //This is used when creating a new per location price;
    private $product_location_price;
    public function setUp()
    {
        parent::setUp();
        
        $this->CI->db->truncate("product_location_price");
        
        $this->business_employee->business_id = $this->slave_business->{Business::$primary_key};
        $this->business_employee->save();
        
        $this->location = UnitTest::create_random_location($this->slave_business->{Business::$primary_key});
        
        $this->product_process = UnitTest::create_random_product_process($this->slave_business->{Business::$primary_key});
        $this->product_process_2 = UnitTest::create_random_product_process($this->slave_business->{Business::$primary_key});
        $this->product_location_price = UnitTest::create_random_product_location_price($this->slave_business->{Business::$primary_key}, $this->product_process->{Product_Process::$primary_key}, $this->location->{Location::$primary_key});
    }
    
    /**
     * The following test verifies the functionality for the superamdin to add an ddelete a per location price.
     */
    public function testAddAndDelete()
    {
        $this->login_as_bizzie_superadmin();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Per Location Pricing")->click();
        $this->driver->get_element("id=location")->send_keys($this->location->address);
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("css=.ui-menu-item a")->click();
        
        $this->driver->get_element("name=submit")->click();
        
        $price = rand(1,20);
        
        $this->driver->get_element("id=product_processID")->select_value($this->product_process2->{Product_Process::$primary_key});
        $this->driver->get_element("name=price")->send_keys($price);
        $this->driver->get_element("name=add")->click();
        
        $this->driver->get_element("css=div.success")->assert_text("New pricing added");
        $this->driver->assert_string_present($this->product_process->relationships['Product'][0]->name);
        $this->driver->assert_string_present($this->product_process->relationships['Process'][0]->name);
        
        $product_location_prices = Product_Location_Price::search_aprax(array("location_id" => $this->location->{Location::$primary_key}));
        $this->assertNotEmpty($product_location_prices);
        $product_location_price = $product_location_prices[0];
        $this->assertEquals($product_location_price->product_process_id, $this->product_process->{Product_Process::$primary_key});  
        
        //The following statements verify the functionality for deleting a location price
        
        $this->driver->get_element("id=delete-{$product_location_price->{Product_Location_Price::$primary_key}}")->click();
        $this->driver->accept_alert();
        
        $this->driver->get_element('css=div.success')->assert_text("Item Deleted");
        
        $deleted_product_location_price = Product_Location_Price::search_aprax(array(Product_Location_Price::$primary_key => $product_location_price->{Product_Location_Price::$primary_key}));
        
        $this->assertEmpty($deleted_product_location_price, "There were unexpected product location prices for the location");
    }    


    public function tearDown()
    {
        if ($this->location instanceOf \App\Libraries\DroplockerObjects\Location)
        {
            $this->location->delete();
        }
        if ($this->product_process instanceOf \App\Libraries\DroplockerObjects\Product_Process)
        {
            $this->product_process->delete();
        }
        if ($this->product_process2 instanceOf \App\Libraries\DroplockerObjects\Product_Process)
        {
            $this->product_process2->delete();
        }
        if ($this->product_location_price instanceOf \App\Libraries\DroplockerObjects\Product_Location_Price)
        {
            $this->product_location_price->delete();
        }
    }
}

?>
