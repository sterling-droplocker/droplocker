<?php

class SuperadminInternationalizationTest extends EmployeeUnitTest {
    private $languageView;
    
    public function setUp() {
        parent::setUp();
        $this->languageView = UnitTest::create_random_languageView();
        $this->login();
        $this->driver->load("{$this->base_url}/admin/superadmin/manage_languageKeys_and_languageViews");
    }
    /**
     * The following test verifies the functionality for adding a new language view.
     */
    public function testAddLanguageView() {
        $this->driver->get_element("add_new_languageView_button")->click();
        $name = UnitTest::get_random_word(2);
        $this->driver->get_element("name")->send_keys($name);
        $this->driver->get_element("name")->submit();
        $this->driver->get_element("css=div.success")->assert_text("Created new language view '$name'");
    }
    /**
     * The following test verifies the functioality for deleting a language view
     */
    public function deleteLanguageView() {
        $this->driver->get_element("delete-languageView-{$this->languageView->languageViewID}")->click();
        $this->driver->get_element("css=div.success")->assert_text("Deleted language view '{$this->languageView->name}'");
        
    }
}

?>
