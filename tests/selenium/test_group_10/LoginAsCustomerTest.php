<?php

class LoginAsCustomerTest extends EmployeeUnitTest
{
    private $test_customer;
    public function setUp()
    {
        parent::setUp();
        $this->login();
        
    }
    /**
     * 
     * The following test verifies the fucntionality for logging in as a customer from the administrative component.
     */
    public function testLoginAsCustomer()
    {
       
        $this->markTestSkipped("This test can not complete successfully because of a bug in Selenium");
        $this->test_customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        $customer_detail_address = sprintf("http://droplocker.%s/admin/customers/detail/%s", $this->domain_extension, $this->test_customer->{App\Libraries\DroplockerObjects\Customer::$primary_key});
        $this->driver->load($customer_detail_address);

        $this->driver->get_element("id=go_button")->click(); //Note, the "login as Customer' menu item is already preselected.

        $this->wait_for_ajax_to_complete();
        sleep(5); //The following statements work around a bug in Selenium where it can not find the window.
        $this->driver->select_window("Laundry Locker :. San Francisco Green Dry Cleaning and Laundry Delivery Service");
        $this->driver->get_element("link text=Preferences")->click();

        $this->driver->get_element("link text=Account Settings")->click();

        $this->driver->get_element("name=firstName")->assert_value($this->test_customer->firstName);
        $this->driver->get_element("name=lastName")->assert_value($this->test_customer->lastName);
        $this->driver->get_element("name=city")->assert_value($this->test_customer->city);        
    }
    
    public function tearDown()
    {
        if ($this->test_customer instanceOf \App\Libraries\DroplockerObjects\Customer)
        {
            $this->test_customer->delete();
        }
        parent::tearDown();
    }
}

?>
