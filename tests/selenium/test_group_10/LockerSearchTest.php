<?php

use App\Libraries\DroplockerObjects\Location;

class LockerSearchTest extends EmployeeUnitTest
{
    private $location;
    private $locker;
    public function setUp()
    {
        parent::setUp();
        
        $this->login();
        $this->driver->get_element("link text=SEARCH")->click();
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});
    }
        /**
     * The following test verifies the functionality for searching by locker information.
     * @throws Exception 
     */
    public function testLocker()
    {

        $this->driver->get_element("lockerName")->send_keys($this->locker->lockerName);
        $this->driver->get_element("lockerName")->submit();
        $this->driver->assert_string_present($this->locker->lockerName);
        $this->driver->assert_string_present($this->locker->relationships['Location'][0]->address);
        $this->driver->assert_string_present($this->locker->relationships['LockerLockType'][0]->name);
        $this->driver->assert_string_present($this->locker->relationships['LockerStatus'][0]->name);
        $this->driver->assert_string_present($this->locker->relationships['LockerStyle'][0]->name);
    }
    
    public function tearDown()
    {
        if ($this->location instanceOf \App\Libraries\DroplockerObjects\Location)
        {
            $this->location->delete();
        }
        
        if ($this->locker instanceOf \App\Libraries\DroplockerObjects\Locker)
        {
            $this->locker->delete();
        }
        parent::tearDown();
        
    }
}

?>
