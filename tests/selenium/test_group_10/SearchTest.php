<?php

class SearchTest extends EmployeeUnitTest 
{

    
    public function setUp()
    {
        parent::setUp();
        
        $this->login();
        $this->driver->get_element("link text=SEARCH")->click();
    }
    /**
     * The following test verifies the functionality for searching by barcode.
     * @throws Exception 
     */
    public function testBarcode()
    {
        $order = UnitTest::create_random_order();
        $orderItems = array();
        $orderItems[] = UnitTest::create_random_orderItem($order->orderID);
        $orderItems[] = UnitTest::create_random_orderItem($order->orderID);
        $orderItems[] = UnitTest::create_random_orderItem($order->orderID);
        
        $barcode = UnitTest::create_random_barcode();
        $barcode->item_id = $orderItems[0]->item_id;
        $barcode->save();
        
        $this->driver->get_element("barcode")->send_keys($barcode->barcode);
        $this->driver->get_element("barcode")->submit();
        $this->driver->assert_string_present($barcode->barcode);

        $this->CI->load->model("barcode_model");
        $orderItems = $this->CI->barcode_model->get_order_item_from_barcode($barcode->barcode);

        foreach ($orderItems as $orderItem)
        {
            if (!empty($orderItem->displayName))
            {
                $this->driver->assert_string_present($orderItem->displayName, "The displayname for the product is not the expected value.");
            }
            if (!empty($orderItem->firstName))
            {
                $this->driver->assert_string_present(trim($orderItem->firstName));
            }
            if (!empty($orderItem->lastName))
            {
                $this->driver->assert_string_present(trim($orderItem->lastName));
            }
            
            $this->driver->assert_string_present($orderItem->order_id);
            $this->driver->assert_string_present($orderItem->unitPrice);
            if (!empty($orderItem->bagNumber))
            {
                $this->driver->assert_string_present($orderItem->bagNumber);
            }
        }
    }        
    
    /**
     * The following test verifies the functinoality for searching by order information.
     * @throws Exception 
     */
    public function testOrder()
    {
        $order = UnitTest::create_random_order($this->business_id);
        $this->driver->get_element("order_id")->send_keys($order->orderID);
        $this->driver->get_element("order_id")->submit();
        //The follwoing assertion verifies that the order's bag nubmer appears in the search results.
        $this->driver->assert_string_present($order->relationships['Bag'][0]->bagNumber);

        $names = $order->relationships['Customer'][0]->firstname . " " . $order->relationships['Customer'][0]->lastName;
        $first_name_and_last_name = preg_replace("/\s+/", " ", $names);
        $this->driver->assert_string_present($first_name_and_last_name);


        $location = new \App\Libraries\DroplockerObjects\Location($order->relationships['Locker'][0]->location_id, false);
        $this->driver->assert_string_present($location->address);      
    }    

    
    /**
     * The following test verifies the functionality for searching by customer information.
     * @throws Exception 
     */
    public function testCustomer()
    {
        $customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        //$customer = new \App\Libraries\DroplockerObjects\Customer(7228);
        $this->driver->get_element("name=firstName")->send_keys($customer->firstName);
        $this->driver->get_element("name=lastName")->send_keys($customer->lastName);
        $this->driver->get_element("name=email")->send_keys($customer->email);
        $this->driver->get_element("name=email")->submit();
        if (!empty($customer->firstName) || !empty($customer->lastName))
        {
            $this->driver->assert_string_present(trim(sprintf("%s %s", $customer->firstName, $customer->lastName)));
        }

        if (!empty($customer->address1))
        {
            $address = preg_replace("/\s+/", " ", $customer->address1);
            $this->driver->assert_string_present($address);
        }
        if (!empty($customer->email))
        {
            $this->driver->assert_string_present($customer->email);
        }
        if (!empty($customer->phone))
        {
            $this->driver->assert_string_present($customer->phone);
        }   
    }    




}

?>
