<?php

class BusinessInternationalizationTest extends EmployeeUnitTest{
    private $language1;
    private $business_language1;
    private $language2;
    public function setUp() {
        parent::setUp();
        $this->language1 = UnitTest::create_random_language();
        $this->language2 = UnitTest::create_random_language();
        $this->login();
        $this->business_language1 = UnitTest::create_random_business_language($this->business_id, $this->language1->languageID);        
        $this->driver->load("{$this->base_url}/admin/admin/manage_business_languages");
    }
    /**
     * The following test verifies the funcitonality for adding a new business language
     */
    public function testAddBusinessLanguage() {
        $this->driver->get_element("add_business_language_button")->click();
        $language_menu = $this->driver->get_element("name=language_id");
        $languageID = $this->language2->languageID;
        $language_menu->select_value($languageID);
        $this->driver->get_element("name=language_id")->submit();
        $this->driver->get_element("css=div.success")->assert_text("Added language '{$this->language2->tag} - {$this->language2->description}' to '{$this->business->companyName}'");
    }
    /**
     * The following test verifies the functionality for deleting a business language.
     */
    public function testDeleteBusinessLanguage() {
        $this->driver->get_element("delete-{$this->business_language1->business_languageID}")->click();
        $this->driver->get_element("css=div.success")->assert_text("Deleted Business Language '{$this->language1->tag} - {$this->language1->description}'");
    }
    
    public function tearDown() {
        try {
            if ($this->business_language1 instanceOf \App\Libraries\DroplockerObjects\Business_Language) {
                $this->business_language1->delete();
            }
            if ($this->language1 instanceOf \App\Libraries\DroplockerObjects\Language) {
                $this->language1->delete();
            }
            if ($this->language2 instanceof \App\Libraries\DroplockerObjects\Language) {
                $this->language2->delete();
            }
        }
        catch (Database_Exception $deletion_exception) {
            echo "Could not complete cleanup";
        }
        parent::tearDown();
    }

    
}

?>
