<?php
use \App\Libraries\DroplockerObjects\ProductCategory;
use \App\Libraries\DroplockerObjects\Product;

class ModuleManagementTest extends EmployeeUnitTest{
    private $productCategory;
    private $product;
    public function setUp() {
        
        parent::setUp();
        $this->productCategory = UnitTest::create_random_productCategory($this->business_id, 9); //Note, 9 is expected to be the shoe module.
        $this->product = UnitTest::create_random_product($this->business_id, $this->productCategory);
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/admin/module_preferences?moduleID=9");
        $this->login();
    }
    /**
    public function testModifyUnitOfMeasurement() {
        $productType_menu = get_element("unitOfMeasurement-{$this->product->{Product::$primary_key}}");
        
        $productTypes = $this->CI->config->item('productTypes');
        
        $productType = array_ra
        $productType_menu->select_value("order");
        $this->wait_for_ajax_to_complete();
        $this->driver->reload();
        $this->driver->get_element("unitOfMeasurement-{$this->product->{Product::$primary_key}}")->assert_value("order");
    }
     *
     */
    /**
     * The following test verifies the functionality for adding a product to a product category.
     */
    public function testCreateProduct() {
        
        $add_product_button = $this->driver->get_element("css=.add_product");
        $add_product_button->click();

        $category_name = $add_product_button->get_attribute_value("data-name");
        
        $name = UnitTest::get_random_word(1);
        $displayName = UnitTest::get_random_word(2);
        $price = 5;
        $this->driver->get_element("new_product_name")->send_keys($name);
        $this->driver->get_element("new_product_displayName")->send_keys($displayName);
        
        $units = $this->CI->config->item('units');
        $units_menu = $this->driver->get_element("new_product_unitOfMeasurement");
        $units_options = $units_menu->get_options();
        foreach ($units_options as $menu_unit) {
            $this->assertArrayHasKey($menu_unit->get_value(), $units);
            $this->assertEquals($units[$menu_unit->get_value()], $menu_unit->get_text());
        }
        
        $this->driver->get_element("new_product_notes");
        
        //The following statements verify that all the avialable product types are present for selection.
        $productTypes = $this->CI->config->item('productTypes');
        $productType_menu = $this->driver->get_element("new_productType");
        $productTypes_options = $productType_menu->get_options();
        $productType_menu->assert_option_count(count($productTypes));
        foreach ($productTypes_options as $menu_productType) {
            $this->assertArrayHasKey($menu_productType->get_value(), $productTypes);
            $this->assertEquals($productTypes[$menu_productType->get_value()], $menu_productType->get_text());
        }
        //The following statements verify that all the available upcharges are present for selection.
        $this->CI->load->model("process_model");
        $processes = $this->CI->process_model->get_aprax();
        $process_menu = $this->driver->get_element("new_product_process");
        $process_menu->assert_option_count(count($processes)+1); //Note, we add 1 to the total number of processes since there is an option of 'none' to sasign no process to a product.
        foreach ($processes as $process) {
            $process_menu->assert_contains_label($process->name);
        }
        
        //The follwing statements verify that all of the avilable upcharges are present for selection.
        $this->CI->load->model("upchargeGroup_model");
        $upchargeGroups = $this->CI->upchargeGroup_model->get_aprax(array("business_id" => $this->business_id));
        $upchargeGroup_menu = $this->driver->get_element("new_product_upchargeGroup");
        $upchargeGroup_menu->assert_option_count(count($upchargeGroups) + 1); //Note, we add one to the expected option count because one of the fields is "None".
        foreach ($upchargeGroups as $upchargeGroup) {
            $upchargeGroup_menu->assert_contains_label($upchargeGroup->name);
        }
        
        $price_element = $this->driver->get_element("new_product_price");
        $price_element->clear();
        $price_element->send_keys($price);
        $price_element->submit();
        $this->driver->get_element("css=div.success")->assert_text("Successfully created product '$displayName' with process 'Dry Clean' for product category '$category_name'");
    }    

    public function testModifyProductProductProcess() {
        $this->markTestSkipped("This test can no run because of a bug in selenium webdriver which fails to correctly identify the visibl e menu.");
        $this->driver->get_element("productProcess-{$this->product->productID}")->click();
        $this->driver->get_element("id=update_process_id")->select_label("Hand Wash");
        $this->driver->get_element("id=update_process_id")->submit();
        $this->driver->get_element("productProcessContainer-{$this->product->productID}")->assert_text("Hand Wash");
    }
    public function testModifyProductActiveStatus() {
        $this->driver->get_element("active-{$this->product->productID}")->click();
        $this->wait_for_ajax_to_complete();
        $this->driver->reload();
        $this->driver->get_element("active-{$this->product->productID}")->assert_not_selected();
    }
    public function testModifyProductType() {
        $this->markTestIncomplete("It is known that the * functionality for updating the product type is broken and can not be tested easily");
        //$this->driver->get_element("");
    }


    /**
     * The following test verifies the functinality for adding a category to a module
     */
    public function testCreateCategory() {
        $name = UnitTest::get_random_word(1);
        $note = UnitTest::get_random_word(3);
        $this->driver->get_element("add_productCategory")->click();
        $this->driver->get_element("new_productCategory_name")->send_keys($name);
        $this->driver->get_element("new_productCategory_note")->send_keys($note);
        $this->driver->get_element("new_productCategory_note")->submit();
        $this->driver->get_element("css=div.success")->assert_text("Successfully added product category '$name' to 'Shoe' module");
        $this->driver->reload();
        $this->driver->assert_string_present($name);
    }
    
    /**
     * The following test verifies the functionality for mdoifying a product's name
     */
    public function testModifyProductName() {
        $name = UnitTest::get_random_word(1);
        $product_name_container = $this->driver->get_element("name-{$this->product->productID}");
        $product_name_container->click();    
        $active_element = $this->driver->get_active_element();
        $active_element->clear();
        $active_element->send_keys($name);
        $active_element->submit();
        $this->wait_for_ajax_to_complete();
        $product_name_container->click();
        $this->driver->get_active_element()->assert_value($name); 
    }
    public function testModifyProductDisplayName() {
        $displayName = UnitTest::get_random_word(3);
        $product_displayName_container = $this->driver->get_element("displayName-{$this->product->productID}");
        $product_displayName_container->click();
        $active_element = $this->driver->get_active_element();
        $active_element->clear();
        $active_element->send_keys($displayName);
        $active_element->submit();
        $this->wait_for_ajax_to_complete();
        $product_displayName_container->click();
        $this->driver->get_active_element()->assert_value($displayName);  
    }    
    public function testModifyProductPrice() {
        $price = rand(1,50);
        $product_price_container= $this->driver->get_element("price-{$this->product->productID}");
        $product_price_container->click();
        $active_element = $this->driver->get_active_element();
        $active_element->clear();
        $active_element->send_keys($price);
        $active_element->submit();
        $this->wait_for_ajax_to_complete();
        $product_price_container->click();
        $this->driver->get_active_element()->assert_value($price);
    }    
    public function testModifyProductTaxable() {
        $this->driver->get_element("taxable-{$this->product->productID}")->click();
        $this->driver->reload();
        $this->driver->get_element("taxable-{$this->product->productID}")->assert_selected();
        
    }
    
}

?>
