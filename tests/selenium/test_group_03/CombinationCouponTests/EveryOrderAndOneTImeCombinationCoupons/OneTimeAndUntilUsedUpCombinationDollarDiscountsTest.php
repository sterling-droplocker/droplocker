<?php

use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;
/*
 * The following test verifies the functionality for applying a combination of one time and until used up dollar discounts.
 */
class OneTimeAndUntilUsedUpCombinationDollarDiscountTest  extends EmployeeUnitTest
{
    public function setUp()
    {
        parent::setUp();
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        
        
        $this->order = UnitTest::create_random_order($this->business_id);
        $this->orderItem1 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem1->unitPrice = 40; //Note, we want to make sure that the total price of the items in the order is greater than the amount of the coupons.
        $this->orderItem1->save();
        
        $this->orderItem2 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem3 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        
        $this->order = new Order($this->order->orderID, true);        
        
        $this->coupon1 = new Coupon();
        $this->coupon1->prefix = UnitTest::get_random_string(2);
        $this->coupon1->code = UnitTest::get_random_string(4, "123456789");
        $this->coupon1->amount = rand(1,20);
        $this->coupon1->business_id = $this->business_id;
        $this->coupon1->endDate = $endDate;
        $this->coupon1->combine = 1;
        $this->coupon1->amountType = "dollars";
        $this->coupon1->description = sprintf("\$%s off one time", $this->coupon1->amount);
        $this->coupon1->frequency = "one time";
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon1->code, "business_id" => $this->coupon1->business_id));
        array_map(function($existing_coupon) {
            $existing_coupon->delete();
        }, $coupons);
        
        $this->coupon1->save();
        
        $this->coupon2 = new Coupon();
        $this->coupon2->prefix = UnitTest::get_random_string(2);
        $this->coupon2->code = UnitTest::get_random_string(4, "123456789");
        $this->coupon2->amount = rand(1,20);
        $this->coupon2->business_id = $this->business_id;
        $this->coupon2->endDate = $endDate;
        $this->coupon2->combine = 1;
        $this->coupon2->amountType = "dollars";
        $this->coupon2->description = sprintf("\$%s off until used up", $this->coupon2->amount);
        $this->coupon2->frequency = "until used up";
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon2->code, "business_id" => $this->coupon2->business_id));
        array_map(function($coupon) {
            $coupon->delete(); 
        }, $coupons);
        
        $this->coupon2->save();      
        
        //THe following stateemnets add two redeemed,combination discounts.
        $this->customerDiscount1 = new CustomerDiscount();
        $this->customerDiscount1->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount1->amount = $this->coupon1->amount;
        $this->customerDiscount1->amountType = $this->coupon1->amountType;
        $this->customerDiscount1->expireDate = $this->coupon1->endDate;
        $this->customerDiscount1->couponCode = $this->coupon1->prefix . $this->coupon1->code;
        $this->customerDiscount1->frequency = $this->coupon1->frequency;
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->active = 1;
        $this->customerDiscount1->combine =$this->coupon1->combine;
        $this->customerDiscount1->origCreateDate = new DateTime();
        $this->customerDiscount1->origCreateDate->sub(new DateInterval("P1D"));
        $this->customerDiscount1->business_id = $this->business_id;
        $this->customerDiscount1->save();
        
        $this->customerDiscount2 = new CustomerDiscount();
        $this->customerDiscount2->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount2->amount = $this->coupon2->amount;
        $this->customerDiscount2->amountType = $this->coupon2->amountType;
        $this->customerDiscount2->expireDate = $this->coupon2->endDate;
        $this->customerDiscount2->couponCode = $this->coupon2->prefix . $this->coupon2->code;
        $this->customerDiscount2->frequency = $this->coupon2->frequency;     
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->active = 1;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->business_id = $this->business_id;
        $this->customerDiscount2->origCreateDate = new DateTime();
        
        $this->customerDiscount2->save();
                
        $this->login();
                
        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);        
    }
    
    /**
     * The following test verifies that applying two combination discounts of types 'one time' and 'until used up' are both applied to an order. 
     */
    public function testApplyTwoCombinationDiscounts()
    {

        $total_after_applying_first_discount = max($this->order->getTotal() - $this->customerDiscount1->amount, 0);
        $expected_until_used_up_remainder = max($this->customerDiscount2->amount - $total_after_applying_first_discount,0);
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->{Order::$primary_key}}]")->click();
        
        //Note, 'one time' discounts are applied before 'until used up' discounts.        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        $this->driver->assert_string_present(sprintf("%s (\$%s remaining)", $this->customerDiscount2->description, number_format($expected_until_used_up_remainder,2)));
        $expected_total = $this->order->get_gross_total() - $this->coupon1->amount - $this->coupon2->amount;
        $this->assertEquals($expected_total, $this->order->getTotal(), "Order {$this->order->orderID} does not have the expected total", 0.1);
       
    }    
    
    /**
     * The following test verifies that applying that only the first non combination discount is applied out of the two non-combination discounts available. 
     */
    public function testApplyTwoNonCombinationDiscounts()
    {
        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable -- ";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->{Order::$primary_key}}]")->click();
        
      $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));      
        
        $expected_total = $this->order->get_gross_total() - $this->coupon1->amount;
        $this->assertEquals($expected_total, $this->order->getTotal(), "Order {$this->order->orderID} does not have the expected total", 0.1);
    }       
}

?>
