<?php
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;

class OneTimePercentCombinationsDiscountsTest extends EmployeeUnitTest
{
    private $coupon1;
    private $coupon2;
    private $customerDiscount1;
    private $customerDiscount2;
    private $orderItem1;
    private $orderItem2;
    private $orderItem3;
    private $order;
    
    /**
     * The following setup function creates a random order with three order items in it, 2 dollar one-type coupons, and two customer discounts for the test customer. 
     */
    public function setUp() 
    {
        parent::setUp();
        
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        
        $this->order = UnitTest::create_random_order($this->business_id);
        $this->orderItem1 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem2 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem3 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        
        $this->order = new Order($this->order->orderID, true);        
        
        $this->coupon1 = new Coupon();
        $this->coupon1->prefix = "AB";
        $this->coupon1->code = "1234";
        $this->coupon1->amount = rand(1,20);
        $this->coupon1->business_id = $this->business_id;
        $this->coupon1->endDate = $endDate;
        $this->coupon1->combine = 1;
        $this->coupon1->amountType = "percent";
        $this->coupon1->description = sprintf("\$%s one time", $this->coupon1->amount);
        $this->coupon1->frequency = "one time";
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon1->code, "business_id" => $this->coupon1->business_id));
        array_map(function($coupon) {
            $coupon->delete();
        }, $coupons);
        
        $this->coupon1->save();
        
        $this->coupon2 = new Coupon();
        $this->coupon2->prefix = "CD";
        $this->coupon2->code = "5678";
        $this->coupon2->amount = rand(1,50);
        $this->coupon2->business_id = $this->business_id;
        $this->coupon2->endDate = $endDate;
        $this->coupon2->combine = 1;
        $this->coupon2->amountType = "percent";
        $this->coupon2->description = sprintf("\$%s one time", $this->coupon2->amount);
        $this->coupon2->frequency = "one time";
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon2->code, "business_id" => $this->coupon2->business_id));
        array_map(function($coupon) {
            $coupon->delete(); 
        }, $coupons);
        
        $this->coupon2->save();      
        
        //THe following stateemnets add two redeemed,combination discounts.
        $this->customerDiscount1 = new CustomerDiscount();
        $this->customerDiscount1->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount1->amount = $this->coupon1->amount;
        $this->customerDiscount1->amountType = $this->coupon1->amountType;
        $this->customerDiscount1->expireDate = $this->coupon1->endDate;
        $this->customerDiscount1->couponCode = $this->coupon1->prefix . $this->coupon1->code;
        $this->customerDiscount1->frequency = $this->coupon1->frequency;
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->active = 1;
        $this->customerDiscount1->combine = $this->coupon1->combine;
        $this->customerDiscount1->business_id = $this->business_id;
        $this->customerDiscount1->save();
        
        $this->customerDiscount2 = new CustomerDiscount();
        $this->customerDiscount2->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount2->amount = $this->coupon2->amount;
        $this->customerDiscount2->amountType = $this->coupon2->amountType;
        $this->customerDiscount2->expireDate = $this->coupon2->endDate;
        $this->customerDiscount2->couponCode = $this->coupon2->prefix . $this->coupon2->code;
        $this->customerDiscount2->frequency = $this->coupon2->frequency;        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->active = 1;
        $this->customerDiscount2->combine = $this->coupon1->combine;
        $this->customerDiscount2->business_id = $this->business_id;
        $this->customerDiscount2->save();
                
        $this->login();
                
        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);
    }
    
    /**
     * The following test verifies the functionality for applying two percent combiantion discounts to an order.
     */
    public function testApplyTwoPercentCombinationDiscounts()
    {
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        $percent_discount1 = 0.01 * $this->customerDiscount1->amount;
        $percent_discount2 = 0.01 * $this->customerDiscount2->amount;
        
        $first_discount = $percent_discount1 * $this->order->get_gross_total();
        $second_discount = $percent_discount2 * ($this->order->get_gross_total() - $first_discount);
        $expected_discount = $first_discount + $second_discount;
        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount2->description, $this->customerDiscount2->couponCode));
        
        $expected_total = number_format($this->order->get_gross_total() - $expected_discount, 2);
        $actual_total = $this->order->getTotal();     
        $this->assertEquals($expected_total, $actual_total, "Order {$this->order->orderID} does not have the expected total", 0.1);
        

    }
}

?>
