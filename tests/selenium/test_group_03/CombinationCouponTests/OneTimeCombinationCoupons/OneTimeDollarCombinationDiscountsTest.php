<?php
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;
class OneTimeDollarCombinationDiscountsTest extends EmployeeUnitTest 
{
    private $coupon1;
    private $coupon2;
    private $customerDiscount1;
    private $customerDiscount2;
    private $orderItem1;
    private $orderItem2;
    private $orderItem3;
    private $order;
    
    /**
     * The following setup function creates a random order with three order items in it, 2 dollar one-type coupons, and two customer discounts for the test customer. 
     */
    public function setUp() 
    {
        parent::setUp();
        
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        
        $this->order = UnitTest::create_random_order($this->business_id);
        
        $this->orderItem1 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem2 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem3 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        
        $this->order = new Order($this->order->orderID, true);   
        //The following statements remove any existing customer discounts for the customer associated with this order.
        $this->CI->load->model("customerdiscount_model");
        $this->CI->customerdiscount_model->delete(array("customer_id" => $this->order->customer_id));
        
        $this->coupon1 = new Coupon();
        $this->coupon1->prefix = "AB";
        $this->coupon1->code = "1234";
        $this->coupon1->amount = rand(1,20);
        $this->coupon1->business_id = $this->business_id;
        $this->coupon1->endDate = $endDate;
        $this->coupon1->combine = 1;
        $this->coupon1->amountType = "dollars";
        $this->coupon1->frequency = "one time";
        $this->coupon1->description = sprintf("\$%s Off Next order", $this->coupon1->amount);
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon1->code, "business_id" => $this->coupon1->business_id));
        array_map(function($coupon) {
            $coupon->delete();
        }, $coupons);
        
        $this->coupon1->save();
        
        $this->coupon2 = new Coupon();
        $this->coupon2->prefix = "CD";
        $this->coupon2->code = "5678";
        $this->coupon2->amount = $this->coupon1->amount / 2.0;
        $this->coupon2->business_id = $this->business_id;
        $this->coupon2->endDate = $endDate;
        $this->coupon2->combine = 1;
        $this->coupon2->amountType = "dollars";
        $this->coupon2->frequency = "one time";
        $this->coupon2->description = sprintf("\$%s off next order", $this->coupon2->amount);
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon2->code, "business_id" => $this->coupon2->business_id));
        array_map(function($coupon) {
            $coupon->delete(); 
        }, $coupons);
        
        $this->coupon2->save();      
        
        //THe following stateemnets add two redeemed,combination discounts.
        $this->customerDiscount1 = new CustomerDiscount();
        $this->customerDiscount1->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount1->amount = $this->coupon1->amount;
        $this->customerDiscount1->amountType = $this->coupon1->amountType;
        $this->customerDiscount1->expireDate = $this->coupon1->endDate;
        $this->customerDiscount1->couponCode = $this->coupon1->prefix . $this->coupon1->code;
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->frequency = $this->coupon1->frequency;
        $this->customerDiscount1->active = 1;
        $this->customerDiscount1->origCreateDate = new DateTime();
        $this->customerDiscount1->combine = $this->coupon1->combine;
        $this->customerDiscount1->business_id = $this->business_id;
        $this->customerDiscount1->save();
        
        $this->customerDiscount2 = new CustomerDiscount();
        $this->customerDiscount2->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount2->amount = $this->coupon2->amount;
        $this->customerDiscount2->amountType = $this->coupon2->amountType;
        $this->customerDiscount2->expireDate = $this->coupon2->endDate;
        $this->customerDiscount2->couponCode = $this->coupon2->prefix . $this->coupon2->code;
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->frequency = $this->coupon2->frequency;
        $this->customerDiscount2->active = 1;
        $this->customerDiscount2->origCreateDate = new DateTime();
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->business_id = $this->business_id;
        $this->customerDiscount2->save();
                
        $this->login();
        
        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);   
    }
    
    /**
     * THe following test verifies that applying two dollar amount combination discounts works correctly when the two discounts are greater than the order total
     * The expected result is that only the first customer discount should be applied to the order.
     */
    public function testApplyTwoCombinationDiscountsGreaterThanOrderTotal()
    {
        $this->customerDiscount1->amount = 100;
       
        $this->customerDiscount1->save();
        $this->customerDiscount2->amount = 150;
        $this->customerDiscount2->save();
       
        $this->orderItem1->unitPrice = 1;
        $this->orderItem1->save();
        
        $this->orderItem2->unitPrice = 2;
        $this->orderItem2->save();
        
        $this->orderItem3->unitPrice = 3;
        $this->orderItem3->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        
        $actual_total = $this->order->getTotal();
        $this->assertEquals(0, $actual_total, "Order {$this->order->orderID} does not have the expected total", 0.1);
    }     
    
    public function testApplyTwoNonCombinationCoupons()
    {
        $this->coupon1->combine = 0;
        $this->coupon1->description .= " --Not Combinable--";
        $this->coupon1->save();
        
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->combine = 0;
        $this->customerDiscount1->save();
        
        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable--";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = 0;
        $this->customerDiscount2->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        $expected_order_total = $this->order->get_gross_total() - $this->coupon1->amount;

        $actual_order_total = $this->order->getTotal();
        
        $this->assertEquals($expected_order_total, $actual_order_total, "Order {$this->order->orderID} does not have the expected total", 0.1);        
    }
    


    
    /**
     * The following test verifies that both dollar combination discounts are applied to an order
     */
    public function testApplyTwoDollarCombinationDiscounts()
    {

        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $expected_order_total = max($this->order->get_gross_total() - $this->customerDiscount1->amount - $this->customerDiscount2->amount, 0);
        $actual_order_total = $this->order->getTotal();
        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount2->description, $this->customerDiscount2->couponCode));
        
        $this->assertEquals($expected_order_total, $actual_order_total, "Order {$this->order->orderID} does not have the expected total", 0.1);
        

    }   
    /**
     * The following test verifies the expected functionality when applying a combination coupon and non combination coupon to the same order. 
     * The expected result is that only the noncombination coupon, should be applied first to the order.
     */
    public function testApplyOneCombationAndOneNonCombinationCoupon()
    {
        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable--";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = 0;
        $this->customerDiscount2->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount2->description, $this->customerDiscount2->couponCode));
        
        $expected_order_total = max($this->order->get_gross_total() - $this->coupon2->amount, 0);
        $actual_order_total = $this->order->getTotal();
        $this->assertEquals($expected_order_total, $actual_order_total, "Order {$this->order->orderID} does not have the expected total", 0.1);        
    }               
    
    public function tearDown()
    {
        parent::tearDown();
        
        if ($this->coupon1 instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon1->delete();
        }
        if ($this->coupon2 instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon2->delete();
        }
    }
}

?>
