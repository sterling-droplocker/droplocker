<?php

use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Customer;
class UntilUsedUpDollarCombinationDiscountsTest extends EmployeeUnitTest
{
    
   /**
     * The following setup function creates a random order with three order items in it, 2 dollar one-type coupons, and two customer discounts for the test customer. 
     */
    public function setUp() 
    {
        parent::setUp();
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
                
        $this->order = UnitTest::create_random_order($this->business_id);
        $this->orderItem1 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem1->unitPrice = 80; //Note, we need to make sure that the order items are greater than the sum of the coupons, in order to ensure that both combination coupons are applied to the order.
        $this->orderItem1->save();
        $this->orderItem2 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem3 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        
        $this->order = new Order($this->order->orderID, true);        
        
        $this->coupon1 = new Coupon();
        $this->coupon1->prefix = "AB";
        $this->coupon1->code = "1234";
        $this->coupon1->amount = rand(1,20);
        $this->coupon1->business_id = $this->business_id;
        $this->coupon1->endDate = $endDate;
        $this->coupon1->combine = 1;
        $this->coupon1->amountType = "dollars";
        $this->coupon1->description = sprintf("\$%s one time", $this->coupon1->amount);
        $this->coupon1->frequency = "until used up";
        $this->coupon1->maxAmt = 5000;
        $coupons = Coupon::search_aprax(array("code" => $this->coupon1->code, "business_id" => $this->coupon1->business_id));
        array_map(function($coupon) {
            $coupon->delete();
        }, $coupons);
        
        $this->coupon1->save();
        
        $this->coupon2 = new Coupon();
        $this->coupon2->prefix = "CD";
        $this->coupon2->code = "5678";
        $this->coupon2->amount = rand(1,50);
        $this->coupon2->business_id = $this->business_id;
        $this->coupon2->endDate = $endDate;
        $this->coupon2->combine = 1;
        $this->coupon2->amountType = "dollars";
        $this->coupon2->description = sprintf("\$%s until used up", $this->coupon2->amount);
        $this->coupon2->frequency = "until used up";
        $this->coupon2->maxAmt = 5000;
        $coupons = Coupon::search_aprax(array("code" => $this->coupon2->code, "business_id" => $this->coupon2->business_id));
        array_map(function($coupon) {
            $coupon->delete(); 
        }, $coupons);
        
        $this->coupon2->save();      
        
        //THe following stateemnets add two redeemed,combination discounts.
        $this->customerDiscount1 = new CustomerDiscount();
        $this->customerDiscount1->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount1->amount = $this->coupon1->amount;
        $this->customerDiscount1->amountType = $this->coupon1->amountType;
        $this->customerDiscount1->expireDate = $this->coupon1->endDate;
        $this->customerDiscount1->couponCode = $this->coupon1->prefix . $this->coupon1->code;
        $this->customerDiscount1->frequency = $this->coupon1->frequency;
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->active = 1;
        $this->customerDiscount1->combine =$this->coupon1->combine;
        $this->customerDiscount1->business_id = $this->business_id;
        $this->customerDiscount1->save();
        
        $this->customerDiscount2 = new CustomerDiscount();
        $this->customerDiscount2->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount2->amount = $this->coupon2->amount;
        $this->customerDiscount2->amountType = $this->coupon2->amountType;
        $this->customerDiscount2->expireDate = $this->coupon2->endDate;
        $this->customerDiscount2->couponCode = $this->coupon2->prefix . $this->coupon2->code;
        $this->customerDiscount2->frequency = $this->coupon2->frequency;     
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->active = 1;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->business_id = $this->business_id;
        $this->customerDiscount2->save();
                
        $this->login();
                
        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);
    }
    /**
     * 
     */
    public function testApplyPartOfCombinationDiscount()
    {
        $difference = 20;
        $this->customerDiscount2->delete();
        $total = $this->order->getTotal();
        $this->customerDiscount1->amount = $total + $difference;
        $this->customerDiscount1->description = sprintf("\$%s until used up", $this->customerDiscount1->amount);
        $this->customerDiscount1->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        $this->driver->assert_string_present(sprintf("%s (\$%s remaining)", $this->customerDiscount1->description, number_format($difference, 2)));
        
        
        $customerDiscounts = CustomerDiscount::search_aprax(array("customer_id" => $this->order->relationships['Customer'][0]->{Customer::$primary_key}, "amount" => $difference, "description" => $this->customerDiscount1->description));
        $customerDiscount = $customerDiscounts[0];
        
        $this->assertEquals($this->customerDiscount1->combine, $customerDiscount->combine, "The combine property does not match the original discount's combine property value.");
        $this->assertEquals($difference, $customerDiscount->amount, "Order {$this->order->orderID} does not have the expected total");
    }
    /**
     * The following test verifies that that only the combination coupon is applied to an order if the customer has a combination discount and a noncombination discount associated to his/her account.
     */
    public function testApplyOneCombinationAndOneNonCombinationCoupon()
    {
        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable--";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->save();
        
        $difference = $this->order->getTotal() - $this->customerDiscount2->amount;
        if ($difference >= 0)
        {
            $remainder = 0;
        }
        else
        {
            $remainder = $this->customerDiscount2->amount - $this->order->getTotal();
        }
        
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $this->driver->assert_string_present(sprintf("%s (\$%s remaining)", $this->customerDiscount2->description, number_format($remainder,2)));
          
        $expected_order_total = max($this->order->get_gross_total() - $this->coupon2->amount,0);
        $this->assertEquals($expected_order_total, $this->order->getTotal(), "Order {$this->order->orderID} does not have the expected total");        
    }         
    
    /**
     * THe following test verifies that applying two dollar amount combination discounts works correctly when the two discounts are greater than the order total
     */
    public function testApplyTwoCombinationDiscountsGreaterThanOrderTotal()
    {
        
        $this->orderItem1->unitPrice = 1;
        $this->orderItem1->save();
        
        $this->orderItem2->unitPrice = 2;
        $this->orderItem2->save();
        
        $this->orderItem3->unitPrice = 3;
        $this->orderItem3->save();
        
        $this->customerDiscount1->description = "$100 until used up";
        $this->customerDiscount1->amount = 100;

        $this->customerDiscount1->save();

        $difference = $this->customerDiscount1->amount - $this->order->getTotal();
        
        $this->customerDiscount2->description = "150 until used up";
        $this->customerDiscount2->amount = 150;
        $this->customerDiscount2->save();
       
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        //Only the first discount should be applied to the order.
        $this->driver->assert_string_present(sprintf("%s (%s remaining)", $this->customerDiscount1->description, number_format($difference,2)));
        
        $this->assertEquals(0, $this->order->getTotal(), "Order {$this->order->orderID} does not have the expected total");
    }      
    
    /**
     * The following test verifies that only the first noncombination coupon is applied to an order
     */
    public function testApplyTwoNonCombinationCoupons()
    {
        $this->coupon1->combine = 0;
        $this->coupon1->description .= " --Not Combinable--";
        $this->coupon1->save();
        
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->combine = $this->coupon1->combine;
        $this->customerDiscount1->save();
        
        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable--";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->save();
        
        $difference = $this->order->getTotal() - $this->customerDiscount1->amount;
        if ($difference > 0)
        {
            $remainder = 0;
        }
        else
        {
            $remainder = abs($difference);
        }
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        //Note, an until used up discount shows the coupon description and the remainder left in the discount
        $this->driver->assert_string_present(sprintf("%s (%s remaining)", $this->customerDiscount1->description, number_format($remainder,2)));
                
        $expected_order_total = max($this->order->get_gross_total() - $this->coupon1->amount, 0);
        $this->assertEquals($expected_order_total, $this->order->getTotal(), "Order {$this->order->orderID} does not have the expected total");   
        

    }
    
     /**
     * THe following test verifies that applying two dollar amount combination discounts works correctly 
     */
    public function testApplyTwoDollarCombinationDiscounts()
    {

        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $expected_order_total = max($this->order->get_gross_total() - $this->customerDiscount1->amount - $this->customerDiscount2->amount, 0);
        
        $this->driver->assert_string_present($this->coupon1->description);
        $this->driver->assert_string_present($this->coupon2->description);
        
        $this->assertEquals($expected_order_total, $this->order->getTotal(), "Order {$this->order->orderID} does not have the expected total");
        

    }
    

    public function tearDown()
    {
        if ($this->coupon1 instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon1->delete();
        }
        if ($this->coupon2 instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon2->delete();
        }
        if ($this->customerDiscount1 instanceof \App\Libraries\DroplockerObjects\CustomerDiscount)
        {
            $this->customerDiscount1->delete();
        }
        if ($this->customerDiscount2 instanceof \App\Libraries\DroplockerObjects\CustomerDiscount)
        {
            $this->customerDiscount2->delete();
        }
        if ($this->orderItem1 instanceof \App\Libraries\DroplockerObjects\OrderItem)
        {
            $this->orderItem1->delete();
        }
        if ($this->orderItem2 instanceof \App\Libraries\DroplockerObjects\OrderItem)
        {
            $this->orderItem2->delete();
        }
        if ($this->orderItem2 instanceof \App\Libraries\DroplockerObjects\OrderItem)
        {
            $this->orderItem2->delete();
        }
        if ($this->orderItem3 instanceof \App\Libraries\DroplockerObjects\OrderItem)
        {
            $this->orderItem3->delete();
        }      
        if ($this->order instanceof \App\Libraries\DroplockerObjects\Order)
        {
            $this->order->delete();
        }
    }
}

?>
