<?php
use App\Libraries\DroplockerObjects\Coupon;
Use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;
class CustomerOneTimeCombinationCouponTest extends CustomerUnitTest
{
    private $coupon1;
    private $coupon2;
    private $order;
    private $customerDiscount1;
    private $customerDiscount2;
    public function setUp() 
    {
        parent::setUp();
        
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        
        $this->coupon1 = new Coupon();
        $this->coupon1->prefix = "AB";
        $this->coupon1->code = "1234";
        $this->coupon1->amount = "5.00";
        $this->coupon1->description = "$5.00 off every order";
        $this->coupon1->business_id = $this->business_id;
        $this->coupon1->endDate = $endDate;
        $this->coupon1->combine = 1;
        $this->coupon1->amountType = "dollars";
        $this->coupon1->frequency = "one time";
        
        $existing_coupons1 = Coupon::search_aprax(array("prefix" => $this->coupon1->prefix, "code" => $this->coupon1->code));
        array_map(function($existing_coupon) {
            $existing_coupon->delete();
        }, $existing_coupons1);
        
        $this->coupon1->save();
        
        $this->coupon2 = new Coupon();
        $this->coupon2->prefix = "CD";
        $this->coupon2->code = "5678";
        $this->coupon2->amount = "10.00";
        $this->coupon1->description = "$10.00 off every order";
        $this->coupon2->business_id = $this->business_id;
        $this->coupon2->endDate = $endDate;
        $this->coupon2->combine = 1;
        $this->coupon2->amountType = "dollars";
        $this->coupon2->frequency = "one time";
        
        $existing_coupons2 = Coupon::search_aprax(array("prefix" => $this->coupon2->prefix, "code" => $this->coupon2->code));
        array_map(function($existing_coupon) {
            $existing_coupon->delete();
                    
            }, $existing_coupons2);
            
        $this->coupon2->save();        
        
        $this->order = UnitTest::create_random_order($this->business_id, $this->test_customer->{Customer::$primary_key});
        //Note, an order needs to in the 'inventoried' status in order for a coupon to be applied automatically."
        
        
        $this->order->orderStatusOption_id = 3;
        $this->order->save();
        
        UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        UnitTest::create_random_orderItem($this->order->{Order::$primary_key});

        $this->login();
    }
    /**
     * The following test verifies that when a customer with an existing order applies a coupon, that the coupon is applied to the order.
     */
    public function testRedeemTwoDollarOneTimeCombinationCouponsOnOpenOrder()
    {
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        
        $discounts_url = "http://laundrylocker.droplocker.{$this->domain_extension}/account/programs/discounts";
        $this->driver->load($discounts_url);
        $this->driver->get_element("name=code")->send_keys($this->coupon1->prefix . $this->coupon1->code);
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.alert-success")->assert_text_contains("A credit for \${$this->coupon1->amount} has been placed in your account.\nCredit applied to order: {$this->order->{Order::$primary_key}} for ($5.00)");
        
        $this->driver->load("http://laundrylocker.droplocker.{$this->domain_extension}/account/orders/view_orders");
        
        $customerDiscounts1 = CustomerDiscount::search_aprax(array("couponCode" => $this->coupon1->prefix . $this->coupon1->code, "customer_id" => $this->test_customer->{Customer::$primary_key}));
        $this->assertNotEmpty($customerDiscounts1, "Could not find first redeemed discount");
        
        $this->driver->get_element("link text={$this->order->{Order::$primary_key}}")->click();
        $this->driver->assert_string_present($this->coupon1->prefix . $this->coupon1->code);
        
        $this->customerDiscount1 = $customerDiscounts1[0];
        
        $this->assertEquals($this->test_customer->customerID, $this->customerDiscount1->customer_id, "The customer ID of the discount does not match the test customer ID");
        $this->assertEquals($this->coupon1->amount, $this->customerDiscount1->amount);
        $this->assertEquals($this->coupon1->amountType, $this->customerDiscount1->amountType);
        $this->assertEquals($this->coupon1->frequency, $this->customerDiscount1->frequency);
        $this->assertEquals($this->coupon1->combine, $this->customerDiscount1->combine, "The customerDiscount 'combine' property does not match the originating coupon.");
        
        $this->driver->load($discounts_url);
        $this->driver->get_element("name=code")->send_keys($this->coupon2->prefix . $this->coupon2->code);
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("link text={$this->order->{Order::$primary_key}}");
        $this->driver->assert_string_present($this->coupon2->prefix, $this->coupon2->code);
        $customerDiscounts2 = CustomerDiscount::search_aprax(array("couponCode" => $this->coupon2->prefix . $this->coupon2->code, "customer_id" => $this->test_customer->{Customer::$primary_key}));
        $this->assertNotEmpty($customerDiscounts2, "Could not find second redeemed discount");
        $this->customerDiscount2 = $customerDiscounts2[0];
        
        $this->assertEquals($this->test_customer->customerID, $this->customerDiscount2->customer_id);
        $this->assertEquals($this->coupon2->amount, $this->customerDiscount2->amount);
        $this->assertEquals($this->coupon2->amountType, $this->customerDiscount2->amountType);
        $this->assertEquals($this->coupon2->frequency, $this->customerDiscount2->frequency);
        $this->assertEquals($this->coupon2->combine, $this->customerDiscount2->combine);    
        
        $employeeUnitTest = new EmployeeUnitTest($this->driver);
        $employeeUnitTest->setUp();
        $employeeUnitTest->login();

        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}");
        
        $expected_order_total = max($this->order->get_gross_total() - $this->coupon1->amount - $this->coupon2->amount, 0);
        
        $this->assertEquals($expected_order_total, $this->order->getTotal(), "The order does not have the expected total");
        
        $this->driver->assert_string_present($this->coupon1->prefix . $this->coupon1->code);
        $this->driver->assert_string_present($this->coupon2->prefix . $this->coupon2->code);
    }
    
    public function tearDown()
    {
        if ($this->coupon1 instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon1->delete();
        }
        if ($this->coupon2 instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon2->delete();
        }
        if ($this->order instanceOf \App\Libraries\DroplockerObjects\Order)
        {
            $this->order->delete();
        }
        if ($this->order instanceof \App\Libraries\DroplockerObjects\CustomerDiscount)
        {
            $this->customerDiscount1->delete();
        }
        if ($this->order instanceof \App\Libraries\DroplockerObjects\CustomerDiscount)
        {
            $this->customerDiscount2->delete();
        }
        parent::tearDown();
    }
}

?>
