<?php

use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;

class EveryOrderDollarCombinationDiscountsTest extends EmployeeUnitTest
{
    private $coupon1;
    private $coupon2;
    private $customerDiscount1;
    private $customerDiscount2;
    private $orderItem1;
    private $orderItem2;
    private $orderItem3;
    private $order;
    
    /**
     * The following setup function creates a random order with three order items in it, 2 dollar one-type coupons, and two customer discounts for the test customer. 
     */
    public function setUp() 
    {
        parent::setUp();
        
        $endDate = new DateTime();
        $endDate->add(new DateInterval("P30D"));
        $this->order = UnitTest::create_random_order($this->business_id);

        $this->orderItem1 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem2 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->orderItem3 = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        
        $this->order = new Order($this->order->orderID, true);        
        
        $this->coupon1 = new Coupon();
        $this->coupon1->prefix = "AB";
        $this->coupon1->code = "1234";
        $this->coupon1->amount = rand(1,20);
        $this->coupon1->business_id = $this->business_id;
        $this->coupon1->endDate = $endDate;
        $this->coupon1->combine = 1;
        $this->coupon1->amountType = "dollars";
        $this->coupon1->description = sprintf("\$%s off every order", $this->coupon1->amount);
        $this->coupon1->frequency = "every order";
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon1->code, "business_id" => $this->coupon1->business_id));
        array_map(function($existing_coupon) {
            $existing_coupon->delete();
        }, $coupons);
        
        $this->coupon1->save();
        
        $this->coupon2 = new Coupon();
        $this->coupon2->prefix = "CD";
        $this->coupon2->code = "5678";
        $this->coupon2->amount = rand(1,50);
        $this->coupon2->business_id = $this->business_id;
        $this->coupon2->endDate = $endDate;
        $this->coupon2->combine = 1;
        $this->coupon2->amountType = "dollars";
        $this->coupon2->description = sprintf("\$%s off every order", $this->coupon2->amount);
        $this->coupon2->frequency = "every order";
        
        $coupons = Coupon::search_aprax(array("code" => $this->coupon2->code, "business_id" => $this->coupon2->business_id));
        array_map(function($coupon) {
            $coupon->delete(); 
        }, $coupons);
        
        $this->coupon2->save();      
        
        //THe following stateemnets add two redeemed,combination discounts.
        $this->customerDiscount1 = new CustomerDiscount();
        $this->customerDiscount1->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount1->amount = $this->coupon1->amount;
        $this->customerDiscount1->amountType = $this->coupon1->amountType;
        $this->customerDiscount1->expireDate = $this->coupon1->endDate;
        $this->customerDiscount1->couponCode = $this->coupon1->prefix . $this->coupon1->code;
        $this->customerDiscount1->frequency = $this->coupon1->frequency;
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->active = 1;
        $this->customerDiscount1->combine =$this->coupon1->combine;
        $this->customerDiscount1->origCreateDate = new DateTime();
        $this->customerDiscount1->origCreateDate->sub(new DateInterval("P1D"));
        $this->customerDiscount1->business_id = $this->business_id;
        $this->customerDiscount1->save();
        
        $this->customerDiscount2 = new CustomerDiscount();
        $this->customerDiscount2->customer_id = $this->order->relationships['Customer'][0]->customerID;
        $this->customerDiscount2->amount = $this->coupon2->amount;
        $this->customerDiscount2->amountType = $this->coupon2->amountType;
        $this->customerDiscount2->expireDate = $this->coupon2->endDate;
        $this->customerDiscount2->couponCode = $this->coupon2->prefix . $this->coupon2->code;
        $this->customerDiscount2->frequency = $this->coupon2->frequency;     
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->active = 1;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->origCreateDate = new DateTime();
        $this->customerDiscount2->business_id = $this->business_id;
        $this->customerDiscount2->save();
                
        $this->login();
                
        $url = "http://droplocker.{$this->domain_extension}/admin/orders/details/{$this->order->orderID}";
        $this->driver->load($url);
    }

   /**
     * The following test verifies that that only the combination coupon is applied to an order
     */
    public function testApplyOneCombinationAndOneNonCombinationCoupon()
    {

        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable--";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $expected_order_total = max($this->order->get_gross_total() - $this->coupon2->amount, 0);
        $actual_total = $this->order->getTotal();
        $this->assertEquals($expected_order_total, $actual_total, "Order {$this->order->orderID} does not have the expected total", 0.1); 
    }             
    
    /**
     * The following test verifies the correct functionality when applying two non combination coupons to the order. The expected result is that only the first noncombination coupon is applied to the order.
     */
    public function testApplyTwoNonCombinationCoupons()
    {
        $this->coupon1->combine = 0;
        $this->coupon1->description .= " --Not Combinable--";
        $this->coupon1->save();
        
        $this->customerDiscount1->description = $this->coupon1->description;
        $this->customerDiscount1->combine = $this->coupon1->combine;
        $this->customerDiscount1->save();
        
        $this->coupon2->combine = 0;
        $this->coupon2->description .= " -- Not Combinable--";
        $this->coupon2->save();
        
        $this->customerDiscount2->description = $this->coupon2->description;
        $this->customerDiscount2->combine = $this->coupon2->combine;
        $this->customerDiscount2->save();
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        //Note, only the first noncombiantion discount should be applied to this order.
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));;
        $expected_total = $this->order->get_gross_total() - $this->coupon1->amount;
        $actual_total = $this->order->getTotal();
        $this->assertEquals($expected_total, $actual_total, "Order {$this->order->orderID} does not have the expected total");        
    }
    

     /**
     * THe following test verifies that applying two dollar amount combination discounts works correctly 
     */
    public function testApplyTwoDollarCombinationDiscounts()
    {

        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount2->description, $this->customerDiscount2->couponCode));
        
        $expected_order_total = max($this->order->get_gross_total() - $this->customerDiscount1->amount - $this->customerDiscount2->amount, 0);
        $actual_total = $this->order->getTotal();
        $this->assertEquals($expected_order_total, $actual_total, "Order {$this->order->orderID} does not have the expected total", 0.1);
        

    }
    
    /**
     * THe following test verifies that applying two dollar amount combination discounts works correctly when the two discounts are greater than the order total.
     */
    public function testApplyTwoCombinationDiscountsGreaterThanOrderTotal()
    {

        
        $this->customerDiscount1->amount = $this->order->getTotal() + 50;
        $this->customerDiscount1->description = "\${$this->customerDiscount1->amount} off every order";
        $this->customerDiscount1->save();
       
        
        $this->driver->get_element("id=process_next_order")->click();
        $this->driver->get_element("link text=Back to order[{$this->order->orderID}]")->click();
        
        //Note, only the first coupon should be applied to the order.
        $this->driver->assert_string_present(sprintf("%s (%s)", $this->customerDiscount1->description, $this->customerDiscount1->couponCode));
        
        $actual_total = $this->order->getTotal();
        $this->assertEquals(0, $actual_total, "Order {$this->order->orderID} does not have the expected total", 0.1);
    }     
}

?>
