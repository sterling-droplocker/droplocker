<?php
use App\Libraries\DroplockerObjects\Coupon;

class EmployeeCouponTest extends EmployeeUnitTest
{
 
    private $coupon;
    public function setUp()
    {
        parent::setUp();
        $this->login();
        
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/");

    }
    
    
    /**
     * The following test creates a coupon with the following attributes
     * amount type: dollar
     * Frequency: one time
     * 1st order only 
     * Combine
     * Recurring: monthly
     */
    public function testCreateOneTimeCombineRecurringDollarPostCardCoupon()
    {
        $extendedDesc = md5(date("U") + $this->domain_extension);
        $recurring_type_id = "2"; //THis should be the monthly recurring type
        $amountType = "dollars";
        $frequency = "one time";
        $amount = 10.00;
        $combine = 1;
        
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Manage Coupons")->click();

        sleep(5); // This statement is used as a workaround to a bug in selenium.
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("name=amount")->send_keys($amount);
        $this->driver->get_element("name=amountType")->select_value($amountType);
        $this->driver->get_element("name=frequency")->select_value($frequency);
        $this->driver->get_element("name=combine")->click();
        $this->driver->get_element("name=recurring_type_id")->select_value($recurring_type_id);
        
        $this->driver->get_element("name=extendedDesc")->send_keys($extendedDesc);
        
        $this->driver->get_element("name=recurring_type_id")->submit();
        
        $this->driver->assert_element_present("css=div.success");
        
        $coupons = Coupon::search_aprax(array("extendedDesc" => $extendedDesc));
        $this->assertNotEmpty($coupons);
        $this->assertEquals(count($coupons), 1);
        $coupon = $coupons[0];
        
        $this->assertEquals($amount, $coupon->amount);
        $this->assertEquals($recurring_type_id, $coupon->recurring_type_id);
        $this->assertEquals($amountType, $coupon->amountType);
        $this->assertEquals($amount, $coupon->amount,  "The coupon does not have the expected amount");
        $this->assertEquals($combine, $coupon->combine, "The coupon does not have the 'combine' property set.");
        $this->driver->get_element("css=div.success")->assert_text(sprintf("Code %s%s created for %s", $coupon->prefix, $coupon->code, $this->business->companyName));
        
        //Cleanup
        
        $coupon->delete();
    }
    


    /**
     * The following test verifies the functionality for modifying a coupon through the employee interface.
     * @throws Exception 
     */
    public function testModifyCoupon()
    {
        $prefix = "66";
        $code = "777777";
        
        $couponObj = \App\Libraries\DroplockerObjects\Coupon::search_aprax(array('code' => $code));
        if(isset($couponObj[0])){
            $couponObj[0]->delete();
        }
        
        $this->coupon = UnitTest::create_random_coupon($this->business_id);
        
        $this->coupon = UnitTest::get_random_active_coupon($this->business_id, false);
        $description = "test description";
        $extendedDesc = "test extended description";
        $startDate = "06/01/88";
        $endDate = "08/06/10";
        $amount = "6.66";

        $couponID = $this->coupon->{Coupon::$primary_key};
        
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Manage Coupons")->click();

        $this->driver->get_element("css=#active_coupons_filter label input")->send_keys($this->coupon->code);

        $this->driver->get_element("id=prefix-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($prefix);
        $this->driver->get_active_element()->submit();        
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=prefix-$couponID")->assert_text($prefix);

        $this->driver->get_element("id=code-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($code);
        $this->driver->get_active_element()->submit();

        $this->wait_for_ajax_to_complete();

        $this->driver->get_element("css=#active_coupons_filter label input")->clear();
        $this->driver->get_element("css=#active_coupons_filter label input")->send_keys($code); //Note, we neeed to search for the new coupon code.

        $this->driver->get_element("id=code-$couponID")->assert_text($code);

        $this->driver->get_element("id=description-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($description);
        $this->driver->get_active_element()->submit();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=description-$couponID")->assert_text($description);

        $this->driver->get_element("id=extendedDesc-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($extendedDesc);
        $this->driver->get_active_element()->submit();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=extendedDesc-$couponID")->assert_text($extendedDesc);

        $this->driver->get_element("id=startDate-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($startDate);
        $this->driver->get_active_element()->submit();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=startDate-$couponID")->assert_text($startDate);

        $this->driver->get_element("id=endDate-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($endDate);
        $this->driver->get_active_element()->submit();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=endDate-$couponID")->assert_text($endDate);

        $this->driver->get_element("id=amount-$couponID")->click();
        $this->driver->get_active_element()->clear();
        $this->driver->get_active_element()->send_keys($amount);
        $this->driver->get_active_element()->submit();
        $this->wait_for_ajax_to_complete();
        $this->driver->get_element("id=amount-$couponID")->assert_text($amount);

        $this->wait_for_ajax_to_complete();

        $this->coupon->refresh();;
        $this->assertEquals($prefix, $this->coupon->prefix);
        $this->assertEquals($description, $this->coupon->description);
        $this->assertEquals($extendedDesc, $this->coupon->extendedDesc);
        $this->assertEquals($amount, $this->coupon->amount);



    }    
    public function tearDown()
    {
        parent::tearDown();
        if ($this->coupon instanceOf \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon->delete();
        }
    }

}
