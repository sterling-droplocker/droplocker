<?php
use \App\Libraries\DroplockerObjects\Item;
use \App\Libraries\DroplockerObjects\ItemIssue;
use \App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Customer;

class EmployeeItemIssueTest extends EmployeeUnitTest
{
    private $itemIssue;
    private $item;
    private $barcode;
    private $order;
    private $orderItem;
    private $customer_item;
    private $customer;
    public function setUp()
    {
        parent::setUp();

        $this->customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        $this->order = UnitTest::create_random_order($this->business_id, $this->customer->{Customer::$primary_key});
        $this->item = UnitTest::create_random_item($this->order->customer_id);
        
        $this->customer_item = UnitTest::create_random_customer_item($this->customer->{Customer::$primary_key}, $this->item->{Item::$primary_key});
        $this->barcode = UnitTest::create_random_barcode($this->business_id, $this->item->{Item::$primary_key});
        $this->orderItem = UnitTest::create_random_orderItem($this->order->{Order::$primary_key});
        $this->order->get_relationships();
        $this->item->get_relationships();
        $this->login();
    }
    
    /**
     * The following test verifies the functionality for creating and deleting an item issue as an employee user. 
     */
    public function testAddAndDeleteIssueAsEmployee()
    {
        
        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/edit_item?itemID={$this->item->{Item::$primary_key}}&orderID={$this->order->{Order::$primary_key}}");
        $this->driver->get_element("link text=Add/Edit Issues")->click();

        $this->driver->assert_string_present($this->item->relationships['Barcode'][0]->barcode);
        
        $front_or_back = "back";
        $issue_type = "Repair";
        $issueTypes = $this->driver->get_all_elements("name=issueType");
        $found = false;
        $this->driver->get_element("issue_image")->click();
        foreach ($issueTypes as $issueType)
        {
            if ($issueType->get_value() == $issue_type)
            {
                $issueType->click();
                $found = true;
                break;
            }
        }
        $this->assertTrue($found, "Could not find issue type radio button");
        $item_issue_notes = UnitTest::get_random_word(3);
        
        
        //The following statements verify the functionality for creating a new issue.
        $this->driver->get_element("name=note")->send_keys($item_issue_notes);
        
        $this->driver->get_element("name=frontBack")->select_value($front_or_back);
        
        $this->driver->get_element("name=submit")->click();
        sleep(3);
        $this->driver->get_element("css=div.success")->assert_text("Added New Issue");
        
        
        $this->driver->assert_string_present($item_issue_notes);

        $itemIssues = ItemIssue::search_aprax(array("status" => "Opened", "item_id" => $this->item->itemID, "frontBack" => $front_or_back, "note" => $item_issue_notes));
        $this->assertEquals(1, count($itemIssues), "Did not find exactly 1 item issue created.");
        $itemIssue = $itemIssues[0];
        
        //The following statements verify the funcitonality for modifying an edxisting item issue.
        $this->driver->get_element("modify-{$itemIssue->itemIssueID}")->click();
        $this->driver->get_element("name=note")->assert_value($item_issue_notes);
        $issueTypes = $this->driver->get_all_elements("name=issueType");
        foreach ($issueTypes as $issueType)
        {
            if ($issueType->get_value() == $issue_type)
            {
                $issueType->assert_selected();
                $found = true;
                break;
            }
        }
        $this->driver->get_element("name=frontBack")->assert_value($front_or_back);
        
        $updated_item_notes = "Test Updated Item Notes";
        //THe following statements verify the functionality for updating an existing item.
        $this->driver->get_element("name=note")->clear();
        $this->driver->get_element("name=note")->send_keys($updated_item_notes);
        $this->driver->get_element("name=submit")->click();
        sleep(3);
        $this->driver->get_element("css=div.success")->assert_text("Updated Issue");
        
        $this->driver->get_element("name=note")->assert_value($updated_item_notes);
        
        //The following statements verify the functinonality for deleting an existing item issue.
        $this->driver->get_element("delete-{$itemIssue->itemIssueID}")->click();
        sleep(3);
        $this->driver->get_element("css=div.success")->assert_text("Deleted Issue");
        
        $this->driver->assert_string_not_present($updated_item_notes);
        $itemIssues = ItemIssue::search_aprax(array("status" => "Opened", "item_id" => $this->item->itemID, "frontBack" => $front_or_back, "note" => $item_issue_notes));
        $this->assertEmpty($itemIssues, "Expected item issue to be deleted, but item issue was still found in database.");        
    }
    /**
     * The following test verifies the functionality that the item issue warning is displayed when an item with an associated issue is added to an order.
     * 
     */
    public function testAddItemWIthIssueToOrder()
    {
        $order = UnitTest::create_random_order($this->business_id);

        $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/details/{$order->orderID}");

        $barcode = UnitTest::create_random_barcode($this->business_id);
        $customer_item = new \App\Libraries\DroplockerObjects\Customer_Item();
        $customer_item->item_id = $barcode->item_id;
        $customer_item->customer_id = $order->customer_id;
        $customer_item->save();

        $barcode->save();
        
        //The following statements delete any exisitng item issues associated with this item.
        $itemIssues = ItemIssue::search_aprax(array("item_id" => $barcode->item_id));
        
        array_map(function ($itemIssue) {
            $itemIssue->delete();
        }, $itemIssues);
        
        
        
        $itemIssue = new ItemIssue();
        $itemIssue->item_id = $barcode->item_id;
        $itemIssue->issueType = ItemIssue::$issueTypes[array_rand(ItemIssue::$issueTypes)];
        $itemIssue->status = "Opened";
        $itemIssue->frontBack = ItemIssue::$frontBacks[array_rand(ItemIssue::$frontBacks)];
        $itemIssue->save();

        $this->driver->get_element("barcode")->send_keys($barcode->barcode);
        $this->driver->get_element("barcode")->submit();

        $this->driver->assert_string_present("{$itemIssue->issueType} / {$itemIssue->frontBack}");
        $this->driver->assert_string_present("Item Barcode {$barcode->barcode}");
        $this->driver->get_element("link text=continue")->click();

        $this->driver->assert_string_present("Item Details - {$barcode->barcode}");
        $itemIssue = new ItemIssue($itemIssue->{ItemIssue::$primary_key}, false);
        $this->assertEquals("Closed", $itemIssue->status, "The item issue is not in the expected 'Closed' status."); 
    }    

    public function teardown()
    {
        if ($this->item instanceOf \App\Libraries\DroplockerObjects\Item)
        {
            $this->item->delete();
        }
        if ($this->order instanceOf \App\Libraries\DroplockerObjects\Order)
        {
            $this->order->delete();
        }
        if ($this->orderItem instanceOf \App\Libraries\DroplockerObjects\OrderItem)
        {
            $this->orderItem->delete();
        }
        if ($this->customer instanceOf \App\Libraries\DroplockerObjects\Customer)
        {
            $this->customer->delete();
        }
        if ($this->customer_item instanceOf \App\Libraries\DroplockerObjects\Customer_Item)
        {
            $this->customer_item->delete();
        }
        if ($this->barcode instanceOf \App\Libraries\DroplockerObjects\Barcode)
        {
            $this->barcode->delete();
        }
        parent::tearDown();
    }
    
}

?>
