<?php
use App\Libraries\DroplockerObjects\Employee;
use App\Libraries\DroplockerObjects\Business_Employee;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Barcode;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Image_Placement;
use App\Libraries\DroplockerObjects\Image;
class EmployeeTest extends EmployeeUnitTest
{
    protected $employee;
    protected $business_employee;
    protected $password;    
    private $image_placement;
    private $image;
    public function setUp()
    {
        parent::setUp();
        
        $this->image = UnitTest::create_random_image($this->business_id);
        $this->image_placement = new Image_Placement();
        $this->image_placement->image_id = $this->image->{Image::$primary_key};
        $this->image_placement->placement_id = 3; //This is expected to be the 'receipt' placement name.
        $this->image_placement->save();
        $this->login();
    }
    
    /**
     * The following test verifies the funcitonality for printing temporary tags.
     * @throws Exception 
     */
     public function testPrintTempTags()
     {
         $number_of_temp_tags = 5;
         $this->driver->get_element("link text=ORDERS")->click();
        
         $this->driver->get_element("link text=Print Bag Tags")->click();
         $this->driver->get_element("name=number_to_print")->clear();
         $this->driver->get_element("name=number_to_print")->send_keys($number_of_temp_tags);
         $this->driver->get_element("name=number_to_print")->submit();
         $tags = $this->driver->get_all_elements("css=img");
        
         $this->assertEquals($number_of_temp_tags, count($tags), "The number of acutal printed temp tags does not match the expected number");
     }     




     /**
      * The following test verifies the funcitonlaity for applying a manual discount to an open order. 
      */
     public function testApplyManualDiscountToOrder()
     {
             $order = UnitTest::create_random_order($this->business_id);
             $this->driver->load("http://droplocker.{$this->domain_extension}/admin/orders/details/{$order->orderID}");
             $this->driver->get_element("10_dollar")->click();
             sleep(3);
             $this->driver->get_element("add_charge")->click();
             sleep(3);
             $this->driver->get_element("css=div.success")->assert_text("Charge has been added");   
     }

    public function tearDown()
    {
        if ($this->image instanceOf \App\Libraries\DroplockerObjects\Image)
        {
            $this->image->delete();
        }
        if ($this->image_placement instanceOf \App\Libraries\DroplockerObjects\Image_Placement)
        {
            $this->image_placement->delete();
        }
        parent::tearDown();
        
    }
}
?>
