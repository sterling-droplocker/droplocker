<?php

use App\Libraries\DroplockerObjects\EmailTemplate;
use App\Libraries\DroplockerObjects\EmailAction;
/**
 * This test suite verifies the functionality for working with email templates.
 */
class EmailTemplateTest extends EmployeeUnitTest
{
    public function setUp()
    {
        parent::setUp();
        $this->login();
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Email Templates")->click();
    }
    /**
     * The following test verifies the functionality for updatin gthe admin part of an e-mail template. 
     */
    public function testUpdateAdmin()
    {
        $edit_admin_links = $this->driver->get_all_elements("css=.edit_admin");
        $edit_admin_link = $edit_admin_links[array_rand($edit_admin_links)];
        $edit_admin_link->click();
        $admin_subject = "This is a test admin subject " . UnitTest::get_random_word(3);
        $admin_message = "This is a test admin message " . UnitTest::get_random_word(3);
        $custom_emails = "test1@example.com;test2@example.com";
        sleep(1);
        $this->driver->get_element("name=subject")->clear();
        $this->driver->get_element("name=subject")->send_keys($admin_subject);
        $this->driver->get_element("name=message")->clear();
        $this->driver->get_element("name=message")->send_keys($admin_message);
        $this->driver->get_element("name=customEmails")->clear();
        $this->driver->get_element("name=customEmails")->send_keys($custom_emails);

        $this->driver->get_element("name=submit")->click();
        sleep(3);
        $this->driver->assert_element_present("css=div.success");
        $url = $this->driver->get_url();
        $url_parts = explode("/", $url);
        $emailActionID = end($url_parts);
        $emailTemplates = EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id), true);
        if (empty($emailTemplates))
        {
            throw new Exception("Could not find email template in database.");
        }
        $emailTemplate = $emailTemplates[0];
        $this->assertEquals($admin_subject, $emailTemplate->adminSubject, "The admin subject is not the expected value.");
        $this->assertEquals($admin_message, $emailTemplate->adminText, "The admin text subject is not the expected value");
        $adminRecipients = unserialize($emailTemplate->adminRecipients);
        $this->assertEquals($custom_emails, $adminRecipients['custom']);  
        
        $this->driver->get_element("css=div.success")->assert_text("Updated {$emailTemplate->relationships['EmailAction'][0]->name} Template");
        $this->driver->get_element("name=subject")->assert_value($admin_subject, "The subject does not match the expected value");
        $this->driver->get_element("name=message")->assert_value($admin_message, "The message does not match the expected value");
        $this->driver->get_element("name=customEmails")->assert_value($custom_emails, "The 'custom email' field does not match the expected value");
    }   
    /**
     * 
     * The following test verifies the functionality for updating the sms part of an e-mail template. 
     */
    public function testUpdateSMS() {
        $edit_sms_links = $this->driver->get_all_elements("css=.edit_sms");
        $edit_sms_link = $edit_sms_links[array_rand($edit_sms_links)];

        $edit_sms_link->click();

        $sms_message = "This is a test sms message " . UnitTest::get_random_word(3);
        $this->driver->get_element("name=message")->clear();
        $this->driver->get_element("name=message")->send_keys($sms_message);
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text_contains("Updated SMS Template");
        $this->driver->get_element("name=message")->assert_value($sms_message);

        $url = $this->driver->get_url();
        $url_parts = explode("/", $url);
        $emailActionID = end($url_parts);
        $emailTemplates = EmailTemplate::search_aprax(array("emailAction_id" => $emailActionID, "business_id" => $this->business_id));
        $emailTemplate = $emailTemplates[0];
        $this->assertEquals($sms_message, $emailTemplate->smsText, "The SMS text does not match the expected value");       
    }    
    

    
    /**
     * The following test verifies the correct display of the email template management view. 
     * 
     */
    public function testEmailTemplateView()
    {

        $emailTemplates = EmailTemplate::search_aprax(array("business_id" => $this->business_id), true);

        $this->driver->assert_string_present("Alert Templates");
        $this->driver->assert_string_present("SMS");
        $this->driver->assert_string_present("Admin");
        foreach ($emailTemplates as $emailTemplate)
        {
            if (!is_null($emailTemplate->relationships['EmailAction'][0]))
            {
                $this->driver->assert_string_present($emailTemplate->relationships['EmailAction'][0]->name);
                $this->driver->assert_string_present($emailTemplate->relationships['EmailAction'][0]->description);
            }
        }
    }
    

}

?>
