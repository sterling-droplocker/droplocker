<?php
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\LockerLootConfig;
use App\Libraries\DroplockerObjects\LockerLoot;
class CustomerLockerLootTest extends CustomerUnitTest
{
    private $lockerLootConfig;
    private $lockerLoot;
    
    public function setUp()
    {
        parent::setUp();

        
        $this->promo_code = "CR{$this->test_customer->customerID}";
        $this->promo_link = "{$this->base_url}/account/main/view_registration?promo={$this->promo_code}";
        
        if (!Coupon::search_aprax(array('prefix' => 'CR', 'business_id' => $this->business_id))) {
            $coupon = new Coupon();
            $coupon->prefix = 'CR';
            $coupon->amount = 25;
            $coupon->amountType = 'percent';
            $coupon->frequency = 'until used up';
            $coupon->description = '25% off first order';
            $coupon->extendedDesc = 'New Customer Referral - 25% off for new customers';
            $coupon->code = '*';
            $coupon->maxAmt = 25;
            $coupon->combine = 0;
            $coupon->firstTime = 1;
            $coupon->business_id = $this->business_id;
            $coupon->order_id = 0;
            $coupon->recurring_type_id = 1;
            $coupon->save();
        }
        $this->CI->load->model("lockerloot_model");
        $this->CI->lockerloot_model->options = array("customer_id" => $this->test_customer->{Customer::$primary_key});
        $this->CI->lockerloot_model->delete();
        $this->lockerLoot = new LockerLoot();
        $this->lockerLoot->points = 100;
        $this->lockerLoot->customer_id = $this->test_customer->{Customer::$primary_key};
        $this->lockerLoot->save();        
        

        $lockerLootConfigs = LockerLootConfig::search_aprax(array("business_id" => $this->business_id));
        array_map(function ($lockerLootConfig) { 
            $lockerLootConfig->delete();}, $lockerLootConfigs);
            
        $this->lockerLootConfig = new LockerLootConfig();
        $this->lockerLootConfig->business_id = $this->business_id;
        $this->lockerLootConfig->percentPerOrder = rand(10,50);
        $this->lockerLootConfig->redeemPercent = rand(10,50);
        $this->lockerLootConfig->minRedeemAmount = rand(5, 50);
        $this->lockerLootConfig->referralAmountPerReward = rand(5,50);
        $this->lockerLootConfig->referralNumberOrders = rand(5,50);
        $this->lockerLootConfig->referralOrderAmount = rand(5,100);
        $this->lockerLootConfig->programName = UnitTest::get_random_word(1);
        $this->lockerLootConfig->lockerLootStatus = 'active';
        $this->lockerLootConfig->save();
        $this->login($this->test_customer->email, $this->test_customer->password);
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account/programs/rewards");
        }
        else {   
            $this->driver->get_element(sprintf("link text=%s Rewards", ucfirst($this->lockerLootConfig->programName)))->click();
        }
    }

    /**
     * The following test verifies that the functionality for a customer to accept the terms and condtions of locker loot. 
     */
    public function testAgreeToTermsAndConditions() {
        $this->test_customer->lockerLootTerms = 0;
        $this->test_customer->save();
        
        $this->driver->get_element("link text=Rewards Terms and Conditions")->click();
        $this->driver->get_element("name=submit")->click();
        $this->driver->get_element("css=div.success")->assert_text("Thank you for your participation the {$this->lockerLootConfig->programName} terms. Back to {$this->lockerLootConfig->programName}");
        $this->driver->assert_string_present("You are participating in this program.");
        $this->driver->get_element("link text=Back to {$this->lockerLootConfig->programName}");
    }    
    
    
    /**
     * The folowing test verifies the functionality for redeeming locker loot.
     */
    public function testRedeemLockerLoot()
    {
        $business = new \App\Libraries\DroplockerObjects\Business($this->business_id);
        setlocale(LC_ALL, $business->locale);

        $this->driver->get_element("link text=Redeem Credit")->click();
        $this->driver->assert_string_present(sprintf("A %s credit has been added to your account", money_format('%.2n', $this->lockerLoot->points)));

        $customerDiscounts = CustomerDiscount::search_aprax(array("customer_id" => $this->test_customer->{Customer::$primary_key}, "business_id" => $this->test_customer->business_id));
        $customerDiscount = $customerDiscounts[0];
        $this->assertEquals(1, $customerDiscount->combine, "The locker loot discount is not combinable");
        $this->assertEquals($customerDiscount->amount, $this->lockerLoot->points);
    }
    
    /**
     * The following test verfies that registering with a locker loot promo code works correctly. 
     */
    public function testRegisterWithPromoCode()
    {
        $this->driver->load($this->promo_link);
        $this->driver->get_element("name=promo_code")->assert_value($this->promo_code);
        $random_word = UnitTest::get_random_word();
        $email = "unittest_$random_word@example.com";
        $password = "webdriver";
        $this->driver->get_element("name=email")->send_keys($email);
        $this->driver->get_element("name=password")->send_keys($password);
        $this->driver->get_element("name=agree")->click();
        
        $this->driver->get_element("name=password")->submit();
        
        $coupons = \App\Libraries\DroplockerObjects\Coupon::search_aprax(array("prefix" => "CR", "code" => "*", "business_id" => $this->business_id));
        $coupon = $coupons[0];
        $this->driver->get_element("css=div.alert-success")->assert_text_contains("Promotional code '{$this->promo_code} - {$coupon->description}' was applied to your new account.");
    }
    /**
     * The following test verifies that the locker loot interface works as expected. 
     */
    public function testLockerLootDisplay()
    {
        $this->driver->get_element("id=email_link")->assert_value($this->promo_link);
        $this->driver->get_element("id=referral_code_link")->assert_value($this->promo_link);
        $this->driver->assert_string_present("Your Promotional Code Is: $this->promo_code");
        $this->driver->assert_string_present("{$this->programName} Balance");
        
        $this->CI->load->model("customer_model");
        $balance = $this->CI->customer_model->get_locker_loot_balance($this->test_customer->customerID);
        $this->driver->assert_string_present(number_format($balance, 2));
    }

    

    public function tearDown()
    {
        if ($this->lockerLoot instanceof \App\Libraries\DroplockerObjects\LockerLoot)
        {
            $this->lockerLoot->delete();
        }
        if ($this->lockerLootConfig instanceOf \App\Libraries\DroplockerObjects\LockerLootConfig)
        {
            $this->lockerLootConfig->delete();
        }
        parent::tearDown();
    }
}

?>
