<?php

class LocationsTest extends EmployeeUnitTest
{
    private $address;
    private $address2;
    private $city;
    private $zipcode;
    private $route_id;
    private $sortOrder;
    private $residentManager;
    private $highTarget;
    private $lowTarget;
    private $notes;
    private $commission;
    private $commissionFrequency;
    private $description;
    private $hours;
    private $accessCode;
    private $units;
    private $quarter;
    private $state;
    
    public function setUp() 
    {
        $this->companyName = "Adams Location";
        $this->address = UnitTest::get_random_word(3);
        $this->address2 = "Apt 305";
        $this->city = "San Francisco";
        $this->zipcode = "94122";
        $this->country = "usa";
        $this->route_id =2;
        $this->sortOrder = 2;
        $this->residentManager = "Adam Prax";
        $this->highTarget = 10.00;
        $this->lowTarget = 5.00;
        $this->notes = "Test Note";
        $this->commission = "test comission";
        $this->commissionFrequency = "test frequency";
        $this->description = "Test Description";
        $this->hours = "3";
        $this->accessCode = 137;
        $this->units = 150;
        $this->quarter = 1;
        $this->state = "CA";
        $this->masterRoute_id = 2;
        $this->masterSortOrder = 3;
        
        parent::setUp();
        
        $this->test_location = UnitTest::create_random_location($this->business_id);
        
        $this->login();
    }
    public function testCreateAndDeleteLocation() {
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=Add Location")->click();
        $this->driver->get_element("name=companyName")->send_keys($this->companyName);
        $this->driver->get_element("name=address")->send_keys($this->address);
        $this->driver->get_element("name=address2")->send_keys($this->address2);
        $this->driver->get_element("name=country")->select_value($this->country);
        $this->driver->get_element("name=city")->send_keys($this->city);
        $this->driver->get_element("name=state")->select_value($this->state);
        $this->driver->get_element("name=zipcode")->send_keys($this->zipcode);
        $this->driver->get_element("name=route_id")->send_keys($this->route_id);
        $this->driver->get_element("name=sortOrder")->send_keys($this->sortOrder);
        //$this->driver->get_element("name=locationType_id")->select_value(UnitTest::get_random_locationType()->{App\Libraries\DroplockerObjects\LocationType::$primary_key});
        //$this->driver->get_element("name=locationType_id")->select_random();
        $this->driver->get_element("name=residentManager")->send_keys($this->residentManager);
        $this->driver->get_element("name=highTarget")->send_keys($this->highTarget);
        $this->driver->get_element("name=lowTarget")->send_keys($this->lowTarget);
        $this->driver->get_element("name=notes")->send_keys($this->notes);
        $this->driver->get_element("name=commission")->send_keys($this->commission);
        $this->driver->get_element("name=commissionFrequency")->send_keys($this->commissionFrequency);
        $this->driver->get_element("name=description")->send_keys($this->description);
        $this->driver->get_element("name=hours")->send_keys($this->hours);
        $this->driver->get_element("name=accessCode")->send_keys($this->accessCode);
        $this->driver->get_element("name=units")->send_keys($this->units);
        $this->driver->get_element("name=masterRoute_id")->send_keys($this->masterRoute_id);
        $this->driver->get_element("name=masterSortOrder")->send_keys($this->masterSortOrder);
        $this->driver->get_element("name=quarter")->send_keys($this->quarter);

        $this->driver->get_element("name=quarter")->submit();

        $this->driver->get_element("css=.message")->assert_text("Created location '{$this->address}, {$this->city}'");

        $this->CI->load->model("location_model");
        
        $locations = $this->CI->location_model->get_aprax(array("address" => $this->address));
        $this->assertNotEmpty($locations, "Could not find the new location by the specified address '{$this->address}'");
        $location = $locations[0];
        $this->assertEquals($this->address, $location->address);
        $this->assertEquals($this->address2, $location->address2);
        $this->assertEquals($this->city, $location->city);
        $this->assertEquals($this->country, $location->country);
        $this->assertEquals($this->state, $location->state);
        $this->assertEquals($this->zipcode, $location->zipcode);
        $this->assertEquals($this->route_id, $location->route_id);
        $this->assertEquals($this->sortOrder, $location->sortOrder);
        $this->assertEquals($this->residentManager, $location->residentManager);
        $this->assertEquals($this->highTarget, $location->highTarget);
        $this->assertEquals($this->lowTarget, $location->lowTarget);
        $this->assertEquals($this->notes, $location->notes);
        $this->assertEquals($this->commission, $location->commission, "The commission is not the expected value.");
        $this->assertEquals($this->commissionFrequency, $location->commissionFrequency);
        $this->assertEquals($this->description, $location->description);
        $this->assertEquals($this->hours, $location->hours);
        $this->assertEquals($this->accessCode, $location->accessCode, "The access code is not the expected value.");
        $this->assertEquals($this->units, $location->units, "The units is not the expected value.");
        $this->assertEquals($this->quarter, $location->quarter, "The quarter is not the expected value.");

        $lockers = \App\Libraries\DroplockerObjects\Locker::search_aprax(array('location_id'=>$location->locationID));
        foreach ($lockers as $locker) {
            $locker->delete();
        }

        $this->driver->get_element("link text=View Locations")->click();
        $this->driver->get_element("delete-" . $location->locationID)->click(); //This marks the location as deleted.
        $this->driver->accept_alert();
        sleep(20);
        $this->driver->get_element("css=div.success")->assert_text("Location deleted");


        $deleted_location = new App\Libraries\DroplockerObjects\Location($location->locationID);
        $this->assertEquals("deleted", $deleted_location->status);
    }     
    
    public function testUpdateLocation() {
        $this->driver->get_element("link text=ADMIN")->click();
        $this->driver->get_element("link text=View Locations")->click();

        $this->driver->get_element("css=.edit")->click();
        $this->driver->get_element("name=companyName")->clear();
        $this->driver->get_element("name=companyName")->send_keys($this->companyName);

        $this->driver->get_element("name=address")->clear();
        $this->driver->get_element("name=address")->send_keys($this->address);

        $this->driver->get_element("name=address2")->clear();
        $this->driver->get_element("name=address2")->send_keys($this->address2);

        $this->driver->get_element("name=country")->select_value($this->country);
        
        $this->driver->get_element("name=city")->clear();
        $this->driver->get_element("name=city")->send_keys($this->city);

        $this->driver->get_element("name=state")->select_value($this->state);

        $this->driver->get_element("name=zipcode")->clear();
        $this->driver->get_element("name=zipcode")->send_keys($this->zipcode);

        $this->driver->get_element("name=route_id")->clear();
        $this->driver->get_element("name=route_id")->send_keys($this->route_id);

        $this->driver->get_element("name=sortOrder")->clear();
        $this->driver->get_element("name=sortOrder")->send_keys($this->sortOrder);

        $this->driver->get_element("name=locationType_id")->select_value(UnitTest::get_random_locationType()->{App\Libraries\DroplockerObjects\LocationType::$primary_key});

        $this->driver->get_element("name=residentManager")->clear();
        $this->driver->get_element("name=residentManager")->send_keys($this->residentManager);

        $this->driver->get_element("name=highTarget")->clear();
        $this->driver->get_element("name=highTarget")->send_keys($this->highTarget);

        $this->driver->get_element("name=lowTarget")->clear();
        $this->driver->get_element("name=lowTarget")->send_keys($this->lowTarget);

        $this->driver->get_element("name=notes")->clear();
        $this->driver->get_element("name=notes")->send_keys($this->notes);

        $this->driver->get_element("name=commission")->clear();
        $this->driver->get_element("name=commission")->send_keys($this->commission);

        $this->driver->get_element("name=commissionFrequency")->clear();
        $this->driver->get_element("name=commissionFrequency")->send_keys($this->commissionFrequency);

        $this->driver->get_element("name=description")->clear();
        $this->driver->get_element("name=description")->send_keys($this->description);

        $this->driver->get_element("name=hours")->clear();
        $this->driver->get_element("name=hours")->send_keys($this->hours);

        $this->driver->get_element("name=accessCode")->clear();
        $this->driver->get_element("name=accessCode")->send_keys($this->accessCode);

        $this->driver->get_element("name=units")->clear();
        $this->driver->get_element("name=units")->send_keys($this->units);

        $this->driver->get_element("name=masterRoute_id")->clear();
        $this->driver->get_element("name=masterRoute_id")->send_keys($this->masterRoute_id);

        $this->driver->get_element("name=masterSortOrder")->clear();
        $this->driver->get_element("name=masterSortOrder")->send_keys($this->masterSortOrder);

        $this->driver->get_element("name=quarter")->clear();
        $this->driver->get_element("name=quarter")->send_keys($this->quarter);

        $url = explode("/", $this->driver->get_url());
        $locationID = $url[count($url)-1];
        
        $this->driver->get_element("name=quarter")->submit();

        $this->driver->get_element("css=.message")->assert_text("Updated location '{$this->address}, {$this->city}'");

        $location = new \App\Libraries\DroplockerObjects\Location($locationID, false);
        $this->assertEquals($this->address, $location->address, "The location address is not the expected value");
        $this->assertEquals($this->address2, $location->address2);
        $this->assertEquals($this->city, $location->city);
        $this->assertEquals($this->country, $location->country);
        $this->assertEquals($this->state, $location->state);
        $this->assertEquals($this->zipcode, $location->zipcode);
        $this->assertEquals($this->route_id, $location->route_id);
        $this->assertEquals($this->sortOrder, $location->sortOrder);
        $this->assertEquals($this->residentManager, $location->residentManager);
        $this->assertEquals($this->highTarget, $location->highTarget);
        $this->assertEquals($this->lowTarget, $location->lowTarget);
        $this->assertEquals($this->notes, $location->notes);
        $this->assertEquals($this->commission, $location->commission, "The commission is not the expected value.");
        $this->assertEquals($this->commissionFrequency, $location->commissionFrequency);
        $this->assertEquals($this->description, $location->description);
        $this->assertEquals($this->hours, $location->hours);
        $this->assertEquals($this->accessCode, $location->accessCode, "The access code is not the expected value.");
        $this->assertEquals($this->units, $location->units, "The units is not the expected value.");
        $this->assertEquals($this->quarter, $location->quarter, "The quarter is not the expected value.");

        $location->save(); //This statement restores the original properties of the location.
    }    
    
    public function tearDown() {
        parent::tearDown();
        if ($this->test_location instanceof \App\Libraries\DroplockerObjects\Location) {
            $this->test_location->delete();
        }
    }
}

?>
