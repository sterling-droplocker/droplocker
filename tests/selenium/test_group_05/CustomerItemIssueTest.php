<?php
use \App\Libraries\DroplockerObjects\Item;
use \App\Libraries\DroplockerObjects\Customer;
use \App\Libraries\DroplockerObjects\ItemIssue;
use \App\Libraries\DroplockerObjects\Order;
class CustomerItemIssueTest extends CustomerUnitTest
{
    private $item;
    private $order;
    private $orderItem;
    private $barcode;
    public function setUp()
    {
        parent::setUp();
        $this->order = UnitTest::create_random_order($this->business_id, $this->test_customer->{Customer::$primary_key});
        $this->orderItem = UnitTest::create_random_orderItem($this->order->orderID);
        
        $this->item = UnitTest::create_random_item($this->order->customer_id, $this->business_id);
        $this->barcode = UnitTest::create_random_barcode($this->business_id);
        $this->barcode->item_id = $this->item->itemID;
        $this->barcode->save();
        
        $this->item = new Item($this->item->itemID, true);
        
        $this->orderItem->item_id = $this->item->{Item::$primary_key};
        $this->orderItem->save();
    }
    /**
     * The following test verifies the fucntionality for adding, modifying, and deleting an item issue through the cusotmer interface. 
     */
    public function testAddModifyAndDeleteIssueAsCustomer()
    {
        $this->login();
        
        if ($this->browser == "chrome")
        {
            $this->driver->load("http://laundrylocker.droplocker.{$this->domain_extension}/account/profile/closet");
        }
        else
        {
            $this->driver->get_element("link text=My Closet")->click();
        }
        $this->driver->get_element("link text={$this->item->relationships['Barcode'][0]->barcode}")->click();
        $this->driver->get_element("link text=Add Notes")->click();
        
        $this->driver->assert_string_present($this->item->relationships['Barcode'][0]->barcode);
        
        $positions = array("front", "back");
        $this->driver->get_element("issue_image")->click();
        $front_or_back = "front";
        $issue_type = "Repair";
        $issueTypes = $this->driver->get_all_elements("name=issueType");
        
        //Note, due to dysnfucntional implementation of item note pop up, there each form element that appears in the DOM.
        $found = false;
        //The following statement select the issue type option.
        foreach ($issueTypes as $issueType) {
            if ($issueType->get_value() == $issue_type && $issueType->is_visible()) {
                $issueType->click();
                $found = true;
                break;
            }
        }
        $this->assertTrue($found, "Could not find issue type radio button");
        $item_issue_notes = UnitTest::get_random_word(5);
        
        //The following statements verify the functionality for creating a new issue.
        $found = false;
        $notes = $this->driver->get_all_elements("name=note");
        foreach ($notes as $note) {
            if ($note->is_visible()) {
                $note->send_keys($item_issue_notes);
                $found = true;
                break;
            }
        }
        $this->assertTrue($found, "Could not find 'note' text area");
        $found_submit_button = false;
        $note_submits = $this->driver->get_all_elements("note_submit");
        foreach ($note_submits as $note_submit) {
            if ($note_submit->is_visible()) {
                $note_submit->click();
                $found_submit_button = true;
            }
        }
        $this->assertTrue($found_submit_button, "Could not find note submit button");
        
        $this->driver->assert_string_present($item_issue_notes);

        $this->CI->load->model("itemissue_model");
        $itemIssues = $this->CI->itemissue_model->get_aprax(array("status" => "Opened", "item_id" => $this->item->itemID, "frontBack" => $front_or_back, "note" => $item_issue_notes));
        $this->assertEquals(1, count($itemIssues), "Did not find exactly 1 item issue created.");
        $itemIssue = $itemIssues[0];
        
        //The following statements verify the funcitonality for modifying an existing item issue.
        $this->driver->get_element("modify-{$itemIssue->itemIssueID}")->click();
        
        $updated_item_notes = "Test Updated Item Notes";
        $notes = $this->driver->get_all_elements("name=note");
        foreach ($notes as $note) {
            if ($note->is_visible()) {
                //The following statement verifies that the current item issue notes display as expected.
                $note->assert_value($item_issue_notes);        
                //THe following statements verify the functionality for updating an existing item.
                $note->clear();
                $note->send_keys($updated_item_notes);
            }
        }
        //The following statements virify that the selected issueType displays as expected.
        $issueTypes = $this->driver->get_all_elements("name=issueType");
        foreach ($issueTypes as $issueType) {
            if ($issueType->get_value() == $issue_type && $issueType->is_visible()) {
                $issueType->assert_selected();
                break;
            }
        }
        
        //The following statements verify that the updatd note displays as expected.
        $notes = $this->driver->get_all_elements("name=note");
        foreach ($notes as $note) {
            if ($note->is_visible()) {
                $note->assert_value($updated_item_notes);
            }
        }
        
        $this->driver->get_element("link text=Hide Window")->click();
        
        //The following statements verify the functinonality for deleting an existing item issue.
        if ($this->browser == "chrome") {
            $this->driver->load("{$this->base_url}/account/profile/delete_note/{$this->item->itemID}/{$itemIssue->itemIssueID}");
        }
        else {
            $this->driver->get_element("delete-{$itemIssue->itemIssueID}")->click();
        }
        
        $this->driver->assert_string_not_present($updated_item_notes);
        $deleted_itemIssues = ItemIssue::search_aprax(array("status" => "Opened", "item_id" => $this->item->{Item::$primary_key}, "frontBack" => $front_or_back, "note" => $item_issue_notes));
        $this->assertEmpty($deleted_itemIssues, "Expected item issue to be deleted, but item issue was still found in database.");          
    }
    public function tearDown()
    {
        $itemIssues = ItemIssue::search_aprax(array("item_id" => $this->item->itemID));
        foreach ($itemIssues as $itemIssue)
        {
            $itemIssue->delete();
        }
        parent::tearDown();
    }
}

?>
