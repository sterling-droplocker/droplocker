<?php

use App\Libraries\DroplockerObjects\Item;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
class LockerTest extends UnitTest {
    private $locker;
    
    public function setUp() {
        parent::setUp();
        $this->locker = UnitTest::create_random_locker($this->business_id);
    }
    
    public function testSaveAndDelete()  {
        $locker = new Locker();
        $location = UnitTest::get_random_object("Location");
        $lockerStatus = UnitTest::get_random_object("LockerStatus");
        $lockerStyle = UnitTest::get_random_object("LockerStyle");
        $lockerLockType = UnitTest::get_random_object("LockerLockType");
        
        $locker->location_id = $location->{$location::$primary_key};
        
        $locker->lockerStatus_id = $lockerStatus->{$lockerStatus::$primary_key};
        $locker->lockerStyle_id = $lockerStyle->{$lockerStyle::$primary_key};
        
        $locker->lockerName = UnitTest::get_random_word();
        $duplicate_lockers = Locker::search_aprax(array("lockerName" => $locker->lockerName));
        foreach ($duplicate_lockers as $duplicate_locker) {
            $duplicate_locker->delete();
        }
        $locker->lockerLockType_id = $lockerLockType->{$lockerLockType::$primary_key};
        $locker->save();
        
        $new_locker = new Locker($locker->{$locker::$primary_key}, false);
        
        $this->assertEquals($locker->location_id, $new_locker->location_id, 'The location ID of the new locker is not the expected value');
        $this->assertEquals($locker->lockerName, $new_locker->lockerName, 'The locker name of the new locker does not match the original locker name');
        $this->assertEquals($locker->lockerStatus_id, $new_locker->lockerStatus_id, 'The locker status ID of the new locker does not match the original locker status ID');
        $this->assertEquals($locker->lockerStyle_id, $new_locker->lockerStyle_id, 'The locker style ID of the new locker does not match the original locker style ID');
        $this->assertEquals($locker->lockerLockType_id, $new_locker->lockerLockType_id, 'The locker lock type of the new locker does not match the original locker lock type ID');
        
        $new_locker->delete();
        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        new Locker($new_locker->lockerID);
    }
    public function testIsMultiOrder() {
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_BOOL, $this->locker->is_multi_order());
    }
    /**
     * The following test verifies the Location hasone relationship functionality.
     */
    public function testHasOneLocation() {
        $location_with_relationships = UnitTest::create_random_location($this->business_id, false);
        $location_without_relationships = new Location($location_with_relationships->{Location::$primary_key}, false);
        $locker = UnitTest::create_random_locker($location_with_relationships->{Location::$primary_key});
        $this->assertEquals($location_without_relationships, $locker->relationships['Location'][0], 'locker object is not included in the location objects relationships');
    }
    public function testHasOneLockerStatus()  {
        $lockerStatus = new \App\Libraries\DroplockerObjects\LockerStatus($this->locker->lockerStatus_id, false);
        $this->assertEquals($lockerStatus, $this->locker->relationships['LockerStatus'][0]);
    }
    public function testHasLockerStyle() {
        $lockerStyle = new App\Libraries\DroplockerObjects\LockerStyle($this->locker->lockerStyle_id, false);
        $this->assertEquals($lockerStyle, $this->locker->relationships['LockerStyle'][0]);
    }
    public function testLockerLockType() {
        $lockerLockType = new \App\Libraries\DroplockerObjects\LockerLockType($this->locker->lockerLockType_id, false);
        $this->assertEquals($lockerLockType, $this->locker->relationships['LockerLockType'][0]);
    }
}

?>
