<?php
use App\Libraries\DroplockerObjects\TicketSubscribers;
class TicketSubscribersTest extends UnitTest
{
    private $emails;
    private $subject;
    protected $business_id;
    
    function __construct(){
        parent::__construct();
        $this->business_id = 3;
        $this->subject = 'Urgent Tickets';
        $this->emails = array('support@laundrylocker.com', 'another@asdasdasd.com');
    }

    /**
     * tests error when passing an empty subject
     */
    function testMissingSubject(){
        $result = TicketSubscribers::updateSubscribers($this->emails, '', $this->business_id);
        $this->assertEquals('Missing Subject', $result['message'], 'The empty subject was not caught');
    }
    
    /**
     * tests error when passing an invalid email address
     */
    function testInvalidEmail(){
        $this->emails[] = 'asdasdad#aasddads.com';
        $result = TicketSubscribers::updateSubscribers($this->emails, $this->subject, $this->business_id);
        $this->assertEquals('Invalid Email: asdasdad#aasddads.com', $result['message'], 'The empty subject was not caught');
    }
    
    /**
     * tests a successful update to the ticketSubscribers table
     */
    function testSuccess(){
        $this->markTestSkipped("This test does not properly construct it's test data");
        $sub = TicketSubscribers::search_aprax(array('business_id'=>$this->business_id));
        $sub = $sub[0];
        $sub->subject = "adsasdas";
        $sub->save();
        
        $result = TicketSubscribers::updateSubscribers($this->emails, $this->subject, $this->business_id);
        $this->assertEquals(1, $result, 'The subscribers were not updated');
        
    }
    
}