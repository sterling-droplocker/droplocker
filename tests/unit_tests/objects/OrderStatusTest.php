<?php
use App\Libraries\DroplockerObjects\OrderStatus;
use App\Libraries\DroplockerObjects\Employee;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderStatusTest
 *
 * @author ariklevy
 */
class OrderStatusTest extends UnitTest
{
    private $random_orderStatus;
    private $random_order;
    protected $business_id;
    
    public function setUp() 
    {
        parent::setUp();
        
        $this->business_id = 3;
        
        $this->order = self::create_random_order();
        
        $this->random_orderStatus = self::create_random_orderStatus($this->order->orderID);
        $this->random_orderStatus = new OrderStatus($this->random_orderStatus->orderStatusID, true);
    }
    /**
     * The following test verifies the functionality for creating, saving, and deleting an OrderStatus Droplocker Object. 
     */
    public function testCreateSaveAndDelete()
    {
        $orderStatus = new OrderStatus();
        $random_order = $this->order;
        
        if(empty($random_order->orderID)){
            throw new \Exception('Order not found: SQL:\n'.$this->CI->db->last_query());
        }
        
        $orderStatus->order_id =$random_order->orderID;
        $random_employee = UnitTest::get_random_employee($this->business_id);
        $orderStatus->employee_id = $random_employee->employeeID;
        $random_orderStatusOption = UnitTest::get_random_orderStatusOption();
        $orderStatus->orderStatusOption_id = $random_orderStatusOption->orderStatusOptionID;
        $orderStatus->employee_id = $random_employee->{Employee::$primary_key};
        $orderStatus->save();
        
        $saved_orderStatus = new OrderStatus($orderStatus->orderStatusID);
        
        $this->assertEquals($orderStatus->order_id, $saved_orderStatus->order_id);
        $this->assertEquals($orderStatus->employee_id, $saved_orderStatus->employee_id);
        $this->assertEquals($orderStatus->orderStatusOption_id, $saved_orderStatus->orderStatusOption_id);
        
        $orderStatus->delete();
        
        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        new OrderStatus($orderStatus->orderStatusID);
    }

    /**
     * The following test verifies the the functionality of the Has-One relationship from OrderStatus to Order. 
     */
    public function testHasOneOrder()
    {
        $order = new \App\Libraries\DroplockerObjects\Order($this->random_orderStatus->order_id, false);
        $this->assertEquals($order, $this->random_orderStatus->relationships['Order'][0]);
    }
    /**
     * The following test verifies the fucntionality of the Has-One Order Status relationship from OrderStatus to OrderStatusOption. 
     */
    public function testHasOneOrderStatusOption()
    {
        $orderStatusOption = new App\Libraries\DroplockerObjects\OrderStatusOption($this->random_orderStatus->orderStatusOption_id, false);
        $this->assertEquals($orderStatusOption, $this->random_orderStatus->relationships['OrderStatusOption'][0]);
    }
    /**
     * The following test verifies the functioanlity 
     */
    public function testHasOneLocker()
    {
        $locker = new App\Libraries\DroplockerObjects\Locker($this->random_orderStatus->locker_id, false);
        $this->assertEquals($locker, $this->random_orderStatus->relationships['Locker'][0]);
    }
    /**
     * The following test verfies the Has-One relationship between the OrderStatus Droplocker Object and the Employee Droplocker Object. 
     */
    public function testHasOneEmployee()
    {
        $employee = new \App\Libraries\DroplockerObjects\Employee($this->random_orderStatus->employee_id, false);
        $this->assertEquals($employee, $this->random_orderStatus->relationships['Employee'][0], "
            
                The employee in the has one relatioship is not the expected employee for the order status.
            
        ");
    }
    public function tearDown()
    {
        if ($this->random_orderStatus instanceOf \App\Libraries\DroplockerObjects\OrderStatus)
        {
            $this->random_orderStatus->delete();
        }
        if ($this->random_order instanceof \App\Libraries\DroplockerObjects\Order)
        {
            $this->random_order->delete();
        }
        parent::tearDown();
        
    }
}

?>
