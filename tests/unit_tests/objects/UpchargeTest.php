<?php
use App\Libraries\DroplockerObjects\Transaction;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;

class UpchargeTest extends UnitTest
{
    private $upchargeGroupID = null;
    private $upchargeID = null;
    
    function __construct(){
        parent::__construct();
    }
    
    function setUp(){
        parent::setUp();
    }
    
    /**
     * tests adding an upchargeGroup using the upchargeGroup object
     */
    function testAddUpchargeGroup(){
        
        $this->createGroup($this->business_id);
        
        //tests that the upcharge group was added
        $this->assertTrue(is_numeric($this->upchargeGroupID), "Inserting upcharge Group FAILED");
        
    }
    
    function testAddUpcharge(){
        $upchargeName = $this->get_random_word(1);
        $supplier = $this->get_random_supplier();
        $this->createGroup($this->business_id);
        
        $upchargeObj = new \App\Libraries\DroplockerObjects\Upcharge();
        $this->upchargeID[] = $upchargeObj->addUpcharge($this->upchargeGroupID, $upchargeName, 2, $supplier->supplierID, 1);
        
        // test that the upcharge was created
        $this->assertTrue(is_numeric($this->upchargeID[0]), "Inserting upcharge FAILED");
        
        // test the values of the upcharge to make sure it belongs to the group
        $upcharge = new \App\Libraries\DroplockerObjects\Upcharge($this->upchargeID[0]);
        $this->assertEquals($this->upchargeGroupID, $upcharge->upchargeGroup_id, 'Upcharge has the wrong group');
        
        // Test that the upcharge has the proper supplier
        $upchargeSupplier = \App\Libraries\DroplockerObjects\UpchargeSupplier::search_aprax(array('upcharge_id'=>$this->upchargeID[0]));
        $this->assertCount(1, $upchargeSupplier, "More than one entry in the upcharge Supplier table");
        $this->assertEquals($supplier->supplierID, $upchargeSupplier[0]->supplier_id, 'Upcharge has the wrong supplier');
        
        // Test the getGroupUpcharges now that we have a group with an upcharge
        $upchargeGroup = new \App\Libraries\DroplockerObjects\UpchargeGroup($this->upchargeGroupID);
        $upcharges = $upchargeGroup->getGroupUpcharges();
        // there should only be one upcharge
        $this->assertCount(1, $upcharges, "More than one entry in the upcharges table");
        $this->assertEquals($upchargeName, $upcharges[0]->name, 'Upcharge has the wrong name');
        
    }
    
    /**
     * tests getting the default supplier for an upcharge
     */
    function testGetDefaultSupplier(){
        
        $upchargeName = $this->get_random_word(1);
        $supplier = $this->get_random_supplier();
        $this->createGroup($this->business_id);
        $upchargeObj = new \App\Libraries\DroplockerObjects\Upcharge();
        $this->upchargeID[] = $upchargeObj->addUpcharge($this->upchargeGroupID, $upchargeName, 2, $supplier->supplierID, 1);
        
        $upchargeObj = new \App\Libraries\DroplockerObjects\Upcharge($this->upchargeID[0]);
        $defaultSupplier = $upchargeObj->getDefaultSupplier();
        
        $this->assertEquals($supplier->supplierID, $defaultSupplier->supplier_id, "failed to get default supplier");
    }
    
    function teardown(){
        parent::tearDown();
        $upchargeGroup = new \App\Libraries\DroplockerObjects\UpchargeGroup($this->upchargeGroupID);
        if($upchargeGroup) $upchargeGroup->delete();
        
        if($this->upchargeID){
            foreach($this->upchargeID as $id){
                $upcharge = new \App\Libraries\DroplockerObjects\Upcharge($id);
                $upcharge->delete();
            }
        }
       
    }
    
    function createGroup($business_id){
        $groupName = $this->get_random_word(1);
        $upchargeGroup = new \App\Libraries\DroplockerObjects\UpchargeGroup();
        $this->upchargeGroupID = $upchargeGroup->addGroup($groupName, $business_id);
    }
    
}