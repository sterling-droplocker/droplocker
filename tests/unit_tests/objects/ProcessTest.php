<?php
use App\Libraries\DroplockerObjects\Process;

class ProcessTest extends UnitTest
{
    public function testCreateAndDelete()
    {
        $process = new Process();
        $process->name = "Adam Prax";
        $process->slug = "aprax";
        $process->save();
        $new_process = new Process($process->{$process::$primary_key}, false);
        $this->assertEquals($new_process->properties, $process->properties);
        
        $process->delete();
        
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new Process($process->{$process::$primary_key}, false);
    }
}

?>
