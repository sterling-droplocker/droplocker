<?php
use App\Libraries\DroplockerObjects\Transaction;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;

class TransactionTest extends UnitTest
{
    private $transaction;
    
    public function setUp() 
    {
        parent::setUp();
        $this->transaction = new Transaction();
        $this->transaction->type = "capture";
        
        $order = UnitTest::create_random_order($this->business_id);
        
        $this->transaction->customer_id = $order->customer_id;
        $this->transaction->resultCode = 0;
        $this->transaction->message = "Approved";
        $this->transaction->order_id = $order->{Order::$primary_key};
        $this->transaction->zipMatch = "Match";
        $this->transaction->cscMatch = "Service Not Requested";
        $this->transaction->business_id = $this->business_id;
        $this->transaction->processor = "verisign";
        $this->transaction->amount = "5.00";
        $this->transaction->pnref = md5(time());
        $this->transaction->resultCode = 0;

        $this->transaction->save();
        
        $this->transaction = new Transaction($this->transaction->{Transaction::$primary_key}, true);
    }
    /**
     * The following test verifies the hasOne relationship with Customer.
     */
    public function testHasOneCustomer()
    {   
        $customer = new App\Libraries\DroplockerObjects\Customer($this->transaction->customer_id, false);
        $this->assertEquals($customer, $this->transaction->relationships['Customer'][0]);
    }
    /**
     * The following test verifies the hasOne relationship with Business. 
     */
    public function testHasOneBusiness()
    {
        $business = new App\Libraries\DroplockerObjects\Business($this->transaction->business_id, false);
        $this->assertEquals($business, $this->transaction->relationships['Business'][0]);
    }
    /**
     * The following test verifies the hasOne relationship with Order. 
     */
    public function testHasOneOrder()
    {
        $order = new App\Libraries\DroplockerObjects\Order($this->transaction->order_id, false);
        $this->assertEquals($order, $this->transaction->relationships['Order'][0]);
    }
    
    public function tearDown() 
    {
        $this->transaction->delete();
        parent::tearDown();
    }
}

?>
