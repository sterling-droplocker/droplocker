<?php

use App\Libraries\DroplockerObjects\EmailLog;

/**
 * The following test suite verifies the funcitonality for the Email Log Droplocker Object.
 */
class EmailLogTest extends UnitTest {
    /**
     * The following test verifies the functionality for creating and deleting an Email Log Droplocker object.
     */
    public function testCreateAndDelete() {
        $emailLog = new EmailLog();
        $emailLog->subject = self::get_random_word(3);
        $emailLog->body = self::get_random_word(10);
        $word = self::get_random_word();
        $emailLog->to = "testing_$word@example.com";
        $business = self::get_random_object("business", false);
        $emailLog->business_id = $business->business_id;
        $emailLog->save();
        
        $saved_emailLog = new EmailLog($emailLog->{EmailLog::$primary_key});
        $this->assertEquals($emailLog->subject, $saved_emailLog->subject);
        $this->assertEquals($emailLog->body, $saved_emailLog->body);
        $this->assertEquals($emailLog->to, $saved_emailLog->to);
        $this->assertEquals($emailLog->business_id, $saved_emailLog->business_id);
        
        $saved_emailLog->delete();
        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        
        new EmailLog($saved_emailLog->emailLogID, false);
    }
}

?>
