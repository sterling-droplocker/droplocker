<?php
use App\Libraries\DroplockerObjects\Product;

class ProductObjectTest extends UnitTest 
{
    public function setUp()
    {
        parent::setUp();
        
        $this->product = UnitTest::create_random_product($this->business_id);
    }
    public function testCreateAndDelete() {
        $productCategory = UnitTest::get_random_object("ProductCategory", false);
        $business = UnitTest::get_random_object("Business", false);
        $product = new Product();
        $product->name = "Test name";
        $product->displayName = "Test Display Name";
        $product->productCategory_id = $productCategory->{$productCategory::$primary_key};
        $product->business_id = $business->businessID;
        $product->save();
        
        $new_product = new Product($product->{$product::$primary_key}, false);
        $this->assertEquals($product->name, $new_product->name);
        $this->assertEquals($product->displayName, $new_product->displayName);
        $this->assertEquals($product->productCategory_id, $new_product->productCategory_id);
        $this->assertEquals($product->business_id, $new_product->business_id);
    }
    public function testGetName()
    {
        $random_product_process = $this->get_random_product_process();
        $name = Product::getName($random_product_process->product_processID);
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_ARRAY, $name);
    }
    public function testHasOneProductCategory()
    {
        $productCategory = new \App\Libraries\DroplockerObjects\ProductCategory($this->product->productCategory_id, false);
        $this->assertEquals($productCategory, $this->product->relationships['ProductCategory'][0]);
    }
    public function testHasOneBusiness()
    {
        $business = new App\Libraries\DroplockerObjects\Business($this->product->business_id, false);
        $this->assertEquals($business, $this->product->relationships['Business'][0]);
    }
}

?>
