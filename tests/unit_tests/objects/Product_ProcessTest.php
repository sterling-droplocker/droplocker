<?php
use App\Libraries\DroplockerObjects\Product_Process;
use App\Libraries\DroplockerObjects\Product;
use App\Libraries\DroplockerObjects\Process;

class Product_ProcessTest extends UnitTest
{
    private $random_product_process;
    public function setUp()
    {
        parent::setUp();
        $this->random_product_process = UnitTest::create_random_product_process();
    }
    
    /**
     * The following test verifies the defined has_one to Process
     */
    public function testHasOneProcess()
    {
        $process = new Process($this->random_product_process->process_id, false);
        if ($this->random_product_process->process_id == 0)
        {
            $this->assertArrayNotHasKey("Process", $this->random_product_process->relationships);
            
        }
        else
        {
            $this->assertEquals($process, $this->random_product_process->relationships['Process'][0], "The product process does not match the expected value.");
        }
    }
    
    
    public function testCreateSaveAndDelete()
    {        
        $product = UnitTest::get_random_object("Product", false);
        $process = UnitTest::get_random_object("Process", false);
        
        $product_processes = Product_Process::search_aprax(array("product_id" => $product->{Product::$primary_key}, "process_id" => $process->{Process::$primary_key}), false);
        if (!empty($product_processes))
        {
            foreach ($product_processes as $product_process)
            {
                $product_process->delete();
            }
        }
        $product_process = new Product_Process();        
        $product_process->product_id = $product->{Product::$primary_key};
        $product_process->process_id = $process->{Process::$primary_key};
        $product_process->price = "5.99";
        $product_process->taxable = "1";
        $product_process->active = 1;
        $product_process->save();
        
        $new_product_process = new Product_Process($product_process->product_processID, false);
        
        if (!empty($new_product_process->dateCreated))
        {
            $this->assertInstanceOf("DateTime", $new_product_process->dateCreated);
        }
        
        $this->assertEquals($product_process->properties, $new_product_process->properties);
        
        $product_process->delete();
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new Product_Process($product_process->{$product_process::$primary_key});
    }
    
    
    
    public function testHasOneProduct()
    {
        $product = new Product($this->random_product_process->product_id, false);
        $this->assertEquals($product, $this->random_product_process->relationships['Product'][0]);
    }
    
    
    public function tearDown()
    {
        if ($this->random_product_process instanceof \App\Libraries\DroplockerObjects\Product_Process)
        {
            $this->random_product_process->delete();
        }
    }

}

?>
