<?php
use App\Libraries\DroplockerObjects\Transaction;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;

class SupplierTest extends UnitTest
{
    
    function __construct(){
        parent::__construct();
    }
    
    function setUp(){
        parent::setUp();
    }
    
    /**
     * tests the exception that is thrown when no business_id is passed to getBusinessSuppliers()
     */
    function testExceptionWhenGettingBusinessSuppliers(){
        
        try{
            \App\Libraries\DroplockerObjects\Supplier::getBusinessSuppliers();
        } catch(Exception $e){
            $this->assertEquals("Business_id was not passed and business_id in the session is not set", $e->getMessage(), 'Exception is was not thrown properly');
        } 
        
    }
    
    /**
     * test the validation for supplier
     */
    function testValidation(){
        
        try{
            $supplier = new \App\Libraries\DroplockerObjects\Supplier();
            $supplier->business_id = "a";
            $supplier->save();
        } catch(Exception $e){
            $this->assertEquals("'business_id' must be numeric.", $e->getMessage(), 'Exception is was not thrown properly');
        }
        
        try{
            $supplier = new \App\Libraries\DroplockerObjects\Supplier();
            $supplier->business_id = 2; // 2 does not exists
            $supplier->save();
        } catch(Exception $e){
            $this->assertEquals("Business ID '2' not found in database.", $e->getMessage(), 'Exception is was not thrown properly');
        }
        
        try{
            $supplier = new \App\Libraries\DroplockerObjects\Supplier();
            $supplier->business_id = 3; // 2 does not exists
            $supplier->supplierBusiness_id = '';
            $supplier->save();
        } catch(Exception $e){
            $this->assertEquals("'supplierBusiness_id' must be numeric.", $e->getMessage(), 'Exception is was not thrown properly');
        }
    
    }
    
}