<?php
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Customer;
/**
 * Description of claimTest
 *
 * @author ariklevy
 */
class ClaimObjectTest extends UnitTest
{
    /**
     * The following test verifies the functionality for saving changes to a claim object's properties to the database. 
     */
    public function testSaveAndDelete()
    {
        $claim = new Claim();
        $location = UnitTest::create_random_location($this->business_id);
        $locker = UnitTest::create_random_locker($location->{Location::$primary_key});
        $customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        $claim->locker_id = $locker->{Locker::$primary_key};
        $claim->business_id = $this->business_id;
        $claim->customer_id = $customer->{Customer::$primary_key};
        $claim->save();
        $new_claim = new Claim($claim->claimID);
        
        $this->assertEquals($new_claim->locker_id, $claim->locker_id);
        $this->assertEquals($new_claim->business_id, $claim->business_id);
        $this->assertEquals($new_claim->customer_id, $claim->customer_id);
        
        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        $new_claim->delete();
        new Claim($new_claim->{Claim::$primary_key});
    }

}

?>
