<?php
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Locker;

class LocationObjectTest extends UnitTest
{
    public function setUp()
    {
        parent::setUp(); 
    }
    
    public function testCreateAndDelete()
    {
        $city = "Test City";
        $address = "Test Address";
        
        $locations = Location::search_aprax(array("city" => $city, "address" => $address));
        foreach ($locations as $location)
        {
            $location->delete();
        }
        $location = new Location();
        
        $locationType = UnitTest::get_random_object("LocationType", false);
        $business = UnitTest::get_random_object("Business", false);
        

        $location->address = $address;
        $location->city = $city;
        $location->state = "California";
        $location->serviceType = "on demand";
        $location->locationType_id = $locationType->{$locationType::$primary_key};
        $location->business_id = $business->businessID;
        $location->save();
        
        $new_location = new Location($location->{$location::$primary_key}, false);
        $this->assertEquals($location->city, $new_location->city);
        $this->assertEquals($location->address, $new_location->address);
        $this->assertEquals($location->state, $new_location->state);
        $this->assertEquals($location->serviceType, $new_location->serviceType);
        $this->assertEquals($location->business_id, $new_location->business_id);
        
        $location->delete();
        
        $this->setExpectedException("\App\Libraries\DroplockerObjects\NotFound_Exception");
        new Location($location->{$location::$primary_key});
    }
    
    /**
     * Test a location that has lockers with the status of removed. 
     * verify that the function isActive returns false
     */
    function testIsActive(){
        $this->markTestSkipped("This Matt test nneeds to be refactored not to use prexisting data in the database.");
        // harcoded - location_id = 495
        
        $locationObj = new \App\Libraries\DroplockerObjects\Location(495);
        
        $this->assertFalse($locationObj->isActive(), "the location has the wrong status");
    }
    
    
    public function testHasManyLocker()
    {
        $location = UnitTest::get_random_location();
        
        $this->CI->db->select(Locker::$primary_key);
        $this->CI->db->where("location_id", $location->{Location::$primary_key});
        $this->CI->db->order_by(Locker::$primary_key);
        
        $get_lockers_query = $this->CI->db->get(Locker::$table_name);
        if ($this->CI->db->_error_message())
        {
            throw new \Database_Exception($this->CI->db->_error_message(), $this->CI->db->last_query(), $this->CI->db->_error_number());
        }
        $lockers = $get_lockers_query->result();
        foreach ($lockers as $index => $locker)
        {
            $locker = new Locker($locker->{Locker::$primary_key}, true);
            $this->assertEquals($locker->properties, $location->relationships["Locker"][$index]->properties);
        }
    }

}

?>

