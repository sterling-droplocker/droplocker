<?php
use App\Libraries\DroplockerObjects\Coupon;


class CouponTest extends UnitTest
{
    public function setUp()
    {
        parent::setUp();
        date_default_timezone_set("America/Los_Angeles");
    }


    /**
     * The following test verifies the functionality for saving and deleting a Coupon instanitated class
     */
    public function testSaveAndDelete()
    {
        $order = UnitTest::create_random_order();

        $coupon = new Coupon();
        $coupon->prefix = "AF";
        $coupon->startDate = new \DateTime("06/01/88");
        $coupon->endDate = new \DateTime("07/01/92");
        $coupon->amountType = "dollars";
        $coupon->frequency = "every order";
        $coupon->description = "test description";
        $coupon->extendedDesc = "test extended description";
        $coupon->email = "testing@example.com";
        $coupon->fullName = "Test Full Name";
        $coupon->firstTime = 1;
        $coupon->amount = "5.00";
        // updated to use random word. hardcoded 'ABCD' was causing random test errors
        $coupon->code = UnitTest::get_random_word(1);
        $coupon->business_id = 3;
        $coupon->maxAmt = "3.00";
        $coupon->combine = 1;
        $coupon->redeemed = 1;
        $coupon->order_id = $order->orderID;
        $coupon->recurring_type_id = 0;
        $coupon->base_id = 0;
        $coupon->sentRecipientEmail = 1;

        $coupon->save();

        $saved_coupon = new Coupon($coupon->couponID);



        $this->assertEquals($coupon->properties, $saved_coupon->properties);

        $saved_coupon->delete();

        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        new Coupon($saved_coupon->couponID);
    }
}

?>
