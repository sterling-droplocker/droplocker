<?php
use App\Libraries\DroplockerObjects\MaintenanceLog;
use App\Libraries\DroplockerObjects\Business;
use App\Libraries\DroplockerObjects\Employee;
class MaintenanceLogTest extends UnitTest{
    protected $employee;

    public function setUp()
    {
        parent::setUp();
        $this->employee = UnitTest::create_random_employee($this->business_id);
    }
    public function testCreateAndDelete()
    {
        
        $maintenance_log = new App\Libraries\DroplockerObjects\MaintenanceLog();
        $maintenance_log->employee_id = $this->employee->{Employee::$primary_key};
        $maintenance_log->business_id = $this->business->{Business::$primary_key};
        $maintenance_log->datePerformed = new DateTime();
        $maintenance_log->machine ="Test machine";
        $maintenance_log->workPerformed ="Test work performed.";
        $maintenance_log->save();
        
        $new_maintenance_log = new MaintenanceLog($maintenance_log->{MaintenanceLog::$primary_key});
        $new_maintenance_log->datePerformed->setTimezone(new DateTimeZone($this->business->timezone));
        $this->assertEquals($new_maintenance_log->properties, $maintenance_log->properties);
        
        $maintenance_log->delete();
        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        new MaintenanceLog($maintenance_log->{MaintenanceLog::$primary_key});
    }
    
    public function tearDown()
    {
        if ($this->employee instanceOf \App\Libraries\DroplockerObjects\Employee)
        {
            $this->employee->delete();
        }
        parent::tearDown();
        
    }
}

?>
