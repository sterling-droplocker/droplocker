<?php
use App\Libraries\DroplockerObjects\Customer;

/**
 * Description of customerTest
 *
 * @author ariklevy
 */
class CustomerObjectTest extends UnitTest
{
    protected $business_id;
    public function setUp()
    {
        parent::setUp();
        $this->business_id = 3;
    }
    
    /**
     * Tests that the correct discounts are returned for a customer
     * 
     * Steps:
     * create a customer
     * add a laundryplan to the customers account
     * add a coupon to the customers account
     * get the customers discounts
     * verify that only the coupon was returned
     */
    function testGetActiveDiscounts()
    {
        $this->markTestSkipped("This test needs to be refactoredt o not depend on preexisting data in the database");
        $customer = $this->create_random_customer_on_autopay($this->business_id);
        $this->addLaundryPlanToCustomer($customer->customerID);
        $this->addCustomerDiscountToCustomer($customer->customerID);
        
        $discounts = \App\Libraries\DroplockerObjects\CustomerDiscount::search_aprax(array('customer_id'=>$customer->customerID));
        $this->assertCount(2, $discounts, 'The customer should have 2 discounts');
        
        $coupons = \App\Libraries\DroplockerObjects\Customer::getActiveDiscounts($customer->customerID, $this->business_id);
        $this->assertCount(1, $coupons, 'The customer should have 1 coupon');
    }
    
    
    /**
     * The following test verifies the functionality for searching for a customer 
     */
    public function testSearch()
    {
        $existing_customer = $this->get_random_customer($this->business_id);
        
        $customer = Customer::search(array("email" => $existing_customer->email));
        $this->assertNotEmpty($customer);

    }
    /**
     *The following test verifies the functionality for deleting a customer.
     */
    public function testCreateAndDelete() {
        $email = "asdfzv@qwerazva.net";
        $this->CI->db->where("email", $email);
        $this->CI->db->delete("customer");
        
        $customer = new Customer();
        $customer->email = $email;
        $customer->firstName = UnitTest::get_random_word(5);
        $customer->lastName = UnitTest::get_random_word(5);
        $customer->password = "asdfzxva";
        $customer->business_id = $this->business_id;
        $customer->save();

        $this->CI->load->model("customer_model");
        $new_customer = $this->CI->customer_model->get_by_primary_key($customer->customerID);

        $this->assertEquals($new_customer->email, $customer->email, "The created customer's email does not match the original customer's email");
        $this->assertEquals($new_customer->firstName, $customer->firstName, "The created customer's first name does not match the original customer's first name");
        $this->assertEquals($new_customer->lastName, $customer->lastName, "The created customer's last name does not match the customer's last name");
        $this->assertEquals($new_customer->password, $customer->password, "The created customer's password doe snot matdch the customer's password");

        $customer->delete();
        $this->setExpectedException("App\Libraries\DroplockerObjects\NotFound_Exception");
        new Customer($new_customer->customerID);        
    }
    /**
     * Note, this test assumes that the MySQL server timezone is set to GMT.
     */
    public function testUpdate() {
        $test_customer = UnitTest::create_random_customer_on_autopay();

        $test_customer->firstName = UnitTest::get_random_word(5);
        $test_customer->lastName = UnitTest::get_random_word(5);
        $test_customer->save();

        $this->CI->load->model("customer_model");
        $updated_customer = $this->CI->customer_model->get_by_primary_key($test_customer->customerID);
        $this->assertEquals($test_customer->firstName, $updated_customer->firstName);
        $this->assertEquals($test_customer->lastName, $updated_customer->lastName);
        $this->assertEquals($test_customer->address1, $updated_customer->address1);
        $this->assertEquals($test_customer->address2, $updated_customer->address2);
        $this->assertEquals($test_customer->city, $updated_customer->city);
        $this->assertEquals($test_customer->state, $updated_customer->state);
        $this->assertEquals($test_customer->zip, $updated_customer->zip);
        $this->assertEquals($test_customer->phone, $updated_customer->phone);
        $this->assertEquals($test_customer->sms, $updated_customer->sms);
        $this->assertEquals($test_customer->business_id, $updated_customer->business_id);
        $this->assertEquals($test_customer->autoPay, $updated_customer->autoPay);
        $this->assertEquals($test_customer->preferences, $updated_customer->preferences);
    }
    /**
     * The following test verifies the functionality of getBags()
     */
    public function testGetBags()
    {
        $this->markTestSkipped('$bag is not set - throwing notice and getBags is deprecated');
        $random_customer = UnitTest::get_random_customer($this->business_id);
        $customer = new Customer($random_customer->customerID);
        $getBags_result = $customer->getBags();
        $this->assertEmpty(array_diff($bags, $getBags_result));
    }
    
    
    public function testUnsubscribe(){
        $customer = $this->create_random_customer_on_autopay();
        $customer->unsubscribe();
        $customerObj = new \App\Libraries\DroplockerObjects\Customer($customer->customerID);
        
        $this->assertEquals(0, $customerObj->noEmail, 'customer will still get email');
    }
        
}

?>
