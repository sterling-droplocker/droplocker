<?php
use App\Libraries\DroplockerObjects\Item;
/**
 * Description of ItemTest
 *
 * @author ariklevy
 */
class ItemTest extends UnitTest
{
    private $random_item;
    public function setUp()
    {
        parent::setUp();
        $this->random_item = UnitTest::create_random_item($this->business_id);
    }
    public function testProductProcess()
    {
        $product_process = UnitTest::create_random_product_process();
        $this->random_item->product_process_id = $product_process->product_processID;
        $this->random_item->save();
        $this->random_item->get_relationships();
        
        $this->assertEquals($product_process->properties, $this->random_item->relationships['Product_Process'][0]->properties);
    }
    public function testGetPicture()
    {
        if (empty($this->random_item->relationships['Picture']))
        {
            $this->assertEmpty($this->random_item->getPicture());
        }
        else
        {
            $this->assertEquals($this->random_item->relationships['Picture'][0]->file, $this->random_item->getPicture());
        }
    }
    public function testGetCustomer()
    {
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_ARRAY, $this->random_item->getCustomer());
    }
}

?>
