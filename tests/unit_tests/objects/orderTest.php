<?php
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Bag;
/**
 * Description of orderTest
 *
 * @author ariklevy
 */
class OrderObjectTest extends UnitTest
{
    private $existing_orderID;
    protected $business_id;
    
    public function setUp()
    {
        parent::setUp();
        
        $random_order = $this->create_random_order($this->business_id);
        
        $this->existing_orderID = $random_order->orderID;
    }
    /**
     * The following test verifies the fucntionality for creating an \App\Libraries\DroplockerObjects\Order.
     */
    public function testCreate()
    {
        $order = new Order();

        $random_customer = UnitTest::create_random_customer_on_autopay($this->business_id);
        
        $order->customer_id = $random_customer->customerID;
        
        $order->business_id = $this->business_id;
        
        $location = UnitTest::create_random_location($this->business_id);
        $locker = UnitTest::create_random_locker($location->{Location::$primary_key});
        $bag = UnitTest::create_random_bag($this->business_id);
        $order->locker_id = $locker->{Locker::$primary_key};
        $order->bag_id = $bag->{Bag::$primary_key};
        $order->orderStatusOption_id=1;
        
        $order->save();
        
        $new_order = new Order($order->orderID);
        
        foreach ($order->to_array() as $property => $value)
        {
            if (empty($value))
            {
                $testProperty = $new_order->property;
                $this->assertTrue(empty($testProperty));
            }
            else
            {
                $this->assertEquals($value, $new_order->$property, "'$property' property does not match.");
            }
        }
    }
    public function testGetTaxAmt()
    {
        $order = new Order($this->existing_orderID);
        $tax_amount = $order->getTaxAmt();
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_FLOAT, $tax_amount);
    }
    public function test_get_discount_total()
    {
        $order = new Order($this->existing_orderID);
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_FLOAT, $order->get_discount_total());
    }
    public function test_get_gross_total()
    {
        $order = new Order($this->existing_orderID);
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_FLOAT, $order->get_gross_total());
    }
    public function test_getTotal()
    {
        $order = new Order($this->existing_orderID);
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_FLOAT, $order->getTotal());
    }
    public function test_getItems()
    {
        $order = new Order($this->existing_orderID);
        $result = $order->getItems();
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_ARRAY, $result);
    }
    
    public function testGetOrderType()
    {
        $this->markTestSkipped("This test needs to use test fixtures instead of depending on preexisting data and also properly escape parameters in queries.");
        $order = new Order($this->existing_orderID);
        $order_type = $order->GetOrderType();
        $this->assertInternalType(PHPUnit_Framework_Constraint_IsType::TYPE_STRING, $order_type);
    }
}

?>
