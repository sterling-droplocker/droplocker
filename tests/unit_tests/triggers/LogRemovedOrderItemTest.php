<?php

use App\Libraries\DroplockerObjects\TriggerOrderItemRemoved;
class LogRemovedOrderItemTest extends UnitTest {
    /**
     * The following test verifies the functionality of the 'logRemovedOrderItem' trigger by creating an order item, deleting it, and asserting that the values logged in the triggerOrderItemRemoved table match the expected property values of the deleted order item.
     */
    public function testLogRemovedOrderItem() {
        $order = UnitTest::create_random_order();
        $orderItem = UnitTest::create_random_orderItem($order->orderID);
        $orderItem->refresh();
        $orderItem->delete();
        
        $orderItemsRemoved = TriggerOrderItemRemoved::search_aprax(array("orderItem_id" => $orderItem->orderItemID));

        $orderItemRemoved = $orderItemsRemoved[0];
        
        $this->assertEquals($orderItem->order_id, $orderItemRemoved->order_id, "The logged order ID is not the expected value");
        $this->assertEquals($orderItem->qty, $orderItemRemoved->qty, "The logged quantity is not the expected value");
        $this->assertEquals($orderItem->supplier_id, $orderItemRemoved->supplier_id, "The logged supplier ID is not the expected value");
        $this->assertEquals($orderItem->unitPrice, $orderItemRemoved->unitPrice, "The logged unit price is not the expected value");
        $this->assertEquals($orderItem->notes, $orderItemRemoved->notes, "The logged notes is not the expected value");
        $this->assertEquals($orderItem->updated, $orderItemRemoved->updated, "The logged updated timestamp is not the expected value");
        $this->assertEquals($orderItem->item_id, $orderItemRemoved->item_id, "The logged item ID is not the expected value");
        $this->assertEquals($orderItem->cost, $orderItemRemoved->cost, "The logged cost is not the expected value");
        $this->assertEquals($orderItem->product_process_id, $orderItemRemoved->product_process_id, "The logged product process ID is not the expected value");
        $this->assertEquals($orderItem->scanned, $orderItemRemoved->scanned, "The scanned status is not the expected value");
    }
}

?>
