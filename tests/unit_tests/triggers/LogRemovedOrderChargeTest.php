<?php
use App\Libraries\DroplockerObjects\TriggerOrderChargeRemoved;
/**
 *  The following test suite verifies the functionality of the 'triggerOrderChargeRemovd'
 */
class LogRemovedOrderChargeTest extends UnitTest{
    
    public function testLogRemovedOrderCharge() {
        $order = UnitTest::create_random_order();
        $orderCharge = UnitTest::create_random_orderCharge($order->orderID);
        $orderCharge->refresh();
        $orderCharge->delete();
        $removedOrderCharges = TriggerOrderChargeRemoved::search_aprax(array("orderCharge_id" => $orderCharge->orderChargeID));
        $this->assertNotEmpty($removedOrderCharges, "Did not find the expected removed order charges in the deleted order charges table");
        $removedOrderCharge = $removedOrderCharges[0];
        $this->assertEquals($orderCharge->order_id, $removedOrderCharge->order_id, "The logged orderID is not the expected value.");
        $this->assertEquals($orderCharge->chargeType, $removedOrderCharge->chargeType, "The logged charge type is not the expected value");
        $this->assertEquals($orderCharge->chargeAmount, $removedOrderCharge->chargeAmount, "The logged charge amount is not the expected value");
        $this->assertEquals($orderCharge->customerDiscount_id, $removedOrderCharge->customerDiscount_id, "The logged customer discount is not the expected value.");
        $this->assertEquals($orderCharge->updated, $removedOrderCharge->updated, "The 'updated' field is not the expected value");
    }
}

?>
