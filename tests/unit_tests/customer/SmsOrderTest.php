<?php
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer_Location;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Claim;
use App\Libraries\DroplockerObjects\Server_Meta;

class SmsOrderTest extends UnitTest
{
    private $customer;
    private $claim;

    private $location;
    private $locker;
    public function setUp()
    {
        parent::setUp();
        Server_Meta::set("sms_keyword", "LAUNDRY");
        $this->CI = get_instance();

        $this->business_id = 3;

        $address = "droplocker.{$this->domain_extension}/account/sms/create_claim";

        $this->curl_connection = curl_init();
        curl_setopt($this->curl_connection, CURLOPT_URL, $address);
        curl_setopt($this->curl_connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl_connection, CURLOPT_POST, true);

        $this->data = array();
        $this->data['id'] = 18432;
        $this->data['message'] = "Locker 07";
        $this->data['date'] = date("Y-m-d");
        $this->data['carrier'] = "ATT";
        $this->data['carrierId'] = 310072;
        $this->data['operator_name'] = "att";
        $this->data['operator_id'] = 13225;

        $this->CI->load->model("customer_model");

        $email = "smsTester@example.com";
        $customers = Customer::search_aprax(array("email" => $email), false);
        foreach ($customers as $customer)
        {
            $customer->delete();
        }

        //The following variables store the test user information.
        $this->customer = new Customer();
        $this->customer->business_id = $this->business_id;
        $this->customer->sms = "9079784495@txt.att.net";
        $this->customer->firstName = "smsTester";
        $this->customer->lastName = "smsTester";

        $this->customer->email = "smsTester@example.com";
        $this->customer->password = "smsTester";
        $this->customer->save();


        $this->CI->load->model("location_model");
        $this->CI->load->model("customer_location_model");
        $this->CI->load->model("order_model");
        $this->CI->load->model("claim_model");
        $this->CI->load->model("locker_model");

    }

    /**
     * The following test verifies the functionality for placing a claim through the SMS order functionality.
     * @throws Exception
     */
    public function testPlaceClaimOnLockers()
    {
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->locationID);
        $this->locker->lockerName = '123';
        $this->locker->save();


        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();

        $customer_location = new Customer_Location();
        $customer_location->customer_id = $this->customer->customerID;
        $customer_location->location_id = $this->location->locationID;
        $customer_location->save();

        $bag  = new Bag();
        $bag->bagNumber = Bag::get_new_bagNumber();
        $bag->customer_id = $this->customer->customerID;
        $bag->business_id = $this->customer->business_id;
        $bag->save();

        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.

        try
        {
            $this->CI->order_model->new_order($this->location->locationID,$this->locker->lockerID,$bag->bagNumber,1, "", 3, array("customer_id" => $this->customer->customerID));
        }
        catch (\InvalidArgumentException $e)
        {
            $this->fail("Could not place order in location '{$this->location->address}' (Location ID '{$this->location->locationID}') Locker ID: '{$this->locker->lockerID}'. '{$e->getMessage()}')");
        }

        $this->data['message'] = "laundry {$this->locker->lockerName}";
        $this->data['phone'] = "1" . $this->customer->sms;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }

        $response = json_decode($result);


        $claims = Claim::search_aprax(array("locker_id" => $this->locker->lockerID));
        $this->assertNotEmpty($claims, "Could not find new claim in database." . print_r($post_data, TRUE) . $response->message);

        $this->claim = $claims[0];

        $this->assertEquals("Hi {$this->customer->firstName} {$this->customer->lastName}, your claim was successfully placed in locker {$this->locker->lockerName} at location {$this->location->address}.",
            $response->message,
            "
              The response message is not the expected value.
                location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}

            "
        );
    }
    public function testPlaceClaimOnUnlinkedPhone()
    {
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});

        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();

        $customer_location = new Customer_Location();
        $customer_location->customer_id = $this->customer->customerID;
        $customer_location->location_id = $this->location->locationID;
        $customer_location->save();

        $bag = UnitTest::create_random_bag($this->business_id, $this->customer->customerID);
        try
        {
            $this->CI->order_model->new_order($this->location->locationID,$this->locker->lockerID,$bag->bagNumber,1, "", 3, array("customer_id" => $this->customer->customerID));
        }
        catch (\InvalidArgumentException $e)
        {
            $this->fail("Could not place order in location '{$this->location->address}' (Location ID '{$this->location->locationID}') Locker ID: '{$this->locker->lockerID}'. '{$e->getMessage()}')");
        }
        //The following code deletes any claims on the locker.
        $this->CI->claim_model->options = array("locker_id" => $this->locker->lockerID);
        $this->CI->claim_model->delete();
        $this->assertEmpty($this->CI->db->_error_message(), "Error at attempt to delete claim on locker: {$this->CI->db->_error_message()}");

        $this->data['message'] = "laundry {$this->locker->lockerName}";
        $nonexistent_sms_number = "11006530808@txt.att.net";
        $this->data['phone'] = $nonexistent_sms_number;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);
        $this->assertEquals("Sorry, this cell phone is not linked to your account.  Please text: LAUNDRY EMAIL [your email].  e.g. LAUNDRY EMAIL TSMITH@GMAIL.COM '" . substr($nonexistent_sms_number, 1) . "'",
            $response->message,
            "
              The response message is not the expected value.
                location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}
            "
        );

        $this->data['message'] = "LAUNDRY EMAIL {$this->customer->email}";
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);
        $response = json_decode($result);
        $this->assertEquals("SMS number: " . substr($nonexistent_sms_number, 1) . " has been linked to acccount: {$this->customer->email}. You may now place your order by texting the word LAUNDRY", $response->message,
            "
              The response message is not the expected value.
                location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}
            "
        );

    }
    public function testPlaceClaimOnLockerAlreadyClaimed() {
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->locationID);
        $this->locker->save();

        $this->customer->location_id = $this->location->{Location::$primary_key};
        $this->customer->save();
        $bag = UnitTest::create_random_bag($this->business_id, $this->customerID);
        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.
        try {
            $this->CI->order_model->new_order($this->location->locationID,$this->locker->lockerID,$bag->bagNumber,1, "", 3, array("customer_id" => $this->customer->customerID));
        }
        catch (\Exception $e) {
            $this->fail("Could not place order in location '{$this->location->address}' (Location ID '{$this->location->locationID}') Locker ID: '{$this->locker->lockerID}'. '{$e->getMessage()}')");
        }

        //The following code retrieves an active locker in the location.

        $this->claim = new Claim();
        $this->claim->customer_id = $this->customer->{\App\Libraries\DroplockerObjects\Customer::$primary_key};
        $this->claim->locker_id = $this->locker->{Locker::$primary_key};
        $this->claim->orderType="a:1:{i:0;i:1;}";
        $this->claim->updated = new DateTime();
        $this->claim->active = "1";
        $this->claim->business_id = $this->business_id;
        $this->claim->notes = "ORDER TYPE: SMS";

        $this->claim->save();
        $this->data['message'] = "laundry {$this->locker->lockerName}";
        $this->data['phone'] = "1" . $this->customer->sms;

        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result) {
            throw new \Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);
        $this->assertEquals("The locker {$this->locker->lockerName} at location {$this->location->address} has already been claimed, please choose another locker.",
            $response->message,
            "
              The response message is not the expected value.
                location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}

            "
        );
    }


    public function testPlaceClaimOnConcierge()
    {
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->location->locationType_id=3;
        $this->location->save();
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});

        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();


        $customer_location = new Customer_Location();
        $customer_location->customer_id = $this->customer->customerID;
        $customer_location->location_id = $this->location->locationID;
        $customer_location->save();

        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.

        UnitTest::create_random_order($this->business_id, $this->customer->{Customer::$primary_key}, $this->locker->{Locker::$primary_key});

        $this->data['message'] = "locker";
        $this->data['phone'] = "1" . $this->customer->sms;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);

        $error_message = " The response message is not the expected value.
                sms message: " . print_r($post_data, TRUE) . " location_id: {$this->location->locationID}, locker_id: {$this->locker->{Locker::$primary_key}}
        ";

        $claims = Claim::search_aprax(array("locker_id" => $this->locker->{Locker::$primary_key}));
        $this->assertNotEmpty($claims, "Could not find new claim in database." . print_r($post_data, TRUE) . $response->message);

        $this->claim = $claims[0];

        //The following statement checks to see if the response message reports a successful claim at the right locker and address.
        $this->assertEquals("Hi {$this->customer->firstName} {$this->customer->lastName}, your claim was successfully placed in {$this->locker->lockerName} at location {$this->location->address}.",
            $response->message, $error_message
        );
    }
    public function testPlaceClaimOnHomeDelivery()
    {
        //The following statements retrieve a random active locker location.
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->location->locationType_id=4;
        $this->location->save();
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});

        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();


        $customer_location = new Customer_Location();
        $customer_location->customer_id = $this->customer->customerID;
        $customer_location->location_id = $this->location->locationID;
        $customer_location->save();

        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.

        //Note, to create a new bag number, we find the top most bag number currently in the system and add 1 to its value.

        $new_bag_number = Bag::get_new_bagNumber();

        UnitTest::create_random_order($this->business_id, $this->customer->{Customer::$primary_key}, $this->locker->{Locker::$primary_key});



        $this->data['message'] = "locker";
        $this->data['phone'] = "1" . $this->customer->sms;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);

        $error_message = " The response message is not the expected value.
                sms message: " . print_r($post_data, TRUE) . " location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}
        ";

        $claims = Claim::search_aprax(array("locker_id" => $this->locker->lockerID));
        $this->assertNotEmpty($claims, "Could not find new claim in database." . print_r($post_data, TRUE). $response->message);

        $this->claim = $claims[0];

        //The following statement checks to see if the response message reports a successful claim at the right locker and address.
        $this->assertEquals("Hi {$this->customer->firstName} {$this->customer->lastName}, your claim was successfully placed in {$this->locker->lockerName} at location {$this->location->address}.",
            $response->message, $error_message

        );
    }
    public function testPlaceClaimOnKiosk()
    {
        $new_bag_number = bag::get_new_bagNumber();

        $this->location = UnitTest::create_random_location($this->business_id);
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});
        $this->locker->save();

        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.
        $this->CI->order_model->new_order($this->location->{Location::$primary_key},$this->locker->{Locker::$primary_key},$new_bag_number,1, "", 3, array("customer_id" => $this->customer->customerID));


        //Note, we need to make sure to set the default location and SMS number for the test customer.
        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();

        //The following statement inserts the default location of the customer into the list of customer locations for the customer.
        $this->CI->customer_location_model->options = array("customer_id" => $this->customer->customerID, "location_id" => $this->location->locationID);
        $this->CI->customer_location_model->insert();
        $this->assertEmpty($this->CI->db->_error_message(), "Error at attempt to insert into customer locations: {$this->CI->db->_error_message()}");

        //The following code deletes any claims on the locker.
        $this->CI->claim_model->options = array("locker_id" => $this->locker->lockerID);
        $this->CI->claim_model->delete();
        $this->assertEmpty($this->CI->db->_error_message(), "Error at attempt to delete claim on locker: {$this->CI->db->_error_message()}");

        $this->data['message'] = "laundry {$this->locker->lockerName}";
        $this->data['phone'] = "1" . $this->customer->sms;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);


        $claims = Claim::search_aprax(array("locker_id" => $this->locker->lockerID));
        $this->assertNotEmpty($claims, "Could not find new claim in database.". print_r($post_data, TRUE) . $response->message);

        $this->claim = $claims[0];

        $this->assertEquals("Hi {$this->customer->firstName} {$this->customer->lastName}, your claim was successfully placed in locker {$this->locker->lockerName} at location {$this->location->address}.",
            $response->message,
            "
              The response message is not the expected value.
                location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}

            "
        );
    }
    public function testPlaceClaimOnCornerStore()
    {
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->location->locationType_id = 3; //Note, "3" is expected to be to be location type "locker".
        $this->location->save();
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});


        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();

        $customer_location = new Customer_Location();
        $customer_location->customer_id = $this->customer->customerID;
        $customer_location->location_id = $this->location->locationID;
        $customer_location->save();

        $bag = UnitTest::create_random_bag($this->business_id, $this->customerID);

        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.
        UnitTest::create_random_order($this->business_id, $this->customer->{Customer::$primary_key}, $this->locker->{Locker::$primary_key});



        $this->data['message'] = "locker";
        $this->data['phone'] = "1" . $this->customer->sms;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);

        $claims = Claim::search_aprax(array("locker_id" => $this->locker->{Locker::$primary_key}));
        $this->assertNotEmpty($claims, "Could not find new claim in database: " . print_r($post_data, TRUE) . $response->message);

        $this->claim = $claims[0];


        $error_message = " The response message is not the expected value.
                sms message: " . print_r($post_data, TRUE) . " location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}
        ";



        //The following statement checks to see if the response message reports a successful claim at the right locker and address.
        $this->assertEquals("Hi {$this->customer->firstName} {$this->customer->lastName}, your claim was successfully placed in {$this->locker->lockerName} at location {$this->location->address}.",
            $response->message, $error_message

        );
    }


    public function testPlaceClaimOnOffice()
    {

        //The following statements retrieve a random active locker location.
        $this->location = UnitTest::create_random_location($this->business_id);
        $this->location->locationType_id=6;
        $this->location->save();
        $this->locker = UnitTest::create_random_locker($this->location->{Location::$primary_key});

        $this->customer->location_id = $this->location->locationID;
        $this->customer->save();

        //The following statement inserts the default location of the customer into the list of customer locations for the customer.
        $this->CI->customer_location_model->options = array("customer_id" => $this->customer->customerID, "location_id" => $this->location->locationID);
        $this->CI->customer_location_model->insert();
        $this->assertEmpty($this->CI->db->_error_message(), "Error at attempt to insert into customer locations: {$this->CI->db->_error_message()}");

        //The following statement creates an order for this test user because we need at least one order in order to place a claim through SMS.

        $new_bag_number = Bag::get_new_bagNumber();
        UnitTest::create_random_order($this->business_id, $this->customer->{Customer::$primary_key}, $this->locker->{Locker::$primary_key});


        $this->data['message'] = "locker";
        $this->data['phone'] = "1" . $this->customer->sms;

        //The following statements send the SMS order text message to the server.
        $post_data = array("JSON" => json_encode($this->data));
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($this->curl_connection);

        if (!$result)
        {
            throw new Exception(curl_error($this->curl_connection));
        }
        $response = json_decode($result);

        $claims = Claim::search_aprax(array("locker_id" => $this->locker->lockerID));
        $this->assertNotEmpty($claims, "Could not find new claim in database." . print_r($post_data, TRUE) . $response->message);

        $this->claim = $claims[0];

        $error_message = " The response message is not the expected value.
                sms message: " . print_r($post_data, TRUE) . " location_id: {$this->location->locationID}, locker_id: {$this->locker->lockerID}
        ";
        //The following statement checks to see if the response message reports a successful claim at the right locker and address.
        $this->assertEquals("Hi {$this->customer->firstName} {$this->customer->lastName}, your claim was successfully placed in {$this->locker->lockerName} at location {$this->location->address}.",
            $response->message, $error_message

        );
    }
    public function tearDown()
    {
        if ($this->claim)
        {
            $this->claim->delete();
        }
        if ($this->customer instanceof \App\Libraries\DroplockerObjects\Customer)
        {
            $this->customer->delete();
        }
        if ($this->location instanceof \App\Libraries\DroplockerObjects\Location)
        {
            $this->location->delete();
        }
        if ($this->locker instanceof \App\Libraries\DroplockerObjects\Locker)
        {
            $this->locker->delete();
        }
        curl_close($this->curl_connection);
        parent::tearDown();
    }
}
?>
