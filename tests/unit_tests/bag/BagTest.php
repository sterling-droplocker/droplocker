<?php
use App\Libraries\DroplockerObjects\Bag;

class BagTest extends UnitTest
{
    var $CI;
    var $bagID;
    var $newBagNumber;
    var $bag;
    CONST business_id = 3;
    CONST customer_id = 21419;
    
    public function setUp()
    {
        parent::setUp();
        $this->CI->load->model("bag_model");
        lm("bag_model");
    }
    
    /**
     * test will insert a new bag (tempBag)
     */
    function testGetId(){
        
        // new temp bag created

        $bagNumber = Bag::get_new_bagNumber();
        
        $bag = $this->CI->bag_model->get_id($bagNumber, self::business_id);
        $this->assertTrue($bag['is_new'], 'test to see if the bag is new');
        $this->assertEquals('', $bag['customer_id'], 'test bag matches customer');
        $this->assertEquals("new bag created", $bag['log'], 'test new bag created');
        
        // Bag found ith customer_id = 0
        $bag = $this->CI->bag_model->get_id($bagNumber, self::business_id);
        //die(print_r($bag));
        //$this->assertEquals(2884, $bag['log'], "bag found without customer, assuming temp bag");
        
        // add a customer to the bag for further testing
        $affected_rows = $this->CI->bag_model->addCustomerToBag(self::customer_id, $bag['bagID']);
        
        //found bag with the correct customer_id
        $bag = $this->CI->bag_model->get_id($bagNumber, self::business_id, self::customer_id);
        $this->assertEquals("bag found with customer id", $bag['log'],"bag found with customer id");
        
        //customer bag mismatch
        $bag = $this->CI->bag_model->get_id($bagNumber, self::business_id, 123);
        $this->assertEquals("bag found with different customer id - Need to do something about this", $bag['log'], 'found problem with bag');
        
        // delete the bag
        $this->CI->bag_model->clear();
        $this->CI->bag_model->bagID = $bag['bagID'];
        $this->CI->bag_model->delete();
        
    }
    
    
   /**
    * Test will get a bag and assign a customer to it
    */
   function testAddCustomerToBag(){

       // add a new bag for further testing
       $bag = $this->CI->bag_model->get_id(Bag::get_new_bagNumber(), self::business_id);
       $this->assertTrue($bag['is_new']);
       
       // add customer to the bag
       $affected_rows = $this->CI->bag_model->addCustomerToBag(self::customer_id, $bag['bagID']);
       $this->assertEquals($affected_rows, 1, 'One affected row');
       
       // This should fail becuase the customer is already added to bag. Therefore there will be no affected rows
       $affected_rows = $this->CI->bag_model->addCustomerToBag(self::customer_id, $bag['bagID']);
       $this->assertEquals($affected_rows, 0 , 'trying to add the same customer to a bag, there should not be an affected rows');
       
       
       // delete the bag
       $this->CI->bag_model->clear();
       $this->CI->bag_model->bagID = $bag['bagID'];
       $this->CI->bag_model->delete();
       
   }
   
   /**
    * tests deleting a bag
    * 
    * steps:
    * create a bag
    * add bag to customer
    * delete the bag.
    * 
    * tests:
    * delete a bag that doesn't exist
    * delete a bag that belongs to a different business
    * delete a bag that does not have any orders
    * delete a bag that has orders
    * 
    */
   function testDeleteBag(){
       $this->markTestSkipped("The test is failing and Matt has refused to fix it.");
       $business_id = 20; 
       
       // try to delete a bag that does not exist
       try {
           $bogusID = 99999999;
           $this->CI->bag_model->deleteBag($bogusID, $business_id);
       } catch(Exception $e){
           $this->assertEquals('bagID \''.$bogusID.'\' not found.', $e->getMessage(), 'did not receive the proper exception from the DropplockerObject');
       }
       
       // try to delete a bag from a different businesses
       // create a LL customer and then when deleting the bag, use the DL id (20)
       $bagNumber = Bag::get_new_bagNumber();
       $customer = $this->get_random_customer(3);
       $bag1 = $this->addBagTOCustomer($bagNumber, $customer->customerID, $business_id);
       
       try {
           $this->CI->bag_model->deleteBag($bag1->bagID, $business_id);
       } catch(Exception $e){
           $this->assertEquals('Cannot delete a bag that do not belong to business.', $e->getMessage(), 'did not receive the proper exception');
       }
       
       // Delete a bag without any orders
       $bagNumber = Bag::get_new_bagNumber();
       $customer = $this->get_random_customer($business_id);
       $bag2 = $this->addBagTOCustomer($bagNumber, $customer->customerID, $business_id);
       $result = $this->CI->bag_model->deleteBag($bag2->bagID, $business_id);
       $this->assertEquals("deleted", $result['status'], 'The bag was not deleted');
       
       // Delete a bag with orders
       $bagNumber = Bag::get_new_bagNumber();
       $customer = $this->get_random_customer($business_id);
       $bag3 = $this->addBagTOCustomer($bagNumber, $customer->customerID, $business_id);
       $order = self::create_random_order($business_id);
       
       $order = new \App\Libraries\DroplockerObjects\Order($order->orderID);
       $order->orderStatusOption_id = 10; //This is the status of complete
       $order->bag_id = $bag3->bagID;
       $order->save();
       
       $result = $this->CI->bag_model->deleteBag($bag3->bagID, $business_id);
       $this->assertEquals("reassigned", $result['status'], 'The bag was not reassigned');
       $this->assertEquals($business_id, $bag3->business_id, 'The bag has the wrong business_id');
       
      
   }
   
}
?>