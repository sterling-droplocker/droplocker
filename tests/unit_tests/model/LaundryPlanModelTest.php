<?php


use App\Libraries\DroplockerObjects\Order;

class LaundryPlanModelTest extends UnitTest
{
    function setUp(){
        parent::setUp();
        lm('laundryplan');
    }
    
    function testGetDefaultPrice(){
        $customer = $this->create_random_customer_on_autopay($this->business_id);
        $price = $this->CI->laundryplan_model->getDefaultPrice($customer->customerID, $this->business_id);
        
        $double = gettype($price);
        
        $this->assertEquals('double', $double, "price is not type of float");
        
    }
}