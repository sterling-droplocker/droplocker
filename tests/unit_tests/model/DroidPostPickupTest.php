<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Order;

class DroidPostPickupTest extends UnitTest{
    
    function setUp(){
        parent::setUp();
        $this->CI->load->model('droidpostpickups_model');
    }
    
    
    function testBagNumberGreaterThanMaxBagNumber(){
        $this->markTestSkipped("This test needs to be refactored to not use preexisting data");
        $location = $this->getRandomLockerInLocation();
        $locker = new Locker($location->lockerID);
        $this->clearActiveClaimInLocker($locker);
        $this->clearActiveOrdersInLocker($locker->lockerID);
        
        $maxBagNumber = Bag::get_max_bagNumber();
        $bagNumber = $maxBagNumber + 100;
        
        $pickup['transaction_id'] = md5(date("Y-m-d H:i:s").rand(0,1000))."84";
        $pickup['bagNumber'] = $bagNumber;
        $pickup['location_id'] = $location->locationID;
        $pickup['lockerName'] = $locker->lockerName;
        $pickup['employeeUsername'] = 'unomateo';
        $pickup['employee_id'] = '84';
        $pickup['deliveryTime'] = date("Y-m-d H:i:s");
        $pickup['dateCreated'] = date("Y-m-d H:i:s");
        $pickup['status'] = 'pending';
        $pickup['business_id'] = $this->business_id;
        $id = $this->CI->droidpostpickups_model->insert($pickup);
        
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $this->CI->droidpostpickups_model->processPickup($pickup);
        
        // get the pickup again becuase it should have been updated
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $this->assertEquals("complete", $pickup->status, 'The pickup was not completed');
        
        $statusNotesArray = unserialize($pickup->statusNotes);
        
        // Test that the email goes out to customer support
        $email = explode(":", $statusNotesArray[1]);
        $email_id = $email[1];
        $email = $this->CI->db->get_where("email", array("emailID"=>$email_id))->row();
        $this->assertEquals("Pickup Bagnumber [{$pickup->bagNumber}] is higher then the last bag number [{$maxBagNumber}]", $email->subject, "The email that is sent to customer support has the wrong subject");

    }
    
    /**
     * Tests a pickup from a locker that already has an open order in it.
     * 
     * Steps:
     * 1. create an order in a locker with status of 1 (picked up)
     * 2. simulate a driver picking up a claim in the same locker
     * 3. process the pickup
     * 4. assert values
     */
    function testPickupWithActiveOrderAlreadyInLockerNoClaim(){
        $this->markTestSkipped("This test needs to use test fixtures instead of depending on preexisting data.");
        // create an order in a kiosk location
        $location = $this->getRandomLockerInLocation();
        $locker = new Locker($location->lockerID);
        $this->clearActiveClaimInLocker($locker);
        $this->clearActiveOrdersInLocker($locker->lockerID);
        $order = $this->create_random_order($this->business_id, null, $location->lockerID);
        $customer = new Customer($order->customer_id);
        $bag = new Bag($order->bag_id);
        
        //-----------------------------------------
        // new pickup
        $bagNumber = Bag::get_new_bagNumber();
        $newCustomer = $this->create_random_customer_on_autopay($this->business_id);
        $this->addBagTOCustomer($bagNumber, $newCustomer->customerID);
        
        $pickup['transaction_id'] = md5(date("Y-m-d H:i:s").rand(0,1000))."84";
        $pickup['bagNumber'] = $bagNumber;
        $pickup['location_id'] = $location->locationID;
        $pickup['lockerName'] = $locker->lockerName;
        $pickup['employeeUsername'] = 'unomateo';
        $pickup['employee_id'] = '84';
        $pickup['deliveryTime'] = date("Y-m-d H:i:s");
        $pickup['dateCreated'] = date("Y-m-d H:i:s");
        $pickup['status'] = 'pending';
        $pickup['business_id'] = $this->business_id;
        $id = $this->CI->droidpostpickups_model->insert($pickup);
        
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        
        $this->CI->droidpostpickups_model->processPickup($pickup);
        
        // get the pickup again becuase it should have been updated
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $statusNotesArray = unserialize($pickup->statusNotes);
        $this->assertEquals("complete", $pickup->status, 'The pickup was not completed');
        $this->assertEquals("Non-completed order already in locker. Order[".$order->orderID."]", $statusNotesArray[0], 'The 1st status note is wrong');
        
        // Test that the email goes out to customer support
        $email = explode(":", $statusNotesArray[1]);
        $email_id = $email[1];
        $email = $this->CI->db->get_where("email", array("emailID"=>$email_id))->row();
        $this->assertEquals("Non-completed order already in locker. Order[".$order->orderID."]", $email->subject, "The email that is sent to customer support has the wrong subject");
    
        // verify that the new order has the proper customer
        $newOrder = new Order($pickup->order_id);
        $this->assertEquals($newCustomer->customerID, $newOrder->customer_id, "The second order belongs to the wrong customer");
        
        // delete the db inserts
        $this->CI->db->delete('email', array('emailID'=>$email_id));
        $this->CI->db->delete('droid_postPickups', array('ID'=>$id));
        $this->CI->db->delete('orders', array('orderID'=>$order->orderID));
        $this->CI->db->delete('orders', array('orderID'=>$pickup->order_id));
    }
    
    
    /**
     * Tests a pickup from a locker that already has an open order in it.
     * The current order status is 9, so that order should be completed and a new order will be created
     * for the picked up bag
     *
     * Steps:
     * 1. create an order in a locker with status of 1 (picked up)
     * 2. simulate a driver picking up a claim in the same locker
     * 3. process the pickup
     * 4. assert values
     */
    function testPickupWithActiveOrderAlreadyInLockerNoClaimWithOrderStatus9(){
        $this->markTestSkipped("This test needs to be refactored to not use preexisting data");
        // create an order in a kiosk location
        $location = $this->getRandomLockerInLocation();
        $locker = new Locker($location->lockerID);
        $this->clearActiveClaimInLocker($locker);
        $this->clearActiveOrdersInLocker($locker->lockerID);
        $order = $this->create_random_order($this->business_id, null, $location->lockerID);
        $customer = new Customer($order->customer_id);
        $bag = new Bag($order->bag_id);
        
        // set the status to 9
        $order->orderStatusOption_id = 9;
        $order->save();
    
        //-----------------------------------------
        // new pickup
        $bagNumber = Bag::get_new_bagNumber();
        $newCustomer = $this->create_random_customer_on_autopay($this->business_id);
        $this->addBagTOCustomer($bagNumber, $newCustomer->customerID);
    
        $pickup->transaction_id = md5(date("Y-m-d H:i:s").rand(0,1000))."84";
        $pickup->bagNumber = $bagNumber;
        $pickup->location_id = $location->locationID;
        $pickup->lockerName = $locker->lockerName;
        $pickup->employeeUsername = 'unomateo';
        $pickup->employee_id = '84';
        $pickup->deliveryTime = date("Y-m-d H:i:s");
        $pickup->dateCreated = date("Y-m-d H:i:s");
        $pickup->status = 'pending';
        $pickup->business_id = $this->business_id;
        $this->CI->db->insert('droid_postPickups', $pickup);
        $id = $this->CI->db->insert_id();
    
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $this->CI->load->model('droidpostpickups_model');
        $this->CI->droidpostpickups_model->processPickup($pickup);
        
        // verify that the original order was completed
        $order = new Order($order->orderID);
        $this->assertEquals(10, $order->orderStatusOption_id, 'The original order was not completed');
    
        // get the pickup again becuase it should have been updated
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $statusNotesArray = unserialize($pickup->statusNotes);
        $this->assertEquals("complete", $pickup->status, 'The pickup was not completed');
        
        // verify that the new order has the proper customer
        $newOrder = new Order($pickup->order_id);
        $this->assertEquals($newCustomer->customerID, $newOrder->customer_id, "The second order belongs to the wrong customer");
        
        
        // delete the db inserts
        $this->CI->db->delete('droid_postPickups', array('ID'=>$id));
        $this->CI->db->delete('orders', array('orderID'=>$order->orderID));
        $this->CI->db->delete('orders', array('orderID'=>$pickup->order_id));
    }
    
    
    /**
     * Tests a pickup from a locker that already has an open order in it.
     *
     * Steps:
     * 1. create an order in a locker with status of 1 (picked up)
     * 2. simulate a driver picking up a claim in the same locker
     * 3. process the pickup
     * 4. assert values
     */
    function testPickupWithActiveOrderAlreadyInConciergeNoClaim(){
        $this->markTestSkipped("This test needs to use a barcode create from scratch.");
        // create an order in a kiosk location
        $location = $this->getRandomLockerInLocation("Concierge");
        $locker = new Locker($location->lockerID);
        $this->clearActiveClaimInLocker($locker);
        $this->clearActiveOrdersInLocker($locker->lockerID);
        $order = $this->create_random_order($this->business_id, null, $location->lockerID);
        $customer = new Customer($order->customer_id);
        $bag = new Bag($order->bag_id);
    
        //-----------------------------------------
        // new pickup
        $bagNumber = Bag::get_new_bagNumber();
        $newCustomer = $this->create_random_customer_on_autopay($this->business_id);
        $this->addBagTOCustomer($bagNumber, $newCustomer->customerID);
    
        $pickup->transaction_id = md5(date("Y-m-d H:i:s").rand(0,1000))."84";
        $pickup->bagNumber = $bagNumber;
        $pickup->location_id = $location->locationID;
        $pickup->lockerName = $locker->lockerName;
        $pickup->employeeUsername = 'unomateo';
        $pickup->employee_id = '84';
        $pickup->deliveryTime = date("Y-m-d H:i:s");
        $pickup->dateCreated = date("Y-m-d H:i:s");
        $pickup->status = 'pending';
        $pickup->business_id = $this->business_id;
        $this->CI->db->insert('droid_postPickups', $pickup);
        $id = $this->CI->db->insert_id();
    
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $this->CI->load->model('droidpostpickups_model');
        $this->CI->droidpostpickups_model->processPickup($pickup);
    
        // get the pickup again becuase it should have been updated
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $statusNotesArray = unserialize($pickup->statusNotes);
        $this->assertEquals("complete", $pickup->status, 'The pickup was not completed');
    
    
        
        // verify that the new order has the proper customer
        $newOrder = new Order($pickup->order_id);
        $this->assertEquals($newCustomer->customerID, $newOrder->customer_id, "The second order belongs to the wrong customer");
    
        // delete the db inserts
        
        $this->CI->db->delete('droid_postPickups', array('ID'=>$id));
        $this->CI->db->delete('orders', array('orderID'=>$order->orderID));
        $this->CI->db->delete('orders', array('orderID'=>$pickup->order_id));
    }
    
    
    function testPickupWithBagInOpenOrder(){
        $this->markTestSkipped("This test needs to use test fixtures instead of depending on preexisting data and also properly escape parameters in queries.");
        // create an order in a kiosk location
        $location = $this->getRandomLockerInLocation("Concierge");
        $locker = new Locker($location->lockerID);
        $this->clearActiveClaimInLocker($locker);
        $this->clearActiveOrdersInLocker($locker->lockerID);
        $order = $this->create_random_order($this->business_id, null, $location->lockerID);
        $customer = new Customer($order->customer_id);
        $bag = new Bag($order->bag_id);
    
        //-----------------------------------------
        // new pickup
        $bagNumber = $bag->bagNumber;
        //newCustomer = $this->create_random_customer_on_autopay($this->business_id);
        //$this->addBagTOCustomer($bagNumber, $newCustomer->customerID);
    
        $pickup->transaction_id = md5(date("Y-m-d H:i:s").rand(0,1000))."84";
        $pickup->bagNumber = $bagNumber;
        $pickup->location_id = $location->locationID;
        $pickup->lockerName = $locker->lockerName;
        $pickup->employeeUsername = 'unomateo';
        $pickup->employee_id = '84';
        $pickup->deliveryTime = date("Y-m-d H:i:s");
        $pickup->dateCreated = date("Y-m-d H:i:s");
        $pickup->status = 'pending';
        $pickup->business_id = $this->business_id;
        $this->CI->db->insert('droid_postPickups', $pickup);
        $id = $this->CI->db->insert_id();
    
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $this->CI->load->model('droidpostpickups_model');
        $this->CI->droidpostpickups_model->processPickup($pickup);
    
        // get the pickup again becuase it should have been updated
        $pickup = $this->CI->db->get_where('droid_postPickups', array('ID'=>$id))->row();
        $statusNotesArray = unserialize($pickup->statusNotes);
        $this->assertEquals("complete", $pickup->status, 'The pickup was not completed');
    
        // Test that the email goes out to customer support
        $email = explode(":", $statusNotesArray[1]);
        $email_id = $email[1];
        $email = $this->CI->db->get_where("email", array("emailID"=>$email_id))->row();
        $this->assertEquals("There is already an open order [".$order->orderID."] for this bag", $email->subject, "The email that is sent to customer support has the wrong subject");
        

        // delete the db inserts
        $this->CI->db->delete('email', array('emailID'=>$email_id));
        $this->CI->db->delete('droid_postPickups', array('ID'=>$id));
        $this->CI->db->delete('orders', array('orderID'=>$order->orderID));
        $this->CI->db->delete('orders', array('orderID'=>$pickup->order_id));
    }
}