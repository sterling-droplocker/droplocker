<?php
class BagModelTest extends UnitTest
{
    
    protected $CI;
    private $customer_id;
    private $bagID;
    protected $business_id;
    private $bagNumber;
    
    function __construct(){
        parent::__construct();
    }
    
    function setup(){
        parent::setUp();
        lm('bag');
        $this->bagNumber = 1000103;
        $this->business_id = 3;
        $resultArray = $this->CI->bag_model->get_id($this->bagNumber, $this->business_id);
        $this->bagID = $resultArray['bagID'];
    }
    
    function teardown(){
        lm('bag');
        $this->CI->bag_model->clear();
        $this->CI->bag_model->bagNumber = $this->bagNumber;
        $this->CI->bag_model->delete();
        parent::tearDown();
    }
    
    /**
     * Tests:
     * Exception - Missing bagNumber
     * Exception - Missing business_id
     * inserts a new bag without a customer.
     * gets the bag after it was inserted to check the different return data
     */
    function testGetID(){
        
        lm('bag');
        $this->CI->bag_model->bagNumber = $this->bagNumber;
        $this->CI->bag_model->delete();
        
        try {
           $this->CI->bag_model->get_id('', $this->business_id);
        } catch (Exception $e) {
            $this->assertEquals('Missing bagNumber', $e->getMessage());
        }
        
        try {
            $this->CI->bag_model->get_id($this->bagNumber, '');
        } catch (Exception $e) {
            $this->assertEquals('Missing business_id', $e->getMessage());
        }
        
        $this->business_id = 3;
        $resultArray = $this->CI->bag_model->get_id($this->bagNumber, $this->business_id);
        $this->assertEquals('', $resultArray['customer_id']);
        $this->assertTrue($resultArray['is_new']);
        $this->assertEquals("new bag created", $resultArray['log']);
        $this->assertGreaterThan(0, $resultArray['bagID']);
        
        $this->bagID = $resultArray['bagID'];
   
        $resultArray = $this->CI->bag_model->get_id($this->bagNumber, $this->business_id);
        $this->assertEquals(2884, $resultArray['customer_id']);
        $this->assertGreaterThan(0, $resultArray['bagID']);
        
        
    }
    
    /**
     * Tests:
     * Exception - missing customer id
     * Exception - missing bagID
     * addCustomerToBag Success - returns the affected_row()
     * gets the bag row after the update and checks to that he customer_id matches the bagID
     */
    function testAddCustomerToBag(){
        
       
        $this->customer_id = 21419;
        
        lm('bag');
        try {
            $this->CI->bag_model->addCustomerToBag('', $this->bagID);
        } catch (Exception $e) {
            $this->assertEquals('Missing customer_id', $e->getMessage());
        }
        
        
        try {
            $this->CI->bag_model->addCustomerToBag($this->customer_id, '');
        } catch (Exception $e) {
            $this->assertEquals('Missing bagID', $e->getMessage());
        }
        
        $affectedRows = $this->CI->bag_model->addCustomerToBag($this->customer_id, $this->bagID);
        $this->assertEquals(1, $affectedRows);
        
        $resultArray = $this->CI->bag_model->get_id($this->bagNumber, $this->business_id);
        $this->assertEquals($this->customer_id, $resultArray['customer_id']);
        $this->assertFalse($resultArray['is_new']);
    }
}