<?php
class BusinessModelTest extends UnitTest
{
    function setUp(){
        
        parent::setUp();
        $this->CI->load->model('business_model');
        
    }
    
    function testCopyAcl(){
        $this->markTestSkipped("Not completed");
        $expectedTotal = $this->CI->db->query("SELECT count(*) from acl WHERE business_id = {$this->business_id}")->row();
        $newBusinessID = $this->create_random_business();
        
        $randcomBusiness = $this->CI->db->query("SELECT * FROM business order by rand();")->row();
        
        $this->CI->business_model->copyAcl($newBusinessID, $this->business_id);
        $total = $this->CI->db->query("SELECT count(*) from acl WHERE business_id = {$newBusinessID}")->row();
        
        $this->assertEquals($expectedTotal, $total, 'Acls was not fully copied');
       
        $business = new \App\Libraries\DroplockerObjects\Business($newBusinessID);
        $business->delete();
    }
}