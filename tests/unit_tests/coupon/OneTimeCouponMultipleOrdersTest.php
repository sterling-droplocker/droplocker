<?php
use App\Libraries\DroplockerObjects\DropLockerEmail;

use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer_Location;
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Claim;

class OneTimeCouponMultipleOrdersTest extends UnitTest
{
    
    function __construct(){
        parent::__construct();
        
    }
    
    function setUp(){
        parent::setUp();
    }
    
    
    /**
     * This verifies that a coupon is applied properly to a customer account when that
     * customer has more then one active order.
     * 
     * This test was written due to one time dollar coupons getting applied to all 
     * open orders for a customer when it should only be applied to one.
     * 
     * scenario:
     * Customer has 2 orders in inventoried status
     * Customer then go to discount section and added a one time dollar coupon
     * 
     * verify that only one order received the discount
     * verify that the other order did not
     * 
     */
    function testOneTimeDollarCouponMultipleOrders(){
        $this->markTestSkipped("This test needs to be refactored to use the create_random_coupon function instead of the broken function that Matt wrote and alos not use the broken * function addItemToOrder, which is a duplicate of create_random_orderItem");
        // create a coupon to be used on one order
        $prefix = "MD";
        $code = "3456";
        $couponObj = $this->create_coupon($prefix, $code, 5.00, "test $5 one time dollar coupon", 1, "dollars", "one time");
        
        // create 2 orders for a customer and put them in inventory status
        $customerObj = $this->create_random_customer_on_autopay();
        $location = $this->getRandomLockerInLocation();
        $orderObj = $this->create_random_order($this->business_id, $customerObj->customerID, $location->lockerID); 
        $this->addItemToOrder($orderObj);
        $this->addItemToOrder($orderObj);
        $this->addItemToOrder($orderObj);
        $orderObj->orderStatusOption_id = 3;
        $orderObj->save();
        
        $orderObj2 = $this->create_random_order($this->business_id, $customerObj->customerID, $location->lockerID);
        $this->addItemToOrder($orderObj2);
        $this->addItemToOrder($orderObj2);
        $this->addItemToOrder($orderObj2);
        $orderObj2->orderStatusOption_id = 3;
        $orderObj2->save();
        
        // now apply the coupon
        $this->CI->load->library('coupon');
        $response = $this->CI->coupon->applyCoupon($prefix.$code, $customerObj->customerID, $this->business_id);
        
        // verify that the discount was applied
        $this->assertEquals('success', $response['status'], "Applying the coupon [$prefix.$code] failed");

        /*
         * NOTE: discounts are applied to the most recent orders, therefore
         * the second order will get the customer discount applied first
         */
        $orderCharges = \App\Libraries\DroplockerObjects\OrderCharge::search_aprax(array('order_id'=>$orderObj2->orderID));
        $orderCharge = $orderCharges[0];
        
        $this->assertEquals("-5.00", $orderCharge->chargeAmount, "orderCharge was not applied to the second order [{$orderObj->orderID}]");
        
        $orderCharges = \App\Libraries\DroplockerObjects\OrderCharge::search_aprax(array('order_id'=>$orderObj->orderID));
        $this->assertEmpty($orderCharges, "orderCharge WAS applied to the first order [{$orderObj->orderID}]");

    }
}