<?php

use App\Libraries\DroplockerObjects\Customer;

class AjaxTest extends UnitTest
{
    const business_id = 3;
    private $ckfile;
    
    function __construct(){
        
    }
    
    public function setUp()
    {
        
        parent::setUp();
        
        
        
        /**
         * There are many errors for these tests. Need to be re-developed
         */
        
        $this->markTestSkipped("It's fubar... ");

    }
        
    /**
     * 
     * serarches for a customer
     * 
     * steps:
     * create a new customer
     * search for the new customer username
     * verify we got an array back
     */
    public function testSearchCustomers()
    {
        
        $this->markTestSkipped("When searching for a customer, Ajax is not used.");
        $customer = new Customer();
        $customer->firstName = "test user";
        $customer->lastName = "test user";
        $customer->password = "asfzva";
        $customer->email = UnitTest::get_random_word() . "_testuser@example.com";
        $customer->business_id = $this->business_id;
        $customer->username = UnitTest::get_random_word(). "_testuser";
        $customer->save();
        

        $this->_cookie();
        $url = $this->url.'/ajax/search_customers/';
        
        $ch = curl_init ($url);
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"username=unomateo");
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        echo $output;
        curl_close($ch);

        $result = json_decode($output);
        print_r($result);
        
        $this->assertEquals($customer->business_id, $result[0]->business_id, 'customer business_id does not match the resulting busineness_id');
        $this->assertEquals($customer->email, $result[0]->email, 'Customer email does not match the resulting emails');
        $this->assertEquals($customer->username, $result[0]->username, "Customer username does not match the resulting username");
        $this->assertEquals($customer->firstName, $result[0]->firstName, "Customer firstName does not match the resulting firstName");
        $this->assertEquals($customer->lastName, $result[0]->lastName, "Customer lastName does not match resulting lastName");
        $this->assertEquals($customer->customerID, $result[0]->customerID, "CustomerID does not match resulting customerID");


        $customer->delete();
    }    
    
    /**
     * Simulates searching for a customer. 
     * I create a new customer and then search for that new customer. 
     * I do this because I was running into oddly named customers.
     * 
     * Steps:
     * create a new customer
     * search for that new customer
     * 
     */
    function testCustomerAutoComplete()
    {
        $this->markTestSkipped("The function in the ajax controller has been deprecated");
        $firstName = rand(0,10000)."fname";
        $lastName = rand(0,10000)."lname";
        $username = $firstName.$lastName;
        $this->CI->db->query("INSERT INTO customer (firstName, lastName, email, username, business_id) VALUES ('{$firstName}', '".$lastName."', '".rand(0,10000)."testuser@gmail.com', '{$username}',3)");
        $customer_id = $this->CI->db->insert_id();
        
        if($customer_id){
            $searchString = $firstName."%20".$lastName;
            $this->_cookie();
            
            $ch = curl_init ($this->url.'/ajax/customer_autocomplete/?term='.$searchString);
            curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close($ch);
            
            $obj = json_decode($output);
            print_r($obj);
            $array = (array)$obj;
            
            $this->assertTrue(in_array($firstName." ".$lastName, $array), 'Customer was not found in the resulting array when searching for the customer based on firstName and last Name');
            
            
            $this->CI->db->query("DELETE FROM customer where customerID = {$customer_id}");
        }

    }
    
    
    /**
     * testLocationAutocomplete simulates searching for lockers in a location
     */
    function testLocationAutocomplete(){
        
        $this->_cookie();
        
        $location = $this->get_random_location(array('business_id'=>self::business_id));
        
        $locationSearchArray = explode(" ", $location->address); 
        $locationSearchString = $locationSearchArray[0].'%20'.$locationSearchArray[1];
        print_r(array('search'=>$locationSearchString));
        
        $ch = curl_init ($this->url.'/ajax/location_autocomplete/?term='.$locationSearchString);
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
        
        $obj = json_decode($output);
        $array = (array)$obj;
        print_r($array);
        
        $this->assertTrue(in_array($location->address." ", $array), "Location address was not in the resulting array when searching for location based on address");
    }
    
    
    
    /**
     * Simulates searching for an order by the order_id
     */
    function testSearchOrders(){
        $this->markTestIncomplete("This test needs informative assertion failure messages and needs to be refactored to not use harcoded setup values.");
       $order = $this->CI->db->query('SELECT * FROM orders WHERE business_id = 3 and customer_id > 10000 ORDER BY RAND() LIMIT 1')->row();
        
       $this->_cookie();
        
        $ch = curl_init ($this->domain.'/ajax/search_orders/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&order_id=".$order->orderID);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
        
        $obj = json_decode($output);
        $array = (array)$obj;
        
        $this->assertEquals($array[0]->customerID, $order->customer_id);
        
    }
    
    
    /**
     * Tests the searching of barcodes
     * 
     * Steps:
     * get a barcode that has an item that exists (inner join on item table)
     * search for it
     */
    function testSearchBarcodes(){
        
        $this->markTestSkipped('This test does not coorelate to the actual function. Need to update this test');
        
        $barcode = $this->CI->db->query('SELECT * FROM barcode 
                                        INNER JOIN item ON itemID = item_id
                                        WHERE barcode.business_id = 3 
                                        AND barcode.item_id > 0 
                                        AND barcode.item_id IS NOT NULL ORDER BY barcodeID DESC LIMIT 1')->row();
        
        $this->_cookie();
        
        $ch = curl_init ($this->domain.'/ajax/search_barcodes/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&barcode={$barcode->barcode}");
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
        
        $obj = json_decode($output);
        $array = (array)$obj;
        
        //die(print_r($array));
        
        $this->assertEquals($array[0]->itemID, $barcode->item_id);
    }
    
    /**
     * simulates searching for a locker by the lockerName
     */
    function testLockerSearch(){
        $this->markTestIncomplete("This test needs informative assertion failure messages and needs to be refactored to not use harcoded setup values.");
        $locker = $this->CI->db->query('SELECT * FROM locker WHERE lockerStatus_id=1 AND lockerName like "%_Unit" ORDER BY RAND() LIMIT 1')->row();
        print_r($locker);
        $this->_cookie();
        
        $ch = curl_init ($this->domain.'/ajax/search_lockers/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&lockerName=".$locker->lockerName);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
        
        $obj = json_decode($output);
        $array = (array)$obj;
        
        $this->assertGreaterThan(0, sizeof($array));
    }
    
    
    
    /**
     * testLocationLockerSearch simulate a search for a lockers in a location
     */
    function testLocationLockerSearch()
    {
        
        $this->markTestIncomplete("This test needs informative assertion failure messages and needs to be refactored to not use harcoded setup values.");
       $location = $this->CI->db->query("SELECT locationID, 
        (select count(*) from locker WHERE location_id = locationID) as totalLockers
         FROM location WHERE business_id = 3 and locationType_id=2
        HAVING totalLockers > 1 ORDER BY rand() LIMIT 1")->row();
       
       
       $this->_cookie();
       
        $ch = curl_init ($this->domain.'/ajax/get_lockers_at_location/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&location_id=".$location->locationID);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
    
        $obj = json_decode($output);
        $array = (array)$obj;
    
       // die(print_r($array));
       $bool = array_key_exists('Lockers', $array);
       $this->assertTrue($bool);
    }
    

    
    
    /**
     * simulates getting all the roles for a business
     */
    function testGetRoles(){
        $this->_cookie();
    $this->markTestIncomplete("This test needs informative assertion failure messages and needs to be refactored to not use harcoded setup values.");
        $ch = curl_init ($this->domain.'/ajax/get_roles/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&business_id=3");
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
    
        $obj = json_decode($output);
        $array = (array)$obj;
    
        //die(print_r($array));
    
        $this->assertEquals($array[0]->name, 'Admin');
    }
    
    
    /**
     * simulates searching for bags that belong to a customer. Search by customer_id
     */
    function testGetBagsOfCustomer(){
        $this->markTestIncomplete("This test needs informative assertion failure messages and needs to be refactored to not use harcoded setup values.");
        $customer = $this->CI->db->query("SELECT customerID, firstName, lastName, username FROM customer INNER JOIN bag ON bag.customer_id = customerID where inactive = 0 AND username != '' and firstName !='' and lastName !='' HAVING count(bag.bagID) > 0  ORDER BY rand() LIMIT 1 ")->row();
        print_r($customer);
        
        $this->_cookie();
    
        $ch = curl_init ($this->domain.'/ajax/get_bags_of_customer/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&customer_id={$customer->customerID}");
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
    
        $obj = json_decode($output);
        $array = (array)$obj;
    
        //die(print_r($array));

        $this->assertGreaterThan(1, sizeof($array));
    }
    
    /**
     * This test needs to be redone because the array changes due to order by address
     */
    function testGetLocations(){
       $this->markTestIncomplete("This test needs informative assertion failure messages and needs to be refactored to not use harcoded setup values.");
        $this->_cookie();
    
        $ch = curl_init ($this->domain.'/ajax/get_locations/');
        curl_setopt ($ch, CURLOPT_COOKIEFILE, $this->ckfile);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close($ch);
    
        $obj = json_decode($output);
        $array = (array)$obj;//print_r($array);
        $this->assertGreaterThan(100,sizeof($array));
    }
    
    
    function _cookie(){
        $this->ckfile = tempnam ("/tmp", "CURLCOOKIE");
        $ch = curl_init ($this->domain."/admin/index/login");
        curl_setopt ($ch, CURLOPT_COOKIEJAR, $this->ckfile);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,"submit=true&email_or_username=411matt@gmail.com&password=1234");
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        return curl_exec ($ch);
    }

}
