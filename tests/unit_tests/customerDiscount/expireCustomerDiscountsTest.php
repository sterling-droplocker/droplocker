<?php
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Customer;

class ExpireCustomerDiscountsTest extends UnitTest{

    public function setUp()
    {
        parent::setUp();
    }


    /**
     * verifies that all customerDiscounts will be exired if they are active and the expire date has passed
     *
     * steps:
     *
     * create 3 customerDiscounts
     * for one of the discounts, delete the customer
     * set them to expire
     * verify that they all were expired
     *
     */
    function testExpireCustomerDiscounts(){
        $pastDate = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y")));

        $customerObj = $this->create_random_customer_on_autopay();
        $customerDiscountID = $this->addCustomerDiscountToCustomer($customerObj->customerID, "20", "dollars", "one time", "test discount", "test discount", $pastDate);
        $customerDiscountObj = new CustomerDiscount($customerDiscountID);
        $this->assertEquals(1, $customerDiscountObj->active, "first discount is not active OR was not created");



        $customerObj2 = $this->create_random_customer_on_autopay();
        $customerDiscountID2 = $this->addCustomerDiscountToCustomer($customerObj2->customerID, "20", "dollars", "one time", "test discount", "test discount", $pastDate);
        $customerDiscountObj2 = new CustomerDiscount($customerDiscountID2);
        $this->assertEquals(1, $customerDiscountObj2->active, "second discount is not active OR was not created");

        $customerObj3 = $this->create_random_customer_on_autopay();
        $customerDiscountID3 = $this->addCustomerDiscountToCustomer($customerObj3->customerID, "20", "dollars", "one time", "test discount", "test discount", $pastDate);
        $customerDiscountObj3 = new CustomerDiscount($customerDiscountID3);
        $this->assertEquals(1, $customerDiscountObj3->active, "third discount is not active OR was not created");

        // delete the first customer
        $customerObj->delete();

        // expire all the customerDiscounts
        $CI = get_instance();
        $CI->load->model("customerDiscount_model");
        $CI->customerDiscount_model->expireCustomerDiscount($this->business_id);

        // get the customerDiscounts again and verify that they have been expired
        $customerDiscountObj = new CustomerDiscount($customerDiscountID);
        $this->assertEquals(0, $customerDiscountObj->active, "first discount was not expired");

    }

}