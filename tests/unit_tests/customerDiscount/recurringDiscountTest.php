<?php
use App\Libraries\DroplockerObjects\Coupon;
use App\Libraries\DroplockerObjects\CustomerDiscount;
use App\Libraries\DroplockerObjects\Customer;

class RecurringDiscountTest extends UnitTest{
    private $test_customer;
    private $coupon;
    private $customerDiscount;
    private $this_month;
    private $last_month;

    public function setUp()
    {
        parent::setUp();

        $this->test_customer = $this->create_random_customer_on_autopay();

        $this->coupon = new Coupon();
        $this->coupon->prefix = UnitTest::get_random_string(2);
        $this->coupon->startDate = new DateTime("now", new DateTimeZone($this->business->timezone));
        $this->coupon->amount = 123;
        $this->coupon->amountType = "dollars";
        $this->coupon->frequency = "one time";
        $this->coupon->description = "123 dollars of next order";
        $this->coupon->code = rand(1000,9999);
        $this->coupon->combine = 1;
        $this->coupon->business_id = $this->business_id;
        $this->coupon->recurring_type_id = 2;
        $this->coupon->save();

        $this->customerDiscount = new CustomerDiscount();
        $this->customerDiscount->amount = $this->coupon->amount;
        $this->customerDiscount->amountType = $this->coupon->amountType;
        $this->customerDiscount->frequency = $this->coupon->frequency;
        $this->customerDiscount->description = $this->coupon->description;
        $this->customerDiscount->business_id = $this->coupon->business_id;
        $this->customerDiscount->recurring_type_id = $this->coupon->recurring_type_id;
        $this->customerDiscount->couponCode = $this->coupon->prefix . $this->coupon->code;
        $this->customerDiscount->customer_id = $this->test_customer->{Customer::$primary_key};

        $this->this_month = new DateTime("now", new DateTimeZone($this->business->timezone));

        $this->last_month = new DateTime("now", new DateTimeZone($this->business->timezone));
        $this->last_month->modify("first day of previous month");

        $last_day_of_last_month = new DateTime($this->last_month->format("Y-m-t"), new DateTimeZone($this->business->timezone));

        $this->customerDiscount->expireDate = $last_day_of_last_month;
        $this->customerDiscount->save();
    }

    /**
     * The following test verifies that reapplying an expired customer discount works as expected.
     */
    public function testMonthlyDiscount() {
        $CI = get_instance();
        $CI->load->model("customerDiscount_model");
        $CI->customerDiscount_model->expireCustomerDiscount($this->business_id);

        $customerDiscounts = CustomerDiscount::search_aprax(array("description" => $this->customerDiscount->description, "customer_id" => $this->customerDiscount->customer_id, "active" => 1));
        $customerDiscount = $customerDiscounts[0];
        $this->assertEquals(2, $customerDiscount->recurring_type_id);
        $end_of_this_month = new DateTime("last day of this month", new DateTimeZone($this->business->timezone));
        $end_of_this_month->setTime(23, 59, 59);
        $customerDiscount->expireDate->setTimeZone(new DateTimeZone($this->business->timezone));
        $this->assertEquals($end_of_this_month, $customerDiscount->expireDate);

    }

    /**
     * Verifies that if a customer no longer exists, the customerDiscount will still get expired, and the new
     * discount will not get created. This test was written due to random errors from the exceptions
     * when the customer object could not be created becuase the customer was deleted.
     * This should never happen on the production server
     */
    public function testMonthlyDiscountNoCustomerFound()
    {
        // delete the customer
        $this->test_customer->delete();

        $CI = get_instance();
        $CI->load->model("customerDiscount_model");
        $CI->customerDiscount_model->expireCustomerDiscount($this->business_id);

        // verify that the customer has no active discounts
        $customerDiscounts = CustomerDiscount::search_aprax(array("description" => $this->customerDiscount->description, "customer_id" => $this->test_customer->customerID, "active" => 1));
        $this->assertCount(0, $customerDiscounts);

    }

    public function tearDown()
    {
        if ($this->coupon instanceof \App\Libraries\DroplockerObjects\Coupon)
        {
            $this->coupon->delete();
        }

        if ($this->customerDiscount instanceof \App\Libraries\DroplockerObjects\CustomerDiscount)
        {
            $this->customerDiscount->delete();
        }
    }
}

?>
