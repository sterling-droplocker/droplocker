<?php

class EmployeeMatchTest extends UnitTest
{

    function __construct(){
        parent::__construct();
        $CI = get_instance();
    }

    function setup(){
        parent::setUp();
    }


    /**
     * create an employee
     * give him an employee credit
     * create an order
     * add items
     * complete the order
     * verify that the order is complete and has the proper values
     */
    function testEmployeeDiscount(){

        $this->markTestSkipped("not properly configured for discount");

        $customer = $this->create_random_customer_on_autopay();

        echo "Customer: ". $customer->customerID."\n";

        $password = $this->get_random_word();

        $employee = new \App\Libraries\DroplockerObjects\Employee();
        $employee->firstName = $customer->firstName;
        $employee->lastName = $customer->lastName;
        $employee->email = $customer->email;
        $employee->password = $password;
        $employee->allowance = 50;
        $employee->customer_id = $customer->customerID;
        $employee->manager_id = 1;
        $employeeID = $employee->save();

        $this->addCustomerDiscountToCustomer($customer->customerID, 50, "percent",
            "until used up", "$50 employee discount", "");

        $employee = new \App\Libraries\DroplockerObjects\Employee($employeeID);

        // verify that we create an employee
        $this->assertEquals($customer->customerID, $employee->customer_id, "Customer id's do not match");

        $order = $this->create_random_order($this->business_id, $customer->customerID);
        $this->addItemToOrder($order);
        $this->addItemToOrder($order);

        echo "Order: ". $order->orderID."\n";

        $this->CI->load->library('discounts');
        $this->CI->discounts->apply_discounts($order);

        $order->orderStatusOption_id = 3;
        $order->orderPaymentStatusOption_id = 3;
        $order->save();

        // refresh the order object
        $order = new \App\Libraries\DroplockerObjects\Order($order->orderID);

        $this->CI->load->model('order_model');
        $options['order_id'] = $order->orderID;
        $options['orderStatusOption_id'] = 9;
        $options['orderPaymentStatusOption_id'] = $order->orderPaymentStatusOption_id;
        $options['locker_id'] = $order->locker_id;
        $options['employee_id'] = 84;

        // This will create an entire new order in which we have to get the new order id
        $this->CI->order_model->update_order($options);

        $maxOrderRow = $this->CI->db->query("SELECT max(orderID) FROM orders WHERE customer_id = ". $customer->customerID)->row();
        $NewOrderObj = new \App\Libraries\DroplockerObjects\Order($maxOrderRow->orderID);

        echo "NewOrderObj: ". $NewOrderObj->orderID."\n";

        $this->assertEquals(10, $NewOrderObj->orderStatusOption_id, "new order is not complete");

    }
}
