<?php
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Claim;

class ClaimTest extends UnitTest
{
    protected $CI;
    private $bagID;
    private $bagNumber;
    private $customerBagNumber;
    private $claim_id;

    CONST customer_id = 21419;
    CONST business_id = 3;

    function __construct(){
        $this->CI = get_instance();
        $this->CI->load->model('claim_model');
        $this->CI->load->model('order_model');
        $this->CI->load->model('orderstatus_model');

        $sql = "SELECT bagNumber FROM bag WHERE business_id = ".self::business_id." ORDER BY cast(`bagNumber` as signed) DESC limit 1";
        $query = $this->CI->db->query($sql);
        $bag = $query->row();
        //$this->bagNumber = "0".(int)$bag->bagNumber + 10;

        $this->customerBagNumber = (int)$bag->bagNumber + 10;

        // make a bag and assign a customer to it
        $this->CI->load->model('bag_model');
        $this->CI->bag_model->clear();
        $this->CI->bag_model->customer_id = self::customer_id;
        $this->CI->bag_model->bagNumber = $this->customerBagNumber;
        $this->bagID = $this->CI->bag_model->insert();
    }


    /**
     * testing new_claim function in claim model
     */
    public function testPlaceClaim()
    {
        $this->markTestSkipped("This test needs to be refactored to not use preexisting data");
        $location = $this->getRandomLockerInLocation('kiosk');
        $locker = new Locker($location->lockerID);
        $location = new Location($location->locationID);
        $inProceessLocker = $location->getInProcessLocker($location->locationID);

        $customer = $this->get_random_customer(self::business_id);

        // make a claim
        $options['customer_id'] = $customer->customerID;
        $options['business_id'] = self::business_id;
        $options['locker_id'] = $locker->lockerID;
        $options['orderType'] = "a:1:{i:0;i:1;}";
        $options['order_notes'] = "test note";
        $this->claim_id = $this->CI->claim_model->new_claim($options);

        $claim = new Claim($this->claim_id);

        $this->assertEquals($customer->customerID, $claim->customer_id, 'The claim customer_id does not match the customer for this test');
        $this->assertEquals($locker->lockerID, $claim->locker_id, 'The claim locker_id is not the same as the locker used in this test.');

    }

    public function tearDown()
    {
        //delete the claim
        $this->CI->claim_model->clear();
        $this->CI->claim_model->claimID = $this->claim_id;
        $this->CI->claim_model->delete();
    }


}
?>
