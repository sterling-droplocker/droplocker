<?php

use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Location;
use App\Libraries\DroplockerObjects\Customer;

class PartialDeliveryTest extends UnitTest
{
    
    function setUp()
    {
        parent::setUp();
        
    }
    
    function testPartialDelivery(){
        $this->markTestSkipped("This test needs to use test fixtures instead of depending on preexisting data and also properly escape parameters in queries.");
        $location = $this->getRandomLockerInLocation();
        $bagNumber = Bag::get_new_bagNumber();
        $customer = $this->get_random_customer($this->business_id);
        $this->addBagTOCustomer($bagNumber, $customer->customerID);
        $locker = new Locker($location->lockerID);
        $this->clearActiveClaimInLocker($locker);
        $this->clearActiveOrdersInLocker($locker->lockerID);
        
        $order_id = $this->createOrder($location->locationID, $location->lockerID, $bagNumber);
        //echo $order_id;
        
        
        $this->CI->load->model('order_model');
        
        $options['orderStatusOption_id'] = 3; //partial delivery
        $options['locker_id'] = $location->lockerID;
        $options['order_id'] = $order_id;
        $options['employee_id'] = 84;
        $options['orderPaymentStatusOption_id'] = 3;
        $this->CI->order_model->update_order($options);
        
        $order = new Order($order_id);
        $order->locker_id = $location->lockerID;
        $order->save();
        
        $this->assertEquals($location->lockerID, $order->locker_id, 'Order locker_id does not match the locker_id used in this test');
        
        $options['orderStatusOption_id'] = 11; //partial delivery
        $options['locker_id'] = $location->lockerID;
        $options['order_id'] = $order_id;
        $options['employee_id'] = 84;
        $options['orderPaymentStatusOption_id'] = 3;
        $this->CI->order_model->update_order($options);
        
        $order = new Order($order_id);
        
        $location = new Location($location->locationID);
        $inProcessLocker = $location->getInProcessLocker($location->locationID);
        
        $this->assertEquals(11, $order->orderStatusOption_id, 'Order was not placed in partial deliver');
        $this->assertEquals($inProcessLocker->lockerID, $order->locker_id, 'The order is not in the inProcess Locker');
    }
    
}