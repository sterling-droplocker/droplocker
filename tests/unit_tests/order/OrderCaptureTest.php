<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\CreditCard;
use App\Libraries\DroplockerObjects\Transaction;


class OrderCaptureTest extends UnitTest
{
    protected $CI;
    private $options;
    private $customer;
    private $location;
    private $bagNumber;
    protected $business_id;
    private $processorName;
    private $processor;
    private $creditCardNumber;
    private $creditCard;
    private $orderObj;
    
    function __construct(){
        $this->processorName = 'paypro';
        $this->creditCardNumber = '4217651111111119';
    }
    
    
    function setUp(){
        
        parent::setUp();
        
        // make sure the processor is paypro and we are in test mode
        update_business_meta(3, 'processor', 'paypro');
        update_business_meta(3, 'paypro_test_token', '3374397FCE62500F388AEDFF8D56AB6DCFC1259444B0C3F6E8A7492787DA5AE49A56BB50CDF186E798');
        update_business_meta(3, 'paypro_mode', 'test');
        
        $this->customer = $this->create_random_customer_on_autopay();
        
        //get a mock credit card for testing
        $this->creditCard = $this->get_mock_creditCard($this->customer, $this->creditCardNumber);
        
    }

    
    function tearDown(){
        // delete the customer
        if (isset($this->customer) && $this->customer instanceOf \App\Libraries\DroplockerObjects\Customer);
        {
            $this->customer->delete();
        }
    
        // delete the credit card
        $creditCard = CreditCard::search_aprax(array('customer_id'=>$this->customer->customerID));
        if(isset($creditCard[0]))
            $creditCard[0]->delete();
    
        //delete the transaction
        $transaction = Transaction::search_aprax(array('customer_id'=>$this->customer->customerID));
        if(isset($transaction[0]))
            $transaction[0]->delete();
        
        //delete the transaction
        $transaction = Transaction::search_aprax(array('customer_id'=>$this->customer->customerID));
        if(isset($transaction[0]))
            $transaction[0]->delete();
        
        if(!empty($this->orderObj->orderID)){
            $this->orderObj->delete();
        }
        
    
    }
    
    
}