<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Customer;


class DFUTest extends UnitTest
{
    protected $CI;
    private $url;
    private $domain;
    private $options;
    private $customer;
    private $location;
    private $bagNumber;
    protected $business_id;

    //const business_id = 3;

    function __construct(){

        $this->CI = get_instance();

     // make sure the processor is paypro and we are in test mode
        update_business_meta(3, 'processor', 'paypro');
        update_business_meta(3, 'paypro_test_token', '3374397FCE62500F388AEDFF8D56AB6DCFC1259444B0C3F6E8A7492787DA5AE49A56BB50CDF186E798');
        update_business_meta(3, 'paypro_mode', 'test');

        $this->options['inactive'] = 0;
        $this->options['invoice'] = 0;
        $this->options['autoPay'] = 1;
    }

    function setUp(){
    	parent::setUp();
        if(gethostname()=='development')
        {
            $this->domain = '/var/www/html/droplocker.info';
            $this->url = 'https://droplocker.info';
        }
        else if (gethostname() == "Ariks-Mac-mini.local")
        {
            $this->domain = "/Library/WebServer/Documents/dropLocker/dropLocker";
            $this->url = "http://droplocker.loc";
        }
        else if (gethostname() == "AdamPowerComputer")
        {
            $this->domain = "/var/www/html/droplocker";
            $this->url = "http://droplocker.loc";
        }
        else
        {
            $this->domain = '/home/matt/workspace/dropLocker';
            $this->url = 'http://droplocker.loc';
        }

        $this->customer = $this->get_random_customer($this->business_id,$this->options);
        $this->location = $this->getRandomLockerInLocation();
        $this->bagNumber = Bag::get_new_bagNumber();
    }



    /**
     * tests a delivery of an order that has not been captured.
     * This test will successfully capture the payment and set the order to complete
     *
     * Steps:
     * - create an order
     * - set to inventoried
     * - make sure we have a customer with a valid creditcard on file
     * - insert the order into the droid post delivery table
     * - process the delivery
     * - test that the orders has been paid
     * - test that the order status is complete (9)
     */
    function testDFUSuccess(){

        // random customer
        $customer = $this->customer;
        $location = $this->location;
        $locker = new Locker($location->lockerID);
        $bagNumber = $this->bagNumber;

        $bag = $this->addBagTOCustomer($bagNumber, $customer->customerID);

        $this->clearActiveOrdersInLocker($locker->lockerID);

        // create the order
        if(!$order_id = $this->createOrder($location->locationID, $locker->lockerID, $bagNumber)){
            throw new \Exception('Order_id is missing, order was not created');
        }

        $order = new Order($order_id);
        if(empty($order)){
            throw new \Exception("Order not found");
        }

        echo $order_id;

        $this->addItemToOrder($order);

        // add creditcard to the customer
        $this->addCreditCardToCustomer($customer);

        // Set the status to inventoried and payment remains uncaptured
        $options['orderStatusOption_id'] = 3;
        $options['locker_id'] = $order->locker_id;
        $options['order_id'] = $order->orderID;
        $options['employee_id'] = 84;
        $this->CI->order_model->update_order($options);

        // now we need to process try to deliver the order
        // Do a post from the phone that this order was delivered
        $transaction_id = md5(uniqid().date("Y-m-d H:i:s")).'84';
        $post_data = array( 'BAGNUMBER'=>$bagNumber,
                'LOCKERNAME'=>$locker->lockerName,
                'DELMANID'=>'unomateo',
                'PASSWORD'=>md5('1234'),
                'DELTIME'=>urlencode(date("Y-m-d H:i:s")),
                'PARTIAL'=>'',
                'TRANSID'=>$transaction_id,
                'BUSINESSID'=>$customer->business_id,
                'EMPLOYEEID'=>84,
                'TYPE'=>'post',
                'ORDERID'=>$order->orderID);

        $fields_string = '';
        foreach($post_data as $key=>$value) {
            $fields_string .= $key.'='.$value.'&';
        }
        $query = rtrim($fields_string,'&');

        $url = $this->url.'/admin/mobile_2_4/postDeliveries/?'.$query;
        $result = $this->curl_get($url);

        $this->assertEquals($transaction_id, $result->transaction_id, 'Transaction_id return after processing delivery is correct');

        $this->_process();

        $order = $this->CI->order_model->get(array('orderID'=>$order->orderID, 'business_id'=>$customer->business_id));
        if(empty($order)){
            throw new \Exception("Order not found");
        }

        $this->assertEquals($customer->customerID, $order[0]->customer_id, 'order has the proper customer_id');
        $this->assertEquals(10, $order[0]->orderStatusOption_id, 'order has been completed');

    }

    /**
     * tests an orders that is delivered before its captured and the credit card failed when trying to capture it
     * This test is exactly like the test for success, but I make sure the credit card will fail
     */
    function testDFUCreditCardFail(){

        $customer = $this->customer;
        $location = $this->location;
        $locker = new Locker($location->lockerID);
        $bagNumber = $this->bagNumber;
        $bag = $this->addBagTOCustomer($bagNumber, $customer->customerID);

        $this->clearActiveOrdersInLocker($locker->lockerID);

        // create the order
        if(!$order_id = $this->createOrder($location->locationID, $locker->lockerID, $bagNumber)){
            throw new \Exception('Order_id is missing, order was not created');
        }

        $order = new Order($order_id);
        if(empty($order)){
            throw new \Exception("Order not found");
        }

        echo $order_id;

        $this->addItemToOrder($order);

        // add creditcard to the customer
        $creditCardID = $this->addCreditCardToCustomer($customer);

        // update the card so it fails
        $sql = "UPDATE creditCard SET payer_id = '1349410500|0000013a-2f24-ede6-0000-00007310b2bb' WHERE creditCardID = {$creditCardID}";
        $this->CI->db->query($sql);

        // Set the status to inventoried and payment remains uncaptured
        $options['orderStatusOption_id'] = 3;
        $options['locker_id'] = $order->locker_id;
        $options['order_id'] = $order->orderID;
        $options['employee_id'] = 84;
        $this->CI->order_model->update_order($options);

        // now we need to process try to deliver the order
        // Do a post from the phone that this order was delivered
        $transaction_id = md5(uniqid().date("Y-m-d H:i:s")).'84';
        $post_data = array( 'BAGNUMBER'=>$bagNumber,
                'LOCKERNAME'=>$locker->lockerName,
                'DELMANID'=>'unomateo',
                'PASSWORD'=>md5('1234'),
                'DELTIME'=>urlencode(date("Y-m-d H:i:s")),
                'PARTIAL'=>'',
                'TRANSID'=>$transaction_id,
                'BUSINESSID'=>$customer->business_id,
                'EMPLOYEEID'=>84,
                'TYPE'=>'post',
                'ORDERID'=>$order->orderID);

        $fields_string = '';
        foreach($post_data as $key=>$value) {
            $fields_string .= $key.'='.$value.'&';
        }
        $query = rtrim($fields_string,'&');

        $url = $this->url.'/admin/mobile_2_4/postDeliveries/?'.$query;
        $result = $this->curl_get($url);

        $this->assertEquals($transaction_id, $result->transaction_id, 'Transaction_id return after processing delivery is correct');

        $this->_process();

        $order = $this->CI->order_model->get(array('orderID'=>$order->orderID, 'business_id'=>$customer->business_id));
        if(empty($order)){
            throw new \Exception("Order not found");
        }

        $this->assertEquals($customer->customerID, $order[0]->customer_id, 'order has the proper customer_id');
        $this->assertEquals(13, $order[0]->orderStatusOption_id, 'order status has been set to delivery status fail');

    }


    function testDFUOrderStatusIsInPaymentHold(){

        $customer = $this->customer;
        $location = $this->location;
        $locker = new Locker($location->lockerID);
        $bagNumber = $this->bagNumber;
        $bag = $this->addBagTOCustomer($bagNumber, $customer->customerID);

        $this->clearActiveOrdersInLocker($locker->lockerID);

        // create the order
        if(!$order_id = $this->createOrder($location->locationID, $locker->lockerID, $bagNumber)){
            throw new \Exception('Order_id is missing, order was not created');
        }

        $order = new Order($order_id);
        if(empty($order)){
            throw new \Exception("Order not found");
        }

        echo $order_id;

        $this->addItemToOrder($order);

        // add creditcard to the customer
        $creditCardID = $this->addCreditCardToCustomer($customer);



        // Set the status to inventoried and payment remains uncaptured
        $options['orderStatusOption_id'] = 7;
        $options['locker_id'] = $order->locker_id;
        $options['order_id'] = $order->orderID;
        $options['employee_id'] = 84;
        $this->CI->order_model->update_order($options);

        // now we need to process try to deliver the order
        // Do a post from the phone that this order was delivered
        $transaction_id = md5(uniqid().date("Y-m-d H:i:s")).'84';
        $post_data = array( 'BAGNUMBER'=>$bagNumber,
                'LOCKERNAME'=>$locker->lockerName,
                'DELMANID'=>'unomateo',
                'PASSWORD'=>md5('1234'),
                'DELTIME'=>urlencode(date("Y-m-d H:i:s")),
                'PARTIAL'=>'',
                'TRANSID'=>$transaction_id,
                'BUSINESSID'=>$customer->business_id,
                'EMPLOYEEID'=>84,
                'TYPE'=>'post',
                'ORDERID'=>$order->orderID);

        $fields_string = '';
        foreach($post_data as $key=>$value) {
            $fields_string .= $key.'='.$value.'&';
        }
        $query = rtrim($fields_string,'&');

        $url = $this->url.'/admin/mobile_2_4/postDeliveries/?'.$query;
        $result = $this->curl_get($url);

        $this->assertEquals($transaction_id, $result->transaction_id, 'Transaction_id return after processing delivery is correct');

        $this->_process();

        $order = $this->CI->order_model->get(array('orderID'=>$order->orderID, 'business_id'=>$customer->business_id));
        if(empty($order)){
            throw new \Exception("Order not found");
        }

        $this->assertEquals($customer->customerID, $order[0]->customer_id, 'order has the proper customer_id');
        $this->assertEquals(13, $order[0]->orderStatusOption_id, 'order status has been set to delivery status fail');
    }


    /**
     * simulates the cron job
     */
    function _process(){
        system('php '.$this->domain.'/index.php admin/mobilecron processAllDeliveries');
    }

}
