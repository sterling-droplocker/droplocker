<?php

class LanguageView_HelperTest extends UnitTest {
    private $languageKey1;
    private $languageView1;
    private $languageView1_parameter1_name;
    private $languageView1_parameter2_name;
    private $business_language;
    private $language;
    private $customer;
    
    public function setUp() {
        parent::setUp();
        $this->languageView1_parameter1_name = "parameter1";
        $this->languageView1_parameter2_name = "parameter2";
        
        $this->business = UnitTest::create_random_business();
        
        
        $this->languageView1 = self::create_random_languageView();
        $this->languageKey1 = self::create_random_languageKey($this->languageView1->languageViewID, "This is my first parameter, %parameter1%, and this is my second parameter, %parameter2%");
        $this->language = self::create_random_language();
        $this->business_language = self::create_random_business_language($this->business->businessID, $this->language->languageID);
        $this->customer = self::create_random_customer_on_autopay($this->business->businessID);
        
        $this->business->default_business_language_id = $this->business_language->business_languageID;
        $this->business->save();
    }
    
    public function test_get_translation_with_business_custom_text() {
        $parameter1 = UnitTest::get_random_word(rand(1,5));
        $parameter2 = UnitTest::get_random_word(rand(1,5));
        
        $business_translation = sprintf("'%%%s%%' '%%%s%%'", $this->languageView1_parameter1_name, $this->languageView1_parameter2_name);
        $expected_business_translation_result = sprintf("'%s' '%s'", $parameter1, $parameter2);
        
        UnitTest::create_random_languageKey_business_language($this->business_language->business_id, $this->languageKey1->languageKeyID, $this->business_language->language_id, $business_translation);
        $translation_result = get_translation($this->languageKey1->name, $this->languageView1->name, array($this->languageView1_parameter1_name => $parameter1, $this->languageView1_parameter2_name => $parameter2), "This is the fallback text", $this->customer->customerID);
        $this->assertEquals($expected_business_translation_result, $translation_result);
                
    }
    
    public function test_get_translation_with_default_text() {
        $parameter1 = UnitTest::get_random_word(rand(1,5));
        $parameter2 = UnitTest::get_random_word(rand(1,5));
        $translation = get_translation($this->languageKey1->name, $this->languageView1->name, array($this->languageView1_parameter1_name => $parameter1, $this->languageView1_parameter2_name => $parameter2)); 
        $this->assertEquals("This is my first parameter, $parameter1, and this is my second parameter, $parameter2", $translation);     
    }

}

?>
