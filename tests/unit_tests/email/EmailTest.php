<?php
use App\Libraries\DroplockerObjects\Bag;
use App\Libraries\DroplockerObjects\Order;
use App\Libraries\DroplockerObjects\Locker;
use App\Libraries\DroplockerObjects\Customer;
use App\Libraries\DroplockerObjects\DropLockerEmail;


class EmailTest extends UnitTest
{
    const business_id = 3;
    protected $CI;
    private $customer;

    function __construct(){
        $this->CI = get_instance();
    }

    function setUp(){
        $options['inactive'] = 0;
        $this->customer = $this->get_random_customer(self::business_id, $options);

    }

    /**
     * Tests that sms messages do not have html tags
     */
    function testStripTags(){
        $this->markTestSkipped("The test is failing and Matt has refused to fix it.");
        $ci = $this->CI;

        $customer = UnitTest::create_random_customer_on_autopay($this->business_id);

        $ci->load->library('email_actions');
        $string = "String with a <strong>tag</strong>";
        $stringWithoutTags = strip_tags($string);

        $emailID = $ci->email_actions->send_sms($customer, $string, self::business_id, array());
        $email = new DropLockerEmail($emailID);

        $this->assertEquals($stringWithoutTags, $email->body, "SMS message has had the html tags removed");
    }


    function testBusinessMacros(){

        $options['business_id'] = self::business_id;
        $data = get_macros($options);
        print_r($data);

    }

    function testLockerLootMacros(){
        $this->markTestSkipped("The test is failing and Matt has refused to fix it.");
        $lockerLoot = $this->get_random_locker_loot();
        $options['lockerLoot_id'] = $lockerLoot->lockerLootID;
        $data = get_macros($options);
        print_r($data);

    }





}