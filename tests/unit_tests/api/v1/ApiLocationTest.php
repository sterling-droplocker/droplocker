<?php

class ApiLocationTest extends ApiUnitTest {
    /**
     * The following test verifies the functionality location/search
     */
    public function testSearchLocation() {
        $location = $this->create_random_location($this->business_id);
        $search_text = $location->address;;
        $path = "location/search";

        $url = $this->webserviceURL . $path;
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);


        $data = array(
            'address' => $search_text,
            'business_id' => $this->business_id,
            "token" => $this->token
            
        );
        
        $data['signature'] = $this->generateSignature($data, $this->secretKey);

        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);
        $this->assertEquals("success", $result->status, "The 'success' parameter was not found in the response: $output");
        $this->assertEquals( "Name Match", $result->type);
        $this->assertObjectHasAttribute("locations", $result);
    }

    /**
     * The following test verifies the functionality of customer/sendForgotPasswordEmail
     */
    public function testSendForgotPasswordEmail() {
        $customer = $this->create_random_customer_on_autopay($this->business_id);
        $path = "customer/sendForgotPasswordEmail";
        $url = $this->webserviceURL . $path;
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);
        $data = array( 'business_id' => $this->business_id, 'token' => $this->token, "email" => $customer->email);
        $data['signature'] = $this->generateSignature($data, $this->secretKey);
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);
        $this->assertObjectHasAttribute("status", $result, "Did not find the 'status' attribute in the response");
        $this->assertObjectHasAttribute("message", $result, "Did not find the expected 'message' attribute in the response");
        $this->assertEquals("success", $result->status, "The 'status' parameter is not the expected result of 'success'");
    }
}

?>
