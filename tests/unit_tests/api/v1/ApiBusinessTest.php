<?php

class ApiBusinessTest extends ApiUnitTest {
    public function setUp() {
        parent::setUp();
        $this->create_random_location($this->business_id);
        
    }
    public function testGetLocations() {
        
        $path = "business/getLocations";
        $url = $this->webserviceURL . $path;
        
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);
        
        $parameters = array('sessionToken' => $this->sessionToken, 'token' => $this->token);
        $signature = $this->generateSignature($parameters, $this->secretKey);
        $parameters['signature'] = $signature;
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $parameters);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);
        
        $this->assertObjectHasAttribute("message", $result, "The 'message' parater was not found in the response.");
        $this->assertObjectHasAttribute("data", $result, "The 'data' parameter was not found in the response.");
        $this->assertEquals("success", $result->status, "The status message of 'success' was not found in the database.");
        
        
        
    }
}

?>
