<?php
use App\Libraries\DroplockerObjects\Customer;
class ApiCustomerTest extends ApiUnitTest {
    /**
     * 
     * The follwoing test verifies the funcitonality for creating a customer and validating a customer using the customer/create customer/validate
     * This test expects the response in the following format:
     *      @status string Must be value 'success'
     *      @message string
     *      @data array Must contain the following properties
     *          @customer_id
     *          @sessionToken
     *          @sessionTokenExpireDate
     */
    public function testCreateAndValidate() {
        $path = "customer/create";
        $url = $this->webserviceURL . $path;
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);

        $word = UnitTest::get_random_word();
        $email = "webdriver_$word@example.com";
        $password = UnitTest::get_random_word();
        $data = array(
            "email" => $email,
            "password" => $password,
            "token" => $this->token,
            "business_id" => 3
        );
        $data['signature'] = $this->generateSignature($data, $this->secretKey);
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);

        $this->assertEquals("success", $result->status, "The status message of 'success' was not found in the response" . print_r($result, 1));
        $this->assertObjectHasAttribute("data", $result, "The 'data' property was not found in the response");
        $data = $result->data;
        $this->assertObjectHasAttribute("customer_id", $data, "The 'customer_id' property was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("sessionToken", $data, "The 'sessionToken' property was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("sessionTokenExpireDate", $data, "The 'sessionTo=kenExpireDate' property was not found in the response." . print_r($result, 1));
        
        $data = array(
            "username" => $email,
            "password" => $password,
            "token" => $this->token,
            "business_id" => 3
        );
        $data['signature'] = $this->generateSignature($data, $this->secretKey);
        
        $path = "customer/validate";
        $url = $this->webserviceURL . $path;
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);
        
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);
        
        $this->assertEquals("success", $result->status, "The status message of 'success' was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("customer_id", $result, "The 'customer_id' property was not found in the response." . print_r($result, 1));
        
        $this->assertObjectHasAttribute("sessionToken", $result, "The 'sessionToken' property was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("sessionTokenExpireDate", $result, "The 'sessionTokenExpireDate' property was not found in the response." . print_r($result, 1));
        
        $customers = Customer::search_aprax(array("email" => $email));
        foreach ($customers as $customer) {
            $customer->delete();
        }
    }
    /**
     * The following test verifies the fucntionality for retrieving the orders for a customer.
     */
    public function testGetOrders() {
        $path = "customer/getOrders";
        $url = $this->webserviceURL . $path;
        $data = array("token" => $this->token, "sessionToken" => $this->sessionToken);
        $signature = $this->generateSignature($data, $this->secretKey);
        $data['signature'] = $signature;
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);
        $this->assertNotNull(result, "Result is not a JSON encoded object");
        $this->assertObjectHasAttribute("status", $result, "The 'status' property was not found in the response.");
        $this->assertEquals("success", $result->status, "The status message of 'success' was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("data", $result, "The 'data' property was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("message", $result, "The 'message' property was not found in the response." . print_r($result,1));
        $this->assertEquals($result->message, "Orders for {$this->test_customer->firstName} {$this->test_customer->lastName}");
    }
    /**
     * The following test verifies the functionality for retrieving the claims for a customer.
     */
    public function testGetClaims() {
        $path = "customer/getClaims";
        $url = $this->webserviceURL . $path;
        curl_setopt($this->curl_connection, CURLOPT_URL, $url);
        $data = array("token" => $this->token, "sessionToken" => $this->sessionToken);
        $signature = $this->generateSignature($data, $this->secretKey);
        $data['signature'] = $signature;
        curl_setopt($this->curl_connection, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($this->curl_connection);
        $result = json_decode($output);
        $this->assertNotNull($result, "Result is not a JSON encoded object");
        $this->assertObjectHasAttribute("status", $result, "The 'status' property was not found in the response.");
        $this->assertEquals("success", $result->status, "The status message of 'success' was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("data", $result, "The 'data' property was not found in the response." . print_r($result, 1));
        $this->assertObjectHasAttribute("message", $result, "The 'message' property was not found in the response" . print_r($result,1));
        $this->assertEquals($result->message, "Claims for {$this->test_customer->firstName} {$this->test_customer->lastName}");
    }
}
?>

