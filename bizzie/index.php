



<?php
/**
 * Business Account Template
 */
?>
<!DOCTYPE html>

<html>
    <head>
        <title>bizzie</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       


        <style>
        body{
        margin:0px;
        }
        header, footer, nav { 
            display: block;
            font:18px/18px 'Trebuchet MS', Arial, Verdana, Helvetica, sans-serif; 
        }
        
        #header{
            margin-top:25px;
            padding:0;
            background:#333;
            height:58px;
            width:100%;
            box-shadow:0 2px 2px #333;
            -webkit-box-shadow:0 2px 2px #333;
            -moz-box-shadow:0 2px 2px #333;
            
           
        }
        .header-holder{
            overflow:hidden;
            padding:0 10px 0 300px;
            margin:0 auto;
            width:970px;
            position:relative;
        }
        .logo_holder{
            width:970px;
            margin:0 auto;
            position:relative;
        }
        
        
        .logo{
            background:url(http://droplocker.loc/images/bizzie/bizzie_logo.png) no-repeat;
            width:120px;
            height:120px;
            margin: 0 20px 0 20px;
            text-indent:-9999px;
            background-size:100%;
            float:left;
            position:absolute;
            top:-25px;
        }
        .logo a{
            display:block;
            height:100%;
        }
        
        #nav ul{
            padding:14px 0 0;
            margin:0 0px 0 0;
            list-style:none;
            font-size:19px;
            letter-spacing:-1px;
        }
        #nav li{
            float:left;
            margin:4px 0 0 0px;
        }
        #nav li:first-child{margin:4px;}
        #nav a{
            float:left;
            color:#fff;
            padding:0 43px 0 0;
        }
        #nav span{
            float:left;
            height:39px;
            line-height:30px;
            padding:0 0 0 35px;
        }
        #nav a:hover,
        #nav .active a{background:url(https://dashlocker.com/images/bg-nav-hover.png) no-repeat 100% 0;}
        #nav a:hover span,
        #nav .active a span{background:url(https://dashlocker.com/images/bg-nav-hover.png) no-repeat 0 -39px;}
        #nav em{
            float:left;
            font-style:normal;
            cursor:pointer;
            margin:0 -15px;
            position:relative;
        }
        
        #footer{
        	margin:0;
        	padding:0;
        	color:#FFF;
        	font-size:11px;
        	background:#bdd73c;
        	width:100%;
        	bottom:0px;
        	height:300px;
        	text-align:center;
        }
        
        .footer_logo_holder{
            width:970px;
            margin:0 auto;
            position:relative;
        }
        
        .footer_logo{
             background:url(http://droplocker.loc/images/bizzie/about_time.png) no-repeat;
            width:200px;
            height:200px;
            margin: 0 20px 0 20px;
            text-indent:-9999px;
            background-size:100%;
            float:left;
            position:absolute;
            top:-75px;
        }
        .footer-holder{
            margin:0 auto;
        	overflow:hidden;
        	padding:27px 1px 15px 2px;
        	width:970px;
        	height:300px;
        	background-color:#000;
        	position:relative;
        }
        #about_time{
            position:absolute;
            left:0px;
            bottom:150px;
        }
        
        .card-list{
        	padding:0 210px 0;
        	margin:0;
        	list-style:none;
        	float:right;
        }
        .card-list li{
        	float:left;
        	margin:0 0 0 4px;
        }
        #footer p{margin:0;}
        
        /****************************/
        /* Account section styles
        /****************************/
        
        #default_account_nav{
            padding:5px;
            margin:0px;
            width:300px;
        }
        
        #default_account_nav ul{
            margin-left: 0;
            padding-left: 0;
            list-style-type: none;
            width:100%;
        }
        
        
        #default_account_nav a{
            display: block;
            padding: 5px;
            margin:2px 0px;
            color:#333;
            background-color: #eaeaea;
            border-bottom: 1px solid #eee;
            border-radius:3px;
        }
    
        </style>

           
    </head>
    <body>
        
        
        <!-- Desktop Header -->
        <?php 
            /**
             * The header will be pulled from the database
             * Hardcoded for testing
             */
        ?>  
        <div id="header">
            <div class="logo_holder">
            <strong class="logo"><a href="https://dashlocker.com">Load. Lock. Live. DashLocker</a></strong>
            </div>
            <div class="header-holder">
                
                <nav id="nav">
                    <ul>
                        <li class="active"><a href="https://dashlocker.com/index.html"><span><em>Home</em></span></a></li>
                        <li><a href="http://www.dashlocker.com/#/howitworks"><span><em>How It Works</em></span></a></li>
                        <li><a href="http://www.dashlocker.com/#/services"><span><em>Our Services</em></span></a></li>
                        <li><a href="http://www.dashlocker.com/#/prices"><span><em>Prices</em></span></a></li>
                        <li><a href="http://www.dashlocker.com/#/locations"><span><em>Locations</em></span></a></li>
                        <li><a href="http://www.dashlocker.com/#/faq"><span><em>FAQ</em></span></a></li>
                    </ul>
                </nav>
            </div>
         
        </div>
        <!-- End Desktop header -->
        <br>
        
        <div style='min-height:500px;'>
        
        </div>
        <div id="footer">
            <div class="footer_logo_holder">
            <strong class="footer_logo"><a href="https://dashlocker.com">Load. Lock. Live. DashLocker</a></strong>
            </div>
            <div class='footer_holder'>
            
                
            </div>
            <?php //echo $footer?>
            
        </div>
        
        
       
    </body>
</html>