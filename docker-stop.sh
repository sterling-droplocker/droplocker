#!/bin/bash

set -e

echo
echo "-- Stopping droplocker-5.6 server"
sudo docker ps -q --filter ancestor="droplocker-5.6" | xargs -r sudo docker stop
sudo docker rm -v $(sudo docker ps -a -q -f status=exited)
