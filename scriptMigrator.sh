#!/bin/bash

echo -n "Name: "
read name

NAME=$(echo `date -u +%Y-%m-%d-%H.%M.%S` $name.sql | sed -e 's/ /_/g')

#touch $NAME
#nano -w $NAME

nano scripts/$NAME # change to your editor

echo New migration created: $NAME
